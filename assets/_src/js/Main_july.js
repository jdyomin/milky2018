(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [];


// symbols:



(lib.bg_part1 = function() {
	this.initialize(img.bg_part1);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,369,271);


(lib.bg_part2 = function() {
	this.initialize(img.bg_part2);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,833,333);


(lib.grass = function() {
	this.initialize(img.grass);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,560,74);


(lib.pack = function() {
	this.initialize(img.pack);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,334,438);


(lib.tree = function() {
	this.initialize(img.tree);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,431,599);// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.Symbol7 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("Aj0g3IBVlDIBVgaIBlgDQAygeANAAQAJAAALAcQAGAOAEAOIAZAjIAtFEIAlB9IALAWIgHAbIgJAOIAIArIAPAUIgFAjIgOAhIgSAaIgWAYIgLAkIgkA0IgIgEIgfgxIgUgYIgJALIgZAYIhgAtg");
	mask.setTransform(-44.275,75.625);

	// Layer_1
	this.instance = new lib.tree();
	this.instance.parent = this;
	this.instance.setTransform(-215.5,-299.5);

	var maskedShapeInstanceList = [this.instance];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol7, new cjs.Rectangle(-68.8,31.8,49.099999999999994,87.7), null);


(lib.Symbol6 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(11.5,1,1).p("ABTCAQh8h8gpiD");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	// Layer_3
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(11.5,1,1).p("ABfCZQjtgsA8kF");
	this.shape_1.setTransform(-1.245,-2.575);

	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(1));

	// Layer_2
	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(11.5,1,1).p("AgkA7IBJh1");
	this.shape_2.setTransform(11.475,6.325);

	this.timeline.addTween(cjs.Tween.get(this.shape_2).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol6, new cjs.Rectangle(-16.5,-23.6,37.5,42.1), null);


(lib.Symbol5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_3
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(11.5,1,1).p("AAoAKIhPgT");
	this.shape.setTransform(-6.975,-2.3);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	// Layer_2
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(11.5,1,1).p("ACGAwQiRgVh6hK");
	this.shape_1.setTransform(5.925,-3.475);

	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(1));

	// Layer_1
	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(11.5,1,1).p("ABKBTQhPgrhEh6");
	this.shape_2.setTransform(-0.025,0);

	this.timeline.addTween(cjs.Tween.get(this.shape_2).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol5, new cjs.Rectangle(-16.7,-14,41.8,28), null);


(lib.Symbol1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.bg_part1();
	this.instance.parent = this;
	this.instance.setTransform(-184.5,-135.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol1, new cjs.Rectangle(-184.5,-135.5,369,271), null);


(lib.Symbol_4_Layer_3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_3
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(11.5,1,1).p("Ai6giQAcA5BCAbQA7AZBCgIQBAgIArgjQAuglABg3");
	this.shape.setTransform(39.25,38.797);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(11.5,1,1).p("AjEgdQAmA5BBAWQA7AUBBgIQBBgIArgfQAvgfALg3");
	this.shape_1.setTransform(39.3,38.2143);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(11.5,1,1).p("AjNgYQAuA6BAAQQA7APBBgIQBBgIAtgZQAugaAUg5");
	this.shape_2.setTransform(39.35,37.6672);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(11.5,1,1).p("AjUgUQA1A7A+ALQA7ALBCgIQBBgIAtgVQAvgWAdg5");
	this.shape_3.setTransform(39.4,37.198);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(11.5,1,1).p("AjbgRQA8A7A+AIQA7AGBBgIQBBgIAtgRQAvgSAkg5");
	this.shape_4.setTransform(39.425,36.8207);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(11.5,1,1).p("AjhgOQBBA7A+AEQA7ADBAgIQBCgIAugNQAvgPAqg5");
	this.shape_5.setTransform(39.475,36.5159);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(11.5,1,1).p("AjngMQBHA8A8ABQA8AABBgIQBBgIAvgLQAvgLAwg7");
	this.shape_6.setTransform(39.5,36.2765);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(11.5,1,1).p("AjrgLQBLA8A8gCQA8gCBAgIQBCgIAugIQAwgJAzg7");
	this.shape_7.setTransform(39.5,36.1518);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(11.5,1,1).p("AjugKQBOA8A8gDQA7gEBAgIQBCgIAvgHQAvgHA4g7");
	this.shape_8.setTransform(39.525,36.0345);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(11.5,1,1).p("AjwgJQBPA8A8gFQA7gFBAgIQBCgIAvgFQAwgGA6g7");
	this.shape_9.setTransform(39.55,35.9438);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(11.5,1,1).p("AjxgJQBQA8A8gFQA7gGBBgIQBBgIAwgFQAvgFA8g7");
	this.shape_10.setTransform(39.55,35.9226);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(11.5,1,1).p("AjygJQBRA8A8gGQA7gGBBgIQBBgIAwgEQAvgFA8g7");
	this.shape_11.setTransform(39.55,35.9017);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(11.5,1,1).p("AjpgMQBJA8A8AAQA8gBBAgIQBCgIAvgJQAvgLAyg6");
	this.shape_12.setTransform(39.5,36.225);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(11.5,1,1).p("AjggPQBAA7A+AFQA7AFBBgIQBBgIAugPQAvgQAog5");
	this.shape_13.setTransform(39.45,36.606);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(11.5,1,1).p("AjYgTQA5A7A+AKQA7AJBCgIQBBgIAtgTQAvgVAgg4");
	this.shape_14.setTransform(39.4,37.0439);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(11.5,1,1).p("AjRgWQAyA6A/AOQA7ANBBgIQBBgIAtgXQAvgYAZg5");
	this.shape_15.setTransform(39.375,37.4311);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(11.5,1,1).p("AjLgZQAtA6A/ARQA7AQBCgIQBAgIAtgaQAugbATg5");
	this.shape_16.setTransform(39.325,37.7833);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(11.5,1,1).p("AjGgcQAoA6BAAUQA7ATBBgIQBBgIAsgdQAugeANg4");
	this.shape_17.setTransform(39.3,38.0958);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#000000").ss(11.5,1,1).p("AjBgeQAjA5BBAXQA7AVBBgIQBBgIAsgfQAughAJg3");
	this.shape_18.setTransform(39.3,38.3353);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#000000").ss(11.5,1,1).p("Ai+ggQAgA5BBAZQA7AXBCgIQBAgIAsghQAugjAFg3");
	this.shape_19.setTransform(39.275,38.5299);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#000000").ss(11.5,1,1).p("Ai8giQAeA5BCAbQA7AYBCgIQBAgIArgjQAugkADg2");
	this.shape_20.setTransform(39.25,38.7);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#000000").ss(11.5,1,1).p("Ai7giQAdA5BCAbQA7AZBCgIQBAgIArgkQAugkACg3");
	this.shape_21.setTransform(39.25,38.772);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},23).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_11}]},24).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape}]},1).wait(31));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_4_Layer_2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(11.5,1,1).p("AiiBTQgFhRA1guQAugpBDADQBAADAvAqQAzAsACA8");
	this.shape.setTransform(65.6533,-3.6851);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(11.5,1,1).p("AiiBWQgFhRA2gwQAxgtBBADQBAAEAvAtQAxAuACA8");
	this.shape_1.setTransform(65.6538,-3.9626);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(11.5,1,1).p("AiiBZQgFhRA4gzQAzgwBAAEQA+AEAvAvQAwAxACA7");
	this.shape_2.setTransform(65.6543,-4.2154);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(11.5,1,1).p("AiiBbQgFhRA5g1QA1gzA/AEQA+AFAuAyQAvAyACA7");
	this.shape_3.setTransform(65.6547,-4.4432);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(11.5,1,1).p("AiiBdQgFhRA6g3QA3g2A+AGQA9AEAuA0QAuA0ACA7");
	this.shape_4.setTransform(65.6552,-4.6254);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(11.5,1,1).p("AiiBeQgFhRA7g4QA5g4A8AGQA9AFAuA1QAtA1ACA8");
	this.shape_5.setTransform(65.6555,-4.7996);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(11.5,1,1).p("AiiBgQgFhRA8g5QA6g6A8AGQA8AFAtA3QAtA2ACA8");
	this.shape_6.setTransform(65.6556,-4.9531);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(11.5,1,1).p("AiiBhQgFhRA9g6QA7g8A7AGQA8AGAtA4QAsA3ACA8");
	this.shape_7.setTransform(65.6559,-5.0775);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(11.5,1,1).p("AiiBiQgFhRA9g7QA8g9A7AHQA8AFAsA5QAsA4ACA8");
	this.shape_8.setTransform(65.6561,-5.1565);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(11.5,1,1).p("AiiBjQgFhRA9g8QA9g+A7AHQA7AGAtA5QArA5ACA7");
	this.shape_9.setTransform(65.6561,-5.2311);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(11.5,1,1).p("AiiBjQgFhRA+g8QA9g+A6AHQA8AFAsA6QArA5ACA8");
	this.shape_10.setTransform(65.6562,-5.2561);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(11.5,1,1).p("AiiBjQgFhRA+g8QA9g+A6AGQA8AGAsA6QAsA5ABA8");
	this.shape_11.setTransform(65.6553,-5.2913);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(11.5,1,1).p("AiiBhQgFhRA8g6QA7g7A7AGQA9AGAsA3QAtA3ACA7");
	this.shape_12.setTransform(65.6558,-5.0029);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(11.5,1,1).p("AiiBeQgFhRA7g4QA4g3A9AFQA9AFAtA1QAuA1ACA7");
	this.shape_13.setTransform(65.6553,-4.7498);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(11.5,1,1).p("AiiBcQgFhRA6g2QA1g0A/AFQA+AEAtAzQAvAzACA7");
	this.shape_14.setTransform(65.655,-4.5217);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(11.5,1,1).p("AiiBaQgFhRA4g0QA0gyBAAEQA+AFAuAwQAwAyACA7");
	this.shape_15.setTransform(65.6545,-4.34);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(11.5,1,1).p("AiiBYQgFhRA3gyQAzgwBAAEQA/AFAuAuQAxAwACA8");
	this.shape_16.setTransform(65.6542,-4.1655);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(11.5,1,1).p("AiiBXQgFhRA3gxQAxguBBAEQA/AEAvAtQAxAvACA7");
	this.shape_17.setTransform(65.654,-4.0125);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#000000").ss(11.5,1,1).p("AiiBVQgFhRA2gwQAwgsBBAEQBAADAvAtQAyAtACA8");
	this.shape_18.setTransform(65.6536,-3.8879);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#000000").ss(11.5,1,1).p("AiiBVQgFhRA1gvQAwgrBCADQBAAEAvArQAyAtACA7");
	this.shape_19.setTransform(65.6534,-3.8098);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#000000").ss(11.5,1,1).p("AiiBUQgFhRA1gvQAvgqBDADQA/AEAvAqQAzAtACA7");
	this.shape_20.setTransform(65.6534,-3.735);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#000000").ss(11.5,1,1).p("AiiBUQgFhRA1guQAugqBDADQBAAEAvAqQAzAsACA7");
	this.shape_21.setTransform(65.6533,-3.71);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},23).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_11}]},24).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape}]},1).wait(31));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_4_Layer_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(11.5,1,1).p("AitBbQgChaA4gxQAxgsBHADQBFADAyAtQA3AwAABD");
	this.shape.setTransform(-0.0034,-0.0096);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(11.5,1,1).p("AitBeQgChZA4g1QAygwBHADQBFAEAyAwQA2AzAABD");
	this.shape_1.setTransform(-0.0034,-0.3588);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(11.5,1,1).p("AitBhQgChZA4g4QAzgzBGADQBGADAyA0QA1A2AABD");
	this.shape_2.setTransform(-0.0034,-0.6833);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(11.5,1,1).p("AitBkQgChZA4g6QA0g3BGADQBGADAyA4QA0A4AABD");
	this.shape_3.setTransform(-0.0034,-0.9827);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(11.5,1,1).p("AitBnQgChaA3g8QA1g6BGADQBGAEAyA6QA0A7AABD");
	this.shape_4.setTransform(-0.0035,-1.2324);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(11.5,1,1).p("AitBpQgChZA3g+QA2g9BGADQBGAEAyA8QAzA9AABD");
	this.shape_5.setTransform(-0.0035,-1.4571);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(11.5,1,1).p("AitBrQgChaA3g/QA3g/BFADQBHADAxA/QAzA/AABD");
	this.shape_6.setTransform(-0.0035,-1.6342);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(11.5,1,1).p("AitBsQgChZA3hBQA3hBBGAEQBGADAyBAQAyBAAABD");
	this.shape_7.setTransform(-0.0035,-1.7839);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(11.5,1,1).p("AitBuQgChaA3hBQA3hDBGAEQBGADAyBCQAyBBAABD");
	this.shape_8.setTransform(-0.0035,-1.9087);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(11.5,1,1).p("AitBuQgChZA3hDQA4hDBFAEQBHADAxBCQAyBCAABD");
	this.shape_9.setTransform(-0.0035,-1.9837);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(11.5,1,1).p("AitBvQgChaA3hDQA4hDBFADQBHADAxBDQAyBDAABD");
	this.shape_10.setTransform(-0.0035,-2.0336);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(11.5,1,1).p("AitBvQgChZA3hDQA4hEBFADQBHADAxBEQAyBCAABD");
	this.shape_11.setTransform(-0.0035,-2.0699);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(11.5,1,1).p("AitBsQgChaA3hAQA3hABFAEQBHADAxBAQAzA/AABD");
	this.shape_12.setTransform(-0.0035,-1.7091);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(11.5,1,1).p("AitBoQgChZA3g9QA2g9BGAEQBGADAyA8QAzA8AABD");
	this.shape_13.setTransform(-0.0035,-1.3846);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(11.5,1,1).p("AitBlQgChZA3g7QA1g5BGAEQBGADAyA5QA0A5AABD");
	this.shape_14.setTransform(-0.0035,-1.0852);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(11.5,1,1).p("AitBjQgChaA4g4QAzg2BHADQBFADAyA3QA1A3AABD");
	this.shape_15.setTransform(-0.0034,-0.8357);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(11.5,1,1).p("AitBhQgChaA4g2QAzg0BGAEQBGADAyA0QA1A1AABD");
	this.shape_16.setTransform(-0.0034,-0.6111);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(11.5,1,1).p("AitBfQgChaA4g1QAygxBHADQBFAEAyAxQA2A0AABD");
	this.shape_17.setTransform(-0.0034,-0.4337);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#000000").ss(11.5,1,1).p("AitBdQgChZA4g0QAygvBGADQBGADAyAwQA2AyAABD");
	this.shape_18.setTransform(-0.0034,-0.284);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#000000").ss(11.5,1,1).p("AitBcQgChZA4gzQAxguBHADQBFAEAzAuQA2AxAABD");
	this.shape_19.setTransform(-0.0034,-0.1593);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#000000").ss(11.5,1,1).p("AitBbQgChZA4gyQAxgtBHADQBFADAyAuQA3AwAABD");
	this.shape_20.setTransform(-0.0034,-0.0844);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#000000").ss(11.5,1,1).p("AitBbQgChaA4gxQAxgtBHADQBFAEAyAtQA3AwAABD");
	this.shape_21.setTransform(-0.0034,-0.0345);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},23).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_11}]},24).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape}]},1).wait(31));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_3_copy_Layer_4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_4
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(11.5,1,1).p("AAWr9QAhDAAECfQAECfAEBWQADBXgdDIQgeDJhNG/");
	this.shape.setTransform(-16.3915,39.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(11.5,1,1).p("AimJcQBElVBJipQBHinAMhFQAMhEAkh8QAkh9AZiQ");
	this.shape_1.setTransform(-31.25,22.25);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(11.5,1,1).p("AkfG7QA4jsB0iIQBziIAagyQAbgyBNhaQBMhbBShg");
	this.shape_2.setTransform(-43.85,5.45);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(11.5,1,1).p("AmZEZQAtiBCfhoQCfhoAqggQApgfB0g5QB0g4CNgw");
	this.shape_3.setTransform(-56.475,-11.4);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(11.5,1,1).p("AITh3QjGAAicAXQicAWg5AOQg5AMjKBIQjLBIghAY");
	this.shape_4.setTransform(-69.1,-28.25);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(11.5,1,1).p("AnMCrQgkhBCFhVQCEhQBWggQBQgeCsgZQCjgYDGAA");
	this.shape_5.setTransform(-66.593,-22.975);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(11.5,1,1).p("AmEDTQhahiBPhfQBOhWBtgwQBggpC5gcQCqgZDGAA");
	this.shape_6.setTransform(-66.3464,-18.9);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(11.5,1,1).p("AlGDwQiBh5AphnQAnhbB9g6QBtgzDBgcQCvgbDGAA");
	this.shape_7.setTransform(-67.2338,-15.95);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(11.5,1,1).p("AkcEBQiYiHARhrQAQhdCHhBQB0g4DHgeQCwgbDGAA");
	this.shape_8.setTransform(-68.1903,-14.2);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(11.5,1,1).p("AGlkFQjGAAixAbQjJAeh3A6QiJBDgJBeQgJBsCfCL");
	this.shape_9.setTransform(-68.5767,-13.625);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(11.5,1,1).p("Aj7ENQiyiWAMhrQAKhhCFhBQB2g6DGgeQC0gbDFgD");
	this.shape_10.setTransform(-68.3599,-13);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(11.5,1,1).p("AjqESQjCifAMhrQAMhhCBhAQB2g7DEgdQC2gcDEgE");
	this.shape_11.setTransform(-68.1878,-12.425);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(11.5,1,1).p("AjbEXQjRioAOhqQAOhiB9g/QB1g7DCgdQC4gcDEgG");
	this.shape_12.setTransform(-68.019,-11.925);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(11.5,1,1).p("AjPEcQjciwAPhpQAOhkB7g+QB0g7DBgdQC5gcDDgI");
	this.shape_13.setTransform(-67.8728,-11.5);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(11.5,1,1).p("AjEEfQjni2AQhoQAQhkB4g+QBzg7DAgdQC6gcDDgJ");
	this.shape_14.setTransform(-67.7518,-11.125);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(11.5,1,1).p("Ai7EiQjvi7AQhnQARhmB2g8QBzg8C+gcQC8gdDCgK");
	this.shape_15.setTransform(-67.6532,-10.825);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(11.5,1,1).p("Ai0ElQj3i/AShnQARhnB0g8QBzg7C9gdQC9gcDCgM");
	this.shape_16.setTransform(-67.583,-10.6);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(11.5,1,1).p("AivEmQj7jCARhmQAShnBzg8QBzg7C9gdQC9gcDCgM");
	this.shape_17.setTransform(-67.532,-10.425);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#000000").ss(11.5,1,1).p("AisEnQj+jDAShmQARhoBzg7QByg8C9gcQC9gdDCgM");
	this.shape_18.setTransform(-67.4846,-10.325);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#000000").ss(11.5,1,1).p("AGaknQjCANi9AcQi9AdhyA7QhyA8gSBnQgSBmD/DF");
	this.shape_19.setTransform(-67.4846,-10.3);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#000000").ss(11.5,1,1).p("Ai+EhQjsi5AQhoQAQhlB3g9QBzg7C/gdQC7gcDDgK");
	this.shape_20.setTransform(-67.7007,-10.925);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#000000").ss(11.5,1,1).p("AjeEXQjOinAOhqQANhjB+g/QB1g6DCgeQC4gcDEgG");
	this.shape_21.setTransform(-68.0415,-12);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#000000").ss(11.5,1,1).p("Aj1EPQi3iZALhrQALhhCEhBQB2g7DFgdQC1gbDFgE");
	this.shape_22.setTransform(-68.3089,-12.8);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#000000").ss(11.5,1,1).p("Aj+EMQiviUALhsQAKhgCGhBQB2g7DHgeQCzgbDGgC");
	this.shape_23.setTransform(-68.4075,-13.1);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#000000").ss(11.5,1,1).p("AkFEJQioiQAKhsQAKhfCHhCQB3g6DHgeQCzgbDFgB");
	this.shape_24.setTransform(-68.4781,-13.325);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f().s("#000000").ss(11.5,1,1).p("AkKEIQijiNAKhtQAJheCIhDQB3g6DIgeQCygbDGgB");
	this.shape_25.setTransform(-68.5288,-13.5);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f().s("#000000").ss(11.5,1,1).p("AkNEHQigiMAJhsQAJheCJhEQB3g5DJgeQCxgcDGAA");
	this.shape_26.setTransform(-68.5765,-13.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_9}]},15).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_19}]},25).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_9}]},11).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape}]},1).wait(11));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_3_copy_2_Layer_4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_4
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(11.5,1,1).p("AAWr9QAhDAAECfQAECfAEBWQADBXgdDIQgeDJhNG/");
	this.shape.setTransform(-16.3915,39.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(11.5,1,1).p("AimJcQBElVBJipQBHinAMhFQAMhEAkh8QAkh9AZiQ");
	this.shape_1.setTransform(-31.25,22.25);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(11.5,1,1).p("AkfG7QA4jsB0iIQBziIAagyQAbgyBNhaQBMhbBShg");
	this.shape_2.setTransform(-43.85,5.45);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(11.5,1,1).p("AmZEZQAtiBCfhoQCfhoAqggQApgfB0g5QB0g4CNgw");
	this.shape_3.setTransform(-56.475,-11.4);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(11.5,1,1).p("AITh3QjGAAicAXQicAWg5AOQg5AMjKBIQjLBIghAY");
	this.shape_4.setTransform(-69.1,-28.25);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(11.5,1,1).p("AnMCrQgkhBCFhVQCEhQBWggQBQgeCsgZQCjgYDGAA");
	this.shape_5.setTransform(-66.593,-22.975);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(11.5,1,1).p("AmEDTQhahiBPhfQBOhWBtgwQBggpC5gcQCqgZDGAA");
	this.shape_6.setTransform(-66.3464,-18.9);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(11.5,1,1).p("AlGDwQiBh5AphnQAnhbB9g6QBtgzDBgcQCvgbDGAA");
	this.shape_7.setTransform(-67.2338,-15.95);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(11.5,1,1).p("AkcEBQiYiHARhrQAQhdCHhBQB0g4DHgeQCwgbDGAA");
	this.shape_8.setTransform(-68.1903,-14.2);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(11.5,1,1).p("AGlkFQjGAAixAbQjJAeh3A6QiJBDgJBeQgJBsCfCL");
	this.shape_9.setTransform(-68.5767,-13.625);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(11.5,1,1).p("Aj7ENQiyiWAMhrQAKhhCFhBQB2g6DGgeQC0gbDFgD");
	this.shape_10.setTransform(-68.3599,-13);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(11.5,1,1).p("AjqESQjCifAMhrQAMhhCBhAQB2g7DEgdQC2gcDEgE");
	this.shape_11.setTransform(-68.1878,-12.425);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(11.5,1,1).p("AjbEXQjRioAOhqQAOhiB9g/QB1g7DCgdQC4gcDEgG");
	this.shape_12.setTransform(-68.019,-11.925);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(11.5,1,1).p("AjPEcQjciwAPhpQAOhkB7g+QB0g7DBgdQC5gcDDgI");
	this.shape_13.setTransform(-67.8728,-11.5);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(11.5,1,1).p("AjEEfQjni2AQhoQAQhkB4g+QBzg7DAgdQC6gcDDgJ");
	this.shape_14.setTransform(-67.7518,-11.125);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(11.5,1,1).p("Ai7EiQjvi7AQhnQARhmB2g8QBzg8C+gcQC8gdDCgK");
	this.shape_15.setTransform(-67.6532,-10.825);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(11.5,1,1).p("Ai0ElQj3i/AShnQARhnB0g8QBzg7C9gdQC9gcDCgM");
	this.shape_16.setTransform(-67.583,-10.6);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(11.5,1,1).p("AivEmQj7jCARhmQAShnBzg8QBzg7C9gdQC9gcDCgM");
	this.shape_17.setTransform(-67.532,-10.425);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#000000").ss(11.5,1,1).p("AisEnQj+jDAShmQARhoBzg7QByg8C9gcQC9gdDCgM");
	this.shape_18.setTransform(-67.4846,-10.325);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#000000").ss(11.5,1,1).p("AGaknQjCANi9AcQi9AdhyA7QhyA8gSBnQgSBmD/DF");
	this.shape_19.setTransform(-67.4846,-10.3);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#000000").ss(11.5,1,1).p("Ai+EhQjsi5AQhoQAQhlB3g9QBzg7C/gdQC7gcDDgK");
	this.shape_20.setTransform(-67.7007,-10.925);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#000000").ss(11.5,1,1).p("AjeEXQjOinAOhqQANhjB+g/QB1g6DCgeQC4gcDEgG");
	this.shape_21.setTransform(-68.0415,-12);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#000000").ss(11.5,1,1).p("Aj1EPQi3iZALhrQALhhCEhBQB2g7DFgdQC1gbDFgE");
	this.shape_22.setTransform(-68.3089,-12.8);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#000000").ss(11.5,1,1).p("Aj+EMQiviUALhsQAKhgCGhBQB2g7DHgeQCzgbDGgC");
	this.shape_23.setTransform(-68.4075,-13.1);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#000000").ss(11.5,1,1).p("AkFEJQioiQAKhsQAKhfCHhCQB3g6DHgeQCzgbDFgB");
	this.shape_24.setTransform(-68.4781,-13.325);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f().s("#000000").ss(11.5,1,1).p("AkKEIQijiNAKhtQAJheCIhDQB3g6DIgeQCygbDGgB");
	this.shape_25.setTransform(-68.5288,-13.5);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f().s("#000000").ss(11.5,1,1).p("AkNEHQigiMAJhsQAJheCJhEQB3g5DJgeQCxgcDGAA");
	this.shape_26.setTransform(-68.5765,-13.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_9}]},15).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_19}]},25).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_9}]},11).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape}]},1).wait(11));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_3_copy_2_Layer_3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_3
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(11.5,1,1).p("AEOuNQAWDLgdEgQgdEghSD+QhREAhzDhQh0Dhh3BQ");
	this.shape.setTransform(83.3067,69.4);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(11.5,1,1).p("AmaMAQB5gqCSiqQCRiqCCjTQCBjSBJkDQBKkDADjW");
	this.shape_1.setTransform(69.35,55.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(11.5,1,1).p("AoRKDQB6gKCth5QCsh6CrirQCsiqBwjoQBxjqAYjh");
	this.shape_2.setTransform(57.475,42.475);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(11.5,1,1).p("Ap5IUQB9ATDDhPQDDhQDQiIQDQiICTjSQCTjTAqjq");
	this.shape_3.setTransform(47.15,31.7005);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(11.5,1,1).p("ArRGwQB+AqDXgrQDXgsDuhrQDwhrCvi/QCvi/A7jz");
	this.shape_4.setTransform(38.425,23.3251);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(11.5,1,1).p("AsZFWQB/A+DogOQDmgPEIhTQEJhTDHiwQDGiwBIj4");
	this.shape_5.setTransform(31.275,17.2289);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(11.5,1,1).p("AtREKQCABND0AIQDzAJEchBQEchBDZikQDZijBSj+");
	this.shape_6.setTransform(25.725,13.1492);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(11.5,1,1).p("At5DPQCBBYD8AZQD8AYEqgzQErg0DmibQDmibBZkB");
	this.shape_7.setTransform(21.775,10.6449);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(11.5,1,1).p("AuRCqQCCBeEBAjQEBAiEzgrQEzgsDuiWQDuiVBdkE");
	this.shape_8.setTransform(19.4,9.2958);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(11.5,1,1).p("AOak1QhfEEjwCUQjxCUk2ApQk1ApkDglQkEgmiBhh");
	this.shape_9.setTransform(18.6,8.8707);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(11.5,1,1).p("AuUClQB3BYEPArQEOAqEwgvQExgvDqiWQDriUBfkE");
	this.shape_10.setTransform(19.1,9.4466);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(11.5,1,1).p("AuPCrQBuBREYAvQEZAuErg0QEsg0DliXQDliWBfkE");
	this.shape_11.setTransform(19.575,9.9726);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(11.5,1,1).p("AuLCwQBmBLEhAyQEiAzEng5QEog5DgiXQDgiXBfkE");
	this.shape_12.setTransform(19.975,10.4167);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(11.5,1,1).p("AuIC1QBgBFEpA2QEpA1Ejg8QElg9DciZQDciXBfkE");
	this.shape_13.setTransform(20.3,10.8054);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(11.5,1,1).p("AuFC6QBZBAEwA4QEwA5EhhAQEhhADZiaQDYiYBfkE");
	this.shape_14.setTransform(20.6,11.1496);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(11.5,1,1).p("AuCC9QBUA8E1A7QE1A6EehCQEfhDDWiaQDViZBfkE");
	this.shape_15.setTransform(20.85,11.4189);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(11.5,1,1).p("AuADAQBQA5E5A8QE6A9EchFQEdhFDTibQDUiZBekE");
	this.shape_16.setTransform(21.025,11.6382);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(11.5,1,1).p("At/DCQBOA2E8A+QE8A+EbhHQEbhGDSibQDSiaBfkE");
	this.shape_17.setTransform(21.175,11.7827);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#000000").ss(11.5,1,1).p("At+DDQBMA1E+A/QE+A+EahHQEbhIDRibQDRiaBekE");
	this.shape_18.setTransform(21.25,11.8826);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#000000").ss(11.5,1,1).p("AN/lTQhfEEjQCaQjRCbkbBIQkZBHk/g+Qk+g/hMg1");
	this.shape_19.setTransform(21.275,11.8995);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#000000").ss(11.5,1,1).p("AuDC8QBWA9EzA6QEzA6EfhCQEghCDXiZQDWiZBfkE");
	this.shape_20.setTransform(20.775,11.3315);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#000000").ss(11.5,1,1).p("AuMCwQBnBLEhAyQEgAyEog4QEog4DhiYQDhiXBfkE");
	this.shape_21.setTransform(19.9,10.3613);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#000000").ss(11.5,1,1).p("AuSCnQB0BVESAtQESArEugwQEvgxDpiWQDoiVBfkE");
	this.shape_22.setTransform(19.275,9.6284);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#000000").ss(11.5,1,1).p("AuUCjQB4BaENAqQENApEwguQEyguDriVQDsiVBekE");
	this.shape_23.setTransform(19.025,9.3591);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#000000").ss(11.5,1,1).p("AuWChQB8BcEJApQEIAnEygrQE0gsDuiVQDtiUBfkE");
	this.shape_24.setTransform(18.85,9.1399);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f().s("#000000").ss(11.5,1,1).p("AuYCfQB/BfEGAnQEFAmE0gqQE2gqDviVQDviUBfkE");
	this.shape_25.setTransform(18.7,8.9957);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f().s("#000000").ss(11.5,1,1).p("AuYCeQCABgEEAmQEEAmE1gpQE1gqDxiUQDwiUBekE");
	this.shape_26.setTransform(18.625,8.8957);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f().s("#000000").ss(11.5,1,1).p("AuNCwQCBBdEBAhQEAAhExgtQEygtDtiXQDtiWBckD");
	this.shape_27.setTransform(19.75,9.4859);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f().s("#000000").ss(11.5,1,1).p("AtmDqQCABTD4ASQD4AREjg6QElg6DgifQDgifBVj/");
	this.shape_28.setTransform(23.6,11.7927);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f().s("#000000").ss(11.5,1,1).p("AsdFQQB/A/DogMQDngNEKhSQELhRDIiwQDIiuBIj5");
	this.shape_29.setTransform(30.85,16.896);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f().s("#000000").ss(11.5,1,1).p("AqwHXQB9AhDQg5QDQg5Djh2QDjh2CljGQCljHA0jv");
	this.shape_30.setTransform(41.7,26.3834);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f().s("#000000").ss(11.5,1,1).p("AorJoQB7gDCzhvQCxhvC1iiQC0ijB5jiQB5jkAdjj");
	this.shape_31.setTransform(54.95,39.825);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f().s("#000000").ss(11.5,1,1).p("AmtLsQB5gmCXihQCWijCHjMQCIjLBQj/QBQj/AGjY");
	this.shape_32.setTransform(67.425,53.05);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f().s("#000000").ss(11.5,1,1).p("AlTNLQB4g+CCjHQCBjHBojqQBojpAzkSQAykUgLjQ");
	this.shape_33.setTransform(76.5234,62.6);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f().s("#000000").ss(11.5,1,1).p("AkkN/QB3hMB3jbQB2jbBXj7QBWj6AikdQAjkdgUjM");
	this.shape_34.setTransform(81.717,67.85);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_9}]},15).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_19}]},25).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_9}]},11).to({state:[{t:this.shape_27}]},1).to({state:[{t:this.shape_28}]},1).to({state:[{t:this.shape_29}]},1).to({state:[{t:this.shape_30}]},1).to({state:[{t:this.shape_31}]},1).to({state:[{t:this.shape_32}]},1).to({state:[{t:this.shape_33}]},1).to({state:[{t:this.shape_34}]},1).to({state:[{t:this.shape}]},1).wait(11));

}).prototype = p = new cjs.MovieClip();


(lib.Scene_1_Layer_9 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_9
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("EhUXA1IMAAAhqPMCovAAAMAAABqPg");
	this.shape.setTransform(540,340);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Scene_1_Layer_9, null, null);


(lib.Scene_1_Layer_6 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_6
	this.instance = new lib.bg_part2();
	this.instance.parent = this;
	this.instance.setTransform(346,505);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Scene_1_Layer_6, null, null);


(lib.Scene_1_Layer_4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_4
	this.instance = new lib.grass();
	this.instance.parent = this;
	this.instance.setTransform(384,606);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Scene_1_Layer_4, null, null);


(lib.Scene_1_Layer_3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_3
	this.instance = new lib.pack();
	this.instance.parent = this;
	this.instance.setTransform(598,242);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Scene_1_Layer_3, null, null);


(lib.Scene_1_Layer_2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.instance = new lib.tree();
	this.instance.parent = this;
	this.instance.setTransform(405,81);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Scene_1_Layer_2, null, null);


(lib.Symbol4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_99 = function() {
		this.___loopingOver___ = true;
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(99).call(this.frame_99).wait(1));

	// Layer_3_obj_
	this.Layer_3 = new lib.Symbol_4_Layer_3();
	this.Layer_3.name = "Layer_3";
	this.Layer_3.parent = this;
	this.Layer_3.setTransform(39.3,38.8,1,1,0,0,0,39.3,38.8);
	this.Layer_3.depth = 0;
	this.Layer_3.isAttachedToCamera = 0
	this.Layer_3.isAttachedToMask = 0
	this.Layer_3.layerDepth = 0
	this.Layer_3.layerIndex = 0
	this.Layer_3.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_3).wait(100));

	// Layer_2_obj_
	this.Layer_2 = new lib.Symbol_4_Layer_2();
	this.Layer_2.name = "Layer_2";
	this.Layer_2.parent = this;
	this.Layer_2.setTransform(65.7,-3.7,1,1,0,0,0,65.7,-3.7);
	this.Layer_2.depth = 0;
	this.Layer_2.isAttachedToCamera = 0
	this.Layer_2.isAttachedToMask = 0
	this.Layer_2.layerDepth = 0
	this.Layer_2.layerIndex = 1
	this.Layer_2.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_2).wait(100));

	// Layer_1_obj_
	this.Layer_1 = new lib.Symbol_4_Layer_1();
	this.Layer_1.name = "Layer_1";
	this.Layer_1.parent = this;
	this.Layer_1.depth = 0;
	this.Layer_1.isAttachedToCamera = 0
	this.Layer_1.isAttachedToMask = 0
	this.Layer_1.layerDepth = 0
	this.Layer_1.layerIndex = 2
	this.Layer_1.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_1).wait(100));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-23.2,-20.9,110.9,72.4);


(lib.Symbol_3_copy_Layer_5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_5
	this.instance = new lib.Symbol5();
	this.instance.parent = this;
	this.instance.setTransform(-23.15,115.6,1,1,74.9998,0,0,-11,-8.2);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({scaleX:0.9989,scaleY:0.9989,rotation:68.9757,x:-47.7,y:78.9},1).to({scaleX:0.9978,scaleY:0.9978,rotation:62.9565,x:-73.7,y:50.15},1).to({regX:-10.9,regY:-8.1,scaleX:0.997,scaleY:0.997,rotation:86.9363,x:-98,y:18.35},1).to({regX:-11,scaleX:0.9968,scaleY:0.9968,rotation:103.3632,x:-122.5,y:-17.15},1).to({regX:-10.9,regY:-8.3,scaleX:1,scaleY:1,rotation:44.9994,x:-117.85,y:-8.9},1).to({regX:-11,rotation:0,x:-97.65,y:12.05},4,cjs.Ease.get(1)).wait(15).to({x:-89.65,y:18.25},10,cjs.Ease.get(1)).wait(25).to({x:-97.65,y:12.05},10,cjs.Ease.get(1)).wait(11).to({regX:-10.9,rotation:44.9994,x:-117.85,y:-8.9},4,cjs.Ease.get(-1)).to({regX:-11,regY:-8.1,scaleX:0.9968,scaleY:0.9968,rotation:103.3632,x:-122.5,y:-17.15},1).to({regX:-10.9,scaleX:0.997,scaleY:0.997,rotation:86.9363,x:-98,y:18.35},1).to({regX:-11,regY:-8.2,scaleX:0.9978,scaleY:0.9978,rotation:62.9565,x:-73.7,y:50.15},1).to({scaleX:0.9989,scaleY:0.9989,rotation:68.9757,x:-47.7,y:78.9},1).to({scaleX:1,scaleY:1,rotation:74.9998,x:-23.15,y:115.6},1).wait(11));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_3_copy_2_Layer_6 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_6
	this.instance = new lib.Symbol6();
	this.instance.parent = this;
	this.instance.setTransform(59.4,155.9,1,1,-90,0,0,10.3,12.9);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({rotation:0,x:-70.45,y:26.75},9,cjs.Ease.get(1)).wait(15).to({x:-62.45,y:32.95},10,cjs.Ease.get(1)).wait(25).to({x:-70.45,y:26.75},10,cjs.Ease.get(1)).wait(12).to({regX:2.2,regY:-2.6,rotation:-1.612,x:-76.6,y:13.8},0).wait(1).to({rotation:-7.0321,x:-70.2,y:22.4},0).wait(1).to({rotation:-17.1669,x:-57.95,y:38.9},0).wait(1).to({rotation:-32.3721,x:-38.9,y:64.4},0).wait(1).to({rotation:-50.9181,x:-14.05,y:96.3},0).wait(1).to({rotation:-68.4108,x:10.85,y:126.75},0).wait(1).to({rotation:-81.0311,x:29.9,y:148.6},0).wait(1).to({rotation:-87.9456,x:40.65,y:160.45},0).wait(1).to({regX:10.3,regY:12.9,rotation:-90,x:59.4,y:155.9},0).wait(11));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_3_copy_2_Layer_5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_5
	this.instance = new lib.Symbol5();
	this.instance.parent = this;
	this.instance.setTransform(-23.15,115.6,1,1,74.9998,0,0,-11,-8.2);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({scaleX:0.9989,scaleY:0.9989,rotation:68.9757,x:-47.7,y:78.9},1).to({scaleX:0.9978,scaleY:0.9978,rotation:62.9565,x:-73.7,y:50.15},1).to({regX:-10.9,regY:-8.1,scaleX:0.997,scaleY:0.997,rotation:86.9363,x:-98,y:18.35},1).to({regX:-11,scaleX:0.9968,scaleY:0.9968,rotation:103.3632,x:-122.5,y:-17.15},1).to({regX:-10.9,regY:-8.3,scaleX:1,scaleY:1,rotation:44.9994,x:-117.85,y:-8.9},1).to({regX:-11,rotation:0,x:-97.65,y:12.05},4,cjs.Ease.get(1)).wait(15).to({x:-89.65,y:18.25},10,cjs.Ease.get(1)).wait(25).to({x:-97.65,y:12.05},10,cjs.Ease.get(1)).wait(11).to({regX:-10.9,rotation:44.9994,x:-117.85,y:-8.9},4,cjs.Ease.get(-1)).to({regX:-11,regY:-8.1,scaleX:0.9968,scaleY:0.9968,rotation:103.3632,x:-122.5,y:-17.15},1).to({regX:-10.9,scaleX:0.997,scaleY:0.997,rotation:86.9363,x:-98,y:18.35},1).to({regX:-11,regY:-8.2,scaleX:0.9978,scaleY:0.9978,rotation:62.9565,x:-73.7,y:50.15},1).to({scaleX:0.9989,scaleY:0.9989,rotation:68.9757,x:-47.7,y:78.9},1).to({scaleX:1,scaleY:1,rotation:74.9998,x:-23.15,y:115.6},1).wait(11));

}).prototype = p = new cjs.MovieClip();


(lib.Scene_1_Layer_12 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_12
	this.instance = new lib.Symbol7();
	this.instance.parent = this;
	this.instance.setTransform(620.5,380.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Scene_1_Layer_12, null, null);


(lib.Scene_1_Layer_8 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_8
	this.instance = new lib.Symbol1();
	this.instance.parent = this;
	this.instance.setTransform(23.15,568.2,0.6895,0.6895,-11.5618,0,0,0.1,0.3);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Scene_1_Layer_8, null, null);


(lib.Scene_1_Layer_7 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_7
	this.instance = new lib.Symbol1();
	this.instance.parent = this;
	this.instance.setTransform(1055.5,241.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Scene_1_Layer_7, null, null);


(lib.Scene_1_Layer_5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_5
	this.instance = new lib.Symbol4();
	this.instance.parent = this;
	this.instance.setTransform(679.25,394.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Scene_1_Layer_5, null, null);


(lib.Symbol3copy2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_99 = function() {
		this.___loopingOver___ = true;
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(99).call(this.frame_99).wait(1));

	// Layer_6_obj_
	this.Layer_6 = new lib.Symbol_3_copy_2_Layer_6();
	this.Layer_6.name = "Layer_6";
	this.Layer_6.parent = this;
	this.Layer_6.setTransform(43.9,164,1,1,0,0,0,43.9,164);
	this.Layer_6.depth = 0;
	this.Layer_6.isAttachedToCamera = 0
	this.Layer_6.isAttachedToMask = 0
	this.Layer_6.layerDepth = 0
	this.Layer_6.layerIndex = 0
	this.Layer_6.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_6).wait(81).to({regX:-16.1,regY:86.4,x:-16.1,y:86.4},0).wait(8).to({regX:43.9,regY:164,x:43.9,y:164},0).wait(11));

	// Layer_3_obj_
	this.Layer_3 = new lib.Symbol_3_copy_2_Layer_3();
	this.Layer_3.name = "Layer_3";
	this.Layer_3.parent = this;
	this.Layer_3.setTransform(83.3,69.4,1,1,0,0,0,83.3,69.4);
	this.Layer_3.depth = 0;
	this.Layer_3.isAttachedToCamera = 0
	this.Layer_3.isAttachedToMask = 0
	this.Layer_3.layerDepth = 0
	this.Layer_3.layerIndex = 1
	this.Layer_3.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_3).wait(100));

	// Layer_2 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	var mask_graphics_0 = new cjs.Graphics().p("AkvHlIAAvJIIsAAIAAA3IAjgBIAABeIALA6IgDBLIgNAXIAABBIAEAdQABAbgFAbIAOCAIgaC1IAABCIAhArIgqBjg");
	var mask_graphics_4 = new cjs.Graphics().p("AoOHlIAAvJIIsAAIAAA3IAjgBIAABeIALA6IgDBLIgNAXIAABBIAEAdQABAbgFAbIDHhOIELCCIgZHKImJghIhOAog");
	var mask_graphics_80 = new cjs.Graphics().p("AoOHlIAAvJIIsAAIAAA3IAjgBIAABeIALA6IgDBLIgNAXIAABBIAEAdQABAbgFAbIDHhOIELCCIgZHKImJghIhOAog");
	var mask_graphics_85 = new cjs.Graphics().p("AkvHlIAAvJIIsAAIAAA3IAjgBIAABeIALA6IgDBLIgNAXIAABBIAEAdQABAbgFAbIAOCAIgaC1IAABCIAhArIgqBjg");

	this.timeline.addTween(cjs.Tween.get(mask).to({graphics:mask_graphics_0,x:-131.25,y:-5.025}).wait(4).to({graphics:mask_graphics_4,x:-108.95,y:-5.025}).wait(76).to({graphics:mask_graphics_80,x:-108.95,y:-5.025}).wait(5).to({graphics:mask_graphics_85,x:-131.25,y:-5.025}).wait(15));

	// Layer_5_obj_
	this.Layer_5 = new lib.Symbol_3_copy_2_Layer_5();
	this.Layer_5.name = "Layer_5";
	this.Layer_5.parent = this;
	this.Layer_5.setTransform(-27.1,132.3,1,1,0,0,0,-27.1,132.3);
	this.Layer_5.depth = 0;
	this.Layer_5.isAttachedToCamera = 0
	this.Layer_5.isAttachedToMask = 0
	this.Layer_5.layerDepth = 0
	this.Layer_5.layerIndex = 2
	this.Layer_5.maskLayerName = 0

	var maskedShapeInstanceList = [this.Layer_5];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.Layer_5).wait(100));

	// Layer_4_obj_
	this.Layer_4 = new lib.Symbol_3_copy_2_Layer_4();
	this.Layer_4.name = "Layer_4";
	this.Layer_4.parent = this;
	this.Layer_4.setTransform(-16.4,39.1,1,1,0,0,0,-16.4,39.1);
	this.Layer_4.depth = 0;
	this.Layer_4.isAttachedToCamera = 0
	this.Layer_4.isAttachedToMask = 0
	this.Layer_4.layerDepth = 0
	this.Layer_4.layerIndex = 3
	this.Layer_4.maskLayerName = 0

	var maskedShapeInstanceList = [this.Layer_4];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.Layer_4).wait(100));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-149.5,-45.9,266.5,228.6);


(lib.Symbol3copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_99 = function() {
		this.___loopingOver___ = true;
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(99).call(this.frame_99).wait(1));

	// Layer_7 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("AoOTaMAAAgmzIQeAAMAAAAmzg");
	mask.setTransform(-43.95,56.45);

	// Layer_5_obj_
	this.Layer_5 = new lib.Symbol_3_copy_Layer_5();
	this.Layer_5.name = "Layer_5";
	this.Layer_5.parent = this;
	this.Layer_5.setTransform(-27.1,132.3,1,1,0,0,0,-27.1,132.3);
	this.Layer_5.depth = 0;
	this.Layer_5.isAttachedToCamera = 0
	this.Layer_5.isAttachedToMask = 0
	this.Layer_5.layerDepth = 0
	this.Layer_5.layerIndex = 0
	this.Layer_5.maskLayerName = 0

	var maskedShapeInstanceList = [this.Layer_5];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.Layer_5).wait(100));

	// Layer_4_obj_
	this.Layer_4 = new lib.Symbol_3_copy_Layer_4();
	this.Layer_4.name = "Layer_4";
	this.Layer_4.parent = this;
	this.Layer_4.setTransform(-16.4,39.1,1,1,0,0,0,-16.4,39.1);
	this.Layer_4.depth = 0;
	this.Layer_4.isAttachedToCamera = 0
	this.Layer_4.isAttachedToMask = 0
	this.Layer_4.layerDepth = 0
	this.Layer_4.layerIndex = 1
	this.Layer_4.maskLayerName = 0

	var maskedShapeInstanceList = [this.Layer_4];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.Layer_4).wait(100));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-122.1,-48.1,239.1,230.79999999999998);


(lib.Scene_1_Layer_11 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_11
	this.instance = new lib.Symbol3copy();
	this.instance.parent = this;
	this.instance.setTransform(702.05,467.9);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Scene_1_Layer_11, null, null);


(lib.Scene_1_Layer_10 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_10
	this.instance = new lib.Symbol3copy2();
	this.instance.parent = this;
	this.instance.setTransform(702.05,467.9);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Scene_1_Layer_10, null, null);


// stage content:
(lib.Main_july = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	this.___GetDepth___ = function(obj) {
		var depth = obj.depth;
		var cameraObj = this.___camera___instance;
		if(cameraObj && cameraObj.depth && obj.isAttachedToCamera)
		{
			depth += depth + cameraObj.depth;
		}
		return depth;
		}
	this.___needSorting___ = function() {
		for (var i = 0; i < this.getNumChildren() - 1; i++)
		{
			var prevDepth = this.___GetDepth___(this.getChildAt(i));
			var nextDepth = this.___GetDepth___(this.getChildAt(i + 1));
			if (prevDepth < nextDepth)
				return true;
		}
		return false;
	}
	this.___sortFunction___ = function(obj1, obj2) {
		return (this.exportRoot.___GetDepth___(obj2) - this.exportRoot.___GetDepth___(obj1));
	}
	this.on('tick', function (event){
		var curTimeline = event.currentTarget;
		if (curTimeline.___needSorting___()){
			this.sortChildren(curTimeline.___sortFunction___);
		}
	});

	// Layer_12_obj_
	this.Layer_12 = new lib.Scene_1_Layer_12();
	this.Layer_12.name = "Layer_12";
	this.Layer_12.parent = this;
	this.Layer_12.setTransform(620.5,380.5,1,1,0,0,0,620.5,380.5);
	this.Layer_12.depth = 0;
	this.Layer_12.isAttachedToCamera = 0
	this.Layer_12.isAttachedToMask = 0
	this.Layer_12.layerDepth = 0
	this.Layer_12.layerIndex = 0
	this.Layer_12.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_12).wait(1));

	// Layer_5_obj_
	this.Layer_5 = new lib.Scene_1_Layer_5();
	this.Layer_5.name = "Layer_5";
	this.Layer_5.parent = this;
	this.Layer_5.setTransform(711.5,411.4,1,1,0,0,0,711.5,411.4);
	this.Layer_5.depth = 0;
	this.Layer_5.isAttachedToCamera = 0
	this.Layer_5.isAttachedToMask = 0
	this.Layer_5.layerDepth = 0
	this.Layer_5.layerIndex = 1
	this.Layer_5.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_5).wait(1));

	// Layer_10_obj_
	this.Layer_10 = new lib.Scene_1_Layer_10();
	this.Layer_10.name = "Layer_10";
	this.Layer_10.parent = this;
	this.Layer_10.setTransform(679.8,532.5,1,1,0,0,0,679.8,532.5);
	this.Layer_10.depth = 0;
	this.Layer_10.isAttachedToCamera = 0
	this.Layer_10.isAttachedToMask = 0
	this.Layer_10.layerDepth = 0
	this.Layer_10.layerIndex = 2
	this.Layer_10.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_10).wait(1));

	// Layer_4_obj_
	this.Layer_4 = new lib.Scene_1_Layer_4();
	this.Layer_4.name = "Layer_4";
	this.Layer_4.parent = this;
	this.Layer_4.setTransform(664,643,1,1,0,0,0,664,643);
	this.Layer_4.depth = 0;
	this.Layer_4.isAttachedToCamera = 0
	this.Layer_4.isAttachedToMask = 0
	this.Layer_4.layerDepth = 0
	this.Layer_4.layerIndex = 3
	this.Layer_4.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_4).wait(1));

	// Layer_3_obj_
	this.Layer_3 = new lib.Scene_1_Layer_3();
	this.Layer_3.name = "Layer_3";
	this.Layer_3.parent = this;
	this.Layer_3.setTransform(765,461,1,1,0,0,0,765,461);
	this.Layer_3.depth = 0;
	this.Layer_3.isAttachedToCamera = 0
	this.Layer_3.isAttachedToMask = 0
	this.Layer_3.layerDepth = 0
	this.Layer_3.layerIndex = 4
	this.Layer_3.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_3).wait(1));

	// Layer_2_obj_
	this.Layer_2 = new lib.Scene_1_Layer_2();
	this.Layer_2.name = "Layer_2";
	this.Layer_2.parent = this;
	this.Layer_2.setTransform(620.5,380.5,1,1,0,0,0,620.5,380.5);
	this.Layer_2.depth = 0;
	this.Layer_2.isAttachedToCamera = 0
	this.Layer_2.isAttachedToMask = 0
	this.Layer_2.layerDepth = 0
	this.Layer_2.layerIndex = 5
	this.Layer_2.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_2).wait(1));

	// Layer_11_obj_
	this.Layer_11 = new lib.Scene_1_Layer_11();
	this.Layer_11.name = "Layer_11";
	this.Layer_11.parent = this;
	this.Layer_11.setTransform(658.1,524.4,1,1,0,0,0,658.1,524.4);
	this.Layer_11.depth = 0;
	this.Layer_11.isAttachedToCamera = 0
	this.Layer_11.isAttachedToMask = 0
	this.Layer_11.layerDepth = 0
	this.Layer_11.layerIndex = 6
	this.Layer_11.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_11).wait(1));

	// Layer_8_obj_
	this.Layer_8 = new lib.Scene_1_Layer_8();
	this.Layer_8.name = "Layer_8";
	this.Layer_8.parent = this;
	this.Layer_8.setTransform(23.1,568,1,1,0,0,0,23.1,568);
	this.Layer_8.depth = 0;
	this.Layer_8.isAttachedToCamera = 0
	this.Layer_8.isAttachedToMask = 0
	this.Layer_8.layerDepth = 0
	this.Layer_8.layerIndex = 7
	this.Layer_8.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_8).wait(1));

	// Layer_7_obj_
	this.Layer_7 = new lib.Scene_1_Layer_7();
	this.Layer_7.name = "Layer_7";
	this.Layer_7.parent = this;
	this.Layer_7.setTransform(1055.5,241.5,1,1,0,0,0,1055.5,241.5);
	this.Layer_7.depth = 0;
	this.Layer_7.isAttachedToCamera = 0
	this.Layer_7.isAttachedToMask = 0
	this.Layer_7.layerDepth = 0
	this.Layer_7.layerIndex = 8
	this.Layer_7.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_7).wait(1));

	// Layer_6_obj_
	this.Layer_6 = new lib.Scene_1_Layer_6();
	this.Layer_6.name = "Layer_6";
	this.Layer_6.parent = this;
	this.Layer_6.setTransform(762.5,671.5,1,1,0,0,0,762.5,671.5);
	this.Layer_6.depth = 0;
	this.Layer_6.isAttachedToCamera = 0
	this.Layer_6.isAttachedToMask = 0
	this.Layer_6.layerDepth = 0
	this.Layer_6.layerIndex = 9
	this.Layer_6.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_6).wait(1));

	// Layer_9_obj_
	this.Layer_9 = new lib.Scene_1_Layer_9();
	this.Layer_9.name = "Layer_9";
	this.Layer_9.parent = this;
	this.Layer_9.setTransform(540,340,1,1,0,0,0,540,340);
	this.Layer_9.depth = 0;
	this.Layer_9.isAttachedToCamera = 0
	this.Layer_9.isAttachedToMask = 0
	this.Layer_9.layerDepth = 0
	this.Layer_9.layerIndex = 10
	this.Layer_9.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_9).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(419.7,340,820.3,498);
// library properties:
lib.properties = {
	id: 'D226EA9643738A419EA54E51ACA42C4B',
	width: 1080,
	height: 680,
	fps: 24,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: [
		{src:"assets/images/bg_part147.jpg", id:"bg_part1"},
		{src:"assets/images/bg_part247.jpg", id:"bg_part2"},
		{src:"assets/images/grass47.png", id:"grass"},
		{src:"assets/images/pack47.png", id:"pack"},
		{src:"assets/images/tree47.png", id:"tree"}
	],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['D226EA9643738A419EA54E51ACA42C4B'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}


// Layer depth API : 

AdobeAn.Layer = new function() {
	this.getLayerZDepth = function(timeline, layerName)
	{
		if(layerName === "Camera")
		layerName = "___camera___instance";
		var script = "if(timeline." + layerName + ") timeline." + layerName + ".depth; else 0;";
		return eval(script);
	}
	this.setLayerZDepth = function(timeline, layerName, zDepth)
	{
		const MAX_zDepth = 10000;
		const MIN_zDepth = -5000;
		if(zDepth > MAX_zDepth)
			zDepth = MAX_zDepth;
		else if(zDepth < MIN_zDepth)
			zDepth = MIN_zDepth;
		if(layerName === "Camera")
		layerName = "___camera___instance";
		var script = "if(timeline." + layerName + ") timeline." + layerName + ".depth = " + zDepth + ";";
		eval(script);
	}
	this.removeLayer = function(timeline, layerName)
	{
		if(layerName === "Camera")
		layerName = "___camera___instance";
		var script = "if(timeline." + layerName + ") timeline.removeChild(timeline." + layerName + ");";
		eval(script);
	}
	this.addNewLayer = function(timeline, layerName, zDepth)
	{
		if(layerName === "Camera")
		layerName = "___camera___instance";
		zDepth = typeof zDepth !== 'undefined' ? zDepth : 0;
		var layer = new createjs.MovieClip();
		layer.name = layerName;
		layer.depth = zDepth;
		layer.layerIndex = 0;
		timeline.addChild(layer);
	}
}


})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;

$(function () {
    var canvas, stage, exportRoot, anim_container, dom_overlay_container, fnStartAnimation;
    function init() {
        canvas = document.getElementById("canvas47");
        anim_container = document.getElementById("animation_container47");
        dom_overlay_container = document.getElementById("dom_overlay_container47");
        var comp=AdobeAn.getComposition("D226EA9643738A419EA54E51ACA42C4B");
        var lib=comp.getLibrary();
        var loader = new createjs.LoadQueue(false);
        loader.addEventListener("fileload", function(evt){handleFileLoad(evt,comp)});
        loader.addEventListener("complete", function(evt){handleComplete(evt,comp)});
        var lib=comp.getLibrary();
        loader.loadManifest(lib.properties.manifest);
    }
    function handleFileLoad(evt, comp) {
        var images=comp.getImages();
        if (evt && (evt.item.type == "image")) { images[evt.item.id] = evt.result; }
    }
    function handleComplete(evt,comp) {
        //This function is always called, irrespective of the content. You can use the variable "stage" after it is created in token create_stage.
        var lib=comp.getLibrary();
        var ss=comp.getSpriteSheet();
        var queue = evt.target;
        var ssMetadata = lib.ssMetadata;
        for(i=0; i<ssMetadata.length; i++) {
            ss[ssMetadata[i].name] = new createjs.SpriteSheet( {"images": [queue.getResult(ssMetadata[i].name)], "frames": ssMetadata[i].frames} )
        }
        var preloaderDiv = document.getElementById("_preload_div_47");
        preloaderDiv.style.display = 'none';
        canvas.style.display = 'block';
        exportRoot = new lib.Main_july();
        stage = new lib.Stage(canvas);
        //Registers the "tick" event listener.
        fnStartAnimation = function() {
            stage.addChild(exportRoot);
            createjs.Ticker.setFPS(lib.properties.fps);
            createjs.Ticker.addEventListener("tick", stage)
            stage.addEventListener("tick", handleTick)
            function getProjectionMatrix(container, totalDepth) {
                var focalLength = 528.25;
                var projectionCenter = { x : lib.properties.width/2, y : lib.properties.height/2 };
                var scale = (totalDepth + focalLength)/focalLength;
                var scaleMat = new createjs.Matrix2D;
                scaleMat.a = 1/scale;
                scaleMat.d = 1/scale;
                var projMat = new createjs.Matrix2D;
                projMat.tx = -projectionCenter.x;
                projMat.ty = -projectionCenter.y;
                projMat = projMat.prependMatrix(scaleMat);
                projMat.tx += projectionCenter.x;
                projMat.ty += projectionCenter.y;
                return projMat;
            }
            function handleTick(event) {
                var cameraInstance = exportRoot.___camera___instance;
                if(cameraInstance !== undefined && cameraInstance.pinToObject !== undefined)
                {
                    cameraInstance.x = cameraInstance.pinToObject.x + cameraInstance.pinToObject.pinOffsetX;
                    cameraInstance.y = cameraInstance.pinToObject.y + cameraInstance.pinToObject.pinOffsetY;
                    if(cameraInstance.pinToObject.parent !== undefined && cameraInstance.pinToObject.parent.depth !== undefined)
                        cameraInstance.depth = cameraInstance.pinToObject.parent.depth + cameraInstance.pinToObject.pinOffsetZ;
                }
                applyLayerZDepth(exportRoot);
            }
            function applyLayerZDepth(parent)
            {
                var cameraInstance = parent.___camera___instance;
                var focalLength = 528.25;
                var projectionCenter = { 'x' : 0, 'y' : 0};
                if(parent === exportRoot)
                {
                    var stageCenter = { 'x' : lib.properties.width/2, 'y' : lib.properties.height/2 };
                    projectionCenter.x = stageCenter.x;
                    projectionCenter.y = stageCenter.y;
                }
                for(child in parent.children)
                {
                    var layerObj = parent.children[child];
                    if(layerObj == cameraInstance)
                        continue;
                    applyLayerZDepth(layerObj, cameraInstance);
                    if(layerObj.layerDepth === undefined)
                        continue;
                    if(layerObj.currentFrame != layerObj.parent.currentFrame)
                    {
                        layerObj.gotoAndPlay(layerObj.parent.currentFrame);
                    }
                    var matToApply = new createjs.Matrix2D;
                    var cameraMat = new createjs.Matrix2D;
                    var totalDepth = layerObj.layerDepth ? layerObj.layerDepth : 0;
                    var cameraDepth = 0;
                    if(cameraInstance && !layerObj.isAttachedToCamera)
                    {
                        var mat = cameraInstance.getMatrix();
                        mat.tx -= projectionCenter.x;
                        mat.ty -= projectionCenter.y;
                        cameraMat = mat.invert();
                        cameraMat.prependTransform(projectionCenter.x, projectionCenter.y, 1, 1, 0, 0, 0, 0, 0);
                        cameraMat.appendTransform(-projectionCenter.x, -projectionCenter.y, 1, 1, 0, 0, 0, 0, 0);
                        if(cameraInstance.depth)
                            cameraDepth = cameraInstance.depth;
                    }
                    if(layerObj.depth)
                    {
                        totalDepth = layerObj.depth;
                    }
                    //Offset by camera depth
                    totalDepth -= cameraDepth;
                    if(totalDepth < -focalLength)
                    {
                        matToApply.a = 0;
                        matToApply.d = 0;
                    }
                    else
                    {
                        if(layerObj.layerDepth)
                        {
                            var sizeLockedMat = getProjectionMatrix(parent, layerObj.layerDepth);
                            if(sizeLockedMat)
                            {
                                sizeLockedMat.invert();
                                matToApply.prependMatrix(sizeLockedMat);
                            }
                        }
                        matToApply.prependMatrix(cameraMat);
                        var projMat = getProjectionMatrix(parent, totalDepth);
                        if(projMat)
                        {
                            matToApply.prependMatrix(projMat);
                        }
                    }
                    layerObj.transformMatrix = matToApply;
                }
            }
        }
        //Code to support hidpi screens and responsive scaling.
        function makeResponsive(isResp, respDim, isScale, scaleType) {
            var lastW, lastH, lastS=1;
            window.addEventListener('resize', resizeCanvas);
            resizeCanvas();
            function resizeCanvas() {
                var w = lib.properties.width, h = lib.properties.height;
                var iw = window.innerWidth, ih=window.innerHeight;
                var pRatio = window.devicePixelRatio || 1, xRatio=iw/w, yRatio=ih/h, sRatio=1;
                if(isResp) {
                    if((respDim=='width'&&lastW==iw) || (respDim=='height'&&lastH==ih)) {
                        sRatio = lastS;
                    }
                    else if(!isScale) {
                        if(iw<w || ih<h)
                            sRatio = Math.min(xRatio, yRatio);
                    }
                    else if(scaleType==1) {
                        sRatio = Math.min(xRatio, yRatio);
                    }
                    else if(scaleType==2) {
                        sRatio = Math.max(xRatio, yRatio);
                    }
                }
                canvas.width = w*pRatio*sRatio;
                canvas.height = h*pRatio*sRatio;
                canvas.style.width = anim_container.style.width = dom_overlay_container.style.width = preloaderDiv.style.width = w*sRatio+'px';
                canvas.style.height = anim_container.style.height = dom_overlay_container.style.height = preloaderDiv.style.height = h*sRatio+'px';
                stage.scaleX = pRatio*sRatio;
                stage.scaleY = pRatio*sRatio;
                lastW = iw; lastH = ih; lastS = sRatio;
                stage.tickOnUpdate = false;
                stage.update();
                stage.tickOnUpdate = true;
            }
        }
        makeResponsive(true,'both',true,2);
        AdobeAn.compositionLoaded(lib.properties.id);
        fnStartAnimation();
    }
    init();
});