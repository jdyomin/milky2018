(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [];


// symbols:



(lib.cup = function() {
	this.initialize(img.cup);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,523,302);


(lib.cup1 = function() {
	this.initialize(img.cup1);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,550,302);


(lib.hand2_part = function() {
	this.initialize(img.hand2_part);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,123,140);


(lib.pack = function() {
	this.initialize(img.pack);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,319,523);


(lib.Symbol6 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(12,1,1).p("AiTAkQCTiGCUB2");
	this.shape.setTransform(114.4,152.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(12,1,1).p("AiVAjQCXiFCTB4");
	this.shape_1.setTransform(114.2,151.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(12,1,1).p("AiXAjQCbiECUB6");
	this.shape_2.setTransform(113.9,151);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(12,1,1).p("AiYAiQCfiDCSB8");
	this.shape_3.setTransform(113.7,150.4);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(12,1,1).p("AiaAiQCjiDCSB+");
	this.shape_4.setTransform(113.4,149.9);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(12,1,1).p("AibAhQCmiCCRB/");
	this.shape_5.setTransform(113.2,149.5);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(12,1,1).p("AidAhQCqiBCRCA");
	this.shape_6.setTransform(113,149);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(12,1,1).p("AieAfQCtiACQCC");
	this.shape_7.setTransform(112.9,148.7);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(12,1,1).p("AifAeQCwh/CPCC");
	this.shape_8.setTransform(112.7,148.4);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(12,1,1).p("AigAcQCyh+CPCE");
	this.shape_9.setTransform(112.5,148.1);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(12,1,1).p("AihAbQC1h+COCF");
	this.shape_10.setTransform(112.4,147.9);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(12,1,1).p("AiiAbQC3h+COCG");
	this.shape_11.setTransform(112.3,147.6);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(12,1,1).p("AijAaQC5h9COCG");
	this.shape_12.setTransform(112.2,147.4);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(12,1,1).p("AikAZQC7h9COCH");
	this.shape_13.setTransform(112.1,147.3);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(12,1,1).p("AikAYQC8h8CNCI");
	this.shape_14.setTransform(112,147.1);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(12,1,1).p("AilAYQC9h8COCI");
	this.shape_15.setTransform(111.9,147);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(12,1,1).p("AilAXQC+h7CNCI");
	this.shape_16.setTransform(111.9,146.9);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(12,1,1).p("AilAXQC+h8CNCJ");
	this.shape_17.setTransform(111.8,146.9);
	this.shape_17._off = true;

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#000000").ss(12,1,1).p("AihAcQC0h+CPCE");
	this.shape_18.setTransform(112.5,147.9);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#000000").ss(12,1,1).p("AidAgQCriACQCA");
	this.shape_19.setTransform(113,148.9);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#000000").ss(12,1,1).p("AibAhQCniCCRB/");
	this.shape_20.setTransform(113.2,149.4);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#000000").ss(12,1,1).p("AiaAhQCjiCCSB+");
	this.shape_21.setTransform(113.4,149.9);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#000000").ss(12,1,1).p("AiXAiQCciECTB7");
	this.shape_22.setTransform(113.8,150.8);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#000000").ss(12,1,1).p("AiWAjQCaiFCTB6");
	this.shape_23.setTransform(114,151.1);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#000000").ss(12,1,1).p("AiVAjQCYiFCTB5");
	this.shape_24.setTransform(114.1,151.4);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f().s("#000000").ss(12,1,1).p("AiVAjQCWiFCVB4");
	this.shape_25.setTransform(114.2,151.7);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f().s("#000000").ss(12,1,1).p("AiUAkQCViGCUB3");
	this.shape_26.setTransform(114.3,151.9);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f().s("#000000").ss(12,1,1).p("AiTAkQCTiGCUB3");
	this.shape_27.setTransform(114.4,152);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape,p:{y:152.2}}]}).to({state:[{t:this.shape,p:{y:152.2}}]},10).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3,p:{x:113.7}}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12,p:{x:112.2}}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_17}]},30).to({state:[{t:this.shape_12,p:{x:112.1}}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_3,p:{x:113.6}}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_27}]},1).to({state:[{t:this.shape,p:{y:152.1}}]},1).to({state:[{t:this.shape,p:{y:152.2}}]},1).wait(22));
	this.timeline.addTween(cjs.Tween.get(this.shape_17).wait(27).to({_off:false},0).wait(1).to({y:146.8},0).wait(31).to({_off:true},1).wait(36));

	// Layer 2
	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f().s("#000000").ss(12,1,1).p("AiVAEQCuhkB9CH");
	this.shape_28.setTransform(41.4,156.6);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f().s("#000000").ss(12,1,1).p("AiVAKQCshpB/CE");
	this.shape_29.setTransform(41.7,155.7);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f().s("#000000").ss(12,1,1).p("AiWAQQCqhuCCCB");
	this.shape_30.setTransform(42,155);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f().s("#000000").ss(12,1,1).p("AiWAVQCohyCFB/");
	this.shape_31.setTransform(42.2,154.3);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f().s("#000000").ss(12,1,1).p("AiWAZQClh1CIB8");
	this.shape_32.setTransform(42.4,153.6);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f().s("#000000").ss(12,1,1).p("AiXAeQClh4CKB5");
	this.shape_33.setTransform(42.6,152.9);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f().s("#000000").ss(12,1,1).p("AiXAgQCjh8CMB3");
	this.shape_34.setTransform(42.8,152.6);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f().s("#000000").ss(12,1,1).p("AiXAhQChh/COB1");
	this.shape_35.setTransform(43,152.3);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f().s("#000000").ss(12,1,1).p("AiYAjQCgiCCQBz");
	this.shape_36.setTransform(43.2,152);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f().s("#000000").ss(12,1,1).p("AiXAkQCeiECSBx");
	this.shape_37.setTransform(43.3,151.8);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f().s("#000000").ss(12,1,1).p("AiYAlQCdiGCUBv");
	this.shape_38.setTransform(43.5,151.5);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f().s("#000000").ss(12,1,1).p("AiYAmQCciICVBt");
	this.shape_39.setTransform(43.6,151.3);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f().s("#000000").ss(12,1,1).p("AiYAnQCbiKCWBs");
	this.shape_40.setTransform(43.7,151.1);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f().s("#000000").ss(12,1,1).p("AiYAoQCaiMCXBr");
	this.shape_41.setTransform(43.8,151);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f().s("#000000").ss(12,1,1).p("AiZApQCaiOCZBr");
	this.shape_42.setTransform(43.9,150.8);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f().s("#000000").ss(12,1,1).p("AiYApQCZiOCZBq");
	this.shape_43.setTransform(43.9,150.7);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f().s("#000000").ss(12,1,1).p("AiZAqQCZiQCaBq");
	this.shape_44.setTransform(44,150.6);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f().s("#000000").ss(12,1,1).p("AiZAqQCZiQCaBp");
	this.shape_45.setTransform(44,150.6);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.f().s("#000000").ss(12,1,1).p("AiZAqQCZiQCaBo");
	this.shape_46.setTransform(44,150.5);

	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.f().s("#000000").ss(12,1,1).p("AiYAnQCaiLCXBs");
	this.shape_47.setTransform(43.7,151.1);

	this.shape_48 = new cjs.Shape();
	this.shape_48.graphics.f().s("#000000").ss(12,1,1).p("AiXAjQCfiCCQBz");
	this.shape_48.setTransform(43.1,152.1);

	this.shape_49 = new cjs.Shape();
	this.shape_49.graphics.f().s("#000000").ss(12,1,1).p("AiXAgQCih9CNB3");
	this.shape_49.setTransform(42.9,152.5);

	this.shape_50 = new cjs.Shape();
	this.shape_50.graphics.f().s("#000000").ss(12,1,1).p("AiXAeQCkh4CLB4");
	this.shape_50.setTransform(42.6,152.9);

	this.shape_51 = new cjs.Shape();
	this.shape_51.graphics.f().s("#000000").ss(12,1,1).p("AiWAaQClh1CIB7");
	this.shape_51.setTransform(42.4,153.6);

	this.shape_52 = new cjs.Shape();
	this.shape_52.graphics.f().s("#000000").ss(12,1,1).p("AiWAVQCohxCFB9");
	this.shape_52.setTransform(42.2,154.2);

	this.shape_53 = new cjs.Shape();
	this.shape_53.graphics.f().s("#000000").ss(12,1,1).p("AiWASQCphvCECA");
	this.shape_53.setTransform(42,154.7);

	this.shape_54 = new cjs.Shape();
	this.shape_54.graphics.f().s("#000000").ss(12,1,1).p("AiWAOQCqhsCCCB");
	this.shape_54.setTransform(41.9,155.2);

	this.shape_55 = new cjs.Shape();
	this.shape_55.graphics.f().s("#000000").ss(12,1,1).p("AiVALQCrhqCACD");
	this.shape_55.setTransform(41.8,155.6);

	this.shape_56 = new cjs.Shape();
	this.shape_56.graphics.f().s("#000000").ss(12,1,1).p("AiVAJQCshoB/CE");
	this.shape_56.setTransform(41.7,155.9);

	this.shape_57 = new cjs.Shape();
	this.shape_57.graphics.f().s("#000000").ss(12,1,1).p("AiVAHQCthnB+CG");
	this.shape_57.setTransform(41.6,156.2);

	this.shape_58 = new cjs.Shape();
	this.shape_58.graphics.f().s("#000000").ss(12,1,1).p("AiVAGQCuhmB9CG");
	this.shape_58.setTransform(41.5,156.4);

	this.shape_59 = new cjs.Shape();
	this.shape_59.graphics.f().s("#000000").ss(12,1,1).p("AiVAFQCuhlB9CH");
	this.shape_59.setTransform(41.5,156.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_28}]}).to({state:[{t:this.shape_28}]},10).to({state:[{t:this.shape_29}]},1).to({state:[{t:this.shape_30}]},1).to({state:[{t:this.shape_31}]},1).to({state:[{t:this.shape_32}]},1).to({state:[{t:this.shape_33}]},1).to({state:[{t:this.shape_34}]},1).to({state:[{t:this.shape_35}]},1).to({state:[{t:this.shape_36}]},1).to({state:[{t:this.shape_37}]},1).to({state:[{t:this.shape_38,p:{x:43.5,y:151.5}}]},1).to({state:[{t:this.shape_39}]},1).to({state:[{t:this.shape_40}]},1).to({state:[{t:this.shape_41}]},1).to({state:[{t:this.shape_42}]},1).to({state:[{t:this.shape_43}]},1).to({state:[{t:this.shape_44}]},1).to({state:[{t:this.shape_45,p:{y:150.6}}]},1).to({state:[{t:this.shape_45,p:{y:150.5}}]},1).to({state:[{t:this.shape_46}]},1).to({state:[{t:this.shape_46}]},30).to({state:[{t:this.shape_47}]},1).to({state:[{t:this.shape_38,p:{x:43.4,y:151.6}}]},1).to({state:[{t:this.shape_48}]},1).to({state:[{t:this.shape_49}]},1).to({state:[{t:this.shape_50}]},1).to({state:[{t:this.shape_51}]},1).to({state:[{t:this.shape_52}]},1).to({state:[{t:this.shape_53}]},1).to({state:[{t:this.shape_54}]},1).to({state:[{t:this.shape_55}]},1).to({state:[{t:this.shape_56}]},1).to({state:[{t:this.shape_57}]},1).to({state:[{t:this.shape_58}]},1).to({state:[{t:this.shape_59}]},1).to({state:[{t:this.shape_28}]},1).wait(22));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(20.5,142.6,114.7,23.8);


(lib.Symbol5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 4
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(12,1,1).p("ABYA/Iivh9");
	this.shape.setTransform(37.9,11.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(12,1,1).p("AhNhDQBhBCA6BF");
	this.shape_1.setTransform(36.9,11.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(12,1,1).p("AhDhIQBqBGAdBL");
	this.shape_2.setTransform(35.9,12.2);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(12,1,1).p("Ag5hNQBzBJgBBS");
	this.shape_3.setTransform(34.9,12.7);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(12,1,1).p("AgxhSQB7BMgdBZ");
	this.shape_4.setTransform(34.2,13.2);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(12,1,1).p("AAcBZQA8hhiGhQ");
	this.shape_5.setTransform(33.8,13.7);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(12,1,1).p("AgwhTQB9BMgjBb");
	this.shape_6.setTransform(34.1,13.3);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(12,1,1).p("Ag2hPQB2BKgKBV");
	this.shape_7.setTransform(34.6,12.8);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(12,1,1).p("Ag+hKQBvBHAOBP");
	this.shape_8.setTransform(35.4,12.4);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(12,1,1).p("AhGhGQBmBEAnBJ");
	this.shape_9.setTransform(36.2,12);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(12,1,1).p("AhOhCQBeBCA/BD");
	this.shape_10.setTransform(37.1,11.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},12).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},4).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape}]},1).wait(15));
	this.timeline.addTween(cjs.Tween.get(this.shape).wait(12).to({_off:true},1).wait(10).to({_off:false},0).wait(4).to({_off:true},1).wait(11).to({_off:false},0).wait(15));

	// Layer 3
	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(12,1,1).p("AgeBIQgMh2BLgZ");
	this.shape_11.setTransform(23.2,16.8);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(12,1,1).p("AgZBIQgUh2BLgZ");
	this.shape_12.setTransform(23.5,16.8);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(12,1,1).p("AgUBHQgbh0BLgZ");
	this.shape_13.setTransform(23.7,16.7);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(12,1,1).p("AgOBHQgjh0BLgZ");
	this.shape_14.setTransform(23.9,16.7);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(12,1,1).p("AgJBHQgqhzBLgZ");
	this.shape_15.setTransform(24.1,16.7);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(12,1,1).p("AgCBGQgyhyBLgZ");
	this.shape_16.setTransform(24.2,16.6);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(12,1,1).p("AgIBHQgrhzBLgZ");
	this.shape_17.setTransform(24.1,16.7);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#000000").ss(12,1,1).p("AgMBHQglh0BLgZ");
	this.shape_18.setTransform(23.9,16.7);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#000000").ss(12,1,1).p("AgRBHQgfh0BLgZ");
	this.shape_19.setTransform(23.8,16.7);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#000000").ss(12,1,1).p("AgWBHQgYh1BLgZ");
	this.shape_20.setTransform(23.6,16.8);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#000000").ss(12,1,1).p("AgaBIQgSh2BLgZ");
	this.shape_21.setTransform(23.4,16.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_11}]}).to({state:[{t:this.shape_11}]},12).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_11}]},4).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_11}]},1).wait(15));
	this.timeline.addTween(cjs.Tween.get(this.shape_11).wait(12).to({_off:true},1).wait(10).to({_off:false},0).wait(4).to({_off:true},1).wait(11).to({_off:false},0).wait(15));

	// Layer 2
	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#000000").ss(12,1,1).p("AhbBNQAPiBCogY");
	this.shape_22.setTransform(15.9,13.3);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#000000").ss(12,1,1).p("AhVBNQADiCCogX");
	this.shape_23.setTransform(16.5,13.3);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#000000").ss(12,1,1).p("AhOBOQgKiDCogY");
	this.shape_24.setTransform(17.1,13.4);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f().s("#000000").ss(12,1,1).p("AhHBPQgWiFCogY");
	this.shape_25.setTransform(17.6,13.5);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f().s("#000000").ss(12,1,1).p("Ag/BPQgjiGCogX");
	this.shape_26.setTransform(18,13.5);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f().s("#000000").ss(12,1,1).p("Ag3BQQgviHCogY");
	this.shape_27.setTransform(18.4,13.6);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f().s("#000000").ss(12,1,1).p("Ag+BPQgliGCogX");
	this.shape_28.setTransform(18.1,13.5);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f().s("#000000").ss(12,1,1).p("AhFBPQgaiFCogY");
	this.shape_29.setTransform(17.7,13.5);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f().s("#000000").ss(12,1,1).p("AhLBOQgQiECogX");
	this.shape_30.setTransform(17.3,13.4);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f().s("#000000").ss(12,1,1).p("AhQBOQgGiDCogY");
	this.shape_31.setTransform(16.9,13.4);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f().s("#000000").ss(12,1,1).p("AhWBNQAFiCCogX");
	this.shape_32.setTransform(16.4,13.3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_22}]}).to({state:[{t:this.shape_22}]},12).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_27}]},1).to({state:[{t:this.shape_28}]},1).to({state:[{t:this.shape_29}]},1).to({state:[{t:this.shape_30}]},1).to({state:[{t:this.shape_31}]},1).to({state:[{t:this.shape_32}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_22}]},4).to({state:[{t:this.shape_32}]},1).to({state:[{t:this.shape_31}]},1).to({state:[{t:this.shape_30}]},1).to({state:[{t:this.shape_29}]},1).to({state:[{t:this.shape_28}]},1).to({state:[{t:this.shape_27}]},1).to({state:[{t:this.shape_28}]},1).to({state:[{t:this.shape_29}]},1).to({state:[{t:this.shape_30}]},1).to({state:[{t:this.shape_31}]},1).to({state:[{t:this.shape_32}]},1).to({state:[{t:this.shape_22}]},1).wait(15));
	this.timeline.addTween(cjs.Tween.get(this.shape_22).wait(12).to({_off:true},1).wait(10).to({_off:false},0).wait(4).to({_off:true},1).wait(11).to({_off:false},0).wait(15));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,-1.1,53,31.2);


(lib.Symbol3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 4
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(12,1,1).p("ABtAwQghgYglgYQhLgwgZABQgaABgNAsQgHAVgBAW");
	this.shape.setTransform(22.1,121.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(12,1,1).p("AhxAiQAHgWAKgTQAQgoAbABQAaAABIAuQAkAXAhAY");
	this.shape_1.setTransform(21.6,121.3);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(12,1,1).p("Ah2AbQANgWANgQQASglAdADQAcADBDAqQAkAWAhAZ");
	this.shape_2.setTransform(21.2,121.3);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(12,1,1).p("Ah7AUQATgWAPgOQAWghAdAFQAeAEBAAoQAjAVAhAZ");
	this.shape_3.setTransform(20.7,121.4);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(12,1,1).p("AiAANQAZgWATgMQAYgcAeAGQAeAGA+AlQAiAUAhAZ");
	this.shape_4.setTransform(20.2,121.5);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(12,1,1).p("AiFAHQAfgXAWgKQAagYAgAIQAfAJA7AhQAhAUAhAZ");
	this.shape_5.setTransform(19.7,121.5);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(12,1,1).p("AiKABQAlgXAZgIQAcgUAiAKQAgAKA3AfQAhASAhAa");
	this.shape_6.setTransform(19.2,121.5);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(12,1,1).p("AiPgEQArgYAcgGQAfgQAjAMQAhAMA0AcQAgARAhAa");
	this.shape_7.setTransform(18.7,121.5);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(12,1,1).p("AiUgLQAxgYAfgDQAhgMAjAOQAkAOAxAZQAfAQAhAa");
	this.shape_8.setTransform(18.2,121.5);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(12,1,1).p("AiZgQQA3gZAiAAQAkgJAkAQQAmAQAsAXQAfAOAgAa");
	this.shape_9.setTransform(17.7,121.5);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(12,1,1).p("AiegVQA9gZAlACQAmgFAmASQAmASAqAUQAeANAgAa");
	this.shape_10.setTransform(17.2,121.4);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(12,1,1).p("AiigZQBCgaAoAEQApAAAnATQAoAUAmARQAdANAgAa");
	this.shape_11.setTransform(16.7,121.2);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(12,1,1).p("AingdQBIgaArAHQAsADAoAWQApAVAjAPQAcAMAgAa");
	this.shape_12.setTransform(16.2,120.9);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(12,1,1).p("AisghQBOgaAuAJQAuAHApAYQArAXAfAMQAcALAgAa");
	this.shape_13.setTransform(15.7,120.7);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(12,1,1).p("ACyA4QgggbgbgJQgcgKgsgZQgsgZgvgLQgxgMhUAb");
	this.shape_14.setTransform(15.2,120.4);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(12,1,1).p("AiwgkQBSgbAxAJQAwAIAsAbQAsAaAbAMQAbALAgAc");
	this.shape_15.setTransform(15.3,120.2);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(12,1,1).p("AivgkQBRgbAxAFQAwAGAsAdQArAcAbANQAbANAgAd");
	this.shape_16.setTransform(15.5,120);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(12,1,1).p("AiugkQBQgcAxADQAvADAsAfQAsAeAbAPQAaAOAgAe");
	this.shape_17.setTransform(15.6,119.8);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#000000").ss(12,1,1).p("AitgjQBPgdAxABQAvAAAsAgQArAgAbAQQAaAQAgAf");
	this.shape_18.setTransform(15.7,119.6);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#000000").ss(12,1,1).p("AisgjQBNgdAxgCQAwgDArAjQAsAiAaARQAaASAgAg");
	this.shape_19.setTransform(15.8,119.3);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#000000").ss(12,1,1).p("AirgiQBLgdAxgFQAwgFAsAkQArAkAaATQAaATAgAh");
	this.shape_20.setTransform(15.9,119);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#000000").ss(12,1,1).p("AiqghQBKgdAxgIQAwgIArAmQArAmAaAUQAaAVAgAi");
	this.shape_21.setTransform(16,118.7);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#000000").ss(12,1,1).p("AipggQBJgeAxgKQAwgLArAoQArAoAaAVQAZAWAgAk");
	this.shape_22.setTransform(16.1,118.4);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#000000").ss(12,1,1).p("AiogfQBIgeAxgNQAvgOArAqQAsApAZAXQAZAYAgAl");
	this.shape_23.setTransform(16.2,118.1);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#000000").ss(12,1,1).p("AimgeQBFgeAxgRQAwgQArAsQArArAZAZQAZAZAgAm");
	this.shape_24.setTransform(16.3,117.8);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f().s("#000000").ss(12,1,1).p("AimgcQBFgfAxgTQAwgTArAtQArAtAZAbQAYAbAgAm");
	this.shape_25.setTransform(16.4,117.5);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f().s("#000000").ss(12,1,1).p("AilgbQBDggAygVQAwgWAqAvQArAvAZAcQAYAdAfAn");
	this.shape_26.setTransform(16.5,117.2);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f().s("#000000").ss(12,1,1).p("AikgaQBCggAxgYQAxgZAqAxQArAxAYAdQAZAeAfAp");
	this.shape_27.setTransform(16.6,116.9);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f().s("#000000").ss(12,1,1).p("AiigYQBAghAxgbQAxgbApAyQArAzAYAfQAYAgAfAq");
	this.shape_28.setTransform(16.7,116.5);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f().s("#000000").ss(12,1,1).p("ACiBiQgfgrgYgiQgYgggqg1Qgqg0gxAeQgxAeg+Ah");
	this.shape_29.setTransform(16.8,116.2);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f().s("#000000").ss(12,1,1).p("AiegTQA7ggAugeQAvgfAoAxQApAxAcAiQAZAgAfAq");
	this.shape_30.setTransform(17.2,116.6);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f().s("#000000").ss(12,1,1).p("AiagPQA2ggAsgdQAsggAnAuQAoAtAfAiQAaAhAfAo");
	this.shape_31.setTransform(17.5,117);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f().s("#000000").ss(12,1,1).p("AiXgMQAzgfAogcQAqghAmAqQAnAqAjAjQAaAgAgAn");
	this.shape_32.setTransform(17.9,117.4);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f().s("#000000").ss(12,1,1).p("AiTgIQAugeAlgcQAoghAlAmQAmAmAmAlQAbAfAgAm");
	this.shape_33.setTransform(18.3,117.7);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f().s("#000000").ss(12,1,1).p("AiPgEQAqgdAjgcQAlgiAkAiQAkAjAqAlQAcAfAgAl");
	this.shape_34.setTransform(18.6,118.1);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f().s("#000000").ss(12,1,1).p("AiMAAQAmgcAggbQAjgkAjAfQAjAfAtAnQAdAeAgAj");
	this.shape_35.setTransform(19,118.5);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f().s("#000000").ss(12,1,1).p("AiIADQAigaAdgbQAggkAjAbQAhAcAxAnQAeAdAgAj");
	this.shape_36.setTransform(19.3,118.8);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f().s("#000000").ss(12,1,1).p("AiFAHQAegaAbgZQAegmAhAYQAhAYAzAoQAfAdAgAh");
	this.shape_37.setTransform(19.7,119.2);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f().s("#000000").ss(12,1,1).p("AiCALQAagZAYgZQAbgmAhAUQAfAUA3AqQAhAcAfAg");
	this.shape_38.setTransform(20,119.5);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f().s("#000000").ss(12,1,1).p("Ah+AQQAWgYAVgZQAZgnAfAQQAfARA6ArQAhAbAgAf");
	this.shape_39.setTransform(20.4,119.8);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f().s("#000000").ss(12,1,1).p("Ah7AVQATgYARgYQAXgoAfANQAeANA8ArQAiAbAgAe");
	this.shape_40.setTransform(20.7,120.1);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f().s("#000000").ss(12,1,1).p("Ah3AZQAOgXAPgXQAUgpAdAJQAdAKBAAsQAjAbAhAc");
	this.shape_41.setTransform(21.1,120.4);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f().s("#000000").ss(12,1,1).p("AhzAeQAJgXAMgWQATgqAbAGQAcAGBEAuQAkAZAgAb");
	this.shape_42.setTransform(21.4,120.7);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f().s("#000000").ss(12,1,1).p("AhwAkQAGgXAJgVQAQgrAbACQAaACBHAvQAlAZAhAa");
	this.shape_43.setTransform(21.8,120.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_27}]},1).to({state:[{t:this.shape_28}]},1).to({state:[{t:this.shape_29}]},1).to({state:[{t:this.shape_30}]},1).to({state:[{t:this.shape_31}]},1).to({state:[{t:this.shape_32}]},1).to({state:[{t:this.shape_33}]},1).to({state:[{t:this.shape_34}]},1).to({state:[{t:this.shape_35}]},1).to({state:[{t:this.shape_36}]},1).to({state:[{t:this.shape_37}]},1).to({state:[{t:this.shape_38}]},1).to({state:[{t:this.shape_39}]},1).to({state:[{t:this.shape_40}]},1).to({state:[{t:this.shape_41}]},1).to({state:[{t:this.shape_42}]},1).to({state:[{t:this.shape_43}]},1).to({state:[{t:this.shape}]},1).wait(1));

	// Layer 3
	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f().s("#000000").ss(12,1,1).p("AiLhoQArgHAzABQBlACApAoQApApACBIQABAlgIAd");
	this.shape_44.setTransform(19.7,113.4);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f().s("#000000").ss(12,1,1).p("AiJheQAqgLAzgCQBhgBApAnQAoAoAEBHQACAlgIAd");
	this.shape_45.setTransform(19.8,113.6);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.f().s("#000000").ss(12,1,1).p("AiHhUQApgQAygFQBdgDApAmQAoAnAFBFQADAngHAd");
	this.shape_46.setTransform(20,113.9);

	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.f().s("#000000").ss(12,1,1).p("AiFhKQAngUAzgIQBZgGAoAmQApAmAFBDQAFAogIAd");
	this.shape_47.setTransform(20.1,114.1);

	this.shape_48 = new cjs.Shape();
	this.shape_48.graphics.f().s("#000000").ss(12,1,1).p("AiDg/QAngaAygKQBVgJAoAlQAoAmAHBBQAGApgIAd");
	this.shape_48.setTransform(20.2,114.3);

	this.shape_49 = new cjs.Shape();
	this.shape_49.graphics.f().s("#000000").ss(12,1,1).p("AiCg1QAmgeAygNQBRgMAoAlQAoAkAIBAQAHAqgHAd");
	this.shape_49.setTransform(20.4,114.5);

	this.shape_50 = new cjs.Shape();
	this.shape_50.graphics.f().s("#000000").ss(12,1,1).p("AiAgqQAlgjAygQQBNgOAoAjQAnAkAJA+QAJArgIAd");
	this.shape_50.setTransform(20.5,114.7);

	this.shape_51 = new cjs.Shape();
	this.shape_51.graphics.f().s("#000000").ss(12,1,1).p("Ah+gfQAkgoAxgSQBKgSAnAjQAoAjAKA9QAJArgHAd");
	this.shape_51.setTransform(20.7,114.8);

	this.shape_52 = new cjs.Shape();
	this.shape_52.graphics.f().s("#000000").ss(12,1,1).p("Ah9gVQAjgsAxgVQBGgVAnAjQAoAiALA7QALAsgIAd");
	this.shape_52.setTransform(20.8,115);

	this.shape_53 = new cjs.Shape();
	this.shape_53.graphics.f().s("#000000").ss(12,1,1).p("Ah7gKQAigxAxgXQBCgYAnAiQAnAhAMA6QAMAtgHAd");
	this.shape_53.setTransform(21,115.2);

	this.shape_54 = new cjs.Shape();
	this.shape_54.graphics.f().s("#000000").ss(12,1,1).p("Ah5AAQAhg0AwgbQA+gaAnAhQAnAgAOA4QANAugIAd");
	this.shape_54.setTransform(21.1,115.3);

	this.shape_55 = new cjs.Shape();
	this.shape_55.graphics.f().s("#000000").ss(12,1,1).p("Ah4ALQAhg5AwgdQA6gdAmAgQAnAgAPA2QAOAvgHAd");
	this.shape_55.setTransform(21.3,115.5);

	this.shape_56 = new cjs.Shape();
	this.shape_56.graphics.f().s("#000000").ss(12,1,1).p("Ah2AWQAgg9AwggQA2ggAmAfQAnAfAQA1QAPAwgHAd");
	this.shape_56.setTransform(21.4,115.6);

	this.shape_57 = new cjs.Shape();
	this.shape_57.graphics.f().s("#000000").ss(12,1,1).p("Ah0AhQAehCAwgjQAygiAmAeQAmAfARAyQARAxgHAd");
	this.shape_57.setTransform(21.6,115.7);

	this.shape_58 = new cjs.Shape();
	this.shape_58.graphics.f().s("#000000").ss(12,1,1).p("AhzAsQAehGAvgmQAugmAmAeQAmAeATAxQASAygIAd");
	this.shape_58.setTransform(21.7,115.9);

	this.shape_59 = new cjs.Shape();
	this.shape_59.graphics.f().s("#000000").ss(12,1,1).p("Ah1AoQAghCAvgjQAsgjAmAcQAmAcAUAuQAUAwgEAc");
	this.shape_59.setTransform(21.3,116.3);

	this.shape_60 = new cjs.Shape();
	this.shape_60.graphics.f().s("#000000").ss(12,1,1).p("Ah5AjQAlg9AsghQAsggAmAaQAlAbAWArQAVAtgBAb");
	this.shape_60.setTransform(21,116.7);

	this.shape_61 = new cjs.Shape();
	this.shape_61.graphics.f().s("#000000").ss(12,1,1).p("Ah8AfQAog5ArgeQAqgeAmAZQAmAZAXApQAXAqACAa");
	this.shape_61.setTransform(20.7,117.1);

	this.shape_62 = new cjs.Shape();
	this.shape_62.graphics.f().s("#000000").ss(12,1,1).p("AiAAbQArg1AqgbQAqgcAlAYQAlAXAZAmQAZAnAGAa");
	this.shape_62.setTransform(20.4,117.5);

	this.shape_63 = new cjs.Shape();
	this.shape_63.graphics.f().s("#000000").ss(12,1,1).p("AiDAWQAugwApgZQAogZAlAWQAmAWAaAjQAaAlAJAZ");
	this.shape_63.setTransform(20,118);

	this.shape_64 = new cjs.Shape();
	this.shape_64.graphics.f().s("#000000").ss(12,1,1).p("AiHASQAygrAogXQAmgWAmAUQAlAUAcAhQAbAhANAZ");
	this.shape_64.setTransform(19.7,118.4);

	this.shape_65 = new cjs.Shape();
	this.shape_65.graphics.f().s("#000000").ss(12,1,1).p("AiKANQA1gmAmgUQAmgUAlATQAlASAeAeQAdAfAPAX");
	this.shape_65.setTransform(19.4,118.8);

	this.shape_66 = new cjs.Shape();
	this.shape_66.graphics.f().s("#000000").ss(12,1,1).p("AiNAJQA4giAlgRQAkgSAkARQAmARAfAcQAfAbASAX");
	this.shape_66.setTransform(19.1,119.2);

	this.shape_67 = new cjs.Shape();
	this.shape_67.graphics.f().s("#000000").ss(12,1,1).p("AiRAFQA8gdAjgPQAjgQAkAQQAmAQAgAYQAhAZAWAW");
	this.shape_67.setTransform(18.8,119.6);

	this.shape_68 = new cjs.Shape();
	this.shape_68.graphics.f().s("#000000").ss(12,1,1).p("AiVAAQBAgYAhgNQAjgNAkAPQAlANAiAWQAiAWAaAW");
	this.shape_68.setTransform(18.5,120.1);

	this.shape_69 = new cjs.Shape();
	this.shape_69.graphics.f().s("#000000").ss(12,1,1).p("AiYgDQBCgVAhgKQAhgKAkAMQAlANAkATQAjATAdAV");
	this.shape_69.setTransform(18.1,120.5);

	this.shape_70 = new cjs.Shape();
	this.shape_70.graphics.f().s("#000000").ss(12,1,1).p("AibgIQBFgQAggHQAfgIAkALQAlALAlARQAmAQAfAU");
	this.shape_70.setTransform(17.8,120.9);

	this.shape_71 = new cjs.Shape();
	this.shape_71.graphics.f().s("#000000").ss(12,1,1).p("AifgMQBJgMAegFQAegFAkAKQAlAJAnAOQAnANAjAT");
	this.shape_71.setTransform(17.5,121.3);

	this.shape_72 = new cjs.Shape();
	this.shape_72.graphics.f().s("#000000").ss(12,1,1).p("AiigRQBMgGAdgDQAcgDAkAIQAlAIApAMQAoAKAmAS");
	this.shape_72.setTransform(17.2,121.7);

	this.shape_73 = new cjs.Shape();
	this.shape_73.graphics.f().s("#000000").ss(12,1,1).p("AimgUQBQgCAbgBQAbAAAkAGQAlAHAqAIQAqAIAqAS");
	this.shape_73.setTransform(16.9,122.1);

	this.shape_74 = new cjs.Shape();
	this.shape_74.graphics.f().s("#000000").ss(12,1,1).p("AikgaQBNgCAeAAQAggBAkAJQAlAIAnANQAoAKAmAS");
	this.shape_74.setTransform(17,121.5);

	this.shape_75 = new cjs.Shape();
	this.shape_75.graphics.f().s("#000000").ss(12,1,1).p("AiiggQBLgCAeAAQAlAAAlALQAlAKAlARQAlAMAjAT");
	this.shape_75.setTransform(17.2,120.9);

	this.shape_76 = new cjs.Shape();
	this.shape_76.graphics.f().s("#000000").ss(12,1,1).p("AigglQBIgDAhAAQAqAAAkANQAmANAiAVQAiAOAgAU");
	this.shape_76.setTransform(17.4,120.4);

	this.shape_77 = new cjs.Shape();
	this.shape_77.graphics.f().s("#000000").ss(12,1,1).p("AiegrQBGgDAhAAQAvAAAmAQQAmAPAfAZQAfAQAcAV");
	this.shape_77.setTransform(17.6,119.8);

	this.shape_78 = new cjs.Shape();
	this.shape_78.graphics.f().s("#000000").ss(12,1,1).p("AicgwQBDgEAkABQA0AAAlASQAmARAdAeQAcARAaAW");
	this.shape_78.setTransform(17.7,119.2);

	this.shape_79 = new cjs.Shape();
	this.shape_79.graphics.f().s("#000000").ss(12,1,1).p("Aiag2QBBgDAlAAQA4AAAnAUQAmAUAaAiQAaAUAWAW");
	this.shape_79.setTransform(17.9,118.6);

	this.shape_80 = new cjs.Shape();
	this.shape_80.graphics.f().s("#000000").ss(12,1,1).p("AiXg8QA+gEAmABQA9AAAnAXQAnAWAXAmQAXAVATAX");
	this.shape_80.setTransform(18.1,118);

	this.shape_81 = new cjs.Shape();
	this.shape_81.graphics.f().s("#000000").ss(12,1,1).p("AiWhBQA8gEAoAAQBCABAnAYQAnAZAVAqQAUAYAQAX");
	this.shape_81.setTransform(18.2,117.4);

	this.shape_82 = new cjs.Shape();
	this.shape_82.graphics.f().s("#000000").ss(12,1,1).p("AiThHQA5gFApABQBHABAoAbQAnAaASAvQASAZAMAZ");
	this.shape_82.setTransform(18.4,116.9);

	this.shape_83 = new cjs.Shape();
	this.shape_83.graphics.f().s("#000000").ss(12,1,1).p("AiShMQA3gFArAAQBMACAoAcQAoAdAPAzQAPAcAJAZ");
	this.shape_83.setTransform(18.6,116.3);

	this.shape_84 = new cjs.Shape();
	this.shape_84.graphics.f().s("#000000").ss(12,1,1).p("AiQhSQA1gFAtABQBRABAnAfQAoAfANA4QAMAdAGAa");
	this.shape_84.setTransform(18.7,115.7);

	this.shape_85 = new cjs.Shape();
	this.shape_85.graphics.f().s("#000000").ss(12,1,1).p("AiNhYQAygFAuABQBWABAoAhQAoAiAKA8QAJAfADAa");
	this.shape_85.setTransform(18.9,115.1);

	this.shape_86 = new cjs.Shape();
	this.shape_86.graphics.f().s("#000000").ss(12,1,1).p("AiMhdQAwgGAwABQBbACAoAjQApAkAHBAQAGAhgBAb");
	this.shape_86.setTransform(19.1,114.5);

	this.shape_87 = new cjs.Shape();
	this.shape_87.graphics.f().s("#000000").ss(12,1,1).p("AiLhiQAtgHAxABQBgACApAmQApAmAFBEQADAjgEAc");
	this.shape_87.setTransform(19.4,113.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_44}]}).to({state:[{t:this.shape_45}]},1).to({state:[{t:this.shape_46}]},1).to({state:[{t:this.shape_47}]},1).to({state:[{t:this.shape_48}]},1).to({state:[{t:this.shape_49}]},1).to({state:[{t:this.shape_50}]},1).to({state:[{t:this.shape_51}]},1).to({state:[{t:this.shape_52}]},1).to({state:[{t:this.shape_53}]},1).to({state:[{t:this.shape_54}]},1).to({state:[{t:this.shape_55}]},1).to({state:[{t:this.shape_56}]},1).to({state:[{t:this.shape_57}]},1).to({state:[{t:this.shape_58}]},1).to({state:[{t:this.shape_59}]},1).to({state:[{t:this.shape_60}]},1).to({state:[{t:this.shape_61}]},1).to({state:[{t:this.shape_62}]},1).to({state:[{t:this.shape_63}]},1).to({state:[{t:this.shape_64}]},1).to({state:[{t:this.shape_65}]},1).to({state:[{t:this.shape_66}]},1).to({state:[{t:this.shape_67}]},1).to({state:[{t:this.shape_68}]},1).to({state:[{t:this.shape_69}]},1).to({state:[{t:this.shape_70}]},1).to({state:[{t:this.shape_71}]},1).to({state:[{t:this.shape_72}]},1).to({state:[{t:this.shape_73}]},1).to({state:[{t:this.shape_74}]},1).to({state:[{t:this.shape_75}]},1).to({state:[{t:this.shape_76}]},1).to({state:[{t:this.shape_77}]},1).to({state:[{t:this.shape_78}]},1).to({state:[{t:this.shape_79}]},1).to({state:[{t:this.shape_80}]},1).to({state:[{t:this.shape_81}]},1).to({state:[{t:this.shape_82}]},1).to({state:[{t:this.shape_83}]},1).to({state:[{t:this.shape_84}]},1).to({state:[{t:this.shape_85}]},1).to({state:[{t:this.shape_86}]},1).to({state:[{t:this.shape_87}]},1).to({state:[{t:this.shape_44}]},1).wait(1));

	// Layer 2
	this.instance = new lib.hand2_part();
	this.instance.parent = this;
	this.instance.setTransform(28,1);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(45));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-0.2,0,151.3,141);


(lib.Symbol2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(12,1,1).p("AAGhDQgnhKgwgeQgygegmAfQgsBGA5ByQA6B2BzA3QBmhJArhbQAihIgJg8QgEgbgOgOQgPgPgagFQhEgMg2Bzg");
	this.shape.setTransform(25.8,24.4);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(10.9,1,1).p("AipiqQAmgfAyAeQAwAeAnBKQA2hzBEAMQAaAFAPAPQAOAOAEAbQAJA8giBIQgrBbhmBJQhzg3g6h2Qg5hyAshGg");
	this.shape_1.setTransform(25.8,24.4);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(9.9,1,1).p("AipiqQAmgfAyAeQAwAeAnBKQA2hzBEAMQAaAFAPAPQAOAOAEAbQAJA8giBIQgrBbhmBJQhzg3g6h2Qg5hyAshGg");
	this.shape_2.setTransform(25.8,24.4);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(9.1,1,1).p("AipiqQAmgfAyAeQAwAeAnBKQA2hzBEAMQAaAFAPAPQAOAOAEAbQAJA8giBIQgrBbhmBJQhzg3g6h2Qg5hyAshGg");
	this.shape_3.setTransform(25.8,24.4);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(8.5,1,1).p("AipiqQAmgfAyAeQAwAeAnBKQA2hzBEAMQAaAFAPAPQAOAOAEAbQAJA8giBIQgrBbhmBJQhzg3g6h2Qg5hyAshGg");
	this.shape_4.setTransform(25.8,24.4);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(8,1,1).p("AAGhDQgnhKgwgeQgygegmAfQgsBGA5ByQA6B2BzA3QBmhJArhbQAihIgJg8QgEgbgOgOQgPgPgagFQhEgMg2Bzg");
	this.shape_5.setTransform(25.8,24.4);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(8.8,1,1).p("AipiqQAmgfAyAeQAwAeAnBKQA2hzBEAMQAaAFAPAPQAOAOAEAbQAJA8giBIQgrBbhmBJQhzg3g6h2Qg5hyAshGg");
	this.shape_6.setTransform(25.8,24.4);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(9.6,1,1).p("AipiqQAmgfAyAeQAwAeAnBKQA2hzBEAMQAaAFAPAPQAOAOAEAbQAJA8giBIQgrBbhmBJQhzg3g6h2Qg5hyAshGg");
	this.shape_7.setTransform(25.8,24.4);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(10.2,1,1).p("AipiqQAmgfAyAeQAwAeAnBKQA2hzBEAMQAaAFAPAPQAOAOAEAbQAJA8giBIQgrBbhmBJQhzg3g6h2Qg5hyAshGg");
	this.shape_8.setTransform(25.8,24.4);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(10.8,1,1).p("AipiqQAmgfAyAeQAwAeAnBKQA2hzBEAMQAaAFAPAPQAOAOAEAbQAJA8giBIQgrBbhmBJQhzg3g6h2Qg5hyAshGg");
	this.shape_9.setTransform(25.8,24.4);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(11.3,1,1).p("AipiqQAmgfAyAeQAwAeAnBKQA2hzBEAMQAaAFAPAPQAOAOAEAbQAJA8giBIQgrBbhmBJQhzg3g6h2Qg5hyAshGg");
	this.shape_10.setTransform(25.8,24.4);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(11.7,1,1).p("AipiqQAmgfAyAeQAwAeAnBKQA2hzBEAMQAaAFAPAPQAOAOAEAbQAJA8giBIQgrBbhmBJQhzg3g6h2Qg5hyAshGg");
	this.shape_11.setTransform(25.8,24.4);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},6).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},6).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape}]},1).wait(6));
	this.timeline.addTween(cjs.Tween.get(this.shape).to({_off:true},1).wait(11).to({_off:false},0).wait(6).to({_off:true},1).wait(11).to({_off:false},0).wait(6).to({_off:true},1).wait(11).to({_off:false},0).wait(6));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,-0.2,52,49.4);


(lib.Symbol1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(12,1,1).p("AAGhDQgnhKgwgeQgygegmAfQgsBGA5ByQA6B2BzA3QBmhJArhbQAihIgJg8QgEgbgOgOQgPgPgagFQhEgMg2Bzg");
	this.shape.setTransform(25.8,24.4);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(10.9,1,1).p("AipiqQAmgfAyAeQAwAeAnBKQA2hzBEAMQAaAFAPAPQAOAOAEAbQAJA8giBIQgrBbhmBJQhzg3g6h2Qg5hyAshGg");
	this.shape_1.setTransform(25.8,24.4);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(9.9,1,1).p("AipiqQAmgfAyAeQAwAeAnBKQA2hzBEAMQAaAFAPAPQAOAOAEAbQAJA8giBIQgrBbhmBJQhzg3g6h2Qg5hyAshGg");
	this.shape_2.setTransform(25.8,24.4);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(9.1,1,1).p("AipiqQAmgfAyAeQAwAeAnBKQA2hzBEAMQAaAFAPAPQAOAOAEAbQAJA8giBIQgrBbhmBJQhzg3g6h2Qg5hyAshGg");
	this.shape_3.setTransform(25.8,24.4);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(8.5,1,1).p("AipiqQAmgfAyAeQAwAeAnBKQA2hzBEAMQAaAFAPAPQAOAOAEAbQAJA8giBIQgrBbhmBJQhzg3g6h2Qg5hyAshGg");
	this.shape_4.setTransform(25.8,24.4);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(8,1,1).p("AAGhDQgnhKgwgeQgygegmAfQgsBGA5ByQA6B2BzA3QBmhJArhbQAihIgJg8QgEgbgOgOQgPgPgagFQhEgMg2Bzg");
	this.shape_5.setTransform(25.8,24.4);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(8.8,1,1).p("AipiqQAmgfAyAeQAwAeAnBKQA2hzBEAMQAaAFAPAPQAOAOAEAbQAJA8giBIQgrBbhmBJQhzg3g6h2Qg5hyAshGg");
	this.shape_6.setTransform(25.8,24.4);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(9.6,1,1).p("AipiqQAmgfAyAeQAwAeAnBKQA2hzBEAMQAaAFAPAPQAOAOAEAbQAJA8giBIQgrBbhmBJQhzg3g6h2Qg5hyAshGg");
	this.shape_7.setTransform(25.8,24.4);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(10.2,1,1).p("AipiqQAmgfAyAeQAwAeAnBKQA2hzBEAMQAaAFAPAPQAOAOAEAbQAJA8giBIQgrBbhmBJQhzg3g6h2Qg5hyAshGg");
	this.shape_8.setTransform(25.8,24.4);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(10.8,1,1).p("AipiqQAmgfAyAeQAwAeAnBKQA2hzBEAMQAaAFAPAPQAOAOAEAbQAJA8giBIQgrBbhmBJQhzg3g6h2Qg5hyAshGg");
	this.shape_9.setTransform(25.8,24.4);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(11.3,1,1).p("AipiqQAmgfAyAeQAwAeAnBKQA2hzBEAMQAaAFAPAPQAOAOAEAbQAJA8giBIQgrBbhmBJQhzg3g6h2Qg5hyAshGg");
	this.shape_10.setTransform(25.8,24.4);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(11.7,1,1).p("AipiqQAmgfAyAeQAwAeAnBKQA2hzBEAMQAaAFAPAPQAOAOAEAbQAJA8giBIQgrBbhmBJQhzg3g6h2Qg5hyAshGg");
	this.shape_11.setTransform(25.8,24.4);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},6).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},6).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape}]},1).wait(6));
	this.timeline.addTween(cjs.Tween.get(this.shape).to({_off:true},1).wait(11).to({_off:false},0).wait(6).to({_off:true},1).wait(11).to({_off:false},0).wait(6).to({_off:true},1).wait(11).to({_off:false},0).wait(6));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,-0.2,52,49.4);


// stage3 content:
(lib.Zapas_global_big = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 6
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(12,1,1).p("AjOhNQAABBA0ApQA0ApBGAHQBFAGBDgZQBEgZAYgxQAMgZgBgV");
	this.shape.setTransform(785.9,308.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(54));

	// Layer 16
	this.instance = new lib.Symbol5();
	this.instance.parent = this;
	this.instance.setTransform(621.5,383.2,1,1,0,0,0,26.5,15);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(3).to({x:630.3,y:384.7},8,cjs.Ease.get(1)).wait(29).to({x:621.5,y:383.2},8,cjs.Ease.get(1)).wait(6));

	// Layer 11
	this.instance_1 = new lib.Symbol3();
	this.instance_1.parent = this;
	this.instance_1.setTransform(842.5,381,1,1,0,0,0,75.5,70);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(54));

	// Layer 13
	this.instance_2 = new lib.Symbol2();
	this.instance_2.parent = this;
	this.instance_2.setTransform(827,269.5,1,1,0,0,0,26,24.5);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(1).to({regX:25.8,regY:24.4,scaleX:1.17,scaleY:1.17,x:826.8,y:269.3},0).wait(1).to({scaleX:1.24,scaleY:1.24},0).wait(1).to({scaleX:1.28,scaleY:1.28,x:826.7},0).wait(1).to({scaleX:1.3,scaleY:1.3},0).wait(1).to({regX:26,regY:24.5,scaleX:1.3,scaleY:1.3,x:827,y:269.5},0).wait(1).to({regX:25.8,regY:24.4,scaleX:1.29,scaleY:1.29,x:826.8,y:269.3},0).wait(1).to({scaleX:1.26,scaleY:1.26},0).wait(1).to({scaleX:1.22,scaleY:1.22},0).wait(1).to({scaleX:1.15,scaleY:1.15,x:826.7},0).wait(1).to({scaleX:1.08,scaleY:1.08,x:826.8,y:269.4},0).wait(1).to({scaleX:1.02,scaleY:1.02},0).wait(1).to({regX:26,regY:24.5,scaleX:1,scaleY:1,x:827,y:269.5},0).wait(7).to({regX:25.8,regY:24.4,scaleX:1.17,scaleY:1.17,x:826.8,y:269.3},0).wait(1).to({scaleX:1.24,scaleY:1.24},0).wait(1).to({scaleX:1.28,scaleY:1.28,x:826.7},0).wait(1).to({scaleX:1.3,scaleY:1.3},0).wait(1).to({regX:26,regY:24.5,scaleX:1.3,scaleY:1.3,x:827,y:269.5},0).wait(1).to({regX:25.8,regY:24.4,scaleX:1.29,scaleY:1.29,x:826.8,y:269.3},0).wait(1).to({scaleX:1.26,scaleY:1.26},0).wait(1).to({scaleX:1.22,scaleY:1.22},0).wait(1).to({scaleX:1.15,scaleY:1.15,x:826.7},0).wait(1).to({scaleX:1.08,scaleY:1.08,x:826.8,y:269.4},0).wait(1).to({scaleX:1.02,scaleY:1.02},0).wait(1).to({regX:26,regY:24.5,scaleX:1,scaleY:1,x:827,y:269.5},0).wait(7).to({regX:25.8,regY:24.4,scaleX:1.17,scaleY:1.17,x:826.8,y:269.3},0).wait(1).to({scaleX:1.24,scaleY:1.24},0).wait(1).to({scaleX:1.28,scaleY:1.28,x:826.7},0).wait(1).to({scaleX:1.3,scaleY:1.3},0).wait(1).to({regX:26,regY:24.5,scaleX:1.3,scaleY:1.3,x:827,y:269.5},0).wait(1).to({regX:25.8,regY:24.4,scaleX:1.29,scaleY:1.29,x:826.8,y:269.3},0).wait(1).to({scaleX:1.26,scaleY:1.26},0).wait(1).to({scaleX:1.22,scaleY:1.22},0).wait(1).to({scaleX:1.15,scaleY:1.15,x:826.7},0).wait(1).to({scaleX:1.08,scaleY:1.08,x:826.8,y:269.4},0).wait(1).to({scaleX:1.02,scaleY:1.02},0).wait(1).to({regX:26,regY:24.5,scaleX:1,scaleY:1,x:827,y:269.5},0).wait(6));

	// Layer 12
	this.instance_3 = new lib.Symbol1();
	this.instance_3.parent = this;
	this.instance_3.setTransform(745,270.5,1,1,0,0,0,26,24.5);

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(1).to({regX:25.8,regY:24.4,scaleX:1.17,scaleY:1.17,x:744.8,y:270.3},0).wait(1).to({scaleX:1.24,scaleY:1.24},0).wait(1).to({scaleX:1.28,scaleY:1.28,x:744.7},0).wait(1).to({scaleX:1.3,scaleY:1.3},0).wait(1).to({regX:26,regY:24.5,scaleX:1.3,scaleY:1.3,x:745,y:270.5},0).wait(1).to({regX:25.8,regY:24.4,scaleX:1.29,scaleY:1.29,x:744.8,y:270.3},0).wait(1).to({scaleX:1.26,scaleY:1.26},0).wait(1).to({scaleX:1.22,scaleY:1.22},0).wait(1).to({scaleX:1.15,scaleY:1.15,x:744.7},0).wait(1).to({scaleX:1.08,scaleY:1.08,x:744.8,y:270.4},0).wait(1).to({scaleX:1.02,scaleY:1.02},0).wait(1).to({regX:26,regY:24.5,scaleX:1,scaleY:1,x:745,y:270.5},0).wait(7).to({regX:25.8,regY:24.4,scaleX:1.17,scaleY:1.17,x:744.8,y:270.3},0).wait(1).to({scaleX:1.24,scaleY:1.24},0).wait(1).to({scaleX:1.28,scaleY:1.28,x:744.7},0).wait(1).to({scaleX:1.3,scaleY:1.3},0).wait(1).to({regX:26,regY:24.5,scaleX:1.3,scaleY:1.3,x:745,y:270.5},0).wait(1).to({regX:25.8,regY:24.4,scaleX:1.29,scaleY:1.29,x:744.8,y:270.3},0).wait(1).to({scaleX:1.26,scaleY:1.26},0).wait(1).to({scaleX:1.22,scaleY:1.22},0).wait(1).to({scaleX:1.15,scaleY:1.15,x:744.7},0).wait(1).to({scaleX:1.08,scaleY:1.08,x:744.8,y:270.4},0).wait(1).to({scaleX:1.02,scaleY:1.02},0).wait(1).to({regX:26,regY:24.5,scaleX:1,scaleY:1,x:745,y:270.5},0).wait(7).to({regX:25.8,regY:24.4,scaleX:1.17,scaleY:1.17,x:744.8,y:270.3},0).wait(1).to({scaleX:1.24,scaleY:1.24},0).wait(1).to({scaleX:1.28,scaleY:1.28,x:744.7},0).wait(1).to({scaleX:1.3,scaleY:1.3},0).wait(1).to({regX:26,regY:24.5,scaleX:1.3,scaleY:1.3,x:745,y:270.5},0).wait(1).to({regX:25.8,regY:24.4,scaleX:1.29,scaleY:1.29,x:744.8,y:270.3},0).wait(1).to({scaleX:1.26,scaleY:1.26},0).wait(1).to({scaleX:1.22,scaleY:1.22},0).wait(1).to({scaleX:1.15,scaleY:1.15,x:744.7},0).wait(1).to({scaleX:1.08,scaleY:1.08,x:744.8,y:270.4},0).wait(1).to({scaleX:1.02,scaleY:1.02},0).wait(1).to({regX:26,regY:24.5,scaleX:1,scaleY:1,x:745,y:270.5},0).wait(6));

	// Layer 24
	this.instance_4 = new lib.Symbol6();
	this.instance_4.parent = this;
	this.instance_4.setTransform(664,451,1,1,0,0,0,124,111);

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(54));

	// Layer 23
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(12,1,1).p("AjshAQgDAxAsA2QAtA2BngBQBngBBMgzQBLgyAehR");
	this.shape_1.setTransform(618.1,530.2);

	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(54));

	// Layer 19
	this.instance_5 = new lib.cup1();
	this.instance_5.parent = this;
	this.instance_5.setTransform(352,378);

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(54));

	// Layer 8
	this.instance_6 = new lib.cup();
	this.instance_6.parent = this;
	this.instance_6.setTransform(362,378);

	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(54));

	// Layer 2
	this.instance_7 = new lib.pack();
	this.instance_7.parent = this;
	this.instance_7.setTransform(712,129);

	this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(54));

	// Layer 14
	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(12,1,1).p("AJUilQmsgXk4BZQk1BYiOC2");
	this.shape_2.setTransform(685.4,357.2);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(12,1,1).p("ApJCqQCSi9EthWQEvhVGlAb");
	this.shape_3.setTransform(686.4,357);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(12,1,1).p("ApBCqQCXjFEmhSQEnhSGfAe");
	this.shape_4.setTransform(687.2,356.8);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(12,1,1).p("Ao6CqQCajKEghQQEhhQGZAh");
	this.shape_5.setTransform(687.9,356.6);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(12,1,1).p("Ao0CqQCdjPEchOQEbhOGVAj");
	this.shape_6.setTransform(688.5,356.5);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(12,1,1).p("AovCqQCfjTEYhMQEWhNGSAl");
	this.shape_7.setTransform(688.9,356.4);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(12,1,1).p("AosCqQChjWEUhLQEVhLGPAm");
	this.shape_8.setTransform(689.3,356.3);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(12,1,1).p("AoqCqQCijYEThKQEShLGOAn");
	this.shape_9.setTransform(689.4,356.3);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(12,1,1).p("AIribQmOgokSBKQkSBKijDZ");
	this.shape_10.setTransform(689.5,356.2);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(12,1,1).p("AozCqQCdjQEbhNQEbhOGUAj");
	this.shape_11.setTransform(688.5,356.5);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(12,1,1).p("Ao8CqQCZjJEihQQEihRGcAg");
	this.shape_12.setTransform(687.7,356.7);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(12,1,1).p("ApDCqQCWjDEohTQEphTGgAd");
	this.shape_13.setTransform(687,356.9);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(12,1,1).p("ApJCqQCTi+EthVQEuhVGkAb");
	this.shape_14.setTransform(686.4,357);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(12,1,1).p("ApNCrQCQi7ExhWQEyhXGoAZ");
	this.shape_15.setTransform(686,357.1);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(12,1,1).p("ApQCrQCPi4EzhYQE1hYGqAY");
	this.shape_16.setTransform(685.6,357.2);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(12,1,1).p("ApSCrQCNi2E1hZQE4hZGrAY");
	this.shape_17.setTransform(685.5,357.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2}]}).to({state:[{t:this.shape_2}]},3).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_10}]},29).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_2}]},1).wait(6));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(540,340,1080,680);
// library properties:
lib.properties = {
	id: 'D91558A3021CF348A02031A3C900B7BC',
	width: 1080,
	height: 680,
	fps: 30,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: [
		{src:"assets/images/cup1.png", id:"cup1"},
		{src:"assets/images/hand2_part.png", id:"hand2_part"},
		{src:"assets/images/pack5.png", id:"pack"}
	],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['D91558A3021CF348A02031A3C900B7BC'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}



})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;


$(function () {
    var canvas, stage3, exportRoot, anim_container, dom_overlay_container, fnStartAnimation;
    function init() {
        canvas = document.getElementById("canvas3");
        anim_container = document.getElementById("animation_container3");
        dom_overlay_container = document.getElementById("dom_overlay_container3");
        var comp=AdobeAn.getComposition("D91558A3021CF348A02031A3C900B7BC");
        var lib=comp.getLibrary();
        var loader = new createjs.LoadQueue(false);
        loader.addEventListener("fileload", function(evt){handleFileLoad(evt,comp)});
        loader.addEventListener("complete", function(evt){handleComplete(evt,comp)});
        var lib=comp.getLibrary();
        loader.loadManifest(lib.properties.manifest);
    }
    function handleFileLoad(evt, comp) {
        var images=comp.getImages();
        if (evt && (evt.item.type == "image")) { images[evt.item.id] = evt.result; }
    }
    function handleComplete(evt,comp) {
        //This function is always called, irrespective of the content. You can use the variable "stage3" after it is created in token create_stage3.
        var lib=comp.getLibrary();
        var ss=comp.getSpriteSheet();
        var queue = evt.target;
        var ssMetadata = lib.ssMetadata;
        for(i=0; i<ssMetadata.length; i++) {
            ss[ssMetadata[i].name] = new createjs.SpriteSheet( {"images": [queue.getResult(ssMetadata[i].name)], "frames": ssMetadata[i].frames} )
        }
        exportRoot = new lib.Zapas_global_big();
        window.stage3 = new lib.Stage(canvas);
        //Registers the "tick" event listener.
        fnStartAnimation = function() {
            window.stage3.addChild(exportRoot);
            createjs.Ticker.setFPS(lib.properties.fps);
            createjs.Ticker.addEventListener("tick", window.stage3);
        }
        //Code to support hidpi screens and responsive scaling.
        function makeResponsive(isResp, respDim, isScale, scaleType) {
            var lastW, lastH, lastS=1;
            window.addEventListener('resize', resizeCanvas);
            resizeCanvas();
            function resizeCanvas() {
                var w = lib.properties.width, h = lib.properties.height;
                var iw = window.innerWidth, ih=window.innerHeight;
                var pRatio = window.devicePixelRatio || 1, xRatio=iw/w, yRatio=ih/h, sRatio=1;
                if(isResp) {
                    if((respDim=='width'&&lastW==iw) || (respDim=='height'&&lastH==ih)) {
                        sRatio = lastS;
                    }
                    else if(!isScale) {
                        if(iw<w || ih<h)
                            sRatio = Math.min(xRatio, yRatio);
                    }
                    else if(scaleType==1) {
                        sRatio = Math.min(xRatio, yRatio);
                    }
                    else if(scaleType==2) {
                        sRatio = Math.max(xRatio, yRatio);
                    }
                }
                canvas.width = w*pRatio*sRatio;
                canvas.height = h*pRatio*sRatio;
                canvas.style.width = dom_overlay_container.style.width = anim_container.style.width =  w*sRatio+'px';
                canvas.style.height = anim_container.style.height = dom_overlay_container.style.height = h*sRatio+'px';
                window.stage3.scaleX = pRatio*sRatio;
                window.stage3.scaleY = pRatio*sRatio;
                lastW = iw; lastH = ih; lastS = sRatio;
                window.stage3.tickOnUpdate = false;
                window.stage3.update();
                window.stage3.tickOnUpdate = true;
            }
        }
        makeResponsive(true,'both',true,1);
        AdobeAn.compositionLoaded(lib.properties.id);
        fnStartAnimation();
    }
    init();
});