(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [];


// symbols:



(lib.hand1_part1 = function() {
	this.initialize(img.hand1_part1);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,44,167);


(lib.hand1_part2 = function() {
	this.initialize(img.hand1_part2);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,30,37);


(lib.hand2_part1 = function() {
	this.initialize(img.hand2_part1);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,71,68);


(lib.hand2_part2 = function() {
	this.initialize(img.hand2_part2);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,52,72);


(lib.pack = function() {
	this.initialize(img.pack);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,490,483);


(lib.spoon = function() {
	this.initialize(img.spoon);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,240,180);


(lib.stars = function() {
	this.initialize(img.stars);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,555,262);// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.Symbol6 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.spoon();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol6, new cjs.Rectangle(0,0,240,180), null);


(lib.Symbol5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.hand1_part1();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol5, new cjs.Rectangle(0,0,44,167), null);


(lib.Symbol4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.hand1_part2();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol4, new cjs.Rectangle(0,0,30,37), null);


(lib.Symbol2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.hand2_part2();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol2, new cjs.Rectangle(0,0,52,72), null);


(lib.Symbol1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.hand2_part1();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol1, new cjs.Rectangle(0,0,71,68), null);


(lib.Symbol3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// hand1_part1.png
	this.instance = new lib.Symbol5();
	this.instance.parent = this;
	this.instance.setTransform(21.6,7,1,1,0,0,0,12.6,7);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(24).to({regX:12.7,rotation:-9.9,x:21.7},13,cjs.Ease.get(1)).wait(45).to({regX:12.6,rotation:0,x:21.6},13,cjs.Ease.get(1)).wait(55));

	// hand1_part2.png
	this.instance_1 = new lib.Symbol4();
	this.instance_1.parent = this;
	this.instance_1.setTransform(40.4,152,1,1,0,0,0,3.4,10);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(24).to({regX:3.5,rotation:5.3,x:66.7,y:146.5},13,cjs.Ease.get(1)).wait(5).to({regX:3.6,rotation:-4.2},4,cjs.Ease.get(1)).to({regX:3.5,rotation:5.3},6,cjs.Ease.get(1)).wait(1).to({regX:3.6,rotation:-4.2},4,cjs.Ease.get(1)).to({regX:3.5,rotation:5.3},6,cjs.Ease.get(1)).wait(1).to({regX:3.6,rotation:-4.2},4,cjs.Ease.get(1)).to({regX:3.5,rotation:5.3},6,cjs.Ease.get(1)).wait(8).to({regX:3.4,rotation:0,x:40.4,y:152},13,cjs.Ease.get(1)).wait(55));

	// spoon.png
	this.instance_2 = new lib.Symbol6();
	this.instance_2.parent = this;
	this.instance_2.setTransform(52,160.6,1,1,0,0,0,52,106.5);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(24).to({rotation:32.7,x:79.4,y:153},13,cjs.Ease.get(1)).wait(5).to({rotation:24.2,y:152.4},4,cjs.Ease.get(1)).to({rotation:32.7,y:153},6,cjs.Ease.get(1)).wait(1).to({rotation:24.2,y:152.4},4,cjs.Ease.get(1)).to({rotation:32.7,y:153},6,cjs.Ease.get(1)).wait(1).to({rotation:24.2,y:152.4},4,cjs.Ease.get(1)).to({rotation:32.7,y:153},6,cjs.Ease.get(1)).wait(8).to({rotation:0,x:52,y:160.6},13,cjs.Ease.get(1)).wait(55));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,240,234.1);


// stage content:
(lib._12april_satellite = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 11
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(12,1,1).p("AjegoQAOAYAVATQBDBBBvADQBuAABLhLQAlgmAKgc");
	this.shape.setTransform(675.7,319.9);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(12,1,1).p("AhwgTQAHAMAKAJQAiAgA5ACQA3AAAmgmQATgSAFgO");
	this.shape_1.setTransform(675.7,319.9);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(12,1,1).p("AiYgbQAKARAOAMQAuAsBMADQBLgBAzgyQAagaAHgT");
	this.shape_2.setTransform(675.7,319.9);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(12,1,1).p("Ai3ghQAMAUARAQQA3A1BcACQBaAAA+g9QAegfAJgX");
	this.shape_3.setTransform(675.7,319.9);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(12,1,1).p("AjNglQANAWAUASQA9A7BnAEQBlgBBFhEQAigjAKga");
	this.shape_4.setTransform(675.7,319.9);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(12,1,1).p("AjagnQAOAXAUATQBCA/BtAEQBsgBBJhJQAlglAKgc");
	this.shape_5.setTransform(675.7,319.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[]},18).to({state:[{t:this.shape_1}]},27).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape}]},1).wait(25));

	// Layer 10
	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(12,1,1).p("AhAg2QAThBA1AQQA2ARgZBfQAnAagOAoQgPAphngL");
	this.shape_6.setTransform(676.2,319.5);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(12,1,1).p("AhVgsQA2hYA5ASQA7ARglBjQAqApgEAmQgLAxiYgQ");
	this.shape_7.setTransform(678.3,318.6);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(12,1,1).p("AhnglQBRhpA9ATQA+ASguBmQAtA0AEAlQgJA4i+gV");
	this.shape_8.setTransform(680.1,317.9);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(12,1,1).p("Ah0ggQBlh0BAATQBBATg2BnQAvA8AKAkQgHA9jagX");
	this.shape_9.setTransform(681.3,317.3);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(12,1,1).p("Ah7gdQBwh8BBAUQBDASg6BpQAwBBAOAkQgGBAjqga");
	this.shape_10.setTransform(682.1,317.1);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(12,1,1).p("Ah+gcQB0h+BCATQBDAUg7BoQAwBEAPAjQgFBBjwga");
	this.shape_11.setTransform(682.4,316.9);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(12,1,1).p("AhugiQBchvA+ATQBAASgyBnQAtA4AIAlQgIA6jNgW");
	this.shape_12.setTransform(680.8,317.6);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(12,1,1).p("AhegpQBDhgA8ATQA8ARgqBlQAsAuAAAmQgKA0irgS");
	this.shape_13.setTransform(679.2,318.2);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(12,1,1).p("AhPgvQArhRA4ARQA6ASgiBhQAqAlgHAmQgNAviJgP");
	this.shape_14.setTransform(677.7,318.9);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(12,1,1).p("AhpgkQBUhqA+ASQA+ATgvBlQAtA2AFAkQgJA5jCgV");
	this.shape_15.setTransform(680.2,317.8);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(12,1,1).p("AhUgtQA0hWA5ASQA6ARgkBiQAqAogEAnQgMAwiVgQ");
	this.shape_16.setTransform(678.1,318.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_6}]},18).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_6}]},1).to({state:[]},1).wait(30));

	// Layer 9
	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(12,1,1).p("Ah9AgQAngeAcgMQAbgOAMgCQALgCAlgDQAlgDA8AZ");
	this.shape_17.setTransform(708,273.7);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#000000").ss(12,1,1).p("Ah9AoQAcgkAbgSQAbgTAVgEQAVgFAoAFQAnAFAwAf");
	this.shape_18.setTransform(708,272.1);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#000000").ss(12,1,1).p("Ah9AxQASgpAbgXQAbgZAdgGQAcgGArAMQAqALAlAj");
	this.shape_19.setTransform(708,270.6);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#000000").ss(12,1,1).p("Ah9A4QAKgtAagcQAbgdAkgHQAjgHAtARQAsARAcAn");
	this.shape_20.setTransform(708,269.4);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#000000").ss(12,1,1).p("Ah9A9QADgvAbgfQAbggApgJQAogIAuAVQAtAVAWAr");
	this.shape_21.setTransform(708,268.4);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#000000").ss(12,1,1).p("Ah9BBQgBgyAbghQAbgjAtgJQAsgJAuAYQAuAZARAs");
	this.shape_22.setTransform(707.9,267.7);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#000000").ss(12,1,1).p("Ah9BDQgDgyAagjQAbgkAvgKQAugJAvAaQAvAaAOAu");
	this.shape_23.setTransform(707.9,267.3);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#000000").ss(12,1,1).p("Ah9BEQgEgzAagjQAbglAwgJQAvgKAvAbQAvAaANAv");
	this.shape_24.setTransform(707.9,267.1);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f().s("#000000").ss(12,1,1).p("Ah9A/QACgxAaggQAbghArgIQApgJAuAXQAuAWAUAr");
	this.shape_25.setTransform(708,268.1);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f().s("#000000").ss(12,1,1).p("Ah9A5QAIgtAbgdQAageAmgHQAlgHAsASQAsASAbAo");
	this.shape_26.setTransform(708,269.1);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f().s("#000000").ss(12,1,1).p("Ah9A0QAOgrAbgZQAbgbAggGQAggGArAOQArANAhAl");
	this.shape_27.setTransform(708,270.1);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f().s("#000000").ss(12,1,1).p("Ah9AuQAUgnAbgWQAcgXAbgGQAagFAqAKQApAKAoAh");
	this.shape_28.setTransform(708,271);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f().s("#000000").ss(12,1,1).p("Ah9ApQAbgkAbgTQAbgUAWgEQAWgFAnAGQAoAGAvAf");
	this.shape_29.setTransform(708,272);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f().s("#000000").ss(12,1,1).p("Ah9AkQAhghAbgQQAcgRAQgDQARgDAmABQAnACA1Ab");
	this.shape_30.setTransform(708,272.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_17}]}).to({state:[{t:this.shape_17}]},13).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_24}]},25).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_27}]},1).to({state:[{t:this.shape_28}]},1).to({state:[{t:this.shape_29}]},1).to({state:[{t:this.shape_30}]},1).to({state:[{t:this.shape_17}]},1).wait(23));

	// Layer 5
	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f().s("#000000").ss(12,1,1).p("AidAdQAggYApgPQAogQAngCQAlgBArAMQArAMAoAP");
	this.shape_31.setTransform(644,280.3);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f().s("#000000").ss(12,1,1).p("AidApQAYgfApgWQAogXAsgEQArgEAsASQAtASAiAc");
	this.shape_32.setTransform(644,278.4);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f().s("#000000").ss(12,1,1).p("AidAzQARgkAqgdQAogdAwgFQAvgGAsAWQAwAXAdAo");
	this.shape_33.setTransform(644,276.7);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f().s("#000000").ss(12,1,1).p("AidA7QAMgoAqgjQAoghAzgHQAzgHAtAZQAxAcAaAx");
	this.shape_34.setTransform(644.1,275.4);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f().s("#000000").ss(12,1,1).p("AieBBQAIgsAqgmQAoglA2gIQA2gIAuAbQAyAgAXA4");
	this.shape_35.setTransform(644.1,274.3);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f().s("#000000").ss(12,1,1).p("AieBFQAFgtAqgqQApgnA3gJQA3gJAvAdQA0AiAUA+");
	this.shape_36.setTransform(644.1,273.6);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f().s("#000000").ss(12,1,1).p("AieBIQADgvAqgrQAogqA5gJQA5gJAvAeQA0AjATBB");
	this.shape_37.setTransform(644.1,273.1);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f().s("#000000").ss(12,1,1).p("AieBJQADgwAqgrQAogqA5gKQA5gJAvAfQA0AjATBC");
	this.shape_38.setTransform(644.1,273);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f().s("#000000").ss(12,1,1).p("AieBDQAHgtAqgnQAogmA3gJQA2gIAuAcQAzAgAWA7");
	this.shape_39.setTransform(644.1,274);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f().s("#000000").ss(12,1,1).p("AieA8QALgpAqgjQAogjA0gHQA0gHAtAaQAyAcAZA0");
	this.shape_40.setTransform(644.1,275.1);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f().s("#000000").ss(12,1,1).p("AidA2QAPgmAqgfQAogfAwgGQAygGAtAXQAwAZAcAt");
	this.shape_41.setTransform(644.1,276.1);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f().s("#000000").ss(12,1,1).p("AidAwQATgiApgcQApgbAugFQAugFAsAUQAwAXAeAk");
	this.shape_42.setTransform(644,277.2);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f().s("#000000").ss(12,1,1).p("AidAqQAXgfApgYQApgXAsgEQArgEAsASQAuATAhAd");
	this.shape_43.setTransform(644,278.2);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f().s("#000000").ss(12,1,1).p("AidAjQAcgbAogTQApgUApgDQAogCArAOQAtAQAlAW");
	this.shape_44.setTransform(644,279.3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_31}]}).to({state:[{t:this.shape_31}]},13).to({state:[{t:this.shape_32}]},1).to({state:[{t:this.shape_33}]},1).to({state:[{t:this.shape_34}]},1).to({state:[{t:this.shape_35}]},1).to({state:[{t:this.shape_36}]},1).to({state:[{t:this.shape_37}]},1).to({state:[{t:this.shape_38}]},1).to({state:[{t:this.shape_38}]},25).to({state:[{t:this.shape_39}]},1).to({state:[{t:this.shape_40}]},1).to({state:[{t:this.shape_41}]},1).to({state:[{t:this.shape_42}]},1).to({state:[{t:this.shape_43}]},1).to({state:[{t:this.shape_44}]},1).to({state:[{t:this.shape_31}]},1).wait(23));

	// Layer 8
	this.instance = new lib.Symbol3();
	this.instance.parent = this;
	this.instance.setTransform(649,437,1,1,0,0,0,120,117);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(75));

	// Layer 2
	this.instance_1 = new lib.pack();
	this.instance_1.parent = this;
	this.instance_1.setTransform(442,155);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(75));

	// Layer 4
	this.instance_2 = new lib.Symbol2();
	this.instance_2.parent = this;
	this.instance_2.setTransform(789.9,335.3,1,1,-15,0,0,18.6,68.8);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(9).to({rotation:0,x:775.6,y:313.8},9,cjs.Ease.get(1)).wait(33).to({rotation:-15,x:789.9,y:335.3},9,cjs.Ease.get(1)).wait(15));

	// Layer 3
	this.instance_3 = new lib.Symbol1();
	this.instance_3.parent = this;
	this.instance_3.setTransform(689.7,367.1,1,1,15,0,0,-23.6,63.4);

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(9).to({rotation:0,x:691.4,y:365.4},9,cjs.Ease.get(1)).wait(33).to({rotation:15,x:689.7,y:367.1},9,cjs.Ease.get(1)).wait(15));

	// Layer 6
	this.instance_4 = new lib.stars();
	this.instance_4.parent = this;
	this.instance_4.setTransform(425,97);

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(75));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(540,340,1080,680);
// library properties:
lib.properties = {
	id: 'ED21AD3C28A4634CB57FAAE5F53EC281',
	width: 1080,
	height: 680,
	fps: 30,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: [
		{src:"assets/images/hand1_part1.png", id:"hand1_part1"},
		{src:"assets/images/hand1_part2.png", id:"hand1_part2"},
		{src:"assets/images/hand2_part1.png", id:"hand2_part1"},
		{src:"assets/images/hand2_part2.png", id:"hand2_part2"},
		{src:"assets/images/pack6.png", id:"pack"},
		{src:"assets/images/spoon.png", id:"spoon"},
		{src:"assets/images/stars.png", id:"stars"}
	],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['ED21AD3C28A4634CB57FAAE5F53EC281'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}



})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;

$(function () {
    var canvas, stage, exportRoot, anim_container, dom_overlay_container, fnStartAnimation;
    function init() {
        canvas = document.getElementById("canvas4");
        anim_container = document.getElementById("animation_container4");
        dom_overlay_container = document.getElementById("dom_overlay_container4");
        var comp=AdobeAn.getComposition("ED21AD3C28A4634CB57FAAE5F53EC281");
        var lib=comp.getLibrary();
        var loader = new createjs.LoadQueue(false);
        loader.addEventListener("fileload", function(evt){handleFileLoad(evt,comp)});
        loader.addEventListener("complete", function(evt){handleComplete(evt,comp)});
        var lib=comp.getLibrary();
        loader.loadManifest(lib.properties.manifest);
    }
    function handleFileLoad(evt, comp) {
        var images=comp.getImages();
        if (evt && (evt.item.type == "image")) { images[evt.item.id] = evt.result; }
    }
    function handleComplete(evt,comp) {
        //This function is always called, irrespective of the content. You can use the variable "stage" after it is created in token create_stage.
        var lib=comp.getLibrary();
        var ss=comp.getSpriteSheet();
        var queue = evt.target;
        var ssMetadata = lib.ssMetadata;
        for(i=0; i<ssMetadata.length; i++) {
            ss[ssMetadata[i].name] = new createjs.SpriteSheet( {"images": [queue.getResult(ssMetadata[i].name)], "frames": ssMetadata[i].frames} )
        }
        exportRoot = new lib._12april_satellite();
        stage = new lib.Stage(canvas);
        //Registers the "tick" event listener.
        fnStartAnimation = function() {
            stage.addChild(exportRoot);
            createjs.Ticker.setFPS(lib.properties.fps);
            createjs.Ticker.addEventListener("tick", stage);
        }
        //Code to support hidpi screens and responsive scaling.
        function makeResponsive(isResp, respDim, isScale, scaleType) {
            var lastW, lastH, lastS=1;
            window.addEventListener('resize', resizeCanvas);
            resizeCanvas();
            function resizeCanvas() {
                var w = lib.properties.width, h = lib.properties.height;
                var iw = window.innerWidth, ih=window.innerHeight;
                var pRatio = window.devicePixelRatio || 1, xRatio=iw/w, yRatio=ih/h, sRatio=1;
                if(isResp) {
                    if((respDim=='width'&&lastW==iw) || (respDim=='height'&&lastH==ih)) {
                        sRatio = lastS;
                    }
                    else if(!isScale) {
                        if(iw<w || ih<h)
                            sRatio = Math.min(xRatio, yRatio);
                    }
                    else if(scaleType==1) {
                        sRatio = Math.min(xRatio, yRatio);
                    }
                    else if(scaleType==2) {
                        sRatio = Math.max(xRatio, yRatio);
                    }
                }
                canvas.width = w*pRatio*sRatio;
                canvas.height = h*pRatio*sRatio;
                canvas.style.width = dom_overlay_container.style.width = anim_container.style.width =  w*sRatio+'px';
                canvas.style.height = anim_container.style.height = dom_overlay_container.style.height = h*sRatio+'px';
                stage.scaleX = pRatio*sRatio;
                stage.scaleY = pRatio*sRatio;
                lastW = iw; lastH = ih; lastS = sRatio;
                stage.tickOnUpdate = false;
                stage.update();
                stage.tickOnUpdate = true;
            }
        }
        makeResponsive(true,'both',true,1);
        AdobeAn.compositionLoaded(lib.properties.id);
        fnStartAnimation();
    }
    init();
});