(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [];


// symbols:



(lib.bg = function() {
	this.initialize(img.bg);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,1086,686);


(lib.pack = function() {
	this.initialize(img.pack);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,315,372);// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.Symbol3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.pack();
	this.instance.parent = this;
	this.instance.setTransform(-157.5,-186);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol3, new cjs.Rectangle(-157.5,-186,315,372), null);


(lib.Symbol_5_Layer_3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_3
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(8.5,1,1).p("AhggkQBfCHBihw");
	this.shape.setTransform(9.025,4.1728);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(8.5,1,1).p("AhggiQBgB+Biho");
	this.shape_1.setTransform(9.15,4.2686);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(8.5,1,1).p("AhigfQBiB1Bjhh");
	this.shape_2.setTransform(9.25,4.343);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(8.5,1,1).p("AhjgdQBjBtBkha");
	this.shape_3.setTransform(9.35,4.4298);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(8.5,1,1).p("AhjgbQBkBlBjhT");
	this.shape_4.setTransform(9.475,4.5092);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(8.5,1,1).p("AhkgZQBmBeBjhN");
	this.shape_5.setTransform(9.55,4.5708);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(8.5,1,1).p("AhlgXQBoBYBjhI");
	this.shape_6.setTransform(9.65,4.6448);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(8.5,1,1).p("AhlgWQBoBSBjhD");
	this.shape_7.setTransform(9.7,4.7011);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(8.5,1,1).p("AhmgVQBqBOBjg/");
	this.shape_8.setTransform(9.775,4.7322);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(8.5,1,1).p("AhmgUQBqBKBjg7");
	this.shape_9.setTransform(9.825,4.7756);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(8.5,1,1).p("AhmgTQBqBGBjg4");
	this.shape_10.setTransform(9.875,4.8115);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(8.5,1,1).p("AhngSQBsBEBjg2");
	this.shape_11.setTransform(9.925,4.8497);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(8.5,1,1).p("AhngRQBsBBBjg0");
	this.shape_12.setTransform(9.95,4.8803);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(8.5,1,1).p("AhngRQBsBABjgz");
	this.shape_13.setTransform(9.95,4.8855);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(8.5,1,1).p("AhngQQBsA+Bjgy");
	this.shape_14.setTransform(9.975,4.8985);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(8.5,1,1).p("AhngRQBsA/Bjgy");
	this.shape_15.setTransform(9.975,4.9019);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(8.5,1,1).p("AhngTQBsBHBjg5");
	this.shape_16.setTransform(9.85,4.8062);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(8.5,1,1).p("AhmgVQBpBQBkhB");
	this.shape_17.setTransform(9.75,4.7316);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#000000").ss(8.5,1,1).p("AhkgaQBmBgBjhO");
	this.shape_18.setTransform(9.525,4.5654);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#000000").ss(8.5,1,1).p("AhjgcQBkBnBjhU");
	this.shape_19.setTransform(9.45,4.5038);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#000000").ss(8.5,1,1).p("AhigfQBiBzBjhf");
	this.shape_20.setTransform(9.3,4.3736);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#000000").ss(8.5,1,1).p("AhhggQBhB4Bihj");
	this.shape_21.setTransform(9.225,4.3426);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#000000").ss(8.5,1,1).p("AhhghQBhB8Bihn");
	this.shape_22.setTransform(9.175,4.2992);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#000000").ss(8.5,1,1).p("AhhgiQBhB/Bihp");
	this.shape_23.setTransform(9.125,4.2632);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#000000").ss(8.5,1,1).p("AhggjQBfCCBihs");
	this.shape_24.setTransform(9.075,4.2252);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f().s("#000000").ss(8.5,1,1).p("AhggjQBfCEBihu");
	this.shape_25.setTransform(9.05,4.1946);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f().s("#000000").ss(8.5,1,1).p("AhggkQBfCGBihv");
	this.shape_26.setTransform(9.05,4.1893);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape,p:{y:4.1728}}]}).to({state:[{t:this.shape,p:{y:4.1728}}]},9).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_15}]},30).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape,p:{y:4.1765}}]},1).to({state:[{t:this.shape,p:{y:4.1728}}]},1).wait(6));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_5_Layer_2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(8.5,1,1).p("Ahsg1QBHCcCShJ");
	this.shape.setTransform(8.375,8.1037);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(8.5,1,1).p("Ahtg1QBHCcCUhG");
	this.shape_1.setTransform(8.45,8.1793);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(8.5,1,1).p("Ahug2QBHCcCWhD");
	this.shape_2.setTransform(8.55,8.2398);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(8.5,1,1).p("Ahug2QBGCcCXhC");
	this.shape_3.setTransform(8.625,8.2891);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(8.5,1,1).p("Ahvg3QBHCcCYg/");
	this.shape_4.setTransform(8.675,8.352);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(8.5,1,1).p("Ahwg4QBHCcCag9");
	this.shape_5.setTransform(8.75,8.4034);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(8.5,1,1).p("Ahwg4QBGCcCbg7");
	this.shape_6.setTransform(8.8,8.4558);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(8.5,1,1).p("Ahxg4QBHCcCcg6");
	this.shape_7.setTransform(8.85,8.4957);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(8.5,1,1).p("Ahxg5QBGCcCdg4");
	this.shape_8.setTransform(8.9,8.5362);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(8.5,1,1).p("Ahyg5QBHCcCeg3");
	this.shape_9.setTransform(8.95,8.5634);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(8.5,1,1).p("Ahyg5QBHCcCeg2");
	this.shape_10.setTransform(8.975,8.591);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(8.5,1,1).p("Ahyg6QBGCcCfg1");
	this.shape_11.setTransform(9,8.6188);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(8.5,1,1).p("Ahyg6QBGCcCfg0");
	this.shape_12.setTransform(9.025,8.6328);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(8.5,1,1).p("Ahzg6QBHCcCgg0");
	this.shape_13.setTransform(9.05,8.6469);
	this.shape_13._off = true;

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(8.5,1,1).p("Ahxg5QBHCcCcg5");
	this.shape_14.setTransform(8.875,8.5091);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(8.5,1,1).p("Ahwg3QBHCcCag+");
	this.shape_15.setTransform(8.75,8.3905);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(8.5,1,1).p("Ahtg2QBGCcCVhE");
	this.shape_16.setTransform(8.525,8.2154);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(8.5,1,1).p("Ahtg1QBHCcCUhH");
	this.shape_17.setTransform(8.45,8.1674);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#000000").ss(8.5,1,1).p("Ahsg1QBGCcCThH");
	this.shape_18.setTransform(8.425,8.1437);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#000000").ss(8.5,1,1).p("Ahsg1QBGCcCThI");
	this.shape_19.setTransform(8.4,8.1319);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#000000").ss(8.5,1,1).p("Ahsg1QBHCcCShI");
	this.shape_20.setTransform(8.375,8.1202);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape,p:{y:8.1037}}]}).to({state:[{t:this.shape,p:{y:8.1037}}]},9).to({state:[{t:this.shape_1,p:{x:8.45,y:8.1793}}]},1).to({state:[{t:this.shape_2,p:{x:8.55,y:8.2398}}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4,p:{y:8.352}}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9,p:{x:8.95,y:8.5634}}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_13}]},30).to({state:[{t:this.shape_9,p:{x:8.975,y:8.5772}}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_4,p:{y:8.3393}}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_2,p:{x:8.575,y:8.252}}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_1,p:{x:8.475,y:8.1913}}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape,p:{y:8.1085}}]},1).to({state:[{t:this.shape,p:{y:8.1037}}]},1).wait(6));
	this.timeline.addTween(cjs.Tween.get(this.shape_13).wait(22).to({_off:false},0).wait(1).to({y:8.661},0).wait(1).to({y:8.657},0).wait(30).to({_off:true},1).wait(20));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_5_Layer_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(8.5,1,1).p("AgbAUIA3gn");
	this.shape.setTransform(-0.025,-0.025);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(8.5,1,1).p("AgbAWIA3gr");
	this.shape_1.setTransform(-0.05,-0.225);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(8.5,1,1).p("AgbAYIA3gv");
	this.shape_2.setTransform(-0.05,-0.425);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(8.5,1,1).p("AgaAaIA1gz");
	this.shape_3.setTransform(-0.075,-0.6);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(8.5,1,1).p("AgaAbIA1g1");
	this.shape_4.setTransform(-0.075,-0.775);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(8.5,1,1).p("AgaAdIA1g5");
	this.shape_5.setTransform(-0.1,-0.925);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(8.5,1,1).p("AgaAeIA1g7");
	this.shape_6.setTransform(-0.1,-1.05);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(8.5,1,1).p("AgaAfIA1g9");
	this.shape_7.setTransform(-0.125,-1.175);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(8.5,1,1).p("AgaAgIA1g/");
	this.shape_8.setTransform(-0.125,-1.275);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(8.5,1,1).p("AgaAhIA1hB");
	this.shape_9.setTransform(-0.125,-1.375);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(8.5,1,1).p("AgaAiIA1hD");
	this.shape_10.setTransform(-0.125,-1.45);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(8.5,1,1).p("AgaAiIA1hE");
	this.shape_11.setTransform(-0.15,-1.5);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(8.5,1,1).p("AgaAjIA1hF");
	this.shape_12.setTransform(-0.15,-1.55);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(8.5,1,1).p("AgaAkIA1hH");
	this.shape_13.setTransform(-0.15,-1.6);
	this.shape_13._off = true;

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(8.5,1,1).p("AgaAcIA1g3");
	this.shape_14.setTransform(-0.1,-0.875);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(8.5,1,1).p("AgbAXIA3gt");
	this.shape_15.setTransform(-0.05,-0.375);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(8.5,1,1).p("AgbAVIA3gq");
	this.shape_16.setTransform(-0.05,-0.2);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(8.5,1,1).p("AgbAVIA3gp");
	this.shape_17.setTransform(-0.025,-0.15);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},9).to({state:[{t:this.shape_1,p:{y:-0.225}}]},1).to({state:[{t:this.shape_2,p:{y:-0.425}}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4,p:{y:-0.775}}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8,p:{y:-1.275}}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10,p:{y:-1.45}}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_13}]},30).to({state:[{t:this.shape_10,p:{y:-1.425}}]},1).to({state:[{t:this.shape_8,p:{y:-1.225}}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_4,p:{y:-0.725}}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_2,p:{y:-0.475}}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_1,p:{y:-0.275}}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17,p:{y:-0.15}}]},1).to({state:[{t:this.shape_17,p:{y:-0.1}}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).wait(6));
	this.timeline.addTween(cjs.Tween.get(this.shape).wait(9).to({_off:true},1).wait(57).to({_off:false,y:-0.05},0).wait(1).to({y:-0.025},0).wait(7));
	this.timeline.addTween(cjs.Tween.get(this.shape_13).wait(22).to({_off:false},0).wait(1).to({y:-1.625},0).wait(31).to({_off:true},1).wait(20));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_4_Layer_2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(10,1,1).p("AA2BTQAqi8ieAa");
	this.shape.setTransform(-5.4634,-2.5809);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(10,1,1).p("Ag+hGQCegngqC7");
	this.shape_1.setTransform(-5.4873,-2.1093);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(10,1,1).p("Ag+g+QCeg0gqC8");
	this.shape_2.setTransform(-5.4873,-1.6872);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(10,1,1).p("AA3BGQAqi7ifBB");
	this.shape_3.setTransform(-5.5156,-1.3054);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(10,1,1).p("Ag8hCQCbgugqC8");
	this.shape_4.setTransform(-5.3045,-1.8762);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(10,1,1).p("Ag6hOQCWgbgqC7");
	this.shape_5.setTransform(-5.0692,-2.5251);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(10,1,1).p("AAwBaQAqi7iSAI");
	this.shape_6.setTransform(-4.8619,-3.3177);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(10,1,1).p("Ag4hRQCQgTgoC3");
	this.shape_7.setTransform(-4.8676,-2.6389);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(10,1,1).p("Ag3hIQCOgegmCz");
	this.shape_8.setTransform(-4.9011,-2.0143);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(10,1,1).p("Ag3g/QCMgogkCu");
	this.shape_9.setTransform(-4.9079,-1.4462);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(10,1,1).p("AAxBCQAiiqiKAz");
	this.shape_10.setTransform(-4.9366,-0.9095);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(10,1,1).p("Ag5g/QCRgqglCx");
	this.shape_11.setTransform(-5.126,-1.4768);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(10,1,1).p("Ag7hGQCWgjgnC2");
	this.shape_12.setTransform(-5.2758,-1.9566);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(10,1,1).p("Ag9hLQCbgegpC5");
	this.shape_13.setTransform(-5.3816,-2.3083);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(10,1,1).p("Ag+hOQCdgbgpC7");
	this.shape_14.setTransform(-5.4434,-2.5195);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},9).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape}]},1).wait(51));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_4_Layer_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(10,1,1).p("AAzBQQASgngIhJQgHhKh0As");
	this.shape.setTransform(-0.3147,0.0331);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(10,1,1).p("Ag/hOQB1gXAIBKQAHBJgSAn");
	this.shape_1.setTransform(-0.3897,-0.4713);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(10,1,1).p("AhAhbQB3gCAIBKQAHBJgSAn");
	this.shape_2.setTransform(-0.4897,-1.2526);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(10,1,1).p("AA1BnQASgmgHhJQgIhLh5gT");
	this.shape_3.setTransform(-0.5647,-2.3);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(10,1,1).p("AhAhaQB3gFAHBLQAIBJgSAm");
	this.shape_4.setTransform(-0.4397,-1.1378);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(10,1,1).p("Ag+hKQB0gdAHBLQAIBJgSAm");
	this.shape_5.setTransform(-0.3147,-0.3193);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(10,1,1).p("AAxBOQASgmgHhJQgIhLhxA0");
	this.shape_6.setTransform(-0.1897,0.1812);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(10,1,1).p("Ag7hEQBygoAEBNQAEBLgSAn");
	this.shape_7.setTransform(0.2856,-0.2725);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(10,1,1).p("Ag5hRQBygaABBPQAABNgTAn");
	this.shape_8.setTransform(0.8006,-0.7984);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(10,1,1).p("Ag3hbQBygOgDBRQgCBPgUAn");
	this.shape_9.setTransform(1.3288,-1.4219);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(10,1,1).p("AAdBmQAUgoAGhRQAFhThyAB");
	this.shape_10.setTransform(1.8892,-2.2003);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(10,1,1).p("Ag5hZQBzgQgBBQQgBBOgTAn");
	this.shape_11.setTransform(1.0504,-1.234);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(10,1,1).p("Ag7hOQB0gcACBNQADBMgTAn");
	this.shape_12.setTransform(0.4149,-0.6331);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(10,1,1).p("Ag9hFQB0glAFBMQAGBKgTAn");
	this.shape_13.setTransform(0.013,-0.2558);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(10,1,1).p("Ag+hAQB0gqAHBKQAHBKgSAm");
	this.shape_14.setTransform(-0.252,-0.0321);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},9).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape}]},1).wait(51));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_2_Layer_2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(12,1,1).p("AAGhDQgnhKgwgeQgygegmAfQgsBGA5ByQA6B2BzA3QBmhJArhbQAihIgJg8QgEgbgOgOQgPgPgagFQhEgMg2Bzg");
	this.shape.setTransform(25.8032,24.4498);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(10.9,1,1).p("AipiqQAmgfAyAeQAwAeAnBKQA2hzBEAMQAaAFAPAPQAOAOAEAbQAJA8giBIQgrBbhmBJQhzg3g6h2Qg5hyAshGg");
	this.shape_1.setTransform(25.8032,24.4498);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(9.9,1,1).p("AipiqQAmgfAyAeQAwAeAnBKQA2hzBEAMQAaAFAPAPQAOAOAEAbQAJA8giBIQgrBbhmBJQhzg3g6h2Qg5hyAshGg");
	this.shape_2.setTransform(25.8032,24.4498);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(9.1,1,1).p("AipiqQAmgfAyAeQAwAeAnBKQA2hzBEAMQAaAFAPAPQAOAOAEAbQAJA8giBIQgrBbhmBJQhzg3g6h2Qg5hyAshGg");
	this.shape_3.setTransform(25.8032,24.4498);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(8.5,1,1).p("AipiqQAmgfAyAeQAwAeAnBKQA2hzBEAMQAaAFAPAPQAOAOAEAbQAJA8giBIQgrBbhmBJQhzg3g6h2Qg5hyAshGg");
	this.shape_4.setTransform(25.8032,24.4498);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(8,1,1).p("AAGhDQgnhKgwgeQgygegmAfQgsBGA5ByQA6B2BzA3QBmhJArhbQAihIgJg8QgEgbgOgOQgPgPgagFQhEgMg2Bzg");
	this.shape_5.setTransform(25.8032,24.4498);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(8.8,1,1).p("AipiqQAmgfAyAeQAwAeAnBKQA2hzBEAMQAaAFAPAPQAOAOAEAbQAJA8giBIQgrBbhmBJQhzg3g6h2Qg5hyAshGg");
	this.shape_6.setTransform(25.8032,24.4498);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(9.6,1,1).p("AipiqQAmgfAyAeQAwAeAnBKQA2hzBEAMQAaAFAPAPQAOAOAEAbQAJA8giBIQgrBbhmBJQhzg3g6h2Qg5hyAshGg");
	this.shape_7.setTransform(25.8032,24.4498);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(10.2,1,1).p("AipiqQAmgfAyAeQAwAeAnBKQA2hzBEAMQAaAFAPAPQAOAOAEAbQAJA8giBIQgrBbhmBJQhzg3g6h2Qg5hyAshGg");
	this.shape_8.setTransform(25.8032,24.4498);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(10.8,1,1).p("AipiqQAmgfAyAeQAwAeAnBKQA2hzBEAMQAaAFAPAPQAOAOAEAbQAJA8giBIQgrBbhmBJQhzg3g6h2Qg5hyAshGg");
	this.shape_9.setTransform(25.8032,24.4498);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(11.3,1,1).p("AipiqQAmgfAyAeQAwAeAnBKQA2hzBEAMQAaAFAPAPQAOAOAEAbQAJA8giBIQgrBbhmBJQhzg3g6h2Qg5hyAshGg");
	this.shape_10.setTransform(25.8032,24.4498);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(11.7,1,1).p("AipiqQAmgfAyAeQAwAeAnBKQA2hzBEAMQAaAFAPAPQAOAOAEAbQAJA8giBIQgrBbhmBJQhzg3g6h2Qg5hyAshGg");
	this.shape_11.setTransform(25.8032,24.4498);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(12,1,1).p("AipiqQgsBGA5ByQA6B2BzA3QBmhJArhbQAihIgJg8QgEgbgOgOQgPgPgagFQhEgMg2BzQgnhKgwgeQgygegmAfg");
	this.shape_12.setTransform(25.8032,24.4498);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},24).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},6).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).wait(21));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_1_Layer_6 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_6
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(8.5,1,1).p("ABnA7IjNh1");
	this.shape.setTransform(-88.15,-41.425);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(8.5,1,1).p("Ahmg6IDNB1");
	this.shape_1.setTransform(-88.175,-41.5);
	this.shape_1._off = true;

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(8.5,1,1).p("Ahmg5IDNBz");
	this.shape_2.setTransform(-88.25,-41.675);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(8.5,1,1).p("Ahng5IDPBz");
	this.shape_3.setTransform(-88.35,-41.9);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(8.5,1,1).p("Ahng4IDPBx");
	this.shape_4.setTransform(-88.425,-42.125);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(8.5,1,1).p("Ahog3IDRBv");
	this.shape_5.setTransform(-88.5,-42.35);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(8.5,1,1).p("Ahog2IDRBt");
	this.shape_6.setTransform(-88.575,-42.575);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(8.5,1,1).p("Ahog2IDSBt");
	this.shape_7.setTransform(-88.65,-42.725);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(8.5,1,1).p("Ahpg1IDTBr");
	this.shape_8.setTransform(-88.7,-42.875);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(8.5,1,1).p("Ahpg0IDUBp");
	this.shape_9.setTransform(-88.8,-43.125);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(8.5,1,1).p("Ahqg0IDVBp");
	this.shape_10.setTransform(-88.825,-43.2);
	this.shape_10._off = true;

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(8.5,1,1).p("ABrA0IjVhn");
	this.shape_11.setTransform(-88.9,-43.375);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(8.5,1,1).p("Ahpg2IDTBt");
	this.shape_12.setTransform(-88.625,-42.675);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(8.5,1,1).p("Ahng6IDOB1");
	this.shape_13.setTransform(-88.2,-41.575);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},9).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3,p:{x:-88.35,y:-41.9}}]},1).to({state:[{t:this.shape_4,p:{x:-88.425,y:-42.125}}]},1).to({state:[{t:this.shape_5,p:{x:-88.5,y:-42.35}}]},1).to({state:[{t:this.shape_6,p:{x:-88.575,y:-42.575}}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8,p:{x:-88.7,y:-42.875}}]},1).to({state:[{t:this.shape_8,p:{x:-88.775,y:-43}}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_11}]},30).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_8,p:{x:-88.7,y:-42.9}}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_6,p:{x:-88.55,y:-42.475}}]},1).to({state:[{t:this.shape_5,p:{x:-88.475,y:-42.3}}]},1).to({state:[{t:this.shape_4,p:{x:-88.425,y:-42.125}}]},1).to({state:[{t:this.shape_4,p:{x:-88.375,y:-41.975}}]},1).to({state:[{t:this.shape_3,p:{x:-88.325,y:-41.85}}]},1).to({state:[{t:this.shape_3,p:{x:-88.275,y:-41.725}}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape}]},1).wait(6));
	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(10).to({_off:false},0).to({_off:true},1).wait(53).to({_off:false,x:-88.25,y:-41.65},0).to({_off:true},1).wait(1).to({_off:false,x:-88.175,y:-41.5},0).wait(1).to({x:-88.15,y:-41.45},0).wait(1).to({y:-41.425},0).to({_off:true},1).wait(6));
	this.timeline.addTween(cjs.Tween.get(this.shape_10).wait(20).to({_off:false},0).wait(1).to({x:-88.875,y:-43.275},0).wait(1).to({y:-43.3},0).wait(1).to({x:-88.9,y:-43.35},0).to({_off:true},1).wait(51));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_1_Layer_4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_4
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(10,1,1).p("AFgnGQA5D7gXDFQgXC+hkB2QhkB2ikAbQirAcjbhL");
	this.shape.setTransform(16.1806,-6.5444);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(10,1,1).p("AmFGUQDcBJCpgdQCkgcBkh3QBjh2AVi/QAWjEg7j7");
	this.shape_1.setTransform(16.1139,-6.4127);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(10,1,1).p("AmAGbQDdBGCpggQCjgfBih4QBgh4ASi/QATjEg/j5");
	this.shape_2.setTransform(15.985,-6.0456);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(10,1,1).p("Al6GmQDeBACogjQCigjBfh6QBeh5AOi/QAOjFhEj3");
	this.shape_3.setTransform(15.8445,-5.6199);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(10,1,1).p("Al1GvQDgA8CngnQChgmBch8QBbh7AKi/QAKjFhJj2");
	this.shape_4.setTransform(15.7047,-5.141);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(10,1,1).p("AlvG5QDgA3CmgrQChgpBZh+QBZh9AGi/QAFjFhOj0");
	this.shape_5.setTransform(15.6181,-4.6987);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(10,1,1).p("AlrHBQDhAzCmguQCggsBXh/QBWh/ADi/QABjFhSjz");
	this.shape_6.setTransform(15.5763,-4.2593);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(10,1,1).p("AlnHIQDiAvClgwQCfgvBViBQBUiAAAi/QgCjFhWjy");
	this.shape_7.setTransform(15.5001,-3.8874);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(10,1,1).p("AlkHOQDjAsCkgzQCfgwBTiDQBTiBgDi/QgFjGhajw");
	this.shape_8.setTransform(15.4803,-3.5311);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(10,1,1).p("AlhHUQDjApCkg1QCegzBSiDQBRiCgFjAQgHjFhdjv");
	this.shape_9.setTransform(15.4645,-3.2511);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(10,1,1).p("AlfHYQDlAnCjg2QCdg1BRiEQBQiDgIjAQgIjFhfjv");
	this.shape_10.setTransform(15.4321,-3.0073);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(10,1,1).p("AldHbQDlAlCig4QCeg1BPiFQBPiEgJjAQgKjFhhju");
	this.shape_11.setTransform(15.446,-2.773);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(10,1,1).p("AlcHdQDlAkCjg4QCdg3BPiGQBOiEgKjAQgMjFhjjt");
	this.shape_12.setTransform(15.4318,-2.6245);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(10,1,1).p("AlaHfQDlAjCig5QCdg4BOiGQBNiFgLi/QgMjGhkjt");
	this.shape_13.setTransform(15.4434,-2.4878);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(10,1,1).p("AlaHhQDmAiCig6QCcg5BOiGQBNiFgMjAQgNjFhljt");
	this.shape_14.setTransform(15.4251,-2.3967);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(10,1,1).p("ADmntQBlDsAODGQAMC/hNCGQhNCGidA5QiiA6jlgh");
	this.shape_15.setTransform(15.4314,-2.3401);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(10,1,1).p("AlfHYQDkAnCkg2QCdg1BRiFQBPiDgHi/QgJjGhgju");
	this.shape_16.setTransform(15.4535,-2.9616);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(10,1,1).p("AljHPQDjAsCkgzQCegxBTiDQBTiBgEjAQgEjFhbjw");
	this.shape_17.setTransform(15.4821,-3.5061);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#000000").ss(10,1,1).p("AloHGQDiAwClgvQCfguBWiBQBViAAAi/QAAjFhWjy");
	this.shape_18.setTransform(15.525,-3.9999);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#000000").ss(10,1,1).p("AltG9QDhA1CmgsQCggrBYh/QBXh+AFi/QADjFhRjz");
	this.shape_19.setTransform(15.5822,-4.4345);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#000000").ss(10,1,1).p("AlxG2QDgA4CmgpQChgoBah+QBah8AHi/QAHjFhNj1");
	this.shape_20.setTransform(15.6505,-4.8463);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#000000").ss(10,1,1).p("Al1GvQDfA8CngnQCigmBch8QBbh7ALi/QAKjFhJj1");
	this.shape_21.setTransform(15.7352,-5.1818);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#000000").ss(10,1,1).p("Al5GpQDfA/CnglQCigjBeh7QBdh6ANi/QANjFhGj3");
	this.shape_22.setTransform(15.8018,-5.4805);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#000000").ss(10,1,1).p("Al8GjQDeBCCogjQCjghBfh6QBfh5APi/QAPjEhCj4");
	this.shape_23.setTransform(15.888,-5.7328);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#000000").ss(10,1,1).p("Al/GeQDeBECoghQCjggBhh4QBgh4ARi/QASjFhAj4");
	this.shape_24.setTransform(15.9379,-5.9495);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f().s("#000000").ss(10,1,1).p("AmBGaQDcBGCpgfQCkgfBih4QBhh3ATi+QATjFg+j5");
	this.shape_25.setTransform(16.0228,-6.1243);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f().s("#000000").ss(10,1,1).p("AmEGWQDdBICpgeQCkgdBjh3QBih3AUi/QAVjEg8j6");
	this.shape_26.setTransform(16.061,-6.2962);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f().s("#000000").ss(10,1,1).p("AmFGUQDcBJCpgdQClgcBjh3QBjh2AVi/QAWjEg7j7");
	this.shape_27.setTransform(16.1139,-6.4025);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f().s("#000000").ss(10,1,1).p("AmGGRQDcBLCqgdQCkgbBkh3QBjh1AWi/QAXjEg6j7");
	this.shape_28.setTransform(16.1394,-6.4807);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f().s("#000000").ss(10,1,1).p("AmHGQQDbBLCrgcQCkgbBkh2QBkh2AXi+QAWjFg5j7");
	this.shape_29.setTransform(16.1684,-6.5326);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},9).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_15}]},30).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_27}]},1).to({state:[{t:this.shape_28}]},1).to({state:[{t:this.shape_29}]},1).to({state:[{t:this.shape}]},1).wait(6));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_1_Layer_2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(12,1,1).p("AAGhDQgnhKgwgeQgygegmAfQgsBGA5ByQA6B2BzA3QBmhJArhbQAihIgJg8QgEgbgOgOQgPgPgagFQhEgMg2Bzg");
	this.shape.setTransform(25.8032,24.4498);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(10.9,1,1).p("AipiqQAmgfAyAeQAwAeAnBKQA2hzBEAMQAaAFAPAPQAOAOAEAbQAJA8giBIQgrBbhmBJQhzg3g6h2Qg5hyAshGg");
	this.shape_1.setTransform(25.8032,24.4498);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(9.9,1,1).p("AipiqQAmgfAyAeQAwAeAnBKQA2hzBEAMQAaAFAPAPQAOAOAEAbQAJA8giBIQgrBbhmBJQhzg3g6h2Qg5hyAshGg");
	this.shape_2.setTransform(25.8032,24.4498);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(9.1,1,1).p("AipiqQAmgfAyAeQAwAeAnBKQA2hzBEAMQAaAFAPAPQAOAOAEAbQAJA8giBIQgrBbhmBJQhzg3g6h2Qg5hyAshGg");
	this.shape_3.setTransform(25.8032,24.4498);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(8.5,1,1).p("AipiqQAmgfAyAeQAwAeAnBKQA2hzBEAMQAaAFAPAPQAOAOAEAbQAJA8giBIQgrBbhmBJQhzg3g6h2Qg5hyAshGg");
	this.shape_4.setTransform(25.8032,24.4498);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(8,1,1).p("AAGhDQgnhKgwgeQgygegmAfQgsBGA5ByQA6B2BzA3QBmhJArhbQAihIgJg8QgEgbgOgOQgPgPgagFQhEgMg2Bzg");
	this.shape_5.setTransform(25.8032,24.4498);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(8.8,1,1).p("AipiqQAmgfAyAeQAwAeAnBKQA2hzBEAMQAaAFAPAPQAOAOAEAbQAJA8giBIQgrBbhmBJQhzg3g6h2Qg5hyAshGg");
	this.shape_6.setTransform(25.8032,24.4498);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(9.6,1,1).p("AipiqQAmgfAyAeQAwAeAnBKQA2hzBEAMQAaAFAPAPQAOAOAEAbQAJA8giBIQgrBbhmBJQhzg3g6h2Qg5hyAshGg");
	this.shape_7.setTransform(25.8032,24.4498);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(10.2,1,1).p("AipiqQAmgfAyAeQAwAeAnBKQA2hzBEAMQAaAFAPAPQAOAOAEAbQAJA8giBIQgrBbhmBJQhzg3g6h2Qg5hyAshGg");
	this.shape_8.setTransform(25.8032,24.4498);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(10.8,1,1).p("AipiqQAmgfAyAeQAwAeAnBKQA2hzBEAMQAaAFAPAPQAOAOAEAbQAJA8giBIQgrBbhmBJQhzg3g6h2Qg5hyAshGg");
	this.shape_9.setTransform(25.8032,24.4498);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(11.3,1,1).p("AipiqQAmgfAyAeQAwAeAnBKQA2hzBEAMQAaAFAPAPQAOAOAEAbQAJA8giBIQgrBbhmBJQhzg3g6h2Qg5hyAshGg");
	this.shape_10.setTransform(25.8032,24.4498);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(11.7,1,1).p("AipiqQAmgfAyAeQAwAeAnBKQA2hzBEAMQAaAFAPAPQAOAOAEAbQAJA8giBIQgrBbhmBJQhzg3g6h2Qg5hyAshGg");
	this.shape_11.setTransform(25.8032,24.4498);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},24).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},6).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape}]},1).wait(21));
	this.timeline.addTween(cjs.Tween.get(this.shape).wait(24).to({_off:true},1).wait(11).to({_off:false},0).wait(6).to({_off:true},1).wait(11).to({_off:false},0).wait(21));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_1_Layer_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(8.5,1,1).p("AA7iUQioC3BJBy");
	this.shape.setTransform(-158.4903,-33.525);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(8.5,1,1).p("AgjCWQhKh0Cni3");
	this.shape_1.setTransform(-158.4435,-33.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(8.5,1,1).p("AggCYQhMh5Cli2");
	this.shape_2.setTransform(-158.3316,-33.775);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(8.5,1,1).p("AgcCaQhQh+Cji1");
	this.shape_3.setTransform(-158.2115,-34.025);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(8.5,1,1).p("AgYCcQhUiDChi0");
	this.shape_4.setTransform(-158.095,-34.275);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(8.5,1,1).p("AgUCeQhXiICfiz");
	this.shape_5.setTransform(-157.9822,-34.475);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(8.5,1,1).p("AgRCgQhaiNCdiy");
	this.shape_6.setTransform(-157.8831,-34.7);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(8.5,1,1).p("AgOCiQhciRCbiy");
	this.shape_7.setTransform(-157.7966,-34.875);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(8.5,1,1).p("AgLCjQhfiUCaix");
	this.shape_8.setTransform(-157.7336,-35.025);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(8.5,1,1).p("AgJClQhgiYCYix");
	this.shape_9.setTransform(-157.6624,-35.15);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(8.5,1,1).p("AgHClQhiiaCXiv");
	this.shape_10.setTransform(-157.6327,-35.275);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(8.5,1,1).p("AgFCmQhkicCXiv");
	this.shape_11.setTransform(-157.5841,-35.375);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(8.5,1,1).p("AgECnQhlieCWiv");
	this.shape_12.setTransform(-157.545,-35.425);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(8.5,1,1).p("AgDCoQhmifCWiw");
	this.shape_13.setTransform(-157.5441,-35.5);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(8.5,1,1).p("AgCCoQhmigCViv");
	this.shape_14.setTransform(-157.5143,-35.525);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(8.5,1,1).p("AAtinQiWCuBnCh");
	this.shape_15.setTransform(-157.509,-35.55);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(8.5,1,1).p("AgGClQhjiaCXiw");
	this.shape_16.setTransform(-157.6235,-35.3);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(8.5,1,1).p("AgLCjQhfiVCaix");
	this.shape_17.setTransform(-157.7336,-35.05);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#000000").ss(8.5,1,1).p("AgPChQhbiPCbiy");
	this.shape_18.setTransform(-157.8379,-34.825);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#000000").ss(8.5,1,1).p("AgSCfQhZiLCeiy");
	this.shape_19.setTransform(-157.9355,-34.625);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#000000").ss(8.5,1,1).p("AgVCeQhWiHCfi0");
	this.shape_20.setTransform(-158.0252,-34.425);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#000000").ss(8.5,1,1).p("AgbCaQhRh/Cii1");
	this.shape_21.setTransform(-158.1945,-34.1);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#000000").ss(8.5,1,1).p("AgdCZQhPh8Cki1");
	this.shape_22.setTransform(-158.2565,-33.975);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#000000").ss(8.5,1,1).p("AgfCYQhNh5Cli2");
	this.shape_23.setTransform(-158.3083,-33.825);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#000000").ss(8.5,1,1).p("AggCYQhMh4Cli3");
	this.shape_24.setTransform(-158.3659,-33.75);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f().s("#000000").ss(8.5,1,1).p("AgiCXQhKh2Cmi3");
	this.shape_25.setTransform(-158.4241,-33.675);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f().s("#000000").ss(8.5,1,1).p("AgjCVQhKhzCoi3");
	this.shape_26.setTransform(-158.4662,-33.55);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f().s("#000000").ss(8.5,1,1).p("AgkCVQhJhyCoi3");
	this.shape_27.setTransform(-158.4905,-33.525);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},9).to({state:[{t:this.shape_1,p:{x:-158.4435}}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4,p:{y:-34.275}}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_15}]},30).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_4,p:{y:-34.25}}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_1,p:{x:-158.4314}}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_27}]},1).to({state:[{t:this.shape}]},1).wait(6));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_1_copy_2_Layer_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(11,1,1).p("Aitg5QAFA/AuAlQAvAkA7ABQA6AAA3gfQA2gfARgyQAIgagDgV");
	this.shape.setTransform(244.6548,-37.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(11,1,1).p("Ai1g4QALA+AwAjQAuAjA7ABQA5AAA4gdQA4geASgvQAKgaACgV");
	this.shape_1.setTransform(244.85,-37.7996);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(11,1,1).p("Ai8g2QAQA8AwAhQAwAiA5ABQA5ABA4gcQA5gdAUgsQANgZAFgW");
	this.shape_2.setTransform(245.075,-37.7746);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(11,1,1).p("AjDg1QAUA7AxAgQAxAgA4ABQA4ACA6gbQA6gcAVgpQAQgZAIgX");
	this.shape_3.setTransform(245.3,-37.7961);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(11,1,1).p("AjJgzQAYA5AyAeQAxAfA4ACQA3ACA7gaQA7gbAWgmQASgZAKgX");
	this.shape_4.setTransform(245.5,-37.7709);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(11,1,1).p("AjOgyQAcA3AyAeQAxAeA4ACQA2ACA8gZQA8gaAXgjQATgZAOgY");
	this.shape_5.setTransform(245.675,-37.7676);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(11,1,1).p("AjTgyQAgA3AyAcQAyAdA3ADQA1ACA9gZQA9gZAYggQAVgaAQgX");
	this.shape_6.setTransform(245.825,-37.7425);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(11,1,1).p("AjYgwQAjA1AzAcQAyAcA2ACQA1ADA+gYQA9gYAagfQAWgZATgY");
	this.shape_7.setTransform(245.975,-37.763);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(11,1,1).p("AjcgwQAmA1AzAbQAzAaA1ADQA1ADA+gXQA+gYAagdQAYgYAVgZ");
	this.shape_8.setTransform(246.1,-37.7377);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(11,1,1).p("AjfgvQAoA0AzAaQA0AaA1ADQA0ADA/gWQA/gXAagcQAZgYAWgZ");
	this.shape_9.setTransform(246.2,-37.7574);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(11,1,1).p("AjigvQAqA0A0AZQAzAaA1ADQA0ADA/gWQA/gXAcgaQAagYAXgZ");
	this.shape_10.setTransform(246.275,-37.732);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(11,1,1).p("AjlguQAsAzA0AZQA0AZA0ADQAzADBAgWQBAgWAcgZQAbgYAZgZ");
	this.shape_11.setTransform(246.375,-37.726);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(11,1,1).p("AjnguQAuAzA0AYQA0AZA0ADQAzAEBAgWQBAgWAcgYQAcgYAaga");
	this.shape_12.setTransform(246.425,-37.7255);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(11,1,1).p("AjogtQAvAyA0AYQA0AYA0AEQAzADBAgVQBAgWAdgXQAcgZAagZ");
	this.shape_13.setTransform(246.45,-37.7255);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(11,1,1).p("AjpgtQAvAyA1AYQA0AYA0ADQAyAEBBgVQBAgWAdgXQAcgYAbga");
	this.shape_14.setTransform(246.475,-37.725);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(11,1,1).p("AjpgtQAvAyA0AYQA1AYAzADQAzAEBBgVQBAgWAdgXQAcgYAbga");
	this.shape_15.setTransform(246.5,-37.7272);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(11,1,1).p("AjdgwQAnA1AzAaQAzAbA1ADQA0ACA/gWQA/gYAagcQAYgZAWgY");
	this.shape_16.setTransform(246.15,-37.7375);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(11,1,1).p("AjKgzQAZA5AyAeQAxAfA4ABQA2ACA8gaQA7gaAWglQATgZALgX");
	this.shape_17.setTransform(245.525,-37.7679);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#000000").ss(11,1,1).p("Ai8g2QAQA8AwAhQAvAiA6ABQA5ABA5gcQA5gdATgrQANgaAFgW");
	this.shape_18.setTransform(245.1,-37.7746);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#000000").ss(11,1,1).p("Ai2g3QALA9AwAiQAvAjA6ABQA5ABA4gdQA4geATguQALgaACgV");
	this.shape_19.setTransform(244.925,-37.7746);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#000000").ss(11,1,1).p("Aiyg4QAIA+AvAjQAvAkA7ABQA5AAA4geQA3geASgwQAKgaAAgV");
	this.shape_20.setTransform(244.8,-37.8);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#000000").ss(11,1,1).p("Aivg5QAGA/AvAkQAvAkA7ABQA6AAA3geQA2gfARgxQAJgagBgV");
	this.shape_21.setTransform(244.7107,-37.8);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#000000").ss(11,1,1).p("Aiug5QAGA/AuAlQAvAkA7ABQA6AAA3gfQA2gfARgyQAIgagCgV");
	this.shape_22.setTransform(244.67,-37.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},9).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_15}]},30).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape}]},1).wait(11));

}).prototype = p = new cjs.MovieClip();


(lib.Scene_1_Layer_2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.instance = new lib.bg();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Scene_1_Layer_2, null, null);


(lib.Symbol5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_74 = function() {
		this.___loopingOver___ = true;
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(74).call(this.frame_74).wait(1));

	// Layer_3_obj_
	this.Layer_3 = new lib.Symbol_5_Layer_3();
	this.Layer_3.name = "Layer_3";
	this.Layer_3.parent = this;
	this.Layer_3.setTransform(9,4.2,1,1,0,0,0,9,4.2);
	this.Layer_3.depth = 0;
	this.Layer_3.isAttachedToCamera = 0
	this.Layer_3.isAttachedToMask = 0
	this.Layer_3.layerDepth = 0
	this.Layer_3.layerIndex = 0
	this.Layer_3.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_3).wait(75));

	// Layer_2_obj_
	this.Layer_2 = new lib.Symbol_5_Layer_2();
	this.Layer_2.name = "Layer_2";
	this.Layer_2.parent = this;
	this.Layer_2.setTransform(8.3,8.1,1,1,0,0,0,8.3,8.1);
	this.Layer_2.depth = 0;
	this.Layer_2.isAttachedToCamera = 0
	this.Layer_2.isAttachedToMask = 0
	this.Layer_2.layerDepth = 0
	this.Layer_2.layerIndex = 1
	this.Layer_2.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_2).wait(75));

	// Layer_1_obj_
	this.Layer_1 = new lib.Symbol_5_Layer_1();
	this.Layer_1.name = "Layer_1";
	this.Layer_1.parent = this;
	this.Layer_1.depth = 0;
	this.Layer_1.isAttachedToCamera = 0
	this.Layer_1.isAttachedToMask = 0
	this.Layer_1.layerDepth = 0
	this.Layer_1.layerIndex = 2
	this.Layer_1.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_1).wait(75));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-7,-9.4,31.9,28.200000000000003);


(lib.Symbol4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_74 = function() {
		this.___loopingOver___ = true;
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(74).call(this.frame_74).wait(1));

	// Layer_2_obj_
	this.Layer_2 = new lib.Symbol_4_Layer_2();
	this.Layer_2.name = "Layer_2";
	this.Layer_2.parent = this;
	this.Layer_2.setTransform(-5.5,-2.6,1,1,0,0,0,-5.5,-2.6);
	this.Layer_2.depth = 0;
	this.Layer_2.isAttachedToCamera = 0
	this.Layer_2.isAttachedToMask = 0
	this.Layer_2.layerDepth = 0
	this.Layer_2.layerIndex = 0
	this.Layer_2.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_2).wait(75));

	// Layer_1_obj_
	this.Layer_1 = new lib.Symbol_4_Layer_1();
	this.Layer_1.name = "Layer_1";
	this.Layer_1.parent = this;
	this.Layer_1.setTransform(-0.4,0,1,1,0,0,0,-0.4,0);
	this.Layer_1.depth = 0;
	this.Layer_1.isAttachedToCamera = 0
	this.Layer_1.isAttachedToMask = 0
	this.Layer_1.layerDepth = 0
	this.Layer_1.layerIndex = 1
	this.Layer_1.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_1).wait(75));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-16.8,-17.6,29.200000000000003,30.6);


(lib.Symbol2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_74 = function() {
		this.___loopingOver___ = true;
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(74).call(this.frame_74).wait(1));

	// Layer_2_obj_
	this.Layer_2 = new lib.Symbol_2_Layer_2();
	this.Layer_2.name = "Layer_2";
	this.Layer_2.parent = this;
	this.Layer_2.setTransform(25.8,24.4,1,1,0,0,0,25.8,24.4);
	this.Layer_2.depth = 0;
	this.Layer_2.isAttachedToCamera = 0
	this.Layer_2.isAttachedToMask = 0
	this.Layer_2.layerDepth = 0
	this.Layer_2.layerIndex = 0
	this.Layer_2.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_2).wait(75));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,-0.2,52,49.400000000000006);


(lib.Symbol_1_pack_png = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// pack_png
	this.instance = new lib.Symbol3();
	this.instance.parent = this;
	this.instance.setTransform(34,154.4,1,1,0,0,0,34,154.4);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(10).to({regX:0,regY:0,scaleY:1.0001,skewX:-0.1173,x:-0.3159,y:-0.0083},0).wait(1).to({scaleY:1.0002,skewX:-0.4028,x:-1.0849,y:-0.0256},0).wait(1).to({scaleY:1.0004,skewX:-0.7601,x:-2.0478,y:-0.042},0).wait(1).to({scaleY:1.0006,skewX:-1.126,x:-3.0338,y:-0.0525},0).wait(1).to({scaleY:1.0007,skewX:-1.4691,x:-3.9587,y:-0.0566},0).wait(1).to({scaleY:1.0008,skewX:-1.7771,x:-4.7892,y:-0.0556},0).wait(1).to({scaleY:1.001,skewX:-2.0469,x:-5.5166,y:-0.0511},0).wait(1).to({scaleY:1.0011,skewX:-2.279,x:-6.1426,y:-0.0444},0).wait(1).to({scaleY:1.0012,skewX:-2.4758,x:-6.6732,y:-0.0368},0).wait(1).to({skewX:-2.64,x:-7.116,y:-0.029},0).wait(1).to({scaleY:1.0013,skewX:-2.7742,x:-7.4781,y:-0.0217},0).wait(1).to({scaleY:1.0014,skewX:-2.8812,x:-7.7666,y:-0.0153},0).wait(1).to({skewX:-2.9632,x:-7.9878,y:-0.01},0).wait(1).to({scaleY:1.0015,skewX:-3.0224,x:-8.1474,y:-0.006},0).wait(1).to({regX:34,regY:154.4,scaleY:1.0014,skewX:-3.0605,x:34,y:154.4},0).wait(30).to({scaleY:1,skewX:0},15,cjs.Ease.get(1)).wait(6));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_1_Layer_7 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_7
	this.instance = new lib.Symbol5();
	this.instance.parent = this;
	this.instance.setTransform(-163.5,-20.95,1,1,0,0,0,-2.9,1.9);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(10).to({regX:8.9,regY:4.7,rotation:-0.3157,x:-151.45,y:-18.15},0).wait(1).to({rotation:-1.0838,x:-151,y:-18.25},0).wait(1).to({rotation:-2.0456,x:-150.5,y:-18.35},0).wait(1).to({rotation:-3.0301,x:-149.9,y:-18.45},0).wait(1).to({rotation:-3.9534,x:-149.35,y:-18.55},0).wait(1).to({rotation:-4.7824,x:-148.9,y:-18.7},0).wait(1).to({rotation:-5.5083,x:-148.45,y:-18.75},0).wait(1).to({rotation:-6.1329,x:-148.1,y:-18.85},0).wait(1).to({rotation:-6.6625,x:-147.8,y:-18.95},0).wait(1).to({rotation:-7.1042,x:-147.5,y:-19},0).wait(1).to({rotation:-7.4656,x:-147.35},0).wait(1).to({rotation:-7.7535,x:-147.2,y:-19.05},0).wait(1).to({rotation:-7.9742,x:-147.05,y:-19.1},0).wait(1).to({rotation:-8.1333,x:-147},0).wait(1).to({regX:-2.9,regY:1.8,rotation:-8.2359,x:-159,y:-20.25},0).wait(30).to({regY:1.9,rotation:0,x:-163.5,y:-20.95},15,cjs.Ease.get(1)).wait(6));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_1_Layer_5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_5
	this.instance = new lib.Symbol4();
	this.instance.parent = this;
	this.instance.setTransform(-13.85,36.2,1,1,0,0,0,2.6,7.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(10).to({regX:-2.2,regY:-2.3,rotation:-0.4014,x:-18.5,y:26.8},0).wait(1).to({rotation:-1.3783,x:-18.3,y:27.9},0).wait(1).to({rotation:-2.6013,x:-18.05,y:29.25},0).wait(1).to({rotation:-3.8532,x:-17.75,y:30.65},0).wait(1).to({rotation:-5.0274,x:-17.45,y:31.95},0).wait(1).to({rotation:-6.0816,x:-17.25,y:33.15},0).wait(1).to({rotation:-7.0047,x:-17.05,y:34.15},0).wait(1).to({rotation:-7.7991,x:-16.8,y:35.05},0).wait(1).to({rotation:-8.4724,x:-16.7,y:35.8},0).wait(1).to({rotation:-9.0342,x:-16.45,y:36.45},0).wait(1).to({rotation:-9.4937,x:-16.4,y:36.95},0).wait(1).to({rotation:-9.8598,x:-16.3,y:37.4},0).wait(1).to({rotation:-10.1405,x:-16.2,y:37.7},0).wait(1).to({rotation:-10.3429,x:-16.15,y:37.95},0).wait(1).to({regX:2.6,regY:7.5,rotation:-10.4734,x:-9.65,y:46.85},0).wait(30).to({rotation:0,x:-13.85,y:36.2},15,cjs.Ease.get(1)).wait(6));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_1_copy_2_Layer_2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.instance = new lib.Symbol2();
	this.instance.parent = this;
	this.instance.setTransform(287,-70.55,1,1,0,0,0,26,24.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(24).to({regX:26.1,regY:24.4,scaleX:1.0999,scaleY:1.0999,x:287.05,y:-70.6},5,cjs.Ease.get(0.5)).to({regX:26,regY:24.5,scaleX:1,scaleY:1,x:287,y:-70.55},7,cjs.Ease.get(0.5)).wait(6).to({regX:26.1,regY:24.4,scaleX:1.0999,scaleY:1.0999,x:287.05,y:-70.6},5,cjs.Ease.get(0.5)).to({regX:26,regY:24.5,scaleX:1,scaleY:1,x:287,y:-70.55},7,cjs.Ease.get(0.5)).wait(21));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_74 = function() {
		this.___loopingOver___ = true;
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(74).call(this.frame_74).wait(1));

	// Layer_2_obj_
	this.Layer_2 = new lib.Symbol_1_Layer_2();
	this.Layer_2.name = "Layer_2";
	this.Layer_2.parent = this;
	this.Layer_2.setTransform(25.8,24.4,1,1,0,0,0,25.8,24.4);
	this.Layer_2.depth = 0;
	this.Layer_2.isAttachedToCamera = 0
	this.Layer_2.isAttachedToMask = 0
	this.Layer_2.layerDepth = 0
	this.Layer_2.layerIndex = 0
	this.Layer_2.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_2).wait(75));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,-0.2,52,49.400000000000006);


(lib.Symbol_1_copy_2_Layer_3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_3
	this.instance = new lib.Symbol1();
	this.instance.parent = this;
	this.instance.setTransform(205,-69.55,1,1,0,0,0,26,24.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(24).to({regY:24.4,scaleX:1.0999,scaleY:1.0999,y:-69.6},5,cjs.Ease.get(0.5)).to({regY:24.5,scaleX:1,scaleY:1,y:-69.55},7,cjs.Ease.get(0.5)).wait(6).to({regY:24.4,scaleX:1.0999,scaleY:1.0999,y:-69.6},5,cjs.Ease.get(0.5)).to({regY:24.5,scaleX:1,scaleY:1,y:-69.55},7,cjs.Ease.get(0.5)).wait(21));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol1copy2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_74 = function() {
		this.___loopingOver___ = true;
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(74).call(this.frame_74).wait(1));

	// Layer_1_obj_
	this.Layer_1 = new lib.Symbol_1_copy_2_Layer_1();
	this.Layer_1.name = "Layer_1";
	this.Layer_1.parent = this;
	this.Layer_1.setTransform(244.7,-37.8,1,1,0,0,0,244.7,-37.8);
	this.Layer_1.depth = 0;
	this.Layer_1.isAttachedToCamera = 0
	this.Layer_1.isAttachedToMask = 0
	this.Layer_1.layerDepth = 0
	this.Layer_1.layerIndex = 0
	this.Layer_1.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_1).wait(75));

	// Layer_2_obj_
	this.Layer_2 = new lib.Symbol_1_copy_2_Layer_2();
	this.Layer_2.name = "Layer_2";
	this.Layer_2.parent = this;
	this.Layer_2.setTransform(286.8,-70.6,1,1,0,0,0,286.8,-70.6);
	this.Layer_2.depth = 0;
	this.Layer_2.isAttachedToCamera = 0
	this.Layer_2.isAttachedToMask = 0
	this.Layer_2.layerDepth = 0
	this.Layer_2.layerIndex = 1
	this.Layer_2.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_2).wait(75));

	// Layer_3_obj_
	this.Layer_3 = new lib.Symbol_1_copy_2_Layer_3();
	this.Layer_3.name = "Layer_3";
	this.Layer_3.parent = this;
	this.Layer_3.setTransform(204.8,-69.6,1,1,0,0,0,204.8,-69.6);
	this.Layer_3.depth = 0;
	this.Layer_3.isAttachedToCamera = 0
	this.Layer_3.isAttachedToMask = 0
	this.Layer_3.layerDepth = 0
	this.Layer_3.layerIndex = 2
	this.Layer_3.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_3).wait(75));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(177.4,-97.7,136.70000000000002,73.4);


(lib.Symbol_1_Symbol_1_copy_2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Symbol_1_copy_2
	this.instance = new lib.Symbol1copy2();
	this.instance.parent = this;
	this.instance.setTransform(-35.05,-71.25,0.82,0.82,0,0,0,245.8,-56.4);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(10).to({regY:-61,x:-35.5,y:-75},0).wait(1).to({x:-36.7},0).wait(1).to({x:-38.15},0).wait(1).to({x:-39.65},0).wait(1).to({x:-41.05},0).wait(1).to({x:-42.35},0).wait(1).to({x:-43.45},0).wait(1).to({x:-44.4},0).wait(1).to({x:-45.2},0).wait(1).to({x:-45.9},0).wait(1).to({x:-46.45},0).wait(1).to({x:-46.9},0).wait(1).to({x:-47.2},0).wait(1).to({x:-47.45},0).wait(1).to({regY:-56.4,x:-47.65,y:-71.25},0).wait(30).to({x:-35.05},15,cjs.Ease.get(1)).wait(6));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol1_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_74 = function() {
		this.___loopingOver___ = true;
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(74).call(this.frame_74).wait(1));

	// Layer_7_obj_
	this.Layer_7 = new lib.Symbol_1_Layer_7();
	this.Layer_7.name = "Layer_7";
	this.Layer_7.parent = this;
	this.Layer_7.setTransform(-152.4,-17.2,1,1,0,0,0,-152.4,-17.2);
	this.Layer_7.depth = 0;
	this.Layer_7.isAttachedToCamera = 0
	this.Layer_7.isAttachedToMask = 0
	this.Layer_7.layerDepth = 0
	this.Layer_7.layerIndex = 0
	this.Layer_7.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_7).wait(10).to({regX:-148.4,regY:-19.1,x:-148.4,y:-19.1},0).wait(14).to({regX:-152.4,regY:-17.2,x:-152.4,y:-17.2},0).wait(51));

	// Layer_9 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("AiikiIDmAAQAoCVA2F6IlEA2g");
	mask.setTransform(-168.95,-28.575);

	// Layer_1_obj_
	this.Layer_1 = new lib.Symbol_1_Layer_1();
	this.Layer_1.name = "Layer_1";
	this.Layer_1.parent = this;
	this.Layer_1.setTransform(-158.5,-33.5,1,1,0,0,0,-158.5,-33.5);
	this.Layer_1.depth = 0;
	this.Layer_1.isAttachedToCamera = 0
	this.Layer_1.isAttachedToMask = 0
	this.Layer_1.layerDepth = 0
	this.Layer_1.layerIndex = 1
	this.Layer_1.maskLayerName = 0

	var maskedShapeInstanceList = [this.Layer_1];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.Layer_1).wait(75));

	// Layer_5_obj_
	this.Layer_5 = new lib.Symbol_1_Layer_5();
	this.Layer_5.name = "Layer_5";
	this.Layer_5.parent = this;
	this.Layer_5.setTransform(-19.4,27.3,1,1,0,0,0,-19.4,27.3);
	this.Layer_5.depth = 0;
	this.Layer_5.isAttachedToCamera = 0
	this.Layer_5.isAttachedToMask = 0
	this.Layer_5.layerDepth = 0
	this.Layer_5.layerIndex = 2
	this.Layer_5.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_5).wait(10).to({regX:-16.2,regY:33.4,x:-16.2,y:33.4},0).wait(14).to({regX:-19.4,regY:27.3,x:-19.4,y:27.3},0).wait(51));

	// Layer_4_obj_
	this.Layer_4 = new lib.Symbol_1_Layer_4();
	this.Layer_4.name = "Layer_4";
	this.Layer_4.parent = this;
	this.Layer_4.setTransform(16.1,-6.5,1,1,0,0,0,16.1,-6.5);
	this.Layer_4.depth = 0;
	this.Layer_4.isAttachedToCamera = 0
	this.Layer_4.isAttachedToMask = 0
	this.Layer_4.layerDepth = 0
	this.Layer_4.layerIndex = 3
	this.Layer_4.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_4).wait(75));

	// Symbol_1_copy_2_obj_
	this.Symbol_1_copy_2 = new lib.Symbol_1_Symbol_1_copy_2();
	this.Symbol_1_copy_2.name = "Symbol_1_copy_2";
	this.Symbol_1_copy_2.parent = this;
	this.Symbol_1_copy_2.setTransform(-35.1,-74,1,1,0,0,0,-35.1,-74);
	this.Symbol_1_copy_2.depth = 0;
	this.Symbol_1_copy_2.isAttachedToCamera = 0
	this.Symbol_1_copy_2.isAttachedToMask = 0
	this.Symbol_1_copy_2.layerDepth = 0
	this.Symbol_1_copy_2.layerIndex = 4
	this.Symbol_1_copy_2.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Symbol_1_copy_2).wait(10).to({regX:-41.4,regY:-75,x:-41.4,y:-75},0).wait(14).to({regX:-35.1,regY:-74,x:-35.1,y:-74},0).wait(51));

	// pack_png_obj_
	this.pack_png = new lib.Symbol_1_pack_png();
	this.pack_png.name = "pack_png";
	this.pack_png.parent = this;
	this.pack_png.depth = 0;
	this.pack_png.isAttachedToCamera = 0
	this.pack_png.isAttachedToMask = 0
	this.pack_png.layerDepth = 0
	this.pack_png.layerIndex = 5
	this.pack_png.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.pack_png).wait(10).to({regX:-8.3,regY:-0.1,x:-8.3,y:-0.1},0).wait(14).to({regX:0,regY:0,x:0,y:0},0).wait(51));

	// Layer_10 (mask)
	var mask_1 = new cjs.Shape();
	mask_1._off = true;
	mask_1.graphics.p("AnMDVIAAoaICuAAIAAIag");
	mask_1.setTransform(-46.125,-32.625);

	// Layer_6_obj_
	this.Layer_6 = new lib.Symbol_1_Layer_6();
	this.Layer_6.name = "Layer_6";
	this.Layer_6.parent = this;
	this.Layer_6.setTransform(-88.2,-41.5,1,1,0,0,0,-88.2,-41.5);
	this.Layer_6.depth = 0;
	this.Layer_6.isAttachedToCamera = 0
	this.Layer_6.isAttachedToMask = 0
	this.Layer_6.layerDepth = 0
	this.Layer_6.layerIndex = 6
	this.Layer_6.maskLayerName = 0

	var maskedShapeInstanceList = [this.Layer_6];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask_1;
	}

	this.timeline.addTween(cjs.Tween.get(this.Layer_6).wait(75));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-175.7,-186,334.9,372.2);


(lib.Scene_1_Layer_4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_4
	this.instance = new lib.Symbol1_1();
	this.instance.parent = this;
	this.instance.setTransform(851.5,478.9);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Scene_1_Layer_4, null, null);


// stage content:
(lib.Sept_01 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	this.___GetDepth___ = function(obj) {
		var depth = obj.depth;
		var cameraObj = this.___camera___instance;
		if(cameraObj && cameraObj.depth && obj.isAttachedToCamera)
		{
			depth += depth + cameraObj.depth;
		}
		return depth;
		}
	this.___needSorting___ = function() {
		for (var i = 0; i < this.getNumChildren() - 1; i++)
		{
			var prevDepth = this.___GetDepth___(this.getChildAt(i));
			var nextDepth = this.___GetDepth___(this.getChildAt(i + 1));
			if (prevDepth < nextDepth)
				return true;
		}
		return false;
	}
	this.___sortFunction___ = function(obj1, obj2) {
		return (this.exportRoot.___GetDepth___(obj2) - this.exportRoot.___GetDepth___(obj1));
	}
	this.on('tick', function (event){
		var curTimeline = event.currentTarget;
		if (curTimeline.___needSorting___()){
			this.sortChildren(curTimeline.___sortFunction___);
		}
	});

	// Layer_4_obj_
	this.Layer_4 = new lib.Scene_1_Layer_4();
	this.Layer_4.name = "Layer_4";
	this.Layer_4.parent = this;
	this.Layer_4.setTransform(837.6,478.9,1,1,0,0,0,837.6,478.9);
	this.Layer_4.depth = 0;
	this.Layer_4.isAttachedToCamera = 0
	this.Layer_4.isAttachedToMask = 0
	this.Layer_4.layerDepth = 0
	this.Layer_4.layerIndex = 0
	this.Layer_4.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_4).wait(1));

	// Layer_2_obj_
	this.Layer_2 = new lib.Scene_1_Layer_2();
	this.Layer_2.name = "Layer_2";
	this.Layer_2.parent = this;
	this.Layer_2.setTransform(543,343,1,1,0,0,0,543,343);
	this.Layer_2.depth = 0;
	this.Layer_2.isAttachedToCamera = 0
	this.Layer_2.isAttachedToMask = 0
	this.Layer_2.layerDepth = 0
	this.Layer_2.layerIndex = 1
	this.Layer_2.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_2).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(540,340,546,346);
// library properties:
lib.properties = {
	id: '343241879435144087C46130A76BEB31',
	width: 1080,
	height: 680,
	fps: 24,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: [
		{src:"assets/images/bg52.jpg", id:"bg"},
		{src:"assets/images/pack52.png", id:"pack"}
	],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['343241879435144087C46130A76BEB31'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}


// Layer depth API : 

AdobeAn.Layer = new function() {
	this.getLayerZDepth = function(timeline, layerName)
	{
		if(layerName === "Camera")
		layerName = "___camera___instance";
		var script = "if(timeline." + layerName + ") timeline." + layerName + ".depth; else 0;";
		return eval(script);
	}
	this.setLayerZDepth = function(timeline, layerName, zDepth)
	{
		const MAX_zDepth = 10000;
		const MIN_zDepth = -5000;
		if(zDepth > MAX_zDepth)
			zDepth = MAX_zDepth;
		else if(zDepth < MIN_zDepth)
			zDepth = MIN_zDepth;
		if(layerName === "Camera")
		layerName = "___camera___instance";
		var script = "if(timeline." + layerName + ") timeline." + layerName + ".depth = " + zDepth + ";";
		eval(script);
	}
	this.removeLayer = function(timeline, layerName)
	{
		if(layerName === "Camera")
		layerName = "___camera___instance";
		var script = "if(timeline." + layerName + ") timeline.removeChild(timeline." + layerName + ");";
		eval(script);
	}
	this.addNewLayer = function(timeline, layerName, zDepth)
	{
		if(layerName === "Camera")
		layerName = "___camera___instance";
		zDepth = typeof zDepth !== 'undefined' ? zDepth : 0;
		var layer = new createjs.MovieClip();
		layer.name = layerName;
		layer.depth = zDepth;
		layer.layerIndex = 0;
		timeline.addChild(layer);
	}
}


})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;

$(function () {
  var canvas, stage, exportRoot, anim_container, dom_overlay_container, fnStartAnimation;
  function init() {
    canvas = document.getElementById("canvas52");
    anim_container = document.getElementById("animation_container52");
    dom_overlay_container = document.getElementById("dom_overlay_container52");
    var comp=AdobeAn.getComposition("343241879435144087C46130A76BEB31");
    var lib=comp.getLibrary();
    var loader = new createjs.LoadQueue(false);
    loader.addEventListener("fileload", function(evt){handleFileLoad(evt,comp)});
    loader.addEventListener("complete", function(evt){handleComplete(evt,comp)});
    var lib=comp.getLibrary();
    loader.loadManifest(lib.properties.manifest);
  }
  function handleFileLoad(evt, comp) {
    var images=comp.getImages();
    if (evt && (evt.item.type == "image")) { images[evt.item.id] = evt.result; }
  }
  function handleComplete(evt,comp) {
    //This function is always called, irrespective of the content. You can use the variable "stage" after it is created in token create_stage.
    var lib=comp.getLibrary();
    var ss=comp.getSpriteSheet();
    var queue = evt.target;
    var ssMetadata = lib.ssMetadata;
    for(i=0; i<ssMetadata.length; i++) {
      ss[ssMetadata[i].name] = new createjs.SpriteSheet( {"images": [queue.getResult(ssMetadata[i].name)], "frames": ssMetadata[i].frames} )
    }
    var preloaderDiv = document.getElementById("_preload_div_52");
    preloaderDiv.style.display = 'none';
    canvas.style.display = 'block';
    exportRoot = new lib.Sept_01();
    stage = new lib.Stage(canvas);
    //Registers the "tick" event listener.
    fnStartAnimation = function() {
      stage.addChild(exportRoot);
      createjs.Ticker.setFPS(lib.properties.fps);
      createjs.Ticker.addEventListener("tick", stage)
      stage.addEventListener("tick", handleTick)
      function getProjectionMatrix(container, totalDepth) {
        var focalLength = 528.25;
        var projectionCenter = { x : lib.properties.width/2, y : lib.properties.height/2 };
        var scale = (totalDepth + focalLength)/focalLength;
        var scaleMat = new createjs.Matrix2D;
        scaleMat.a = 1/scale;
        scaleMat.d = 1/scale;
        var projMat = new createjs.Matrix2D;
        projMat.tx = -projectionCenter.x;
        projMat.ty = -projectionCenter.y;
        projMat = projMat.prependMatrix(scaleMat);
        projMat.tx += projectionCenter.x;
        projMat.ty += projectionCenter.y;
        return projMat;
      }
      function handleTick(event) {
        var cameraInstance = exportRoot.___camera___instance;
        if(cameraInstance !== undefined && cameraInstance.pinToObject !== undefined)
        {
          cameraInstance.x = cameraInstance.pinToObject.x + cameraInstance.pinToObject.pinOffsetX;
          cameraInstance.y = cameraInstance.pinToObject.y + cameraInstance.pinToObject.pinOffsetY;
          if(cameraInstance.pinToObject.parent !== undefined && cameraInstance.pinToObject.parent.depth !== undefined)
            cameraInstance.depth = cameraInstance.pinToObject.parent.depth + cameraInstance.pinToObject.pinOffsetZ;
        }
        applyLayerZDepth(exportRoot);
      }
      function applyLayerZDepth(parent)
      {
        var cameraInstance = parent.___camera___instance;
        var focalLength = 528.25;
        var projectionCenter = { 'x' : 0, 'y' : 0};
        if(parent === exportRoot)
        {
          var stageCenter = { 'x' : lib.properties.width/2, 'y' : lib.properties.height/2 };
          projectionCenter.x = stageCenter.x;
          projectionCenter.y = stageCenter.y;
        }
        for(child in parent.children)
        {
          var layerObj = parent.children[child];
          if(layerObj == cameraInstance)
            continue;
          applyLayerZDepth(layerObj, cameraInstance);
          if(layerObj.layerDepth === undefined)
            continue;
          if(layerObj.currentFrame != layerObj.parent.currentFrame)
          {
            layerObj.gotoAndPlay(layerObj.parent.currentFrame);
          }
          var matToApply = new createjs.Matrix2D;
          var cameraMat = new createjs.Matrix2D;
          var totalDepth = layerObj.layerDepth ? layerObj.layerDepth : 0;
          var cameraDepth = 0;
          if(cameraInstance && !layerObj.isAttachedToCamera)
          {
            var mat = cameraInstance.getMatrix();
            mat.tx -= projectionCenter.x;
            mat.ty -= projectionCenter.y;
            cameraMat = mat.invert();
            cameraMat.prependTransform(projectionCenter.x, projectionCenter.y, 1, 1, 0, 0, 0, 0, 0);
            cameraMat.appendTransform(-projectionCenter.x, -projectionCenter.y, 1, 1, 0, 0, 0, 0, 0);
            if(cameraInstance.depth)
              cameraDepth = cameraInstance.depth;
          }
          if(layerObj.depth)
          {
            totalDepth = layerObj.depth;
          }
          //Offset by camera depth
          totalDepth -= cameraDepth;
          if(totalDepth < -focalLength)
          {
            matToApply.a = 0;
            matToApply.d = 0;
          }
          else
          {
            if(layerObj.layerDepth)
            {
              var sizeLockedMat = getProjectionMatrix(parent, layerObj.layerDepth);
              if(sizeLockedMat)
              {
                sizeLockedMat.invert();
                matToApply.prependMatrix(sizeLockedMat);
              }
            }
            matToApply.prependMatrix(cameraMat);
            var projMat = getProjectionMatrix(parent, totalDepth);
            if(projMat)
            {
              matToApply.prependMatrix(projMat);
            }
          }
          layerObj.transformMatrix = matToApply;
        }
      }
    }
    //Code to support hidpi screens and responsive scaling.
    function makeResponsive(isResp, respDim, isScale, scaleType) {
      var lastW, lastH, lastS=1;
      window.addEventListener('resize', resizeCanvas);
      resizeCanvas();
      function resizeCanvas() {
        var w = lib.properties.width, h = lib.properties.height;
        var iw = window.innerWidth, ih=window.innerHeight;
        var pRatio = window.devicePixelRatio || 1, xRatio=iw/w, yRatio=ih/h, sRatio=1;
        if(isResp) {
          if((respDim=='width'&&lastW==iw) || (respDim=='height'&&lastH==ih)) {
            sRatio = lastS;
          }
          else if(!isScale) {
            if(iw<w || ih<h)
              sRatio = Math.min(xRatio, yRatio);
          }
          else if(scaleType==1) {
            sRatio = Math.min(xRatio, yRatio);
          }
          else if(scaleType==2) {
            sRatio = Math.max(xRatio, yRatio);
          }
        }
        canvas.width = w*pRatio*sRatio;
        canvas.height = h*pRatio*sRatio;
        canvas.style.width = anim_container.style.width = dom_overlay_container.style.width = preloaderDiv.style.width = w*sRatio+'px';
        canvas.style.height = anim_container.style.height = dom_overlay_container.style.height = preloaderDiv.style.height = h*sRatio+'px';
        stage.scaleX = pRatio*sRatio;
        stage.scaleY = pRatio*sRatio;
        lastW = iw; lastH = ih; lastS = sRatio;
        stage.tickOnUpdate = false;
        stage.update();
        stage.tickOnUpdate = true;
      }
    }
    makeResponsive(true,'both',true,2);
    AdobeAn.compositionLoaded(lib.properties.id);
    fnStartAnimation();
  }
  init();
})