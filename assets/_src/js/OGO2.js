(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [];


// symbols:



(lib.bg = function() {
	this.initialize(img.bg);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,1080,680);


(lib.icon1 = function() {
	this.initialize(img.icon1);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,67,67);


(lib.packs = function() {
	this.initialize(img.packs);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,588,468);


(lib.shadow1 = function() {
	this.initialize(img.shadow1);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,532,200);


(lib.tablet = function() {
	this.initialize(img.tablet);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,505,179);// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.Symbol16 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.shadow1();
	this.instance.parent = this;
	this.instance.setTransform(-266,-100);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol16, new cjs.Rectangle(-266,-100,532,200), null);


(lib.Symbol15 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.icon1();
	this.instance.parent = this;
	this.instance.setTransform(-33.5,-33.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol15, new cjs.Rectangle(-33.5,-33.5,67,67), null);


(lib.Symbol13 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(11.5,1,1).p("AiDBlQCUigBzgp");
	this.shape.setTransform(0.025,0);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	// Layer_1
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(11.5,1,1).p("AhrB9QBzjBBkg4");
	this.shape_1.setTransform(2.45,2.425);

	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol13, new cjs.Rectangle(-18.9,-15.8,37.9,36.5), null);


(lib.Symbol12 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_3
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(11.5,1,1).p("ABCByQhXhqgsh5");
	this.shape.setTransform(-19.825,21.6);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	// Layer_2
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(11.5,1,1).p("AAWAYQgBgigpgN");
	this.shape_1.setTransform(7.1,-0.125);

	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(1));

	// Layer_1
	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(11.5,1,1).p("AgTAVIAngp");
	this.shape_2.setTransform(1.6,0);

	this.timeline.addTween(cjs.Tween.get(this.shape_2).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol12, new cjs.Rectangle(-32.1,-8.3,47.1,47.099999999999994), null);


(lib.Symbol11 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_3 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("AAoBpQg6BEhsA1IhnmnIHIhFIADHmIi+Avg");
	mask.setTransform(-0.175,4.9);

	// Layer_2
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(10.5,1,1).p("AhDgfQAvg1AqgGQAqgGADAcQADAcgSAjQgTAihDA/");
	this.shape.setTransform(3.8188,17.6316);

	var maskedShapeInstanceList = [this.shape];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	// Layer_1
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(11.5,1,1).p("AhHg1QCxg7grC0");
	this.shape_1.setTransform(0.0021,0.0105);

	var maskedShapeInstanceList = [this.shape_1];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol11, new cjs.Rectangle(-12.9,-12.6,28.8,44.2), null);


(lib.Symbol8 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#E41A21").s().p("Ag6A+QgYgYAAgmQAAglAYgaQAYgZAiAAQAjAAAXAZQAZAaAAAlQAAAmgZAYQgXAbgjAAQgiAAgYgbg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol8, new cjs.Rectangle(-8.2,-8.9,16.5,17.8), null);


(lib.Symbol5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.tablet();
	this.instance.parent = this;
	this.instance.setTransform(-252.5,-89.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol5, new cjs.Rectangle(-252.5,-89.5,505,179), null);


(lib.Symbol4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.icon1();
	this.instance.parent = this;
	this.instance.setTransform(-33.5,-33.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol4, new cjs.Rectangle(-33.5,-33.5,67,67), null);


(lib.Symbol_10_Layer_10 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_10
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(12,1,1).p("AiAA2QCaBBBni9");
	this.shape.setTransform(-34.575,31.6806);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(12,1,1).p("AiFAvQCuBJBdi7");
	this.shape_1.setTransform(-35.025,31.3579);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(12,1,1).p("AiJAoQDABRBTi5");
	this.shape_2.setTransform(-35.45,31.0787);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(12,1,1).p("AiNAiQDPBYBMi3");
	this.shape_3.setTransform(-35.8,30.8459);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(12,1,1).p("AiQAcQDbBfBFi3");
	this.shape_4.setTransform(-36.1,30.6562);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(12,1,1).p("AiSAYQDmBjA/i1");
	this.shape_5.setTransform(-36.375,30.494);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(12,1,1).p("AiVAUQDwBnA6i0");
	this.shape_6.setTransform(-36.6,30.3678);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(12,1,1).p("AiWARQD2BrA3i0");
	this.shape_7.setTransform(-36.75,30.276);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(12,1,1).p("AiXAPQD8BtAziz");
	this.shape_8.setTransform(-36.875,30.2055);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(12,1,1).p("AiYAOQD/BuAyiz");
	this.shape_9.setTransform(-36.95,30.1648);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(12,1,1).p("AiYANQEABvAxiz");
	this.shape_10.setTransform(-36.975,30.1585);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(12,1,1).p("AiUAVQDtBmA8i0");
	this.shape_11.setTransform(-36.525,30.4099);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(12,1,1).p("AiMAjQDMBXBNi4");
	this.shape_12.setTransform(-35.75,30.8798);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(12,1,1).p("AiGAsQC0BNBZi7");
	this.shape_13.setTransform(-35.175,31.2584);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(12,1,1).p("AiEAwQCrBIBei7");
	this.shape_14.setTransform(-34.95,31.4069);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(12,1,1).p("AiDAzQCkBFBji8");
	this.shape_15.setTransform(-34.8,31.5227);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(12,1,1).p("AiBA1QCeBCBli8");
	this.shape_16.setTransform(-34.675,31.6137);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(12,1,1).p("AiBA2QCcBBBmi9");
	this.shape_17.setTransform(-34.6,31.6646);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},35).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_10}]},40).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape}]},1).wait(5));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_10_Layer_9 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_9
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(12,1,1).p("AiZgVQAPAWAkAWQAlAVAuAFQB2ALA3ht");
	this.shape.setTransform(-7.275,3.9);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(12,1,1).p("AiZgVQAPAWAkAWQAlAWAuAEQB2ALA3ht");
	this.shape_1.setTransform(-7.275,3.5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(12,1,1).p("AiZgVQAPAXAkAVQAlAWAuAEQB2ALA3ht");
	this.shape_2.setTransform(-7.275,3.2);
	this.shape_2._off = true;

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(12,1,1).p("AiZgVQAPAWAkAVQAlAWAuAFQB2ALA3ht");
	this.shape_3.setTransform(-7.275,2.3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},35).to({state:[{t:this.shape_1,p:{y:3.5}}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_1,p:{y:2}}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},40).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).wait(5));
	this.timeline.addTween(cjs.Tween.get(this.shape).wait(35).to({_off:true},1).wait(2).to({_off:false,y:2.9},0).to({_off:true},1).wait(1).to({_off:false,y:2.4},0).to({_off:true},1).wait(3).to({_off:false,y:1.9},0).wait(41).to({_off:true},1).wait(2).to({_off:false,y:2.9},0).to({_off:true},1).wait(1).to({_off:false,y:3.4},0).to({_off:true},1).wait(3).to({_off:false,y:3.9},0).wait(6));
	this.timeline.addTween(cjs.Tween.get(this.shape_2).wait(37).to({_off:false},0).to({_off:true},1).wait(1).to({_off:false,y:2.6},0).to({_off:true},1).wait(1).to({_off:false,y:2.2},0).wait(1).to({y:2.1},0).to({_off:true},1).wait(44).to({_off:false,y:2.6},0).to({_off:true},1).wait(1).to({_off:false,y:3.2},0).to({_off:true},1).wait(1).to({_off:false,y:3.6},0).wait(1).to({y:3.7},0).wait(1).to({y:3.8},0).to({_off:true},1).wait(6));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_10_Layer_8 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_8
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(12,1,1).p("AiZgVQAPAWAkAWQAlAVAuAFQB2ALA3ht");
	this.shape.setTransform(-68.925,3.9);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(12,1,1).p("AiZgVQAPAWAkAWQAlAWAuAEQB2ALA3ht");
	this.shape_1.setTransform(-68.925,3.5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(12,1,1).p("AiZgVQAPAXAkAVQAlAWAuAEQB2ALA3ht");
	this.shape_2.setTransform(-68.925,3.2);
	this.shape_2._off = true;

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(12,1,1).p("AiZgVQAPAWAkAVQAlAWAuAFQB2ALA3ht");
	this.shape_3.setTransform(-68.925,2.3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},35).to({state:[{t:this.shape_1,p:{y:3.5}}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_1,p:{y:2}}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},40).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).wait(5));
	this.timeline.addTween(cjs.Tween.get(this.shape).wait(35).to({_off:true},1).wait(2).to({_off:false,y:2.9},0).to({_off:true},1).wait(1).to({_off:false,y:2.4},0).to({_off:true},1).wait(3).to({_off:false,y:1.9},0).wait(41).to({_off:true},1).wait(2).to({_off:false,y:2.9},0).to({_off:true},1).wait(1).to({_off:false,y:3.4},0).to({_off:true},1).wait(3).to({_off:false,y:3.9},0).wait(6));
	this.timeline.addTween(cjs.Tween.get(this.shape_2).wait(37).to({_off:false},0).to({_off:true},1).wait(1).to({_off:false,y:2.6},0).to({_off:true},1).wait(1).to({_off:false,y:2.2},0).wait(1).to({y:2.1},0).to({_off:true},1).wait(44).to({_off:false,y:2.6},0).to({_off:true},1).wait(1).to({_off:false,y:3.2},0).to({_off:true},1).wait(1).to({_off:false,y:3.6},0).wait(1).to({y:3.7},0).wait(1).to({y:3.8},0).to({_off:true},1).wait(6));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_10_Layer_7 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_7
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AhnBrQgrgsAAg/QAAg+ArgtQArgsA8AAQA9AAArAsQArAtAAA+QAAA/grAsQgrAsg9AAQg8AAgrgsg");
	this.shape.setTransform(-8.1,-10.2);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(100));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_10_Layer_5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_5
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#C7C7C7").s().p("AhjAIQApgpA8AAQA9AAAjAiQAjAjgSgNQgSgNglgQQglgRgcACQgdACgPAJQgPAJgmAZQgPAKgDAAQgEAAAZgag");
	this.shape.setTransform(-8.3306,-21.9285);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#C7C7C7").s().p("AiTA1QADgdArgrQArgsA8AAQA9AAArAsQArArgBAXQgBAXg3gxQg3gvg3AKQg3AKgnAsQgXAagIAAQgFAAABgLg");
	this.shape_1.setTransform(-8.3213,-18.9323);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#C7C7C7").s().p("ABwBLQgkgphAAHQg/AHgwAgQgxAgACg6QABg4ArgtQArgsA8AAQA9AAArAsQArAtAAA6QgBAkgMAAQgJAAgOgRg");
	this.shape_2.setTransform(-8.2036,-16.1799);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#C7C7C7").s().p("AAAB7Qg7gFgsgNQgrgOAAg/QAAg+ArgsQArgsA8AAQA9AAArAsQArAsAAA+QAAA/grASQgiAOgtAAIgZAAg");
	this.shape_3.setTransform(-8.1,-13.0028);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#C7C7C7").s().p("AhnBrQgrgsAAg/QAAg+ArgtQArgsA8AAQA9AAArAsQArAtAAA+QAAA/grAsQgrAsg9AAQg8AAgrgsg");
	this.shape_4.setTransform(-8.1,-10.2);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#C7C7C7").s().p("AABB7Qg9gFgrgNQgrgOAAg/QAAg+ArgsQArgsA8AAQA9AAArAsQArAsAAA+QAAA/grASQgiAOguAAIgXAAg");
	this.shape_5.setTransform(-8.1,-13.005);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#C7C7C7").s().p("ABvBLQgjgphAAHQg/AHgxAgQgwAgABg6QACg4ArgtQArgsA8AAQA9AAArAsQArAtAAA6QgBAkgMAAQgJAAgPgRg");
	this.shape_6.setTransform(-8.2156,-16.1805);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#C7C7C7").s().p("AiTA1QADgdArgrQArgsA8AAQA9AAArAsQArArgBAXQgBAXg3gxQg3gvg2AKQg4AKgmAsQgYAagIAAQgFAAABgLg");
	this.shape_7.setTransform(-8.3097,-18.9359);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape}]},12).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape}]},1).to({state:[]},1).to({state:[{t:this.shape}]},44).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape}]},1).to({state:[]},1).wait(24));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_10_Layer_3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_3
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AhnBrQgrgsAAg/QAAg+ArgtQArgrA8gBQA9ABArArQArAtAAA+QAAA/grAsQgrAtg9AAQg8AAgrgtg");
	this.shape.setTransform(-68.7,-10.3);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(100));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_10_Layer_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#C7C7C7").s().p("AhjAIQApgpA8AAQA9AAAjAiQAjAjgSgNQgSgNglgQQglgRgcACQgdACgPAJQgPAJgmAZQgPAKgDAAQgEAAAZgag");
	this.shape.setTransform(-68.9275,-22.0279);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#C7C7C7").s().p("AiTA1QADgdArgrQArgsA8AAQA9AAArAsQArArgBAXQgBAXg3gxQg3gvg2AKQg4AKgmAsQgYAagIAAQgFAAABgLg");
	this.shape_1.setTransform(-68.9097,-19.0359);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#C7C7C7").s().p("ABwBLQgkgphAAHQg/AHgwAgQgxAgACg6QABg4ArgtQArgsA8AAQA9AAArAsQArAtAAA6QgBAkgMAAQgJAAgOgRg");
	this.shape_2.setTransform(-68.8036,-16.2799);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#C7C7C7").s().p("AAAB7Qg7gFgrgNQgsgOAAg/QAAg+ArgsQArgsA8AAQA9AAArAsQArAsAAA+QAAA/gqASQgjAOguAAIgYAAg");
	this.shape_3.setTransform(-68.7,-13.105);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#C7C7C7").s().p("AhnBrQgrgsAAg/QAAg+ArgtQArgrA8gBQA9ABArArQArAtAAA+QAAA/grAsQgrAtg9AAQg8AAgrgtg");
	this.shape_4.setTransform(-68.7,-10.3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape}]},12).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape}]},1).to({state:[]},1).to({state:[{t:this.shape}]},44).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape}]},1).to({state:[]},1).wait(24));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_7_Layer_4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_4
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(12,1,1).p("AiQgxIEhBj");
	this.shape.setTransform(72.75,11.925);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(12,1,1).p("AiUgfIEpA/");
	this.shape_1.setTransform(73.2,10.125);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(12,1,1).p("AiXgRIEvAj");
	this.shape_2.setTransform(73.525,8.75);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(12,1,1).p("AiagHIE1AP");
	this.shape_3.setTransform(73.775,7.75);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(12,1,1).p("AibgBIE3AD");
	this.shape_4.setTransform(73.925,7.15);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(12,1,1).p("AicAAIE5AA");
	this.shape_5.setTransform(73.975,6.95);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(12,1,1).p("AiRgpIEkBT");
	this.shape_6.setTransform(72.95,11.125);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(12,1,1).p("AiQgvIEhBf");
	this.shape_7.setTransform(72.8,11.725);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},4).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_5}]},30).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape}]},1).wait(56));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_7_Layer_3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_3
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(12,1,1).p("Ah6BCID1iD");
	this.shape.setTransform(70.6,0.375);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(100));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_7_Layer_2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(12,1,1).p("Ah+A8ID9h3");
	this.shape.setTransform(1.325,11.675);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(12,1,1).p("AiHAqIEPhS");
	this.shape_1.setTransform(0.425,9.85);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(12,1,1).p("AiOAbIEdg1");
	this.shape_2.setTransform(-0.275,8.4);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(12,1,1).p("AiTARIEngh");
	this.shape_3.setTransform(-0.775,7.4);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(12,1,1).p("AiWALIEtgV");
	this.shape_4.setTransform(-1.075,6.775);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(12,1,1).p("AiXAJIEvgR");
	this.shape_5.setTransform(-1.175,6.575);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(12,1,1).p("AiCA0IEFhm");
	this.shape_6.setTransform(0.925,10.85);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(12,1,1).p("Ah/A6ID/hz");
	this.shape_7.setTransform(1.225,11.475);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},4).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_5}]},30).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape}]},1).wait(56));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_7_Layer_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(12,1,1).p("AiLg4IEXBx");
	this.shape.setTransform(0.025,0.025);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(100));

}).prototype = p = new cjs.MovieClip();


(lib.Scene_1_Layer_18 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_18
	this.instance = new lib.bg();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(100));

}).prototype = p = new cjs.MovieClip();


(lib.Scene_1_Layer_17 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_17
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(11.5,1,1).p("AHMipQjthSjKADQizACh9BOQhyBIgrB0QgqByAxB0");
	this.shape.setTransform(545.6558,345.7463);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(11.5,1,1).p("AmvD3Qgxh0AqhxQAsh0ByhGQB8hNCygBQDJgBDrBT");
	this.shape_1.setTransform(545.7504,345.0744);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(11.5,1,1).p("AmpDuQgxh1AshuQAthxBxhCQB8hJCvAEQDFAFDkBY");
	this.shape_2.setTransform(546.0841,342.8145);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(11.5,1,1).p("AmdDeQgxh2AvhoQAvhtBxg6QB6hACoANQC+AODWBi");
	this.shape_3.setTransform(546.687,338.391);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(11.5,1,1).p("AmMDGQgvh4AyhgQAyhjBxgvQB4gzCeAbQCzAfDBBx");
	this.shape_4.setTransform(547.668,331.2827);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(11.5,1,1).p("Al1CsQguh7A3hWQA3hYBwgfQB1giCSAtQCkAzClCE");
	this.shape_5.setTransform(548.8849,321.8264);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(11.5,1,1).p("AleCVQgth+A8hLQA8hNBvgPQBygSCFA/QCWBHCJCZ");
	this.shape_6.setTransform(550.0688,311.9598);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(11.5,1,1).p("AlMBnQgsh/A/hEQBAhEBvgDQBwgEB6BNQCLBXB0Co");
	this.shape_7.setTransform(551.0332,307.0901);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(11.5,1,1).p("AlABDQgsiABCg/QBCg/BvAFQBuAEB0BWQCDBgBnCz");
	this.shape_8.setTransform(551.62,304.6316);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(11.5,1,1).p("Ak6AxQgsiBBEg8QBDg8BuAJQBuAJBxBaQB/BmBgC4");
	this.shape_9.setTransform(551.9449,303.2969);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(11.5,1,1).p("AFLDEQhei5h+hnQhwhchugKQhugKhDA7QhEA7AsCB");
	this.shape_10.setTransform(552.0399,302.9004);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(11.5,1,1).p("Ak7AzQgriCBEg8QBDg8BuAJQBuAIBxBaQB/BlBgC3");
	this.shape_11.setTransform(551.9265,303.4223);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(11.5,1,1).p("AlCBMQgriCBDg/QBDg/BuADQBtADB2BSQCDBdBnCx");
	this.shape_12.setTransform(551.5846,305.1682);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(11.5,1,1).p("AlRB9QgpiBBBhFQBChFBugIQBtgIB+BEQCKBPB2Ci");
	this.shape_13.setTransform(550.9108,308.4079);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(11.5,1,1).p("AloCgQgniDBAhNQA/hOBugYQBsgZCMAuQCWA3CMCO");
	this.shape_14.setTransform(549.8734,317.7129);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(11.5,1,1).p("AmGDDQgjiDA9hZQA8haBtguQBtguCcASQClAYCpB1");
	this.shape_15.setTransform(548.5736,330.6219);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(11.5,1,1).p("AmkDwQgfiDA6hmQA5hkBthEQBshDCugLQCzgHDGBb");
	this.shape_16.setTransform(547.2321,342.475);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(11.5,1,1).p("Am7EZQgciDA3hvQA4huBshUQBshUC7ggQC/gfDcBG");
	this.shape_17.setTransform(546.1915,351.1016);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#000000").ss(11.5,1,1).p("AnKE3QgaiDA2h1QA2h0BsheQBrhfDEgvQDGguDqA5");
	this.shape_18.setTransform(545.5138,356.3278);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#000000").ss(11.5,1,1).p("AnRFHQgaiEA1h3QA1h3BshkQBshlDIg2QDKg2DyAy");
	this.shape_19.setTransform(545.1722,359.0048);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#000000").ss(11.5,1,1).p("AHdk0Qj0gxjLA5QjKA4hrBmQhsBmg1B3Qg1B5AZCE");
	this.shape_20.setTransform(545.0555,359.779);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#000000").ss(11.5,1,1).p("AnTFKQgaiEA1h4QA1h4BshlQBshlDJg3QDLg4DzAy");
	this.shape_21.setTransform(545.0694,359.4832);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#000000").ss(11.5,1,1).p("AnQFCQgciCAzh4QA0h4BthiQBthjDIgzQDLgzDzA0");
	this.shape_22.setTransform(545.1569,358.406);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#000000").ss(11.5,1,1).p("AnLEzQggh/Ayh3QAzh3BthdQBwhgDEgqQDLgqDyA5");
	this.shape_23.setTransform(545.2506,356.2591);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#000000").ss(11.5,1,1).p("AnDEgQglh8Awh1QAwh2BvhXQB0haC+gdQDLgeDwBB");
	this.shape_24.setTransform(545.4149,353.0895);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f().s("#000000").ss(11.5,1,1).p("Am6ENQgrh4AthzQAuh2BwhPQB4hVC5gRQDKgRDvBJ");
	this.shape_25.setTransform(545.5429,349.7394);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f().s("#000000").ss(11.5,1,1).p("Am0EBQgvh1AshzQAsh1BxhLQB7hRC1gHQDKgJDuBO");
	this.shape_26.setTransform(545.5988,347.3582);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f().s("#000000").ss(11.5,1,1).p("AmyD7QgwhzAqhzQArh0ByhIQB8hPC0gEQDKgDDtBQ");
	this.shape_27.setTransform(545.6576,346.0927);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f().s("#000000").ss(11.5,1,1).p("AmuD0QgziGAvhvQAwhzBwhAQB3hFCzAGQDJAHDoBc");
	this.shape_28.setTransform(545.3037,342.926);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f().s("#000000").ss(11.5,1,1).p("AmrDxQg2iXA0htQA0hwBug6QB0g9CyANQDJAODkBl");
	this.shape_29.setTransform(544.993,340.4866);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f().s("#000000").ss(11.5,1,1).p("AmpDuQg4ijA3hsQA3hvBtg0QBxg3CxATQDJAUDhBs");
	this.shape_30.setTransform(544.7562,338.4156);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f().s("#000000").ss(11.5,1,1).p("AmnDsQg6itA6hrQA6htBsgwQBugyCxAXQDIAaDfBx");
	this.shape_31.setTransform(544.5687,336.7852);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f().s("#000000").ss(11.5,1,1).p("AmmDrQg6i1A8hqQA7hsBrgtQBsguCxAaQDIAdDdB1");
	this.shape_32.setTransform(544.4496,335.6306);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f().s("#000000").ss(11.5,1,1).p("AmmDrQg7i5A+hqQA8hrBrgsQBrgrCwAbQDIAfDcB4");
	this.shape_33.setTransform(544.3556,334.8924);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f().s("#000000").ss(11.5,1,1).p("AHDhGQjbh5jIgfQixgdhqArQhrArg9BrQg9BqA7C6");
	this.shape_34.setTransform(544.3276,334.657);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f().s("#000000").ss(11.5,1,1).p("AmoDtQg5inA5hsQA4huBsgyQBwg1CxAVQDIAWDhBu");
	this.shape_35.setTransform(544.6875,337.7372);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f().s("#000000").ss(11.5,1,1).p("AmrDwQg2iYA0htQA0hwBug5QB0g8CxANQDJAPDlBl");
	this.shape_36.setTransform(544.9995,340.251);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f().s("#000000").ss(11.5,1,1).p("AmtDzQg0iLAwhuQAxhyBwg+QB2hDCyAIQDKAIDnBe");
	this.shape_37.setTransform(545.2355,342.2823);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f().s("#000000").ss(11.5,1,1).p("AmvD2QgyiBAthxQAuhyBxhCQB5hICzADQDJAEDqBZ");
	this.shape_38.setTransform(545.4217,343.8169);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f().s("#000000").ss(11.5,1,1).p("AmvD4Qgyh5ArhyQAthzBxhGQB7hLCzAAQDKABDrBU");
	this.shape_39.setTransform(545.5381,344.8748);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f().s("#000000").ss(11.5,1,1).p("AmwD5Qgxh1AqhyQArh0ByhHQB8hOCzgBQDKgCDtBT");
	this.shape_40.setTransform(545.6308,345.5476);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},19).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_10}]},5).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_20}]},5).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_27}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},10).to({state:[{t:this.shape_28}]},1).to({state:[{t:this.shape_29}]},1).to({state:[{t:this.shape_30}]},1).to({state:[{t:this.shape_31}]},1).to({state:[{t:this.shape_32}]},1).to({state:[{t:this.shape_33}]},1).to({state:[{t:this.shape_34}]},1).to({state:[{t:this.shape_35}]},1).to({state:[{t:this.shape_36}]},1).to({state:[{t:this.shape_37}]},1).to({state:[{t:this.shape_38}]},1).to({state:[{t:this.shape_39}]},1).to({state:[{t:this.shape_40}]},1).to({state:[{t:this.shape}]},1).wait(19));
	this.timeline.addTween(cjs.Tween.get(this.shape).wait(19).to({_off:true},1).wait(37).to({_off:false},0).wait(10).to({_off:true},1).wait(13).to({_off:false},0).wait(19));

}).prototype = p = new cjs.MovieClip();


(lib.Scene_1_Layer_9 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_9
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(11.5,1,1).p("AggmTQgOFpBRG9");
	this.shape.setTransform(745.8728,368.35);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(11.5,1,1).p("Ag8mNQALFnBuG0");
	this.shape_1.setTransform(749.05,367.3);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(11.5,1,1).p("AhTmIQAgFlCHGs");
	this.shape_2.setTransform(751.675,366.4);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(11.5,1,1).p("AhmmDQAxFjCcGl");
	this.shape_3.setTransform(753.825,365.65);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(11.5,1,1).p("Ah1mAQA/FiCsGf");
	this.shape_4.setTransform(755.475,365.075);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(11.5,1,1).p("Ah/l+QBIFhC3Gc");
	this.shape_5.setTransform(756.675,364.675);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(11.5,1,1).p("AiGl9QBOFgC/Ga");
	this.shape_6.setTransform(757.375,364.45);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(11.5,1,1).p("AiIl8QBQFgDBGZ");
	this.shape_7.setTransform(757.625,364.35);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(11.5,1,1).p("AhsmCQA2FiCkGj");
	this.shape_8.setTransform(754.55,365.4);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(11.5,1,1).p("AhVmHQAhFkCKGr");
	this.shape_9.setTransform(751.925,366.3);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(11.5,1,1).p("AhCmLQAQFmB1Gx");
	this.shape_10.setTransform(749.775,367.05);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(11.5,1,1).p("Ag0mOQADFmBmG3");
	this.shape_11.setTransform(748.125,367.625);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(11.5,1,1).p("AgpmRQgHFoBaG7");
	this.shape_12.setTransform(746.9033,368.025);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(11.5,1,1).p("AgimSQgMFoBTG9");
	this.shape_13.setTransform(746.144,368.25);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},19).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},5).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},5).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},10).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape}]},1).wait(19));
	this.timeline.addTween(cjs.Tween.get(this.shape).wait(20).to({x:745.8724},0).wait(9).to({x:745.8728},0).wait(6).to({x:745.8724},0).wait(9).to({x:745.8728},0).wait(6).to({x:745.8724},0).wait(7).to({x:745.8728},0).wait(10).to({_off:true},1).wait(13).to({_off:false},0).wait(19));

}).prototype = p = new cjs.MovieClip();


(lib.Scene_1_Layer_8 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_8
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(11.5,1,1).p("AklnyQAVFgI2KF");
	this.shape.setTransform(969.125,358.025);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(11.5,1,1).p("AklnyQAZFiIyKD");
	this.shape_1.setTransform(969.125,358.025);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(11.5,1,1).p("AklnyQAnFqIkJ7");
	this.shape_2.setTransform(969.125,358.025);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(11.5,1,1).p("AklnyQBBF4IKJt");
	this.shape_3.setTransform(969.125,358.025);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(11.5,1,1).p("AklnyQBqGOHhJX");
	this.shape_4.setTransform(969.125,358.025);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(11.5,1,1).p("AklnyQCeGqGtI7");
	this.shape_5.setTransform(969.125,358.025);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(11.5,1,1).p("AklnyQDSHGF5If");
	this.shape_6.setTransform(969.125,358.025);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(11.5,1,1).p("AklnyQD7HcFQIJ");
	this.shape_7.setTransform(969.125,358.025);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(11.5,1,1).p("AklnyQEVHqE2H7");
	this.shape_8.setTransform(969.125,358.025);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(11.5,1,1).p("AklnyQEiHxEpH0");
	this.shape_9.setTransform(969.125,358.025);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(11.5,1,1).p("AklnyIJLPl");
	this.shape_10.setTransform(969.125,358.025);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(11.5,1,1).p("AkknzQEhHxEoH2");
	this.shape_11.setTransform(969,358.1);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(11.5,1,1).p("Akfn1QERHqEuIB");
	this.shape_12.setTransform(968.5,358.35);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(11.5,1,1).p("AkWn5QDwHcE+IX");
	this.shape_13.setTransform(967.55,358.85);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(11.5,1,1).p("AkJn/QC/HFFUI6");
	this.shape_14.setTransform(966.075,359.625);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(11.5,1,1).p("Aj5oHQCAGpFyJm");
	this.shape_15.setTransform(964.2,360.625);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(11.5,1,1).p("AjooQQBBGNGQKT");
	this.shape_16.setTransform(962.275,361.6);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(11.5,1,1).p("AjaoWQAPF3GnK2");
	this.shape_17.setTransform(960.8,362.375);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#000000").ss(11.5,1,1).p("AjSoaQgRFpG2LM");
	this.shape_18.setTransform(959.8183,362.875);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#000000").ss(11.5,1,1).p("AjMocQgiFhG+LY");
	this.shape_19.setTransform(959.2331,363.125);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#000000").ss(11.5,1,1).p("AjLodQgmFfHALb");
	this.shape_20.setTransform(959.073,363.2);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#000000").ss(11.5,1,1).p("AjNocQglFfHDLZ");
	this.shape_21.setTransform(959.31,363.1);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#000000").ss(11.5,1,1).p("AjVoYQggFfHNLS");
	this.shape_22.setTransform(960.1713,362.65);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#000000").ss(11.5,1,1).p("AjjoRQgXFfHfLE");
	this.shape_23.setTransform(961.8225,361.825);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#000000").ss(11.5,1,1).p("Aj4oHQgJFfH7Kw");
	this.shape_24.setTransform(964.1922,360.625);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f().s("#000000").ss(11.5,1,1).p("AkNn9QAFFgIWKc");
	this.shape_25.setTransform(966.475,359.4);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f().s("#000000").ss(11.5,1,1).p("Akbn3QAOFhIpKO");
	this.shape_26.setTransform(968.075,358.575);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f().s("#000000").ss(11.5,1,1).p("AkinzQATFgIyKH");
	this.shape_27.setTransform(968.9,358.125);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f().s("#000000").ss(11.5,1,1).p("Ak2nnQAhFfJMJw");
	this.shape_28.setTransform(971.05,356.65);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f().s("#000000").ss(11.5,1,1).p("AlEndQAsFeJdJd");
	this.shape_29.setTransform(972.675,355.45);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f().s("#000000").ss(11.5,1,1).p("AlQnUQA0FcJtJN");
	this.shape_30.setTransform(974.025,354.5);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f().s("#000000").ss(11.5,1,1).p("AlZnOQA7FbJ5JC");
	this.shape_31.setTransform(975.05,353.75);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f().s("#000000").ss(11.5,1,1).p("AlgnKQBAFbKBI6");
	this.shape_32.setTransform(975.8,353.225);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f().s("#000000").ss(11.5,1,1).p("AlknHQBDFbKGI0");
	this.shape_33.setTransform(976.225,352.9);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f().s("#000000").ss(11.5,1,1).p("AllnGQBEFaKHIz");
	this.shape_34.setTransform(976.375,352.8);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f().s("#000000").ss(11.5,1,1).p("AlUnSQA3FcJyJJ");
	this.shape_35.setTransform(974.45,354.175);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f().s("#000000").ss(11.5,1,1).p("AlGncQAtFdJgJc");
	this.shape_36.setTransform(972.825,355.375);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f().s("#000000").ss(11.5,1,1).p("Ak6nkQAlFfJQJq");
	this.shape_37.setTransform(971.475,356.325);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f().s("#000000").ss(11.5,1,1).p("AkxnqQAeFfJEJ2");
	this.shape_38.setTransform(970.45,357.075);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f().s("#000000").ss(11.5,1,1).p("AkqnvQAZFgI8J+");
	this.shape_39.setTransform(969.7,357.6);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f().s("#000000").ss(11.5,1,1).p("AkmnxQAWFgI3KD");
	this.shape_40.setTransform(969.275,357.925);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},19).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_10}]},5).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_20}]},5).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_27}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},10).to({state:[{t:this.shape_28}]},1).to({state:[{t:this.shape_29}]},1).to({state:[{t:this.shape_30}]},1).to({state:[{t:this.shape_31}]},1).to({state:[{t:this.shape_32}]},1).to({state:[{t:this.shape_33}]},1).to({state:[{t:this.shape_34}]},1).to({state:[{t:this.shape_35}]},1).to({state:[{t:this.shape_36}]},1).to({state:[{t:this.shape_37}]},1).to({state:[{t:this.shape_38}]},1).to({state:[{t:this.shape_39}]},1).to({state:[{t:this.shape_40}]},1).to({state:[{t:this.shape}]},1).wait(19));
	this.timeline.addTween(cjs.Tween.get(this.shape).wait(19).to({_off:true},1).wait(37).to({_off:false},0).wait(10).to({_off:true},1).wait(13).to({_off:false},0).wait(19));

}).prototype = p = new cjs.MovieClip();


(lib.Scene_1_Layer_3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_3
	this.instance = new lib.packs();
	this.instance.parent = this;
	this.instance.setTransform(493,153);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(100));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol9 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AgpArQgRgRAAgaQAAgZARgRQARgTAYABQAYgBASATQARARAAAZQAAAagRARQgSATgYgBQgYABgRgTg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol9, new cjs.Rectangle(-5.9,-6.1,11.9,12.3), null);


(lib.Symbol14 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Symbol12();
	this.instance.parent = this;
	this.instance.setTransform(-234.5,-62.2);

	this.instance_1 = new lib.Symbol11();
	this.instance_1.parent = this;
	this.instance_1.setTransform(250.35,-15.45);

	this.instance_2 = new lib.Symbol5();
	this.instance_2.parent = this;
	this.instance_2.setTransform(3.55,0);

	this.instance_3 = new lib.Symbol13();
	this.instance_3.parent = this;
	this.instance_3.setTransform(-244.45,-54.45);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_3},{t:this.instance_2},{t:this.instance_1},{t:this.instance}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol14, new cjs.Rectangle(-266.6,-89.5,539.8,179), null);


(lib.Symbol9_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_3
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(12,1,1).p("AiGg0QBRCQC8g2");
	this.shape_1.setTransform(-0.5,-1.9905);

	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(40));

	// Symbol_8
	this.instance = new lib.Symbol8();
	this.instance.parent = this;
	this.instance.setTransform(5.75,3.1,1,1,0,0,0,0,3.1);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1).to({regY:0,rotation:0.1572,x:5.697,y:0.0301},0).wait(1).to({rotation:0.6593,x:5.5318,y:0.0451},0).wait(1).to({rotation:1.558,x:5.2486,y:0.0685},0).wait(1).to({rotation:2.9132,x:4.8268,y:0.0976},0).wait(1).to({rotation:4.7911,x:4.244,y:0.1258},0).wait(1).to({rotation:7.2587,x:3.4758,y:0.1405},0).wait(1).to({rotation:10.3678,x:2.4994,y:0.1196},0).wait(1).to({rotation:14.1255,x:1.3071,y:0.0284},0).wait(1).to({rotation:18.448,x:-0.0764,y:-0.1784},0).wait(1).to({rotation:23.1203,x:-1.5815,y:-0.5436},0).wait(1).to({rotation:27.8107,x:-3.0864,y:-1.0777},0).wait(1).to({rotation:32.1678,x:-4.4596,y:-1.7376},0).wait(1).to({rotation:35.9357,x:-5.6076,y:-2.437},0).wait(1).to({rotation:38.9992,x:-6.499,y:-3.0876},0).wait(1).to({rotation:41.3542,x:-7.151,y:-3.6309},0).wait(1).to({rotation:43.0558,x:-7.6012,y:-4.043},0).wait(1).to({rotation:44.1803,x:-7.8882,y:-4.3226},0).wait(1).to({rotation:44.805,x:-8.0438,y:-4.4801},0).wait(1).to({regX:-0.1,regY:3.1,rotation:44.9994,x:-10.3,y:-2.35},0).wait(1).to({regX:0,regY:0,rotation:44.8578,x:-8.0244,y:-4.4613},0).wait(1).to({rotation:44.4072,x:-7.9243,y:-4.3598},0).wait(1).to({rotation:43.6037,x:-7.7458,y:-4.1829},0).wait(1).to({rotation:42.3969,x:-7.4728,y:-3.9228},0).wait(1).to({rotation:40.7305,x:-7.0845,y:-3.5731},0).wait(1).to({rotation:38.5465,x:-6.5536,y:-3.1311},0).wait(1).to({rotation:35.7946,x:-5.8472,y:-2.6026},0).wait(1).to({rotation:32.4521,x:-4.9325,y:-2.0094},0).wait(1).to({rotation:28.5559,x:-3.7899,y:-1.396},0).wait(1).to({rotation:24.2402,x:-2.4357,y:-0.8278},0).wait(1).to({rotation:19.7486,x:-0.9449,y:-0.3732},0).wait(1).to({rotation:15.3884,x:0.5553,y:-0.0712},0).wait(1).to({rotation:11.437,x:1.9331,y:0.0866},0).wait(1).to({rotation:8.0688,x:3.1019,y:0.1409},0).wait(1).to({rotation:5.3465,x:4.0325,y:0.1368},0).wait(1).to({rotation:3.2549,x:4.7352,y:0.1079},0).wait(1).to({rotation:1.7401,x:5.2362,y:0.0747},0).wait(1).to({rotation:0.7353,x:5.5643,y:0.0473},0).wait(1).to({rotation:0.175,x:5.7457,y:0.0304},0).wait(1).to({regY:3.1,rotation:0,x:5.75,y:3.1},0).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-20.2,-16.6,39.2,25.900000000000002);


(lib.Symbol1copy5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Symbol4();
	this.instance.parent = this;
	this.instance.setTransform(0,0,0.9999,0.9999,-19.0341);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({scaleX:0.4995,scaleY:0.4995,rotation:-19.03},4,cjs.Ease.get(0.5)).to({scaleX:0.5994,scaleY:0.5994,rotation:-19.0291},10,cjs.Ease.get(1)).wait(15).to({scaleX:1.0988,scaleY:1.0988,rotation:-19.029},4,cjs.Ease.get(0.5)).to({scaleX:0.9999,scaleY:0.9999,rotation:-19.0341},10,cjs.Ease.get(1)).wait(17));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-46.8,-46.8,93.6,93.6);


(lib.Symbol1copy4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Symbol4();
	this.instance.parent = this;
	this.instance.setTransform(0,0,0.9999,0.9999,-42.2156);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({scaleX:0.4992,scaleY:0.4992,rotation:-42.138},4,cjs.Ease.get(0.5)).to({scaleX:0.599,scaleY:0.599,rotation:-42.1389},10,cjs.Ease.get(1)).wait(15).to({scaleX:1.0982,scaleY:1.0982,rotation:-42.1385},4,cjs.Ease.get(0.5)).to({scaleX:0.9999,scaleY:0.9999,rotation:-42.2156},10,cjs.Ease.get(1)).wait(17));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-52,-52,104,104);


(lib.Symbol1copy3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Symbol4();
	this.instance.parent = this;
	this.instance.setTransform(0,0,1,1,-2.2047);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({scaleX:0.4999,scaleY:0.4999,rotation:-2.0518},4,cjs.Ease.get(0.5)).to({scaleX:0.5999,scaleY:0.5999,rotation:-2.0524},10,cjs.Ease.get(1)).wait(15).to({scaleX:1.0998,scaleY:1.0998,rotation:-2.0529},4,cjs.Ease.get(0.5)).to({scaleX:1,scaleY:1,rotation:-2.2047},10,cjs.Ease.get(1)).wait(17));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-38.1,-38.1,76.2,76.2);


(lib.Symbol1copy2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Symbol4();
	this.instance.parent = this;
	this.instance.setTransform(0,0,0.9999,0.9999,-20.4432);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({scaleX:0.4995,scaleY:0.4995,rotation:-20.3206},4,cjs.Ease.get(0.5)).to({scaleX:0.5993,scaleY:0.5993,rotation:-20.322},10,cjs.Ease.get(1)).wait(15).to({scaleX:1.0988,scaleY:1.0988,rotation:-20.3214},4,cjs.Ease.get(0.5)).to({scaleX:0.9999,scaleY:0.9999,rotation:-20.4432},10,cjs.Ease.get(1)).wait(17));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-47.3,-47.3,94.6,94.6);


(lib.Symbol1copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Symbol4();
	this.instance.parent = this;
	this.instance.setTransform(0,0,1,1,-30.9999);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({scaleX:0.4993,scaleY:0.4993,rotation:-31.0499},4,cjs.Ease.get(0.5)).to({scaleX:0.5991,scaleY:0.5991,rotation:-31.0491},10,cjs.Ease.get(1)).wait(15).to({scaleX:1.0984,scaleY:1.0984,rotation:-31.0495},4,cjs.Ease.get(0.5)).to({scaleX:0.9985,scaleY:0.9985},10,cjs.Ease.get(1)).wait(17));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-50.5,-50.5,101,101);


(lib.Symbol1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Symbol15();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).to({scaleX:0.5,scaleY:0.5},4,cjs.Ease.get(0.5)).to({scaleX:0.6,scaleY:0.6},10,cjs.Ease.get(1)).wait(15).to({scaleX:1.1,scaleY:1.1},4,cjs.Ease.get(0.5)).to({scaleX:1,scaleY:1},10,cjs.Ease.get(1)).wait(17));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-36.8,-36.8,73.69999999999999,73.69999999999999);


(lib.Symbol_10_Symbol_9_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Symbol_9
	this.instance = new lib.Symbol9();
	this.instance.parent = this;
	this.instance.setTransform(-11.9,-14.15);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(4).to({x:-16.4,y:-9.65},6,cjs.Ease.get(1)).wait(26).to({x:-7.4,y:-14.15},6,cjs.Ease.get(1)).wait(44).to({x:-11.9},6,cjs.Ease.get(1)).wait(8));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_10_Symbol_9 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Symbol_9
	this.instance = new lib.Symbol9();
	this.instance.parent = this;
	this.instance.setTransform(-72.5,-14.25);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(4).to({x:-77,y:-9.75},6,cjs.Ease.get(1)).wait(26).to({x:-68,y:-14.25},6,cjs.Ease.get(1)).wait(44).to({x:-72.5},6,cjs.Ease.get(1)).wait(8));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_7_Layer_6 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_6
	this.instance = new lib.Symbol9_1();
	this.instance.parent = this;
	this.instance.setTransform(32.8,39.55,1,1,0,0,0,-0.5,-2);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(100));

}).prototype = p = new cjs.MovieClip();


(lib.Scene_1_Layer_20 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_20
	this.instance = new lib.Symbol16();
	this.instance.parent = this;
	this.instance.setTransform(749,437);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(20).to({rotation:0.1094,x:749.1058,y:436.9431},0).wait(1).to({rotation:0.4827,x:749.4667,y:436.7488},0).wait(1).to({rotation:1.1978,x:750.158,y:436.3768},0).wait(1).to({rotation:2.3118,x:751.2351,y:435.7972},0).wait(1).to({rotation:3.7425,x:752.6183,y:435.0528},0).wait(1).to({rotation:5.1731,x:754.0014,y:434.3085},0).wait(1).to({rotation:6.2871,x:755.0785,y:433.7289},0).wait(1).to({rotation:7.0022,x:755.7698,y:433.3568},0).wait(1).to({rotation:7.3755,x:756.1307,y:433.1626},0).wait(1).to({regX:0.1,regY:0.1,rotation:7.4849,x:756.3,y:433.1},0).wait(6).to({regX:0,regY:0,rotation:7.3542,x:756.0772,y:432.9422},0).wait(1).to({rotation:6.9081,x:755.6582,y:432.7451},0).wait(1).to({rotation:6.0538,x:754.8557,y:432.3675},0).wait(1).to({rotation:4.7226,x:753.6054,y:431.7793},0).wait(1).to({rotation:3.0132,x:751.9999,y:431.024},0).wait(1).to({rotation:1.3038,x:750.3943,y:430.2689},0).wait(1).to({rotation:-0.0273,x:749.1441,y:429.6809},0).wait(1).to({rotation:-0.8816,x:748.3417,y:429.3035},0).wait(1).to({rotation:-1.3277,x:747.9228,y:429.1065},0).wait(1).to({regX:0.1,regY:0.1,rotation:-1.4584,x:747.85,y:429.15},0).wait(6).to({regX:0,regY:0,rotation:-1.4243,x:747.7769,y:429.2337},0).wait(1).to({rotation:-1.304,x:747.8718,y:429.8813},0).wait(1).to({rotation:-1.0694,x:748.0567,y:431.1438},0).wait(1).to({rotation:-0.7292,x:748.325,y:432.975},0).wait(1).to({rotation:-0.389,x:748.5933,y:434.8062},0).wait(1).to({rotation:-0.1544,x:748.7782,y:436.0687},0).wait(1).to({rotation:-0.0341,x:748.8731,y:436.7163},0).wait(1).to({rotation:0,x:749,y:437},0).wait(10).to({y:425},7,cjs.Ease.get(1)).to({y:437},7,cjs.Ease.get(1)).wait(19));

}).prototype = p = new cjs.MovieClip();


(lib.Scene_1_Layer_14 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_14
	this.instance = new lib.Symbol14();
	this.instance.parent = this;
	this.instance.setTransform(766.25,422.5,1,1,0,0,0,3.3,0);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(20).to({regX:-0.2,rotation:0.1823,x:762.75,y:422.3},0).wait(1).to({rotation:0.804,y:421.65},0).wait(1).to({rotation:1.9948,y:420.45},0).wait(1).to({rotation:3.8501,y:418.55},0).wait(1).to({rotation:6.2327,x:762.8,y:416.15},0).wait(1).to({rotation:8.6153,y:413.65},0).wait(1).to({rotation:10.4707,x:762.85,y:411.75},0).wait(1).to({rotation:11.6615,y:410.55},0).wait(1).to({rotation:12.2832,y:409.9},0).wait(1).to({regX:3.3,rotation:12.4655,x:766.25,y:410.5},0).wait(6).to({regX:-0.2,rotation:12.2034,x:762.8,y:409.9},0).wait(1).to({rotation:11.3095,y:410.45},0).wait(1).to({rotation:9.5971,y:411.5},0).wait(1).to({rotation:6.9294,y:413.2},0).wait(1).to({rotation:3.5034,y:415.3},0).wait(1).to({rotation:0.0775,y:417.45},0).wait(1).to({rotation:-2.5902,y:419.1},0).wait(1).to({rotation:-4.3026,y:420.15},0).wait(1).to({rotation:-5.1965,x:762.85,y:420.7},0).wait(1).to({regX:3.4,regY:0.1,rotation:-5.4586,x:766.35,y:420.65},0).wait(6).to({regX:-0.2,regY:0,rotation:-5.3308,x:762.7,y:420.85},0).wait(1).to({rotation:-4.8805,y:421},0).wait(1).to({rotation:-4.0026,y:421.25},0).wait(1).to({rotation:-2.7293,x:762.65,y:421.65},0).wait(1).to({rotation:-1.4559,x:762.6,y:422},0).wait(1).to({rotation:-0.578,y:422.25},0).wait(1).to({rotation:-0.1277,y:422.4},0).wait(1).to({regX:3.3,rotation:0,x:766.25,y:422.5},0).wait(10).to({y:410.5},7,cjs.Ease.get(1)).to({y:422.5},7,cjs.Ease.get(1)).wait(19));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol10 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_99 = function() {
		this.___loopingOver___ = true;
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(99).call(this.frame_99).wait(1));

	// Layer_10_obj_
	this.Layer_10 = new lib.Symbol_10_Layer_10();
	this.Layer_10.name = "Layer_10";
	this.Layer_10.parent = this;
	this.Layer_10.setTransform(-34.6,31.7,1,1,0,0,0,-34.6,31.7);
	this.Layer_10.depth = 0;
	this.Layer_10.isAttachedToCamera = 0
	this.Layer_10.isAttachedToMask = 0
	this.Layer_10.layerDepth = 0
	this.Layer_10.layerIndex = 0
	this.Layer_10.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_10).wait(100));

	// Layer_9_obj_
	this.Layer_9 = new lib.Symbol_10_Layer_9();
	this.Layer_9.name = "Layer_9";
	this.Layer_9.parent = this;
	this.Layer_9.setTransform(-7.3,3.9,1,1,0,0,0,-7.3,3.9);
	this.Layer_9.depth = 0;
	this.Layer_9.isAttachedToCamera = 0
	this.Layer_9.isAttachedToMask = 0
	this.Layer_9.layerDepth = 0
	this.Layer_9.layerIndex = 1
	this.Layer_9.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_9).wait(100));

	// Layer_8_obj_
	this.Layer_8 = new lib.Symbol_10_Layer_8();
	this.Layer_8.name = "Layer_8";
	this.Layer_8.parent = this;
	this.Layer_8.setTransform(-69,3.9,1,1,0,0,0,-69,3.9);
	this.Layer_8.depth = 0;
	this.Layer_8.isAttachedToCamera = 0
	this.Layer_8.isAttachedToMask = 0
	this.Layer_8.layerDepth = 0
	this.Layer_8.layerIndex = 2
	this.Layer_8.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_8).wait(100));

	// Layer_1_obj_
	this.Layer_1 = new lib.Symbol_10_Layer_1();
	this.Layer_1.name = "Layer_1";
	this.Layer_1.parent = this;
	this.Layer_1.depth = 0;
	this.Layer_1.isAttachedToCamera = 0
	this.Layer_1.isAttachedToMask = 0
	this.Layer_1.layerDepth = 0
	this.Layer_1.layerIndex = 3
	this.Layer_1.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_1).wait(100));

	// Symbol_9_obj_
	this.Symbol_9 = new lib.Symbol_10_Symbol_9();
	this.Symbol_9.name = "Symbol_9";
	this.Symbol_9.parent = this;
	this.Symbol_9.setTransform(-72.5,-14.3,1,1,0,0,0,-72.5,-14.3);
	this.Symbol_9.depth = 0;
	this.Symbol_9.isAttachedToCamera = 0
	this.Symbol_9.isAttachedToMask = 0
	this.Symbol_9.layerDepth = 0
	this.Symbol_9.layerIndex = 4
	this.Symbol_9.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Symbol_9).wait(100));

	// Layer_3_obj_
	this.Layer_3 = new lib.Symbol_10_Layer_3();
	this.Layer_3.name = "Layer_3";
	this.Layer_3.parent = this;
	this.Layer_3.setTransform(-68.7,-10.3,1,1,0,0,0,-68.7,-10.3);
	this.Layer_3.depth = 0;
	this.Layer_3.isAttachedToCamera = 0
	this.Layer_3.isAttachedToMask = 0
	this.Layer_3.layerDepth = 0
	this.Layer_3.layerIndex = 5
	this.Layer_3.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_3).wait(100));

	// Layer_5_obj_
	this.Layer_5 = new lib.Symbol_10_Layer_5();
	this.Layer_5.name = "Layer_5";
	this.Layer_5.parent = this;
	this.Layer_5.depth = 0;
	this.Layer_5.isAttachedToCamera = 0
	this.Layer_5.isAttachedToMask = 0
	this.Layer_5.layerDepth = 0
	this.Layer_5.layerIndex = 6
	this.Layer_5.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_5).wait(100));

	// Symbol_9_obj_
	this.Symbol_9_1 = new lib.Symbol_10_Symbol_9_1();
	this.Symbol_9_1.name = "Symbol_9_1";
	this.Symbol_9_1.parent = this;
	this.Symbol_9_1.setTransform(-11.9,-14.2,1,1,0,0,0,-11.9,-14.2);
	this.Symbol_9_1.depth = 0;
	this.Symbol_9_1.isAttachedToCamera = 0
	this.Symbol_9_1.isAttachedToMask = 0
	this.Symbol_9_1.layerDepth = 0
	this.Symbol_9_1.layerIndex = 7
	this.Symbol_9_1.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Symbol_9_1).wait(100));

	// Layer_7_obj_
	this.Layer_7 = new lib.Symbol_10_Layer_7();
	this.Layer_7.name = "Layer_7";
	this.Layer_7.parent = this;
	this.Layer_7.setTransform(-8.1,-10.2,1,1,0,0,0,-8.1,-10.2);
	this.Layer_7.depth = 0;
	this.Layer_7.isAttachedToCamera = 0
	this.Layer_7.isAttachedToMask = 0
	this.Layer_7.layerDepth = 0
	this.Layer_7.layerIndex = 8
	this.Layer_7.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_7).wait(100));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-90.3,-25.4,104.5,70.19999999999999);


(lib.Symbol2copy5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Symbol1copy5("synched",0,false);
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1).to({y:0.0133,startPosition:1},0).wait(1).to({y:0.0548,startPosition:2},0).wait(1).to({y:0.1271,startPosition:3},0).wait(1).to({y:0.2334,startPosition:4},0).wait(1).to({y:0.3767,startPosition:5},0).wait(1).to({y:0.5607,startPosition:6},0).wait(1).to({y:0.7893,startPosition:7},0).wait(1).to({y:1.0661,startPosition:8},0).wait(1).to({y:1.3949,startPosition:9},0).wait(1).to({y:1.7786,startPosition:10},0).wait(1).to({y:2.2187,startPosition:11},0).wait(1).to({y:2.7141,startPosition:12},0).wait(1).to({y:3.2603,startPosition:13},0).wait(1).to({y:3.8474,startPosition:14},0).wait(1).to({y:4.4607,startPosition:15},0).wait(1).to({y:5.0811,startPosition:16},0).wait(1).to({y:5.6878,startPosition:17},0).wait(1).to({y:6.2617,startPosition:18},0).wait(1).to({y:6.788,startPosition:19},0).wait(1).to({y:7.2573,startPosition:20},0).wait(1).to({y:7.6653,startPosition:21},0).wait(1).to({y:8.0116,startPosition:22},0).wait(1).to({y:8.2985,startPosition:23},0).wait(1).to({y:8.5296,startPosition:24},0).wait(1).to({y:8.7093,startPosition:25},0).wait(1).to({y:8.8421,startPosition:26},0).wait(1).to({y:8.9322,startPosition:27},0).wait(1).to({y:8.9836,startPosition:28},0).wait(1).to({y:9,startPosition:29},0).wait(1).to({y:8.9876,startPosition:30},0).wait(1).to({y:8.9489,startPosition:31},0).wait(1).to({y:8.8816,startPosition:32},0).wait(1).to({y:8.7829,startPosition:33},0).wait(1).to({y:8.6499,startPosition:34},0).wait(1).to({y:8.4795,startPosition:35},0).wait(1).to({y:8.2682,startPosition:36},0).wait(1).to({y:8.0127,startPosition:37},0).wait(1).to({y:7.7094,startPosition:38},0).wait(1).to({y:7.3556,startPosition:39},0).wait(1).to({y:6.9493,startPosition:40},0).wait(1).to({y:6.4905,startPosition:41},0).wait(1).to({y:5.9821,startPosition:42},0).wait(1).to({y:5.4309,startPosition:43},0).wait(1).to({y:4.8481,startPosition:44},0).wait(1).to({y:4.2494,startPosition:45},0).wait(1).to({y:3.6531,startPosition:46},0).wait(1).to({y:3.0777,startPosition:47},0).wait(1).to({y:2.5391,startPosition:48},0).wait(1).to({y:2.0489,startPosition:49},0).wait(1).to({y:1.6138,startPosition:50},0).wait(1).to({y:1.2364,startPosition:51},0).wait(1).to({y:0.9163,startPosition:52},0).wait(1).to({y:0.651,startPosition:53},0).wait(1).to({y:0.437,startPosition:54},0).wait(1).to({y:0.2704,startPosition:55},0).wait(1).to({y:0.1471,startPosition:56},0).wait(1).to({y:0.0632,startPosition:57},0).wait(1).to({y:0.0153,startPosition:58},0).wait(1).to({y:0,startPosition:59},0).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-46.8,-42.5,93.6,98.2);


(lib.Symbol2copy4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Symbol1copy4("synched",0,false);
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1).to({y:0.0133,startPosition:1},0).wait(1).to({y:0.0548,startPosition:2},0).wait(1).to({y:0.1271,startPosition:3},0).wait(1).to({y:0.2334,startPosition:4},0).wait(1).to({y:0.3767,startPosition:5},0).wait(1).to({y:0.5607,startPosition:6},0).wait(1).to({y:0.7893,startPosition:7},0).wait(1).to({y:1.0661,startPosition:8},0).wait(1).to({y:1.3949,startPosition:9},0).wait(1).to({y:1.7786,startPosition:10},0).wait(1).to({y:2.2187,startPosition:11},0).wait(1).to({y:2.7141,startPosition:12},0).wait(1).to({y:3.2603,startPosition:13},0).wait(1).to({y:3.8474,startPosition:14},0).wait(1).to({y:4.4607,startPosition:15},0).wait(1).to({y:5.0811,startPosition:16},0).wait(1).to({y:5.6878,startPosition:17},0).wait(1).to({y:6.2617,startPosition:18},0).wait(1).to({y:6.788,startPosition:19},0).wait(1).to({y:7.2573,startPosition:20},0).wait(1).to({y:7.6653,startPosition:21},0).wait(1).to({y:8.0116,startPosition:22},0).wait(1).to({y:8.2985,startPosition:23},0).wait(1).to({y:8.5296,startPosition:24},0).wait(1).to({y:8.7093,startPosition:25},0).wait(1).to({y:8.8421,startPosition:26},0).wait(1).to({y:8.9322,startPosition:27},0).wait(1).to({y:8.9836,startPosition:28},0).wait(1).to({y:9,startPosition:29},0).wait(1).to({y:8.9876,startPosition:30},0).wait(1).to({y:8.9489,startPosition:31},0).wait(1).to({y:8.8816,startPosition:32},0).wait(1).to({y:8.7829,startPosition:33},0).wait(1).to({y:8.6499,startPosition:34},0).wait(1).to({y:8.4795,startPosition:35},0).wait(1).to({y:8.2682,startPosition:36},0).wait(1).to({y:8.0127,startPosition:37},0).wait(1).to({y:7.7094,startPosition:38},0).wait(1).to({y:7.3556,startPosition:39},0).wait(1).to({y:6.9493,startPosition:40},0).wait(1).to({y:6.4905,startPosition:41},0).wait(1).to({y:5.9821,startPosition:42},0).wait(1).to({y:5.4309,startPosition:43},0).wait(1).to({y:4.8481,startPosition:44},0).wait(1).to({y:4.2494,startPosition:45},0).wait(1).to({y:3.6531,startPosition:46},0).wait(1).to({y:3.0777,startPosition:47},0).wait(1).to({y:2.5391,startPosition:48},0).wait(1).to({y:2.0489,startPosition:49},0).wait(1).to({y:1.6138,startPosition:50},0).wait(1).to({y:1.2364,startPosition:51},0).wait(1).to({y:0.9163,startPosition:52},0).wait(1).to({y:0.651,startPosition:53},0).wait(1).to({y:0.437,startPosition:54},0).wait(1).to({y:0.2704,startPosition:55},0).wait(1).to({y:0.1471,startPosition:56},0).wait(1).to({y:0.0632,startPosition:57},0).wait(1).to({y:0.0153,startPosition:58},0).wait(1).to({y:0,startPosition:59},0).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-51.9,-47.3,103.9,108.1);


(lib.Symbol2copy3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Symbol1copy3("synched",0,false);
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1).to({y:0.0133,startPosition:1},0).wait(1).to({y:0.0548,startPosition:2},0).wait(1).to({y:0.1271,startPosition:3},0).wait(1).to({y:0.2334,startPosition:4},0).wait(1).to({y:0.3767,startPosition:5},0).wait(1).to({y:0.5607,startPosition:6},0).wait(1).to({y:0.7893,startPosition:7},0).wait(1).to({y:1.0661,startPosition:8},0).wait(1).to({y:1.3949,startPosition:9},0).wait(1).to({y:1.7786,startPosition:10},0).wait(1).to({y:2.2187,startPosition:11},0).wait(1).to({y:2.7141,startPosition:12},0).wait(1).to({y:3.2603,startPosition:13},0).wait(1).to({y:3.8474,startPosition:14},0).wait(1).to({y:4.4607,startPosition:15},0).wait(1).to({y:5.0811,startPosition:16},0).wait(1).to({y:5.6878,startPosition:17},0).wait(1).to({y:6.2617,startPosition:18},0).wait(1).to({y:6.788,startPosition:19},0).wait(1).to({y:7.2573,startPosition:20},0).wait(1).to({y:7.6653,startPosition:21},0).wait(1).to({y:8.0116,startPosition:22},0).wait(1).to({y:8.2985,startPosition:23},0).wait(1).to({y:8.5296,startPosition:24},0).wait(1).to({y:8.7093,startPosition:25},0).wait(1).to({y:8.8421,startPosition:26},0).wait(1).to({y:8.9322,startPosition:27},0).wait(1).to({y:8.9836,startPosition:28},0).wait(1).to({y:9,startPosition:29},0).wait(1).to({y:8.9876,startPosition:30},0).wait(1).to({y:8.9489,startPosition:31},0).wait(1).to({y:8.8816,startPosition:32},0).wait(1).to({y:8.7829,startPosition:33},0).wait(1).to({y:8.6499,startPosition:34},0).wait(1).to({y:8.4795,startPosition:35},0).wait(1).to({y:8.2682,startPosition:36},0).wait(1).to({y:8.0127,startPosition:37},0).wait(1).to({y:7.7094,startPosition:38},0).wait(1).to({y:7.3556,startPosition:39},0).wait(1).to({y:6.9493,startPosition:40},0).wait(1).to({y:6.4905,startPosition:41},0).wait(1).to({y:5.9821,startPosition:42},0).wait(1).to({y:5.4309,startPosition:43},0).wait(1).to({y:4.8481,startPosition:44},0).wait(1).to({y:4.2494,startPosition:45},0).wait(1).to({y:3.6531,startPosition:46},0).wait(1).to({y:3.0777,startPosition:47},0).wait(1).to({y:2.5391,startPosition:48},0).wait(1).to({y:2.0489,startPosition:49},0).wait(1).to({y:1.6138,startPosition:50},0).wait(1).to({y:1.2364,startPosition:51},0).wait(1).to({y:0.9163,startPosition:52},0).wait(1).to({y:0.651,startPosition:53},0).wait(1).to({y:0.437,startPosition:54},0).wait(1).to({y:0.2704,startPosition:55},0).wait(1).to({y:0.1471,startPosition:56},0).wait(1).to({y:0.0632,startPosition:57},0).wait(1).to({y:0.0153,startPosition:58},0).wait(1).to({y:0,startPosition:59},0).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-38.1,-34.7,76.30000000000001,81.7);


(lib.Symbol2copy2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Symbol1copy2("synched",0,false);
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1).to({y:0.0133,startPosition:1},0).wait(1).to({y:0.0548,startPosition:2},0).wait(1).to({y:0.1271,startPosition:3},0).wait(1).to({y:0.2334,startPosition:4},0).wait(1).to({y:0.3767,startPosition:5},0).wait(1).to({y:0.5607,startPosition:6},0).wait(1).to({y:0.7893,startPosition:7},0).wait(1).to({y:1.0661,startPosition:8},0).wait(1).to({y:1.3949,startPosition:9},0).wait(1).to({y:1.7786,startPosition:10},0).wait(1).to({y:2.2187,startPosition:11},0).wait(1).to({y:2.7141,startPosition:12},0).wait(1).to({y:3.2603,startPosition:13},0).wait(1).to({y:3.8474,startPosition:14},0).wait(1).to({y:4.4607,startPosition:15},0).wait(1).to({y:5.0811,startPosition:16},0).wait(1).to({y:5.6878,startPosition:17},0).wait(1).to({y:6.2617,startPosition:18},0).wait(1).to({y:6.788,startPosition:19},0).wait(1).to({y:7.2573,startPosition:20},0).wait(1).to({y:7.6653,startPosition:21},0).wait(1).to({y:8.0116,startPosition:22},0).wait(1).to({y:8.2985,startPosition:23},0).wait(1).to({y:8.5296,startPosition:24},0).wait(1).to({y:8.7093,startPosition:25},0).wait(1).to({y:8.8421,startPosition:26},0).wait(1).to({y:8.9322,startPosition:27},0).wait(1).to({y:8.9836,startPosition:28},0).wait(1).to({y:9,startPosition:29},0).wait(1).to({y:8.9876,startPosition:30},0).wait(1).to({y:8.9489,startPosition:31},0).wait(1).to({y:8.8816,startPosition:32},0).wait(1).to({y:8.7829,startPosition:33},0).wait(1).to({y:8.6499,startPosition:34},0).wait(1).to({y:8.4795,startPosition:35},0).wait(1).to({y:8.2682,startPosition:36},0).wait(1).to({y:8.0127,startPosition:37},0).wait(1).to({y:7.7094,startPosition:38},0).wait(1).to({y:7.3556,startPosition:39},0).wait(1).to({y:6.9493,startPosition:40},0).wait(1).to({y:6.4905,startPosition:41},0).wait(1).to({y:5.9821,startPosition:42},0).wait(1).to({y:5.4309,startPosition:43},0).wait(1).to({y:4.8481,startPosition:44},0).wait(1).to({y:4.2494,startPosition:45},0).wait(1).to({y:3.6531,startPosition:46},0).wait(1).to({y:3.0777,startPosition:47},0).wait(1).to({y:2.5391,startPosition:48},0).wait(1).to({y:2.0489,startPosition:49},0).wait(1).to({y:1.6138,startPosition:50},0).wait(1).to({y:1.2364,startPosition:51},0).wait(1).to({y:0.9163,startPosition:52},0).wait(1).to({y:0.651,startPosition:53},0).wait(1).to({y:0.437,startPosition:54},0).wait(1).to({y:0.2704,startPosition:55},0).wait(1).to({y:0.1471,startPosition:56},0).wait(1).to({y:0.0632,startPosition:57},0).wait(1).to({y:0.0153,startPosition:58},0).wait(1).to({y:0,startPosition:59},0).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-47.3,-43,94.6,99.2);


(lib.Symbol2copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Symbol1copy("synched",0,false);
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1).to({y:0.0133,startPosition:1},0).wait(1).to({y:0.0548,startPosition:2},0).wait(1).to({y:0.1271,startPosition:3},0).wait(1).to({y:0.2334,startPosition:4},0).wait(1).to({y:0.3767,startPosition:5},0).wait(1).to({y:0.5607,startPosition:6},0).wait(1).to({y:0.7893,startPosition:7},0).wait(1).to({y:1.0661,startPosition:8},0).wait(1).to({y:1.3949,startPosition:9},0).wait(1).to({y:1.7786,startPosition:10},0).wait(1).to({y:2.2187,startPosition:11},0).wait(1).to({y:2.7141,startPosition:12},0).wait(1).to({y:3.2603,startPosition:13},0).wait(1).to({y:3.8474,startPosition:14},0).wait(1).to({y:4.4607,startPosition:15},0).wait(1).to({y:5.0811,startPosition:16},0).wait(1).to({y:5.6878,startPosition:17},0).wait(1).to({y:6.2617,startPosition:18},0).wait(1).to({y:6.788,startPosition:19},0).wait(1).to({y:7.2573,startPosition:20},0).wait(1).to({y:7.6653,startPosition:21},0).wait(1).to({y:8.0116,startPosition:22},0).wait(1).to({y:8.2985,startPosition:23},0).wait(1).to({y:8.5296,startPosition:24},0).wait(1).to({y:8.7093,startPosition:25},0).wait(1).to({y:8.8421,startPosition:26},0).wait(1).to({y:8.9322,startPosition:27},0).wait(1).to({y:8.9836,startPosition:28},0).wait(1).to({y:9,startPosition:29},0).wait(1).to({y:8.9876,startPosition:30},0).wait(1).to({y:8.9489,startPosition:31},0).wait(1).to({y:8.8816,startPosition:32},0).wait(1).to({y:8.7829,startPosition:33},0).wait(1).to({y:8.6499,startPosition:34},0).wait(1).to({y:8.4795,startPosition:35},0).wait(1).to({y:8.2682,startPosition:36},0).wait(1).to({y:8.0127,startPosition:37},0).wait(1).to({y:7.7094,startPosition:38},0).wait(1).to({y:7.3556,startPosition:39},0).wait(1).to({y:6.9493,startPosition:40},0).wait(1).to({y:6.4905,startPosition:41},0).wait(1).to({y:5.9821,startPosition:42},0).wait(1).to({y:5.4309,startPosition:43},0).wait(1).to({y:4.8481,startPosition:44},0).wait(1).to({y:4.2494,startPosition:45},0).wait(1).to({y:3.6531,startPosition:46},0).wait(1).to({y:3.0777,startPosition:47},0).wait(1).to({y:2.5391,startPosition:48},0).wait(1).to({y:2.0489,startPosition:49},0).wait(1).to({y:1.6138,startPosition:50},0).wait(1).to({y:1.2364,startPosition:51},0).wait(1).to({y:0.9163,startPosition:52},0).wait(1).to({y:0.651,startPosition:53},0).wait(1).to({y:0.437,startPosition:54},0).wait(1).to({y:0.2704,startPosition:55},0).wait(1).to({y:0.1471,startPosition:56},0).wait(1).to({y:0.0632,startPosition:57},0).wait(1).to({y:0.0153,startPosition:58},0).wait(1).to({y:0,mode:"single",startPosition:59},0).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-50.5,-45.9,101,105.3);


(lib.Symbol2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Symbol1("synched",0,false);
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1).to({y:0.0133,startPosition:1},0).wait(1).to({y:0.0548,startPosition:2},0).wait(1).to({y:0.1271,startPosition:3},0).wait(1).to({y:0.2334,startPosition:4},0).wait(1).to({y:0.3767,startPosition:5},0).wait(1).to({y:0.5607,startPosition:6},0).wait(1).to({y:0.7893,startPosition:7},0).wait(1).to({y:1.0661,startPosition:8},0).wait(1).to({y:1.3949,startPosition:9},0).wait(1).to({y:1.7786,startPosition:10},0).wait(1).to({y:2.2187,startPosition:11},0).wait(1).to({y:2.7141,startPosition:12},0).wait(1).to({y:3.2603,startPosition:13},0).wait(1).to({y:3.8474,startPosition:14},0).wait(1).to({y:4.4607,startPosition:15},0).wait(1).to({y:5.0811,startPosition:16},0).wait(1).to({y:5.6878,startPosition:17},0).wait(1).to({y:6.2617,startPosition:18},0).wait(1).to({y:6.788,startPosition:19},0).wait(1).to({y:7.2573,startPosition:20},0).wait(1).to({y:7.6653,startPosition:21},0).wait(1).to({y:8.0116,startPosition:22},0).wait(1).to({y:8.2985,startPosition:23},0).wait(1).to({y:8.5296,startPosition:24},0).wait(1).to({y:8.7093,startPosition:25},0).wait(1).to({y:8.8421,startPosition:26},0).wait(1).to({y:8.9322,startPosition:27},0).wait(1).to({y:8.9836,startPosition:28},0).wait(1).to({y:9,startPosition:29},0).wait(1).to({y:8.9876,startPosition:30},0).wait(1).to({y:8.9489,startPosition:31},0).wait(1).to({y:8.8816,startPosition:32},0).wait(1).to({y:8.7829,startPosition:33},0).wait(1).to({y:8.6499,startPosition:34},0).wait(1).to({y:8.4795,startPosition:35},0).wait(1).to({y:8.2682,startPosition:36},0).wait(1).to({y:8.0127,startPosition:37},0).wait(1).to({y:7.7094,startPosition:38},0).wait(1).to({y:7.3556,startPosition:39},0).wait(1).to({y:6.9493,startPosition:40},0).wait(1).to({y:6.4905,startPosition:41},0).wait(1).to({y:5.9821,startPosition:42},0).wait(1).to({y:5.4309,startPosition:43},0).wait(1).to({y:4.8481,startPosition:44},0).wait(1).to({y:4.2494,startPosition:45},0).wait(1).to({y:3.6531,startPosition:46},0).wait(1).to({y:3.0777,startPosition:47},0).wait(1).to({y:2.5391,startPosition:48},0).wait(1).to({y:2.0489,startPosition:49},0).wait(1).to({y:1.6138,startPosition:50},0).wait(1).to({y:1.2364,startPosition:51},0).wait(1).to({y:0.9163,startPosition:52},0).wait(1).to({y:0.651,startPosition:53},0).wait(1).to({y:0.437,startPosition:54},0).wait(1).to({y:0.2704,startPosition:55},0).wait(1).to({y:0.1471,startPosition:56},0).wait(1).to({y:0.0632,startPosition:57},0).wait(1).to({y:0.0153,startPosition:58},0).wait(1).to({y:0,startPosition:59},0).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-36.8,-33.5,73.69999999999999,79.2);


(lib.Symbol_7_Layer_7 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_7
	this.instance = new lib.Symbol10();
	this.instance.parent = this;
	this.instance.setTransform(264.4,19.7);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(100));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol7 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_99 = function() {
		this.___loopingOver___ = true;
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(99).call(this.frame_99).wait(1));

	// Layer_7_obj_
	this.Layer_7 = new lib.Symbol_7_Layer_7();
	this.Layer_7.name = "Layer_7";
	this.Layer_7.parent = this;
	this.Layer_7.setTransform(226.3,29.4,1,1,0,0,0,226.3,29.4);
	this.Layer_7.depth = 0;
	this.Layer_7.isAttachedToCamera = 0
	this.Layer_7.isAttachedToMask = 0
	this.Layer_7.layerDepth = 0
	this.Layer_7.layerIndex = 0
	this.Layer_7.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_7).wait(100));

	// Layer_6_obj_
	this.Layer_6 = new lib.Symbol_7_Layer_6();
	this.Layer_6.name = "Layer_6";
	this.Layer_6.parent = this;
	this.Layer_6.setTransform(32.8,39.6,1,1,0,0,0,32.8,39.6);
	this.Layer_6.depth = 0;
	this.Layer_6.isAttachedToCamera = 0
	this.Layer_6.isAttachedToMask = 0
	this.Layer_6.layerDepth = 0
	this.Layer_6.layerIndex = 1
	this.Layer_6.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_6).wait(100));

	// Layer_4_obj_
	this.Layer_4 = new lib.Symbol_7_Layer_4();
	this.Layer_4.name = "Layer_4";
	this.Layer_4.parent = this;
	this.Layer_4.setTransform(72.8,11.9,1,1,0,0,0,72.8,11.9);
	this.Layer_4.depth = 0;
	this.Layer_4.isAttachedToCamera = 0
	this.Layer_4.isAttachedToMask = 0
	this.Layer_4.layerDepth = 0
	this.Layer_4.layerIndex = 2
	this.Layer_4.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_4).wait(100));

	// Layer_3_obj_
	this.Layer_3 = new lib.Symbol_7_Layer_3();
	this.Layer_3.name = "Layer_3";
	this.Layer_3.parent = this;
	this.Layer_3.setTransform(70.6,0.4,1,1,0,0,0,70.6,0.4);
	this.Layer_3.depth = 0;
	this.Layer_3.isAttachedToCamera = 0
	this.Layer_3.isAttachedToMask = 0
	this.Layer_3.layerDepth = 0
	this.Layer_3.layerIndex = 3
	this.Layer_3.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_3).wait(100));

	// Layer_2_obj_
	this.Layer_2 = new lib.Symbol_7_Layer_2();
	this.Layer_2.name = "Layer_2";
	this.Layer_2.parent = this;
	this.Layer_2.setTransform(1.3,11.7,1,1,0,0,0,1.3,11.7);
	this.Layer_2.depth = 0;
	this.Layer_2.isAttachedToCamera = 0
	this.Layer_2.isAttachedToMask = 0
	this.Layer_2.layerDepth = 0
	this.Layer_2.layerIndex = 4
	this.Layer_2.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_2).wait(100));

	// Layer_1_obj_
	this.Layer_1 = new lib.Symbol_7_Layer_1();
	this.Layer_1.name = "Layer_1";
	this.Layer_1.parent = this;
	this.Layer_1.depth = 0;
	this.Layer_1.isAttachedToCamera = 0
	this.Layer_1.isAttachedToMask = 0
	this.Layer_1.layerDepth = 0
	this.Layer_1.layerIndex = 5
	this.Layer_1.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_1).wait(100));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-22.4,-12.2,301,76.7);


(lib.Symbol3copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_6
	this.instance = new lib.Symbol2copy5("synched",50,false);
	this.instance.parent = this;
	this.instance.setTransform(-439.2,-67.05,0.3901,0.3901,0,0,0,-0.1,-0.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(9).to({startPosition:0},0).wait(51));

	// Layer_5
	this.instance_1 = new lib.Symbol2copy4("synched",30,false);
	this.instance_1.parent = this;
	this.instance_1.setTransform(-395.85,-59.8,0.687,0.687,0,0,0,-0.1,-0.4);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(29).to({startPosition:0},0).wait(31));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-457.4,-92,97.39999999999998,74.3);


(lib.Symbol3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_4
	this.instance = new lib.Symbol2copy3("synched",20,false);
	this.instance.parent = this;
	this.instance.setTransform(-192.55,-90.9,0.5861,0.5861,0,0,0,-0.1,-0.4);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(39).to({startPosition:0},0).wait(21));

	// Layer_3
	this.instance_1 = new lib.Symbol2copy2("synched",10,false);
	this.instance_1.parent = this;
	this.instance_1.setTransform(21.95,-60.15,0.6914,0.6914,0,0,0,0.1,-0.2);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(49).to({startPosition:0},0).wait(11));

	// Layer_2
	this.instance_2 = new lib.Symbol2copy("synched",40,false);
	this.instance_2.parent = this;
	this.instance_2.setTransform(-0.6,268.8);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(19).to({startPosition:0},0).wait(41));

	// Layer_1
	this.instance_3 = new lib.Symbol2("synched",0,false);
	this.instance_3.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(60));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-214.8,-111,269.5,439.1);


(lib.Scene_1_Layer_11 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_11
	this.instance = new lib.Symbol7();
	this.instance.parent = this;
	this.instance.setTransform(613.85,262.55);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(100));

}).prototype = p = new cjs.MovieClip();


(lib.Scene_1_Layer_10 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_10
	this.instance = new lib.Symbol3copy();
	this.instance.parent = this;
	this.instance.setTransform(978.5,232.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(100));

}).prototype = p = new cjs.MovieClip();


(lib.Scene_1_Layer_4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_4
	this.instance = new lib.Symbol3();
	this.instance.parent = this;
	this.instance.setTransform(978.5,232.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(100));

}).prototype = p = new cjs.MovieClip();


// stage content:
(lib.OGO2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	this.___GetDepth___ = function(obj) {
		var depth = obj.depth;
		var cameraObj = this.___camera___instance;
		if(cameraObj && cameraObj.depth && obj.isAttachedToCamera)
		{
			depth += depth + cameraObj.depth;
		}
		return depth;
		}
	this.___needSorting___ = function() {
		for (var i = 0; i < this.getNumChildren() - 1; i++)
		{
			var prevDepth = this.___GetDepth___(this.getChildAt(i));
			var nextDepth = this.___GetDepth___(this.getChildAt(i + 1));
			if (prevDepth < nextDepth)
				return true;
		}
		return false;
	}
	this.___sortFunction___ = function(obj1, obj2) {
		return (this.exportRoot.___GetDepth___(obj2) - this.exportRoot.___GetDepth___(obj1));
	}
	this.on('tick', function (event){
		var curTimeline = event.currentTarget;
		if (curTimeline.___needSorting___()){
			this.sortChildren(curTimeline.___sortFunction___);
		}
	});

	// timeline functions:
	this.frame_99 = function() {
		this.___loopingOver___ = true;
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(99).call(this.frame_99).wait(1));

	// Layer_11_obj_
	this.Layer_11 = new lib.Scene_1_Layer_11();
	this.Layer_11.name = "Layer_11";
	this.Layer_11.parent = this;
	this.Layer_11.setTransform(743.1,288.7,1,1,0,0,0,743.1,288.7);
	this.Layer_11.depth = 0;
	this.Layer_11.isAttachedToCamera = 0
	this.Layer_11.isAttachedToMask = 0
	this.Layer_11.layerDepth = 0
	this.Layer_11.layerIndex = 0
	this.Layer_11.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_11).wait(100));

	// Layer_14_obj_
	this.Layer_14 = new lib.Scene_1_Layer_14();
	this.Layer_14.name = "Layer_14";
	this.Layer_14.parent = this;
	this.Layer_14.setTransform(766.2,422.5,1,1,0,0,0,766.2,422.5);
	this.Layer_14.depth = 0;
	this.Layer_14.isAttachedToCamera = 0
	this.Layer_14.isAttachedToMask = 0
	this.Layer_14.layerDepth = 0
	this.Layer_14.layerIndex = 1
	this.Layer_14.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_14).wait(20).to({regX:762.8,regY:409.8,x:762.8,y:409.8},0).wait(9).to({regX:766.2,regY:422.5,x:766.2,y:422.5},0).wait(6).to({regX:762.8,regY:409.8,x:762.8,y:409.8},0).wait(9).to({regX:766.2,regY:422.5,x:766.2,y:422.5},0).wait(6).to({regX:762.8,regY:409.8,x:762.8,y:409.8},0).wait(7).to({regX:766.2,regY:422.5,x:766.2,y:422.5},0).wait(43));

	// Layer_9_obj_
	this.Layer_9 = new lib.Scene_1_Layer_9();
	this.Layer_9.name = "Layer_9";
	this.Layer_9.parent = this;
	this.Layer_9.setTransform(745.9,368.4,1,1,0,0,0,745.9,368.4);
	this.Layer_9.depth = 0;
	this.Layer_9.isAttachedToCamera = 0
	this.Layer_9.isAttachedToMask = 0
	this.Layer_9.layerDepth = 0
	this.Layer_9.layerIndex = 2
	this.Layer_9.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_9).wait(100));

	// Layer_8_obj_
	this.Layer_8 = new lib.Scene_1_Layer_8();
	this.Layer_8.name = "Layer_8";
	this.Layer_8.parent = this;
	this.Layer_8.setTransform(969.1,358,1,1,0,0,0,969.1,358);
	this.Layer_8.depth = 0;
	this.Layer_8.isAttachedToCamera = 0
	this.Layer_8.isAttachedToMask = 0
	this.Layer_8.layerDepth = 0
	this.Layer_8.layerIndex = 3
	this.Layer_8.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_8).wait(100));

	// Layer_19 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("A9dP3IAA/GIcNAtIAAeZgABEu6Icag8IgKbXI8LAwg");
	mask.setTransform(766.85,438);

	// Layer_20_obj_
	this.Layer_20 = new lib.Scene_1_Layer_20();
	this.Layer_20.name = "Layer_20";
	this.Layer_20.parent = this;
	this.Layer_20.setTransform(749,437,1,1,0,0,0,749,437);
	this.Layer_20.depth = 0;
	this.Layer_20.isAttachedToCamera = 0
	this.Layer_20.isAttachedToMask = 0
	this.Layer_20.layerDepth = 0
	this.Layer_20.layerIndex = 4
	this.Layer_20.maskLayerName = 0

	var maskedShapeInstanceList = [this.Layer_20];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.Layer_20).wait(20).to({regX:756.1,regY:433,x:756.1,y:433},0).wait(9).to({regX:749,regY:437,x:749,y:437},0).wait(6).to({regX:756.1,regY:433,x:756.1,y:433},0).wait(9).to({regX:749,regY:437,x:749,y:437},0).wait(6).to({regX:756.1,regY:433,x:756.1,y:433},0).wait(7).to({regX:749,regY:437,x:749,y:437},0).wait(43));

	// Layer_4_obj_
	this.Layer_4 = new lib.Scene_1_Layer_4();
	this.Layer_4.name = "Layer_4";
	this.Layer_4.parent = this;
	this.Layer_4.setTransform(897.9,334.4,1,1,0,0,0,897.9,334.4);
	this.Layer_4.depth = 0;
	this.Layer_4.isAttachedToCamera = 0
	this.Layer_4.isAttachedToMask = 0
	this.Layer_4.layerDepth = 0
	this.Layer_4.layerIndex = 5
	this.Layer_4.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_4).wait(100));

	// Layer_3_obj_
	this.Layer_3 = new lib.Scene_1_Layer_3();
	this.Layer_3.name = "Layer_3";
	this.Layer_3.parent = this;
	this.Layer_3.setTransform(787,387,1,1,0,0,0,787,387);
	this.Layer_3.depth = 0;
	this.Layer_3.isAttachedToCamera = 0
	this.Layer_3.isAttachedToMask = 0
	this.Layer_3.layerDepth = 0
	this.Layer_3.layerIndex = 6
	this.Layer_3.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_3).wait(100));

	// Layer_10_obj_
	this.Layer_10 = new lib.Scene_1_Layer_10();
	this.Layer_10.name = "Layer_10";
	this.Layer_10.parent = this;
	this.Layer_10.setTransform(750.6,186.5,1,1,0,0,0,750.6,186.5);
	this.Layer_10.depth = 0;
	this.Layer_10.isAttachedToCamera = 0
	this.Layer_10.isAttachedToMask = 0
	this.Layer_10.layerDepth = 0
	this.Layer_10.layerIndex = 7
	this.Layer_10.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_10).wait(100));

	// Layer_17_obj_
	this.Layer_17 = new lib.Scene_1_Layer_17();
	this.Layer_17.name = "Layer_17";
	this.Layer_17.parent = this;
	this.Layer_17.setTransform(545.6,345.8,1,1,0,0,0,545.6,345.8);
	this.Layer_17.depth = 0;
	this.Layer_17.isAttachedToCamera = 0
	this.Layer_17.isAttachedToMask = 0
	this.Layer_17.layerDepth = 0
	this.Layer_17.layerIndex = 8
	this.Layer_17.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_17).wait(100));

	// Layer_18_obj_
	this.Layer_18 = new lib.Scene_1_Layer_18();
	this.Layer_18.name = "Layer_18";
	this.Layer_18.parent = this;
	this.Layer_18.setTransform(540,340,1,1,0,0,0,540,340);
	this.Layer_18.depth = 0;
	this.Layer_18.isAttachedToCamera = 0
	this.Layer_18.isAttachedToMask = 0
	this.Layer_18.layerDepth = 0
	this.Layer_18.layerIndex = 9
	this.Layer_18.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_18).wait(100));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(540,340,541,340);
// library properties:
lib.properties = {
	id: 'BFAA81EE19FF1441B8DC61E308BF9B19',
	width: 1080,
	height: 680,
	fps: 30,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: [
		{src:"assets/images/bg42.jpg", id:"bg"},
		{src:"assets/images/icon142.png", id:"icon1"},
		{src:"assets/images/packs42.png", id:"packs"},
		{src:"assets/images/shadow142.png", id:"shadow1"},
		{src:"assets/images/tablet42.png", id:"tablet"}
	],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['BFAA81EE19FF1441B8DC61E308BF9B19'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}


// Layer depth API : 

AdobeAn.Layer = new function() {
	this.getLayerZDepth = function(timeline, layerName)
	{
		if(layerName === "Camera")
		layerName = "___camera___instance";
		var script = "if(timeline." + layerName + ") timeline." + layerName + ".depth; else 0;";
		return eval(script);
	}
	this.setLayerZDepth = function(timeline, layerName, zDepth)
	{
		const MAX_zDepth = 10000;
		const MIN_zDepth = -5000;
		if(zDepth > MAX_zDepth)
			zDepth = MAX_zDepth;
		else if(zDepth < MIN_zDepth)
			zDepth = MIN_zDepth;
		if(layerName === "Camera")
		layerName = "___camera___instance";
		var script = "if(timeline." + layerName + ") timeline." + layerName + ".depth = " + zDepth + ";";
		eval(script);
	}
	this.removeLayer = function(timeline, layerName)
	{
		if(layerName === "Camera")
		layerName = "___camera___instance";
		var script = "if(timeline." + layerName + ") timeline.removeChild(timeline." + layerName + ");";
		eval(script);
	}
	this.addNewLayer = function(timeline, layerName, zDepth)
	{
		if(layerName === "Camera")
		layerName = "___camera___instance";
		zDepth = typeof zDepth !== 'undefined' ? zDepth : 0;
		var layer = new createjs.MovieClip();
		layer.name = layerName;
		layer.depth = zDepth;
		layer.layerIndex = 0;
		timeline.addChild(layer);
	}
}


})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;

$(function () {
    var canvas, stage, exportRoot, anim_container, dom_overlay_container, fnStartAnimation;
    function init() {
        canvas = document.getElementById("canvas42");
        anim_container = document.getElementById("animation_container42");
        dom_overlay_container = document.getElementById("dom_overlay_container42");
        var comp=AdobeAn.getComposition("BFAA81EE19FF1441B8DC61E308BF9B19");
        var lib=comp.getLibrary();
        var loader = new createjs.LoadQueue(false);
        loader.addEventListener("fileload", function(evt){handleFileLoad(evt,comp)});
        loader.addEventListener("complete", function(evt){handleComplete(evt,comp)});
        var lib=comp.getLibrary();
        loader.loadManifest(lib.properties.manifest);
    }
    function handleFileLoad(evt, comp) {
        var images=comp.getImages();
        if (evt && (evt.item.type == "image")) { images[evt.item.id] = evt.result; }
    }
    function handleComplete(evt,comp) {
        //This function is always called, irrespective of the content. You can use the variable "stage" after it is created in token create_stage.
        var lib=comp.getLibrary();
        var ss=comp.getSpriteSheet();
        var queue = evt.target;
        var ssMetadata = lib.ssMetadata;
        for(i=0; i<ssMetadata.length; i++) {
            ss[ssMetadata[i].name] = new createjs.SpriteSheet( {"images": [queue.getResult(ssMetadata[i].name)], "frames": ssMetadata[i].frames} )
        }
        var preloaderDiv = document.getElementById("_preload_div_42");
        preloaderDiv.style.display = 'none';
        canvas.style.display = 'block';
        exportRoot = new lib.OGO2();
        stage = new lib.Stage(canvas);
        //Registers the "tick" event listener.
        fnStartAnimation = function() {
            stage.addChild(exportRoot);
            createjs.Ticker.setFPS(lib.properties.fps);
            createjs.Ticker.addEventListener("tick", stage)
            stage.addEventListener("tick", handleTick)
            function getProjectionMatrix(container, totalDepth) {
                var focalLength = 528.25;
                var projectionCenter = { x : lib.properties.width/2, y : lib.properties.height/2 };
                var scale = (totalDepth + focalLength)/focalLength;
                var scaleMat = new createjs.Matrix2D;
                scaleMat.a = 1/scale;
                scaleMat.d = 1/scale;
                var projMat = new createjs.Matrix2D;
                projMat.tx = -projectionCenter.x;
                projMat.ty = -projectionCenter.y;
                projMat = projMat.prependMatrix(scaleMat);
                projMat.tx += projectionCenter.x;
                projMat.ty += projectionCenter.y;
                return projMat;
            }
            function handleTick(event) {
                var cameraInstance = exportRoot.___camera___instance;
                if(cameraInstance !== undefined && cameraInstance.pinToObject !== undefined)
                {
                    cameraInstance.x = cameraInstance.pinToObject.x + cameraInstance.pinToObject.pinOffsetX;
                    cameraInstance.y = cameraInstance.pinToObject.y + cameraInstance.pinToObject.pinOffsetY;
                    if(cameraInstance.pinToObject.parent !== undefined && cameraInstance.pinToObject.parent.depth !== undefined)
                        cameraInstance.depth = cameraInstance.pinToObject.parent.depth + cameraInstance.pinToObject.pinOffsetZ;
                }
                applyLayerZDepth(exportRoot);
            }
            function applyLayerZDepth(parent)
            {
                var cameraInstance = parent.___camera___instance;
                var focalLength = 528.25;
                var projectionCenter = { 'x' : 0, 'y' : 0};
                if(parent === exportRoot)
                {
                    var stageCenter = { 'x' : lib.properties.width/2, 'y' : lib.properties.height/2 };
                    projectionCenter.x = stageCenter.x;
                    projectionCenter.y = stageCenter.y;
                }
                for(child in parent.children)
                {
                    var layerObj = parent.children[child];
                    if(layerObj == cameraInstance)
                        continue;
                    applyLayerZDepth(layerObj, cameraInstance);
                    if(layerObj.layerDepth === undefined)
                        continue;
                    if(layerObj.currentFrame != layerObj.parent.currentFrame)
                    {
                        layerObj.gotoAndPlay(layerObj.parent.currentFrame);
                    }
                    var matToApply = new createjs.Matrix2D;
                    var cameraMat = new createjs.Matrix2D;
                    var totalDepth = layerObj.layerDepth ? layerObj.layerDepth : 0;
                    var cameraDepth = 0;
                    if(cameraInstance && !layerObj.isAttachedToCamera)
                    {
                        var mat = cameraInstance.getMatrix();
                        mat.tx -= projectionCenter.x;
                        mat.ty -= projectionCenter.y;
                        cameraMat = mat.invert();
                        cameraMat.prependTransform(projectionCenter.x, projectionCenter.y, 1, 1, 0, 0, 0, 0, 0);
                        cameraMat.appendTransform(-projectionCenter.x, -projectionCenter.y, 1, 1, 0, 0, 0, 0, 0);
                        if(cameraInstance.depth)
                            cameraDepth = cameraInstance.depth;
                    }
                    if(layerObj.depth)
                    {
                        totalDepth = layerObj.depth;
                    }
                    //Offset by camera depth
                    totalDepth -= cameraDepth;
                    if(totalDepth < -focalLength)
                    {
                        matToApply.a = 0;
                        matToApply.d = 0;
                    }
                    else
                    {
                        if(layerObj.layerDepth)
                        {
                            var sizeLockedMat = getProjectionMatrix(parent, layerObj.layerDepth);
                            if(sizeLockedMat)
                            {
                                sizeLockedMat.invert();
                                matToApply.prependMatrix(sizeLockedMat);
                            }
                        }
                        matToApply.prependMatrix(cameraMat);
                        var projMat = getProjectionMatrix(parent, totalDepth);
                        if(projMat)
                        {
                            matToApply.prependMatrix(projMat);
                        }
                    }
                    layerObj.transformMatrix = matToApply;
                }
            }
        }
        //Code to support hidpi screens and responsive scaling.
        function makeResponsive(isResp, respDim, isScale, scaleType) {
            var lastW, lastH, lastS=1;
            window.addEventListener('resize', resizeCanvas);
            resizeCanvas();
            function resizeCanvas() {
                var w = lib.properties.width, h = lib.properties.height;
                var iw = window.innerWidth, ih=window.innerHeight;
                var pRatio = window.devicePixelRatio || 1, xRatio=iw/w, yRatio=ih/h, sRatio=1;
                if(isResp) {
                    if((respDim=='width'&&lastW==iw) || (respDim=='height'&&lastH==ih)) {
                        sRatio = lastS;
                    }
                    else if(!isScale) {
                        if(iw<w || ih<h)
                            sRatio = Math.min(xRatio, yRatio);
                    }
                    else if(scaleType==1) {
                        sRatio = Math.min(xRatio, yRatio);
                    }
                    else if(scaleType==2) {
                        sRatio = Math.max(xRatio, yRatio);
                    }
                }
                canvas.width = w*pRatio*sRatio;
                canvas.height = h*pRatio*sRatio;
                canvas.style.width = anim_container.style.width = dom_overlay_container.style.width = preloaderDiv.style.width = w*sRatio+'px';
                canvas.style.height = anim_container.style.height = dom_overlay_container.style.height = preloaderDiv.style.height = h*sRatio+'px';
                stage.scaleX = pRatio*sRatio;
                stage.scaleY = pRatio*sRatio;
                lastW = iw; lastH = ih; lastS = sRatio;
                stage.tickOnUpdate = false;
                stage.update();
                stage.tickOnUpdate = true;
            }
        }
        makeResponsive(true,'both',true,2);
        AdobeAn.compositionLoaded(lib.properties.id);
        fnStartAnimation();
    }
    init();
});