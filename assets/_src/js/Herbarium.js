(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [];


// symbols:



(lib.bg_part1 = function() {
	this.initialize(img.bg_part1);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,731,260);


(lib.herbs = function() {
	this.initialize(img.herbs);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,596,421);


(lib.leaf1 = function() {
	this.initialize(img.leaf1);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,175,186);


(lib.pack = function() {
	this.initialize(img.pack);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,257,486);


(lib.shadow1 = function() {
	this.initialize(img.shadow1);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,170,89);


(lib.shadow2 = function() {
	this.initialize(img.shadow2);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,176,89);// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.Symbol11 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.leaf1();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol11, new cjs.Rectangle(0,0,175,186), null);


(lib.Symbol9 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(13,1,1).p("AiOAkQEbh2ACBQ");
	this.shape.setTransform(14.3,3.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol9, new cjs.Rectangle(-6.5,-6.5,41.6,20.1), null);


(lib.Symbol8 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(13,1,1).p("Ah0B6QB0igB1hT");
	this.shape.setTransform(11.7,12.2);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol8, new cjs.Rectangle(-6.5,-6.5,36.4,37.5), null);


(lib.Symbol7 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(13,1,1).p("AAlBiQAghqhwhZ");
	this.shape.setTransform(4.4,9.8);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol7, new cjs.Rectangle(-6.5,-6.5,21.8,32.7), null);


(lib.Symbol5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.shadow1();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol5, new cjs.Rectangle(0,0,170,89), null);


(lib.Symbol4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.shadow2();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol4, new cjs.Rectangle(0,0,176,89), null);


(lib.Symbol2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#1B1814").ss(13,1,1).p("AjFAAQAlhUBQgbQBJgZBJAYQBLAYAjA9QAnBCgdBW");
	this.shape.setTransform(19.8,12.5);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#1B1814").ss(13,1,1).p("AjFABQAlhVBQgdQBKgbBJAYQBKAYAjA/QAmBEgdBZ");
	this.shape_1.setTransform(19.8,12.2);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#1B1814").ss(13,1,1).p("AjFAEQAmhYBQgfQBJgcBJAYQBLAYAjBAQAlBGgdBc");
	this.shape_2.setTransform(19.8,11.8);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#1B1814").ss(13,1,1).p("AjFAGQAmhaBQghQBKgdBIAYQBLAZAjBBQAlBHgeBe");
	this.shape_3.setTransform(19.9,11.5);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#1B1814").ss(13,1,1).p("AjEAJQAlhdBRghQBJgfBJAYQBKAZAjBDQAlBHgeBh");
	this.shape_4.setTransform(19.9,11.2);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#1B1814").ss(13,1,1).p("AjEAKQAmhfBQgiQBKgfBIAYQBLAYAiBEQAlBJgeBi");
	this.shape_5.setTransform(19.9,11.1);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#1B1814").ss(13,1,1).p("AjEALQAmhgBRgjQBJggBJAYQBKAZAiBEQAlBKgfBk");
	this.shape_6.setTransform(19.9,10.9);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#1B1814").ss(13,1,1).p("AjEANQAmhiBQgkQBKggBIAYQBLAZAiBFQAlBKgfBl");
	this.shape_7.setTransform(19.9,10.7);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#1B1814").ss(13,1,1).p("AjEANQAmhiBRgkQBJghBJAYQBKAZAiBFQAlBLgfBm");
	this.shape_8.setTransform(19.9,10.6);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#1B1814").ss(13,1,1).p("AjEAOQAmhjBRgkQBJghBJAYQBKAYAiBGQAlBLgfBm");
	this.shape_9.setTransform(19.9,10.6);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#1B1814").ss(13,1,1).p("AjEAOQAmhjBRgkQBJghBJAYQBKAYAiBGQAlBLgfBn");
	this.shape_10.setTransform(19.9,10.5);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#1B1814").ss(13,1,1).p("AjEALQAmhgBQgjQBKgfBIAYQBLAYAiBFQAlBJgeBj");
	this.shape_11.setTransform(19.9,10.9);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#1B1814").ss(13,1,1).p("AjEAGQAlhbBQgfQBKgdBJAYQBKAYAjBBQAlBHgdBe");
	this.shape_12.setTransform(19.8,11.6);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#1B1814").ss(13,1,1).p("AjFADQAlhXBQgeQBKgbBJAYQBLAYAiBAQAmBEgdBa");
	this.shape_13.setTransform(19.8,12);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#1B1814").ss(13,1,1).p("AjFACQAlhWBQgdQBJgaBJAYQBLAYAjA/QAmBDgdBZ");
	this.shape_14.setTransform(19.8,12.2);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#1B1814").ss(13,1,1).p("AjFAAQAlhUBQgcQBKgaBJAYQBLAYAiA+QAnBDgdBX");
	this.shape_15.setTransform(19.8,12.3);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#1B1814").ss(13,1,1).p("AjFAAQAlhUBQgbQBJgaBJAYQBLAYAjA+QAnBCgdBX");
	this.shape_16.setTransform(19.8,12.4);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#1B1814").ss(13,1,1).p("AjFAAQAlhTBQgcQBJgZBJAYQBLAYAjA9QAnBCgdBW");
	this.shape_17.setTransform(19.8,12.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},14).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_10}]},20).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape}]},1).wait(21));

	// Layer 3
	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#1B1814").ss(13,1,1).p("AjFAAQAlhUBQgbQBJgZBJAYQBLAYAjA9QAnBCgdBW");
	this.shape_18.setTransform(93.3,34.8);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#1B1814").ss(13,1,1).p("AjFACQAlhWBQgdQBKgaBJAXQBLAZAiA+QAnBEgeBZ");
	this.shape_19.setTransform(93.4,34.4);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#1B1814").ss(13,1,1).p("AjFAEQAmhYBQgfQBJgcBJAYQBLAYAjBAQAmBGgeBc");
	this.shape_20.setTransform(93.5,34);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#1B1814").ss(13,1,1).p("AjFAGQAmhaBQggQBKgdBIAXQBLAZAjBBQAlBHgeBe");
	this.shape_21.setTransform(93.6,33.7);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#1B1814").ss(13,1,1).p("AjEAIQAlhdBRghQBJgeBJAYQBLAZAiBCQAlBIgeBh");
	this.shape_22.setTransform(93.6,33.4);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#1B1814").ss(13,1,1).p("AjFAKQAmhfBRgiQBJgfBJAYQBKAYAjBEQAlBJgeBi");
	this.shape_23.setTransform(93.7,33.2);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#1B1814").ss(13,1,1).p("AjEALQAmhgBRgjQBJgfBIAXQBLAZAiBEQAlBKgeBk");
	this.shape_24.setTransform(93.7,32.9);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f().s("#1B1814").ss(13,1,1).p("AjEAMQAmhhBRgjQBJghBJAYQBKAZAiBFQAlBKgfBl");
	this.shape_25.setTransform(93.8,32.8);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f().s("#1B1814").ss(13,1,1).p("AjEANQAmhiBRgkQBJghBIAYQBLAZAiBFQAlBLgfBm");
	this.shape_26.setTransform(93.8,32.7);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f().s("#1B1814").ss(13,1,1).p("AjEANQAmhjBRgjQBJghBJAXQBKAZAiBGQAlBLgfBm");
	this.shape_27.setTransform(93.8,32.6);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f().s("#1B1814").ss(13,1,1).p("AjEAOQAmhjBRgkQBJghBJAXQBKAZAiBGQAlBLgfBn");
	this.shape_28.setTransform(93.8,32.6);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f().s("#1B1814").ss(13,1,1).p("AjEALQAmhgBRgjQBJgfBIAYQBLAYAiBEQAlBKgeBj");
	this.shape_29.setTransform(93.7,33);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f().s("#1B1814").ss(13,1,1).p("AjFAGQAmhbBQgfQBJgdBJAYQBLAYAiBBQAmBHgeBe");
	this.shape_30.setTransform(93.6,33.7);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f().s("#1B1814").ss(13,1,1).p("AjFACQAlhWBRgeQBJgbBJAYQBLAYAiA/QAnBFgeBa");
	this.shape_31.setTransform(93.4,34.3);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f().s("#1B1814").ss(13,1,1).p("AjFABQAlhVBQgdQBKgaBJAYQBKAYAjA+QAnBEgeBY");
	this.shape_32.setTransform(93.4,34.5);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f().s("#1B1814").ss(13,1,1).p("AjFAAQAlhUBQgcQBKgaBIAYQBLAYAjA+QAnBDgdBX");
	this.shape_33.setTransform(93.3,34.6);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f().s("#1B1814").ss(13,1,1).p("AjFAAQAlhUBQgcQBJgZBJAYQBLAYAjA9QAmBDgdBX");
	this.shape_34.setTransform(93.3,34.7);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f().s("#1B1814").ss(13,1,1).p("AjFAAQAlhTBQgcQBJgZBJAYQBLAYAjA9QAnBCgdBW");
	this.shape_35.setTransform(93.3,34.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_18}]}).to({state:[{t:this.shape_18}]},14).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_27}]},1).to({state:[{t:this.shape_28}]},1).to({state:[{t:this.shape_28}]},20).to({state:[{t:this.shape_29}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_30}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_31}]},1).to({state:[{t:this.shape_32}]},1).to({state:[{t:this.shape_33}]},1).to({state:[{t:this.shape_34}]},1).to({state:[{t:this.shape_35}]},1).to({state:[{t:this.shape_18}]},1).wait(21));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-6.5,-6.5,126.1,60.3);


(lib.Symbol2_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#F51431").s().p("AAAAqQgRAAgNgMQgMgMAAgSQAAgQAMgNQANgNARAAIADAAQAPABALAMQANANAAAQQAAASgNAMQgLAMgPAAIgDAAg");
	this.shape_36.setTransform(4.3,4.3);

	this.timeline.addTween(cjs.Tween.get(this.shape_36).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol2_1, new cjs.Rectangle(0,0,8.5,8.5), null);


(lib.Symbol15 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 8 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	var mask_graphics_14 = new cjs.Graphics().p("AmOEYQgCAAgzjsQg0jtAEgDQADgED6hGID4hGIHyDEQimDdiyg4QA2E8hQAEIgKAAQhfAAmng9g");
	var mask_graphics_16 = new cjs.Graphics().p("ACFFXQhTgBnAhBQgCABgzjtQg0jsAEgEQADgDD6hHID4hFIHyDDQimDtishIQA2FFhSAAIgBAAg");
	var mask_graphics_52 = new cjs.Graphics().p("ACFFXQhTgBnAhBQgCABgzjtQg0jsAEgEQADgDD6hHID4hFIHyDDQimDtishIQA2FFhSAAIgBAAg");
	var mask_graphics_54 = new cjs.Graphics().p("AmOEYQgCAAgzjsQg0jtAEgDQADgED6hGID4hGIHyDEQimDdiyg4QA2E8hQAEIgKAAQhfAAmng9g");

	this.timeline.addTween(cjs.Tween.get(mask).to({graphics:null,x:0,y:0}).wait(14).to({graphics:mask_graphics_14,x:112.8,y:129.6}).wait(2).to({graphics:mask_graphics_16,x:114.5,y:131.3}).wait(36).to({graphics:mask_graphics_52,x:114.5,y:131.3}).wait(2).to({graphics:mask_graphics_54,x:112.8,y:129.6}).wait(1).to({graphics:null,x:0,y:0}).wait(20));

	// Symbol 13
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(13,1,1).p("ABugEQiphwgyCm");
	this.shape.setTransform(133.4,148.9);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(13,1,1).p("Ah8AEQBBgqC4A1");
	this.shape_1.setTransform(131.9,146.9);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(13,1,1).p("AiLgmQBQBQDGgD");
	this.shape_2.setTransform(130.4,144.5);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(13,1,1).p("AhfhEQg0CQD8gH");
	this.shape_3.setTransform(133.8,141.5);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(13,1,1).p("Ag4hXQiDC3EdgJ");
	this.shape_4.setTransform(134.6,139.7);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(13,1,1).p("ABhBdQkoAKCejE");
	this.shape_5.setTransform(134.7,139.1);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(13,1,1).p("AhThKQhPCdEIgI");
	this.shape_6.setTransform(134.2,140.9);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(13,1,1).p("Ahzg4QABB2DmgF");
	this.shape_7.setTransform(132.7,142.7);

	var maskedShapeInstanceList = [this.shape,this.shape_1,this.shape_2,this.shape_3,this.shape_4,this.shape_5,this.shape_6,this.shape_7];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape}]},14).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_5}]},30).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape}]},1).to({state:[]},1).wait(20));

	// Symbol 12
	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(13,1,1).p("ACoAiQkDishLC7");
	this.shape_8.setTransform(127.5,142.9);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(13,1,1).p("AidgNQA9haD+CW");
	this.shape_9.setTransform(128.4,141.5);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(13,1,1).p("AiUhEQAuAJD7CA");
	this.shape_10.setTransform(129.3,139.3);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(13,1,1).p("AhohPQg7CGEWAZ");
	this.shape_11.setTransform(132.7,138.2);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(13,1,1).p("AhEhZQh5DSElgl");
	this.shape_12.setTransform(133.8,137.9);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(13,1,1).p("ABmBTQkrA6COjr");
	this.shape_13.setTransform(134,137.9);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(13,1,1).p("AhchSQhQCgEbAF");
	this.shape_14.setTransform(133.1,138);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(13,1,1).p("Ah8hLQgRBUELBD");
	this.shape_15.setTransform(131.6,138.7);

	var maskedShapeInstanceList = [this.shape_8,this.shape_9,this.shape_10,this.shape_11,this.shape_12,this.shape_13,this.shape_14,this.shape_15];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_8}]},14).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_13}]},30).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_8}]},1).to({state:[]},1).wait(20));

	// Symbol 14
	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(13,1,1).p("AAVB/QhCiwAohN");
	this.shape_16.setTransform(142.4,134.4);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(13,1,1).p("AgWh3QgGA9AzCy");
	this.shape_17.setTransform(142.2,135.1);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#000000").ss(13,1,1).p("AgghwQAcAtAlC1");
	this.shape_18.setTransform(141.3,135.7);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#000000").ss(13,1,1).p("Ag0hiQBTAJAWC8");
	this.shape_19.setTransform(139.2,137.2);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#000000").ss(13,1,1).p("AhBhYQB2gNANC/");
	this.shape_20.setTransform(137.9,138);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#000000").ss(13,1,1).p("ABGBYQgKjBiBAU");
	this.shape_21.setTransform(137.5,138.3);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#000000").ss(13,1,1).p("Ag5hfQBfACAUC9");
	this.shape_22.setTransform(138.7,137.5);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#000000").ss(13,1,1).p("AgshoQA8AXAdC5");
	this.shape_23.setTransform(140,136.6);

	var maskedShapeInstanceList = [this.shape_16,this.shape_17,this.shape_18,this.shape_19,this.shape_20,this.shape_21,this.shape_22,this.shape_23];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_16}]},14).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_21}]},30).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_16}]},1).to({state:[]},1).wait(20));

	// Symbol 11
	this.instance = new lib.Symbol11();
	this.instance.parent = this;
	this.instance.setTransform(87.5,93,1,1,0,0,0,87.5,93);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(75));

	// Symbol 13
	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#000000").ss(13,1,1).p("ABugEQiphwgyCm");
	this.shape_24.setTransform(133.4,148.9);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f().s("#000000").ss(13,1,1).p("Ah8AEQBBgqC4A1");
	this.shape_25.setTransform(131.9,146.9);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f().s("#000000").ss(13,1,1).p("AiLgmQBQBQDGgD");
	this.shape_26.setTransform(130.4,144.5);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f().s("#000000").ss(13,1,1).p("AhfhEQg0CQD8gH");
	this.shape_27.setTransform(133.8,141.5);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f().s("#000000").ss(13,1,1).p("Ag4hXQiDC3EdgJ");
	this.shape_28.setTransform(134.6,139.7);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f().s("#000000").ss(13,1,1).p("ABhBdQkoAKCejE");
	this.shape_29.setTransform(134.7,139.1);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f().s("#000000").ss(13,1,1).p("AhThKQhPCdEIgI");
	this.shape_30.setTransform(134.2,140.9);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f().s("#000000").ss(13,1,1).p("Ahzg4QABB2DmgF");
	this.shape_31.setTransform(132.7,142.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_24}]}).to({state:[{t:this.shape_24}]},14).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_27}]},1).to({state:[{t:this.shape_28}]},1).to({state:[{t:this.shape_29}]},1).to({state:[{t:this.shape_29}]},30).to({state:[{t:this.shape_30}]},1).to({state:[{t:this.shape_31}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_24}]},1).wait(21));

	// Symbol 12
	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f().s("#000000").ss(13,1,1).p("ACoAiQkDishLC7");
	this.shape_32.setTransform(127.5,142.9);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f().s("#000000").ss(13,1,1).p("AidgNQA9haD+CW");
	this.shape_33.setTransform(128.4,141.5);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f().s("#000000").ss(13,1,1).p("AiUhEQAuAJD7CA");
	this.shape_34.setTransform(129.3,139.3);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f().s("#000000").ss(13,1,1).p("AhohPQg7CGEWAZ");
	this.shape_35.setTransform(132.7,138.2);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f().s("#000000").ss(13,1,1).p("AhEhZQh5DSElgl");
	this.shape_36.setTransform(133.8,137.9);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f().s("#000000").ss(13,1,1).p("ABmBTQkrA6COjr");
	this.shape_37.setTransform(134,137.9);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f().s("#000000").ss(13,1,1).p("AhchSQhQCgEbAF");
	this.shape_38.setTransform(133.1,138);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f().s("#000000").ss(13,1,1).p("Ah8hLQgRBUELBD");
	this.shape_39.setTransform(131.6,138.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_32}]}).to({state:[{t:this.shape_32}]},14).to({state:[{t:this.shape_33}]},1).to({state:[{t:this.shape_34}]},1).to({state:[{t:this.shape_35}]},1).to({state:[{t:this.shape_36}]},1).to({state:[{t:this.shape_37}]},1).to({state:[{t:this.shape_37}]},30).to({state:[{t:this.shape_38}]},1).to({state:[{t:this.shape_39}]},1).to({state:[{t:this.shape_34}]},1).to({state:[{t:this.shape_33}]},1).to({state:[{t:this.shape_32}]},1).wait(21));

	// Symbol 14
	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f().s("#000000").ss(13,1,1).p("AAVB/QhCiwAohN");
	this.shape_40.setTransform(142.4,134.4);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f().s("#000000").ss(13,1,1).p("AgWh3QgGA9AzCy");
	this.shape_41.setTransform(142.2,135.1);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f().s("#000000").ss(13,1,1).p("AgghwQAcAtAlC1");
	this.shape_42.setTransform(141.3,135.7);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f().s("#000000").ss(13,1,1).p("Ag0hiQBTAJAWC8");
	this.shape_43.setTransform(139.2,137.2);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f().s("#000000").ss(13,1,1).p("AhBhYQB2gNANC/");
	this.shape_44.setTransform(137.9,138);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f().s("#000000").ss(13,1,1).p("ABGBYQgKjBiBAU");
	this.shape_45.setTransform(137.5,138.3);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.f().s("#000000").ss(13,1,1).p("Ag5hfQBfACAUC9");
	this.shape_46.setTransform(138.7,137.5);

	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.f().s("#000000").ss(13,1,1).p("AgshoQA8AXAdC5");
	this.shape_47.setTransform(140,136.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_40}]}).to({state:[{t:this.shape_40}]},14).to({state:[{t:this.shape_41}]},1).to({state:[{t:this.shape_42}]},1).to({state:[{t:this.shape_43}]},1).to({state:[{t:this.shape_44}]},1).to({state:[{t:this.shape_45}]},1).to({state:[{t:this.shape_45}]},30).to({state:[{t:this.shape_46}]},1).to({state:[{t:this.shape_47}]},1).to({state:[{t:this.shape_42}]},1).to({state:[{t:this.shape_41}]},1).to({state:[{t:this.shape_40}]},1).wait(21));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,175,186);


(lib.Symbol10 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.Symbol9();
	this.instance.parent = this;
	this.instance.setTransform(22.1,21,1,1,0,0,0,14.3,3.6);

	this.instance_1 = new lib.Symbol8();
	this.instance_1.parent = this;
	this.instance_1.setTransform(19.5,12.2,1,1,0,0,0,11.7,12.2);

	this.instance_2 = new lib.Symbol7();
	this.instance_2.parent = this;
	this.instance_2.setTransform(4.4,13.2,1,1,0,0,0,4.4,9.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_2},{t:this.instance_1},{t:this.instance}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol10, new cjs.Rectangle(-6.5,-6.5,49.4,37.5), null);


(lib.Symbol3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 6
	this.instance = new lib.Symbol10();
	this.instance.parent = this;
	this.instance.setTransform(369.7,130.4,1,1,0,0,0,6,25.9);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(19).to({rotation:75,x:339.7,y:199.4},5,cjs.Ease.get(1)).to({rotation:0,x:369.7,y:130.4},6,cjs.Ease.get(-1)).to({rotation:75,x:339.7,y:199.4},6,cjs.Ease.get(1)).to({rotation:0,x:369.7,y:130.4},7,cjs.Ease.get(-1)).wait(32));

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(13,1,1).p("AJxCIQkgC5k/h7Qkph0lZlI");
	this.shape.setTransform(307.2,116.9);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(13,1,1).p("Ao6lBQEeFxERCbQEiCmEkhC");
	this.shape_1.setTransform(300.6,125.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(13,1,1).p("AoQmVQDwGQD9C6QENDHEnAa");
	this.shape_2.setTransform(295.6,134.1);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(13,1,1).p("AnynYQDQGmDvDQQD9DeEpBd");
	this.shape_3.setTransform(291.9,141.2);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(13,1,1).p("AngoBQC8G0DmDdQD1DtEqCF");
	this.shape_4.setTransform(289.8,145.5);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(13,1,1).p("AHbIPQkqiSjyjxQjjjii2m4");
	this.shape_5.setTransform(289,146.9);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(13,1,1).p("AneoEQC7G1DlDeQDzDuEqCJ");
	this.shape_6.setTransform(289.5,146);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(13,1,1).p("AnrnoQDIGsDrDVQD6DkEpBt");
	this.shape_7.setTransform(291.1,143);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(13,1,1).p("An/m6QDeGcD1DGQEFDUEnA/");
	this.shape_8.setTransform(293.6,138);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(13,1,1).p("Aodl5QD+GGEDCxQEUC9ElgB");
	this.shape_9.setTransform(297.1,131.1);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(13,1,1).p("ApCkzQEnFqEUCVQEnCgEjhU");
	this.shape_10.setTransform(301.6,123.6);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(13,1,1).p("AndoHQC5G2DlDfQDzDvEqCL");
	this.shape_11.setTransform(289.4,146.2);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(13,1,1).p("AnmnyQDEGvDoDYQD4DnEpB3");
	this.shape_12.setTransform(290.5,144);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(13,1,1).p("An2nQQDUGkDwDNQEADbEpBV");
	this.shape_13.setTransform(292.4,140.4);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(13,1,1).p("AoLmgQDrGUD7C9QEKDLEnAl");
	this.shape_14.setTransform(294.9,135.3);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(13,1,1).p("AomlkQEIF/EICqQEYC1ElgY");
	this.shape_15.setTransform(298.3,128.9);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(13,1,1).p("ApIkqQEtFmEXCRQEqCaEjhh");
	this.shape_16.setTransform(302.4,122.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},19).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape}]},1).wait(32));

	// Layer 4
	this.instance_1 = new lib.Symbol15();
	this.instance_1.parent = this;
	this.instance_1.setTransform(4.1,5.3,1,1,0,0,0,145.6,151.3);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(19).to({rotation:35.7,x:22.2,y:-25.9},5,cjs.Ease.get(1)).to({rotation:-8.2,x:9.3,y:-11.9},6,cjs.Ease.get(-1)).to({rotation:35.7,x:22.2,y:-25.9},6,cjs.Ease.get(1)).to({rotation:0,x:4.1,y:5.3},7,cjs.Ease.get(-1)).wait(32));

	// Layer 1
	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(13,1,1).p("AIPEfQkpCDk+kAQh0hdiAiYQg6hFiIiz");
	this.shape_17.setTransform(52.7,33.2);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#000000").ss(13,1,1).p("AnqlvQB1C9AyBJQBxCiBqBnQEiEbExhm");
	this.shape_18.setTransform(57.3,27.3);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#000000").ss(13,1,1).p("AnPmNQBnDGAtBMQBkCqBjBvQEMEvE4hP");
	this.shape_19.setTransform(60.9,22.9);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#000000").ss(13,1,1).p("Am7mjQBdDLAoBPQBcCvBdB1QD9E+E8g/");
	this.shape_20.setTransform(63.5,19.8);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#000000").ss(13,1,1).p("AmwmxQBXDPAnBQQBWCzBZB3QD0FIFAg2");
	this.shape_21.setTransform(65,18);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#000000").ss(13,1,1).p("AGsGwQlAAyjxlKQhYh5hUi0QgmhRhVjQ");
	this.shape_22.setTransform(65.5,17.4);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#000000").ss(13,1,1).p("Amtm0QBVDQAmBRQBVC0BZB4QDyFJFAg0");
	this.shape_23.setTransform(65.3,17.6);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#000000").ss(13,1,1).p("AmymvQBYDPAnBQQBXCyBaB3QD2FGE/g3");
	this.shape_24.setTransform(64.7,18.2);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f().s("#000000").ss(13,1,1).p("Am6mnQBcDMApBQQBaCwBcB2QD8FAE+g9");
	this.shape_25.setTransform(63.7,19.3);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f().s("#000000").ss(13,1,1).p("AnFmcQBhDKArBOQBfCuBgBzQEEE5E7hG");
	this.shape_26.setTransform(62.3,20.8);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f().s("#000000").ss(13,1,1).p("AnTmOQBoDGAuBNQBlCrBkBvQEOEwE6hR");
	this.shape_27.setTransform(60.4,22.8);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f().s("#000000").ss(13,1,1).p("AHlFoQk2BekcklQhohrhtinQgxhLhxjC");
	this.shape_28.setTransform(58.2,25.2);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f().s("#000000").ss(13,1,1).p("AmumzQBWDPAmBRQBVCzBZB4QDyFJFBg0");
	this.shape_29.setTransform(65.3,17.7);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f().s("#000000").ss(13,1,1).p("Am0msQBZDNAoBQQBYCyBaB2QD3FEE+g4");
	this.shape_30.setTransform(64.5,18.6);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f().s("#000000").ss(13,1,1).p("Am+mhQBeDLAqBPQBcCvBeBzQD/E9E8hB");
	this.shape_31.setTransform(63.2,20.2);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f().s("#000000").ss(13,1,1).p("AnMmRQBmDHAsBNQBjCqBhBwQEKEyE5hN");
	this.shape_32.setTransform(61.3,22.4);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f().s("#000000").ss(13,1,1).p("Andl9QBuDBAwBLQBrCmBnBqQEXElE1hc");
	this.shape_33.setTransform(59,25.2);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f().s("#000000").ss(13,1,1).p("An0llQB7C6A0BIQB1CgBtBkQEpEUEvhu");
	this.shape_34.setTransform(56.1,28.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_17}]}).to({state:[{t:this.shape_17}]},19).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_27}]},1).to({state:[{t:this.shape_28}]},1).to({state:[{t:this.shape_27}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_29}]},1).to({state:[{t:this.shape_30}]},1).to({state:[{t:this.shape_31}]},1).to({state:[{t:this.shape_32}]},1).to({state:[{t:this.shape_33}]},1).to({state:[{t:this.shape_34}]},1).to({state:[{t:this.shape_17}]},1).wait(32));

	// Symbol 4
	this.instance_2 = new lib.Symbol4();
	this.instance_2.parent = this;
	this.instance_2.setTransform(231.5,115.1,1,1,0,0,0,0,16.6);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(19).to({regY:16.5,rotation:26.5,x:231.6,y:115},5,cjs.Ease.get(1)).to({regY:16.6,rotation:0,x:231.5,y:115.1},6,cjs.Ease.get(-1)).to({regY:16.5,rotation:26.5,x:231.6,y:115},6,cjs.Ease.get(1)).to({regY:16.6,rotation:0,x:231.5,y:115.1},7,cjs.Ease.get(-1)).wait(32));

	// Symbol 5
	this.instance_3 = new lib.Symbol5();
	this.instance_3.parent = this;
	this.instance_3.setTransform(94.5,82,1,1,0,0,0,171,66.5);

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(19).to({regX:171.1,rotation:18.2,x:94.6,y:81.9},5,cjs.Ease.get(1)).to({regY:66.6,rotation:10.8,x:94.5,y:82},6,cjs.Ease.get(-1)).to({regY:66.5,rotation:18.5,x:94.6,y:81.9},6,cjs.Ease.get(1)).to({regX:171,rotation:0,x:94.5,y:82},7,cjs.Ease.get(-1)).wait(32));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-141.5,-146,549,333.5);


(lib.Symbol1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#1B1814").ss(11,1,1).p("AkLhfQBDDEDRgGQBWgDBJgrQBKgtAahG");
	this.shape.setTransform(156,-52.6);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(75));

	// Layer 3
	this.instance = new lib.Symbol2_1();
	this.instance.parent = this;
	this.instance.setTransform(137.2,-49.4,2.017,1.042,0,46.1,51.9,5.4,7.5);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(12).to({_off:false},0).to({scaleY:2.02,guide:{path:[137.2,-49.3,137.5,-49,137.9,-48.7]}},4).wait(1).to({regX:4.3,regY:4.3,scaleX:2.02,scaleY:2.02,skewX:45.7,skewY:51.5,x:141.4,y:-54.8},0).wait(1).to({scaleX:2.01,scaleY:2.02,skewX:44.8,skewY:50.4,x:141.8,y:-54.4},0).wait(1).to({scaleX:2.01,scaleY:2.02,skewX:43.2,skewY:48.6,x:142.7,y:-53.7},0).wait(1).to({scaleX:2,scaleY:2.03,skewX:40.7,skewY:45.8,x:144.2,y:-52.8},0).wait(1).to({scaleX:2,scaleY:2.04,skewX:37.3,skewY:41.9,x:146.4,y:-51.7},0).wait(1).to({scaleX:1.98,scaleY:2.05,skewX:32.8,skewY:36.8,x:149.5,y:-50.7},0).wait(1).to({scaleX:1.97,scaleY:2.06,skewX:27.4,skewY:30.6,x:153.3,y:-50.1},0).wait(1).to({scaleX:1.96,scaleY:2.08,skewX:21.5,skewY:23.9,x:157.6},0).wait(1).to({scaleX:1.94,scaleY:2.09,skewX:16,skewY:17.6,x:161.1,y:-50.5},0).wait(1).to({scaleX:1.93,scaleY:2.1,skewX:11.4,skewY:12.4,x:164,y:-51.2},0).wait(1).to({scaleX:1.92,scaleY:2.11,skewX:8,skewY:8.6,x:166.1,y:-52},0).wait(1).to({scaleX:1.92,scaleY:2.11,skewX:5.8,skewY:6,x:167.5,y:-52.6},0).wait(1).to({scaleX:1.91,scaleY:2.12,skewX:4.5,skewY:4.6,x:168.2,y:-53},0).wait(1).to({regX:5.4,regY:7.5,scaleX:1.91,scaleY:2.12,rotation:4,skewX:0,skewY:0,x:170.2,y:-46.3},0).wait(7).to({regX:4.3,regY:4.3,scaleX:1.91,scaleY:2.12,rotation:0,skewX:4.6,skewY:4.7,x:168.2,y:-52.9},0).wait(1).to({scaleX:1.92,scaleY:2.11,skewX:6,skewY:6.2,x:167.3,y:-52.5},0).wait(1).to({scaleX:1.92,scaleY:2.11,skewX:8.3,skewY:8.8,x:165.8,y:-51.9},0).wait(1).to({scaleX:1.93,scaleY:2.1,skewX:11.5,skewY:12.6,x:163.6,y:-51.1},0).wait(1).to({scaleX:1.94,scaleY:2.09,skewX:15.7,skewY:17.4,x:160.6,y:-50.4},0).wait(1).to({scaleX:1.95,scaleY:2.08,skewX:20.7,skewY:23,x:157,y:-50},0).wait(1).to({scaleX:1.97,scaleY:2.06,skewX:26.1,skewY:29.1,x:153.3,y:-50.1},0).wait(1).to({scaleX:1.98,scaleY:2.05,skewX:31.2,skewY:35,x:150.1,y:-50.5},0).wait(1).to({scaleX:1.99,scaleY:2.04,skewX:35.9,skewY:40.2,x:147.3,y:-51.3},0).wait(1).to({scaleX:2,scaleY:2.03,skewX:39.6,skewY:44.5,x:145.1,y:-52.2},0).wait(1).to({scaleX:2.01,scaleY:2.03,skewX:42.5,skewY:47.7,x:143.3,y:-53.2},0).wait(1).to({scaleX:2.01,scaleY:2.02,skewX:44.4,skewY:50,x:142.1,y:-54.1},0).wait(1).to({scaleX:2.02,scaleY:2.02,skewX:45.6,skewY:51.3,x:141.5,y:-54.7},0).wait(1).to({regX:5.4,regY:7.5,scaleX:2.02,scaleY:2.02,skewX:46.1,skewY:51.9,x:137.9,y:-48.7},0).to({regX:5.5,regY:7.4,scaleY:1.23,x:135,y:-45.5},4).to({_off:true},1).wait(20));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(123.8,-67.6,64.6,30.1);


// stage content:
(lib.Herbarium = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 11
	this.instance = new lib.Symbol2();
	this.instance.parent = this;
	this.instance.setTransform(817.8,234.3,1,1,0,0,0,56.6,23.7);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(75));

	// Layer 9
	this.instance_1 = new lib.Symbol1();
	this.instance_1.parent = this;
	this.instance_1.setTransform(811.4,282.1,1.113,1.069,13,0,0,156.2,-52.6);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(75));

	// Layer 3
	this.instance_2 = new lib.pack();
	this.instance_2.parent = this;
	this.instance_2.setTransform(667,106);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(75));

	// Layer 10
	this.instance_3 = new lib.Symbol3();
	this.instance_3.parent = this;
	this.instance_3.setTransform(685.9,261.9,1,1,0,0,0,52.6,33.1);

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(75));

	// Layer 4
	this.instance_4 = new lib.herbs();
	this.instance_4.parent = this;
	this.instance_4.setTransform(437,216);

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(75));

	// Layer 2
	this.instance_5 = new lib.bg_part1();
	this.instance_5.parent = this;
	this.instance_5.setTransform(349,420);

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(75));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(540,-10.9,1238.7,1030.9);
// library properties:
lib.properties = {
	id: 'E5E957A33CF1784BA976B64B7A724477',
	width: 1080,
	height: 680,
	fps: 30,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: [
		{src:"assets/images/herbs26.png", id:"herbs"},
		{src:"assets/images/leaf126.png", id:"leaf1"},
		{src:"assets/images/pack26.png", id:"pack"},
		{src:"assets/images/shadow126.png", id:"shadow1"},
		{src:"assets/images/shadow226.png", id:"shadow2"}
	],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['E5E957A33CF1784BA976B64B7A724477'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}



})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;

$(function () {
    var canvas, stage, exportRoot, anim_container, dom_overlay_container, fnStartAnimation;
    function init() {
        canvas = document.getElementById("canvas26");
        anim_container = document.getElementById("animation_container26");
        dom_overlay_container = document.getElementById("dom_overlay_container26");
        var comp=AdobeAn.getComposition("E5E957A33CF1784BA976B64B7A724477");
        var lib=comp.getLibrary();
        createjs.MotionGuidePlugin.install();
        var loader = new createjs.LoadQueue(false);
        loader.addEventListener("fileload", function(evt){handleFileLoad(evt,comp)});
        loader.addEventListener("complete", function(evt){handleComplete(evt,comp)});
        var lib=comp.getLibrary();
        loader.loadManifest(lib.properties.manifest);
    }
    function handleFileLoad(evt, comp) {
        var images=comp.getImages();
        if (evt && (evt.item.type == "image")) { images[evt.item.id] = evt.result; }
    }
    function handleComplete(evt,comp) {
        //This function is always called, irrespective of the content. You can use the variable "stage" after it is created in token create_stage.
        var lib=comp.getLibrary();
        var ss=comp.getSpriteSheet();
        var queue = evt.target;
        var ssMetadata = lib.ssMetadata;
        for(i=0; i<ssMetadata.length; i++) {
            ss[ssMetadata[i].name] = new createjs.SpriteSheet( {"images": [queue.getResult(ssMetadata[i].name)], "frames": ssMetadata[i].frames} )
        }
        exportRoot = new lib.Herbarium();
        stage = new lib.Stage(canvas);
        //Registers the "tick" event listener.
        fnStartAnimation = function() {
            stage.addChild(exportRoot);
            createjs.Ticker.setFPS(lib.properties.fps);
            createjs.Ticker.addEventListener("tick", stage);
        }
        //Code to support hidpi screens and responsive scaling.
        function makeResponsive(isResp, respDim, isScale, scaleType) {
            var lastW, lastH, lastS=1;
            window.addEventListener('resize', resizeCanvas);
            resizeCanvas();
            function resizeCanvas() {
                var w = lib.properties.width, h = lib.properties.height;
                var iw = window.innerWidth, ih=window.innerHeight;
                var pRatio = window.devicePixelRatio || 1, xRatio=iw/w, yRatio=ih/h, sRatio=1;
                if(isResp) {
                    if((respDim=='width'&&lastW==iw) || (respDim=='height'&&lastH==ih)) {
                        sRatio = lastS;
                    }
                    else if(!isScale) {
                        if(iw<w || ih<h)
                            sRatio = Math.min(xRatio, yRatio);
                    }
                    else if(scaleType==1) {
                        sRatio = Math.min(xRatio, yRatio);
                    }
                    else if(scaleType==2) {
                        sRatio = Math.max(xRatio, yRatio);
                    }
                }
                canvas.width = w*pRatio*sRatio;
                canvas.height = h*pRatio*sRatio;
                canvas.style.width = dom_overlay_container.style.width = anim_container.style.width =  w*sRatio+'px';
                canvas.style.height = anim_container.style.height = dom_overlay_container.style.height = h*sRatio+'px';
                stage.scaleX = pRatio*sRatio;
                stage.scaleY = pRatio*sRatio;
                lastW = iw; lastH = ih; lastS = sRatio;
                stage.tickOnUpdate = false;
                stage.update();
                stage.tickOnUpdate = true;
            }
        }
        makeResponsive(true,'both',true,2);
        AdobeAn.compositionLoaded(lib.properties.id);
        fnStartAnimation();
    }
    init();
});