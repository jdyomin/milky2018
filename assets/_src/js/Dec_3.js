(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [];


// symbols:



(lib.bg = function() {
	this.initialize(img.bg);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,1391,957);


(lib.cookie = function() {
	this.initialize(img.cookie);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,379,435);


(lib.cookie_shadow = function() {
	this.initialize(img.cookie_shadow);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,237,16);


(lib.pack = function() {
	this.initialize(img.pack);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,313,498);// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.Symbol8 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.cookie();
	this.instance.parent = this;
	this.instance.setTransform(-189.5,-217.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol8, new cjs.Rectangle(-189.5,-217.5,379,435), null);


(lib.Symbol3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#F6706E").s().p("AhTBFQgUgYALgmQALgkAigcQAigdAmgDQAngDAUAYQAUAYgLAmQgKAkgiAdQgjAcgmADIgHAAQgiAAgSgVg");
	this.shape.setTransform(-0.0356,0.0451);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol3, new cjs.Rectangle(-9.7,-9,19.4,18.1), null);


(lib.Symbol2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.cookie_shadow();
	this.instance.parent = this;
	this.instance.setTransform(-118.5,-8);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol2, new cjs.Rectangle(-118.5,-8,237,16), null);


(lib.Symbol1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.shape = new cjs.Shape();
	this.shape.graphics.lf(["#FAFAFA","rgba(250,250,250,0)"],[0.918,1],-1.8,-141.9,2,146.1).s().p("EhvXAWcMAAAgs3MDevAAAMAAAAs3g");
	this.shape.setTransform(-4,-590.8);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	// Layer_1
	this.instance = new lib.bg();
	this.instance.parent = this;
	this.instance.setTransform(-695.5,-478.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol1, new cjs.Rectangle(-716.8,-734.4,1425.6,1212.9), null);


(lib.Symbol_10_Layer_11 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_11
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(11,1,1).p("Aj1hPQAQBbBIAzQBDAuBXgEQBWgEBDg1QBJg4AXhh");
	this.shape.setTransform(-36,38.4658);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(100));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_10_Layer_9 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_9
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(10,1,1).p("AiZgVQAPAWAkAWQAlAVAuAFQB2ALA3ht");
	this.shape.setTransform(-7.275,3.9);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(10,1,1).p("AiZgVQAPAWAkAWQAlAWAuAEQB2ALA3ht");
	this.shape_1.setTransform(-7.275,3.5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(10,1,1).p("AiZgVQAPAXAkAVQAlAWAuAEQB2ALA3ht");
	this.shape_2.setTransform(-7.275,3.2);
	this.shape_2._off = true;

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(10,1,1).p("AiZgVQAPAWAkAVQAlAWAuAFQB2ALA3ht");
	this.shape_3.setTransform(-7.275,2.3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},35).to({state:[{t:this.shape_1,p:{y:3.5}}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_1,p:{y:2}}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},40).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).wait(5));
	this.timeline.addTween(cjs.Tween.get(this.shape).wait(35).to({_off:true},1).wait(2).to({_off:false,y:2.9},0).to({_off:true},1).wait(1).to({_off:false,y:2.4},0).to({_off:true},1).wait(3).to({_off:false,y:1.9},0).wait(41).to({_off:true},1).wait(2).to({_off:false,y:2.9},0).to({_off:true},1).wait(1).to({_off:false,y:3.4},0).to({_off:true},1).wait(3).to({_off:false,y:3.9},0).wait(6));
	this.timeline.addTween(cjs.Tween.get(this.shape_2).wait(37).to({_off:false},0).to({_off:true},1).wait(1).to({_off:false,y:2.6},0).to({_off:true},1).wait(1).to({_off:false,y:2.2},0).wait(1).to({y:2.1},0).to({_off:true},1).wait(44).to({_off:false,y:2.6},0).to({_off:true},1).wait(1).to({_off:false,y:3.2},0).to({_off:true},1).wait(1).to({_off:false,y:3.6},0).wait(1).to({y:3.7},0).wait(1).to({y:3.8},0).to({_off:true},1).wait(6));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_10_Layer_8 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_8
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(10,1,1).p("AiZgVQAPAWAkAWQAlAVAuAFQB2ALA3ht");
	this.shape.setTransform(-68.925,3.9);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(10,1,1).p("AiZgVQAPAWAkAWQAlAWAuAEQB2ALA3ht");
	this.shape_1.setTransform(-68.925,3.5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(10,1,1).p("AiZgVQAPAXAkAVQAlAWAuAEQB2ALA3ht");
	this.shape_2.setTransform(-68.925,3.2);
	this.shape_2._off = true;

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(10,1,1).p("AiZgVQAPAWAkAVQAlAWAuAFQB2ALA3ht");
	this.shape_3.setTransform(-68.925,2.3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},35).to({state:[{t:this.shape_1,p:{y:3.5}}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_1,p:{y:2}}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},40).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).wait(5));
	this.timeline.addTween(cjs.Tween.get(this.shape).wait(35).to({_off:true},1).wait(2).to({_off:false,y:2.9},0).to({_off:true},1).wait(1).to({_off:false,y:2.4},0).to({_off:true},1).wait(3).to({_off:false,y:1.9},0).wait(41).to({_off:true},1).wait(2).to({_off:false,y:2.9},0).to({_off:true},1).wait(1).to({_off:false,y:3.4},0).to({_off:true},1).wait(3).to({_off:false,y:3.9},0).wait(6));
	this.timeline.addTween(cjs.Tween.get(this.shape_2).wait(37).to({_off:false},0).to({_off:true},1).wait(1).to({_off:false,y:2.6},0).to({_off:true},1).wait(1).to({_off:false,y:2.2},0).wait(1).to({y:2.1},0).to({_off:true},1).wait(44).to({_off:false,y:2.6},0).to({_off:true},1).wait(1).to({_off:false,y:3.2},0).to({_off:true},1).wait(1).to({_off:false,y:3.6},0).wait(1).to({y:3.7},0).wait(1).to({y:3.8},0).to({_off:true},1).wait(6));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_10_Layer_7 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_7
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AhnBrQgrgsAAg/QAAg+ArgtQArgsA8AAQA9AAArAsQArAtAAA+QAAA/grAsQgrAsg9AAQg8AAgrgsg");
	this.shape.setTransform(-8.1,-10.2);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(100));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_10_Layer_5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_5
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#C7C7C7").s().p("AhjAIQApgpA8AAQA9AAAjAiQAjAjgSgNQgSgNglgQQglgRgcACQgdACgPAJQgPAJgmAZQgPAKgDAAQgEAAAZgag");
	this.shape.setTransform(-8.3275,-21.9279);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#C7C7C7").s().p("AiTA1QADgdArgrQArgsA8AAQA9AAArAsQArArgBAXQgBAXg3gxQg3gvg2AKQg4AKgmAsQgYAagIAAQgFAAABgLg");
	this.shape_1.setTransform(-8.3097,-18.9359);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#C7C7C7").s().p("ABwBLQgkgphAAHQg/AHgwAgQgxAgACg6QABg4ArgtQArgsA8AAQA9AAArAsQArAtAAA6QgBAkgMAAQgJAAgOgRg");
	this.shape_2.setTransform(-8.2036,-16.1799);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#C7C7C7").s().p("AABB7Qg9gFgrgNQgrgOAAg/QAAg+ArgsQArgsA8AAQA9AAArAsQArAsAAA+QAAA/grASQgiAOguAAIgXAAg");
	this.shape_3.setTransform(-8.1,-13.005);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#C7C7C7").s().p("AhnBrQgrgsAAg/QAAg+ArgtQArgsA8AAQA9AAArAsQArAtAAA+QAAA/grAsQgrAsg9AAQg8AAgrgsg");
	this.shape_4.setTransform(-8.1,-10.2);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#C7C7C7").s().p("AiTA1QADgdArgrQArgsA8AAQA9AAArAsQArArgBAXQgBAXg3gxQg3gvg3AKQg3AKgnAsQgXAagIAAQgFAAABgLg");
	this.shape_5.setTransform(-8.3213,-18.9323);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#C7C7C7").s().p("ABvBLQgjgphAAHQg/AHgxAgQgwAgABg6QACg4ArgtQArgsA8AAQA9AAArAsQArAtAAA6QgBAkgMAAQgJAAgPgRg");
	this.shape_6.setTransform(-8.2156,-16.1805);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#C7C7C7").s().p("AAAB7Qg7gFgsgNQgrgOAAg/QAAg+ArgsQArgsA8AAQA9AAArAsQArAsAAA+QAAA/grASQgiAOgtAAIgZAAg");
	this.shape_7.setTransform(-8.1,-13.0028);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape,p:{x:-8.3275,y:-21.9279}}]},12).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape,p:{x:-8.3275,y:-21.9279}}]},1).to({state:[]},1).to({state:[{t:this.shape,p:{x:-8.3306,y:-21.9285}}]},44).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape,p:{x:-8.3306,y:-21.9285}}]},1).to({state:[]},1).wait(24));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_10_Layer_4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_4
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(11,1,1).p("AhMgUQAXAgA2AHQAuAGAegW");
	this.shape.setTransform(-11.65,25.6132);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(100));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_10_Layer_3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_3
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AhnBrQgrgsAAg/QAAg+ArgtQArgrA8gBQA9ABArArQArAtAAA+QAAA/grAsQgrAtg9AAQg8AAgrgtg");
	this.shape.setTransform(-68.7,-10.3);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(100));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_10_Layer_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#C7C7C7").s().p("AhjAIQApgpA8AAQA9AAAjAiQAjAjgSgNQgSgNglgQQglgRgcACQgdACgPAJQgPAJgmAZQgPAKgDAAQgEAAAZgag");
	this.shape.setTransform(-68.9275,-22.0279);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#C7C7C7").s().p("AiTA1QADgdArgrQArgsA8AAQA9AAArAsQArArgBAXQgBAXg3gxQg3gvg2AKQg4AKgmAsQgYAagIAAQgFAAABgLg");
	this.shape_1.setTransform(-68.9097,-19.0359);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#C7C7C7").s().p("ABwBLQgkgphAAHQg/AHgwAgQgxAgACg6QABg4ArgtQArgsA8AAQA9AAArAsQArAtAAA6QgBAkgMAAQgJAAgOgRg");
	this.shape_2.setTransform(-68.8036,-16.2799);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#C7C7C7").s().p("AAAB7Qg7gFgrgNQgsgOAAg/QAAg+ArgsQArgsA8AAQA9AAArAsQArAsAAA+QAAA/gqASQgjAOguAAIgYAAg");
	this.shape_3.setTransform(-68.7,-13.105);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#C7C7C7").s().p("AhnBrQgrgsAAg/QAAg+ArgtQArgrA8gBQA9ABArArQArAtAAA+QAAA/grAsQgrAtg9AAQg8AAgrgtg");
	this.shape_4.setTransform(-68.7,-10.3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape}]},12).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape}]},1).to({state:[]},1).to({state:[{t:this.shape}]},44).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape}]},1).to({state:[]},1).wait(24));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_7_Layer_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(12,1,1).p("AKMnDQtSEinFJl");
	this.shape.setTransform(0,0.025);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(12,1,1).p("AqaGRQILnLMqlW");
	this.shape_1.setTransform(-1.575,-5.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(12,1,1).p("AqlFwQI5lpMSl2");
	this.shape_2.setTransform(-2.6,-8.35);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(12,1,1).p("AqsFaQJYklMBmO");
	this.shape_3.setTransform(-3.3,-10.6);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(12,1,1).p("AqxFJQJvjzL0me");
	this.shape_4.setTransform(-3.8,-12.25);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(12,1,1).p("Aq1E9QKAjOLrmr");
	this.shape_5.setTransform(-4.2,-13.5);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(12,1,1).p("Aq4EzQKOiwLjm1");
	this.shape_6.setTransform(-4.5,-14.475);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(12,1,1).p("Aq6ErQKYiZLdm8");
	this.shape_7.setTransform(-4.725,-15.25);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(12,1,1).p("Aq8ElQKgiHLZnC");
	this.shape_8.setTransform(-4.925,-15.825);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(12,1,1).p("Aq9EhQKmh6LVnH");
	this.shape_9.setTransform(-5.05,-16.3);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(12,1,1).p("Aq+EdQKrhuLSnL");
	this.shape_10.setTransform(-5.175,-16.65);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(12,1,1).p("Aq/EaQKvhmLQnN");
	this.shape_11.setTransform(-5.25,-16.925);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(12,1,1).p("ArAEZQKyhhLPnQ");
	this.shape_12.setTransform(-5.3,-17.1);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(12,1,1).p("ArAEXQKzhdLOnQ");
	this.shape_13.setTransform(-5.35,-17.225);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(12,1,1).p("ArAEXQK0hbLNnS");
	this.shape_14.setTransform(-5.375,-17.3);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(12,1,1).p("ALBkVQrNHRq0Ba");
	this.shape_15.setTransform(-5.375,-17.325);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(12,1,1).p("Aq2E3QKHi9Lmmw");
	this.shape_16.setTransform(-4.35,-14.025);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(12,1,1).p("AqtFVQJekXL9mS");
	this.shape_17.setTransform(-3.45,-11.075);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#000000").ss(12,1,1).p("AqlFvQI6lmMRl3");
	this.shape_18.setTransform(-2.625,-8.475);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#000000").ss(12,1,1).p("AqeGFQIbmoMilh");
	this.shape_19.setTransform(-1.925,-6.225);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#000000").ss(12,1,1).p("AqYGYQIBniMwlN");
	this.shape_20.setTransform(-1.35,-4.3);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#000000").ss(12,1,1).p("AqTGoQHroRM8k+");
	this.shape_21.setTransform(-0.85,-2.75);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#000000").ss(12,1,1).p("AqPG0QHao2NFkx");
	this.shape_22.setTransform(-0.475,-1.525);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#000000").ss(12,1,1).p("AqNG9QHPpQNMkp");
	this.shape_23.setTransform(-0.225,-0.675);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#000000").ss(12,1,1).p("AqLHCQHHpgNQkj");
	this.shape_24.setTransform(-0.05,-0.15);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},4).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_15}]},10).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape}]},1).wait(61));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_5_Layer_3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_3
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(12,1,1).p("ABFg1QiAC2gJh/");
	this.shape.setTransform(7.375,16.4139);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(12,1,1).p("AhbAPQA0BjCDil");
	this.shape_1.setTransform(5.075,16.1992);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(12,1,1).p("AhpAXQBOBSCFib");
	this.shape_2.setTransform(3.625,16.0906);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(12,1,1).p("Ah0AcQBjBGCGiT");
	this.shape_3.setTransform(2.6,16.0239);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(12,1,1).p("Ah7AgQBwA9CHiO");
	this.shape_4.setTransform(1.85,15.9893);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(12,1,1).p("AiBAiQB6A2CJiJ");
	this.shape_5.setTransform(1.3,15.9594);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(12,1,1).p("AiFAkQCDAxCIiG");
	this.shape_6.setTransform(0.85,15.9524);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(12,1,1).p("AiJAmQCJAtCKiD");
	this.shape_7.setTransform(0.5,15.9454);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(12,1,1).p("AiLAnQCNAqCKiC");
	this.shape_8.setTransform(0.25,15.9507);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(12,1,1).p("AiNAoQCQAnCLiA");
	this.shape_9.setTransform(0.05,15.9527);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(12,1,1).p("AiPApQCUAlCLh+");
	this.shape_10.setTransform(-0.125,15.9411);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(12,1,1).p("AiQAqQCWAkCLh+");
	this.shape_11.setTransform(-0.25,15.9475);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(12,1,1).p("AiRAqQCXAjCMh9");
	this.shape_12.setTransform(-0.325,15.9458);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(12,1,1).p("AiRAqQCYAiCLh9");
	this.shape_13.setTransform(-0.375,15.9613);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(12,1,1).p("AiSAqQCZAiCMh9");
	this.shape_14.setTransform(-0.425,15.9529);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(12,1,1).p("ACTgxQiMB9iZgi");
	this.shape_15.setTransform(-0.425,15.9536);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(12,1,1).p("AiDAkQB+AzCJiI");
	this.shape_16.setTransform(1.05,15.9686);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(12,1,1).p("Ah2AdQBmBDCHiR");
	this.shape_17.setTransform(2.375,16.0038);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#000000").ss(12,1,1).p("AhqAXQBQBRCFia");
	this.shape_18.setTransform(3.55,16.09);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#000000").ss(12,1,1).p("AhgASQA9BdCEii");
	this.shape_19.setTransform(4.575,16.1676);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#000000").ss(12,1,1).p("AhXANQAtBnCCin");
	this.shape_20.setTransform(5.425,16.2288);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#000000").ss(12,1,1).p("AhQAJQAgBwCBit");
	this.shape_21.setTransform(6.125,16.293);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#000000").ss(12,1,1).p("AhLAGQAWB3CBix");
	this.shape_22.setTransform(6.675,16.343);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#000000").ss(12,1,1).p("AhHAEQAPB7CAi0");
	this.shape_23.setTransform(7.075,16.3763);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#000000").ss(12,1,1).p("AhFADQALB9CAi1");
	this.shape_24.setTransform(7.3,16.4069);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},4).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_15}]},10).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape}]},1).wait(61));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_5_Layer_2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(12,1,1).p("ADfADIm9gF");
	this.shape.setTransform(-14.375,8.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(12,1,1).p("AjWgEQDQARDdgN");
	this.shape_1.setTransform(-13.525,8.5186);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(12,1,1).p("AjRgHQDGAcDdgZ");
	this.shape_2.setTransform(-13,8.8521);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(12,1,1).p("AjNgIQC/AiDcgg");
	this.shape_3.setTransform(-12.625,9.0699);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(12,1,1).p("AjKgKQC5AoDcgm");
	this.shape_4.setTransform(-12.35,9.2506);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(12,1,1).p("AjIgKQC2ArDbgq");
	this.shape_5.setTransform(-12.15,9.3816);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(12,1,1).p("AjGgLQCzAuDbgt");
	this.shape_6.setTransform(-12,9.5001);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(12,1,1).p("AjFgLQCwAwDbgw");
	this.shape_7.setTransform(-11.875,9.5938);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(12,1,1).p("AjEgMQCuAzDbgy");
	this.shape_8.setTransform(-11.775,9.6438);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(12,1,1).p("AjEgMQCuA0Dbg0");
	this.shape_9.setTransform(-11.7,9.6813);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(12,1,1).p("AjDgMQCsA0Dbg0");
	this.shape_10.setTransform(-11.625,9.7375);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(12,1,1).p("AjCgNQCrA2Dbg2");
	this.shape_11.setTransform(-11.6,9.7625);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(12,1,1).p("AjCgNQCqA3Dbg3");
	this.shape_12.setTransform(-11.55,9.7875);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(12,1,1).p("ADDgNQjbA3iqg3");
	this.shape_13.setTransform(-11.525,9.8007);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(12,1,1).p("AjHgLQC0AtDbgs");
	this.shape_14.setTransform(-12.075,9.4626);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(12,1,1).p("AjMgJQC9AkDcgi");
	this.shape_15.setTransform(-12.55,9.1507);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(12,1,1).p("AjQgHQDFAcDcgZ");
	this.shape_16.setTransform(-12.975,8.8645);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(12,1,1).p("AjUgFQDNAVDcgR");
	this.shape_17.setTransform(-13.35,8.605);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#000000").ss(12,1,1).p("AjXgEQDSAQDdgM");
	this.shape_18.setTransform(-13.65,8.4276);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#000000").ss(12,1,1).p("AjagDQDXALDegH");
	this.shape_19.setTransform(-13.925,8.2674);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#000000").ss(12,1,1).p("AjcgCQDbAHDegD");
	this.shape_20.setTransform(-14.125,8.1548);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#000000").ss(12,1,1).p("AjdgCQDdAFDeAA");
	this.shape_21.setTransform(-14.25,8.1);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#000000").ss(12,1,1).p("AjegCQDeADDfAC");
	this.shape_22.setTransform(-14.35,8.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},4).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12,p:{x:-11.55,y:9.7875}}]},1).to({state:[{t:this.shape_12,p:{x:-11.55,y:9.7875}}]},1).to({state:[{t:this.shape_12,p:{x:-11.525,y:9.8}}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_13}]},10).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape}]},1).wait(61));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_5_Layer_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(12,1,1).p("ABcBOQAnj9jkCg");
	this.shape.setTransform(0.0082,0.0176);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(12,1,1).p("AhzgTQDTiBAUDY");
	this.shape_1.setTransform(-2.325,0.9952);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(12,1,1).p("AiAgWQDHhtA6DB");
	this.shape_2.setTransform(-3.625,1.6112);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(12,1,1).p("AiJgXQC/hfBUCw");
	this.shape_3.setTransform(-4.55,2.041);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(12,1,1).p("AiQgZQC5hVBoCk");
	this.shape_4.setTransform(-5.2,2.355);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(12,1,1).p("AiVgZQC1hOB2Cb");
	this.shape_5.setTransform(-5.725,2.5889);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(12,1,1).p("AiZgaQCxhICCCU");
	this.shape_6.setTransform(-6.1,2.7658);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(12,1,1).p("AicgbQCvhCCKCO");
	this.shape_7.setTransform(-6.4,2.8971);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(12,1,1).p("AiegbQCthACQCL");
	this.shape_8.setTransform(-6.65,3.0068);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(12,1,1).p("AiggbQCrg9CWCH");
	this.shape_9.setTransform(-6.825,3.0936);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(12,1,1).p("AihgcQCpg6CaCE");
	this.shape_10.setTransform(-6.975,3.1588);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(12,1,1).p("AiigcQCog5CdCD");
	this.shape_11.setTransform(-7.075,3.201);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(12,1,1).p("AijgcQCog3CfCB");
	this.shape_12.setTransform(-7.15,3.2447);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(12,1,1).p("AikgcQCog3ChCA");
	this.shape_13.setTransform(-7.225,3.2657);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(12,1,1).p("AikgcQCng2CiB/");
	this.shape_14.setTransform(-7.25,3.2761);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(12,1,1).p("AClAtQiih/inA2");
	this.shape_15.setTransform(-7.25,3.271);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(12,1,1).p("AiXgaQCzhKB8CX");
	this.shape_16.setTransform(-5.925,2.6775);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(12,1,1).p("AiLgYQC+hcBZCt");
	this.shape_17.setTransform(-4.75,2.131);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#000000").ss(12,1,1).p("AiBgVQDHhtA8DA");
	this.shape_18.setTransform(-3.7,1.6445);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#000000").ss(12,1,1).p("Ah3gUQDOh6AhDQ");
	this.shape_19.setTransform(-2.775,1.2116);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#000000").ss(12,1,1).p("AhwgSQDWiGALDd");
	this.shape_20.setTransform(-2.025,0.8566);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#000000").ss(12,1,1).p("AhqgRQDbiPgHDp");
	this.shape_21.setTransform(-1.3892,0.5475);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#000000").ss(12,1,1).p("AhlgQQDfiXgVDy");
	this.shape_22.setTransform(-0.81,0.3183);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#000000").ss(12,1,1).p("AhjgPQDiidgfD4");
	this.shape_23.setTransform(-0.3638,0.155);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#000000").ss(12,1,1).p("AhigPQDkifglD7");
	this.shape_24.setTransform(-0.0927,0.0532);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},4).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_15}]},10).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape}]},1).wait(61));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_4_Layer_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(12,1,1).p("AH3p2QBOErgZEMQgZEPiECxQiMC8jpAsQj+AxlNiD");
	this.shape.setTransform(0.0269,0.0071);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(12,1,1).p("Ao2HzQFaCFD6ghQDsgfCJirQCFikAZkPQAZkMhPkr");
	this.shape_1.setTransform(-0.4481,-2.489);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(12,1,1).p("Ao5HbQFjCID3gZQDtgWCIigQCFicAZkOQAZkNhPkq");
	this.shape_2.setTransform(-0.7481,-4.038);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(12,1,1).p("Ao7HKQFpCKD1gSQDugRCIiZQCEiVAZkPQAZkMhOkr");
	this.shape_3.setTransform(-0.9731,-5.0712);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(12,1,1).p("Ao9G9QFtCLD0gNQDvgMCHiUQCFiRAZkOQAZkNhPkq");
	this.shape_4.setTransform(-1.1231,-5.8194);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(12,1,1).p("Ao+G0QFwCLDzgJQDvgJCHiQQCFiNAZkQQAZkLhPkr");
	this.shape_5.setTransform(-1.2481,-6.3947);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(12,1,1).p("Ao/GtQFzCMDygHQDwgGCGiNQCFiLAZkPQAZkMhPkq");
	this.shape_6.setTransform(-1.3231,-6.8106);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(12,1,1).p("ApAGmQF1CNDxgEQDxgECFiLQCFiJAZkPQAZkMhOkq");
	this.shape_7.setTransform(-1.3981,-7.143);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(12,1,1).p("ApAGiQF2CNDxgDQDwgCCGiIQCFiIAZkPQAZkMhPkq");
	this.shape_8.setTransform(-1.4481,-7.4219);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(12,1,1).p("ApBGeQF4CNDwgBQDxgBCFiHQCFiGAZkQQAZkLhOkr");
	this.shape_9.setTransform(-1.4981,-7.5996);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(12,1,1).p("ApBGcQF4CNDxAAQDxAACFiGQCFiFAZkQQAZkMhPkq");
	this.shape_10.setTransform(-1.5231,-7.75);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(12,1,1).p("ApBGaQF4CNDxABQDxAACFiFQCFiEAZkQQAZkLhPkr");
	this.shape_11.setTransform(-1.5481,-7.8749);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(12,1,1).p("ApBGYQF5CODwABQDxABCFiFQCFiEAZkPQAZkMhOkq");
	this.shape_12.setTransform(-1.5731,-7.9496);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(12,1,1).p("ApCGXQF6CODwABQDxABCFiEQCFiDAZkQQAZkLhOkr");
	this.shape_13.setTransform(-1.5981,-7.9996);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(12,1,1).p("ApCGXQF6CODwABQDxABCFiDQCFiEAZkPQAZkMhOkq");
	this.shape_14.setTransform(-1.5981,-8.0492);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(12,1,1).p("AIHolQBOEqgZEMQgZEPiFCEQiECEjygCQjwgCl6iO");
	this.shape_15.setTransform(-1.5981,-8.0491);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(12,1,1).p("Ao/GwQFyCMDzgIQDvgHCGiPQCFiMAZkPQAZkMhOkq");
	this.shape_16.setTransform(-1.2981,-6.6283);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(12,1,1).p("Ao8HGQFqCKD1gQQDvgQCHiXQCFiUAZkPQAZkMhPkr");
	this.shape_17.setTransform(-1.0231,-5.2869);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#000000").ss(12,1,1).p("Ao5HaQFjCID3gYQDtgWCJigQCEibAZkPQAZkMhOkr");
	this.shape_18.setTransform(-0.7731,-4.095);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#000000").ss(12,1,1).p("Ao3HqQFdCHD5gfQDsgbCJioQCFihAZkOQAZkNhPkq");
	this.shape_19.setTransform(-0.5481,-3.0431);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#000000").ss(12,1,1).p("Ao1H4QFYCGD6gkQDsghCKiuQCEimAZkOQAZkNhOkq");
	this.shape_20.setTransform(-0.3731,-2.1177);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#000000").ss(12,1,1).p("Ao0IEQFVCED7goQDrgmCLiyQCEiqAZkPQAZkMhPkr");
	this.shape_21.setTransform(-0.2231,-1.3509);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#000000").ss(12,1,1).p("AozIMQFSCED8gsQDqgoCMi3QCEitAZkPQAZkMhPkr");
	this.shape_22.setTransform(-0.1231,-0.7734);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#000000").ss(12,1,1).p("AoyISQFPCDD9guQDqgrCMi5QCEiwAZkOQAZkNhPkq");
	this.shape_23.setTransform(-0.0481,-0.3409);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#000000").ss(12,1,1).p("AoyIWQFOCDD+gwQDpgsCMi7QCEixAZkPQAZkMhOkr");
	this.shape_24.setTransform(0.0019,-0.0796);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},4).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_15}]},10).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape}]},1).wait(61));

}).prototype = p = new cjs.MovieClip();


(lib.Scene_1_Layer_3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_3
	this.instance = new lib.pack();
	this.instance.parent = this;
	this.instance.setTransform(640.3,127);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Scene_1_Layer_3, null, null);


(lib.Symbol9 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AgpArQgRgRAAgaQAAgZARgRQARgTAYABQAYgBASATQARARAAAZQAAAagRARQgSATgYgBQgYABgRgTg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol9, new cjs.Rectangle(-5.9,-6.1,11.9,12.3), null);


(lib.Symbol7 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_99 = function() {
		this.___loopingOver___ = true;
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(99).call(this.frame_99).wait(1));

	// Layer_1_obj_
	this.Layer_1 = new lib.Symbol_7_Layer_1();
	this.Layer_1.name = "Layer_1";
	this.Layer_1.parent = this;
	this.Layer_1.depth = 0;
	this.Layer_1.isAttachedToCamera = 0
	this.Layer_1.isAttachedToMask = 0
	this.Layer_1.layerDepth = 0
	this.Layer_1.layerIndex = 0
	this.Layer_1.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_1).wait(100));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-81.9,-51.1,153.10000000000002,102.30000000000001);


(lib.Symbol6 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// cookie_png
	this.instance = new lib.Symbol8();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(5).to({y:-6.6316},0).wait(1).to({y:-10.8463},0).wait(1).to({y:-13.775},0).wait(1).to({y:-15.924},0).wait(1).to({y:-17.5536},0).wait(1).to({y:-18.8134},0).wait(1).to({y:-19.7969},0).wait(1).to({y:-20.5664},0).wait(1).to({y:-21.1653},0).wait(1).to({y:-21.6251},0).wait(1).to({y:-21.9694},0).wait(1).to({y:-22.2162},0).wait(1).to({y:-22.3797},0).wait(1).to({y:-22.4712},0).wait(1).to({y:-22.5},0).wait(10).to({y:0},10,cjs.Ease.get(1)).wait(61));

	// Symbol_2
	this.instance_1 = new lib.Symbol2();
	this.instance_1.parent = this;
	this.instance_1.setTransform(-43.2,188);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(5).to({scaleX:1.0536,scaleY:1.28,alpha:0.8146},0).wait(1).to({scaleX:1.0877,scaleY:1.458,alpha:0.6968},0).wait(1).to({scaleX:1.1113,scaleY:1.5816,alpha:0.615},0).wait(1).to({scaleX:1.1287,scaleY:1.6724,alpha:0.5549},0).wait(1).to({scaleX:1.1419,scaleY:1.7412,alpha:0.5094},0).wait(1).to({scaleX:1.1521,scaleY:1.7944,alpha:0.4741},0).wait(1).to({scaleX:1.16,scaleY:1.8359,alpha:0.4466},0).wait(1).to({scaleX:1.1662,scaleY:1.8684,alpha:0.4251},0).wait(1).to({scaleX:1.1711,scaleY:1.8937,alpha:0.4084},0).wait(1).to({scaleX:1.1748,scaleY:1.9131,alpha:0.3955},0).wait(1).to({scaleX:1.1776,scaleY:1.9276,alpha:0.3859},0).wait(1).to({scaleX:1.1796,scaleY:1.9381,alpha:0.379},0).wait(1).to({scaleX:1.1809,scaleY:1.945,alpha:0.3745},0).wait(1).to({scaleX:1.1816,scaleY:1.9488,alpha:0.3719},0).wait(1).to({scaleX:1.1819,scaleY:1.95,alpha:0.3711},0).wait(10).to({scaleX:1,scaleY:1,alpha:1},10,cjs.Ease.get(1)).wait(61));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-189.5,-240,379,457.5);


(lib.Symbol5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_99 = function() {
		this.___loopingOver___ = true;
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(99).call(this.frame_99).wait(1));

	// Layer_3_obj_
	this.Layer_3 = new lib.Symbol_5_Layer_3();
	this.Layer_3.name = "Layer_3";
	this.Layer_3.parent = this;
	this.Layer_3.setTransform(7.4,16.4,1,1,0,0,0,7.4,16.4);
	this.Layer_3.depth = 0;
	this.Layer_3.isAttachedToCamera = 0
	this.Layer_3.isAttachedToMask = 0
	this.Layer_3.layerDepth = 0
	this.Layer_3.layerIndex = 0
	this.Layer_3.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_3).wait(100));

	// Layer_2_obj_
	this.Layer_2 = new lib.Symbol_5_Layer_2();
	this.Layer_2.name = "Layer_2";
	this.Layer_2.parent = this;
	this.Layer_2.setTransform(-14.4,8.1,1,1,0,0,0,-14.4,8.1);
	this.Layer_2.depth = 0;
	this.Layer_2.isAttachedToCamera = 0
	this.Layer_2.isAttachedToMask = 0
	this.Layer_2.layerDepth = 0
	this.Layer_2.layerIndex = 1
	this.Layer_2.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_2).wait(100));

	// Layer_1_obj_
	this.Layer_1 = new lib.Symbol_5_Layer_1();
	this.Layer_1.name = "Layer_1";
	this.Layer_1.parent = this;
	this.Layer_1.depth = 0;
	this.Layer_1.isAttachedToCamera = 0
	this.Layer_1.isAttachedToMask = 0
	this.Layer_1.layerDepth = 0
	this.Layer_1.layerIndex = 2
	this.Layer_1.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_1).wait(100));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-42.7,-13.7,63,41.5);


(lib.Symbol_10_Symbol_9_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Symbol_9
	this.instance = new lib.Symbol9();
	this.instance.parent = this;
	this.instance.setTransform(-11.9,-14.15);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(4).to({x:-16.4,y:-9.65},6,cjs.Ease.get(1)).wait(26).to({x:-7.4,y:-14.15},6,cjs.Ease.get(1)).wait(44).to({x:-11.9},6,cjs.Ease.get(1)).wait(8));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_10_Symbol_9 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Symbol_9
	this.instance = new lib.Symbol9();
	this.instance.parent = this;
	this.instance.setTransform(-72.5,-14.25);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(4).to({x:-77,y:-9.75},6,cjs.Ease.get(1)).wait(26).to({x:-68,y:-14.25},6,cjs.Ease.get(1)).wait(44).to({x:-72.5},6,cjs.Ease.get(1)).wait(8));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_10_Layer_6 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_6
	this.instance = new lib.Symbol3();
	this.instance.parent = this;
	this.instance.setTransform(-16.15,38.3,1,1,0,0,0,-4.1,-3.9);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(8).to({regX:-4.2,scaleX:0.9999,scaleY:0.9999,rotation:1.2539,x:-16.85,y:39.1},0).wait(1).to({regX:-4.1,scaleX:0.9997,scaleY:0.9997,rotation:6.0194,x:-19.35,y:41.7},0).wait(1).to({regX:-4.2,scaleX:0.9992,scaleY:0.9992,rotation:15.5283,x:-25.25,y:46},0).wait(1).to({regX:-4.1,scaleX:0.9987,scaleY:0.9987,rotation:27.2964,x:-33.65,y:48.75},0).wait(1).to({regX:-4,scaleX:0.9984,scaleY:0.9984,rotation:37.1104,x:-41.15,y:48.85},0).wait(1).to({regX:-4.1,rotation:44.3901,x:-46.6,y:47.5},0).wait(1).to({regX:-4.2,rotation:49.6566,x:-50.3,y:45.7},0).wait(1).to({regX:-4,rotation:53.4231,x:-52.55,y:44.2},0).wait(1).to({regX:-4.1,scaleX:0.9985,scaleY:0.9985,rotation:56.1642,x:-54.15,y:42.8},0).wait(1).to({rotation:57.9415,x:-55.1,y:41.85},0).wait(1).to({scaleX:0.9986,scaleY:0.9986,rotation:59.1777,x:-55.8,y:41.2},0).wait(1).to({regY:-3.8,rotation:59.9034,x:-56.1,y:40.8},0).wait(1).to({regX:-4,regY:-3.9,scaleX:1,scaleY:1,rotation:59.9996,x:-56.15,y:40.7},0).wait(1).to({scaleX:0.9985,scaleY:0.9985,rotation:59.9147,x:-56.05,y:40.75},0).wait(1).to({rotation:59.4131,x:-55.85,y:41.05},0).wait(1).to({regY:-3.8,rotation:58.4331,x:-55.45,y:41.55},0).wait(1).to({rotation:57.141,x:-54.65,y:42.4},0).wait(1).to({regY:-3.9,scaleX:0.9984,scaleY:0.9984,rotation:55.1298,x:-53.5,y:43.35},0).wait(1).to({rotation:52.171,x:-51.8,y:44.7},0).wait(1).to({scaleX:0.9983,scaleY:0.9983,rotation:48.3873,x:-49.3,y:46.2},0).wait(1).to({rotation:43.1197,x:-45.6,y:47.8},0).wait(1).to({regX:-4.1,scaleX:0.9984,scaleY:0.9984,rotation:35.8431,x:-40.2,y:48.95},0).wait(1).to({regX:-4,scaleX:0.9987,scaleY:0.9987,rotation:25.8364,x:-32.6,y:48.6},0).wait(1).to({scaleX:0.9992,scaleY:0.9992,rotation:14.2739,x:-24.25,y:45.5},0).wait(1).to({regX:-4.1,scaleX:0.9997,scaleY:0.9997,rotation:5.2819,x:-18.9,y:41.35},0).wait(1).to({regX:-4,scaleX:0.9999,scaleY:0.9999,rotation:1.0239,x:-16.65,y:38.95},0).wait(1).to({regX:-4.1,scaleX:1,scaleY:1,rotation:0,x:-16.15,y:38.3},0).wait(66));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_4_Layer_2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.instance = new lib.Symbol5();
	this.instance.parent = this;
	this.instance.setTransform(-52.15,55.05,1,1,0,0,0,14,10.3);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(5).to({regX:-11.2,regY:7,rotation:2.865,x:-79.3,y:43.9},0).wait(1).to({rotation:4.6858,x:-80.45,y:38.9},0).wait(1).to({rotation:5.951,x:-81.3,y:35.4},0).wait(1).to({rotation:6.8794,x:-81.85,y:32.85},0).wait(1).to({rotation:7.5834,x:-82.3,y:30.9},0).wait(1).to({rotation:8.1276,x:-82.7,y:29.45},0).wait(1).to({rotation:8.5525,x:-82.95,y:28.25},0).wait(1).to({rotation:8.885,x:-83.15,y:27.35},0).wait(1).to({rotation:9.1437,x:-83.3,y:26.65},0).wait(1).to({rotation:9.3423,x:-83.45,y:26.15},0).wait(1).to({rotation:9.4911,x:-83.5,y:25.7},0).wait(1).to({rotation:9.5977,x:-83.55,y:25.45},0).wait(1).to({rotation:9.6683,x:-83.65,y:25.2},0).wait(1).to({rotation:9.7079,x:-83.7,y:25.1},0).wait(1).to({regX:14,regY:10.3,rotation:9.7203,x:-59.4,y:32.55},0).wait(10).to({rotation:0,x:-52.15,y:55.05},10,cjs.Ease.get(1)).wait(61));

}).prototype = p = new cjs.MovieClip();


(lib.Scene_1_Layer_5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_5
	this.instance = new lib.Symbol7();
	this.instance.parent = this;
	this.instance.setTransform(704.15,390.15);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Scene_1_Layer_5, null, null);


(lib.Scene_1_Layer_4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_4
	this.instance = new lib.Symbol6();
	this.instance.parent = this;
	this.instance.setTransform(704.8,422.4);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Scene_1_Layer_4, null, null);


(lib.Scene_1_Layer_2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.instance = new lib.Symbol1();
	this.instance.parent = this;
	this.instance.setTransform(548.5,512.35);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Scene_1_Layer_2, null, null);


(lib.Symbol10 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_99 = function() {
		this.___loopingOver___ = true;
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(99).call(this.frame_99).wait(1));

	// Layer_4_obj_
	this.Layer_4 = new lib.Symbol_10_Layer_4();
	this.Layer_4.name = "Layer_4";
	this.Layer_4.parent = this;
	this.Layer_4.setTransform(-11.7,25.6,1,1,0,0,0,-11.7,25.6);
	this.Layer_4.depth = 0;
	this.Layer_4.isAttachedToCamera = 0
	this.Layer_4.isAttachedToMask = 0
	this.Layer_4.layerDepth = 0
	this.Layer_4.layerIndex = 0
	this.Layer_4.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_4).wait(100));

	// Layer_11_obj_
	this.Layer_11 = new lib.Symbol_10_Layer_11();
	this.Layer_11.name = "Layer_11";
	this.Layer_11.parent = this;
	this.Layer_11.setTransform(-36,38.5,1,1,0,0,0,-36,38.5);
	this.Layer_11.depth = 0;
	this.Layer_11.isAttachedToCamera = 0
	this.Layer_11.isAttachedToMask = 0
	this.Layer_11.layerDepth = 0
	this.Layer_11.layerIndex = 1
	this.Layer_11.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_11).wait(100));

	// Layer_6_obj_
	this.Layer_6 = new lib.Symbol_10_Layer_6();
	this.Layer_6.name = "Layer_6";
	this.Layer_6.parent = this;
	this.Layer_6.setTransform(-12.1,42.2,1,1,0,0,0,-12.1,42.2);
	this.Layer_6.depth = 0;
	this.Layer_6.isAttachedToCamera = 0
	this.Layer_6.isAttachedToMask = 0
	this.Layer_6.layerDepth = 0
	this.Layer_6.layerIndex = 2
	this.Layer_6.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_6).wait(100));

	// Layer_9_obj_
	this.Layer_9 = new lib.Symbol_10_Layer_9();
	this.Layer_9.name = "Layer_9";
	this.Layer_9.parent = this;
	this.Layer_9.setTransform(-7.3,3.9,1,1,0,0,0,-7.3,3.9);
	this.Layer_9.depth = 0;
	this.Layer_9.isAttachedToCamera = 0
	this.Layer_9.isAttachedToMask = 0
	this.Layer_9.layerDepth = 0
	this.Layer_9.layerIndex = 3
	this.Layer_9.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_9).wait(100));

	// Layer_8_obj_
	this.Layer_8 = new lib.Symbol_10_Layer_8();
	this.Layer_8.name = "Layer_8";
	this.Layer_8.parent = this;
	this.Layer_8.setTransform(-69,3.9,1,1,0,0,0,-69,3.9);
	this.Layer_8.depth = 0;
	this.Layer_8.isAttachedToCamera = 0
	this.Layer_8.isAttachedToMask = 0
	this.Layer_8.layerDepth = 0
	this.Layer_8.layerIndex = 4
	this.Layer_8.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_8).wait(100));

	// Layer_1_obj_
	this.Layer_1 = new lib.Symbol_10_Layer_1();
	this.Layer_1.name = "Layer_1";
	this.Layer_1.parent = this;
	this.Layer_1.depth = 0;
	this.Layer_1.isAttachedToCamera = 0
	this.Layer_1.isAttachedToMask = 0
	this.Layer_1.layerDepth = 0
	this.Layer_1.layerIndex = 5
	this.Layer_1.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_1).wait(100));

	// Symbol_9_obj_
	this.Symbol_9 = new lib.Symbol_10_Symbol_9();
	this.Symbol_9.name = "Symbol_9";
	this.Symbol_9.parent = this;
	this.Symbol_9.setTransform(-72.5,-14.3,1,1,0,0,0,-72.5,-14.3);
	this.Symbol_9.depth = 0;
	this.Symbol_9.isAttachedToCamera = 0
	this.Symbol_9.isAttachedToMask = 0
	this.Symbol_9.layerDepth = 0
	this.Symbol_9.layerIndex = 6
	this.Symbol_9.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Symbol_9).wait(100));

	// Layer_3_obj_
	this.Layer_3 = new lib.Symbol_10_Layer_3();
	this.Layer_3.name = "Layer_3";
	this.Layer_3.parent = this;
	this.Layer_3.setTransform(-68.7,-10.3,1,1,0,0,0,-68.7,-10.3);
	this.Layer_3.depth = 0;
	this.Layer_3.isAttachedToCamera = 0
	this.Layer_3.isAttachedToMask = 0
	this.Layer_3.layerDepth = 0
	this.Layer_3.layerIndex = 7
	this.Layer_3.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_3).wait(100));

	// Layer_5_obj_
	this.Layer_5 = new lib.Symbol_10_Layer_5();
	this.Layer_5.name = "Layer_5";
	this.Layer_5.parent = this;
	this.Layer_5.depth = 0;
	this.Layer_5.isAttachedToCamera = 0
	this.Layer_5.isAttachedToMask = 0
	this.Layer_5.layerDepth = 0
	this.Layer_5.layerIndex = 8
	this.Layer_5.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_5).wait(100));

	// Symbol_9_obj_
	this.Symbol_9_1 = new lib.Symbol_10_Symbol_9_1();
	this.Symbol_9_1.name = "Symbol_9_1";
	this.Symbol_9_1.parent = this;
	this.Symbol_9_1.setTransform(-11.9,-14.2,1,1,0,0,0,-11.9,-14.2);
	this.Symbol_9_1.depth = 0;
	this.Symbol_9_1.isAttachedToCamera = 0
	this.Symbol_9_1.isAttachedToMask = 0
	this.Symbol_9_1.layerDepth = 0
	this.Symbol_9_1.layerIndex = 9
	this.Symbol_9_1.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Symbol_9_1).wait(100));

	// Layer_7_obj_
	this.Layer_7 = new lib.Symbol_10_Layer_7();
	this.Layer_7.name = "Layer_7";
	this.Layer_7.parent = this;
	this.Layer_7.setTransform(-8.1,-10.2,1,1,0,0,0,-8.1,-10.2);
	this.Layer_7.depth = 0;
	this.Layer_7.isAttachedToCamera = 0
	this.Layer_7.isAttachedToMask = 0
	this.Layer_7.layerDepth = 0
	this.Layer_7.layerIndex = 10
	this.Layer_7.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_7).wait(100));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-89.3,-25.4,102.5,92.9);


(lib.Symbol4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_99 = function() {
		this.___loopingOver___ = true;
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(99).call(this.frame_99).wait(1));

	// Layer_2_obj_
	this.Layer_2 = new lib.Symbol_4_Layer_2();
	this.Layer_2.name = "Layer_2";
	this.Layer_2.parent = this;
	this.Layer_2.setTransform(-77.4,51.8,1,1,0,0,0,-77.4,51.8);
	this.Layer_2.depth = 0;
	this.Layer_2.isAttachedToCamera = 0
	this.Layer_2.isAttachedToMask = 0
	this.Layer_2.layerDepth = 0
	this.Layer_2.layerIndex = 0
	this.Layer_2.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_2).wait(5).to({regX:-82.1,regY:35.9,x:-82.1,y:35.9},0).wait(14).to({regX:-77.4,regY:51.8,x:-77.4,y:51.8},0).wait(81));

	// Layer_1_obj_
	this.Layer_1 = new lib.Symbol_4_Layer_1();
	this.Layer_1.name = "Layer_1";
	this.Layer_1.parent = this;
	this.Layer_1.depth = 0;
	this.Layer_1.isAttachedToCamera = 0
	this.Layer_1.isAttachedToMask = 0
	this.Layer_1.layerDepth = 0
	this.Layer_1.layerIndex = 1
	this.Layer_1.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_1).wait(100));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-116,-69,178.3,141.6);


(lib.Scene_1_Layer_8 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_8
	this.instance = new lib.Symbol4();
	this.instance.parent = this;
	this.instance.setTransform(877.85,374.2);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Scene_1_Layer_8, null, null);


(lib.Scene_1_Layer_6 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_6
	this.instance = new lib.Symbol10();
	this.instance.parent = this;
	this.instance.setTransform(859.25,268.9,1.0937,1.0937);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Scene_1_Layer_6, null, null);


// stage content:
(lib.Dec_3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	this.___GetDepth___ = function(obj) {
		var depth = obj.depth;
		var cameraObj = this.___camera___instance;
		if(cameraObj && cameraObj.depth && obj.isAttachedToCamera)
		{
			depth += depth + cameraObj.depth;
		}
		return depth;
		}
	this.___needSorting___ = function() {
		for (var i = 0; i < this.getNumChildren() - 1; i++)
		{
			var prevDepth = this.___GetDepth___(this.getChildAt(i));
			var nextDepth = this.___GetDepth___(this.getChildAt(i + 1));
			if (prevDepth < nextDepth)
				return true;
		}
		return false;
	}
	this.___sortFunction___ = function(obj1, obj2) {
		return (this.exportRoot.___GetDepth___(obj2) - this.exportRoot.___GetDepth___(obj1));
	}
	this.on('tick', function (event){
		var curTimeline = event.currentTarget;
		if (curTimeline.___needSorting___()){
			this.sortChildren(curTimeline.___sortFunction___);
		}
	});

	// Layer_4_obj_
	this.Layer_4 = new lib.Scene_1_Layer_4();
	this.Layer_4.name = "Layer_4";
	this.Layer_4.parent = this;
	this.Layer_4.setTransform(704.8,422.4,1,1,0,0,0,704.8,422.4);
	this.Layer_4.depth = 0;
	this.Layer_4.isAttachedToCamera = 0
	this.Layer_4.isAttachedToMask = 0
	this.Layer_4.layerDepth = 0
	this.Layer_4.layerIndex = 0
	this.Layer_4.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_4).wait(1));

	// Layer_6_obj_
	this.Layer_6 = new lib.Scene_1_Layer_6();
	this.Layer_6.name = "Layer_6";
	this.Layer_6.parent = this;
	this.Layer_6.setTransform(817.6,284.8,1,1,0,0,0,817.6,284.8);
	this.Layer_6.depth = 0;
	this.Layer_6.isAttachedToCamera = 0
	this.Layer_6.isAttachedToMask = 0
	this.Layer_6.layerDepth = 0
	this.Layer_6.layerIndex = 1
	this.Layer_6.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_6).wait(1));

	// Layer_8_obj_
	this.Layer_8 = new lib.Scene_1_Layer_8();
	this.Layer_8.name = "Layer_8";
	this.Layer_8.parent = this;
	this.Layer_8.setTransform(854.6,375.9,1,1,0,0,0,854.6,375.9);
	this.Layer_8.depth = 0;
	this.Layer_8.isAttachedToCamera = 0
	this.Layer_8.isAttachedToMask = 0
	this.Layer_8.layerDepth = 0
	this.Layer_8.layerIndex = 2
	this.Layer_8.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_8).wait(1));

	// Layer_3_obj_
	this.Layer_3 = new lib.Scene_1_Layer_3();
	this.Layer_3.name = "Layer_3";
	this.Layer_3.parent = this;
	this.Layer_3.setTransform(796.8,376,1,1,0,0,0,796.8,376);
	this.Layer_3.depth = 0;
	this.Layer_3.isAttachedToCamera = 0
	this.Layer_3.isAttachedToMask = 0
	this.Layer_3.layerDepth = 0
	this.Layer_3.layerIndex = 3
	this.Layer_3.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_3).wait(1));

	// Layer_5_obj_
	this.Layer_5 = new lib.Scene_1_Layer_5();
	this.Layer_5.name = "Layer_5";
	this.Layer_5.parent = this;
	this.Layer_5.setTransform(704.1,390.2,1,1,0,0,0,704.1,390.2);
	this.Layer_5.depth = 0;
	this.Layer_5.isAttachedToCamera = 0
	this.Layer_5.isAttachedToMask = 0
	this.Layer_5.layerDepth = 0
	this.Layer_5.layerIndex = 4
	this.Layer_5.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_5).wait(1));

	// Layer_2_obj_
	this.Layer_2 = new lib.Scene_1_Layer_2();
	this.Layer_2.name = "Layer_2";
	this.Layer_2.parent = this;
	this.Layer_2.setTransform(544.5,384.4,1,1,0,0,0,544.5,384.4);
	this.Layer_2.depth = 0;
	this.Layer_2.isAttachedToCamera = 0
	this.Layer_2.isAttachedToMask = 0
	this.Layer_2.layerDepth = 0
	this.Layer_2.layerIndex = 5
	this.Layer_2.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_2).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(371.7,118,885.5999999999999,872.9);
// library properties:
lib.properties = {
	id: '045988BA5DB4994C9723C4EC9E3D7185',
	width: 1080,
	height: 680,
	fps: 24,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: [
		//{src:"assets/images/bg57.jpg", id:"bg"},
		{src:"assets/images/cookie57.png", id:"cookie"},
		{src:"assets/images/cookie_shadow57.png", id:"cookie_shadow"},
		{src:"assets/images/pack57.png", id:"pack"}
	],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['045988BA5DB4994C9723C4EC9E3D7185'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}


// Layer depth API : 

AdobeAn.Layer = new function() {
	this.getLayerZDepth = function(timeline, layerName)
	{
		if(layerName === "Camera")
		layerName = "___camera___instance";
		var script = "if(timeline." + layerName + ") timeline." + layerName + ".depth; else 0;";
		return eval(script);
	}
	this.setLayerZDepth = function(timeline, layerName, zDepth)
	{
		const MAX_zDepth = 10000;
		const MIN_zDepth = -5000;
		if(zDepth > MAX_zDepth)
			zDepth = MAX_zDepth;
		else if(zDepth < MIN_zDepth)
			zDepth = MIN_zDepth;
		if(layerName === "Camera")
		layerName = "___camera___instance";
		var script = "if(timeline." + layerName + ") timeline." + layerName + ".depth = " + zDepth + ";";
		eval(script);
	}
	this.removeLayer = function(timeline, layerName)
	{
		if(layerName === "Camera")
		layerName = "___camera___instance";
		var script = "if(timeline." + layerName + ") timeline.removeChild(timeline." + layerName + ");";
		eval(script);
	}
	this.addNewLayer = function(timeline, layerName, zDepth)
	{
		if(layerName === "Camera")
		layerName = "___camera___instance";
		zDepth = typeof zDepth !== 'undefined' ? zDepth : 0;
		var layer = new createjs.MovieClip();
		layer.name = layerName;
		layer.depth = zDepth;
		layer.layerIndex = 0;
		timeline.addChild(layer);
	}
}


})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;

$(function () {
  var canvas, stage, exportRoot, anim_container, dom_overlay_container, fnStartAnimation;
  function init() {
    canvas = document.getElementById("canvas57");
    anim_container = document.getElementById("animation_container57");
    dom_overlay_container = document.getElementById("dom_overlay_container57");
    var comp=AdobeAn.getComposition("045988BA5DB4994C9723C4EC9E3D7185");
    var lib=comp.getLibrary();
    var loader = new createjs.LoadQueue(false);
    loader.addEventListener("fileload", function(evt){handleFileLoad(evt,comp)});
    loader.addEventListener("complete", function(evt){handleComplete(evt,comp)});
    var lib=comp.getLibrary();
    loader.loadManifest(lib.properties.manifest);
  }
  function handleFileLoad(evt, comp) {
    var images=comp.getImages();
    if (evt && (evt.item.type == "image")) { images[evt.item.id] = evt.result; }
  }
  function handleComplete(evt,comp) {
    //This function is always called, irrespective of the content. You can use the variable "stage" after it is created in token create_stage.
    var lib=comp.getLibrary();
    var ss=comp.getSpriteSheet();
    var queue = evt.target;
    var ssMetadata = lib.ssMetadata;
    for(i=0; i<ssMetadata.length; i++) {
      ss[ssMetadata[i].name] = new createjs.SpriteSheet( {"images": [queue.getResult(ssMetadata[i].name)], "frames": ssMetadata[i].frames} )
    }
    var preloaderDiv = document.getElementById("_preload_div_57");
    preloaderDiv.style.display = 'none';
    canvas.style.display = 'block';
    exportRoot = new lib.Dec_3();
    stage = new lib.Stage(canvas);
    //Registers the "tick" event listener.
    fnStartAnimation = function() {
      stage.addChild(exportRoot);
      createjs.Ticker.setFPS(lib.properties.fps);
      createjs.Ticker.addEventListener("tick", stage)
      stage.addEventListener("tick", handleTick)
      function getProjectionMatrix(container, totalDepth) {
        var focalLength = 528.25;
        var projectionCenter = { x : lib.properties.width/2, y : lib.properties.height/2 };
        var scale = (totalDepth + focalLength)/focalLength;
        var scaleMat = new createjs.Matrix2D;
        scaleMat.a = 1/scale;
        scaleMat.d = 1/scale;
        var projMat = new createjs.Matrix2D;
        projMat.tx = -projectionCenter.x;
        projMat.ty = -projectionCenter.y;
        projMat = projMat.prependMatrix(scaleMat);
        projMat.tx += projectionCenter.x;
        projMat.ty += projectionCenter.y;
        return projMat;
      }
      function handleTick(event) {
        var cameraInstance = exportRoot.___camera___instance;
        if(cameraInstance !== undefined && cameraInstance.pinToObject !== undefined)
        {
          cameraInstance.x = cameraInstance.pinToObject.x + cameraInstance.pinToObject.pinOffsetX;
          cameraInstance.y = cameraInstance.pinToObject.y + cameraInstance.pinToObject.pinOffsetY;
          if(cameraInstance.pinToObject.parent !== undefined && cameraInstance.pinToObject.parent.depth !== undefined)
            cameraInstance.depth = cameraInstance.pinToObject.parent.depth + cameraInstance.pinToObject.pinOffsetZ;
        }
        applyLayerZDepth(exportRoot);
      }
      function applyLayerZDepth(parent)
      {
        var cameraInstance = parent.___camera___instance;
        var focalLength = 528.25;
        var projectionCenter = { 'x' : 0, 'y' : 0};
        if(parent === exportRoot)
        {
          var stageCenter = { 'x' : lib.properties.width/2, 'y' : lib.properties.height/2 };
          projectionCenter.x = stageCenter.x;
          projectionCenter.y = stageCenter.y;
        }
        for(child in parent.children)
        {
          var layerObj = parent.children[child];
          if(layerObj == cameraInstance)
            continue;
          applyLayerZDepth(layerObj, cameraInstance);
          if(layerObj.layerDepth === undefined)
            continue;
          if(layerObj.currentFrame != layerObj.parent.currentFrame)
          {
            layerObj.gotoAndPlay(layerObj.parent.currentFrame);
          }
          var matToApply = new createjs.Matrix2D;
          var cameraMat = new createjs.Matrix2D;
          var totalDepth = layerObj.layerDepth ? layerObj.layerDepth : 0;
          var cameraDepth = 0;
          if(cameraInstance && !layerObj.isAttachedToCamera)
          {
            var mat = cameraInstance.getMatrix();
            mat.tx -= projectionCenter.x;
            mat.ty -= projectionCenter.y;
            cameraMat = mat.invert();
            cameraMat.prependTransform(projectionCenter.x, projectionCenter.y, 1, 1, 0, 0, 0, 0, 0);
            cameraMat.appendTransform(-projectionCenter.x, -projectionCenter.y, 1, 1, 0, 0, 0, 0, 0);
            if(cameraInstance.depth)
              cameraDepth = cameraInstance.depth;
          }
          if(layerObj.depth)
          {
            totalDepth = layerObj.depth;
          }
          //Offset by camera depth
          totalDepth -= cameraDepth;
          if(totalDepth < -focalLength)
          {
            matToApply.a = 0;
            matToApply.d = 0;
          }
          else
          {
            if(layerObj.layerDepth)
            {
              var sizeLockedMat = getProjectionMatrix(parent, layerObj.layerDepth);
              if(sizeLockedMat)
              {
                sizeLockedMat.invert();
                matToApply.prependMatrix(sizeLockedMat);
              }
            }
            matToApply.prependMatrix(cameraMat);
            var projMat = getProjectionMatrix(parent, totalDepth);
            if(projMat)
            {
              matToApply.prependMatrix(projMat);
            }
          }
          layerObj.transformMatrix = matToApply;
        }
      }
    }
    //Code to support hidpi screens and responsive scaling.
    function makeResponsive(isResp, respDim, isScale, scaleType) {
      var lastW, lastH, lastS=1;
      window.addEventListener('resize', resizeCanvas);
      resizeCanvas();
      function resizeCanvas() {
        var w = lib.properties.width, h = lib.properties.height;
        var iw = window.innerWidth, ih=window.innerHeight;
        var pRatio = window.devicePixelRatio || 1, xRatio=iw/w, yRatio=ih/h, sRatio=1;
        if(isResp) {
          if((respDim=='width'&&lastW==iw) || (respDim=='height'&&lastH==ih)) {
            sRatio = lastS;
          }
          else if(!isScale) {
            if(iw<w || ih<h)
              sRatio = Math.min(xRatio, yRatio);
          }
          else if(scaleType==1) {
            sRatio = Math.min(xRatio, yRatio);
          }
          else if(scaleType==2) {
            sRatio = Math.max(xRatio, yRatio);
          }
        }
        canvas.width = w*pRatio*sRatio;
        canvas.height = h*pRatio*sRatio;
        canvas.style.width = anim_container.style.width = dom_overlay_container.style.width = preloaderDiv.style.width = w*sRatio+'px';
        canvas.style.height = anim_container.style.height = dom_overlay_container.style.height = preloaderDiv.style.height = h*sRatio+'px';
        stage.scaleX = pRatio*sRatio;
        stage.scaleY = pRatio*sRatio;
        lastW = iw; lastH = ih; lastS = sRatio;
        stage.tickOnUpdate = false;
        stage.update();
        stage.tickOnUpdate = true;
      }
    }
    makeResponsive(true,'both',true,2);
    AdobeAn.compositionLoaded(lib.properties.id);
    fnStartAnimation();
  }
  init();
});