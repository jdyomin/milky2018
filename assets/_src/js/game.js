(function (lib, img, cjs, ss, an) {

var p; // shortcut to reference prototypes
lib.webFontTxtInst = {}; 
var loadedTypekitCount = 0;
var loadedGoogleCount = 0;
var gFontsUpdateCacheList = [];
var tFontsUpdateCacheList = [];
lib.ssMetadata = [
		{name:"game_atlas_", frames: [[0,842,690,690],[692,2926,228,76],[692,2494,202,142],[692,2638,202,142],[692,2782,202,142],[0,2918,690,690],[0,0,1427,840],[935,1710,90,94],[899,2030,97,97],[692,1534,241,494],[935,1619,96,89],[935,1534,112,83],[692,2030,205,462],[1033,1620,80,80],[0,2226,690,690],[1049,1534,83,84],[935,1806,91,92],[692,842,690,690],[935,1900,85,96],[0,1534,690,690],[1027,1710,81,88]]}
];



lib.updateListCache = function (cacheList) {		
	for(var i = 0; i < cacheList.length; i++) {		
		if(cacheList[i].cacheCanvas)		
			cacheList[i].updateCache();		
	}		
};		

lib.addElementsToCache = function (textInst, cacheList) {		
	var cur = textInst;		
	while(cur != exportRoot) {		
		if(cacheList.indexOf(cur) != -1)		
			break;		
		cur = cur.parent;		
	}		
	if(cur != exportRoot) {		
		var cur2 = textInst;		
		var index = cacheList.indexOf(cur);		
		while(cur2 != cur) {		
			cacheList.splice(index, 0, cur2);		
			cur2 = cur2.parent;		
			index++;		
		}		
	}		
	else {		
		cur = textInst;		
		while(cur != exportRoot) {		
			cacheList.push(cur);		
			cur = cur.parent;		
		}		
	}		
};		

lib.gfontAvailable = function(family, totalGoogleCount) {		
	lib.properties.webfonts[family] = true;		
	var txtInst = lib.webFontTxtInst && lib.webFontTxtInst[family] || [];		
	for(var f = 0; f < txtInst.length; ++f)		
		lib.addElementsToCache(txtInst[f], gFontsUpdateCacheList);		

	loadedGoogleCount++;		
	if(loadedGoogleCount == totalGoogleCount) {		
		lib.updateListCache(gFontsUpdateCacheList);		
	}		
};		

lib.tfontAvailable = function(family, totalTypekitCount) {		
	lib.properties.webfonts[family] = true;		
	var txtInst = lib.webFontTxtInst && lib.webFontTxtInst[family] || [];		
	for(var f = 0; f < txtInst.length; ++f)		
		lib.addElementsToCache(txtInst[f], tFontsUpdateCacheList);		

	loadedTypekitCount++;		
	if(loadedTypekitCount == totalTypekitCount) {		
		lib.updateListCache(tFontsUpdateCacheList);		
	}		
};
// symbols:



(lib.adw2 = function() {
	this.spriteSheet = ss["game_atlas_"];
	this.gotoAndStop(0);
}).prototype = p = new cjs.Sprite();



(lib.bn = function() {
	this.spriteSheet = ss["game_atlas_"];
	this.gotoAndStop(1);
}).prototype = p = new cjs.Sprite();



(lib.buble1 = function() {
	this.spriteSheet = ss["game_atlas_"];
	this.gotoAndStop(2);
}).prototype = p = new cjs.Sprite();



(lib.buble2 = function() {
	this.spriteSheet = ss["game_atlas_"];
	this.gotoAndStop(3);
}).prototype = p = new cjs.Sprite();



(lib.buble3 = function() {
	this.spriteSheet = ss["game_atlas_"];
	this.gotoAndStop(4);
}).prototype = p = new cjs.Sprite();



(lib.failwindow = function() {
	this.spriteSheet = ss["game_atlas_"];
	this.gotoAndStop(5);
}).prototype = p = new cjs.Sprite();



(lib.fon22 = function() {
	this.spriteSheet = ss["game_atlas_"];
	this.gotoAndStop(6);
}).prototype = p = new cjs.Sprite();



(lib.green = function() {
	this.spriteSheet = ss["game_atlas_"];
	this.gotoAndStop(7);
}).prototype = p = new cjs.Sprite();



(lib.logo = function() {
	this.spriteSheet = ss["game_atlas_"];
	this.gotoAndStop(8);
}).prototype = p = new cjs.Sprite();



(lib.m2 = function() {
	this.spriteSheet = ss["game_atlas_"];
	this.gotoAndStop(9);
}).prototype = p = new cjs.Sprite();



(lib.m3 = function() {
	this.spriteSheet = ss["game_atlas_"];
	this.gotoAndStop(10);
}).prototype = p = new cjs.Sprite();



(lib.m4 = function() {
	this.spriteSheet = ss["game_atlas_"];
	this.gotoAndStop(11);
}).prototype = p = new cjs.Sprite();



(lib.milkbox = function() {
	this.spriteSheet = ss["game_atlas_"];
	this.gotoAndStop(12);
}).prototype = p = new cjs.Sprite();



(lib.percent = function() {
	this.spriteSheet = ss["game_atlas_"];
	this.gotoAndStop(13);
}).prototype = p = new cjs.Sprite();



(lib.raitWin = function() {
	this.spriteSheet = ss["game_atlas_"];
	this.gotoAndStop(14);
}).prototype = p = new cjs.Sprite();



(lib.red = function() {
	this.spriteSheet = ss["game_atlas_"];
	this.gotoAndStop(15);
}).prototype = p = new cjs.Sprite();



(lib.sel = function() {
	this.spriteSheet = ss["game_atlas_"];
	this.gotoAndStop(16);
}).prototype = p = new cjs.Sprite();



(lib.start3 = function() {
	this.spriteSheet = ss["game_atlas_"];
	this.gotoAndStop(17);
}).prototype = p = new cjs.Sprite();



(lib.violet = function() {
	this.spriteSheet = ss["game_atlas_"];
	this.gotoAndStop(18);
}).prototype = p = new cjs.Sprite();



(lib.winwindow2 = function() {
	this.spriteSheet = ss["game_atlas_"];
	this.gotoAndStop(19);
}).prototype = p = new cjs.Sprite();



(lib.yellow = function() {
	this.spriteSheet = ss["game_atlas_"];
	this.gotoAndStop(20);
}).prototype = p = new cjs.Sprite();
// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.Symbol1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("Ak2E3QiBiBAAi2QAAi1CBiBQCAiBC2AAQC2AACBCBQCBCBAAC1QAAC2iBCBQiBCBi2AAQi2AAiAiBg");
	this.shape.setTransform(44,44);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol1, new cjs.Rectangle(0,0,88,88), null);


(lib.f = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2
	this.instance = new lib.sel();
	this.instance.parent = this;
	this.instance.setTransform(14,15);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.f, new cjs.Rectangle(14,15,91,92), null);


(lib.Символ23 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("EgYhAmmMAAAgnNIKUAAMAAAgl+IU7AAIAAC+IOEAAMAAAAjAIDwAAMAAAAnNg");
	this.shape.setTransform(24.9,3);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Символ23, new cjs.Rectangle(-132.1,-244,314,494), null);


(lib.Символ21 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(2));

	// Слой 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgBBYIgBAAQgGAAgDgCQgDgCAFgdIADgTIAFgZIADgaIADgRIgZggIgIgMQAAgDADgCQACgCADAAQABAAABAAQAAAAABABQAAAAABAAQAAAAABABQAEACAIALQAIALADACIABgEIABgIIABgIIACgGIAEgEQACgCADAAQADAAACACQADACgFAjIgKA5IgFAeIgFAeQgDASgCABIgBAAg");
	this.shape.setTransform(56,0.7);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgYBdQgEgBgBgLIAAiMQAAgfACgBQADgBAHAAIAKAAIALACIALADQAFAAADADQAFAEACAPQABATgLALQgDADgOACQgMABAAABQAFABgIBYIgBAFIAAAHIAAAGIAAAFQgCAJgGAAIgDAAgAgOhMQgCABADAcQAAADAJAAIAEAAIAGgBIAFgBIADgDQADgCAAgJQABgKgDgDIgEgDIgFgBIgGgBIgEAAQgLAAABACg");
	this.shape_1.setTransform(44.4,0.5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgJBXQgEgEgCgOIgCgjIgGhYIAAgEIgBgGIgBgJQAAgJAGgCIAHAAIAEAAIADAAIALgCIAHgBIAEAAIAEABIADABQABABAAAAQAAAAAAABQAAAAABABQAAAAAAABQABAIgOACIgLACIgIABIAAAJIABAVIACAaIACAZIADAqIABAJIAAAJQABALgEACQgDACgDAAQAAAAgBAAQgBAAAAAAQAAAAgBgBQAAAAAAAAg");
	this.shape_2.setTransform(31.2,0.7);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AgXBWQgDgJABgcIAAgSIAAgeIgBgcIABgUIAAgTIAAgPQABgIAFAAQAIABACAMIAAAJIAAAPIgBBPIAAAAIAAAAIABgEIADgIQANgmAAgYIAAgFIACgJIACgNQABgKAGgCIACgBQADAAABAEIACAVIABAdIAAA0IAAANIgBARIgBARIAAALQAAAMgGAAIgCgBIgBABQgDAAgDgFQgBgGAAgHIAEhOQgMBEgJAXQgDAGgFAAQgFAAgCgEg");
	this.shape_3.setTransform(18.8,1);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AgLBZQgFgBgCgEQgDgDAAgGIgBgMIgBgRIgCgsIAAgGIAAgLIgBgVIgBgVIgCgWQAAgJAHgCIAEAAIADAAQAFAAAAAOIABATIABAbIAAANQAHgDAFAAQAPAAAHAUQAFAdgCAWQgFAhgOAFQgEACgGAAQgGAAgFgCgAgGgFIgCACIACA6QABAUADABIAEgBIAEgDQAEgGABgFQAFgZgFgbQgCgQgJAAQgCAAgEACg");
	this.shape_4.setTransform(0.7,0.5);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AgCBYQgIgEABgfIAAgEIAAgHIgBgKIABgyQABgZACgYIAAAAIAAAAQgBABAAAAQAAAAAAAAQgBAAAAAAQgBAAAAAAIgNgCQgKgDABgHQAAgDADgCIAIgDIAJgBIAGgBIAKABIASAAQAHABACAJQACAIgEAAIgEAAIgDAAIgFAAIgCgBIgCAAQgBAAAAAAQgBAAgBABQAAAAAAAAQAAAAAAABQgDAUAAAqIAAApIAAAZIAAAKIAAAGIgBAFQgBAIgFAAIgDgBg");
	this.shape_5.setTransform(-12,0.7);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AgfBWQgEgGACgXIADgWIADgWIADgLIAFgYIAIgiIAFgWQADgJADAAIADABQADACADASIAJA3IAFAeIAFAZQAJAggDAHQgEADgDAAQgFAAgCgGIgCgGIgEgRIgGgfIgDgKIgGAAIgHABQgEANgDAVQgEAkgBAAIgGABQgDAAgCgCgAgDgUIgDATQAHAAABgBQAAgXgDgCIAAAAIgCAHg");
	this.shape_6.setTransform(-24.6,0.8);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AANBXQgCgEAAgNIAAgiIgCgiQgBgBgEAAIgNACQgIAAgEgDQgEgDAAgIIAAgrIAAgYQABgEACgCQABAAAAgBQABAAABAAQAAgBABAAQAAAAABAAIADABQAEACgBAhQgBAiACACQAAAAAAABQAAAAAAAAQABAAAAAAQABAAAAAAIAEAAIAEgBIAJgBQABgCgBgdQgBgfACgFQADgGADAAQAGAAAAACIADBWIABBPQAAAHgFACIgDABIgFgCg");
	this.shape_7.setTransform(-37.2,0.7);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AgfBWQgEgGACgXIADgWIADgWIADgLIAFgYIAIgiIAFgWQADgJADAAIADABQADACADASIAJA3IAFAeIAFAZQAJAggDAHQgEADgDAAQgFAAgCgGIgCgGIgEgRIgGgfIgDgKIgGAAIgHABQgEANgDAVQgEAkgBAAIgGABQgDAAgCgCgAgDgUIgDATQAHAAABgBQAAgXgDgCIAAAAIgCAHg");
	this.shape_8.setTransform(-49.6,0.8);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AgUBZQgFAAgBgRIAAgHIAAgJIABgKIAAgFIAAhfIAAgYQABgEACgCIAFgBIADAAQAEACAAAhQAAAiABACIADAAIADAAIADAAIAJgBQABgCgBgeQgBgeACgFQADgGAEAAQAFAAAAACIADBWIACBPQAAAGgGADIgDAAQgCABgDgCQgCgFAAgMIAAgjIgCghQgBgCgEAAIgNADQgEAAADA6QABAVgCAFQgCACgFAAg");
	this.shape_9.setTransform(-62.1,0.7);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("AgQBMQgEgygDg2IgBgNIgCgTQgBgRAIgEIAFgBIAKgCIAJgCIAGgBQAFAAACACQADAEgCAEQgDAGgKAAQgLAAgEAEIAAALIACASIABARIABANQALACgBAGQAAADgJAEIABANIACAUQABAVABAMQAHgCAFAAQAHAAAFADQADABgCAFQgCAFgCAAIgfADQgHAAAAgMg");
	this.shape_10.setTransform(84,0.8);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("AgeBVQgBgXADg2IACgvIABgeIAAgHIAAgEQABgDACgDQACgCADAAQADAAADACIAAAJIAAALIABAVQAFgEAGgQQAHgQAFgFQADgBADgBIABAAQAEAAABACQABAFgHAMIgPAhIgJASQALAUASAwQAJAWgCAIIgDADIgFABIgEgBQgFgCgCgJIgLgiIgMgjQgCAHAAAVIgBA2QAAABgBAAQAAABAAAAQgBABAAAAQgBAAgBABIgFAAQgHAAAAgEg");
	this.shape_11.setTransform(28.8,0.9);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("AAVBXIgDgGIgCgHIAAgGIgBgoIgBgXIAAgQQgIgBgFAHQgEAFgDAIQgDAJgBAJIAAAPIgDAqIgDAEQAAAAAAAAQgBABAAAAQgBAAgBAAQAAAAgBAAQgFAAgCgFIAAgTIAAgeQAAgMACgJQACgKANgUIgNgLQgIgHgCgMQgBgNAHgMQADgFAFgDIAMgFIAMgDIAMAAQAKAAABADQAFgCAAAZIgBAIIAAANIAAAMIAAAIIABAzIABA2QgDAFgEAAQgDAAgDgCgAAHhHIgIADIgIADIgGADQgCADABAKIACAIQABADADABQACACADABIAIABIADABIADAAQAGgBAAgCQAEgigDgCQAAAAAAAAQAAAAAAAAQAAAAgBAAQAAgBgBAAIgHABg");
	this.shape_12.setTransform(10.6,0.7);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFFFFF").s().p("AgSBNQgFgIgDgeIgBghIgBgIQgBgzAEgNQADgJAGgHQAGgGAKAAQAHAAAHAFQAGADAEANQACAHgGAEQAAAAgBABQAAAAgBAAQgBAAAAABQgBAAAAAAQgBAAAAAAQgBgBAAAAQAAAAgBgBQAAAAAAgBQgFgKgGgCIgCgBQgFAAgDALIgBAIIAAANQgCA1AEAeQADAeAIAAIABAAQAIgBABgFQABgFAGgCIACAAQAAAAABAAQAAAAABAAQAAABABAAQAAABABAAQAAABABAAQAAABAAABQABAAAAABQAAAAAAABQAAACgFAHQgEAGgFACQgGADgHAAQgNgBgHgLg");
	this.shape_13.setTransform(-2.4,0.9);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FFFFFF").s().p("AgVBZQgEAAAAgRIAAgHIAAgJIAAgKIAAgFIAAhfIABgYQAAgEACgCIAFgBIAEAAQADACgBAhQAAAiABACIAFAAIACAAIADAAIAKgBQABgCgBgeQgCgeACgFQADgGADAAQAGAAAAACIADBWIABBPQAAAGgEADIgDAAQgEABgCgCQgCgFAAgMIAAgjIgBghQgCgCgEAAIgNADQgEAAADA6QABAVgDAFQgBACgFAAg");
	this.shape_14.setTransform(-52.6,0.7);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FFFFFF").s().p("AgYBdQgEgBgBgLIAAiMQAAgfACgBQADgBAHAAIAKAAIALACIALADQAFAAADADQAFAEACAPQABATgLALQgDADgOACQgMABAAABQAFABgIBYIgBAFIAAAHIAAAGIAAAFQgCAJgGAAIgDAAgAgOhMQgCABADAcQAAADAJAAIAEAAIAGgBIAFgBIADgDQADgCAAgJQABgKgDgDIgEgDIgFgBIgGgBIgEAAQgLAAABACg");
	this.shape_15.setTransform(-64.6,0.5);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FFFFFF").s().p("AgQBMQgEgygDg2IgBgNIgCgTQgBgRAIgEIAFgBIAKgCIAJgCIAGgBQAFAAACACQADAEgCAEQgDAGgKAAQgLAAgEAEIAAALIACASIABARIABANQALACgBAGQAAADgJAEIABANIACAUQABAVABAMQAHgCAFAAQAHAAAFADQADABgCAFQgCAFgCAAIgfADQgHAAAAgMg");
	this.shape_16.setTransform(-78,0.8);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#FFFFFF").s().p("AgLBZQgFgBgCgEQgDgDAAgGIgCgMIAAgRIgCgsIAAgGIgBgLIAAgUIgBgVIgBgMIABgIQAAgEACgDQADgDADgBIAHgCIAHgBQAMAAAIAHQAIAIACAFQAGANgFAMQgDAMgMAIQAJAPADALQAEAOABAMQABAMgBAMQgEAhgPAFQgDACgGAAQgHAAgEgCgAgIgQIAAAQIABASIABAlQAAAUAEABIADgBIAFgDQAEgGABgFQAFgZgFgbQgFgZgNgCIgBACgAgKhMQgBAHACANIABAUQAGAAACgBQAEgBAEgFQAHgIgDgLQAAgGgGgEQgGgFgGAAIgCAAIgCABg");
	this.shape_17.setTransform(-89.9,0.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5,p:{x:-12}},{t:this.shape_4,p:{x:0.7}},{t:this.shape_3,p:{x:18.8}},{t:this.shape_2,p:{x:31.2}},{t:this.shape_1,p:{x:44.4}},{t:this.shape,p:{x:56}}]}).to({state:[{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape,p:{x:-40.5}},{t:this.shape_5,p:{x:-27.5}},{t:this.shape_4,p:{x:-14.8}},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_3,p:{x:46.8}},{t:this.shape_2,p:{x:59.2}},{t:this.shape_1,p:{x:72.4}},{t:this.shape_10}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-67.6,-16.4,135.2,32.8);


(lib.Символ20 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой 1
	this.instance = new lib.logo();
	this.instance.parent = this;
	this.instance.setTransform(-48.5,-48.5);

	this.instance_1 = new lib.green();
	this.instance_1.parent = this;
	this.instance_1.setTransform(-45,-47);

	this.instance_2 = new lib.red();
	this.instance_2.parent = this;
	this.instance_2.setTransform(-42,-42);

	this.instance_3 = new lib.violet();
	this.instance_3.parent = this;
	this.instance_3.setTransform(-43,-48);

	this.instance_4 = new lib.yellow();
	this.instance_4.parent = this;
	this.instance_4.setTransform(-41,-44);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},1).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_3}]},1).to({state:[{t:this.instance_4}]},1).to({state:[]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-48.5,-48.5,97,97);


(lib.Символ19 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.n.textBaseline="middle";
		this.s.textBaseline="middle";
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(1));

	// Слой 1
	this.s = new cjs.Text("100", "18px 'Circe'", "#0196D3");
	this.s.name = "s";
	this.s.textAlign = "center";
	this.s.lineHeight = 27;
	this.s.lineWidth = 60;
	this.s.parent = this;
	this.s.setTransform(251.5,-6.8);

	this.n = new cjs.Text("NikeName1", "18px 'Circe'", "#0196D3");
	this.n.name = "n";
	this.n.lineHeight = 27;
	this.n.lineWidth = 208;
	this.n.parent = this;
	this.n.setTransform(-158.5,-6.8);

	this.shape = new cjs.Shape();
	this.shape.graphics.f("#0196D3").s().p("AAGA/IAAhpIgRAPIgIgMIAdgXIAKAAIAAB9g");
	this.shape.setTransform(-278.7,-7.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape},{t:this.n},{t:this.s}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Символ19, new cjs.Rectangle(-283.5,-21.8,567,43.6), null);


(lib.Символ18 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.n.textBaseline="middle";
		this.s.textBaseline="middle";
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(1));

	// Слой 1
	this.s = new cjs.Text("100", "18px 'Circe'", "#0196D3");
	this.s.name = "s";
	this.s.textAlign = "center";
	this.s.lineHeight = 27;
	this.s.lineWidth = 60;
	this.s.parent = this;
	this.s.setTransform(251.5,-6.8);

	this.n = new cjs.Text("NikeName2", "18px 'Circe'", "#0196D3");
	this.n.name = "n";
	this.n.lineHeight = 27;
	this.n.lineWidth = 208;
	this.n.parent = this;
	this.n.setTransform(-158.5,-6.8);

	this.shape = new cjs.Shape();
	this.shape.graphics.f("#0196D3").s().p("AgmBAIAAgMIASgRIASgVQAJgLAFgKQAGgLAAgKQAAgGgDgEQgCgFgEgDQgFgCgGAAQgFAAgFACQgEACgEAFQgDAEgBAHIgNgFQADgKAEgGQAFgHAIgDQAIgEAIAAQALAAAIAFQAHAFAFAHQADAHAAAIQAAALgEAKQgFAKgHAKIgOASIgNAOIgJAIIA6AAIAAAOg");
	this.shape.setTransform(-276.8,-7.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape},{t:this.n},{t:this.s}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Символ18, new cjs.Rectangle(-283.5,-21.8,567,43.6), null);


(lib.Символ17 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#6666FF").s().p("Am3G4IAAtvINvAAIAANvg");
	this.shape.setTransform(44,44);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Символ17, new cjs.Rectangle(0,0,88,88), null);


(lib.Символ16 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.n.textBaseline="middle";
		this.s.textBaseline="middle";
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(1));

	// Слой 1
	this.s = new cjs.Text("100", "18px 'Circe'", "#0196D3");
	this.s.name = "s";
	this.s.textAlign = "center";
	this.s.lineHeight = 27;
	this.s.lineWidth = 60;
	this.s.parent = this;
	this.s.setTransform(251.5,-6.8);

	this.n = new cjs.Text("NikeName3", "18px 'Circe'", "#0196D3");
	this.n.name = "n";
	this.n.lineHeight = 27;
	this.n.lineWidth = 208;
	this.n.parent = this;
	this.n.setTransform(-158.5,-6.8);

	this.shape = new cjs.Shape();
	this.shape.graphics.f("#0196D3").s().p("AgXA8QgKgGgFgLIAIgKQAGAIAHAFQAIAFAJAAQALAAAGgHQAHgHgBgJQAAgKgGgHQgIgFgKAAIgKAAIAAgOIAJAAQAFAAAFgCQAEgDADgFQAEgFAAgGQAAgKgGgFQgFgGgIAAQgKAAgGAFQgGAEgEAHIgIgKQAEgJAJgGQAJgFAMAAQAQAAAIAJQAKAIAAAPQAAALgFAHQgFAIgKAFIAAAAQAJABAFADQAGAFACAHQADAGAAAHQAAALgFAJQgFAIgJAFQgJAEgLAAQgNAAgKgFg");
	this.shape.setTransform(-277,-7.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape},{t:this.n},{t:this.s}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Символ16, new cjs.Rectangle(-283.5,-21.8,567,43.6), null);


(lib.Символ15 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.n.textBaseline="middle";
		this.s.textBaseline="middle";
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(1));

	// Слой 1
	this.s = new cjs.Text("100", "18px 'Circe'", "#0196D3");
	this.s.name = "s";
	this.s.textAlign = "center";
	this.s.lineHeight = 27;
	this.s.lineWidth = 60;
	this.s.parent = this;
	this.s.setTransform(251.5,-6.7);

	this.n = new cjs.Text("NikeName4", "18px 'Circe'", "#0196D3");
	this.n.name = "n";
	this.n.lineHeight = 27;
	this.n.lineWidth = 208;
	this.n.parent = this;
	this.n.setTransform(-158.5,-6.7);

	this.shape = new cjs.Shape();
	this.shape.graphics.f("#0196D3").s().p("AANA/IAAggIg5AAIAAgJIAzhUIARAAIgxBPIAmAAIAAgiIAPAAIAAAiIARAAIAAAOIgRAAIAAAgg");
	this.shape.setTransform(-276.6,-7.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape},{t:this.n},{t:this.s}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Символ15, new cjs.Rectangle(-283.5,-21.7,567,43.6), null);


(lib.Символ14 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.n.textBaseline="middle";
		this.s.textBaseline="middle";
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(1));

	// Слой 1
	this.s = new cjs.Text("100", "18px 'Circe'", "#0196D3");
	this.s.name = "s";
	this.s.textAlign = "center";
	this.s.lineHeight = 27;
	this.s.lineWidth = 60;
	this.s.parent = this;
	this.s.setTransform(250.2,-10.6);

	this.n = new cjs.Text("NikeName5", "18px 'Circe'", "#0196D3");
	this.n.name = "n";
	this.n.lineHeight = 27;
	this.n.lineWidth = 208;
	this.n.parent = this;
	this.n.setTransform(-159.8,-10.6);

	this.shape = new cjs.Shape();
	this.shape.graphics.f("#0196D3").s().p("AgQA+QgGgCgGgFQgGgEgEgGIAJgLQACAFAGADQAEAEAGACQAEACAGAAQAGgBAGgDQAFgDAEgHQADgFAAgIQAAgIgEgGQgEgGgGgDQgHgDgJAAIgLABIgIACIAIg/IA3AAIAAAPIgqAAIgGAiIAGgCIAFAAQALABAIAEQAJAFAFAHQAGAJAAAMQAAAOgGAIQgGAKgJAFQgJAFgLAAQgHAAgHgCg");
	this.shape.setTransform(-278.3,-11.4);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape},{t:this.n},{t:this.s}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Символ14, new cjs.Rectangle(-282.1,-17.8,564.3,35.8), null);


(lib.delEffect = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(3));

	// Слой 1
	this.instance = new lib.buble1();
	this.instance.parent = this;
	this.instance.setTransform(-101,-71);

	this.instance_1 = new lib.buble2();
	this.instance_1.parent = this;
	this.instance_1.setTransform(-101,-71);

	this.instance_2 = new lib.buble3();
	this.instance_2.parent = this;
	this.instance_2.setTransform(-101,-71);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},1).to({state:[{t:this.instance_2}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-101,-71,202,142);


(lib.Символ12 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(3));

	// Слой 2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgfA4IAAhvIA+AAIAAANIgxAAIAAAkIAsAAIAAAMIgsAAIAAAlIAyAAIAAANg");
	this.shape.setTransform(69,-0.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgiA4IAAhvIAIAAIALAAIAKAAQAPAAAJAFQAJAFAEAIQADAIAAAJQAAAJgEAIQgEAHgJAFQgJAFgNAAIgIAAIgJgBIAAArgAgVgqIAAAqIAIABIAIAAQAMAAAHgFQAHgGAAgKQAAgLgGgFQgHgGgMAAIgIAAIgJAAg");
	this.shape_1.setTransform(59.8,-0.3);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgeA4IAAhvIA9AAIAAANIgwAAIAABig");
	this.shape_2.setTransform(51.3,-0.2);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AAhA4IAAhZIhDBZIgMAAIAAhvIAOAAIAABYIBDhYIAMAAIAABvg");
	this.shape_3.setTransform(40.1,-0.2);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AAZA4Igyg6IAwg1IAQAAIgwA0IAzA7gAgpA4IAAhvIANAAIAABvg");
	this.shape_4.setTransform(25.6,-0.2);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AAaA4IAAgpIgVAAIgcApIgPAAIAegsQgGgCgEgEQgFgFgEgFQgCgGAAgIQgBgKAFgJQAEgIALgFQAJgFAOAAIAJAAIAKAAIAIAAIAABvgAgDgnQgGADgDAFQgDAFAAAHQAAAGADAFQADAEAEADQAEACAEABIAXAAIAAgsIgIAAIgIAAQgIAAgFADg");
	this.shape_5.setTransform(10.5,-0.3);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AgSAyQgNgGgGgOQgIgMAAgSQAAgPAHgNQAHgOAMgHQAMgIARAAQALAAAIADQAJADAGAFIgHALQgEgFgIgCQgHgCgIAAQgLAAgJAGQgKAFgFAKQgGALABAMQgBAOAGAJQAFAKAKAGQAKAGAMAAQAIAAAHgDQAIgDAGgEIAFAKQgDAEgGADIgMAEQgHACgHgBQgQABgNgIg");
	this.shape_6.setTransform(1.1,-0.2);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AgXA4IgMAAIAAhvIAOAAIAAApIAIAAIAHAAQAVAAAKAJQALAIAAAQQgBASgLAJQgMAKgVAAIgOAAgAgNgBIgIAAIAAAsIAGAAIAIAAQANAAAHgGQAIgFAAgNQAAgLgHgFQgIgFgNAAIgGABg");
	this.shape_7.setTransform(-8.7,-0.2);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AgGA4IAAhiIgnAAIAAgNIBbAAIAAANIgnAAIAABig");
	this.shape_8.setTransform(-18.7,-0.2);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AgWA3IgGgDIAFgMIAEACIAEACQAFAAADgEQAEgDACgFIABgEIgyhTIAPAAIApBFIAfhFIAOAAIgnBVQgFANgHAGQgGAHgJAAIgHgBg");
	this.shape_9.setTransform(-28.8,-0.2);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("AAfA4IAAgzIg9AAIAAAzIgNAAIAAhvIANAAIAAAxIA9AAIAAgxIANAAIAABvg");
	this.shape_10.setTransform(-40.3,-0.2);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("AgiA4IAAhvIAIAAIALAAIAKAAQAPAAAJAFQAJAFAEAIQADAIAAAJQAAAJgEAIQgEAHgJAFQgJAFgNAAIgIAAIgJgBIAAArgAgVgqIAAAqIAIABIAIAAQAMAAAHgFQAHgGAAgKQAAgLgGgFQgHgGgMAAIgIAAIgJAAg");
	this.shape_11.setTransform(-50.7,-0.3);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("AgfA4IAAhvIA+AAIAAANIgxAAIAAAkIAsAAIAAAMIgsAAIAAAlIAyAAIAAANg");
	this.shape_12.setTransform(-60.2,-0.2);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFFFFF").s().p("AgPA5IgLgBIgIAAIAAhvIAIAAIALgBIALAAQANABAIAEQAHADAEAGQADAGAAAIQAAAKgFAHQgGAGgIADIAAAAQALACAGAGQAGAGAAAMQAAAMgFAGQgFAIgKADQgJADgKABIgLAAgAgVArIAIABIAJAAQAMgBAHgFQAHgEAAgLQAAgJgHgEQgHgFgLAAIgSAAgAgVgqIAAAkIASAAQAFAAAFgCQAFgDADgEQADgDAAgHQAAgIgGgFQgGgEgKAAIgJAAIgIAAg");
	this.shape_13.setTransform(-69.8,-0.2);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#0295D4").s().p("AuvD6QhWAAg8hJQg8hKAAhnQAAhmA8hKQA8hJBWAAIdfAAQBWAAA8BJQA8BKAABmQAABng8BKQg8BJhWAAg");
	this.shape_14.setTransform(0,-0.5);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#175491").s().p("AuvD6QhWAAg8hJQg8hKAAhnQAAhmA8hKQA8hJBWAAIdfAAQBWAAA8BJQA8BKAABmQAABng8BKQg8BJhWAAg");
	this.shape_15.setTransform(0,-0.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).to({state:[{t:this.shape_15},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]},1).to({state:[{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-115,-25.5,230,50);


(lib.Символ11 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(3));

	// Слой 2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgQA3QgHgDgFgEQgGgDgCgFIAHgJIAJAHIAKAFQAFACAEAAQAHAAAFgDQAGgCADgFQADgEABgIQAAgIgGgFQgGgFgKAAIgMAAIAAgLIALAAQAEgBAFgCQAEgDADgDQACgFAAgFQAAgJgGgGQgFgEgIAAQgIAAgHADQgGAEgFAFIgIgJQAHgHAJgEQAIgFALAAQAJAAAIAEQAHADAEAHQAEAGAAAKQAAAIgEAGQgEAIgIAEIAAABQAGAAAFADQAEAEADAFQADAGgBAGQAAALgFAHQgFAIgJAEQgIADgKAAQgIABgIgDg");
	this.shape.setTransform(61.2,-0.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AAoA4IgPgiIgyAAIgOAiIgOAAIAwhvIALAAIAwBvgAATAKIgTgvIgUAvIAnAAg");
	this.shape_1.setTransform(51.1,-0.3);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgiA4IAAhvIAIAAIALAAIAKAAQAPAAAJAFQAJAFAEAIQADAIAAAJQAAAJgEAIQgEAHgJAFQgJAFgNAAIgIAAIgJgBIAAArgAgVgqIAAAqIAIABIAIAAQAMAAAHgFQAHgGAAgKQAAgLgGgFQgHgGgMAAIgIAAIgJAAg");
	this.shape_2.setTransform(41.1,-0.3);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AgfA4IAAhvIA+AAIAAANIgxAAIAAAkIAsAAIAAAMIgsAAIAAAlIAyAAIAAANg");
	this.shape_3.setTransform(28,-0.2);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AA6BDIAAgWIh/AAIAAhvIANAAIAABiIArAAIAAhiIANAAIAABiIAqAAIAAhiIANAAIAABiIAPAAIAAAjg");
	this.shape_4.setTransform(15.6,0.8);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AgfA4IAAhvIA+AAIAAANIgxAAIAAAkIAsAAIAAAMIgsAAIAAAlIAyAAIAAANg");
	this.shape_5.setTransform(2.6,-0.2);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AgXA4IgMAAIAAhvIAOAAIAAApIAIAAIAHAAQAVAAAKAJQALAIAAAQQgBASgLAJQgMAKgVAAIgOAAgAgNgBIgIAAIAAAsIAGAAIAIAAQANAAAHgGQAIgFAAgNQAAgLgHgFQgIgFgNAAIgGABg");
	this.shape_6.setTransform(-10.3,-0.2);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AgFA4IAAhiIgoAAIAAgNIBbAAIAAANIgnAAIAABig");
	this.shape_7.setTransform(-20.3,-0.2);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AAnA4IgOgiIgyAAIgPAiIgNAAIAwhvIALAAIAwBvgAATAKIgTgvIgTAvIAmAAg");
	this.shape_8.setTransform(-30.7,-0.3);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AgiA4IAAhvIAIAAIALAAIAKAAQAPAAAJAFQAJAFAEAIQADAIAAAJQAAAJgEAIQgEAHgJAFQgJAFgNAAIgIAAIgJgBIAAArgAgVgqIAAAqIAIABIAIAAQAMAAAHgFQAHgGAAgKQAAgLgGgFQgHgGgMAAIgIAAIgJAAg");
	this.shape_9.setTransform(-40.7,-0.3);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("AgeA4IAAhvIA9AAIAAANIgwAAIAABig");
	this.shape_10.setTransform(-49.2,-0.2);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("AAhA4IAAhZIhDBZIgMAAIAAhvIAOAAIAABYIBDhYIAMAAIAABvg");
	this.shape_11.setTransform(-60.4,-0.2);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#0295D4").s().p("AuHD6QhRAAg6hKQg5hIAAhoQAAhnA5hJQA6hJBRAAIcPAAQBRAAA5BJQA6BJAABnQAABog6BIQg5BKhRAAg");

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#175491").s().p("AuHD6QhRAAg6hKQg5hIAAhoQAAhnA5hJQA6hJBRAAIcPAAQBRAAA5BJQA6BJAABnQAABog6BIQg5BKhRAAg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).to({state:[{t:this.shape_13},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]},1).to({state:[{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-110,-25,220,50);


(lib.Символ10 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(3));

	// Слой 2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgPA3QgIgCgGgEQgEgFgEgEIAIgJIAIAHIAKAFQAFACAFAAQAGAAAHgCQAFgDAEgFQACgFAAgGQAAgJgFgFQgFgFgLAAIgMAAIAAgMIALAAQAEAAAFgCQAEgDACgEQAEgEAAgGQgBgIgGgFQgFgFgIAAQgIAAgGAEQgHACgEAHIgIgJQAGgIAIgEQAKgFAKABQAJgBAHAEQAIADAEAHQAEAGAAAKQAAAHgEAIQgEAGgIAFIAAAAQAGABAFAEQAEADADAGQADAFAAAHQgBAKgFAIQgFAHgJADQgJAEgJABQgJAAgGgDg");
	this.shape.setTransform(92.3,-0.7);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AAnA4IgOgiIgyAAIgPAiIgNAAIAwhvIALAAIAwBvgAAUAKIgUgvIgTAvIAnAAg");
	this.shape_1.setTransform(82.2,-0.8);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgiA4IAAhvIAIAAIALAAIAKAAQAPAAAJAFQAJAFAEAIQADAIAAAJQAAAJgEAIQgEAHgJAFQgJAFgNAAIgIAAIgJgBIAAArgAgVgqIAAAqIAIABIAIAAQAMAAAHgFQAHgGAAgKQAAgLgGgFQgHgGgMAAIgIAAIgJAAg");
	this.shape_2.setTransform(72.2,-0.8);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AgfA4IAAhvIA+AAIAAANIgxAAIAAAkIAsAAIAAAMIgsAAIAAAlIAyAAIAAANg");
	this.shape_3.setTransform(59.1,-0.7);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AA6BCIAAgUIh/AAIAAhwIANAAIAABiIArAAIAAhiIANAAIAABiIAqAAIAAhiIANAAIAABiIAPAAIAAAig");
	this.shape_4.setTransform(46.7,0.3);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AgfA4IAAhvIA+AAIAAANIgxAAIAAAkIAsAAIAAAMIgsAAIAAAlIAyAAIAAANg");
	this.shape_5.setTransform(33.7,-0.7);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AgXA4IgMAAIAAhvIAOAAIAAApIAIAAIAHAAQAVAAAKAJQALAIAAAQQgBASgLAJQgMAKgVAAIgOAAgAgNgBIgIAAIAAAsIAGAAIAIAAQANAAAHgGQAIgFAAgNQAAgLgHgFQgIgFgNAAIgGABg");
	this.shape_6.setTransform(20.8,-0.7);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AgGA4IAAhiIgnAAIAAgNIBbAAIAAANIgnAAIAABig");
	this.shape_7.setTransform(10.8,-0.7);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AAoA4IgPgiIgyAAIgOAiIgOAAIAwhvIALAAIAwBvgAAUAKIgUgvIgUAvIAoAAg");
	this.shape_8.setTransform(0.4,-0.8);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AgPA4IgLAAIgIAAIAAhvIAIAAIALAAIALAAQANAAAIADQAHAEAEAGQADAGAAAIQAAALgFAGQgGAHgIACIAAABQALABAGAGQAGAGAAAMQAAALgFAIQgFAGgKAEQgJADgKAAIgLAAgAgVArIAIAAIAJAAQAMABAHgGQAHgEAAgLQAAgIgHgGQgHgEgLAAIgSAAgAgVgqIAAAkIASAAQAFAAAFgCQAFgDADgEQADgEAAgGQAAgIgGgFQgGgFgKAAIgJAAIgIABg");
	this.shape_9.setTransform(-9.9,-0.7);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("AgcAzQgNgIgIgNQgHgNAAgRQAAgQAHgMQAHgNANgIQANgHAQAAQAQgBANAIQANAHAHANQAIANAAAQQAAARgIANQgHANgNAHQgNAIgQAAQgPgBgNgGgAgVgmQgKAGgFAKQgGAKAAAMQAAANAGAKQAGAKAJAGQAKAGALAAQANgBAJgFQAKgGAFgKQAGgKAAgNQgBgMgFgKQgGgKgJgGQgKgGgMAAQgLAAgKAGg");
	this.shape_10.setTransform(-21.6,-0.7);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("AgWA4IgMAAIAAhvIA+AAIAAANIgwAAIAAAgIAJgBIAIAAQASAAAKAJQAKAHAAAQQAAARgLAJQgMAJgTAAIgPAAgAgUACIAAApIAHAAIAIABQANgBAHgFQAGgGAAgKQAAgKgGgGQgHgFgMAAIgIAAIgIABg");
	this.shape_11.setTransform(-32.8,-0.7);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("AgcAzQgNgIgIgNQgHgNAAgRQAAgQAHgMQAHgNANgIQANgHAQAAQAQgBANAIQANAHAHANQAIANAAAQQAAARgIANQgHANgNAHQgNAIgQAAQgPgBgNgGgAgVgmQgKAGgFAKQgGAKAAAMQAAANAGAKQAGAKAJAGQAKAGALAAQANgBAJgFQAKgGAFgKQAGgKAAgNQgBgMgFgKQgGgKgJgGQgKgGgMAAQgLAAgKAGg");
	this.shape_12.setTransform(-44.4,-0.7);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFFFFF").s().p("AgiA4IAAhvIAIAAIALAAIAKAAQAPAAAJAFQAJAFAEAIQADAIAAAJQAAAJgEAIQgEAHgJAFQgJAFgNAAIgIAAIgJgBIAAArgAgVgqIAAAqIAIABIAIAAQAMAAAHgFQAHgGAAgKQAAgLgGgFQgHgGgMAAIgIAAIgJAAg");
	this.shape_13.setTransform(-55.3,-0.8);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FFFFFF").s().p("AAeA4IAAhiIg7AAIAABiIgOAAIAAhvIBXAAIAABvg");
	this.shape_14.setTransform(-66.5,-0.7);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FFFFFF").s().p("AgcAzQgNgIgIgNQgHgNAAgRQAAgQAHgMQAHgNANgIQANgHAQAAQAQgBANAIQANAHAHANQAIANAAAQQAAARgIANQgHANgNAHQgNAIgQAAQgPgBgNgGgAgVgmQgKAGgFAKQgGAKAAAMQAAANAGAKQAGAKAJAGQAKAGALAAQANgBAJgFQAKgGAFgKQAGgKAAgNQgBgMgFgKQgGgKgJgGQgKgGgMAAQgLAAgKAGg");
	this.shape_15.setTransform(-79.1,-0.7);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FFFFFF").s().p("AAeA4IAAhiIg8AAIAABiIgNAAIAAhvIBXAAIAABvg");
	this.shape_16.setTransform(-91.7,-0.7);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#0295D4").s().p("Ax9D6QhnAAhKhJQhJhKAAhnQAAhmBJhKQBKhJBnAAMAj7AAAQBnAABKBJQBJBKAABmQAABnhJBKQhKBJhnAAg");
	this.shape_17.setTransform(0,-0.5);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#175491").s().p("Ax9D6QhnAAhKhJQhJhKAAhnQAAhmBJhKQBKhJBnAAMAj7AAAQBnAABKBJQBJBKAABmQAABnhJBKQhKBJhnAAg");
	this.shape_18.setTransform(0,-0.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).to({state:[{t:this.shape_18},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]},1).to({state:[{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-140,-25.5,280,50);


(lib.Символ6 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("EgP6AggMgADhB4QC/AGC1A5QCbArCZAJQEDgFBzg/QDbhICIgRQB1gBBkAJQA3AUCeARQB3gIA9gbMAAYBDyI/7AKg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Символ6, new cjs.Rectangle(-102.2,-217.9,204.4,435.9), null);


(lib.Символ4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FF0000").s().p("AvMExIAAphIeYAAIAAJhg");
	this.shape.setTransform(97.3,30.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Символ4, new cjs.Rectangle(0,0,194.5,60.9), null);


(lib.Символ3копия9 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(3));

	// Слой 3
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgmA4IgMAAIAAhvIAOAAIAAApIAIAAIAHAAQAVAAAJAJQAKAIAAAQQAAASgMAJQgKAKgVAAIgOAAgAgkgBIAAAsIAGAAIAHAAQAOAAAHgGQAHgGAAgMQAAgKgHgGQgHgFgNAAIgOABgAAlA4IAAhvIAOAAIAABvg");
	this.shape.setTransform(171.6,24.3);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgiA4IAAhvIAIAAIALAAIAKAAQAPAAAJAFQAJAFAEAIQADAIAAAJQAAAJgEAIQgEAHgJAFQgJAFgNAAIgIAAIgJgBIAAArgAgVgqIAAAqIAIABIAIAAQAMAAAHgFQAHgGAAgKQAAgLgGgFQgHgGgMAAIgIAAIgJAAg");
	this.shape_1.setTransform(160.5,24.3);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgeA4IAAhvIA9AAIAAANIgvAAIAABig");
	this.shape_2.setTransform(152,24.3);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AAhA4IAAhYIhDBYIgMAAIAAhvIAOAAIAABYIBDhYIAMAAIAABvg");
	this.shape_3.setTransform(140.8,24.3);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AAnA4IgOgiIgyAAIgPAiIgNAAIAwhvIALAAIAwBvgAAUAKIgUgvIgUAvIAoAAg");
	this.shape_4.setTransform(125.2,24.3);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AgvA4IAAgOIAGAAQADgBADgEQADgEADgIQADgJACgOIAEgjIACgWIBCAAIAABvIgOAAIAAhiIgoAAIgCAVQgBAVgDANQgDAPgEAJQgEAIgEAFQgFAEgFABIgFABIgFAAg");
	this.shape_5.setTransform(113.1,24.3);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AAhA4IAAhYIhDBYIgMAAIAAhvIAOAAIAABYIBDhYIAMAAIAABvg");
	this.shape_6.setTransform(101.6,24.3);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AgPA5IgLgBIgIAAIAAhvIAIAAIALgBIALAAQANAAAIAFQAHADAEAGQADAHAAAHQAAALgFAGQgGAGgIADIAAAAQALACAGAGQAGAGAAAMQAAAMgFAGQgFAIgKADQgJADgKABIgLAAgAgVArIAIAAIAJAAQAMAAAHgEQAHgGAAgKQAAgJgHgEQgHgFgLAAIgSAAgAgVgqIAAAkIASAAQAFAAAFgCQAFgCADgEQADgFAAgGQAAgJgGgEQgGgFgKABIgJAAIgIAAg");
	this.shape_7.setTransform(90.6,24.3);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AAnA4IgOgiIgyAAIgPAiIgNAAIAwhvIALAAIAwBvgAAUAKIgUgvIgUAvIAoAAg");
	this.shape_8.setTransform(79.8,24.3);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AgiA4IAAhvIAIAAIALAAIAKAAQAPAAAJAFQAJAFAEAIQADAIAAAJQAAAJgEAIQgEAHgJAFQgJAFgNAAIgIAAIgJgBIAAArgAgVgqIAAAqIAIABIAIAAQAMAAAHgFQAHgGAAgKQAAgLgGgFQgHgGgMAAIgIAAIgJAAg");
	this.shape_9.setTransform(69.8,24.3);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("AAeA4IAAhiIg8AAIAABiIgNAAIAAhvIBXAAIAABvg");
	this.shape_10.setTransform(58.5,24.3);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#FFFFFF").ss(2,1,1).p("AuDj5IcHAAQBoAABJBJQBJBJAABnQAABohJBJQhJBJhoAAI8HAAQhoAAhJhJQhJhJAAhoQAAhnBJhJQBJhJBoAAg");
	this.shape_11.setTransform(115,25);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#0196D3").s().p("AuDD6QhnAAhKhJQhJhKAAhnQAAhmBJhKQBKhJBnAAIcHAAQBnAABKBJQBJBKAABmQAABnhJBKQhKBJhnAAg");
	this.shape_12.setTransform(115,25);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).to({state:[{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]},1).to({state:[{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1,-1,232,52);


(lib.Символ3копия8 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(3));

	// Слой 3
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgmA4IgMAAIAAhvIAOAAIAAApIAIAAIAHAAQAVAAAJAJQAKAIAAAQQAAASgMAJQgKAKgVAAIgOAAgAgkgBIAAAsIAGAAIAHAAQAOAAAHgGQAHgGAAgMQAAgKgHgGQgHgFgNAAIgOABgAAlA4IAAhvIAOAAIAABvg");
	this.shape.setTransform(188.3,24.3);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgFA4IAAhiIgoAAIAAgNIBbAAIAAANIgnAAIAABig");
	this.shape_1.setTransform(176.8,24.3);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgfA4IAAhvIA+AAIAAANIgxAAIAAAkIAsAAIAAAMIgsAAIAAAlIAyAAIAAANg");
	this.shape_2.setTransform(167.7,24.3);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AgPA5IgLgBIgIAAIAAhvIAIAAIALgBIALAAQANAAAIAFQAHADAEAGQADAHAAAHQAAALgFAGQgGAGgIADIAAAAQALACAGAGQAGAGAAAMQAAAMgFAGQgFAIgKADQgJADgKABIgLAAgAgVArIAIAAIAJAAQAMAAAHgEQAHgGAAgKQAAgJgHgEQgHgFgLAAIgSAAgAgVgqIAAAkIASAAQAFAAAFgCQAFgCADgEQADgFAAgGQAAgJgGgEQgGgFgKABIgJAAIgIAAg");
	this.shape_3.setTransform(158.2,24.3);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AgcAyQgNgHgIgNQgHgNAAgRQAAgPAHgOQAHgNANgHQANgHAQgBQAQAAANAIQANAHAHANQAIANAAAQQAAARgIANQgHAMgNAIQgNAHgQABQgPAAgNgIgAgVgmQgKAGgFAKQgGAKAAAMQAAANAGAKQAGAKAJAGQAKAGALAAQANAAAJgGQAKgGAFgKQAGgKAAgNQgBgMgFgKQgGgKgJgGQgKgGgMAAQgLAAgKAGg");
	this.shape_4.setTransform(146.5,24.3);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AgSAyQgNgGgGgNQgIgOAAgRQAAgPAHgNQAGgNANgIQAMgHARgBQAKABAJACQAJADAGAFIgHAKQgEgEgHgBQgIgDgIAAQgLAAgJAGQgKAFgFALQgGAKAAAMQAAANAGAKQAGALAKAFQAJAGAMAAQAIAAAIgDQAHgDAGgEIAFAKQgDAEgGADIgMAEQgHABgHABQgQAAgNgIg");
	this.shape_5.setTransform(134.9,24.3);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AgfA4IAAhvIA+AAIAAANIgxAAIAAAkIAsAAIAAAMIgsAAIAAAlIAyAAIAAANg");
	this.shape_6.setTransform(121.6,24.3);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AgmA4IgMAAIAAhvIAOAAIAAApIAIAAIAHAAQAVAAAJAJQAKAIAAAQQAAASgMAJQgKAKgVAAIgOAAgAgkgBIAAAsIAGAAIAHAAQAOAAAHgGQAHgGAAgMQAAgKgHgGQgHgFgNAAIgOABgAAlA4IAAhvIAOAAIAABvg");
	this.shape_7.setTransform(109.9,24.3);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AAfA4IAAgzIg9AAIAAAzIgNAAIAAhvIANAAIAAAxIA9AAIAAgxIANAAIAABvg");
	this.shape_8.setTransform(97,24.3);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AgPA3QgIgDgGgDQgEgEgEgFIAIgJIAIAIIAKAEQAGACAEAAQAGAAAHgDQAFgCAEgFQACgEAAgIQABgIgGgFQgFgFgLAAIgMAAIAAgLIALAAQAFAAAEgDQAEgCACgEQAEgFAAgFQgBgKgGgFQgFgEgIAAQgIAAgGAEQgHACgEAHIgIgJQAGgIAIgEQAKgEAKgBQAJABAIADQAHAEAEAGQAEAHAAAJQAAAHgEAHQgEAHgIAFIAAAAQAGABAFAEQAFADACAGQACAFABAGQgBALgFAHQgGAIgIADQgJAFgJAAQgJgBgGgCg");
	this.shape_9.setTransform(86.1,24.3);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("AgfA4IAAhvIA+AAIAAANIgxAAIAAAkIAsAAIAAAMIgsAAIAAAlIAyAAIAAANg");
	this.shape_10.setTransform(77.4,24.3);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("AgvA4IAAgOIAGAAQADgBADgEQADgEADgIQADgJACgOIAEgjIACgWIBCAAIAABvIgOAAIAAhiIgoAAIgCAVQgBAVgDANQgDAPgEAJQgEAIgEAFQgFAEgFABIgFABIgFAAg");
	this.shape_11.setTransform(66,24.3);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("AgcAyQgNgHgIgNQgHgNAAgRQAAgPAHgOQAHgNANgHQANgHAQgBQAQAAANAIQANAHAHANQAIANAAAQQAAARgIANQgHAMgNAIQgNAHgQABQgPAAgNgIgAgVgmQgKAGgFAKQgGAKAAAMQAAANAGAKQAGAKAJAGQAKAGALAAQANAAAJgGQAKgGAFgKQAGgKAAgNQgBgMgFgKQgGgKgJgGQgKgGgMAAQgLAAgKAGg");
	this.shape_12.setTransform(54.4,24.3);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFFFFF").s().p("AAeA4IAAhiIg7AAIAABiIgOAAIAAhvIBXAAIAABvg");
	this.shape_13.setTransform(41.8,24.3);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#FFFFFF").ss(2,1,1).p("AuDj5IcHAAQBoAABJBJQBJBJAABnQAABohJBJQhJBJhoAAI8HAAQhoAAhJhJQhJhJAAhoQAAhnBJhJQBJhJBoAAg");
	this.shape_14.setTransform(115,25);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#0196D3").s().p("AuDD6QhnAAhKhJQhJhKAAhnQAAhmBJhKQBKhJBnAAIcHAAQBnAABKBJQBJBKAABmQAABnhJBKQhKBJhnAAg");
	this.shape_15.setTransform(115,25);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).to({state:[{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]},1).to({state:[{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1,-1,232,52);


(lib.Символ3копия6 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(3));

	// Слой 3
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgPA5IgLgBIgIAAIAAhvIAIAAIALgBIALAAQANAAAIAEQAHAEAEAGQADAGAAAIQAAAKgFAHQgGAHgIACIAAAAQALACAGAGQAGAHAAALQAAALgFAHQgFAHgKAEQgJAEgKAAIgLAAgAgVArIAIAAIAJAAQAMAAAHgEQAHgGAAgKQAAgIgHgGQgHgEgLAAIgSAAgAgVgqIAAAkIASAAQAFAAAFgCQAFgCADgEQADgFAAgGQAAgIgGgFQgGgFgKAAIgJAAIgIABg");
	this.shape.setTransform(209.3,24.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgcAzQgNgIgIgNQgHgNAAgRQAAgPAHgNQAHgNANgIQANgHAQAAQAQgBANAIQANAHAHANQAIANAAAQQAAARgIANQgHANgNAHQgNAIgQAAQgPgBgNgGgAgVgmQgKAFgFALQgGAKAAAMQAAANAGAKQAGAKAJAGQAKAGALAAQANAAAJgGQAKgGAFgKQAGgKAAgNQgBgMgFgKQgGgLgJgFQgKgGgMAAQgLAAgKAGg");
	this.shape_1.setTransform(197.6,24.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AAZA4Igyg6IAwg1IAQAAIgwA1IAzA6gAgpA4IAAhvIANAAIAABvg");
	this.shape_2.setTransform(186.6,24.6);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AAhA4IAAhYIhDBYIgMAAIAAhvIAOAAIAABYIBDhYIAMAAIAABvg");
	this.shape_3.setTransform(174.3,24.6);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AAfA4IAAgyIg9AAIAAAyIgNAAIAAhvIANAAIAAAwIA9AAIAAgwIANAAIAABvg");
	this.shape_4.setTransform(161.8,24.6);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AgFA4IAAhiIgoAAIAAgNIBbAAIAAANIgnAAIAABig");
	this.shape_5.setTransform(151,24.6);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AgSAzQgMgIgHgMQgIgNAAgSQAAgQAHgMQAHgNAMgIQAMgHARAAQALgBAJADQAIADAGAFIgHAKQgEgDgIgCQgGgDgJAAQgLAAgKAGQgJAGgFAKQgGAJABANQgBANAGALQAFAKAKAFQAKAGAMAAQAIAAAHgDQAIgCAGgFIAFAKQgEAEgFADIgMAEQgHACgHAAQgQgBgNgGg");
	this.shape_6.setTransform(141.1,24.6);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AAoA4IgPgiIgyAAIgPAiIgNAAIAwhvIALAAIAwBvgAAUAKIgUgvIgUAvIAoAAg");
	this.shape_7.setTransform(130.1,24.5);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AAZA4IAAgxQgFAEgHACQgIACgJABQgJAAgHgDQgIgFgFgHQgEgHAAgNIAAgkIANAAIAAAhQAAAOAHAGQAGAFALAAQAHgBAHgCIALgEIAAgzIANAAIAABvg");
	this.shape_8.setTransform(118.9,24.6);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AgWA3IgGgDIAFgLIAEABIAEABQAFAAADgDQAEgDACgFIABgEIgyhTIAPAAIApBFIAfhFIAOAAIgnBWQgFAMgHAHQgGAGgJAAIgHgBg");
	this.shape_9.setTransform(108.7,24.6);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("AgeA4IAAhvIA9AAIAAANIgwAAIAABig");
	this.shape_10.setTransform(96.5,24.6);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("AAfA4IAAgyIg9AAIAAAyIgNAAIAAhvIANAAIAAAwIA9AAIAAgwIANAAIAABvg");
	this.shape_11.setTransform(85.5,24.6);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("AAhA4IAAhYIhDBYIgMAAIAAhvIAOAAIAABYIBDhYIAMAAIAABvg");
	this.shape_12.setTransform(73.1,24.6);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFFFFF").s().p("AgGA4IAAhiIgnAAIAAgNIBbAAIAAANIgnAAIAABig");
	this.shape_13.setTransform(62,24.6);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FFFFFF").s().p("AAhBFIAAhZIhDBZIgMAAIAAhvIAOAAIAABYIBDhYIAMAAIAABvgAgTg1QgHgGgCgJIANAAQACAFAEADQAEADAGAAQAGAAAFgDQAEgDABgFIANAAQgBAGgEAFQgEAEgGADQgGACgIAAQgMAAgIgFg");
	this.shape_14.setTransform(50.9,23.3);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FFFFFF").s().p("AgfA4IAAhvIA+AAIAAANIgxAAIAAAkIAsAAIAAAMIgsAAIAAAlIAyAAIAAANg");
	this.shape_15.setTransform(40.2,24.6);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FFFFFF").s().p("AgiA4IAAhvIAIAAIALAAIAKAAQAPAAAJAFQAJAFAEAIQADAIAAAJQAAAJgEAIQgEAHgJAFQgJAFgNAAIgIAAIgJgBIAAArgAgVgqIAAAqIAIABIAIAAQAMAAAHgFQAHgGAAgKQAAgLgGgFQgHgGgMAAIgIAAIgJAAg");
	this.shape_16.setTransform(31,24.5);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#FFFFFF").ss(2,1,1).p("Aumj5IdNAAQBsAABMBJQBMBJAABnQAABohMBJQhMBJhsAAI9NAAQhsAAhMhJQhMhJAAhoQAAhnBMhJQBMhJBsAAg");
	this.shape_17.setTransform(120,25);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#185492").s().p("AumD6QhsAAhLhJQhNhKAAhnQAAhmBNhKQBLhJBsAAIdNAAQBrAABMBJQBNBKAABmQAABnhNBKQhMBJhrAAg");
	this.shape_18.setTransform(120,25);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).to({state:[{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]},1).to({state:[{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-0.4,-1,240.9,52);


(lib.fonMC = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(2));

	// Слой 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AjHDIIAAmPIGPAAIAAGPg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(2));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-20,-20,40,40);


(lib.Символ8 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(3));

	// Слой 4
	this.txt = new lib.Символ21();
	this.txt.parent = this;
	this.txt.setTransform(0.2,0.4);

	this.timeline.addTween(cjs.Tween.get(this.txt).wait(3));

	// Слой 3
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#0295D4").s().p("AuvD6QhWAAg8hJQg8hKAAhnQAAhmA8hKQA8hJBWAAIdfAAQBWAAA8BJQA8BKAABmQAABng8BKQg8BJhWAAg");
	this.shape.setTransform(0,-0.5);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#175491").s().p("AuvD6QhWAAg8hJQg8hKAAhnQAAhmA8hKQA8hJBWAAIdfAAQBWAAA8BJQA8BKAABmQAABng8BKQg8BJhWAAg");
	this.shape_1.setTransform(0,-0.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-115,-25.5,230,50);


(lib.Символ5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#30628E").ss(3,1,1).p("AvxgjQC+AGC2A4QCaArCaAJQEDgFBzhAQDbhGCIgSQB2gBBjAKQA4AUCdAQQB4gHA8gc");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	// Слой 2
	this.instance = new lib.Символ6();
	this.instance.parent = this;
	this.instance.setTransform(1.2,210.1);
	this.instance.alpha = 0.68;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Символ5, new cjs.Rectangle(-102.5,-9.4,205.9,437.4), null);


(lib.Символ3копия4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(1));

	// Слой 2
	this.p1 = new lib.Символ19();
	this.p1.parent = this;
	this.p1.setTransform(-71.4,-138.5);

	this.p2 = new lib.Символ18();
	this.p2.parent = this;
	this.p2.setTransform(-71.4,-85.4);

	this.p3 = new lib.Символ16();
	this.p3.parent = this;
	this.p3.setTransform(-71.4,-32.3);

	this.p4 = new lib.Символ15();
	this.p4.parent = this;
	this.p4.setTransform(-71.4,20.7);

	this.p5 = new lib.Символ14();
	this.p5.parent = this;
	this.p5.setTransform(-70.1,77.7);

	this.closebtn = new lib.Символ4();
	this.closebtn.parent = this;
	this.closebtn.setTransform(-67.1,288.8,1.183,0.803,0,0,0,97.2,30.7);
	this.closebtn.alpha = 0.5;

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.closebtn},{t:this.p5},{t:this.p4},{t:this.p3},{t:this.p2},{t:this.p1}]}).wait(1));

	// Слой 4
	this.closebtng = new lib.Символ12();
	this.closebtng.parent = this;
	this.closebtng.setTransform(-67,289);

	this.timeline.addTween(cjs.Tween.get(this.closebtng).wait(1));

	// Слой 3
	this.instance = new lib.raitWin();
	this.instance.parent = this;
	this.instance.setTransform(-411,-350);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Символ3копия4, new cjs.Rectangle(-411,-350,690,690), null);


(lib.Символ3копия3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой 4
	this.mainbtn = new lib.Символ4();
	this.mainbtn.parent = this;
	this.mainbtn.setTransform(-64,204.7,1.449,0.803,0,0,0,97.2,30.7);
	this.mainbtn.alpha = 0.5;

	this.timeline.addTween(cjs.Tween.get(this.mainbtn).wait(1));

	// Слой 2
	this.mainbtng = new lib.Символ10();
	this.mainbtng.parent = this;
	this.mainbtng.setTransform(-64,205);

	this.timeline.addTween(cjs.Tween.get(this.mainbtng).wait(1));

	// Слой 3
	this.instance = new lib.failwindow();
	this.instance.parent = this;
	this.instance.setTransform(-410,-350);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Символ3копия3, new cjs.Rectangle(-410,-350,690,690), null);


(lib.Символ3копия2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.scoretxt.textBaseline="middle";
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(1));

	// Слой 5
	this.mainbtn = new lib.Символ4();
	this.mainbtn.parent = this;
	this.mainbtn.setTransform(-61.4,200.7,1.137,0.803,0,0,0,97.2,30.7);
	this.mainbtn.alpha = 0.5;

	this.timeline.addTween(cjs.Tween.get(this.mainbtn).wait(1));

	// Слой 4
	this.mainbtng = new lib.Символ11();
	this.mainbtng.parent = this;
	this.mainbtng.setTransform(-62,201);

	this.timeline.addTween(cjs.Tween.get(this.mainbtng).wait(1));

	// Слой 3
	this.scoretxt = new cjs.Text("100", "40px 'HappyFoxCondensed'", "#0196D3");
	this.scoretxt.name = "scoretxt";
	this.scoretxt.textAlign = "center";
	this.scoretxt.lineHeight = 48;
	this.scoretxt.lineWidth = 47;
	this.scoretxt.parent = this;
	this.scoretxt.setTransform(-46.4,63.2);

	this.instance = new lib.winwindow2();
	this.instance.parent = this;
	this.instance.setTransform(-411,-350);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance},{t:this.scoretxt}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Символ3копия2, new cjs.Rectangle(-411,-350,690,690), null);


(lib.Символ3копия = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.titletxt.textBaseline="middle";
		this.txt.textBaseline="middle";
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(1));

	// Слой 7
	this.closebtn = new lib.Символ4();
	this.closebtn.parent = this;
	this.closebtn.setTransform(-66.9,288.7,1.183,0.803,0,0,0,97.3,30.7);
	this.closebtn.alpha = 0.5;

	this.closebtng = new lib.Символ12();
	this.closebtng.parent = this;
	this.closebtng.setTransform(-67,289);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.closebtng},{t:this.closebtn}]}).wait(1));

	// Слой 6
	this.txt = new cjs.Text("1234567890 1234567890 12345678901  234567890 1234567890123456789012 3456789012345678901234567890123 4567890", "18px 'Circe'");
	this.txt.name = "txt";
	this.txt.lineHeight = 27;
	this.txt.lineWidth = 646;
	this.txt.parent = this;
	this.txt.setTransform(-381.9,-10);

	this.timeline.addTween(cjs.Tween.get(this.txt).wait(1));

	// Слой 3
	this.titletxt = new cjs.Text("Как сохранить пользу молока дольше? 123456", "40px 'HappyFoxCondensed'", "#FFFFFF");
	this.titletxt.name = "titletxt";
	this.titletxt.lineHeight = 44;
	this.titletxt.lineWidth = 645;
	this.titletxt.parent = this;
	this.titletxt.setTransform(-382.3,-144);

	this.timeline.addTween(cjs.Tween.get(this.titletxt).wait(1));

	// Слой 8
	this.instance = new lib.adw2();
	this.instance.parent = this;
	this.instance.setTransform(-410,-350);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Символ3копия, new cjs.Rectangle(-410,-350,690,690), null);


(lib.Символ4_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		var er = this;
		
		er.traceOff = false;
		
		var unit0Init = false;
		
		
		er.fonmcx.visible = false;
		
		var tim = exportRoot.game.tim;
		
		//граница после которой начисляют очки
		var goodTileStart = 3;
		
		var canMove = false;
		var loopRF = 0;
		
		var level = {
			x: 0, // X position
			y: 0, // Y position
			columns: 6, // Number of tile columns
			rows: 6, // Number of tile rows
			tilewidth: 110, // Visual width of a tile
			tileheight: 110, // Visual height of a tile
			tiles: [], // The two-dimensional tile array
			typesEnabled: 5,
			selectedtile: {
				selected: false,
				column: 0,
				row: 0
			}
		};
		
		var clusters = [];
		var moves = [];
		var drag = false;
		
		//графика////////////////////////////////////////////////
		
		var fonArr = [];
		var tileArr = [];
		
		//////////////////////////////////////////////////
		var currentmove = {
			column1: 0,
			row1: 0,
			column2: 0,
			row2: 0
		};
		
		// Game states
		var gamestates = {
			init: 0,
			ready: 1,
			resolve: 2
		};
		var gamestate = gamestates.init;
		
		// Score
		var score = 0;
		
		// Animation variables
		var animationstate = 0;
		var animationtime = 0;
		var animationtimetotal = 0.3;
		
		// Show available moves
		var showmoves = false;
		
		// The AI bot
		var aibot = false;
		
		// Game Over
		var gameover = false;
		
		var startPath = [];
		
		//stage.enableMouseOver(true);
		
		
		function init0() {
		
			er.addEventListener("pressmove", onMouseMove);
			er.addEventListener("mousedown", onMouseDown);
			er.addEventListener("pressup", onMouseUp);
			er.addEventListener("mouseout", onMouseOut);
		
			startPathCreate();
		
			for (var i = 0; i < level.columns; i++) {
				level.tiles[i] = [];
				for (var j = 0; j < level.rows; j++) {
					// Define a tile type and a shift parameter for animation
					level.tiles[i][j] = {
						type: 0,
						shift: 0,
						mc: null
					}
				}
			}
		}
		
		
		function createLevel() {
			var done = false;
		
			// Keep generating levels until it is correct
			while (!done) {
		
				// Create a level with random tiles
				for (var i = 0; i < level.columns; i++) {
					for (var j = 0; j < level.rows; j++) {
						//console.log(i + " " + j);
						level.tiles[i][j].type = getRandomTile();
					}
				}
		
				// Resolve the clusters
				resolveClusters();
		
				// Check if there are valid moves
				findMoves();
		
				// Done when there is a valid move
				if (moves.length > 2) {
					done = true;
				}
			}
		
			//setTimeout(drawGameStage, 100);
			drawGameStage();
			tileStartIn();
		}
		
		
		function getRandomTile() {
			return randomInteger(1, level.typesEnabled);
		}
		
		function randomInteger(min, max) {
			var rand = min + Math.random() * (max - min)
			rand = Math.round(rand);
			return rand;
		}
		
		this.startGame = function () {
		
			if (!unit0Init) {
				init0();
				createLevel();
				unit0Init = true;
			} else {
				canMove = false;
				er.restartGameAfterGO();
			}
		}
		
		function drawGameStage() {
		
			var s = 0;
			var tile;
		
			for (var i = 0; i < level.columns; i++) {
		
		
				for (var j = 0; j < level.rows; j++) {
		
		
					tile = new lib.tileMC();
		
					tile.x0 = tile.x = level.tilewidth * i + level.tilewidth / 2;
					tile.y0 = tile.y = level.tileheight * j + level.tilewidth / 2;
		
					//trace("x0"+tile.x0 +"y0"+ tile.y0);
		
					tile.column = i;
					tile.row = j;
		
					tile.br.visible = false;
		
					tile.on("mouseover", tileOverOut);
					tile.on("mouseout", tileOverOut);
		
					tileArr.push(tile);
		
					tile.gotoAndStop(level.tiles[i][j].type - 1);
					tile.pic.gotoAndStop(level.tiles[i][j].type - 1);
		
					tile.mouseChildren = false;
					er.addChild(tile);
				}
		
			}
		}
		
		
		function tileOverOut(evt) {
			if (!canMove) {
				return;
			};
			if (evt.type == "mouseover") {
				evt.currentTarget.pic.scaleX = evt.currentTarget.pic.scaleY = 1.05;
			} else if (evt.type == "mouseout") {
				evt.currentTarget.pic.scaleX = evt.currentTarget.pic.scaleY = 1;
			}
		}
		
		function brRemover() {
			for (var i = 0; i < tileArr.length; i++) {
				tileArr[i].br.visible = false;
			}
		}
		
		
		
		//mouse/////////////////////////////////////////////////////
		function onMouseMove(e) {
		
			e.nativeEvent.preventDefault();
			
			if (!canMove) {
				return;
			};
		
			var pos = getMousePos(er, e);
		
			// Check if we are dragging with a tile selected
		
			if (drag && level.selectedtile.selected) {
				// Get the tile under the mouse
		
				mt = getMouseTile(pos);
				//console.log(mt.valid);
				
				if (mt.valid) {
					// Check if the tiles can be swapped
					//console.log(canSwap(mt.x, mt.y, level.selectedtile.column, level.selectedtile.row));
					if (canSwap(mt.x, mt.y, level.selectedtile.column, level.selectedtile.row)) {
						// Swap the tiles				
						mouseSwap(mt.x, mt.y, level.selectedtile.column, level.selectedtile.row);
		
					}
				}
			}
		}
		
		function onMouseDown(e) {
		
			e.nativeEvent.preventDefault();
		
			if (!canMove) {
				return;
			};
		
			var pos = getMousePos(er, e);
			if (!drag) {
				// Get the tile under the mouse
				mt = getMouseTile(pos);
		
				if (mt.valid) {
					// Valid tile		
					var swapped = false;
					if (level.selectedtile.selected) {
						if (mt.x == level.selectedtile.column && mt.y == level.selectedtile.row) {
							// Same tile selected, deselect
							level.selectedtile.selected = false;
							drag = true;
							return;
						} else if (canSwap(mt.x, mt.y, level.selectedtile.column, level.selectedtile.row)) {
							// Tiles can be swapped, swap the tiles
							mouseSwap(mt.x, mt.y, level.selectedtile.column, level.selectedtile.row);
							swapped = true;
						}
					}
		
		
					//console.log(swapped);
		
					if (!swapped) {
						// Set the new selected tile
						level.selectedtile.column = mt.x;
						level.selectedtile.row = mt.y;
						level.selectedtile.selected = true;
		
						showSelected(mt);
					}
				} else {
					// Invalid tile
					level.selectedtile.selected = false;
				}
		
				// Start dragging
				drag = true;
			}
		
		}
		
		function showSelected(mt) {
			for (var i = 0; i < tileArr.length; i++) {
				if ((tileArr[i].x == level.tilewidth * mt.x + level.tilewidth / 2) &&
					(tileArr[i].y == level.tileheight * mt.y + level.tileheight / 2)) {
					tileArr[i].f.alpha = .5;
		
				} else {
					tileArr[i].f.alpha = .01;
				}
			}
		}
		
		function findMC(mcx, mcy) {
			for (var i = 0; i < tileArr.length; i++) {
				if ((tileArr[i].x == mcx + level.tilewidth / 2) &&
					(tileArr[i].y == mcy + level.tileheight / 2)) {
		
					return tileArr[i];
				}
			}
			return null;
		}
		
		function getMousePos(er, e) {
		
			var point = er.globalToLocal(stage.mouseX, stage.mouseY);
		
			return {
				x: Math.round(point.x),
				y: Math.round(point.y)
			};
		}
		
		function onMouseUp(e) {
			// Reset dragging
			e.nativeEvent.preventDefault();
		
			drag = false;
		}
		
		function onMouseOut(e) {
			// Reset dragging
			e.nativeEvent.preventDefault();
		
			drag = false;
		}
		
		
		
		
		//логика//////////////////////////////////////////////////////
		//////////////////////////////////////////////////////
		
		function resolveClusters() {
			// Check for clusters
			findClusters();
		
			// While there are clusters left
			while (clusters.length > 0) {
				// Remove clusters
		
				removeClusters();
		
				// Shift tiles
				shiftTiles();
		
				// Check if there are clusters left
				findClusters();
			}
		}
		
		
		// Find clusters in the level
		function findClusters(setScore) {
			// Reset clusters
			if (setScore === undefined) {
				setScore = false;
			}
		
		
			clusters = [];
		
			// Find horizontal clusters
			for (var j = 0; j < level.rows; j++) {
				// Start with a single tile, cluster of 1
				var matchlength = 1;
				for (var i = 0; i < level.columns; i++) {
					var checkcluster = false;
		
					if (i == level.columns - 1) {
						// Last tile
						checkcluster = true;
					} else {
						// Check the type of the next tile
						if (level.tiles[i][j].type == level.tiles[i + 1][j].type &&
							level.tiles[i][j].type != -1) {
							// Same type as the previous tile, increase matchlength
							matchlength += 1;
						} else {
							// Different type
							checkcluster = true;
						}
					}
		
					// Check if there was a cluster
					if (checkcluster) {
						if (matchlength >= 3) {
							// Found a horizontal cluster
		
							if (setScore) {
								//trace("нашёл на удаление(" + matchlength + ") тип=" + level.tiles[i][j].type)
								sendScore(matchlength, level.tiles[i][j].type, i, j, "h");
							};
		
							clusters.push({
								column: i + 1 - matchlength,
								row: j,
								length: matchlength,
								horizontal: true
							});
						}
		
						matchlength = 1;
					}
				}
			}
		
			// Find vertical clusters
			for (var i = 0; i < level.columns; i++) {
				// Start with a single tile, cluster of 1
				var matchlength = 1;
				for (var j = 0; j < level.rows; j++) {
					var checkcluster = false;
		
					if (j == level.rows - 1) {
						// Last tile
						checkcluster = true;
					} else {
						// Check the type of the next tile
						if (level.tiles[i][j].type == level.tiles[i][j + 1].type &&
							level.tiles[i][j].type != -1) {
							// Same type as the previous tile, increase matchlength
							matchlength += 1;
						} else {
							// Different type
							checkcluster = true;
						}
					}
		
					// Check if there was a cluster
					if (checkcluster) {
						if (matchlength >= 3) {
							// Found a vertical cluster
							if (setScore) {
								//trace("нашёл на удаление(" + matchlength + ") тип=" + level.tiles[i][j].type);
								sendScore(matchlength, level.tiles[i][j].type, i, j, "v");
							};
							clusters.push({
								column: i,
								row: j + 1 - matchlength,
								length: matchlength,
								horizontal: false
							});
						}
		
						matchlength = 1;
					}
				}
			}
		}
		
		
		function removeClusters() {
			// Change the type of the tiles to -1, indicating a removed tile
			loopClusters(function (index, column, row, cluster) {
				level.tiles[column][row].type = -1;
			});
		
			// Calculate how much a tile should be shifted downwards
			for (var i = 0; i < level.columns; i++) {
				var shift = 0;
				for (var j = level.rows - 1; j >= 0; j--) {
					// Loop from bottom to top
					if (level.tiles[i][j].type == -1) {
						// Tile is removed, increase shift
						shift++;
						level.tiles[i][j].shift = 0;
					} else {
						// Set the shift
						level.tiles[i][j].shift = shift;
					}
				}
			}
		}
		
		// Shift tiles and insert new tiles
		function shiftTiles() {
			// Shift tiles
			for (var i = 0; i < level.columns; i++) {
				for (var j = level.rows - 1; j >= 0; j--) {
					// Loop from bottom to top
					if (level.tiles[i][j].type == -1) {
						// Insert new random tile
						level.tiles[i][j].type = getRandomTile();
					} else {
						// Swap tile to shift it
						var shift = level.tiles[i][j].shift;
						if (shift > 0) {
							swap(i, j, i, j + shift)
						}
					}
		
					// Reset shift
					level.tiles[i][j].shift = 0;
				}
			}
		}
		
		// Find available moves
		function findMoves() {
			// Reset moves
			moves = []
		
			// Check horizontal swaps
			for (var j = 0; j < level.rows; j++) {
				for (var i = 0; i < level.columns - 1; i++) {
					// Swap, find clusters and swap back
					swap(i, j, i + 1, j);
					findClusters();
					swap(i, j, i + 1, j);
		
					// Check if the swap made a cluster
					if (clusters.length > 0) {
						// Found a move
						moves.push({
							column1: i,
							row1: j,
							column2: i + 1,
							row2: j
						});
					}
				}
			}
		
			// Check vertical swaps
			for (var i = 0; i < level.columns; i++) {
				for (var j = 0; j < level.rows - 1; j++) {
					// Swap, find clusters and swap back
					swap(i, j, i, j + 1);
					findClusters();
					swap(i, j, i, j + 1);
		
					// Check if the swap made a cluster
					if (clusters.length > 0) {
						// Found a move
						moves.push({
							column1: i,
							row1: j,
							column2: i,
							row2: j + 1
						});
					}
				}
			}
		
			// Reset clusters
			clusters = []
		}
		
		function swap(x1, y1, x2, y2) {
			var typeswap = level.tiles[x1][y1].type;
			level.tiles[x1][y1].type = level.tiles[x2][y2].type;
			level.tiles[x2][y2].type = typeswap;
		}
		
		function canSwap(x1, y1, x2, y2) {
			// Check if the tile is a direct neighbor of the selected tile
			if ((Math.abs(x1 - x2) == 1 && y1 == y2) ||
				(Math.abs(y1 - y2) == 1 && x1 == x2)) {
				return true;
			}
		
			return false;
		}
		
		function loopClusters(func) {
			for (var i = 0; i < clusters.length; i++) {
				//  { column, row, length, horizontal }
				var cluster = clusters[i];
				var coffset = 0;
				var roffset = 0;
				for (var j = 0; j < cluster.length; j++) {
					func(i, cluster.column + coffset, cluster.row + roffset, cluster);
		
					if (cluster.horizontal) {
						coffset++;
					} else {
						roffset++;
					}
				}
			}
		}
		
		function getMouseTile(pos) {
			// Calculate the index of the tile
			var tx = Math.floor((pos.x - level.x) / level.tilewidth);
			var ty = Math.floor((pos.y - level.y) / level.tileheight);
		
			// Check if the tile is valid
			if (tx >= 0 && tx < level.columns && ty >= 0 && ty < level.rows) {
				// Tile is valid
				return {
					valid: true,
					x: tx,
					y: ty
				};
			}
		
			// No valid tile
			return {
				valid: false,
				x: 0,
				y: 0
			};
		}
		
		function mouseSwap(c1, r1, c2, r2) {
			// Save the current move
			currentmove = {
				column1: c1,
				row1: r1,
				column2: c2,
				row2: r2
			};
		
			// Deselect
			level.selectedtile.selected = false;
		
			// Start animation
			animationstate = 2;
			startSwapAnimation();
			animationtime = 0;
			gamestate = gamestates.resolve;
		}
		
		var turn = {
			mc1: null,
			col1: null,
			c1: 0,
			r1: 0,
			x1: 0,
			y1: 0,
			mc2: null,
			col2: null,
			c2: 0,
			r2: 0,
			x2: 0,
			y2: 0
		}
		
			function startSwapAnimation() {
		
				brRemover();
		
				canMove = false;
		
				var shiftx = currentmove.column2 - currentmove.column1;
				var shifty = currentmove.row2 - currentmove.row1;
		
				// First tile
				var coord1 = getTileCoordinate(currentmove.column1, currentmove.row1, 0, 0);
				var coord1shift = getTileCoordinate(currentmove.column1, currentmove.row1, (animationtime / animationtimetotal) * shiftx, (animationtime / animationtimetotal) * shifty);
				var col1 = level.tiles[currentmove.column1][currentmove.row1].type;
				// Second tile
				var coord2 = getTileCoordinate(currentmove.column2, currentmove.row2, 0, 0);
				var coord2shift = getTileCoordinate(currentmove.column2, currentmove.row2, (animationtime / animationtimetotal) * -shiftx, (animationtime / animationtimetotal) * -shifty);
				var col2 = level.tiles[currentmove.column2][currentmove.row2].type;
		
				var mc1 = findMC2(currentmove.column1, currentmove.row1);
				var mc2 = findMC2(currentmove.column2, currentmove.row2);
		
				turn.mc1 = mc1;
				turn.mc2 = mc2;
				turn.col1 = col1;
				turn.col2 = col2;
				turn.c1 = currentmove.column1;
				turn.c2 = currentmove.column2;
				turn.r1 = currentmove.row1;
				turn.r2 = currentmove.row2;
		
				level.tiles[currentmove.column2][currentmove.row2].type = col1;
				level.tiles[currentmove.column1][currentmove.row1].type = col2;
		
		
				mc1.parent.addChild(mc1);
				mc2.parent.addChild(mc2);
		
		
				mc1.f.alpha = .01;
				mc2.f.alpha = .01;
		
				createjs.Tween.get(mc1).to({
					x: mc2.x0,
					y: mc2.y0
				}, 300);
				createjs.Tween.get(mc2).to({
					x: mc1.x0,
					y: mc1.y0
				}, 300); //.call(changeTile);
		
				timAnimationWithDelay(changeTile, 350);
			}
		
			function changeTile() {
		
				var mc1;
				var mc2;
		
				findClusters(true);
		
				if (clusters.length > 0) {
					//trace("Очко!");
		
					reDrawMap();
		
					removeClusters();
					loopRF = 0;
					deleteTileAnimation();
				} else {
					//trace("Не Очко!");
		
					level.tiles[currentmove.column1][currentmove.row1].type = turn.col1;
					level.tiles[currentmove.column2][currentmove.row2].type = turn.col2;
		
					mc1 = turn.mc1;
					mc2 = turn.mc2;
		
					mc1.parent.addChild(mc1);
					mc2.parent.addChild(mc2);
		
					createjs.Tween.get(mc1).to({
						x: mc1.x0,
						y: mc1.y0
					}, 300);
					createjs.Tween.get(mc2).to({
						x: mc2.x0,
						y: mc2.y0
					}, 300);
					timAnimationWithDelay(reDrawMap, 350);
					timAnimationWithDelay(setCanMove, 360);
				}
			}
		
		
		
			function gameContinue() {
				findMoves();
		
				if (moves.length > 0) {
		
					setCanMove();
					// Get a random valid move
					//var move = moves[Math.floor(Math.random() * moves.length)];                        
					// Simulate a player using the mouse to swap two tiles
					//mouseSwap(move.column1, move.row1, move.column2, move.row2);
				} else {
		
					//trace("GameOver");
					exportRoot.game.onGame = false;
					/*for (var i=0; i<level.columns; i++) {
		                for (var j=0; j<level.rows; j++) {
		                    level.tiles[i][j].type = -1;
		                }
		            }*/
					reDrawMap();
					timAnimationWithDelay(tileStartOut, 200);
		
		
					//timAnimation(reDrawReCreate);
		
					// No moves left, Game Over. We could start a new game.
					// newGame();
				}
			}
		
			function reDrawReCreate() {
				//reDrawMap();	
				reCreateStage();
			}
		
			function reCreateStage() {
				var done = false;
				while (!done) {
		
					// Create a level with random tiles
					for (var i = 0; i < level.columns; i++) {
						for (var j = 0; j < level.rows; j++) {
							level.tiles[i][j].type = getRandomTile();
						}
					}
		
					// Resolve the clusters
					resolveClusters();
		
					// Check if there are valid moves
					findMoves();
		
					// Done when there is a valid move
					if (moves.length > 0) {
						done = true;
					}
				}
		
				reDrawMap();
		
			}
		
			function timAnimation(func) {
				tim.alpha = 1;
				createjs.Tween.get(tim).to({
					alpha: 0
				}, 100).to({
					alpha: 1
				}, 100).call(func);
			}
		
		
		
			function deliteAlphaZero2() {
				reDrawMap();
			}
		
		
		
		
			function reDrawMap() {
				var mc;
		
				for (var i = 0; i < level.columns; i++) {
					for (var j = 0; j < level.rows; j++) {
						mc = findMC2(i, j);
		
		
						mc.x = mc.x0;
						mc.y = mc.y0;
		
						if (mc.scaleX != 1) {
							//trace("Scale Fix");
							mc.scaleX = 1;
							mc.scaleY = 1;
						}
		
		
						if (level.tiles[i][j].type != -1) {
							mc.alpha = 1;
							mc.gotoAndStop(level.tiles[i][j].type - 1);
							mc.f.alpha = .01;
							mc.pic.scaleX = mc.pic.scaleY = 1;
							mc.pic.gotoAndStop(level.tiles[i][j].type - 1);
						} else {
							mc.alpha = 0;
						}
					}
				}
			}
		
			function findMC2(c, r) {
		
				for (var i = 0; i < tileArr.length; i++) {
					if (tileArr[i].column == c && tileArr[i].row == r) {
						return tileArr[i];
					}
				}
				return null;
				//trace("findMC2 tile error");
			}
		
			function findMcToShift() {
				for (var i = 0; i < level.columns; i++) {
					for (var j = level.rows - 1; j >= 0; j--) {
						if (level.tiles[i][j].type == -1) {
		
						}
					}
				}
			}
		
		
		
			function anim2End() {
				//trace("Был фейл мув, продолжаем");
			}
		
			function getTileCoordinate(column, row, columnoffset, rowoffset) {
				var tilex = level.x + (column + columnoffset) * level.tilewidth;
				var tiley = level.y + (row + rowoffset) * level.tileheight;
				return {
					tilex: tilex,
					tiley: tiley
				};
			}
		
			//render////////////////////
		
			//exportRoot.helpBTN.addEventListener("click", renderMoves);
		
		er.lineArray = [];
		/*
		function renderMoves() {
		
			var i = 0;
		
			if (exportRoot.helpBTN.f.alpha == 1) {
				for (i = 0; i < er.lineArray.length; i++) {
					er.removeChild(er.lineArray[i]);
				}
				er.lineArray = [];
				exportRoot.helpBTN.f.alpha = .5;
			} else {
		
				for (i = 0; i < moves.length; i++) {
					// Calculate coordinates of tile 1 and 2
					var coord1 = getTileCoordinate(moves[i].column1, moves[i].row1, 0, 0);
					var coord2 = getTileCoordinate(moves[i].column2, moves[i].row2, 0, 0);
		
					// Draw a line from tile 1 to tile 2
		
					var line = new createjs.Shape();
					line.graphics.setStrokeStyle(6).beginStroke("#14B055");
					line.graphics.moveTo(coord1.tilex + level.tilewidth / 2, coord1.tiley + level.tileheight / 2);
					line.graphics.lineTo(coord2.tilex + level.tilewidth / 2, coord2.tiley + level.tileheight / 2);
					line.graphics.endStroke();
					er.addChild(line);
					er.lineArray.push(line);
		
				}
		
				exportRoot.helpBTN.f.alpha = 1;
			}
		}
		*/
		
		//createjs.Ticker.timingMode = createjs.Ticker.RAF;
		//createjs.Ticker.addEventListener("tick", tick);
		//function tick(event) {
		//}
		
		function drawTileX(x, y, mc) {
		
			//context.fillStyle = "rgb(" + r + "," + g + "," + b + ")";
			//context.fillRect(x + 2, y + 2, level.tilewidth - 4, level.tileheight - 4);
		}
		
		function drawTile(x, y, r, g, b) {
			context.fillStyle = "rgb(" + r + "," + g + "," + b + ")";
			context.fillRect(x + 2, y + 2, level.tilewidth - 4, level.tileheight - 4);
		}
		
		var tilecolors = [
			[255, 128, 128],
			[128, 255, 128],
			[128, 128, 255],
			[255, 255, 128],
			[255, 128, 255],
			[128, 255, 255],
			[255, 255, 255]
		];
		
		function trace(str) {
			if (er.traceOff) {
				return;
			}
			console.log(str);
		}
		
		function traceMap() {
		
		
			var str = "";
			trace("===MAP===");
			for (var i = 0; i < level.rows; i++) {
				str = "";
				for (var j = 0; j < level.columns; j++) {
					str += level.tiles[j][i].type + ",";
				}
				trace(str);
			}
			trace("===\MAP===");
		}
		
		function startPathCreate() {
			var miniArr = [];
			for (var i = 0; i < level.rows; i++) {
				if ((i + 1) % 2) {
					//нечетный
					for (var j = 0; j < level.columns; j++) {
						miniArr = [];
						miniArr = [j, i];
						startPath.push(miniArr);
					}
		
				} else {
					//четный
					for (var j = level.columns - 1; j >= 0; j--) {
						miniArr = [];
						miniArr = [j, i];
						startPath.push(miniArr);
					}
		
				}
			}
		
			startPath.reverse();
		
		}
		
		function tileStartIn() {
			var mc;
			for (var i = 0; i < tileArr.length; i++) {
				//tileArr[i].y = -level.tileheight+50;
				//createjs.Tween.get(tileArr[i]).wait(i*500).to({y:tileArr[i].y0},500, createjs.Ease.getPowIn(2.5));
				mc = findMC2(startPath[i][0], startPath[i][1]);
				mc.y = -level.tileheight - 50; //-mc.y0;
				//createjs.Tween.get(mc).wait(i*100).to({y:mc.y0},500, createjs.Ease.getPowIn(2.5))
		
				if (i != tileArr.length - 1) {
					createjs.Tween.get(mc).wait(i * 100).to({
						y: mc.y0
					}, 250, createjs.Ease.getPowIn(2.5))
				} else {
					createjs.Tween.get(mc).wait(i * 100).to({
						y: mc.y0
					}, 250, createjs.Ease.getPowIn(2.5)).call(endTileStartIn);
				}
			}
		
			//.call(fadeTweenComplete)
		}
		
		function endTileStartIn() {
			/*findMoves();
		
				// Done when there is a valid move
			if (moves.length <= 0) {
					//   done = true;
				tileStartOut ();
			} else {
				
			}*/
		
			exportRoot.game.onGame = true;
			setCanMove();
		
		}
		
		function tileStartOut() {
		
			for (var i = 0; i < tileArr.length; i++) {
				//tileArr[i].y = -level.tileheight+50;
				//createjs.Tween.get(tileArr[i]).wait(i*500).to({y:tileArr[i].y0},500, createjs.Ease.getPowIn(2.5));
		
				mc = findMC2(startPath[i][0], startPath[i][1]);
				mc.parent.addChild(mc);
				//mc.y=-level.tileheight+50;//-mc.y0;
		
				if (i != tileArr.length - 1) {
					createjs.Tween.get(mc).wait(i * 100).to({
						y: level.tileheight * level.rows + level.tileheight + 50
					}, 200, createjs.Ease.getPowIn(2.5));
				} else {
					createjs.Tween.get(mc).wait(i * 100).to({
						y: level.tileheight * level.rows + level.tileheight + 50
					}, 210, createjs.Ease.getPowIn(2.5)).call(endtileStartOut);
				}
			}
		}
		
		function endtileStartOut() {
		
			er.restartGameAfterGO();
		
			//exportRoot.startGameOver();
			//reDrawMap();
			//reDrawReCreate();
			//tileStartIn();
		}
		
		er.restartGameAfterGO = function () {
			reDrawMap();
			reDrawReCreate();
			tileStartIn();
		}
		
		er.restartGamePrepair = function () {
			reDrawMap();
			reDrawReCreate();
		}
		
		function timAnimationWithDelay(func, delay) {
			tim.alpha = 1;
			var halfDelay = Math.floor(delay / 2);
			createjs.Tween.get(tim).to({
				alpha: 0
			}, halfDelay).to({
				alpha: 1
			}, halfDelay).call(func);
		}
		
		
		
		
		
		var fallArray = [];
		
		function deleteTileUp() {
		
			var shift = 1;
			var mc;
			var tempArr = [];
			fallArray = [];
			for (var i = 0; i < level.columns; i++) {
				shift = 1;
				for (var j = 0; j < level.rows; j++) {
					if (level.tiles[i][j].type == -1) {
						mc = findMC2(i, j);
						//mc.alpha = 1;
						mc.scaleX = 1;
						mc.scaleY = 1;
						mc.y = level.tileheight / 2 - level.tileheight * shift;
						shift++;
						tempArr = [i, j, shift];
					}
				}
				fallArray.push(tempArr);
			}
		}
		
		function fallAnimation() {
			var mc;
			var col;
			var sht=0;
			var sdv=0;
		
			var colSFT = 0;
			var fallTileAndSdvig = [];
			var fallMini = [];
			var i = 0;
			var j = 0;
		
			var tile;
		
			var rArr = [];
			var metka = "x";
			/*
			for ( i = 0; i < level.columns; i++) {
				
				for ( j = 0; j < level.rows; i++) {
					
					tile = findMC2(i,j);
					//trace("x"+tile.x +" y"+ tile.y +" a"+ tile.alpha + " " + tile.name);
					if (tile!=null)
					{
						metka = "v";
					} else {
						metka = "x";
					}
					
					
				}
			}
			*/
		
			var str = "";
			/*
			trace("===MAP===");
			trace(gRTS(0,0)+","+gRTS(1,0)+","+gRTS(2,0)+","+gRTS(3,0)+","+gRTS(4,0)+","+gRTS(5,0));
			trace(gRTS(0,1)+","+gRTS(1,1)+","+gRTS(2,1)+","+gRTS(3,1)+","+gRTS(4,1)+","+gRTS(5,1));
			trace(gRTS(0,2)+","+gRTS(1,2)+","+gRTS(2,2)+","+gRTS(3,2)+","+gRTS(4,2)+","+gRTS(5,2));
			trace(gRTS(0,3)+","+gRTS(1,3)+","+gRTS(2,3)+","+gRTS(3,3)+","+gRTS(4,3)+","+gRTS(5,3));
			trace(gRTS(0,4)+","+gRTS(1,4)+","+gRTS(2,4)+","+gRTS(3,4)+","+gRTS(4,4)+","+gRTS(5,4));
			trace(gRTS(0,5)+","+gRTS(1,5)+","+gRTS(2,5)+","+gRTS(3,5)+","+gRTS(4,5)+","+gRTS(5,5));	
			trace("===\MAP===");
			*/
		
			var columnMaxShift = [];
		
			for (i = 0; i < level.columns; i++) {
				colSFT = 0;
				for (j = level.rows; j >= 0; j--) {
		
					if (gRTS(i, j) == 0) {
						colSFT++;
					}
		
					if (colSFT > 0) {
						tile = findMC2(i, j);
						fallMini = [];
						fallMini = [tile, colSFT, i];
						fallTileAndSdvig.push(fallMini);
					}
		
				}
				columnMaxShift.push(colSFT);
			}
		
			for (i = 0; i < tileArr.length; i++) {
				if (tileArr[i].y < 0) {
		
					//trace(col)
					col = Math.round(tileArr[i].x / level.tilewidth) - 1;			
					sdv = -((tileArr[i].y - level.tileheight / 2) / level.tileheight);
					sdv = Math.round(sdv);
					for (var j = 0; j < fallArray.length; j++) {
						if (Math.round(fallArray[j][0]) == col) {
							sht = fallArray[j][2] - 1;
							//trace("col="+col+" sht="+(fallArray[j][2] - 1));
							break;
						}
					}
		
					//trace("tileArr[" + i + "].gotoAndStop(level.tiles[" + col + "][Math.abs(" + sdv + "-" + sht + ")].type-1)");
					//баг?		
		
					//trace(col + "," + Math.abs(sdv - sht))
		
					var picframe = level.typesEnabled+1;			
		
					try {				
						picframe = level.tiles[col][Math.abs(sdv - sht)].type - 1;
						
						if (typeof picframe == 'undefined')
						{
							trace("fallAnimation error (" + col + "," + Math.abs(sdv - sht) + ")");
							trace("sdv="+sdv);
							trace("sht="+sht);
							//picframe = level.typesEnabled+1;
						}
					} catch (e) {
						trace("fallAnimation error (" + col + "," + Math.abs(sdv - sht) + ")");
						trace("sdv="+sdv);					
						trace("sht="+sht);
					}
		
					tileArr[i].gotoAndStop(picframe);
					tileArr[i].pic.gotoAndStop(picframe);
					tileArr[i].pic.scaleX = tileArr[i].pic.scaleY = 1;
				}
			}
		
			var fally;
		
			for (i = 0; i < tileArr.length; i++) {
				for (j = 0; j < fallArray.length; j++) {
		
					try {
		
						col = Math.round(tileArr[i].x / level.tilewidth) - 1;
						if ((fallArray[j][0] == col) && (tileArr[i].y < (fallArray[j][1] * level.tileheight) - 1)) {
							tileArr[i].alpha = 1;
							tileArr[i].scaleX = 1;
							tileArr[i].scaleY = 1;
							tileArr[i].parent.addChild(tileArr[i]);
		
							//fally = tileArr[i].y + (fallArray[j][2] - 1) * level.tileheight;
							//createjs.Tween.get(tileArr[i]).wait(0).to({y: fally}, 300, createjs.Ease.getPowIn(2.5));
						}
		
					} catch (e) {
						trace("fallAnimation error (" + e + ")");
						continue;
					}
				}
			}
		
			//newFallY!!!
			for (i = 0; i < fallTileAndSdvig.length; i++) {
				try {
				tile = fallTileAndSdvig[i][0];
		
				if (tile.y < level.tileheight / 2) {
					//если обновленцы - узнаём колонну, присваем максимальный шифт			
					fally = columnMaxShift[fallTileAndSdvig[i][2]] * level.tileheight + tile.y;
				} else {
					fally = fallTileAndSdvig[i][1] * level.tileheight + tile.y;
				}
		
				createjs.Tween.get(tile).wait(0).to({
					y: fally
				}, 300, createjs.Ease.getPowIn(2.5));
				
				} catch (e) {
						trace("fallAnimation error (" + e + ")");
						continue;
					}
				//trace("тайл из (" + tile.x + "," + tile.y + ") летит на " + fally+", т.к. сдвиг ="+fallTileAndSdvig[i][1])
			}
		
		
			timAnimationWithDelay(loopReorganization, 700);
		}
		
		function gRTS(x, y) {
			var tile = findMC2(x, y);
		
			if (tile != null) {
				return tile.alpha;
				//return "("+tile.x+","+tile.y+")";
			} else {
				return "x";
			}
		
		}
		
		
		function setCanMove() {
			canMove = true;
		}
		
		function deleteTileAnimation() {
			//reDrawMap();	
			var mc;
			for (var i = 0; i < level.columns; i++) {
				for (var j = level.rows - 1; j >= 0; j--) {
					if (level.tiles[i][j].type == -1) {
						mc = findMC2(i, j);
						//добавляем очки
						//if (mc.currentFrame>goodTileStart-1)
						//{
						//trace("добавляем очки+1");
						//exportRoot.setNewScore(1);
						//}				
						//trace("mc.alpha=" + mc.alpha);
						createjs.Tween.get(mc).wait(0).to({
							alpha: 0,
							scaleX: .5,
							scaleY: .5
						}, 300, createjs.Ease.getPowIn(2.5));
					}
				}
			}
		
			loopRF = 1;
			timAnimationWithDelay(deleteTileUp, 350);
			timAnimationWithDelay(loopReorganization, 400);
		}
		
		
		function loopReorganization() {
			//trace("Вход в loopReorganization с " + loopRF); // Вход в loopReorganization с 2 баг!!!
		
			switch (loopRF) {
				case 0:
					reDrawMap();
					removeClusters();
					//reDrawMap();
					loopRF = 1;
					//timAnimationWithDelay(deleteTileAnimation, 100);	
					//timAnimationWithDelay(loopReorganization, 500);
					//timAnimation(loopReorganization);
					//loopReorganization();
					//timAnimationWithDelay(loopReorganization, 100);		
					break;
				case 1:
					/*if (clusters.length > 0)
						{
							reDrawMap();
							removeClusters();
							timAnimationWithDelay(deleteTileAnimation, 100);
							return;
						}*/
					//removeClusters();
					shiftTiles();
					//findClusters();
					loopRF = 2;
					//reDrawMap();
					fallAnimation();
					break;
				case 2:
					//trace("вход в 2");
					findClusters(true);
					loopRF = 0;
					reDrawMap();
		
					//trace("clusters.length=" + clusters.length);
		
					if (clusters.length > 0) {
						//trace("clusters.length > 0")
						//reDrawMap();
						removeClusters();
						//loopRF = 0;
						deleteTileAnimation();
					} else {
						//trace("clusters.length <= 0")
						gameContinue();
						//продолжаем играть
					}
		
					break;
			}
			//trace("Выход в loopReorganization с " + loopRF);
		}
		
		function deleteAnim(mc) {
			er.removeChild(mc);
		}
		
		function sendScore(matchlength, type, i, j, position) {
		
			//position h / v	
			//animation(?)
			var anim = new lib.delEffect();
			anim.x = i * level.tilewidth + level.tilewidth / 2;
			anim.y = j * level.tileheight + level.tileheight / 2;
		
			var sdvig = 1;
		
		
		
			if (matchlength == 3) {
				exportRoot.setNewScore(1);
				anim.gotoAndStop(0);
				sdvig = 1;
			} else if (matchlength == 4) {
				exportRoot.setNewScore(2);
				anim.gotoAndStop(1);
				sdvig = 1.5;
			} else if (matchlength >= 5) {
		
				if (type == 1) {
					exportRoot.setNewScore(4);
				} else {
					exportRoot.setNewScore(3);
				}
		
				anim.gotoAndStop(2);
				sdvig = 2;
			}
		
			er.addChild(anim);
		
			if (position == "h") {
				anim.x -= sdvig * level.tilewidth;
			} else {
				anim.y -= sdvig * level.tileheight;
			}
		
			anim.scaleX = .01;
			anim.scaleY = .01;
			anim.alpha = 0;
		
			createjs.Tween.get(anim)
				.to({
					rotation: 0
				}, 500)
				.call(upAnim, [anim], this);
		
			createjs.Tween.get(anim)
				.to({
					scaleX: 1,
					scaleY: 1,
					alpha: 1
				}, 150)
				.wait(500)
				.to({
					y: anim.y - 50,
					alpha: 0.1
				}, 300)
				.call(deleteAnim, [anim], this);
		
		
		
			// .to({y: anim.y-50, alpha:0, scaleX: 0.1, scaleY: 0.1, visible:false}, 200)
		}
		
		function upAnim(mc) {
			try{
			mc.parent.addChild(mc);
			}catch(e)
			{		
			}
		}
		
		//er.startGame();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(1));

	// Слой 5
	this.fonmcx = new lib.Символ17();
	this.fonmcx.parent = this;
	this.fonmcx.setTransform(50,50,1,1,0,0,0,50,50);

	this.timeline.addTween(cjs.Tween.get(this.fonmcx).wait(1));

}).prototype = getMCSymbolPrototype(lib.Символ4_1, new cjs.Rectangle(0,-24,314,142), null);


(lib.tileMC = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(5));

	// Layer 2
	this.br = new lib.Symbol1();
	this.br.parent = this;
	this.br.setTransform(0,0,1.136,1.136,0,0,0,44,44);
	this.br.alpha = 0.262;

	this.timeline.addTween(cjs.Tween.get(this.br).wait(5));

	// Слой 5
	this.pic = new lib.Символ20();
	this.pic.parent = this;
	this.pic.setTransform(-0.5,-0.5);

	this.timeline.addTween(cjs.Tween.get(this.pic).wait(5));

	// Слой 2
	this.f = new lib.f();
	this.f.parent = this;
	this.f.setTransform(0.7,-1.2,1.209,1.196,0,0,0,60,60);
	this.f.alpha = 0.012;

	this.timeline.addTween(cjs.Tween.get(this.f).wait(5));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-55,-55,110,110);


(lib.startwindow = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой 2
	this.startGamebtn = new lib.Символ4();
	this.startGamebtn.parent = this;
	this.startGamebtn.setTransform(-71,264.6,1.183,0.803,0,0,0,97.2,30.6);
	this.startGamebtn.alpha = 0.5;

	this.timeline.addTween(cjs.Tween.get(this.startGamebtn).wait(1));

	// Слой 1
	this.startGamebtng = new lib.Символ8();
	this.startGamebtng.parent = this;
	this.startGamebtng.setTransform(-71,265);

	this.timeline.addTween(cjs.Tween.get(this.startGamebtng).wait(1));

	// Слой 4
	this.instance = new lib.start3();
	this.instance.parent = this;
	this.instance.setTransform(-411,-350);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.startwindow, new cjs.Rectangle(-411,-350,690,690), null);


(lib.Символ22 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой 4
	this.ptxt = new cjs.Text("100", "60px 'HappyFoxCondensed'", "#FFFFFF");
	this.ptxt.name = "ptxt";
	this.ptxt.textAlign = "center";
	this.ptxt.lineHeight = 72;
	this.ptxt.lineWidth = 75;
	this.ptxt.parent = this;
	this.ptxt.setTransform(-54.6,-50.9);

	this.instance = new lib.percent();
	this.instance.parent = this;
	this.instance.setTransform(-91,-55);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance},{t:this.ptxt}]}).wait(1));

	// Слой 5
	this.instance_1 = new lib.m2();
	this.instance_1.parent = this;
	this.instance_1.setTransform(-109,-261);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(1));

	// Слой 1 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("Av2fuMgAKg+HIRIBnIK6G5ICCg/IgUkTICRoXMAAABCAIvPFFg");
	mask.setTransform(-1.5,-3.5);

	// Слой 2
	this.milkmc = new lib.Символ5();
	this.milkmc.parent = this;
	this.milkmc.setTransform(-2,227.1,1,1,0,0,0,-0.1,0.1);

	var maskedShapeInstanceList = [this.milkmc];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.milkmc).wait(1));

	// Слой 3
	this.instance_2 = new lib.milkbox();
	this.instance_2.parent = this;
	this.instance_2.setTransform(-104,-240);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(1));

	// Слой 8
	this.instance_3 = new lib.m4();
	this.instance_3.parent = this;
	this.instance_3.setTransform(-175,110);

	this.instance_4 = new lib.m3();
	this.instance_4.parent = this;
	this.instance_4.setTransform(-153,-19);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_4},{t:this.instance_3}]}).wait(1));

	// Слой 7
	this.instance_5 = new lib.Символ23();
	this.instance_5.parent = this;
	this.instance_5.setTransform(-46.2,-20.1);
	this.instance_5.alpha = 0.012;

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(1));

}).prototype = getMCSymbolPrototype(lib.Символ22, new cjs.Rectangle(-178.3,-264.1,314,497.1), null);


(lib.Символ1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		var er = this;
		er.onGame = false;
		
		er.scoretxt.textBaseline="middle";
		er.timertxt.textBaseline="middle";
		er.milkbox.ptxt.textBaseline="middle";
		
		er.milkbox.mouseChildren=false;
		
		er.scoretxt.y+=36;
		er.timertxt.y+=36;
		er.milkbox.ptxt.y+=36;
		
		er.playTime = 120;
		//er.timertxt.text = "02:00";
		
		er.timerId;
		
		er.startTimer = function () {
			er.timerId = setInterval(function () {
		
				if (!er.onGame) {
					return;
				}
		
				if (er.playTime == 0) {
					clearInterval(er.timerId);
					//start gameover
					exportRoot.startGameOver();
				} else {
					er.playTime--;
					er.showtime();
				}
			}, 1000);
		}
		
		er.stopTimer = function () {
			clearInterval(er.timerId);
		}
		
		
		er.showtime = function() {
			var min = (Math.floor(er.playTime / 60));
			var sec = er.playTime % 60;
		
			var zmin = "";
			var zsec = "";
		
			if (min < 10) {
				zmin = "0";
			}
			if (sec < 10) {
				zsec = "0";
			}
			er.timertxt.text = zmin + min + ":" + zsec + sec;
		}
		
		er.milkAnimation = function(percent) {
			
			//milkbox.milkmc
			//milkbox.ptxt
			
			
			if (percent==0)
			{		
				er.milkbox.milkmc.y = 227;
			} else if (percent>=100)
			{
				er.milkbox.milkmc.y = -173;
				percent=100;
			} else {
				er.milkbox.milkmc.y = 227 - 4*percent;
			}
			er.milkbox.ptxt.text = percent;
		}
		
		er.milkAnimation(0);
		//er.startTimer();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(1));

	// Слой 11
	this.rulesbtn = new lib.Символ4();
	this.rulesbtn.parent = this;
	this.rulesbtn.setTransform(-685.4,316.5,1.234,0.837,0,0,0,0,31.1);
	this.rulesbtn.alpha = 0.5;

	this.rulesbtng = new lib.Символ3копия9();
	this.rulesbtng.parent = this;
	this.rulesbtng.setTransform(-679.4,291);

	this.sovietbtn = new lib.Символ4();
	this.sovietbtn.parent = this;
	this.sovietbtn.setTransform(-685.4,246.5,1.234,0.837,0,0,0,0,31.1);
	this.sovietbtn.alpha = 0.5;

	this.sovietbtng = new lib.Символ3копия8();
	this.sovietbtng.parent = this;
	this.sovietbtng.setTransform(-679.4,221);

	this.ratingbtn = new lib.Символ4();
	this.ratingbtn.parent = this;
	this.ratingbtn.setTransform(420,316.3,1.234,0.837,0,0,0,0,31.1);
	this.ratingbtn.alpha = 0.5;

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.ratingbtn},{t:this.sovietbtng},{t:this.sovietbtn},{t:this.rulesbtng},{t:this.rulesbtn}]}).wait(1));

	// btns
	this.ratingbtng = new lib.Символ3копия6();
	this.ratingbtng.parent = this;
	this.ratingbtng.setTransform(420,291);

	this.timeline.addTween(cjs.Tween.get(this.ratingbtng).wait(1));

	// Слой 5
	this.scoretxt = new cjs.Text("000", "60px 'HappyFoxCondensed'", "#FFFFFF");
	this.scoretxt.name = "scoretxt";
	this.scoretxt.textAlign = "center";
	this.scoretxt.lineHeight = 72;
	this.scoretxt.lineWidth = 171;
	this.scoretxt.parent = this;
	this.scoretxt.setTransform(-564.4,-143.5);

	this.timertxt = new cjs.Text("02:00", "60px 'HappyFoxCondensed'", "#FFFFFF");
	this.timertxt.name = "timertxt";
	this.timertxt.textAlign = "center";
	this.timertxt.lineHeight = 72;
	this.timertxt.lineWidth = 171;
	this.timertxt.parent = this;
	this.timertxt.setTransform(-564.4,37);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.timertxt},{t:this.scoretxt}]}).wait(1));

	// Слой 7
	this.milkbox = new lib.Символ22();
	this.milkbox.parent = this;
	this.milkbox.setTransform(539.8,0.1);

	this.timeline.addTween(cjs.Tween.get(this.milkbox).wait(1));

	// Слой 2
	this.tim = new lib.tileMC();
	this.tim.parent = this;
	this.tim.setTransform(-810.9,-371.9);

	this.gamemc = new lib.Символ4_1();
	this.gamemc.parent = this;
	this.gamemc.setTransform(-36,25.1,1,1,0,0,0,360,360);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.gamemc},{t:this.tim}]}).wait(1));

	// Слой 1
	this.instance = new lib.bn();
	this.instance.parent = this;
	this.instance.setTransform(-675,-325);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	// Слой 6
	this.instance_1 = new lib.fon22();
	this.instance_1.parent = this;
	this.instance_1.setTransform(-714,-420);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(1));

}).prototype = getMCSymbolPrototype(lib.Символ1, new cjs.Rectangle(-865.9,-428.5,1586.4,1083.5), null);


// stage content:
(lib.game = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		var er = this;
		er.score = 0;
		var maxumumScore = 100;
		var levelTime = 120;
		var waitingForGame = true;
		
		createjs.Touch.enable(stage, false, true);
		stage.preventSelection = false;	
		//createjs.Touch.enable(stage);
		
		er.onGame=false;
		/////////////////////////////////////////////
		
		//стартуем
		er.enterMainMenu = function() {
			allWindowVisibleOff();
			er.startmc.visible = true;
			
			
			if (er.startmc.startGamebtng.txt.currentFrame==1)
			{
				er.startmc.startGamebtng.txt.gotoAndStop(0);
			}
		}
		
		er.enterMainMenu();
		
		function gamePause()
		{
			if (er.onGame)
			{	
				
				er.game.onGame=false;
				er.game.gamemc.mouseEnabled = false;		
			}
		}
		
		function gameUnPause()
		{
			if (er.onGame)
			{		
				er.game.onGame = true;
				er.game.gamemc.mouseEnabled = true;		
			}
		}
		
		
		///кнопки//////////////////
		stage.enableMouseOver();
		
		er.btnAnimArr = [
			[er.startmc.startGamebtn, er.startmc.startGamebtng, function () { er.playTheGame(); }],
			[er.winmc.mainbtn, er.winmc.mainbtng, function () { er.restartGame(); }],	
			[er.failmc.mainbtn, er.failmc.mainbtng, function () { er.restartGame(); }],
			[er.ratingmc.closebtn, er.ratingmc.closebtng, function () {closeMcBtn(er.ratingmc); if (waitingForGame) {er.startmc.visible = true;} else {er.game.gamemc.visible = true;}}],
			[er.sovietmc.closebtn, er.sovietmc.closebtng, function () {closeMcBtn(er.sovietmc); if (waitingForGame) {er.startmc.visible = true;} else {er.game.gamemc.visible = true;}}],	
			
			[er.game.ratingbtn, er.game.ratingbtng, function () { if (er.ratingmc.visible) {return;} gamePause(); loadRaiting(); er.ratingWindowShow();  }],	
			[er.game.sovietbtn, er.game.sovietbtng, function () { gamePause(); loadAdvice(); er.sovietShow(); }],	
			[er.game.rulesbtn, er.game.rulesbtng, function () {  if (er.startmc.visible) {return;} gamePause(); er.enterMainMenu(); 		if (er.startmc.startGamebtng.txt.currentFrame==0) { er.startmc.startGamebtng.txt.gotoAndStop(1);} }]
		]
		
			
		er.restartGame=function()
		{
			er.enterMainMenu();
			er.playTheGame();
		}
		//запуск игры
		er.playTheGame = function () {
		
			allWindowVisibleOff();
			er.game.gamemc.visible = true;
			
			if (!er.onGame)
			{
				
			waitingForGame = false;
				
			er.game.playTime = levelTime;
			er.game.showtime();		
			
			er.game.onGame=false;
			er.game.startTimer();	
			er.game.gamemc.startGame();
		
			er.score = 0;
			er.game.scoretxt.text = "000";
			er.game.milkbox.ptxt.text = "0";
			er.game.milkAnimation(0);
			
			er.onGame=true;
				
			} 
			else {		
				 gameUnPause();
			}
			
			
		}
		
		//game over
		er.startGameOver = function () {
			er.onGame=false;
			waitingForGame = true;
			
			er.game.gamemc.restartGamePrepair();
			
			if (er.score >= maxumumScore) {
				startWinWindow();
			} else {
				startFailWindow();
			}
		}
		
		function startWinWindow() {
			er.scoreRequest.open("POST", "https://ru.omoloke.com/game/progress", true);
			er.scoreRequest.send(er.score);
			allWindowVisibleOff();
			er.winmc.visible = true;
			er.winmc.scoretxt.text = er.score;
		}
		
		function startFailWindow() {
			allWindowVisibleOff();
			er.failmc.visible = true;
		}
		
		er.ratingWindowShow = function()
		{
			allWindowVisibleOff();
			if (er.ratingmc.visible)
			{
				return;
			}
			er.ratingmc.visible = true;
		}
		er.sovietShow = function()
		{
			if (er.sovietmc.visible)
			{
				return;
			}
			
			allWindowVisibleOff();
			er.sovietmc.visible = true;
		}
		
		
		
		function addBtnAnim() {
			for (var i = 0; i < er.btnAnimArr.length; i++) {
				er.btnAnimArr[i][0].on("mouseover", handleBTNMouseEvent);
				er.btnAnimArr[i][0].on("mouseout", handleBTNMouseEvent);
				er.btnAnimArr[i][0].on("mousedown", handleBTNMouseEvent);
				//er.btnAnimArr[i][0].on("pressup", handleBTNMouseEvent);
				er.btnAnimArr[i][0].cursor = "pointer";
				er.btnAnimArr[i][0].alpha = .01;
				//er.btnAnimArr[i][1].gotoAndStop(0);	
			}
		}
		
		
		er.game.milkbox.on("mouseover", milkboxMouseEvent);
		er.game.milkbox.on("mouseout", milkboxMouseEvent);
		
		var mby = er.game.milkbox.y;
		
		
		function milkboxMouseEvent(evt) {
			
			if (evt.type == "mouseover") {
						//er.game.milkbox.scaleX=er.game.milkbox.scaleY=1.05;
				
						createjs.Tween.get(er.game.milkbox).wait(0).to({y:mby-12, scaleX: 1.05, scaleY: 1.05}, 100, createjs.Ease.getPowIn(2.5));
			
						
					} else if (evt.type == "mouseout") {
						//er.game.milkbox.scaleX=er.game.milkbox.scaleY=1;
						createjs.Tween.get(er.game.milkbox).wait(0).to({y:mby, scaleX: 1, scaleY: 1}, 100, createjs.Ease.getPowIn(2.5));
					} 	
		}
		
		function handleBTNMouseEvent(evt) {
			//console.log(evt.type);
			
			evt.nativeEvent.preventDefault();
			
			for (var i = 0; i < er.btnAnimArr.length; i++) {
				if (er.btnAnimArr[i][0] == evt.currentTarget) {
					if (evt.type == "mouseover") {
						er.btnAnimArr[i][1].gotoAndStop(1);
					} else if (evt.type == "mouseout") {
						er.btnAnimArr[i][1].gotoAndStop(0);
					} else if ("mousedown") {
						er.btnAnimArr[i][1].gotoAndStop(2);
						//} else if ("pressup") {
						var func = er.btnAnimArr[i][2];
						func();
					}
					return;
				}
			}
		}
		
		
		
		
		////////////////////////
		//начисление очков///////////////////////////////
		er.setNewScore = function (scorex) {
			
			if(!er.onGame)
			{
				return;
			}
			
			switch (scorex) {
				case 1: //3 в ряд
					er.score += 3;
					break;
				case 2: //4 в ряд
					er.score += 5;
					break;
				case 3: //5 в ряд
					er.score += 5;
					break;
				case 4: //5 в ряд + BMC
					er.score = er.score + getbonusBMC(er.score);
					gamePause(); er.sovietShow();
					break;
			}
		
			var stxt = "";
			if (er.score < 100) {
				stxt = "0";
			}
			if (er.score < 10) {
				stxt += "0";
			}
			stxt = stxt + er.score.toString();
			er.game.scoretxt.text = stxt;
		
			var p = er.score * 100 / maxumumScore;
		
			er.game.milkAnimation(p);
		
		}
		
		 function getbonusBMC(score)
		{
			return Math.round((score*5)/100);
		}
		
		function closeMcBtn(mc)
		{
			mc.visible=false;
			gameUnPause();
		}
		
		function allWindowVisibleOff() {
			er.ratingmc.visible = false;
			er.failmc.visible = false;
			er.winmc.visible = false;
			er.sovietmc.visible = false;
			er.startmc.visible = false;
			er.game.gamemc.visible = false;
		}
		
		addBtnAnim();
		
		
		
		
		//server
		
		//1) загрузка советов
		
		var adv = new XMLHttpRequest();
		
		adv.onload = function (){	
		
			var obj = JSON.parse(adv.responseText);	
			var curAdv = getRandomInt(0, obj.items.length);	
			
			er.sovietmc.titletxt.text = obj.items[curAdv].title;
			
			if (er.sovietmc.titletxt.getBounds().height>80)
			{
				er.sovietmc.titletxt.y = -146;
			} else {
				er.sovietmc.titletxt.y = -106;
			}
			
			er.sovietmc.txt.text = obj.items[curAdv].text;	
		}
		
		function loadAdvice()
		{	
			adv.open("GET", "https://ru.omoloke.com/game/facts", true);
			adv.send(null);
			er.sovietmc.titletxt.text = "";
			er.sovietmc.txt.text = "";
		}
		
		//2) загрузка рейтинга
		var rait = new XMLHttpRequest();
		
		var pArr = [er.ratingmc.p1, er.ratingmc.p2, er.ratingmc.p3, er.ratingmc.p4, er.ratingmc.p5];
		
		rait.onload = function (){	
			
			var obj = JSON.parse(rait.responseText);	
			
			raitAllNameVisFalse ();
			
		        
			for (var i=0; i < obj.items.length; i++)
			{
				if (i>4)
				{
					return;
				}
				
				pArr[i].n.text= obj.items[i].title;
				pArr[i].s.text= obj.items[i].points;
				pArr[i].visible = true;
			}	
		}
		
		function loadRaiting()
		{	
			rait.open("GET", "https://ru.omoloke.com/game/rating", true);
			rait.send(null);
		}
		
		function raitAllNameVisFalse ()
		{
			for (var i=0; i < pArr.length; i++)
			{
				pArr[i].visible = false;
			}
		}
		raitAllNameVisFalse ();
		
		//3) отправка результатов на сервер
		er.scoreRequest = new XMLHttpRequest();
		er.scoreRequest.onload = function (){
		    //alert( x.responseText);
			//var obj = JSON.parse(x.responseText);
		}
		
		
		
		//GET / POST запрос
		//var x = new XMLHttpRequest();
		//факты
		/*
		x.open("GET", "https://ru.omoloke.com/game/facts", true);
		x.onload = function (){
		    alert( x.responseText);
			var obj = JSON.parse(x.responseText);
			alert(obj.items[0].title);
			alert(obj.items[0].text);
		}
		x.send(null);
		*/
		
		//запись результата
		/*
		x.open("POST", "https://ru.omoloke.com/game/progress", true);
		x.onload = function (){
		    alert( x.responseText);
			//var obj = JSON.parse(x.responseText);
			//alert(obj.items[0].title);
			//alert(obj.items[0].text);
		}
		x.send(115);
		*/
		
		//рейтинг
		/*
		
		*/
		
		
		/////////////////////////
		
		//не включая max!  
		function getRandomInt(min, max) {
		  return Math.floor(Math.random() * (max - min)) + min;
		}
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(1));

	// рэйтинг
	this.ratingmc = new lib.Символ3копия4();
	this.ratingmc.parent = this;
	this.ratingmc.setTransform(713.5,420);

	this.timeline.addTween(cjs.Tween.get(this.ratingmc).wait(1));

	// Советы
	this.sovietmc = new lib.Символ3копия();
	this.sovietmc.parent = this;
	this.sovietmc.setTransform(713.5,420);

	this.timeline.addTween(cjs.Tween.get(this.sovietmc).wait(1));

	// title
	this.startmc = new lib.startwindow();
	this.startmc.parent = this;
	this.startmc.setTransform(713.5,420);

	this.timeline.addTween(cjs.Tween.get(this.startmc).wait(1));

	// win
	this.winmc = new lib.Символ3копия2();
	this.winmc.parent = this;
	this.winmc.setTransform(713.5,420);

	this.timeline.addTween(cjs.Tween.get(this.winmc).wait(1));

	// fail
	this.failmc = new lib.Символ3копия3();
	this.failmc.parent = this;
	this.failmc.setTransform(713.5,420);

	this.timeline.addTween(cjs.Tween.get(this.failmc).wait(1));

	// game
	this.game = new lib.Символ1();
	this.game.parent = this;
	this.game.setTransform(713.5,420);

	this.timeline.addTween(cjs.Tween.get(this.game).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(561.1,413.1,1578.9,1082);
// library properties:
lib.properties = {
	width: 1427,
	height: 840,
	fps: 24,
	color: "#FFFFFF",
	opacity: 1.00,
	webfonts: {},
	manifest: [
		{src:"assets/images/game_atlas_.png", id:"game_atlas_"}
	],
	preloads: []
};




})(lib = lib||{}, images = images||{}, createjs = createjs||{}, ss = ss||{}, AdobeAn = AdobeAn||{});
var lib, images, createjs, ss, AdobeAn;