(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [];


// symbols:



(lib.bg = function() {
	this.initialize(img.bg);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,1080,680);


(lib.pack1 = function() {
	this.initialize(img.pack1);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,219,398);


(lib.pack2 = function() {
	this.initialize(img.pack2);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,258,388);


(lib.pack3 = function() {
	this.initialize(img.pack3);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,219,398);


(lib.rumyanec = function() {
	this.initialize(img.rumyanec);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,46,38);// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.Symbol18 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgfAfQgNgNAAgSQAAgRANgOQAOgNARAAQASAAANANQAOAOAAARQAAASgOANQgNAOgSAAQgRAAgOgOg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol18, new cjs.Rectangle(-4.5,-4.5,9,9), null);


(lib.Symbol13 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_3
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(10,1,1).p("AA2AQIhrgf");
	this.shape.setTransform(-9.3,8.375);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	// Layer_2
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(10,1,1).p("AgMBtQAzhtgzhs");
	this.shape_1.setTransform(-6.7448,-2.575);

	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(1));

	// Layer_1
	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(10,1,1).p("AgrBqQBrhNgYiG");
	this.shape_2.setTransform(-0.006,-0.325);

	this.timeline.addTween(cjs.Tween.get(this.shape_2).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol13, new cjs.Rectangle(-19.7,-18.5,29.1,33.8), null);


(lib.Symbol11 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AgvAwQgTgUAAgcQAAgbATgUQAUgUAbAAQAcAAAUAUQATAUAAAbQAAAcgTAUQgUATgcABQgbgBgUgTg");
	this.shape.setTransform(6.725,6.75);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol11, new cjs.Rectangle(0,0,13.5,13.5), null);


(lib.Symbol10 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AgvAwQgTgUAAgcQAAgbATgUQAUgUAbAAQAcAAAUAUQATAUAAAbQAAAcgTAUQgUATgcABQgbgBgUgTg");
	this.shape.setTransform(6.725,6.75);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol10, new cjs.Rectangle(0,0,13.5,13.5), null);


(lib.Symbol9 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#B7B7B7").s().p("AiMCMQg5g6gBhSQABhSA5g6QA6g5BSgBQBSABA6A5QA6A6AABSQAABSg6A6Qg6A6hSAAQhSAAg6g6g");
	this.shape.setTransform(19.85,19.85);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol9, new cjs.Rectangle(0,0,39.7,39.7), null);


(lib.Symbol7 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#B7B7B7").s().p("AiMCMQg5g6gBhSQABhSA5g6QA6g5BSgBQBSABA6A5QA6A6AABSQAABSg6A6Qg6A6hSAAQhSAAg6g6g");
	this.shape.setTransform(19.85,19.85);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol7, new cjs.Rectangle(0,0,39.7,39.7), null);


(lib.Symbol5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_3
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(10,1,1).p("AhAAVICBgp");
	this.shape.setTransform(12.05,9.35);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	// Layer_2
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(10,1,1).p("AARBtQg8hcAxh9");
	this.shape_1.setTransform(7.5397,-2.025);

	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(1));

	// Layer_1
	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(10,1,1).p("AAlBmQhVhFAOiG");
	this.shape_2.setTransform(-0.0225,0);

	this.timeline.addTween(cjs.Tween.get(this.shape_2).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol5, new cjs.Rectangle(-8.7,-17.9,32.2,34.3), null);


(lib.Symbol_22_Layer_3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_3
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(10,1,1).p("AlgmXQAPGVDQDCQCbCSFHBG");
	this.shape.setTransform(-12.9,-9.825);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(10,1,1).p("AlLmMQgeGdDGC6QCaCTFYAv");
	this.shape_1.setTransform(-14.9978,-10.925);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(10,1,1).p("AkymCQhGGlC7CyQCZCTFqAb");
	this.shape_2.setTransform(-17.4416,-11.925);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(10,1,1).p("AkZl5QhpGrCyCsQCYCTF5AJ");
	this.shape_3.setTransform(-19.9328,-12.775);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(10,1,1).p("AkClyQiGGyCqCmQCXCTGFgG");
	this.shape_4.setTransform(-22.2705,-13.5113);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(10,1,1).p("AjtlsQigG3CkCgQCWCUGQgU");
	this.shape_5.setTransform(-24.3567,-14.056);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(10,1,1).p("AjblpQi1G7CeCdQCVCUGagf");
	this.shape_6.setTransform(-26.132,-14.4323);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(10,1,1).p("AjNlmQjGG+CaCZQCVCVGhgn");
	this.shape_7.setTransform(-27.5485,-14.6944);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(10,1,1).p("AjDlkQjRHACXCXQCUCVGmgt");
	this.shape_8.setTransform(-28.5752,-14.8678);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(10,1,1).p("Ai9ljQjYHCCVCVQCUCVGpgx");
	this.shape_9.setTransform(-29.1912,-14.9687);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(10,1,1).p("Ai6ljQjbHDCUCVQCUCUGrgx");
	this.shape_10.setTransform(-29.4051,-15.0016);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(10,1,1).p("AjhlqQiuG6CfCeQCWCUGXgb");
	this.shape_11.setTransform(-25.5391,-14.3167);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(10,1,1).p("Akdl6QhjGrCzCsQCYCTF2AL");
	this.shape_12.setTransform(-19.5774,-12.675);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(10,1,1).p("AlDmIQgrGfDBC4QCaCSFeAo");
	this.shape_13.setTransform(-15.7992,-11.275);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(10,1,1).p("AlOmOQgXGcDHC8QCbCSFVAz");
	this.shape_14.setTransform(-14.6191,-10.75);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(10,1,1).p("AlWmSQgHGZDLC/QCbCSFPA7");
	this.shape_15.setTransform(-13.8101,-10.35);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(10,1,1).p("AlcmVQAFGXDPDBQCaCRFLBC");
	this.shape_16.setTransform(-13.3,-10.05);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(10,1,1).p("AlfmWQAMGVDQDCQCbCRFIBF");
	this.shape_17.setTransform(-13,-9.875);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},49).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_10}]},30).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_21_Layer_3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_3
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(10,1,1).p("ABrnVQCxBjAhC4QAeClhbCsQhYCribBYQinBfipg1");
	this.shape.setTransform(10.7876,-5.301);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(10,1,1).p("AlMG0QDBA0CphaQCghVBSigQBTiggeimQghi1ixhk");
	this.shape_1.setTransform(9.958,-6.8613);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(10,1,1).p("AlUGmQDXA0CrhXQCkhTBMiUQBNiXgfimQghiyiwhl");
	this.shape_2.setTransform(9.2429,-8.2523);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(10,1,1).p("AlbGZQDqA0CthUQCnhRBHiLQBIiNggimQghixiwhl");
	this.shape_3.setTransform(8.5923,-9.4809);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(10,1,1).p("AlhGOQD6A0CvhSQCqhPBDiDQBDiEgginQgiivivhl");
	this.shape_4.setTransform(8.0172,-10.5373);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(10,1,1).p("AlmGFQEIAzCwhPQCthNA/h8QA/h9ghioQgiiuivhl");
	this.shape_5.setTransform(7.578,-11.4338);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(10,1,1).p("AlqF9QETA0CxhNQCvhNA8h2QA8h3ghipQgiisivhm");
	this.shape_6.setTransform(7.1886,-12.168);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(10,1,1).p("AluF3QEcAzCyhLQCyhLA5hyQA5hzghipQgiirivhm");
	this.shape_7.setTransform(6.8916,-12.7469);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(10,1,1).p("AlwFzQEiAzCzhKQCyhKA4hvQA3hwghipQgiirivhm");
	this.shape_8.setTransform(6.6805,-13.1596);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(10,1,1).p("AlyFxQEmAzCzhKQC0hKA2htQA2hughipQgiiqivhm");
	this.shape_9.setTransform(6.5626,-13.4054);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(10,1,1).p("ACWmEQCuBmAiCrQAiCpg2BtQg2Bti0BJQizBKkngz");
	this.shape_10.setTransform(6.5142,-13.487);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(10,1,1).p("AlpGAQEPAzCxhOQCvhMA8h4QA9h6ggioQgiitivhl");
	this.shape_11.setTransform(7.3148,-11.926);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(10,1,1).p("AlaGbQDnA0CthVQCnhRBHiMQBJiOgfimQgiiyiwhk");
	this.shape_12.setTransform(8.6643,-9.3095);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(10,1,1).p("AlPGvQDJA0CqhZQChhUBQicQBRicgeimQghi1ixhk");
	this.shape_13.setTransform(9.6922,-7.3545);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(10,1,1).p("AlLG2QC+A1CphcQCfhVBSihQBViigeimQghi1ixhk");
	this.shape_14.setTransform(10.0913,-6.618);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(10,1,1).p("AlHG8QC1A1CohdQCchXBWilQBXingeilQghi3ixhj");
	this.shape_15.setTransform(10.3985,-6.038);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(10,1,1).p("AlFHAQCuA1CoheQCbhXBXipQBZipgdimQghi3ixhj");
	this.shape_16.setTransform(10.6172,-5.6228);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(10,1,1).p("AlEHDQCrA1CnhfQCbhYBYiqQBaisgdilQghi3ixhk");
	this.shape_17.setTransform(10.7391,-5.376);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},23).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_10}]},49).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape}]},1).wait(8));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_17_Layer_3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_3
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(10,1,1).p("AlsGGQGpgXC4jSQC2jNhelV");
	this.shape.setTransform(-3.0101,11.95);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(10,1,1).p("AlgGIQGpgYC4jRQC2jNiklZ");
	this.shape_1.setTransform(-4.2202,11.775);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(10,1,1).p("AlZGJQGpgXC4jRQC2jOjelb");
	this.shape_2.setTransform(-4.9005,11.625);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(10,1,1).p("AlVGKQGpgXC4jRQC2jOkKld");
	this.shape_3.setTransform(-5.3072,11.525);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(10,1,1).p("AlSGLQGogXC5jSQC2jNkrlf");
	this.shape_4.setTransform(-5.5532,11.45);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(10,1,1).p("AlRGMQGogYC5jRQC2jOk+lg");
	this.shape_5.setTransform(-5.6843,11.4);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(10,1,1).p("AlRGMQGpgYC4jRQC2jNlElh");
	this.shape_6.setTransform(-5.7269,11.375);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(10,1,1).p("Al4GEQGogYC5jRQC2jOgvlQ");
	this.shape_7.setTransform(-1.7677,12.2);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(10,1,1).p("AmHGBQGogXC5jRQC2jOgIlL");
	this.shape_8.setTransform(-0.2832,12.425);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(10,1,1).p("AmWGAQGogYC5jRQC2jOAWlI");
	this.shape_9.setTransform(1.225,12.6);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(10,1,1).p("AmhF/QGogYC5jRQC2jOAslG");
	this.shape_10.setTransform(2.3,12.7);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(10,1,1).p("AmoF+QGpgYC5jRQC2jNA5lF");
	this.shape_11.setTransform(2.95,12.775);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(10,1,1).p("AmqF+QGqgYC3jRQC2jOA+lE");
	this.shape_12.setTransform(3.175,12.8);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(10,1,1).p("AmoF+QGogYC5jRQC2jNA6lF");
	this.shape_13.setTransform(3.025,12.775);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(10,1,1).p("AmkF+QGpgXC5jRQC2jOAxlF");
	this.shape_14.setTransform(2.55,12.725);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(10,1,1).p("AmcF/QGpgXC5jSQC2jNAhlH");
	this.shape_15.setTransform(1.75,12.65);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(10,1,1).p("AmQGAQGogXC5jRQC2jOAKlJ");
	this.shape_16.setTransform(0.625,12.525);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(10,1,1).p("AmDGCQGpgYC4jRQC2jNgSlN");
	this.shape_17.setTransform(-0.719,12.375);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#000000").ss(10,1,1).p("Al2GEQGogYC5jRQC2jNg1lR");
	this.shape_18.setTransform(-1.9523,12.175);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},49).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape}]},1).wait(1));
	this.timeline.addTween(cjs.Tween.get(this.shape).wait(49).to({_off:true},1).wait(11).to({_off:false},0).to({_off:true},1).wait(12).to({_off:false},0).to({_off:true},1).wait(11).to({_off:false},0).to({_off:true},1).wait(12).to({_off:false},0).wait(1));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_16_Layer_3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_3
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(10,1,1).p("AHfDGQpGB0jmiJQjLh5BSky");
	this.shape.setTransform(3.0433,11.679);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(10,1,1).p("Am3kPQhKE4DECIQDaCVIwhW");
	this.shape_1.setTransform(4.8459,7.5136);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(10,1,1).p("AmqkiQhCE9C9CVQDPCfIdg8");
	this.shape_2.setTransform(6.4459,3.995);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(10,1,1).p("Amfk2Qg8FCC4ChQDFCoIMgl");
	this.shape_3.setTransform(7.8623,1.0581);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(10,1,1).p("AmUlJQg3FICzCqQC9CwH9gR");
	this.shape_4.setTransform(9.089,-1.3253);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(10,1,1).p("AmMlaQgxFLCuCyQC2C4HxAA");
	this.shape_5.setTransform(10.1129,-3.175);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(10,1,1).p("AmFlpQgtFOCrC5QCwC+HnAO");
	this.shape_6.setTransform(10.948,-4.65);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(10,1,1).p("Al/l0QgrFQCpC/QCsDBHeAZ");
	this.shape_7.setTransform(11.598,-5.775);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(10,1,1).p("Al8l8QgoFSCnDCQCoDEHZAh");
	this.shape_8.setTransform(12.0635,-6.6);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(10,1,1).p("Al5mBQgnFSClDFQCnDGHVAm");
	this.shape_9.setTransform(12.3531,-7.075);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(10,1,1).p("AGBGEQnVgniljHQimjGAnlT");
	this.shape_10.setTransform(12.4352,-7.25);
	this.shape_10._off = true;

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(10,1,1).p("AmHlkQgvFNCsC3QCyC7HqAK");
	this.shape_11.setTransform(10.6718,-4.15);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(10,1,1).p("AmgkzQg9FBC4CfQDHCoIOgo");
	this.shape_12.setTransform(7.6765,1.4355);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(10,1,1).p("AmzkWQhHE6DBCNQDXCZIphO");
	this.shape_13.setTransform(5.4203,6.2529);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(10,1,1).p("Am6kLQhLE3DFCFQDcCTIzhb");
	this.shape_14.setTransform(4.5641,8.1647);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(10,1,1).p("Am/kEQhOE1DHCAQDgCPI8hm");
	this.shape_15.setTransform(3.9018,9.6733);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(10,1,1).p("AnDj+QhQEzDJB8QDkCMJBhu");
	this.shape_16.setTransform(3.424,10.795);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(10,1,1).p("AnFj7QhSEyDLB6QDlCKJFhy");
	this.shape_17.setTransform(3.1306,11.4461);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},23).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_10}]},2).to({state:[{t:this.shape_10}]},3).to({state:[{t:this.shape_10}]},4).to({state:[{t:this.shape_10}]},40).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape}]},1).wait(8));
	this.timeline.addTween(cjs.Tween.get(this.shape_10).wait(33).to({_off:false},0).wait(49).to({_off:true},1).wait(17));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_15_Layer_3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_3
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(10,1,1).p("AhjgJQCEhHBDB1");
	this.shape.setTransform(2.15,-9.8311);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(10,1,1).p("AhigEQCEhIBBBt");
	this.shape_1.setTransform(2.075,-10.3242);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(10,1,1).p("AhiAAQCEhIBABm");
	this.shape_2.setTransform(2,-10.7311);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(10,1,1).p("AhhADQCEhHA/Bg");
	this.shape_3.setTransform(1.925,-11.1133);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(10,1,1).p("AhgAHQCEhHA9BZ");
	this.shape_4.setTransform(1.875,-11.4701);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(10,1,1).p("AhgAJQCEhHA9BV");
	this.shape_5.setTransform(1.825,-11.7405);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(10,1,1).p("AhfAMQCEhHA7BR");
	this.shape_6.setTransform(1.8,-11.9831);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(10,1,1).p("AhfAOQCEhHA7BO");
	this.shape_7.setTransform(1.775,-12.1666);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(10,1,1).p("AhfAPQCEhHA7BM");
	this.shape_8.setTransform(1.75,-12.3205);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(10,1,1).p("AhfAQQCEhHA7BK");
	this.shape_9.setTransform(1.725,-12.3823);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(10,1,1).p("AhfALQCEhHA7BS");
	this.shape_10.setTransform(1.8,-11.8919);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(10,1,1).p("AhigCQCEhIBBBq");
	this.shape_11.setTransform(2.05,-10.469);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(10,1,1).p("AhigFQCEhIBBBv");
	this.shape_12.setTransform(2.075,-10.2375);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(10,1,1).p("AhjgGQCFhIBCBx");
	this.shape_13.setTransform(2.1,-10.0648);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(10,1,1).p("AhjgIQCEhIBDB0");
	this.shape_14.setTransform(2.125,-9.9214);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(10,1,1).p("AhjgIQCEhIBDB1");
	this.shape_15.setTransform(2.15,-9.8642);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},49).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3,p:{x:1.925,y:-11.1133}}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9,p:{y:-12.3823}}]},1).to({state:[{t:this.shape_9,p:{y:-12.4084}}]},1).to({state:[{t:this.shape_9,p:{y:-12.4084}}]},30).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_3,p:{x:1.95,y:-11.0837}}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_15_Layer_2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(10,1,1).p("Ah9glQBXBACkAL");
	this.shape.setTransform(4.8,-7.05);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(10,1,1).p("Ah3giQBiBJCNgE");
	this.shape_1.setTransform(4.125,-7.2897);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(10,1,1).p("AhxghQBqBRB5gR");
	this.shape_2.setTransform(3.525,-7.3798);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(10,1,1).p("AhsghQBxBZBngd");
	this.shape_3.setTransform(3,-7.3731);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(10,1,1).p("AhngiQB3BgBYgm");
	this.shape_4.setTransform(2.525,-7.328);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(10,1,1).p("AhjgiQB9BlBKgv");
	this.shape_5.setTransform(2.15,-7.2862);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(10,1,1).p("AhggjQCBBqBAg2");
	this.shape_6.setTransform(1.825,-7.2444);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(10,1,1).p("AhdgjQCEBtA3g7");
	this.shape_7.setTransform(1.575,-7.1982);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(10,1,1).p("AhcgjQCIBwAxg/");
	this.shape_8.setTransform(1.4,-7.1724);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(10,1,1).p("AhagkQCIByAthB");
	this.shape_9.setTransform(1.275,-7.1481);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(10,1,1).p("AhagjQCJBxAshB");
	this.shape_10.setTransform(1.25,-7.1521);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(10,1,1).p("AhhgiQCABoBDgz");
	this.shape_11.setTransform(1.925,-7.2553);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(10,1,1).p("AhsghQBwBYBpgb");
	this.shape_12.setTransform(3.05,-7.371);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(10,1,1).p("Ah1giQBlBMCGgI");
	this.shape_13.setTransform(3.925,-7.3327);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(10,1,1).p("Ah4gjQBgBICRgC");
	this.shape_14.setTransform(4.225,-7.2485);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(10,1,1).p("Ah6gjQBcBECZAD");
	this.shape_15.setTransform(4.475,-7.175);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(10,1,1).p("Ah8gkQBaBCCfAH");
	this.shape_16.setTransform(4.65,-7.1);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(10,1,1).p("Ah9gkQBYBACjAJ");
	this.shape_17.setTransform(4.775,-7.075);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},49).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_10}]},30).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_15_Layer_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(10,1,1).p("AhdhXQAgCDCbAs");
	this.shape.setTransform(0,0.025);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(10,1,1).p("AhZhZQAiB+CRA1");
	this.shape_1.setTransform(-0.375,0.275);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(10,1,1).p("AhVhcQAjB7CJA+");
	this.shape_2.setTransform(-0.7,0.525);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(10,1,1).p("AhTheQAlB4CCBF");
	this.shape_3.setTransform(-1,0.725);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(10,1,1).p("AhQhgQAmB1B7BM");
	this.shape_4.setTransform(-1.275,0.9);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(10,1,1).p("AhOhhQAoByB1BR");
	this.shape_5.setTransform(-1.475,1.05);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(10,1,1).p("AhMhiQAoBwBxBV");
	this.shape_6.setTransform(-1.65,1.175);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(10,1,1).p("AhLhjQApBuBuBZ");
	this.shape_7.setTransform(-1.8,1.275);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(10,1,1).p("AhKhkQAqBuBrBb");
	this.shape_8.setTransform(-1.9,1.35);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(10,1,1).p("AhJhkQAqBsBpBd");
	this.shape_9.setTransform(-1.95,1.375);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(10,1,1).p("AhJhlQAqBtBpBe");
	this.shape_10.setTransform(-1.975,1.4);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(10,1,1).p("AhNhiQAoBwBzBV");
	this.shape_11.setTransform(-1.6,1.15);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(10,1,1).p("AhYhaQAiB9CPA4");
	this.shape_12.setTransform(-0.5,0.375);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(10,1,1).p("AhZhZQAhB/CSA0");
	this.shape_13.setTransform(-0.325,0.25);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(10,1,1).p("AhbhYQAhCBCWAw");
	this.shape_14.setTransform(-0.175,0.15);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(10,1,1).p("AhchXQAgCBCZAu");
	this.shape_15.setTransform(-0.075,0.075);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(10,1,1).p("AhchXQAfCDCaAs");
	this.shape_16.setTransform(-0.025,0.05);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},49).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3,p:{x:-1,y:0.725}}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_10}]},30).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_3,p:{x:-0.975,y:0.7}}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_14_Layer_3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_3
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(10,1,1).p("AAtBEQAwhYiVgw");
	this.shape.setTransform(4.9801,-12.3);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(10,1,1).p("Ag4g9QCRAdgpBe");
	this.shape_1.setTransform(4.7343,-11.625);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(10,1,1).p("Ag4g3QCOAMgkBj");
	this.shape_2.setTransform(4.512,-11.025);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(10,1,1).p("Ag4gxQCLgDgfBn");
	this.shape_3.setTransform(4.331,-10.5029);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(10,1,1).p("Ag5gsQCJgPgbBq");
	this.shape_4.setTransform(4.1641,-10.1231);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(10,1,1).p("Ag5gmQCHgbgXBt");
	this.shape_5.setTransform(4.0419,-9.8822);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(10,1,1).p("Ag5giQCFgjgUBw");
	this.shape_6.setTransform(3.9299,-9.7403);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(10,1,1).p("Ag5geQCDgpgSBx");
	this.shape_7.setTransform(3.8774,-9.6288);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(10,1,1).p("Ag6gbQCDgugQBy");
	this.shape_8.setTransform(3.8118,-9.5674);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(10,1,1).p("Ag6gZQCCgxgPBz");
	this.shape_9.setTransform(3.7771,-9.5153);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(10,1,1).p("AA5ApQAPh0iCAz");
	this.shape_10.setTransform(3.7734,-9.5107);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(10,1,1).p("Ag5gjQCFgggVBu");
	this.shape_11.setTransform(3.9668,-9.7818);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(10,1,1).p("Ag4gyQCLgBgfBm");
	this.shape_12.setTransform(4.3384,-10.5505);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(10,1,1).p("Ag4g7QCQAXgnBg");
	this.shape_13.setTransform(4.6478,-11.4);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(10,1,1).p("Ag4g+QCSAggrBd");
	this.shape_14.setTransform(4.7778,-11.725);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(10,1,1).p("Ag4hAQCUAmgtBb");
	this.shape_15.setTransform(4.8483,-11.975);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(10,1,1).p("Ag4hCQCUArguBa");
	this.shape_16.setTransform(4.928,-12.15);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(10,1,1).p("Ag4hDQCVAugwBZ");
	this.shape_17.setTransform(4.9727,-12.275);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},23).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_10}]},49).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape}]},1).wait(8));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_14_Layer_2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(10,1,1).p("ABQgBQg6hkhlCQ");
	this.shape.setTransform(-5.525,-8.5649);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(10,1,1).p("AhFAqQBAiKBLBb");
	this.shape_1.setTransform(-4.55,-8.1094);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(10,1,1).p("Ag9AqQAfiFBcBS");
	this.shape_2.setTransform(-3.7,-7.7065);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(10,1,1).p("Ag1ApQABiBBqBL");
	this.shape_3.setTransform(-2.925,-7.3436);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(10,1,1).p("AgsApQgah+B3BF");
	this.shape_4.setTransform(-2.4985,-7.0251);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(10,1,1).p("AgjApQgvh7CCA/");
	this.shape_5.setTransform(-2.3473,-6.7891);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(10,1,1).p("AgaApQhBh4CKA6");
	this.shape_6.setTransform(-2.2964,-6.5783);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(10,1,1).p("AgTApQhPh2CRA3");
	this.shape_7.setTransform(-2.2869,-6.4193);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(10,1,1).p("AgNApQhZh1CWA1");
	this.shape_8.setTransform(-2.3002,-6.3107);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(10,1,1).p("AgKApQhfh0CZAz");
	this.shape_9.setTransform(-2.3122,-6.2241);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(10,1,1).p("AAwgYQiagzBhB0");
	this.shape_10.setTransform(-2.3174,-6.2181);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(10,1,1).p("AgdApQg7h5CIA8");
	this.shape_11.setTransform(-2.3153,-6.639);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(10,1,1).p("Ag2ApQAFiBBoBL");
	this.shape_12.setTransform(-3.05,-7.3836);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(10,1,1).p("AhCAqQA0iIBRBX");
	this.shape_13.setTransform(-4.25,-7.9514);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(10,1,1).p("AhHAqQBHiLBIBc");
	this.shape_14.setTransform(-4.725,-8.1741);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(10,1,1).p("AhKAqQBTiNBCBg");
	this.shape_15.setTransform(-5.075,-8.3451);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(10,1,1).p("AhNArQBdiPA+Bi");
	this.shape_16.setTransform(-5.325,-8.4634);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(10,1,1).p("AhOArQBjiPA6Bj");
	this.shape_17.setTransform(-5.475,-8.5579);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},23).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_10}]},49).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape}]},1).wait(8));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_14_Layer_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(10,1,1).p("ABDhAQhUgbgxCg");
	this.shape.setTransform(0,-0.0001);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(10,1,1).p("Ag3BAQAViTBbAY");
	this.shape_1.setTransform(1.05,-0.4674);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(10,1,1).p("AguA7QgCiHBfAV");
	this.shape_2.setTransform(1.973,-0.9159);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(10,1,1).p("AgkA2QgXh7BkAS");
	this.shape_3.setTransform(2.5791,-1.2887);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(10,1,1).p("AgZAzQgqhzBpAQ");
	this.shape_4.setTransform(2.927,-1.5917);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(10,1,1).p("AgPAwQg5hrBsAO");
	this.shape_5.setTransform(3.1321,-1.8753);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(10,1,1).p("AgGAtQhHhkBvAM");
	this.shape_6.setTransform(3.2633,-2.1137);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(10,1,1).p("AAAArQhQhfByAL");
	this.shape_7.setTransform(3.3384,-2.2768);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(10,1,1).p("AAFAqQhXhbBzAJ");
	this.shape_8.setTransform(3.3939,-2.4195);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(10,1,1).p("AAIApQhbhZB0AJ");
	this.shape_9.setTransform(3.4251,-2.4909);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(10,1,1).p("AAhgnQh1gJBdBZ");
	this.shape_10.setTransform(3.4274,-2.5182);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(10,1,1).p("AgJAuQhChmBuAM");
	this.shape_11.setTransform(3.2195,-2.0426);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(10,1,1).p("AglA3QgVh9BkAS");
	this.shape_12.setTransform(2.5263,-1.218);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(10,1,1).p("Ag0A+QAOiPBbAX");
	this.shape_13.setTransform(1.375,-0.6339);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(10,1,1).p("Ag5BAQAaiVBZAZ");
	this.shape_14.setTransform(0.875,-0.397);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(10,1,1).p("Ag9BCQAkiaBXAa");
	this.shape_15.setTransform(0.5,-0.2367);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(10,1,1).p("AhABEQAsieBVAb");
	this.shape_16.setTransform(0.225,-0.096);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(10,1,1).p("AhCBFQAwigBVAb");
	this.shape_17.setTransform(0.05,-0.0256);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},23).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_10}]},49).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape}]},1).wait(8));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_3_Layer_5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_5
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(13,1,1).p("AiPAFQAdAlAtARQAtAQAxgPQAygQAdgrQAdgqALgZ");
	this.shape.setTransform(6.125,25.5759);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(13,1,1).p("AiKAGQAaAnAuARQAtARAxgPQAygQAcgsQAcgsAFge");
	this.shape_1.setTransform(5.925,25.3196);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(13,1,1).p("AiHAHQAYAoAvASQAtASAygQQAygPAagtQAcguABgj");
	this.shape_2.setTransform(5.75,25.1015);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(13,1,1).p("AiDAIQAWApAvATQAuATAxgQQAygQAZgtQAbgvgDgo");
	this.shape_3.setTransform(5.5903,24.8907);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(13,1,1).p("AiAAJQAUAqAwAUQAtATAxgPQAygQAZguQAZgxgHgt");
	this.shape_4.setTransform(5.5004,24.6732);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(13,1,1).p("Ah+AJQATAsAwAUQAuAUAxgQQAygQAXguQAZgygKgx");
	this.shape_5.setTransform(5.4303,24.5056);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(13,1,1).p("Ah8ALQARAsAxAVQAuAUAwgPQAygRAXgvQAZgzgOg0");
	this.shape_6.setTransform(5.4045,24.3384);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(13,1,1).p("Ah6ALQAPAtAyAWQAuAUAwgPQAygRAWgwQAYgzgQg4");
	this.shape_7.setTransform(5.3861,24.2087);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(13,1,1).p("Ah5AMQAOAtAyAXQAvAVAvgQQAzgRAVgwQAYg1gUg6");
	this.shape_8.setTransform(5.3672,24.0709);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(13,1,1).p("Ah4AMQANAuAyAXQAvAWAwgQQAygRAVgxQAXg1gVg9");
	this.shape_9.setTransform(5.3511,23.9663);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(13,1,1).p("Ah3AMQAMAvAzAXQAuAWAwgQQAzgRAUgwQAWg2gXg/");
	this.shape_10.setTransform(5.3563,23.8618);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(13,1,1).p("Ah2ANQAMAvAyAYQAvAWAwgQQAygRAUgxQAWg3gYhA");
	this.shape_11.setTransform(5.3386,23.7868);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(13,1,1).p("Ah2ANQALAwAzAXQAvAXAwgQQAygRATgxQAXg3gahC");
	this.shape_12.setTransform(5.3719,23.7325);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(13,1,1).p("Ah1ANQALAwAzAYQAvAWAvgQQAzgRATgxQAWg4gbhC");
	this.shape_13.setTransform(5.349,23.7075);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(13,1,1).p("Ah1ANQAKAwAzAYQAvAXAwgQQAygRATgyQAWg3gbhE");
	this.shape_14.setTransform(5.3639,23.6575);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(13,1,1).p("Ah4AMQANAvAyAXQAvAWAwgQQAygRAVgxQAXg2gXg+");
	this.shape_15.setTransform(5.3563,23.9118);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(13,1,1).p("Ah6ALQAPAuAyAWQAuAVAwgQQAzgRAVgvQAYg1gSg5");
	this.shape_16.setTransform(5.3654,24.1291);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(13,1,1).p("Ah+AJQATArAwAVQAuAUAwgQQAzgRAXguQAagygKgv");
	this.shape_17.setTransform(5.4408,24.5556);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#000000").ss(13,1,1).p("AiBAJQAVAqAvAUQAuATAxgQQAygQAZguQAagwgHgs");
	this.shape_18.setTransform(5.515,24.7232);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#000000").ss(13,1,1).p("AiFAHQAXApAvASQAuATAwgQQAzgQAagtQAbgugBgl");
	this.shape_19.setTransform(5.6755,25.021);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#000000").ss(13,1,1).p("AiIAGQAZAoAuASQAtASAygPQAygQAagtQAcgtADgi");
	this.shape_20.setTransform(5.8,25.1586);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#000000").ss(13,1,1).p("AiJAGQAZAnAuASQAuARAxgPQAygQAbgsQAcgtAEgf");
	this.shape_21.setTransform(5.875,25.264);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#000000").ss(13,1,1).p("AiLAGQAaAmAuASQAuARAxgQQAygPAcgsQAcgsAGge");
	this.shape_22.setTransform(5.95,25.3696);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#000000").ss(13,1,1).p("AiMAGQAbAmAtARQAtARAygQQAygPAcgsQAdgrAHgc");
	this.shape_23.setTransform(6.025,25.4446);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#000000").ss(13,1,1).p("AiOAFQAcAmAtARQAuAQAxgPQAygQAdgrQAcgrAKgb");
	this.shape_24.setTransform(6.05,25.5004);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f().s("#000000").ss(13,1,1).p("AiOAFQAcAmAtAQQAtARAygQQAygPAdgrQAdgrAJga");
	this.shape_25.setTransform(6.1,25.5254);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape,p:{y:25.5759}}]}).to({state:[{t:this.shape,p:{y:25.5759}}]},19).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_14}]},39).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape,p:{y:25.5754}}]},1).to({state:[{t:this.shape,p:{y:25.5759}}]},1).wait(12));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_3_Layer_4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_4
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(13,1,1).p("AikA0QANhEAxgdQArgZA1AHQAzAHAtAiQAsAgAfA4");
	this.shape.setTransform(-41.425,-18.1504);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(13,1,1).p("AikA1QANhGAxgeQArgaA1AHQAzAIAtAjQAsAhAfA6");
	this.shape_1.setTransform(-41.425,-18.3492);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(13,1,1).p("AikA2QANhIAxgeQArgbA1AIQAzAHAtAkQAsAjAfA7");
	this.shape_2.setTransform(-41.425,-18.5315);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(13,1,1).p("AikA4QANhKAxgfQArgbA1AHQAzAIAtAlQAsAjAfA9");
	this.shape_3.setTransform(-41.425,-18.7054);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(13,1,1).p("AikA5QANhLAxggQArgcA1AIQAzAHAtAmQAsAkAfA+");
	this.shape_4.setTransform(-41.425,-18.8627);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(13,1,1).p("AikA6QANhNAxggQArgdA1AIQAzAIAtAnQAsAlAfA/");
	this.shape_5.setTransform(-41.425,-19.0115);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(13,1,1).p("AikA7QANhOAxghQArgeA1AJQAzAIAtAnQAsAlAfBB");
	this.shape_6.setTransform(-41.425,-19.1451);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(13,1,1).p("AikA8QANhPAxgiQArgeA1AJQAzAIAtAoQAsAmAfBB");
	this.shape_7.setTransform(-41.425,-19.2688);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(13,1,1).p("AikA9QANhRAxghQArgfA1AJQAzAIAtAoQAsAnAfBC");
	this.shape_8.setTransform(-41.425,-19.3676);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(13,1,1).p("AikA+QANhSAxgiQArgfA1AJQAzAIAtApQAsAnAfBD");
	this.shape_9.setTransform(-41.425,-19.4525);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(13,1,1).p("AikA+QANhSAxgjQArgfA1AJQAzAIAtAqQAsAnAfBE");
	this.shape_10.setTransform(-41.425,-19.5262);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(13,1,1).p("AikA+QANhSAxgjQArgfA1AIQAzAJAtApQAsAoAfBE");
	this.shape_11.setTransform(-41.425,-19.5915);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(13,1,1).p("AikA/QANhTAxgkQArgfA1AJQAzAIAtAqQAsAoAfBF");
	this.shape_12.setTransform(-41.425,-19.6415);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(13,1,1).p("AikA/QANhUAxgjQArggA1AJQAzAJAtAqQAsAoAfBF");
	this.shape_13.setTransform(-41.425,-19.7);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(13,1,1).p("AikA+QANhSAxgiQArgfA1AJQAzAIAtApQAsAnAfBE");
	this.shape_14.setTransform(-41.425,-19.5012);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(13,1,1).p("AikA9QANhQAxgiQArgeA1AIQAzAIAtAoQAsAnAfBC");
	this.shape_15.setTransform(-41.425,-19.3188);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(13,1,1).p("AikA6QANhNAxggQArgdA1AIQAzAIAtAmQAsAlAfA/");
	this.shape_16.setTransform(-41.425,-18.9877);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(13,1,1).p("AikA3QANhJAxgeQArgbA1AHQAzAIAtAkQAsAjAfA7");
	this.shape_17.setTransform(-41.425,-18.5815);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#000000").ss(13,1,1).p("AikA2QANhHAxgfQArgaA1AHQAzAHAtAkQAsAiAfA7");
	this.shape_18.setTransform(-41.425,-18.4827);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#000000").ss(13,1,1).p("AikA1QANhGAxgeQArgaA1AHQAzAHAtAjQAsAiAfA6");
	this.shape_19.setTransform(-41.425,-18.3981);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#000000").ss(13,1,1).p("AikA1QANhGAxgdQArgaA1AHQAzAHAtAjQAsAhAfA6");
	this.shape_20.setTransform(-41.425,-18.3242);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#000000").ss(13,1,1).p("AikA1QANhGAxgdQArgaA1AIQAzAHAtAiQAsAhAfA5");
	this.shape_21.setTransform(-41.425,-18.259);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#000000").ss(13,1,1).p("AikA0QANhFAxgcQArgaA1AHQAzAHAtAiQAsAhAfA5");
	this.shape_22.setTransform(-41.425,-18.209);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#000000").ss(13,1,1).p("AikA0QANhFAxgcQArgaA1AHQAzAHAtAiQAsAhAfA4");
	this.shape_23.setTransform(-41.425,-18.1754);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},19).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4,p:{y:-18.8627}}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12,p:{y:-19.6415}}]},1).to({state:[{t:this.shape_12,p:{y:-19.675}}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_13}]},39).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_4,p:{y:-18.8389}}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).wait(12));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_3_Layer_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(13,1,1).p("Ai0BBQAIg+AxgjQAtghBAAAQA/gBA1AhQA5AkAWBA");
	this.shape.setTransform(27.875,-18.5504);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(13,1,1).p("Ai0BDQAIhAAxgkQAtgiBAAAQA/gBA1AiQA5AlAWBC");
	this.shape_1.setTransform(27.875,-18.7504);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(13,1,1).p("Ai0BEQAIhBAxglQAtgjBAAAQA/gBA1AjQA5AmAWBE");
	this.shape_2.setTransform(27.875,-18.9504);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(13,1,1).p("Ai0BGQAIhDAxgmQAtgkBAAAQA/AAA1AjQA5AnAWBF");
	this.shape_3.setTransform(27.875,-19.1503);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(13,1,1).p("Ai0BIQAIhFAxgnQAtglBAAAQA/AAA1AkQA5AoAWBH");
	this.shape_4.setTransform(27.875,-19.3253);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(13,1,1).p("Ai0BJQAIhGAxgoQAtglBAAAQA/gBA1AmQA5AoAWBI");
	this.shape_5.setTransform(27.875,-19.4753);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(13,1,1).p("Ai0BKQAIhHAxgoQAtgmBAAAQA/gBA1AmQA5ApAWBK");
	this.shape_6.setTransform(27.875,-19.6003);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(13,1,1).p("Ai0BLQAIhIAxgpQAtgnBAAAQA/AAA1AmQA5AqAWBL");
	this.shape_7.setTransform(27.875,-19.7253);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(13,1,1).p("Ai0BNQAIhKAxgpQAtgnBAAAQA/gBA1AnQA5AqAWBM");
	this.shape_8.setTransform(27.875,-19.8503);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(13,1,1).p("Ai0BNQAIhKAxgqQAtgnBAAAQA/gBA1AoQA5AqAWBN");
	this.shape_9.setTransform(27.875,-19.9503);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(13,1,1).p("Ai0BOQAIhLAxgrQAtgnBAAAQA/gBA1AoQA5ArAWBN");
	this.shape_10.setTransform(27.875,-20.0253);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(13,1,1).p("Ai0BOQAIhLAxgrQAtgoBAAAQA/AAA1AnQA5AsAWBO");
	this.shape_11.setTransform(27.875,-20.0753);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(13,1,1).p("Ai0BPQAIhMAxgrQAtgoBAAAQA/gBA1AoQA5AsAWBO");
	this.shape_12.setTransform(27.875,-20.1253);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(13,1,1).p("Ai0BPQAIhMAxgrQAtgpBAAAQA/AAA1AoQA5AsAWBP");
	this.shape_13.setTransform(27.875,-20.1753);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(13,1,1).p("Ai0BQQAIhNAxgrQAtgpBAAAQA/AAA1AoQA5AsAWBP");
	this.shape_14.setTransform(27.875,-20.2003);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(13,1,1).p("Ai0BOQAIhLAxgqQAtgoBAAAQA/AAA1AnQA5ArAWBN");
	this.shape_15.setTransform(27.875,-20.0003);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(13,1,1).p("Ai0BMQAIhJAxgpQAtgnBAAAQA/AAA1AmQA5AqAWBL");
	this.shape_16.setTransform(27.875,-19.8003);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(13,1,1).p("Ai0BIQAIhFAxgoQAtglBAAAQA/AAA1AlQA5AoAWBI");
	this.shape_17.setTransform(27.875,-19.4253);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#000000").ss(13,1,1).p("Ai0BHQAIhEAxgnQAtgkBAAAQA/gBA1AkQA5AoAWBG");
	this.shape_18.setTransform(27.875,-19.2753);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#000000").ss(13,1,1).p("Ai0BFQAIhCAxgmQAtgjBAAAQA/AAA1AjQA5AmAWBE");
	this.shape_19.setTransform(27.875,-19.0254);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#000000").ss(13,1,1).p("Ai0BEQAIhBAxglQAtgjBAAAQA/AAA1AiQA5AmAWBD");
	this.shape_20.setTransform(27.875,-18.9004);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#000000").ss(13,1,1).p("Ai0BDQAIhAAxglQAtgiBAAAQA/AAA1AiQA5AlAWBC");
	this.shape_21.setTransform(27.875,-18.8004);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#000000").ss(13,1,1).p("Ai0BCQAIhAAxgjQAtgiBAAAQA/gBA1AiQA5AkAWBC");
	this.shape_22.setTransform(27.875,-18.7254);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#000000").ss(13,1,1).p("Ai0BCQAIg/AxgkQAtgiBAAAQA/AAA1AiQA5AkAWBB");
	this.shape_23.setTransform(27.875,-18.6754);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#000000").ss(13,1,1).p("Ai0BBQAIg+AxgkQAtghBAAAQA/gBA1AiQA5AkAWBA");
	this.shape_24.setTransform(27.875,-18.6254);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f().s("#000000").ss(13,1,1).p("Ai0BBQAIg/AxgjQAtghBAAAQA/AAA1AhQA5AjAWBB");
	this.shape_25.setTransform(27.875,-18.5754);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},19).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_14}]},39).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).wait(12));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_1_Layer_1copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#1B1814").ss(11,1,1).p("AkLhfQBDDEDRgGQBWgDBJgrQBKgtAahG");
	this.shape.setTransform(156.025,-52.5925);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(100));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_1_copy_2_Layer_5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_5
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(12,1,1).p("AgGhDQArhKAzgeQA2geApAfQAwBGg+ByQg/B2h8A3QhthJgvhbQgkhIAKg8QAEgbAPgOQAQgPAcgFQBKgMA5Bzg");
	this.shape.setTransform(216.5445,-69.9502);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(12,1,1).p("Ajeh5QAFgeAQgQQARgQAggFQBRgOBACAQAvhTA5ggQA8giAtAjQA1BNhEB+QhGCDiJA9Qh5hRg0hlQgohPALhDg");
	this.shape_1.setTransform(216.5683,-69.9627);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(12,1,1).p("AjviCQAFggASgSQATgSAigFQBXgPBECKQAzhZA+gjQA/gkAxAlQA6BThKCIQhLCNiUBCQiChXg4huQgrhVAMhHg");
	this.shape_2.setTransform(216.5572,-69.9377);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(12,1,1).p("Aj7iIQAFgiATgSQAUgTAjgGQBcgPBICQQA1hdBBglQBDgmAzAnQA9BXhNCPQhPCUicBFQiIhbg7hzQguhZANhLg");
	this.shape_3.setTransform(216.5725,-69.9627);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(12,1,1).p("AkCiNQAFgjATgSQAVgUAkgGQBfgQBKCVQA3hgBCgmQBFgnA1AoQA+BahPCTQhSCZigBHQiMhfg8h2QgvhcANhNg");
	this.shape_4.setTransform(216.5802,-69.9377);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(12,1,1).p("AgIhXQA3hhBDgmQBGgoA1ApQA/BahQCVQhSCaiiBHQiNhfg9h3QgwhdANhOQAGgjATgTQAVgTAlgGQBfgQBLCWg");
	this.shape_5.setTransform(216.5809,-69.9502);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(12,1,1).p("Aj1iFQAFghATgSQATgSAjgGQBZgPBGCNQA0hbA/gkQBBglAzAmQA6BVhLCMQhNCQiXBDQiFhZg5hvQgthYAMhJg");
	this.shape_6.setTransform(216.5709,-69.9627);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(12,1,1).p("Ajnh+QAFgfARgQQASgSAhgFQBVgOBCCFQAxhWA7giQA+gjAvAkQA4BQhHCEQhJCIiPBAQh+hUg2hqQgqhSAMhGg");
	this.shape_7.setTransform(216.5689,-69.9627);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(12,1,1).p("Ajch4QAEgdARgQQARgRAfgFQBRgNA/B/QAvhSA4ghQA7ghAtAiQA1BNhEB9QhFCCiIA9Qh4hRgzhkQgohPALhCg");
	this.shape_8.setTransform(216.5548,-69.9502);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(12,1,1).p("AjUhzQAFgdAQgPQAQgQAegFQBOgNA8B6QAthPA3gfQA4ggArAhQAzBKhBB4QhCB9iDA6QhzhNgyhhQgmhLAKg/g");
	this.shape_9.setTransform(216.5586,-69.9377);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(12,1,1).p("AjNhvQAEgcAQgPQAQgPAdgFQBLgNA7B3QAshNA0geQA3gfAqAgQAxBHg/B1QhAB5h/A5QhwhLgwheQglhJAKg9g");
	this.shape_10.setTransform(216.5416,-69.9502);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(12,1,1).p("AjJhtQAEgcAPgOQAQgPAcgFQBKgMA6B0QArhLA0geQA2geApAfQAwBGg+BzQg/B3h9A3QhuhJgvhcQglhIALg8g");
	this.shape_11.setTransform(216.5483,-69.9502);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},3).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},6).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},6).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape}]},1).wait(3));
	this.timeline.addTween(cjs.Tween.get(this.shape).wait(3).to({_off:true},1).wait(11).to({_off:false},0).wait(6).to({_off:true},1).wait(11).to({_off:false},0).wait(6).to({_off:true},1).wait(11).to({_off:false},0).wait(3));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_1_copy_2_Layer_4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_4
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(12,1,1).p("AgGhDQArhKAzgeQA2geApAfQAwBGg+ByQg/B2h8A3QhthJgvhbQgkhIAKg8QAEgbAPgOQAQgPAcgFQBKgMA5Bzg");
	this.shape.setTransform(286.2945,-69.9502);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(12,1,1).p("Ajeh5QAFgeAQgQQARgQAggFQBRgOBACAQAvhTA5ggQA8giAtAjQA1BNhEB+QhGCDiJA9Qh5hRg0hlQgohPALhDg");
	this.shape_1.setTransform(286.3183,-69.9627);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(12,1,1).p("AjviCQAFggASgSQATgSAigFQBXgPBECKQAzhZA+gjQA/gkAxAlQA6BThKCIQhLCNiUBCQiChXg4huQgrhVAMhHg");
	this.shape_2.setTransform(286.3072,-69.9377);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(12,1,1).p("Aj7iIQAFgiATgSQAUgTAjgGQBcgPBICQQA1hdBBglQBDgmAzAnQA9BXhNCPQhPCUicBFQiIhbg7hzQguhZANhLg");
	this.shape_3.setTransform(286.3225,-69.9627);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(12,1,1).p("AkCiNQAFgjATgSQAVgUAkgGQBfgQBKCVQA3hgBCgmQBFgnA1AoQA+BahPCTQhSCZigBHQiMhfg8h2QgvhcANhNg");
	this.shape_4.setTransform(286.3302,-69.9377);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(12,1,1).p("AgIhXQA3hhBDgmQBGgoA1ApQA/BahQCVQhSCaiiBHQiNhfg9h3QgwhdANhOQAGgjATgTQAVgTAlgGQBfgQBLCWg");
	this.shape_5.setTransform(286.3309,-69.9502);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(12,1,1).p("Aj1iFQAFghATgSQATgSAjgGQBZgPBGCNQA0hbA/gkQBBglAzAmQA6BVhLCMQhNCQiXBDQiFhZg5hvQgthYAMhJg");
	this.shape_6.setTransform(286.3209,-69.9627);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(12,1,1).p("Ajnh+QAFgfARgQQASgSAhgFQBVgOBCCFQAxhWA7giQA+gjAvAkQA4BQhHCEQhJCIiPBAQh+hUg2hqQgqhSAMhGg");
	this.shape_7.setTransform(286.3189,-69.9627);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(12,1,1).p("Ajch4QAEgdARgQQARgRAfgFQBRgNA/B/QAvhSA4ghQA7ghAtAiQA1BNhEB9QhFCCiIA9Qh4hRgzhkQgohPALhCg");
	this.shape_8.setTransform(286.3048,-69.9502);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(12,1,1).p("AjUhzQAFgdAQgPQAQgQAegFQBOgNA8B6QAthPA3gfQA4ggArAhQAzBKhBB4QhCB9iDA6QhzhNgyhhQgmhLAKg/g");
	this.shape_9.setTransform(286.3086,-69.9377);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(12,1,1).p("AjNhvQAEgcAQgPQAQgPAdgFQBLgNA7B3QAshNA0geQA3gfAqAgQAxBHg/B1QhAB5h/A5QhwhLgwheQglhJAKg9g");
	this.shape_10.setTransform(286.2916,-69.9502);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(12,1,1).p("AjJhtQAEgcAPgOQAQgPAcgFQBKgMA6B0QArhLA0geQA2geApAfQAwBGg+BzQg/B3h9A3QhuhJgvhcQglhIALg8g");
	this.shape_11.setTransform(286.2983,-69.9502);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},3).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},6).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},6).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape}]},1).wait(3));
	this.timeline.addTween(cjs.Tween.get(this.shape).wait(3).to({_off:true},1).wait(11).to({_off:false},0).wait(6).to({_off:true},1).wait(11).to({_off:false},0).wait(6).to({_off:true},1).wait(11).to({_off:false},0).wait(3));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_1_copy_2_Layer_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(12,1,1).p("AjOhNQAABBA0ApQA0ApBGAHQBFAGBEgZQBDgZAYgxQAMgZgBgV");
	this.shape.setTransform(253.7789,-30.916);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(54));

}).prototype = p = new cjs.MovieClip();


(lib.Scene_1_Layer_5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_5
	this.instance = new lib.pack3();
	this.instance.parent = this;
	this.instance.setTransform(791.15,118.9);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Scene_1_Layer_5, null, null);


(lib.Scene_1_Layer_4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_4
	this.instance = new lib.pack2();
	this.instance.parent = this;
	this.instance.setTransform(638.75,127.15);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Scene_1_Layer_4, null, null);


(lib.Scene_1_Layer_3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_3
	this.instance = new lib.pack1();
	this.instance.parent = this;
	this.instance.setTransform(519.25,118.9);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Scene_1_Layer_3, null, null);


(lib.Scene_1_Layer_2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.instance = new lib.bg();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Scene_1_Layer_2, null, null);


(lib.Symbol2copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.rumyanec();
	this.instance.parent = this;
	this.instance.setTransform(-26,-16.6);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol2copy, new cjs.Rectangle(-26,-16.6,46,38), null);


(lib.Symbol1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.rumyanec();
	this.instance.parent = this;
	this.instance.setTransform(-27.2,-4.4);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol1, new cjs.Rectangle(-27.2,-4.4,46,38), null);


(lib.Symbol19copy2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Symbol18();
	this.instance.parent = this;
	this.instance.setTransform(48.7,26.55,0.3222,0.3222);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({x:-68.3,y:726.55},234).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-69.7,25.1,119.9,702.9);


(lib.Symbol19copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Symbol18();
	this.instance.parent = this;
	this.instance.setTransform(48.7,26.55,0.6,0.6,0,0,0,0.1,0.1);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({regX:-0.1,regY:0,x:-101.3,y:726.55},143).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-103.9,23.8,155.3,705.5);


(lib.Symbol19 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Symbol18();
	this.instance.parent = this;
	this.instance.setTransform(48.7,26.55,0.3222,0.3222);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({x:-101.3,y:726.55},184).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-102.7,25.1,152.9,702.9);


(lib.Symbol15 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_99 = function() {
		this.___loopingOver___ = true;
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(99).call(this.frame_99).wait(1));

	// Layer_3_obj_
	this.Layer_3 = new lib.Symbol_15_Layer_3();
	this.Layer_3.name = "Layer_3";
	this.Layer_3.parent = this;
	this.Layer_3.setTransform(2.1,-9.8,1,1,0,0,0,2.1,-9.8);
	this.Layer_3.depth = 0;
	this.Layer_3.isAttachedToCamera = 0
	this.Layer_3.isAttachedToMask = 0
	this.Layer_3.layerDepth = 0
	this.Layer_3.layerIndex = 0
	this.Layer_3.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_3).wait(100));

	// Layer_2_obj_
	this.Layer_2 = new lib.Symbol_15_Layer_2();
	this.Layer_2.name = "Layer_2";
	this.Layer_2.parent = this;
	this.Layer_2.setTransform(4.8,-7,1,1,0,0,0,4.8,-7);
	this.Layer_2.depth = 0;
	this.Layer_2.isAttachedToCamera = 0
	this.Layer_2.isAttachedToMask = 0
	this.Layer_2.layerDepth = 0
	this.Layer_2.layerIndex = 1
	this.Layer_2.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_2).wait(100));

	// Layer_1_obj_
	this.Layer_1 = new lib.Symbol_15_Layer_1();
	this.Layer_1.name = "Layer_1";
	this.Layer_1.parent = this;
	this.Layer_1.depth = 0;
	this.Layer_1.isAttachedToCamera = 0
	this.Layer_1.isAttachedToMask = 0
	this.Layer_1.layerDepth = 0
	this.Layer_1.layerIndex = 2
	this.Layer_1.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_1).wait(100));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-14.3,-19.3,36.8,35.900000000000006);


(lib.Symbol14 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_99 = function() {
		this.___loopingOver___ = true;
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(99).call(this.frame_99).wait(1));

	// Layer_3_obj_
	this.Layer_3 = new lib.Symbol_14_Layer_3();
	this.Layer_3.name = "Layer_3";
	this.Layer_3.parent = this;
	this.Layer_3.setTransform(5,-12.3,1,1,0,0,0,5,-12.3);
	this.Layer_3.depth = 0;
	this.Layer_3.isAttachedToCamera = 0
	this.Layer_3.isAttachedToMask = 0
	this.Layer_3.layerDepth = 0
	this.Layer_3.layerIndex = 0
	this.Layer_3.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_3).wait(100));

	// Layer_2_obj_
	this.Layer_2 = new lib.Symbol_14_Layer_2();
	this.Layer_2.name = "Layer_2";
	this.Layer_2.parent = this;
	this.Layer_2.setTransform(-5.5,-8.6,1,1,0,0,0,-5.5,-8.6);
	this.Layer_2.depth = 0;
	this.Layer_2.isAttachedToCamera = 0
	this.Layer_2.isAttachedToMask = 0
	this.Layer_2.layerDepth = 0
	this.Layer_2.layerIndex = 1
	this.Layer_2.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_2).wait(100));

	// Layer_1_obj_
	this.Layer_1 = new lib.Symbol_14_Layer_1();
	this.Layer_1.name = "Layer_1";
	this.Layer_1.parent = this;
	this.Layer_1.depth = 0;
	this.Layer_1.isAttachedToCamera = 0
	this.Layer_1.isAttachedToMask = 0
	this.Layer_1.layerDepth = 0
	this.Layer_1.layerIndex = 2
	this.Layer_1.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_1).wait(100));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-18.5,-24.1,34.2,36);


(lib.Symbol12 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Symbol_6 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("ABZCMQg6g6AAhSQAAhSA6g6QA6g5BTAAQBSAAA5A5QA7A6AABSQAABSg7A6Qg5A6hSABQhTgBg6g6g");
	mask.setTransform(42.8,18.4);

	// Symbol_7
	this.instance = new lib.Symbol7();
	this.instance.parent = this;
	this.instance.setTransform(65.95,58.75,1,1,0,0,0,19.9,19.9);

	var maskedShapeInstanceList = [this.instance];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1).to({y:54.4},0).wait(1).to({y:51.75},0).wait(1).to({y:49.9},0).wait(1).to({y:48.5},0).wait(1).to({y:47.4},0).wait(1).to({y:46.6},0).wait(1).to({y:45.95},0).wait(1).to({y:45.4},0).wait(1).to({y:45},0).wait(1).to({y:44.7},0).wait(1).to({y:44.45},0).wait(1).to({y:44.25},0).wait(1).to({y:44.1},0).wait(1).to({y:44.05},0).wait(1).to({y:44},0).wait(37).to({x:67.75,y:46.5},0).to({y:58.75},8,cjs.Ease.get(1)).wait(40));

	// Symbol_8 (mask)
	var mask_1 = new cjs.Shape();
	mask_1._off = true;
	var mask_1_graphics_0 = new cjs.Graphics().p("AiMCMQg5g6gBhSQABhSA5g6QA6g5BSAAQBSAAA6A5QA6A6AABSQAABSg6A6Qg6A6hSABQhSgBg6g6g");
	var mask_1_graphics_52 = new cjs.Graphics().p("AiMCMQg5g6gBhSQABhSA5g6QA6g5BSgBQBSABA6A5QA6A6AABSQAABSg6A6Qg6A6hSAAQhSAAg6g6g");

	this.timeline.addTween(cjs.Tween.get(mask_1).to({graphics:mask_1_graphics_0,x:19.85,y:16.6}).wait(52).to({graphics:mask_1_graphics_52,x:19.85,y:19.85}).wait(48));

	// Symbol_9
	this.instance_1 = new lib.Symbol9();
	this.instance_1.parent = this;
	this.instance_1.setTransform(20.2,57.25,1,1,0,0,0,19.9,19.9);

	var maskedShapeInstanceList = [this.instance_1];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask_1;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(1).to({y:53.85},0).wait(1).to({y:51.8},0).wait(1).to({y:50.35},0).wait(1).to({y:49.25},0).wait(1).to({y:48.4},0).wait(1).to({y:47.75},0).wait(1).to({y:47.25},0).wait(1).to({y:46.85},0).wait(1).to({y:46.55},0).wait(1).to({y:46.3},0).wait(1).to({y:46.1},0).wait(1).to({y:45.95},0).wait(1).to({y:45.85},0).wait(1).to({y:45.75},0).wait(38).to({x:22,y:48.25},0).to({y:60.5},8,cjs.Ease.get(1)).wait(40));

	// Symbol_11
	this.instance_2 = new lib.Symbol11();
	this.instance_2.parent = this;
	this.instance_2.setTransform(60.65,23.15,1,1,0,0,0,6.7,6.8);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(15).to({x:64.15,y:21.3},7,cjs.Ease.get(1)).wait(3).to({x:60.65,y:23.15},7,cjs.Ease.get(1)).wait(3).to({x:64.15,y:21.3},7,cjs.Ease.get(1)).wait(3).to({x:60.65,y:23.15},7,cjs.Ease.get(1)).wait(48));

	// Symbol_10
	this.instance_3 = new lib.Symbol10();
	this.instance_3.parent = this;
	this.instance_3.setTransform(12.5,24.15,1,1,0,0,0,6.7,6.8);

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(15).to({x:16,y:22.3},7,cjs.Ease.get(1)).wait(3).to({x:12.5,y:24.15},7,cjs.Ease.get(1)).wait(3).to({x:16,y:22.3},7,cjs.Ease.get(1)).wait(3).to({x:12.5,y:24.15},7,cjs.Ease.get(1)).wait(48));

	// Layer_8
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AiMCMQg5g6gBhSQABhSA5g6QA6g5BSAAQBSAAA6A5QA6A6AABSQAABSg6A6Qg6A6hSABQhSgBg6g6g");
	this.shape.setTransform(19.85,16.6);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(100));

	// Layer_9
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AiMCMQg5g6gBhSQABhSA5g6QA6g5BSAAQBSAAA6A5QA6A6AABSQAABSg6A6Qg6A6hSABQhSgBg6g6g");
	this.shape_1.setTransform(65.75,18.4);

	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(100));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,-3.2,85.6,42.900000000000006);


(lib.Symbol1copy2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_53 = function() {
		this.___loopingOver___ = true;
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(53).call(this.frame_53).wait(1));

	// Layer_5_obj_
	this.Layer_5 = new lib.Symbol_1_copy_2_Layer_5();
	this.Layer_5.name = "Layer_5";
	this.Layer_5.parent = this;
	this.Layer_5.setTransform(216.6,-70,1,1,0,0,0,216.6,-70);
	this.Layer_5.depth = 0;
	this.Layer_5.isAttachedToCamera = 0
	this.Layer_5.isAttachedToMask = 0
	this.Layer_5.layerDepth = 0
	this.Layer_5.layerIndex = 0
	this.Layer_5.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_5).wait(54));

	// Layer_4_obj_
	this.Layer_4 = new lib.Symbol_1_copy_2_Layer_4();
	this.Layer_4.name = "Layer_4";
	this.Layer_4.parent = this;
	this.Layer_4.setTransform(286.3,-70,1,1,0,0,0,286.3,-70);
	this.Layer_4.depth = 0;
	this.Layer_4.isAttachedToCamera = 0
	this.Layer_4.isAttachedToMask = 0
	this.Layer_4.layerDepth = 0
	this.Layer_4.layerIndex = 1
	this.Layer_4.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_4).wait(54));

	// Layer_1_obj_
	this.Layer_1 = new lib.Symbol_1_copy_2_Layer_1();
	this.Layer_1.name = "Layer_1";
	this.Layer_1.parent = this;
	this.Layer_1.setTransform(253.8,-30.9,1,1,0,0,0,253.8,-30.9);
	this.Layer_1.depth = 0;
	this.Layer_1.isAttachedToCamera = 0
	this.Layer_1.isAttachedToMask = 0
	this.Layer_1.layerDepth = 0
	this.Layer_1.layerIndex = 2
	this.Layer_1.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_1).wait(54));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(184.1,-102.7,135.00000000000003,85.6);


(lib.Symbol1_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_99 = function() {
		this.___loopingOver___ = true;
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(99).call(this.frame_99).wait(1));

	// Layer_1_obj_
	this.Layer_1 = new lib.Symbol_1_Layer_1copy();
	this.Layer_1.name = "Layer_1";
	this.Layer_1.parent = this;
	this.Layer_1.setTransform(156,-52.6,1,1,0,0,0,156,-52.6);
	this.Layer_1.depth = 0;
	this.Layer_1.isAttachedToCamera = 0
	this.Layer_1.isAttachedToMask = 0
	this.Layer_1.layerDepth = 0
	this.Layer_1.layerIndex = 0
	this.Layer_1.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_1).wait(100));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(123.8,-67.8,64.50000000000001,30.299999999999997);


(lib.Symbol_22_Symbol_15 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Symbol_15
	this.instance = new lib.Symbol15();
	this.instance.parent = this;
	this.instance.setTransform(19,30.15,1,1,0,0,0,-11.7,-11.7);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(49).to({rotation:-44.9994,x:1.05,y:18.6},10,cjs.Ease.get(1)).wait(30).to({rotation:0,x:19,y:30.15},10,cjs.Ease.get(1)).wait(1));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_21_Symbol_14 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Symbol_14
	this.instance = new lib.Symbol14();
	this.instance.parent = this;
	this.instance.setTransform(-15.55,41.3,1,1,0,0,0,14.2,-4.2);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(23).to({x:-30.55,y:23.3},10,cjs.Ease.get(1)).wait(49).to({x:-15.55,y:41.3},10,cjs.Ease.get(1)).wait(8));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_17_Symbol_13 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Symbol_13
	this.instance = new lib.Symbol13();
	this.instance.parent = this;
	this.instance.setTransform(30.9,-24.35,1,1,0,0,0,-4.2,13.1);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(49).to({regY:13,rotation:-49.9549,x:7.5,y:-28},6,cjs.Ease.get(1)).to({regY:13.1,rotation:0,x:30.9,y:-24.35},6,cjs.Ease.get(-1)).to({rotation:21.4775,x:46.1,y:-25.8},6,cjs.Ease.get(1)).to({rotation:0,x:30.9,y:-24.35},7,cjs.Ease.get(-1)).to({regY:13,rotation:-49.9549,x:7.5,y:-28},6,cjs.Ease.get(1)).to({regY:13.1,rotation:0,x:30.9,y:-24.35},6,cjs.Ease.get(-1)).to({rotation:21.4775,x:46.1,y:-25.8},6,cjs.Ease.get(1)).to({rotation:0,x:30.9,y:-24.35},7,cjs.Ease.get(-1)).wait(1));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_16_Symbol_5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Symbol_5
	this.instance = new lib.Symbol5();
	this.instance.parent = this;
	this.instance.setTransform(-43.25,-10,1,1,0,0,0,3.9,13.9);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(23).to({x:-25.25,y:-46},10,cjs.Ease.get(1)).wait(2).to({regY:13.8,rotation:-26.7132,x:-25.3,y:-46.1},3,cjs.Ease.get(1)).to({regY:13.9,rotation:0,x:-25.25,y:-46},4,cjs.Ease.get(-1)).to({regX:3.8,rotation:24.9888,x:-25.35,y:-46.05},4,cjs.Ease.get(1)).to({regX:3.9,rotation:0,x:-25.25,y:-46},4,cjs.Ease.get(-1)).to({regY:13.8,rotation:-26.7132,x:-25.3,y:-46.1},3,cjs.Ease.get(1)).to({regY:13.9,rotation:0,x:-25.25,y:-46},4,cjs.Ease.get(-1)).to({regX:3.8,rotation:24.9888,x:-25.35,y:-46.05},4,cjs.Ease.get(1)).to({regX:3.9,rotation:0,x:-25.25,y:-46},4,cjs.Ease.get(-1)).to({regY:13.8,rotation:-26.7132,x:-25.3,y:-46.1},3,cjs.Ease.get(1)).to({regY:13.9,rotation:0,x:-25.25,y:-46},4,cjs.Ease.get(-1)).to({regX:3.8,rotation:24.9888,x:-25.35,y:-46.05},4,cjs.Ease.get(1)).to({regX:3.9,rotation:0,x:-25.25,y:-46},4,cjs.Ease.get(-1)).wait(2).to({x:-43.25,y:-10},10,cjs.Ease.get(1)).wait(8));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_3_Symbol_2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Symbol_2
	this.instance = new lib.Symbol2copy();
	this.instance.parent = this;
	this.instance.setTransform(-58.8,8.1,0.8859,0.8277,0,0,180,-0.1,0.1);
	this.instance.alpha = 0;
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(19).to({_off:false},0).to({alpha:1},15).wait(39).to({alpha:0},15).to({_off:true},1).wait(11));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_3_Symbol_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Symbol_1
	this.instance = new lib.Symbol1();
	this.instance.parent = this;
	this.instance.setTransform(56.25,5.8);
	this.instance.alpha = 0;
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(19).to({_off:false},0).to({alpha:1},15).wait(39).to({alpha:0},15).to({_off:true},1).wait(11));

}).prototype = p = new cjs.MovieClip();


(lib.Scene_1_Layer_9 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_9
	this.instance = new lib.Symbol1copy2();
	this.instance.parent = this;
	this.instance.setTransform(772.8,241.45,0.8962,0.8962,0,0,180,245.8,-56.6);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Scene_1_Layer_9, null, null);


(lib.Symbol22 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_99 = function() {
		this.___loopingOver___ = true;
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(99).call(this.frame_99).wait(1));

	// Symbol_15_obj_
	this.Symbol_15 = new lib.Symbol_22_Symbol_15();
	this.Symbol_15.name = "Symbol_15";
	this.Symbol_15.parent = this;
	this.Symbol_15.setTransform(34.8,39.5,1,1,0,0,0,34.8,39.5);
	this.Symbol_15.depth = 0;
	this.Symbol_15.isAttachedToCamera = 0
	this.Symbol_15.isAttachedToMask = 0
	this.Symbol_15.layerDepth = 0
	this.Symbol_15.layerIndex = 0
	this.Symbol_15.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Symbol_15).wait(100));

	// Layer_3_obj_
	this.Layer_3 = new lib.Symbol_22_Layer_3();
	this.Layer_3.name = "Layer_3";
	this.Layer_3.parent = this;
	this.Layer_3.setTransform(-12.9,-9.8,1,1,0,0,0,-12.9,-9.8);
	this.Layer_3.depth = 0;
	this.Layer_3.isAttachedToCamera = 0
	this.Layer_3.isAttachedToMask = 0
	this.Layer_3.layerDepth = 0
	this.Layer_3.layerIndex = 1
	this.Layer_3.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_3).wait(100));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-66.1,-55.6,119.3,111.30000000000001);


(lib.Symbol21 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_99 = function() {
		this.___loopingOver___ = true;
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(99).call(this.frame_99).wait(1));

	// Symbol_14_obj_
	this.Symbol_14 = new lib.Symbol_21_Symbol_14();
	this.Symbol_14.name = "Symbol_14";
	this.Symbol_14.parent = this;
	this.Symbol_14.setTransform(-31.2,39.4,1,1,0,0,0,-31.2,39.4);
	this.Symbol_14.depth = 0;
	this.Symbol_14.isAttachedToCamera = 0
	this.Symbol_14.isAttachedToMask = 0
	this.Symbol_14.layerDepth = 0
	this.Symbol_14.layerIndex = 0
	this.Symbol_14.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Symbol_14).wait(100));

	// Layer_3_obj_
	this.Layer_3 = new lib.Symbol_21_Layer_3();
	this.Layer_3.name = "Layer_3";
	this.Layer_3.parent = this;
	this.Layer_3.setTransform(10.8,-5.3,1,1,0,0,0,10.8,-5.3);
	this.Layer_3.depth = 0;
	this.Layer_3.isAttachedToCamera = 0
	this.Layer_3.isAttachedToMask = 0
	this.Layer_3.layerDepth = 0
	this.Layer_3.layerIndex = 1
	this.Layer_3.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_3).wait(100));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-63.2,-57.3,111.9,114.69999999999999);


(lib.Symbol20 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_75 = function() {
		/* Stop at This Frame
		The  timeline will stop/pause at the frame where you insert this code.
		Can also be used to stop/pause the timeline of movieclips.
		*/
		
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(75).call(this.frame_75).wait(1));

	// Layer_33
	this.instance = new lib.Symbol19copy2();
	this.instance.parent = this;
	this.instance.setTransform(-877,-26.55);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(36).to({_off:false},0).wait(40));

	// Layer_32
	this.instance_1 = new lib.Symbol19copy2();
	this.instance_1.parent = this;
	this.instance_1.setTransform(-807.7,-26.55);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(70).to({_off:false},0).wait(6));

	// Layer_31
	this.instance_2 = new lib.Symbol19copy2();
	this.instance_2.parent = this;
	this.instance_2.setTransform(-725.2,-26.55);
	this.instance_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(6).to({_off:false},0).wait(70));

	// Layer_30
	this.instance_3 = new lib.Symbol19copy2();
	this.instance_3.parent = this;
	this.instance_3.setTransform(-593.2,-26.55);
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(59).to({_off:false},0).wait(17));

	// Layer_29
	this.instance_4 = new lib.Symbol19copy2();
	this.instance_4.parent = this;
	this.instance_4.setTransform(-527.2,-26.55);
	this.instance_4._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(20).to({_off:false},0).wait(56));

	// Layer_28
	this.instance_5 = new lib.Symbol19copy2();
	this.instance_5.parent = this;
	this.instance_5.setTransform(-428.2,-26.55);
	this.instance_5._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(48).to({_off:false},0).wait(28));

	// Layer_27
	this.instance_6 = new lib.Symbol19copy2();
	this.instance_6.parent = this;
	this.instance_6.setTransform(-312.7,-26.55);
	this.instance_6._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(3).to({_off:false},0).wait(73));

	// Layer_26
	this.instance_7 = new lib.Symbol19copy2();
	this.instance_7.parent = this;
	this.instance_7.setTransform(-197.2,-26.55);
	this.instance_7._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(40).to({_off:false},0).wait(36));

	// Layer_25
	this.instance_8 = new lib.Symbol19copy2();
	this.instance_8.parent = this;
	this.instance_8.setTransform(-81.7,-26.55);
	this.instance_8._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_8).wait(18).to({_off:false},0).wait(58));

	// Layer_23
	this.instance_9 = new lib.Symbol19copy();
	this.instance_9.parent = this;
	this.instance_9.setTransform(-620.2,-26.55);
	this.instance_9._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_9).wait(26).to({_off:false},0).wait(50));

	// Layer_22
	this.instance_10 = new lib.Symbol19copy();
	this.instance_10.parent = this;
	this.instance_10.setTransform(-9.7,-26.55);
	this.instance_10._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_10).wait(15).to({_off:false},0).wait(61));

	// Layer_21
	this.instance_11 = new lib.Symbol19copy();
	this.instance_11.parent = this;
	this.instance_11.setTransform(-108.7,-26.55);
	this.instance_11._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_11).wait(36).to({_off:false},0).wait(40));

	// Layer_20
	this.instance_12 = new lib.Symbol19copy();
	this.instance_12.parent = this;
	this.instance_12.setTransform(-950.2,-26.55);
	this.instance_12._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_12).wait(5).to({_off:false},0).wait(71));

	// Layer_19
	this.instance_13 = new lib.Symbol19copy();
	this.instance_13.parent = this;
	this.instance_13.setTransform(-851.2,-26.55);
	this.instance_13._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_13).wait(56).to({_off:false},0).wait(20));

	// Layer_18
	this.instance_14 = new lib.Symbol19copy();
	this.instance_14.parent = this;
	this.instance_14.setTransform(-702.7,-26.55);
	this.instance_14._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_14).wait(20).to({_off:false},0).wait(56));

	// Layer_17
	this.instance_15 = new lib.Symbol19copy();
	this.instance_15.parent = this;
	this.instance_15.setTransform(-554.2,-26.55);
	this.instance_15._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_15).wait(45).to({_off:false},0).wait(31));

	// Layer_16
	this.instance_16 = new lib.Symbol19copy();
	this.instance_16.parent = this;
	this.instance_16.setTransform(-455.2,-26.55);
	this.instance_16._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_16).wait(10).to({_off:false},0).wait(66));

	// Layer_15
	this.instance_17 = new lib.Symbol19copy();
	this.instance_17.parent = this;
	this.instance_17.setTransform(-356.2,-26.55);
	this.instance_17._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_17).wait(63).to({_off:false},0).wait(13));

	// Layer_14
	this.instance_18 = new lib.Symbol19copy();
	this.instance_18.parent = this;
	this.instance_18.setTransform(-224.2,-26.55);
	this.instance_18._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_18).wait(2).to({_off:false},0).wait(74));

	// Layer_12
	this.instance_19 = new lib.Symbol19();
	this.instance_19.parent = this;
	this.instance_19.setTransform(-164.2,-26.55);
	this.instance_19._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_19).wait(39).to({_off:false},0).wait(37));

	// Layer_11
	this.instance_20 = new lib.Symbol19();
	this.instance_20.parent = this;
	this.instance_20.setTransform(-395.2,-26.55);
	this.instance_20._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_20).wait(27).to({_off:false},0).wait(49));

	// Layer_10
	this.instance_21 = new lib.Symbol19();
	this.instance_21.parent = this;
	this.instance_21.setTransform(-659.2,-26.55);
	this.instance_21._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_21).wait(11).to({_off:false},0).wait(65));

	// Layer_9
	this.instance_22 = new lib.Symbol19();
	this.instance_22.parent = this;
	this.instance_22.setTransform(-989.2,-26.55);
	this.instance_22._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_22).wait(51).to({_off:false},0).wait(25));

	// Layer_8
	this.instance_23 = new lib.Symbol19();
	this.instance_23.parent = this;
	this.instance_23.setTransform(-906.7,-26.55);
	this.instance_23._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_23).wait(4).to({_off:false},0).wait(72));

	// Layer_7
	this.instance_24 = new lib.Symbol19();
	this.instance_24.parent = this;
	this.instance_24.setTransform(-758.2,-26.55);
	this.instance_24._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_24).wait(33).to({_off:false},0).wait(43));

	// Layer_6
	this.instance_25 = new lib.Symbol19();
	this.instance_25.parent = this;
	this.instance_25.setTransform(-576.7,-26.55);
	this.instance_25._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_25).wait(9).to({_off:false},0).wait(67));

	// Layer_5
	this.instance_26 = new lib.Symbol19();
	this.instance_26.parent = this;
	this.instance_26.setTransform(-494.2,-26.55);
	this.instance_26._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_26).wait(66).to({_off:false},0).wait(10));

	// Layer_4
	this.instance_27 = new lib.Symbol19();
	this.instance_27.parent = this;
	this.instance_27.setTransform(-279.7,-26.55);
	this.instance_27._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_27).wait(1).to({_off:false},0).wait(75));

	// Layer_3
	this.instance_28 = new lib.Symbol19copy2();
	this.instance_28.parent = this;
	this.instance_28.setTransform(-180.7,-26.55);
	this.instance_28._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_28).wait(54).to({_off:false},0).wait(22));

	// Layer_2
	this.instance_29 = new lib.Symbol19copy();
	this.instance_29.parent = this;
	this.instance_29.setTransform(-108.7,-26.55);
	this.instance_29._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_29).wait(22).to({_off:false},0).wait(54));

	// Layer_1
	this.instance_30 = new lib.Symbol19();
	this.instance_30.parent = this;
	this.instance_30.setTransform(-48.7,-26.55);

	this.timeline.addTween(cjs.Tween.get(this.instance_30).wait(76));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-941.9,-2.7,983.6,5.4);


(lib.Symbol17 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_99 = function() {
		this.___loopingOver___ = true;
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(99).call(this.frame_99).wait(1));

	// Symbol_13_obj_
	this.Symbol_13 = new lib.Symbol_17_Symbol_13();
	this.Symbol_13.name = "Symbol_13";
	this.Symbol_13.parent = this;
	this.Symbol_13.setTransform(29.9,-39.1,1,1,0,0,0,29.9,-39.1);
	this.Symbol_13.depth = 0;
	this.Symbol_13.isAttachedToCamera = 0
	this.Symbol_13.isAttachedToMask = 0
	this.Symbol_13.layerDepth = 0
	this.Symbol_13.layerIndex = 0
	this.Symbol_13.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Symbol_13).wait(100));

	// Layer_3_obj_
	this.Layer_3 = new lib.Symbol_17_Layer_3();
	this.Layer_3.name = "Layer_3";
	this.Layer_3.parent = this;
	this.Layer_3.setTransform(-3,12,1,1,0,0,0,-3,12);
	this.Layer_3.depth = 0;
	this.Layer_3.isAttachedToCamera = 0
	this.Layer_3.isAttachedToMask = 0
	this.Layer_3.layerDepth = 0
	this.Layer_3.layerIndex = 1
	this.Layer_3.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_3).wait(100));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-44.5,-58.4,113.9,114.4);


(lib.Symbol16 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_99 = function() {
		this.___loopingOver___ = true;
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(99).call(this.frame_99).wait(1));

	// Symbol_5_obj_
	this.Symbol_5 = new lib.Symbol_16_Symbol_5();
	this.Symbol_5.name = "Symbol_5";
	this.Symbol_5.parent = this;
	this.Symbol_5.setTransform(-39.8,-24.7,1,1,0,0,0,-39.8,-24.7);
	this.Symbol_5.depth = 0;
	this.Symbol_5.isAttachedToCamera = 0
	this.Symbol_5.isAttachedToMask = 0
	this.Symbol_5.layerDepth = 0
	this.Symbol_5.layerIndex = 0
	this.Symbol_5.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Symbol_5).wait(100));

	// Layer_3_obj_
	this.Layer_3 = new lib.Symbol_16_Layer_3();
	this.Layer_3.name = "Layer_3";
	this.Layer_3.parent = this;
	this.Layer_3.setTransform(3.1,11.7,1,1,0,0,0,3.1,11.7);
	this.Layer_3.depth = 0;
	this.Layer_3.isAttachedToCamera = 0
	this.Layer_3.isAttachedToMask = 0
	this.Layer_3.layerDepth = 0
	this.Layer_3.layerIndex = 1
	this.Layer_3.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_3).wait(100));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-55.9,-79.1,111.8,120.89999999999999);


(lib.Symbol3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_99 = function() {
		this.___loopingOver___ = true;
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(99).call(this.frame_99).wait(1));

	// Layer_5_obj_
	this.Layer_5 = new lib.Symbol_3_Layer_5();
	this.Layer_5.name = "Layer_5";
	this.Layer_5.parent = this;
	this.Layer_5.setTransform(6.1,25.6,1,1,0,0,0,6.1,25.6);
	this.Layer_5.depth = 0;
	this.Layer_5.isAttachedToCamera = 0
	this.Layer_5.isAttachedToMask = 0
	this.Layer_5.layerDepth = 0
	this.Layer_5.layerIndex = 0
	this.Layer_5.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_5).wait(100));

	// Layer_1_obj_
	this.Layer_1 = new lib.Symbol_3_Layer_1();
	this.Layer_1.name = "Layer_1";
	this.Layer_1.parent = this;
	this.Layer_1.setTransform(27.9,-18.6,1,1,0,0,0,27.9,-18.6);
	this.Layer_1.depth = 0;
	this.Layer_1.isAttachedToCamera = 0
	this.Layer_1.isAttachedToMask = 0
	this.Layer_1.layerDepth = 0
	this.Layer_1.layerIndex = 1
	this.Layer_1.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_1).wait(100));

	// Layer_4_obj_
	this.Layer_4 = new lib.Symbol_3_Layer_4();
	this.Layer_4.name = "Layer_4";
	this.Layer_4.parent = this;
	this.Layer_4.setTransform(-41.5,-18.2,1,1,0,0,0,-41.5,-18.2);
	this.Layer_4.depth = 0;
	this.Layer_4.isAttachedToCamera = 0
	this.Layer_4.isAttachedToMask = 0
	this.Layer_4.layerDepth = 0
	this.Layer_4.layerIndex = 2
	this.Layer_4.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_4).wait(100));

	// Symbol_2_obj_
	this.Symbol_2 = new lib.Symbol_3_Symbol_2();
	this.Symbol_2.name = "Symbol_2";
	this.Symbol_2.parent = this;
	this.Symbol_2.depth = 0;
	this.Symbol_2.isAttachedToCamera = 0
	this.Symbol_2.isAttachedToMask = 0
	this.Symbol_2.layerDepth = 0
	this.Symbol_2.layerIndex = 3
	this.Symbol_2.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Symbol_2).wait(100));

	// Symbol_1_obj_
	this.Symbol_1 = new lib.Symbol_3_Symbol_1();
	this.Symbol_1.name = "Symbol_1";
	this.Symbol_1.parent = this;
	this.Symbol_1.depth = 0;
	this.Symbol_1.isAttachedToCamera = 0
	this.Symbol_1.isAttachedToMask = 0
	this.Symbol_1.layerDepth = 0
	this.Symbol_1.layerIndex = 4
	this.Symbol_1.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Symbol_1).wait(100));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-76.6,-34.9,151.7,74.9);


(lib.Symbol2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.instance = new lib.Symbol12();
	this.instance.parent = this;
	this.instance.setTransform(30.35,-14.4,1.0998,1.0998,0,0.0485,-179.9507,43.4,34.9);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	// Layer_1
	this.instance_1 = new lib.Symbol1_1();
	this.instance_1.parent = this;
	this.instance_1.setTransform(30.8,13.65,1.06,1.06,0,0,0,156,-52.6);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol2, new cjs.Rectangle(-16.2,-56.3,94.4,89.9), null);


(lib.Symbol_4_Layer_11 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_11
	this.instance = new lib.Symbol22();
	this.instance.parent = this;
	this.instance.setTransform(355.3,55.25);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol_4_Layer_11, null, null);


(lib.Symbol_4_Layer_8 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_8
	this.instance = new lib.Symbol21();
	this.instance.parent = this;
	this.instance.setTransform(133.8,45.65);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol_4_Layer_8, null, null);


(lib.Symbol_4_Layer_5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_5
	this.instance = new lib.Symbol17();
	this.instance.parent = this;
	this.instance.setTransform(466.05,-27.05);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol_4_Layer_5, null, null);


(lib.Symbol_4_Layer_2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.instance = new lib.Symbol16();
	this.instance.parent = this;
	this.instance.setTransform(-3.05,-11.65);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol_4_Layer_2, null, null);


(lib.Scene_1_Symbol_3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Symbol_3
	this.instance = new lib.Symbol3();
	this.instance.parent = this;
	this.instance.setTransform(621.15,240.8,0.7671,0.7671,0,0,0,0.1,-3.3);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Scene_1_Symbol_3, null, null);


(lib.Scene_1_Symbol_2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Symbol_2
	this.instance = new lib.Symbol2();
	this.instance.parent = this;
	this.instance.setTransform(899.75,261.35,0.7056,0.7056,0,0,0,27.7,15.8);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Scene_1_Symbol_2, null, null);


(lib.Scene_1_Layer_10 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_10
	this.instance = new lib.Symbol20();
	this.instance.parent = this;
	this.instance.setTransform(983.45,-19.5,1,1,0,0,0,-23.7,-12.6);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Scene_1_Layer_10, null, null);


(lib.Symbol4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_12 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("ArePLIAA+VIW9AAIAAeVg");
	mask.setTransform(377.375,57.2);

	// Layer_11_obj_
	this.Layer_11 = new lib.Symbol_4_Layer_11();
	this.Layer_11.name = "Layer_11";
	this.Layer_11.parent = this;
	this.Layer_11.setTransform(355.3,55.3,1,1,0,0,0,355.3,55.3);
	this.Layer_11.depth = 0;
	this.Layer_11.isAttachedToCamera = 0
	this.Layer_11.isAttachedToMask = 0
	this.Layer_11.layerDepth = 0
	this.Layer_11.layerIndex = 0
	this.Layer_11.maskLayerName = 0

	var maskedShapeInstanceList = [this.Layer_11];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.Layer_11).wait(1));

	// Layer_9 (mask)
	var mask_1 = new cjs.Shape();
	mask_1._off = true;
	mask_1.graphics.p("A1NCKIGftVISmA0IAAktIRWiYMgR8Ai5g");
	mask_1.setTransform(140.475,82.525);

	// Layer_8_obj_
	this.Layer_8 = new lib.Symbol_4_Layer_8();
	this.Layer_8.name = "Layer_8";
	this.Layer_8.parent = this;
	this.Layer_8.setTransform(133.8,45.6,1,1,0,0,0,133.8,45.6);
	this.Layer_8.depth = 0;
	this.Layer_8.isAttachedToCamera = 0
	this.Layer_8.isAttachedToMask = 0
	this.Layer_8.layerDepth = 0
	this.Layer_8.layerIndex = 1
	this.Layer_8.maskLayerName = 0

	var maskedShapeInstanceList = [this.Layer_8];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask_1;
	}

	this.timeline.addTween(cjs.Tween.get(this.Layer_8).wait(1));

	// Layer_6 (mask)
	var mask_2 = new cjs.Shape();
	mask_2._off = true;
	mask_2.graphics.p("AqUQuMAAAghbIUpAAMAAAAhbg");
	mask_2.setTransform(507.1,-17.275);

	// Layer_5_obj_
	this.Layer_5 = new lib.Symbol_4_Layer_5();
	this.Layer_5.name = "Layer_5";
	this.Layer_5.parent = this;
	this.Layer_5.setTransform(466.1,-27.1,1,1,0,0,0,466.1,-27.1);
	this.Layer_5.depth = 0;
	this.Layer_5.isAttachedToCamera = 0
	this.Layer_5.isAttachedToMask = 0
	this.Layer_5.layerDepth = 0
	this.Layer_5.layerIndex = 2
	this.Layer_5.maskLayerName = 0

	var maskedShapeInstanceList = [this.Layer_5];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask_2;
	}

	this.timeline.addTween(cjs.Tween.get(this.Layer_5).wait(1));

	// Layer_3 (mask)
	var mask_3 = new cjs.Shape();
	mask_3._off = true;
	mask_3.graphics.p("Ay0bMMAAAg2XMAlpAAAMAAAA2Xg");
	mask_3.setTransform(-86.65,3.025);

	// Layer_2_obj_
	this.Layer_2 = new lib.Symbol_4_Layer_2();
	this.Layer_2.name = "Layer_2";
	this.Layer_2.parent = this;
	this.Layer_2.setTransform(-3.1,-11.7,1,1,0,0,0,-3.1,-11.7);
	this.Layer_2.depth = 0;
	this.Layer_2.isAttachedToCamera = 0
	this.Layer_2.isAttachedToMask = 0
	this.Layer_2.layerDepth = 0
	this.Layer_2.layerIndex = 3
	this.Layer_2.maskLayerName = 0

	var maskedShapeInstanceList = [this.Layer_2];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask_3;
	}

	this.timeline.addTween(cjs.Tween.get(this.Layer_2).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol4, new cjs.Rectangle(-58.9,-83,569.5,193.9), null);


(lib.Scene_1_Layer_8 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_8
	this.instance = new lib.Symbol4();
	this.instance.parent = this;
	this.instance.setTransform(527.25,284.8);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Scene_1_Layer_8, null, null);


// stage content:
(lib.Dec_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	this.___GetDepth___ = function(obj) {
		var depth = obj.depth;
		var cameraObj = this.___camera___instance;
		if(cameraObj && cameraObj.depth && obj.isAttachedToCamera)
		{
			depth += depth + cameraObj.depth;
		}
		return depth;
		}
	this.___needSorting___ = function() {
		for (var i = 0; i < this.getNumChildren() - 1; i++)
		{
			var prevDepth = this.___GetDepth___(this.getChildAt(i));
			var nextDepth = this.___GetDepth___(this.getChildAt(i + 1));
			if (prevDepth < nextDepth)
				return true;
		}
		return false;
	}
	this.___sortFunction___ = function(obj1, obj2) {
		return (this.exportRoot.___GetDepth___(obj2) - this.exportRoot.___GetDepth___(obj1));
	}
	this.on('tick', function (event){
		var curTimeline = event.currentTarget;
		if (curTimeline.___needSorting___()){
			this.sortChildren(curTimeline.___sortFunction___);
		}
	});

	// Layer_10_obj_
	this.Layer_10 = new lib.Scene_1_Layer_10();
	this.Layer_10.name = "Layer_10";
	this.Layer_10.parent = this;
	this.Layer_10.setTransform(983.5,-19.4,1,1,0,0,0,983.5,-19.4);
	this.Layer_10.depth = 0;
	this.Layer_10.isAttachedToCamera = 0
	this.Layer_10.isAttachedToMask = 0
	this.Layer_10.layerDepth = 0
	this.Layer_10.layerIndex = 0
	this.Layer_10.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_10).wait(1));

	// Layer_8_obj_
	this.Layer_8 = new lib.Scene_1_Layer_8();
	this.Layer_8.name = "Layer_8";
	this.Layer_8.parent = this;
	this.Layer_8.setTransform(710.3,296.4,1,1,0,0,0,710.3,296.4);
	this.Layer_8.depth = 0;
	this.Layer_8.isAttachedToCamera = 0
	this.Layer_8.isAttachedToMask = 0
	this.Layer_8.layerDepth = 0
	this.Layer_8.layerIndex = 1
	this.Layer_8.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_8).wait(1));

	// Layer_9_obj_
	this.Layer_9 = new lib.Scene_1_Layer_9();
	this.Layer_9.name = "Layer_9";
	this.Layer_9.parent = this;
	this.Layer_9.setTransform(767.8,242.1,1,1,0,0,0,767.8,242.1);
	this.Layer_9.depth = 0;
	this.Layer_9.isAttachedToCamera = 0
	this.Layer_9.isAttachedToMask = 0
	this.Layer_9.layerDepth = 0
	this.Layer_9.layerIndex = 2
	this.Layer_9.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_9).wait(1));

	// Symbol_3_obj_
	this.Symbol_3 = new lib.Scene_1_Symbol_3();
	this.Symbol_3.name = "Symbol_3";
	this.Symbol_3.parent = this;
	this.Symbol_3.setTransform(616.5,246.1,1,1,0,0,0,616.5,246.1);
	this.Symbol_3.depth = 0;
	this.Symbol_3.isAttachedToCamera = 0
	this.Symbol_3.isAttachedToMask = 0
	this.Symbol_3.layerDepth = 0
	this.Symbol_3.layerIndex = 3
	this.Symbol_3.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Symbol_3).wait(1));

	// Symbol_2_obj_
	this.Symbol_2 = new lib.Scene_1_Symbol_2();
	this.Symbol_2.name = "Symbol_2";
	this.Symbol_2.parent = this;
	this.Symbol_2.setTransform(902.1,242.2,1,1,0,0,0,902.1,242.2);
	this.Symbol_2.depth = 0;
	this.Symbol_2.isAttachedToCamera = 0
	this.Symbol_2.isAttachedToMask = 0
	this.Symbol_2.layerDepth = 0
	this.Symbol_2.layerIndex = 4
	this.Symbol_2.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Symbol_2).wait(1));

	// Layer_4_obj_
	this.Layer_4 = new lib.Scene_1_Layer_4();
	this.Layer_4.name = "Layer_4";
	this.Layer_4.parent = this;
	this.Layer_4.setTransform(767.8,321.2,1,1,0,0,0,767.8,321.2);
	this.Layer_4.depth = 0;
	this.Layer_4.isAttachedToCamera = 0
	this.Layer_4.isAttachedToMask = 0
	this.Layer_4.layerDepth = 0
	this.Layer_4.layerIndex = 5
	this.Layer_4.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_4).wait(1));

	// Layer_5_obj_
	this.Layer_5 = new lib.Scene_1_Layer_5();
	this.Layer_5.name = "Layer_5";
	this.Layer_5.parent = this;
	this.Layer_5.setTransform(900.6,317.9,1,1,0,0,0,900.6,317.9);
	this.Layer_5.depth = 0;
	this.Layer_5.isAttachedToCamera = 0
	this.Layer_5.isAttachedToMask = 0
	this.Layer_5.layerDepth = 0
	this.Layer_5.layerIndex = 6
	this.Layer_5.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_5).wait(1));

	// Layer_3_obj_
	this.Layer_3 = new lib.Scene_1_Layer_3();
	this.Layer_3.name = "Layer_3";
	this.Layer_3.parent = this;
	this.Layer_3.setTransform(628.8,317.9,1,1,0,0,0,628.8,317.9);
	this.Layer_3.depth = 0;
	this.Layer_3.isAttachedToCamera = 0
	this.Layer_3.isAttachedToMask = 0
	this.Layer_3.layerDepth = 0
	this.Layer_3.layerIndex = 7
	this.Layer_3.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_3).wait(1));

	// Layer_2_obj_
	this.Layer_2 = new lib.Scene_1_Layer_2();
	this.Layer_2.name = "Layer_2";
	this.Layer_2.parent = this;
	this.Layer_2.setTransform(540,340,1,1,0,0,0,540,340);
	this.Layer_2.depth = 0;
	this.Layer_2.isAttachedToCamera = 0
	this.Layer_2.isAttachedToMask = 0
	this.Layer_2.layerDepth = 0
	this.Layer_2.layerIndex = 8
	this.Layer_2.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_2).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(540,331.7,560.5,348.3);
// library properties:
lib.properties = {
	id: '5BA6846239A2F64F9C18B7672E3B98F8',
	width: 1080,
	height: 680,
	fps: 24,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: [
		{src:"assets/images/bg59.png", id:"bg"},
		{src:"assets/images/pack159.png", id:"pack1"},
		{src:"assets/images/pack259.png", id:"pack2"},
		{src:"assets/images/pack359.png", id:"pack3"},
		{src:"assets/images/rumyanec59.png", id:"rumyanec"}
	],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['5BA6846239A2F64F9C18B7672E3B98F8'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}


// Layer depth API : 

AdobeAn.Layer = new function() {
	this.getLayerZDepth = function(timeline, layerName)
	{
		if(layerName === "Camera")
		layerName = "___camera___instance";
		var script = "if(timeline." + layerName + ") timeline." + layerName + ".depth; else 0;";
		return eval(script);
	}
	this.setLayerZDepth = function(timeline, layerName, zDepth)
	{
		const MAX_zDepth = 10000;
		const MIN_zDepth = -5000;
		if(zDepth > MAX_zDepth)
			zDepth = MAX_zDepth;
		else if(zDepth < MIN_zDepth)
			zDepth = MIN_zDepth;
		if(layerName === "Camera")
		layerName = "___camera___instance";
		var script = "if(timeline." + layerName + ") timeline." + layerName + ".depth = " + zDepth + ";";
		eval(script);
	}
	this.removeLayer = function(timeline, layerName)
	{
		if(layerName === "Camera")
		layerName = "___camera___instance";
		var script = "if(timeline." + layerName + ") timeline.removeChild(timeline." + layerName + ");";
		eval(script);
	}
	this.addNewLayer = function(timeline, layerName, zDepth)
	{
		if(layerName === "Camera")
		layerName = "___camera___instance";
		zDepth = typeof zDepth !== 'undefined' ? zDepth : 0;
		var layer = new createjs.MovieClip();
		layer.name = layerName;
		layer.depth = zDepth;
		layer.layerIndex = 0;
		timeline.addChild(layer);
	}
}


})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;

$(function () {
  var canvas, stage, exportRoot, anim_container, dom_overlay_container, fnStartAnimation;
  function init() {
    canvas = document.getElementById("canvas59");
    anim_container = document.getElementById("animation_container59");
    dom_overlay_container = document.getElementById("dom_overlay_container59");
    var comp=AdobeAn.getComposition("5BA6846239A2F64F9C18B7672E3B98F8");
    var lib=comp.getLibrary();
    var loader = new createjs.LoadQueue(false);
    loader.addEventListener("fileload", function(evt){handleFileLoad(evt,comp)});
    loader.addEventListener("complete", function(evt){handleComplete(evt,comp)});
    var lib=comp.getLibrary();
    loader.loadManifest(lib.properties.manifest);
  }
  function handleFileLoad(evt, comp) {
    var images=comp.getImages();
    if (evt && (evt.item.type == "image")) { images[evt.item.id] = evt.result; }
  }
  function handleComplete(evt,comp) {
    //This function is always called, irrespective of the content. You can use the variable "stage" after it is created in token create_stage.
    var lib=comp.getLibrary();
    var ss=comp.getSpriteSheet();
    var queue = evt.target;
    var ssMetadata = lib.ssMetadata;
    for(i=0; i<ssMetadata.length; i++) {
      ss[ssMetadata[i].name] = new createjs.SpriteSheet( {"images": [queue.getResult(ssMetadata[i].name)], "frames": ssMetadata[i].frames} )
    }
    var preloaderDiv = document.getElementById("_preload_div_59");
    preloaderDiv.style.display = 'none';
    canvas.style.display = 'block';
    exportRoot = new lib.Dec_1();
    stage = new lib.Stage(canvas);
    //Registers the "tick" event listener.
    fnStartAnimation = function() {
      stage.addChild(exportRoot);
      createjs.Ticker.setFPS(lib.properties.fps);
      createjs.Ticker.addEventListener("tick", stage)
      stage.addEventListener("tick", handleTick)
      function getProjectionMatrix(container, totalDepth) {
        var focalLength = 528.25;
        var projectionCenter = { x : lib.properties.width/2, y : lib.properties.height/2 };
        var scale = (totalDepth + focalLength)/focalLength;
        var scaleMat = new createjs.Matrix2D;
        scaleMat.a = 1/scale;
        scaleMat.d = 1/scale;
        var projMat = new createjs.Matrix2D;
        projMat.tx = -projectionCenter.x;
        projMat.ty = -projectionCenter.y;
        projMat = projMat.prependMatrix(scaleMat);
        projMat.tx += projectionCenter.x;
        projMat.ty += projectionCenter.y;
        return projMat;
      }
      function handleTick(event) {
        var cameraInstance = exportRoot.___camera___instance;
        if(cameraInstance !== undefined && cameraInstance.pinToObject !== undefined)
        {
          cameraInstance.x = cameraInstance.pinToObject.x + cameraInstance.pinToObject.pinOffsetX;
          cameraInstance.y = cameraInstance.pinToObject.y + cameraInstance.pinToObject.pinOffsetY;
          if(cameraInstance.pinToObject.parent !== undefined && cameraInstance.pinToObject.parent.depth !== undefined)
            cameraInstance.depth = cameraInstance.pinToObject.parent.depth + cameraInstance.pinToObject.pinOffsetZ;
        }
        applyLayerZDepth(exportRoot);
      }
      function applyLayerZDepth(parent)
      {
        var cameraInstance = parent.___camera___instance;
        var focalLength = 528.25;
        var projectionCenter = { 'x' : 0, 'y' : 0};
        if(parent === exportRoot)
        {
          var stageCenter = { 'x' : lib.properties.width/2, 'y' : lib.properties.height/2 };
          projectionCenter.x = stageCenter.x;
          projectionCenter.y = stageCenter.y;
        }
        for(child in parent.children)
        {
          var layerObj = parent.children[child];
          if(layerObj == cameraInstance)
            continue;
          applyLayerZDepth(layerObj, cameraInstance);
          if(layerObj.layerDepth === undefined)
            continue;
          if(layerObj.currentFrame != layerObj.parent.currentFrame)
          {
            layerObj.gotoAndPlay(layerObj.parent.currentFrame);
          }
          var matToApply = new createjs.Matrix2D;
          var cameraMat = new createjs.Matrix2D;
          var totalDepth = layerObj.layerDepth ? layerObj.layerDepth : 0;
          var cameraDepth = 0;
          if(cameraInstance && !layerObj.isAttachedToCamera)
          {
            var mat = cameraInstance.getMatrix();
            mat.tx -= projectionCenter.x;
            mat.ty -= projectionCenter.y;
            cameraMat = mat.invert();
            cameraMat.prependTransform(projectionCenter.x, projectionCenter.y, 1, 1, 0, 0, 0, 0, 0);
            cameraMat.appendTransform(-projectionCenter.x, -projectionCenter.y, 1, 1, 0, 0, 0, 0, 0);
            if(cameraInstance.depth)
              cameraDepth = cameraInstance.depth;
          }
          if(layerObj.depth)
          {
            totalDepth = layerObj.depth;
          }
          //Offset by camera depth
          totalDepth -= cameraDepth;
          if(totalDepth < -focalLength)
          {
            matToApply.a = 0;
            matToApply.d = 0;
          }
          else
          {
            if(layerObj.layerDepth)
            {
              var sizeLockedMat = getProjectionMatrix(parent, layerObj.layerDepth);
              if(sizeLockedMat)
              {
                sizeLockedMat.invert();
                matToApply.prependMatrix(sizeLockedMat);
              }
            }
            matToApply.prependMatrix(cameraMat);
            var projMat = getProjectionMatrix(parent, totalDepth);
            if(projMat)
            {
              matToApply.prependMatrix(projMat);
            }
          }
          layerObj.transformMatrix = matToApply;
        }
      }
    }
    //Code to support hidpi screens and responsive scaling.
    function makeResponsive(isResp, respDim, isScale, scaleType) {
      var lastW, lastH, lastS=1;
      window.addEventListener('resize', resizeCanvas);
      resizeCanvas();
      function resizeCanvas() {
        var w = lib.properties.width, h = lib.properties.height;
        var iw = window.innerWidth, ih=window.innerHeight;
        var pRatio = window.devicePixelRatio || 1, xRatio=iw/w, yRatio=ih/h, sRatio=1;
        if(isResp) {
          if((respDim=='width'&&lastW==iw) || (respDim=='height'&&lastH==ih)) {
            sRatio = lastS;
          }
          else if(!isScale) {
            if(iw<w || ih<h)
              sRatio = Math.min(xRatio, yRatio);
          }
          else if(scaleType==1) {
            sRatio = Math.min(xRatio, yRatio);
          }
          else if(scaleType==2) {
            sRatio = Math.max(xRatio, yRatio);
          }
        }
        canvas.width = w*pRatio*sRatio;
        canvas.height = h*pRatio*sRatio;
        canvas.style.width = anim_container.style.width = dom_overlay_container.style.width = preloaderDiv.style.width = w*sRatio+'px';
        canvas.style.height = anim_container.style.height = dom_overlay_container.style.height = preloaderDiv.style.height = h*sRatio+'px';
        stage.scaleX = pRatio*sRatio;
        stage.scaleY = pRatio*sRatio;
        lastW = iw; lastH = ih; lastS = sRatio;
        stage.tickOnUpdate = false;
        stage.update();
        stage.tickOnUpdate = true;
      }
    }
    makeResponsive(true,'both',true,2);
    AdobeAn.compositionLoaded(lib.properties.id);
    fnStartAnimation();
  }
  init();
});