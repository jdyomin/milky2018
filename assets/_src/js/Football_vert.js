(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [];


// symbols:



(lib.ball = function() {
	this.initialize(img.ball);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,78,78);


(lib.bg1 = function() {
	this.initialize(img.bg1);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,1080,680);


(lib.footbolist = function() {
	this.initialize(img.footbolist);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,347,493);


(lib.leg1 = function() {
	this.initialize(img.leg1);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,58,46);


(lib.pack = function() {
	this.initialize(img.pack);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,220,350);// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.Symbol19 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Symbol 18
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#150F06").ss(5.5,1,1).p("AgeAaQASg8ArAK");
	this.shape.setTransform(9.7,2.7);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#150F06").ss(5.5,1,1).p("AgeAaQASg9ArAM");
	this.shape_1.setTransform(9.6,2.7);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#150F06").ss(5.5,1,1).p("AgeAZQASg9ArAP");
	this.shape_2.setTransform(9.6,2.8);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#150F06").ss(5.5,1,1).p("AgeAYQASg8ArAQ");
	this.shape_3.setTransform(9.6,2.9);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#150F06").ss(5.5,1,1).p("AgdAXQASg8ApAT");
	this.shape_4.setTransform(9.6,3);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#150F06").ss(5.5,1,1).p("AgdAXQASg9ApAV");
	this.shape_5.setTransform(9.6,3);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#150F06").ss(5.5,1,1).p("AgdAWQASg8ApAX");
	this.shape_6.setTransform(9.6,3.1);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#150F06").ss(5.5,1,1).p("AgdAWQASg9ApAa");
	this.shape_7.setTransform(9.5,3.1);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#150F06").ss(5.5,1,1).p("AgdAVQASg8ApAb");
	this.shape_8.setTransform(9.5,3.2);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#150F06").ss(5.5,1,1).p("AgdAVQASg9AoAe");
	this.shape_9.setTransform(9.5,3.2);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#150F06").ss(5.5,1,1).p("AgcAUQASg8AnAg");
	this.shape_10.setTransform(9.5,3.3);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#150F06").ss(5.5,1,1).p("AgcAUQASg9AnAi");
	this.shape_11.setTransform(9.5,3.3);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#150F06").ss(5.5,1,1).p("AgcATQASg8AnAk");
	this.shape_12.setTransform(9.5,3.4);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#150F06").ss(5.5,1,1).p("AgcATQASg9AnAn");
	this.shape_13.setTransform(9.4,3.4);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#150F06").ss(5.5,1,1).p("AgcATQASg9AmAp");
	this.shape_14.setTransform(9.4,3.4);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#150F06").ss(5.5,1,1).p("AgcASQASg8AmAq");
	this.shape_15.setTransform(9.4,3.5);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#150F06").ss(5.5,1,1).p("AgbASQASg9AlAs");
	this.shape_16.setTransform(9.4,3.5);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#150F06").ss(5.5,1,1).p("AgcASQASg8AmAp");
	this.shape_17.setTransform(9.4,3.5);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#150F06").ss(5.5,1,1).p("AgcATQASg8AnAm");
	this.shape_18.setTransform(9.4,3.4);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#150F06").ss(5.5,1,1).p("AgcAUQASg9AnAj");
	this.shape_19.setTransform(9.5,3.3);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#150F06").ss(5.5,1,1).p("AgcAUQASg8AnAf");
	this.shape_20.setTransform(9.5,3.3);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#150F06").ss(5.5,1,1).p("AgdAWQASg8ApAY");
	this.shape_21.setTransform(9.6,3.1);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#150F06").ss(5.5,1,1).p("AgdAXQASg8ApAU");
	this.shape_22.setTransform(9.6,3);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#150F06").ss(5.5,1,1).p("AgeAYQASg8ArAR");
	this.shape_23.setTransform(9.6,2.9);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#150F06").ss(5.5,1,1).p("AgeAZQASg8ArAN");
	this.shape_24.setTransform(9.6,2.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape}]},1).wait(1));

	// Symbol 17
	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f().s("#150F06").ss(5.5,1,1).p("AgjAJQAjhPAkBh");
	this.shape_25.setTransform(7,7.1);

	this.timeline.addTween(cjs.Tween.get(this.shape_25).wait(27));

	// Symbol 16
	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f().s("#150F06").ss(5.5,1,1).p("AgcAAQAghAAZBb");
	this.shape_26.setTransform(2.9,9.5);

	this.timeline.addTween(cjs.Tween.get(this.shape_26).wait(27));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-2.7,-2.7,18.3,17.8);


(lib.Symbol15 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.ball();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol15, new cjs.Rectangle(0,0,78,78), null);


(lib.Symbol14 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 3
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#1B1815").ss(5.5,1,1).p("AgvAQQA7guAkAW");
	this.shape.setTransform(59.8,36.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#1B1815").ss(5.5,1,1).p("AgwAOQA/gmAhAP");
	this.shape_1.setTransform(59.5,41.9);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#1B1815").ss(5.5,1,1).p("AgwAMQBBgfAgAJ");
	this.shape_2.setTransform(59.3,46.4);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#1B1815").ss(5.5,1,1).p("AgwALQBDgZAeAF");
	this.shape_3.setTransform(59,50.3);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#1B1815").ss(5.5,1,1).p("AgxALQBGgVAdAB");
	this.shape_4.setTransform(58.8,53.5);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#1B1815").ss(5.5,1,1).p("AgxAKQBHgQAcgD");
	this.shape_5.setTransform(58.7,56.2);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#1B1815").ss(5.5,1,1).p("AgxAKQBIgNAbgG");
	this.shape_6.setTransform(58.6,58.3);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#1B1815").ss(5.5,1,1).p("AgxAKQBJgLAagI");
	this.shape_7.setTransform(58.5,59.7);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#1B1815").ss(5.5,1,1).p("AgyAKQBKgKAbgI");
	this.shape_8.setTransform(58.4,60.6);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#1B1815").ss(5.5,1,1).p("AgyAJQBKgKAbgI");
	this.shape_9.setTransform(58.5,60);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#1B1815").ss(5.5,1,1).p("AgxAKQBHgPAcgE");
	this.shape_10.setTransform(58.6,57.1);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#1B1815").ss(5.5,1,1).p("AgxALQBFgWAeAB");
	this.shape_11.setTransform(58.9,52.3);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#1B1815").ss(5.5,1,1).p("AgwANQBBghAgAL");
	this.shape_12.setTransform(59.3,45.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8,p:{y:60.6}}]},1).to({state:[{t:this.shape_8,p:{y:60.9}}]},1).to({state:[{t:this.shape_8,p:{y:60.9}}]},12).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape}]},1).wait(1));

	// Layer 2
	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#1B1815").ss(5.5,1,1).p("AkEhFQEJktEAIi");
	this.shape_13.setTransform(29,20.7);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#1B1815").ss(5.5,1,1).p("AkBhuQETjmDwIJ");
	this.shape_14.setTransform(28.9,25.2);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#1B1815").ss(5.5,1,1).p("Aj+iRQEcinDhH0");
	this.shape_15.setTransform(28.8,28.9);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#1B1815").ss(5.5,1,1).p("Aj8isQEkhyDVHh");
	this.shape_16.setTransform(28.8,31.9);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#1B1815").ss(5.5,1,1).p("Aj7jCQEshDDLHR");
	this.shape_17.setTransform(28.7,34.2);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#1B1815").ss(5.5,1,1).p("Aj5jSQExgdDCHE");
	this.shape_18.setTransform(28.7,35.9);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#1B1815").ss(5.5,1,1).p("Aj4jcQE2gBC7G6");
	this.shape_19.setTransform(28.7,37.1);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#1B1815").ss(5.5,1,1).p("Aj3jjQE5AUC2Gz");
	this.shape_20.setTransform(28.6,37.9);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#1B1815").ss(5.5,1,1).p("Aj3jnQE8AgCzGv");
	this.shape_21.setTransform(28.6,38.4);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#1B1815").ss(5.5,1,1).p("Aj3jpQE8AlCzGu");
	this.shape_22.setTransform(28.6,38.5);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#1B1815").ss(5.5,1,1).p("Aj3jkQE6AXC1Gy");
	this.shape_23.setTransform(28.6,38);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#1B1815").ss(5.5,1,1).p("Aj5jWQE0gSC/HA");
	this.shape_24.setTransform(28.7,36.5);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f().s("#1B1815").ss(5.5,1,1).p("Aj7i6QEphVDOHX");
	this.shape_25.setTransform(28.7,33.4);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f().s("#1B1815").ss(5.5,1,1).p("Aj/iKQEbi0DkH4");
	this.shape_26.setTransform(28.8,28.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_13}]}).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_22}]},12).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_13}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,68,43);


(lib.Symbol9 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#1B1815").ss(5,1,1).p("AgKgXIAVAv");
	this.shape.setTransform(1.1,2.4);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol9, new cjs.Rectangle(-2.5,-2.5,7.2,9.8), null);


(lib.Symbol8 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#1B1815").ss(5,1,1).p("AglAdIBLg5");
	this.shape.setTransform(3.8,2.9);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol8, new cjs.Rectangle(-2.5,-2.5,12.5,10.8), null);


(lib.Symbol7 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#1B1815").ss(5,1,1).p("AgaAzQAFg0Awgx");
	this.shape.setTransform(2.7,5.1);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol7, new cjs.Rectangle(-2.5,-2.5,10.3,15.2), null);


(lib.Symbol3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#F4142B").s().p("AggAgQgNgNAAgTQAAgSANgOQAOgNASAAQATAAANANQANAOAAASQAAATgNANQgNANgTAAQgSAAgOgNg");
	this.shape.setTransform(4.6,4.6);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol3, new cjs.Rectangle(0,0,9.1,9.1), null);


(lib.Символ2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой_1
	this.instance = new lib.pack();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Символ2, new cjs.Rectangle(0,0,220,350), null);


(lib.Символ1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой_1
	this.instance = new lib.footbolist();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Символ1, new cjs.Rectangle(0,0,347,493), null);


(lib.Symbol13 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Symbol 7
	this.instance = new lib.Symbol7();
	this.instance.parent = this;
	this.instance.setTransform(2.2,5.8,1,1,-3.2,0,0,2.6,5.1);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	// Symbol 8
	this.instance_1 = new lib.Symbol8();
	this.instance_1.parent = this;
	this.instance_1.setTransform(3.2,8.2,1,1,11.2,0,0,3.8,2.9);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(1));

	// Symbol 9
	this.instance_2 = new lib.Symbol9();
	this.instance_2.parent = this;
	this.instance_2.setTransform(1.7,13.5,1,1,0,0,0,1.1,2.4);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol13, new cjs.Rectangle(-4.1,-2.1,14.4,20.5), null);


(lib.Symbol11 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 6
	this.instance = new lib.Symbol19();
	this.instance.parent = this;
	this.instance.setTransform(68.2,30.6,1,1,0,0,0,0.6,9.3);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({rotation:-8.7,x:62.1,y:38.8},16,cjs.Ease.get(1)).to({rotation:0,x:68.2,y:30.6},10,cjs.Ease.get(-1)).wait(1));

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#150F06").ss(5.5,1,1).p("AkMiTQAoCKBkBRQCcCADwhZ");
	this.shape.setTransform(43.8,18.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#150F06").ss(5.5,1,1).p("AkHiXQAjCKBhBVQCWCED1hX");
	this.shape_1.setTransform(43.2,19.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#150F06").ss(5.5,1,1).p("AkCibQAeCLBeBXQCRCJD4hV");
	this.shape_2.setTransform(42.6,19.6);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#150F06").ss(5.5,1,1).p("Aj+ifQAbCMBaBaQCNCND7hT");
	this.shape_3.setTransform(42.1,20);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#150F06").ss(5.5,1,1).p("Aj6iiQAWCMBZBcQCJCRD9hR");
	this.shape_4.setTransform(41.6,20.4);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#150F06").ss(5.5,1,1).p("Aj3ilQATCMBWBfQCGCUEAhQ");
	this.shape_5.setTransform(41.2,20.8);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#150F06").ss(5.5,1,1).p("AjzioQAPCNBUBgQCCCYEDhP");
	this.shape_6.setTransform(40.8,21.2);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#150F06").ss(5.5,1,1).p("AjwirQAMCOBSBiQB/CaEFhN");
	this.shape_7.setTransform(40.5,21.5);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#150F06").ss(5.5,1,1).p("AjuitQAKCOBQBkQB8CcEHhM");
	this.shape_8.setTransform(40.1,21.8);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#150F06").ss(5.5,1,1).p("AjsivQAICOBOBlQB6CfEJhL");
	this.shape_9.setTransform(39.8,22);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#150F06").ss(5.5,1,1).p("AjpixQAFCOBNBnQB4ChEKhK");
	this.shape_10.setTransform(39.6,22.2);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#150F06").ss(5.5,1,1).p("AjoizQAECPBMBoQB2CiELhJ");
	this.shape_11.setTransform(39.4,22.4);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#150F06").ss(5.5,1,1).p("Ajmi0QACCPBLBoQB1ClEMhJ");
	this.shape_12.setTransform(39.2,22.6);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#150F06").ss(5.5,1,1).p("Ajli1QABCPBKBpQBzClENhI");
	this.shape_13.setTransform(39.1,22.7);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#150F06").ss(5.5,1,1).p("Ajli2QABCQBJBpQBzCmEOhI");
	this.shape_14.setTransform(39,22.8);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#150F06").ss(5.5,1,1).p("Ajki2QAACPBJBqQByCmEOhH");
	this.shape_15.setTransform(38.9,22.8);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#150F06").ss(5.5,1,1).p("Ajki2QAACPBJBqQByCnEOhI");
	this.shape_16.setTransform(38.9,22.8);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#150F06").ss(5.5,1,1).p("Ajli2QABCPBJBqQByCmEOhI");
	this.shape_17.setTransform(39,22.8);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#150F06").ss(5.5,1,1).p("Ajmi1QACCPBKBpQBzClENhI");
	this.shape_18.setTransform(39.1,22.7);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#150F06").ss(5.5,1,1).p("AjnizQADCPBMBnQB1CjEMhJ");
	this.shape_19.setTransform(39.4,22.5);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#150F06").ss(5.5,1,1).p("AjqixQAGCPBNBmQB5CgEJhK");
	this.shape_20.setTransform(39.7,22.2);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#150F06").ss(5.5,1,1).p("AjyipQAOCNBTBhQCBCZEDhO");
	this.shape_21.setTransform(40.6,21.3);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#150F06").ss(5.5,1,1).p("Aj3ikQATCMBWBeQCHCUD/hR");
	this.shape_22.setTransform(41.3,20.7);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#150F06").ss(5.5,1,1).p("Aj9ifQAZCLBaBbQCMCND8hT");
	this.shape_23.setTransform(42,20.1);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#150F06").ss(5.5,1,1).p("AkEiZQAgCLBfBWQCTCHD3hW");
	this.shape_24.setTransform(42.9,19.4);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(14.2,1.1,68.9,35.2);


(lib.Symbol10 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Symbol 7
	this.instance = new lib.Symbol13();
	this.instance.parent = this;
	this.instance.setTransform(36.9,19.7,1,1,43.7,0,0,-1.3,11.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({regY:11.4,rotation:-15,x:52.5,y:29.7},16,cjs.Ease.get(1)).to({regY:11.5,rotation:43.7,x:36.9,y:19.7},10,cjs.Ease.get(-1)).wait(1));

	// Layer 5
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#1B1815").ss(5.5,1,1).p("AjaB9QC4keD9Ar");
	this.shape.setTransform(18.6,30.9);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#1B1815").ss(5.5,1,1).p("AjeByQDCj8D7Ac");
	this.shape_1.setTransform(19.5,31.4);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#1B1815").ss(5.5,1,1).p("AjjBoQDOjeD5AQ");
	this.shape_2.setTransform(20.4,31.9);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#1B1815").ss(5.5,1,1).p("AjnBfQDYjBD3AE");
	this.shape_3.setTransform(21.3,32.2);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#1B1815").ss(5.5,1,1).p("AjqBYQDginD1gH");
	this.shape_4.setTransform(22,32.6);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#1B1815").ss(5.5,1,1).p("AjuBRQDpiPD0gR");
	this.shape_5.setTransform(22.7,32.8);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#1B1815").ss(5.5,1,1).p("AjxBKQDwh4Dzgb");
	this.shape_6.setTransform(23.4,33.1);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#1B1815").ss(5.5,1,1).p("Aj0BEQD3hkDygj");
	this.shape_7.setTransform(24,33.3);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#1B1815").ss(5.5,1,1).p("Aj2A/QD8hSDxgr");
	this.shape_8.setTransform(24.5,33.5);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#1B1815").ss(5.5,1,1).p("Aj4A7QEBhDDwgx");
	this.shape_9.setTransform(24.9,33.7);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#1B1815").ss(5.5,1,1).p("Aj6A3QEGg2Dvg2");
	this.shape_10.setTransform(25.3,33.8);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#1B1815").ss(5.5,1,1).p("Aj8AzQEKgpDvg8");
	this.shape_11.setTransform(25.7,33.9);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#1B1815").ss(5.5,1,1).p("Aj9AwQENggDug/");
	this.shape_12.setTransform(26,34);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#1B1815").ss(5.5,1,1).p("Aj+AuQEQgZDthC");
	this.shape_13.setTransform(26.2,34.1);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#1B1815").ss(5.5,1,1).p("Aj/AtQESgUDthF");
	this.shape_14.setTransform(26.3,34.2);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#1B1815").ss(5.5,1,1).p("Aj/AsQESgRDthG");
	this.shape_15.setTransform(26.4,34.2);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#1B1815").ss(5.5,1,1).p("AkAArQETgPDuhH");
	this.shape_16.setTransform(26.5,34.2);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#1B1815").ss(5.5,1,1).p("Aj/AsQESgSDthF");
	this.shape_17.setTransform(26.4,34.2);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#1B1815").ss(5.5,1,1).p("Aj+AvQEPgbDuhC");
	this.shape_18.setTransform(26.2,34.1);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#1B1815").ss(5.5,1,1).p("Aj8AyQEKgoDvg8");
	this.shape_19.setTransform(25.8,34);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#1B1815").ss(5.5,1,1).p("Aj6A4QEFg6Dwg1");
	this.shape_20.setTransform(25.2,33.7);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#1B1815").ss(5.5,1,1).p("AjyBIQDyhwDzgf");
	this.shape_21.setTransform(23.6,33.1);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#1B1815").ss(5.5,1,1).p("AjtBSQDniTD0gQ");
	this.shape_22.setTransform(22.6,32.8);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#1B1815").ss(5.5,1,1).p("AjnBeQDYi8D3AC");
	this.shape_23.setTransform(21.4,32.3);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#1B1815").ss(5.5,1,1).p("AjhBsQDJjqD6AV");
	this.shape_24.setTransform(20.1,31.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-6,8.9,58.3,37.3);


(lib.Symbol4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 6
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#100E0A").ss(5.5,1,1).p("AA1g4QhJAwg6A5ICeAI");
	this.shape.setTransform(44.3,2.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#100E0A").ss(5.5,1,1).p("AA3gyQhLArg6A5ICdAB");
	this.shape_1.setTransform(44.3,2.8);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#100E0A").ss(5.5,1,1).p("AA5guQhNAkg6A5ICdgG");
	this.shape_2.setTransform(44.3,3);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#100E0A").ss(5.5,1,1).p("AA7grQhPAfg6A5ICdgN");
	this.shape_3.setTransform(44.3,3.3);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#100E0A").ss(5.5,1,1).p("AA9gpQhRAZg6A6ICdgU");
	this.shape_4.setTransform(44.3,3.6);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#100E0A").ss(5.5,1,1).p("AA/gmQhTAUg6A5ICdga");
	this.shape_5.setTransform(44.2,3.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_5}]},11).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape}]},1).wait(6));

	// Layer 2
	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#100E0A").ss(5.5,1,1).p("AhSg4QBjAKBCAfQhmAvgqAZ");
	this.shape_6.setTransform(4.9,14.9);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#100E0A").ss(5.5,1,1).p("AhSgyQBkAHBBAfQhhAogxAX");
	this.shape_7.setTransform(4.9,14.6);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#100E0A").ss(5.5,1,1).p("AhTgtQBlAEBCAfQhdAig5AW");
	this.shape_8.setTransform(4.8,14.4);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#100E0A").ss(5.5,1,1).p("AhTgnQBmABBBAfQhYAbhBAU");
	this.shape_9.setTransform(4.8,14.1);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#100E0A").ss(5.5,1,1).p("AhUghQBngDBCAfQhVAWhIAR");
	this.shape_10.setTransform(4.8,13.9);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#100E0A").ss(5.5,1,1).p("AhUgbQBogGBBAfIifAf");
	this.shape_11.setTransform(4.7,13.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_6}]}).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_11}]},11).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_6}]},1).wait(6));

	// Layer 3
	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#1B1815").ss(5.5,1,1).p("AhDgCQgFAIAJAGQAPAKA1AAQAugCAJgFQAJgGAAgKQAAgKgOgFQgPgGghABQggABgIAFQgIAFAIAQ");
	this.shape_12.setTransform(29,33.3);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#1B1815").ss(5.5,1,1).p("AhDgFQgFAQAJALQAPAUA1gBQAugDAJgKQAJgLAAgTQAAgUgOgKQgPgKghACQggACgIAJQgIAJAIAg");
	this.shape_13.setTransform(29,34.1);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#1B1815").ss(5.5,1,1).p("AhDgIQgFAYAJARQAPAdA1gCQAugFAJgOQAJgQAAgcQAAgdgOgPQgPgPghADQggACgIAOQgIANAIAw");
	this.shape_14.setTransform(29,35);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#1B1815").ss(5.5,1,1).p("AhDgKQgFAfAJAWQAPAnA1gCQAugHAJgUQAJgUAAgmQAAgmgOgTQgPgUghADQggADgIATQgIARAIBA");
	this.shape_15.setTransform(29,35.8);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#1B1815").ss(5.5,1,1).p("AhDgNQgFAoAJAbQAPAwA1gDQAugIAJgYQAJgZAAgvQAAgwgOgYQgPgZghAFQggADgIAXQgIAWAIBP");
	this.shape_16.setTransform(29,36.6);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#1B1815").ss(5.5,1,1).p("AhDgPQgFAvAJAgQAPA6A1gDQAugKAJgdQAJgeAAg5QAAg5gOgdQgPgdghAFQggAFgIAbQgIAaAIBf");
	this.shape_17.setTransform(29,37.5);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#1B1815").ss(5.5,1,1).p("AhDgQQgFAxAJAhQAPA8A1gEQAugJAJgfQAJgfAAg6QAAg8gOgdQgPgeghAFQggAFgIAcQgIAbAIBi");
	this.shape_18.setTransform(29,37);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#1B1815").ss(5.5,1,1).p("AhDgQQgFAyAJAiQAPA+A1gEQAugKAJgfQAJggAAg8QAAg+gOgeQgPgfghAFQggAFgIAdQgIAcAIBl");
	this.shape_19.setTransform(29,36.5);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#1B1815").ss(5.5,1,1).p("AhDgQQgFAyAJAhQAPA8A1gDQAugKAJgfQAJgfAAg7QAAg8gOgeQgPgeghAFQggAFgIAcQgIAcAIBi");
	this.shape_20.setTransform(29,36.8);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#1B1815").ss(5.5,1,1).p("AhDgQQgFAxAJAgQAPA7A1gDQAugKAJgeQAJgeAAg6QAAg7gOgdQgPgeghAFQggAFgIAcQgIAbAIBg");
	this.shape_21.setTransform(29,37.2);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#1B1815").ss(5.5,1,1).p("AhDgQQgFAyAJAhQAPA9A1gEQAugKAJgfQAJgfAAg7QAAg9gOgdQgPgfghAFQggAFgIAdQgIAbAIBj");
	this.shape_22.setTransform(29,36.8);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#1B1815").ss(5.5,1,1).p("AhDgPQgFAwAJAgQAPA7A1gDQAugKAJgeQAJgeAAg6QAAg6gOgdQgPgeghAFQggAFgIAcQgIAaAIBg");
	this.shape_23.setTransform(29,37.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_12}]}).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_12}]},1).wait(6));

	// Symbol 3
	this.instance = new lib.Symbol3();
	this.instance.parent = this;
	this.instance.setTransform(28.5,33.2,1,1,0,0,0,4.5,4.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({y:43.2},5).to({y:44.4},2).to({y:43.2},3).to({y:44.4},2).to({y:43.2},4).to({y:33.2},5).wait(6));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-6.1,-5.6,61.1,43.8);


(lib.Symbol1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 3
	this.instance = new lib.Symbol15();
	this.instance.parent = this;
	this.instance.setTransform(39.3,35.8,0.56,0.56,0,0,0,39,39);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol1, new cjs.Rectangle(17.4,13.9,43.7,43.7), null);


(lib.Symbol2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.Symbol1();
	this.instance.parent = this;
	this.instance.setTransform(36,36,1,1,0,0,0,36,36);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1).to({regX:39.2,regY:35.7,rotation:15.4,x:39.2,y:17.4},0).wait(1).to({rotation:25.1,x:39,y:5.8},0).wait(1).to({rotation:31.5,x:38.9,y:-1.9},0).wait(1).to({rotation:36,x:38.8,y:-7.4},0).wait(1).to({rotation:39.4,x:38.7,y:-11.4},0).wait(1).to({rotation:41.9,x:38.6,y:-14.4},0).wait(1).to({rotation:43.9,x:38.5,y:-16.8},0).wait(1).to({rotation:45.4,y:-18.7},0).wait(1).to({rotation:46.6,x:38.4,y:-20.1},0).wait(1).to({rotation:47.6,y:-21.2},0).wait(1).to({rotation:48.3,y:-22.1},0).wait(1).to({rotation:48.9,x:38.3,y:-22.9},0).wait(1).to({rotation:49.3,y:-23.4},0).wait(1).to({rotation:49.7,y:-23.7},0).wait(1).to({rotation:49.9,y:-24},0).wait(1).to({regX:36,regY:36,rotation:50,x:36,y:-26.4},0).wait(1).to({regX:39.2,regY:35.7,rotation:49.6,x:38.3,y:-23.6},0).wait(1).to({rotation:48.7,x:38.4,y:-22.6},0).wait(1).to({rotation:47.4,y:-21},0).wait(1).to({rotation:45.5,x:38.5,y:-18.7},0).wait(1).to({rotation:42.9,x:38.6,y:-15.6},0).wait(1).to({rotation:39.6,y:-11.6},0).wait(1).to({rotation:35,x:38.8,y:-6.1},0).wait(1).to({rotation:28.8,x:39,y:1.3},0).wait(1).to({rotation:19.5,x:39.1,y:12.4},0).wait(1).to({regX:36.1,regY:36,rotation:0,x:36.1,y:36},0).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(17.4,13.9,43.7,43.7);


// stage content:
(lib.Football_vert = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Symbol 4
	this.instance = new lib.Symbol4();
	this.instance.parent = this;
	this.instance.setTransform(451,388.6,1.082,1.082,0,0,0,25.7,24.7);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	// Symbol 11
	this.instance_1 = new lib.Symbol11();
	this.instance_1.parent = this;
	this.instance_1.setTransform(428.8,437.5,1.082,1.082,0,0,0,30.5,13.2);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(1));

	// leg1.png
	this.instance_2 = new lib.leg1();
	this.instance_2.parent = this;
	this.instance_2.setTransform(377,509,1.082,1.082);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(1));

	// Symbol 2
	this.instance_3 = new lib.Symbol2();
	this.instance_3.parent = this;
	this.instance_3.setTransform(535.4,466.5,1.082,1.082,0,0,0,35.9,36.1);

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(1));

	// Слой_5
	this.instance_4 = new lib.Символ2();
	this.instance_4.parent = this;
	this.instance_4.setTransform(453.5,438.4,0.602,0.602,0,0,0,110,175.1);

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(1));

	// Symbol 10
	this.instance_5 = new lib.Symbol10();
	this.instance_5.parent = this;
	this.instance_5.setTransform(512.3,390.5,1.082,1.082,0,0,0,25.9,18.8);

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(1));

	// Symbol 14
	this.instance_6 = new lib.Symbol14();
	this.instance_6.parent = this;
	this.instance_6.setTransform(541.5,508.4,1.082,1.082,0,0,0,33.9,21.6);

	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(1));

	// Слой_8
	this.instance_7 = new lib.Символ1();
	this.instance_7.parent = this;
	this.instance_7.setTransform(611.6,458.6,0.602,0.602,0,0,0,173.6,246.6);

	this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(1));

	// Слой_9
	this.instance_8 = new lib.bg1();
	this.instance_8.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance_8).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(540,340,1080,680);
// library properties:
lib.properties = {
	id: '7CCDB8E1F980074890DAF24DEDBAD95A',
	width: 1080,
	height: 680,
	fps: 30,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: [
		{src:"assets/images/ball16-2.png", id:"ball"},
		{src:"assets/images/bg116-2.jpg", id:"bg1"},
		{src:"assets/images/footbolist16-2.png", id:"footbolist"},
		{src:"assets/images/leg116-2.png", id:"leg1"},
		{src:"assets/images/pack16-2.png", id:"pack"}
	],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['7CCDB8E1F980074890DAF24DEDBAD95A'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}



})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;

$(function () {
    var canvas, stage, exportRoot, anim_container, dom_overlay_container, fnStartAnimation;
    function init() {
        canvas = document.getElementById("canvas16-2");
        anim_container = document.getElementById("animation_container16-2");
        dom_overlay_container = document.getElementById("dom_overlay_container16-2");
        var comp=AdobeAn.getComposition("7CCDB8E1F980074890DAF24DEDBAD95A");
        var lib=comp.getLibrary();
        var loader = new createjs.LoadQueue(false);
        loader.addEventListener("fileload", function(evt){handleFileLoad(evt,comp)});
        loader.addEventListener("complete", function(evt){handleComplete(evt,comp)});
        var lib=comp.getLibrary();
        loader.loadManifest(lib.properties.manifest);
    }
    function handleFileLoad(evt, comp) {
        var images=comp.getImages();
        if (evt && (evt.item.type == "image")) { images[evt.item.id] = evt.result; }
    }
    function handleComplete(evt,comp) {
        //This function is always called, irrespective of the content. You can use the variable "stage" after it is created in token create_stage.
        var lib=comp.getLibrary();
        var ss=comp.getSpriteSheet();
        var queue = evt.target;
        var ssMetadata = lib.ssMetadata;
        for(i=0; i<ssMetadata.length; i++) {
            ss[ssMetadata[i].name] = new createjs.SpriteSheet( {"images": [queue.getResult(ssMetadata[i].name)], "frames": ssMetadata[i].frames} )
        }
        exportRoot = new lib.Football_vert();
        stage = new lib.Stage(canvas);
        //Registers the "tick" event listener.
        fnStartAnimation = function() {
            stage.addChild(exportRoot);
            createjs.Ticker.setFPS(lib.properties.fps);
            createjs.Ticker.addEventListener("tick", stage);
        }
        //Code to support hidpi screens and responsive scaling.
        function makeResponsive(isResp, respDim, isScale, scaleType) {
            var lastW, lastH, lastS=1;
            window.addEventListener('resize', resizeCanvas);
            resizeCanvas();
            function resizeCanvas() {
                var w = lib.properties.width, h = lib.properties.height;
                var iw = window.innerWidth, ih=window.innerHeight;
                var pRatio = window.devicePixelRatio || 1, xRatio=iw/w, yRatio=ih/h, sRatio=1;
                if(isResp) {
                    if((respDim=='width'&&lastW==iw) || (respDim=='height'&&lastH==ih)) {
                        sRatio = lastS;
                    }
                    else if(!isScale) {
                        if(iw<w || ih<h)
                            sRatio = Math.min(xRatio, yRatio);
                    }
                    else if(scaleType==1) {
                        sRatio = Math.min(xRatio, yRatio);
                    }
                    else if(scaleType==2) {
                        sRatio = Math.max(xRatio, yRatio);
                    }
                }
                canvas.width = w*pRatio*sRatio;
                canvas.height = h*pRatio*sRatio;
                canvas.style.width = dom_overlay_container.style.width = anim_container.style.width =  w*sRatio+'px';
                canvas.style.height = anim_container.style.height = dom_overlay_container.style.height = h*sRatio+'px';
                stage.scaleX = pRatio*sRatio;
                stage.scaleY = pRatio*sRatio;
                lastW = iw; lastH = ih; lastS = sRatio;
                stage.tickOnUpdate = false;
                stage.update();
                stage.tickOnUpdate = true;
            }
        }
        makeResponsive(true,'both',true,2);
        AdobeAn.compositionLoaded(lib.properties.id);
        fnStartAnimation();
    }
    init();
});