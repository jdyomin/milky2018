(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [];


// symbols:



(lib.BG = function() {
	this.initialize(img.BG);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,1440,680);


(lib.D = function() {
	this.initialize(img.D);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,28,80);


(lib.pack1 = function() {
	this.initialize(img.pack1);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,213,484);


(lib.pack2 = function() {
	this.initialize(img.pack2);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,327,578);


(lib.pack3 = function() {
	this.initialize(img.pack3);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,96,613);


(lib.pack_part1 = function() {
	this.initialize(img.pack_part1);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,25,56);


(lib.pack_part2 = function() {
	this.initialize(img.pack_part2);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,53,102);


(lib.pack_part2_shadow = function() {
	this.initialize(img.pack_part2_shadow);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,40,90);


(lib.plashka1 = function() {
	this.initialize(img.plashka1);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,304,102);


(lib.plashka2 = function() {
	this.initialize(img.plashka2);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,547,102);


(lib.plashka3 = function() {
	this.initialize(img.plashka3);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,306,102);// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.Symbol17 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.plashka2();
	this.instance.parent = this;
	this.instance.setTransform(-273.5,-51);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol17, new cjs.Rectangle(-273.5,-51,547,102), null);


(lib.Symbol16 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.plashka1();
	this.instance.parent = this;
	this.instance.setTransform(-152,-51);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol16, new cjs.Rectangle(-152,-51,304,102), null);


(lib.Symbol15 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.plashka3();
	this.instance.parent = this;
	this.instance.setTransform(-153,-51);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol15, new cjs.Rectangle(-153,-51,306,102), null);


(lib.Symbol7 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.pack3();
	this.instance.parent = this;
	this.instance.setTransform(-48,-306.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol7, new cjs.Rectangle(-48,-306.5,96,613), null);


(lib.Symbol6 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.pack2();
	this.instance.parent = this;
	this.instance.setTransform(-163.5,-289);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol6, new cjs.Rectangle(-163.5,-289,327,578), null);


(lib.Symbol5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.pack_part2_shadow();
	this.instance.parent = this;
	this.instance.setTransform(-20,-45);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol5, new cjs.Rectangle(-20,-45,40,90), null);


(lib.Symbol4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.pack_part2();
	this.instance.parent = this;
	this.instance.setTransform(-26.5,-51);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol4, new cjs.Rectangle(-26.5,-51,53,102), null);


(lib.Symbol3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("AjMDaICzn5IDmAAIgOHRIibBug");
	mask.setTransform(-3.975,-0.975);

	// Layer_1
	this.instance = new lib.pack_part1();
	this.instance.parent = this;
	this.instance.setTransform(-12.5,-28);

	var maskedShapeInstanceList = [this.instance];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol3, new cjs.Rectangle(-12.5,-28,25,55.9), null);


(lib.Symbol2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.pack1();
	this.instance.parent = this;
	this.instance.setTransform(-106.5,-242);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol2, new cjs.Rectangle(-106.5,-242,213,484), null);


(lib.Symbol_25_Layer_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#3DACE5").s().p("Ah8FOQgPgXAGhbQACgdAJg2IAPhWQACgWAch1IAdiEQAMg/AIgXQASgwAXARQALAHANBHIAgDWQAVCEAUBVQAgB+gIAZQgpAZgQgkIgWhcIgiifIAAAAIAAAAQgJACgSAAQgZAAgEABQgQA1gJBSQgQCKgEAAQgOAGgKAAQgNAAgHgJgAgNhSQgEAQgKA5QAdAAAFgDQAAhZgNgGIAAAAQgBAAgGAZg");
	this.shape.setTransform(133.5764,3.243);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#3DACE5").s().p("AgNFVQgegOAEh3QABgRgCgaIAAgoQAAjHAMi+QgCALg8gPQgmgLADgdQABghBdgDQATgCBnAFQAaAFAHAgQAIAegPAAQgMAFgigFQgdgDAAAHQgMBOABCjQACDEgEBCIAAAnQABAbgCAOQgFAhgUAAQgHAAgJgFg");
	this.shape_1.setTransform(106.3813,2.8677);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#3DACE5").s().p("AAJFaQg3gBgcgtQgUgigJhxIgIikQgEjKAPgwQAWhJA+gLQAkgFAkAYQAXANAOAvQALAdgYAPQgXAPgIgRQgTgqgWgGQgegKgKAxQgFAVgCA7QgEDUANB1QAOB9AkgHQAhgGADgTQAFgUAVgGQAMgEAKAMQAJAJAAAHQABAHgTAeQgcAqg4AAIgCAAg");
	this.shape_2.setTransform(79.6371,3.408);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#3DACE5").s().p("AhCEnQgQjAgKjZQgBgjgJhYQgGhDAfgQQAFgCBpgSQAagEANALQAMAMgKATQgNAVgmABQgtACgQAMQAAAlAIBNQAHBNgBAnQAwAJgFAZQgCAQgiAMQADAkAGBeQAFBTAFAtQA7gQAmATQAJAGgHASQgGAUgKAAQg9AHg+AEQgcAAAAgvg");
	this.shape_3.setTransform(53.0755,3.2828);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#3DACE5").s().p("AA8FRQgBgCAAhNQAAkHALinQgHAEgjA/QgOAXgGAFQgEACgGgCQgcgFgVhHQgGgOgEgEIgBA0IgBAqQAABHgFBhIgBBbQABA7gCAaQgEBGgOAGQgUAJgIgQQgFgOgCh7IgCguQgBgeABgSQAFhNACh2IADjuQAFgZAlAGQAKAAAOArIAKAiIAHAXQAHAOADAPQAUAsACADQgDgEBNiNQAKgSACgBQAZgOAOAVQADACgDBJQgIDygBFKQAAAYgaAFQgFABgFAAQgMAAgIgMg");
	this.shape_4.setTransform(25.275,2.446);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#3DACE5").s().p("AhQEpQgTgegCgVQgEggACgUQACgkAAhIQAChVACgjIAAh1QAAhyASggQAVglAegGQBLgNAnA4QARAbAABAIABCRQABBDgEA8IgFA7QgEAnAAAaQgEBRgTAfQgYAmghADIgNAAQg0AAgbgugAgKkYQgdALgFAsQgDAPADBLQABAyAABRIgCB8IgDA3QgCAhACARQAFAyAvACQAXAAADgwIACgoQABgYACgJQAeifgSjNQgDgigLgYQgFgIgNgGQgHgDgHAAQgFAAgGADg");
	this.shape_5.setTransform(-26.9967,2.9386);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#3DACE5").s().p("AgoFSQgOgOgIg5QgEgggZnBIgFgnQgDgXAAgMQgBgiAYgIIAcABQAPAAALgCQA1gLAVgBQAtgBADAZQAEAdg1ALQgQADgdACIgjAEQgCANAHBoQAHBpALBkQALB4ACApQAAALADAZIACAkQABAqgNAHQgPAKgLAAQgJAAgFgGg");
	this.shape_6.setTransform(-51.2116,2.8524);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#3DACE5").s().p("AhQEpQgTgegCgVQgEggACgUQACgkAAhIQAChVACgjIAAh1QAAhyASggQAVglAegGQBLgNAnA4QARAbAABAIABCRQABBDgEA8IgFA7QgEAnAAAaQgEBRgTAfQgYAmghADIgNAAQg0AAgbgugAgKkYQgdALgFAsQgDAPADBLQABAyAABRIgCB8IgDA3QgCAhACARQAFAyAvACQAXAAADgwIACgoQABgYACgJQAeifgSjNQgDgigLgYQgFgIgNgGQgHgDgHAAQgFAAgGADg");
	this.shape_7.setTransform(-78.1467,2.9386);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#3DACE5").s().p("AhSFaQgTABgChDIABg+IADg9IABlyQAAhXABgGQADgPAIgJQAMgKATAHQANAGgBCCQAACFAEAFQAAAEAngDQAjgBACgCQAFgHgEhzQgFh2AIgUQAKgZAOAAQAWAAABAKQAPFeAAEmQABAagTAKQgSAFgOgKQgJgRABgxQABiIgJiAQgHgMhCAOQgRAAAMDlQAFBQgLAVQgEAGgTAAIgMgBg");
	this.shape_8.setTransform(-104.1,2.6711);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#3DACE5").s().p("AA8FRQgBgCAAhNQAAkHALinQgHAEgjA/QgOAXgGAFQgEACgGgCQgcgFgVhHQgGgOgEgEIgBA0IgBAqQAABHgFBhIgBBbQABA7gCAaQgEBGgOAGQgUAJgIgQQgFgOgCh7IgCguQgBgeABgSQAFhNACh2IADjuQAFgZAlAGQAKAAAOArIAKAiIAHAXQAHAOADAPQAUAsACADQgDgEBNiNQAKgSACgBQAZgOAOAVQADACgDBJQgIDygBFKQAAAYgaAFQgFABgFAAQgMAAgIgMg");
	this.shape_9.setTransform(-133.125,2.446);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol_25_Layer_1, null, null);


(lib.Symbol_24_Layer_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgNFVQgegOAEh3QABgRgCgaIAAgoQAAjHAMi+QgCALg8gPQgmgLADgdQABghBdgDQATgCBnAFQAaAFAHAgQAIAegPAAQgMAFgigFQgdgDAAAHQgMBOABCjQACDEgEBCIAAAnQABAbgCAOQgFAhgUAAQgHAAgJgFg");
	this.shape.setTransform(108.0313,2.8677);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("Ag9FkQgQjAgKjZQgBgjgJhYQgGhDAfgQQAGgCBogSQAagEANALQANAMgLATQgNAVgmABQgsACgRAMQAAAlAIBNQAHBNgBAmQAxAJgGAZQgCARgiAMQAEAkAFBeQAEBTAGAtQA7gQAmATQAKAGgHASQgGAUgLAAQg8AHg/AEQgcAAAAgvgAhRkdQgZgIgDgYQgDggAVgWQAOgOATAGQAUAEAHATIAFAVQgCAZgNAPQgMAMgQAAQgGAAgGgCgAAMk5QgSgCgGgPQgQggAVgXQAKgQAMgBQAWgEAMAXQAGAJAAAWQACARgQAPQgIAHgNAAIgIAAg");
	this.shape_1.setTransform(81.2655,-2.8296);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AA8FRQgBgCAAhNQAAkHALinQgHAEgjA/QgOAXgGAFQgEACgGgCQgcgFgVhHQgGgOgEgEIgBA0IgBAqQAABHgFBhIgBBbQABA7gCAaQgEBGgOAGQgUAJgIgQQgFgOgCh7IgCguQgBgeABgSQAFhNACh2IADjuQAFgZAlAGQAKAAAOArIAKAiIAHAXQAHAOADAPQAUAsACADQgDgEBNiNQAKgSACgBQAZgOAOAVQADACgDBJQgIDygBFKQAAAYgaAFQgFABgFAAQgMAAgIgMg");
	this.shape_2.setTransform(53.975,2.446);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AhFGbQgRAAgIgPQgJgmAChsQABgrgCiQQgCh+ADg9IABiGQADgeAUAAQAeABAIAxQADATgCBJIgDE3IANgwQA1iSABhgQAAgNAIgqIAIgyQAGgpAVgHQAVgIADAWQALBFgBB7QgBCpABAlQACAqgEBKQgEBSABAbQgBA2gegIQgRAGgJgXQgIgVABgeIAQk0QgvEPgkBZQgQAWgSAAIgCAAgAgQk1IgQgNQgHgDgJgOQgDgGgEgDIgKgOQgQgUAFgRQAEgRARAJQAGANAGAGIAJAKQAGAIAIAHIAHADQANAHAMAAIAHABQAAAAABAAQAAgBAAAAQABAAAAgBQABAAABAAIAEgCQADgBAFgDIASgNQAEgBAFgKIAJgQQAagFgLAaQgBAIgCAGQgIAWgFAIIgKAKQgDAFgBAAQgEAAgEAEQgFAFgCAAQgCAAgGAGQgKAHgLAAQgMAAgQgLg");
	this.shape_3.setTransform(24.4325,-2.2896);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("Ah8FOQgPgXAGhbQACgdAJg2IAPhWQACgWAch1IAdiEQAMg/AIgXQASgwAXARQALAHANBHIAgDWQAVCEAUBVQAgB+gIAZQgpAZgQgkIgWhcIgiifIAAAAIAAAAQgJACgSAAQgZAAgEABQgQA1gJBSQgQCKgEAAQgOAGgKAAQgNAAgHgJgAgNhSQgEAQgKA5QAdAAAFgDQAAhZgNgGIAAAAQgBAAgGAZg");
	this.shape_4.setTransform(-3.3736,3.243);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AhWEzQgNgLACgWQADgdARgKQARgIAPANIAUATQAJAKAGAEQAPAJAegNQAVgKAHhEQACgkgFglQgEgxgOgKQgIgIgfAIQghAGgOgFQgQgHgKgRQgLgVAJgVQACgDAxgfQAmgcAOgrQAchOgMguQgLgjgjAAQggAAgaA4QgDAFgEAUQgFATgFAJQgNAegagQQgXgNACghIAJgaQAchvBagOQAvgHAvAzQAlAogGBcIgRBOQgCALgSAZQgPAXAAAKQAAAKAQAPQASASACAEQAvBZgUB0QgIAzgaAfQgcAcguAFIgMAAQg3AAgogng");
	this.shape_5.setTransform(-33.8723,2.2843);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AhCEnQgQjAgKjZQgBgjgJhYQgGhDAfgQQAFgCBpgSQAagEANALQAMAMgKATQgNAVgmABQgtACgQAMQAAAlAIBNQAHBNgBAnQAwAJgFAZQgCAQgiAMQADAkAGBeQAFBTAFAtQA7gQAmATQAJAGgHASQgGAUgKAAQg9AHg+AEQgcAAAAgvg");
	this.shape_6.setTransform(-84.1745,3.2828);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AhSFaQgSABgChDIABg+IACg9IAAlyQAAhXABgGQAEgPAIgJQAMgKATAHQANAGgBCCQgBCFAGAFQAAAEAmgDQAjgBACgCQAEgHgDhzQgFh2AIgUQAKgZAOAAQAWAAABAKQAOFeACEmQAAAagSAKQgTAFgOgKQgJgRAAgxQABiIgIiAQgIgMhBAOQgQAAALDlQAFBQgLAVQgEAGgTAAIgMgBg");
	this.shape_7.setTransform(-109.3,2.6711);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol_24_Layer_1, null, null);


(lib.Symbol_23_Layer_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#3DACE5").s().p("Ah8FOQgPgXAGhbQACgdAJg2IAPhWQACgWAch1IAdiEQAMg/AIgXQASgwAXARQALAHANBHIAgDWQAVCEAUBVQAgB+gIAZQgpAZgQgkIgWhcIgiifIAAAAIAAAAQgJACgSAAQgZAAgEABQgQA1gJBSQgQCKgEAAQgOAGgKAAQgNAAgHgJgAgNhSQgEAQgKA5QAdAAAFgDQAAhZgNgGIAAAAQgBAAgGAZg");
	this.shape.setTransform(117.3764,3.243);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#3DACE5").s().p("AhSFaQgTABgBhDIAAg+IACg9IABlyQAAhXABgGQAEgPAJgJQAMgKARAHQAOAGAACCQgCCFAFAFQABAEAmgDQAjgBACgCQAEgHgDhzQgFh2AIgUQAKgZAOAAQAXAAgBAKQAPFeACEmQgBAagRAKQgTAFgPgKQgHgRgBgxQACiIgJiAQgIgMhCAOQgPAAAMDlQAEBQgLAVQgEAGgTAAIgMgBg");
	this.shape_1.setTransform(90.8,2.6711);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#3DACE5").s().p("AhQEpQgTgegCgVQgEggACgUQACgkAAhIQAChVACgjIAAh1QAAhyASggQAVglAegGQBLgNAnA4QARAbAABAIABCRQABBDgEA8IgFA7QgEAnAAAaQgEBRgTAfQgYAmghADIgNAAQg0AAgbgugAgKkYQgdALgFAsQgDAPADBLQABAyAABRIgCB8IgDA3QgCAhACARQAFAyAvACQAXAAADgwIACgoQABgYACgJQAeifgSjNQgDgigLgYQgFgIgNgGQgHgDgHAAQgFAAgGADg");
	this.shape_2.setTransform(63.3533,2.9386);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#3DACE5").s().p("AgQFhQgigFgRgMQgVgPgBhOIgKjtIgChFIgChPQAAgRgFhBIgGhXQgBgjAZgHIAcABQAUAAABAzQABA0ACAZIAFCYQBigqAkByQAUBugKBYQgRB/g4AWQgOAHgVAAIgTgBgAgcgWQgEACAAADQABAaABA/IAECMQADBPALAFQAQAEAUgWQARgVADgSQAShkgShoQgJhAgjAAQgMAAgQAHg");
	this.shape_3.setTransform(13.9036,2.1448);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#3DACE5").s().p("AhfFoQgSgEgDgpIAAokQAAh4AKgCQARgKBJAHQBNAHAeAVQATAOAGA/QAGBFgqAsQgOAPg2AGQgvAEAAAFQARAAggFdQgBALAAAfQAAAfgCANQgFAlgXAAQgGAAgIgCgAg5kpQgGAGAKBrQAAALAwgCQAwgBAOgQQAKgLABgkQACgkgKgMQgNgSg1gCIgMAAQgqAAADAKg");
	this.shape_4.setTransform(-13.2932,1.7993);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#3DACE5").s().p("AhCEnQgQjAgKjZQgBgjgJhYQgGhDAfgQQAFgCBpgSQAagEANALQAMAMgKATQgNAVgmABQgtACgQAMQAAAlAIBNQAHBNgBAnQAwAJgFAZQgCAQgiAMQADAkAGBeQAFBTAFAtQA7gQAmATQAJAGgHASQgGAUgKAAQg9AHg+AEQgcAAAAgvg");
	this.shape_5.setTransform(-40.7745,3.2828);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#3DACE5").s().p("AhUFTQgQgCABg1IADgvIACgvQAAjagFhAIgCg9QgDgxABgfQABhkAbAAIA+gGQA4gEAgABQAeAAgIBNIgBAkQAABMAECaIAEDsQADBKgUAUQgNAPgWgLQgLgGAAgRQADhHgGiQQgHiUAChCQAFh6gBgiIgLABQg+gCACAJQgGBSAGCsQAHCrgHBWQgFBhgfAAQgHAAgHgEg");
	this.shape_6.setTransform(-66.6004,3.3765);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#3DACE5").s().p("AhCEnQgQjAgKjZQgBgjgJhYQgGhDAfgQQAFgCBpgSQAagEANALQAMAMgKATQgNAVgmABQgtACgQAMQAAAlAIBNQAHBNgBAnQAwAJgFAZQgCAQgiAMQADAkAGBeQAFBTAFAtQA7gQAmATQAJAGgHASQgGAUgKAAQg9AHg+AEQgcAAAAgvg");
	this.shape_7.setTransform(-92.8745,3.2828);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#3DACE5").s().p("AgNFVQgegOAEh3QABgRgCgaIAAgoQAAjHAMi+QgCALg8gPQgmgLADgdQABghBdgDQATgCBnAFQAaAFAHAgQAIAegPAAQgMAFgigFQgdgDAAAHQgMBOABCjQACDEgEBCIAAAnQABAbgCAOQgFAhgUAAQgHAAgJgFg");
	this.shape_8.setTransform(-118.6187,2.8677);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol_23_Layer_1, null, null);


(lib.Symbol_22_Layer_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#3DACE5").s().p("AhfFoQgSgEgDgpIAAokQAAh4AKgCQARgKBJAHQBNAHAeAVQATAOAGA/QAGBFgqAsQgOAPg2AGQgvAEAAAFQARAAggFdIgBAqQAAAfgCANQgFAlgXAAQgGAAgIgCgAg5kpQgGAGAKBrQAAALAwgCQAwgBAOgQQAKgLABgkQACgkgKgMQgNgSg1gCIgMAAQgqAAADAKg");
	this.shape.setTransform(272.9568,1.7993);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#3DACE5").s().p("AhCEnQgQjAgKjZQgBgjgJhYQgGhDAfgQQAFgCBpgSQAagEANALQAMAMgKATQgNAVgmABQgtACgQAMQAAAlAIBNQAHBNgBAnQAwAJgFAZQgCAQgiAMQADAkAGBeQAFBTAFAtQA7gQAmATQAJAGgHASQgGAUgKAAQg9AHg+AEQgcAAAAgvg");
	this.shape_1.setTransform(245.4755,3.2828);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#3DACE5").s().p("AhSFaQgTABgChDIABg+IADg9IABlyQAAhXABgGQADgPAIgJQAMgKATAHQANAGAACCQgBCFAEAFQAAAEAngDQAjgBACgCQAFgHgEhzQgFh2AIgUQAKgZAOAAQAWAAABAKQAPFeAAEmQABAagTAKQgSAFgOgKQgJgRABgxQABiIgJiAQgHgMhCAOQgRAAAMDlQAFBQgLAVQgEAGgTAAIgMgBg");
	this.shape_2.setTransform(220.35,2.6711);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#3DACE5").s().p("AhSFaQgSABgChDIABg+IACg9IABlyQgBhXABgGQAEgPAIgJQAMgKATAHQANAGgBCCQgBCFAGAFQAAAEAmgDQAjgBACgCQAEgHgDhzQgFh2AIgUQAKgZAOAAQAWAAABAKQAOFeACEmQAAAagSAKQgTAFgOgKQgJgRAAgxQABiIgIiAQgIgMhBAOQgQAAALDlQAFBQgLAVQgEAGgTAAIgMgBg");
	this.shape_3.setTransform(194,2.6711);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#3DACE5").s().p("Ah8FOQgPgXAGhbQACgdAJg2IAPhWQACgWAch1IAdiEQAMg/AIgXQASgwAXARQALAHANBHIAgDWQAVCEAUBVQAgB+gIAZQgpAZgQgkIgWhcIgiifIAAAAIAAAAQgJACgSAAQgZAAgEABQgQA1gJBSQgQCKgEAAQgOAGgKAAQgNAAgHgJgAgNhSQgEAQgKA5QAdAAAFgDQAAhZgNgGIAAAAQgBAAgGAZg");
	this.shape_4.setTransform(166.2264,3.243);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#3DACE5").s().p("AgRFnQghgEgRgNQgVgPgChNIgJjuIgChFIgBhOQAAgSgGhBIgGhWQgBgjAYgHIAcAAQAOAAAMgBQA1gMAVgBQAtgCADAaQAEAeg1AKIgsAGIgjADQgBAGACATIAEAsIAFCZQBigqAkByQAUBtgJBYQgSB/g3AXQgOAHgWAAIgUgCgAgcgPQgFABAAAEQACAZABA/IAECMQADBQALAEQAQAFAUgXQAQgUAEgTQAShkgShoQgJhAgjAAQgMAAgQAIg");
	this.shape_5.setTransform(138.6651,1.4626);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#3DACE5").s().p("AgNFVQgegOAEh3QABgRgCgaIAAgoQAAjHAMi+QgCALg8gPQgmgLADgdQABghBdgDQATgCBnAFQAaAFAHAgQAIAegPAAQgMAFgigFQgdgDAAAHQgMBOABCjQACDEgEBCIAAAnQABAbgCAOQgFAhgUAAQgHAAgJgFg");
	this.shape_6.setTransform(88.7313,2.8677);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#3DACE5").s().p("AhQEpQgTgegCgVQgEggACgUQACgkAAhIQAChVACgjIAAh1QAAhyASggQAVglAegGQBLgNAnA4QARAbAABAIABCRQABBDgEA8IgFA7QgEAnAAAaQgEBRgTAfQgYAmghADIgNAAQg0AAgbgugAgKkYQgdALgFAsQgDAPADBLQABAyAABRIgCB8IgDA3QgCAhACARQAFAyAvACQAXAAADgwIACgoQABgYACgJQAeifgSjNQgDgigLgYQgFgIgNgGQgHgDgHAAQgFAAgGADg");
	this.shape_7.setTransform(60.4533,2.9386);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#3DACE5").s().p("AgNFVQgegOAEh3QABgRgCgaIAAgoQAAjHAMi+QgCALg8gPQgmgLADgdQABghBdgDQATgCBnAFQAaAFAHAgQAIAegPAAQgMAFgigFQgdgDAAAHQgMBOABCjQACDEgEBCIAAAnQABAbgCAOQgFAhgUAAQgHAAgJgFg");
	this.shape_8.setTransform(33.8813,2.8677);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#3DACE5").s().p("AhDFHQgXgNgNgvQgKgeAYgPQAXgOAIARQARAqAWAHQAfAIALgxQAGgVADg7QADg9ABg1QgGADgJAAQgTABglAAIgIAAQgFgCgFAAQgJAAgOgWIAAgLIABgJQAGgWARAAQAKAAAPADIATABIAugFQAAhZgFg+QgJh+gmAIQggAGgFASQgFAUgVAHQgLAEgKgMQgJgLAAgFQgBgIAUgdQAfgrA4ABQA4AAAZAtQAUAjAGBwIACCkQgCDLgQAvQgYBKg/AKIgOABQgdAAgbgTg");
	this.shape_9.setTransform(4.8476,3.4329);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#3DACE5").s().p("Ah3FOQgEhfAMjTQANjWgDhVQgCgfAFgNQAEgSAOgIQARgJAQAOQACAaABAyIADBPQASgOAbg+QAcg/ATgQQAJgIAPgCQASgCAEAMQAGASgbAxQhHCXgZAuQAtBQBFC7QAiBUgIAeQgDAOgRAFQgRAFgKgIQgTgLgJgiQgjiAg5iNQgKA8AAEDQAAAQgfABIgEAAQgbAAAAgNg");
	this.shape_10.setTransform(-44.7057,3.314);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#3DACE5").s().p("Ah8FOQgPgXAGhbQACgdAJg2IAPhWQACgWAch1IAdiEQAMg/AIgXQASgwAXARQALAHANBHIAgDWQAVCEAUBVQAgB+gIAZQgpAZgQgkIgWhcIgiifIAAAAIAAAAQgJACgSAAQgZAAgEABQgQA1gJBSQgQCKgEAAQgOAGgKAAQgNAAgHgJgAgNhSQgEAQgKA5QAdAAAFgDQAAhZgNgGIAAAAQgBAAgGAZg");
	this.shape_11.setTransform(-74.3736,3.243);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#3DACE5").s().p("Ah3FOQgEhfAMjTQANjWgDhVQgCgfAFgNQAEgSAOgIQARgJAQAOQACAaABAyIADBPQASgOAbg+QAcg/ATgQQAJgIAPgCQASgCAEAMQAGASgbAxQhHCXgZAuQAtBQBFC7QAiBUgIAeQgDAOgRAFQgRAFgKgIQgTgLgJgiQgjiAg5iNQgKA8AAEDQAAAQgfABIgEAAQgbAAAAgNg");
	this.shape_12.setTransform(-100.9057,3.314);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#3DACE5").s().p("AhQEpQgTgegCgVQgEggACgUQACgkAAhIQAChVACgjIAAh1QAAhyASggQAVglAegGQBLgNAnA4QARAbAABAIABCRQABBDgEA8IgFA7QgEAnAAAaQgEBRgTAfQgYAmghADIgNAAQg0AAgbgugAgKkYQgdALgFAsQgDAPADBLQABAyAABRIgCB8IgDA3QgCAhACARQAFAyAvACQAXAAADgwIACgoQABgYACgJQAeifgSjNQgDgigLgYQgFgIgNgGQgHgDgHAAQgFAAgGADg");
	this.shape_13.setTransform(-152.5467,2.9386);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#3DACE5").s().p("AA8FRQgBgCAAhNQAAkHALinQgHAEgjA/QgOAXgGAFQgEACgGgCQgcgFgVhHQgGgOgEgEIgBA0IgBAqQAABHgFBhIgBBbQABA7gCAaQgEBGgOAGQgUAJgIgQQgFgOgCh7IgCguQgBgeABgSQAFhNACh2IADjuQAFgZAlAGQAKAAAOArIAKAiIAHAXQAHAOADAPQAUAsACADQgDgEBNiNQAKgSACgBQAZgOAOAVQADACgDBJQgIDygBFKQAAAYgaAFQgFABgFAAQgMAAgIgMg");
	this.shape_14.setTransform(-181.175,2.446);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#3DACE5").s().p("ABZFVQgcgTAAg6IgDibQgChmABg3QghgFgZAlQgqA3AAB4QgLCmACgDQgLAYgTgDQgRgDgJgSQgBgCAAhHQgDiFANhCQAHgpAzhMIg0grQgfgegGgvQgHg0AcgsQAcguBdgNQBXgLAMASQASgEgBBeIgCBTIgCBMQAAA2AGCUQAECEgCBOQgLAVgOAAQgJAAgJgGgAALkTQg5AOgNARQgKAMAEAkQAFAiAKALQAOAQAxADQAvAFAAgLQAOiEgKgHQACgGgLAAQgNAAgfAIg");
	this.shape_15.setTransform(-214.2064,2.7799);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#3DACE5").s().p("AhfFoQgSgEgDgpIAAokQAAh4AKgCQARgKBJAHQBNAHAeAVQATAOAGA/QAGBFgqAsQgOAPg2AGQgvAEAAAFQARAAggFdIgBAqQAAAfgCANQgFAlgXAAQgGAAgIgCgAg5kpQgGAGAKBrQAAALAwgCQAwgBAOgQQAKgLABgkQACgkgKgMQgNgSg1gCIgMAAQgqAAADAKg");
	this.shape_16.setTransform(-242.9932,1.7993);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#3DACE5").s().p("AhUFTQgQgCACg1IACgvIACgvQAAjagEhAIgDg9QgDgxABgfQABhkAbAAIA+gGQA4gEAgABQAeAAgIBNIgBAkQAABMAECaIAEDsQACBKgTAUQgMAPgXgLQgLgGAAgRQADhHgGiQQgHiUAChCQAFh6gBgiIgLABQg+gCACAJQgGBSAGCsQAHCrgHBWQgFBhgfAAQgHAAgHgEg");
	this.shape_17.setTransform(-272.1004,3.3765);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol_22_Layer_1, null, null);


(lib.Symbol_21_Layer_2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.instance = new lib.D();
	this.instance.parent = this;
	this.instance.setTransform(-89.05,-31.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol_21_Layer_2, null, null);


(lib.Symbol_21_Layer_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AiCAeQgGgCgEAAQgLAAgPgQIAAgIIAAgGQAHgRAUAAQAKAAARACIAVABIB6gIQAmgBAHgBQAZgDAZAAQARgBAPALQAKAFgBAMQAAAGgFAFQgNAKgMAAQg+AAgOgBQgZgCgnAGQgGACgMACQgCAAgIgEQgHAHgUAAIg/ABg");
	this.shape.setTransform(229.05,10.2957);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AhCEnQgQjAgKjZQgBgjgJhYQgGhDAfgQQAFgCBpgSQAagEANALQAMAMgKATQgNAVgmABQgtACgQAMQAAAlAIBNQAHBNgBAnQAwAJgFAZQgCAQgiAMQADAkAGBeQAFBTAFAtQA7gQAmATQAJAGgHASQgGAUgKAAQg9AHg+AEQgcAAAAgvg");
	this.shape_1.setTransform(176.1255,3.2828);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AhCEnQgQjAgKjZQgBgjgJhYQgGhDAfgQQAFgCBpgSQAagEANALQAMAMgKATQgNAVgmABQgtACgQAMQAAAlAIBNQAHBNgBAnQAwAJgFAZQgCAQgiAMQADAkAGBeQAFBTAFAtQA7gQAmATQAJAGgHASQgGAUgKAAQg9AHg+AEQgcAAAAgvg");
	this.shape_2.setTransform(151.9255,3.2828);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AhCEnQgQjAgKjZQgBgjgJhYQgGhDAfgQQAFgCBpgSQAagEANALQAMAMgKATQgNAVgmABQgtACgQAMQAAAlAIBNQAHBNgBAnQAwAJgFAZQgCAQgiAMQADAkAGBeQAFBTAFAtQA7gQAmATQAJAGgHASQgGAUgKAAQg9AHg+AEQgcAAAAgvg");
	this.shape_3.setTransform(105.4255,3.2828);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AgNFVQgegOAEh3QABgRgCgaIAAgoQAAjHAMi+QgCALg8gPQgmgLADgdQABghBdgDQATgCBnAFQAaAFAHAgQAIAegPAAQgMAFgigFQgdgDAAAHQgMBOABCjQACDEgEBCIAAAnQABAbgCAOQgFAhgUAAQgHAAgJgFg");
	this.shape_4.setTransform(79.6813,2.8677);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AhFGbQgRAAgIgPQgJgmAChsQABgrgCiQQgCh+ADg9IABiGQADgeAUAAQAeABAIAxQADATgCBJIgDE3IANgwQA1iSABhgQAAgNAIgqIAIgyQAGgpAVgHQAVgIADAWQALBFgBB7QgBCpABAlQACAqgEBKQgEBSABAbQgBA2gegIQgRAGgJgXQgIgVABgeIAQk0QgvEPgkBZQgQAWgSAAIgCAAgAgQk1IgQgNQgHgDgJgOQgDgGgEgDIgKgOQgQgUAFgRQAEgRARAJQAGANAGAGIAJAKQAGAIAIAHIAHADQANAHAMAAIAHABQAAAAABAAQAAgBAAAAQABAAAAgBQABAAABAAIAEgCQADgBAFgDIASgNQAEgBAFgKIAJgQQAagFgLAaQgBAIgCAGQgIAWgFAIIgKAKQgDAFgBAAQgEAAgEAEQgFAFgCAAQgCAAgGAGQgKAHgLAAQgMAAgQgLg");
	this.shape_5.setTransform(51.8325,-2.2896);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("Ah8FOQgPgXAGhbQACgdAJg2IAPhWQACgWAch1IAdiEQAMg/AIgXQASgwAXARQALAHANBHIAgDWQAVCEAUBVQAgB+gIAZQgpAZgQgkIgWhcIgiifIAAAAIAAAAQgJACgSAAQgZAAgEABQgQA1gJBSQgQCKgEAAQgOAGgKAAQgNAAgHgJgAgNhSQgEAQgKA5QAdAAAFgDQAAhZgNgGIAAAAQgBAAgGAZg");
	this.shape_6.setTransform(24.0264,3.243);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AgTFhQgggFgRgMQgUgPgDhOIgJjtIgChFIgBhPQAAgRgGhBQgEgyADgbQAFgrA/gIQA8gIApAjQAeAcAJAVQAWAwgRAzQgQAvgqAdQAiA6ALAuQAeBrgJBYQgSB/g3AWQgPAHgWAAIgUgBgAgjhDIAECLIAECMQADBPALAFQAQAEATgWQARgVAEgSQAShkgShoQgUhlg1gHQgFACAAAEgAgqktQgBAbAEAyIAEBQQApADAZgeQAZgdgIgsQgFgdgagRQgSgMgXAAQgJAAgJABg");
	this.shape_7.setTransform(-3.9591,2.1029);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AhCFlQghgFgRgMQgUgPgChOIgKjtIgChFIgBhPQAAgRgGhBIgGhXQgBgjAZgHIAcABQAUAAABAzIADBNIAFCYQBigqAlByQATBugJBYQgSB/g3AWQgOAHgVAAIgVgBgAhNgSQgEACAAADQACAaABA/IADCMQADBPALAFQARAEAUgWQAQgVAEgSQARhkgRhoQgJhAgkAAQgMAAgQAHgAB3FcQgVgWgChPIgEj3QgDiigEhQIgCglQgCgWABgLQACghAWgKQAZgMAIAzIANDAQAGBFgBCcQgBCYAHBLQABAQgLAHQgJAFgHAAQgLAAgHgIg");
	this.shape_8.setTransform(-38.9978,1.7305);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("Ah8FOQgPgXAGhbQACgdAJg2IAPhWQACgWAch1IAdiEQAMg/AIgXQASgwAXARQALAHANBHIAgDWQAVCEAUBVQAgB+gIAZQgpAZgQgkIgWhcIgiifIAAAAIAAAAQgJACgSAAQgZAAgEABQgQA1gJBSQgQCKgEAAQgOAGgKAAQgNAAgHgJgAgNhSQgEAQgKA5QAdAAAFgDQAAhZgNgGIAAAAQgBAAgGAZg");
	this.shape_9.setTransform(-103.4736,3.243);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("AA7EFQgIhnAFiqQgDisgLhSQAAgHgugEIAAA4IAEDDIAGDBQACBQgLAmQgPA0goAFIgMgBQgRgCgPgHQgYgIAHgdQAGgdAiAIIAKABQAPgLAAhrQABgkgDg0IgDg/QABgigChIIgDhxIABhGQgkgGgDgIQgLgtAfAAQAggDA5AGIBIAGQAbAAgDBkQACAfABAwIABA9QgBBBAOE3QAFA1gPADQgJAFgHAAQgeAAgGhTg");
	this.shape_10.setTransform(-132.2568,2.6659);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("Ah3FOQgEhfAMjTQANjWgDhVQgCgfAFgNQAEgSAOgIQARgJAQAOQACAaABAyIADBPQASgOAbg+QAcg/ATgQQAJgIAPgCQASgCAEAMQAGASgbAxQhHCXgZAuQAtBQBFC7QAiBUgIAeQgDAOgRAFQgRAFgKgIQgTgLgJgiQgjiAg5iNQgKA8AAEDQAAAQgfABIgEAAQgbAAAAgNg");
	this.shape_11.setTransform(-157.7557,3.314);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("AAJFaQg3gBgcgtQgUgigJhxIgIikQgEjKAPgwQAWhJA+gLQAkgFAkAYQAXANAOAvQALAdgYAPQgXAPgIgRQgTgqgWgGQgegKgKAxQgFAVgCA7QgEDUANB1QAOB9AkgHQAhgGADgTQAFgUAVgGQAMgEAKAMQAJAJAAAHQABAHgTAeQgcAqg4AAIgCAAg");
	this.shape_12.setTransform(-185.5629,3.408);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFFFFF").s().p("AhFFeQgRAAgIgPQgJgmAChsQABgrgCiQQgCh+ADg9IABiGQADgeAUAAQAeABAIAxQADATgCBJIgDE3IANgwQA1iSABhgQAAgNAIgqIAIgyQAGgpAVgHQAVgIADAWQALBFgBB7QgBCpABAlQACAqgEBKQgEBSABAbQgBA2gegIQgRAGgJgXQgIgVABgeIAQkzQgvEOgkBZQgQAWgSAAIgCAAg");
	this.shape_13.setTransform(-236.2175,3.8271);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol_21_Layer_1, null, null);


(lib.Symbol_20_Layer_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#3DACE5").s().p("AgsFOQgLgFARh0QAKg6AUhxQAUh2AIg0Ihlh7QgcgmgBgIQAAgRARgHQAQgHAQAKQARAJAgApQAgAsAOAJQADgKAEgnQAEglAGgQQAKgQAOgGQAQgHAQANQALAGgWCJIgUB3IgQBpQgGAqgOBMIgUB2QgNBFgFADIgLAAQgZAAgKgIg");
	this.shape.setTransform(175.3723,2.7765);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#3DACE5").s().p("Ah3FOQgEhfAMjTQANjWgDhVQgCgfAFgNQAEgSAOgIQARgJAQAOQACAaABAyIADBPQASgOAbg+QAcg/ATgQQAJgIAPgCQASgCAEAMQAGASgbAxQhHCXgZAuQAtBQBFC7QAiBUgIAeQgDAOgRAFQgRAFgKgIQgTgLgJgiQgjiAg5iNQgKA8AAEDQAAAQgfABIgEAAQgbAAAAgNg");
	this.shape_1.setTransform(150.4943,3.314);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#3DACE5").s().p("AgTFhQgggFgRgMQgUgPgDhOIgJjtIgChFIgBhPQAAgRgGhBQgEgyADgbQAFgrA/gIQA8gIApAjQAeAcAJAVQAWAwgRAzQgQAvgqAdQAiA6ALAuQAeBrgJBYQgSB/g3AWQgPAHgWAAIgUgBgAgjhDIAECLIAECMQADBPALAFQAQAEATgWQARgVAEgSQAShkgShoQgUhlg1gHQgFACAAAEgAgqktQgBAbAEAyIAEBQQApADAZgeQAZgdgIgsQgFgdgagRQgSgMgXAAQgJAAgJABg");
	this.shape_2.setTransform(120.8409,2.1029);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#3DACE5").s().p("AhQEpQgTgegCgVQgEggACgUQACgkAAhIQAChVACgjIAAh1QAAhyASggQAVglAegGQBLgNAnA4QARAbAABAIABCRQABBDgEA8IgFA7QgEAnAAAaQgEBRgTAfQgYAmghADIgNAAQg0AAgbgugAgKkYQgdALgFAsQgDAPADBLQABAyAABRIgCB8IgDA3QgCAhACARQAFAyAvACQAXAAADgwIACgoQABgYACgJQAeifgSjNQgDgigLgYQgFgIgNgGQgHgDgHAAQgFAAgGADg");
	this.shape_3.setTransform(92.5033,2.9386);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#3DACE5").s().p("Ah3FOQgEhfAMjTQANjWgDhVQgCgfAFgNQAEgSAOgIQARgJAQAOQACAaABAyIADBPQASgOAbg+QAcg/ATgQQAJgIAPgCQASgCAEAMQAGASgbAxQhHCXgZAuQAtBQBFC7QAiBUgIAeQgDAOgRAFQgRAFgKgIQgTgLgJgiQgjiAg5iNQgKA8AAEDQAAAQgfABIgEAAQgbAAAAgNg");
	this.shape_4.setTransform(66.5943,3.314);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#3DACE5").s().p("Ah8FOQgPgXAGhbQACgdAJg2IAPhWQACgWAch1IAdiEQAMg/AIgXQASgwAXARQALAHANBHIAgDWQAVCEAUBVQAgB+gIAZQgpAZgQgkIgWhcIgiifIAAAAIAAAAQgJACgSAAQgZAAgEABQgQA1gJBSQgQCKgEAAQgOAGgKAAQgNAAgHgJgAgNhSQgEAQgKA5QAdAAAFgDQAAhZgNgGIAAAAQgBAAgGAZg");
	this.shape_5.setTransform(36.9264,3.243);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#3DACE5").s().p("AhUFTQgQgCACg1IACgvIACgvQAAjagEhAIgDg9QgDgxABgfQABhkAbAAIA+gGQA3gEAhABQAeAAgJBNIAAAkQAABMAECaIAEDsQADBKgUAUQgMAPgXgLQgLgGAAgRQADhHgGiQQgHiUAChCQAFh6gBgiIgLABQg+gCACAJQgHBSAHCsQAGCrgGBWQgFBhgfAAQgHAAgHgEg");
	this.shape_6.setTransform(9.6496,3.3765);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#3DACE5").s().p("AgsFOQgLgFARh0QAKg6AUhxQAUh2AIg0Ihlh7QgcgmgBgIQAAgRARgHQAQgHAQAKQARAJAgApQAgAsAOAJQADgKAEgnQAEglAGgQQAKgQAOgGQAQgHAQANQALAGgWCJIgUB3IgQBpQgGAqgOBMIgUB2QgNBFgFADIgLAAQgZAAgKgIg");
	this.shape_7.setTransform(-17.4777,2.7765);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#3DACE5").s().p("AhCEnQgQjAgKjZQgBgjgJhYQgGhDAfgQQAFgCBpgSQAagEANALQAMAMgKATQgNAVgmABQgtACgQAMQAAAlAIBNQAHBNgBAnQAwAJgFAZQgCAQgiAMQADAkAGBeQAFBTAFAtQA7gQAmATQAJAGgHASQgGAUgKAAQg9AHg+AEQgcAAAAgvg");
	this.shape_8.setTransform(-63.7745,3.2828);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#3DACE5").s().p("AgNFVQgegOAEh3QABgRgCgaIAAgoQAAjHAMi+QgCALg8gPQgmgLADgdQABghBdgDQATgCBnAFQAaAFAHAgQAIAegPAAQgMAFgigFQgdgDAAAHQgMBOABCjQACDEgEBCIAAAnQABAbgCAOQgFAhgUAAQgHAAgJgFg");
	this.shape_9.setTransform(-89.5187,2.8677);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#3DACE5").s().p("AhFGbQgRAAgIgPQgJgmAChsQABgrgCiQQgCh+ADg9IABiGQADgeAUAAQAeABAIAxQADATgCBJIgDE3IANgwQA1iSABhgQAAgNAIgqIAIgyQAGgpAVgHQAVgIADAWQALBFgBB7QgBCpABAlQACAqgEBKQgEBSABAbQgBA2gegIQgRAGgJgXQgIgVABgeIAQk0QgvEPgkBZQgQAWgSAAIgCAAgAgQk1IgQgNQgHgDgJgOQgDgGgEgDIgKgOQgQgUAFgRQAEgRARAJQAGANAGAGIAJAKQAGAIAIAHIAHADQANAHAMAAIAHABQAAAAABAAQAAgBAAAAQABAAAAgBQABAAABAAIAEgCQADgBAFgDIASgNQAEgBAFgKIAJgQQAagFgLAaQgBAIgCAGQgIAWgFAIIgKAKQgDAFgBAAQgEAAgEAEQgFAFgCAAQgCAAgGAGQgKAHgLAAQgMAAgQgLg");
	this.shape_10.setTransform(-117.3675,-2.2896);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#3DACE5").s().p("AhQEpQgTgegCgVQgEggACgUQACgkAAhIQAChVACgjIAAh1QAAhyASggQAVglAegGQBLgNAnA4QARAbAABAIABCRQABBDgEA8IgFA7QgEAnAAAaQgEBRgTAfQgYAmghADIgNAAQg0AAgbgugAgKkYQgdALgFAsQgDAPADBLQABAyAABRIgCB8IgDA3QgCAhACARQAFAyAvACQAXAAADgwIACgoQABgYACgJQAeifgSjNQgDgigLgYQgFgIgNgGQgHgDgHAAQgFAAgGADg");
	this.shape_11.setTransform(-144.8467,2.9386);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#3DACE5").s().p("AA8FRQgBgCAAhNQAAkHALinQgHAEgjA/QgOAXgGAFQgEACgGgCQgcgFgVhHQgGgOgEgEIgBA0IgBAqQAABHgFBhIgBBbQABA7gCAaQgEBGgOAGQgUAJgIgQQgFgOgCh7IgCguQgBgeABgSQAFhNACh2IADjuQAFgZAlAGQAKAAAOArIAKAiIAHAXQAHAOADAPQAUAsACADQgDgEBNiNQAKgSACgBQAZgOAOAVQADACgDBJQgIDygBFKQAAAYgaAFQgFABgFAAQgMAAgIgMg");
	this.shape_12.setTransform(-173.475,2.446);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol_20_Layer_1, null, null);


(lib.Symbol_19_Layer_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#03A2E3").s().p("AgQFhQgigFgRgMQgVgPgBhOIgKjtIgChFIgChPQAAgRgFhBIgGhXQgBgjAZgHIAcABQAUAAABAzQABA0ACAZIAFCYQBigqAkByQAUBugKBYQgRB/g4AWQgOAHgVAAIgTgBgAgcgWQgEACAAADQABAaABA/IAECMQADBPALAFQAQAEAUgWQARgVADgSQAShkgShoQgJhAgjAAQgMAAgQAHg");
	this.shape.setTransform(207.1036,2.1448);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#03A2E3").s().p("AAJFaQg3gBgcgtQgUgigJhxIgIikQgEjKAPgwQAWhJA+gLQAkgFAkAYQAXANAOAvQALAdgYAPQgXAPgIgRQgTgqgWgGQgegKgKAxQgFAVgCA7QgEDUANB1QAOB9AkgHQAhgGADgTQAFgUAVgGQAMgEAKAMQAJAJAAAHQABAHgTAeQgcAqg4AAIgCAAg");
	this.shape_1.setTransform(179.6871,3.408);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#03A2E3").s().p("AhCEnQgQjAgKjZQgBgjgJhYQgGhDAfgQQAFgCBpgSQAagEANALQAMAMgKATQgNAVgmABQgtACgQAMQAAAlAIBNQAHBNgBAnQAwAJgFAZQgCAQgiAMQADAkAGBeQAFBTAFAtQA7gQAmATQAJAGgHASQgGAUgKAAQg9AHg+AEQgcAAAAgvg");
	this.shape_2.setTransform(153.1255,3.2828);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#03A2E3").s().p("AgNFVQgegOAEh3QABgRgCgaIAAgoQAAjHAMi+QgCALg8gPQgmgLADgdQABghBdgDQATgCBnAFQAaAFAHAgQAIAegPAAQgMAFgigFQgdgDAAAHQgMBOABCjQACDEgEBCIAAAnQABAbgCAOQgFAhgUAAQgHAAgJgFg");
	this.shape_3.setTransform(127.3813,2.8677);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#03A2E3").s().p("AhFGbQgRAAgIgPQgJgmAChsQABgrgCiQQgCh+ADg9IABiGQADgeAUAAQAeABAIAxQADATgCBJIgDE3IANgwQA1iSABhgQAAgNAIgqIAIgyQAGgpAVgHQAVgIADAWQALBFgBB7QgBCpABAlQACAqgEBKQgEBSABAbQgBA2gegIQgRAGgJgXQgIgVABgeIAQk0QgvEPgkBZQgQAWgSAAIgCAAgAgQk1IgQgNQgHgDgJgOQgDgGgEgDIgKgOQgQgUAFgRQAEgRARAJQAGANAGAGIAJAKQAGAIAIAHIAHADQANAHAMAAIAHABQAAAAABAAQAAgBAAAAQABAAAAgBQABAAABAAIAEgCQADgBAFgDIASgNQAEgBAFgKIAJgQQAagFgLAaQgBAIgCAGQgIAWgFAIIgKAKQgDAFgBAAQgEAAgEAEQgFAFgCAAQgCAAgGAGQgKAHgLAAQgMAAgQgLg");
	this.shape_4.setTransform(99.5325,-2.2896);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#03A2E3").s().p("AgsFOQgLgFARh0QAKg6AUhxQAUh2AIg0Ihlh7QgcgmgBgIQAAgRARgHQAQgHAQAKQARAJAgApQAgAsAOAJQADgKAEgnQAEglAGgQQAKgQAOgGQAQgHAQANQALAGgWCJIgUB3IgQBpQgGAqgOBMIgUB2QgNBFgFADIgLAAQgZAAgKgIg");
	this.shape_5.setTransform(73.2223,2.7765);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#03A2E3").s().p("AhfFoQgSgEgDgpIAAokQAAh4AKgCQARgKBJAHQBNAHAeAVQATAOAGA/QAGBFgqAsQgOAPg2AGQgvAEAAAFQARAAggFdIgBAqQAAAfgCANQgFAlgXAAQgGAAgIgCgAg5kpQgGAGAKBrQAAALAwgCQAwgBAOgQQAKgLABgkQACgkgKgMQgNgSg1gCIgMAAQgqAAADAKg");
	this.shape_6.setTransform(48.9568,1.7993);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#03A2E3").s().p("AhFFeQgRAAgIgPQgJgmAChsQABgrgCiQQgCh+ADg9IABiGQADgeAUAAQAeABAIAxQADATgCBJIgDE3IANgwQA1iSABhgQAAgNAIgqIAIgyQAGgpAVgHQAVgIADAWQALBFgBB7QgBCpABAlQACAqgEBKQgEBSABAbQgBA2gegIQgRAGgJgXQgIgVABgeIAQkzQgvEOgkBZQgQAWgSAAIgCAAg");
	this.shape_7.setTransform(19.6825,3.8271);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#03A2E3").s().p("AA7EFQgIhnAFiqQgDisgLhSQAAgHgugEIAAA4IAEDDIAGDBQACBQgLAmQgPA0goAFIgMgBQgRgCgPgHQgYgIAHgdQAGgdAiAIIAKABQAPgLAAhrQABgkgDg0IgDg/QABgigChIIgDhxIABhGQgkgGgDgIQgLgtAfAAQAggDA5AGIBIAGQAbAAgDBkQACAfABAwIABA9QgBBBAOE3QAFA1gPADQgJAFgHAAQgeAAgGhTg");
	this.shape_8.setTransform(-8.9068,2.6659);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#03A2E3").s().p("AhQEpQgTgegCgVQgEggACgUQACgkAAhIQAChVACgjIAAh1QAAhyASggQAVglAegGQBLgNAnA4QARAbAABAIABCRQABBDgEA8IgFA7QgEAnAAAaQgEBRgTAfQgYAmghADIgNAAQg0AAgbgugAgKkYQgdALgFAsQgDAPADBLQABAyAABRIgCB8IgDA3QgCAhACARQAFAyAvACQAXAAADgwIACgoQABgYACgJQAeifgSjNQgDgigLgYQgFgIgNgGQgHgDgHAAQgFAAgGADg");
	this.shape_9.setTransform(-35.5467,2.9386);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#03A2E3").s().p("AhWEzQgNgLACgWQADgdARgKQARgIAPANIAUATQAJAKAGAEQAPAJAegNQAVgKAHhEQACgkgFglQgEgxgOgKQgIgIgfAIQghAGgOgFQgQgHgKgRQgLgVAJgVQACgDAxgfQAmgcAOgrQAchOgMguQgLgjgjAAQggAAgaA4QgDAFgEAUQgFATgFAJQgNAegagQQgXgNACghIAJgaQAchvBagOQAvgHAvAzQAlAogGBcIgRBOQgCALgSAZQgPAXAAAKQAAAKAQAPQASASACAEQAvBZgUB0QgIAzgaAfQgcAcguAFIgMAAQg3AAgogng");
	this.shape_10.setTransform(-65.4223,2.2843);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#03A2E3").s().p("AhFFeQgRAAgIgPQgJgmAChsQABgrgCiQQgCh+ADg9IABiGQADgeAUAAQAeABAIAxQADATgCBJIgDE3IANgwQA1iSABhgQAAgNAIgqIAIgyQAGgpAVgHQAVgIADAWQALBFgBB7QgBCpABAlQACAqgEBKQgEBSABAbQgBA2gegIQgRAGgJgXQgIgVABgeIAQkzQgvEOgkBZQgQAWgSAAIgCAAg");
	this.shape_11.setTransform(-95.2175,3.8271);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#03A2E3").s().p("AhQEpQgTgegCgVQgEggACgUQACgkAAhIQAChVACgjIAAh1QAAhyASggQAVglAegGQBLgNAnA4QARAbAABAIABCRQABBDgEA8IgFA7QgEAnAAAaQgEBRgTAfQgYAmghADIgNAAQg0AAgbgugAgKkYQgdALgFAsQgDAPADBLQABAyAABRIgCB8IgDA3QgCAhACARQAFAyAvACQAXAAADgwIACgoQABgYACgJQAeifgSjNQgDgigLgYQgFgIgNgGQgHgDgHAAQgFAAgGADg");
	this.shape_12.setTransform(-122.6967,2.9386);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#03A2E3").s().p("AA8FRQgBgCAAhNQAAkHALinQgHAEgjA/QgOAXgGAFQgEACgGgCQgcgFgVhHQgGgOgEgEIgBA0IgBAqQAABHgFBhIgBBbQABA7gCAaQgEBGgOAGQgUAJgIgQQgFgOgCh7IgCguQgBgeABgSQAFhNACh2IADjuQAFgZAlAGQAKAAAOArIAKAiIAHAXQAHAOADAPQAUAsACADQgDgEBNiNQAKgSACgBQAZgOAOAVQADACgDBJQgIDygBFKQAAAYgaAFQgFABgFAAQgMAAgIgMg");
	this.shape_13.setTransform(-151.325,2.446);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#03A2E3").s().p("Ah8FOQgPgXAGhbQACgdAJg2IAPhWQACgWAch1IAdiEQAMg/AIgXQASgwAXARQALAHANBHIAgDWQAVCEAUBVQAgB+gIAZQgpAZgQgkIgWhcIgiifIAAAAIAAAAQgJACgSAAQgZAAgEABQgQA1gJBSQgQCKgEAAQgOAGgKAAQgNAAgHgJgAgNhSQgEAQgKA5QAdAAAFgDQAAhZgNgGIAAAAQgBAAgGAZg");
	this.shape_14.setTransform(-181.6236,3.243);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#03A2E3").s().p("AAJFaQg3gBgcgtQgUgigJhxIgIikQgEjKAPgwQAWhJA+gLQAkgFAkAYQAXANAOAvQALAdgYAPQgXAPgIgRQgTgqgWgGQgegKgKAxQgFAVgCA7QgEDUANB1QAOB9AkgHQAhgGADgTQAFgUAVgGQAMgEAKAMQAJAJAAAHQABAHgTAeQgcAqg4AAIgCAAg");
	this.shape_15.setTransform(-207.7629,3.408);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol_19_Layer_1, null, null);


(lib.Symbol_18_Layer_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AhQEpQgTgegCgVQgEggACgUQACgkAAhIQAChVACgjIAAh1QAAhyASggQAVglAegGQBLgNAnA4QARAbAABAIABCRQABBDgEA8IgFA7QgEAnAAAaQgEBRgTAfQgYAmghADIgNAAQg0AAgbgugAgKkYQgdALgFAsQgDAPADBLQABAyAABRIgCB8IgDA3QgCAhACARQAFAyAvACQAXAAADgwIACgoQABgYACgJQAeifgSjNQgDgigLgYQgFgIgNgGQgHgDgHAAQgFAAgGADg");
	this.shape.setTransform(354.8033,2.9386);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AhSFaQgTABgBhDIABg+IABg9IABlyQAAhXABgGQAEgPAJgJQAMgKARAHQAOAGAACCQgCCFAFAFQAAAEAngDQAjgBACgCQAEgHgDhzQgFh2AIgUQAKgZAOAAQAXAAgBAKQAPFeACEmQgBAagRAKQgTAFgPgKQgHgRgBgxQACiIgJiAQgIgMhCAOQgPAAAMDlQAEBQgLAVQgEAGgTAAIgMgBg");
	this.shape_1.setTransform(328.85,2.6711);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AAzFUQgJgRAAgxQACiJgJiAQgIgMhCAOQg7AFgCg+IgBimQAAhYABgGQAEgPAJgIQAMgKASAGQAOAHgCCCQgDCFAGAHQAAAIAngJQAkgCACgCQAGgGgFhzQgFh3AIgUQAKgYAOAAQAXAAgBAJQAQFeABEmQgBAbgRAJQgHACgGAAQgLAAgJgGg");
	this.shape_2.setTransform(301.95,2.6226);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AhFFeQgRAAgIgPQgJgmAChsQABgrgCiQQgCh+ADg9IABiGQADgeAUAAQAeABAIAxQADATgCBJIgDE3IANgwQA1iSABhgQAAgNAIgqIAIgyQAGgpAVgHQAVgIADAWQALBFgBB7QgBCpABAlQACAqgEBKQgEBSABAbQgBA2gegIQgRAGgJgXQgIgVABgeIAQkzQgvEOgkBZQgQAWgSAAIgCAAg");
	this.shape_3.setTransform(274.7325,3.8271);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AgoFSQgOgOgIg5QgEgggZnBIgFgnQgDgXAAgMQgBgiAYgIIAcABQAPAAALgCQA1gLAVgBQAtgBADAZQAEAdg1ALQgQADgdACIgjAEQgCANAHBoQAHBpALBkQALB4ACApQAAALADAZIACAkQABAqgNAHQgPAKgLAAQgJAAgFgGg");
	this.shape_4.setTransform(250.0884,2.8524);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AhQEpQgTgegCgVQgEggACgUQACgkAAhIQAChVACgjIAAh1QAAhyASggQAVglAegGQBLgNAnA4QARAbAABAIABCRQABBDgEA8IgFA7QgEAnAAAaQgEBRgTAfQgYAmghADIgNAAQg0AAgbgugAgKkYQgdALgFAsQgDAPADBLQABAyAABRIgCB8IgDA3QgCAhACARQAFAyAvACQAXAAADgwIACgoQABgYACgJQAeifgSjNQgDgigLgYQgFgIgNgGQgHgDgHAAQgFAAgGADg");
	this.shape_5.setTransform(223.1533,2.9386);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AA7EFQgIhnAFiqQgDisgLhSQAAgHgugEIAAA4IAEDDIAGDBQACBQgLAmQgPA0goAFIgMgBQgRgCgPgHQgYgIAHgdQAGgdAiAIIAKABQAPgLAAhrQABgkgDg0IgDg/QABgigChIIgDhxIABhGQgkgGgDgIQgLgtAfAAQAggDA5AGIBIAGQAbAAgDBkQACAfABAwIABA9QgBBBAOE3QAFA1gPADQgJAFgHAAQgeAAgGhTg");
	this.shape_6.setTransform(194.9932,2.6659);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#C3E7F9").s().p("AhQEpQgTgegCgVQgEggACgUQACgkAAhIQAChVACgjIAAh1QAAhyASggQAVglAegGQBLgNAnA4QARAbAABAIABCRQABBDgEA8IgFA7QgEAnAAAaQgEBRgTAfQgYAmghADIgNAAQg0AAgbgugAgKkYQgdALgFAsQgDAPADBLQABAyAABRIgCB8IgDA3QgCAhACARQAFAyAvACQAXAAADgwIACgoQABgYACgJQAeifgSjNQgDgigLgYQgFgIgNgGQgHgDgHAAQgFAAgGADg");
	this.shape_7.setTransform(168.3533,2.9386);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#C3E7F9").s().p("Ah3FOQgEhfAMjTQANjWgDhVQgCgfAFgNQAEgSAOgIQARgJAQAOQACAaABAyIADBPQASgOAbg+QAcg/ATgQQAJgIAPgCQASgCAEAMQAGASgbAxQhHCXgZAuQAtBQBFC7QAiBUgIAeQgDAOgRAFQgRAFgKgIQgTgLgJgiQgjiAg5iNQgKA8AAEDQAAAQgfABIgEAAQgbAAAAgNg");
	this.shape_8.setTransform(142.4443,3.314);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#C3E7F9").s().p("AhDFHQgXgNgNgvQgKgeAYgPQAXgOAIARQARAqAWAHQAfAIALgxQAGgVADg7QADg9ABg1QgGADgJAAQgTABglAAIgIAAQgFgCgFAAQgJAAgOgWIAAgLIABgJQAGgWARAAQAKAAAPADIATABIAugFQAAhZgFg+QgJh+gmAIQggAGgFASQgFAUgVAHQgLAEgKgMQgJgLAAgFQgBgIAUgdQAfgrA4ABQA4AAAZAtQAUAjAGBwIACCkQgCDLgQAvQgYBKg/AKIgOABQgdAAgbgTg");
	this.shape_9.setTransform(112.3476,3.4329);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol_18_Layer_1, null, null);


(lib.Symbol_1_Layer_16 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_16
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#E7E7E7").s().p("AwQFDIAAqFMAggAAAIAAKFg");
	this.shape.setTransform(168.6,195.7);
	this.shape._off = true;

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(141).to({_off:false},0).to({_off:true},12).wait(112));

}).prototype = p = new cjs.MovieClip();


(lib.Scene_1_Layer_5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_5
	this.instance = new lib.BG();
	this.instance.parent = this;
	this.instance.setTransform(1440,0,1,1,0,0,180);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(265));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol25 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1_obj_
	this.Layer_1 = new lib.Symbol_25_Layer_1();
	this.Layer_1.name = "Layer_1";
	this.Layer_1.parent = this;
	this.Layer_1.setTransform(0.7,2.8,1,1,0,0,0,0.7,2.8);
	this.Layer_1.depth = 0;
	this.Layer_1.isAttachedToCamera = 0
	this.Layer_1.isAttachedToMask = 0
	this.Layer_1.layerDepth = 0
	this.Layer_1.layerIndex = 0
	this.Layer_1.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_1).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol25, new cjs.Rectangle(-150.6,-58.1,301.2,116.2), null);


(lib.Symbol24 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1_obj_
	this.Layer_1 = new lib.Symbol_24_Layer_1();
	this.Layer_1.name = "Layer_1";
	this.Layer_1.parent = this;
	this.Layer_1.setTransform(0.5,-2.3,1,1,0,0,0,0.5,-2.3);
	this.Layer_1.depth = 0;
	this.Layer_1.isAttachedToCamera = 0
	this.Layer_1.isAttachedToMask = 0
	this.Layer_1.layerDepth = 0
	this.Layer_1.layerIndex = 0
	this.Layer_1.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_1).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol24, new cjs.Rectangle(-124.2,-58.1,248.5,116.2), null);


(lib.Symbol23 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1_obj_
	this.Layer_1 = new lib.Symbol_23_Layer_1();
	this.Layer_1.name = "Layer_1";
	this.Layer_1.parent = this;
	this.Layer_1.setTransform(-0.1,1.8,1,1,0,0,0,-0.1,1.8);
	this.Layer_1.depth = 0;
	this.Layer_1.isAttachedToCamera = 0
	this.Layer_1.isAttachedToMask = 0
	this.Layer_1.layerDepth = 0
	this.Layer_1.layerIndex = 0
	this.Layer_1.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_1).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol23, new cjs.Rectangle(-134.4,-58.1,268.8,116.2), null);


(lib.Symbol22 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1_obj_
	this.Layer_1 = new lib.Symbol_22_Layer_1();
	this.Layer_1.name = "Layer_1";
	this.Layer_1.parent = this;
	this.Layer_1.setTransform(1.1,1.7,1,1,0,0,0,1.1,1.7);
	this.Layer_1.depth = 0;
	this.Layer_1.isAttachedToCamera = 0
	this.Layer_1.isAttachedToMask = 0
	this.Layer_1.layerDepth = 0
	this.Layer_1.layerIndex = 0
	this.Layer_1.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_1).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol22, new cjs.Rectangle(-287.9,-58.1,575.8,116.2), null);


(lib.Symbol21 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2_obj_
	this.Layer_2 = new lib.Symbol_21_Layer_2();
	this.Layer_2.name = "Layer_2";
	this.Layer_2.parent = this;
	this.Layer_2.setTransform(-75,8.5,1,1,0,0,0,-75,8.5);
	this.Layer_2.depth = 0;
	this.Layer_2.isAttachedToCamera = 0
	this.Layer_2.isAttachedToMask = 0
	this.Layer_2.layerDepth = 0
	this.Layer_2.layerIndex = 0
	this.Layer_2.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_2).wait(1));

	// Layer_1_obj_
	this.Layer_1 = new lib.Symbol_21_Layer_1();
	this.Layer_1.name = "Layer_1";
	this.Layer_1.parent = this;
	this.Layer_1.setTransform(-0.4,-2.3,1,1,0,0,0,-0.4,-2.3);
	this.Layer_1.depth = 0;
	this.Layer_1.isAttachedToCamera = 0
	this.Layer_1.isAttachedToMask = 0
	this.Layer_1.layerDepth = 0
	this.Layer_1.layerIndex = 1
	this.Layer_1.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_1).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol21, new cjs.Rectangle(-251.2,-58.1,502.5,116.2), null);


(lib.Symbol20 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1_obj_
	this.Layer_1 = new lib.Symbol_20_Layer_1();
	this.Layer_1.name = "Layer_1";
	this.Layer_1.parent = this;
	this.Layer_1.setTransform(0.1,-2.3,1,1,0,0,0,0.1,-2.3);
	this.Layer_1.depth = 0;
	this.Layer_1.isAttachedToCamera = 0
	this.Layer_1.isAttachedToMask = 0
	this.Layer_1.layerDepth = 0
	this.Layer_1.layerIndex = 0
	this.Layer_1.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_1).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol20, new cjs.Rectangle(-190.9,-58.1,381.8,116.2), null);


(lib.Symbol19 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1_obj_
	this.Layer_1 = new lib.Symbol_19_Layer_1();
	this.Layer_1.name = "Layer_1";
	this.Layer_1.parent = this;
	this.Layer_1.setTransform(-0.3,-2.3,1,1,0,0,0,-0.3,-2.3);
	this.Layer_1.depth = 0;
	this.Layer_1.isAttachedToCamera = 0
	this.Layer_1.isAttachedToMask = 0
	this.Layer_1.layerDepth = 0
	this.Layer_1.layerIndex = 0
	this.Layer_1.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_1).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol19, new cjs.Rectangle(-223.8,-58.1,447.70000000000005,116.2), null);


(lib.Symbol18 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1_obj_
	this.Layer_1 = new lib.Symbol_18_Layer_1();
	this.Layer_1.name = "Layer_1";
	this.Layer_1.parent = this;
	this.Layer_1.setTransform(233.4,3.4,1,1,0,0,0,233.4,3.4);
	this.Layer_1.depth = 0;
	this.Layer_1.isAttachedToCamera = 0
	this.Layer_1.isAttachedToMask = 0
	this.Layer_1.layerDepth = 0
	this.Layer_1.layerIndex = 0
	this.Layer_1.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_1).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol18, new cjs.Rectangle(96.5,-58.1,275,116.2), null);


(lib.Symbol14 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_10
	this.instance = new lib.Symbol25();
	this.instance.parent = this;
	this.instance.setTransform(36.75,195.75);
	this.instance.alpha = 0;
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(175).to({_off:false},0).wait(1).to({regX:0.7,regY:2.8,x:75.8,y:198.55,alpha:0.48},0).wait(1).to({x:90.25,alpha:0.6605},0).wait(1).to({x:98.95,alpha:0.7692},0).wait(1).to({x:104.85,alpha:0.8426},0).wait(1).to({x:109,alpha:0.8946},0).wait(1).to({x:112,alpha:0.9322},0).wait(1).to({x:114.15,alpha:0.9594},0).wait(1).to({x:115.7,alpha:0.9787},0).wait(1).to({x:116.75,alpha:0.9918},0).wait(1).to({regX:0,regY:0,y:195.75,alpha:1},0).wait(66).to({regX:0.7,regY:2.8,x:116.85,y:198.55,alpha:0.9853},0).wait(1).to({x:114.8,alpha:0.9346},0).wait(1).to({x:110.85,alpha:0.8351},0).wait(1).to({x:104.4,alpha:0.674},0).wait(1).to({x:95.75,alpha:0.4587},0).wait(1).to({x:87.3,alpha:0.2465},0).wait(1).to({x:81.35,alpha:0.0983},0).wait(1).to({x:78.3,alpha:0.0217},0).wait(1).to({regX:0,regY:0,x:76.75,y:195.75,alpha:0},0).wait(6));

	// Layer_9
	this.instance_1 = new lib.Symbol24();
	this.instance_1.parent = this;
	this.instance_1.setTransform(10.4,98.25);
	this.instance_1.alpha = 0;
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(171).to({_off:false},0).wait(1).to({regX:0.5,regY:-2.3,x:72.3,y:95.95,alpha:0.48},0).wait(1).to({x:95.4,alpha:0.6605},0).wait(1).to({x:109.35,alpha:0.7692},0).wait(1).to({x:118.7,alpha:0.8426},0).wait(1).to({x:125.4,alpha:0.8946},0).wait(1).to({x:130.2,alpha:0.9322},0).wait(1).to({x:133.65,alpha:0.9594},0).wait(1).to({x:136.15,alpha:0.9787},0).wait(1).to({x:137.85,alpha:0.9918},0).wait(1).to({regX:0,regY:0,x:138.4,y:98.25,alpha:1},0).wait(67).to({regX:0.5,regY:-2.3,x:136.4,y:95.95,alpha:0.9853},0).wait(1).to({x:127.9,alpha:0.9346},0).wait(1).to({x:111.2,alpha:0.8351},0).wait(1).to({x:84.1,alpha:0.674},0).wait(1).to({x:47.95,alpha:0.4587},0).wait(1).to({x:12.3,alpha:0.2465},0).wait(1).to({x:-12.55,alpha:0.0983},0).wait(1).to({x:-25.4,alpha:0.0217},0).wait(1).to({regX:0,regY:0,x:-29.6,y:98.25,alpha:0},0).wait(9));

	// Layer_8
	this.instance_2 = new lib.Symbol23();
	this.instance_2.parent = this;
	this.instance_2.setTransform(20.55,0);
	this.instance_2.alpha = 0;
	this.instance_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(169).to({_off:false},0).wait(1).to({regX:-0.2,regY:1.8,x:74.55,y:1.8,alpha:0.48},0).wait(1).to({x:94.95,alpha:0.6605},0).wait(1).to({x:107.25,alpha:0.7692},0).wait(1).to({x:115.55,alpha:0.8426},0).wait(1).to({x:121.4,alpha:0.8946},0).wait(1).to({x:125.65,alpha:0.9322},0).wait(1).to({x:128.75,alpha:0.9594},0).wait(1).to({x:130.9,alpha:0.9787},0).wait(1).to({x:132.4,alpha:0.9918},0).wait(1).to({regX:0,regY:0,x:133.55,y:0,alpha:1},0).wait(66).to({regX:-0.2,regY:1.8,x:132.25,y:1.8,alpha:0.9853},0).wait(1).to({x:128.55,alpha:0.9346},0).wait(1).to({x:121.3,alpha:0.8351},0).wait(1).to({x:109.55,alpha:0.674},0).wait(1).to({x:93.8,alpha:0.4587},0).wait(1).to({x:78.3,alpha:0.2465},0).wait(1).to({x:67.5,alpha:0.0983},0).wait(1).to({x:61.9,alpha:0.0217},0).wait(1).to({regX:0,regY:0,x:60.55,y:0,alpha:0},0).wait(12));

	// Layer_6
	this.instance_3 = new lib.Symbol22();
	this.instance_3.parent = this;
	this.instance_3.setTransform(194.05,195.75);
	this.instance_3.alpha = 0;
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(86).to({_off:false},0).wait(1).to({regX:1.1,regY:1.7,x:102.5,y:197.45,alpha:0.5062},0).wait(1).to({x:69.05,alpha:0.6889},0).wait(1).to({x:49.35,alpha:0.7966},0).wait(1).to({x:36.3,alpha:0.8678},0).wait(1).to({x:27.3,alpha:0.9169},0).wait(1).to({x:21.05,alpha:0.9513},0).wait(1).to({x:16.7,alpha:0.975},0).wait(1).to({x:13.85,alpha:0.9906},0).wait(1).to({regX:0,regY:0,x:11.05,y:195.75,alpha:1},0).wait(75).to({x:244.05,alpha:0},6,cjs.Ease.get(-1)).to({_off:true},1).wait(88));

	// Layer_5
	this.instance_4 = new lib.Symbol21();
	this.instance_4.parent = this;
	this.instance_4.setTransform(207.35,97.5);
	this.instance_4.alpha = 0;
	this.instance_4._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(84).to({_off:false},0).wait(1).to({regX:-0.4,regY:2.6,x:115.8,y:100.1,alpha:0.5062},0).wait(1).to({x:82.9,alpha:0.6889},0).wait(1).to({x:63.55,alpha:0.7966},0).wait(1).to({x:50.7,alpha:0.8678},0).wait(1).to({x:41.85,alpha:0.9169},0).wait(1).to({x:35.7,alpha:0.9513},0).wait(1).to({x:31.45,alpha:0.975},0).wait(1).to({x:28.6,alpha:0.9906},0).wait(1).to({regX:0,regY:0,x:27.35,y:97.5,alpha:1},0).wait(74).to({x:137.35,alpha:0},6,cjs.Ease.get(-1)).to({_off:true},1).wait(91));

	// Layer_4
	this.instance_5 = new lib.Symbol20();
	this.instance_5.parent = this;
	this.instance_5.setTransform(147.1,0);
	this.instance_5.alpha = 0;
	this.instance_5._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(81).to({_off:false},0).wait(1).to({regX:0.2,regY:-2.3,x:124.5,y:-2.3,alpha:0.5062},0).wait(1).to({x:116.25,alpha:0.6889},0).wait(1).to({x:111.45,alpha:0.7966},0).wait(1).to({x:108.2,alpha:0.8678},0).wait(1).to({x:106,alpha:0.9169},0).wait(1).to({x:104.45,alpha:0.9513},0).wait(1).to({x:103.4,alpha:0.975},0).wait(1).to({x:102.7,alpha:0.9906},0).wait(1).to({regX:0,regY:0,x:102.1,y:0,alpha:1},0).wait(74).to({x:167.1,alpha:0},6,cjs.Ease.get(-1)).to({_off:true},1).wait(94));

	// Layer_2
	this.instance_6 = new lib.Symbol18();
	this.instance_6.parent = this;
	this.instance_6.setTransform(-156.3,97.5);
	this.instance_6.alpha = 0;
	this.instance_6._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(5).to({_off:false},0).wait(1).to({regX:233.4,regY:3.4,x:110.7,y:100.9,alpha:0.48},0).wait(1).to({x:123.35,alpha:0.6605},0).wait(1).to({x:130.95,alpha:0.7692},0).wait(1).to({x:136.1,alpha:0.8426},0).wait(1).to({x:139.75,alpha:0.8946},0).wait(1).to({x:142.4,alpha:0.9322},0).wait(1).to({x:144.3,alpha:0.9594},0).wait(1).to({x:145.65,alpha:0.9787},0).wait(1).to({x:146.55,alpha:0.9918},0).wait(1).to({regX:0,regY:0,x:-86.3,y:97.5,alpha:1},0).wait(64).to({regX:233.4,regY:3.4,x:144.75,y:100.9,alpha:0.9853},0).wait(1).to({x:136.65,alpha:0.9346},0).wait(1).to({x:120.75,alpha:0.8351},0).wait(1).to({x:94.95,alpha:0.674},0).wait(1).to({x:60.5,alpha:0.4587},0).wait(1).to({x:26.55,alpha:0.2465},0).wait(1).to({x:2.85,alpha:0.0983},0).wait(1).to({x:-9.4,alpha:0.0217},0).wait(1).to({regX:0,regY:0,x:-246.3,y:97.5,alpha:0},0).to({_off:true},1).wait(177));

	// Layer_1
	this.instance_7 = new lib.Symbol19();
	this.instance_7.parent = this;
	this.instance_7.setTransform(-50,0);
	this.instance_7.alpha = 0;

	this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(1).to({regX:-0.2,regY:-2.3,x:2.55,y:-2.3,alpha:0.48},0).wait(1).to({x:22.45,alpha:0.6605},0).wait(1).to({x:34.4,alpha:0.7692},0).wait(1).to({x:42.45,alpha:0.8426},0).wait(1).to({x:48.2,alpha:0.8946},0).wait(1).to({x:52.3,alpha:0.9322},0).wait(1).to({x:55.3,alpha:0.9594},0).wait(1).to({x:57.45,alpha:0.9787},0).wait(1).to({x:58.85,alpha:0.9918},0).wait(1).to({regX:0,regY:0,x:60,y:0,alpha:1},0).wait(66).to({regX:-0.2,regY:-2.3,x:57.55,y:-2.3,alpha:0.9853},0).wait(1).to({x:49.95,alpha:0.9346},0).wait(1).to({x:35.05,alpha:0.8351},0).wait(1).to({x:10.9,alpha:0.674},0).wait(1).to({x:-21.35,alpha:0.4587},0).wait(1).to({x:-53.2,alpha:0.2465},0).wait(1).to({x:-75.4,alpha:0.0983},0).wait(1).to({x:-86.9,alpha:0.0217},0).wait(1).to({regX:0,regY:0,x:-90,y:0,alpha:0},0).to({_off:true},1).wait(180));

	// Layer_7
	this.instance_8 = new lib.Symbol15();
	this.instance_8.parent = this;
	this.instance_8.setTransform(23.05,102.6,1.7975,1,0,0,0,0.1,0);
	this.instance_8._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_8).wait(169).to({_off:false},0).to({regX:0,scaleX:1,x:137.5},9,cjs.Ease.get(1)).wait(70).to({x:136.0302,alpha:0.9853},0).wait(1).to({x:130.9575,alpha:0.9346},0).wait(1).to({x:121.0126,alpha:0.8351},0).wait(1).to({x:104.9046,alpha:0.674},0).wait(1).to({x:83.3707,alpha:0.4587},0).wait(1).to({x:62.1514,alpha:0.2465},0).wait(1).to({x:47.3334,alpha:0.0983},0).wait(1).to({x:39.6744,alpha:0.0217},0).wait(1).to({x:37.5,alpha:0},0).to({_off:true},1).wait(8));

	// Layer_3
	this.instance_9 = new lib.Symbol17();
	this.instance_9.parent = this;
	this.instance_9.setTransform(68.9,101.6,0.6943,1);
	this.instance_9.alpha = 0;
	this.instance_9._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_9).wait(85).to({_off:false},0).to({scaleX:1,x:24.5,alpha:1},5,cjs.Ease.get(1)).wait(79).to({regX:-0.2,scaleX:0.5585,x:137.65,alpha:0},9,cjs.Ease.get(1)).to({_off:true},1).wait(86));

	// Layer_11
	this.instance_10 = new lib.Symbol16();
	this.instance_10.parent = this;
	this.instance_10.setTransform(48.5,101.6);
	this.instance_10.alpha = 0;
	this.instance_10._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_10).wait(2).to({_off:false},0).wait(1).to({x:109.653,alpha:0.6115},0).wait(1).to({x:128.1643,alpha:0.7966},0).wait(1).to({x:137.9582,alpha:0.8946},0).wait(1).to({x:143.6283,alpha:0.9513},0).wait(1).to({x:146.8671,alpha:0.9837},0).wait(1).to({x:148.5,alpha:1},0).wait(72).to({scaleX:1.7681,x:23.2},9,cjs.Ease.get(1)).to({_off:true},1).wait(175));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-313.8,-58.1,845.8,312);


(lib.Symbol12 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("A3DLZIAAuAIIooxIZ/BIILgKvIAAK6g");
	mask.setTransform(-8.2,230.9);

	// Layer_1
	this.instance = new lib.Symbol6();
	this.instance.parent = this;

	var maskedShapeInstanceList = [this.instance];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol12, new cjs.Rectangle(-155.8,158,295.20000000000005,131), null);


(lib.Symbol11 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("AtrHSIp0nNIAAncMAu6AAAIAFILIrQGkg");
	mask.setTransform(-11.925,-261.55);

	// Layer_1
	this.instance = new lib.Symbol6();
	this.instance.parent = this;

	var maskedShapeInstanceList = [this.instance];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol11, new cjs.Rectangle(-162.3,-289,300.8,74.69999999999999), null);


(lib.Symbol10 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("EgEaglTIJNGyMgALA6QIpaJlg");
	mask.setTransform(-130.1,-19.55);

	// Layer_1
	this.instance = new lib.Symbol6();
	this.instance.parent = this;

	var maskedShapeInstanceList = [this.instance];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol10, new cjs.Rectangle(-160.8,-258.3,61.400000000000006,477.5), null);


(lib.Symbol9 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("AmgbzMAAHg7WIM6ngMgA6BOHg");
	mask.setTransform(107.175,-12.475);

	// Layer_1
	this.instance = new lib.Symbol6();
	this.instance.parent = this;

	var maskedShapeInstanceList = [this.instance];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol9, new cjs.Rectangle(65.5,-262.4,83.4,499.9), null);


(lib.Symbol8 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("AtGckMAAKg6RIaDAAMgAQA7bg");
	mask.setTransform(-16.6,-24.75);

	// Layer_1
	this.instance = new lib.Symbol6();
	this.instance.parent = this;

	var maskedShapeInstanceList = [this.instance];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol8, new cjs.Rectangle(-100.5,-214.9,167.9,380.4), null);


(lib.Symbol_1_pack3_png = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// pack3_png
	this.instance = new lib.Symbol7();
	this.instance.parent = this;
	this.instance.setTransform(152.4,-14.1,1.4792,1);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(178).to({_off:false},0).to({scaleX:1},5,cjs.Ease.get(1)).wait(67).to({scaleX:2.1031,scaleY:0.8173,x:183.4},3,cjs.Ease.get(-1)).to({_off:true},1).wait(11));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_1_pack2_png_4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// pack2_png
	this.instance = new lib.Symbol9();
	this.instance.parent = this;
	this.instance.setTransform(244.9,7.7,0.5873,1.1485,0,0,0,65.5,-12.4);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(139).to({_off:false},0).to({scaleX:0.9174,scaleY:1.0297,x:244.95,y:-12},4).to({regY:-12.5,scaleX:1,scaleY:1,x:244.9,y:-17},9,cjs.Ease.get(1)).to({_off:true},21).wait(92));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_1_pack2_png_3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// pack2_png
	this.instance = new lib.Symbol10();
	this.instance.parent = this;
	this.instance.setTransform(79.9,-24.1,0.0009,1,0,0,0,-113,-19.6);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(139).to({_off:false},0).to({regX:-112.7,regY:-19.4,scaleX:0.8066,scaleY:1.0974,x:80.05,y:-0.8},1).to({regX:-112.5,regY:-19.3,scaleX:1,scaleY:1.0243,x:76.1,y:-18.2},3).to({scaleY:1,x:67,y:-23.7},9,cjs.Ease.get(1)).to({_off:true},21).wait(92));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_1_pack2_png_2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// pack2_png
	this.instance = new lib.Symbol8();
	this.instance.parent = this;
	this.instance.setTransform(162,-219.4,1,1.1341,0,0,0,-17.4,-214.9);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(139).to({_off:false},0).to({regY:-214.8,scaleY:1.1178},1).to({scaleY:1.0294,y:-219.5},3).to({regY:-215,scaleY:1},9,cjs.Ease.get(1)).to({_off:true},21).wait(92));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_1_pack2_png_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// pack2_png
	this.instance = new lib.Symbol12();
	this.instance.parent = this;
	this.instance.setTransform(171.4,196,0.8634,0.2929,1.7971,0,0,-7.9,158.3);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(140).to({_off:false},0).to({regX:-7.7,regY:158.5,scaleX:0.9044,scaleY:0.9711,rotation:0.7869,x:174.7,y:171.65},2).to({scaleX:0.9522,scaleY:0.9855,rotation:0.2857,x:172.95,y:162.65},1).to({regX:-8.2,regY:158,scaleX:1,scaleY:1,rotation:0,x:171.2,y:153.5},9,cjs.Ease.get(1)).to({_off:true},21).wait(92));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_1_pack2_png = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// pack2_png
	this.instance = new lib.Symbol11();
	this.instance.parent = this;
	this.instance.setTransform(167.4,-219.05,1,0.7516,0,-0.2408,0,-12,-214.6);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(140).to({_off:false},0).to({regY:-214.7,scaleY:0.9379,rotation:-0.0149,skewX:0,x:167.45,y:-219},3).to({regY:-214.3,scaleY:1,rotation:0,x:167.4,y:-218.8},9,cjs.Ease.get(1)).to({_off:true},21).wait(92));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_1_pack1_png_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// pack1_png
	this.instance = new lib.Symbol2();
	this.instance.parent = this;
	this.instance.setTransform(245.35,213.45,1,1,0,0,0,60.1,241.2);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(69).to({regX:60.2,rotation:-1.266,x:245.45,y:213.5},2).to({scaleY:1.0002,rotation:0,skewX:1.0438,y:213.45},2).to({scaleY:1,rotation:-1.266,skewX:0,y:213.5},2).to({scaleY:1.0002,rotation:0,skewX:1.0438,y:213.45},2).to({regX:60.1,scaleY:1,skewX:0,x:245.35},2).to({_off:true},61).wait(125));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_1_pack1_png = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// pack1_png
	this.instance = new lib.Symbol2();
	this.instance.parent = this;
	this.instance.setTransform(245.6,223.45,0.7706,1,0,0,0,60.6,241.2);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(253).to({_off:false},0).wait(1).to({regX:0,regY:0,scaleX:0.8905,x:191.5342,y:-22.974},0).wait(1).to({scaleX:0.9361,x:188.7299,y:-24.9628},0).wait(1).to({scaleX:0.962,x:187.1349,y:-26.094},0).wait(1).to({scaleX:0.9782,x:186.1384,y:-26.8008},0).wait(1).to({scaleX:0.9886,x:185.5031,y:-27.2514},0).wait(1).to({scaleX:0.995,x:185.1098,y:-27.5303},0).wait(1).to({scaleX:0.9985,x:184.8899,y:-27.6862},0).wait(1).to({regX:60.1,regY:241.2,scaleX:1,x:245.35,y:213.45},0).wait(4));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_1_pack_part2_shadow_png = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// pack_part2_shadow_png
	this.instance = new lib.Symbol5();
	this.instance.parent = this;
	this.instance.setTransform(265.7,-175.45);
	this.instance.alpha = 0;
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(79).to({_off:false},0).wait(1).to({scaleX:1.0916,scaleY:1.3361,skewX:-0.1745,x:264.3873,y:-177.9927,alpha:0.2606},0).wait(1).to({scaleX:1.2286,scaleY:1.8384,skewX:-0.4354,x:262.4249,y:-181.7936,alpha:0.6501},0).wait(1).to({scaleX:1.3179,scaleY:2.1658,skewX:-0.6054,x:261.1461,y:-184.2707,alpha:0.904},0).wait(1).to({regX:0.1,regY:-0.2,scaleX:1.3516,scaleY:2.2896,skewX:-0.6698,x:260.75,y:-185.2,alpha:1},0).to({regX:0,regY:0,scaleX:1,scaleY:1,skewX:0,x:265.7,y:-175.45},2,cjs.Ease.get(1)).to({_off:true},55).wait(125));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_1_pack_part2_png_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// pack_part2_png
	this.instance = new lib.Symbol4();
	this.instance.parent = this;
	this.instance.setTransform(247.45,-220.75,0.8458,0.7918,0,3.3949,-8.2115,-23.8,-8.3);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(66).to({regX:0,regY:0,scaleX:0.8456,skewX:3.3595,skewY:-8.1603,x:266.9649,y:-217.061},0).wait(1).to({scaleX:0.8454,scaleY:0.7917,skewX:3.3266,skewY:-8.1128,x:266.9783,y:-217.0713},0).wait(1).to({scaleX:0.8452,scaleY:0.7916,skewX:3.2964,skewY:-8.0692,x:266.9907,y:-217.0807},0).wait(1).to({regX:-23.8,regY:-8.3,scaleX:0.8451,skewX:3.2688,skewY:-8.0293,x:247.45,y:-220.8},0).wait(1).to({regX:0,regY:0,scaleX:0.8453,scaleY:0.7917,skewX:1.3239,skewY:-9.9994,x:263.1704,y:-217.2994},0).wait(1).to({regX:-23.6,regY:-8.3,scaleX:0.8454,skewX:-0.4163,skewY:-11.7622,x:240,y:-220},0).wait(1).to({regX:0,regY:0,scaleX:0.8453,scaleY:0.7916,skewX:1.5602,skewY:-9.7655,x:268.5879,y:-217.0933},0).wait(1).to({regX:-23.7,regY:-8.3,scaleX:0.8451,skewX:3.2732,skewY:-8.035,x:256.95,y:-220.5},0).wait(1).to({regX:0,regY:0,scaleX:0.8453,scaleY:0.7917,skewX:1.244,skewY:-10.085,x:267.1931,y:-217.1655},0).wait(1).to({regX:-23.6,regY:-8.3,scaleX:0.8454,skewX:-0.4163,skewY:-11.7622,x:240,y:-220},0).wait(1).to({regX:0,regY:0,scaleX:0.8453,scaleY:0.7916,skewX:1.5602,skewY:-9.7655,x:268.5879,y:-217.0933},0).wait(1).to({regX:-23.7,regY:-8.3,scaleX:0.8451,skewX:3.2732,skewY:-8.035,x:256.95,y:-220.5},0).wait(1).to({regX:0,regY:0,scaleX:0.8455,scaleY:0.7917,skewX:3.3401,skewY:-8.1321,x:271.1553,y:-216.8865},0).wait(1).to({regX:-23.8,regY:-8.3,scaleX:0.8458,scaleY:0.7918,skewX:3.3949,skewY:-8.2115,x:247.45,y:-220.75},0).wait(1).to({regX:0,regY:0,scaleX:0.914,scaleY:0.8362,skewX:0.5958,skewY:-6.1738,x:268.9261,y:-216.1139},0).wait(1).to({scaleX:1.0159,scaleY:0.9024,skewX:-3.5884,skewY:-3.1278,x:271.9164,y:-214.5266},0).wait(1).to({scaleX:1.0823,scaleY:0.9456,skewX:-6.3152,skewY:-1.1427,x:273.8792,y:-213.3753},0).wait(1).to({regX:-24,regY:-7.9,scaleX:1.1074,scaleY:0.962,skewX:-7.347,skewY:-0.3916,x:247.3,y:-220.65},0).to({regX:-24.3,regY:-7.5,scaleX:1,scaleY:1,skewX:0,skewY:0,x:247.25,y:-220.25},2,cjs.Ease.get(1)).to({_off:true},55).wait(125));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_1_pack_part2_png = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// pack_part2_png
	this.instance = new lib.Symbol4();
	this.instance.parent = this;
	this.instance.setTransform(247.5,-209.25,0.7243,0.7895,0,2.5489,-19.6999,-23.4,-8.6);
	this.instance.alpha = 0;
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(253).to({_off:false},0).wait(1).to({regX:0,regY:0,scaleX:0.7878,scaleY:0.7907,skewX:2.9908,skewY:-13.6984,x:265.0524,y:-212.8166,alpha:0.5224},0).wait(1).to({scaleX:0.812,scaleY:0.7912,skewX:3.1591,skewY:-11.4135,x:265.7471,y:-214.5007,alpha:0.7213},0).wait(1).to({scaleX:0.8257,scaleY:0.7915,skewX:3.2548,skewY:-10.114,x:266.1332,y:-215.4362,alpha:0.8344},0).wait(1).to({scaleX:0.8343,scaleY:0.7916,skewX:3.3146,skewY:-9.3021,x:266.3707,y:-216.0124,alpha:0.9051},0).wait(1).to({scaleX:0.8398,scaleY:0.7917,skewX:3.3527,skewY:-8.7844,x:266.5207,y:-216.3766,alpha:0.9501},0).wait(1).to({scaleX:0.8432,scaleY:0.7918,skewX:3.3763,skewY:-8.464,x:266.6128,y:-216.6007,alpha:0.978},0).wait(1).to({scaleX:0.845,skewX:3.3895,skewY:-8.2848,x:266.6642,y:-216.7256,alpha:0.9936},0).wait(1).to({regX:-23.8,regY:-8.3,scaleX:0.8458,skewX:3.3949,skewY:-8.2115,x:247.45,y:-220.75,alpha:1},0).wait(4));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_1_pack_part1_png = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// pack_part1_png
	this.instance = new lib.Symbol3();
	this.instance.parent = this;
	this.instance.setTransform(80.6,-220.35,1,1,-26.7484,0,0,9.5,-24.8);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(79).to({_off:false},0).wait(1).to({regX:0,regY:0,scaleX:1.089,rotation:-18.871,x:78.8337,y:-193.4996},0).wait(1).to({scaleX:1.2221,rotation:-7.0958,x:72.1888,y:-194.2527},0).wait(1).to({scaleX:1.3089,rotation:0.5782,x:67.9906,y:-195.6116},0).wait(1).to({regX:9.5,regY:-24.9,scaleX:1.3417,rotation:3.4817,x:80.65,y:-220.3},0).to({regX:9.4,regY:-24.8,scaleX:1,rotation:0,x:80.55,y:-220.25},2,cjs.Ease.get(1)).to({_off:true},55).wait(125));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_1_Layer_17 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_17
	this.instance = new lib.Symbol6();
	this.instance.parent = this;
	this.instance.setTransform(179.4,-4.45);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(173).to({_off:false},0).to({scaleX:0.3517,x:164.4},4,cjs.Ease.get(-1)).to({_off:true},1).wait(87));

}).prototype = p = new cjs.MovieClip();


(lib.Scene_1_Layer_7 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_7
	this.instance = new lib.Symbol14("synched",0,false);
	this.instance.parent = this;
	this.instance.setTransform(1117.7,209.55);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(265));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_264 = function() {
		this.___loopingOver___ = true;
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(264).call(this.frame_264).wait(1));

	// Layer_5 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	var mask_graphics_261 = new cjs.Graphics().p("AjYJCIAAyDIGxAAIgQSDg");

	this.timeline.addTween(cjs.Tween.get(mask).to({graphics:null,x:0,y:0}).wait(261).to({graphics:mask_graphics_261,x:264.375,y:-213.85}).wait(4));

	// pack_part2_png_obj_
	this.pack_part2_png = new lib.Symbol_1_pack_part2_png();
	this.pack_part2_png.name = "pack_part2_png";
	this.pack_part2_png.parent = this;
	this.pack_part2_png.depth = 0;
	this.pack_part2_png.isAttachedToCamera = 0
	this.pack_part2_png.isAttachedToMask = 0
	this.pack_part2_png.layerDepth = 0
	this.pack_part2_png.layerIndex = 0
	this.pack_part2_png.maskLayerName = 0

	var maskedShapeInstanceList = [this.pack_part2_png];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.pack_part2_png).wait(254).to({regX:266.8,regY:-211,x:266.8,y:-211},0).wait(7).to({regX:0,regY:0,x:0,y:0},0).wait(4));

	// pack1_png_obj_
	this.pack1_png = new lib.Symbol_1_pack1_png();
	this.pack1_png.name = "pack1_png";
	this.pack1_png.parent = this;
	this.pack1_png.depth = 0;
	this.pack1_png.isAttachedToCamera = 0
	this.pack1_png.isAttachedToMask = 0
	this.pack1_png.layerDepth = 0
	this.pack1_png.layerIndex = 1
	this.pack1_png.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.pack1_png).wait(254).to({regX:185.1,regY:-22.8,x:185.1,y:-22.8},0).wait(7).to({regX:0,regY:0,x:0,y:0},0).wait(4));

	// pack3_png_obj_
	this.pack3_png = new lib.Symbol_1_pack3_png();
	this.pack3_png.name = "pack3_png";
	this.pack3_png.parent = this;
	this.pack3_png.depth = 0;
	this.pack3_png.isAttachedToCamera = 0
	this.pack3_png.isAttachedToMask = 0
	this.pack3_png.layerDepth = 0
	this.pack3_png.layerIndex = 2
	this.pack3_png.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.pack3_png).wait(265));

	// Layer_17_obj_
	this.Layer_17 = new lib.Symbol_1_Layer_17();
	this.Layer_17.name = "Layer_17";
	this.Layer_17.parent = this;
	this.Layer_17.depth = 0;
	this.Layer_17.isAttachedToCamera = 0
	this.Layer_17.isAttachedToMask = 0
	this.Layer_17.layerDepth = 0
	this.Layer_17.layerIndex = 3
	this.Layer_17.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_17).wait(265));

	// Layer_13 (mask)
	var mask_1 = new cjs.Shape();
	mask_1._off = true;
	var mask_1_graphics_140 = new cjs.Graphics().p("At1G9IlNi2IAArIMAmFAAAIAALGIm9C9g");
	var mask_1_graphics_141 = new cjs.Graphics().p("At7G9Il1jeIAAqgMAnhAAAIAAKWInxDtg");
	var mask_1_graphics_142 = new cjs.Graphics().p("AuAG9ImdkGIAAp4MAo7AAAIAAJwIojETg");
	var mask_1_graphics_143 = new cjs.Graphics().p("AuEG9InNk+IAApAMAqjAAAIAAI6IpbFJg");
	var mask_1_graphics_144 = new cjs.Graphics().p("AuAG9Inkk8IAApCMArJAAAIAAJCIprFBg");
	var mask_1_graphics_145 = new cjs.Graphics().p("At+G9In2k6IAApEMArpAAAIAAJIIp4E7g");
	var mask_1_graphics_146 = new cjs.Graphics().p("At7G9IoHk4IAApGMAsFAAAIAAJOIqEE1g");
	var mask_1_graphics_147 = new cjs.Graphics().p("At5G9IoUk3IAApHMAscAAAIAAJTIqOEwg");
	var mask_1_graphics_148 = new cjs.Graphics().p("At4G9Iofk2IAApIMAsvAAAIAAJXIqVEsg");
	var mask_1_graphics_149 = new cjs.Graphics().p("At2G9Iopk1IAApJMAs+AAAIAAJaIqbEpg");
	var mask_1_graphics_150 = new cjs.Graphics().p("At1G9Iovk0IAApKMAtJAAAIAAJcIqgEng");
	var mask_1_graphics_151 = new cjs.Graphics().p("At1G9Ioyk0IAApKMAtPAAAIAAJeIqiElg");
	var mask_1_graphics_152 = new cjs.Graphics().p("At1G9Iozk0IAApKMAtRAAAIAAJeIqjElg");

	this.timeline.addTween(cjs.Tween.get(mask_1).to({graphics:null,x:0,y:0}).wait(140).to({graphics:mask_1_graphics_140,x:168.375,y:-263.775}).wait(1).to({graphics:mask_1_graphics_141,x:168.975,y:-263.775}).wait(1).to({graphics:mask_1_graphics_142,x:169.475,y:-263.775}).wait(1).to({graphics:mask_1_graphics_143,x:169.875,y:-263.775}).wait(1).to({graphics:mask_1_graphics_144,x:169.55,y:-263.775}).wait(1).to({graphics:mask_1_graphics_145,x:169.275,y:-263.775}).wait(1).to({graphics:mask_1_graphics_146,x:169.05,y:-263.775}).wait(1).to({graphics:mask_1_graphics_147,x:168.85,y:-263.775}).wait(1).to({graphics:mask_1_graphics_148,x:168.675,y:-263.775}).wait(1).to({graphics:mask_1_graphics_149,x:168.55,y:-263.775}).wait(1).to({graphics:mask_1_graphics_150,x:168.45,y:-263.775}).wait(1).to({graphics:mask_1_graphics_151,x:168.4,y:-263.775}).wait(1).to({graphics:mask_1_graphics_152,x:168.375,y:-263.775}).wait(21).to({graphics:null,x:0,y:0}).wait(92));

	// pack2_png_obj_
	this.pack2_png = new lib.Symbol_1_pack2_png();
	this.pack2_png.name = "pack2_png";
	this.pack2_png.parent = this;
	this.pack2_png.depth = 0;
	this.pack2_png.isAttachedToCamera = 0
	this.pack2_png.isAttachedToMask = 0
	this.pack2_png.layerDepth = 0
	this.pack2_png.layerIndex = 4
	this.pack2_png.maskLayerName = 0

	var maskedShapeInstanceList = [this.pack2_png];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask_1;
	}

	this.timeline.addTween(cjs.Tween.get(this.pack2_png).wait(265));

	// pack2_png_obj_
	this.pack2_png_1 = new lib.Symbol_1_pack2_png_1();
	this.pack2_png_1.name = "pack2_png_1";
	this.pack2_png_1.parent = this;
	this.pack2_png_1.depth = 0;
	this.pack2_png_1.isAttachedToCamera = 0
	this.pack2_png_1.isAttachedToMask = 0
	this.pack2_png_1.layerDepth = 0
	this.pack2_png_1.layerIndex = 5
	this.pack2_png_1.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.pack2_png_1).wait(265));

	// pack2_png_obj_
	this.pack2_png_2 = new lib.Symbol_1_pack2_png_2();
	this.pack2_png_2.name = "pack2_png_2";
	this.pack2_png_2.parent = this;
	this.pack2_png_2.depth = 0;
	this.pack2_png_2.isAttachedToCamera = 0
	this.pack2_png_2.isAttachedToMask = 0
	this.pack2_png_2.layerDepth = 0
	this.pack2_png_2.layerIndex = 6
	this.pack2_png_2.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.pack2_png_2).wait(265));

	// Layer_15 (mask)
	var mask_2 = new cjs.Shape();
	mask_2._off = true;
	var mask_2_graphics_139 = new cjs.Graphics().p("EgRTgmMMAinACEMAAABBXI5kI9g");
	var mask_2_graphics_140 = new cjs.Graphics().p("EgNjgrPIbGOHMAAABBXI5cHBg");
	var mask_2_graphics_141 = new cjs.Graphics().p("EgOtgu3IdbRNMAAABBXI4CLLg");
	var mask_2_graphics_142 = new cjs.Graphics().p("EgOYgwEIcwR9MAAABBYI0WM0g");
	var mask_2_graphics_143 = new cjs.Graphics().p("EgN0guoIboSRMAAABBXI5cJpg");
	var mask_2_graphics_144 = new cjs.Graphics().p("EgNmgujIbNSQMAAABBXI5fJgg");
	var mask_2_graphics_145 = new cjs.Graphics().p("EgNagueIa0SOMAAABBXI5fJYg");
	var mask_2_graphics_146 = new cjs.Graphics().p("EgNPguaIafSNMAAABBXI5hJRg");
	var mask_2_graphics_147 = new cjs.Graphics().p("EgNGguXIaNSMMAAABBXI5iJLg");
	var mask_2_graphics_148 = new cjs.Graphics().p("EgM/guUIZ+SLMAAABBXI5iJHg");
	var mask_2_graphics_149 = new cjs.Graphics().p("EgM5guRIZzSKMAAABBXI5jJCg");
	var mask_2_graphics_150 = new cjs.Graphics().p("EgM1guQIZrSKMAAABBXI5kJAg");
	var mask_2_graphics_151 = new cjs.Graphics().p("EgMyguPIZlSJMAAABBYI5kI+g");
	var mask_2_graphics_152 = new cjs.Graphics().p("EgMyguOIZkSJMAAABBXI5kI9g");

	this.timeline.addTween(cjs.Tween.get(mask_2).to({graphics:null,x:0,y:0}).wait(139).to({graphics:mask_2_graphics_139,x:-30.3,y:11.1}).wait(1).to({graphics:mask_2_graphics_140,x:-6.2,y:-33.725}).wait(1).to({graphics:mask_2_graphics_141,x:-13.7,y:-30.35}).wait(1).to({graphics:mask_2_graphics_142,x:-11.5,y:-27.475}).wait(1).to({graphics:mask_2_graphics_143,x:-7.9,y:-38.625}).wait(1).to({graphics:mask_2_graphics_144,x:-6.525,y:-39}).wait(1).to({graphics:mask_2_graphics_145,x:-5.3,y:-39.325}).wait(1).to({graphics:mask_2_graphics_146,x:-4.225,y:-39.6}).wait(1).to({graphics:mask_2_graphics_147,x:-3.325,y:-39.85}).wait(1).to({graphics:mask_2_graphics_148,x:-2.6,y:-40.05}).wait(1).to({graphics:mask_2_graphics_149,x:-2.025,y:-40.2}).wait(1).to({graphics:mask_2_graphics_150,x:-1.625,y:-40.325}).wait(1).to({graphics:mask_2_graphics_151,x:-1.375,y:-40.375}).wait(1).to({graphics:mask_2_graphics_152,x:-1.3,y:-40.4}).wait(21).to({graphics:null,x:0,y:0}).wait(92));

	// pack2_png_obj_
	this.pack2_png_3 = new lib.Symbol_1_pack2_png_3();
	this.pack2_png_3.name = "pack2_png_3";
	this.pack2_png_3.parent = this;
	this.pack2_png_3.depth = 0;
	this.pack2_png_3.isAttachedToCamera = 0
	this.pack2_png_3.isAttachedToMask = 0
	this.pack2_png_3.layerDepth = 0
	this.pack2_png_3.layerIndex = 7
	this.pack2_png_3.maskLayerName = 0

	var maskedShapeInstanceList = [this.pack2_png_3];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask_2;
	}

	this.timeline.addTween(cjs.Tween.get(this.pack2_png_3).wait(265));

	// Layer_14 (mask)
	var mask_3 = new cjs.Shape();
	mask_3._off = true;
	var mask_3_graphics_139 = new cjs.Graphics().p("AkC9mIIFolMAAABFLIoFHMg");
	var mask_3_graphics_140 = new cjs.Graphics().p("EgEoAkUMAAAhCKIJRpFMgBpBN3g");
	var mask_3_graphics_141 = new cjs.Graphics().p("EgFOAiMMAAAhAnIKdplMgCOBQBg");
	var mask_3_graphics_142 = new cjs.Graphics().p("AlqfYMAAAg/GILVo3MgCLBRLg");
	var mask_3_graphics_143 = new cjs.Graphics().p("AmFeAMAAAg9kIMLoKMgBuBPcg");
	var mask_3_graphics_144 = new cjs.Graphics().p("AmLdlMAAAg9PIMXoBMgBkBPXg");
	var mask_3_graphics_145 = new cjs.Graphics().p("AmQdNMAAAg89IMgn4MgBaBPRg");
	var mask_3_graphics_146 = new cjs.Graphics().p("AmUc3MAAAg8tIMpnwMgBSBPNg");
	var mask_3_graphics_147 = new cjs.Graphics().p("AmYclMAAAg8fIMxnrMgBLBPKg");
	var mask_3_graphics_148 = new cjs.Graphics().p("AmbcXMAAAg8VIM3nlMgBFBPHg");
	var mask_3_graphics_149 = new cjs.Graphics().p("AmdcMMAAAg8NIM7nhMgBBBPFg");
	var mask_3_graphics_150 = new cjs.Graphics().p("AmfcEMAAAg8HIM+neMgA9BPDg");
	var mask_3_graphics_151 = new cjs.Graphics().p("Amgb/MAAAg8DINBndMgA9BPDg");
	var mask_3_graphics_152 = new cjs.Graphics().p("Amgb+MAAAg8CINBncMgA7BPBg");

	this.timeline.addTween(cjs.Tween.get(mask_3).to({graphics:null,x:0,y:0}).wait(139).to({graphics:mask_3_graphics_139,x:270.825,y:-29.825}).wait(1).to({graphics:mask_3_graphics_140,x:274.625,y:-28.225}).wait(1).to({graphics:mask_3_graphics_141,x:278.425,y:-24.5}).wait(1).to({graphics:mask_3_graphics_142,x:281.15,y:-16.2}).wait(1).to({graphics:mask_3_graphics_143,x:283.875,y:-17.25}).wait(1).to({graphics:mask_3_graphics_144,x:284.45,y:-16.575}).wait(1).to({graphics:mask_3_graphics_145,x:284.95,y:-15.975}).wait(1).to({graphics:mask_3_graphics_146,x:285.375,y:-15.425}).wait(1).to({graphics:mask_3_graphics_147,x:285.75,y:-15}).wait(1).to({graphics:mask_3_graphics_148,x:286.05,y:-14.65}).wait(1).to({graphics:mask_3_graphics_149,x:286.275,y:-14.35}).wait(1).to({graphics:mask_3_graphics_150,x:286.45,y:-14.15}).wait(1).to({graphics:mask_3_graphics_151,x:286.55,y:-14.025}).wait(1).to({graphics:mask_3_graphics_152,x:286.575,y:-14}).wait(21).to({graphics:null,x:0,y:0}).wait(92));

	// pack2_png_obj_
	this.pack2_png_4 = new lib.Symbol_1_pack2_png_4();
	this.pack2_png_4.name = "pack2_png_4";
	this.pack2_png_4.parent = this;
	this.pack2_png_4.depth = 0;
	this.pack2_png_4.isAttachedToCamera = 0
	this.pack2_png_4.isAttachedToMask = 0
	this.pack2_png_4.layerDepth = 0
	this.pack2_png_4.layerIndex = 8
	this.pack2_png_4.maskLayerName = 0

	var maskedShapeInstanceList = [this.pack2_png_4];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask_3;
	}

	this.timeline.addTween(cjs.Tween.get(this.pack2_png_4).wait(265));

	// Layer_16_obj_
	this.Layer_16 = new lib.Symbol_1_Layer_16();
	this.Layer_16.name = "Layer_16";
	this.Layer_16.parent = this;
	this.Layer_16.depth = 0;
	this.Layer_16.isAttachedToCamera = 0
	this.Layer_16.isAttachedToMask = 0
	this.Layer_16.layerDepth = 0
	this.Layer_16.layerIndex = 9
	this.Layer_16.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_16).wait(265));

	// Layer_8 (mask)
	var mask_4 = new cjs.Shape();
	mask_4._off = true;
	var mask_4_graphics_0 = new cjs.Graphics().p("AjYJCIAAyDIGxAAIgQSDg");
	var mask_4_graphics_65 = new cjs.Graphics().p("AjYJCIAAyDIGxAAIgQSDg");
	var mask_4_graphics_66 = new cjs.Graphics().p("AjYJCIAAyDIGxAAIgQSDg");
	var mask_4_graphics_67 = new cjs.Graphics().p("AjYJCIAAyDIGxAAIgQSDg");
	var mask_4_graphics_68 = new cjs.Graphics().p("AjYJCIAAyDIGxAAIgQSDg");
	var mask_4_graphics_69 = new cjs.Graphics().p("AjYJCIAAyDIGxAAIgQSDg");
	var mask_4_graphics_70 = new cjs.Graphics().p("AjYJCIAAyDIGxAAIgQSDg");
	var mask_4_graphics_71 = new cjs.Graphics().p("AjYJCIAAyDIGxAAIgQSDg");
	var mask_4_graphics_72 = new cjs.Graphics().p("AjYJCIAAyDIGxAAIgQSDg");
	var mask_4_graphics_73 = new cjs.Graphics().p("AjYJCIAAyDIGxAAIgQSDg");
	var mask_4_graphics_74 = new cjs.Graphics().p("AjYJCIAAyDIGxAAIgQSDg");
	var mask_4_graphics_75 = new cjs.Graphics().p("AjYJCIAAyDIGxAAIgQSDg");
	var mask_4_graphics_76 = new cjs.Graphics().p("AjYJCIAAyDIGxAAIgQSDg");
	var mask_4_graphics_77 = new cjs.Graphics().p("AjYJCIAAyDIGxAAIgQSDg");
	var mask_4_graphics_78 = new cjs.Graphics().p("AjYJCIAAyDIGxAAIgQSDg");
	var mask_4_graphics_79 = new cjs.Graphics().p("AjYJCIAAyDIGxAAIgQSDg");
	var mask_4_graphics_80 = new cjs.Graphics().p("AkbJCIAAyDII3AAIgISDg");
	var mask_4_graphics_81 = new cjs.Graphics().p("AldJCIAAyDIK7AAIAASDg");

	this.timeline.addTween(cjs.Tween.get(mask_4).to({graphics:mask_4_graphics_0,x:264.375,y:-213.85}).wait(65).to({graphics:mask_4_graphics_65,x:264.375,y:-213.85}).wait(1).to({graphics:mask_4_graphics_66,x:264.375,y:-213.85}).wait(1).to({graphics:mask_4_graphics_67,x:264.375,y:-213.85}).wait(1).to({graphics:mask_4_graphics_68,x:264.375,y:-213.85}).wait(1).to({graphics:mask_4_graphics_69,x:264.375,y:-213.85}).wait(1).to({graphics:mask_4_graphics_70,x:260.175,y:-213.85}).wait(1).to({graphics:mask_4_graphics_71,x:255.975,y:-213.85}).wait(1).to({graphics:mask_4_graphics_72,x:264.375,y:-213.85}).wait(1).to({graphics:mask_4_graphics_73,x:272.775,y:-213.85}).wait(1).to({graphics:mask_4_graphics_74,x:264.375,y:-213.85}).wait(1).to({graphics:mask_4_graphics_75,x:255.975,y:-213.85}).wait(1).to({graphics:mask_4_graphics_76,x:264.375,y:-213.85}).wait(1).to({graphics:mask_4_graphics_77,x:272.775,y:-213.85}).wait(1).to({graphics:mask_4_graphics_78,x:268.575,y:-213.85}).wait(1).to({graphics:mask_4_graphics_79,x:264.375,y:-213.85}).wait(1).to({graphics:mask_4_graphics_80,x:271.025,y:-213.85}).wait(1).to({graphics:mask_4_graphics_81,x:277.65,y:-213.85}).wait(59).to({graphics:null,x:0,y:0}).wait(125));

	// pack_part2_png_obj_
	this.pack_part2_png_1 = new lib.Symbol_1_pack_part2_png_1();
	this.pack_part2_png_1.name = "pack_part2_png_1";
	this.pack_part2_png_1.parent = this;
	this.pack_part2_png_1.setTransform(266.9,-217.1,1,1,0,0,0,266.9,-217.1);
	this.pack_part2_png_1.depth = 0;
	this.pack_part2_png_1.isAttachedToCamera = 0
	this.pack_part2_png_1.isAttachedToMask = 0
	this.pack_part2_png_1.layerDepth = 0
	this.pack_part2_png_1.layerIndex = 10
	this.pack_part2_png_1.maskLayerName = 0

	var maskedShapeInstanceList = [this.pack_part2_png_1];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask_4;
	}

	this.timeline.addTween(cjs.Tween.get(this.pack_part2_png_1).wait(66).to({regX:273.9,regY:-212.8,x:273.9,y:-212.8},0).wait(3).to({regX:266.9,regY:-217.1,x:266.9,y:-217.1},0).wait(1).to({regX:273.9,regY:-212.8,x:273.9,y:-212.8},0).wait(1).to({regX:266.9,regY:-217.1,x:266.9,y:-217.1},0).wait(1).to({regX:273.9,regY:-212.8,x:273.9,y:-212.8},0).wait(1).to({regX:266.9,regY:-217.1,x:266.9,y:-217.1},0).wait(1).to({regX:273.9,regY:-212.8,x:273.9,y:-212.8},0).wait(1).to({regX:266.9,regY:-217.1,x:266.9,y:-217.1},0).wait(1).to({regX:273.9,regY:-212.8,x:273.9,y:-212.8},0).wait(1).to({regX:266.9,regY:-217.1,x:266.9,y:-217.1},0).wait(1).to({regX:273.9,regY:-212.8,x:273.9,y:-212.8},0).wait(1).to({regX:266.9,regY:-217.1,x:266.9,y:-217.1},0).wait(1).to({regX:273.9,regY:-212.8,x:273.9,y:-212.8},0).wait(3).to({regX:266.9,regY:-217.1,x:266.9,y:-217.1},0).wait(182));

	// Layer_2 (mask)
	var mask_5 = new cjs.Shape();
	mask_5._off = true;
	var mask_5_graphics_79 = new cjs.Graphics().p("Ai1OZIAA2bIFrmWIAAcxg");

	this.timeline.addTween(cjs.Tween.get(mask_5).to({graphics:null,x:0,y:0}).wait(79).to({graphics:mask_5_graphics_79,x:266.875,y:-169.375}).wait(6).to({graphics:null,x:0,y:0}).wait(180));

	// pack_part2_shadow_png_obj_
	this.pack_part2_shadow_png = new lib.Symbol_1_pack_part2_shadow_png();
	this.pack_part2_shadow_png.name = "pack_part2_shadow_png";
	this.pack_part2_shadow_png.parent = this;
	this.pack_part2_shadow_png.depth = 0;
	this.pack_part2_shadow_png.isAttachedToCamera = 0
	this.pack_part2_shadow_png.isAttachedToMask = 0
	this.pack_part2_shadow_png.layerDepth = 0
	this.pack_part2_shadow_png.layerIndex = 11
	this.pack_part2_shadow_png.maskLayerName = 0

	var maskedShapeInstanceList = [this.pack_part2_shadow_png];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask_5;
	}

	this.timeline.addTween(cjs.Tween.get(this.pack_part2_shadow_png).wait(80).to({regX:260.6,regY:-184.8,x:260.6,y:-184.8},0).wait(3).to({regX:0,regY:0,x:0,y:0},0).wait(182));

	// pack1_png_obj_
	this.pack1_png_1 = new lib.Symbol_1_pack1_png_1();
	this.pack1_png_1.name = "pack1_png_1";
	this.pack1_png_1.parent = this;
	this.pack1_png_1.setTransform(185.3,-27.8,1,1,0,0,0,185.3,-27.8);
	this.pack1_png_1.depth = 0;
	this.pack1_png_1.isAttachedToCamera = 0
	this.pack1_png_1.isAttachedToMask = 0
	this.pack1_png_1.layerDepth = 0
	this.pack1_png_1.layerIndex = 12
	this.pack1_png_1.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.pack1_png_1).wait(265));

	// pack_part1_png_obj_
	this.pack_part1_png = new lib.Symbol_1_pack_part1_png();
	this.pack_part1_png.name = "pack_part1_png";
	this.pack_part1_png.parent = this;
	this.pack_part1_png.depth = 0;
	this.pack_part1_png.isAttachedToCamera = 0
	this.pack_part1_png.isAttachedToMask = 0
	this.pack_part1_png.layerDepth = 0
	this.pack_part1_png.layerIndex = 13
	this.pack_part1_png.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.pack_part1_png).wait(80).to({regX:77.5,regY:-194,x:77.5,y:-194},0).wait(3).to({regX:0,regY:0,x:0,y:0},0).wait(182));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(15.9,-320.6,327,672.7);


(lib.Scene_1_Layer_11 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_11
	this.instance = new lib.Symbol1("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(248.2,339.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(74).to({startPosition:74,loop:false},0).wait(1).to({regX:179.4,regY:-10.2,x:428.55,y:329.3,startPosition:75},0).wait(1).to({x:431.75,startPosition:76},0).wait(1).to({x:437.8,startPosition:77},0).wait(1).to({x:447.35,startPosition:78},0).wait(1).to({x:461.25,startPosition:79},0).wait(1).to({x:479.85,startPosition:80},0).wait(1).to({x:502.6,startPosition:81},0).wait(1).to({x:527.35,startPosition:82},0).wait(1).to({x:551.25,startPosition:83},0).wait(1).to({x:572.35,startPosition:84},0).wait(1).to({x:590.05,startPosition:85},0).wait(1).to({x:604.6,startPosition:86},0).wait(1).to({x:616.4,startPosition:87},0).wait(1).to({x:625.85,startPosition:88},0).wait(1).to({x:633.35,startPosition:89},0).wait(1).to({x:639.2,startPosition:90},0).wait(1).to({x:643.65,startPosition:91},0).wait(1).to({x:646.95,startPosition:92},0).wait(1).to({x:649.2,startPosition:93},0).wait(1).to({regX:0,regY:0,x:471.2,y:339.5,startPosition:94},0).to({x:501.2,startPosition:169},75).wait(1).to({regX:179.4,regY:-10.2,x:682.05,y:329.3,startPosition:170},0).wait(1).to({x:686.95,startPosition:171},0).wait(1).to({x:696.15,startPosition:172},0).wait(1).to({x:710.75,startPosition:173},0).wait(1).to({x:731.9,startPosition:174},0).wait(1).to({x:760.3,startPosition:175},0).wait(1).to({x:795,startPosition:176},0).wait(1).to({x:832.7,startPosition:177},0).wait(1).to({x:869.1,startPosition:178},0).wait(1).to({x:901.3,startPosition:179},0).wait(1).to({x:928.3,startPosition:180},0).wait(1).to({x:950.5,startPosition:181},0).wait(1).to({x:968.45,startPosition:182},0).wait(1).to({x:982.85,startPosition:183},0).wait(1).to({x:994.3,startPosition:184},0).wait(1).to({x:1003.2,startPosition:185},0).wait(1).to({x:1010.05,startPosition:186},0).wait(1).to({x:1015.05,startPosition:187},0).wait(1).to({x:1018.5,startPosition:188},0).wait(1).to({regX:0,regY:0,x:841.2,y:339.5,startPosition:189},0).wait(55).to({startPosition:244},0).wait(1).to({regX:179.4,regY:-10.2,x:1018,y:329.3,startPosition:245},0).wait(1).to({x:1009.45,startPosition:246},0).wait(1).to({x:993.4,startPosition:247},0).wait(1).to({x:967.95,startPosition:248},0).wait(1).to({x:931.05,startPosition:249},0).wait(1).to({x:881.55,startPosition:250},0).wait(1).to({x:821.05,startPosition:251},0).wait(1).to({x:755.25,startPosition:252},0).wait(1).to({x:691.75,startPosition:253},0).wait(1).to({x:635.65,startPosition:254},0).wait(1).to({x:588.5,startPosition:255},0).wait(1).to({x:549.8,startPosition:256},0).wait(1).to({x:518.5,startPosition:257},0).wait(1).to({x:493.4,startPosition:258},0).wait(1).to({x:473.45,startPosition:259},0).wait(1).to({x:457.85,startPosition:260},0).wait(1).to({x:445.95,startPosition:261},0).wait(1).to({x:437.2,startPosition:262},0).wait(1).to({x:431.2,startPosition:263},0).wait(1).to({regX:0,regY:0,x:248.2,y:339.5,startPosition:264},0).wait(1));

}).prototype = p = new cjs.MovieClip();


// stage content:
(lib.Tp_april = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	this.___GetDepth___ = function(obj) {
		var depth = obj.depth;
		var cameraObj = this.___camera___instance;
		if(cameraObj && cameraObj.depth && obj.isAttachedToCamera)
		{
			depth += depth + cameraObj.depth;
		}
		return depth;
		}
	this.___needSorting___ = function() {
		for (var i = 0; i < this.getNumChildren() - 1; i++)
		{
			var prevDepth = this.___GetDepth___(this.getChildAt(i));
			var nextDepth = this.___GetDepth___(this.getChildAt(i + 1));
			if (prevDepth < nextDepth)
				return true;
		}
		return false;
	}
	this.___sortFunction___ = function(obj1, obj2) {
		return (this.exportRoot.___GetDepth___(obj2) - this.exportRoot.___GetDepth___(obj1));
	}
	this.on('tick', function (event){
		var curTimeline = event.currentTarget;
		if (curTimeline.___needSorting___()){
			this.sortChildren(curTimeline.___sortFunction___);
		}
	});

	// timeline functions:
	this.frame_264 = function() {
		this.___loopingOver___ = true;
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(264).call(this.frame_264).wait(1));

	// Layer_11_obj_
	this.Layer_11 = new lib.Scene_1_Layer_11();
	this.Layer_11.name = "Layer_11";
	this.Layer_11.parent = this;
	this.Layer_11.setTransform(394.1,310.8,1,1,0,0,0,394.1,310.8);
	this.Layer_11.depth = 0;
	this.Layer_11.isAttachedToCamera = 0
	this.Layer_11.isAttachedToMask = 0
	this.Layer_11.layerDepth = 0
	this.Layer_11.layerIndex = 0
	this.Layer_11.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_11).wait(75).to({regX:724.1,regY:329.3,x:724.1,y:329.3},0).wait(19).to({regX:394.1,regY:310.8,x:394.1,y:310.8},0).wait(76).to({regX:724.1,regY:329.3,x:724.1,y:329.3},0).wait(19).to({regX:394.1,regY:310.8,x:394.1,y:310.8},0).wait(56).to({regX:724.1,regY:329.3,x:724.1,y:329.3},0).wait(19).to({regX:394.1,regY:310.8,x:394.1,y:310.8},0).wait(1));

	// Layer_7_obj_
	this.Layer_7 = new lib.Scene_1_Layer_7();
	this.Layer_7.name = "Layer_7";
	this.Layer_7.parent = this;
	this.Layer_7.setTransform(1067.5,207.3,1,1,0,0,0,1067.5,207.3);
	this.Layer_7.depth = 0;
	this.Layer_7.isAttachedToCamera = 0
	this.Layer_7.isAttachedToMask = 0
	this.Layer_7.layerDepth = 0
	this.Layer_7.layerIndex = 1
	this.Layer_7.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_7).wait(265));

	// Layer_6 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	var mask_graphics_14 = new cjs.Graphics().p("EhwfA1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_graphics_15 = new cjs.Graphics().p("EhwcA1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_graphics_16 = new cjs.Graphics().p("EhwaA1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_graphics_17 = new cjs.Graphics().p("EhwXA1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_graphics_18 = new cjs.Graphics().p("EhwVA1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_graphics_19 = new cjs.Graphics().p("EhwSA1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_graphics_20 = new cjs.Graphics().p("EhwQA1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_graphics_21 = new cjs.Graphics().p("EhwNA1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_graphics_22 = new cjs.Graphics().p("EhwLA1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_graphics_23 = new cjs.Graphics().p("EhwJA1IMAAAhqPMDhAAAAMAAABqPg");
	var mask_graphics_24 = new cjs.Graphics().p("EhwGA1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_graphics_25 = new cjs.Graphics().p("EhwEA1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_graphics_26 = new cjs.Graphics().p("EhwBA1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_graphics_27 = new cjs.Graphics().p("Ehv/A1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_graphics_28 = new cjs.Graphics().p("Ehv8A1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_graphics_29 = new cjs.Graphics().p("Ehv6A1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_graphics_30 = new cjs.Graphics().p("Ehv3A1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_graphics_31 = new cjs.Graphics().p("Ehv1A1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_graphics_32 = new cjs.Graphics().p("EhvyA1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_graphics_33 = new cjs.Graphics().p("EhvvA1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_graphics_34 = new cjs.Graphics().p("EhvtA1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_graphics_35 = new cjs.Graphics().p("EhvqA1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_graphics_36 = new cjs.Graphics().p("EhvoA1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_graphics_37 = new cjs.Graphics().p("EhvlA1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_graphics_38 = new cjs.Graphics().p("EhvjA1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_graphics_39 = new cjs.Graphics().p("EhvgA1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_graphics_40 = new cjs.Graphics().p("EhveA1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_graphics_41 = new cjs.Graphics().p("EhvcA1IMAAAhqPMDhAAAAMAAABqPg");
	var mask_graphics_42 = new cjs.Graphics().p("EhvZA1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_graphics_43 = new cjs.Graphics().p("EhvXA1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_graphics_44 = new cjs.Graphics().p("EhvUA1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_graphics_45 = new cjs.Graphics().p("EhvSA1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_graphics_46 = new cjs.Graphics().p("EhvPA1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_graphics_47 = new cjs.Graphics().p("EhvNA1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_graphics_48 = new cjs.Graphics().p("EhvKA1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_graphics_49 = new cjs.Graphics().p("EhvIA1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_graphics_50 = new cjs.Graphics().p("EhvFA1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_graphics_51 = new cjs.Graphics().p("EhvCA1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_graphics_52 = new cjs.Graphics().p("EhvAA1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_graphics_53 = new cjs.Graphics().p("Ehu9A1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_graphics_54 = new cjs.Graphics().p("Ehu7A1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_graphics_55 = new cjs.Graphics().p("Ehu4A1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_graphics_56 = new cjs.Graphics().p("Ehu2A1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_graphics_57 = new cjs.Graphics().p("EhuzA1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_graphics_58 = new cjs.Graphics().p("EhuxA1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_graphics_59 = new cjs.Graphics().p("EhuvA1IMAAAhqPMDhAAAAMAAABqPg");
	var mask_graphics_60 = new cjs.Graphics().p("EhusA1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_graphics_61 = new cjs.Graphics().p("EhuqA1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_graphics_62 = new cjs.Graphics().p("EhunA1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_graphics_63 = new cjs.Graphics().p("EhulA1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_graphics_64 = new cjs.Graphics().p("EhuiA1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_graphics_65 = new cjs.Graphics().p("EhugA1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_graphics_66 = new cjs.Graphics().p("EhudA1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_graphics_67 = new cjs.Graphics().p("EhubA1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_graphics_68 = new cjs.Graphics().p("EhuYA1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_graphics_69 = new cjs.Graphics().p("EhuVA1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_graphics_70 = new cjs.Graphics().p("EhuTA1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_graphics_71 = new cjs.Graphics().p("EhuQA1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_graphics_72 = new cjs.Graphics().p("EhuOA1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_graphics_73 = new cjs.Graphics().p("EhuLA1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_graphics_74 = new cjs.Graphics().p("EhuJA1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_graphics_75 = new cjs.Graphics().p("Eht4A1IMAAAhqPMDgtAAAMAAABqPg");
	var mask_graphics_76 = new cjs.Graphics().p("EhtBA1IMAAAhqPMDf0AAAMAAABqPg");
	var mask_graphics_77 = new cjs.Graphics().p("EhrZA1IMAAAhqPMDeIAAAMAAABqPg");
	var mask_graphics_78 = new cjs.Graphics().p("Eho0A1IMAAAhqPMDbdAAAMAAABqPg");
	var mask_graphics_79 = new cjs.Graphics().p("EhlFA1IMAAAhqPMDXkAAAMAAABqPg");
	var mask_graphics_80 = new cjs.Graphics().p("EhgFA1IMAAAhqPMDSYAAAMAAABqPg");
	var mask_graphics_81 = new cjs.Graphics().p("EhZ9A1IMAAAhqPMDMAAAAMAAABqPg");
	var mask_graphics_82 = new cjs.Graphics().p("EhTTA1IMAAAhqPMDFGAAAMAAABqPg");
	var mask_graphics_83 = new cjs.Graphics().p("EhM4A1IMAAAhqPMC+bAAAMAAABqPg");
	var mask_graphics_84 = new cjs.Graphics().p("EhHMA1IMAAAhqPMC4gAAAMAAABqPg");
	var mask_graphics_85 = new cjs.Graphics().p("EhCbA1IMAAAhqPMCzjAAAMAAABqPg");
	var mask_graphics_86 = new cjs.Graphics().p("Eg+hA1IMAAAhqPMCvgAAAMAAABqPg");
	var mask_graphics_87 = new cjs.Graphics().p("Eg7WA1IMAAAhqPMCsNAAAMAAABqPg");
	var mask_graphics_88 = new cjs.Graphics().p("Eg4zA1IMAAAhqPMCpjAAAMAAABqPg");
	var mask_graphics_89 = new cjs.Graphics().p("Eg2yA1IMAAAhqPMCndAAAMAAABqPg");
	var mask_graphics_90 = new cjs.Graphics().p("Eg1NA1IMAAAhqPMCl0AAAMAAABqPg");
	var mask_graphics_91 = new cjs.Graphics().p("Eg0AA1IMAAAhqPMCkkAAAMAAABqPg");
	var mask_graphics_92 = new cjs.Graphics().p("EgzIA1IMAAAhqPMCjqAAAMAAABqPg");
	var mask_graphics_93 = new cjs.Graphics().p("EgyhA1IMAAAhqPMCjCAAAMAAABqPg");
	var mask_graphics_94 = new cjs.Graphics().p("EgyJA1IMAAAhqPMCipAAAMAAABqPg");
	var mask_graphics_95 = new cjs.Graphics().p("EgyHA1IMAAAhqPMCipAAAMAAABqPg");
	var mask_graphics_96 = new cjs.Graphics().p("EgyFA1IMAAAhqPMCipAAAMAAABqPg");
	var mask_graphics_97 = new cjs.Graphics().p("EgyDA1IMAAAhqPMCipAAAMAAABqPg");
	var mask_graphics_98 = new cjs.Graphics().p("EgyBA1IMAAAhqPMCipAAAMAAABqPg");
	var mask_graphics_99 = new cjs.Graphics().p("Egx/A1IMAAAhqPMCipAAAMAAABqPg");
	var mask_graphics_100 = new cjs.Graphics().p("Egx9A1IMAAAhqPMCipAAAMAAABqPg");
	var mask_graphics_101 = new cjs.Graphics().p("Egx7A1IMAAAhqPMCipAAAMAAABqPg");
	var mask_graphics_102 = new cjs.Graphics().p("Egx5A1IMAAAhqPMCipAAAMAAABqPg");
	var mask_graphics_103 = new cjs.Graphics().p("Egx3A1IMAAAhqPMCipAAAMAAABqPg");
	var mask_graphics_104 = new cjs.Graphics().p("Egx1A1IMAAAhqPMCipAAAMAAABqPg");
	var mask_graphics_105 = new cjs.Graphics().p("EgxzA1IMAAAhqPMCipAAAMAAABqPg");
	var mask_graphics_106 = new cjs.Graphics().p("EgxxA1IMAAAhqPMCipAAAMAAABqPg");
	var mask_graphics_107 = new cjs.Graphics().p("EgxvA1IMAAAhqPMCipAAAMAAABqPg");
	var mask_graphics_108 = new cjs.Graphics().p("EgxtA1IMAAAhqPMCipAAAMAAABqPg");
	var mask_graphics_109 = new cjs.Graphics().p("EgxrA1IMAAAhqPMCipAAAMAAABqPg");
	var mask_graphics_110 = new cjs.Graphics().p("EgxpA1IMAAAhqPMCipAAAMAAABqPg");
	var mask_graphics_111 = new cjs.Graphics().p("EgxnA1IMAAAhqPMCipAAAMAAABqPg");
	var mask_graphics_112 = new cjs.Graphics().p("EgxlA1IMAAAhqPMCipAAAMAAABqPg");
	var mask_graphics_113 = new cjs.Graphics().p("EgxjA1IMAAAhqPMCipAAAMAAABqPg");
	var mask_graphics_114 = new cjs.Graphics().p("EgxhA1IMAAAhqPMCipAAAMAAABqPg");
	var mask_graphics_115 = new cjs.Graphics().p("EgxfA1IMAAAhqPMCipAAAMAAABqPg");
	var mask_graphics_116 = new cjs.Graphics().p("EgxdA1IMAAAhqPMCipAAAMAAABqPg");
	var mask_graphics_117 = new cjs.Graphics().p("EgxbA1IMAAAhqPMCipAAAMAAABqPg");
	var mask_graphics_118 = new cjs.Graphics().p("EgxZA1IMAAAhqPMCipAAAMAAABqPg");
	var mask_graphics_119 = new cjs.Graphics().p("EgxXA1IMAAAhqPMCipAAAMAAABqPg");
	var mask_graphics_120 = new cjs.Graphics().p("EgxVA1IMAAAhqPMCipAAAMAAABqPg");
	var mask_graphics_121 = new cjs.Graphics().p("EgxTA1IMAAAhqPMCipAAAMAAABqPg");
	var mask_graphics_122 = new cjs.Graphics().p("EgxRA1IMAAAhqPMCipAAAMAAABqPg");
	var mask_graphics_123 = new cjs.Graphics().p("EgxPA1IMAAAhqPMCipAAAMAAABqPg");
	var mask_graphics_124 = new cjs.Graphics().p("EgxNA1IMAAAhqPMCipAAAMAAABqPg");
	var mask_graphics_125 = new cjs.Graphics().p("EgxLA1IMAAAhqPMCipAAAMAAABqPg");
	var mask_graphics_126 = new cjs.Graphics().p("EgxJA1IMAAAhqPMCipAAAMAAABqPg");
	var mask_graphics_127 = new cjs.Graphics().p("EgxHA1IMAAAhqPMCipAAAMAAABqPg");
	var mask_graphics_128 = new cjs.Graphics().p("EgxFA1IMAAAhqPMCipAAAMAAABqPg");
	var mask_graphics_129 = new cjs.Graphics().p("EgxDA1IMAAAhqPMCipAAAMAAABqPg");
	var mask_graphics_130 = new cjs.Graphics().p("EgxBA1IMAAAhqPMCipAAAMAAABqPg");
	var mask_graphics_131 = new cjs.Graphics().p("Egw/A1IMAAAhqPMCipAAAMAAABqPg");
	var mask_graphics_132 = new cjs.Graphics().p("Egw9A1IMAAAhqPMCipAAAMAAABqPg");
	var mask_graphics_133 = new cjs.Graphics().p("Egw7A1IMAAAhqPMCipAAAMAAABqPg");
	var mask_graphics_134 = new cjs.Graphics().p("Egw5A1IMAAAhqPMCipAAAMAAABqPg");
	var mask_graphics_135 = new cjs.Graphics().p("Egw3A1IMAAAhqPMCipAAAMAAABqPg");
	var mask_graphics_136 = new cjs.Graphics().p("Egw1A1IMAAAhqPMCipAAAMAAABqPg");
	var mask_graphics_137 = new cjs.Graphics().p("EgwzA1IMAAAhqPMCipAAAMAAABqPg");
	var mask_graphics_138 = new cjs.Graphics().p("EgwxA1IMAAAhqPMCipAAAMAAABqPg");
	var mask_graphics_139 = new cjs.Graphics().p("EgwvA1IMAAAhqPMCipAAAMAAABqPg");
	var mask_graphics_140 = new cjs.Graphics().p("EgwtA1IMAAAhqPMCipAAAMAAABqPg");
	var mask_graphics_141 = new cjs.Graphics().p("EgwrA1IMAAAhqPMCipAAAMAAABqPg");
	var mask_graphics_142 = new cjs.Graphics().p("EgwpA1IMAAAhqPMCipAAAMAAABqPg");
	var mask_graphics_143 = new cjs.Graphics().p("EgwnA1IMAAAhqPMCipAAAMAAABqPg");
	var mask_graphics_144 = new cjs.Graphics().p("EgwlA1IMAAAhqPMCipAAAMAAABqPg");
	var mask_graphics_145 = new cjs.Graphics().p("EgwjA1IMAAAhqPMCipAAAMAAABqPg");
	var mask_graphics_146 = new cjs.Graphics().p("EgwhA1IMAAAhqPMCipAAAMAAABqPg");
	var mask_graphics_147 = new cjs.Graphics().p("EgwfA1IMAAAhqPMCipAAAMAAABqPg");
	var mask_graphics_148 = new cjs.Graphics().p("EgwdA1IMAAAhqPMCipAAAMAAABqPg");
	var mask_graphics_149 = new cjs.Graphics().p("EgwbA1IMAAAhqPMCipAAAMAAABqPg");
	var mask_graphics_150 = new cjs.Graphics().p("EgwZA1IMAAAhqPMCipAAAMAAABqPg");
	var mask_graphics_151 = new cjs.Graphics().p("EgwXA1IMAAAhqPMCipAAAMAAABqPg");
	var mask_graphics_152 = new cjs.Graphics().p("EgwVA1IMAAAhqPMCipAAAMAAABqPg");
	var mask_graphics_153 = new cjs.Graphics().p("EgwTA1IMAAAhqPMCipAAAMAAABqPg");
	var mask_graphics_154 = new cjs.Graphics().p("EgwRA1IMAAAhqPMCipAAAMAAABqPg");
	var mask_graphics_155 = new cjs.Graphics().p("EgwPA1IMAAAhqPMCipAAAMAAABqPg");
	var mask_graphics_156 = new cjs.Graphics().p("EgwNA1IMAAAhqPMCipAAAMAAABqPg");
	var mask_graphics_157 = new cjs.Graphics().p("EgwLA1IMAAAhqPMCipAAAMAAABqPg");
	var mask_graphics_158 = new cjs.Graphics().p("EgwJA1IMAAAhqPMCipAAAMAAABqPg");
	var mask_graphics_159 = new cjs.Graphics().p("EgwHA1IMAAAhqPMCipAAAMAAABqPg");
	var mask_graphics_160 = new cjs.Graphics().p("EgwFA1IMAAAhqPMCipAAAMAAABqPg");
	var mask_graphics_161 = new cjs.Graphics().p("EgwDA1IMAAAhqPMCipAAAMAAABqPg");
	var mask_graphics_162 = new cjs.Graphics().p("EgwBA1IMAAAhqPMCipAAAMAAABqPg");
	var mask_graphics_163 = new cjs.Graphics().p("Egv/A1IMAAAhqPMCipAAAMAAABqPg");
	var mask_graphics_164 = new cjs.Graphics().p("Egv9A1IMAAAhqPMCipAAAMAAABqPg");
	var mask_graphics_165 = new cjs.Graphics().p("Egv7A1IMAAAhqPMCipAAAMAAABqPg");
	var mask_graphics_166 = new cjs.Graphics().p("Egv5A1IMAAAhqPMCipAAAMAAABqPg");
	var mask_graphics_167 = new cjs.Graphics().p("Egv3A1IMAAAhqPMCipAAAMAAABqPg");
	var mask_graphics_168 = new cjs.Graphics().p("Egv1A1IMAAAhqPMCipAAAMAAABqPg");
	var mask_graphics_169 = new cjs.Graphics().p("EgvzA1IMAAAhqPMCipAAAMAAABqPg");
	var mask_graphics_170 = new cjs.Graphics().p("EgveA1IMAAAhqPMCiTAAAMAAABqPg");
	var mask_graphics_171 = new cjs.Graphics().p("EguYA1IMAAAhqPMChLAAAMAAABqPg");
	var mask_graphics_172 = new cjs.Graphics().p("EgsUA1IMAAAhqPMCfDAAAMAAABqPg");
	var mask_graphics_173 = new cjs.Graphics().p("EgpCA1IMAAAhqPMCbrAAAMAAABqPg");
	var mask_graphics_174 = new cjs.Graphics().p("EgkTA1IMAAAhqPMCWzAAAMAAABqPg");
	var mask_graphics_175 = new cjs.Graphics().p("Egd8A1IMAAAhqPMCQPAAAMAAABqPg");
	var mask_graphics_176 = new cjs.Graphics().p("EgWKA1IMAAAhqPMCIOAAAMAAABqPg");
	var mask_graphics_177 = new cjs.Graphics().p("EgNtA1IMAAAhqPMB/hAAAMAAABqPg");
	var mask_graphics_178 = new cjs.Graphics().p("EgFjA1IMAAAhqPMB3IAAAMAAABqPg");
	var mask_graphics_179 = new cjs.Graphics().p("EABpA1IMAAAhqPMBvuAAAMAAABqPg");
	var mask_graphics_180 = new cjs.Graphics().p("EAHtA1IMAAAhqPMBpeAAAMAAABqPg");
	var mask_graphics_181 = new cjs.Graphics().p("EAMrA1IMAAAhqPMBkXAAAMAAABqPg");
	var mask_graphics_182 = new cjs.Graphics().p("EAQsA1IMAAAhqPMBgOAAAMAAABqPg");
	var mask_graphics_183 = new cjs.Graphics().p("EAT7A1IMAAAhqPMBc5AAAMAAABqPg");
	var mask_graphics_184 = new cjs.Graphics().p("EAWfA1IMAAAhqPMBaQAAAMAAABqPg");
	var mask_graphics_185 = new cjs.Graphics().p("EAYfA1IMAAAhqPMBYMAAAMAAABqPg");
	var mask_graphics_186 = new cjs.Graphics().p("EAaBA1IMAAAhqPMBWnAAAMAAABqPg");
	var mask_graphics_187 = new cjs.Graphics().p("EAbJA1IMAAAhqPMBVdAAAMAAABqPg");
	var mask_graphics_188 = new cjs.Graphics().p("EAb6A1IMAAAhqPMBUrAAAMAAABqPg");
	var mask_graphics_189 = new cjs.Graphics().p("EAcYA1IMAAAhqPMBULAAAMAAABqPg");
	var mask_graphics_244 = new cjs.Graphics().p("EAcYA1IMAAAhqPMBULAAAMAAABqPg");
	var mask_graphics_245 = new cjs.Graphics().p("EAbwA1IMAAAhqPMBUzAAAMAAABqPg");
	var mask_graphics_246 = new cjs.Graphics().p("EAZvA1IMAAAhqPMBW0AAAMAAABqPg");
	var mask_graphics_247 = new cjs.Graphics().p("EAV6A1IMAAAhqPMBapAAAMAAABqPg");
	var mask_graphics_248 = new cjs.Graphics().p("EAP3A1IMAAAhqPMBgsAAAMAAABqPg");
	var mask_graphics_249 = new cjs.Graphics().p("EAHHA1IMAAAhqPMBpcAAAMAAABqPg");
	var mask_graphics_250 = new cjs.Graphics().p("EgEpA1IMAAAhqPMB1MAAAMAAABqPg");
	var mask_graphics_251 = new cjs.Graphics().p("EgTBA1IMAAAhqPMCDjAAAMAAABqPg");
	var mask_graphics_252 = new cjs.Graphics().p("EgiqA1IMAAAhqPMCTMAAAMAAABqPg");
	var mask_graphics_253 = new cjs.Graphics().p("EgxvA1IMAAAhqPMCiQAAAMAAABqPg");
	var mask_graphics_254 = new cjs.Graphics().p("Eg/EA1IMAAAhqPMCvkAAAMAAABqPg");
	var mask_graphics_255 = new cjs.Graphics().p("EhKRA1IMAAAhqPMC6xAAAMAAABqPg");
	var mask_graphics_256 = new cjs.Graphics().p("EhTdA1IMAAAhqPMDD9AAAMAAABqPg");
	var mask_graphics_257 = new cjs.Graphics().p("Eha5A1IMAAAhqPMDLZAAAMAAABqPg");
	var mask_graphics_258 = new cjs.Graphics().p("Ehg3A1IMAAAhqPMDRXAAAMAAABqPg");
	var mask_graphics_259 = new cjs.Graphics().p("EhlmA1IMAAAhqPMDWFAAAMAAABqPg");
	var mask_graphics_260 = new cjs.Graphics().p("EhpTA1IMAAAhqPMDZyAAAMAAABqPg");
	var mask_graphics_261 = new cjs.Graphics().p("EhsIA1IMAAAhqPMDcnAAAMAAABqPg");
	var mask_graphics_262 = new cjs.Graphics().p("EhuNA1IMAAAhqPMDesAAAMAAABqPg");
	var mask_graphics_263 = new cjs.Graphics().p("EhvoA1IMAAAhqPMDgHAAAMAAABqPg");
	var mask_graphics_264 = new cjs.Graphics().p("EhwfA1IMAAAhqPMDg/AAAMAAABqPg");

	this.timeline.addTween(cjs.Tween.get(mask).to({graphics:null,x:0,y:0}).wait(14).to({graphics:mask_graphics_14,x:720,y:340}).wait(1).to({graphics:mask_graphics_15,x:720.25,y:340}).wait(1).to({graphics:mask_graphics_16,x:720.5,y:340}).wait(1).to({graphics:mask_graphics_17,x:720.75,y:340}).wait(1).to({graphics:mask_graphics_18,x:721,y:340}).wait(1).to({graphics:mask_graphics_19,x:721.25,y:340}).wait(1).to({graphics:mask_graphics_20,x:721.5,y:340}).wait(1).to({graphics:mask_graphics_21,x:721.75,y:340}).wait(1).to({graphics:mask_graphics_22,x:722,y:340}).wait(1).to({graphics:mask_graphics_23,x:722.25,y:340}).wait(1).to({graphics:mask_graphics_24,x:722.5,y:340}).wait(1).to({graphics:mask_graphics_25,x:722.75,y:340}).wait(1).to({graphics:mask_graphics_26,x:723,y:340}).wait(1).to({graphics:mask_graphics_27,x:723.25,y:340}).wait(1).to({graphics:mask_graphics_28,x:723.5,y:340}).wait(1).to({graphics:mask_graphics_29,x:723.75,y:340}).wait(1).to({graphics:mask_graphics_30,x:724,y:340}).wait(1).to({graphics:mask_graphics_31,x:724.25,y:340}).wait(1).to({graphics:mask_graphics_32,x:724.5,y:340}).wait(1).to({graphics:mask_graphics_33,x:724.75,y:340}).wait(1).to({graphics:mask_graphics_34,x:725,y:340}).wait(1).to({graphics:mask_graphics_35,x:725.25,y:340}).wait(1).to({graphics:mask_graphics_36,x:725.5,y:340}).wait(1).to({graphics:mask_graphics_37,x:725.75,y:340}).wait(1).to({graphics:mask_graphics_38,x:726,y:340}).wait(1).to({graphics:mask_graphics_39,x:726.25,y:340}).wait(1).to({graphics:mask_graphics_40,x:726.5,y:340}).wait(1).to({graphics:mask_graphics_41,x:726.75,y:340}).wait(1).to({graphics:mask_graphics_42,x:727,y:340}).wait(1).to({graphics:mask_graphics_43,x:727.25,y:340}).wait(1).to({graphics:mask_graphics_44,x:727.5,y:340}).wait(1).to({graphics:mask_graphics_45,x:727.75,y:340}).wait(1).to({graphics:mask_graphics_46,x:728,y:340}).wait(1).to({graphics:mask_graphics_47,x:728.25,y:340}).wait(1).to({graphics:mask_graphics_48,x:728.5,y:340}).wait(1).to({graphics:mask_graphics_49,x:728.75,y:340}).wait(1).to({graphics:mask_graphics_50,x:729,y:340}).wait(1).to({graphics:mask_graphics_51,x:729.25,y:340}).wait(1).to({graphics:mask_graphics_52,x:729.5,y:340}).wait(1).to({graphics:mask_graphics_53,x:729.75,y:340}).wait(1).to({graphics:mask_graphics_54,x:730,y:340}).wait(1).to({graphics:mask_graphics_55,x:730.25,y:340}).wait(1).to({graphics:mask_graphics_56,x:730.5,y:340}).wait(1).to({graphics:mask_graphics_57,x:730.75,y:340}).wait(1).to({graphics:mask_graphics_58,x:731,y:340}).wait(1).to({graphics:mask_graphics_59,x:731.25,y:340}).wait(1).to({graphics:mask_graphics_60,x:731.5,y:340}).wait(1).to({graphics:mask_graphics_61,x:731.75,y:340}).wait(1).to({graphics:mask_graphics_62,x:732,y:340}).wait(1).to({graphics:mask_graphics_63,x:732.25,y:340}).wait(1).to({graphics:mask_graphics_64,x:732.5,y:340}).wait(1).to({graphics:mask_graphics_65,x:732.75,y:340}).wait(1).to({graphics:mask_graphics_66,x:733,y:340}).wait(1).to({graphics:mask_graphics_67,x:733.25,y:340}).wait(1).to({graphics:mask_graphics_68,x:733.5,y:340}).wait(1).to({graphics:mask_graphics_69,x:733.75,y:340}).wait(1).to({graphics:mask_graphics_70,x:734,y:340}).wait(1).to({graphics:mask_graphics_71,x:734.25,y:340}).wait(1).to({graphics:mask_graphics_72,x:734.5,y:340}).wait(1).to({graphics:mask_graphics_73,x:734.75,y:340}).wait(1).to({graphics:mask_graphics_74,x:735,y:340}).wait(1).to({graphics:mask_graphics_75,x:734.941,y:340}).wait(1).to({graphics:mask_graphics_76,x:734.7268,y:340}).wait(1).to({graphics:mask_graphics_77,x:734.2992,y:340}).wait(1).to({graphics:mask_graphics_78,x:733.67,y:340}).wait(1).to({graphics:mask_graphics_79,x:732.7432,y:340}).wait(1).to({graphics:mask_graphics_80,x:731.4656,y:340}).wait(1).to({graphics:mask_graphics_81,x:729.9368,y:340}).wait(1).to({graphics:mask_graphics_82,x:728.268,y:340}).wait(1).to({graphics:mask_graphics_83,x:726.6588,y:340.025}).wait(1).to({graphics:mask_graphics_84,x:725.2466,y:340.025}).wait(1).to({graphics:mask_graphics_85,x:724.0395,y:340.025}).wait(1).to({graphics:mask_graphics_86,x:723.0605,y:340.025}).wait(1).to({graphics:mask_graphics_87,x:722.2676,y:340.025}).wait(1).to({graphics:mask_graphics_88,x:721.6434,y:340.025}).wait(1).to({graphics:mask_graphics_89,x:721.137,y:340.025}).wait(1).to({graphics:mask_graphics_90,x:720.7418,y:340.025}).wait(1).to({graphics:mask_graphics_91,x:720.4368,y:340.025}).wait(1).to({graphics:mask_graphics_92,x:720.2201,y:340.025}).wait(1).to({graphics:mask_graphics_93,x:720.0594,y:340.025}).wait(1).to({graphics:mask_graphics_94,x:719.9746,y:340}).wait(1).to({graphics:mask_graphics_95,x:720.1691,y:339.95}).wait(1).to({graphics:mask_graphics_96,x:720.3691,y:339.95}).wait(1).to({graphics:mask_graphics_97,x:720.5691,y:339.95}).wait(1).to({graphics:mask_graphics_98,x:720.7691,y:339.95}).wait(1).to({graphics:mask_graphics_99,x:720.9691,y:339.95}).wait(1).to({graphics:mask_graphics_100,x:721.1691,y:339.95}).wait(1).to({graphics:mask_graphics_101,x:721.3691,y:339.95}).wait(1).to({graphics:mask_graphics_102,x:721.5691,y:339.95}).wait(1).to({graphics:mask_graphics_103,x:721.7691,y:339.95}).wait(1).to({graphics:mask_graphics_104,x:721.9691,y:339.95}).wait(1).to({graphics:mask_graphics_105,x:722.1691,y:339.95}).wait(1).to({graphics:mask_graphics_106,x:722.3691,y:339.95}).wait(1).to({graphics:mask_graphics_107,x:722.5691,y:339.95}).wait(1).to({graphics:mask_graphics_108,x:722.7691,y:339.95}).wait(1).to({graphics:mask_graphics_109,x:722.9691,y:339.95}).wait(1).to({graphics:mask_graphics_110,x:723.1691,y:339.95}).wait(1).to({graphics:mask_graphics_111,x:723.3691,y:339.95}).wait(1).to({graphics:mask_graphics_112,x:723.5691,y:339.95}).wait(1).to({graphics:mask_graphics_113,x:723.7691,y:339.95}).wait(1).to({graphics:mask_graphics_114,x:723.9691,y:339.95}).wait(1).to({graphics:mask_graphics_115,x:724.1691,y:339.95}).wait(1).to({graphics:mask_graphics_116,x:724.3691,y:339.95}).wait(1).to({graphics:mask_graphics_117,x:724.5691,y:339.95}).wait(1).to({graphics:mask_graphics_118,x:724.7691,y:339.95}).wait(1).to({graphics:mask_graphics_119,x:724.9691,y:339.95}).wait(1).to({graphics:mask_graphics_120,x:725.1691,y:339.95}).wait(1).to({graphics:mask_graphics_121,x:725.3691,y:339.95}).wait(1).to({graphics:mask_graphics_122,x:725.5691,y:339.95}).wait(1).to({graphics:mask_graphics_123,x:725.7691,y:339.95}).wait(1).to({graphics:mask_graphics_124,x:725.9691,y:339.95}).wait(1).to({graphics:mask_graphics_125,x:726.1691,y:339.95}).wait(1).to({graphics:mask_graphics_126,x:726.3691,y:339.95}).wait(1).to({graphics:mask_graphics_127,x:726.5691,y:339.95}).wait(1).to({graphics:mask_graphics_128,x:726.7691,y:339.95}).wait(1).to({graphics:mask_graphics_129,x:726.9691,y:339.95}).wait(1).to({graphics:mask_graphics_130,x:727.1691,y:339.95}).wait(1).to({graphics:mask_graphics_131,x:727.3691,y:339.95}).wait(1).to({graphics:mask_graphics_132,x:727.5691,y:339.95}).wait(1).to({graphics:mask_graphics_133,x:727.7691,y:339.95}).wait(1).to({graphics:mask_graphics_134,x:727.9691,y:339.95}).wait(1).to({graphics:mask_graphics_135,x:728.1691,y:339.95}).wait(1).to({graphics:mask_graphics_136,x:728.3691,y:339.95}).wait(1).to({graphics:mask_graphics_137,x:728.5691,y:339.95}).wait(1).to({graphics:mask_graphics_138,x:728.7691,y:339.95}).wait(1).to({graphics:mask_graphics_139,x:728.9691,y:339.95}).wait(1).to({graphics:mask_graphics_140,x:729.1691,y:339.95}).wait(1).to({graphics:mask_graphics_141,x:729.3691,y:339.95}).wait(1).to({graphics:mask_graphics_142,x:729.5691,y:339.95}).wait(1).to({graphics:mask_graphics_143,x:729.7691,y:339.95}).wait(1).to({graphics:mask_graphics_144,x:729.9691,y:339.95}).wait(1).to({graphics:mask_graphics_145,x:730.1691,y:339.95}).wait(1).to({graphics:mask_graphics_146,x:730.3691,y:339.95}).wait(1).to({graphics:mask_graphics_147,x:730.5691,y:339.95}).wait(1).to({graphics:mask_graphics_148,x:730.7691,y:339.95}).wait(1).to({graphics:mask_graphics_149,x:730.9691,y:339.95}).wait(1).to({graphics:mask_graphics_150,x:731.1691,y:339.95}).wait(1).to({graphics:mask_graphics_151,x:731.3691,y:339.95}).wait(1).to({graphics:mask_graphics_152,x:731.5691,y:339.95}).wait(1).to({graphics:mask_graphics_153,x:731.7691,y:339.95}).wait(1).to({graphics:mask_graphics_154,x:731.9691,y:339.95}).wait(1).to({graphics:mask_graphics_155,x:732.1691,y:339.95}).wait(1).to({graphics:mask_graphics_156,x:732.3691,y:339.95}).wait(1).to({graphics:mask_graphics_157,x:732.5691,y:339.95}).wait(1).to({graphics:mask_graphics_158,x:732.7691,y:339.95}).wait(1).to({graphics:mask_graphics_159,x:732.9691,y:339.95}).wait(1).to({graphics:mask_graphics_160,x:733.1691,y:339.95}).wait(1).to({graphics:mask_graphics_161,x:733.3691,y:339.95}).wait(1).to({graphics:mask_graphics_162,x:733.5691,y:339.95}).wait(1).to({graphics:mask_graphics_163,x:733.7691,y:339.95}).wait(1).to({graphics:mask_graphics_164,x:733.9691,y:339.95}).wait(1).to({graphics:mask_graphics_165,x:734.1691,y:339.95}).wait(1).to({graphics:mask_graphics_166,x:734.3691,y:339.95}).wait(1).to({graphics:mask_graphics_167,x:734.5691,y:339.95}).wait(1).to({graphics:mask_graphics_168,x:734.7691,y:339.95}).wait(1).to({graphics:mask_graphics_169,x:734.9746,y:340}).wait(1).to({graphics:mask_graphics_170,x:734.9003,y:339.95}).wait(1).to({graphics:mask_graphics_171,x:734.6876,y:339.95}).wait(1).to({graphics:mask_graphics_172,x:734.2873,y:339.95}).wait(1).to({graphics:mask_graphics_173,x:733.6735,y:339.95}).wait(1).to({graphics:mask_graphics_174,x:732.7677,y:339.95}).wait(1).to({graphics:mask_graphics_175,x:731.5368,y:339.95}).wait(1).to({graphics:mask_graphics_176,x:730.0462,y:339.95}).wait(1).to({graphics:mask_graphics_177,x:728.4406,y:339.95}).wait(1).to({graphics:mask_graphics_178,x:726.8738,y:339.95}).wait(1).to({graphics:mask_graphics_179,x:725.4921,y:339.95}).wait(1).to({graphics:mask_graphics_180,x:724.336,y:339.95}).wait(1).to({graphics:mask_graphics_181,x:723.3762,y:339.95}).wait(1).to({graphics:mask_graphics_182,x:722.5959,y:339.95}).wait(1).to({graphics:mask_graphics_183,x:721.973,y:339.95}).wait(1).to({graphics:mask_graphics_184,x:721.4988,y:339.95}).wait(1).to({graphics:mask_graphics_185,x:721.1029,y:339.95}).wait(1).to({graphics:mask_graphics_186,x:720.8315,y:339.95}).wait(1).to({graphics:mask_graphics_187,x:720.6053,y:339.95}).wait(1).to({graphics:mask_graphics_188,x:720.4614,y:339.95}).wait(1).to({graphics:mask_graphics_189,x:720.3479,y:340}).wait(55).to({graphics:mask_graphics_244,x:720.3479,y:340}).wait(1).to({graphics:mask_graphics_245,x:720.3202,y:340}).wait(1).to({graphics:mask_graphics_246,x:720.3471,y:340}).wait(1).to({graphics:mask_graphics_247,x:720.3,y:340}).wait(1).to({graphics:mask_graphics_248,x:720.318,y:340}).wait(1).to({graphics:mask_graphics_249,x:720.2756,y:340}).wait(1).to({graphics:mask_graphics_250,x:720.2592,y:340}).wait(1).to({graphics:mask_graphics_251,x:720.2035,y:340}).wait(1).to({graphics:mask_graphics_252,x:720.1529,y:340}).wait(1).to({graphics:mask_graphics_253,x:720.1039,y:339.95}).wait(1).to({graphics:mask_graphics_254,x:720.0424,y:339.95}).wait(1).to({graphics:mask_graphics_255,x:720.0361,y:339.95}).wait(1).to({graphics:mask_graphics_256,x:719.9998,y:339.95}).wait(1).to({graphics:mask_graphics_257,x:719.973,y:339.95}).wait(1).to({graphics:mask_graphics_258,x:719.9646,y:339.95}).wait(1).to({graphics:mask_graphics_259,x:719.9452,y:339.95}).wait(1).to({graphics:mask_graphics_260,x:719.9473,y:339.95}).wait(1).to({graphics:mask_graphics_261,x:719.9432,y:339.95}).wait(1).to({graphics:mask_graphics_262,x:719.9361,y:339.95}).wait(1).to({graphics:mask_graphics_263,x:719.9157,y:339.95}).wait(1).to({graphics:mask_graphics_264,x:720,y:340}).wait(1));

	// Layer_5_obj_
	this.Layer_5 = new lib.Scene_1_Layer_5();
	this.Layer_5.name = "Layer_5";
	this.Layer_5.parent = this;
	this.Layer_5.setTransform(720,340,1,1,0,0,0,720,340);
	this.Layer_5.depth = 0;
	this.Layer_5.isAttachedToCamera = 0
	this.Layer_5.isAttachedToMask = 0
	this.Layer_5.layerDepth = 0
	this.Layer_5.layerIndex = 2
	this.Layer_5.maskLayerName = 0

	var maskedShapeInstanceList = [this.Layer_5];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.Layer_5).wait(265));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(720,340,929.7,340);
// library properties:
lib.properties = {
	id: '70307C49E07A104EB48CF442B7FFCC17',
	width: 1440,
	height: 680,
	fps: 30,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: [
		{src:"assets/images/BG60.png", id:"BG"},
		{src:"assets/images/D60.png", id:"D"},
		{src:"assets/images/pack160.png", id:"pack1"},
		{src:"assets/images/pack260.png", id:"pack2"},
		{src:"assets/images/pack360.png", id:"pack3"},
		{src:"assets/images/pack_part160.png", id:"pack_part1"},
		{src:"assets/images/pack_part260.png", id:"pack_part2"},
		{src:"assets/images/pack_part260_shadow.png", id:"pack_part2_shadow"},
		{src:"assets/images/plashka160.png", id:"plashka1"},
		{src:"assets/images/plashka260.png", id:"plashka2"},
		{src:"assets/images/plashka360.png", id:"plashka3"}
	],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['70307C49E07A104EB48CF442B7FFCC17'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}


// Layer depth API :

AdobeAn.Layer = new function() {
	this.getLayerZDepth = function(timeline, layerName)
	{
		if(layerName === "Camera")
		layerName = "___camera___instance";
		var script = "if(timeline." + layerName + ") timeline." + layerName + ".depth; else 0;";
		return eval(script);
	}
	this.setLayerZDepth = function(timeline, layerName, zDepth)
	{
		const MAX_zDepth = 10000;
		const MIN_zDepth = -5000;
		if(zDepth > MAX_zDepth)
			zDepth = MAX_zDepth;
		else if(zDepth < MIN_zDepth)
			zDepth = MIN_zDepth;
		if(layerName === "Camera")
		layerName = "___camera___instance";
		var script = "if(timeline." + layerName + ") timeline." + layerName + ".depth = " + zDepth + ";";
		eval(script);
	}
	this.removeLayer = function(timeline, layerName)
	{
		if(layerName === "Camera")
		layerName = "___camera___instance";
		var script = "if(timeline." + layerName + ") timeline.removeChild(timeline." + layerName + ");";
		eval(script);
	}
	this.addNewLayer = function(timeline, layerName, zDepth)
	{
		if(layerName === "Camera")
		layerName = "___camera___instance";
		zDepth = typeof zDepth !== 'undefined' ? zDepth : 0;
		var layer = new createjs.MovieClip();
		layer.name = layerName;
		layer.depth = zDepth;
		layer.layerIndex = 0;
		timeline.addChild(layer);
	}
}


})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;

$(function(){
	var canvas, stage, exportRoot, anim_container, dom_overlay_container, fnStartAnimation;
	function init() {
		canvas = document.getElementById("canvas60");
		anim_container = document.getElementById("animation_container60");
		dom_overlay_container = document.getElementById("dom_overlay_container60");
		var comp=AdobeAn.getComposition("70307C49E07A104EB48CF442B7FFCC17");
		var lib=comp.getLibrary();
		var loader = new createjs.LoadQueue(false);
		loader.addEventListener("fileload", function(evt){handleFileLoad(evt,comp)});
		loader.addEventListener("complete", function(evt){handleComplete(evt,comp)});
		var lib=comp.getLibrary();
		loader.loadManifest(lib.properties.manifest);
	}
	function handleFileLoad(evt, comp) {
		var images=comp.getImages();
		if (evt && (evt.item.type == "image")) { images[evt.item.id] = evt.result; }
	}
	function handleComplete(evt,comp) {
		//This function is always called, irrespective of the content. You can use the variable "stage" after it is created in token create_stage.
		var lib=comp.getLibrary();
		var ss=comp.getSpriteSheet();
		var queue = evt.target;
		var ssMetadata = lib.ssMetadata;
		for(i=0; i<ssMetadata.length; i++) {
			ss[ssMetadata[i].name] = new createjs.SpriteSheet( {"images": [queue.getResult(ssMetadata[i].name)], "frames": ssMetadata[i].frames} )
		}
		var preloaderDiv = document.getElementById("_preload_div_60");
		preloaderDiv.style.display = 'none';
		canvas.style.display = 'block';
		exportRoot = new lib.Tp_april();
		stage = new lib.Stage(canvas);
		//Registers the "tick" event listener.
		fnStartAnimation = function() {
			stage.addChild(exportRoot);
			createjs.Ticker.setFPS(lib.properties.fps);
			createjs.Ticker.addEventListener("tick", stage)
			stage.addEventListener("tick", handleTick)
			function getProjectionMatrix(container, totalDepth) {
				var focalLength = 528.25;
				var projectionCenter = { x : lib.properties.width/2, y : lib.properties.height/2 };
				var scale = (totalDepth + focalLength)/focalLength;
				var scaleMat = new createjs.Matrix2D;
				scaleMat.a = 1/scale;
				scaleMat.d = 1/scale;
				var projMat = new createjs.Matrix2D;
				projMat.tx = -projectionCenter.x;
				projMat.ty = -projectionCenter.y;
				projMat = projMat.prependMatrix(scaleMat);
				projMat.tx += projectionCenter.x;
				projMat.ty += projectionCenter.y;
				return projMat;
			}
			function handleTick(event) {
				var cameraInstance = exportRoot.___camera___instance;
				if(cameraInstance !== undefined && cameraInstance.pinToObject !== undefined)
				{
					cameraInstance.x = cameraInstance.pinToObject.x + cameraInstance.pinToObject.pinOffsetX;
					cameraInstance.y = cameraInstance.pinToObject.y + cameraInstance.pinToObject.pinOffsetY;
					if(cameraInstance.pinToObject.parent !== undefined && cameraInstance.pinToObject.parent.depth !== undefined)
					cameraInstance.depth = cameraInstance.pinToObject.parent.depth + cameraInstance.pinToObject.pinOffsetZ;
				}
				applyLayerZDepth(exportRoot);
			}
			function applyLayerZDepth(parent)
			{
				var cameraInstance = parent.___camera___instance;
				var focalLength = 528.25;
				var projectionCenter = { 'x' : 0, 'y' : 0};
				if(parent === exportRoot)
				{
					var stageCenter = { 'x' : lib.properties.width/2, 'y' : lib.properties.height/2 };
					projectionCenter.x = stageCenter.x;
					projectionCenter.y = stageCenter.y;
				}
				for(child in parent.children)
				{
					var layerObj = parent.children[child];
					if(layerObj == cameraInstance)
						continue;
					applyLayerZDepth(layerObj, cameraInstance);
					if(layerObj.layerDepth === undefined)
						continue;
					if(layerObj.currentFrame != layerObj.parent.currentFrame)
					{
						layerObj.gotoAndPlay(layerObj.parent.currentFrame);
					}
					var matToApply = new createjs.Matrix2D;
					var cameraMat = new createjs.Matrix2D;
					var totalDepth = layerObj.layerDepth ? layerObj.layerDepth : 0;
					var cameraDepth = 0;
					if(cameraInstance && !layerObj.isAttachedToCamera)
					{
						var mat = cameraInstance.getMatrix();
						mat.tx -= projectionCenter.x;
						mat.ty -= projectionCenter.y;
						cameraMat = mat.invert();
						cameraMat.prependTransform(projectionCenter.x, projectionCenter.y, 1, 1, 0, 0, 0, 0, 0);
						cameraMat.appendTransform(-projectionCenter.x, -projectionCenter.y, 1, 1, 0, 0, 0, 0, 0);
						if(cameraInstance.depth)
							cameraDepth = cameraInstance.depth;
					}
					if(layerObj.depth)
					{
						totalDepth = layerObj.depth;
					}
					//Offset by camera depth
					totalDepth -= cameraDepth;
					if(totalDepth < -focalLength)
					{
						matToApply.a = 0;
						matToApply.d = 0;
					}
					else
					{
						if(layerObj.layerDepth)
						{
							var sizeLockedMat = getProjectionMatrix(parent, layerObj.layerDepth);
							if(sizeLockedMat)
							{
								sizeLockedMat.invert();
								matToApply.prependMatrix(sizeLockedMat);
							}
						}
						matToApply.prependMatrix(cameraMat);
						var projMat = getProjectionMatrix(parent, totalDepth);
						if(projMat)
						{
							matToApply.prependMatrix(projMat);
						}
					}
					layerObj.transformMatrix = matToApply;
				}
			}
		}
		//Code to support hidpi screens and responsive scaling.
		function makeResponsive(isResp, respDim, isScale, scaleType) {
			var lastW, lastH, lastS=1;
			window.addEventListener('resize', resizeCanvas);
			resizeCanvas();
			function resizeCanvas() {
				var w = lib.properties.width, h = lib.properties.height;
				var iw = window.innerWidth, ih=window.innerHeight;
				var pRatio = window.devicePixelRatio || 1, xRatio=iw/w, yRatio=ih/h, sRatio=1;
				if(isResp) {
					if((respDim=='width'&&lastW==iw) || (respDim=='height'&&lastH==ih)) {
						sRatio = lastS;
					}
					else if(!isScale) {
						if(iw<w || ih<h)
							sRatio = Math.min(xRatio, yRatio);
					}
					else if(scaleType==1) {
						sRatio = Math.min(xRatio, yRatio);
					}
					else if(scaleType==2) {
						sRatio = Math.max(xRatio, yRatio);
					}
				}
				canvas.width = w*pRatio*sRatio;
				canvas.height = h*pRatio*sRatio;
				canvas.style.width = anim_container.style.width = dom_overlay_container.style.width = preloaderDiv.style.width = w*sRatio+'px';
				canvas.style.height = anim_container.style.height = dom_overlay_container.style.height = preloaderDiv.style.height = h*sRatio+'px';
				stage.scaleX = pRatio*sRatio;
				stage.scaleY = pRatio*sRatio;
				lastW = iw; lastH = ih; lastS = sRatio;
				stage.tickOnUpdate = false;
				stage.update();
				stage.tickOnUpdate = true;
			}
		}
		makeResponsive(true,'both',true,2);
		AdobeAn.compositionLoaded(lib.properties.id);
		fnStartAnimation();
	}
	init();
});
