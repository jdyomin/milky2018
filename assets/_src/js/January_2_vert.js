(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [];


// symbols:



(lib.bg_vert = function() {
	this.initialize(img.bg_vert);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,748,680);


(lib.materials = function() {
	this.initialize(img.materials);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,237,194);


(lib.pack = function() {
	this.initialize(img.pack);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,397,495);


(lib.shadow = function() {
	this.initialize(img.shadow);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,217,201);// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.Symbol12 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("Eg7jAobMAAAhQ1MB3HAAAMAAABQ1g");
	this.shape.setTransform(4.25,-98.425);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	// Layer_1
	this.instance = new lib.bg_vert();
	this.instance.parent = this;
	this.instance.setTransform(-374,-354.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol12, new cjs.Rectangle(-376.9,-357.1,762.4,682.6), null);


(lib.Symbol11 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.shadow();
	this.instance.parent = this;
	this.instance.setTransform(-108.5,-100.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol11, new cjs.Rectangle(-108.5,-100.5,217,201), null);


(lib.Symbol10 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.pack();
	this.instance.parent = this;
	this.instance.setTransform(-198.5,-247.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol10, new cjs.Rectangle(-198.5,-247.5,397,495), null);


(lib.Symbol9copy5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("ApdpBIS7l+IAAYBIy7F9g");
	mask.setTransform(57.925,-1.5);

	// Layer_1
	this.instance = new lib.materials();
	this.instance.parent = this;
	this.instance.setTransform(-118.5,-97);

	var maskedShapeInstanceList = [this.instance];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol9copy5, new cjs.Rectangle(-2.7,-97,121.2,191.5), null);


(lib.Symbol9copy4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("Apdo8IS7l5IAAXyIy7F6g");
	mask.setTransform(32.425,-0.65);

	// Layer_1
	this.instance = new lib.materials();
	this.instance.parent = this;
	this.instance.setTransform(-118.5,-97);

	var maskedShapeInstanceList = [this.instance];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol9copy4, new cjs.Rectangle(-28.2,-95.7,121.3,190.10000000000002), null);


(lib.Symbol9copy3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("ApdpBIS7l+IAAXqIy7GVg");
	mask.setTransform(8.125,0.5);

	// Layer_1
	this.instance = new lib.materials();
	this.instance.parent = this;
	this.instance.setTransform(-118.5,-97);

	var maskedShapeInstanceList = [this.instance];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol9copy3, new cjs.Rectangle(-52.5,-95.4,121.3,191.9), null);


(lib.Symbol9copy2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("ApdpBIS7l+IAAXWIy7Gog");
	mask.setTransform(-13.775,1.2);

	// Layer_1
	this.instance = new lib.materials();
	this.instance.parent = this;
	this.instance.setTransform(-118.5,-97);

	var maskedShapeInstanceList = [this.instance];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol9copy2, new cjs.Rectangle(-74.4,-94.7,121.30000000000001,191.7), null);


(lib.Symbol9copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("ApdpBIS7l+IgUXWIynGog");
	mask.setTransform(-37.775,1.2);

	// Layer_1
	this.instance = new lib.materials();
	this.instance.parent = this;
	this.instance.setTransform(-118.5,-97);

	var maskedShapeInstanceList = [this.instance];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol9copy, new cjs.Rectangle(-98.4,-94.7,121.30000000000001,191.7), null);


(lib.Symbol9 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("ApdpBIS7l+IAAXYIy7Gmg");
	mask.setTransform(-57.875,-0.6);

	// Layer_1
	this.instance = new lib.materials();
	this.instance.parent = this;
	this.instance.setTransform(-118.5,-97);

	var maskedShapeInstanceList = [this.instance];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol9, new cjs.Rectangle(-118.5,-96.5,121.3,191.9), null);


(lib.Symbol6 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(12.5,1,1).p("ACYBTIkvil");
	this.shape.setTransform(0.025,0.025);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol6, new cjs.Rectangle(-21.4,-14.5,42.9,29.1), null);


(lib.Symbol5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(12.5,1,1).p("ACahaIkzC1");
	this.shape.setTransform(0.025,0.025);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol5, new cjs.Rectangle(-21.6,-15.3,43.3,30.700000000000003), null);


(lib.Symbol4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(12.5,1,1).p("AiZBcIEzi3");
	this.shape.setTransform(0.025,0);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol4, new cjs.Rectangle(-21.6,-15.4,43.3,30.8), null);


(lib.Symbol3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(12.5,1,1).p("AiahRIE1Cj");
	this.shape.setTransform(0.025,0.025);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol3, new cjs.Rectangle(-21.7,-14.4,43.5,28.9), null);


(lib.Symbol2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(12.5,1,1).p("AgyhZQCqAEh1Cv");
	this.shape.setTransform(6.667,-4.05);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	// Layer_1
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(12.5,1,1).p("Ag5gTQB4hKgFCL");
	this.shape_1.setTransform(0.0119,-0.0015);

	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol2, new cjs.Rectangle(-12,-19.3,30,30.5), null);


(lib.Symbol1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_3
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(12.5,1,1).p("AAYiEQBOBxiXCY");
	this.shape.setTransform(10.2273,-13);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	// Layer_2
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(12.5,1,1).p("Ag1h+QCcBUhICp");
	this.shape_1.setTransform(2.2111,-10.925);

	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(1));

	// Layer_1
	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(12.5,1,1).p("ABAANQhFgJg6gQ");

	this.timeline.addTween(cjs.Tween.get(this.shape_2).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol1, new cjs.Rectangle(-12.6,-32.5,34.1,40.6), null);


(lib.Symbol_8_Layer_7 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_7
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(12.5,1,1).p("AFjlHQAZEwheCoQhOCMiWAhQh7AciGgxQh7gtglhE");
	this.shape.setTransform(41.7793,25.5943);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(12.5,1,1).p("AloC4QAmBEB6AsQCHAxB7gbQCWgiBOiMQBeingYkw");
	this.shape_1.setTransform(41.661,25.5693);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(12.5,1,1).p("AlrC5QAoBDB7AsQCGAwB7gbQCVgiBQiLQBfingVkv");
	this.shape_2.setTransform(41.2827,25.4758);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(12.5,1,1).p("AlwC7QAsBCB6ArQCFAvB7gcQCWgiBRiLQBhilgPkt");
	this.shape_3.setTransform(40.6068,25.3227);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(12.5,1,1).p("Al4C+QAyBAB5ApQCFAtB7gcQCVgiBViKQBjikgHkq");
	this.shape_4.setTransform(39.6263,25.0776);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(12.5,1,1).p("AmFDDQA7A9B4AmQCDArB8geQCUgiBZiIQBoihADkn");
	this.shape_5.setTransform(38.35,24.7543);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(12.5,1,1).p("AmVDIQBGA6B3AkQCCAmB7geQCTgjBgiGQBsieASkh");
	this.shape_6.setTransform(36.725,24.3319);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(12.5,1,1).p("AmpDPQBTA1B2AgQCAAiB8gfQCRgkBniEQBziZAjkb");
	this.shape_7.setTransform(34.7,23.8086);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(12.5,1,1).p("AnBDXQBjAwB0AbQB9AdB9ggQCPglBwiBQB7iUA4kU");
	this.shape_8.setTransform(32.275,23.2018);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(12.5,1,1).p("AndDhQB1AqBzAVQB6AXB9giQCNglB7h+QCDiOBRkM");
	this.shape_9.setTransform(29.525,22.5302);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(12.5,1,1).p("An6DqQCIAjBxAQQB3ARB+gkQCLgmCFh7QCNiIBqkD");
	this.shape_10.setTransform(26.575,21.8612);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(12.5,1,1).p("AoYDzQCcAdBvAKQB0ALB/gmQCIgoCQh2QCXiCCEj6");
	this.shape_11.setTransform(23.6,21.2247);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(12.5,1,1).p("Ao0D7QCvAXBtAEQBxAFCAgnQCGgpCahzQCfh8Cdjy");
	this.shape_12.setTransform(20.85,20.6784);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(12.5,1,1).p("ApMEBQC/ASBrgBQBuAACAgoQCFgqCjhwQCnh3Cyjr");
	this.shape_13.setTransform(18.425,20.2507);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(12.5,1,1).p("ApgEGQDMANBqgFQBsgECBgpQCDgrCqhuQCthyDEjl");
	this.shape_14.setTransform(16.4,19.9579);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(12.5,1,1).p("ApwEIQDXAKBpgHQBqgICAgqQCCgrCxhtQCyhvDSjf");
	this.shape_15.setTransform(14.775,19.8107);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(12.5,1,1).p("Ap9ELQDgAHBogKQBpgLCBgrQCAgrC2hrQC2hsDdjc");
	this.shape_16.setTransform(13.525,19.7441);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(12.5,1,1).p("AqGELQDmAGBngMQBogNCBgrQCAgsC5hpQC5hrDljZ");
	this.shape_17.setTransform(12.625,19.7114);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#000000").ss(12.5,1,1).p("AqMEMQDqAEBmgNQBogNCBgsQCAgsC7hpQC7hpDqjY");
	this.shape_18.setTransform(12.05,19.6971);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#000000").ss(12.5,1,1).p("AqPEMQDrADBngNQBngPCCgsQB/gsC8hoQC8hpDtjW");
	this.shape_19.setTransform(11.7,19.7023);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#000000").ss(12.5,1,1).p("AKRkMQjuDWi8BoQi8BpiAAsQiBAshnAOQhnAPjsgE");
	this.shape_20.setTransform(11.6,19.7021);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#000000").ss(12.5,1,1).p("AqMEMQDpAEBngNQBogOCBgrQCAgsC7hpQC7hqDqjX");
	this.shape_21.setTransform(12,19.6971);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#000000").ss(12.5,1,1).p("AqHEMQDmAFBogMQBogNCBgrQCAgsC5hpQC5hrDmjZ");
	this.shape_22.setTransform(12.55,19.6985);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#000000").ss(12.5,1,1).p("Ap+ELQDgAHBogLQBpgLCBgrQCAgrC2hrQC3hsDejb");
	this.shape_23.setTransform(13.375,19.74);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#000000").ss(12.5,1,1).p("ApzEJQDZAJBogIQBqgJCBgqQCCgrCyhsQCzhvDUje");
	this.shape_24.setTransform(14.5,19.8007);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f().s("#000000").ss(12.5,1,1).p("ApkEGQDPANBqgFQBrgGCBgpQCCgrCthtQCuhyDHjj");
	this.shape_25.setTransform(15.975,19.9214);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f().s("#000000").ss(12.5,1,1).p("ApSECQDDARBrgCQBugBCAgpQCEgpClhwQCph2C3jp");
	this.shape_26.setTransform(17.85,20.1563);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f().s("#000000").ss(12.5,1,1).p("Ao7D9QC0AVBsADQBwAECAgoQCFgpCdhyQChh7Ckjv");
	this.shape_27.setTransform(20.125,20.5392);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f().s("#000000").ss(12.5,1,1).p("AohD2QCiAbBuAIQBzAJCAgmQCHgoCUh2QCYiACNj3");
	this.shape_28.setTransform(22.75,21.0371);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f().s("#000000").ss(12.5,1,1).p("AoEDtQCPAiBwANQB2APB/gkQCJgnCJh5QCQiGBzkA");
	this.shape_29.setTransform(25.625,21.6456);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f().s("#000000").ss(12.5,1,1).p("AnmDjQB7AoByAUQB5AWB+gjQCMgmB+h9QCGiMBZkJ");
	this.shape_30.setTransform(28.625,22.3246);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f().s("#000000").ss(12.5,1,1).p("AnJDaQBoAuB0AZQB8AcB8ghQCQglBziAQB9iSA/kS");
	this.shape_31.setTransform(31.5,23.0176);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f().s("#000000").ss(12.5,1,1).p("AmvDRQBXA0B1AeQB/AiB8ggQCRgkBpiDQB1iYApka");
	this.shape_32.setTransform(34.075,23.6567);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f().s("#000000").ss(12.5,1,1).p("AmZDKQBIA4B3AjQCBAmB8geQCSgkBiiGQBuicAVkg");
	this.shape_33.setTransform(36.275,24.2167);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f().s("#000000").ss(12.5,1,1).p("AmIDEQA9A8B4AmQCDAqB8gdQCTgjBbiIQBpigAGkm");
	this.shape_34.setTransform(38.05,24.6715);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f().s("#000000").ss(12.5,1,1).p("Al6C/QAzA/B6ApQCEAtB7gdQCVgiBWiKQBkijgGkp");
	this.shape_35.setTransform(39.417,25.0313);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f().s("#000000").ss(12.5,1,1).p("AlxC7QAtBCB6ArQCFAuB7gcQCWgiBSiLQBhilgPks");
	this.shape_36.setTransform(40.4943,25.3012);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f().s("#000000").ss(12.5,1,1).p("AlrC5QAoBDB7AsQCGAwB7gcQCVghBQiMQBfimgUkv");
	this.shape_37.setTransform(41.2319,25.4658);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f().s("#000000").ss(12.5,1,1).p("AloC4QAmBEB7AsQCGAxB7gbQCWgiBOiMQBeingXkw");
	this.shape_38.setTransform(41.635,25.5693);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},4).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_20}]},5).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_27}]},1).to({state:[{t:this.shape_28}]},1).to({state:[{t:this.shape_29}]},1).to({state:[{t:this.shape_30}]},1).to({state:[{t:this.shape_31}]},1).to({state:[{t:this.shape_32}]},1).to({state:[{t:this.shape_33}]},1).to({state:[{t:this.shape_34}]},1).to({state:[{t:this.shape_35}]},1).to({state:[{t:this.shape_36}]},1).to({state:[{t:this.shape_37}]},1).to({state:[{t:this.shape_38}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},5).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_20}]},5).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_27}]},1).to({state:[{t:this.shape_28}]},1).to({state:[{t:this.shape_29}]},1).to({state:[{t:this.shape_30}]},1).to({state:[{t:this.shape_31}]},1).to({state:[{t:this.shape_32}]},1).to({state:[{t:this.shape_33}]},1).to({state:[{t:this.shape_34}]},1).to({state:[{t:this.shape_35}]},1).to({state:[{t:this.shape_36}]},1).to({state:[{t:this.shape_37}]},1).to({state:[{t:this.shape_38}]},1).to({state:[{t:this.shape}]},1).wait(11));
	this.timeline.addTween(cjs.Tween.get(this.shape).wait(4).to({_off:true},1).wait(44).to({_off:false},0).wait(5).to({_off:true},1).wait(44).to({_off:false},0).wait(11));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_8_Layer_6 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_6
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(12.5,1,1).p("AnrhZIPXCz");
	this.shape.setTransform(-105.9,8.925);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(12.5,1,1).p("AnqhZQHqBbHrBY");
	this.shape_1.setTransform(-105.8,8.925);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(12.5,1,1).p("AnnhZQHnBgHoBT");
	this.shape_2.setTransform(-105.5,8.925);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(12.5,1,1).p("AnhhZQHgBqHjBJ");
	this.shape_3.setTransform(-104.925,8.925);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(12.5,1,1).p("AnZhZQHXB4HcA7");
	this.shape_4.setTransform(-104.075,8.925);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(12.5,1,1).p("AnNhZQHJCMHSAn");
	this.shape_5.setTransform(-102.925,8.95);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(12.5,1,1).p("Am+hZQG4ClHFAO");
	this.shape_6.setTransform(-101.4,8.95);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(12.5,1,1).p("AmrhZQGiDFG1gT");
	this.shape_7.setTransform(-99.5,9.0332);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(12.5,1,1).p("AmUheQGHDrGig5");
	this.shape_8.setTransform(-97.2,9.5382);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(12.5,1,1).p("Al6hmQFpEXGMhl");
	this.shape_9.setTransform(-94.6,10.3252);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(12.5,1,1).p("AlehvQFJFFF0iU");
	this.shape_10.setTransform(-91.825,11.3213);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(12.5,1,1).p("AlCh6QEoF1FejE");
	this.shape_11.setTransform(-89.05,12.3585);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(12.5,1,1).p("AkpiEQELGhFIjw");
	this.shape_12.setTransform(-86.45,13.3653);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(12.5,1,1).p("AkRiMQDvHGE1kW");
	this.shape_13.setTransform(-84.15,14.2932);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(12.5,1,1).p("Aj+iUQDZHnEkk3");
	this.shape_14.setTransform(-82.25,15.062);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(12.5,1,1).p("AjviaQDIIAEXlQ");
	this.shape_15.setTransform(-80.725,15.6699);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(12.5,1,1).p("AjkifQC7IUEOlk");
	this.shape_16.setTransform(-79.575,16.178);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(12.5,1,1).p("AjbiiQCxIiEGly");
	this.shape_17.setTransform(-78.725,16.5146);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#000000").ss(12.5,1,1).p("AjVikQCqIrECl7");
	this.shape_18.setTransform(-78.15,16.7434);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#000000").ss(12.5,1,1).p("AjTimQCnIxD/mB");
	this.shape_19.setTransform(-77.85,16.876);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#000000").ss(12.5,1,1).p("AjRimQClIyD+mC");
	this.shape_20.setTransform(-77.75,16.9119);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#000000").ss(12.5,1,1).p("AjVilQCqIsEBl8");
	this.shape_21.setTransform(-78.125,16.7554);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#000000").ss(12.5,1,1).p("AjaijQCwIkEGl0");
	this.shape_22.setTransform(-78.65,16.5507);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#000000").ss(12.5,1,1).p("AjiifQC4IWENlm");
	this.shape_23.setTransform(-79.4,16.238);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#000000").ss(12.5,1,1).p("AjtibQDFIEEWlU");
	this.shape_24.setTransform(-80.475,15.8064);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f().s("#000000").ss(12.5,1,1).p("Aj7iWQDWHuEhk9");
	this.shape_25.setTransform(-81.875,15.2166);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f().s("#000000").ss(12.5,1,1).p("AkMiPQDpHQEwkf");
	this.shape_26.setTransform(-83.625,14.5053);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f().s("#000000").ss(12.5,1,1).p("AkiiGQECGrFDj7");
	this.shape_27.setTransform(-85.75,13.6608);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f().s("#000000").ss(12.5,1,1).p("Ak6h9QEfGDFWjS");
	this.shape_28.setTransform(-88.225,12.6759);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f().s("#000000").ss(12.5,1,1).p("AlWhyQE/FUFtij");
	this.shape_29.setTransform(-90.95,11.6468);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f().s("#000000").ss(12.5,1,1).p("AlyhpQFfEmGGh0");
	this.shape_30.setTransform(-93.75,10.614);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f().s("#000000").ss(12.5,1,1).p("AmNhgQF/D4GchG");
	this.shape_31.setTransform(-96.475,9.7442);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f().s("#000000").ss(12.5,1,1).p("AmlhaQGbDPGwgd");
	this.shape_32.setTransform(-98.9,9.1274);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f().s("#000000").ss(12.5,1,1).p("Am6hZQGzCtHCAG");
	this.shape_33.setTransform(-100.975,8.95);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f().s("#000000").ss(12.5,1,1).p("AnKhZQHGCRHPAi");
	this.shape_34.setTransform(-102.625,8.95);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f().s("#000000").ss(12.5,1,1).p("AnXhZQHVB7HaA4");
	this.shape_35.setTransform(-103.9,8.925);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f().s("#000000").ss(12.5,1,1).p("AnghZQHfBrHiBI");
	this.shape_36.setTransform(-104.825,8.925);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f().s("#000000").ss(12.5,1,1).p("AnnhZQHnBhHnBS");
	this.shape_37.setTransform(-105.45,8.925);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},4).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_20}]},5).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_27}]},1).to({state:[{t:this.shape_28}]},1).to({state:[{t:this.shape_29}]},1).to({state:[{t:this.shape_30}]},1).to({state:[{t:this.shape_31}]},1).to({state:[{t:this.shape_32}]},1).to({state:[{t:this.shape_33}]},1).to({state:[{t:this.shape_34}]},1).to({state:[{t:this.shape_35}]},1).to({state:[{t:this.shape_36}]},1).to({state:[{t:this.shape_37}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},5).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_20}]},5).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_27}]},1).to({state:[{t:this.shape_28}]},1).to({state:[{t:this.shape_29}]},1).to({state:[{t:this.shape_30}]},1).to({state:[{t:this.shape_31}]},1).to({state:[{t:this.shape_32}]},1).to({state:[{t:this.shape_33}]},1).to({state:[{t:this.shape_34}]},1).to({state:[{t:this.shape_35}]},1).to({state:[{t:this.shape_36}]},1).to({state:[{t:this.shape_37}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape}]},1).wait(11));
	this.timeline.addTween(cjs.Tween.get(this.shape).wait(4).to({_off:true},1).wait(44).to({_off:false},0).wait(5).to({_off:true},1).wait(44).to({_off:false},0).wait(11));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_7_Layer_6 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_6
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(12.5,1,1).p("AjdhHQAEA/A9AlQA8AmBNAEQBMAFBCgfQBBggAihA");
	this.shape.setTransform(-1.575,25.0785);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(12.5,1,1).p("AjnhKQAFBCBDApQBCApBUABQBUAAA/giQA+gjAgg/");
	this.shape_1.setTransform(-1.35,24.8504);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(12.5,1,1).p("AjxhNQAHBGBIAsQBHAsBbgDQBbgCA8gmQA8gmAfg/");
	this.shape_2.setTransform(-1.15,24.6345);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(12.5,1,1).p("Aj4hRQAHBJBNAvQBNAvBfgFQBhgGA6goQA7goAchA");
	this.shape_3.setTransform(-1,24.5038);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(12.5,1,1).p("AkAhTQAJBLBRAxQBQAyBlgIQBmgJA5gqQA4gqAbhA");
	this.shape_4.setTransform(-0.85,24.4057);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(12.5,1,1).p("AkGhVQAKBNBUA0QBUAzBpgKQBqgLA3gsQA3gsAag/");
	this.shape_5.setTransform(-0.7,24.2889);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(12.5,1,1).p("AkLhXQAKBPBYA1QBXA1BsgMQBugMA1guQA2guAZg/");
	this.shape_6.setTransform(-0.6,24.2349);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(12.5,1,1).p("AkOhZQAKBRBaA2QBZA3BvgOQBwgOA1gvQA0gvAYg/");
	this.shape_7.setTransform(-0.525,24.158);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(12.5,1,1).p("AkRhaQALBSBbA3QBbA4BxgPQBygPA0gwQA0gwAXg+");
	this.shape_8.setTransform(-0.475,24.1085);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(12.5,1,1).p("AkThbQAMBSBbA4QBcA4BygPQBzgPA0gxQAzgwAYg/");
	this.shape_9.setTransform(-0.425,24.1085);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(12.5,1,1).p("AkThbQALBTBcA3QBcA5BzgPQBzgQA0gxQAzgxAXg+");
	this.shape_10.setTransform(-0.425,24.0893);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(12.5,1,1).p("AkJhXQAKBPBWA1QBWA1BrgMQBsgMA3gtQA2guAZg/");
	this.shape_11.setTransform(-0.65,24.2358);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(12.5,1,1).p("Aj4hQQAIBJBMAuQBLAvBggGQBggFA7goQA6gnAdhA");
	this.shape_12.setTransform(-1,24.5288);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(12.5,1,1).p("AjqhLQAFBEBFApQBEArBXgBQBVAAA+gkQA+gkAfg/");
	this.shape_13.setTransform(-1.275,24.7753);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(12.5,1,1).p("AjlhKQAFBDBBAoQBBAoBUABQBSACBAgiQA/gjAfg/");
	this.shape_14.setTransform(-1.4,24.8532);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(12.5,1,1).p("AjihIQAFBAA/AnQA/AnBRADQBQADBAghQBAghAhhA");
	this.shape_15.setTransform(-1.475,24.9627);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(12.5,1,1).p("AjfhHQAEA/A+AmQA9AmBPAEQBOAEBBggQBAggAihA");
	this.shape_16.setTransform(-1.525,25.0425);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(12.5,1,1).p("AjdhHQAEA/A9AlQA8AmBOAEQBMAFBCggQBBgfAhhA");
	this.shape_17.setTransform(-1.575,25.0531);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},4).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_10}]},75).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape}]},1).wait(11));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_8_Symbol_2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Symbol_2
	this.instance = new lib.Symbol2();
	this.instance.parent = this;
	this.instance.setTransform(-153.2,0.25,1,1,0,0,0,6.6,5.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(5).to({regX:3,regY:-4.1,x:-156.6,y:-9.35},0).wait(1).to({x:-156},0).wait(1).to({x:-154.9},0).wait(1).to({x:-153.25},0).wait(1).to({x:-151},0).wait(1).to({x:-148.05},0).wait(1).to({x:-144.4},0).wait(1).to({x:-139.95},0).wait(1).to({x:-134.95},0).wait(1).to({x:-129.6},0).wait(1).to({x:-124.2},0).wait(1).to({x:-119.2},0).wait(1).to({x:-114.75},0).wait(1).to({x:-111.1},0).wait(1).to({x:-108.15},0).wait(1).to({x:-105.9},0).wait(1).to({x:-104.25},0).wait(1).to({x:-103.15},0).wait(1).to({x:-102.55},0).wait(1).to({regX:6.6,regY:5.5,x:-98.8,y:0.25},0).wait(6).to({regX:3,regY:-4.1,x:-102.55,y:-9.35},0).wait(1).to({x:-103.1},0).wait(1).to({x:-104.1},0).wait(1).to({x:-105.6},0).wait(1).to({x:-107.65},0).wait(1).to({x:-110.35},0).wait(1).to({x:-113.75},0).wait(1).to({x:-117.85},0).wait(1).to({x:-122.65},0).wait(1).to({x:-127.9},0).wait(1).to({x:-133.3},0).wait(1).to({x:-138.55},0).wait(1).to({x:-143.25},0).wait(1).to({x:-147.25},0).wait(1).to({x:-150.45},0).wait(1).to({x:-152.95},0).wait(1).to({x:-154.7},0).wait(1).to({x:-155.9},0).wait(1).to({x:-156.55},0).wait(1).to({regX:6.6,regY:5.5,x:-153.2,y:0.25},0).wait(6).to({regX:3,regY:-4.1,x:-156.6,y:-9.35},0).wait(1).to({x:-156},0).wait(1).to({x:-154.9},0).wait(1).to({x:-153.25},0).wait(1).to({x:-151},0).wait(1).to({x:-148.05},0).wait(1).to({x:-144.4},0).wait(1).to({x:-139.95},0).wait(1).to({x:-134.95},0).wait(1).to({x:-129.6},0).wait(1).to({x:-124.2},0).wait(1).to({x:-119.2},0).wait(1).to({x:-114.75},0).wait(1).to({x:-111.1},0).wait(1).to({x:-108.15},0).wait(1).to({x:-105.9},0).wait(1).to({x:-104.25},0).wait(1).to({x:-103.15},0).wait(1).to({x:-102.55},0).wait(1).to({regX:6.6,regY:5.5,x:-98.8,y:0.25},0).wait(6).to({regX:3,regY:-4.1,x:-102.55,y:-9.35},0).wait(1).to({x:-103.1},0).wait(1).to({x:-104.1},0).wait(1).to({x:-105.6},0).wait(1).to({x:-107.65},0).wait(1).to({x:-110.35},0).wait(1).to({x:-113.75},0).wait(1).to({x:-117.85},0).wait(1).to({x:-122.65},0).wait(1).to({x:-127.9},0).wait(1).to({x:-133.3},0).wait(1).to({x:-138.55},0).wait(1).to({x:-143.25},0).wait(1).to({x:-147.25},0).wait(1).to({x:-150.45},0).wait(1).to({x:-152.95},0).wait(1).to({x:-154.7},0).wait(1).to({x:-155.9},0).wait(1).to({x:-156.55},0).wait(1).to({regX:6.6,regY:5.5,x:-153.2,y:0.25},0).wait(11));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_8_Symbol_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Symbol_1
	this.instance = new lib.Symbol1();
	this.instance.parent = this;
	this.instance.setTransform(5.25,46.05,1,1,0,0,0,7.5,3.1);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(5).to({regX:4.5,regY:-12.2,x:2.05,y:30.75},0).wait(1).to({x:1.4},0).wait(1).to({x:0.15,y:30.8},0).wait(1).to({x:-1.65,y:30.9},0).wait(1).to({x:-4.2,y:31},0).wait(1).to({x:-7.5,y:31.1},0).wait(1).to({x:-11.65,y:31.25},0).wait(1).to({x:-16.65,y:31.45},0).wait(1).to({x:-22.3,y:31.7},0).wait(1).to({x:-28.35,y:31.95},0).wait(1).to({x:-34.35,y:32.15},0).wait(1).to({x:-40,y:32.4},0).wait(1).to({x:-45,y:32.6},0).wait(1).to({x:-49.15,y:32.75},0).wait(1).to({x:-52.45,y:32.85},0).wait(1).to({x:-55,y:32.95},0).wait(1).to({x:-56.8,y:33.05},0).wait(1).to({x:-58.05,y:33.1},0).wait(1).to({x:-58.7},0).wait(1).to({regX:7.5,regY:3.1,x:-55.95,y:48.45},0).wait(6).to({regX:4.5,regY:-12.2,x:-58.75,y:33.1},0).wait(1).to({x:-58.1},0).wait(1).to({x:-57,y:33.05},0).wait(1).to({x:-55.3,y:33},0).wait(1).to({x:-53,y:32.9},0).wait(1).to({x:-49.95,y:32.75},0).wait(1).to({x:-46.15,y:32.6},0).wait(1).to({x:-41.5,y:32.45},0).wait(1).to({x:-36.15,y:32.25},0).wait(1).to({x:-30.25,y:32},0).wait(1).to({x:-24.1,y:31.75},0).wait(1).to({x:-18.25,y:31.55},0).wait(1).to({x:-12.95,y:31.3},0).wait(1).to({x:-8.45,y:31.15},0).wait(1).to({x:-4.85,y:31},0).wait(1).to({x:-2.05,y:30.9},0).wait(1).to({x:-0.05,y:30.8},0).wait(1).to({x:1.3,y:30.75},0).wait(1).to({x:2.05},0).wait(1).to({regX:7.5,regY:3.1,x:5.25,y:46.05},0).wait(6).to({regX:4.5,regY:-12.2,x:2.05,y:30.75},0).wait(1).to({x:1.4},0).wait(1).to({x:0.15,y:30.8},0).wait(1).to({x:-1.65,y:30.9},0).wait(1).to({x:-4.2,y:31},0).wait(1).to({x:-7.5,y:31.1},0).wait(1).to({x:-11.65,y:31.25},0).wait(1).to({x:-16.65,y:31.45},0).wait(1).to({x:-22.3,y:31.7},0).wait(1).to({x:-28.35,y:31.95},0).wait(1).to({x:-34.35,y:32.15},0).wait(1).to({x:-40,y:32.4},0).wait(1).to({x:-45,y:32.6},0).wait(1).to({x:-49.15,y:32.75},0).wait(1).to({x:-52.45,y:32.85},0).wait(1).to({x:-55,y:32.95},0).wait(1).to({x:-56.8,y:33.05},0).wait(1).to({x:-58.05,y:33.1},0).wait(1).to({x:-58.7},0).wait(1).to({regX:7.5,regY:3.1,x:-55.95,y:48.45},0).wait(6).to({regX:4.5,regY:-12.2,x:-58.75,y:33.1},0).wait(1).to({x:-58.1},0).wait(1).to({x:-57,y:33.05},0).wait(1).to({x:-55.3,y:33},0).wait(1).to({x:-53,y:32.9},0).wait(1).to({x:-49.95,y:32.75},0).wait(1).to({x:-46.15,y:32.6},0).wait(1).to({x:-41.5,y:32.45},0).wait(1).to({x:-36.15,y:32.25},0).wait(1).to({x:-30.25,y:32},0).wait(1).to({x:-24.1,y:31.75},0).wait(1).to({x:-18.25,y:31.55},0).wait(1).to({x:-12.95,y:31.3},0).wait(1).to({x:-8.45,y:31.15},0).wait(1).to({x:-4.85,y:31},0).wait(1).to({x:-2.05,y:30.9},0).wait(1).to({x:-0.05,y:30.8},0).wait(1).to({x:1.3,y:30.75},0).wait(1).to({x:2.05},0).wait(1).to({regX:7.5,regY:3.1,x:5.25,y:46.05},0).wait(11));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_8_materials_png_5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// materials_png
	this.instance = new lib.Symbol9();
	this.instance.parent = this;
	this.instance.setTransform(-66.5,54);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(5).to({regX:-57.9,regY:-0.6,scaleX:0.9999,scaleY:0.9999,x:-124.2,y:53.35},0).wait(1).to({scaleX:0.9997,scaleY:0.9997,x:-123.6},0).wait(1).to({scaleX:0.9993,scaleY:0.9993,x:-122.45},0).wait(1).to({scaleX:0.9987,scaleY:0.9987,x:-120.75},0).wait(1).to({scaleX:0.9979,scaleY:0.9979,x:-118.5},0).wait(1).to({scaleX:0.9968,scaleY:0.9968,x:-115.45},0).wait(1).to({scaleX:0.9954,scaleY:0.9954,x:-111.75,y:53.3},0).wait(1).to({scaleX:0.9938,scaleY:0.9938,x:-107.2},0).wait(1).to({scaleX:0.992,scaleY:0.992,x:-102.1,y:53.25},0).wait(1).to({scaleX:0.99,scaleY:0.99,x:-96.6,y:53.2},0).wait(1).to({scaleX:0.988,scaleY:0.988,x:-91.1},0).wait(1).to({scaleX:0.9862,scaleY:0.9862,x:-86,y:53.15},0).wait(1).to({scaleX:0.9846,scaleY:0.9846,x:-81.45},0).wait(1).to({scaleX:0.9832,scaleY:0.9832,x:-77.75,y:53.1},0).wait(1).to({scaleX:0.9821,scaleY:0.9821,x:-74.7},0).wait(1).to({scaleX:0.9813,scaleY:0.9813,x:-72.4},0).wait(1).to({scaleX:0.9807,scaleY:0.9807,x:-70.75},0).wait(1).to({scaleX:0.9803,scaleY:0.9803,x:-69.6},0).wait(1).to({scaleX:0.9801,scaleY:0.9801,x:-69},0).wait(1).to({regX:0,regY:0,scaleX:0.98,scaleY:0.98,x:-12.1,y:53.65},0).wait(5).to({x:-12.45},0).wait(1).to({regX:-57.9,regY:-0.6,scaleX:0.9801,scaleY:0.9801,x:-69.35,y:53.05},0).wait(1).to({scaleX:0.9803,scaleY:0.9803,x:-69.9},0).wait(1).to({scaleX:0.9806,scaleY:0.9806,x:-70.95},0).wait(1).to({scaleX:0.9812,scaleY:0.9812,x:-72.4},0).wait(1).to({scaleX:0.9819,scaleY:0.9819,x:-74.5},0).wait(1).to({scaleX:0.9829,scaleY:0.9829,x:-77.25},0).wait(1).to({scaleX:0.9842,scaleY:0.9842,x:-80.7,y:53.1},0).wait(1).to({scaleX:0.9857,scaleY:0.9857,x:-84.85},0).wait(1).to({scaleX:0.9874,scaleY:0.9874,x:-89.7,y:53.15},0).wait(1).to({scaleX:0.9894,scaleY:0.9894,x:-95.05},0).wait(1).to({scaleX:0.9914,scaleY:0.9914,x:-100.55,y:53.2},0).wait(1).to({scaleX:0.9933,scaleY:0.9933,x:-105.85},0).wait(1).to({scaleX:0.995,scaleY:0.995,x:-110.65,y:53.25},0).wait(1).to({scaleX:0.9965,scaleY:0.9965,x:-114.7},0).wait(1).to({scaleX:0.9977,scaleY:0.9977,x:-117.95,y:53.3},0).wait(1).to({scaleX:0.9986,scaleY:0.9986,x:-120.45},0).wait(1).to({scaleX:0.9992,scaleY:0.9992,x:-122.3},0).wait(1).to({scaleX:0.9997,scaleY:0.9997,x:-123.5},0).wait(1).to({scaleX:0.9999,scaleY:0.9999,x:-124.15},0).wait(1).to({regX:0,regY:0,scaleX:1,scaleY:1,x:-66.5,y:54},0).wait(6).to({regX:-57.9,regY:-0.6,scaleX:0.9999,scaleY:0.9999,x:-124.2,y:53.35},0).wait(1).to({scaleX:0.9997,scaleY:0.9997,x:-123.6},0).wait(1).to({scaleX:0.9993,scaleY:0.9993,x:-122.45},0).wait(1).to({scaleX:0.9987,scaleY:0.9987,x:-120.75},0).wait(1).to({scaleX:0.9979,scaleY:0.9979,x:-118.5},0).wait(1).to({scaleX:0.9968,scaleY:0.9968,x:-115.45},0).wait(1).to({scaleX:0.9954,scaleY:0.9954,x:-111.75,y:53.3},0).wait(1).to({scaleX:0.9938,scaleY:0.9938,x:-107.2},0).wait(1).to({scaleX:0.992,scaleY:0.992,x:-102.1,y:53.25},0).wait(1).to({scaleX:0.99,scaleY:0.99,x:-96.6,y:53.2},0).wait(1).to({scaleX:0.988,scaleY:0.988,x:-91.1},0).wait(1).to({scaleX:0.9862,scaleY:0.9862,x:-86,y:53.15},0).wait(1).to({scaleX:0.9846,scaleY:0.9846,x:-81.45},0).wait(1).to({scaleX:0.9832,scaleY:0.9832,x:-77.75,y:53.1},0).wait(1).to({scaleX:0.9821,scaleY:0.9821,x:-74.7},0).wait(1).to({scaleX:0.9813,scaleY:0.9813,x:-72.4},0).wait(1).to({scaleX:0.9807,scaleY:0.9807,x:-70.75},0).wait(1).to({scaleX:0.9803,scaleY:0.9803,x:-69.6},0).wait(1).to({scaleX:0.9801,scaleY:0.9801,x:-69},0).wait(1).to({regX:0,regY:0,scaleX:0.98,scaleY:0.98,x:-12.1,y:53.65},0).wait(5).to({x:-12.45},0).wait(1).to({regX:-57.9,regY:-0.6,scaleX:0.9801,scaleY:0.9801,x:-69.35,y:53.05},0).wait(1).to({scaleX:0.9803,scaleY:0.9803,x:-69.9},0).wait(1).to({scaleX:0.9806,scaleY:0.9806,x:-70.95},0).wait(1).to({scaleX:0.9812,scaleY:0.9812,x:-72.4},0).wait(1).to({scaleX:0.9819,scaleY:0.9819,x:-74.5},0).wait(1).to({scaleX:0.9829,scaleY:0.9829,x:-77.25},0).wait(1).to({scaleX:0.9842,scaleY:0.9842,x:-80.7,y:53.1},0).wait(1).to({scaleX:0.9857,scaleY:0.9857,x:-84.85},0).wait(1).to({scaleX:0.9874,scaleY:0.9874,x:-89.7,y:53.15},0).wait(1).to({scaleX:0.9894,scaleY:0.9894,x:-95.05},0).wait(1).to({scaleX:0.9914,scaleY:0.9914,x:-100.55,y:53.2},0).wait(1).to({scaleX:0.9933,scaleY:0.9933,x:-105.85},0).wait(1).to({scaleX:0.995,scaleY:0.995,x:-110.65,y:53.25},0).wait(1).to({scaleX:0.9965,scaleY:0.9965,x:-114.7},0).wait(1).to({scaleX:0.9977,scaleY:0.9977,x:-117.95,y:53.3},0).wait(1).to({scaleX:0.9986,scaleY:0.9986,x:-120.45},0).wait(1).to({scaleX:0.9992,scaleY:0.9992,x:-122.3},0).wait(1).to({scaleX:0.9997,scaleY:0.9997,x:-123.5},0).wait(1).to({scaleX:0.9999,scaleY:0.9999,x:-124.15},0).wait(1).to({regX:0,regY:0,scaleX:1,scaleY:1,x:-66.5,y:54},0).wait(11));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_8_materials_png_4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// materials_png
	this.instance = new lib.Symbol9copy();
	this.instance.parent = this;
	this.instance.setTransform(-66.5,54);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(5).to({regX:-37.8,regY:1.1,scaleX:0.9999,scaleY:0.9999,x:-104.15,y:55.05},0).wait(1).to({scaleX:0.9997,scaleY:0.9997,x:-103.75},0).wait(1).to({scaleX:0.9993,scaleY:0.9993,x:-103,y:55},0).wait(1).to({scaleX:0.9987,scaleY:0.9987,x:-101.95},0).wait(1).to({scaleX:0.9979,scaleY:0.9979,x:-100.45,y:54.9},0).wait(1).to({scaleX:0.9968,scaleY:0.9968,x:-98.55,y:54.85},0).wait(1).to({scaleX:0.9954,scaleY:0.9954,x:-96.15,y:54.75},0).wait(1).to({scaleX:0.9938,scaleY:0.9938,x:-93.2,y:54.6},0).wait(1).to({scaleX:0.992,scaleY:0.992,x:-89.9,y:54.45},0).wait(1).to({scaleX:0.99,scaleY:0.99,x:-86.35,y:54.3},0).wait(1).to({scaleX:0.988,scaleY:0.988,x:-82.8,y:54.2},0).wait(1).to({scaleX:0.9862,scaleY:0.9862,x:-79.5,y:54.05},0).wait(1).to({scaleX:0.9846,scaleY:0.9846,x:-76.55,y:53.9},0).wait(1).to({scaleX:0.9832,scaleY:0.9832,x:-74.15,y:53.8},0).wait(1).to({scaleX:0.9821,scaleY:0.9821,x:-72.2,y:53.75},0).wait(1).to({scaleX:0.9813,scaleY:0.9813,x:-70.75,y:53.65},0).wait(1).to({scaleX:0.9807,scaleY:0.9807,x:-69.65},0).wait(1).to({scaleX:0.9803,scaleY:0.9803,x:-68.95,y:53.6},0).wait(1).to({scaleX:0.9801,scaleY:0.9801,x:-68.55},0).wait(1).to({regX:0,regY:0,scaleX:0.98,scaleY:0.98,x:-31.4,y:52.45},0).wait(5).to({x:-32.15},0).wait(1).to({regX:-37.8,regY:1.1,scaleX:0.9801,scaleY:0.9801,x:-69.3,y:53.55},0).wait(1).to({scaleX:0.9803,scaleY:0.9803,x:-69.65},0).wait(1).to({scaleX:0.9806,scaleY:0.9806,x:-70.25},0).wait(1).to({scaleX:0.9812,scaleY:0.9812,x:-71.25,y:53.6},0).wait(1).to({scaleX:0.9819,scaleY:0.9819,x:-72.55,y:53.65},0).wait(1).to({scaleX:0.9829,scaleY:0.9829,x:-74.3,y:53.75},0).wait(1).to({scaleX:0.9842,scaleY:0.9842,x:-76.5,y:53.85},0).wait(1).to({scaleX:0.9857,scaleY:0.9857,x:-79.15,y:53.95},0).wait(1).to({scaleX:0.9874,scaleY:0.9874,x:-82.25,y:54.1},0).wait(1).to({scaleX:0.9894,scaleY:0.9894,x:-85.65,y:54.25},0).wait(1).to({scaleX:0.9914,scaleY:0.9914,x:-89.1,y:54.4},0).wait(1).to({scaleX:0.9933,scaleY:0.9933,x:-92.5,y:54.5},0).wait(1).to({scaleX:0.995,scaleY:0.995,x:-95.55,y:54.65},0).wait(1).to({scaleX:0.9965,scaleY:0.9965,x:-98.1,y:54.75},0).wait(1).to({scaleX:0.9977,scaleY:0.9977,x:-100.2,y:54.85},0).wait(1).to({scaleX:0.9986,scaleY:0.9986,x:-101.8,y:54.9},0).wait(1).to({scaleX:0.9992,scaleY:0.9992,x:-102.95,y:54.95},0).wait(1).to({scaleX:0.9997,scaleY:0.9997,x:-103.75,y:55},0).wait(1).to({scaleX:0.9999,scaleY:0.9999,x:-104.15},0).wait(1).to({regX:0,regY:0,scaleX:1,scaleY:1,x:-66.5,y:54},0).wait(6).to({regX:-37.8,regY:1.1,scaleX:0.9999,scaleY:0.9999,x:-104.15,y:55.05},0).wait(1).to({scaleX:0.9997,scaleY:0.9997,x:-103.75},0).wait(1).to({scaleX:0.9993,scaleY:0.9993,x:-103,y:55},0).wait(1).to({scaleX:0.9987,scaleY:0.9987,x:-101.95},0).wait(1).to({scaleX:0.9979,scaleY:0.9979,x:-100.45,y:54.9},0).wait(1).to({scaleX:0.9968,scaleY:0.9968,x:-98.55,y:54.85},0).wait(1).to({scaleX:0.9954,scaleY:0.9954,x:-96.15,y:54.75},0).wait(1).to({scaleX:0.9938,scaleY:0.9938,x:-93.2,y:54.6},0).wait(1).to({scaleX:0.992,scaleY:0.992,x:-89.9,y:54.45},0).wait(1).to({scaleX:0.99,scaleY:0.99,x:-86.35,y:54.3},0).wait(1).to({scaleX:0.988,scaleY:0.988,x:-82.8,y:54.2},0).wait(1).to({scaleX:0.9862,scaleY:0.9862,x:-79.5,y:54.05},0).wait(1).to({scaleX:0.9846,scaleY:0.9846,x:-76.55,y:53.9},0).wait(1).to({scaleX:0.9832,scaleY:0.9832,x:-74.15,y:53.8},0).wait(1).to({scaleX:0.9821,scaleY:0.9821,x:-72.2,y:53.75},0).wait(1).to({scaleX:0.9813,scaleY:0.9813,x:-70.75,y:53.65},0).wait(1).to({scaleX:0.9807,scaleY:0.9807,x:-69.65},0).wait(1).to({scaleX:0.9803,scaleY:0.9803,x:-68.95,y:53.6},0).wait(1).to({scaleX:0.9801,scaleY:0.9801,x:-68.55},0).wait(1).to({regX:0,regY:0,scaleX:0.98,scaleY:0.98,x:-31.4,y:52.45},0).wait(5).to({x:-32.15},0).wait(1).to({regX:-37.8,regY:1.1,scaleX:0.9801,scaleY:0.9801,x:-69.3,y:53.55},0).wait(1).to({scaleX:0.9803,scaleY:0.9803,x:-69.65},0).wait(1).to({scaleX:0.9806,scaleY:0.9806,x:-70.25},0).wait(1).to({scaleX:0.9812,scaleY:0.9812,x:-71.25,y:53.6},0).wait(1).to({scaleX:0.9819,scaleY:0.9819,x:-72.55,y:53.65},0).wait(1).to({scaleX:0.9829,scaleY:0.9829,x:-74.3,y:53.75},0).wait(1).to({scaleX:0.9842,scaleY:0.9842,x:-76.5,y:53.85},0).wait(1).to({scaleX:0.9857,scaleY:0.9857,x:-79.15,y:53.95},0).wait(1).to({scaleX:0.9874,scaleY:0.9874,x:-82.25,y:54.1},0).wait(1).to({scaleX:0.9894,scaleY:0.9894,x:-85.65,y:54.25},0).wait(1).to({scaleX:0.9914,scaleY:0.9914,x:-89.1,y:54.4},0).wait(1).to({scaleX:0.9933,scaleY:0.9933,x:-92.5,y:54.5},0).wait(1).to({scaleX:0.995,scaleY:0.995,x:-95.55,y:54.65},0).wait(1).to({scaleX:0.9965,scaleY:0.9965,x:-98.1,y:54.75},0).wait(1).to({scaleX:0.9977,scaleY:0.9977,x:-100.2,y:54.85},0).wait(1).to({scaleX:0.9986,scaleY:0.9986,x:-101.8,y:54.9},0).wait(1).to({scaleX:0.9992,scaleY:0.9992,x:-102.95,y:54.95},0).wait(1).to({scaleX:0.9997,scaleY:0.9997,x:-103.75,y:55},0).wait(1).to({scaleX:0.9999,scaleY:0.9999,x:-104.15},0).wait(1).to({regX:0,regY:0,scaleX:1,scaleY:1,x:-66.5,y:54},0).wait(11));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_8_materials_png_3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// materials_png
	this.instance = new lib.Symbol9copy2();
	this.instance.parent = this;
	this.instance.setTransform(-66.5,54);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(5).to({regX:-13.8,regY:1.1,scaleX:0.9999,scaleY:0.9999,x:-80.25,y:55.05},0).wait(1).to({scaleX:0.9997,scaleY:0.9997,x:-80.1},0).wait(1).to({scaleX:0.9993,scaleY:0.9993,x:-79.9},0).wait(1).to({scaleX:0.9987,scaleY:0.9987,x:-79.6,y:55},0).wait(1).to({scaleX:0.9979,scaleY:0.9979,x:-79.1,y:54.95},0).wait(1).to({scaleX:0.9968,scaleY:0.9968,x:-78.5,y:54.9},0).wait(1).to({scaleX:0.9954,scaleY:0.9954,x:-77.75,y:54.8},0).wait(1).to({scaleX:0.9938,scaleY:0.9938,x:-76.85,y:54.7},0).wait(1).to({scaleX:0.992,scaleY:0.992,x:-75.85,y:54.6},0).wait(1).to({scaleX:0.99,scaleY:0.99,x:-74.7,y:54.45},0).wait(1).to({scaleX:0.988,scaleY:0.988,x:-73.65,y:54.35},0).wait(1).to({scaleX:0.9862,scaleY:0.9862,x:-72.6,y:54.25},0).wait(1).to({scaleX:0.9846,scaleY:0.9846,x:-71.75,y:54.15},0).wait(1).to({scaleX:0.9832,scaleY:0.9832,x:-70.95,y:54.05},0).wait(1).to({scaleX:0.9821,scaleY:0.9821,x:-70.35,y:54},0).wait(1).to({scaleX:0.9813,scaleY:0.9813,x:-69.9,y:53.95},0).wait(1).to({scaleX:0.9807,scaleY:0.9807,x:-69.6,y:53.9},0).wait(1).to({scaleX:0.9803,scaleY:0.9803,x:-69.4},0).wait(1).to({scaleX:0.9801,scaleY:0.9801,x:-69.2},0).wait(1).to({regX:0,regY:0,scaleX:0.98,scaleY:0.98,x:-55.65,y:52.75},0).wait(5).to({x:-55.95},0).wait(1).to({regX:-13.8,regY:1.1,scaleX:0.9801,scaleY:0.9801,x:-69.45,y:53.85},0).wait(1).to({scaleX:0.9803,scaleY:0.9803,x:-69.6},0).wait(1).to({scaleX:0.9806,scaleY:0.9806,x:-69.8},0).wait(1).to({scaleX:0.9812,scaleY:0.9812,x:-70.1,y:53.9},0).wait(1).to({scaleX:0.9819,scaleY:0.9819,x:-70.5,y:53.95},0).wait(1).to({scaleX:0.9829,scaleY:0.9829,x:-71,y:54},0).wait(1).to({scaleX:0.9842,scaleY:0.9842,x:-71.7,y:54.1},0).wait(1).to({scaleX:0.9857,scaleY:0.9857,x:-72.5,y:54.15},0).wait(1).to({scaleX:0.9874,scaleY:0.9874,x:-73.5,y:54.25},0).wait(1).to({scaleX:0.9894,scaleY:0.9894,x:-74.5,y:54.4},0).wait(1).to({scaleX:0.9914,scaleY:0.9914,x:-75.6,y:54.5},0).wait(1).to({scaleX:0.9933,scaleY:0.9933,x:-76.6,y:54.6},0).wait(1).to({scaleX:0.995,scaleY:0.995,x:-77.55,y:54.75},0).wait(1).to({scaleX:0.9965,scaleY:0.9965,x:-78.35,y:54.8},0).wait(1).to({scaleX:0.9977,scaleY:0.9977,x:-78.95,y:54.9},0).wait(1).to({scaleX:0.9986,scaleY:0.9986,x:-79.5,y:54.95},0).wait(1).to({scaleX:0.9992,scaleY:0.9992,x:-79.85,y:55},0).wait(1).to({scaleX:0.9997,scaleY:0.9997,x:-80.05},0).wait(1).to({scaleX:0.9999,scaleY:0.9999,x:-80.2},0).wait(1).to({regX:0,regY:0,scaleX:1,scaleY:1,x:-66.5,y:54},0).wait(6).to({regX:-13.8,regY:1.1,scaleX:0.9999,scaleY:0.9999,x:-80.25,y:55.05},0).wait(1).to({scaleX:0.9997,scaleY:0.9997,x:-80.1},0).wait(1).to({scaleX:0.9993,scaleY:0.9993,x:-79.9},0).wait(1).to({scaleX:0.9987,scaleY:0.9987,x:-79.6,y:55},0).wait(1).to({scaleX:0.9979,scaleY:0.9979,x:-79.1,y:54.95},0).wait(1).to({scaleX:0.9968,scaleY:0.9968,x:-78.5,y:54.9},0).wait(1).to({scaleX:0.9954,scaleY:0.9954,x:-77.75,y:54.8},0).wait(1).to({scaleX:0.9938,scaleY:0.9938,x:-76.85,y:54.7},0).wait(1).to({scaleX:0.992,scaleY:0.992,x:-75.85,y:54.6},0).wait(1).to({scaleX:0.99,scaleY:0.99,x:-74.7,y:54.45},0).wait(1).to({scaleX:0.988,scaleY:0.988,x:-73.65,y:54.35},0).wait(1).to({scaleX:0.9862,scaleY:0.9862,x:-72.6,y:54.25},0).wait(1).to({scaleX:0.9846,scaleY:0.9846,x:-71.75,y:54.15},0).wait(1).to({scaleX:0.9832,scaleY:0.9832,x:-70.95,y:54.05},0).wait(1).to({scaleX:0.9821,scaleY:0.9821,x:-70.35,y:54},0).wait(1).to({scaleX:0.9813,scaleY:0.9813,x:-69.9,y:53.95},0).wait(1).to({scaleX:0.9807,scaleY:0.9807,x:-69.6,y:53.9},0).wait(1).to({scaleX:0.9803,scaleY:0.9803,x:-69.4},0).wait(1).to({scaleX:0.9801,scaleY:0.9801,x:-69.2},0).wait(1).to({regX:0,regY:0,scaleX:0.98,scaleY:0.98,x:-55.65,y:52.75},0).wait(5).to({x:-55.95},0).wait(1).to({regX:-13.8,regY:1.1,scaleX:0.9801,scaleY:0.9801,x:-69.45,y:53.85},0).wait(1).to({scaleX:0.9803,scaleY:0.9803,x:-69.6},0).wait(1).to({scaleX:0.9806,scaleY:0.9806,x:-69.8},0).wait(1).to({scaleX:0.9812,scaleY:0.9812,x:-70.1,y:53.9},0).wait(1).to({scaleX:0.9819,scaleY:0.9819,x:-70.5,y:53.95},0).wait(1).to({scaleX:0.9829,scaleY:0.9829,x:-71,y:54},0).wait(1).to({scaleX:0.9842,scaleY:0.9842,x:-71.7,y:54.1},0).wait(1).to({scaleX:0.9857,scaleY:0.9857,x:-72.5,y:54.15},0).wait(1).to({scaleX:0.9874,scaleY:0.9874,x:-73.5,y:54.25},0).wait(1).to({scaleX:0.9894,scaleY:0.9894,x:-74.5,y:54.4},0).wait(1).to({scaleX:0.9914,scaleY:0.9914,x:-75.6,y:54.5},0).wait(1).to({scaleX:0.9933,scaleY:0.9933,x:-76.6,y:54.6},0).wait(1).to({scaleX:0.995,scaleY:0.995,x:-77.55,y:54.75},0).wait(1).to({scaleX:0.9965,scaleY:0.9965,x:-78.35,y:54.8},0).wait(1).to({scaleX:0.9977,scaleY:0.9977,x:-78.95,y:54.9},0).wait(1).to({scaleX:0.9986,scaleY:0.9986,x:-79.5,y:54.95},0).wait(1).to({scaleX:0.9992,scaleY:0.9992,x:-79.85,y:55},0).wait(1).to({scaleX:0.9997,scaleY:0.9997,x:-80.05},0).wait(1).to({scaleX:0.9999,scaleY:0.9999,x:-80.2},0).wait(1).to({regX:0,regY:0,scaleX:1,scaleY:1,x:-66.5,y:54},0).wait(11));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_8_materials_png_2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// materials_png
	this.instance = new lib.Symbol9copy3();
	this.instance.parent = this;
	this.instance.setTransform(-66.5,54);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(5).to({regX:8.1,regY:0.5,scaleX:0.9999,scaleY:0.9999,x:-58.4,y:54.45},0).wait(1).to({scaleX:0.9997,scaleY:0.9997,x:-58.55},0).wait(1).to({scaleX:0.9993,scaleY:0.9993,x:-58.75},0).wait(1).to({scaleX:0.9987,scaleY:0.9987,x:-59.05},0).wait(1).to({scaleX:0.9979,scaleY:0.9979,x:-59.5,y:54.4},0).wait(1).to({scaleX:0.9968,scaleY:0.9968,x:-60.15},0).wait(1).to({scaleX:0.9954,scaleY:0.9954,x:-60.9,y:54.35},0).wait(1).to({scaleX:0.9938,scaleY:0.9938,x:-61.75,y:54.3},0).wait(1).to({scaleX:0.992,scaleY:0.992,x:-62.75,y:54.25},0).wait(1).to({scaleX:0.99,scaleY:0.99,x:-63.9,y:54.15},0).wait(1).to({scaleX:0.988,scaleY:0.988,x:-64.95,y:54.1},0).wait(1).to({scaleX:0.9862,scaleY:0.9862,x:-65.95,y:54.05},0).wait(1).to({scaleX:0.9846,scaleY:0.9846,x:-66.85,y:54},0).wait(1).to({scaleX:0.9832,scaleY:0.9832,x:-67.6,y:53.95},0).wait(1).to({scaleX:0.9821,scaleY:0.9821,x:-68.2},0).wait(1).to({scaleX:0.9813,scaleY:0.9813,x:-68.65,y:53.9},0).wait(1).to({scaleX:0.9807,scaleY:0.9807,x:-68.95},0).wait(1).to({scaleX:0.9803,scaleY:0.9803,x:-69.15},0).wait(1).to({scaleX:0.9801,scaleY:0.9801,x:-69.3},0).wait(1).to({regX:0,regY:0,scaleX:0.98,scaleY:0.98,x:-77.3,y:53.35},0).wait(5).to({x:-77.15},0).wait(1).to({regX:8.1,regY:0.5,scaleX:0.9801,scaleY:0.9801,x:-69.15,y:53.85},0).wait(1).to({scaleX:0.9803,scaleY:0.9803,x:-69.05},0).wait(1).to({scaleX:0.9806,scaleY:0.9806,x:-68.85},0).wait(1).to({scaleX:0.9812,scaleY:0.9812,x:-68.55},0).wait(1).to({scaleX:0.9819,scaleY:0.9819,x:-68.15,y:53.9},0).wait(1).to({scaleX:0.9829,scaleY:0.9829,x:-67.6},0).wait(1).to({scaleX:0.9842,scaleY:0.9842,x:-66.95,y:53.95},0).wait(1).to({scaleX:0.9857,scaleY:0.9857,x:-66.1,y:54},0).wait(1).to({scaleX:0.9874,scaleY:0.9874,x:-65.15,y:54.05},0).wait(1).to({scaleX:0.9894,scaleY:0.9894,x:-64.15,y:54.1},0).wait(1).to({scaleX:0.9914,scaleY:0.9914,x:-63,y:54.15},0).wait(1).to({scaleX:0.9933,scaleY:0.9933,x:-62,y:54.2},0).wait(1).to({scaleX:0.995,scaleY:0.995,x:-61.05,y:54.3},0).wait(1).to({scaleX:0.9965,scaleY:0.9965,x:-60.3},0).wait(1).to({scaleX:0.9977,scaleY:0.9977,x:-59.6,y:54.35},0).wait(1).to({scaleX:0.9986,scaleY:0.9986,x:-59.15,y:54.4},0).wait(1).to({scaleX:0.9992,scaleY:0.9992,x:-58.8},0).wait(1).to({scaleX:0.9997,scaleY:0.9997,x:-58.55},0).wait(1).to({scaleX:0.9999,scaleY:0.9999,x:-58.4},0).wait(1).to({regX:0,regY:0,scaleX:1,scaleY:1,x:-66.5,y:54},0).wait(6).to({regX:8.1,regY:0.5,scaleX:0.9999,scaleY:0.9999,x:-58.4,y:54.45},0).wait(1).to({scaleX:0.9997,scaleY:0.9997,x:-58.55},0).wait(1).to({scaleX:0.9993,scaleY:0.9993,x:-58.75},0).wait(1).to({scaleX:0.9987,scaleY:0.9987,x:-59.05},0).wait(1).to({scaleX:0.9979,scaleY:0.9979,x:-59.5,y:54.4},0).wait(1).to({scaleX:0.9968,scaleY:0.9968,x:-60.15},0).wait(1).to({scaleX:0.9954,scaleY:0.9954,x:-60.9,y:54.35},0).wait(1).to({scaleX:0.9938,scaleY:0.9938,x:-61.75,y:54.3},0).wait(1).to({scaleX:0.992,scaleY:0.992,x:-62.75,y:54.25},0).wait(1).to({scaleX:0.99,scaleY:0.99,x:-63.9,y:54.15},0).wait(1).to({scaleX:0.988,scaleY:0.988,x:-64.95,y:54.1},0).wait(1).to({scaleX:0.9862,scaleY:0.9862,x:-65.95,y:54.05},0).wait(1).to({scaleX:0.9846,scaleY:0.9846,x:-66.85,y:54},0).wait(1).to({scaleX:0.9832,scaleY:0.9832,x:-67.6,y:53.95},0).wait(1).to({scaleX:0.9821,scaleY:0.9821,x:-68.2},0).wait(1).to({scaleX:0.9813,scaleY:0.9813,x:-68.65,y:53.9},0).wait(1).to({scaleX:0.9807,scaleY:0.9807,x:-68.95},0).wait(1).to({scaleX:0.9803,scaleY:0.9803,x:-69.15},0).wait(1).to({scaleX:0.9801,scaleY:0.9801,x:-69.3},0).wait(1).to({regX:0,regY:0,scaleX:0.98,scaleY:0.98,x:-77.3,y:53.35},0).wait(5).to({x:-77.15},0).wait(1).to({regX:8.1,regY:0.5,scaleX:0.9801,scaleY:0.9801,x:-69.15,y:53.85},0).wait(1).to({scaleX:0.9803,scaleY:0.9803,x:-69.05},0).wait(1).to({scaleX:0.9806,scaleY:0.9806,x:-68.85},0).wait(1).to({scaleX:0.9812,scaleY:0.9812,x:-68.55},0).wait(1).to({scaleX:0.9819,scaleY:0.9819,x:-68.15,y:53.9},0).wait(1).to({scaleX:0.9829,scaleY:0.9829,x:-67.6},0).wait(1).to({scaleX:0.9842,scaleY:0.9842,x:-66.95,y:53.95},0).wait(1).to({scaleX:0.9857,scaleY:0.9857,x:-66.1,y:54},0).wait(1).to({scaleX:0.9874,scaleY:0.9874,x:-65.15,y:54.05},0).wait(1).to({scaleX:0.9894,scaleY:0.9894,x:-64.15,y:54.1},0).wait(1).to({scaleX:0.9914,scaleY:0.9914,x:-63,y:54.15},0).wait(1).to({scaleX:0.9933,scaleY:0.9933,x:-62,y:54.2},0).wait(1).to({scaleX:0.995,scaleY:0.995,x:-61.05,y:54.3},0).wait(1).to({scaleX:0.9965,scaleY:0.9965,x:-60.3},0).wait(1).to({scaleX:0.9977,scaleY:0.9977,x:-59.6,y:54.35},0).wait(1).to({scaleX:0.9986,scaleY:0.9986,x:-59.15,y:54.4},0).wait(1).to({scaleX:0.9992,scaleY:0.9992,x:-58.8},0).wait(1).to({scaleX:0.9997,scaleY:0.9997,x:-58.55},0).wait(1).to({scaleX:0.9999,scaleY:0.9999,x:-58.4},0).wait(1).to({regX:0,regY:0,scaleX:1,scaleY:1,x:-66.5,y:54},0).wait(11));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_8_materials_png_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// materials_png
	this.instance = new lib.Symbol9copy4();
	this.instance.parent = this;
	this.instance.setTransform(-66.5,54);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(5).to({regX:32.4,regY:-0.6,scaleX:0.9999,scaleY:0.9999,x:-34.2,y:53.4},0).wait(1).to({scaleX:0.9997,scaleY:0.9997,x:-34.6},0).wait(1).to({scaleX:0.9993,scaleY:0.9993,x:-35.3},0).wait(1).to({scaleX:0.9987,scaleY:0.9987,x:-36.4},0).wait(1).to({scaleX:0.9979,scaleY:0.9979,x:-37.85},0).wait(1).to({scaleX:0.9968,scaleY:0.9968,x:-39.85},0).wait(1).to({scaleX:0.9954,scaleY:0.9954,x:-42.3},0).wait(1).to({scaleX:0.9938,scaleY:0.9938,x:-45.2},0).wait(1).to({scaleX:0.992,scaleY:0.992,x:-48.55},0).wait(1).to({scaleX:0.99,scaleY:0.99,x:-52.1},0).wait(1).to({scaleX:0.988,scaleY:0.988,x:-55.65},0).wait(1).to({scaleX:0.9862,scaleY:0.9862,x:-59},0).wait(1).to({scaleX:0.9846,scaleY:0.9846,x:-61.9},0).wait(1).to({scaleX:0.9832,scaleY:0.9832,x:-64.35},0).wait(1).to({scaleX:0.9821,scaleY:0.9821,x:-66.35},0).wait(1).to({scaleX:0.9813,scaleY:0.9813,x:-67.8},0).wait(1).to({scaleX:0.9807,scaleY:0.9807,x:-68.9},0).wait(1).to({scaleX:0.9803,scaleY:0.9803,x:-69.6},0).wait(1).to({scaleX:0.9801,scaleY:0.9801,x:-70},0).wait(1).to({regX:0,regY:0,scaleX:0.98,scaleY:0.98,x:-101.9,y:54},0).wait(5).to({x:-101.25,y:53.95},0).wait(1).to({regX:32.4,regY:-0.6,scaleX:0.9801,scaleY:0.9801,x:-69.35,y:53.35},0).wait(1).to({scaleX:0.9803,scaleY:0.9803,x:-69},0).wait(1).to({scaleX:0.9806,scaleY:0.9806,x:-68.35},0).wait(1).to({scaleX:0.9812,scaleY:0.9812,x:-67.35},0).wait(1).to({scaleX:0.9819,scaleY:0.9819,x:-66.05},0).wait(1).to({scaleX:0.9829,scaleY:0.9829,x:-64.3},0).wait(1).to({scaleX:0.9842,scaleY:0.9842,x:-62.05},0).wait(1).to({scaleX:0.9857,scaleY:0.9857,x:-59.4},0).wait(1).to({scaleX:0.9874,scaleY:0.9874,x:-56.3},0).wait(1).to({scaleX:0.9894,scaleY:0.9894,x:-52.9},0).wait(1).to({scaleX:0.9914,scaleY:0.9914,x:-49.35},0).wait(1).to({scaleX:0.9933,scaleY:0.9933,x:-45.9},0).wait(1).to({scaleX:0.995,scaleY:0.995,x:-42.85},0).wait(1).to({scaleX:0.9965,scaleY:0.9965,x:-40.25},0).wait(1).to({scaleX:0.9977,scaleY:0.9977,x:-38.2},0).wait(1).to({scaleX:0.9986,scaleY:0.9986,x:-36.6},0).wait(1).to({scaleX:0.9992,scaleY:0.9992,x:-35.4},0).wait(1).to({scaleX:0.9997,scaleY:0.9997,x:-34.65},0).wait(1).to({scaleX:0.9999,scaleY:0.9999,x:-34.2},0).wait(1).to({regX:0,regY:0,scaleX:1,scaleY:1,x:-66.5,y:54},0).wait(6).to({regX:32.4,regY:-0.6,scaleX:0.9999,scaleY:0.9999,x:-34.2,y:53.4},0).wait(1).to({scaleX:0.9997,scaleY:0.9997,x:-34.6},0).wait(1).to({scaleX:0.9993,scaleY:0.9993,x:-35.3},0).wait(1).to({scaleX:0.9987,scaleY:0.9987,x:-36.4},0).wait(1).to({scaleX:0.9979,scaleY:0.9979,x:-37.85},0).wait(1).to({scaleX:0.9968,scaleY:0.9968,x:-39.85},0).wait(1).to({scaleX:0.9954,scaleY:0.9954,x:-42.3},0).wait(1).to({scaleX:0.9938,scaleY:0.9938,x:-45.2},0).wait(1).to({scaleX:0.992,scaleY:0.992,x:-48.55},0).wait(1).to({scaleX:0.99,scaleY:0.99,x:-52.1},0).wait(1).to({scaleX:0.988,scaleY:0.988,x:-55.65},0).wait(1).to({scaleX:0.9862,scaleY:0.9862,x:-59},0).wait(1).to({scaleX:0.9846,scaleY:0.9846,x:-61.9},0).wait(1).to({scaleX:0.9832,scaleY:0.9832,x:-64.35},0).wait(1).to({scaleX:0.9821,scaleY:0.9821,x:-66.35},0).wait(1).to({scaleX:0.9813,scaleY:0.9813,x:-67.8},0).wait(1).to({scaleX:0.9807,scaleY:0.9807,x:-68.9},0).wait(1).to({scaleX:0.9803,scaleY:0.9803,x:-69.6},0).wait(1).to({scaleX:0.9801,scaleY:0.9801,x:-70},0).wait(1).to({regX:0,regY:0,scaleX:0.98,scaleY:0.98,x:-101.9,y:54},0).wait(5).to({x:-101.25,y:53.95},0).wait(1).to({regX:32.4,regY:-0.6,scaleX:0.9801,scaleY:0.9801,x:-69.35,y:53.35},0).wait(1).to({scaleX:0.9803,scaleY:0.9803,x:-69},0).wait(1).to({scaleX:0.9806,scaleY:0.9806,x:-68.35},0).wait(1).to({scaleX:0.9812,scaleY:0.9812,x:-67.35},0).wait(1).to({scaleX:0.9819,scaleY:0.9819,x:-66.05},0).wait(1).to({scaleX:0.9829,scaleY:0.9829,x:-64.3},0).wait(1).to({scaleX:0.9842,scaleY:0.9842,x:-62.05},0).wait(1).to({scaleX:0.9857,scaleY:0.9857,x:-59.4},0).wait(1).to({scaleX:0.9874,scaleY:0.9874,x:-56.3},0).wait(1).to({scaleX:0.9894,scaleY:0.9894,x:-52.9},0).wait(1).to({scaleX:0.9914,scaleY:0.9914,x:-49.35},0).wait(1).to({scaleX:0.9933,scaleY:0.9933,x:-45.9},0).wait(1).to({scaleX:0.995,scaleY:0.995,x:-42.85},0).wait(1).to({scaleX:0.9965,scaleY:0.9965,x:-40.25},0).wait(1).to({scaleX:0.9977,scaleY:0.9977,x:-38.2},0).wait(1).to({scaleX:0.9986,scaleY:0.9986,x:-36.6},0).wait(1).to({scaleX:0.9992,scaleY:0.9992,x:-35.4},0).wait(1).to({scaleX:0.9997,scaleY:0.9997,x:-34.65},0).wait(1).to({scaleX:0.9999,scaleY:0.9999,x:-34.2},0).wait(1).to({regX:0,regY:0,scaleX:1,scaleY:1,x:-66.5,y:54},0).wait(11));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_8_materials_png = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// materials_png
	this.instance = new lib.Symbol9copy5();
	this.instance.parent = this;
	this.instance.setTransform(-66.5,54);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(5).to({regX:57.9,regY:-1.3,x:-8.8,y:52.7},0).wait(1).to({x:-9.45},0).wait(1).to({x:-10.7,y:52.75},0).wait(1).to({x:-12.5,y:52.85},0).wait(1).to({x:-15.05,y:52.95},0).wait(1).to({x:-18.35,y:53.05},0).wait(1).to({x:-22.5,y:53.2},0).wait(1).to({x:-27.5,y:53.4},0).wait(1).to({x:-33.15,y:53.65},0).wait(1).to({x:-39.2,y:53.9},0).wait(1).to({x:-45.2,y:54.1},0).wait(1).to({x:-50.85,y:54.35},0).wait(1).to({x:-55.85,y:54.55},0).wait(1).to({x:-60,y:54.7},0).wait(1).to({x:-63.3,y:54.8},0).wait(1).to({x:-65.85,y:54.9},0).wait(1).to({x:-67.65,y:55},0).wait(1).to({x:-68.9,y:55.05},0).wait(1).to({x:-69.55},0).wait(1).to({regX:0,regY:0,x:-127.7,y:56.4},0).wait(6).to({regX:57.9,regY:-1.3,x:-69.6,y:55.05},0).wait(1).to({x:-68.95},0).wait(1).to({x:-67.85,y:55},0).wait(1).to({x:-66.15,y:54.95},0).wait(1).to({x:-63.85,y:54.85},0).wait(1).to({x:-60.8,y:54.7},0).wait(1).to({x:-57,y:54.55},0).wait(1).to({x:-52.35,y:54.4},0).wait(1).to({x:-47,y:54.2},0).wait(1).to({x:-41.1,y:53.95},0).wait(1).to({x:-34.95,y:53.7},0).wait(1).to({x:-29.1,y:53.5},0).wait(1).to({x:-23.8,y:53.25},0).wait(1).to({x:-19.3,y:53.1},0).wait(1).to({x:-15.7,y:52.95},0).wait(1).to({x:-12.9,y:52.85},0).wait(1).to({x:-10.9,y:52.75},0).wait(1).to({x:-9.55,y:52.7},0).wait(1).to({x:-8.8},0).wait(1).to({regX:0,regY:0,x:-66.5,y:54},0).wait(6).to({regX:57.9,regY:-1.3,x:-8.8,y:52.7},0).wait(1).to({x:-9.45},0).wait(1).to({x:-10.7,y:52.75},0).wait(1).to({x:-12.5,y:52.85},0).wait(1).to({x:-15.05,y:52.95},0).wait(1).to({x:-18.35,y:53.05},0).wait(1).to({x:-22.5,y:53.2},0).wait(1).to({x:-27.5,y:53.4},0).wait(1).to({x:-33.15,y:53.65},0).wait(1).to({x:-39.2,y:53.9},0).wait(1).to({x:-45.2,y:54.1},0).wait(1).to({x:-50.85,y:54.35},0).wait(1).to({x:-55.85,y:54.55},0).wait(1).to({x:-60,y:54.7},0).wait(1).to({x:-63.3,y:54.8},0).wait(1).to({x:-65.85,y:54.9},0).wait(1).to({x:-67.65,y:55},0).wait(1).to({x:-68.9,y:55.05},0).wait(1).to({x:-69.55},0).wait(1).to({regX:0,regY:0,x:-127.7,y:56.4},0).wait(6).to({regX:57.9,regY:-1.3,x:-69.6,y:55.05},0).wait(1).to({x:-68.95},0).wait(1).to({x:-67.85,y:55},0).wait(1).to({x:-66.15,y:54.95},0).wait(1).to({x:-63.85,y:54.85},0).wait(1).to({x:-60.8,y:54.7},0).wait(1).to({x:-57,y:54.55},0).wait(1).to({x:-52.35,y:54.4},0).wait(1).to({x:-47,y:54.2},0).wait(1).to({x:-41.1,y:53.95},0).wait(1).to({x:-34.95,y:53.7},0).wait(1).to({x:-29.1,y:53.5},0).wait(1).to({x:-23.8,y:53.25},0).wait(1).to({x:-19.3,y:53.1},0).wait(1).to({x:-15.7,y:52.95},0).wait(1).to({x:-12.9,y:52.85},0).wait(1).to({x:-10.9,y:52.75},0).wait(1).to({x:-9.55,y:52.7},0).wait(1).to({x:-8.8},0).wait(1).to({regX:0,regY:0,x:-66.5,y:54},0).wait(11));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_8_Layer_3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_3
	this.instance = new lib.Symbol11();
	this.instance.parent = this;
	this.instance.setTransform(-11.5,91.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(5).to({scaleX:0.999,x:-11.6082},0).wait(1).to({scaleX:0.9958,x:-11.9538},0).wait(1).to({scaleX:0.9901,x:-12.5717},0).wait(1).to({scaleX:0.9816,x:-13.5016},0).wait(1).to({scaleX:0.9698,x:-14.7865},0).wait(1).to({scaleX:0.9543,x:-16.4665},0).wait(1).to({scaleX:0.935,x:-18.5687},0).wait(1).to({scaleX:0.9118,x:-21.0858},0).wait(1).to({scaleX:0.8855,x:-23.9504},0).wait(1).to({scaleX:0.8573,x:-27.0179},0).wait(1).to({scaleX:0.829,x:-30.0853},0).wait(1).to({scaleX:0.8027,x:-32.9499},0).wait(1).to({scaleX:0.7795,x:-35.467},0).wait(1).to({scaleX:0.7602,x:-37.5692},0).wait(1).to({scaleX:0.7448,x:-39.2493},0).wait(1).to({scaleX:0.7329,x:-40.5341},0).wait(1).to({scaleX:0.7244,x:-41.4641},0).wait(1).to({scaleX:0.7187,x:-42.0819},0).wait(1).to({scaleX:0.7155,x:-42.4275},0).wait(1).to({scaleX:0.7145,x:-42.5},0).wait(6).to({scaleX:0.7155,x:-42.3917},0).wait(1).to({scaleX:0.7187,x:-42.046},0).wait(1).to({scaleX:0.7244,x:-41.4278},0).wait(1).to({scaleX:0.7329,x:-40.4974},0).wait(1).to({scaleX:0.7448,x:-39.212},0).wait(1).to({scaleX:0.7602,x:-37.5312},0).wait(1).to({scaleX:0.7795,x:-35.428},0).wait(1).to({scaleX:0.8027,x:-32.9097},0).wait(1).to({scaleX:0.829,x:-30.0438},0).wait(1).to({scaleX:0.8573,x:-26.975},0).wait(1).to({scaleX:0.8855,x:-23.9062},0).wait(1).to({scaleX:0.9118,x:-21.0403},0).wait(1).to({scaleX:0.935,x:-18.522},0).wait(1).to({scaleX:0.9543,x:-16.4188},0).wait(1).to({scaleX:0.9698,x:-14.738},0).wait(1).to({scaleX:0.9816,x:-13.4526},0).wait(1).to({scaleX:0.9901,x:-12.5222},0).wait(1).to({scaleX:0.9958,x:-11.904},0).wait(1).to({scaleX:0.999,x:-11.5583},0).wait(1).to({scaleX:1,x:-11.5},0).wait(6).to({scaleX:0.999,x:-11.6082},0).wait(1).to({scaleX:0.9958,x:-11.9538},0).wait(1).to({scaleX:0.9901,x:-12.5717},0).wait(1).to({scaleX:0.9816,x:-13.5016},0).wait(1).to({scaleX:0.9698,x:-14.7865},0).wait(1).to({scaleX:0.9543,x:-16.4665},0).wait(1).to({scaleX:0.935,x:-18.5687},0).wait(1).to({scaleX:0.9118,x:-21.0858},0).wait(1).to({scaleX:0.8855,x:-23.9504},0).wait(1).to({scaleX:0.8573,x:-27.0179},0).wait(1).to({scaleX:0.829,x:-30.0853},0).wait(1).to({scaleX:0.8027,x:-32.9499},0).wait(1).to({scaleX:0.7795,x:-35.467},0).wait(1).to({scaleX:0.7602,x:-37.5692},0).wait(1).to({scaleX:0.7448,x:-39.2493},0).wait(1).to({scaleX:0.7329,x:-40.5341},0).wait(1).to({scaleX:0.7244,x:-41.4641},0).wait(1).to({scaleX:0.7187,x:-42.0819},0).wait(1).to({scaleX:0.7155,x:-42.4275},0).wait(1).to({scaleX:0.7145,x:-42.5},0).wait(6).to({scaleX:0.7155,x:-42.3917},0).wait(1).to({scaleX:0.7187,x:-42.046},0).wait(1).to({scaleX:0.7244,x:-41.4278},0).wait(1).to({scaleX:0.7329,x:-40.4974},0).wait(1).to({scaleX:0.7448,x:-39.212},0).wait(1).to({scaleX:0.7602,x:-37.5312},0).wait(1).to({scaleX:0.7795,x:-35.428},0).wait(1).to({scaleX:0.8027,x:-32.9097},0).wait(1).to({scaleX:0.829,x:-30.0438},0).wait(1).to({scaleX:0.8573,x:-26.975},0).wait(1).to({scaleX:0.8855,x:-23.9062},0).wait(1).to({scaleX:0.9118,x:-21.0403},0).wait(1).to({scaleX:0.935,x:-18.522},0).wait(1).to({scaleX:0.9543,x:-16.4188},0).wait(1).to({scaleX:0.9698,x:-14.738},0).wait(1).to({scaleX:0.9816,x:-13.4526},0).wait(1).to({scaleX:0.9901,x:-12.5222},0).wait(1).to({scaleX:0.9958,x:-11.904},0).wait(1).to({scaleX:0.999,x:-11.5583},0).wait(1).to({scaleX:1,x:-11.5},0).wait(11));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_8_Layer_2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.instance = new lib.Symbol10();
	this.instance.parent = this;
	this.instance.setTransform(-0.5,0);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(110));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_7_Symbol_6 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Symbol_6
	this.instance = new lib.Symbol6();
	this.instance.parent = this;
	this.instance.setTransform(17.6,-13.4,1,1,0,0,0,-15.2,-8.3);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(4).to({rotation:-18.7465},10,cjs.Ease.get(1)).wait(75).to({rotation:0},10,cjs.Ease.get(1)).wait(11));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_7_Symbol_5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Symbol_5
	this.instance = new lib.Symbol5();
	this.instance.parent = this;
	this.instance.setTransform(17.65,-13.3,1,1,0,0,0,-15.3,9.2);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(4).to({rotation:14.9992,x:17.6},10,cjs.Ease.get(1)).wait(75).to({rotation:0,x:17.65},10,cjs.Ease.get(1)).wait(11));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_7_Symbol_4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Symbol_4
	this.instance = new lib.Symbol4();
	this.instance.parent = this;
	this.instance.setTransform(-17.3,-15.95,1,1,0,0,0,15.4,-9.2);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(4).to({rotation:19.193},10,cjs.Ease.get(1)).wait(75).to({rotation:0},10,cjs.Ease.get(1)).wait(11));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_7_Symbol_3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Symbol_3
	this.instance = new lib.Symbol3();
	this.instance.parent = this;
	this.instance.setTransform(-17.25,-15.9,1,1,0,0,0,15.6,8.2);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(4).to({rotation:-14.9992,x:-17.3,y:-15.95},10,cjs.Ease.get(1)).wait(75).to({rotation:0,x:-17.25,y:-15.9},10,cjs.Ease.get(1)).wait(11));

}).prototype = p = new cjs.MovieClip();


(lib.Scene_1_Layer_4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_4
	this.instance = new lib.Symbol12();
	this.instance.parent = this;
	this.instance.setTransform(220.6,422.5,1,1.1967,0,0,0,0.1,0.1);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Scene_1_Layer_4, null, null);


(lib.Symbol8 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_109 = function() {
		this.___loopingOver___ = true;
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(109).call(this.frame_109).wait(1));

	// Symbol_1_obj_
	this.Symbol_1 = new lib.Symbol_8_Symbol_1();
	this.Symbol_1.name = "Symbol_1";
	this.Symbol_1.parent = this;
	this.Symbol_1.setTransform(2.2,30.7,1,1,0,0,0,2.2,30.7);
	this.Symbol_1.depth = 0;
	this.Symbol_1.isAttachedToCamera = 0
	this.Symbol_1.isAttachedToMask = 0
	this.Symbol_1.layerDepth = 0
	this.Symbol_1.layerIndex = 0
	this.Symbol_1.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Symbol_1).wait(5).to({regX:-28.4,regY:31.9,x:-28.4,y:31.9},0).wait(19).to({regX:2.2,regY:30.7,x:2.2,y:30.7},0).wait(6).to({regX:-28.4,regY:31.9,x:-28.4,y:31.9},0).wait(19).to({regX:2.2,regY:30.7,x:2.2,y:30.7},0).wait(6).to({regX:-28.4,regY:31.9,x:-28.4,y:31.9},0).wait(19).to({regX:2.2,regY:30.7,x:2.2,y:30.7},0).wait(6).to({regX:-28.4,regY:31.9,x:-28.4,y:31.9},0).wait(19).to({regX:2.2,regY:30.7,x:2.2,y:30.7},0).wait(11));

	// Layer_7_obj_
	this.Layer_7 = new lib.Symbol_8_Layer_7();
	this.Layer_7.name = "Layer_7";
	this.Layer_7.parent = this;
	this.Layer_7.setTransform(41.8,25.6,1,1,0,0,0,41.8,25.6);
	this.Layer_7.depth = 0;
	this.Layer_7.isAttachedToCamera = 0
	this.Layer_7.isAttachedToMask = 0
	this.Layer_7.layerDepth = 0
	this.Layer_7.layerIndex = 1
	this.Layer_7.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_7).wait(110));

	// materials_png_obj_
	this.materials_png = new lib.Symbol_8_materials_png();
	this.materials_png.name = "materials_png";
	this.materials_png.parent = this;
	this.materials_png.setTransform(-66.5,53.8,1,1,0,0,0,-66.5,53.8);
	this.materials_png.depth = 0;
	this.materials_png.isAttachedToCamera = 0
	this.materials_png.isAttachedToMask = 0
	this.materials_png.layerDepth = 0
	this.materials_png.layerIndex = 2
	this.materials_png.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.materials_png).wait(5).to({regX:-39.2,regY:53.9,x:-39.2,y:53.9},0).wait(19).to({regX:-66.5,regY:53.8,x:-66.5,y:53.8},0).wait(6).to({regX:-39.2,regY:53.9,x:-39.2,y:53.9},0).wait(19).to({regX:-66.5,regY:53.8,x:-66.5,y:53.8},0).wait(6).to({regX:-39.2,regY:53.9,x:-39.2,y:53.9},0).wait(19).to({regX:-66.5,regY:53.8,x:-66.5,y:53.8},0).wait(6).to({regX:-39.2,regY:53.9,x:-39.2,y:53.9},0).wait(19).to({regX:-66.5,regY:53.8,x:-66.5,y:53.8},0).wait(11));

	// materials_png_obj_
	this.materials_png_1 = new lib.Symbol_8_materials_png_1();
	this.materials_png_1.name = "materials_png_1";
	this.materials_png_1.parent = this;
	this.materials_png_1.setTransform(-66.5,54,1,1,0,0,0,-66.5,54);
	this.materials_png_1.depth = 0;
	this.materials_png_1.isAttachedToCamera = 0
	this.materials_png_1.isAttachedToMask = 0
	this.materials_png_1.layerDepth = 0
	this.materials_png_1.layerIndex = 3
	this.materials_png_1.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.materials_png_1).wait(5).to({regX:-51.5,regY:53.4,x:-51.5,y:53.4},0).wait(19).to({regX:-66.5,regY:54,x:-66.5,y:54},0).wait(6).to({regX:-51.5,regY:53.4,x:-51.5,y:53.4},0).wait(19).to({regX:-66.5,regY:54,x:-66.5,y:54},0).wait(6).to({regX:-51.5,regY:53.4,x:-51.5,y:53.4},0).wait(19).to({regX:-66.5,regY:54,x:-66.5,y:54},0).wait(6).to({regX:-51.5,regY:53.4,x:-51.5,y:53.4},0).wait(19).to({regX:-66.5,regY:54,x:-66.5,y:54},0).wait(11));

	// materials_png_obj_
	this.materials_png_2 = new lib.Symbol_8_materials_png_2();
	this.materials_png_2.name = "materials_png_2";
	this.materials_png_2.parent = this;
	this.materials_png_2.setTransform(-66.5,54,1,1,0,0,0,-66.5,54);
	this.materials_png_2.depth = 0;
	this.materials_png_2.isAttachedToCamera = 0
	this.materials_png_2.isAttachedToMask = 0
	this.materials_png_2.layerDepth = 0
	this.materials_png_2.layerIndex = 4
	this.materials_png_2.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.materials_png_2).wait(5).to({regX:-63.2,regY:54.5,x:-63.2,y:54.5},0).wait(19).to({regX:-66.5,regY:54,x:-66.5,y:54},0).wait(6).to({regX:-63.2,regY:54.5,x:-63.2,y:54.5},0).wait(19).to({regX:-66.5,regY:54,x:-66.5,y:54},0).wait(6).to({regX:-63.2,regY:54.5,x:-63.2,y:54.5},0).wait(19).to({regX:-66.5,regY:54,x:-66.5,y:54},0).wait(6).to({regX:-63.2,regY:54.5,x:-63.2,y:54.5},0).wait(19).to({regX:-66.5,regY:54,x:-66.5,y:54},0).wait(11));

	// materials_png_obj_
	this.materials_png_3 = new lib.Symbol_8_materials_png_3();
	this.materials_png_3.name = "materials_png_3";
	this.materials_png_3.parent = this;
	this.materials_png_3.setTransform(-66.5,54.1,1,1,0,0,0,-66.5,54.1);
	this.materials_png_3.depth = 0;
	this.materials_png_3.isAttachedToCamera = 0
	this.materials_png_3.isAttachedToMask = 0
	this.materials_png_3.layerDepth = 0
	this.materials_png_3.layerIndex = 5
	this.materials_png_3.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.materials_png_3).wait(5).to({regX:-75.3,regY:55.1,x:-75.3,y:55.1},0).wait(19).to({regX:-66.5,regY:54.1,x:-66.5,y:54.1},0).wait(6).to({regX:-75.3,regY:55.1,x:-75.3,y:55.1},0).wait(19).to({regX:-66.5,regY:54.1,x:-66.5,y:54.1},0).wait(6).to({regX:-75.3,regY:55.1,x:-75.3,y:55.1},0).wait(19).to({regX:-66.5,regY:54.1,x:-66.5,y:54.1},0).wait(6).to({regX:-75.3,regY:55.1,x:-75.3,y:55.1},0).wait(19).to({regX:-66.5,regY:54.1,x:-66.5,y:54.1},0).wait(11));

	// materials_png_obj_
	this.materials_png_4 = new lib.Symbol_8_materials_png_4();
	this.materials_png_4.name = "materials_png_4";
	this.materials_png_4.parent = this;
	this.materials_png_4.setTransform(-66.5,54.1,1,1,0,0,0,-66.5,54.1);
	this.materials_png_4.depth = 0;
	this.materials_png_4.isAttachedToCamera = 0
	this.materials_png_4.isAttachedToMask = 0
	this.materials_png_4.layerDepth = 0
	this.materials_png_4.layerIndex = 6
	this.materials_png_4.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.materials_png_4).wait(5).to({regX:-87,regY:55.1,x:-87,y:55.1},0).wait(19).to({regX:-66.5,regY:54.1,x:-66.5,y:54.1},0).wait(6).to({regX:-87,regY:55.1,x:-87,y:55.1},0).wait(19).to({regX:-66.5,regY:54.1,x:-66.5,y:54.1},0).wait(6).to({regX:-87,regY:55.1,x:-87,y:55.1},0).wait(19).to({regX:-66.5,regY:54.1,x:-66.5,y:54.1},0).wait(6).to({regX:-87,regY:55.1,x:-87,y:55.1},0).wait(19).to({regX:-66.5,regY:54.1,x:-66.5,y:54.1},0).wait(11));

	// materials_png_obj_
	this.materials_png_5 = new lib.Symbol_8_materials_png_5();
	this.materials_png_5.name = "materials_png_5";
	this.materials_png_5.parent = this;
	this.materials_png_5.setTransform(-66.5,54,1,1,0,0,0,-66.5,54);
	this.materials_png_5.depth = 0;
	this.materials_png_5.isAttachedToCamera = 0
	this.materials_png_5.isAttachedToMask = 0
	this.materials_png_5.layerDepth = 0
	this.materials_png_5.layerIndex = 7
	this.materials_png_5.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.materials_png_5).wait(5).to({regX:-97.2,regY:53.4,x:-97.2,y:53.4},0).wait(19).to({regX:-66.5,regY:54,x:-66.5,y:54},0).wait(6).to({regX:-97.2,regY:53.4,x:-97.2,y:53.4},0).wait(19).to({regX:-66.5,regY:54,x:-66.5,y:54},0).wait(6).to({regX:-97.2,regY:53.4,x:-97.2,y:53.4},0).wait(19).to({regX:-66.5,regY:54,x:-66.5,y:54},0).wait(6).to({regX:-97.2,regY:53.4,x:-97.2,y:53.4},0).wait(19).to({regX:-66.5,regY:54,x:-66.5,y:54},0).wait(11));

	// Layer_4 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("Av5WIMAAAgsPIfzAAMAAAAsPg");
	mask.setTransform(28.35,67.2);

	// Layer_3_obj_
	this.Layer_3 = new lib.Symbol_8_Layer_3();
	this.Layer_3.name = "Layer_3";
	this.Layer_3.parent = this;
	this.Layer_3.setTransform(-11.5,91.5,1,1,0,0,0,-11.5,91.5);
	this.Layer_3.depth = 0;
	this.Layer_3.isAttachedToCamera = 0
	this.Layer_3.isAttachedToMask = 0
	this.Layer_3.layerDepth = 0
	this.Layer_3.layerIndex = 8
	this.Layer_3.maskLayerName = 0

	var maskedShapeInstanceList = [this.Layer_3];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.Layer_3).wait(110));

	// Layer_2_obj_
	this.Layer_2 = new lib.Symbol_8_Layer_2();
	this.Layer_2.name = "Layer_2";
	this.Layer_2.parent = this;
	this.Layer_2.setTransform(-0.5,0,1,1,0,0,0,-0.5,0);
	this.Layer_2.depth = 0;
	this.Layer_2.isAttachedToCamera = 0
	this.Layer_2.isAttachedToMask = 0
	this.Layer_2.layerDepth = 0
	this.Layer_2.layerIndex = 9
	this.Layer_2.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_2).wait(110));

	// Symbol_2_obj_
	this.Symbol_2 = new lib.Symbol_8_Symbol_2();
	this.Symbol_2.name = "Symbol_2";
	this.Symbol_2.parent = this;
	this.Symbol_2.setTransform(-156.8,-9.3,1,1,0,0,0,-156.8,-9.3);
	this.Symbol_2.depth = 0;
	this.Symbol_2.isAttachedToCamera = 0
	this.Symbol_2.isAttachedToMask = 0
	this.Symbol_2.layerDepth = 0
	this.Symbol_2.layerIndex = 10
	this.Symbol_2.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Symbol_2).wait(5).to({regX:-129.6,x:-129.6},0).wait(19).to({regX:-156.8,x:-156.8},0).wait(6).to({regX:-129.6,x:-129.6},0).wait(19).to({regX:-156.8,x:-156.8},0).wait(6).to({regX:-129.6,x:-129.6},0).wait(19).to({regX:-156.8,x:-156.8},0).wait(6).to({regX:-129.6,x:-129.6},0).wait(19).to({regX:-156.8,x:-156.8},0).wait(11));

	// Layer_6_obj_
	this.Layer_6 = new lib.Symbol_8_Layer_6();
	this.Layer_6.name = "Layer_6";
	this.Layer_6.parent = this;
	this.Layer_6.setTransform(-105.9,8.9,1,1,0,0,0,-105.9,8.9);
	this.Layer_6.depth = 0;
	this.Layer_6.isAttachedToCamera = 0
	this.Layer_6.isAttachedToMask = 0
	this.Layer_6.layerDepth = 0
	this.Layer_6.layerIndex = 11
	this.Layer_6.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_6).wait(110));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-246.2,-247.5,444.2,495);


(lib.Symbol7 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_109 = function() {
		this.___loopingOver___ = true;
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(109).call(this.frame_109).wait(1));

	// Symbol_6_obj_
	this.Symbol_6 = new lib.Symbol_7_Symbol_6();
	this.Symbol_6.name = "Symbol_6";
	this.Symbol_6.parent = this;
	this.Symbol_6.setTransform(32.8,-5.1,1,1,0,0,0,32.8,-5.1);
	this.Symbol_6.depth = 0;
	this.Symbol_6.isAttachedToCamera = 0
	this.Symbol_6.isAttachedToMask = 0
	this.Symbol_6.layerDepth = 0
	this.Symbol_6.layerIndex = 0
	this.Symbol_6.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Symbol_6).wait(110));

	// Symbol_5_obj_
	this.Symbol_5 = new lib.Symbol_7_Symbol_5();
	this.Symbol_5.name = "Symbol_5";
	this.Symbol_5.parent = this;
	this.Symbol_5.setTransform(33,-22.5,1,1,0,0,0,33,-22.5);
	this.Symbol_5.depth = 0;
	this.Symbol_5.isAttachedToCamera = 0
	this.Symbol_5.isAttachedToMask = 0
	this.Symbol_5.layerDepth = 0
	this.Symbol_5.layerIndex = 1
	this.Symbol_5.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Symbol_5).wait(110));

	// Symbol_4_obj_
	this.Symbol_4 = new lib.Symbol_7_Symbol_4();
	this.Symbol_4.name = "Symbol_4";
	this.Symbol_4.parent = this;
	this.Symbol_4.setTransform(-32.7,-6.8,1,1,0,0,0,-32.7,-6.8);
	this.Symbol_4.depth = 0;
	this.Symbol_4.isAttachedToCamera = 0
	this.Symbol_4.isAttachedToMask = 0
	this.Symbol_4.layerDepth = 0
	this.Symbol_4.layerIndex = 2
	this.Symbol_4.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Symbol_4).wait(110));

	// Symbol_3_obj_
	this.Symbol_3 = new lib.Symbol_7_Symbol_3();
	this.Symbol_3.name = "Symbol_3";
	this.Symbol_3.parent = this;
	this.Symbol_3.setTransform(-32.9,-24.1,1,1,0,0,0,-32.9,-24.1);
	this.Symbol_3.depth = 0;
	this.Symbol_3.isAttachedToCamera = 0
	this.Symbol_3.isAttachedToMask = 0
	this.Symbol_3.layerDepth = 0
	this.Symbol_3.layerIndex = 3
	this.Symbol_3.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Symbol_3).wait(110));

	// Layer_6_obj_
	this.Layer_6 = new lib.Symbol_7_Layer_6();
	this.Layer_6.name = "Layer_6";
	this.Layer_6.parent = this;
	this.Layer_6.setTransform(-1.6,25.1,1,1,0,0,0,-1.6,25.1);
	this.Layer_6.depth = 0;
	this.Layer_6.isAttachedToCamera = 0
	this.Layer_6.isAttachedToMask = 0
	this.Layer_6.layerDepth = 0
	this.Layer_6.layerIndex = 4
	this.Layer_6.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_6).wait(110));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-60.3,-39.3,120,78.9);


(lib.Scene_1_Layer_2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.instance = new lib.Symbol8();
	this.instance.parent = this;
	this.instance.setTransform(188.85,516.7,0.6772,0.6772);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Scene_1_Layer_2, null, null);


(lib.Scene_1_Layer_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Symbol7();
	this.instance.parent = this;
	this.instance.setTransform(170.5,451.4,0.6772,0.6772);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Scene_1_Layer_1, null, null);


// stage content:
(lib.January_2_vert = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	this.___GetDepth___ = function(obj) {
		var depth = obj.depth;
		var cameraObj = this.___camera___instance;
		if(cameraObj && cameraObj.depth && obj.isAttachedToCamera)
		{
			depth += depth + cameraObj.depth;
		}
		return depth;
		}
	this.___needSorting___ = function() {
		for (var i = 0; i < this.getNumChildren() - 1; i++)
		{
			var prevDepth = this.___GetDepth___(this.getChildAt(i));
			var nextDepth = this.___GetDepth___(this.getChildAt(i + 1));
			if (prevDepth < nextDepth)
				return true;
		}
		return false;
	}
	this.___sortFunction___ = function(obj1, obj2) {
		return (this.exportRoot.___GetDepth___(obj2) - this.exportRoot.___GetDepth___(obj1));
	}
	this.on('tick', function (event){
		var curTimeline = event.currentTarget;
		if (curTimeline.___needSorting___()){
			this.sortChildren(curTimeline.___sortFunction___);
		}
	});

	// Layer_1_obj_
	this.Layer_1 = new lib.Scene_1_Layer_1();
	this.Layer_1.name = "Layer_1";
	this.Layer_1.parent = this;
	this.Layer_1.setTransform(170.5,451.4,1,1,0,0,0,170.5,451.4);
	this.Layer_1.depth = 0;
	this.Layer_1.isAttachedToCamera = 0
	this.Layer_1.isAttachedToMask = 0
	this.Layer_1.layerDepth = 0
	this.Layer_1.layerIndex = 0
	this.Layer_1.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_1).wait(1));

	// Layer_2_obj_
	this.Layer_2 = new lib.Scene_1_Layer_2();
	this.Layer_2.name = "Layer_2";
	this.Layer_2.parent = this;
	this.Layer_2.setTransform(188.5,516.7,1,1,0,0,0,188.5,516.7);
	this.Layer_2.depth = 0;
	this.Layer_2.isAttachedToCamera = 0
	this.Layer_2.isAttachedToMask = 0
	this.Layer_2.layerDepth = 0
	this.Layer_2.layerIndex = 1
	this.Layer_2.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_2).wait(1));

	// Layer_4_obj_
	this.Layer_4 = new lib.Scene_1_Layer_4();
	this.Layer_4.name = "Layer_4";
	this.Layer_4.parent = this;
	this.Layer_4.setTransform(224.8,403.4,1,1,0,0,0,224.8,403.4);
	this.Layer_4.depth = 0;
	this.Layer_4.isAttachedToCamera = 0
	this.Layer_4.isAttachedToMask = 0
	this.Layer_4.layerDepth = 0
	this.Layer_4.layerIndex = 2
	this.Layer_4.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_4).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(31.1,401,574.9,411);
// library properties:
lib.properties = {
	id: 'EEC5A14AC0829544AC01658589217788',
	width: 375,
	height: 812,
	fps: 30,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: [
		{src:"assets/images/materials36-2.png", id:"materials"},
		{src:"assets/images/pack36-2.png", id:"pack"},
		{src:"assets/images/shadow36-2.png", id:"shadow"}
	],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['EEC5A14AC0829544AC01658589217788'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}


// Layer depth API : 

AdobeAn.Layer = new function() {
	this.getLayerZDepth = function(timeline, layerName)
	{
		if(layerName === "Camera")
		layerName = "___camera___instance";
		var script = "if(timeline." + layerName + ") timeline." + layerName + ".depth; else 0;";
		return eval(script);
	}
	this.setLayerZDepth = function(timeline, layerName, zDepth)
	{
		const MAX_zDepth = 10000;
		const MIN_zDepth = -5000;
		if(zDepth > MAX_zDepth)
			zDepth = MAX_zDepth;
		else if(zDepth < MIN_zDepth)
			zDepth = MIN_zDepth;
		if(layerName === "Camera")
		layerName = "___camera___instance";
		var script = "if(timeline." + layerName + ") timeline." + layerName + ".depth = " + zDepth + ";";
		eval(script);
	}
	this.removeLayer = function(timeline, layerName)
	{
		if(layerName === "Camera")
		layerName = "___camera___instance";
		var script = "if(timeline." + layerName + ") timeline.removeChild(timeline." + layerName + ");";
		eval(script);
	}
	this.addNewLayer = function(timeline, layerName, zDepth)
	{
		if(layerName === "Camera")
		layerName = "___camera___instance";
		zDepth = typeof zDepth !== 'undefined' ? zDepth : 0;
		var layer = new createjs.MovieClip();
		layer.name = layerName;
		layer.depth = zDepth;
		layer.layerIndex = 0;
		timeline.addChild(layer);
	}
}


})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;

$(function () {
    var canvas, stage, exportRoot, anim_container, dom_overlay_container, fnStartAnimation;
    function init() {
        canvas = document.getElementById("canvas36-2");
        anim_container = document.getElementById("animation_container36-2");
        dom_overlay_container = document.getElementById("dom_overlay_container36-2");
        var comp=AdobeAn.getComposition("EEC5A14AC0829544AC01658589217788");
        var lib=comp.getLibrary();
        var loader = new createjs.LoadQueue(false);
        loader.addEventListener("fileload", function(evt){handleFileLoad(evt,comp)});
        loader.addEventListener("complete", function(evt){handleComplete(evt,comp)});
        var lib=comp.getLibrary();
        loader.loadManifest(lib.properties.manifest);
    }
    function handleFileLoad(evt, comp) {
        var images=comp.getImages();
        if (evt && (evt.item.type == "image")) { images[evt.item.id] = evt.result; }
    }
    function handleComplete(evt,comp) {
        //This function is always called, irrespective of the content. You can use the variable "stage" after it is created in token create_stage.
        var lib=comp.getLibrary();
        var ss=comp.getSpriteSheet();
        var queue = evt.target;
        var ssMetadata = lib.ssMetadata;
        for(i=0; i<ssMetadata.length; i++) {
            ss[ssMetadata[i].name] = new createjs.SpriteSheet( {"images": [queue.getResult(ssMetadata[i].name)], "frames": ssMetadata[i].frames} )
        }
        var preloaderDiv = document.getElementById("_preload_div_36");
        preloaderDiv.style.display = 'none';
        canvas.style.display = 'block';
        exportRoot = new lib.January_2_vert();
        stage = new lib.Stage(canvas);
        //Registers the "tick" event listener.
        fnStartAnimation = function() {
            stage.addChild(exportRoot);
            createjs.Ticker.setFPS(lib.properties.fps);
            createjs.Ticker.addEventListener("tick", stage)
            stage.addEventListener("tick", handleTick)
            function getProjectionMatrix(container, totalDepth) {
                var focalLength = 528.25;
                var projectionCenter = { x : lib.properties.width/2, y : lib.properties.height/2 };
                var scale = (totalDepth + focalLength)/focalLength;
                var scaleMat = new createjs.Matrix2D;
                scaleMat.a = 1/scale;
                scaleMat.d = 1/scale;
                var projMat = new createjs.Matrix2D;
                projMat.tx = -projectionCenter.x;
                projMat.ty = -projectionCenter.y;
                projMat = projMat.prependMatrix(scaleMat);
                projMat.tx += projectionCenter.x;
                projMat.ty += projectionCenter.y;
                return projMat;
            }
            function handleTick(event) {
                var cameraInstance = exportRoot.___camera___instance;
                if(cameraInstance !== undefined && cameraInstance.pinToObject !== undefined)
                {
                    cameraInstance.x = cameraInstance.pinToObject.x + cameraInstance.pinToObject.pinOffsetX;
                    cameraInstance.y = cameraInstance.pinToObject.y + cameraInstance.pinToObject.pinOffsetY;
                    if(cameraInstance.pinToObject.parent !== undefined && cameraInstance.pinToObject.parent.depth !== undefined)
                        cameraInstance.depth = cameraInstance.pinToObject.parent.depth + cameraInstance.pinToObject.pinOffsetZ;
                }
                applyLayerZDepth(exportRoot);
            }
            function applyLayerZDepth(parent)
            {
                var cameraInstance = parent.___camera___instance;
                var focalLength = 528.25;
                var projectionCenter = { 'x' : 0, 'y' : 0};
                if(parent === exportRoot)
                {
                    var stageCenter = { 'x' : lib.properties.width/2, 'y' : lib.properties.height/2 };
                    projectionCenter.x = stageCenter.x;
                    projectionCenter.y = stageCenter.y;
                }
                for(child in parent.children)
                {
                    var layerObj = parent.children[child];
                    if(layerObj == cameraInstance)
                        continue;
                    applyLayerZDepth(layerObj, cameraInstance);
                    if(layerObj.layerDepth === undefined)
                        continue;
                    if(layerObj.currentFrame != layerObj.parent.currentFrame)
                    {
                        layerObj.gotoAndPlay(layerObj.parent.currentFrame);
                    }
                    var matToApply = new createjs.Matrix2D;
                    var cameraMat = new createjs.Matrix2D;
                    var totalDepth = layerObj.layerDepth ? layerObj.layerDepth : 0;
                    var cameraDepth = 0;
                    if(cameraInstance && !layerObj.isAttachedToCamera)
                    {
                        var mat = cameraInstance.getMatrix();
                        mat.tx -= projectionCenter.x;
                        mat.ty -= projectionCenter.y;
                        cameraMat = mat.invert();
                        cameraMat.prependTransform(projectionCenter.x, projectionCenter.y, 1, 1, 0, 0, 0, 0, 0);
                        cameraMat.appendTransform(-projectionCenter.x, -projectionCenter.y, 1, 1, 0, 0, 0, 0, 0);
                        if(cameraInstance.depth)
                            cameraDepth = cameraInstance.depth;
                    }
                    if(layerObj.depth)
                    {
                        totalDepth = layerObj.depth;
                    }
                    //Offset by camera depth
                    totalDepth -= cameraDepth;
                    if(totalDepth < -focalLength)
                    {
                        matToApply.a = 0;
                        matToApply.d = 0;
                    }
                    else
                    {
                        if(layerObj.layerDepth)
                        {
                            var sizeLockedMat = getProjectionMatrix(parent, layerObj.layerDepth);
                            if(sizeLockedMat)
                            {
                                sizeLockedMat.invert();
                                matToApply.prependMatrix(sizeLockedMat);
                            }
                        }
                        matToApply.prependMatrix(cameraMat);
                        var projMat = getProjectionMatrix(parent, totalDepth);
                        if(projMat)
                        {
                            matToApply.prependMatrix(projMat);
                        }
                    }
                    layerObj.transformMatrix = matToApply;
                }
            }
        }
        //Code to support hidpi screens and responsive scaling.
        function makeResponsive(isResp, respDim, isScale, scaleType) {
            var lastW, lastH, lastS=1;
            window.addEventListener('resize', resizeCanvas);
            resizeCanvas();
            function resizeCanvas() {
                var w = lib.properties.width, h = lib.properties.height;
                var iw = window.innerWidth, ih=window.innerHeight;
                var pRatio = window.devicePixelRatio || 1, xRatio=iw/w, yRatio=ih/h, sRatio=1;
                if(isResp) {
                    if((respDim=='width'&&lastW==iw) || (respDim=='height'&&lastH==ih)) {
                        sRatio = lastS;
                    }
                    else if(!isScale) {
                        if(iw<w || ih<h)
                            sRatio = Math.min(xRatio, yRatio);
                    }
                    else if(scaleType==1) {
                        sRatio = Math.min(xRatio, yRatio);
                    }
                    else if(scaleType==2) {
                        sRatio = Math.max(xRatio, yRatio);
                    }
                }
                canvas.width = w*pRatio*sRatio;
                canvas.height = h*pRatio*sRatio;
                canvas.style.width = anim_container.style.width = dom_overlay_container.style.width = preloaderDiv.style.width = w*sRatio+'px';
                canvas.style.height = anim_container.style.height = dom_overlay_container.style.height = preloaderDiv.style.height = h*sRatio+'px';
                stage.scaleX = pRatio*sRatio;
                stage.scaleY = pRatio*sRatio;
                lastW = iw; lastH = ih; lastS = sRatio;
                stage.tickOnUpdate = false;
                stage.update();
                stage.tickOnUpdate = true;
            }
        }
        makeResponsive(true,'both',true,2);
        AdobeAn.compositionLoaded(lib.properties.id);
        fnStartAnimation();
    }
    init()
})