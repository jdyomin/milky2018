(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [];


// symbols:



(lib.bg_img_vert = function() {
	this.initialize(img.bg_img_vert);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,518,914);


(lib.leaf1 = function() {
	this.initialize(img.leaf1);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,186,205);


(lib.leaf2 = function() {
	this.initialize(img.leaf2);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,225,206);


(lib.leaf3 = function() {
	this.initialize(img.leaf3);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,172,159);


(lib.mouth1 = function() {
	this.initialize(img.mouth1);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,35,97);


(lib.teeth = function() {
	this.initialize(img.teeth);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,16,15);// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.Symbol11 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AgvAwQgTgUAAgcQAAgbATgUQAUgUAbAAQAcAAAUAUQATAUAAAbQAAAcgTAUQgUATgcABQgbgBgUgTg");
	this.shape.setTransform(6.725,6.75);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol11, new cjs.Rectangle(0,0,13.5,13.5), null);


(lib.Symbol10 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AgvAwQgTgUAAgcQAAgbATgUQAUgUAbAAQAcAAAUAUQATAUAAAbQAAAcgTAUQgUATgcABQgbgBgUgTg");
	this.shape.setTransform(6.725,6.75);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol10, new cjs.Rectangle(0,0,13.5,13.5), null);


(lib.Symbol9 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#B7B7B7").s().p("AiMCMQg5g6gBhSQABhSA5g6QA6g5BSgBQBSABA6A5QA6A6AABSQAABSg6A6Qg6A6hSAAQhSAAg6g6g");
	this.shape.setTransform(19.85,19.85);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol9, new cjs.Rectangle(0,0,39.7,39.7), null);


(lib.Symbol7 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#B7B7B7").s().p("AiMCMQg5g6gBhSQABhSA5g6QA6g5BSgBQBSABA6A5QA6A6AABSQAABSg6A6Qg6A6hSAAQhSAAg6g6g");
	this.shape.setTransform(19.85,19.85);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol7, new cjs.Rectangle(0,0,39.7,39.7), null);


(lib.Symbol3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.leaf3();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol3, new cjs.Rectangle(0,0,172,159), null);


(lib.Symbol2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.leaf2();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol2, new cjs.Rectangle(0,0,225,206), null);


(lib.Symbol1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.leaf1();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol1, new cjs.Rectangle(0,0,186,205), null);


(lib.Symbol_12_Layer_9 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_9
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AhiCtQgWgNgUgUQg5g6gBhSQABhSA5g6QA6g5BSAAQBSAAA6A5QA6A6AABSQAABSg6A6QgUAUgWANQgsAag2AAQg1AAgtgag");
	this.shape.setTransform(65.75,18.4);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(75));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_12_Layer_8 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_8
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("Ag1DAQgxgOgmgmQg5g6gBhSQABhRA5g7QA6g6BSAAQBSAAA6A6QA6A7AABRQAABSg6A6QgmAmgwAOQgaAGgcAAQgcAAgZgGg");
	this.shape.setTransform(19.85,16.55);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(75));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_12_Layer_2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(12,1,1).p("ADmhWQgGBZhIAvQg/AphagEQhZgEhBgsQhGgtgEhH");
	this.shape.setTransform(44.475,59.9678);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(12,1,1).p("AjlhIQAEBCBGArQBBAqBZADQBaAEA/gnQBIgsAGhU");
	this.shape_1.setTransform(44.475,59.7436);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(12,1,1).p("AjlhFQAEBABGAoQBBAoBZADQBaAEA/glQBIgqAGhQ");
	this.shape_2.setTransform(44.475,59.5401);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(12,1,1).p("AjlhBQAEA8BGAnQBBAlBZAEQBaADA/gjQBIgoAGhN");
	this.shape_3.setTransform(44.475,59.3407);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(12,1,1).p("Ajlg/QAEA6BGAlQBBAkBZADQBaAEA/giQBIgnAGhJ");
	this.shape_4.setTransform(44.475,59.1663);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(12,1,1).p("Ajlg8QAEA4BGAjQBBAiBZAEQBaADA/ghQBIglAGhG");
	this.shape_5.setTransform(44.475,59.0377);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(12,1,1).p("Ajlg6QAEA2BGAiQBBAhBZADQBaADA/gfQBIgkAGhD");
	this.shape_6.setTransform(44.475,58.9175);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(12,1,1).p("Ajlg4QAEA0BGAhQBBAgBZADQBaADA/geQBIgjAGhB");
	this.shape_7.setTransform(44.475,58.818);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(12,1,1).p("Ajlg3QAEAzBGAhQBBAfBZADQBaADA/geQBIgiAGhA");
	this.shape_8.setTransform(44.475,58.7386);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(12,1,1).p("Ajlg2QAEAyBGAgQBBAfBZADQBaADA/geQBIghAGg/");
	this.shape_9.setTransform(44.475,58.6888);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(12,1,1).p("Ajlg1QAEAxBGAfQBBAfBZADQBaADA/geQBIggAGg+");
	this.shape_10.setTransform(44.475,58.6638);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(12,1,1).p("ADmg8QgGA+hIAhQg/AdhagDQhZgDhBgeQhGgggEgx");
	this.shape_11.setTransform(44.475,58.6391);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(12,1,1).p("Ajlg7QAEA2BGAjQBBAhBZAEQBaADA/ggQBIgkAGhE");
	this.shape_12.setTransform(44.475,58.9629);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(12,1,1).p("Ajlg/QAEA6BGAmQBBAkBZADQBaADA/giQBIgnAGhJ");
	this.shape_13.setTransform(44.475,59.2372);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(12,1,1).p("AjlhDQAEA+BGAoQBBAmBZAEQBaADA/gkQBIgpAGhP");
	this.shape_14.setTransform(44.475,59.4405);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(12,1,1).p("AjlhHQAEBCBGAqQBBAoBZADQBaAEA/gmQBIgrAGhS");
	this.shape_15.setTransform(44.475,59.644);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(12,1,1).p("AjlhJQAEBDBGAsQBBApBZAEQBaAEA/gnQBIgtAGhV");
	this.shape_16.setTransform(44.475,59.7934);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(12,1,1).p("AjlhLQAEBFBGAtQBBAqBZAEQBaAEA/goQBIguAGhX");
	this.shape_17.setTransform(44.475,59.893);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#000000").ss(12,1,1).p("AjlhMQAEBGBGAtQBBAsBZAEQBaAEA/gpQBIgvAGhZ");
	this.shape_18.setTransform(44.475,59.9428);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},4).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_11}]},37).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape}]},1).wait(15));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_1_copy_Layer_5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_5
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(11.5,1,1).p("AiAAtQAFgvAsgYQAogWAzACQA0ACAhAZQAkAbgEAo");
	this.shape.setTransform(179.17,-80.6583);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(11.5,1,1).p("AiAAoQAFgqAsgWQAogUAzACQA0ACAhAXQAkAYgEAk");
	this.shape_1.setTransform(179.17,-80.1841);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(11.5,1,1).p("AiAAkQAFgmAsgUQAogSAzACQA0ACAhAVQAkAVgEAh");
	this.shape_2.setTransform(179.17,-79.785);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(11.5,1,1).p("AiAAiQAFgjAsgSQAogRAzACQA0ABAhATQAkAUgEAd");
	this.shape_3.setTransform(179.17,-79.4812);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(11.5,1,1).p("AiAAfQAFgfAsgRQAogPAzABQA0ABAhASQAkASgEAb");
	this.shape_4.setTransform(179.17,-79.2031);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(11.5,1,1).p("AiAAdQAFgeAsgQQAogNAzABQA0ABAhAQQAkARgEAZ");
	this.shape_5.setTransform(179.17,-78.9784);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(11.5,1,1).p("AiAAbQAFgcAsgPQAogNAzABQA0ABAhAQQAkAQgEAY");
	this.shape_6.setTransform(179.17,-78.8286);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(11.5,1,1).p("AiAAaQAFgbAsgOQAogNAzABQA0ABAhAPQAkAPgEAY");
	this.shape_7.setTransform(179.17,-78.7287);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(11.5,1,1).p("AiAAaQAFgaAsgPQAogMAzABQA0ABAhAPQAkAPgEAX");
	this.shape_8.setTransform(179.17,-78.7037);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(11.5,1,1).p("AiAAfQAFggAsgRQAogOAzABQA0ABAhASQAkARgEAb");
	this.shape_9.setTransform(179.17,-79.1782);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(11.5,1,1).p("AiAAjQAFgkAsgTQAogQAzABQA0ABAhAUQAkAUgEAe");
	this.shape_10.setTransform(179.17,-79.5779);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(11.5,1,1).p("AiAAlQAFgnAsgUQAogSAzABQA0ACAhAVQAkAWgEAi");
	this.shape_11.setTransform(179.17,-79.8808);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(11.5,1,1).p("AiAAoQAFgpAsgWQAogUAzACQA0ABAhAXQAkAYgEAk");
	this.shape_12.setTransform(179.17,-80.1591);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(11.5,1,1).p("AiAAqQAFgsAsgXQAogVAzACQA0ACAhAYQAkAZgEAm");
	this.shape_13.setTransform(179.17,-80.3837);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(11.5,1,1).p("AiAAsQAFguAsgYQAogVAzACQA0ACAhAZQAkAZgEAn");
	this.shape_14.setTransform(179.17,-80.5335);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(11.5,1,1).p("AiAAtQAFgvAsgYQAogWAzACQA0ACAhAZQAkAbgEAn");
	this.shape_15.setTransform(179.17,-80.6333);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},12).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_8}]},32).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape}]},1).wait(15));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_1_copy_Layer_4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_4
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(11.5,1,1).p("AiAAtQAFgvAsgYQAogWAzACQA0ACAhAZQAkAbgEAo");
	this.shape.setTransform(127.97,-80.6583);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(11.5,1,1).p("AiAAoQAFgqAsgWQAogUAzACQA0ACAhAXQAkAYgEAk");
	this.shape_1.setTransform(127.97,-80.1841);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(11.5,1,1).p("AiAAkQAFgmAsgUQAogSAzACQA0ACAhAVQAkAVgEAh");
	this.shape_2.setTransform(127.97,-79.785);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(11.5,1,1).p("AiAAiQAFgjAsgSQAogRAzACQA0ABAhATQAkAUgEAd");
	this.shape_3.setTransform(127.97,-79.4812);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(11.5,1,1).p("AiAAfQAFgfAsgRQAogPAzABQA0ABAhASQAkASgEAb");
	this.shape_4.setTransform(127.97,-79.2031);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(11.5,1,1).p("AiAAdQAFgeAsgQQAogNAzABQA0ABAhAQQAkARgEAZ");
	this.shape_5.setTransform(127.97,-78.9784);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(11.5,1,1).p("AiAAbQAFgcAsgPQAogNAzABQA0ABAhAQQAkAQgEAY");
	this.shape_6.setTransform(127.97,-78.8286);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(11.5,1,1).p("AiAAaQAFgbAsgOQAogNAzABQA0ABAhAPQAkAPgEAY");
	this.shape_7.setTransform(127.97,-78.7287);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(11.5,1,1).p("AiAAaQAFgaAsgPQAogMAzABQA0ABAhAPQAkAPgEAX");
	this.shape_8.setTransform(127.97,-78.7037);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(11.5,1,1).p("AiAAfQAFggAsgRQAogOAzABQA0ABAhASQAkARgEAb");
	this.shape_9.setTransform(127.97,-79.1782);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(11.5,1,1).p("AiAAjQAFgkAsgTQAogQAzABQA0ABAhAUQAkAUgEAe");
	this.shape_10.setTransform(127.97,-79.5779);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(11.5,1,1).p("AiAAlQAFgnAsgUQAogSAzABQA0ACAhAVQAkAWgEAi");
	this.shape_11.setTransform(127.97,-79.8808);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(11.5,1,1).p("AiAAoQAFgpAsgWQAogUAzACQA0ABAhAXQAkAYgEAk");
	this.shape_12.setTransform(127.97,-80.1591);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(11.5,1,1).p("AiAAqQAFgsAsgXQAogVAzACQA0ACAhAYQAkAZgEAm");
	this.shape_13.setTransform(127.97,-80.3837);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(11.5,1,1).p("AiAAsQAFguAsgYQAogVAzACQA0ACAhAZQAkAZgEAn");
	this.shape_14.setTransform(127.97,-80.5335);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(11.5,1,1).p("AiAAtQAFgvAsgYQAogWAzACQA0ACAhAZQAkAbgEAn");
	this.shape_15.setTransform(127.97,-80.6333);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},12).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_8}]},32).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape}]},1).wait(15));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_1_copy_Layer_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(11,1,1).p("AjShIQAeCMCBAEQCBAEA2gnQA1gmAahB");
	this.shape.setTransform(155.825,-50.8444);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(75));

}).prototype = p = new cjs.MovieClip();


(lib.Scene_1_Layer_6 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_6
	this.instance = new lib.bg_img_vert();
	this.instance.parent = this;
	this.instance.setTransform(-72,-41);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Scene_1_Layer_6, null, null);


(lib.Symbol2copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FF52B8").s().p("AgFAmQgPgFgJgQQgIgPACgPQADgQAMgHIACgBQAMgGAMAGQAQAFAIAQQAJAPgDAPQgCAOgKAHIgCACQgHAEgIAAQgGAAgGgDg");
	this.shape.setTransform(4.23,4.2645);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol2copy, new cjs.Rectangle(0.6,0.2,7.300000000000001,8.200000000000001), null);


(lib.Symbol2_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance_1 = new lib.teeth();
	this.instance_1.parent = this;
	this.instance_1.setTransform(0,0,1.1938,1.3609);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol2_1, new cjs.Rectangle(0,0,19.1,20.4), null);


(lib.Symbol1_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance_1 = new lib.mouth1();
	this.instance_1.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol1_1, new cjs.Rectangle(0,0,35,97), null);


(lib.Symbol16 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Symbol3();
	this.instance.parent = this;
	this.instance.setTransform(86,-560.55,1,1,-21.1968,0,0,86,79.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({rotation:23.2221,y:1179.75},199).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-24.4,-665.7,220.8,1952.4);


(lib.Symbol15 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Symbol2();
	this.instance.parent = this;
	this.instance.setTransform(112.5,-437,1,1,0,0,0,112.5,103);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({scaleX:0.9999,scaleY:0.9999,rotation:40.9683,x:112.55,y:1365.1},199).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-39.9,-540,305,2056.7);


(lib.Symbol14 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Symbol1();
	this.instance.parent = this;
	this.instance.setTransform(93.05,-117.5,1,1,-11.444,0,0,93,102.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({rotation:38.2335,x:92.9,y:1672.75},199).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-43.5,-236.4,272.9,2047.3000000000002);


(lib.Symbol13 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_89 = function() {
		/* Stop at This Frame
		The  timeline will stop/pause at the frame where you insert this code.
		Can also be used to stop/pause the timeline of movieclips.
		*/
		
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(89).call(this.frame_89).wait(1));

	// Symbol_3
	this.instance = new lib.Symbol16();
	this.instance.parent = this;
	this.instance.setTransform(445,148.75,1,1,0,0,0,86,79.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(90));

	// Symbol_2
	this.instance_1 = new lib.Symbol15();
	this.instance_1.parent = this;
	this.instance_1.setTransform(112.5,12.25,1,1,0,0,0,112.5,103);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(39).to({_off:false},0).wait(51));

	// Symbol_1
	this.instance_2 = new lib.Symbol14();
	this.instance_2.parent = this;
	this.instance_2.setTransform(425,-295.25,1,1,0,0,0,93,102.5);
	this.instance_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(79).to({_off:false},0).wait(11));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,-634.1,553.9,248.10000000000002);


(lib.Symbol12copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Symbol_6 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	var mask_graphics_13 = new cjs.Graphics().p("ABZCMQg6g6AAhSQAAhSA6g6QA6g6BTABQBSgBA5A6QA7A6AABSQAABSg7A6Qg5A6hSABQhTgBg6g6g");

	this.timeline.addTween(cjs.Tween.get(mask).to({graphics:null,x:0,y:0}).wait(13).to({graphics:mask_graphics_13,x:42.8,y:17.3}).wait(62));

	// Symbol_7
	this.instance = new lib.Symbol7();
	this.instance.parent = this;
	this.instance.setTransform(65.95,57.65,1,1,0,0,0,19.9,19.9);
	this.instance._off = true;

	var maskedShapeInstanceList = [this.instance];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(13).to({_off:false},0).wait(1).to({y:52.45},0).wait(1).to({y:49.25},0).wait(1).to({y:47},0).wait(1).to({y:45.3},0).wait(1).to({y:44},0).wait(1).to({y:43},0).wait(1).to({y:42.25},0).wait(1).to({y:41.6},0).wait(1).to({y:41.1},0).wait(1).to({y:40.75},0).wait(1).to({y:40.45},0).wait(1).to({y:40.2},0).wait(1).to({y:40.05},0).wait(1).to({y:39.95},0).wait(1).to({y:39.9},0).wait(37).to({x:67.75,y:58.75},8,cjs.Ease.get(1)).wait(2));

	// Symbol_8 (mask)
	var mask_1 = new cjs.Shape();
	mask_1._off = true;
	var mask_1_graphics_13 = new cjs.Graphics().p("AiMCMQg5g6gBhSQABhRA5g7QA6g6BSAAQBSAAA6A6QA6A7AABRQAABSg6A6Qg6A7hSgBQhSABg6g7g");

	this.timeline.addTween(cjs.Tween.get(mask_1).to({graphics:null,x:0,y:0}).wait(13).to({graphics:mask_1_graphics_13,x:19.85,y:16.55}).wait(62));

	// Symbol_9
	this.instance_1 = new lib.Symbol9();
	this.instance_1.parent = this;
	this.instance_1.setTransform(20.2,60.5,1,1,0,0,0,19.9,19.9);
	this.instance_1._off = true;

	var maskedShapeInstanceList = [this.instance_1];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask_1;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(13).to({_off:false},0).wait(1).to({y:54},0).wait(1).to({y:50},0).wait(1).to({y:47.2},0).wait(1).to({y:45.1},0).wait(1).to({y:43.5},0).wait(1).to({y:42.25},0).wait(1).to({y:41.25},0).wait(1).to({y:40.5},0).wait(1).to({y:39.85},0).wait(1).to({y:39.4},0).wait(1).to({y:39},0).wait(1).to({y:38.75},0).wait(1).to({y:38.55},0).wait(1).to({y:38.4},0).wait(1).to({y:38.35},0).wait(37).to({x:22,y:60.5},8,cjs.Ease.get(1)).wait(2));

	// Symbol_11
	this.instance_2 = new lib.Symbol11();
	this.instance_2.parent = this;
	this.instance_2.setTransform(59.05,18.55,1,1,0,0,0,6.7,6.8);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(28).to({y:19.65},0).to({x:62.55,y:17.8},7,cjs.Ease.get(1)).wait(3).to({x:59.05,y:19.65},7,cjs.Ease.get(1)).wait(3).to({x:62.55,y:17.8},7,cjs.Ease.get(1)).wait(3).to({x:59.05,y:19.65},7,cjs.Ease.get(1)).wait(10));

	// Symbol_10
	this.instance_3 = new lib.Symbol10();
	this.instance_3.parent = this;
	this.instance_3.setTransform(15.15,17.55,1,1,0,0,0,6.7,6.8);

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(28).to({x:18.65,y:16.95},7,cjs.Ease.get(1)).wait(3).to({x:15.15,y:17.55},7,cjs.Ease.get(1)).wait(3).to({x:18.65,y:16.95},7,cjs.Ease.get(1)).wait(3).to({x:15.15,y:17.55},7,cjs.Ease.get(1)).wait(10));

	// Layer_8
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("Ag1DAQgxgOgmgmQg5g6gBhSQABhRA5g7QA6g6BSAAQBSAAA6A6QA6A7AABRQAABSg6A6QgmAmgwAOQgaAGgcAAQgcAAgZgGg");
	this.shape.setTransform(19.85,16.55);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(75));

	// Layer_9
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AhiCtQgWgNgUgUQg5g6gBhSQABhSA5g6QA6g6BSABQBSgBA6A6QA6A6AABSQAABSg6A6QgTAUgXANQgsAZg2ABQg1gBgtgZg");
	this.shape_1.setTransform(65.75,17.3);

	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(75));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,-3.3,85.6,40.5);


(lib.Symbol4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Symbol2_1();
	this.instance.parent = this;
	this.instance.setTransform(124,-41.55,1,1,0,0,0,8,7.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(2).to({y:-23.35},5).to({y:-47.85},4).to({y:-41.55},4).wait(3).to({y:-23.35},5).to({y:-47.85},4).to({y:-41.55},4).wait(3).to({y:-23.35},5).to({y:-47.85},4).to({y:-41.55},4).wait(2));

	// Layer_2
	this.instance_1 = new lib.Symbol1_1();
	this.instance_1.parent = this;
	this.instance_1.setTransform(127.55,-4.45,1,1,0,0,0,17.5,48.5);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(2).to({scaleY:0.6103},5).to({scaleY:1.1113},4).to({scaleY:1},4).wait(3).to({scaleY:0.6103},5).to({scaleY:1.1113},4).to({scaleY:1},4).wait(3).to({scaleY:0.6103},5).to({scaleY:1.1113},4).to({scaleY:1},4).wait(2));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(110.1,-58.3,35,107.8);


(lib.Symbol_12_Symbol_11 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Symbol_11
	this.instance = new lib.Symbol11();
	this.instance.parent = this;
	this.instance.setTransform(59.05,19.65,1,1,0,0,0,6.7,6.8);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(17).to({x:73.4,y:17.8},10,cjs.Ease.get(1)).wait(16).to({x:59.05,y:19.65},9,cjs.Ease.get(1)).wait(23));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_12_Symbol_10 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Symbol_10
	this.instance = new lib.Symbol10();
	this.instance.parent = this;
	this.instance.setTransform(15.15,17.55,1,1,0,0,0,6.7,6.8);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(17).to({x:29.5,y:16.95},10,cjs.Ease.get(1)).wait(16).to({x:15.15,y:17.55},9,cjs.Ease.get(1)).wait(23));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_12_Symbol_9 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Symbol_9
	this.instance = new lib.Symbol9();
	this.instance.parent = this;
	this.instance.setTransform(20.2,60.5,1,1,0,0,0,19.9,19.9);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1).to({y:54},0).wait(1).to({y:50},0).wait(1).to({y:47.2},0).wait(1).to({y:45.1},0).wait(1).to({y:43.5},0).wait(1).to({y:42.25},0).wait(1).to({y:41.25},0).wait(1).to({y:40.5},0).wait(1).to({y:39.85},0).wait(1).to({y:39.4},0).wait(1).to({y:39},0).wait(1).to({y:38.75},0).wait(1).to({y:38.55},0).wait(1).to({y:38.4},0).wait(1).to({y:38.35},0).wait(37).to({x:22,y:60.5},8,cjs.Ease.get(1)).wait(15));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_12_Symbol_7 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Symbol_7
	this.instance = new lib.Symbol7();
	this.instance.parent = this;
	this.instance.setTransform(65.95,58.75,1,1,0,0,0,19.9,19.9);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1).to({y:53.25},0).wait(1).to({y:49.8},0).wait(1).to({y:47.4},0).wait(1).to({y:45.65},0).wait(1).to({y:44.25},0).wait(1).to({y:43.2},0).wait(1).to({y:42.35},0).wait(1).to({y:41.7},0).wait(1).to({y:41.2},0).wait(1).to({y:40.8},0).wait(1).to({y:40.45},0).wait(1).to({y:40.25},0).wait(1).to({y:40.05},0).wait(1).to({y:39.95},0).wait(1).to({y:39.9},0).wait(37).to({x:67.75,y:58.75},8,cjs.Ease.get(1)).wait(15));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_1_copy_Layer_3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_3
	this.instance = new lib.Symbol2copy();
	this.instance.parent = this;
	this.instance.setTransform(138.05,-50,2.0168,1.0421,0,46.0839,51.8739,5.4,7.5);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(12).to({_off:false},0).to({scaleY:2.0168,guide:{path:[138.1,-49.8,138.3,-49.5,138.6,-49.1]}},4).wait(1).to({regX:4.2,regY:4.3,scaleX:2.0157,scaleY:2.0174,skewX:45.7135,skewY:51.4647,x:141.8,y:-55.4},0).wait(1).to({scaleX:2.0128,scaleY:2.019,skewX:44.7666,skewY:50.4185,x:142.2,y:-54.9},0).wait(1).to({scaleX:2.0077,scaleY:2.0218,skewX:43.122,skewY:48.6014,x:142.9,y:-54.05},0).wait(1).to({regX:5.4,regY:7.5,scaleX:2.0002,scaleY:2.0261,skewX:40.635,skewY:45.8537,x:141.55,y:-46.3},0).wait(1).to({regX:4.2,regY:4.3,scaleX:1.9919,scaleY:2.0343,skewX:35.8259,skewY:40.5708,x:146.25,y:-51.65},0).wait(1).to({scaleX:1.981,scaleY:2.0452,skewX:29.523,skewY:33.647,x:149.65,y:-50.8},0).wait(1).to({scaleX:1.9679,scaleY:2.0583,skewX:21.8959,skewY:25.2684,x:153.6,y:-50.65},0).wait(1).to({scaleX:1.9537,scaleY:2.0725,skewX:13.6434,skewY:16.2028,x:157,y:-50.85},0).wait(1).to({scaleX:1.9403,scaleY:2.0859,skewX:5.8635,skewY:7.6565,x:159.85,y:-51.2},0).wait(1).to({scaleX:1.9293,scaleY:2.0969,skewX:-0.5529,skewY:0.6079,x:162.25,y:-51.75},0).wait(1).to({scaleX:1.9211,scaleY:2.105,skewX:-5.291,skewY:-4.597,x:164.1,y:-52.5},0).wait(1).to({scaleX:1.9157,scaleY:2.1105,skewX:-8.4481,skewY:-8.0652,x:165.3,y:-53.2},0).wait(1).to({regX:5.4,regY:7.5,scaleX:1.9125,scaleY:2.1136,skewX:-10.2658,skewY:-10.062,x:169.5,y:-47.5},0).to({scaleX:1.9124,scaleY:2.1161,rotation:-11.0195,skewX:0,skewY:0,guide:{path:[169.5,-47.5,169.6,-47.6,169.7,-47.7]}},1).wait(6).to({x:169.9,y:-47.75},0).wait(1).to({regX:4.2,regY:4.3,scaleX:1.9134,scaleY:2.1153,rotation:0,skewX:-10.3189,skewY:-10.2667,x:166.2,y:-53.85},0).wait(1).to({scaleX:1.9164,scaleY:2.1124,skewX:-8.0374,skewY:-7.8187,x:165.65,y:-53.45},0).wait(1).to({scaleX:1.9218,scaleY:2.1073,skewX:-3.8923,skewY:-3.371,x:164.65,y:-52.7},0).wait(1).to({scaleX:1.9301,scaleY:2.0994,skewX:2.3559,skewY:3.3333,x:162.45,y:-51.8},0).wait(1).to({scaleX:1.9411,scaleY:2.0889,skewX:10.7425,skewY:12.3321,x:159.15,y:-51.1},0).wait(1).to({scaleX:1.9544,scaleY:2.0763,skewX:20.8508,skewY:23.1784,x:155.5,y:-50.65},0).wait(1).to({scaleX:1.9687,scaleY:2.0627,skewX:31.6853,skewY:34.8038,x:152.15,y:-50.55},0).wait(1).to({scaleX:1.9822,scaleY:2.0498,skewX:41.9809,skewY:45.851,x:149.45,y:-50.65},0).wait(1).to({scaleX:1.9938,scaleY:2.0388,skewX:50.7752,skewY:55.2873,x:147.55,y:-51},0).wait(1).to({scaleX:2.0028,scaleY:2.0301,skewX:57.6617,skewY:62.6765,x:146,y:-51.85},0).wait(1).to({scaleX:2.0094,scaleY:2.0239,skewX:62.6463,skewY:68.025,x:144.95,y:-52.6},0).wait(1).to({scaleX:2.0137,scaleY:2.0198,skewX:65.9217,skewY:71.5395,x:144.35,y:-53.3},0).wait(1).to({scaleX:2.0161,scaleY:2.0175,skewX:67.7259,skewY:73.4754,x:144,y:-53.65},0).wait(1).to({regX:5.4,regY:7.5,scaleX:2.0168,scaleY:2.0168,skewX:68.2829,skewY:74.0731,x:138.5,y:-49.15},0).to({regY:7.4,scaleX:2.0137,scaleY:1.4231,skewX:51.6479,skewY:57.4249,guide:{path:[138.6,-49,138.7,-48.9,138.8,-48.8]}},3).to({regX:5.5,scaleX:2.0168,scaleY:1.2283,skewX:46.0842,skewY:51.8739,x:134.95,y:-45.55},1).to({_off:true},1).wait(20));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol1copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_74 = function() {
		this.___loopingOver___ = true;
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(74).call(this.frame_74).wait(1));

	// Layer_5_obj_
	this.Layer_5 = new lib.Symbol_1_copy_Layer_5();
	this.Layer_5.name = "Layer_5";
	this.Layer_5.parent = this;
	this.Layer_5.setTransform(179.2,-80.7,1,1,0,0,0,179.2,-80.7);
	this.Layer_5.depth = 0;
	this.Layer_5.isAttachedToCamera = 0
	this.Layer_5.isAttachedToMask = 0
	this.Layer_5.layerDepth = 0
	this.Layer_5.layerIndex = 0
	this.Layer_5.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_5).wait(75));

	// Layer_4_obj_
	this.Layer_4 = new lib.Symbol_1_copy_Layer_4();
	this.Layer_4.name = "Layer_4";
	this.Layer_4.parent = this;
	this.Layer_4.setTransform(128,-80.7,1,1,0,0,0,128,-80.7);
	this.Layer_4.depth = 0;
	this.Layer_4.isAttachedToCamera = 0
	this.Layer_4.isAttachedToMask = 0
	this.Layer_4.layerDepth = 0
	this.Layer_4.layerIndex = 1
	this.Layer_4.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_4).wait(75));

	// Layer_1_obj_
	this.Layer_1 = new lib.Symbol_1_copy_Layer_1();
	this.Layer_1.name = "Layer_1";
	this.Layer_1.parent = this;
	this.Layer_1.setTransform(155.8,-50.9,1,1,0,0,0,155.8,-50.9);
	this.Layer_1.depth = 0;
	this.Layer_1.isAttachedToCamera = 0
	this.Layer_1.isAttachedToMask = 0
	this.Layer_1.layerDepth = 0
	this.Layer_1.layerIndex = 2
	this.Layer_1.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_1).wait(75));

	// Layer_3_obj_
	this.Layer_3 = new lib.Symbol_1_copy_Layer_3();
	this.Layer_3.name = "Layer_3";
	this.Layer_3.parent = this;
	this.Layer_3.depth = 0;
	this.Layer_3.isAttachedToCamera = 0
	this.Layer_3.isAttachedToMask = 0
	this.Layer_3.layerDepth = 0
	this.Layer_3.layerIndex = 3
	this.Layer_3.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_3).wait(12).to({x:136.05,y:-53.65},0).wait(5).to({x:136.0632,y:-53.6553},0).wait(3).to({x:136.05,y:-53.65},0).wait(1).to({x:136.0632,y:-53.6553},0).wait(8).to({x:136.05,y:-53.65},0).to({x:0,y:0},1).wait(6).to({x:136.05,y:-53.65},0).wait(1).to({x:136.0632,y:-53.6553},0).wait(13).to({x:136.05,y:-53.65},0).wait(3).to({x:0,y:0},1).wait(21));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(109.3,-91.1,88.60000000000001,53.099999999999994);


(lib.Symbol12 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_74 = function() {
		this.___loopingOver___ = true;
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(74).call(this.frame_74).wait(1));

	// Layer_2_obj_
	this.Layer_2 = new lib.Symbol_12_Layer_2();
	this.Layer_2.name = "Layer_2";
	this.Layer_2.parent = this;
	this.Layer_2.setTransform(44.5,60,1,1,0,0,0,44.5,60);
	this.Layer_2.depth = 0;
	this.Layer_2.isAttachedToCamera = 0
	this.Layer_2.isAttachedToMask = 0
	this.Layer_2.layerDepth = 0
	this.Layer_2.layerIndex = 0
	this.Layer_2.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_2).wait(75));

	// Symbol_6 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("ABZCMQg6g6AAhSQAAhSA6g6QA6g5BTAAQBSAAA5A5QA7A6AABSQAABSg7A6Qg5A6hSABQhTgBg6g6g");
	mask.setTransform(42.8,18.4);

	// Symbol_7_obj_
	this.Symbol_7 = new lib.Symbol_12_Symbol_7();
	this.Symbol_7.name = "Symbol_7";
	this.Symbol_7.parent = this;
	this.Symbol_7.setTransform(65.9,58.7,1,1,0,0,0,65.9,58.7);
	this.Symbol_7.depth = 0;
	this.Symbol_7.isAttachedToCamera = 0
	this.Symbol_7.isAttachedToMask = 0
	this.Symbol_7.layerDepth = 0
	this.Symbol_7.layerIndex = 1
	this.Symbol_7.maskLayerName = 0

	var maskedShapeInstanceList = [this.Symbol_7];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.Symbol_7).wait(1).to({regX:66.8,regY:49.3,x:66.8,y:49.3},0).wait(14).to({regX:65.9,regY:58.7,x:65.9,y:58.7},0).wait(60));

	// Symbol_8 (mask)
	var mask_1 = new cjs.Shape();
	mask_1._off = true;
	var mask_1_graphics_0 = new cjs.Graphics().p("AiMCMQg5g6gBhSQABhRA5g7QA6g6BSAAQBSAAA6A6QA6A7AABRQAABSg6A6Qg6A7hSgBQhSABg6g7g");
	var mask_1_graphics_52 = new cjs.Graphics().p("AiMCMQg5g6gBhSQABhSA5g6QA6g5BSgBQBSABA6A5QA6A6AABSQAABSg6A6Qg6A6hSAAQhSAAg6g6g");

	this.timeline.addTween(cjs.Tween.get(mask_1).to({graphics:mask_1_graphics_0,x:19.85,y:16.55}).wait(52).to({graphics:mask_1_graphics_52,x:19.85,y:19.85}).wait(23));

	// Symbol_9_obj_
	this.Symbol_9 = new lib.Symbol_12_Symbol_9();
	this.Symbol_9.name = "Symbol_9";
	this.Symbol_9.parent = this;
	this.Symbol_9.setTransform(20.2,60.5,1,1,0,0,0,20.2,60.5);
	this.Symbol_9.depth = 0;
	this.Symbol_9.isAttachedToCamera = 0
	this.Symbol_9.isAttachedToMask = 0
	this.Symbol_9.layerDepth = 0
	this.Symbol_9.layerIndex = 2
	this.Symbol_9.maskLayerName = 0

	var maskedShapeInstanceList = [this.Symbol_9];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask_1;
	}

	this.timeline.addTween(cjs.Tween.get(this.Symbol_9).wait(1).to({regX:21.1,regY:49.4,x:21.1,y:49.4},0).wait(14).to({regX:20.2,regY:60.5,x:20.2,y:60.5},0).wait(60));

	// Symbol_11_obj_
	this.Symbol_11 = new lib.Symbol_12_Symbol_11();
	this.Symbol_11.name = "Symbol_11";
	this.Symbol_11.parent = this;
	this.Symbol_11.setTransform(59.1,19.6,1,1,0,0,0,59.1,19.6);
	this.Symbol_11.depth = 0;
	this.Symbol_11.isAttachedToCamera = 0
	this.Symbol_11.isAttachedToMask = 0
	this.Symbol_11.layerDepth = 0
	this.Symbol_11.layerIndex = 3
	this.Symbol_11.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Symbol_11).wait(75));

	// Symbol_10_obj_
	this.Symbol_10 = new lib.Symbol_12_Symbol_10();
	this.Symbol_10.name = "Symbol_10";
	this.Symbol_10.parent = this;
	this.Symbol_10.setTransform(15.2,17.5,1,1,0,0,0,15.2,17.5);
	this.Symbol_10.depth = 0;
	this.Symbol_10.isAttachedToCamera = 0
	this.Symbol_10.isAttachedToMask = 0
	this.Symbol_10.layerDepth = 0
	this.Symbol_10.layerIndex = 4
	this.Symbol_10.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Symbol_10).wait(75));

	// Layer_8_obj_
	this.Layer_8 = new lib.Symbol_12_Layer_8();
	this.Layer_8.name = "Layer_8";
	this.Layer_8.parent = this;
	this.Layer_8.setTransform(19.9,16.6,1,1,0,0,0,19.9,16.6);
	this.Layer_8.depth = 0;
	this.Layer_8.isAttachedToCamera = 0
	this.Layer_8.isAttachedToMask = 0
	this.Layer_8.layerDepth = 0
	this.Layer_8.layerIndex = 5
	this.Layer_8.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_8).wait(75));

	// Layer_9_obj_
	this.Layer_9 = new lib.Symbol_12_Layer_9();
	this.Layer_9.name = "Layer_9";
	this.Layer_9.parent = this;
	this.Layer_9.setTransform(65.8,18.4,1,1,0,0,0,65.8,18.4);
	this.Layer_9.depth = 0;
	this.Layer_9.isAttachedToCamera = 0
	this.Layer_9.isAttachedToMask = 0
	this.Layer_9.layerDepth = 0
	this.Layer_9.layerIndex = 6
	this.Layer_9.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_9).wait(75));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,-3.3,85.6,78);


(lib.Symbol5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.instance = new lib.Symbol12copy();
	this.instance.parent = this;
	this.instance.setTransform(13.45,-40.7,0.9679,0.9679,0,-179.9864,0.0135,43.4,34.9);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	// Layer_1
	this.instance_1 = new lib.Symbol4();
	this.instance_1.parent = this;
	this.instance_1.setTransform(15.65,30.4,0.8911,0.6233,0,0,0,127.6,-4.2);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol5, new cjs.Rectangle(-28.5,-42.8,82.8,103.3), null);


(lib.Scene_1_Layer_3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_3
	this.instance = new lib.Symbol12();
	this.instance.parent = this;
	this.instance.setTransform(175.85,487.65,0.5626,0.5626,0,0.0451,-179.9534,43.1,35.1);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Scene_1_Layer_3, null, null);


(lib.Scene_1_Layer_2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.instance = new lib.Symbol1copy();
	this.instance.parent = this;
	this.instance.setTransform(190,419.05,0.6472,0.6214,0,0,0,156.3,-52.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Scene_1_Layer_2, null, null);


(lib.Scene_1_Layer_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Symbol13();
	this.instance.parent = this;
	this.instance.setTransform(165.85,382.45,0.5812,0.5812,0,0,0,265.6,313.2);

	this.instance_1 = new lib.Symbol5();
	this.instance_1.parent = this;
	this.instance_1.setTransform(186.65,343.65,0.5812,0.5812,0,0,0,15.8,30.4);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_1},{t:this.instance}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Scene_1_Layer_1, null, null);


// stage content:
(lib.Nov_vert = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	this.___GetDepth___ = function(obj) {
		var depth = obj.depth;
		var cameraObj = this.___camera___instance;
		if(cameraObj && cameraObj.depth && obj.isAttachedToCamera)
		{
			depth += depth + cameraObj.depth;
		}
		return depth;
		}
	this.___needSorting___ = function() {
		for (var i = 0; i < this.getNumChildren() - 1; i++)
		{
			var prevDepth = this.___GetDepth___(this.getChildAt(i));
			var nextDepth = this.___GetDepth___(this.getChildAt(i + 1));
			if (prevDepth < nextDepth)
				return true;
		}
		return false;
	}
	this.___sortFunction___ = function(obj1, obj2) {
		return (this.exportRoot.___GetDepth___(obj2) - this.exportRoot.___GetDepth___(obj1));
	}
	this.on('tick', function (event){
		var curTimeline = event.currentTarget;
		if (curTimeline.___needSorting___()){
			this.sortChildren(curTimeline.___sortFunction___);
		}
	});

	// Layer_1_obj_
	this.Layer_1 = new lib.Scene_1_Layer_1();
	this.Layer_1.name = "Layer_1";
	this.Layer_1.parent = this;
	this.Layer_1.setTransform(172.5,118,1,1,0,0,0,172.5,118);
	this.Layer_1.depth = 0;
	this.Layer_1.isAttachedToCamera = 0
	this.Layer_1.isAttachedToMask = 0
	this.Layer_1.layerDepth = 0
	this.Layer_1.layerIndex = 0
	this.Layer_1.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_1).wait(1));

	// Layer_2_obj_
	this.Layer_2 = new lib.Scene_1_Layer_2();
	this.Layer_2.name = "Layer_2";
	this.Layer_2.parent = this;
	this.Layer_2.setTransform(152.8,423.3,1,1,0,0,0,152.8,423.3);
	this.Layer_2.depth = 0;
	this.Layer_2.isAttachedToCamera = 0
	this.Layer_2.isAttachedToMask = 0
	this.Layer_2.layerDepth = 0
	this.Layer_2.layerIndex = 1
	this.Layer_2.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_2).wait(1));

	// Layer_3_obj_
	this.Layer_3 = new lib.Scene_1_Layer_3();
	this.Layer_3.name = "Layer_3";
	this.Layer_3.parent = this;
	this.Layer_3.setTransform(176,489.5,1,1,0,0,0,176,489.5);
	this.Layer_3.depth = 0;
	this.Layer_3.isAttachedToCamera = 0
	this.Layer_3.isAttachedToMask = 0
	this.Layer_3.layerDepth = 0
	this.Layer_3.layerIndex = 2
	this.Layer_3.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_3).wait(1));

	// Layer_6_obj_
	this.Layer_6 = new lib.Scene_1_Layer_6();
	this.Layer_6.name = "Layer_6";
	this.Layer_6.parent = this;
	this.Layer_6.setTransform(187,416,1,1,0,0,0,187,416);
	this.Layer_6.depth = 0;
	this.Layer_6.isAttachedToCamera = 0
	this.Layer_6.isAttachedToMask = 0
	this.Layer_6.layerDepth = 0
	this.Layer_6.layerIndex = 3
	this.Layer_6.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_6).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(115.5,259.7,330.5,613.3);
// library properties:
lib.properties = {
	id: 'E60910625869DC40955AB925093264E5',
	width: 375,
	height: 812,
	fps: 30,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: [
		{src:"assets/images/bg_img_vert32-2.jpg", id:"bg_img_vert"},
		{src:"assets/images/leaf132-2.png", id:"leaf1"},
		{src:"assets/images/leaf232-2.png", id:"leaf2"},
		{src:"assets/images/leaf332-2.png", id:"leaf3"},
		{src:"assets/images/mouth132-2.png", id:"mouth1"},
		{src:"assets/images/teeth32-2.png", id:"teeth"}
	],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['E60910625869DC40955AB925093264E5'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}


// Layer depth API : 

AdobeAn.Layer = new function() {
	this.getLayerZDepth = function(timeline, layerName)
	{
		if(layerName === "Camera")
		layerName = "___camera___instance";
		var script = "if(timeline." + layerName + ") timeline." + layerName + ".depth; else 0;";
		return eval(script);
	}
	this.setLayerZDepth = function(timeline, layerName, zDepth)
	{
		const MAX_zDepth = 10000;
		const MIN_zDepth = -5000;
		if(zDepth > MAX_zDepth)
			zDepth = MAX_zDepth;
		else if(zDepth < MIN_zDepth)
			zDepth = MIN_zDepth;
		if(layerName === "Camera")
		layerName = "___camera___instance";
		var script = "if(timeline." + layerName + ") timeline." + layerName + ".depth = " + zDepth + ";";
		eval(script);
	}
	this.removeLayer = function(timeline, layerName)
	{
		if(layerName === "Camera")
		layerName = "___camera___instance";
		var script = "if(timeline." + layerName + ") timeline.removeChild(timeline." + layerName + ");";
		eval(script);
	}
	this.addNewLayer = function(timeline, layerName, zDepth)
	{
		if(layerName === "Camera")
		layerName = "___camera___instance";
		zDepth = typeof zDepth !== 'undefined' ? zDepth : 0;
		var layer = new createjs.MovieClip();
		layer.name = layerName;
		layer.depth = zDepth;
		layer.layerIndex = 0;
		timeline.addChild(layer);
	}
}


})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;

$(function () {
    var canvas, stage, exportRoot, anim_container, dom_overlay_container, fnStartAnimation;
    function init() {
        canvas = document.getElementById("canvas32-2");
        anim_container = document.getElementById("animation_container32-2");
        dom_overlay_container = document.getElementById("dom_overlay_container32-2");
        var comp=AdobeAn.getComposition("E60910625869DC40955AB925093264E5");
        var lib=comp.getLibrary();
        createjs.MotionGuidePlugin.install();
        var loader = new createjs.LoadQueue(false);
        loader.addEventListener("fileload", function(evt){handleFileLoad(evt,comp)});
        loader.addEventListener("complete", function(evt){handleComplete(evt,comp)});
        var lib=comp.getLibrary();
        loader.loadManifest(lib.properties.manifest);
    }
    function handleFileLoad(evt, comp) {
        var images=comp.getImages();
        if (evt && (evt.item.type == "image")) { images[evt.item.id] = evt.result; }
    }
    function handleComplete(evt,comp) {
        //This function is always called, irrespective of the content. You can use the variable "stage" after it is created in token create_stage.
        var lib=comp.getLibrary();
        var ss=comp.getSpriteSheet();
        var queue = evt.target;
        var ssMetadata = lib.ssMetadata;
        for(i=0; i<ssMetadata.length; i++) {
            ss[ssMetadata[i].name] = new createjs.SpriteSheet( {"images": [queue.getResult(ssMetadata[i].name)], "frames": ssMetadata[i].frames} )
        }
        var preloaderDiv = document.getElementById("_preload_div_32");
        preloaderDiv.style.display = 'none';
        canvas.style.display = 'block';
        exportRoot = new lib.Nov_vert();
        stage = new lib.Stage(canvas);
        //Registers the "tick" event listener.
        fnStartAnimation = function() {
            stage.addChild(exportRoot);
            createjs.Ticker.setFPS(lib.properties.fps);
            createjs.Ticker.addEventListener("tick", stage)
            stage.addEventListener("tick", handleTick)
            function getProjectionMatrix(container, totalDepth) {
                var focalLength = 528.25;
                var projectionCenter = { x : lib.properties.width/2, y : lib.properties.height/2 };
                var scale = (totalDepth + focalLength)/focalLength;
                var scaleMat = new createjs.Matrix2D;
                scaleMat.a = 1/scale;
                scaleMat.d = 1/scale;
                var projMat = new createjs.Matrix2D;
                projMat.tx = -projectionCenter.x;
                projMat.ty = -projectionCenter.y;
                projMat = projMat.prependMatrix(scaleMat);
                projMat.tx += projectionCenter.x;
                projMat.ty += projectionCenter.y;
                return projMat;
            }
            function handleTick(event) {
                var cameraInstance = exportRoot.___camera___instance;
                if(cameraInstance !== undefined && cameraInstance.pinToObject !== undefined)
                {
                    cameraInstance.x = cameraInstance.pinToObject.x + cameraInstance.pinToObject.pinOffsetX;
                    cameraInstance.y = cameraInstance.pinToObject.y + cameraInstance.pinToObject.pinOffsetY;
                    if(cameraInstance.pinToObject.parent !== undefined && cameraInstance.pinToObject.parent.depth !== undefined)
                        cameraInstance.depth = cameraInstance.pinToObject.parent.depth + cameraInstance.pinToObject.pinOffsetZ;
                }
                applyLayerZDepth(exportRoot);
            }
            function applyLayerZDepth(parent)
            {
                var cameraInstance = parent.___camera___instance;
                var focalLength = 528.25;
                var projectionCenter = { 'x' : 0, 'y' : 0};
                if(parent === exportRoot)
                {
                    var stageCenter = { 'x' : lib.properties.width/2, 'y' : lib.properties.height/2 };
                    projectionCenter.x = stageCenter.x;
                    projectionCenter.y = stageCenter.y;
                }
                for(child in parent.children)
                {
                    var layerObj = parent.children[child];
                    if(layerObj == cameraInstance)
                        continue;
                    applyLayerZDepth(layerObj, cameraInstance);
                    if(layerObj.layerDepth === undefined)
                        continue;
                    if(layerObj.currentFrame != layerObj.parent.currentFrame)
                    {
                        layerObj.gotoAndPlay(layerObj.parent.currentFrame);
                    }
                    var matToApply = new createjs.Matrix2D;
                    var cameraMat = new createjs.Matrix2D;
                    var totalDepth = layerObj.layerDepth ? layerObj.layerDepth : 0;
                    var cameraDepth = 0;
                    if(cameraInstance && !layerObj.isAttachedToCamera)
                    {
                        var mat = cameraInstance.getMatrix();
                        mat.tx -= projectionCenter.x;
                        mat.ty -= projectionCenter.y;
                        cameraMat = mat.invert();
                        cameraMat.prependTransform(projectionCenter.x, projectionCenter.y, 1, 1, 0, 0, 0, 0, 0);
                        cameraMat.appendTransform(-projectionCenter.x, -projectionCenter.y, 1, 1, 0, 0, 0, 0, 0);
                        if(cameraInstance.depth)
                            cameraDepth = cameraInstance.depth;
                    }
                    if(layerObj.depth)
                    {
                        totalDepth = layerObj.depth;
                    }
                    //Offset by camera depth
                    totalDepth -= cameraDepth;
                    if(totalDepth < -focalLength)
                    {
                        matToApply.a = 0;
                        matToApply.d = 0;
                    }
                    else
                    {
                        if(layerObj.layerDepth)
                        {
                            var sizeLockedMat = getProjectionMatrix(parent, layerObj.layerDepth);
                            if(sizeLockedMat)
                            {
                                sizeLockedMat.invert();
                                matToApply.prependMatrix(sizeLockedMat);
                            }
                        }
                        matToApply.prependMatrix(cameraMat);
                        var projMat = getProjectionMatrix(parent, totalDepth);
                        if(projMat)
                        {
                            matToApply.prependMatrix(projMat);
                        }
                    }
                    layerObj.transformMatrix = matToApply;
                }
            }
        }
        //Code to support hidpi screens and responsive scaling.
        function makeResponsive(isResp, respDim, isScale, scaleType) {
            var lastW, lastH, lastS=1;
            window.addEventListener('resize', resizeCanvas);
            resizeCanvas();
            function resizeCanvas() {
                var w = lib.properties.width, h = lib.properties.height;
                var iw = window.innerWidth, ih=window.innerHeight;
                var pRatio = window.devicePixelRatio || 1, xRatio=iw/w, yRatio=ih/h, sRatio=1;
                if(isResp) {
                    if((respDim=='width'&&lastW==iw) || (respDim=='height'&&lastH==ih)) {
                        sRatio = lastS;
                    }
                    else if(!isScale) {
                        if(iw<w || ih<h)
                            sRatio = Math.min(xRatio, yRatio);
                    }
                    else if(scaleType==1) {
                        sRatio = Math.min(xRatio, yRatio);
                    }
                    else if(scaleType==2) {
                        sRatio = Math.max(xRatio, yRatio);
                    }
                }
                canvas.width = w*pRatio*sRatio;
                canvas.height = h*pRatio*sRatio;
                canvas.style.width = anim_container.style.width = dom_overlay_container.style.width = preloaderDiv.style.width = w*sRatio+'px';
                canvas.style.height = anim_container.style.height = dom_overlay_container.style.height = preloaderDiv.style.height = h*sRatio+'px';
                stage.scaleX = pRatio*sRatio;
                stage.scaleY = pRatio*sRatio;
                lastW = iw; lastH = ih; lastS = sRatio;
                stage.tickOnUpdate = false;
                stage.update();
                stage.tickOnUpdate = true;
            }
        }
        makeResponsive(true,'both',true,2);
        AdobeAn.compositionLoaded(lib.properties.id);
        fnStartAnimation();
    }
    init();
});