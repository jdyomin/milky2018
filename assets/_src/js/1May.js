(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [];


// symbols:



(lib.bg2 = function() {
	this.initialize(img.bg2);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,1058,1207);// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.Symbol2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(12,1,1).p("AisBkQBdAfAjA1AADi3QBeDpiTBUACtiPQhADBiLB2");
	this.shape.setTransform(17.3,18.4);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol2, new cjs.Rectangle(-6,-6,46.6,48.9), null);


(lib.Symbol1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 5
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(12,1,1).p("AA0huQBOCwjLAt");
	this.shape.setTransform(17,22.5);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(12,1,1).p("AhMBuQC4g6gmih");
	this.shape_1.setTransform(17.3,22.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(12,1,1).p("AhSBtQCnhFgCiU");
	this.shape_2.setTransform(17.9,22.7);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(12,1,1).p("AhaBtQCZhQAciJ");
	this.shape_3.setTransform(18.8,22.7);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(12,1,1).p("AhhBsQCNhYA2h/");
	this.shape_4.setTransform(19.4,22.8);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(12,1,1).p("AhmBsQCChfBLh4");
	this.shape_5.setTransform(20,22.8);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(12,1,1).p("AhrBsQB7hkBchz");
	this.shape_6.setTransform(20.4,22.9);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(12,1,1).p("AhuBrQB1hoBoht");
	this.shape_7.setTransform(20.7,22.9);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(12,1,1).p("AhwBrQByhqBvhr");
	this.shape_8.setTransform(20.9,22.9);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(12,1,1).p("ABxhqIjhDV");
	this.shape_9.setTransform(21,22.9);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(12,1,1).p("AhlBsQCFhdBGh6");
	this.shape_10.setTransform(19.9,22.8);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(12,1,1).p("AhgBsQCPhWAxiB");
	this.shape_11.setTransform(19.3,22.8);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(12,1,1).p("AhUBtQCihJAHiQ");
	this.shape_12.setTransform(18.2,22.7);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(12,1,1).p("AhPBuQCshCgOiZ");
	this.shape_13.setTransform(17.7,22.7);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(12,1,1).p("AhMBuQC2g7gjig");
	this.shape_14.setTransform(17.4,22.6);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(12,1,1).p("AhKBuQDBg0g5in");
	this.shape_15.setTransform(17.1,22.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},18).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_9}]},32).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape}]},1).wait(17));

	// Layer 6
	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(12,1,1).p("AhQg+QBDgiA3ATQA4AUgaAMQgaAMABACQAPA1hvA/");
	this.shape_16.setTransform(14.4,28.3);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(12,1,1).p("AhRhHQBCgZA1ASQA6AUgSASQgFBAhvA/");
	this.shape_17.setTransform(13.2,28.1);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#000000").ss(12,1,1).p("AhUhPQBDgRAyASQA9AUgLAWQAAA+hvA/");
	this.shape_18.setTransform(12.3,27.9);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#000000").ss(12,1,1).p("AhWhWQBDgKAwARQA/AWgFAYQAEA9hvA/");
	this.shape_19.setTransform(11.5,27.7);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#000000").ss(12,1,1).p("AhYhbQBCgEAuARQBBAWAAAbQAIA8hvA+");
	this.shape_20.setTransform(10.9,27.4);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#000000").ss(12,1,1).p("AhbhfQBCABAtAQQBCAXAFAeQAKA6hvA/");
	this.shape_21.setTransform(10.4,27.2);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#000000").ss(12,1,1).p("AhdhhQBCAFAsAPQBDAXAIAhQANA5hvA+");
	this.shape_22.setTransform(10.1,26.9);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#000000").ss(12,1,1).p("AhehjQBCAIArAQQBEAWAKAiQAOA5hvA+");
	this.shape_23.setTransform(9.8,26.8);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#000000").ss(12,1,1).p("AhfhkQBCAKArAPQBEAXAMAjQAPA4hvA+");
	this.shape_24.setTransform(9.7,26.7);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f().s("#000000").ss(12,1,1).p("AhfhkQBCAKAqAPQBFAXAMAjQAAABAAABQAPA2hvA+");
	this.shape_25.setTransform(9.6,26.6);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f().s("#000000").ss(12,1,1).p("AhaheQBCAAAtAQQBCAXAEAdQAJA7hvA+");
	this.shape_26.setTransform(10.5,27.2);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f().s("#000000").ss(12,1,1).p("AhYhaQBDgFAuAQQBBAWgBAcQAHA7hvA/");
	this.shape_27.setTransform(11,27.5);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f().s("#000000").ss(12,1,1).p("AhUhSQBCgOAyARQA9AVgJAXQABA+hvA+");
	this.shape_28.setTransform(12,27.8);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f().s("#000000").ss(12,1,1).p("AhThNQBDgTAzASQA8AUgNAUQgCA/hvA/");
	this.shape_29.setTransform(12.6,28);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f().s("#000000").ss(12,1,1).p("AhShIQBDgYA0ASQA7AUgSASQgEBAhvA/");
	this.shape_30.setTransform(13.2,28.1);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f().s("#000000").ss(12,1,1).p("AhRhDQBDgdA2ATQA5AUgWAOQgHBChvA/");
	this.shape_31.setTransform(13.8,28.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_16}]}).to({state:[{t:this.shape_16}]},18).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_25}]},32).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_27}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_28}]},1).to({state:[{t:this.shape_29}]},1).to({state:[{t:this.shape_30}]},1).to({state:[{t:this.shape_31}]},1).to({state:[{t:this.shape_16}]},1).wait(17));

	// Layer 7
	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f().s("#000000").ss(12,1,1).p("AgRhgQBuBdh+Bk");
	this.shape_32.setTransform(10,22.6);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f().s("#000000").ss(12,1,1).p("AggBvQBvhxhNhs");
	this.shape_33.setTransform(9.9,21.2);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f().s("#000000").ss(12,1,1).p("AggB7QBgh8guh5");
	this.shape_34.setTransform(9.9,20);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f().s("#000000").ss(12,1,1).p("AgiCFQBViGgUiD");
	this.shape_35.setTransform(10.1,19);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f().s("#000000").ss(12,1,1).p("AgmCOQBLiOACiN");
	this.shape_36.setTransform(10.5,18.1);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f().s("#000000").ss(12,1,1).p("AgrCVQBCiVAViU");
	this.shape_37.setTransform(11,17.4);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f().s("#000000").ss(12,1,1).p("AgvCaQA8iaAjiZ");
	this.shape_38.setTransform(11.4,16.9);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f().s("#000000").ss(12,1,1).p("AgxCeQA3ieAsid");
	this.shape_39.setTransform(11.6,16.5);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f().s("#000000").ss(12,1,1).p("AgzChQA0ihAzig");
	this.shape_40.setTransform(11.8,16.2);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f().s("#000000").ss(12,1,1).p("AA1ihIhpFD");
	this.shape_41.setTransform(11.9,16.2);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f().s("#000000").ss(12,1,1).p("AgqCTQBEiTAQiS");
	this.shape_42.setTransform(10.9,17.6);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f().s("#000000").ss(12,1,1).p("AglCMQBNiMgCiL");
	this.shape_43.setTransform(10.4,18.3);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f().s("#000000").ss(12,1,1).p("AggB+QBdh/gnh8");
	this.shape_44.setTransform(9.9,19.7);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f().s("#000000").ss(12,1,1).p("AggB3QBlh5g4h0");
	this.shape_45.setTransform(9.9,20.4);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.f().s("#000000").ss(12,1,1).p("AggBvQBthxhKhs");
	this.shape_46.setTransform(9.9,21.2);

	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.f().s("#000000").ss(12,1,1).p("AggBpQB1hrhbhl");
	this.shape_47.setTransform(9.9,21.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_32}]}).to({state:[{t:this.shape_32}]},18).to({state:[{t:this.shape_33}]},1).to({state:[{t:this.shape_34}]},1).to({state:[{t:this.shape_35}]},1).to({state:[{t:this.shape_36}]},1).to({state:[{t:this.shape_37}]},1).to({state:[{t:this.shape_38}]},1).to({state:[{t:this.shape_39}]},1).to({state:[{t:this.shape_40}]},1).to({state:[{t:this.shape_41}]},1).to({state:[{t:this.shape_41}]},32).to({state:[{t:this.shape_38}]},1).to({state:[{t:this.shape_42}]},1).to({state:[{t:this.shape_43}]},1).to({state:[{t:this.shape_35}]},1).to({state:[{t:this.shape_44}]},1).to({state:[{t:this.shape_45}]},1).to({state:[{t:this.shape_46}]},1).to({state:[{t:this.shape_47}]},1).to({state:[{t:this.shape_32}]},1).wait(17));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0.3,5.5,30.1,37.3);


// stage content:
(lib._1May = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 32 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("AjAMpIg0AEIgjgaIgmgrIhOgiIgqgeIgJgnIhFhnQAEAFgeg1IgegvIhWjgQgFgJgQk0QEf3COYX+QCXKwAAAMQgBANkcBjImXAaIhJAWg");
	mask.setTransform(851.3,157.6);

	// Layer 19
	this.instance = new lib.Symbol2();
	this.instance.parent = this;
	this.instance.setTransform(865.6,189.3,1,1,0,-158.7,21.3,13.3,38.6);

	var maskedShapeInstanceList = [this.instance];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance).to({regX:13.1,skewX:-288,skewY:-108},4).to({regX:13.2,regY:38.8,skewX:-378,skewY:-198,x:845.7,y:186.3},4).to({regX:13.1,regY:38.6,skewX:-288,skewY:-108,x:865.6,y:189.3},4).to({regX:13.3,skewX:-158.7,skewY:21.3},4).wait(1).to({skewY:21.3},0).to({regX:13.1,skewX:-288,skewY:-108},4).to({regX:13.2,regY:38.8,skewX:-378,skewY:-198,x:845.7,y:186.3},4).to({regX:13.1,regY:38.6,skewX:-288,skewY:-108,x:865.6,y:189.3},4).to({regX:13.3,skewX:-158.7,skewY:21.3},4).wait(1).to({skewY:21.3},0).to({regX:13.1,skewX:-288,skewY:-108},4).to({regX:13.2,regY:38.8,skewX:-378,skewY:-198,x:845.7,y:186.3},4).to({regX:13.1,regY:38.6,skewX:-288,skewY:-108,x:865.6,y:189.3},4).to({regX:13.3,skewX:-158.7,skewY:21.3},4).wait(1).to({skewY:21.3},0).to({regX:13.1,skewX:-288,skewY:-108},4).to({regX:13.2,regY:38.8,skewX:-378,skewY:-198,x:845.7,y:186.3},4).to({regX:13.1,regY:38.6,skewX:-288,skewY:-108,x:865.6,y:189.3},4).to({regX:13.3,skewX:-158.7,skewY:21.3},4).wait(1).to({skewY:21.3},0).to({regX:13.1,skewX:-288,skewY:-108},4).to({regX:13.2,regY:38.8,skewX:-378,skewY:-198,x:845.7,y:186.3},4).to({regX:13.1,regY:38.6,skewX:-288,skewY:-108,x:865.6,y:189.3},4).to({regX:13.3,skewX:-158.7,skewY:21.3},4).wait(1));

	// Layer 18
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(12,1,1).p("Al2DnQB6kVBihZQBjhZBRgGQBUgGBzBUQByBUAkCB");
	this.shape.setTransform(828,183.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(12,1,1).p("Al2DIQCEjjBahOQBahOBTgNQBUgNBrBDQBqBDA5Bp");
	this.shape_1.setTransform(828,186.2);
	this.shape_1._off = true;

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(12,1,1).p("Al2CpQCOiwBShEQBThEBTgTQBVgUBjAzQBiAyBNBR");
	this.shape_2.setTransform(828,189.3);
	this.shape_2._off = true;

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(12,1,1).p("Al2CLQCXh+BLg4QBLg5BUgbQBWgaBaAiQBbAhBhA4");
	this.shape_3.setTransform(828,192.3);
	this.shape_3._off = true;

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(12,1,1).p("Al2BwQChhLBEguQBCgvBVghQBXghBSARQBSARB2Af");
	this.shape_4.setTransform(828,195);
	this.shape_4._off = true;

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(12,1,1).p("AlbBdQB+gbBKgUQBJgUBZgdQBcgfBNgNQBQgSBUgb");
	this.shape_5.setTransform(825.3,197);
	this.shape_5._off = true;

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(12,1,1).p("Ak/BVQBaAVBRAHQBOAIBegcQBigcBIgsQBNgzAyhU");
	this.shape_6.setTransform(822.6,197.7);
	this.shape_6._off = true;

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(12,1,1).p("AkkAuQA3BFBYAjQBUAiBigZQBogZBDhLQBKhWAPiN");
	this.shape_7.setTransform(819.8,201.6);
	this.shape_7._off = true;

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(12,1,1).p("AkLADQAUB1BfA+QBZA9BngWQBugWA+hqQBHh4gTjH");
	this.shape_8.setTransform(817.3,205.9);
	this.shape_8._off = true;

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(12,1,1).p("Al/BIQCrgwBKgiQBKgjBZgTQBbgTBOAfQBOAeBwAx");
	this.shape_9.setTransform(827.2,192.8,1,1,-9.4);
	this.shape_9._off = true;

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(12,1,1).p("Al/DDQCmj9BvhIQBvhHBSAHQBUAIBjBlQBjBlAPCG");
	this.shape_10.setTransform(825.2,180.6,1,1,-9.4);
	this.shape_10._off = true;

	var maskedShapeInstanceList = [this.shape,this.shape_1,this.shape_2,this.shape_3,this.shape_4,this.shape_5,this.shape_6,this.shape_7,this.shape_8,this.shape_9,this.shape_10];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.shape).to({_off:true},1).wait(16).to({_off:false},0).to({_off:true},1).wait(16).to({_off:false},0).to({_off:true},1).wait(16).to({_off:false},0).to({_off:true},1).wait(16).to({_off:false},0).to({_off:true},1).wait(16));
	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(1).to({_off:false},0).to({_off:true},1).wait(13).to({_off:false},0).to({_off:true},1).wait(2).to({_off:false},0).to({_off:true},1).wait(13).to({_off:false},0).to({_off:true},1).wait(2).to({_off:false},0).to({_off:true},1).wait(13).to({_off:false},0).to({_off:true},1).wait(2).to({_off:false},0).to({_off:true},1).wait(13).to({_off:false},0).to({_off:true},1).wait(2).to({_off:false},0).to({_off:true},1).wait(13).to({_off:false},0).to({_off:true},1).wait(1));
	this.timeline.addTween(cjs.Tween.get(this.shape_2).wait(2).to({_off:false},0).to({_off:true},1).wait(11).to({_off:false},0).to({_off:true},1).wait(4).to({_off:false},0).to({_off:true},1).wait(11).to({_off:false},0).to({_off:true},1).wait(4).to({_off:false},0).to({_off:true},1).wait(11).to({_off:false},0).to({_off:true},1).wait(4).to({_off:false},0).to({_off:true},1).wait(11).to({_off:false},0).to({_off:true},1).wait(4).to({_off:false},0).to({_off:true},1).wait(11).to({_off:false},0).to({_off:true},1).wait(2));
	this.timeline.addTween(cjs.Tween.get(this.shape_3).wait(3).to({_off:false},0).to({_off:true},1).wait(9).to({_off:false},0).to({_off:true},1).wait(6).to({_off:false},0).to({_off:true},1).wait(9).to({_off:false},0).to({_off:true},1).wait(6).to({_off:false},0).to({_off:true},1).wait(9).to({_off:false},0).to({_off:true},1).wait(6).to({_off:false},0).to({_off:true},1).wait(9).to({_off:false},0).to({_off:true},1).wait(6).to({_off:false},0).to({_off:true},1).wait(9).to({_off:false},0).to({_off:true},1).wait(3));
	this.timeline.addTween(cjs.Tween.get(this.shape_4).wait(4).to({_off:false},0).to({_off:true},1).wait(16).to({_off:false},0).to({_off:true},1).wait(16).to({_off:false},0).to({_off:true},1).wait(16).to({_off:false},0).to({_off:true},1).wait(16).to({_off:false},0).to({_off:true},1).wait(12));
	this.timeline.addTween(cjs.Tween.get(this.shape_5).wait(5).to({_off:false},0).to({_off:true},1).wait(5).to({_off:false},0).to({_off:true},1).wait(10).to({_off:false},0).to({_off:true},1).wait(5).to({_off:false},0).to({_off:true},1).wait(10).to({_off:false},0).to({_off:true},1).wait(5).to({_off:false},0).to({_off:true},1).wait(10).to({_off:false},0).to({_off:true},1).wait(5).to({_off:false},0).to({_off:true},1).wait(10).to({_off:false},0).to({_off:true},1).wait(5).to({_off:false},0).to({_off:true},1).wait(5));
	this.timeline.addTween(cjs.Tween.get(this.shape_6).wait(6).to({_off:false},0).to({_off:true},1).wait(3).to({_off:false},0).to({_off:true},1).wait(12).to({_off:false},0).to({_off:true},1).wait(3).to({_off:false},0).to({_off:true},1).wait(12).to({_off:false},0).to({_off:true},1).wait(3).to({_off:false},0).to({_off:true},1).wait(12).to({_off:false},0).to({_off:true},1).wait(3).to({_off:false},0).to({_off:true},1).wait(12).to({_off:false},0).to({_off:true},1).wait(3).to({_off:false},0).to({_off:true},1).wait(6));
	this.timeline.addTween(cjs.Tween.get(this.shape_7).wait(7).to({_off:false},0).to({_off:true},1).wait(1).to({_off:false},0).to({_off:true},1).wait(14).to({_off:false},0).to({_off:true},1).wait(1).to({_off:false},0).to({_off:true},1).wait(14).to({_off:false},0).to({_off:true},1).wait(1).to({_off:false},0).to({_off:true},1).wait(14).to({_off:false},0).to({_off:true},1).wait(1).to({_off:false},0).to({_off:true},1).wait(14).to({_off:false},0).to({_off:true},1).wait(1).to({_off:false},0).to({_off:true},1).wait(7));
	this.timeline.addTween(cjs.Tween.get(this.shape_8).wait(8).to({_off:false},0).to({_off:true},1).wait(16).to({_off:false},0).to({_off:true},1).wait(16).to({_off:false},0).to({_off:true},1).wait(16).to({_off:false},0).to({_off:true},1).wait(16).to({_off:false},0).to({_off:true},1).wait(8));
	this.timeline.addTween(cjs.Tween.get(this.shape_9).wait(12).to({_off:false},0).to({_off:true},1).wait(16).to({_off:false},0).to({_off:true},1).wait(16).to({_off:false},0).to({_off:true},1).wait(16).to({_off:false},0).to({_off:true},1).wait(16).to({_off:false},0).to({_off:true},1).wait(4));
	this.timeline.addTween(cjs.Tween.get(this.shape_10).wait(16).to({_off:false},0).to({_off:true},1).wait(16).to({_off:false},0).to({_off:true},1).wait(16).to({_off:false},0).to({_off:true},1).wait(16).to({_off:false},0).to({_off:true},1).wait(16).to({_off:false},0).wait(1));

	// Layer 29
	this.instance_1 = new lib.Symbol2();
	this.instance_1.parent = this;
	this.instance_1.setTransform(552.9,299.1,1,1,0,0,0,13.3,38.6);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).to({rotation:-90,x:534.8,y:308.1},4).to({rotation:-219.2,x:534.9,y:308.2},4).to({rotation:-90,x:534.8,y:308.1},4).to({rotation:0,x:552.9,y:299.1},4).wait(1).to({rotation:-90,x:534.8,y:308.1},4).to({rotation:-219.2,x:534.9,y:308.2},4).to({rotation:-90,x:534.8,y:308.1},4).to({rotation:0,x:552.9,y:299.1},4).wait(1).to({rotation:-90,x:534.8,y:308.1},4).to({rotation:-219.2,x:534.9,y:308.2},4).to({rotation:-90,x:534.8,y:308.1},4).to({rotation:0,x:552.9,y:299.1},4).wait(1).to({rotation:-90,x:534.8,y:308.1},4).to({rotation:-219.2,x:534.9,y:308.2},4).to({rotation:-90,x:534.8,y:308.1},4).to({rotation:0,x:552.9,y:299.1},4).wait(1).to({rotation:-90,x:534.8,y:308.1},4).to({rotation:-219.2,x:534.9,y:308.2},4).to({rotation:-90,x:534.8,y:308.1},4).to({rotation:0,x:552.9,y:299.1},4).wait(1));

	// Layer 23
	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(12,1,1).p("AEeh8QASB2hHBXQhCBVhpAMQhvAMhchRQhphegrjC");
	this.shape_11.setTransform(582.8,313.7);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(12,1,1).p("Ak3h0QA6CBBhA8QBXAyBqgIQBmgHBFg6QBJg7AfhT");
	this.shape_12.setTransform(580.2,310.5);
	this.shape_12._off = true;

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(12,1,1).p("AlOgwQBIA/BZAaQBSAUBmgEQBigEBJgeQBKgfBPgw");
	this.shape_13.setTransform(577.9,306.9);
	this.shape_13._off = true;

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(12,1,1).p("AlmATQBYgBBRgIQBNgKBhAAQBfABBLgDQBMgECAgN");
	this.shape_14.setTransform(575.5,303.1);
	this.shape_14._off = true;

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(12,1,1).p("AF/ADQiwgWhPgXQhOgYhcgFQhcgFhJAqQhIAohnBD");
	this.shape_15.setTransform(573.1,300.9);
	this.shape_15._off = true;

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(12,1,1).p("Al+BnQBMhUBLg7QBLg8BagBQBYgBBZAfQBZAfC3BJ");
	this.shape_16.setTransform(573.1,297.9);
	this.shape_16._off = true;

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(12,1,1).p("Al+CFQAxhlBOhNQBNhPBXgHQBXgHBjAnQBjAnC9B7");
	this.shape_17.setTransform(573.1,294.8);
	this.shape_17._off = true;

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#000000").ss(12,1,1).p("Al+CkQAVh2BRhgQBQhhBVgNQBTgNBuAuQBuAuDDCv");
	this.shape_18.setTransform(573.1,291.7);
	this.shape_18._off = true;

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#000000").ss(12,1,1).p("AF/B9QjJjih5g2Qh4g2hRAUQhSAThTB0QhTByAGCH");
	this.shape_19.setTransform(573.1,288.6);
	this.shape_19._off = true;

	this.timeline.addTween(cjs.Tween.get(this.shape_11).to({_off:true},1).wait(15).to({_off:false},0).wait(1).to({_off:true},1).wait(15).to({_off:false},0).wait(1).to({_off:true},1).wait(15).to({_off:false},0).wait(1).to({_off:true},1).wait(15).to({_off:false},0).wait(1).to({_off:true},1).wait(15).to({_off:false},0).wait(1));
	this.timeline.addTween(cjs.Tween.get(this.shape_12).wait(1).to({_off:false},0).to({_off:true},1).wait(13).to({_off:false},0).to({_off:true},1).wait(2).to({_off:false},0).to({_off:true},1).wait(13).to({_off:false},0).to({_off:true},1).wait(2).to({_off:false},0).to({_off:true},1).wait(13).to({_off:false},0).to({_off:true},1).wait(2).to({_off:false},0).to({_off:true},1).wait(13).to({_off:false},0).to({_off:true},1).wait(2).to({_off:false},0).to({_off:true},1).wait(13).to({_off:false},0).to({_off:true},1).wait(1));
	this.timeline.addTween(cjs.Tween.get(this.shape_13).wait(2).to({_off:false},0).to({_off:true},1).wait(11).to({_off:false},0).to({_off:true},1).wait(4).to({_off:false},0).to({_off:true},1).wait(11).to({_off:false},0).to({_off:true},1).wait(4).to({_off:false},0).to({_off:true},1).wait(11).to({_off:false},0).to({_off:true},1).wait(4).to({_off:false},0).to({_off:true},1).wait(11).to({_off:false},0).to({_off:true},1).wait(4).to({_off:false},0).to({_off:true},1).wait(11).to({_off:false},0).to({_off:true},1).wait(2));
	this.timeline.addTween(cjs.Tween.get(this.shape_14).wait(3).to({_off:false},0).to({_off:true},1).wait(9).to({_off:false},0).to({_off:true},1).wait(6).to({_off:false},0).to({_off:true},1).wait(9).to({_off:false},0).to({_off:true},1).wait(6).to({_off:false},0).to({_off:true},1).wait(9).to({_off:false},0).to({_off:true},1).wait(6).to({_off:false},0).to({_off:true},1).wait(9).to({_off:false},0).to({_off:true},1).wait(6).to({_off:false},0).to({_off:true},1).wait(9).to({_off:false},0).to({_off:true},1).wait(3));
	this.timeline.addTween(cjs.Tween.get(this.shape_15).wait(4).to({_off:false},0).to({_off:true},1).wait(7).to({_off:false},0).to({_off:true},1).wait(8).to({_off:false},0).to({_off:true},1).wait(7).to({_off:false},0).to({_off:true},1).wait(8).to({_off:false},0).to({_off:true},1).wait(7).to({_off:false},0).to({_off:true},1).wait(8).to({_off:false},0).to({_off:true},1).wait(7).to({_off:false},0).to({_off:true},1).wait(8).to({_off:false},0).to({_off:true},1).wait(7).to({_off:false},0).to({_off:true},1).wait(4));
	this.timeline.addTween(cjs.Tween.get(this.shape_16).wait(5).to({_off:false},0).to({_off:true},1).wait(5).to({_off:false},0).to({_off:true},1).wait(10).to({_off:false},0).to({_off:true},1).wait(5).to({_off:false},0).to({_off:true},1).wait(10).to({_off:false},0).to({_off:true},1).wait(5).to({_off:false},0).to({_off:true},1).wait(10).to({_off:false},0).to({_off:true},1).wait(5).to({_off:false},0).to({_off:true},1).wait(10).to({_off:false},0).to({_off:true},1).wait(5).to({_off:false},0).to({_off:true},1).wait(5));
	this.timeline.addTween(cjs.Tween.get(this.shape_17).wait(6).to({_off:false},0).to({_off:true},1).wait(3).to({_off:false},0).to({_off:true},1).wait(12).to({_off:false},0).to({_off:true},1).wait(3).to({_off:false},0).to({_off:true},1).wait(12).to({_off:false},0).to({_off:true},1).wait(3).to({_off:false},0).to({_off:true},1).wait(12).to({_off:false},0).to({_off:true},1).wait(3).to({_off:false},0).to({_off:true},1).wait(12).to({_off:false},0).to({_off:true},1).wait(3).to({_off:false},0).to({_off:true},1).wait(6));
	this.timeline.addTween(cjs.Tween.get(this.shape_18).wait(7).to({_off:false},0).to({_off:true},1).wait(1).to({_off:false},0).to({_off:true},1).wait(14).to({_off:false},0).to({_off:true},1).wait(1).to({_off:false},0).to({_off:true},1).wait(14).to({_off:false},0).to({_off:true},1).wait(1).to({_off:false},0).to({_off:true},1).wait(14).to({_off:false},0).to({_off:true},1).wait(1).to({_off:false},0).to({_off:true},1).wait(14).to({_off:false},0).to({_off:true},1).wait(1).to({_off:false},0).to({_off:true},1).wait(7));
	this.timeline.addTween(cjs.Tween.get(this.shape_19).wait(8).to({_off:false},0).to({_off:true},1).wait(16).to({_off:false},0).to({_off:true},1).wait(16).to({_off:false},0).to({_off:true},1).wait(16).to({_off:false},0).to({_off:true},1).wait(16).to({_off:false},0).to({_off:true},1).wait(8));

	// Layer 26
	this.instance_2 = new lib.Symbol1();
	this.instance_2.parent = this;
	this.instance_2.setTransform(990.4,192.4,1,1,-178.3,0,0,9.3,36.9);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(15).to({rotation:0,x:1006.5,y:104.5},9).wait(32).to({rotation:-178.3,x:990.4,y:192.4},9).wait(20));

	// Layer 15
	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#000000").ss(12,1,1).p("AEZjvQmTBdieGD");
	this.shape_20.setTransform(1018.9,165.7);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#000000").ss(12,1,1).p("AkOC/QCHkkGXhZ");
	this.shape_21.setTransform(1019.8,160.8);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#000000").ss(12,1,1).p("AkGCOQBxjHGchU");
	this.shape_22.setTransform(1020.6,155.9);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#000000").ss(12,1,1).p("Aj9BdQBahqGhhP");
	this.shape_23.setTransform(1021.5,150.9);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#000000").ss(12,1,1).p("Aj0ArQBDgMGmhJ");
	this.shape_24.setTransform(1022.4,146);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f().s("#000000").ss(12,1,1).p("AjrgUQArBOGshD");
	this.shape_25.setTransform(1023.3,142.7);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f().s("#000000").ss(12,1,1).p("Ajig+QAVCsGwhA");
	this.shape_26.setTransform(1024.2,137);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f().s("#000000").ss(12,1,1).p("AjZhsQgCEJG1g6");
	this.shape_27.setTransform(1025.1,131.8);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f().s("#000000").ss(12,1,1).p("AjQicQgYFnG6g1");
	this.shape_28.setTransform(1025.9,126.7);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f().s("#000000").ss(12,1,1).p("ADLDIQnAAxAwnF");
	this.shape_29.setTransform(1026.6,121.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_20}]}).to({state:[{t:this.shape_20}]},15).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_27}]},1).to({state:[{t:this.shape_28}]},1).to({state:[{t:this.shape_29}]},1).to({state:[{t:this.shape_29}]},32).to({state:[{t:this.shape_28}]},1).to({state:[{t:this.shape_27}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_20}]},1).wait(20));

	// Layer 14
	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f().s("#000000").ss(12,1,1).p("AhahoQCIArAtCm");
	this.shape_30.setTransform(685.9,557.3);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f().s("#000000").ss(12,1,1).p("AhahoQDAAegMCz");
	this.shape_31.setTransform(685.9,557.3);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f().s("#000000").ss(12,1,1).p("AhfhoQDsAUg4C9");
	this.shape_32.setTransform(686.4,557.3);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f().s("#000000").ss(12,1,1).p("AhkhoQEMAOhYDD");
	this.shape_33.setTransform(686.9,557.3);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f().s("#000000").ss(12,1,1).p("AhohoQEeAJhqDI");
	this.shape_34.setTransform(687.3,557.3);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f().s("#000000").ss(12,1,1).p("AhphoQElAIhxDJ");
	this.shape_35.setTransform(687.4,557.3);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f().s("#000000").ss(12,1,1).p("AhjhoQEFAPhRDC");
	this.shape_36.setTransform(686.8,557.3);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f().s("#000000").ss(12,1,1).p("AhehoQDmAWgyC7");
	this.shape_37.setTransform(686.3,557.3);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f().s("#000000").ss(12,1,1).p("AhahoQDGAdgSC0");
	this.shape_38.setTransform(685.9,557.3);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f().s("#000000").ss(12,1,1).p("AhahoQCnAkAOCt");
	this.shape_39.setTransform(685.9,557.3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_30}]}).to({state:[{t:this.shape_30}]},1).to({state:[{t:this.shape_31}]},1).to({state:[{t:this.shape_32}]},1).to({state:[{t:this.shape_33}]},1).to({state:[{t:this.shape_34}]},1).to({state:[{t:this.shape_35}]},1).to({state:[{t:this.shape_35}]},73).to({state:[{t:this.shape_36}]},1).to({state:[{t:this.shape_37}]},1).to({state:[{t:this.shape_38}]},1).to({state:[{t:this.shape_39}]},1).to({state:[{t:this.shape_30}]},1).wait(1));

	// Layer 13
	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f().s("#000000").ss(12,1,1).p("AhrhYQA3AHAtAXQAtAWAfApQAfApAIAs");
	this.shape_40.setTransform(643.3,506.2);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f().s("#000000").ss(12,1,1).p("AhrhYQBigLAzAlQAnAYASApQAQArgLAu");
	this.shape_41.setTransform(643.3,506.1);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f().s("#000000").ss(12,1,1).p("AhyhWQCDgaA5AxQAiAZAGArQAGArgaAx");
	this.shape_42.setTransform(644,505.9);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f().s("#000000").ss(12,1,1).p("Ah6hVQCbgkA8A5QAfAbgBAqQgCAtglAy");
	this.shape_43.setTransform(644.8,505.8);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f().s("#000000").ss(12,1,1).p("Ah/hTQCpgrA/A+QAcAbgGArQgGAtgsAz");
	this.shape_44.setTransform(645.3,505.6);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f().s("#000000").ss(12,1,1).p("AiBhTQCugtA/BAQAcAcgHArQgJAtgtAz");
	this.shape_45.setTransform(645.5,505.6);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.f().s("#000000").ss(12,1,1).p("Ah4hVQCWgiA8A3QAfAbAAAqQAAAtgjAx");
	this.shape_46.setTransform(644.6,505.8);

	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.f().s("#000000").ss(12,1,1).p("AhxhXQB+gYA4AwQAjAZAIAqQAHAsgYAw");
	this.shape_47.setTransform(643.9,506);

	this.shape_48 = new cjs.Shape();
	this.shape_48.graphics.f().s("#000000").ss(12,1,1).p("AhshYQBngNA0AmQAmAZAQApQAPArgNAv");
	this.shape_48.setTransform(643.4,506.1);

	this.shape_49 = new cjs.Shape();
	this.shape_49.graphics.f().s("#000000").ss(12,1,1).p("AhqhZQBPgDAwAfQAqAXAYApQAXAqgDAt");
	this.shape_49.setTransform(643.2,506.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_40}]}).to({state:[{t:this.shape_40}]},1).to({state:[{t:this.shape_41}]},1).to({state:[{t:this.shape_42}]},1).to({state:[{t:this.shape_43}]},1).to({state:[{t:this.shape_44}]},1).to({state:[{t:this.shape_45}]},1).to({state:[{t:this.shape_45}]},73).to({state:[{t:this.shape_46}]},1).to({state:[{t:this.shape_47}]},1).to({state:[{t:this.shape_48}]},1).to({state:[{t:this.shape_49}]},1).to({state:[{t:this.shape_40}]},1).wait(1));

	// Layer 12
	this.shape_50 = new cjs.Shape();
	this.shape_50.graphics.f().s("#000000").ss(12,1,1).p("AB+DHQC7BIABC4Ak5nFQCCCXAHCi");
	this.shape_50.setTransform(696.4,512.3);

	this.shape_51 = new cjs.Shape();
	this.shape_51.graphics.f().s("#000000").ss(12,1,1).p("ABfDWQCVBcA4CWAkrnHQBECnApCW");
	this.shape_51.setTransform(697.8,512.1);

	this.shape_52 = new cjs.Shape();
	this.shape_52.graphics.f().s("#000000").ss(12,1,1).p("ABHDiQB3BrBiB9AkgnJQATC0BDCN");
	this.shape_52.setTransform(699,511.9);

	this.shape_53 = new cjs.Shape();
	this.shape_53.graphics.f().s("#000000").ss(12,1,1).p("AA3DqQBiB3CBBqAkWnKQgQC8BWCH");
	this.shape_53.setTransform(699.6,511.8);

	this.shape_54 = new cjs.Shape();
	this.shape_54.graphics.f().s("#000000").ss(12,1,1).p("AAxDvQBUB+CUBeAkNnKQglDBBhCC");
	this.shape_54.setTransform(699.7,511.8);

	this.shape_55 = new cjs.Shape();
	this.shape_55.graphics.f().s("#000000").ss(12,1,1).p("AAvDwQBQCACaBcAkKnKQgsDDBlCA");
	this.shape_55.setTransform(699.7,511.8);

	this.shape_56 = new cjs.Shape();
	this.shape_56.graphics.f().s("#000000").ss(12,1,1).p("AA6DoQBlB1B7BuAkZnKQgJC7BSCH");
	this.shape_56.setTransform(699.6,511.9);

	this.shape_57 = new cjs.Shape();
	this.shape_57.graphics.f().s("#000000").ss(12,1,1).p("ABLDgQB7BpBcCBAkhnJQAaCyA/CO");
	this.shape_57.setTransform(698.8,512);

	this.shape_58 = new cjs.Shape();
	this.shape_58.graphics.f().s("#000000").ss(12,1,1).p("ABcDYQCQBeA+CTAkpnHQA9CpAsCV");
	this.shape_58.setTransform(698,512.1);

	this.shape_59 = new cjs.Shape();
	this.shape_59.graphics.f().s("#000000").ss(12,1,1).p("ABtDPQCmBTAfClAkxnHQBfCgAaCc");
	this.shape_59.setTransform(697.2,512.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_50}]}).to({state:[{t:this.shape_50}]},1).to({state:[{t:this.shape_51}]},1).to({state:[{t:this.shape_52}]},1).to({state:[{t:this.shape_53}]},1).to({state:[{t:this.shape_54}]},1).to({state:[{t:this.shape_55}]},1).to({state:[{t:this.shape_55}]},73).to({state:[{t:this.shape_56}]},1).to({state:[{t:this.shape_57}]},1).to({state:[{t:this.shape_58}]},1).to({state:[{t:this.shape_59}]},1).to({state:[{t:this.shape_50}]},1).wait(1));

	// Layer 9
	this.shape_60 = new cjs.Shape();
	this.shape_60.graphics.f().s("#000000").ss(12,1,1).p("AhRmDQgaGFDAGC");
	this.shape_60.setTransform(698.6,659.5);

	this.shape_61 = new cjs.Shape();
	this.shape_61.graphics.f().s("#000000").ss(12,1,1).p("AiFl7QAOF4D9F/");
	this.shape_61.setTransform(703.8,658.8);

	this.shape_62 = new cjs.Shape();
	this.shape_62.graphics.f().s("#000000").ss(12,1,1).p("Aisl2QAsFuEtF/");
	this.shape_62.setTransform(707.7,658.2);

	this.shape_63 = new cjs.Shape();
	this.shape_63.graphics.f().s("#000000").ss(12,1,1).p("AjIlyQBCFoFPF9");
	this.shape_63.setTransform(710.5,657.8);

	this.shape_64 = new cjs.Shape();
	this.shape_64.graphics.f().s("#000000").ss(12,1,1).p("AjZlvQBPFjFkF9");
	this.shape_64.setTransform(712.2,657.6);

	this.shape_65 = new cjs.Shape();
	this.shape_65.graphics.f().s("#000000").ss(12,1,1).p("AjelvQBTFiFqF9");
	this.shape_65.setTransform(712.8,657.5);

	this.shape_66 = new cjs.Shape();
	this.shape_66.graphics.f().s("#000000").ss(12,1,1).p("AhpmAQgIGADbGA");
	this.shape_66.setTransform(701,659.2);

	this.shape_67 = new cjs.Shape();
	this.shape_67.graphics.f().s("#000000").ss(12,1,1).p("AhXmCQgVGDDGGC");
	this.shape_67.setTransform(699.2,659.4);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_60}]}).to({state:[{t:this.shape_60}]},6).to({state:[{t:this.shape_61}]},1).to({state:[{t:this.shape_62}]},1).to({state:[{t:this.shape_63}]},1).to({state:[{t:this.shape_64}]},1).to({state:[{t:this.shape_65}]},1).to({state:[{t:this.shape_65}]},24).to({state:[{t:this.shape_62}]},1).to({state:[{t:this.shape_61}]},1).to({state:[{t:this.shape_66}]},1).to({state:[{t:this.shape_67}]},1).to({state:[{t:this.shape_60}]},1).to({state:[{t:this.shape_60}]},11).to({state:[{t:this.shape_61}]},1).to({state:[{t:this.shape_62}]},1).to({state:[{t:this.shape_63}]},1).to({state:[{t:this.shape_64}]},1).to({state:[{t:this.shape_65}]},1).to({state:[{t:this.shape_65}]},20).to({state:[{t:this.shape_62}]},1).to({state:[{t:this.shape_61}]},1).to({state:[{t:this.shape_66}]},1).to({state:[{t:this.shape_67}]},1).to({state:[{t:this.shape_60}]},1).wait(4));
	this.timeline.addTween(cjs.Tween.get(this.shape_60).wait(6).to({_off:true},1).wait(33).to({_off:false},0).wait(11).to({_off:true},1).wait(29).to({_off:false},0).wait(4));

	// Layer 8
	this.shape_68 = new cjs.Shape();
	this.shape_68.graphics.f().s("#000000").ss(12,1,1).p("AjKBAQCfimD2Ay");
	this.shape_68.setTransform(877.5,477.1);

	this.shape_69 = new cjs.Shape();
	this.shape_69.graphics.f().s("#000000").ss(12,1,1).p("AjKBKQCPjWEGBi");
	this.shape_69.setTransform(877.5,476.1);

	this.shape_70 = new cjs.Shape();
	this.shape_70.graphics.f().s("#000000").ss(12,1,1).p("AjKBRQCEj2ERCC");
	this.shape_70.setTransform(877.5,475.4);

	this.shape_71 = new cjs.Shape();
	this.shape_71.graphics.f().s("#000000").ss(12,1,1).p("AjKBWQB9kLEYCX");
	this.shape_71.setTransform(877.5,474.9);

	this.shape_72 = new cjs.Shape();
	this.shape_72.graphics.f().s("#000000").ss(12,1,1).p("AjKBXQB7kREaCd");
	this.shape_72.setTransform(877.5,474.8);

	this.shape_73 = new cjs.Shape();
	this.shape_73.graphics.f().s("#000000").ss(12,1,1).p("AjKBNQCLjjEKBv");
	this.shape_73.setTransform(877.5,475.8);

	this.shape_74 = new cjs.Shape();
	this.shape_74.graphics.f().s("#000000").ss(12,1,1).p("AjKBFQCWjBD/BN");
	this.shape_74.setTransform(877.5,476.6);

	this.shape_75 = new cjs.Shape();
	this.shape_75.graphics.f().s("#000000").ss(12,1,1).p("AjKBCQCditD4A5");
	this.shape_75.setTransform(877.5,476.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_68}]}).to({state:[{t:this.shape_68}]},4).to({state:[{t:this.shape_69}]},1).to({state:[{t:this.shape_70}]},1).to({state:[{t:this.shape_71}]},1).to({state:[{t:this.shape_72}]},1).to({state:[{t:this.shape_72}]},28).to({state:[{t:this.shape_73}]},1).to({state:[{t:this.shape_74}]},1).to({state:[{t:this.shape_75}]},1).to({state:[{t:this.shape_68}]},1).to({state:[{t:this.shape_68}]},3).to({state:[{t:this.shape_69}]},1).to({state:[{t:this.shape_70}]},1).to({state:[{t:this.shape_71}]},1).to({state:[{t:this.shape_72}]},1).to({state:[{t:this.shape_72}]},30).to({state:[{t:this.shape_73}]},1).to({state:[{t:this.shape_74}]},1).to({state:[{t:this.shape_75}]},1).to({state:[{t:this.shape_68}]},1).wait(4));
	this.timeline.addTween(cjs.Tween.get(this.shape_68).wait(4).to({_off:true},1).wait(35).to({_off:false},0).wait(3).to({_off:true},1).wait(37).to({_off:false},0).wait(4));

	// Layer 7
	this.shape_76 = new cjs.Shape();
	this.shape_76.graphics.f().s("#000000").ss(12,1,1).p("AAAh/IiXD/IEvgn");
	this.shape_76.setTransform(859.9,424.9);

	this.shape_77 = new cjs.Shape();
	this.shape_77.graphics.f().s("#000000").ss(12,1,1).p("AgIiAIiGEBIEdhQ");
	this.shape_77.setTransform(859,424.8);

	this.shape_78 = new cjs.Shape();
	this.shape_78.graphics.f().s("#000000").ss(12,1,1).p("AgOiBIh6EEIERhv");
	this.shape_78.setTransform(858.4,424.7);

	this.shape_79 = new cjs.Shape();
	this.shape_79.graphics.f().s("#000000").ss(12,1,1).p("AgTiCIhxEFIEJiA");
	this.shape_79.setTransform(858,424.6);

	this.shape_80 = new cjs.Shape();
	this.shape_80.graphics.f().s("#000000").ss(12,1,1).p("AgUiCIhvEGIEHiG");
	this.shape_80.setTransform(857.8,424.6);

	this.shape_81 = new cjs.Shape();
	this.shape_81.graphics.f().s("#000000").ss(12,1,1).p("AgLiBIiBEDIEZhc");
	this.shape_81.setTransform(858.7,424.7);

	this.shape_82 = new cjs.Shape();
	this.shape_82.graphics.f().s("#000000").ss(12,1,1).p("AgEiAIiOEBIElg/");
	this.shape_82.setTransform(859.4,424.8);

	this.shape_83 = new cjs.Shape();
	this.shape_83.graphics.f().s("#000000").ss(12,1,1).p("AAAh/IiWD/IEtgt");
	this.shape_83.setTransform(859.8,424.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_76}]}).to({state:[{t:this.shape_76}]},4).to({state:[{t:this.shape_77}]},1).to({state:[{t:this.shape_78}]},1).to({state:[{t:this.shape_79}]},1).to({state:[{t:this.shape_80}]},1).to({state:[{t:this.shape_80}]},28).to({state:[{t:this.shape_81}]},1).to({state:[{t:this.shape_82}]},1).to({state:[{t:this.shape_83}]},1).to({state:[{t:this.shape_76}]},1).to({state:[{t:this.shape_76}]},3).to({state:[{t:this.shape_77}]},1).to({state:[{t:this.shape_78}]},1).to({state:[{t:this.shape_79}]},1).to({state:[{t:this.shape_80}]},1).to({state:[{t:this.shape_80}]},30).to({state:[{t:this.shape_81}]},1).to({state:[{t:this.shape_82}]},1).to({state:[{t:this.shape_83}]},1).to({state:[{t:this.shape_76}]},1).wait(4));
	this.timeline.addTween(cjs.Tween.get(this.shape_76).wait(4).to({_off:true},1).wait(35).to({_off:false},0).wait(3).to({_off:true},1).wait(37).to({_off:false},0).wait(4));

	// Layer 6
	this.shape_84 = new cjs.Shape();
	this.shape_84.graphics.f().s("#000000").ss(12,1,1).p("AiZh4IEzAQIjIDi");
	this.shape_84.setTransform(798.8,465.4);

	this.shape_85 = new cjs.Shape();
	this.shape_85.graphics.f().s("#000000").ss(12,1,1).p("AiQhnIEhAfIjjCw");
	this.shape_85.setTransform(799.7,462.1);

	this.shape_86 = new cjs.Shape();
	this.shape_86.graphics.f().s("#000000").ss(12,1,1).p("AiKhaIEVAqIj3CL");
	this.shape_86.setTransform(800.4,459.8);

	this.shape_87 = new cjs.Shape();
	this.shape_87.graphics.f().s("#000000").ss(12,1,1).p("AiGhTIENAxIkCB1");
	this.shape_87.setTransform(800.7,458.4);

	this.shape_88 = new cjs.Shape();
	this.shape_88.graphics.f().s("#000000").ss(12,1,1).p("AiFhQIELAzIkGBu");
	this.shape_88.setTransform(800.9,457.9);

	this.shape_89 = new cjs.Shape();
	this.shape_89.graphics.f().s("#000000").ss(12,1,1).p("AiOhiIEdAkIjrCh");
	this.shape_89.setTransform(800,461.2);

	this.shape_90 = new cjs.Shape();
	this.shape_90.graphics.f().s("#000000").ss(12,1,1).p("AiUhuIEpAYIjYDF");
	this.shape_90.setTransform(799.3,463.5);

	this.shape_91 = new cjs.Shape();
	this.shape_91.graphics.f().s("#000000").ss(12,1,1).p("AiYh2IExASIjMDb");
	this.shape_91.setTransform(798.9,465);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_84}]}).to({state:[{t:this.shape_84}]},4).to({state:[{t:this.shape_85}]},1).to({state:[{t:this.shape_86}]},1).to({state:[{t:this.shape_87}]},1).to({state:[{t:this.shape_88}]},1).to({state:[{t:this.shape_88}]},28).to({state:[{t:this.shape_89}]},1).to({state:[{t:this.shape_90}]},1).to({state:[{t:this.shape_91}]},1).to({state:[{t:this.shape_84}]},1).to({state:[{t:this.shape_84}]},3).to({state:[{t:this.shape_85}]},1).to({state:[{t:this.shape_86}]},1).to({state:[{t:this.shape_87}]},1).to({state:[{t:this.shape_88}]},1).to({state:[{t:this.shape_88}]},30).to({state:[{t:this.shape_89}]},1).to({state:[{t:this.shape_90}]},1).to({state:[{t:this.shape_91}]},1).to({state:[{t:this.shape_84}]},1).wait(4));
	this.timeline.addTween(cjs.Tween.get(this.shape_84).wait(4).to({_off:true},1).wait(35).to({_off:false},0).wait(3).to({_off:true},1).wait(37).to({_off:false},0).wait(4));

	// Layer 5
	this.shape_92 = new cjs.Shape();
	this.shape_92.graphics.f().s("#000000").ss(12,1,1).p("AgpjFQBsgwCLCIQCKCGhfBCQhgBBg5ATQg5AThFAMQhFAMhNgTQhZgVgWg+QAoinDOiSg");
	this.shape_92.setTransform(589.5,564.5);

	this.shape_93 = new cjs.Shape();
	this.shape_93.graphics.f().s("#000000").ss(12,1,1).p("Aj+BNQASiICoh0QBggjCLCJQCLCJhQA1QhRA1gvAIQgvAHg/gDQhAgDhGgVQhPgWgdg7g");
	this.shape_93.setTransform(593.4,561.1);
	this.shape_93._off = true;

	this.shape_94 = new cjs.Shape();
	this.shape_94.graphics.f().s("#000000").ss(12,1,1).p("AjeAbQgEhpCChXQBWgXCKCMQCMCLhCAoQhCApglgEQglgDg6gSQg5gThAgXQhGgYgjg2g");
	this.shape_94.setTransform(597.3,558.8);
	this.shape_94._off = true;

	this.shape_95 = new cjs.Shape();
	this.shape_95.graphics.f().s("#000000").ss(12,1,1).p("Ai7gaQgbhMBcg4QBLgLCLCOQCMCNgzAcQg0AcgagOQgbgPg0giQg0ghg5gZQg8gagqgxg");
	this.shape_95.setTransform(601,556.9);
	this.shape_95._off = true;

	this.shape_96 = new cjs.Shape();
	this.shape_96.graphics.f().s("#000000").ss(12,1,1).p("AiOibQA/ACCLCQQCNCPglAQQgkAQgRgaQgPgagvgxQgvgwgygbQgzgcgxgtQgxgsA3gcg");
	this.shape_96.setTransform(604.2,555.3);
	this.shape_96._off = true;

	this.shape_97 = new cjs.Shape();
	this.shape_97.graphics.f().s("#000000").ss(12,1,1).p("Ai0glQgfhGBVgzQBIgICLCPQCMCMgwAaQgxAagYgRQgYgRgzgkQgzglg4gZQg6gbgsgvg");
	this.shape_97.setTransform(601.7,556.6);

	this.shape_98 = new cjs.Shape();
	this.shape_98.graphics.f().s("#000000").ss(12,1,1).p("AjRAGQgNhdBzhLQBRgSCLCNQCMCLg8AkQg9AjgggHQghgIg4gZQg3gYg9gYQhCgZgmg0g");
	this.shape_98.setTransform(598.9,558);

	this.shape_99 = new cjs.Shape();
	this.shape_99.graphics.f().s("#000000").ss(12,1,1).p("AjrAxQAFh2CRhjQBagbCLCLQCLCJhIAuQhIAtgoABQgpABg7gMQg9gMhDgWQhJgYghg3g");
	this.shape_99.setTransform(595.7,559.6);

	this.shape_100 = new cjs.Shape();
	this.shape_100.graphics.f().s("#000000").ss(12,1,1).p("AkFBWQAWiOCwh7QBjglCLCJQCLCIhUA4QhUA3gxAKQgxAKhAAAQhBAAhIgVQhRgWgbg7g");
	this.shape_100.setTransform(592.6,561.7);

	this.shape_101 = new cjs.Shape();
	this.shape_101.graphics.f().s("#000000").ss(12,1,1).p("AjzA+QAKh+CchqQBcgfCLCKQCMCJhMAxQhMAxgsAEQgrAEg9gIQg+gIhEgWQhMgXgfg5g");
	this.shape_101.setTransform(594.7,560.2);

	this.shape_102 = new cjs.Shape();
	this.shape_102.graphics.f().s("#000000").ss(12,1,1).p("AjHgIQgUhWBphCQBPgPCLCNQCLCMg4AhQg4AggegLQgegLg2gcQg1gdg8gYQhAgagngyg");
	this.shape_102.setTransform(599.8,557.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_92}]}).to({state:[{t:this.shape_92}]},6).to({state:[{t:this.shape_93}]},1).to({state:[{t:this.shape_94}]},1).to({state:[{t:this.shape_95}]},1).to({state:[{t:this.shape_96}]},1).to({state:[{t:this.shape_97}]},1).to({state:[{t:this.shape_98}]},1).to({state:[{t:this.shape_99}]},1).to({state:[{t:this.shape_100}]},1).to({state:[{t:this.shape_92}]},1).to({state:[{t:this.shape_92}]},1).to({state:[{t:this.shape_101}]},1).to({state:[{t:this.shape_102}]},1).to({state:[{t:this.shape_96}]},1).to({state:[{t:this.shape_95}]},1).to({state:[{t:this.shape_94}]},1).to({state:[{t:this.shape_93}]},1).to({state:[{t:this.shape_92}]},1).to({state:[{t:this.shape_92}]},1).to({state:[{t:this.shape_93}]},1).to({state:[{t:this.shape_94}]},1).to({state:[{t:this.shape_95}]},1).to({state:[{t:this.shape_96}]},1).to({state:[{t:this.shape_95}]},1).to({state:[{t:this.shape_94}]},1).to({state:[{t:this.shape_93}]},1).to({state:[{t:this.shape_92}]},1).to({state:[{t:this.shape_92}]},23).to({state:[{t:this.shape_93}]},1).to({state:[{t:this.shape_94}]},1).to({state:[{t:this.shape_95}]},1).to({state:[{t:this.shape_96}]},1).to({state:[{t:this.shape_97}]},1).to({state:[{t:this.shape_98}]},1).to({state:[{t:this.shape_99}]},1).to({state:[{t:this.shape_100}]},1).to({state:[{t:this.shape_92}]},1).to({state:[{t:this.shape_92}]},1).to({state:[{t:this.shape_101}]},1).to({state:[{t:this.shape_102}]},1).to({state:[{t:this.shape_96}]},1).to({state:[{t:this.shape_95}]},1).to({state:[{t:this.shape_94}]},1).to({state:[{t:this.shape_93}]},1).to({state:[{t:this.shape_92}]},1).to({state:[{t:this.shape_92}]},1).to({state:[{t:this.shape_93}]},1).to({state:[{t:this.shape_94}]},1).to({state:[{t:this.shape_95}]},1).to({state:[{t:this.shape_96}]},1).to({state:[{t:this.shape_95}]},1).to({state:[{t:this.shape_94}]},1).to({state:[{t:this.shape_93}]},1).to({state:[{t:this.shape_92}]},1).wait(4));
	this.timeline.addTween(cjs.Tween.get(this.shape_92).wait(6).to({_off:true},1).wait(8).to({_off:false},0).wait(1).to({_off:true},1).wait(6).to({_off:false},0).wait(1).to({_off:true},1).wait(7).to({_off:false},0).wait(23).to({_off:true},1).wait(8).to({_off:false},0).wait(1).to({_off:true},1).wait(6).to({_off:false},0).wait(1).to({_off:true},1).wait(7).to({_off:false},0).wait(4));
	this.timeline.addTween(cjs.Tween.get(this.shape_93).wait(7).to({_off:false},0).to({_off:true},1).wait(14).to({_off:false},0).to({_off:true},1).wait(2).to({_off:false},0).to({_off:true},1).wait(5).to({_off:false},0).to({_off:true},1).wait(24).to({_off:false},0).to({_off:true},1).wait(14).to({_off:false},0).to({_off:true},1).wait(2).to({_off:false},0).to({_off:true},1).wait(5).to({_off:false},0).to({_off:true},1).wait(4));
	this.timeline.addTween(cjs.Tween.get(this.shape_94).wait(8).to({_off:false},0).to({_off:true},1).wait(12).to({_off:false},0).to({_off:true},1).wait(4).to({_off:false},0).to({_off:true},1).wait(3).to({_off:false},0).to({_off:true},1).wait(26).to({_off:false},0).to({_off:true},1).wait(12).to({_off:false},0).to({_off:true},1).wait(4).to({_off:false},0).to({_off:true},1).wait(3).to({_off:false},0).to({_off:true},1).wait(5));
	this.timeline.addTween(cjs.Tween.get(this.shape_95).wait(9).to({_off:false},0).to({_off:true},1).wait(10).to({_off:false},0).to({_off:true},1).wait(6).to({_off:false},0).to({_off:true},1).wait(1).to({_off:false},0).to({_off:true},1).wait(28).to({_off:false},0).to({_off:true},1).wait(10).to({_off:false},0).to({_off:true},1).wait(6).to({_off:false},0).to({_off:true},1).wait(1).to({_off:false},0).to({_off:true},1).wait(6));
	this.timeline.addTween(cjs.Tween.get(this.shape_96).wait(10).to({_off:false},0).to({_off:true},1).wait(8).to({_off:false},0).to({_off:true},1).wait(8).to({_off:false},0).to({_off:true},1).wait(30).to({_off:false},0).to({_off:true},1).wait(8).to({_off:false},0).to({_off:true},1).wait(8).to({_off:false},0).to({_off:true},1).wait(7));

	// Layer 30
	this.shape_103 = new cjs.Shape();
	this.shape_103.graphics.f("#000000").s().p("AhRBVQghgeAAgvQAAguAhgkQAigjAvgEQAwgDAhAfQAiAfAAAvQAAAugiAjQghAkgwADIgIABQgqAAgfgdg");
	this.shape_103.setTransform(571.4,567.1);

	this.shape_104 = new cjs.Shape();
	this.shape_104.graphics.f("#000000").s().p("AgIBqQgtgFgegjQgegiAEgrQADgrAigcQAkgbAsAEQAvAGAdAiQAeAjgDAqQgEAsgjAbQgdAXgmAAIgNAAg");
	this.shape_104.setTransform(577.7,562.3);
	this.shape_104._off = true;

	this.shape_105 = new cjs.Shape();
	this.shape_105.graphics.f("#000000").s().p("AgQBhQgsgMgagnQgaglAHgoQAHgoAkgUQAkgTArANQAsAOAaAlQAaAmgHAoQgHAogkATQgVAMgYAAQgRAAgRgGg");
	this.shape_105.setTransform(584,557.6);
	this.shape_105._off = true;

	this.shape_106 = new cjs.Shape();
	this.shape_106.graphics.f("#000000").s().p("AgZBaQgqgVgWgqQgVgoAKgmQAKgkAmgMQAmgLAnAVQArAWAVApQAXApgLAkQgKAmgmALQgMAEgOAAQgZAAgbgOg");
	this.shape_106.setTransform(590.2,552.9);
	this.shape_106._off = true;

	this.shape_107 = new cjs.Shape();
	this.shape_107.graphics.f("#000000").s().p("AghBRQgogdgSgtQgTgrAPgjQAOghAmgDQAngDAmAdQAoAeASAtQASArgOAiQgNAignADIgIAAQgiAAgjgbg");
	this.shape_107.setTransform(596.5,548.2);
	this.shape_107._off = true;

	this.shape_108 = new cjs.Shape();
	this.shape_108.graphics.f("#000000").s().p("AgaBYQgqgXgVgqQgWgpAMglQALgkAmgJQAmgKAnAXQAqAXAVAqQAVApgLAkQgKAlgmAJQgLADgMAAQgbAAgcgQg");
	this.shape_108.setTransform(591.5,551.9);

	this.shape_109 = new cjs.Shape();
	this.shape_109.graphics.f("#000000").s().p("AgUBeQgrgQgYgnQgZgnAJgnQAIgnAlgQQAlgQAqAQQAqARAZAnQAYAngIAmQgIAoglAPQgSAJgUAAQgUAAgVgJg");
	this.shape_109.setTransform(586.5,555.7);

	this.shape_110 = new cjs.Shape();
	this.shape_110.graphics.f("#000000").s().p("AgMBlQgtgKgcglQgbgkAGgpQAFgpAkgXQAjgWAsAJQAtAKAbAlQAbAkgFApQgFAqgkAWQgYAQgdAAQgNAAgNgDg");
	this.shape_110.setTransform(581.4,559.5);

	this.shape_111 = new cjs.Shape();
	this.shape_111.graphics.f("#000000").s().p("AgGBrQgugDgfgiQgeghADgsQACgsAjgeQAjgcAtADQAuADAfAiQAeAigCArQgDAtgjAcQgfAbgoAAIgJgBg");
	this.shape_111.setTransform(576.4,563.3);

	this.shape_112 = new cjs.Shape();
	this.shape_112.graphics.f("#000000").s().p("AgKBnQgugIgcgkQgdgjAFgqQAFgqAjgZQAjgZAsAIQAuAIAcAjQAdAkgFApQgEArgkAYQgbAUggAAQgKAAgKgCg");
	this.shape_112.setTransform(579.8,560.8);

	this.shape_113 = new cjs.Shape();
	this.shape_113.graphics.f("#000000").s().p("AgWBcQgqgSgYgpQgXgnAJgmQAKgmAlgOQAlgOApASQAqATAYAoQAXAogJAmQgJAmglAOQgQAGgQAAQgXAAgYgLg");
	this.shape_113.setTransform(588.1,554.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_103}]}).to({state:[{t:this.shape_103}]},6).to({state:[{t:this.shape_104}]},1).to({state:[{t:this.shape_105}]},1).to({state:[{t:this.shape_106}]},1).to({state:[{t:this.shape_107}]},1).to({state:[{t:this.shape_108}]},1).to({state:[{t:this.shape_109}]},1).to({state:[{t:this.shape_110}]},1).to({state:[{t:this.shape_111}]},1).to({state:[{t:this.shape_103}]},1).to({state:[{t:this.shape_103}]},1).to({state:[{t:this.shape_112}]},1).to({state:[{t:this.shape_113}]},1).to({state:[{t:this.shape_107}]},1).to({state:[{t:this.shape_106}]},1).to({state:[{t:this.shape_105}]},1).to({state:[{t:this.shape_104}]},1).to({state:[{t:this.shape_103}]},1).to({state:[{t:this.shape_103}]},1).to({state:[{t:this.shape_104}]},1).to({state:[{t:this.shape_105}]},1).to({state:[{t:this.shape_106}]},1).to({state:[{t:this.shape_107}]},1).to({state:[{t:this.shape_106}]},1).to({state:[{t:this.shape_105}]},1).to({state:[{t:this.shape_104}]},1).to({state:[{t:this.shape_103}]},1).to({state:[{t:this.shape_103}]},23).to({state:[{t:this.shape_104}]},1).to({state:[{t:this.shape_105}]},1).to({state:[{t:this.shape_106}]},1).to({state:[{t:this.shape_107}]},1).to({state:[{t:this.shape_108}]},1).to({state:[{t:this.shape_109}]},1).to({state:[{t:this.shape_110}]},1).to({state:[{t:this.shape_111}]},1).to({state:[{t:this.shape_103}]},1).to({state:[{t:this.shape_103}]},1).to({state:[{t:this.shape_112}]},1).to({state:[{t:this.shape_113}]},1).to({state:[{t:this.shape_107}]},1).to({state:[{t:this.shape_106}]},1).to({state:[{t:this.shape_105}]},1).to({state:[{t:this.shape_104}]},1).to({state:[{t:this.shape_103}]},1).to({state:[{t:this.shape_103}]},1).to({state:[{t:this.shape_104}]},1).to({state:[{t:this.shape_105}]},1).to({state:[{t:this.shape_106}]},1).to({state:[{t:this.shape_107}]},1).to({state:[{t:this.shape_106}]},1).to({state:[{t:this.shape_105}]},1).to({state:[{t:this.shape_104}]},1).to({state:[{t:this.shape_103}]},1).wait(4));
	this.timeline.addTween(cjs.Tween.get(this.shape_103).wait(6).to({_off:true},1).wait(8).to({_off:false},0).wait(1).to({_off:true},1).wait(6).to({_off:false},0).wait(1).to({_off:true},1).wait(7).to({_off:false},0).wait(23).to({_off:true},1).wait(8).to({_off:false},0).wait(1).to({_off:true},1).wait(6).to({_off:false},0).wait(1).to({_off:true},1).wait(7).to({_off:false},0).wait(4));
	this.timeline.addTween(cjs.Tween.get(this.shape_104).wait(7).to({_off:false},0).to({_off:true},1).wait(14).to({_off:false},0).to({_off:true},1).wait(2).to({_off:false},0).to({_off:true},1).wait(5).to({_off:false},0).to({_off:true},1).wait(24).to({_off:false},0).to({_off:true},1).wait(14).to({_off:false},0).to({_off:true},1).wait(2).to({_off:false},0).to({_off:true},1).wait(5).to({_off:false},0).to({_off:true},1).wait(4));
	this.timeline.addTween(cjs.Tween.get(this.shape_105).wait(8).to({_off:false},0).to({_off:true},1).wait(12).to({_off:false},0).to({_off:true},1).wait(4).to({_off:false},0).to({_off:true},1).wait(3).to({_off:false},0).to({_off:true},1).wait(26).to({_off:false},0).to({_off:true},1).wait(12).to({_off:false},0).to({_off:true},1).wait(4).to({_off:false},0).to({_off:true},1).wait(3).to({_off:false},0).to({_off:true},1).wait(5));
	this.timeline.addTween(cjs.Tween.get(this.shape_106).wait(9).to({_off:false},0).to({_off:true},1).wait(10).to({_off:false},0).to({_off:true},1).wait(6).to({_off:false},0).to({_off:true},1).wait(1).to({_off:false},0).to({_off:true},1).wait(28).to({_off:false},0).to({_off:true},1).wait(10).to({_off:false},0).to({_off:true},1).wait(6).to({_off:false},0).to({_off:true},1).wait(1).to({_off:false},0).to({_off:true},1).wait(6));
	this.timeline.addTween(cjs.Tween.get(this.shape_107).wait(10).to({_off:false},0).to({_off:true},1).wait(8).to({_off:false},0).to({_off:true},1).wait(8).to({_off:false},0).to({_off:true},1).wait(30).to({_off:false},0).to({_off:true},1).wait(8).to({_off:false},0).to({_off:true},1).wait(8).to({_off:false},0).to({_off:true},1).wait(7));

	// Layer 4
	this.shape_114 = new cjs.Shape();
	this.shape_114.graphics.f().s("#000000").ss(12,1,1).p("AA1htQAWB7iCBg");
	this.shape_114.setTransform(749.7,531.1);

	this.shape_115 = new cjs.Shape();
	this.shape_115.graphics.f().s("#000000").ss(12,1,1).p("Ag4BuQCAhggSh7");
	this.shape_115.setTransform(749.6,531.1);
	this.shape_115._off = true;

	this.shape_116 = new cjs.Shape();
	this.shape_116.graphics.f().s("#000000").ss(12,1,1).p("Ag5BuQB4hggFh7");
	this.shape_116.setTransform(749.2,531.1);
	this.shape_116._off = true;

	this.shape_117 = new cjs.Shape();
	this.shape_117.graphics.f().s("#000000").ss(12,1,1).p("Ag+BuQBshgARh7");
	this.shape_117.setTransform(748.8,531.1);
	this.shape_117._off = true;

	this.shape_118 = new cjs.Shape();
	this.shape_118.graphics.f().s("#000000").ss(12,1,1).p("ABEhtQgtB7hbBg");
	this.shape_118.setTransform(748.2,531.1);
	this.shape_118._off = true;

	this.timeline.addTween(cjs.Tween.get(this.shape_114).wait(6).to({_off:true},1).wait(7).to({_off:false},0).wait(3).to({_off:true},1).wait(7).to({_off:false},0).wait(3).to({_off:true},1).wait(7).to({_off:false},0).wait(12).to({_off:true},1).wait(7).to({_off:false},0).wait(3).to({_off:true},1).wait(7).to({_off:false},0).wait(3).to({_off:true},1).wait(7).to({_off:false},0).wait(7));
	this.timeline.addTween(cjs.Tween.get(this.shape_115).wait(7).to({_off:false},0).to({_off:true},1).wait(5).to({_off:false},0).to({_off:true},1).wait(4).to({_off:false},0).to({_off:true},1).wait(5).to({_off:false},0).to({_off:true},1).wait(4).to({_off:false},0).to({_off:true},1).wait(5).to({_off:false},0).to({_off:true},1).wait(13).to({_off:false},0).to({_off:true},1).wait(5).to({_off:false},0).to({_off:true},1).wait(4).to({_off:false},0).to({_off:true},1).wait(5).to({_off:false},0).to({_off:true},1).wait(4).to({_off:false},0).to({_off:true},1).wait(5).to({_off:false},0).to({_off:true},1).wait(7));
	this.timeline.addTween(cjs.Tween.get(this.shape_116).wait(8).to({_off:false},0).to({_off:true},1).wait(3).to({_off:false},0).to({_off:true},1).wait(6).to({_off:false},0).to({_off:true},1).wait(3).to({_off:false},0).to({_off:true},1).wait(6).to({_off:false},0).to({_off:true},1).wait(3).to({_off:false},0).to({_off:true},1).wait(15).to({_off:false},0).to({_off:true},1).wait(3).to({_off:false},0).to({_off:true},1).wait(6).to({_off:false},0).to({_off:true},1).wait(3).to({_off:false},0).to({_off:true},1).wait(6).to({_off:false},0).to({_off:true},1).wait(3).to({_off:false},0).to({_off:true},1).wait(8));
	this.timeline.addTween(cjs.Tween.get(this.shape_117).wait(9).to({_off:false},0).to({_off:true},1).wait(1).to({_off:false},0).to({_off:true},1).wait(8).to({_off:false},0).to({_off:true},1).wait(1).to({_off:false},0).to({_off:true},1).wait(8).to({_off:false},0).to({_off:true},1).wait(1).to({_off:false},0).to({_off:true},1).wait(17).to({_off:false},0).to({_off:true},1).wait(1).to({_off:false},0).to({_off:true},1).wait(8).to({_off:false},0).to({_off:true},1).wait(1).to({_off:false},0).to({_off:true},1).wait(8).to({_off:false},0).to({_off:true},1).wait(1).to({_off:false},0).to({_off:true},1).wait(9));
	this.timeline.addTween(cjs.Tween.get(this.shape_118).wait(10).to({_off:false},0).to({_off:true},1).wait(10).to({_off:false},0).to({_off:true},1).wait(10).to({_off:false},0).to({_off:true},1).wait(19).to({_off:false},0).to({_off:true},1).wait(10).to({_off:false},0).to({_off:true},1).wait(10).to({_off:false},0).to({_off:true},1).wait(10));

	// Layer 3
	this.shape_119 = new cjs.Shape();
	this.shape_119.graphics.f().s("#000000").ss(12,1,1).p("AGfjkQj7LHpCmL");
	this.shape_119.setTransform(785.1,536.4);

	this.shape_120 = new cjs.Shape();
	this.shape_120.graphics.f().s("#000000").ss(12,1,1).p("AmfBYQJEGED7q9");
	this.shape_120.setTransform(785.1,536.2);
	this.shape_120._off = true;

	this.shape_121 = new cjs.Shape();
	this.shape_121.graphics.f().s("#000000").ss(12,1,1).p("AmjBYQJMFuD7qe");
	this.shape_121.setTransform(785.1,535.6);
	this.shape_121._off = true;

	this.shape_122 = new cjs.Shape();
	this.shape_122.graphics.f().s("#000000").ss(12,1,1).p("AmpBZQJZFID6pq");
	this.shape_122.setTransform(785,534.6);
	this.shape_122._off = true;

	this.shape_123 = new cjs.Shape();
	this.shape_123.graphics.f().s("#000000").ss(12,1,1).p("AGzi1Qj4IipukU");
	this.shape_123.setTransform(784.9,533.2);
	this.shape_123._off = true;

	this.timeline.addTween(cjs.Tween.get(this.shape_119).wait(6).to({_off:true},1).wait(7).to({_off:false},0).wait(3).to({_off:true},1).wait(7).to({_off:false},0).wait(3).to({_off:true},1).wait(7).to({_off:false},0).wait(12).to({_off:true},1).wait(7).to({_off:false},0).wait(3).to({_off:true},1).wait(7).to({_off:false},0).wait(3).to({_off:true},1).wait(7).to({_off:false},0).wait(7));
	this.timeline.addTween(cjs.Tween.get(this.shape_120).wait(7).to({_off:false},0).to({_off:true},1).wait(5).to({_off:false},0).to({_off:true},1).wait(4).to({_off:false},0).to({_off:true},1).wait(5).to({_off:false},0).to({_off:true},1).wait(4).to({_off:false},0).to({_off:true},1).wait(5).to({_off:false},0).to({_off:true},1).wait(13).to({_off:false},0).to({_off:true},1).wait(5).to({_off:false},0).to({_off:true},1).wait(4).to({_off:false},0).to({_off:true},1).wait(5).to({_off:false},0).to({_off:true},1).wait(4).to({_off:false},0).to({_off:true},1).wait(5).to({_off:false},0).to({_off:true},1).wait(7));
	this.timeline.addTween(cjs.Tween.get(this.shape_121).wait(8).to({_off:false},0).to({_off:true},1).wait(3).to({_off:false},0).to({_off:true},1).wait(6).to({_off:false},0).to({_off:true},1).wait(3).to({_off:false},0).to({_off:true},1).wait(6).to({_off:false},0).to({_off:true},1).wait(3).to({_off:false},0).to({_off:true},1).wait(15).to({_off:false},0).to({_off:true},1).wait(3).to({_off:false},0).to({_off:true},1).wait(6).to({_off:false},0).to({_off:true},1).wait(3).to({_off:false},0).to({_off:true},1).wait(6).to({_off:false},0).to({_off:true},1).wait(3).to({_off:false},0).to({_off:true},1).wait(8));
	this.timeline.addTween(cjs.Tween.get(this.shape_122).wait(9).to({_off:false},0).to({_off:true},1).wait(1).to({_off:false},0).to({_off:true},1).wait(8).to({_off:false},0).to({_off:true},1).wait(1).to({_off:false},0).to({_off:true},1).wait(8).to({_off:false},0).to({_off:true},1).wait(1).to({_off:false},0).to({_off:true},1).wait(17).to({_off:false},0).to({_off:true},1).wait(1).to({_off:false},0).to({_off:true},1).wait(8).to({_off:false},0).to({_off:true},1).wait(1).to({_off:false},0).to({_off:true},1).wait(8).to({_off:false},0).to({_off:true},1).wait(1).to({_off:false},0).to({_off:true},1).wait(9));
	this.timeline.addTween(cjs.Tween.get(this.shape_123).wait(10).to({_off:false},0).to({_off:true},1).wait(10).to({_off:false},0).to({_off:true},1).wait(10).to({_off:false},0).to({_off:true},1).wait(19).to({_off:false},0).to({_off:true},1).wait(10).to({_off:false},0).to({_off:true},1).wait(10).to({_off:false},0).to({_off:true},1).wait(10));

	// Layer 33
	this.instance_3 = new lib.bg2();
	this.instance_3.parent = this;
	this.instance_3.setTransform(252,-441);

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(85));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(540,-101,1310,1207);
// library properties:
lib.properties = {
	id: '882E20D59143E84CB83689380D10534B',
	width: 1080,
	height: 680,
	fps: 30,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: [
		{src:"assets/images/bg2.jpg", id:"bg2"}
	],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['882E20D59143E84CB83689380D10534B'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}



})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;

$(function () {
    var canvas, stage, exportRoot, anim_container, dom_overlay_container, fnStartAnimation;
    function init() {
        canvas = document.getElementById("canvas7");
        anim_container = document.getElementById("animation_container7");
        dom_overlay_container = document.getElementById("dom_overlay_container7");
        var comp=AdobeAn.getComposition("882E20D59143E84CB83689380D10534B");
        var lib=comp.getLibrary();
        var loader = new createjs.LoadQueue(false);
        loader.addEventListener("fileload", function(evt){handleFileLoad(evt,comp)});
        loader.addEventListener("complete", function(evt){handleComplete(evt,comp)});
        var lib=comp.getLibrary();
        loader.loadManifest(lib.properties.manifest);
    }
    function handleFileLoad(evt, comp) {
        var images=comp.getImages();
        if (evt && (evt.item.type == "image")) { images[evt.item.id] = evt.result; }
    }
    function handleComplete(evt,comp) {
        //This function is always called, irrespective of the content. You can use the variable "stage" after it is created in token create_stage.
        var lib=comp.getLibrary();
        var ss=comp.getSpriteSheet();
        var queue = evt.target;
        var ssMetadata = lib.ssMetadata;
        for(i=0; i<ssMetadata.length; i++) {
            ss[ssMetadata[i].name] = new createjs.SpriteSheet( {"images": [queue.getResult(ssMetadata[i].name)], "frames": ssMetadata[i].frames} )
        }
        exportRoot = new lib._1May();
        stage = new lib.Stage(canvas);
        //Registers the "tick" event listener.
        fnStartAnimation = function() {
            stage.addChild(exportRoot);
            createjs.Ticker.setFPS(lib.properties.fps);
            createjs.Ticker.addEventListener("tick", stage);
        }
        //Code to support hidpi screens and responsive scaling.
        function makeResponsive(isResp, respDim, isScale, scaleType) {
            var lastW, lastH, lastS=1;
            window.addEventListener('resize', resizeCanvas);
            resizeCanvas();
            function resizeCanvas() {
                var w = lib.properties.width, h = lib.properties.height;
                var iw = window.innerWidth, ih=window.innerHeight;
                var pRatio = window.devicePixelRatio || 1, xRatio=iw/w, yRatio=ih/h, sRatio=1;
                if(isResp) {
                    if((respDim=='width'&&lastW==iw) || (respDim=='height'&&lastH==ih)) {
                        sRatio = lastS;
                    }
                    else if(!isScale) {
                        if(iw<w || ih<h)
                            sRatio = Math.min(xRatio, yRatio);
                    }
                    else if(scaleType==1) {
                        sRatio = Math.min(xRatio, yRatio);
                    }
                    else if(scaleType==2) {
                        sRatio = Math.max(xRatio, yRatio);
                    }
                }
                canvas.width = w*pRatio*sRatio;
                canvas.height = h*pRatio*sRatio;
                canvas.style.width = dom_overlay_container.style.width = anim_container.style.width =  w*sRatio+'px';
                canvas.style.height = anim_container.style.height = dom_overlay_container.style.height = h*sRatio+'px';
                stage.scaleX = pRatio*sRatio;
                stage.scaleY = pRatio*sRatio;
                lastW = iw; lastH = ih; lastS = sRatio;
                stage.tickOnUpdate = false;
                stage.update();
                stage.tickOnUpdate = true;
            }
        }
        makeResponsive(true,'both',true,1);
        AdobeAn.compositionLoaded(lib.properties.id);
        fnStartAnimation();
    }
    init();
});