(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [];


// symbols:



(lib.bg = function() {
	this.initialize(img.bg);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,1080,680);


(lib.Hat_arm = function() {
	this.initialize(img.Hat_arm);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,165,167);


(lib.pack = function() {
	this.initialize(img.pack);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,417,489);


(lib.tablet_arm = function() {
	this.initialize(img.tablet_arm);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,278,143);// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.Symbol5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(13.5,1,1).p("AjcA7QBihxB+gDQCBgEBYBw");
	this.shape.setTransform(95.8,5.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(13.5,1,1).p("AjcBGQBiiHB+gEQCBgEBYCG");
	this.shape_1.setTransform(95.8,4.7);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(13.5,1,1).p("AjcBQQBiiaB+gEQCBgFBYCZ");
	this.shape_2.setTransform(95.8,3.7);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(13.5,1,1).p("AjcBYQBiiqB+gEQCBgGBYCp");
	this.shape_3.setTransform(95.8,2.9);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(13.5,1,1).p("AjcBeQBii2B+gFQCBgGBYC1");
	this.shape_4.setTransform(95.8,2.3);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(13.5,1,1).p("AjcBkQBijBB+gFQCBgGBYC/");
	this.shape_5.setTransform(95.8,1.7);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(13.5,1,1).p("AjcBnQBijIB+gFQCBgHBYDH");
	this.shape_6.setTransform(95.8,1.4);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(13.5,1,1).p("AjcBqQBijNB+gFQCBgHBYDL");
	this.shape_7.setTransform(95.8,1.1);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(13.5,1,1).p("AjcBqQBijOB+gFQCBgHBYDN");
	this.shape_8.setTransform(95.8,1.1);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(13.5,1,1).p("AjcBfQBii4B+gFQCBgGBYC3");
	this.shape_9.setTransform(95.8,2.2);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(13.5,1,1).p("AjcBVQBiilB+gEQCBgGBYCk");
	this.shape_10.setTransform(95.8,3.2);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(13.5,1,1).p("AjcBNQBiiVB+gEQCBgFBYCU");
	this.shape_11.setTransform(95.8,4);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(13.5,1,1).p("AjcBHQBiiIB+gEQCBgEBYCH");
	this.shape_12.setTransform(95.8,4.6);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(13.5,1,1).p("AjcBBQBih+B+gDQCBgEBYB9");
	this.shape_13.setTransform(95.8,5.2);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(13.5,1,1).p("AjcA+QBih3B+gDQCBgEBYB2");
	this.shape_14.setTransform(95.8,5.5);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(13.5,1,1).p("AjcA7QBihyB+gDQCBgEBYBy");
	this.shape_15.setTransform(95.8,5.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},29).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_8}]},24).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape}]},1).wait(11));

	// Layer 4
	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(13.5,1,1).p("AjcA7QBihxB+gDQCBgEBYBw");
	this.shape_16.setTransform(22.1,5.8);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(13.5,1,1).p("AjcBGQBiiHB+gEQCBgEBYCG");
	this.shape_17.setTransform(22.1,4.7);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#000000").ss(13.5,1,1).p("AjcBQQBiiaB+gEQCBgFBYCZ");
	this.shape_18.setTransform(22.1,3.7);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#000000").ss(13.5,1,1).p("AjcBYQBiiqB+gEQCBgGBYCp");
	this.shape_19.setTransform(22.1,2.9);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#000000").ss(13.5,1,1).p("AjcBeQBii2B+gFQCBgGBYC1");
	this.shape_20.setTransform(22.1,2.3);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#000000").ss(13.5,1,1).p("AjcBkQBijBB+gFQCBgGBYC/");
	this.shape_21.setTransform(22.1,1.7);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#000000").ss(13.5,1,1).p("AjcBnQBijIB+gFQCBgHBYDH");
	this.shape_22.setTransform(22.1,1.4);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#000000").ss(13.5,1,1).p("AjcBqQBijNB+gFQCBgHBYDL");
	this.shape_23.setTransform(22.1,1.1);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#000000").ss(13.5,1,1).p("AjcBqQBijOB+gFQCBgHBYDN");
	this.shape_24.setTransform(22.1,1.1);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f().s("#000000").ss(13.5,1,1).p("AjcBfQBii4B+gFQCBgGBYC3");
	this.shape_25.setTransform(22.1,2.2);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f().s("#000000").ss(13.5,1,1).p("AjcBVQBiilB+gEQCBgGBYCk");
	this.shape_26.setTransform(22.1,3.2);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f().s("#000000").ss(13.5,1,1).p("AjcBNQBiiVB+gEQCBgFBYCU");
	this.shape_27.setTransform(22.1,4);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f().s("#000000").ss(13.5,1,1).p("AjcBHQBiiIB+gEQCBgEBYCH");
	this.shape_28.setTransform(22.1,4.6);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f().s("#000000").ss(13.5,1,1).p("AjcBBQBih+B+gDQCBgEBYB9");
	this.shape_29.setTransform(22.1,5.2);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f().s("#000000").ss(13.5,1,1).p("AjcA+QBih3B+gDQCBgEBYB2");
	this.shape_30.setTransform(22.1,5.5);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f().s("#000000").ss(13.5,1,1).p("AjcA7QBihyB+gDQCBgEBYBy");
	this.shape_31.setTransform(22.1,5.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_16}]}).to({state:[{t:this.shape_16}]},29).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_24}]},24).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_27}]},1).to({state:[{t:this.shape_28}]},1).to({state:[{t:this.shape_29}]},1).to({state:[{t:this.shape_30}]},1).to({state:[{t:this.shape_31}]},1).to({state:[{t:this.shape_16}]},1).wait(11));

	// Layer 3
	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f().s("#000000").ss(13.5,1,1).p("AjDhHQgFA5A2AqQAwAmBLAFQBKAFA9gfQA9ggAYg0");
	this.shape_32.setTransform(61.7,49.4);

	this.timeline.addTween(cjs.Tween.get(this.shape_32).wait(80));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-6.7,-6.7,131.4,70.1);


(lib.Symbol2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.Hat_arm();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol2, new cjs.Rectangle(0,0,165,167), null);


(lib.Symbol1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.tablet_arm();
	this.instance.parent = this;

	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(13,1,1).p("ABTCWQCRkqlggB");
	this.shape.setTransform(268.4,42.3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape},{t:this.instance}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol1, new cjs.Rectangle(0,0,287.5,143), null);


(lib.Symbol4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Symbol 2
	this.instance = new lib.Symbol2();
	this.instance.parent = this;
	this.instance.setTransform(128.2,99.2,1,1,0,0,0,128.2,99.2);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(18).to({regX:128.3,rotation:-19.2,x:182.2,y:41.8},15,cjs.Ease.get(1)).wait(5).to({regX:128.2,rotation:0,x:128.2,y:99.2},9,cjs.Ease.get(-1)).to({regX:128.3,rotation:-19.2,x:182.2,y:41.8},9,cjs.Ease.get(1)).wait(5).to({regX:128.2,rotation:0,x:128.2,y:99.2},15,cjs.Ease.get(1)).wait(4));

	// Layer 3
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(13.5,1,1).p("AHiHqQoTg9kEj9QkEj9CEmc");
	this.shape.setTransform(142.3,162.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(13.5,1,1).p("Al6oGQiqGfDyEDQDxEDICBo");
	this.shape_1.setTransform(146.4,159.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(13.5,1,1).p("AlCogQjNGhDhEJQDgEJHyCO");
	this.shape_2.setTransform(150,156.6);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(13.5,1,1).p("AkMo5QjtGjDQEOQDQEOHkCz");
	this.shape_3.setTransform(153.1,153.8);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(13.5,1,1).p("AjZpPQkLGkDCETQDBETHWDV");
	this.shape_4.setTransform(155.9,151.3);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(13.5,1,1).p("AiqpkQknGmC1EYQC0EXHKD0");
	this.shape_5.setTransform(158.4,148.9);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(13.5,1,1).p("Ah/p3Qk/GoCoEbQCoEcG/EQ");
	this.shape_6.setTransform(160.6,146.8);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(13.5,1,1).p("AhXqIQlWGqCeEfQCdEgG0Eo");
	this.shape_7.setTransform(162.4,145);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(13.5,1,1).p("Ag0qWQlqGpCVEjQCTEjGsE+");
	this.shape_8.setTransform(164.1,143.3);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(13.5,1,1).p("AgWqjQl6GrCMElQCLEmGkFR");
	this.shape_9.setTransform(165.5,141.9);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(13.5,1,1).p("AADquQmHGsCFEoQCEEoGdFh");
	this.shape_10.setTransform(166.6,140.6);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(13.5,1,1).p("AAZq2QmUGsCAEqQB+EqGYFu");
	this.shape_11.setTransform(167.6,139.7);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(13.5,1,1).p("AApq9QmcGsB7ErQB6EsGUF4");
	this.shape_12.setTransform(168.3,138.9);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(13.5,1,1).p("AA1rCQmjGtB4EsQB3EtGRF/");
	this.shape_13.setTransform(168.8,138.3);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(13.5,1,1).p("AA8rFQmmGtB1EtQB1EtGPGE");
	this.shape_14.setTransform(169.1,138);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(13.5,1,1).p("AEPLHQmPmFh1kuQh0ktGomt");
	this.shape_15.setTransform(169.2,137.9);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(13.5,1,1).p("AA4rEQmlGuB3EsQB2EtGRGC");
	this.shape_16.setTransform(169,138.2);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(13.5,1,1).p("AAkq8QmZGuB8EqQB7ErGVF2");
	this.shape_17.setTransform(168.1,139.1);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#000000").ss(13.5,1,1).p("AgqqbQlvGqCREkQCREkGpFF");
	this.shape_18.setTransform(164.6,142.8);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#000000").ss(13.5,1,1).p("AhkqCQlPGoChEeQChEfG4Eg");
	this.shape_19.setTransform(161.9,145.6);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#000000").ss(13.5,1,1).p("Aj6pAQj4GjDMEPQDLERHeC+");
	this.shape_20.setTransform(154.1,152.9);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#000000").ss(13.5,1,1).p("AlUoXQjCGgDmEGQDmEIH3CB");
	this.shape_21.setTransform(148.8,157.5);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#000000").ss(13.5,1,1).p("AgFqqQmEGsCIEmQCHEoGfFb");
	this.shape_22.setTransform(166.2,141.1);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#000000").ss(13.5,1,1).p("AhFqPQlgGpCZEhQCYEhGvE0");
	this.shape_23.setTransform(163.3,144.1);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#000000").ss(13.5,1,1).p("AizpgQkhGmC3EXQC2EXHMDt");
	this.shape_24.setTransform(157.9,149.4);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f().s("#000000").ss(13.5,1,1).p("AjipMQkGGlDEESQDEETHZDP");
	this.shape_25.setTransform(155.4,151.7);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f().s("#000000").ss(13.5,1,1).p("AkwooQjYGhDcELQDaEKHuCb");
	this.shape_26.setTransform(151,155.7);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f().s("#000000").ss(13.5,1,1).p("AlQoZQjEGgDlEHQDkEIH3CE");
	this.shape_27.setTransform(149,157.3);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f().s("#000000").ss(13.5,1,1).p("AlsoMQizGfDuEEQDsEFH+Bx");
	this.shape_28.setTransform(147.3,158.8);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f().s("#000000").ss(13.5,1,1).p("AmCoCQilGeD0ECQD0EDIFBh");
	this.shape_29.setTransform(145.8,160);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f().s("#000000").ss(13.5,1,1).p("AmVn5QiYGeD5EAQD6EAIKBV");
	this.shape_30.setTransform(144.6,161);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f().s("#000000").ss(13.5,1,1).p("AmjnyQiQGdD/D/QD9D/IPBK");
	this.shape_31.setTransform(143.6,161.8);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f().s("#000000").ss(13.5,1,1).p("AmtntQiJGdECD9QEBD+IRBD");
	this.shape_32.setTransform(142.9,162.3);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f().s("#000000").ss(13.5,1,1).p("AmznqQiFGdEDD8QEDD9ITA/");
	this.shape_33.setTransform(142.5,162.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},18).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_15}]},5).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_15}]},5).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_27}]},1).to({state:[{t:this.shape_28}]},1).to({state:[{t:this.shape_29}]},1).to({state:[{t:this.shape_30}]},1).to({state:[{t:this.shape_31}]},1).to({state:[{t:this.shape_32}]},1).to({state:[{t:this.shape_33}]},1).to({state:[{t:this.shape}]},1).wait(4));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,197.3,218.5);


(lib.Symbol3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Symbol 1
	this.instance = new lib.Symbol1();
	this.instance.parent = this;
	this.instance.setTransform(278,72.4,1,1,0,0,0,278,61.1);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(9).to({regX:278.1,rotation:15,x:315.9,y:99},15,cjs.Ease.get(1)).wait(30).to({regX:278,rotation:0,x:278,y:72.4},15,cjs.Ease.get(1)).wait(11));

	// Layer 3
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(13.5,1,1).p("ACZnKQERGVAnDQQAnDRiTA9QiUA+jygyQj0gyjDiT");
	this.shape.setTransform(321.8,45.9);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(13.5,1,1).p("AnPEUQDLCGDzAkQDzAkCJhJQCJhJgyjMQgyjLkjmC");
	this.shape_1.setTransform(326.2,45.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(13.5,1,1).p("AnGE1QDRB4D0AYQDyAYCAhUQB/hUg8jIQg7jHk1lw");
	this.shape_2.setTransform(330.3,45.5);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(13.5,1,1).p("Am/FTQDYBsDyAMQD0AMB2heQB3hehFjEQhFjDlFlf");
	this.shape_3.setTransform(334.2,45.5);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(13.5,1,1).p("Am5FsQDeBiDyABQDzABBvhnQBuhnhNjBQhOi/lSlQ");
	this.shape_4.setTransform(337.8,45.7);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(13.5,1,1).p("AmzGDQDiBXDygIQDzgJBohvQBmhwhVi9QhVi9lflB");
	this.shape_5.setTransform(341.2,45.9);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(13.5,1,1).p("AmvGWQDoBPDygSQDygRBhh3QBgh3hci7Qhci5lsk1");
	this.shape_6.setTransform(344.2,46.2);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(13.5,1,1).p("AmrGnQDsBGDygZQDzgaBah9QBbh+hji4Qhii3l3kp");
	this.shape_7.setTransform(346.9,46.5);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(13.5,1,1).p("AmnG2QDwA+DxggQDzghBViDQBViEhoi1Qhoi1mAkg");
	this.shape_8.setTransform(349.3,46.8);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(13.5,1,1).p("AmlHBQDzA5DygnQDzgmBQiJQBRiJhtiyQhti0mIkX");
	this.shape_9.setTransform(351.5,47.2);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(13.5,1,1).p("AmiHLQD1A0DygsQDzgsBMiNQBNiNhxixQhxiymPkQ");
	this.shape_10.setTransform(353.3,47.5);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(13.5,1,1).p("AmhHTQD4AvDygwQDygwBKiRQBJiRh0ivQh0ixmVkJ");
	this.shape_11.setTransform(354.8,47.8);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(13.5,1,1).p("AmfHZQD5ArDygzQDygzBIiUQBGiTh2ivQh3ivmakF");
	this.shape_12.setTransform(355.9,48);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(13.5,1,1).p("AmeHdQD7ApDxg1QDzg2BFiVQBFiWh4iuQh5iumdkC");
	this.shape_13.setTransform(356.7,48.2);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(13.5,1,1).p("AmeHgQD8AoDyg3QDyg4BFiWQBDiXh5itQh6ivmfj/");
	this.shape_14.setTransform(357.2,48.3);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(13.5,1,1).p("AjJnwQGfD/B6CuQB6CthECYQhDCXjzA3QjxA4j8go");
	this.shape_15.setTransform(357.4,48.3);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(13.5,1,1).p("AmjHIQD0A1DygqQDzgqBOiLQBOiMhwiyQhviymNkT");
	this.shape_16.setTransform(352.6,47.4);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(13.5,1,1).p("AmpGvQDuBCDygdQDzgdBXiBQBYiBhmi2Qhli3l8kk");
	this.shape_17.setTransform(348.2,46.7);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#000000").ss(13.5,1,1).p("Am0F/QDhBZDzgHQDygHBphtQBohuhTi+QhUi9ldlF");
	this.shape_18.setTransform(340.5,45.8);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#000000").ss(13.5,1,1).p("Am6FoQDdBjDyADQDzADBwhlQBwhlhMjBQhMjBlQlS");
	this.shape_19.setTransform(337.2,45.6);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#000000").ss(13.5,1,1).p("AnEE/QDTB0DzAUQDzAUB9hXQB8hXg+jHQg/jGk6lq");
	this.shape_20.setTransform(331.6,45.5);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#000000").ss(13.5,1,1).p("AnIEtQDPB8D0AbQDyAbCChRQCChRg5jJQg5jJkxl0");
	this.shape_21.setTransform(329.3,45.5);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#000000").ss(13.5,1,1).p("AnMEdQDMCCDzAhQDzAhCHhMQCGhMg0jLQg1jKkol9");
	this.shape_22.setTransform(327.3,45.6);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#000000").ss(13.5,1,1).p("AnQEPQDKCHDzAnQDzAmCLhIQCKhHgxjNQgwjMkhmE");
	this.shape_23.setTransform(325.6,45.7);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#000000").ss(13.5,1,1).p("AnTEEQDHCLD0ArQDzAqCNhDQCOhEgtjOQgtjOkbmK");
	this.shape_24.setTransform(324.2,45.7);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f().s("#000000").ss(13.5,1,1).p("AnVD7QDFCPD0AtQDzAuCQhBQCQhBgrjPQgqjPkXmP");
	this.shape_25.setTransform(323.2,45.8);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f().s("#000000").ss(13.5,1,1).p("AnXD0QDECSD0AwQDzAwCSg/QCRg/gojQQgpjQkTmS");
	this.shape_26.setTransform(322.4,45.9);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f().s("#000000").ss(13.5,1,1).p("AnYDxQDDCSD0AyQDzAyCTg+QCTg+gojRQgnjPkSmV");
	this.shape_27.setTransform(322,45.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},9).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_15}]},30).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_27}]},1).to({state:[{t:this.shape}]},1).wait(11));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,-6.7,375.9,161.1);


// stage content:
(lib.Travel = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 15
	this.instance = new lib.Symbol5();
	this.instance.parent = this;
	this.instance.setTransform(890.1,272.1,1,1,0,0,0,59,28.3);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(80));

	// Layer 10
	this.instance_1 = new lib.Symbol4();
	this.instance_1.parent = this;
	this.instance_1.setTransform(690.7,223.3,1,1,0,0,0,98.7,109.3);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(80));

	// Layer 12
	this.instance_2 = new lib.Symbol3();
	this.instance_2.parent = this;
	this.instance_2.setTransform(799.9,408.4,1,1,0,0,0,187.9,73.8);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(80));

	// Layer 9
	this.instance_3 = new lib.pack();
	this.instance_3.parent = this;
	this.instance_3.setTransform(619,153);

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(80));

	// Layer 8
	this.instance_4 = new lib.bg();
	this.instance_4.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(80));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(540,340,1080,680);
// library properties:
lib.properties = {
	id: 'AA99F51356CD3B4F95B3C5755C9C92C5',
	width: 1080,
	height: 680,
	fps: 30,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: [
		{src:"assets/images/Hat_arm21.png", id:"Hat_arm"},
		{src:"assets/images/pack21.png", id:"pack"},
		{src:"assets/images/tablet_arm21.png", id:"tablet_arm"}
	],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['AA99F51356CD3B4F95B3C5755C9C92C5'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}



})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;

$(function () {
    var canvas, stage, exportRoot, anim_container, dom_overlay_container, fnStartAnimation;
    function init() {
        canvas = document.getElementById("canvas21");
        anim_container = document.getElementById("animation_container21");
        dom_overlay_container = document.getElementById("dom_overlay_container21");
        var comp=AdobeAn.getComposition("AA99F51356CD3B4F95B3C5755C9C92C5");
        var lib=comp.getLibrary();
        var loader = new createjs.LoadQueue(false);
        loader.addEventListener("fileload", function(evt){handleFileLoad(evt,comp)});
        loader.addEventListener("complete", function(evt){handleComplete(evt,comp)});
        var lib=comp.getLibrary();
        loader.loadManifest(lib.properties.manifest);
    }
    function handleFileLoad(evt, comp) {
        var images=comp.getImages();
        if (evt && (evt.item.type == "image")) { images[evt.item.id] = evt.result; }
    }
    function handleComplete(evt,comp) {
        //This function is always called, irrespective of the content. You can use the variable "stage" after it is created in token create_stage.
        var lib=comp.getLibrary();
        var ss=comp.getSpriteSheet();
        var queue = evt.target;
        var ssMetadata = lib.ssMetadata;
        for(i=0; i<ssMetadata.length; i++) {
            ss[ssMetadata[i].name] = new createjs.SpriteSheet( {"images": [queue.getResult(ssMetadata[i].name)], "frames": ssMetadata[i].frames} )
        }
        exportRoot = new lib.Travel();
        stage = new lib.Stage(canvas);
        //Registers the "tick" event listener.
        fnStartAnimation = function() {
            stage.addChild(exportRoot);
            createjs.Ticker.setFPS(lib.properties.fps);
            createjs.Ticker.addEventListener("tick", stage);
        }
        //Code to support hidpi screens and responsive scaling.
        function makeResponsive(isResp, respDim, isScale, scaleType) {
            var lastW, lastH, lastS=1;
            window.addEventListener('resize', resizeCanvas);
            resizeCanvas();
            function resizeCanvas() {
                var w = lib.properties.width, h = lib.properties.height;
                var iw = window.innerWidth, ih=window.innerHeight;
                var pRatio = window.devicePixelRatio || 1, xRatio=iw/w, yRatio=ih/h, sRatio=1;
                if(isResp) {
                    if((respDim=='width'&&lastW==iw) || (respDim=='height'&&lastH==ih)) {
                        sRatio = lastS;
                    }
                    else if(!isScale) {
                        if(iw<w || ih<h)
                            sRatio = Math.min(xRatio, yRatio);
                    }
                    else if(scaleType==1) {
                        sRatio = Math.min(xRatio, yRatio);
                    }
                    else if(scaleType==2) {
                        sRatio = Math.max(xRatio, yRatio);
                    }
                }
                canvas.width = w*pRatio*sRatio;
                canvas.height = h*pRatio*sRatio;
                canvas.style.width = dom_overlay_container.style.width = anim_container.style.width =  w*sRatio+'px';
                canvas.style.height = anim_container.style.height = dom_overlay_container.style.height = h*sRatio+'px';
                stage.scaleX = pRatio*sRatio;
                stage.scaleY = pRatio*sRatio;
                lastW = iw; lastH = ih; lastS = sRatio;
                stage.tickOnUpdate = false;
                stage.update();
                stage.tickOnUpdate = true;
            }
        }
        makeResponsive(true,'both',true,2);
        AdobeAn.compositionLoaded(lib.properties.id);
        fnStartAnimation();
    }
    init();
});