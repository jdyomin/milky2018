(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [];


// symbols:



(lib.bg_pack = function() {
	this.initialize(img.bg_pack);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,1080,680);// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.Symbol14 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#F27172").s().p("Ag0AhQgIgXAMgaQALgbAVgJQBEgGAEAHQACADgGAHQggAOgCAWIABAJQABAMAEAGQADAfgjADIgGAAQgfAAgHgXg");
	this.shape.setTransform(5.6,5.6);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol14, new cjs.Rectangle(0,0,11.3,11.2), null);


(lib.Symbol11 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.rf(["rgba(124,124,124,0.698)","rgba(139,139,139,0)"],[0,1],0.7,-0.6,0,0.7,-0.6,25.6).s().p("AirCtQhLhLAChiQABhhBPg2QBPg2BigbQBjgcBLBLQBKBKgUBaQgUBZhBBSQhABSheAIIgRABQhVAAhDhEg");
	this.shape.setTransform(24.5,24.1);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol11, new cjs.Rectangle(0,0,49.1,48.1), null);


(lib.Symbol10 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Symbol 9
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#030303").ss(6.5,1,1).p("Ag0AXQAGhUBjBH");
	this.shape.setTransform(7.9,60.5);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#030303").ss(6.5,1,1).p("Ag3AVQAMhPBjBH");
	this.shape_1.setTransform(7.5,60.2);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#030303").ss(6.5,1,1).p("Ag6ATQAThJBjBH");
	this.shape_2.setTransform(7.2,59.8);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#030303").ss(6.5,1,1).p("Ag+APQAahDBjBH");
	this.shape_3.setTransform(6.9,59.7);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#030303").ss(6.5,1,1).p("AhBAJQAgg9BjBH");
	this.shape_4.setTransform(6.6,59.6);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#030303").ss(6.5,1,1).p("AhEAFQAmg4BjBH");
	this.shape_5.setTransform(6.3,59.5);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#030303").ss(6.5,1,1).p("AhHAAQAsgyBjBH");
	this.shape_6.setTransform(5.9,59.4);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#030303").ss(6.5,1,1).p("AhLgEQA0gtBjBH");
	this.shape_7.setTransform(5.6,59.3);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#030303").ss(6.5,1,1).p("AhLgOQA3gmBgBP");
	this.shape_8.setTransform(5.6,58.9);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#030303").ss(6.5,1,1).p("AhKgXQA6gfBbBW");
	this.shape_9.setTransform(5.6,58.4);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#030303").ss(6.5,1,1).p("AhKgfQA+gZBXBe");
	this.shape_10.setTransform(5.6,57.8);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#030303").ss(6.5,1,1).p("AhKgoQBCgRBTBk");
	this.shape_11.setTransform(5.7,57.2);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#030303").ss(6.5,1,1).p("AhKgwQBGgKBPBs");
	this.shape_12.setTransform(5.7,56.6);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#030303").ss(6.5,1,1).p("AhKg3QBKgEBLB0");
	this.shape_13.setTransform(5.7,55.9);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#030303").ss(6.5,1,1).p("AhKg/QBNAEBIB7");
	this.shape_14.setTransform(5.7,55.2);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#030303").ss(6.5,1,1).p("AhIAAQAugyBjBH");
	this.shape_15.setTransform(5.9,59.4);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#030303").ss(6.5,1,1).p("AhFADQAog2BjBH");
	this.shape_16.setTransform(6.2,59.5);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#030303").ss(6.5,1,1).p("AhCAIQAig7BjBH");
	this.shape_17.setTransform(6.5,59.6);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#030303").ss(6.5,1,1).p("Ag/AMQAchABjBH");
	this.shape_18.setTransform(6.7,59.7);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#030303").ss(6.5,1,1).p("Ag8AQQAXhFBjBH");
	this.shape_19.setTransform(7,59.7);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#030303").ss(6.5,1,1).p("Ag5ATQARhKBjBH");
	this.shape_20.setTransform(7.3,59.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},14).to({state:[{t:this.shape_1,p:{x:7.5}}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_1,p:{x:7.6}}]},1).to({state:[{t:this.shape}]},1).wait(22));

	// Symbol 8
	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#030303").ss(6.5,1,1).p("AhHg2QChgdgUCP");
	this.shape_21.setTransform(7.2,56);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#030303").ss(6.5,1,1).p("AhIgzQCgghgQCO");
	this.shape_22.setTransform(7,56.2);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#030303").ss(6.5,1,1).p("AhJgvQCggngNCO");
	this.shape_23.setTransform(6.8,56.4);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#030303").ss(6.5,1,1).p("AhLgrQCggsgKCO");
	this.shape_24.setTransform(6.6,56.5);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f().s("#030303").ss(6.5,1,1).p("AhNgnQChgygHCO");
	this.shape_25.setTransform(6.4,56.7);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f().s("#030303").ss(6.5,1,1).p("AhOgjQCgg3gDCN");
	this.shape_26.setTransform(6.2,56.9);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f().s("#030303").ss(6.5,1,1).p("AhQgfQCgg8ABCN");
	this.shape_27.setTransform(6.1,57);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f().s("#030303").ss(6.5,1,1).p("AhSgbQCghBAFCM");
	this.shape_28.setTransform(5.9,57.2);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f().s("#030303").ss(6.5,1,1).p("AhRgVQCZhIAKCL");
	this.shape_29.setTransform(6,57.4);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f().s("#030303").ss(6.5,1,1).p("AhQgPQCRhPARCK");
	this.shape_30.setTransform(6,57.6);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f().s("#030303").ss(6.5,1,1).p("AhQgIQCKhXAXCK");
	this.shape_31.setTransform(6.1,57.7);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f().s("#030303").ss(6.5,1,1).p("AhPgCQCBheAeCJ");
	this.shape_32.setTransform(6.1,57.9);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f().s("#030303").ss(6.5,1,1).p("AhPADQB6hjAlCH");
	this.shape_33.setTransform(6.2,58.1);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f().s("#030303").ss(6.5,1,1).p("AhOAKQByhrArCH");
	this.shape_34.setTransform(6.2,58.2);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f().s("#030303").ss(6.5,1,1).p("AhOAQQBrhxAyCF");
	this.shape_35.setTransform(6.3,58.4);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f().s("#030303").ss(6.5,1,1).p("AhQgeQCgg9ABCN");
	this.shape_36.setTransform(6.1,57);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f().s("#030303").ss(6.5,1,1).p("AhPgiQCgg4gCCN");
	this.shape_37.setTransform(6.2,56.9);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f().s("#030303").ss(6.5,1,1).p("AhNgmQCggzgFCN");
	this.shape_38.setTransform(6.4,56.8);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f().s("#030303").ss(6.5,1,1).p("AhMgpQCggvgICO");
	this.shape_39.setTransform(6.5,56.6);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f().s("#030303").ss(6.5,1,1).p("AhLgsQChgrgLCO");
	this.shape_40.setTransform(6.7,56.5);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f().s("#030303").ss(6.5,1,1).p("AhJgwQCggmgOCP");
	this.shape_41.setTransform(6.8,56.3);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f().s("#030303").ss(6.5,1,1).p("AhIgzQCgghgRCO");
	this.shape_42.setTransform(7,56.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_21}]}).to({state:[{t:this.shape_21}]},14).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_27}]},1).to({state:[{t:this.shape_28}]},1).to({state:[{t:this.shape_29}]},1).to({state:[{t:this.shape_30}]},1).to({state:[{t:this.shape_31}]},1).to({state:[{t:this.shape_32}]},1).to({state:[{t:this.shape_33}]},1).to({state:[{t:this.shape_34}]},1).to({state:[{t:this.shape_35}]},1).to({state:[{t:this.shape_34}]},1).to({state:[{t:this.shape_33}]},1).to({state:[{t:this.shape_32}]},1).to({state:[{t:this.shape_31}]},1).to({state:[{t:this.shape_30}]},1).to({state:[{t:this.shape_29}]},1).to({state:[{t:this.shape_28}]},1).to({state:[{t:this.shape_36}]},1).to({state:[{t:this.shape_37}]},1).to({state:[{t:this.shape_38}]},1).to({state:[{t:this.shape_39}]},1).to({state:[{t:this.shape_40}]},1).to({state:[{t:this.shape_41}]},1).to({state:[{t:this.shape_42}]},1).to({state:[{t:this.shape_21}]},1).wait(22));

	// Layer 4
	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f().s("#030303").ss(6.5,1,1).p("AEFlLQAnCqgPCNQgPCNhEBZQhHBdh5AXQh/AYirg8");
	this.shape_43.setTransform(43.8,33.2);

	this.timeline.addTween(cjs.Tween.get(this.shape_43).wait(65));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-3.2,-3.2,79.2,73);


(lib.Symbol7 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Symbol 6
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#030303").ss(5.5,1,1).p("AgDAxIAGhh");
	this.shape.setTransform(12.6,13.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#030303").ss(5.5,1,1).p("AgDAwQAFguADgx");
	this.shape_1.setTransform(12.5,13.5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#030303").ss(5.5,1,1).p("AgHAvQAMgqADgz");
	this.shape_2.setTransform(12.1,13.4);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#030303").ss(5.5,1,1).p("AgMAsQAYggABg3");
	this.shape_3.setTransform(11.6,13.1);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#030303").ss(5.5,1,1).p("AgUApQApgUgBg9");
	this.shape_4.setTransform(10.9,12.8);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#030303").ss(5.5,1,1).p("AgdAkQA+gDgDhE");
	this.shape_5.setTransform(9.9,12.3);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#030303").ss(5.5,1,1).p("AgVAoQAsgRgBg+");
	this.shape_6.setTransform(10.7,12.7);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#030303").ss(5.5,1,1).p("AgOArQAdgcAAg5");
	this.shape_7.setTransform(11.4,13);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#030303").ss(5.5,1,1).p("AgJAuQARgmACg1");
	this.shape_8.setTransform(11.9,13.3);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#030303").ss(5.5,1,1).p("AgGAvQAKgrADgy");
	this.shape_9.setTransform(12.3,13.4);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#030303").ss(5.5,1,1).p("AgDAxQAEgwADgx");
	this.shape_10.setTransform(12.5,13.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},9).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},4).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},4).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape}]},1).wait(25));
	this.timeline.addTween(cjs.Tween.get(this.shape).wait(9).to({_off:true},1).wait(10).to({_off:false},0).wait(4).to({_off:true},1).wait(10).to({_off:false},0).wait(4).to({_off:true},1).wait(10).to({_off:false},0).wait(25));

	// Symbol 5
	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#030303").ss(5.5,1,1).p("AgyAIQApguA8A0");
	this.shape_11.setTransform(7.7,7);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#030303").ss(5.5,1,1).p("AgyAJQAoguA9Az");
	this.shape_12.setTransform(7.7,7.1);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#030303").ss(5.5,1,1).p("AgxAMQAmgwA9Ax");
	this.shape_13.setTransform(7.7,7.2);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#030303").ss(5.5,1,1).p("AgxANQAkgyA/At");
	this.shape_14.setTransform(7.8,7.7);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#030303").ss(5.5,1,1).p("AgwAPQAfg0BCAm");
	this.shape_15.setTransform(7.8,8.4);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#030303").ss(5.5,1,1).p("AgvATQAag5BFAg");
	this.shape_16.setTransform(7.9,9.2);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#030303").ss(5.5,1,1).p("AgwAQQAfg1BCAl");
	this.shape_17.setTransform(7.9,8.5);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#030303").ss(5.5,1,1).p("AgxAOQAjgzBAAr");
	this.shape_18.setTransform(7.8,7.9);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#030303").ss(5.5,1,1).p("AgxANQAlgxA+Av");
	this.shape_19.setTransform(7.8,7.4);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#030303").ss(5.5,1,1).p("AgyAKQAngvA+Ay");
	this.shape_20.setTransform(7.7,7.2);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#030303").ss(5.5,1,1).p("AgyAJQAogvA9A0");
	this.shape_21.setTransform(7.7,7.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_11}]}).to({state:[{t:this.shape_11}]},9).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_11}]},4).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_11}]},4).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_11}]},1).wait(25));
	this.timeline.addTween(cjs.Tween.get(this.shape_11).wait(9).to({_off:true},1).wait(10).to({_off:false},0).wait(4).to({_off:true},1).wait(10).to({_off:false},0).wait(4).to({_off:true},1).wait(10).to({_off:false},0).wait(25));

	// Symbol 4
	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#030303").ss(5.5,1,1).p("AhBgfQBOgnA1Bw");
	this.shape_22.setTransform(6.6,4.2);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#030303").ss(5.5,1,1).p("AhBgeQBNgoA2Bw");
	this.shape_23.setTransform(6.6,4.3);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#030303").ss(5.5,1,1).p("AhBgbQBNgsA2Bv");
	this.shape_24.setTransform(6.7,4.5);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f().s("#030303").ss(5.5,1,1).p("AhAgUQBLgzA2Bs");
	this.shape_25.setTransform(6.7,4.7);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f().s("#030303").ss(5.5,1,1).p("Ag/gLQBIg8A3Bp");
	this.shape_26.setTransform(6.8,5);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f().s("#030303").ss(5.5,1,1).p("Ag9AAQBEhIA4Bm");
	this.shape_27.setTransform(7,5.4);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f().s("#030303").ss(5.5,1,1).p("Ag/gKQBIg+A3Bp");
	this.shape_28.setTransform(6.9,5.1);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f().s("#030303").ss(5.5,1,1).p("AhAgSQBKg1A3Br");
	this.shape_29.setTransform(6.8,4.8);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f().s("#030303").ss(5.5,1,1).p("AhAgYQBLgvA2Bu");
	this.shape_30.setTransform(6.7,4.6);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f().s("#030303").ss(5.5,1,1).p("AhBgcQBNgrA2Bw");
	this.shape_31.setTransform(6.7,4.4);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_22}]}).to({state:[{t:this.shape_22}]},9).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_27}]},1).to({state:[{t:this.shape_28}]},1).to({state:[{t:this.shape_29}]},1).to({state:[{t:this.shape_30}]},1).to({state:[{t:this.shape_31}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_22}]},4).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_27}]},1).to({state:[{t:this.shape_28}]},1).to({state:[{t:this.shape_29}]},1).to({state:[{t:this.shape_30}]},1).to({state:[{t:this.shape_31}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_22}]},4).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_27}]},1).to({state:[{t:this.shape_28}]},1).to({state:[{t:this.shape_29}]},1).to({state:[{t:this.shape_30}]},1).to({state:[{t:this.shape_31}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_22}]},1).wait(25));
	this.timeline.addTween(cjs.Tween.get(this.shape_22).wait(9).to({_off:true},1).wait(9).to({_off:false,y:4.3},0).wait(1).to({y:4.2},0).wait(4).to({_off:true},1).wait(9).to({_off:false,y:4.3},0).wait(1).to({y:4.2},0).wait(4).to({_off:true},1).wait(9).to({_off:false,y:4.3},0).wait(1).to({y:4.2},0).wait(25));

	// Layer 5
	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f().s("#000000").ss(6.5,1,1).p("AjhjeQDcAQDnGt");
	this.shape_32.setTransform(37,30.9);

	this.timeline.addTween(cjs.Tween.get(this.shape_32).wait(75));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-2.7,-2.7,65.6,59.2);


(lib.Symbol3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 4
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#1B1815").ss(6.5,1,1).p("AhegQQAbAzBGgDQBHgCAVg+");
	this.shape.setTransform(48.5,6.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	// Layer 5
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#1B1815").ss(6.5,1,1).p("AhegQQAbAzBGgDQBHgCAVg+");
	this.shape_1.setTransform(12.3,6.5);

	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol3, new cjs.Rectangle(-0.4,0,61.7,13), null);


(lib.Symbol1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#050502").s().p("AgYAZQgKgLAAgOQAAgNAKgLQALgKANAAQAOAAALAKQAJALABANQgBAOgJALQgLAJgOABQgNgBgLgJg");
	this.shape.setTransform(3.5,3.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol1, new cjs.Rectangle(0,0,6.9,6.9), null);


(lib.Symbol13 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.Symbol14();
	this.instance.parent = this;
	this.instance.setTransform(9.3,0.1,1,1,0,0,0,9.3,0.1);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({scaleY:1.33,y:2.1},6,cjs.Ease.get(-1)).to({scaleY:1,y:0.1},11,cjs.Ease.get(1)).wait(1));

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#030303").ss(6.5,1,1).p("Ah3gnQDGh6ApC7QgQBdhcgwQgrgtAuhL");
	this.shape.setTransform(1.9,-3.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#030303").ss(6.5,1,1).p("Ah3gnQDGh6ApC7QgQBehcgxQgrgtAuhL");
	this.shape_1.setTransform(1.9,-3);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#030303").ss(6.5,1,1).p("Ah3goQDGh7ApC+QgQBehcgxQgrgtAuhM");
	this.shape_2.setTransform(1.9,-2.9);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#030303").ss(6.5,1,1).p("Ah3goQDGh9ApDAQgQBghcgyQgrguAuhN");
	this.shape_3.setTransform(1.9,-2.7);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#030303").ss(6.5,1,1).p("Ah3gpQDGiAApDFQgQBihcgzQgrgvAuhP");
	this.shape_4.setTransform(1.9,-2.5);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#030303").ss(6.5,1,1).p("Ah3grQDGiDApDLQgQBkhcg0QgrgwAuhS");
	this.shape_5.setTransform(1.9,-2.1);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#030303").ss(6.5,1,1).p("Ah3gsQDGiHApDRQgQBohcg2QgrgyAuhU");
	this.shape_6.setTransform(1.9,-1.7);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#030303").ss(6.5,1,1).p("Ah3grQDGiFApDNQgQBmhcg1QgrgxAuhS");
	this.shape_7.setTransform(1.9,-2);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#030303").ss(6.5,1,1).p("Ah3gqQDGiDApDKQgQBkhcg0QgrgwAuhR");
	this.shape_8.setTransform(1.9,-2.2);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#030303").ss(6.5,1,1).p("Ah3gpQDGiBApDGQgQBjhcgzQgrgwAuhQ");
	this.shape_9.setTransform(1.9,-2.4);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#030303").ss(6.5,1,1).p("Ah3gpQDGh/ApDEQgQBhhcgyQgrgvAuhP");
	this.shape_10.setTransform(1.9,-2.5);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#030303").ss(6.5,1,1).p("Ah3gpQDGh9ApDBQgQBhhcgyQgrgvAuhN");
	this.shape_11.setTransform(1.9,-2.7);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#030303").ss(6.5,1,1).p("Ah3goQDGh9ApDAQgQBfhcgxQgrguAuhN");
	this.shape_12.setTransform(1.9,-2.8);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#030303").ss(6.5,1,1).p("Ah3goQDGh7ApC+QgQBehcgwQgrguAuhM");
	this.shape_13.setTransform(1.9,-2.9);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#030303").ss(6.5,1,1).p("Ah3goQDGh6ApC9QgQBdhcgwQgrgtAuhM");
	this.shape_14.setTransform(1.9,-3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-13.3,-15.1,30.4,26.3);


(lib.Symbol12 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.Symbol11();
	this.instance.parent = this;
	this.instance.setTransform(-59.4,34.2,1,1,0,0,0,51.6,-5);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({scaleX:1,scaleY:1,skewX:92.3,skewY:87.5},1).to({regX:51.4,regY:-4.7,scaleX:0.76,scaleY:0.78,skewX:157.8,skewY:219.9,y:34.1},2).to({regX:51.5,regY:-4.9,scaleX:1.29,scaleY:0.45,skewX:239.4,skewY:259.5,x:-59.5,y:33.9},1).to({regX:51.4,regY:-4.5,scaleX:1.36,scaleY:0.98,skewX:311.7,skewY:299.4,x:-59,y:34},1).to({regX:51.6,regY:-5,scaleX:1,scaleY:1,skewX:360,skewY:360,x:-59.4,y:34.2},2).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-111,39.2,49.1,48.1);


(lib.Symbol2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// eyes_part.png
	this.instance = new lib.Symbol3();
	this.instance.parent = this;
	this.instance.setTransform(30.5,17.5,1,1,0,0,0,30.5,6.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(19).to({y:16.7},2).wait(23).to({y:17.5},2).wait(30));

	// Layer 8
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#C5C5C3").s().p("AhZAiQABgWAbgaQAbgbAlAAQAmAAAaAbQAbAagFAEQgFAFgegeQgegfgjAGQgkAGgWAqQgOAcgEAAQgDAAABgIg");
	this.shape.setTransform(48.3,4.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#C5C5C3").s().p("AhcAkQABgiAagaQAbgbAmAAQAlAAAbAbQAbAaABAdQABAcgggpQghgoglAIQgnAHgWAmQgMAVgFAAQgFAAAAgQg");
	this.shape_1.setTransform(48.7,5.2);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#C5C5C3").s().p("AhbAhQAAglAbgbQAbgbAlAAQAmAAAbAbQAbAbAAAlQAAAmgegYQgegYgkAGQgmAHgYARQgIAGgFAAQgMAAAAgag");
	this.shape_2.setTransform(48.6,5.9);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#C5C5C3").s().p("AhZAiQABgWAbgaQAbgbAlAAQAmAAAaAbQAcAagGAEQgFAFgegeQgegfgjAGQgkAGgWAqQgOAcgEAAQgDAAABgIg");
	this.shape_3.setTransform(48.3,4.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape}]},19).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_2}]},23).to({state:[{t:this.shape_1}]},2).to({state:[{t:this.shape_3}]},1).to({state:[]},1).wait(28));

	// Symbol 1
	this.instance_1 = new lib.Symbol1();
	this.instance_1.parent = this;
	this.instance_1.setTransform(45.3,8.9,0.9,0.9,0,0,0,3.5,3.5);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(19).to({x:48.8,y:8.4},5,cjs.Ease.get(1)).wait(20).to({x:45.3,y:8.9},8,cjs.Ease.get(1)).wait(24));

	// Layer 9
	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AhABAQgbgaAAgmQAAglAbgbQAbgbAlAAQAmAAAbAbQAbAbAAAlQAAAmgbAaQgbAbgmAAQglAAgbgbg");
	this.shape_4.setTransform(48.6,9.2);

	this.timeline.addTween(cjs.Tween.get(this.shape_4).wait(76));

	// Layer 6
	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#C5C5C3").s().p("AhZAiQABgWAbgaQAbgbAlAAQAmAAAaAbQAcAagGAEQgFAFgegeQgegfgjAGQgkAGgWAqQgOAcgEAAQgDAAABgIg");
	this.shape_5.setTransform(12.3,4.2);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#C5C5C3").s().p("AhbAkQAAgiAbgaQAbgbAlAAQAmAAAaAbQAbAaABAcQABAdgggpQghgoglAIQgmAHgXAmQgMAVgFAAQgFAAABgQg");
	this.shape_6.setTransform(12.6,5.2);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#C5C5C3").s().p("AhbAhQAAglAbgbQAbgbAlAAQAmAAAbAbQAbAbAAAlQgBAmgdgYQgegYgkAGQgmAHgYARQgIAGgFAAQgLAAgBgag");
	this.shape_7.setTransform(12.6,5.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_5}]},19).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_7}]},23).to({state:[{t:this.shape_6}]},2).to({state:[{t:this.shape_5}]},1).to({state:[]},1).wait(28));

	// Symbol 1
	this.instance_2 = new lib.Symbol1();
	this.instance_2.parent = this;
	this.instance_2.setTransform(9.3,8.9,0.9,0.9,0,0,0,3.5,3.5);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(19).to({x:12.8,y:8.4},5,cjs.Ease.get(1)).wait(20).to({x:9.3,y:8.9},8,cjs.Ease.get(1)).wait(24));

	// Layer 4
	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AhABAQgbgaAAgmQAAglAbgbQAbgbAlAAQAmAAAbAbQAbAbAAAlQAAAmgbAaQgbAbgmAAQglAAgbgbg");
	this.shape_8.setTransform(12.6,9.2);

	this.timeline.addTween(cjs.Tween.get(this.shape_8).wait(76));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-0.4,0,61.7,24);


// stage content:
(lib.Main = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 6
	this.instance = new lib.Symbol2();
	this.instance.parent = this;
	this.instance.setTransform(835.4,267.1,1.836,1.836,0,0,0,30.5,12);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(76));

	// Layer 5
	this.instance_1 = new lib.Symbol13();
	this.instance_1.parent = this;
	this.instance_1.setTransform(848.2,339.1,1.836,1.836,0,0,0,5.6,5.5);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(76));

	// Layer 4
	this.instance_2 = new lib.Symbol10();
	this.instance_2.parent = this;
	this.instance_2.setTransform(943.9,315.3,1.836,1.836,0,0,0,69.9,-0.3);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(76));

	// Layer 3 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("At1RDMAAAgiFIbrAAMAAAAiFg");
	mask.setTransform(674.5,332.7);

	// Layer 7
	this.instance_3 = new lib.Symbol7();
	this.instance_3.parent = this;
	this.instance_3.setTransform(774.4,379.6,1.836,1.836,0,0,0,59.6,53.2);

	var maskedShapeInstanceList = [this.instance_3];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(4).to({regX:59.5,rotation:-5,x:774.3},10,cjs.Ease.get(1)).wait(29).to({regX:59.6,rotation:0,x:774.4},10,cjs.Ease.get(1)).wait(23));

	// Layer 8 (mask)
	var mask_1 = new cjs.Shape();
	mask_1._off = true;
	mask_1.graphics.p("Al/ONQmDl4igoVQihoUCfl5QCfl5GCAAQGCAAGBF5QGDF5ChIUQChIVigF4QifF5mBAAQmDAAmBl5gAFMo9QhOAbgKBvQgLByBACFQA/CGBjBNQBkBOBOgbQBOgZAKhxQALhwhAiGQg/iGhkhOQhLg5g+AAQgVAAgTAGg");
	mask_1.setTransform(628.7,378.4);

	// Layer 9
	this.instance_4 = new lib.Symbol12();
	this.instance_4.parent = this;
	this.instance_4.setTransform(661.9,360.6,1.836,1.836,0,0,0,-63,39);
	this.instance_4._off = true;

	var maskedShapeInstanceList = [this.instance_4];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask_1;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(6).to({_off:false},0).wait(70));

	// Layer 10
	this.instance_5 = new lib.Symbol12();
	this.instance_5.parent = this;
	this.instance_5.setTransform(661.9,360.6,1.836,1.836,0,0,0,-63,39);
	this.instance_5._off = true;

	var maskedShapeInstanceList = [this.instance_5];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask_1;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(3).to({_off:false},0).wait(73));

	// Layer 11
	this.instance_6 = new lib.Symbol12();
	this.instance_6.parent = this;
	this.instance_6.setTransform(661.9,360.6,1.836,1.836,0,0,0,-63,39);

	var maskedShapeInstanceList = [this.instance_6];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask_1;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(76));

	// Layer 2
	this.instance_7 = new lib.bg_pack();
	this.instance_7.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(76));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(540,340,1080,680);
// library properties:
lib.properties = {
	id: '6E1A8219982DB2449C28679FA7C04821',
	width: 1080,
	height: 680,
	fps: 30,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: [
		{src:"assets/images/bg_pack.jpg", id:"bg_pack"}
	],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['6E1A8219982DB2449C28679FA7C04821'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}



})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;

$(function () {
    var canvas, stage, exportRoot, anim_container, dom_overlay_container, fnStartAnimation;
    function init() {
        canvas = document.getElementById("canvas11");
        anim_container = document.getElementById("animation_container11");
        dom_overlay_container = document.getElementById("dom_overlay_container11");
        var comp=AdobeAn.getComposition("6E1A8219982DB2449C28679FA7C04821");
        var lib=comp.getLibrary();
        var loader = new createjs.LoadQueue(false);
        loader.addEventListener("fileload", function(evt){handleFileLoad(evt,comp)});
        loader.addEventListener("complete", function(evt){handleComplete(evt,comp)});
        var lib=comp.getLibrary();
        loader.loadManifest(lib.properties.manifest);
    }
    function handleFileLoad(evt, comp) {
        var images=comp.getImages();
        if (evt && (evt.item.type == "image")) { images[evt.item.id] = evt.result; }
    }
    function handleComplete(evt,comp) {
        //This function is always called, irrespective of the content. You can use the variable "stage" after it is created in token create_stage.
        var lib=comp.getLibrary();
        var ss=comp.getSpriteSheet();
        var queue = evt.target;
        var ssMetadata = lib.ssMetadata;
        for(i=0; i<ssMetadata.length; i++) {
            ss[ssMetadata[i].name] = new createjs.SpriteSheet( {"images": [queue.getResult(ssMetadata[i].name)], "frames": ssMetadata[i].frames} )
        }
        exportRoot = new lib.Main();
        stage = new lib.Stage(canvas);
        //Registers the "tick" event listener.
        fnStartAnimation = function() {
            stage.addChild(exportRoot);
            createjs.Ticker.setFPS(lib.properties.fps);
            createjs.Ticker.addEventListener("tick", stage);
        }
        //Code to support hidpi screens and responsive scaling.
        function makeResponsive(isResp, respDim, isScale, scaleType) {
            var lastW, lastH, lastS=1;
            window.addEventListener('resize', resizeCanvas);
            resizeCanvas();
            function resizeCanvas() {
                var w = lib.properties.width, h = lib.properties.height;
                var iw = window.innerWidth, ih=window.innerHeight;
                var pRatio = window.devicePixelRatio || 1, xRatio=iw/w, yRatio=ih/h, sRatio=1;
                if(isResp) {
                    if((respDim=='width'&&lastW==iw) || (respDim=='height'&&lastH==ih)) {
                        sRatio = lastS;
                    }
                    else if(!isScale) {
                        if(iw<w || ih<h)
                            sRatio = Math.min(xRatio, yRatio);
                    }
                    else if(scaleType==1) {
                        sRatio = Math.min(xRatio, yRatio);
                    }
                    else if(scaleType==2) {
                        sRatio = Math.max(xRatio, yRatio);
                    }
                }
                canvas.width = w*pRatio*sRatio;
                canvas.height = h*pRatio*sRatio;
                canvas.style.width = dom_overlay_container.style.width = anim_container.style.width =  w*sRatio+'px';
                canvas.style.height = anim_container.style.height = dom_overlay_container.style.height = h*sRatio+'px';
                stage.scaleX = pRatio*sRatio;
                stage.scaleY = pRatio*sRatio;
                lastW = iw; lastH = ih; lastS = sRatio;
                stage.tickOnUpdate = false;
                stage.update();
                stage.tickOnUpdate = true;
            }
        }
        makeResponsive(true,'both',true,2);
        AdobeAn.compositionLoaded(lib.properties.id);
        fnStartAnimation();
    }

    init();
});