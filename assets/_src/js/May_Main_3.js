(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [];


// symbols:



(lib.bg = function() {
	this.initialize(img.bg);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,1080,680);


(lib.milk_part = function() {
	this.initialize(img.milk_part);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,119,36);


(lib.pack = function() {
	this.initialize(img.pack);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,334,460);// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.Symbol9 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_6 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	var mask_graphics_0 = new cjs.Graphics().p("EgmTAQqIAAmhIAMAAQBcAABmgOQA0gHB/gYQDxgrC6gCQChABD4AtQB8AYA/AIQBrAOBjgBIAVgBIAMAAQBcAABlgOQA0gHB/gYQDygrC5gCQCfABD5AtQB8AYA/AIQBsAOBjgBIAUgBIALAAQBdAABlgOQA0gHCAgYQDygrC4gCQChABD5AtQB8AYA/AIQBrAOBigBIAWgBIAAGhg");
	var mask_graphics_1 = new cjs.Graphics().p("EgmTAQqIAAmhIAMAAQBcAABmgOQA0gHB/gYQDxgrC6gCQChABD4AtQB8AYA/AIQBsAOBigBIAVgBIAMAAQBcAABlgOQA0gHB/gYQDygrC5gCQCfABD5AtQB8AYA/AIQBsAOBjgBIAUgBIALAAQBdAABlgOQA0gHCAgYQDygrC4gCQChABD5AtQB8AYA/AIQBrAOBigBIAWgBIAAGhg");
	var mask_graphics_2 = new cjs.Graphics().p("EgmTAQqIAAmhIAMAAQBcAABmgOQA0gHB/gYQDxgrC6gCQChABD4AtQB8AYA/AIQBsAOBigBIAVgBIAMAAQBcAABlgOQA0gHB/gYQDygrC5gCQCfABD5AtQB8AYA/AIQBsAOBjgBIAUgBIALAAQBdAABlgOQA0gHCAgYQDygrC4gCQChABD5AtQB8AYA/AIQBrAOBigBIAWgBIAAGhg");
	var mask_graphics_3 = new cjs.Graphics().p("EgmTAQqIAAmhIAMAAQBcAABmgOQA0gHB/gYQDxgrC6gCQChABD4AtQB8AYBAAIQBqAOBjgBIAVgBIAMAAQBcAABlgOQA0gHB/gYQDygrC5gCQCfABD5AtQB8AYA/AIQBsAOBjgBIAUgBIALAAQBdAABlgOQA0gHCAgYQDygrC4gCQChABD5AtQB8AYA/AIQBrAOBigBIAWgBIAAGhg");
	var mask_graphics_4 = new cjs.Graphics().p("EgmTAQqIAAmhIAMAAQBcAABmgOQA0gHB/gYQDxgrC6gCQChABD4AtQB8AYBAAIQBqAOBjgBIAVgBIAMAAQBcAABlgOQA0gHB/gYQDygrC5gCQCfABD5AtQB8AYA/AIQBsAOBjgBIAUgBIAMAAQBcAABlgOQA0gHCAgYQDygrC4gCQChABD5AtQB8AYA/AIQBrAOBigBIAWgBIAAGhg");
	var mask_graphics_5 = new cjs.Graphics().p("EgmTAQqIAAmhIAMAAQBcAABmgOQA0gHB/gYQDxgrC6gCQChABD4AtQB8AYBAAIQBqAOBjgBIAVgBIAMAAQBcAABlgOQA0gHB/gYQDygrC5gCQCfABD5AtQB8AYA/AIQBsAOBigBIAVgBIAMAAQBcAABlgOQA0gHB/gYQDzgrC4gCQChABD5AtQB8AYA/AIQBrAOBjgBIAVgBIAAGhg");
	var mask_graphics_6 = new cjs.Graphics().p("EgmTAQqIAAmhIAMAAQBcAABmgOQA0gHB/gYQDxgrC6gCQChABD4AtQB8AYBAAIQBqAOBjgBIAVgBIALAAQBdAABlgOQA0gHB/gYQDygrC5gCQCfABD5AtQB8AYA/AIQBsAOBigBIAVgBIAMAAQBcAABlgOQA0gHB/gYQDzgrC4gCQChABD5AtQB8AYA/AIQBrAOBjgBIAVgBIAAGhg");
	var mask_graphics_7 = new cjs.Graphics().p("EgmTAQqIAAmhIAMAAQBcAABmgOQA0gHB/gYQDxgrC6gCQChABD4AtQB8AYBAAIQBrAOBigBIAVgBIALAAQBdAABlgOQA0gHB/gYQDygrC5gCQCfABD5AtQB8AYA/AIQBsAOBigBIAVgBIAMAAQBcAABlgOQA0gHB/gYQDzgrC4gCQChABD5AtQB8AYA/AIQBrAOBjgBIAVgBIAAGhg");
	var mask_graphics_8 = new cjs.Graphics().p("EgmTAQqIAAmhIAMAAQBcAABmgOQA0gHB/gYQDxgrC6gCQChABD4AtQB8AYBAAIQBrAOBigBIAVgBIALAAQBdAABlgOQA0gHB/gYQDygrC5gCQCfABD5AtQB9AYA+AIQBsAOBigBIAVgBIAMAAQBcAABlgOQA0gHB/gYQDzgrC4gCQChABD5AtQB8AYA/AIQBrAOBjgBIAVgBIAAGhg");
	var mask_graphics_9 = new cjs.Graphics().p("EgmTAQqIAAmhIAMAAQBcAABmgOQA0gHB/gYQDxgrC6gCQChABD4AtQB8AYBAAIQBrAOBigBIAVgBIALAAQBdAABlgOQA0gHB/gYQDygrC5gCQCfABD5AtQB9AYA+AIQBsAOBigBIAVgBIAMAAQBdAABkgOQA0gHB/gYQDzgrC5gCQCgABD5AtQB8AYA/AIQBrAOBjgBIAVgBIAAGhg");
	var mask_graphics_10 = new cjs.Graphics().p("EgmTAQqIAAmhIAMAAQBdAABlgOQA0gHB/gYQDxgrC6gCQChABD4AtQB8AYBAAIQBrAOBigBIAVgBIALAAQBdAABlgOQA0gHB/gYQDygrC5gCQCfABD5AtQB9AYA+AIQBsAOBigBIAVgBIAMAAQBdAABkgOQA0gHB/gYQDzgrC5gCQCgABD5AtQB8AYA/AIQBrAOBjgBIAVgBIAAGhg");
	var mask_graphics_11 = new cjs.Graphics().p("EgmTAQqIAAmhIAMAAQBdAABkgOQA1gHB/gYQDxgrC6gCQChABD4AtQB8AYBAAIQBrAOBigBIAVgBIALAAQBdAABlgOQA0gHB/gYQDygrC5gCQCfABD5AtQB9AYA+AIQBsAOBigBIAVgBIAMAAQBcAABlgOQA0gHB/gYQDygrC6gCQCgABD4AtQB9AYA/AIQBrAOBjgBIAVgBIAAGhg");
	var mask_graphics_12 = new cjs.Graphics().p("EgmTAQqIAAmhIAMAAQBdAABkgOQA1gHB/gYQDxgrC6gCQChABD4AtQB8AYBAAIQBrAOBigBIAVgBIALAAQBdAABlgOQA0gHB/gYQDygrC5gCQCgABD4AtQB9AYA+AIQBsAOBigBIAVgBIAMAAQBcAABlgOQA0gHB/gYQDygrC6gCQCgABD4AtQB9AYA/AIQBrAOBjgBIAUgBIAAGhg");
	var mask_graphics_13 = new cjs.Graphics().p("EgmTAQqIAAmhIAMAAQBdAABkgOQA1gHB/gYQDxgrC6gCQChABD4AtQB8AYBAAIQBrAOBigBIAVgBIALAAQBdAABlgOQA0gHB/gYQDygrC5gCQCgABD4AtQB9AYA+AIQBsAOBigBIAVgBIAMAAQBcAABlgOQA0gHB/gYQDygrC6gCQCgABD4AtQB9AYA/AIQBrAOBjgBIAUgBIAAGhg");
	var mask_graphics_14 = new cjs.Graphics().p("EgmTAQqIAAmhIAMAAQBdAABkgOQA0gHCAgYQDxgrC6gCQChABD4AtQB8AYBAAIQBrAOBigBIAVgBIALAAQBdAABlgOQA0gHB/gYQDygrC5gCQCfABD5AtQB9AYA+AIQBsAOBigBIAWgBIALAAQBcAABlgOQA0gHB/gYQDygrC6gCQCgABD4AtQB9AYA/AIQBrAOBjgBIAUgBIAAGhg");
	var mask_graphics_15 = new cjs.Graphics().p("EgmTAQqIAAmhIAMAAQBdAABkgOQA0gHCAgYQDxgrC6gCQChABD4AtQB8AYBAAIQBrAOBigBIAVgBIALAAQBdAABlgOQA0gHB/gYQDygrC5gCQCfABD5AtQB9AYA+AIQBsAOBigBIAWgBIALAAQBcAABlgOQA0gHB/gYQDygrC6gCQCgABD4AtQB9AYA/AIQBrAOBjgBIAUgBIAAGhg");
	var mask_graphics_16 = new cjs.Graphics().p("EgmTAQqIAAmhIAMAAQBdAABkgOQA0gHCAgYQDxgrC6gCQChABD4AtQB8AYBAAIQBrAOBigBIAVgBIALAAQBdAABlgOQA0gHB/gYQDygrC5gCQCfABD5AtQB9AYA+AIQBsAOBigBIAWgBIALAAQBcAABlgOQA0gHB/gYQDygrC6gCQCgABD4AtQB9AYA/AIQBrAOBjgBIAUgBIAAGhg");
	var mask_graphics_17 = new cjs.Graphics().p("EgmTAQqIAAmhIAMAAQBdAABkgOQA0gHCAgYQDxgrC6gCQChABD4AtQB8AYBAAIQBrAOBigBIAVgBIALAAQBdAABlgOQA0gHB/gYQDygrC5gCQCfABD5AtQB9AYA+AIQBrAOBjgBIAWgBIALAAQBcAABlgOQA0gHB/gYQDygrC6gCQCgABD4AtQB9AYA/AIQBrAOBjgBIAUgBIAAGhg");
	var mask_graphics_18 = new cjs.Graphics().p("EgmTAQqIAAmhIAMAAQBdAABkgOQA0gHCAgYQDxgrC6gCQChABD4AtQB8AYBAAIQBrAOBigBIAVgBIALAAQBdAABlgOQA0gHB/gYQDygrC5gCQCfABD5AtQB9AYA+AIQBrAOBjgBIAWgBIALAAQBcAABlgOQA0gHB/gYQDygrC6gCQCgABD4AtQB9AYA/AIQBrAOBjgBIAUgBIAAGhg");
	var mask_graphics_19 = new cjs.Graphics().p("EgmTAQqIAAmhIAMAAQBdAABkgOQA0gHCAgYQDygrC5gCQCgABD5AtQB8AYBAAIQBrAOBigBIAVgBIALAAQBdAABlgOQA0gHB/gYQDygrC5gCQCgABD4AtQB9AYA+AIQBrAOBjgBIAWgBIALAAQBcAABlgOQA0gHB/gYQDygrC6gCQCgABD4AtQB9AYA/AIQBrAOBjgBIAUgBIAAGhg");
	var mask_graphics_20 = new cjs.Graphics().p("EgmTAQqIAAmhIAMAAQBdAABkgOQA0gHCAgYQDygrC5gCQCgABD5AtQB8AYBAAIQBrAOBigBIAVgBIALAAQBdAABlgOQA0gHB/gYQDygrC5gCQCgABD4AtQB9AYA+AIQBrAOBjgBIAWgBIALAAQBcAABlgOQA0gHB/gYQDygrC6gCQCgABD4AtQB9AYA/AIQBrAOBjgBIAUgBIAAGhg");
	var mask_graphics_21 = new cjs.Graphics().p("EgmTAQqIAAmhIAMAAQBdAABkgOQA0gHCAgYQDygrC5gCQCgABD5AtQB8AYBAAIQBrAOBigBIAVgBIALAAQBdAABlgOQA0gHB/gYQDygrC5gCQCgABD4AtQB9AYA+AIQBrAOBjgBIAWgBIALAAQBcAABlgOQA0gHB/gYQDygrC6gCQCgABD4AtQB9AYA/AIQBrAOBjgBIAUgBIAAGhg");
	var mask_graphics_22 = new cjs.Graphics().p("EgmTAQqIAAmhIAMAAQBdAABkgOQA0gHCAgYQDxgrC6gCQCgABD5AtQB8AYBAAIQBrAOBigBIAVgBIALAAQBdAABlgOQA0gHB/gYQDygrC5gCQCgABD4AtQB9AYA+AIQBrAOBjgBIAWgBIALAAQBcAABlgOQA0gHB/gYQDygrC6gCQCgABD4AtQB9AYA/AIQBrAOBjgBIAUgBIAAGhg");
	var mask_graphics_23 = new cjs.Graphics().p("EgmTAQqIAAmhIAMAAQBdAABkgOQA0gHCAgYQDxgrC6gCQCgABD5AtQB8AYBAAIQBrAOBigBIAVgBIALAAQBdAABlgOQA0gHB/gYQDygrC5gCQCgABD4AtQB9AYA+AIQBrAOBjgBIAWgBIALAAQBcAABlgOQA0gHB/gYQDygrC6gCQCgABD4AtQB9AYA/AIQBrAOBjgBIAUgBIAAGhg");
	var mask_graphics_24 = new cjs.Graphics().p("EgmSAQqIAAmhIALAAQBdAABkgOQA0gHCAgYQDxgrC6gCQCgABD5AtQB8AYBAAIQBrAOBigBIAVgBIALAAQBdAABlgOQA0gHB/gYQDygrC5gCQCgABD4AtQB9AYA+AIQBrAOBjgBIAWgBIALAAQBcAABlgOQA0gHB/gYQDygrC6gCQCgABD4AtQB9AYA/AIQBrAOBjgBIAUgBIAAGhg");
	var mask_graphics_25 = new cjs.Graphics().p("EgmSAQqIAAmhIALAAQBdAABkgOQA0gHCAgYQDxgrC6gCQCgABD5AtQB8AYBAAIQBrAOBigBIAVgBIALAAQBdAABlgOQA0gHB/gYQDygrC5gCQCgABD4AtQB8AYBAAIQBqAOBjgBIAWgBIALAAQBdAABkgOQA0gHB/gYQDygrC6gCQCgABD4AtQB9AYA/AIQBrAOBjgBIAUgBIAAGhg");
	var mask_graphics_26 = new cjs.Graphics().p("EgmSAQqIAAmhIALAAQBdAABkgOQA0gHCAgYQDxgrC6gCQCgABD5AtQB8AYBAAIQBrAOBigBIAVgBIALAAQBdAABlgOQA0gHB/gYQDygrC5gCQCgABD4AtQB8AYBAAIQBqAOBjgBIAWgBIALAAQBdAABkgOQA0gHB/gYQDygrC6gCQCgABD4AtQB9AYA/AIQBrAOBjgBIAUgBIAAGhg");
	var mask_graphics_27 = new cjs.Graphics().p("EgmSAQqIAAmhIALAAQBdAABkgOQA0gHCAgYQDxgrC6gCQCgABD5AtQB8AYA/AIQBsAOBigBIAVgBIALAAQBdAABlgOQA0gHB/gYQDygrC5gCQCgABD4AtQB8AYBAAIQBqAOBjgBIAWgBIAKAAQBeAABkgOQA0gHB/gYQDygrC6gCQCgABD4AtQB9AYA/AIQBrAOBigBIAVgBIAAGhg");
	var mask_graphics_28 = new cjs.Graphics().p("EgmSAQqIAAmhIALAAQBdAABkgOQA0gHCAgYQDygrC5gCQCgABD5AtQB8AYA/AIQBsAOBigBIAVgBIALAAQBdAABlgOQA0gHB/gYQDygrC5gCQCgABD4AtQB8AYBAAIQBrAOBigBIAWgBIAKAAQBeAABkgOQA0gHB/gYQDygrC6gCQCgABD4AtQB9AYA/AIQBrAOBigBIAVgBIAAGhg");
	var mask_graphics_29 = new cjs.Graphics().p("EgmSAQqIAAmhIALAAQBdAABkgOQA0gHCAgYQDygrC5gCQCgABD5AtQB8AYA/AIQBsAOBigBIAVgBIALAAQBdAABlgOQA0gHB/gYQDygrC5gCQCgABD4AtQB8AYBAAIQBrAOBigBIAWgBIAKAAQBeAABkgOQA0gHB/gYQDygrC6gCQCgABD4AtQB9AYA/AIQBrAOBigBIAVgBIAAGhg");
	var mask_graphics_30 = new cjs.Graphics().p("EgmSAQqIAAmhIALAAQBdAABkgOQA0gHCAgYQDygrC5gCQCgABD5AtQB8AYA/AIQBsAOBigBIAVgBIAMAAQBcAABlgOQA0gHB/gYQDygrC5gCQCgABD4AtQB9AYA/AIQBqAOBkgBIAVgBIAKAAQBeAABkgOQA0gHCAgYQDxgrC6gCQCgABD4AtQB9AYA/AIQBrAOBigBIAVgBIAAGhg");
	var mask_graphics_31 = new cjs.Graphics().p("EgmSAQqIAAmhIALAAQBdAABkgOQA0gHCAgYQDygrC5gCQCgABD5AtQB8AYA/AIQBsAOBigBIAVgBIAMAAQBcAABlgOQA0gHB/gYQDygrC5gCQCgABD4AtQB9AYA/AIQBqAOBkgBIAVgBIAKAAQBeAABkgOQA0gHCAgYQDxgrC6gCQCgABD4AtQB9AYA/AIQBrAOBigBIAVgBIAAGhg");
	var mask_graphics_32 = new cjs.Graphics().p("EgmSAQqIAAmhIALAAQBdAABkgOQA0gHCAgYQDygrC5gCQCgABD5AtQB8AYA/AIQBsAOBigBIAVgBIAMAAQBcAABlgOQA0gHB/gYQDygrC5gCQCgABD4AtQB9AYA/AIQBqAOBkgBIAVgBIAKAAQBeAABkgOQA0gHCAgYQDxgrC5gCQChABD4AtQB9AYA/AIQBrAOBigBIAVgBIAAGhg");
	var mask_graphics_33 = new cjs.Graphics().p("EgmSAQqIAAmhIALAAQBcAABlgOQA0gHCAgYQDygrC5gCQCgABD5AtQB8AYA/AIQBrAOBjgBIAVgBIAMAAQBcAABlgOQA0gHB/gYQDygrC5gCQCgABD4AtQB9AYA/AIQBqAOBkgBIAVgBIAKAAQBeAABkgOQA0gHCAgYQDxgrC5gCQChABD4AtQB9AYA/AIQBrAOBigBIAVgBIAAGhg");
	var mask_graphics_34 = new cjs.Graphics().p("EgmSAQqIAAmhIALAAQBcAABlgOQA0gHCAgYQDygrC5gCQCgABD5AtQB8AYA/AIQBrAOBjgBIAVgBIAMAAQBcAABlgOQA0gHB/gYQDygrC5gCQCgABD4AtQB9AYA/AIQBqAOBkgBIAVgBIAKAAQBeAABkgOQA0gHCAgYQDygrC4gCQChABD4AtQB9AYA/AIQBrAOBigBIAVgBIAAGhg");
	var mask_graphics_35 = new cjs.Graphics().p("EgmSAQqIAAmhIALAAQBcAABmgOQAzgHCAgYQDygrC5gCQCgABD5AtQB8AYA/AIQBrAOBjgBIAVgBIAMAAQBcAABlgOQA0gHB/gYQDygrC5gCQCgABD4AtQB9AYA/AIQBqAOBkgBIAVgBIAKAAQBeAABkgOQA0gHCAgYQDxgrC5gCQChABD4AtQB9AYA/AIQBrAOBigBIAVgBIAAGhg");
	var mask_graphics_36 = new cjs.Graphics().p("EgmSAQqIAAmhIALAAQBcAABmgOQAzgHCAgYQDygrC5gCQCgABD5AtQB8AYA/AIQBrAOBjgBIAVgBIAMAAQBcAABlgOQA0gHB/gYQDygrC5gCQCgABD4AtQB9AYA/AIQBqAOBkgBIAVgBIAKAAQBeAABkgOQA0gHCAgYQDxgrC5gCQChABD5AtQB8AYA/AIQBrAOBigBIAVgBIAAGhg");
	var mask_graphics_37 = new cjs.Graphics().p("EgmSAQqIAAmhIALAAQBcAABmgOQAzgHCAgYQDygrC5gCQCgABD5AtQB8AYA/AIQBrAOBjgBIAVgBIAMAAQBcAABlgOQA0gHB/gYQDygrC5gCQCgABD4AtQB9AYA/AIQBqAOBkgBIAVgBIAKAAQBeAABkgOQA0gHCAgYQDxgrC5gCQChABD5AtQB8AYA/AIQBrAOBigBIAWgBIAAGhg");
	var mask_graphics_38 = new cjs.Graphics().p("EgmSAQqIAAmhIALAAQBcAABmgOQAzgHCAgYQDygrC5gCQCgABD5AtQB8AYA/AIQBrAOBjgBIAVgBIAMAAQBcAABlgOQA0gHB/gYQDygrC5gCQCgABD4AtQB9AYA/AIQBqAOBkgBIAUgBIALAAQBeAABkgOQA0gHCAgYQDxgrC5gCQChABD5AtQB8AYA/AIQBrAOBigBIAWgBIAAGhg");
	var mask_graphics_39 = new cjs.Graphics().p("EgmSAQqIAAmhIALAAQBcAABmgOQAzgHCAgYQDygrC5gCQChABD4AtQB8AYA/AIQBrAOBjgBIAVgBIAMAAQBcAABlgOQA0gHB/gYQDygrC5gCQCgABD4AtQB9AYA/AIQBqAOBkgBIAUgBIALAAQBeAABkgOQA0gHCAgYQDxgrC5gCQChABD5AtQB8AYA/AIQBrAOBigBIAWgBIAAGhg");
	var mask_graphics_40 = new cjs.Graphics().p("EgmSAQqIAAmhIALAAQBcAABmgOQAzgHCAgYQDygrC5gCQCgABD5AtQB8AYA/AIQBrAOBjgBIAVgBIAMAAQBcAABlgOQA0gHB/gYQDygrC5gCQCgABD4AtQB9AYA/AIQBqAOBkgBIAUgBIALAAQBeAABkgOQA0gHCAgYQDxgrC5gCQChABD5AtQB8AYA/AIQBrAOBigBIAWgBIAAGhg");
	var mask_graphics_41 = new cjs.Graphics().p("EgmSAQqIAAmhIALAAQBcAABmgOQAzgHCAgYQDygrC5gCQCgABD5AtQB8AYA/AIQBrAOBjgBIAVgBIAMAAQBcAABlgOQA0gHB/gYQDygrC5gCQCfABD5AtQB8AYBAAIQBqAOBkgBIAUgBIALAAQBeAABkgOQA0gHCAgYQDxgrC5gCQChABD5AtQB8AYA/AIQBrAOBigBIAWgBIAAGhg");
	var mask_graphics_42 = new cjs.Graphics().p("EgmSAQqIAAmhIALAAQBdAABlgOQAzgHCAgYQDygrC5gCQCgABD5AtQB8AYA/AIQBrAOBjgBIAVgBIAMAAQBcAABlgOQA0gHB/gYQDygrC5gCQCfABD5AtQB8AYBAAIQBqAOBkgBIAUgBIALAAQBeAABkgOQA0gHCAgYQDxgrC5gCQChABD5AtQB8AYA/AIQBrAOBigBIAWgBIAAGhg");
	var mask_graphics_43 = new cjs.Graphics().p("EgmSAQqIAAmhIALAAQBdAABlgOQA0gHB/gYQDygrC5gCQCgABD5AtQB8AYA/AIQBrAOBjgBIAVgBIAMAAQBcAABlgOQA0gHB/gYQDygrC5gCQCfABD5AtQB8AYBAAIQBqAOBkgBIAUgBIALAAQBeAABkgOQA0gHCAgYQDygrC4gCQChABD5AtQB8AYA/AIQBrAOBigBIAWgBIAAGhg");
	var mask_graphics_44 = new cjs.Graphics().p("EgmSAQqIAAmhIALAAQBdAABlgOQA0gHB/gYQDygrC5gCQCgABD5AtQB8AYA/AIQBrAOBjgBIAVgBIAMAAQBcAABlgOQA0gHB/gYQDygrC5gCQCfABD5AtQB8AYBAAIQBqAOBkgBIAUgBIALAAQBeAABkgOQA0gHCAgYQDygrC4gCQChABD5AtQB8AYA/AIQBrAOBigBIAWgBIAAGhg");
	var mask_graphics_45 = new cjs.Graphics().p("EgmSAQqIAAmhIALAAQBcAABmgOQA0gHB/gYQDygrC5gCQCgABD5AtQB8AYA/AIQBrAOBjgBIAVgBIAMAAQBcAABlgOQA0gHB/gYQDygrC5gCQCfABD5AtQB8AYBAAIQBqAOBkgBIAUgBIALAAQBeAABkgOQA0gHCAgYQDygrC4gCQChABD5AtQB8AYA/AIQBrAOBigBIAWgBIAAGhg");
	var mask_graphics_46 = new cjs.Graphics().p("EgmSAQqIAAmhIALAAQBcAABmgOQA0gHB/gYQDxgrC6gCQChABD4AtQB8AYA/AIQBrAOBjgBIAVgBIAMAAQBcAABlgOQA0gHB/gYQDygrC5gCQCfABD5AtQB8AYBAAIQBrAOBjgBIAUgBIALAAQBeAABkgOQA0gHCAgYQDygrC4gCQChABD5AtQB8AYA/AIQBrAOBigBIAWgBIAAGhg");
	var mask_graphics_47 = new cjs.Graphics().p("EgmSAQqIAAmhIALAAQBcAABmgOQA0gHB/gYQDxgrC6gCQChABD4AtQB8AYA/AIQBrAOBjgBIAVgBIAMAAQBcAABlgOQA0gHB/gYQDygrC5gCQCfABD5AtQB8AYBAAIQBrAOBjgBIAUgBIALAAQBdAABlgOQA0gHCAgYQDygrC4gCQChABD5AtQB8AYA/AIQBrAOBigBIAWgBIAAGhg");
	var mask_graphics_48 = new cjs.Graphics().p("EgmTAQqIAAmhIAMAAQBcAABmgOQA0gHB/gYQDxgrC6gCQChABD4AtQB8AYA/AIQBrAOBjgBIAVgBIAMAAQBcAABlgOQA0gHB/gYQDygrC5gCQCfABD5AtQB8AYBAAIQBrAOBjgBIAUgBIALAAQBdAABlgOQA0gHCAgYQDygrC4gCQChABD5AtQB8AYA/AIQBrAOBigBIAWgBIAAGhg");
	var mask_graphics_49 = new cjs.Graphics().p("EgmTAQqIAAmhIAMAAQBcAABmgOQA0gHB/gYQDxgrC6gCQChABD4AtQB8AYA/AIQBrAOBjgBIAVgBIAMAAQBcAABlgOQA0gHB/gYQDygrC5gCQCfABD5AtQB8AYA/AIQBsAOBjgBIAUgBIALAAQBdAABlgOQA0gHCAgYQDygrC4gCQChABD5AtQB8AYA/AIQBrAOBigBIAWgBIAAGhg");
	var mask_graphics_50 = new cjs.Graphics().p("EgmTAQqIAAmhIAMAAQBcAABmgOQA0gHB/gYQDxgrC6gCQChABD4AtQB8AYA/AIQBsAOBigBIAVgBIAMAAQBcAABlgOQA0gHB/gYQDygrC5gCQCfABD5AtQB8AYA/AIQBsAOBjgBIAUgBIALAAQBdAABlgOQA0gHCAgYQDygrC4gCQChABD5AtQB8AYA/AIQBrAOBigBIAWgBIAAGhg");
	var mask_graphics_51 = new cjs.Graphics().p("EgmTAQqIAAmhIAMAAQBcAABmgOQA0gHB/gYQDxgrC6gCQChABD4AtQB8AYBAAIQBqAOBjgBIAVgBIAMAAQBcAABlgOQA0gHB/gYQDygrC5gCQCfABD5AtQB8AYA/AIQBsAOBjgBIAUgBIAMAAQBcAABlgOQA0gHCAgYQDygrC4gCQChABD5AtQB8AYA/AIQBrAOBigBIAWgBIAAGhg");
	var mask_graphics_52 = new cjs.Graphics().p("EgmTAQqIAAmhIAMAAQBcAABmgOQA0gHB/gYQDxgrC6gCQChABD4AtQB8AYBAAIQBqAOBjgBIAVgBIAMAAQBcAABlgOQA0gHB/gYQDygrC5gCQCfABD5AtQB8AYA/AIQBsAOBjgBIAUgBIAMAAQBcAABlgOQA0gHCAgYQDygrC4gCQChABD5AtQB8AYA/AIQBrAOBjgBIAVgBIAAGhg");
	var mask_graphics_53 = new cjs.Graphics().p("EgmTAQqIAAmhIAMAAQBcAABmgOQA0gHB/gYQDxgrC6gCQChABD4AtQB8AYBAAIQBqAOBjgBIAVgBIAMAAQBcAABlgOQA0gHB/gYQDygrC5gCQCfABD5AtQB8AYA/AIQBsAOBjgBIAUgBIAMAAQBcAABlgOQA0gHCAgYQDygrC4gCQChABD5AtQB8AYA/AIQBrAOBjgBIAVgBIAAGhg");
	var mask_graphics_54 = new cjs.Graphics().p("EgmTAQqIAAmhIAMAAQBcAABmgOQA0gHB/gYQDxgrC6gCQChABD4AtQB8AYBAAIQBqAOBjgBIAVgBIALAAQBdAABlgOQA0gHB/gYQDygrC5gCQCfABD5AtQB8AYA/AIQBsAOBigBIAVgBIAMAAQBcAABlgOQA0gHB/gYQDzgrC4gCQChABD5AtQB8AYA/AIQBrAOBjgBIAVgBIAAGhg");
	var mask_graphics_55 = new cjs.Graphics().p("EgmTAQqIAAmhIAMAAQBcAABmgOQA0gHB/gYQDxgrC6gCQChABD4AtQB8AYBAAIQBrAOBigBIAVgBIALAAQBdAABlgOQA0gHB/gYQDygrC5gCQCfABD5AtQB8AYA/AIQBsAOBigBIAVgBIAMAAQBcAABlgOQA0gHB/gYQDzgrC4gCQChABD5AtQB8AYA/AIQBrAOBjgBIAVgBIAAGhg");
	var mask_graphics_56 = new cjs.Graphics().p("EgmTAQqIAAmhIAMAAQBcAABmgOQA0gHB/gYQDxgrC6gCQChABD4AtQB8AYBAAIQBrAOBigBIAVgBIALAAQBdAABlgOQA0gHB/gYQDygrC5gCQCfABD5AtQB8AYA/AIQBsAOBigBIAVgBIAMAAQBcAABlgOQA0gHB/gYQDzgrC4gCQChABD5AtQB8AYA/AIQBrAOBjgBIAVgBIAAGhg");
	var mask_graphics_57 = new cjs.Graphics().p("EgmTAQqIAAmhIAMAAQBcAABmgOQA0gHB/gYQDxgrC6gCQChABD4AtQB8AYBAAIQBrAOBigBIAVgBIALAAQBdAABlgOQA0gHB/gYQDygrC5gCQCfABD5AtQB9AYA+AIQBsAOBigBIAVgBIAMAAQBdAABkgOQA0gHB/gYQDzgrC5gCQCgABD5AtQB8AYA/AIQBrAOBjgBIAVgBIAAGhg");
	var mask_graphics_58 = new cjs.Graphics().p("EgmTAQqIAAmhIAMAAQBcAABmgOQA0gHB/gYQDxgrC6gCQChABD4AtQB8AYBAAIQBrAOBigBIAVgBIALAAQBdAABlgOQA0gHB/gYQDygrC5gCQCfABD5AtQB9AYA+AIQBsAOBigBIAVgBIAMAAQBdAABkgOQA0gHB/gYQDzgrC5gCQCgABD5AtQB8AYA/AIQBrAOBjgBIAVgBIAAGhg");
	var mask_graphics_59 = new cjs.Graphics().p("EgmTAQqIAAmhIAMAAQBdAABkgOQA1gHB/gYQDxgrC6gCQChABD4AtQB8AYBAAIQBrAOBigBIAVgBIALAAQBdAABlgOQA0gHB/gYQDygrC5gCQCfABD5AtQB9AYA+AIQBsAOBigBIAVgBIAMAAQBcAABlgOQA0gHB/gYQDzgrC5gCQCgABD5AtQB8AYA/AIQBrAOBjgBIAUgBIAAGhg");

	this.timeline.addTween(cjs.Tween.get(mask).to({graphics:mask_graphics_0,x:179.9,y:106.6}).wait(1).to({graphics:mask_graphics_1,x:177.1271,y:106.6}).wait(1).to({graphics:mask_graphics_2,x:174.3542,y:106.6}).wait(1).to({graphics:mask_graphics_3,x:171.5814,y:106.6}).wait(1).to({graphics:mask_graphics_4,x:168.8085,y:106.6}).wait(1).to({graphics:mask_graphics_5,x:166.0356,y:106.6}).wait(1).to({graphics:mask_graphics_6,x:163.2627,y:106.6}).wait(1).to({graphics:mask_graphics_7,x:160.4898,y:106.6}).wait(1).to({graphics:mask_graphics_8,x:157.717,y:106.6}).wait(1).to({graphics:mask_graphics_9,x:154.9441,y:106.6}).wait(1).to({graphics:mask_graphics_10,x:152.1712,y:106.6}).wait(1).to({graphics:mask_graphics_11,x:149.3983,y:106.6}).wait(1).to({graphics:mask_graphics_12,x:146.6254,y:106.6}).wait(1).to({graphics:mask_graphics_13,x:143.8525,y:106.6}).wait(1).to({graphics:mask_graphics_14,x:141.0797,y:106.6}).wait(1).to({graphics:mask_graphics_15,x:138.3068,y:106.6}).wait(1).to({graphics:mask_graphics_16,x:135.5339,y:106.6}).wait(1).to({graphics:mask_graphics_17,x:132.761,y:106.6}).wait(1).to({graphics:mask_graphics_18,x:129.9881,y:106.6}).wait(1).to({graphics:mask_graphics_19,x:127.2153,y:106.6}).wait(1).to({graphics:mask_graphics_20,x:124.4424,y:106.6}).wait(1).to({graphics:mask_graphics_21,x:121.6695,y:106.6}).wait(1).to({graphics:mask_graphics_22,x:118.8966,y:106.6}).wait(1).to({graphics:mask_graphics_23,x:116.1237,y:106.6}).wait(1).to({graphics:mask_graphics_24,x:113.3509,y:106.6}).wait(1).to({graphics:mask_graphics_25,x:110.578,y:106.6}).wait(1).to({graphics:mask_graphics_26,x:107.8051,y:106.6}).wait(1).to({graphics:mask_graphics_27,x:105.0322,y:106.6}).wait(1).to({graphics:mask_graphics_28,x:102.2593,y:106.6}).wait(1).to({graphics:mask_graphics_29,x:99.4864,y:106.6}).wait(1).to({graphics:mask_graphics_30,x:96.7136,y:106.6}).wait(1).to({graphics:mask_graphics_31,x:93.9407,y:106.6}).wait(1).to({graphics:mask_graphics_32,x:91.1678,y:106.6}).wait(1).to({graphics:mask_graphics_33,x:88.3949,y:106.6}).wait(1).to({graphics:mask_graphics_34,x:85.622,y:106.6}).wait(1).to({graphics:mask_graphics_35,x:82.8492,y:106.6}).wait(1).to({graphics:mask_graphics_36,x:80.0763,y:106.6}).wait(1).to({graphics:mask_graphics_37,x:77.3034,y:106.6}).wait(1).to({graphics:mask_graphics_38,x:74.5305,y:106.6}).wait(1).to({graphics:mask_graphics_39,x:71.7576,y:106.6}).wait(1).to({graphics:mask_graphics_40,x:68.9847,y:106.6}).wait(1).to({graphics:mask_graphics_41,x:66.2119,y:106.6}).wait(1).to({graphics:mask_graphics_42,x:63.439,y:106.6}).wait(1).to({graphics:mask_graphics_43,x:60.6661,y:106.6}).wait(1).to({graphics:mask_graphics_44,x:57.8932,y:106.6}).wait(1).to({graphics:mask_graphics_45,x:55.1203,y:106.6}).wait(1).to({graphics:mask_graphics_46,x:52.3475,y:106.6}).wait(1).to({graphics:mask_graphics_47,x:49.5746,y:106.6}).wait(1).to({graphics:mask_graphics_48,x:46.8017,y:106.6}).wait(1).to({graphics:mask_graphics_49,x:44.0288,y:106.6}).wait(1).to({graphics:mask_graphics_50,x:41.2559,y:106.6}).wait(1).to({graphics:mask_graphics_51,x:38.4831,y:106.6}).wait(1).to({graphics:mask_graphics_52,x:35.7102,y:106.6}).wait(1).to({graphics:mask_graphics_53,x:32.9373,y:106.6}).wait(1).to({graphics:mask_graphics_54,x:30.1644,y:106.6}).wait(1).to({graphics:mask_graphics_55,x:27.3915,y:106.6}).wait(1).to({graphics:mask_graphics_56,x:24.6186,y:106.6}).wait(1).to({graphics:mask_graphics_57,x:21.8458,y:106.6}).wait(1).to({graphics:mask_graphics_58,x:19.0729,y:106.6}).wait(1).to({graphics:mask_graphics_59,x:16.3,y:106.6}).wait(1));

	// Layer_2
	this.instance = new lib.milk_part();
	this.instance.parent = this;
	this.instance.setTransform(-48,159);

	var maskedShapeInstanceList = [this.instance];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(60));

	// bg_png
	this.instance_1 = new lib.bg();
	this.instance_1.parent = this;
	this.instance_1.setTransform(-540,-340);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(60));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-540,-340,1080,680);


(lib.Symbol5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_3 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("Axdv/ITMAAII7jeMAIoAkBMgmjAC6g");
	mask.setTransform(14.825,92.825);

	// pack_png
	this.instance = new lib.pack();
	this.instance.parent = this;
	this.instance.setTransform(-167,-231);

	var maskedShapeInstanceList = [this.instance];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol5, new cjs.Rectangle(-108.6,-31.8,246.9,249.3), null);


(lib.Symbol3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_3
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(11.5,1,1).p("AAlAxQAQg2hbgr");
	this.shape.setTransform(3.1679,-11.8);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	// Layer_2
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(11.5,1,1).p("AAIhvQAHBVgYCK");
	this.shape_1.setTransform(7.7166,3.375);

	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(1));

	// Layer_1
	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(11.5,1,1).p("ABZhWQg0Bjh9BK");
	this.shape_2.setTransform(0.025,0);

	this.timeline.addTween(cjs.Tween.get(this.shape_2).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol3, new cjs.Rectangle(-14.6,-22.4,29.299999999999997,42.8), null);


(lib.Symbol2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_3
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(11.5,1,1).p("AAzg6QglBBhAA0");
	this.shape.setTransform(-0.525,17.2);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	// Layer_2
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(11.5,1,1).p("AgcBnQBWhtgthg");
	this.shape_1.setTransform(7.8056,1.375);

	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(1));

	// Layer_1
	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(11.5,1,1).p("AAjB0QAlithzg6");
	this.shape_2.setTransform(0.0239,0.025);

	this.timeline.addTween(cjs.Tween.get(this.shape_2).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol2, new cjs.Rectangle(-11.3,-17.3,27.7,46.2), null);


(lib.Symbol_8_Symbol_7 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Symbol_7
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(11.5,1,1).p("AF2HYQkRgXitjkQihjTiMnh");
	this.shape.setTransform(-121.775,-78.825);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(11.5,1,1).p("AmXmfQC+HaC5CwQDDC9D1gI");
	this.shape_1.setTransform(-125.45,-72.4568);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(11.5,1,1).p("Am2luQDsHUDPCOQDYCZDagl");
	this.shape_2.setTransform(-128.875,-66.1819);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(11.5,1,1).p("AnTlFQEXHODjBwQDrB3DChA");
	this.shape_3.setTransform(-132.05,-59.9695);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(11.5,1,1).p("AnukkQE9HKD3BTQD9BYCrhZ");
	this.shape_4.setTransform(-134.95,-53.8187);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(11.5,1,1).p("AoGkJQFhHFEIA5QENA8CXhv");
	this.shape_5.setTransform(-137.6,-47.836);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(11.5,1,1).p("Aocj0QGCHAEYAiQEaAjCFiE");
	this.shape_6.setTransform(-140.025,-42.0749);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(11.5,1,1).p("AowjlQGfG9EmANQEnAMB1iW");
	this.shape_7.setTransform(-142.175,-36.6263);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(11.5,1,1).p("ApBi/QG0FzEgAMQEhALCOiF");
	this.shape_8.setTransform(-144.075,-34.1542);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(11.5,1,1).p("ApQifQHGE0EbAKQEbALClh2");
	this.shape_9.setTransform(-145.725,-32.0283);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(11.5,1,1).p("ApdiEQHWD+EWAKQEXAKC4hp");
	this.shape_10.setTransform(-147.15,-30.2067);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(11.5,1,1).p("ApohuQHjDSETAKQETAJDIhf");
	this.shape_11.setTransform(-148.25,-28.7318);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(11.5,1,1).p("ApvhcQHsCvEQAJQEQAJDUhX");
	this.shape_12.setTransform(-149.15,-27.5828);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(11.5,1,1).p("Ap1hQQH0CXENAJQEOAJDdhR");
	this.shape_13.setTransform(-149.8,-26.7596);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(11.5,1,1).p("Ap5hIQH4CIEMAJQENAIDihO");
	this.shape_14.setTransform(-150.175,-26.2582);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(11.5,1,1).p("AJ7ACQjjBNkNgJQkMgJn5iD");
	this.shape_15.setTransform(-150.3,-26.1059);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(11.5,1,1).p("Ap5hJQH2CJENAJQEOAJDihO");
	this.shape_16.setTransform(-150.15,-26.3782);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(11.5,1,1).p("Ap0hRQHqCXEQALQERAKDehQ");
	this.shape_17.setTransform(-149.65,-27.1641);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#000000").ss(11.5,1,1).p("ApsheQHWCvEWANQEWANDYhU");
	this.shape_18.setTransform(-148.8,-28.5124);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#000000").ss(11.5,1,1).p("ApihxQG7DREdAQQEeAQDPhZ");
	this.shape_19.setTransform(-147.625,-30.371);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#000000").ss(11.5,1,1).p("ApUiKQGYD9EnAUQEnAVDDhg");
	this.shape_20.setTransform(-146.15,-32.7615);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#000000").ss(11.5,1,1).p("ApEioQFuEyEzAaQEzAaC1ho");
	this.shape_21.setTransform(-144.35,-35.682);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#000000").ss(11.5,1,1).p("AowjMQE7FxFBAhQFBAgCkhy");
	this.shape_22.setTransform(-142.175,-39.1141);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#000000").ss(11.5,1,1).p("An3kOQEGGTEPBXQEVBcDFhI");
	this.shape_23.setTransform(-135.925,-52.7572);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#000000").ss(11.5,1,1).p("AnIlQQDaGvDpCDQDuCODggn");
	this.shape_24.setTransform(-130.85,-62.7633);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f().s("#000000").ss(11.5,1,1).p("AmkmJQC4HEDJCnQDSCzD2gL");
	this.shape_25.setTransform(-126.875,-69.9654);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f().s("#000000").ss(11.5,1,1).p("AmKm0QCgHUCzC/QC9DPEFAH");
	this.shape_26.setTransform(-124.05,-74.9);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f().s("#000000").ss(11.5,1,1).p("Al7nOQCSHeCmDOQCxDfEOAS");
	this.shape_27.setTransform(-122.35,-77.85);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},17).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_15}]},32).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_27}]},1).to({state:[{t:this.shape}]},1).wait(8));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_8_Symbol_6 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Symbol_6
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(11.5,1,1).p("AkOoOQEwC0CHCSQCECOgQB9QgPB3iXB3QiFBqkPB0");
	this.shape.setTransform(91.3917,5.575);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(11.5,1,1).p("AksIUQEShpCNhjQCbhtAZh3QAbh8hyiZQh2ickfjG");
	this.shape_1.setTransform(83.5576,6.45);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(11.5,1,1).p("Ak7IZQEVhgCThaQCghjAkh3QAmh8hiiiQhkinkOjY");
	this.shape_2.setTransform(75.8708,7.35);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(11.5,1,1).p("AlLIdQEXhVCbhTQCkhYAvh4QAwh6hQitQhSiwj9jr");
	this.shape_3.setTransform(68.2848,8.25);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(11.5,1,1).p("AlcIiQEahLCihKQCohPA6h4QA6h5g/i4QhAi6jtj8");
	this.shape_4.setTransform(60.7901,9.125);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(11.5,1,1).p("AlvInQEchBCphDQCthFBFh3QBEh5gujCQgujEjckO");
	this.shape_5.setTransform(53.499,10.025);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(11.5,1,1).p("ACWorQDLEhAdDOQAcDMhPB4QhPB4ixA6QiwA7kgA3");
	this.shape_6.setTransform(46.3517,10.9);
	this.shape_6._off = true;

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(11.5,1,1).p("Al6InQEagnCuhAQCuhBBMh6QBLh6gjjLQgkjNjVkZ");
	this.shape_7.setTransform(49.4068,10.425);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(11.5,1,1).p("AlwIjQEVgXCrhGQCthHBHh8QBHh8gqjKQgqjMjekT");
	this.shape_8.setTransform(52.5055,9.9);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(11.5,1,1).p("AA6odQDnEMAxDKQAxDIhDB/QhDB/iqBNQiqBLkPAH");
	this.shape_9.setTransform(55.6183,9.425);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(11.5,1,1).p("AlwICQEJALCwhBQCxhCBKh3QBKh4gwjIQgxjLjokL");
	this.shape_10.setTransform(54.4601,6.8546);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(11.5,1,1).p("Al6HjQECAdC3g2QC4g4BShwQBRhxgxjIQgxjKjnkM");
	this.shape_11.setTransform(53.3332,4.6036);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(11.5,1,1).p("ABcnZQDoEMAxDKQAwDIhYBqQhYBpjAAsQi+Asj8gu");
	this.shape_12.setTransform(52.1837,2.6374);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(11.5,1,1).p("AmQHZQEUAgC7gsQC8gtBYhsQBYhsgljKQgljMjXkY");
	this.shape_13.setTransform(47.5726,3.9237);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(11.5,1,1).p("AmcHwQEsASC4gtQC4guBYhuQBYhvgZjMQgZjOjGkk");
	this.shape_14.setTransform(43.0265,5.295);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(11.5,1,1).p("ADloEQC2EwANDQQANDNhYByQhYBxi0AuQi1AtlEgD");
	this.shape_15.setTransform(38.5398,6.8594);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(11.5,1,1).p("AmcISQE4gQCzgyQCzgzBVhzQBVh0gSjNQgSjPi9kr");
	this.shape_16.setTransform(41.1153,8.2);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(11.5,1,1).p("AmQIfQEsgkCxg2QCyg2BTh2QBSh1gYjNQgXjPjEkm");
	this.shape_17.setTransform(43.7367,9.55);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#000000").ss(11.5,1,1).p("AlgIjQEbhJCjhKQCphNA8h3QA7h6g8i5Qg9i8jqj/");
	this.shape_18.setTransform(59.5563,9.275);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#000000").ss(11.5,1,1).p("AlFIcQEWhZCYhVQCjhdArh3QAth7hWipQhYitkDjk");
	this.shape_19.setTransform(70.7837,7.95);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#000000").ss(11.5,1,1).p("AkzIXQEThlCQheQCdhoAfh3QAhh8hrieQhsihkXjQ");
	this.shape_20.setTransform(79.7049,6.9);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#000000").ss(11.5,1,1).p("AknITQERhuCKhlQCahwAWh3QAXh8h4iWQh8iYkkjB");
	this.shape_21.setTransform(86.1668,6.15);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#000000").ss(11.5,1,1).p("AkgIQQEQhyCHhpQCXh2ARh2QASh9iBiQQiFiUkti3");
	this.shape_22.setTransform(90.0852,5.725);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},32).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_6}]},2).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_6}]},13).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape}]},1).wait(2));
	this.timeline.addTween(cjs.Tween.get(this.shape_6).wait(38).to({_off:false},0).wait(2).to({_off:true},1).wait(11).to({_off:false},0).to({_off:true},1).wait(11).to({_off:false},0).wait(13).to({_off:true},1).wait(7));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_8_pack_png = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// pack_png
	this.instance = new lib.pack();
	this.instance.parent = this;
	this.instance.setTransform(-161.9,-220.8);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(85));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_1_Layer_3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_3
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(11.5,1,1).p("AiPgaQARAmAxAXQAuAWAzgEQAzgEAjgdQAlgeABgr");
	this.shape.setTransform(42.65,37.1058);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(11.5,1,1).p("AiWgXQAZAmAxAVQAtAUAygFQA0gFAjgbQAmgdAHgp");
	this.shape_1.setTransform(42.575,36.825);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(11.5,1,1).p("AiegVQAhAmAwAUQAtASAzgGQA0gGAlgaQAmgbANgo");
	this.shape_2.setTransform(42.5,36.5735);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(11.5,1,1).p("AikgTQAoAnAwASQAtAQAygGQA0gHAlgZQAngaASgm");
	this.shape_3.setTransform(42.425,36.3418);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(11.5,1,1).p("AiqgRQAvAnAvAQQAsAPAzgHQA0gIAmgYQAogYAWgl");
	this.shape_4.setTransform(42.375,36.1473);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(11.5,1,1).p("AiwgPQA2AnAuAOQAsAOAzgIQA0gJAngWQAogXAbgk");
	this.shape_5.setTransform(42.3,35.9356);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(11.5,1,1).p("Ai1gOQA7AnAuAOQAsAMAzgJQAzgJAogWQApgWAfgi");
	this.shape_6.setTransform(42.25,35.7762);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(11.5,1,1).p("Ai5gMQBAAnAtAMQAsALAzgKQA0gJAogVQApgVAjgh");
	this.shape_7.setTransform(42.2,35.6076);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(11.5,1,1).p("Ai+gLQBFAnAtALQAsAKAygKQA0gKApgUQAqgUAlgg");
	this.shape_8.setTransform(42.15,35.475);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(11.5,1,1).p("AjBgKQBIAnAtAKQAsAJAzgKQAzgLAqgUQAqgSAogg");
	this.shape_9.setTransform(42.125,35.3577);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(11.5,1,1).p("AjFgJQBNAnAsAJQAsAJAzgMQA0gLApgTQArgSArge");
	this.shape_10.setTransform(42.075,35.2556);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(11.5,1,1).p("AjIgIQBQAnAsAIQArAIAzgLQA0gMArgSQAqgSAuge");
	this.shape_11.setTransform(42.075,35.152);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(11.5,1,1).p("AjKgIQBSAoAsAHQArAHAzgMQA0gMArgRQArgRAvgd");
	this.shape_12.setTransform(42.025,35.1039);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(11.5,1,1).p("AjMgHQBUAoAsAHQArAGAzgMQA0gMArgSQArgQAxgd");
	this.shape_13.setTransform(42.025,35.0392);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(11.5,1,1).p("AjOgHQBXAoArAHQAsAFAygLQA0gOAsgQQArgRAygb");
	this.shape_14.setTransform(41.975,35);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(11.5,1,1).p("AjPgGQBYAoArAGQArAGAzgNQA0gNAsgQQArgQAzgc");
	this.shape_15.setTransform(41.975,34.9473);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(11.5,1,1).p("AjQgGQBZAoArAGQArAFAzgMQA0gNAsgRQArgPA0gc");
	this.shape_16.setTransform(41.975,34.934);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(11.5,1,1).p("AjFgJQBNAoAsAJQAsAIAzgLQAzgMArgTQAqgRArgf");
	this.shape_17.setTransform(42.075,35.2434);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#000000").ss(11.5,1,1).p("Ai8gLQBDAnAtALQAsALAzgKQA0gLAogUQAqgUAkgg");
	this.shape_18.setTransform(42.2,35.5439);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#000000").ss(11.5,1,1).p("AizgOQA5AnAuANQAsANAzgJQA0gJAngWQApgWAdgi");
	this.shape_19.setTransform(42.275,35.8091);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#000000").ss(11.5,1,1).p("AisgQQAxAmAvAQQAsAPAzgIQAzgIAngYQAogXAYgl");
	this.shape_20.setTransform(42.35,36.0641);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#000000").ss(11.5,1,1).p("AilgTQApAnAwARQAsARA0gHQAzgHAmgZQAngZASgm");
	this.shape_21.setTransform(42.4,36.3043);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#000000").ss(11.5,1,1).p("AifgUQAjAmAwATQAtASAygGQA0gHAlgZQAmgbAOgn");
	this.shape_22.setTransform(42.475,36.525);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#000000").ss(11.5,1,1).p("AiagWQAdAmAxAVQAtATAygGQA0gFAkgbQAmgcAKgo");
	this.shape_23.setTransform(42.525,36.6867);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#000000").ss(11.5,1,1).p("AiWgXQAZAmAxAVQAtAUAygEQA0gFAjgcQAmgdAHgp");
	this.shape_24.setTransform(42.575,36.8413);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f().s("#000000").ss(11.5,1,1).p("AiTgYQAVAlAxAXQAuAVAygFQA0gEAjgdQAmgdAEgq");
	this.shape_25.setTransform(42.6,36.9647);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f().s("#000000").ss(11.5,1,1).p("AiRgZQATAmAyAXQAtAVAzgEQAzgEAjgdQAlgeADgr");
	this.shape_26.setTransform(42.625,37.0314);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f().s("#000000").ss(11.5,1,1).p("AiQgZQASAlAxAYQAuAWAzgFQAzgEAjgdQAlgeACgr");
	this.shape_27.setTransform(42.65,37.0882);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},32).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16,p:{y:34.934}}]},1).to({state:[{t:this.shape_16,p:{y:34.9239}}]},1).to({state:[{t:this.shape_16,p:{y:34.9239}}]},18).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_27}]},1).to({state:[{t:this.shape}]},1).wait(6));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_1_Layer_2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(11.5,1,1).p("AithGQAFBMA8AuQA3ApBFgBQBHgCAsgtQAwgygFhW");
	this.shape.setTransform(65.578,0.1026);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(11.5,1,1).p("AithAQAFBMA9AoQA5AjBFgCQBGgBArgoQAvgsgFhV");
	this.shape_1.setTransform(65.5788,-0.5447);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(11.5,1,1).p("Aitg6QAFBMA+AiQA6AdBFgCQBGgCAqgiQAuglgFhV");
	this.shape_2.setTransform(65.5794,-1.1437);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(11.5,1,1).p("Aitg0QAFBMA/AcQA7AYBFgCQBGgDAqgcQAsgggFhU");
	this.shape_3.setTransform(65.5803,-1.7134);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(11.5,1,1).p("AitgvQAFBMBAAWQA9AUBEgDQBGgCApgYQArgagFhU");
	this.shape_4.setTransform(65.5809,-2.2358);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(11.5,1,1).p("AitgqQAFBMBBARQA+APBEgCQBFgDApgTQArgWgGhT");
	this.shape_5.setTransform(65.5867,-2.7071);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(11.5,1,1).p("AitgmQAFBMBBANQA/ALBEgDQBGgEAogOQAqgRgGhT");
	this.shape_6.setTransform(65.5875,-3.1429);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(11.5,1,1).p("AitghQAFBMBCAIQBAAHBEgDQBFgEAogKQApgNgGhS");
	this.shape_7.setTransform(65.5883,-3.5526);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(11.5,1,1).p("AitgeQAFBMBCAEQBBAEBEgEQBFgDAngHQApgJgGhS");
	this.shape_8.setTransform(65.5887,-3.8875);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(11.5,1,1).p("AitgcQAFBMBDABQBCABBEgEQBEgEAngDQAogGgGhS");
	this.shape_9.setTransform(65.5896,-4.1469);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(11.5,1,1).p("AitgaQAFBMBDgCQBDgDBEgDQBEgEAngBQAngCgGhS");
	this.shape_10.setTransform(65.59,-4.2975);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(11.5,1,1).p("AitgZQAFBMBEgEQBDgFBEgEQBEgEAmACQAnAAgGhS");
	this.shape_11.setTransform(65.5904,-4.4126);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(11.5,1,1).p("AitgYQAFBMBEgHQBEgHBDgDQBFgFAmAEQAmACgGhR");
	this.shape_12.setTransform(65.5909,-4.4997);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(11.5,1,1).p("AitgXQAFBMBEgJQBFgIBDgEQBEgEAmAFQAmAEgGhR");
	this.shape_13.setTransform(65.5914,-4.5827);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(11.5,1,1).p("AitgXQAFBMBEgJQBFgKBEgEQBEgFAlAHQAmAFgGhR");
	this.shape_14.setTransform(65.5914,-4.6228);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(11.5,1,1).p("AitgWQAFBMBFgLQBEgLBEgEQBEgEAmAHQAlAHgGhR");
	this.shape_15.setTransform(65.5919,-4.662);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(11.5,1,1).p("AitgWQAFBMBFgLQBFgLBDgEQBEgFAmAIQAlAHgGhR");
	this.shape_16.setTransform(65.5919,-4.7003);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(11.5,1,1).p("AitgWQAFBMBFgLQBEgMBEgEQBEgFAlAIQAmAIgGhR");
	this.shape_17.setTransform(65.5913,-4.701);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#000000").ss(11.5,1,1).p("AitgaQAFBMBDgCQBDgDBEgEQBEgEAnAAQAngCgGhS");
	this.shape_18.setTransform(65.59,-4.3211);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#000000").ss(11.5,1,1).p("AitgfQAFBMBCAGQBBAFBEgEQBFgDAngJQApgKgGhS");
	this.shape_19.setTransform(65.5887,-3.7529);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#000000").ss(11.5,1,1).p("AitgnQAFBMBBAOQA/AMBEgDQBFgDApgQQAqgSgGhT");
	this.shape_20.setTransform(65.5875,-3.045);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#000000").ss(11.5,1,1).p("AitgtQAFBMBAAUQA9ASBFgCQBFgDApgWQArgZgFhT");
	this.shape_21.setTransform(65.5812,-2.3848);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#000000").ss(11.5,1,1).p("AitgzQAFBMA/AbQA8AXBEgCQBGgDAqgbQAsgfgFhU");
	this.shape_22.setTransform(65.5803,-1.813);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#000000").ss(11.5,1,1).p("Aitg4QAFBMA+AfQA7AdBFgCQBGgDAqggQAtgkgFhU");
	this.shape_23.setTransform(65.5797,-1.2934);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#000000").ss(11.5,1,1).p("Aitg8QAFBMA+AkQA5AgBFgCQBGgCArgkQAugpgFhU");
	this.shape_24.setTransform(65.5791,-0.8692);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f().s("#000000").ss(11.5,1,1).p("AithAQAFBMA9AoQA5AjBFgBQBGgCArgoQAvgsgFhV");
	this.shape_25.setTransform(65.5788,-0.522);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f().s("#000000").ss(11.5,1,1).p("AithDQAFBMA9ArQA4AmBFgCQBGgBAsgrQAvgugFhW");
	this.shape_26.setTransform(65.5785,-0.2472);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f().s("#000000").ss(11.5,1,1).p("AithFQAFBMA8AtQA4AoBFgCQBHgBArgsQAwgwgFhX");
	this.shape_27.setTransform(65.5783,-0.0473);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f().s("#000000").ss(11.5,1,1).p("AithGQAFBMA8AuQA3ApBFgCQBHgBAsgtQAwgxgFhX");
	this.shape_28.setTransform(65.578,0.0526);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},32).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_17}]},18).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_27}]},1).to({state:[{t:this.shape_28}]},1).to({state:[{t:this.shape}]},1).wait(6));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_1_Layer_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(11.5,1,1).p("AihhAQAFBCA4AqQAzAnBAgCQBBgBApgqQAtgugFhJ");
	this.shape.setTransform(0,0.0028);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(11.5,1,1).p("Aihg6QAFBBA4AlQAzAhBAgBQBCgCAoglQAtgogFhI");
	this.shape_1.setTransform(0.0003,-0.5468);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(11.5,1,1).p("Aihg1QAFBCA4AfQA0AcBAgBQBBgCApggQAsgjgFhI");
	this.shape_2.setTransform(0.0005,-1.0712);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(11.5,1,1).p("AihgwQAFBCA4AaQA0AXBAgBQBCgCAogbQAsgegFhI");
	this.shape_3.setTransform(0.0008,-1.5705);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(11.5,1,1).p("AihgrQAFBBA4AWQA1ATBAgCQBBgBApgXQArgagFhH");
	this.shape_4.setTransform(0.001,-2.0195);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(11.5,1,1).p("AihgnQAFBBA4ARQA1APBAgBQBCgCAogSQArgWgFhH");
	this.shape_5.setTransform(0.0013,-2.4432);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(11.5,1,1).p("AihgjQAFBBA4ANQA1AMBBgCQBBgBAogPQArgSgFhH");
	this.shape_6.setTransform(0.0013,-2.8163);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(11.5,1,1).p("AihggQAFBCA4AJQA2AIBAgCQBBgBApgMQAqgNgFhI");
	this.shape_7.setTransform(0.0016,-3.1875);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(11.5,1,1).p("AihgdQAFBCA4AGQA2AFBAgCQBBgBApgJQAqgKgFhI");
	this.shape_8.setTransform(0.0016,-3.4827);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(11.5,1,1).p("AihgaQAFBCA4ADQA2ACBBgCQBBgBAogGQAqgHgFhI");
	this.shape_9.setTransform(0.0019,-3.7679);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(11.5,1,1).p("AihgYQAFBCA4AAQA2AABBgCQBBgBAogEQAqgEgFhI");
	this.shape_10.setTransform(0.0019,-3.95);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(11.5,1,1).p("AihgXQAFBBA4gBQA3gDBAgCQBBgBApgBQApgDgFhH");
	this.shape_11.setTransform(0.0022,-4.0483);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(11.5,1,1).p("AihgWQAFBBA4gDQA3gFBAgBQBBgCApABQApgBgFhH");
	this.shape_12.setTransform(0.0022,-4.1412);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(11.5,1,1).p("AihgVQAFBBA4gFQA3gGBAgBQBCgCAoACQApABgFhH");
	this.shape_13.setTransform(0.0025,-4.2075);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(11.5,1,1).p("AihgVQAFBCA4gHQA3gHBBgBQBBgCAoADQApADgFhI");
	this.shape_14.setTransform(0.0025,-4.2711);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(11.5,1,1).p("AihgUQAFBBA4gHQA3gIBBgBQBBgCAoAEQApADgFhH");
	this.shape_15.setTransform(0.0025,-4.312);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(11.5,1,1).p("AihgUQAFBBA4gIQA3gIBBgBQBBgCAoAEQApAEgFhH");
	this.shape_16.setTransform(0.0025,-4.332);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(11.5,1,1).p("AihgUQAFBBA4gIQA3gIBBgCQBBgBAoAEQApAEgFhH");
	this.shape_17.setTransform(0.0025,-4.342);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#000000").ss(11.5,1,1).p("AihgYQAFBCA4AAQA3gBBAgCQBBgBApgDQApgEgFhI");
	this.shape_18.setTransform(0.0022,-3.975);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#000000").ss(11.5,1,1).p("AihgeQAFBCA4AHQA2AGBAgBQBBgCApgKQAqgLgFhI");
	this.shape_19.setTransform(0.0016,-3.36);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#000000").ss(11.5,1,1).p("AihgkQAFBBA4AOQA1AMBBgBQBBgCAogPQArgTgFhH");
	this.shape_20.setTransform(0.0013,-2.7417);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#000000").ss(11.5,1,1).p("AihgqQAFBCA4AUQA1ARBAgBQBBgCApgVQArgYgFhI");
	this.shape_21.setTransform(0.001,-2.1691);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#000000").ss(11.5,1,1).p("AihgvQAFBBA4AaQA0AWBAgBQBCgCAogaQAsgegFhH");
	this.shape_22.setTransform(0.0008,-1.6453);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#000000").ss(11.5,1,1).p("AihgzQAFBBA4AeQA0AbBAgCQBBgBApgeQAsgjgFhH");
	this.shape_23.setTransform(0.0005,-1.221);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#000000").ss(11.5,1,1).p("Aihg3QAFBBA4AiQA0AeBAgBQBBgCApgiQAsglgFhI");
	this.shape_24.setTransform(0.0005,-0.8465);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f().s("#000000").ss(11.5,1,1).p("Aihg6QAFBBA4AlQAzAhBAgBQBCgCAogkQAtgpgFhI");
	this.shape_25.setTransform(0.0003,-0.5467);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f().s("#000000").ss(11.5,1,1).p("Aihg9QAFBCA4AnQAzAkBAgCQBCgBAognQAtgrgFhJ");
	this.shape_26.setTransform(0.0003,-0.297);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f().s("#000000").ss(11.5,1,1).p("Aihg+QAFBBA4ApQAzAmBAgCQBBgBApgpQAtgtgFhI");
	this.shape_27.setTransform(0,-0.1221);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f().s("#000000").ss(11.5,1,1).p("Aihg/QAFBBA4AqQAzAnBAgCQBBgBApgqQAtgugFhI");
	this.shape_28.setTransform(0,-0.0222);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},32).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_17}]},18).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_27}]},1).to({state:[{t:this.shape_28}]},1).to({state:[{t:this.shape}]},1).wait(6));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_84 = function() {
		this.___loopingOver___ = true;
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(84).call(this.frame_84).wait(1));

	// Layer_3_obj_
	this.Layer_3 = new lib.Symbol_1_Layer_3();
	this.Layer_3.name = "Layer_3";
	this.Layer_3.parent = this;
	this.Layer_3.setTransform(42.6,37.1,1,1,0,0,0,42.6,37.1);
	this.Layer_3.depth = 0;
	this.Layer_3.isAttachedToCamera = 0
	this.Layer_3.isAttachedToMask = 0
	this.Layer_3.layerDepth = 0
	this.Layer_3.layerIndex = 0
	this.Layer_3.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_3).wait(85));

	// Layer_2_obj_
	this.Layer_2 = new lib.Symbol_1_Layer_2();
	this.Layer_2.name = "Layer_2";
	this.Layer_2.parent = this;
	this.Layer_2.setTransform(65.5,0.1,1,1,0,0,0,65.5,0.1);
	this.Layer_2.depth = 0;
	this.Layer_2.isAttachedToCamera = 0
	this.Layer_2.isAttachedToMask = 0
	this.Layer_2.layerDepth = 0
	this.Layer_2.layerIndex = 1
	this.Layer_2.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_2).wait(85));

	// Layer_1_obj_
	this.Layer_1 = new lib.Symbol_1_Layer_1();
	this.Layer_1.name = "Layer_1";
	this.Layer_1.parent = this;
	this.Layer_1.depth = 0;
	this.Layer_1.isAttachedToCamera = 0
	this.Layer_1.isAttachedToMask = 0
	this.Layer_1.layerDepth = 0
	this.Layer_1.layerIndex = 2
	this.Layer_1.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_1).wait(85));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-21.9,-14.8,110.69999999999999,63.099999999999994);


(lib.Symbol_8_Symbol_3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Symbol_3
	this.instance = new lib.Symbol3();
	this.instance.parent = this;
	this.instance.setTransform(64.05,57.75,1,1,0,0,0,12.3,-8.9);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(32).to({rotation:19.9563,x:7.4,y:66.5},6).wait(2).to({rotation:34.9552,x:19.05,y:64},3).to({regY:-8.8,rotation:41.9229,x:14.9,y:49.95},3).to({x:0.1,y:58.85},3).to({regY:-8.9,rotation:19.9563,x:7.4,y:66.5},3).to({rotation:34.9552,x:19.05,y:64},3).to({regY:-8.8,rotation:41.9229,x:14.9,y:49.95},3).to({x:0.1,y:58.85},3).to({regY:-8.9,rotation:19.9563,x:7.4,y:66.5},3).wait(13).to({rotation:0,x:64.05,y:57.75},6,cjs.Ease.get(1)).wait(2));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_8_Symbol_2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Symbol_2
	this.instance = new lib.Symbol2();
	this.instance.parent = this;
	this.instance.setTransform(-158.45,-121.9,1,1,0,0,0,7.2,17.4);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(18).to({regX:2.5,regY:5.8,scaleX:0.9998,scaleY:0.9998,rotation:2.8987,x:-169.55,y:-122.25},0).wait(1).to({scaleX:0.9995,scaleY:0.9995,rotation:5.5975,x:-175.5,y:-111.85},0).wait(1).to({scaleX:0.9993,scaleY:0.9993,rotation:8.0963,x:-181.05,y:-102.15},0).wait(1).to({scaleX:0.9991,scaleY:0.9991,rotation:10.3953,x:-186.1,y:-93.2},0).wait(1).to({scaleX:0.9989,scaleY:0.9989,rotation:12.4944,x:-190.7,y:-85},0).wait(1).to({scaleX:0.9987,scaleY:0.9987,rotation:14.3935,x:-194.95,y:-77.65},0).wait(1).to({regX:7.2,regY:17.3,scaleX:0.9985,scaleY:0.9985,rotation:16.0927,x:-197.4,y:-58.6},0).to({scaleX:1,scaleY:1,rotation:-82.2098,x:-210.45,y:-36.45},8,cjs.Ease.get(1)).wait(32).to({scaleX:0.9985,scaleY:0.9985,rotation:-13.9065,x:-197.35,y:-58.6},7,cjs.Ease.get(-1)).to({regY:17.4,scaleX:1,scaleY:1,rotation:0,x:-158.45,y:-121.9},6,cjs.Ease.get(1)).wait(8));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_8_Symbol_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Symbol_1
	this.instance = new lib.Symbol1();
	this.instance.parent = this;
	this.instance.setTransform(-83.9,-65.15);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(85));

}).prototype = p = new cjs.MovieClip();


(lib.Scene_1_Layer_2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.instance = new lib.Symbol9();
	this.instance.parent = this;
	this.instance.setTransform(540,340);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Scene_1_Layer_2, null, null);


(lib.Symbol8 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_84 = function() {
		this.___loopingOver___ = true;
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(84).call(this.frame_84).wait(1));

	// Symbol_3_obj_
	this.Symbol_3 = new lib.Symbol_8_Symbol_3();
	this.Symbol_3.name = "Symbol_3";
	this.Symbol_3.parent = this;
	this.Symbol_3.setTransform(51.8,65.6,1,1,0,0,0,51.8,65.6);
	this.Symbol_3.depth = 0;
	this.Symbol_3.isAttachedToCamera = 0
	this.Symbol_3.isAttachedToMask = 0
	this.Symbol_3.layerDepth = 0
	this.Symbol_3.layerIndex = 0
	this.Symbol_3.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Symbol_3).wait(85));

	// Symbol_6_obj_
	this.Symbol_6 = new lib.Symbol_8_Symbol_6();
	this.Symbol_6.name = "Symbol_6";
	this.Symbol_6.parent = this;
	this.Symbol_6.setTransform(91.4,5.5,1,1,0,0,0,91.4,5.5);
	this.Symbol_6.depth = 0;
	this.Symbol_6.isAttachedToCamera = 0
	this.Symbol_6.isAttachedToMask = 0
	this.Symbol_6.layerDepth = 0
	this.Symbol_6.layerIndex = 1
	this.Symbol_6.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Symbol_6).wait(85));

	// Symbol_2_obj_
	this.Symbol_2 = new lib.Symbol_8_Symbol_2();
	this.Symbol_2.name = "Symbol_2";
	this.Symbol_2.parent = this;
	this.Symbol_2.setTransform(-163.2,-133.5,1,1,0,0,0,-163.2,-133.5);
	this.Symbol_2.depth = 0;
	this.Symbol_2.isAttachedToCamera = 0
	this.Symbol_2.isAttachedToMask = 0
	this.Symbol_2.layerDepth = 0
	this.Symbol_2.layerIndex = 2
	this.Symbol_2.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Symbol_2).wait(18).to({regX:-198.3,regY:-86.5,x:-198.3,y:-86.5},0).wait(6).to({regX:-163.2,regY:-133.5,x:-163.2,y:-133.5},0).wait(61));

	// Symbol_1_obj_
	this.Symbol_1 = new lib.Symbol_8_Symbol_1();
	this.Symbol_1.name = "Symbol_1";
	this.Symbol_1.parent = this;
	this.Symbol_1.setTransform(-50.5,-48.5,1,1,0,0,0,-50.5,-48.5);
	this.Symbol_1.depth = 0;
	this.Symbol_1.isAttachedToCamera = 0
	this.Symbol_1.isAttachedToMask = 0
	this.Symbol_1.layerDepth = 0
	this.Symbol_1.layerIndex = 3
	this.Symbol_1.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Symbol_1).wait(85));

	// Layer_8 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("An/S5MgMjglkMApFgANMgAbAiTIo7Deg");
	mask.setTransform(-40.675,-120.275);

	// pack_png_obj_
	this.pack_png = new lib.Symbol_8_pack_png();
	this.pack_png.name = "pack_png";
	this.pack_png.parent = this;
	this.pack_png.setTransform(5.1,9.2,1,1,0,0,0,5.1,9.2);
	this.pack_png.depth = 0;
	this.pack_png.isAttachedToCamera = 0
	this.pack_png.isAttachedToMask = 0
	this.pack_png.layerDepth = 0
	this.pack_png.layerIndex = 4
	this.pack_png.maskLayerName = 0

	var maskedShapeInstanceList = [this.pack_png];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.pack_png).wait(85));

	// Symbol_7_obj_
	this.Symbol_7 = new lib.Symbol_8_Symbol_7();
	this.Symbol_7.name = "Symbol_7";
	this.Symbol_7.parent = this;
	this.Symbol_7.setTransform(-121.8,-78.9,1,1,0,0,0,-121.8,-78.9);
	this.Symbol_7.depth = 0;
	this.Symbol_7.isAttachedToCamera = 0
	this.Symbol_7.isAttachedToMask = 0
	this.Symbol_7.layerDepth = 0
	this.Symbol_7.layerIndex = 5
	this.Symbol_7.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Symbol_7).wait(85));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-247.1,-220.8,372.9,315.6);


(lib.Symbol4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Symbol_3
	this.instance = new lib.Symbol8();
	this.instance.parent = this;
	this.instance.setTransform(-91.9,-9.5,1,1,0,0,0,-91.5,0.7);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(9).to({rotation:3.4528,x:-91.45},15,cjs.Ease.get(1)).wait(40).to({rotation:0,x:-91.9},12,cjs.Ease.get(1)).wait(9));

	// Layer_8
	this.instance_1 = new lib.Symbol5();
	this.instance_1.parent = this;
	this.instance_1.setTransform(4.7,0);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(85));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-177.4,-255.8,362.5,500.20000000000005);


(lib.Scene_1_Layer_9 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_9
	this.instance = new lib.Symbol4();
	this.instance.parent = this;
	this.instance.setTransform(880.4,449,1,1,0,0,0,-2.9,0);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Scene_1_Layer_9, null, null);


// stage content:
(lib.May_Main_3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	this.___GetDepth___ = function(obj) {
		var depth = obj.depth;
		var cameraObj = this.___camera___instance;
		if(cameraObj && cameraObj.depth && obj.isAttachedToCamera)
		{
			depth += depth + cameraObj.depth;
		}
		return depth;
		}
	this.___needSorting___ = function() {
		for (var i = 0; i < this.getNumChildren() - 1; i++)
		{
			var prevDepth = this.___GetDepth___(this.getChildAt(i));
			var nextDepth = this.___GetDepth___(this.getChildAt(i + 1));
			if (prevDepth < nextDepth)
				return true;
		}
		return false;
	}
	this.___sortFunction___ = function(obj1, obj2) {
		return (this.exportRoot.___GetDepth___(obj2) - this.exportRoot.___GetDepth___(obj1));
	}
	this.on('tick', function (event){
		var curTimeline = event.currentTarget;
		if (curTimeline.___needSorting___()){
			this.sortChildren(curTimeline.___sortFunction___);
		}
	});

	// Layer_9_obj_
	this.Layer_9 = new lib.Scene_1_Layer_9();
	this.Layer_9.name = "Layer_9";
	this.Layer_9.parent = this;
	this.Layer_9.setTransform(880.5,437.8,1,1,0,0,0,880.5,437.8);
	this.Layer_9.depth = 0;
	this.Layer_9.isAttachedToCamera = 0
	this.Layer_9.isAttachedToMask = 0
	this.Layer_9.layerDepth = 0
	this.Layer_9.layerIndex = 0
	this.Layer_9.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_9).wait(1));

	// Layer_2_obj_
	this.Layer_2 = new lib.Scene_1_Layer_2();
	this.Layer_2.name = "Layer_2";
	this.Layer_2.parent = this;
	this.Layer_2.setTransform(540,340,1,1,0,0,0,540,340);
	this.Layer_2.depth = 0;
	this.Layer_2.isAttachedToCamera = 0
	this.Layer_2.isAttachedToMask = 0
	this.Layer_2.layerDepth = 0
	this.Layer_2.layerIndex = 1
	this.Layer_2.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_2).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(540,340,540,340);
// library properties:
lib.properties = {
	id: '7B3EDDC45380734C9951A40595213333',
	width: 1080,
	height: 680,
	fps: 24,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: [
		{src:"assets/images/bg45.jpg", id:"bg"},
		{src:"assets/images/milk_part45.png", id:"milk_part"},
		{src:"assets/images/pack45.png", id:"pack"}
	],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['7B3EDDC45380734C9951A40595213333'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}


// Layer depth API : 

AdobeAn.Layer = new function() {
	this.getLayerZDepth = function(timeline, layerName)
	{
		if(layerName === "Camera")
		layerName = "___camera___instance";
		var script = "if(timeline." + layerName + ") timeline." + layerName + ".depth; else 0;";
		return eval(script);
	}
	this.setLayerZDepth = function(timeline, layerName, zDepth)
	{
		const MAX_zDepth = 10000;
		const MIN_zDepth = -5000;
		if(zDepth > MAX_zDepth)
			zDepth = MAX_zDepth;
		else if(zDepth < MIN_zDepth)
			zDepth = MIN_zDepth;
		if(layerName === "Camera")
		layerName = "___camera___instance";
		var script = "if(timeline." + layerName + ") timeline." + layerName + ".depth = " + zDepth + ";";
		eval(script);
	}
	this.removeLayer = function(timeline, layerName)
	{
		if(layerName === "Camera")
		layerName = "___camera___instance";
		var script = "if(timeline." + layerName + ") timeline.removeChild(timeline." + layerName + ");";
		eval(script);
	}
	this.addNewLayer = function(timeline, layerName, zDepth)
	{
		if(layerName === "Camera")
		layerName = "___camera___instance";
		zDepth = typeof zDepth !== 'undefined' ? zDepth : 0;
		var layer = new createjs.MovieClip();
		layer.name = layerName;
		layer.depth = zDepth;
		layer.layerIndex = 0;
		timeline.addChild(layer);
	}
}


})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;

$(function () {
    var canvas, stage, exportRoot, anim_container, dom_overlay_container, fnStartAnimation;
    function init() {
        canvas = document.getElementById("canvas45");
        anim_container = document.getElementById("animation_container45");
        dom_overlay_container = document.getElementById("dom_overlay_container45");
        var comp=AdobeAn.getComposition("7B3EDDC45380734C9951A40595213333");
        var lib=comp.getLibrary();
        var loader = new createjs.LoadQueue(false);
        loader.addEventListener("fileload", function(evt){handleFileLoad(evt,comp)});
        loader.addEventListener("complete", function(evt){handleComplete(evt,comp)});
        var lib=comp.getLibrary();
        loader.loadManifest(lib.properties.manifest);
    }
    function handleFileLoad(evt, comp) {
        var images=comp.getImages();
        if (evt && (evt.item.type == "image")) { images[evt.item.id] = evt.result; }
    }
    function handleComplete(evt,comp) {
        //This function is always called, irrespective of the content. You can use the variable "stage" after it is created in token create_stage.
        var lib=comp.getLibrary();
        var ss=comp.getSpriteSheet();
        var queue = evt.target;
        var ssMetadata = lib.ssMetadata;
        for(i=0; i<ssMetadata.length; i++) {
            ss[ssMetadata[i].name] = new createjs.SpriteSheet( {"images": [queue.getResult(ssMetadata[i].name)], "frames": ssMetadata[i].frames} )
        }
        var preloaderDiv = document.getElementById("_preload_div_45");
        preloaderDiv.style.display = 'none';
        canvas.style.display = 'block';
        exportRoot = new lib.May_Main_3();
        stage = new lib.Stage(canvas);
        //Registers the "tick" event listener.
        fnStartAnimation = function() {
            stage.addChild(exportRoot);
            createjs.Ticker.setFPS(lib.properties.fps);
            createjs.Ticker.addEventListener("tick", stage)
            stage.addEventListener("tick", handleTick)
            function getProjectionMatrix(container, totalDepth) {
                var focalLength = 528.25;
                var projectionCenter = { x : lib.properties.width/2, y : lib.properties.height/2 };
                var scale = (totalDepth + focalLength)/focalLength;
                var scaleMat = new createjs.Matrix2D;
                scaleMat.a = 1/scale;
                scaleMat.d = 1/scale;
                var projMat = new createjs.Matrix2D;
                projMat.tx = -projectionCenter.x;
                projMat.ty = -projectionCenter.y;
                projMat = projMat.prependMatrix(scaleMat);
                projMat.tx += projectionCenter.x;
                projMat.ty += projectionCenter.y;
                return projMat;
            }
            function handleTick(event) {
                var cameraInstance = exportRoot.___camera___instance;
                if(cameraInstance !== undefined && cameraInstance.pinToObject !== undefined)
                {
                    cameraInstance.x = cameraInstance.pinToObject.x + cameraInstance.pinToObject.pinOffsetX;
                    cameraInstance.y = cameraInstance.pinToObject.y + cameraInstance.pinToObject.pinOffsetY;
                    if(cameraInstance.pinToObject.parent !== undefined && cameraInstance.pinToObject.parent.depth !== undefined)
                        cameraInstance.depth = cameraInstance.pinToObject.parent.depth + cameraInstance.pinToObject.pinOffsetZ;
                }
                applyLayerZDepth(exportRoot);
            }
            function applyLayerZDepth(parent)
            {
                var cameraInstance = parent.___camera___instance;
                var focalLength = 528.25;
                var projectionCenter = { 'x' : 0, 'y' : 0};
                if(parent === exportRoot)
                {
                    var stageCenter = { 'x' : lib.properties.width/2, 'y' : lib.properties.height/2 };
                    projectionCenter.x = stageCenter.x;
                    projectionCenter.y = stageCenter.y;
                }
                for(child in parent.children)
                {
                    var layerObj = parent.children[child];
                    if(layerObj == cameraInstance)
                        continue;
                    applyLayerZDepth(layerObj, cameraInstance);
                    if(layerObj.layerDepth === undefined)
                        continue;
                    if(layerObj.currentFrame != layerObj.parent.currentFrame)
                    {
                        layerObj.gotoAndPlay(layerObj.parent.currentFrame);
                    }
                    var matToApply = new createjs.Matrix2D;
                    var cameraMat = new createjs.Matrix2D;
                    var totalDepth = layerObj.layerDepth ? layerObj.layerDepth : 0;
                    var cameraDepth = 0;
                    if(cameraInstance && !layerObj.isAttachedToCamera)
                    {
                        var mat = cameraInstance.getMatrix();
                        mat.tx -= projectionCenter.x;
                        mat.ty -= projectionCenter.y;
                        cameraMat = mat.invert();
                        cameraMat.prependTransform(projectionCenter.x, projectionCenter.y, 1, 1, 0, 0, 0, 0, 0);
                        cameraMat.appendTransform(-projectionCenter.x, -projectionCenter.y, 1, 1, 0, 0, 0, 0, 0);
                        if(cameraInstance.depth)
                            cameraDepth = cameraInstance.depth;
                    }
                    if(layerObj.depth)
                    {
                        totalDepth = layerObj.depth;
                    }
                    //Offset by camera depth
                    totalDepth -= cameraDepth;
                    if(totalDepth < -focalLength)
                    {
                        matToApply.a = 0;
                        matToApply.d = 0;
                    }
                    else
                    {
                        if(layerObj.layerDepth)
                        {
                            var sizeLockedMat = getProjectionMatrix(parent, layerObj.layerDepth);
                            if(sizeLockedMat)
                            {
                                sizeLockedMat.invert();
                                matToApply.prependMatrix(sizeLockedMat);
                            }
                        }
                        matToApply.prependMatrix(cameraMat);
                        var projMat = getProjectionMatrix(parent, totalDepth);
                        if(projMat)
                        {
                            matToApply.prependMatrix(projMat);
                        }
                    }
                    layerObj.transformMatrix = matToApply;
                }
            }
        }
        //Code to support hidpi screens and responsive scaling.
        function makeResponsive(isResp, respDim, isScale, scaleType) {
            var lastW, lastH, lastS=1;
            window.addEventListener('resize', resizeCanvas);
            resizeCanvas();
            function resizeCanvas() {
                var w = lib.properties.width, h = lib.properties.height;
                var iw = window.innerWidth, ih=window.innerHeight;
                var pRatio = window.devicePixelRatio || 1, xRatio=iw/w, yRatio=ih/h, sRatio=1;
                if(isResp) {
                    if((respDim=='width'&&lastW==iw) || (respDim=='height'&&lastH==ih)) {
                        sRatio = lastS;
                    }
                    else if(!isScale) {
                        if(iw<w || ih<h)
                            sRatio = Math.min(xRatio, yRatio);
                    }
                    else if(scaleType==1) {
                        sRatio = Math.min(xRatio, yRatio);
                    }
                    else if(scaleType==2) {
                        sRatio = Math.max(xRatio, yRatio);
                    }
                }
                canvas.width = w*pRatio*sRatio;
                canvas.height = h*pRatio*sRatio;
                canvas.style.width = anim_container.style.width = dom_overlay_container.style.width = preloaderDiv.style.width = w*sRatio+'px';
                canvas.style.height = anim_container.style.height = dom_overlay_container.style.height = preloaderDiv.style.height = h*sRatio+'px';
                stage.scaleX = pRatio*sRatio;
                stage.scaleY = pRatio*sRatio;
                lastW = iw; lastH = ih; lastS = sRatio;
                stage.tickOnUpdate = false;
                stage.update();
                stage.tickOnUpdate = true;
            }
        }
        makeResponsive(true,'both',true,2);
        AdobeAn.compositionLoaded(lib.properties.id);
        fnStartAnimation();
    }
    init();
})