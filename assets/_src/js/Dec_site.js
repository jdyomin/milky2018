(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [];


// symbols:



(lib.bg = function() {
	this.initialize(img.bg);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,1080,680);


(lib.pack = function() {
	this.initialize(img.pack);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,608,472);


(lib.present = function() {
	this.initialize(img.present);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,319,195);


(lib.rumyanec = function() {
	this.initialize(img.rumyanec);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,80,52);// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.Symbol6 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_3
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(11.5,1,1).p("AAABBIAAiB");
	this.shape.setTransform(67,120.225);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	// Layer_1
	this.instance = new lib.present();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	// Layer_4
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(11.5,1,1).p("Ah0hKQB1CvB0ge");
	this.shape_1.setTransform(77.325,133.7426);

	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(1));

	// Layer_2
	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(11.5,1,1).p("AixgjQB/AyDlAV");
	this.shape_2.setTransform(83.45,129.775);

	this.timeline.addTween(cjs.Tween.get(this.shape_2).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol6, new cjs.Rectangle(0,0,319,195), null);


(lib.Symbol5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_3
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(11.5,1,1).p("AgIA+IARh7");
	this.shape.setTransform(20.2,17.725);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	// Layer_2
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(11.5,1,1).p("AB3gUQiLBehihj");
	this.shape_1.setTransform(6.475,24.0032);

	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(1));

	// Layer_1
	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(11.5,1,1).p("ABcBuQjqguBBit");
	this.shape_2.setTransform(9.2111,10.95);

	this.timeline.addTween(cjs.Tween.get(this.shape_2).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol5, new cjs.Rectangle(-11.2,-5.7,38.099999999999994,38), null);


(lib.Symbol2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.rumyanec();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol2, new cjs.Rectangle(0,0,80,52), null);


(lib.Symbol1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.rumyanec();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol1, new cjs.Rectangle(0,0,80,52), null);


(lib.Symbol_8_Layer_6 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_6
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(11.5,1,1).p("AiZgUQANATAuAKQAoAKA2ABQAzABArgHQAtgIAPgN");
	this.shape.setTransform(87.8,2.1559);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(11.5,1,1).p("AiZgYQANAXAuANQAoAMA2ABQAzABArgJQAtgJAPgQ");
	this.shape_1.setTransform(87.8,2.63);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(11.5,1,1).p("AiZgdQANAcAuAPQAoAOA2ACQAzABArgLQAtgLAPgT");
	this.shape_2.setTransform(87.8,3.134);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(11.5,1,1).p("AiZgiQANAhAuARQAoARA2ABQAzACArgNQAtgNAPgW");
	this.shape_3.setTransform(87.8,3.608);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(11.5,1,1).p("AiZgmQANAlAuAUQAoATA2ABQAzACArgOQAtgPAPgZ");
	this.shape_4.setTransform(87.8,4.0871);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(11.5,1,1).p("AiZgrQANApAuAXQAoAVA2ACQAzACArgQQAtgRAPgc");
	this.shape_5.setTransform(87.8,4.5611);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(11.5,1,1).p("AiZgwQANAuAuAZQAoAXA2ACQAzADArgSQAtgTAPge");
	this.shape_6.setTransform(87.8,5.0652);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(11.5,1,1).p("AiZg0QANAyAuAcQAoAZA2ACQAzADArgUQAtgUAPgi");
	this.shape_7.setTransform(87.8,5.5392);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(11.5,1,1).p("AiZgsQANArAuAWQAoAWA2ACQAzACArgRQAtgQAPgd");
	this.shape_8.setTransform(87.8,4.6358);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(11.5,1,1).p("AiZgkQANAjAuATQAoASA2ABQAzACArgOQAtgOAPgX");
	this.shape_9.setTransform(87.8,3.8825);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(11.5,1,1).p("AiZgeQANAdAuAQQAoAPA2ABQAzABArgLQAtgMAPgT");
	this.shape_10.setTransform(87.8,3.259);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(11.5,1,1).p("AiZgaQANAZAuANQAoANA2ABQAzACArgKQAtgKAPgR");
	this.shape_11.setTransform(87.8,2.7602);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(11.5,1,1).p("AiZgWQANAVAuAMQAoALA2ABQAzABArgIQAtgJAPgO");
	this.shape_12.setTransform(87.8,2.4303);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(11.5,1,1).p("AiZgUQANATAuALQAoAKA2ABQAzABArgIQAtgIAPgN");
	this.shape_13.setTransform(87.8,2.2306);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},14).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_7}]},18).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape}]},1).wait(14));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_8_Layer_5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_5
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(11.5,1,1).p("Ai8g1QBjBRBhAVQBiAUBTg6");
	this.shape.setTransform(51.225,36.1677);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(11.5,1,1).p("Ai6g6QBcBbBjAVQBgAUBWg8");
	this.shape_1.setTransform(51.425,36.5234);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(11.5,1,1).p("Ai4hAQBWBnBjAVQBgAUBYhA");
	this.shape_2.setTransform(51.625,36.8631);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(11.5,1,1).p("Ai2hFQBQBxBlAVQBeAUBahD");
	this.shape_3.setTransform(51.825,37.2211);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(11.5,1,1).p("AizhKQBIB7BnAWQBcATBchG");
	this.shape_4.setTransform(52.05,37.5624);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(11.5,1,1).p("AiyhPQBCCFBpAWQBbATBehI");
	this.shape_5.setTransform(52.25,37.9223);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(11.5,1,1).p("AiwhVQA9CRBpAWQBaATBhhM");
	this.shape_6.setTransform(52.45,38.265);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(11.5,1,1).p("AithaQA1CbBrAWQBYATBjhP");
	this.shape_7.setTransform(52.65,38.6264);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(11.5,1,1).p("AixhQQBBCHBoAWQBbATBfhJ");
	this.shape_8.setTransform(52.275,37.9712);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(11.5,1,1).p("Ai1hIQBMB3BnAVQBcAUBchF");
	this.shape_9.setTransform(51.95,37.4161);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(11.5,1,1).p("Ai3hBQBUBpBkAVQBfAUBYhB");
	this.shape_10.setTransform(51.7,36.9603);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(11.5,1,1).p("Ai5g8QBaBfBjAVQBgAUBWg+");
	this.shape_11.setTransform(51.475,36.6204);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(11.5,1,1).p("Ai6g4QBeBXBiAVQBhAUBVg7");
	this.shape_12.setTransform(51.35,36.3627);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(11.5,1,1).p("Ai8g2QBiBTBiAVQBhAUBTg6");
	this.shape_13.setTransform(51.25,36.2314);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},14).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_7}]},18).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape}]},1).wait(14));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_8_Layer_4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_4
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(11.5,1,1).p("AiZgUQANAUAuAKQAoAKA2AAQA0ABArgHQAtgIAOgM");
	this.shape.setTransform(15.4,2.0559);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(11.5,1,1).p("AiZgYQANAYAuAMQAoAMA2ABQA0ABArgJQAtgJAOgQ");
	this.shape_1.setTransform(15.4,2.53);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(11.5,1,1).p("AiZgdQANAdAuAPQAoAOA2ABQA0ABArgLQAtgLAOgS");
	this.shape_2.setTransform(15.4,2.984);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(11.5,1,1).p("AiZgiQANAhAuASQAoAQA2ABQA0ACArgNQAtgNAOgV");
	this.shape_3.setTransform(15.4,3.458);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(11.5,1,1).p("AiZgmQANAlAuAUQAoATA2ABQA0ACArgOQAtgPAOgZ");
	this.shape_4.setTransform(15.4,3.9371);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(11.5,1,1).p("AiZgrQANApAuAXQAoAVA2ACQA0ACArgQQAtgRAOgc");
	this.shape_5.setTransform(15.4,4.4111);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(11.5,1,1).p("AiZgwQANAuAuAZQAoAYA2ABQA0ADArgSQAtgSAOgf");
	this.shape_6.setTransform(15.4,4.8652);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(11.5,1,1).p("AiZg0QANAyAuAcQAoAZA2ACQA0ADArgUQAtgUAOgi");
	this.shape_7.setTransform(15.4,5.3392);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(11.5,1,1).p("AiZgsQANArAuAWQAoAWA2ABQA0ACArgQQAtgRAOgc");
	this.shape_8.setTransform(15.4,4.4611);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(11.5,1,1).p("AiZgkQANAjAuATQAoASA2ABQA0ACArgOQAtgOAOgX");
	this.shape_9.setTransform(15.4,3.7325);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(11.5,1,1).p("AiZgeQANAdAuAQQAoAPA2ABQA0ACArgMQAtgLAOgU");
	this.shape_10.setTransform(15.4,3.1337);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(11.5,1,1).p("AiZgaQANAZAuAOQAoANA2AAQA0ACArgKQAtgKAOgQ");
	this.shape_11.setTransform(15.4,2.6598);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(11.5,1,1).p("AiZgWQANAWAuALQAoALA2ABQA0ABArgIQAtgJAOgO");
	this.shape_12.setTransform(15.4,2.3303);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(11.5,1,1).p("AiZgUQANAUAuAKQAoALA2AAQA0ABArgIQAtgHAOgN");
	this.shape_13.setTransform(15.4,2.1306);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},14).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_7}]},18).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape}]},1).wait(14));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_7_Layer_5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_5
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(11.5,1,1).p("AI0lkQghEZhVCjQhSCgiNA/QiFA9jJgTQi0gQkQhT");
	this.shape.setTransform(311.775,79.2416);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(11.5,1,1).p("AoyD+QEPBTC0AQQDJATCFg9QCNg/BTigQBUijAhkZ");
	this.shape_1.setTransform(311.8,79.2416);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(11.5,1,1).p("AoxD+QEPBUC0APQDJATCFg9QCNg/BSigQBUijAgka");
	this.shape_2.setTransform(311.9,79.2666);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(11.5,1,1).p("AowD+QEOBUC1AQQDIASCFg8QCNhABRigQBUijAfka");
	this.shape_3.setTransform(312.1,79.3416);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(11.5,1,1).p("AouD9QEOBVC1AQQDIASCFg8QCMhABQihQBUijAdka");
	this.shape_4.setTransform(312.325,79.4064);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(11.5,1,1).p("AorD9QENBWC1AQQDIASCFg9QCLhABQihQBSikAbkZ");
	this.shape_5.setTransform(312.625,79.5051);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(11.5,1,1).p("AooD9QEMBXC2AQQDIASCEg+QCLg/BOiiQBRikAZka");
	this.shape_6.setTransform(313,79.6288);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(11.5,1,1).p("AokD9QEKBYC3AQQDHARCEg9QCKhABNiiQBPilAXka");
	this.shape_7.setTransform(313.45,79.7688);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(11.5,1,1).p("AogD9QEJBZC4AQQDHARCDg+QCJhABLiiQBOimAUka");
	this.shape_8.setTransform(313.975,79.9176);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(11.5,1,1).p("AoaD9QEGBbC5APQDHARCCg+QCIhABKijQBLinAQka");
	this.shape_9.setTransform(314.55,80.0829);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(11.5,1,1).p("AoCD7QDOBMDSAaQDbAaCMg2QCPg4A8iuQA9iwgNkX");
	this.shape_10.setTransform(317.9634,80.4862);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(11.5,1,1).p("An0D4QChBBDnAhQDrAjCSgwQCUgxAyi3QAyi3gjkW");
	this.shape_11.setTransform(321.1577,80.8835);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(11.5,1,1).p("AnsD2QCBA4D1AoQD3AoCXgsQCYgtAqi8QAri8g0kV");
	this.shape_12.setTransform(323.6982,81.1956);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(11.5,1,1).p("AnoD0QBuAzD+AsQD9ArCagpQCagqAmjAQAmjAg+kT");
	this.shape_13.setTransform(325.272,81.4006);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(11.5,1,1).p("AG+loQBBETgkDBQglDBiaApQibAokAgsQkBgthngx");
	this.shape_14.setTransform(325.8073,81.4623);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(11.5,1,1).p("AnlD1QCqBJDhAiQDgAhCOgzQCOgzAti4QAti4grkX");
	this.shape_15.setTransform(323.4756,81.4271);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(11.5,1,1).p("AnkD1QDYBbDKAZQDJAZCGg6QCFg7AyixQA0izgckY");
	this.shape_16.setTransform(321.9931,81.4871);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(11.5,1,1).p("AnlD1QD0BlC9AUQC7AUCBg+QB/g/A2itQA3iwgSka");
	this.shape_17.setTransform(321.1813,81.5282);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#000000").ss(11.5,1,1).p("AHjlzQAQEbg5CuQg2Csh+BAQh/BAi3gTQi4gSj+ho");
	this.shape_18.setTransform(320.9168,81.5635);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#000000").ss(11.5,1,1).p("AnmD1QD+BoC4ASQC3ATB/hAQB+hAA2isQA5iugPkb");
	this.shape_19.setTransform(320.8907,81.5675);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#000000").ss(11.5,1,1).p("AnnD1QD+BoC4ASQC4ATB+hAQB/hAA3isQA4iugOka");
	this.shape_20.setTransform(320.7731,81.5425);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#000000").ss(11.5,1,1).p("AnoD1QD9BoC5ASQC4ATB/hAQB/hBA3irQA5iugOkb");
	this.shape_21.setTransform(320.5871,81.5074);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#000000").ss(11.5,1,1).p("AnqD1QD8BoC5ASQC6ASB/hAQB/hAA4irQA6iugNka");
	this.shape_22.setTransform(320.3608,81.4574);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#000000").ss(11.5,1,1).p("AnsD2QD7BoC6ARQC7ASB/hAQCAhAA5irQA7itgMkb");
	this.shape_23.setTransform(320.0438,81.3976);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#000000").ss(11.5,1,1).p("AnvD1QD6BpC7ARQC8ARCAhAQCAhAA6iqQA8itgJka");
	this.shape_24.setTransform(319.6399,81.3379);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f().s("#000000").ss(11.5,1,1).p("AnyD2QD4BpC8AQQC+AQCAg/QCBhAA7iqQA9itgHka");
	this.shape_25.setTransform(319.211,81.2452);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f().s("#000000").ss(11.5,1,1).p("An2D2QD2BpC9APQDBAQCAhAQCChAA8ipQA/isgEka");
	this.shape_26.setTransform(318.715,81.1521);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f().s("#000000").ss(11.5,1,1).p("An7D2QD1BqC+AOQDDAPCBg/QCChAA/ipQBAirgBka");
	this.shape_27.setTransform(318.1517,81.0433);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f().s("#000000").ss(11.5,1,1).p("AocD7QDjBhDFAOQDIAOCJg9QCKg9BTinQBUipAPkZ");
	this.shape_28.setTransform(314.55,80.2807);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f().s("#000000").ss(11.5,1,1).p("Ao1D/QDVBaDKANQDMANCPg6QCQg7BiimQBkinAbkZ");
	this.shape_29.setTransform(311.75,79.6524);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f().s("#000000").ss(11.5,1,1).p("ApHECQDLBVDOANQDPANCTg5QCUg5BuimQBuimAkkY");
	this.shape_30.setTransform(309.75,79.2457);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f().s("#000000").ss(11.5,1,1).p("ApSEEQDEBRDSANQDPANCWg4QCWg4B1imQB1ikAqkY");
	this.shape_31.setTransform(308.55,78.9891);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f().s("#000000").ss(11.5,1,1).p("AJXljQgsEYh3CkQh3CmiXA3QiXA4jQgNQjSgMjChQ");
	this.shape_32.setTransform(308.15,78.8836);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f().s("#000000").ss(11.5,1,1).p("ApGEBQDkBSDFAOQDNAPCPg5QCTg8BnijQBoijAmkZ");
	this.shape_33.setTransform(309.725,79.0552);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f().s("#000000").ss(11.5,1,1).p("Ao7D/QD8BTC7APQDLARCKg7QCPg+BcihQBdijAjkZ");
	this.shape_34.setTransform(310.875,79.1639);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f().s("#000000").ss(11.5,1,1).p("Ao1D+QELBTC2AQQDJASCGg8QCOg/BVigQBXijAhkZ");
	this.shape_35.setTransform(311.55,79.2327);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},9).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_18}]},12).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_27}]},1).to({state:[{t:this.shape_28}]},1).to({state:[{t:this.shape_29}]},1).to({state:[{t:this.shape_30}]},1).to({state:[{t:this.shape_31}]},1).to({state:[{t:this.shape_32}]},1).to({state:[{t:this.shape_33}]},1).to({state:[{t:this.shape_34}]},1).to({state:[{t:this.shape_35}]},1).to({state:[{t:this.shape}]},1).wait(3));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_7_Layer_4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_4
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(11.5,1,1).p("An9nwQD/IKL8HY");
	this.shape.setTransform(50.975,76.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(11.5,1,1).p("An+nwQEBIKL8HY");
	this.shape_1.setTransform(51.1,76.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(11.5,1,1).p("AoAnwQEGIKL7HY");
	this.shape_2.setTransform(51.25,76.6);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(11.5,1,1).p("AoCnwQELIKL6HY");
	this.shape_3.setTransform(51.475,76.6);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(11.5,1,1).p("AoEnxQERILL4HX");
	this.shape_4.setTransform(51.775,76.55);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(11.5,1,1).p("AoInxQEaILL3HX");
	this.shape_5.setTransform(52.125,76.55);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(11.5,1,1).p("AoMnxQElIML0HW");
	this.shape_6.setTransform(52.525,76.55);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(11.5,1,1).p("AoQnxQEvINLyHW");
	this.shape_7.setTransform(53.025,76.5);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(11.5,1,1).p("AoWnxQE+INLvHW");
	this.shape_8.setTransform(53.55,76.5);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(11.5,1,1).p("AotnqQFwIILrHN");
	this.shape_9.setTransform(56,75.725);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(11.5,1,1).p("Ao/nlQGXIFLoHG");
	this.shape_10.setTransform(57.9,75.125);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(11.5,1,1).p("ApMniQG0IELlHB");
	this.shape_11.setTransform(59.275,74.675);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(11.5,1,1).p("ApTngQHEICLjG/");
	this.shape_12.setTransform(60.075,74.425);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(11.5,1,1).p("ApWnfQHKICLjG9");
	this.shape_13.setTransform(60.35,74.35);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(11.5,1,1).p("ApQnnQHDIILeHH");
	this.shape_14.setTransform(59.65,75.2);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(11.5,1,1).p("ApLnsQG+IMLZHO");
	this.shape_15.setTransform(59.15,75.8);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(11.5,1,1).p("ApInwQG7IPLWHS");
	this.shape_16.setTransform(58.825,76.175);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(11.5,1,1).p("ApHnxQG6IQLVHT");
	this.shape_17.setTransform(58.725,76.3);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#000000").ss(11.5,1,1).p("ApHnxQG6IPLVHU");
	this.shape_18.setTransform(58.7,76.3);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#000000").ss(11.5,1,1).p("ApGnxQG3IPLWHU");
	this.shape_19.setTransform(58.625,76.3);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#000000").ss(11.5,1,1).p("ApFnxQG0IPLXHU");
	this.shape_20.setTransform(58.475,76.3);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#000000").ss(11.5,1,1).p("ApDnxQGvIPLYHU");
	this.shape_21.setTransform(58.3,76.3);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#000000").ss(11.5,1,1).p("ApBnxQGqIPLZHU");
	this.shape_22.setTransform(58.025,76.325);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#000000").ss(11.5,1,1).p("Ao+nxQGjIPLaHU");
	this.shape_23.setTransform(57.725,76.325);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#000000").ss(11.5,1,1).p("Ao6nxQGZIOLcHV");
	this.shape_24.setTransform(57.375,76.35);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f().s("#000000").ss(11.5,1,1).p("Ao3nxQGQIOLfHV");
	this.shape_25.setTransform(56.95,76.375);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f().s("#000000").ss(11.5,1,1).p("AoynxQGEIOLhHV");
	this.shape_26.setTransform(56.5,76.375);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f().s("#000000").ss(11.5,1,1).p("AoVnxQElIWMGHN");
	this.shape_27.setTransform(53.55,76.525);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f().s("#000000").ss(11.5,1,1).p("An/nyQDcIdMjHI");
	this.shape_28.setTransform(51.225,76.625);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f().s("#000000").ss(11.5,1,1).p("AnvnyQCnIiM4HD");
	this.shape_29.setTransform(49.6,76.7);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f().s("#000000").ss(11.5,1,1).p("AnlnyQCHIkNEHB");
	this.shape_30.setTransform(48.6,76.775);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f().s("#000000").ss(11.5,1,1).p("AninyQB9IlNIHA");
	this.shape_31.setTransform(48.275,76.775);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f().s("#000000").ss(11.5,1,1).p("AnunyQC2IaMnHK");
	this.shape_32.setTransform(49.45,76.7);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f().s("#000000").ss(11.5,1,1).p("An2nxQDeIRMPHS");
	this.shape_33.setTransform(50.3,76.65);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f().s("#000000").ss(11.5,1,1).p("An7nwQD2IMMBHW");
	this.shape_34.setTransform(50.8,76.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},9).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_17}]},12).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_27}]},1).to({state:[{t:this.shape_28}]},1).to({state:[{t:this.shape_29}]},1).to({state:[{t:this.shape_30}]},1).to({state:[{t:this.shape_31}]},1).to({state:[{t:this.shape_32}]},1).to({state:[{t:this.shape_33}]},1).to({state:[{t:this.shape_34}]},1).to({state:[{t:this.shape}]},1).wait(3));
	this.timeline.addTween(cjs.Tween.get(this.shape).wait(10).to({x:51},0).to({_off:true},1).wait(46).to({_off:false,x:50.975},0).wait(3));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_4_Layer_2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#E89489").s().p("AgSBAQgYgGgLgXQgMgWAIgaQAJgaAWgPQAWgPAXAGQAYAGALAWQAMAYgIAZQgIAbgXAOQgQAKgQAAIgNgBg");
	this.shape.setTransform(17.575,23.2628);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(10).to({y:20.2628},0).wait(1).to({y:17.2628},0).wait(1).to({y:14.2628},0).wait(1).to({y:19.2628},0).wait(1).to({y:24.2628},0).wait(1).to({y:29.2628},0).wait(1).to({y:27.2628},0).wait(1).to({y:25.2628},0).wait(1).to({y:23.2628},0).wait(1).to({y:20.2628},0).wait(1).to({y:17.2628},0).wait(1).to({y:14.2628},0).wait(1).to({y:17.2628},0).wait(1).to({y:20.2628},0).wait(1).to({y:23.2628},0).wait(1).to({y:20.2628},0).wait(1).to({y:17.2628},0).wait(1).to({y:14.2628},0).wait(1).to({y:17.2628},0).wait(1).to({y:20.2628},0).wait(1).to({y:23.2628},0).wait(1).to({y:20.2628},0).wait(1).to({y:17.2628},0).wait(1).to({y:14.2628},0).wait(1).to({y:19.2628},0).wait(1).to({y:24.2628},0).wait(1).to({y:29.2628},0).wait(1).to({y:27.2628},0).wait(1).to({y:25.2628},0).wait(1).to({y:23.2628},0).wait(21));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_4_Layer_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(11.5,1,1).p("AiTh+QBPgsBeASQBdARAbA+QAKBWgiBBQgnBKhOAGQhvgHAoj+");
	this.shape.setTransform(14.8287,15.8137);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(11.5,1,1).p("AiThrQBPglBeAPQBdAPAbA0QAKBJgiA4QgnA+hOAGQhvgHAojY");
	this.shape_1.setTransform(14.8287,13.5837);
	this.shape_1._off = true;

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(11.5,1,1).p("AiThYQBPgfBeANQBdAMAbArQAKA8giAuQgnAzhOAFQhvgGAoix");
	this.shape_2.setTransform(14.8287,11.3204);
	this.shape_2._off = true;

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(11.5,1,1).p("AiThFQBPgYBeAKQBdAKAbAhQAKAvgiAkQgnAphOADQhvgEAoiL");
	this.shape_3.setTransform(14.8287,9.0903);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(11.5,1,1).p("AiTheQBPghBeAOQBdANAbAuQAKBAgiAxQgnA3hOAEQhvgFAoi+");
	this.shape_4.setTransform(14.8287,12.329);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(11.5,1,1).p("AiTh3QBPgpBeAQQBdARAbA6QAKBRgiA+QgnBGhOAGQhvgIAojv");
	this.shape_5.setTransform(14.8287,15.5903);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(11.5,1,1).p("AiTiQQBPgyBeAUQBdAUAbBHQAKBigiBLQgnBUhOAHQhvgJAoki");
	this.shape_6.setTransform(14.8287,18.829);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(11.5,1,1).p("AiTiKQBPgwBeATQBdAUAbBDQAKBegiBIQgnBQhOAHQhvgIAokW");
	this.shape_7.setTransform(14.8287,17.8204);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(11.5,1,1).p("AiTiEQBPguBeATQBdASAbBAQAKBagiBFQgnBNhOAHQhvgIAokK");
	this.shape_8.setTransform(14.8287,16.8223);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},9).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape}]},1).wait(21));
	this.timeline.addTween(cjs.Tween.get(this.shape).wait(9).to({_off:true},1).wait(8).to({_off:false},0).to({_off:true},1).wait(5).to({_off:false},0).to({_off:true},1).wait(5).to({_off:false},0).to({_off:true},1).wait(8).to({_off:false},0).wait(21));
	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(10).to({_off:false},0).to({_off:true},1).wait(8).to({_off:false},0).to({_off:true},1).wait(3).to({_off:false},0).to({_off:true},1).wait(1).to({_off:false},0).to({_off:true},1).wait(3).to({_off:false},0).to({_off:true},1).wait(1).to({_off:false},0).to({_off:true},1).wait(28));
	this.timeline.addTween(cjs.Tween.get(this.shape_2).wait(11).to({_off:false},0).to({_off:true},1).wait(8).to({_off:false},0).to({_off:true},1).wait(1).to({_off:false},0).to({_off:true},1).wait(3).to({_off:false},0).to({_off:true},1).wait(1).to({_off:false},0).to({_off:true},1).wait(3).to({_off:false},0).to({_off:true},1).wait(27));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_3_Layer_5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_5
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(11.5,1,1).p("ABzA3Ijlht");
	this.shape.setTransform(73.375,15.7);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(11.5,1,1).p("Ah3gqIDvBV");
	this.shape_1.setTransform(73.85,14.45);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(11.5,1,1).p("Ah7gfID3A/");
	this.shape_2.setTransform(74.225,13.425);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(11.5,1,1).p("Ah+gYID9Aw");
	this.shape_3.setTransform(74.525,12.65);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(11.5,1,1).p("AiAgSIEBAl");
	this.shape_4.setTransform(74.725,12.075);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(11.5,1,1).p("AiBgPIEDAf");
	this.shape_5.setTransform(74.85,11.75);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(11.5,1,1).p("ACCAOIkEgb");
	this.shape_6.setTransform(74.9,11.625);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(11.5,1,1).p("Ah9gaID7A1");
	this.shape_7.setTransform(74.425,12.875);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(11.5,1,1).p("Ah5gkIDzBJ");
	this.shape_8.setTransform(74.05,13.9);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(11.5,1,1).p("Ah2gsIDtBZ");
	this.shape_9.setTransform(73.75,14.675);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(11.5,1,1).p("Ah0gyIDpBl");
	this.shape_10.setTransform(73.55,15.25);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(11.5,1,1).p("Ahzg1IDnBr");
	this.shape_11.setTransform(73.425,15.575);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},9).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_6}]},24).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape}]},1).wait(15));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_3_Layer_4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_4
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(11.5,1,1).p("ACBgzIkBBn");
	this.shape.setTransform(74.75,5);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(11.5,1,1).p("Ah5A4ID0hv");
	this.shape_1.setTransform(74.1,4.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(11.5,1,1).p("Ah0A7IDph1");
	this.shape_2.setTransform(73.55,4.275);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(11.5,1,1).p("AhwA+IDhh7");
	this.shape_3.setTransform(73.15,4);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(11.5,1,1).p("AhtBAIDbh/");
	this.shape_4.setTransform(72.85,3.825);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(11.5,1,1).p("AhrBBIDXiB");
	this.shape_5.setTransform(72.65,3.7);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(11.5,1,1).p("ABshAIjWCB");
	this.shape_6.setTransform(72.6,3.675);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(11.5,1,1).p("AhxA9IDjh5");
	this.shape_7.setTransform(73.25,4.075);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(11.5,1,1).p("Ah2A6IDuhz");
	this.shape_8.setTransform(73.8,4.4);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(11.5,1,1).p("Ah7A3ID3ht");
	this.shape_9.setTransform(74.225,4.675);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(11.5,1,1).p("Ah9A2ID8hr");
	this.shape_10.setTransform(74.5,4.85);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(11.5,1,1).p("AiAA0IEAhn");
	this.shape_11.setTransform(74.7,4.975);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},9).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_6}]},24).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape}]},1).wait(15));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_3_Layer_2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(11.5,1,1).p("AiAA4IEBhv");
	this.shape.setTransform(10.975,16.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(11.5,1,1).p("AiAAnIEBhN");
	this.shape_1.setTransform(10.975,14.5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(11.5,1,1).p("AiAAZIEBgx");
	this.shape_2.setTransform(10.975,13.1);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(11.5,1,1).p("AiAAOIEBgb");
	this.shape_3.setTransform(10.975,12);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(11.5,1,1).p("AiAAGIEBgL");
	this.shape_4.setTransform(10.975,11.225);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(11.5,1,1).p("AiAACIEBgD");
	this.shape_5.setTransform(10.975,10.75);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(11.5,1,1).p("AiAAAIEBAA");
	this.shape_6.setTransform(10.975,10.6);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(11.5,1,1).p("AiAARIEBgh");
	this.shape_7.setTransform(10.975,12.3);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(11.5,1,1).p("AiAAfIEBg9");
	this.shape_8.setTransform(10.975,13.7);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(11.5,1,1).p("AiAAqIEBhT");
	this.shape_9.setTransform(10.975,14.8);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(11.5,1,1).p("AiAAyIEBhj");
	this.shape_10.setTransform(10.975,15.575);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(11.5,1,1).p("AiAA2IEBhs");
	this.shape_11.setTransform(10.975,16.05);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},9).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_6}]},24).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape}]},1).wait(15));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_3_Layer_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(11.5,1,1).p("Ah2g0IDtBp");
	this.shape.setTransform(11.925,5.3);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(11.5,1,1).p("Ahxg5IDjBz");
	this.shape_1.setTransform(12.5,4.775);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(11.5,1,1).p("Ahsg+IDZB8");
	this.shape_2.setTransform(12.95,4.35);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(11.5,1,1).p("AhohBIDRCD");
	this.shape_3.setTransform(13.325,4);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(11.5,1,1).p("AhmhDIDNCH");
	this.shape_4.setTransform(13.575,3.775);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(11.5,1,1).p("AhkhFIDJCL");
	this.shape_5.setTransform(13.725,3.625);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(11.5,1,1).p("AhqhAIDVCB");
	this.shape_6.setTransform(13.2,4.1);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(11.5,1,1).p("Ahug8IDdB5");
	this.shape_7.setTransform(12.75,4.525);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(11.5,1,1).p("Ahyg4IDlBx");
	this.shape_8.setTransform(12.4,4.875);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(11.5,1,1).p("Ah0g2IDpBt");
	this.shape_9.setTransform(12.125,5.1);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(11.5,1,1).p("Ah2g1IDtBq");
	this.shape_10.setTransform(11.975,5.25);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},9).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5,p:{x:13.725,y:3.625}}]},1).to({state:[{t:this.shape_5,p:{x:13.775,y:3.575}}]},1).to({state:[{t:this.shape_5,p:{x:13.775,y:3.575}}]},24).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape}]},1).wait(15));

}).prototype = p = new cjs.MovieClip();


(lib.Scene_1_Layer_4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_4
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(11.5,1,1).p("Aivh9QDwAiBvDZ");
	this.shape.setTransform(765.325,352.35);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Scene_1_Layer_4, null, null);


(lib.Scene_1_Layer_3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_3
	this.instance = new lib.pack();
	this.instance.parent = this;
	this.instance.setTransform(472,150);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Scene_1_Layer_3, null, null);


(lib.Scene_1_Layer_2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.instance = new lib.bg();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Scene_1_Layer_2, null, null);


(lib.Symbol9 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Symbol_1
	this.instance = new lib.Symbol1();
	this.instance.parent = this;
	this.instance.setTransform(40,26,1,1,0,0,0,40,26);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(14).to({alpha:0},10,cjs.Ease.get(-1)).wait(15).to({alpha:1},5).wait(16));

	// Symbol_2
	this.instance_1 = new lib.Symbol2();
	this.instance_1.parent = this;
	this.instance_1.setTransform(125,27,1,1,0,0,0,40,26);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(14).to({alpha:0},10,cjs.Ease.get(-1)).wait(15).to({alpha:1},5).wait(16));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,165,53);


(lib.Symbol4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_59 = function() {
		this.___loopingOver___ = true;
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(59).call(this.frame_59).wait(1));

	// Layer_1_obj_
	this.Layer_1 = new lib.Symbol_4_Layer_1();
	this.Layer_1.name = "Layer_1";
	this.Layer_1.parent = this;
	this.Layer_1.setTransform(14.8,15.8,1,1,0,0,0,14.8,15.8);
	this.Layer_1.depth = 0;
	this.Layer_1.isAttachedToCamera = 0
	this.Layer_1.isAttachedToMask = 0
	this.Layer_1.layerDepth = 0
	this.Layer_1.layerIndex = 0
	this.Layer_1.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_1).wait(60));

	// Layer_2_obj_
	this.Layer_2 = new lib.Symbol_4_Layer_2();
	this.Layer_2.name = "Layer_2";
	this.Layer_2.parent = this;
	this.Layer_2.setTransform(17.6,23.3,1,1,0,0,0,17.6,23.3);
	this.Layer_2.depth = 0;
	this.Layer_2.isAttachedToCamera = 0
	this.Layer_2.isAttachedToMask = 0
	this.Layer_2.layerDepth = 0
	this.Layer_2.layerIndex = 1
	this.Layer_2.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_2).wait(60));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-5.7,-5.7,41.1,48.400000000000006);


(lib.Symbol3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_59 = function() {
		this.___loopingOver___ = true;
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(59).call(this.frame_59).wait(1));

	// Layer_5_obj_
	this.Layer_5 = new lib.Symbol_3_Layer_5();
	this.Layer_5.name = "Layer_5";
	this.Layer_5.parent = this;
	this.Layer_5.setTransform(73.4,15.7,1,1,0,0,0,73.4,15.7);
	this.Layer_5.depth = 0;
	this.Layer_5.isAttachedToCamera = 0
	this.Layer_5.isAttachedToMask = 0
	this.Layer_5.layerDepth = 0
	this.Layer_5.layerIndex = 0
	this.Layer_5.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_5).wait(60));

	// Layer_4_obj_
	this.Layer_4 = new lib.Symbol_3_Layer_4();
	this.Layer_4.name = "Layer_4";
	this.Layer_4.parent = this;
	this.Layer_4.setTransform(74.8,5,1,1,0,0,0,74.8,5);
	this.Layer_4.depth = 0;
	this.Layer_4.isAttachedToCamera = 0
	this.Layer_4.isAttachedToMask = 0
	this.Layer_4.layerDepth = 0
	this.Layer_4.layerIndex = 1
	this.Layer_4.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_4).wait(60));

	// Layer_2_obj_
	this.Layer_2 = new lib.Symbol_3_Layer_2();
	this.Layer_2.name = "Layer_2";
	this.Layer_2.parent = this;
	this.Layer_2.setTransform(11,16.2,1,1,0,0,0,11,16.2);
	this.Layer_2.depth = 0;
	this.Layer_2.isAttachedToCamera = 0
	this.Layer_2.isAttachedToMask = 0
	this.Layer_2.layerDepth = 0
	this.Layer_2.layerIndex = 2
	this.Layer_2.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_2).wait(60));

	// Layer_1_obj_
	this.Layer_1 = new lib.Symbol_3_Layer_1();
	this.Layer_1.name = "Layer_1";
	this.Layer_1.parent = this;
	this.Layer_1.setTransform(11.9,5.3,1,1,0,0,0,11.9,5.3);
	this.Layer_1.depth = 0;
	this.Layer_1.isAttachedToCamera = 0
	this.Layer_1.isAttachedToMask = 0
	this.Layer_1.layerDepth = 0
	this.Layer_1.layerIndex = 3
	this.Layer_1.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_1).wait(60));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-7.6,-9.2,101.3,36.8);


(lib.Symbol_8_Symbol_4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Symbol_4
	this.instance = new lib.Symbol4("synched",0,false);
	this.instance.parent = this;
	this.instance.setTransform(229.25,55.2,1,1,0,0,0,14.8,15.8);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(60));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_8_Symbol_3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Symbol_3
	this.instance = new lib.Symbol3("synched",0,false);
	this.instance.parent = this;
	this.instance.setTransform(199.75,7.95,1,1,0,0,0,11.9,5.3);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(60));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_7_Symbol_6 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Symbol_6
	this.instance = new lib.Symbol6();
	this.instance.parent = this;
	this.instance.setTransform(196.2,97.5,1,1,0,0,0,159.5,97.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(9).to({x:200.85},9,cjs.Ease.get(-1)).to({x:217.2},5,cjs.Ease.get(1)).to({x:210.2},4,cjs.Ease.get(1)).wait(12).to({x:206.2},9,cjs.Ease.get(-1)).to({x:189.2},5,cjs.Ease.get(1)).to({x:196.2},4,cjs.Ease.get(1)).wait(3));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_7_Symbol_5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Symbol_5
	this.instance = new lib.Symbol5();
	this.instance.parent = this;
	this.instance.setTransform(249.05,92.55,1,1,0,0,0,9.2,11);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(9).to({x:253.7},9,cjs.Ease.get(-1)).to({x:270.05},5,cjs.Ease.get(1)).to({x:263.05},4,cjs.Ease.get(1)).wait(12).to({x:259.05},9,cjs.Ease.get(-1)).to({x:242.05},5,cjs.Ease.get(1)).to({x:249.05},4,cjs.Ease.get(1)).wait(3));

}).prototype = p = new cjs.MovieClip();


(lib.Scene_1_Layer_6 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_6
	this.instance = new lib.Symbol9();
	this.instance.parent = this;
	this.instance.setTransform(688.5,297.5,1,1,0,0,0,82.5,26.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Scene_1_Layer_6, null, null);


(lib.Symbol8 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_59 = function() {
		this.___loopingOver___ = true;
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(59).call(this.frame_59).wait(1));

	// Symbol_3_obj_
	this.Symbol_3 = new lib.Symbol_8_Symbol_3();
	this.Symbol_3.name = "Symbol_3";
	this.Symbol_3.parent = this;
	this.Symbol_3.setTransform(230.7,13.5,1,1,0,0,0,230.7,13.5);
	this.Symbol_3.depth = 0;
	this.Symbol_3.isAttachedToCamera = 0
	this.Symbol_3.isAttachedToMask = 0
	this.Symbol_3.layerDepth = 0
	this.Symbol_3.layerIndex = 0
	this.Symbol_3.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Symbol_3).wait(60));

	// Symbol_4_obj_
	this.Symbol_4 = new lib.Symbol_8_Symbol_4();
	this.Symbol_4.name = "Symbol_4";
	this.Symbol_4.parent = this;
	this.Symbol_4.setTransform(229.3,55.2,1,1,0,0,0,229.3,55.2);
	this.Symbol_4.depth = 0;
	this.Symbol_4.isAttachedToCamera = 0
	this.Symbol_4.isAttachedToMask = 0
	this.Symbol_4.layerDepth = 0
	this.Symbol_4.layerIndex = 1
	this.Symbol_4.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Symbol_4).wait(60));

	// Layer_4_obj_
	this.Layer_4 = new lib.Symbol_8_Layer_4();
	this.Layer_4.name = "Layer_4";
	this.Layer_4.parent = this;
	this.Layer_4.setTransform(15.4,2,1,1,0,0,0,15.4,2);
	this.Layer_4.depth = 0;
	this.Layer_4.isAttachedToCamera = 0
	this.Layer_4.isAttachedToMask = 0
	this.Layer_4.layerDepth = 0
	this.Layer_4.layerIndex = 2
	this.Layer_4.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_4).wait(60));

	// Layer_5_obj_
	this.Layer_5 = new lib.Symbol_8_Layer_5();
	this.Layer_5.name = "Layer_5";
	this.Layer_5.parent = this;
	this.Layer_5.setTransform(51.2,36.1,1,1,0,0,0,51.2,36.1);
	this.Layer_5.depth = 0;
	this.Layer_5.isAttachedToCamera = 0
	this.Layer_5.isAttachedToMask = 0
	this.Layer_5.layerDepth = 0
	this.Layer_5.layerIndex = 3
	this.Layer_5.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_5).wait(60));

	// Layer_6_obj_
	this.Layer_6 = new lib.Symbol_8_Layer_6();
	this.Layer_6.name = "Layer_6";
	this.Layer_6.parent = this;
	this.Layer_6.setTransform(87.8,2.1,1,1,0,0,0,87.8,2.1);
	this.Layer_6.depth = 0;
	this.Layer_6.isAttachedToCamera = 0
	this.Layer_6.isAttachedToMask = 0
	this.Layer_6.layerDepth = 0
	this.Layer_6.layerIndex = 4
	this.Layer_6.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_6).wait(60));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-5.7,-6.5,287.3,88.6);


(lib.Symbol7 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_59 = function() {
		this.___loopingOver___ = true;
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(59).call(this.frame_59).wait(1));

	// Layer_4_obj_
	this.Layer_4 = new lib.Symbol_7_Layer_4();
	this.Layer_4.name = "Layer_4";
	this.Layer_4.parent = this;
	this.Layer_4.setTransform(51,76.6,1,1,0,0,0,51,76.6);
	this.Layer_4.depth = 0;
	this.Layer_4.isAttachedToCamera = 0
	this.Layer_4.isAttachedToMask = 0
	this.Layer_4.layerDepth = 0
	this.Layer_4.layerIndex = 0
	this.Layer_4.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_4).wait(60));

	// Layer_5_obj_
	this.Layer_5 = new lib.Symbol_7_Layer_5();
	this.Layer_5.name = "Layer_5";
	this.Layer_5.parent = this;
	this.Layer_5.setTransform(311.8,79.3,1,1,0,0,0,311.8,79.3);
	this.Layer_5.depth = 0;
	this.Layer_5.isAttachedToCamera = 0
	this.Layer_5.isAttachedToMask = 0
	this.Layer_5.layerDepth = 0
	this.Layer_5.layerIndex = 1
	this.Layer_5.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_5).wait(60));

	// Symbol_5_obj_
	this.Symbol_5 = new lib.Symbol_7_Symbol_5();
	this.Symbol_5.name = "Symbol_5";
	this.Symbol_5.parent = this;
	this.Symbol_5.setTransform(247.7,94.8,1,1,0,0,0,247.7,94.8);
	this.Symbol_5.depth = 0;
	this.Symbol_5.isAttachedToCamera = 0
	this.Symbol_5.isAttachedToMask = 0
	this.Symbol_5.layerDepth = 0
	this.Symbol_5.layerIndex = 2
	this.Symbol_5.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Symbol_5).wait(60));

	// Symbol_6_obj_
	this.Symbol_6 = new lib.Symbol_7_Symbol_6();
	this.Symbol_6.name = "Symbol_6";
	this.Symbol_6.parent = this;
	this.Symbol_6.setTransform(196.2,97.5,1,1,0,0,0,196.2,97.5);
	this.Symbol_6.depth = 0;
	this.Symbol_6.isAttachedToCamera = 0
	this.Symbol_6.isAttachedToMask = 0
	this.Symbol_6.layerDepth = 0
	this.Symbol_6.layerIndex = 3
	this.Symbol_6.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Symbol_6).wait(60));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-5.7,0,386.09999999999997,195);


(lib.Scene_1_Layer_15 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_15
	this.instance = new lib.Symbol7();
	this.instance.parent = this;
	this.instance.setTransform(780.4,416.5,1,1,0,0,0,184.1,97.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Scene_1_Layer_15, null, null);


(lib.Scene_1_Layer_7 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_7
	this.instance = new lib.Symbol8();
	this.instance.parent = this;
	this.instance.setTransform(774.8,310.8,1,1,0,0,0,137.8,35.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Scene_1_Layer_7, null, null);


// stage content:
(lib.Dec_site = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	this.___GetDepth___ = function(obj) {
		var depth = obj.depth;
		var cameraObj = this.___camera___instance;
		if(cameraObj && cameraObj.depth && obj.isAttachedToCamera)
		{
			depth += depth + cameraObj.depth;
		}
		return depth;
		}
	this.___needSorting___ = function() {
		for (var i = 0; i < this.getNumChildren() - 1; i++)
		{
			var prevDepth = this.___GetDepth___(this.getChildAt(i));
			var nextDepth = this.___GetDepth___(this.getChildAt(i + 1));
			if (prevDepth < nextDepth)
				return true;
		}
		return false;
	}
	this.___sortFunction___ = function(obj1, obj2) {
		return (this.exportRoot.___GetDepth___(obj2) - this.exportRoot.___GetDepth___(obj1));
	}
	this.on('tick', function (event){
		var curTimeline = event.currentTarget;
		if (curTimeline.___needSorting___()){
			this.sortChildren(curTimeline.___sortFunction___);
		}
	});

	// Layer_15_obj_
	this.Layer_15 = new lib.Scene_1_Layer_15();
	this.Layer_15.name = "Layer_15";
	this.Layer_15.parent = this;
	this.Layer_15.setTransform(780.4,416.5,1,1,0,0,0,780.4,416.5);
	this.Layer_15.depth = 0;
	this.Layer_15.isAttachedToCamera = 0
	this.Layer_15.isAttachedToMask = 0
	this.Layer_15.layerDepth = 0
	this.Layer_15.layerIndex = 0
	this.Layer_15.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_15).wait(1));

	// Layer_7_obj_
	this.Layer_7 = new lib.Scene_1_Layer_7();
	this.Layer_7.name = "Layer_7";
	this.Layer_7.parent = this;
	this.Layer_7.setTransform(774.8,310.8,1,1,0,0,0,774.8,310.8);
	this.Layer_7.depth = 0;
	this.Layer_7.isAttachedToCamera = 0
	this.Layer_7.isAttachedToMask = 0
	this.Layer_7.layerDepth = 0
	this.Layer_7.layerIndex = 1
	this.Layer_7.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_7).wait(1));

	// Layer_6_obj_
	this.Layer_6 = new lib.Scene_1_Layer_6();
	this.Layer_6.name = "Layer_6";
	this.Layer_6.parent = this;
	this.Layer_6.setTransform(688.5,297.5,1,1,0,0,0,688.5,297.5);
	this.Layer_6.depth = 0;
	this.Layer_6.isAttachedToCamera = 0
	this.Layer_6.isAttachedToMask = 0
	this.Layer_6.layerDepth = 0
	this.Layer_6.layerIndex = 2
	this.Layer_6.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_6).wait(1));

	// Layer_3_obj_
	this.Layer_3 = new lib.Scene_1_Layer_3();
	this.Layer_3.name = "Layer_3";
	this.Layer_3.parent = this;
	this.Layer_3.setTransform(776,386,1,1,0,0,0,776,386);
	this.Layer_3.depth = 0;
	this.Layer_3.isAttachedToCamera = 0
	this.Layer_3.isAttachedToMask = 0
	this.Layer_3.layerDepth = 0
	this.Layer_3.layerIndex = 3
	this.Layer_3.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_3).wait(1));

	// Layer_4_obj_
	this.Layer_4 = new lib.Scene_1_Layer_4();
	this.Layer_4.name = "Layer_4";
	this.Layer_4.parent = this;
	this.Layer_4.setTransform(765.3,352.4,1,1,0,0,0,765.3,352.4);
	this.Layer_4.depth = 0;
	this.Layer_4.isAttachedToCamera = 0
	this.Layer_4.isAttachedToMask = 0
	this.Layer_4.layerDepth = 0
	this.Layer_4.layerIndex = 4
	this.Layer_4.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_4).wait(1));

	// Layer_2_obj_
	this.Layer_2 = new lib.Scene_1_Layer_2();
	this.Layer_2.name = "Layer_2";
	this.Layer_2.parent = this;
	this.Layer_2.setTransform(540,340,1,1,0,0,0,540,340);
	this.Layer_2.depth = 0;
	this.Layer_2.isAttachedToCamera = 0
	this.Layer_2.isAttachedToMask = 0
	this.Layer_2.layerDepth = 0
	this.Layer_2.layerIndex = 5
	this.Layer_2.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_2).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(540,340,540,340);
// library properties:
lib.properties = {
	id: '7BC4CB4A378C764CB86D1BB271D0AC48',
	width: 1080,
	height: 680,
	fps: 30,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: [
		{src:"assets/images/bg33.png", id:"bg"},
		{src:"assets/images/pack33.png", id:"pack"},
		{src:"assets/images/present33.png", id:"present"},
		{src:"assets/images/rumyanec33.png", id:"rumyanec"}
	],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['7BC4CB4A378C764CB86D1BB271D0AC48'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}


// Layer depth API : 

AdobeAn.Layer = new function() {
	this.getLayerZDepth = function(timeline, layerName)
	{
		if(layerName === "Camera")
		layerName = "___camera___instance";
		var script = "if(timeline." + layerName + ") timeline." + layerName + ".depth; else 0;";
		return eval(script);
	}
	this.setLayerZDepth = function(timeline, layerName, zDepth)
	{
		const MAX_zDepth = 10000;
		const MIN_zDepth = -5000;
		if(zDepth > MAX_zDepth)
			zDepth = MAX_zDepth;
		else if(zDepth < MIN_zDepth)
			zDepth = MIN_zDepth;
		if(layerName === "Camera")
		layerName = "___camera___instance";
		var script = "if(timeline." + layerName + ") timeline." + layerName + ".depth = " + zDepth + ";";
		eval(script);
	}
	this.removeLayer = function(timeline, layerName)
	{
		if(layerName === "Camera")
		layerName = "___camera___instance";
		var script = "if(timeline." + layerName + ") timeline.removeChild(timeline." + layerName + ");";
		eval(script);
	}
	this.addNewLayer = function(timeline, layerName, zDepth)
	{
		if(layerName === "Camera")
		layerName = "___camera___instance";
		zDepth = typeof zDepth !== 'undefined' ? zDepth : 0;
		var layer = new createjs.MovieClip();
		layer.name = layerName;
		layer.depth = zDepth;
		layer.layerIndex = 0;
		timeline.addChild(layer);
	}
}


})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;

$(function () {
    var canvas, stage, exportRoot, anim_container, dom_overlay_container, fnStartAnimation;
    function init() {
        canvas = document.getElementById("canvas33");
        anim_container = document.getElementById("animation_container33");
        dom_overlay_container = document.getElementById("dom_overlay_container33");
        var comp=AdobeAn.getComposition("7BC4CB4A378C764CB86D1BB271D0AC48");
        var lib=comp.getLibrary();
        var loader = new createjs.LoadQueue(false);
        loader.addEventListener("fileload", function(evt){handleFileLoad(evt,comp)});
        loader.addEventListener("complete", function(evt){handleComplete(evt,comp)});
        var lib=comp.getLibrary();
        loader.loadManifest(lib.properties.manifest);
    }
    function handleFileLoad(evt, comp) {
        var images=comp.getImages();
        if (evt && (evt.item.type == "image")) { images[evt.item.id] = evt.result; }
    }
    function handleComplete(evt,comp) {
        //This function is always called, irrespective of the content. You can use the variable "stage" after it is created in token create_stage.
        var lib=comp.getLibrary();
        var ss=comp.getSpriteSheet();
        var queue = evt.target;
        var ssMetadata = lib.ssMetadata;
        for(i=0; i<ssMetadata.length; i++) {
            ss[ssMetadata[i].name] = new createjs.SpriteSheet( {"images": [queue.getResult(ssMetadata[i].name)], "frames": ssMetadata[i].frames} )
        }
        var preloaderDiv = document.getElementById("_preload_div_33");
        preloaderDiv.style.display = 'none';
        canvas.style.display = 'block';
        exportRoot = new lib.Dec_site();
        stage = new lib.Stage(canvas);
        //Registers the "tick" event listener.
        fnStartAnimation = function() {
            stage.addChild(exportRoot);
            createjs.Ticker.setFPS(lib.properties.fps);
            createjs.Ticker.addEventListener("tick", stage)
            stage.addEventListener("tick", handleTick)
            function getProjectionMatrix(container, totalDepth) {
                var focalLength = 528.25;
                var projectionCenter = { x : lib.properties.width/2, y : lib.properties.height/2 };
                var scale = (totalDepth + focalLength)/focalLength;
                var scaleMat = new createjs.Matrix2D;
                scaleMat.a = 1/scale;
                scaleMat.d = 1/scale;
                var projMat = new createjs.Matrix2D;
                projMat.tx = -projectionCenter.x;
                projMat.ty = -projectionCenter.y;
                projMat = projMat.prependMatrix(scaleMat);
                projMat.tx += projectionCenter.x;
                projMat.ty += projectionCenter.y;
                return projMat;
            }
            function handleTick(event) {
                var cameraInstance = exportRoot.___camera___instance;
                if(cameraInstance !== undefined && cameraInstance.pinToObject !== undefined)
                {
                    cameraInstance.x = cameraInstance.pinToObject.x + cameraInstance.pinToObject.pinOffsetX;
                    cameraInstance.y = cameraInstance.pinToObject.y + cameraInstance.pinToObject.pinOffsetY;
                    if(cameraInstance.pinToObject.parent !== undefined && cameraInstance.pinToObject.parent.depth !== undefined)
                        cameraInstance.depth = cameraInstance.pinToObject.parent.depth + cameraInstance.pinToObject.pinOffsetZ;
                }
                applyLayerZDepth(exportRoot);
            }
            function applyLayerZDepth(parent)
            {
                var cameraInstance = parent.___camera___instance;
                var focalLength = 528.25;
                var projectionCenter = { 'x' : 0, 'y' : 0};
                if(parent === exportRoot)
                {
                    var stageCenter = { 'x' : lib.properties.width/2, 'y' : lib.properties.height/2 };
                    projectionCenter.x = stageCenter.x;
                    projectionCenter.y = stageCenter.y;
                }
                for(child in parent.children)
                {
                    var layerObj = parent.children[child];
                    if(layerObj == cameraInstance)
                        continue;
                    applyLayerZDepth(layerObj, cameraInstance);
                    if(layerObj.layerDepth === undefined)
                        continue;
                    if(layerObj.currentFrame != layerObj.parent.currentFrame)
                    {
                        layerObj.gotoAndPlay(layerObj.parent.currentFrame);
                    }
                    var matToApply = new createjs.Matrix2D;
                    var cameraMat = new createjs.Matrix2D;
                    var totalDepth = layerObj.layerDepth ? layerObj.layerDepth : 0;
                    var cameraDepth = 0;
                    if(cameraInstance && !layerObj.isAttachedToCamera)
                    {
                        var mat = cameraInstance.getMatrix();
                        mat.tx -= projectionCenter.x;
                        mat.ty -= projectionCenter.y;
                        cameraMat = mat.invert();
                        cameraMat.prependTransform(projectionCenter.x, projectionCenter.y, 1, 1, 0, 0, 0, 0, 0);
                        cameraMat.appendTransform(-projectionCenter.x, -projectionCenter.y, 1, 1, 0, 0, 0, 0, 0);
                        if(cameraInstance.depth)
                            cameraDepth = cameraInstance.depth;
                    }
                    if(layerObj.depth)
                    {
                        totalDepth = layerObj.depth;
                    }
                    //Offset by camera depth
                    totalDepth -= cameraDepth;
                    if(totalDepth < -focalLength)
                    {
                        matToApply.a = 0;
                        matToApply.d = 0;
                    }
                    else
                    {
                        if(layerObj.layerDepth)
                        {
                            var sizeLockedMat = getProjectionMatrix(parent, layerObj.layerDepth);
                            if(sizeLockedMat)
                            {
                                sizeLockedMat.invert();
                                matToApply.prependMatrix(sizeLockedMat);
                            }
                        }
                        matToApply.prependMatrix(cameraMat);
                        var projMat = getProjectionMatrix(parent, totalDepth);
                        if(projMat)
                        {
                            matToApply.prependMatrix(projMat);
                        }
                    }
                    layerObj.transformMatrix = matToApply;
                }
            }
        }
        //Code to support hidpi screens and responsive scaling.
        function makeResponsive(isResp, respDim, isScale, scaleType) {
            var lastW, lastH, lastS=1;
            window.addEventListener('resize', resizeCanvas);
            resizeCanvas();
            function resizeCanvas() {
                var w = lib.properties.width, h = lib.properties.height;
                var iw = window.innerWidth, ih=window.innerHeight;
                var pRatio = window.devicePixelRatio || 1, xRatio=iw/w, yRatio=ih/h, sRatio=1;
                if(isResp) {
                    if((respDim=='width'&&lastW==iw) || (respDim=='height'&&lastH==ih)) {
                        sRatio = lastS;
                    }
                    else if(!isScale) {
                        if(iw<w || ih<h)
                            sRatio = Math.min(xRatio, yRatio);
                    }
                    else if(scaleType==1) {
                        sRatio = Math.min(xRatio, yRatio);
                    }
                    else if(scaleType==2) {
                        sRatio = Math.max(xRatio, yRatio);
                    }
                }
                canvas.width = w*pRatio*sRatio;
                canvas.height = h*pRatio*sRatio;
                canvas.style.width = anim_container.style.width = dom_overlay_container.style.width = preloaderDiv.style.width = w*sRatio+'px';
                canvas.style.height = anim_container.style.height = dom_overlay_container.style.height = preloaderDiv.style.height = h*sRatio+'px';
                stage.scaleX = pRatio*sRatio;
                stage.scaleY = pRatio*sRatio;
                lastW = iw; lastH = ih; lastS = sRatio;
                stage.tickOnUpdate = false;
                stage.update();
                stage.tickOnUpdate = true;
            }
        }
        makeResponsive(true,'both',true,2);
        AdobeAn.compositionLoaded(lib.properties.id);
        fnStartAnimation();
    }
    init();
});