(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [];


// symbols:



(lib.bg_pack = function() {
	this.initialize(img.bg_pack);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,1080,680);


(lib.butterfly_white = function() {
	this.initialize(img.butterfly_white);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,75,68);


(lib.butterfly_yellow = function() {
	this.initialize(img.butterfly_yellow);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,172,156);


(lib.leafs = function() {
	this.initialize(img.leafs);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,635,405);// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.Symbol18 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("AiomgIE8A6IAJIIIAMCwIkoBPg");
	mask.setTransform(-19.925,-1.9);

	// Layer_1
	this.instance = new lib.butterfly_white();
	this.instance.parent = this;
	this.instance.setTransform(-15.45,-48.2,1,1,29.9992);

	var maskedShapeInstanceList = [this.instance];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol18, new cjs.Rectangle(-36.8,-43.5,33.8,83.3), null);


(lib.Symbol17 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("Aj2FHIAAjRIAYgwIADgkIgNggIgJgtIHolFIgHLig");
	mask.setTransform(23.675,7.95);

	// Layer_1
	this.instance = new lib.butterfly_white();
	this.instance.parent = this;
	this.instance.setTransform(-15.45,-48.2,1,1,29.9992);

	var maskedShapeInstanceList = [this.instance];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol17, new cjs.Rectangle(-1,-29,49.4,73.9), null);


(lib.Symbol16 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("AgpgXIgshZIAHgLIAtABIA1A4IAJBCIA5BqIgdASg");
	mask.setTransform(6.775,17.225);

	// Layer_1
	this.instance = new lib.butterfly_white();
	this.instance.parent = this;
	this.instance.setTransform(-37.5,-34);

	var maskedShapeInstanceList = [this.instance];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol16, new cjs.Rectangle(-1.8,4.9,17.2,24.700000000000003), null);


(lib.Symbol14 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.butterfly_yellow();
	this.instance.parent = this;
	this.instance.setTransform(-86,-78);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol14, new cjs.Rectangle(-86,-78,172,156), null);


(lib.Symbol7 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_4
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(11,1,1).p("ABvgKIjdAV");
	this.shape.setTransform(-36.825,-30);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	// Layer_3
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(11,1,1).p("ABlAMIjJgX");
	this.shape_1.setTransform(-35.425,-38.525);

	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(1));

	// Layer_2
	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(11,1,1).p("AgIhVQAUBLgEBh");
	this.shape_2.setTransform(-24.9446,-26.1);

	this.timeline.addTween(cjs.Tween.get(this.shape_2).wait(1));

	// Layer_1
	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(11,1,1).p("AlCHQQERhTC2jKQCkizAXi7QALhYgchAQgehEhDgfQiZhIkjCG");
	this.shape_3.setTransform(0.0051,0.0061);

	this.timeline.addTween(cjs.Tween.get(this.shape_3).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol7, new cjs.Rectangle(-53.4,-51.9,91.19999999999999,103.8), null);


(lib.Symbol6 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#9AADA9").s().p("AiMCNQg7g7AAhSQAAhRA7g7QA6g7BSAAQBSAAA7A7QA7A7AABRQAABSg7A7Qg7A7hSAAQhSAAg6g7g");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol6, new cjs.Rectangle(-19.9,-19.9,39.9,39.9), null);


(lib.Symbol4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AiMCNQg7g7AAhSQAAhRA7g7QA6g7BSAAQBSAAA7A7QA7A7AABRQAABSg7A7Qg7A7hSAAQhSAAg6g7g");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol4, new cjs.Rectangle(-19.9,-19.9,39.9,39.9), null);


(lib.Symbol2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AgvAwQgUgUAAgcQAAgbAUgUQAUgTAbgBQAcABAUATQATAUABAbQgBAcgTAUQgUATgcABQgbgBgUgTg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol2, new cjs.Rectangle(-6.7,-6.7,13.5,13.5), null);


(lib.Symbol_13_Layer_3copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_3
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#1A1714").ss(11,1,1).p("AkegHQA9BiBmAZQBYAXBjgkQBbggBBhCQBBhBAChA");
	this.shape.setTransform(48.225,37.4114);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#1A1714").ss(11,1,1).p("AkoAIQBGBVBjAVQBYASBigiQBcgfBCg8QBCg7AOhD");
	this.shape_1.setTransform(48.425,36.6396);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#1A1714").ss(11,1,1).p("AkxAWQBOBJBhAQQBYAOBhggQBcgfBDg2QBDg1AZhG");
	this.shape_2.setTransform(48.625,35.9608);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#1A1714").ss(11,1,1).p("Ak4AiQBVA/BeAMQBYALBggfQBdgeBEgyQBEgwAhhI");
	this.shape_3.setTransform(48.775,35.3924);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#1A1714").ss(11,1,1).p("Ak/AsQBcA2BcAJQBYAIBggeQBdgeBEgtQBFgtAohJ");
	this.shape_4.setTransform(48.9,34.9081);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#1A1714").ss(11,1,1).p("AlEA1QBgAuBbAHQBYAFBfgdQBdgdBFgqQBFgqAwhL");
	this.shape_5.setTransform(49.025,34.5182);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#1A1714").ss(11,1,1).p("AlIA7QBkApBaAEQBYAEBegdQBegdBFgnQBGgnA0hM");
	this.shape_6.setTransform(49.075,34.2242);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#1A1714").ss(11,1,1).p("AlLA/QBmAlBaADQBYADBegdQBegcBFgmQBGglA4hN");
	this.shape_7.setTransform(49.15,34.0101);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#1A1714").ss(11,1,1).p("AlMBCQBnAjBYACQBZACBegcQBegdBGgkQBGgkA6hO");
	this.shape_8.setTransform(49.2,33.8817);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#1A1714").ss(11,1,1).p("AlNBDQBoAiBYABQBZACBdgcQBfgcBGglQBGgjA6hN");
	this.shape_9.setTransform(49.2,33.8423);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#1A1714").ss(11,1,1).p("AlDA0QBfAvBbAHQBYAGBfgeQBegdBEgqQBGgqAuhL");
	this.shape_10.setTransform(49,34.55);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#1A1714").ss(11,1,1).p("Ak7AmQBYA8BeALQBYAKBggfQBdgeBEgwQBEgvAkhJ");
	this.shape_11.setTransform(48.8,35.197);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#1A1714").ss(11,1,1).p("AkzAaQBQBGBgAPQBYANBhggQBcgfBDg0QBEg0AbhH");
	this.shape_12.setTransform(48.65,35.7628);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#1A1714").ss(11,1,1).p("AktAQQBLBOBhASQBZAQBhghQBcgfBCg4QBDg4AUhF");
	this.shape_13.setTransform(48.525,36.2612);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#1A1714").ss(11,1,1).p("AkoAHQBGBWBjAVQBZASBigiQBbgfBCg8QBDg7ANhD");
	this.shape_14.setTransform(48.4,36.676);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#1A1714").ss(11,1,1).p("AkjABQBCBbBkAXQBYAUBigiQBcggBBg/QBCg9AJhC");
	this.shape_15.setTransform(48.35,36.992);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#1A1714").ss(11,1,1).p("AkhgDQBABeBlAZQBYAVBjgjQBbggBBhAQBChAAFhB");
	this.shape_16.setTransform(48.275,37.2341);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#1A1714").ss(11,1,1).p("AkfgGQA+BhBmAZQBYAWBjgjQBbggBBhCQBBhAADhB");
	this.shape_17.setTransform(48.225,37.3709);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},34).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_9}]},17).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape}]},1).wait(6));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_13_Layer_3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_3
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(12,1,1).p("AAbBlQA9h0iFhV");
	this.shape.setTransform(10.3136,-12.05);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(12,1,1).p("AgzhhQCYBMhJB3");
	this.shape_1.setTransform(10.1681,-11.825);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(12,1,1).p("Ag4hfQCpBEhTB7");
	this.shape_2.setTransform(10.0225,-11.6);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(12,1,1).p("Ag+hdQC8A8heB/");
	this.shape_3.setTransform(9.8389,-11.4);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(12,1,1).p("AAhBcQBpiDjNg0");
	this.shape_4.setTransform(9.6895,-11.175);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(12,1,1).p("AhAhSQDAAmheB/");
	this.shape_5.setTransform(9.5617,-10.3);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(12,1,1).p("Ag9hJQC0AYhTB7");
	this.shape_6.setTransform(9.443,-9.4);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(12,1,1).p("Ag6hAQCnAKhIB3");
	this.shape_7.setTransform(9.3178,-8.525);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(12,1,1).p("AAmA5QA9h0iZAE");
	this.shape_8.setTransform(9.1973,-7.6544);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(12,1,1).p("Ag1hAQCWAOg9Bz");
	this.shape_9.setTransform(9.4149,-8.525);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(12,1,1).p("AgzhJQCSAgg9Bz");
	this.shape_10.setTransform(9.632,-9.4);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(12,1,1).p("AgxhSQCOAyg+Bz");
	this.shape_11.setTransform(9.877,-10.3);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(12,1,1).p("AgvhbQCKBEg+Bz");
	this.shape_12.setTransform(10.0956,-11.175);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},59).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape}]},1).wait(3));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_13_Layer_2copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#1A1714").ss(11,1,1).p("AjDBtQgHhPA2g9QAyg4BLgQQBMgQA7AkQBDAoARBa");
	this.shape.setTransform(73.2598,-14.1712);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#1A1714").ss(11,1,1).p("AjDBvQgHhPA1g/QAxg7BMgPQBMgPA8AlQBDApARBa");
	this.shape_1.setTransform(73.2592,-14.3143);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#1A1714").ss(11,1,1).p("AjDBzQgHhPAyhFQAwhDBNgMQBMgMA/AqQBDAtARBa");
	this.shape_2.setTransform(73.257,-14.792);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#1A1714").ss(11,1,1).p("AjDB8QgGhPAthPQAthQBOgIQBOgHBDAyQBDAyARBa");
	this.shape_3.setTransform(73.2536,-15.6243);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#1A1714").ss(11,1,1).p("AjDB4QgHhPAwhLQAuhKBOgJQBNgKBBAvQBDAvARBa");
	this.shape_4.setTransform(73.255,-15.2377);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#1A1714").ss(11,1,1).p("AjDB0QgHhPAyhGQAvhEBNgMQBNgLA/ArQBDAtARBa");
	this.shape_5.setTransform(73.257,-14.8713);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#1A1714").ss(11,1,1).p("AjDBxQgHhPA0hCQAxg+BMgOQBMgOA9AoQBDAqARBa");
	this.shape_6.setTransform(73.2585,-14.5082);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#1A1714").ss(11,1,1).p("AjDB1QgHhPAxhHQAvhGBOgLQBMgLBAAsQBDAuARBa");
	this.shape_7.setTransform(73.2563,-14.9681);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#1A1714").ss(11,1,1).p("AjDBuQgHhPA2g+QAxg6BMgPQBLgQA8AlQBDApARBa");
	this.shape_8.setTransform(73.2595,-14.2538);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},34).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape}]},1).wait(27));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_13_Layer_2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(12,1,1).p("AB+A0QhMi0ivCG");
	this.shape.setTransform(-3.5,-10.3147);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(12,1,1).p("Ah/gKQC4h3BHC6");
	this.shape_1.setTransform(-3.725,-10.8851);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(12,1,1).p("AiBgbQDBhnBCDC");
	this.shape_2.setTransform(-3.975,-11.5193);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(12,1,1).p("AiEgrQDLhXA+DJ");
	this.shape_3.setTransform(-4.2,-12.2095);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(12,1,1).p("ACHBOQg5jQjUBH");
	this.shape_4.setTransform(-4.45,-12.9575);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(12,1,1).p("Ah3ghQCyhlA9DJ");
	this.shape_5.setTransform(-2.95,-11.8945);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(12,1,1).p("AhogGQCPiCBCDD");
	this.shape_6.setTransform(-1.45,-11);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(12,1,1).p("AhZAWQBsieBHC7");
	this.shape_7.setTransform(0.05,-10.2231);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(12,1,1).p("ABLAoQhLizhKC7");
	this.shape_8.setTransform(1.55,-9.1661);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(12,1,1).p("AhUArQBeixBLCz");
	this.shape_9.setTransform(0.55,-9.694);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(12,1,1).p("AheAhQByimBLC0");
	this.shape_10.setTransform(-0.475,-9.8378);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(12,1,1).p("AhoAYQCGibBLCz");
	this.shape_11.setTransform(-1.475,-9.9839);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(12,1,1).p("AhzAPQCciRBLC0");
	this.shape_12.setTransform(-2.5,-10.147);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},59).to({state:[{t:this.shape}]},2).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape}]},1).wait(1));
	this.timeline.addTween(cjs.Tween.get(this.shape).wait(61).to({_off:true},1).wait(12).to({_off:false},0).wait(1));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_13_Layer_1copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#1A1714").ss(11,1,1).p("AjDBtQgHhPA2g9QAyg4BLgQQBMgQA7AkQBDAoARBa");
	this.shape.setTransform(0.0098,0.0288);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#1A1714").ss(11,1,1).p("AjDBvQgHhPA2g/QAyg7BLgQQBMgPA8AmQBCAqARBa");
	this.shape_1.setTransform(0.0098,-0.1623);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#1A1714").ss(11,1,1).p("AjDB1QgHhPA1hGQAzhEBLgNQBMgNA9AsQBBAuARBa");
	this.shape_2.setTransform(0.0092,-0.7232);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#1A1714").ss(11,1,1).p("AjDB/QgHhPA0hRQA0hTBMgIQBLgJA/A1QA/A2ARBa");
	this.shape_3.setTransform(0.0083,-1.7129);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#1A1714").ss(11,1,1).p("AjDB6QgHhPA1hMQAzhLBMgLQBLgLA+AyQBAAyARBa");
	this.shape_4.setTransform(0.0088,-1.2669);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#1A1714").ss(11,1,1).p("AjDB2QgHhPA1hHQAzhFBMgNQBLgMA9AtQBBAuARBa");
	this.shape_5.setTransform(0.0092,-0.8109);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#1A1714").ss(11,1,1).p("AjDBxQgHhPA2hCQAyg/BLgOQBMgOA8AoQBCAsARBa");
	this.shape_6.setTransform(0.0095,-0.3911);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#1A1714").ss(11,1,1).p("AjDB3QgHhPA1hIQAzhHBMgMQBLgMA9AuQBBAvARBa");
	this.shape_7.setTransform(0.0092,-0.9241);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#1A1714").ss(11,1,1).p("AjDBuQgHhPA2g+QAyg6BLgQQBMgPA7AlQBDApARBa");
	this.shape_8.setTransform(0.0098,-0.0788);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},34).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape}]},1).wait(27));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_13_Layer_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(12,1,1).p("AB6gYQhzhiiAC1");
	this.shape.setTransform(0,0.0069);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(12,1,1).p("Ah9AtQCJiXByBo");
	this.shape_1.setTransform(-0.4,-2.1746);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(12,1,1).p("AiBAhQCRh7ByBu");
	this.shape_2.setTransform(-0.775,-4.4215);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(12,1,1).p("AiFAKQCaheBxB1");
	this.shape_3.setTransform(-1.175,-5.7109);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(12,1,1).p("ACKApQhwh8ijBD");
	this.shape_4.setTransform(-1.575,-6.504);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(12,1,1).p("Ah1AEQB6hXBxB1");
	this.shape_5.setTransform(0.45,-5.8296);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(12,1,1).p("AhgAaQBQhsBxBu");
	this.shape_6.setTransform(2.5,-5.2502);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(12,1,1).p("AhMAkQAoiBBxBn");
	this.shape_7.setTransform(4.525,-3.4735);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(12,1,1).p("AA5gHQhyhiACCX");
	this.shape_8.setTransform(6.5483,-1.6708);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(12,1,1).p("AhFAxQAYidBzBi");
	this.shape_9.setTransform(5.25,-1.3258);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(12,1,1).p("AhSAzQAzijByBi");
	this.shape_10.setTransform(3.925,-0.9828);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(12,1,1).p("AhfA2QBNipByBi");
	this.shape_11.setTransform(2.625,-0.6701);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(12,1,1).p("AhsA4QBniuByBi");
	this.shape_12.setTransform(1.3,-0.3308);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},59).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape}]},1).wait(3));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_5_Layer_4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_4
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(11,1,1).p("AG8nPQCMEGhVDlQhJDGjYB9QjKB1jWgEQjggEhkiL");
	this.shape.setTransform(-2.5577,-22.5667);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(11,1,1).p("AoQF1QB5BzDegKQDWgJDHh/QDRiFBBjIQBOjoiWj/");
	this.shape_1.setTransform(-0.977,-20.5388);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(11,1,1).p("AoOGgQCLBfDcgVQDWgVDEiHQDLiMA7jJQBHjqiej7");
	this.shape_2.setTransform(0.365,-18.7091);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(11,1,1).p("AoNHCQCaBODbgeQDVgfDCiNQDGiRA3jKQBAjtikj3");
	this.shape_3.setTransform(1.5002,-17.1213);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(11,1,1).p("AoMHcQCmBBDZgmQDWgmDAiTQDCiUAzjMQA7juioj0");
	this.shape_4.setTransform(2.3781,-15.774);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(11,1,1).p("AoMHuQCvA4DYgrQDVgsC/iWQDAiYAwjMQA4jvirjy");
	this.shape_5.setTransform(3.0251,-14.7662);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(11,1,1).p("AoMH4QC0AzDXgvQDWguC+iZQC+iaAujMQA3jwiujx");
	this.shape_6.setTransform(3.4014,-14.1354);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(11,1,1).p("AGHoTQCvDwg2DwQguDNi+CZQi+CajVAwQjXAvi1gw");
	this.shape_7.setTransform(3.5381,-13.9206);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(11,1,1).p("AoNHGQCdBMDZgfQDWggDCiOQDFiSA2jKQBAjtikj3");
	this.shape_8.setTransform(1.6362,-16.9135);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(11,1,1).p("AoPGXQCIBjDcgTQDWgSDFiGQDMiKA9jIQBHjribj8");
	this.shape_9.setTransform(0.101,-19.097);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(11,1,1).p("AoQFyQB4B1DegJQDWgJDHh+QDRiFBCjIQBOjniVkA");
	this.shape_10.setTransform(-1.0741,-20.6677);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(11,1,1).p("AoRFXQBtCBDfgCQDWgBDIh5QDWiBBFjHQBSjmiQkD");
	this.shape_11.setTransform(-1.9124,-21.7485);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(11,1,1).p("AoSFGQBmCJDgACQDWADDJh2QDYh+BIjGQBUjliNkF");
	this.shape_12.setTransform(-2.3953,-22.3713);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_7}]},46).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape}]},1).wait(16));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_3_Layer_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(12,1,1).p("AHXG3QhEAAitgrQjmg4ilhYQjdh1g+iYQhLi9Ctjo");
	this.shape.setTransform(0.0054,0.025);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(12,1,1).p("Aljm2QioDoBMC8QA+CZDdB2QCkBXDnA5QCtAqBEAA");
	this.shape_1.setTransform(-0.0562,0);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(12,1,1).p("Alzm3QiZDqBNC7QBACaDdB1QCkBYDnA4QCtArBEAA");
	this.shape_2.setTransform(-0.2528,-0.05);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(12,1,1).p("AmOm3Qh/DsBOC3QBDCcDdB2QClBXDmA5QCtAqBFAA");
	this.shape_3.setTransform(-0.6125,-0.125);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(12,1,1).p("Amzm5QhaDvBQC0QBHCgDdB1QClBYDmA4QCtArBFAA");
	this.shape_4.setTransform(-1.2199,-0.25);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(12,1,1).p("Anfm6QgrDzBSCvQBNCjDdB2QClBXDmA5QCtAqBFAA");
	this.shape_5.setTransform(-2.3283,-0.4);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(12,1,1).p("AIJG9QhEAAitgrQjng4ilhYQjch1hVioQhVipgOj4");
	this.shape_6.setTransform(-4.95,-0.575);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(12,1,1).p("Alim2QipDoBLC8QA+CZDdB1QClBYDmA4QCtArBFAA");
	this.shape_7.setTransform(-0.0383,0.025);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(12,1,1).p("Alum2QieDpBMC7QBACZDdB2QCkBXDnA5QCtAqBEAA");
	this.shape_8.setTransform(-0.1842,-0.025);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(12,1,1).p("AmCm3QiLDrBOC5QBBCbDdB1QClBYDmA4QCtArBFAA");
	this.shape_9.setTransform(-0.4371,-0.075);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(12,1,1).p("Amdm4QhwDtBOC3QBFCdDdB1QClBYDmA4QCtArBFAA");
	this.shape_10.setTransform(-0.8424,-0.175);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(12,1,1).p("Am/m5QhODwBRCzQBJCgDdB1QCkBYDnA4QCtArBEAA");
	this.shape_11.setTransform(-1.4692,-0.275);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(12,1,1).p("Anlm6QgkDzBSCuQBPCkDdB2QCkBXDnA5QCtAqBEAA");
	this.shape_12.setTransform(-2.5613,-0.425);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(12,1,1).p("Anqm6QgeDzBTCuQBPCkDdB2QCkBXDnA5QCtAqBEAA");
	this.shape_13.setTransform(-2.7635,-0.425);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(12,1,1).p("AnIm5QhEDwBQCyQBLChDdB2QCkBXDnA5QCtAqBEAA");
	this.shape_14.setTransform(-1.6819,-0.3);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(12,1,1).p("Ampm4QhkDuBPC1QBGCeDdB2QClBXDmA4QCtArBFAA");
	this.shape_15.setTransform(-1.0433,-0.2);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(12,1,1).p("Al5m3QiTDqBNC7QBACaDdB1QClBYDmA4QCtArBFAA");
	this.shape_16.setTransform(-0.3234,-0.05);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(12,1,1).p("Alqm2QiiDoBMC8QA/CZDdB2QClBXDmA5QCtAqBFAA");
	this.shape_17.setTransform(-0.1384,0);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#000000").ss(12,1,1).p("Alhm2QiqDoBLC9QA+CYDdB1QClBYDmA4QCtArBFAA");
	this.shape_18.setTransform(-0.0336,0.025);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},19).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape}]},1).wait(29));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_1_Layer_6 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_6
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(11,1,1).p("AjehRQAHBpBMA6QBEA0BZgJQBagJA6hBQBAhHgIhp");
	this.shape.setTransform(33.0194,54.7484);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(11,1,1).p("AjehLQAHBpBUA1QBMAwBWgOQBWgOA0g8QA6hBgEhh");
	this.shape_1.setTransform(32.988,54.1331);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(11,1,1).p("AjehGQAHBpBaAwQBVAsBSgSQBSgRAwg5QAzg8AAhZ");
	this.shape_2.setTransform(32.9752,53.6113);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(11,1,1).p("AjehBQAHBoBgAsQBcAqBOgWQBPgVAsg1QAug4ADhS");
	this.shape_3.setTransform(32.975,53.1919);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(11,1,1).p("Ajeg+QAHBpBlAoQBiAnBLgZQBNgYAngzQAqgzAGhN");
	this.shape_4.setTransform(32.975,52.8264);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(11,1,1).p("Ajeg7QAHBpBpAlQBnAkBJgbQBKgbAlgwQAmgwAIhI");
	this.shape_5.setTransform(32.975,52.5286);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(11,1,1).p("Ajeg4QAHBoBtAjQBqAiBIgdQBIgdAiguQAjguAKhD");
	this.shape_6.setTransform(32.975,52.2852);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(11,1,1).p("Ajeg3QAHBpBvAhQBuAgBHgfQBGgfAggsQAhgrALhB");
	this.shape_7.setTransform(32.975,52.1067);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(11,1,1).p("Ajeg1QAHBoBxAgQBwAfBGggQBFggAfgrQAfgqAMg+");
	this.shape_8.setTransform(32.975,51.9813);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(11,1,1).p("Ajeg1QAHBpByAfQBxAeBGghQBEggAegrQAegpANg9");
	this.shape_9.setTransform(32.975,51.9075);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(11,1,1).p("Ajeg0QAHBoBzAfQBxAeBFghQBFghAdgqQAegpANg8");
	this.shape_10.setTransform(32.975,51.8813);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(11,1,1).p("Ajeg5QAHBoBrAkQBqAjBIgdQBJgcAjgvQAkguAJhF");
	this.shape_11.setTransform(32.975,52.3696);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(11,1,1).p("AjehCQAHBpBfAtQBbApBPgVQBPgVAsg2QAvg4ADhT");
	this.shape_12.setTransform(32.975,53.2306);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(11,1,1).p("AjehJQAHBoBWA0QBQAuBUgPQBUgPAzg7QA4g/gDhe");
	this.shape_13.setTransform(32.9827,53.9579);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(11,1,1).p("AjehMQAHBpBSA2QBMAwBWgNQBWgNA2g9QA6hCgEhi");
	this.shape_14.setTransform(32.9911,54.2374);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(11,1,1).p("AjehOQAHBoBPA4QBIAyBYgLQBYgLA3g/QA9hEgGhl");
	this.shape_15.setTransform(33.0019,54.4575);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(11,1,1).p("AjehQQAHBpBNA5QBGAzBZgKQBZgKA5hAQA+hGgHhn");
	this.shape_16.setTransform(33.0103,54.6146);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(11,1,1).p("AjehRQAHBpBMA6QBFA0BZgKQBagJA5hBQBAhGgIhp");
	this.shape_17.setTransform(33.0194,54.7234);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},14).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_10}]},30).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape}]},1).wait(11));

}).prototype = p = new cjs.MovieClip();


(lib.Scene_1_Layer_7 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_7
	this.instance = new lib.leafs();
	this.instance.parent = this;
	this.instance.setTransform(445,0);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Scene_1_Layer_7, null, null);


(lib.Scene_1_Layer_2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.instance = new lib.bg_pack();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Scene_1_Layer_2, null, null);


(lib.Symbol4_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_3
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(12,1,1).p("AhFAAICLAA");
	this.shape_1.setTransform(13.3,14.95);

	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(1));

	// Layer_2
	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(12,1,1).p("AgiCEQgeiSBqh1");
	this.shape_2.setTransform(11.8777,-0.1);

	this.timeline.addTween(cjs.Tween.get(this.shape_2).wait(1));

	// Layer_1
	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(12,1,1).p("AAbCWQhjiMBSif");
	this.shape_3.setTransform(-0.0322,-0.025);

	this.timeline.addTween(cjs.Tween.get(this.shape_3).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol4_1, new cjs.Rectangle(-8.7,-21,35,42), null);


(lib.Symbol20 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Symbol14();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol20, new cjs.Rectangle(-86,-78,172,156), null);


(lib.Symbol13 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_74 = function() {
		this.___loopingOver___ = true;
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(74).call(this.frame_74).wait(1));

	// Layer_3_obj_
	this.Layer_3 = new lib.Symbol_13_Layer_3copy();
	this.Layer_3.name = "Layer_3";
	this.Layer_3.parent = this;
	this.Layer_3.setTransform(48.2,37.4,1,1,0,0,0,48.2,37.4);
	this.Layer_3.depth = 0;
	this.Layer_3.isAttachedToCamera = 0
	this.Layer_3.isAttachedToMask = 0
	this.Layer_3.layerDepth = 0
	this.Layer_3.layerIndex = 0
	this.Layer_3.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_3).wait(75));

	// Layer_2_obj_
	this.Layer_2 = new lib.Symbol_13_Layer_2copy();
	this.Layer_2.name = "Layer_2";
	this.Layer_2.parent = this;
	this.Layer_2.setTransform(73.3,-14.2,1,1,0,0,0,73.3,-14.2);
	this.Layer_2.depth = 0;
	this.Layer_2.isAttachedToCamera = 0
	this.Layer_2.isAttachedToMask = 0
	this.Layer_2.layerDepth = 0
	this.Layer_2.layerIndex = 1
	this.Layer_2.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_2).wait(75));

	// Layer_1_obj_
	this.Layer_1 = new lib.Symbol_13_Layer_1copy();
	this.Layer_1.name = "Layer_1";
	this.Layer_1.parent = this;
	this.Layer_1.depth = 0;
	this.Layer_1.isAttachedToCamera = 0
	this.Layer_1.isAttachedToMask = 0
	this.Layer_1.layerDepth = 0
	this.Layer_1.layerIndex = 2
	this.Layer_1.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_1).wait(75));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-25.1,-33.5,123.5,88.9);


(lib.Symbol8 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_4 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("AsBpNIT3j6IEMUmI3vFpg");
	mask.setTransform(-244.925,53.95);

	// Layer_2
	this.instance = new lib.Symbol7();
	this.instance.parent = this;
	this.instance.setTransform(-208.45,43.55,1,1,0,-27.7252,152.2748,0.1,0.1);

	var maskedShapeInstanceList = [this.instance];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	// Layer_3 (mask)
	var mask_1 = new cjs.Shape();
	mask_1._off = true;
	mask_1.graphics.p("ApXmWIRpneIBGY/IurCqg");
	mask_1.setTransform(25.025,-23.475);

	// Layer_1
	this.instance_1 = new lib.Symbol7();
	this.instance_1.parent = this;
	this.instance_1.setTransform(7.8,0);

	var maskedShapeInstanceList = [this.instance_1];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask_1;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol8, new cjs.Rectangle(-266,-51.9,311.6,158.9), null);


(lib.Symbol5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Symbol_3 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("AiMCNQg7g7AAhSQAAhRA7g7QA6g7BSAAQBTAAA6A7QA7A7gBBRQABBSg7A7Qg6A6hTABQhSgBg6g6g");
	mask.setTransform(-5.15,13.15);

	// Layer_5
	this.instance = new lib.Symbol6();
	this.instance.parent = this;
	this.instance.setTransform(5.15,-13.15);

	var maskedShapeInstanceList = [this.instance];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(14).to({x:8.15,y:-24.55},10,cjs.Ease.get(1)).wait(30).to({x:5.15,y:-13.15},11,cjs.Ease.get(1)).wait(10));

	// Symbol_2
	this.instance_1 = new lib.Symbol2();
	this.instance_1.parent = this;
	this.instance_1.setTransform(-6.3,7.05);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(14).to({x:-11.7,y:17.85},10,cjs.Ease.get(1)).wait(10).to({x:-5.4,y:14.55},10,cjs.Ease.get(1)).wait(10).to({x:-6.3,y:7.05},11,cjs.Ease.get(1)).wait(10));

	// Symbol_4
	this.instance_2 = new lib.Symbol4();
	this.instance_2.parent = this;
	this.instance_2.setTransform(-5.15,13.15);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(75));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-25.1,-6.8,39.900000000000006,39.9);


(lib.Symbol_15_Layer_5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_5
	this.instance = new lib.Symbol18();
	this.instance.parent = this;
	this.instance.setTransform(4.7,10.8,1,1,0,0,0,-2.5,11.7);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(50).to({scaleX:0.1391,x:4.75},3).to({scaleX:1,x:4.7},3).to({scaleX:0.1391,x:4.75},3).to({scaleX:1,x:4.7},3).to({scaleX:0.1391,x:4.75},3).to({scaleX:1,x:4.7},5).wait(5));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_15_Layer_4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_4
	this.instance = new lib.Symbol17();
	this.instance.parent = this;
	this.instance.setTransform(5.6,11.2,1,1,0,0,0,-1.6,12.1);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(50).to({regX:-1.2,regY:12,scaleX:0.3744,skewY:-76.7409,x:5.8,y:11.15},3).to({regX:-2.5,regY:11.7,scaleX:1,skewY:0,x:4.7,y:10.8},3).to({regX:-1.2,regY:12,scaleX:0.3744,skewY:-76.7409,x:5.8,y:11.15},3).to({regX:-2.5,regY:11.7,scaleX:1,skewY:0,x:4.7,y:10.8},3).to({regX:-1.2,regY:12,scaleX:0.3744,skewY:-76.7409,x:5.8,y:11.15},3).to({regX:-2.5,regY:11.7,scaleX:1,skewY:0,x:4.7,y:10.8},5).wait(5));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_15_Layer_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Symbol16();
	this.instance.parent = this;
	this.instance.setTransform(5.4,13,1,1,29.9992,0,0,5.4,13);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(75));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_3_Layer_2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.instance = new lib.Symbol4_1();
	this.instance.parent = this;
	this.instance.setTransform(-37.3,-40.7,1,1,0,0,0,-0.2,18.3);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(19).to({rotation:-34.2068,x:-57.3,y:-44.3},6,cjs.Ease.get(-1)).to({rotation:0,x:-37.3,y:-40.7},6,cjs.Ease.get(1)).to({rotation:-34.2068,x:-57.3,y:-44.3},7,cjs.Ease.get(-1)).to({rotation:0,x:-37.3,y:-40.7},8,cjs.Ease.get(1)).wait(29));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_1_Layer_5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_5
	this.instance = new lib.Symbol5();
	this.instance.parent = this;
	this.instance.setTransform(5.15,-13.15);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(75));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_1_Layer_4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_4
	this.instance = new lib.Symbol5();
	this.instance.parent = this;
	this.instance.setTransform(47.75,-2.65);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(75));

}).prototype = p = new cjs.MovieClip();


(lib.Scene_1_Layer_10 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_10
	this.instance = new lib.Symbol8();
	this.instance.parent = this;
	this.instance.setTransform(1028.5,457.95);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Scene_1_Layer_10, null, null);


(lib.Scene_1_Layer_8 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_8
	this.instance = new lib.Symbol13();
	this.instance.parent = this;
	this.instance.setTransform(874.85,407);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Scene_1_Layer_8, null, null);


(lib.Symbol13_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_74 = function() {
		this.___loopingOver___ = true;
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(74).call(this.frame_74).wait(1));

	// Layer_2_obj_
	this.Layer_2_1 = new lib.Symbol_13_Layer_2();
	this.Layer_2_1.name = "Layer_2_1";
	this.Layer_2_1.parent = this;
	this.Layer_2_1.setTransform(-3.5,-10.3,1,1,0,0,0,-3.5,-10.3);
	this.Layer_2_1.depth = 0;
	this.Layer_2_1.isAttachedToCamera = 0
	this.Layer_2_1.isAttachedToMask = 0
	this.Layer_2_1.layerDepth = 0
	this.Layer_2_1.layerIndex = 0
	this.Layer_2_1.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_2_1).wait(75));

	// Layer_1_obj_
	this.Layer_1_1 = new lib.Symbol_13_Layer_1();
	this.Layer_1_1.name = "Layer_1_1";
	this.Layer_1_1.parent = this;
	this.Layer_1_1.depth = 0;
	this.Layer_1_1.isAttachedToCamera = 0
	this.Layer_1_1.isAttachedToMask = 0
	this.Layer_1_1.layerDepth = 0
	this.Layer_1_1.layerIndex = 1
	this.Layer_1_1.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_1_1).wait(75));

	// Layer_3_obj_
	this.Layer_3_1 = new lib.Symbol_13_Layer_3();
	this.Layer_3_1.name = "Layer_3_1";
	this.Layer_3_1.parent = this;
	this.Layer_3_1.setTransform(10.3,-12.1,1,1,0,0,0,10.3,-12.1);
	this.Layer_3_1.depth = 0;
	this.Layer_3_1.isAttachedToCamera = 0
	this.Layer_3_1.isAttachedToMask = 0
	this.Layer_3_1.layerDepth = 0
	this.Layer_3_1.layerIndex = 2
	this.Layer_3_1.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_3_1).wait(75));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-23.9,-28.1,46.5,40);


(lib.Symbol3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		/* Stop at This Frame
		The  timeline will stop/pause at the frame where you insert this code.
		Can also be used to stop/pause the timeline of movieclips.
		*/
		
		this.stop();
	}
	this.frame_74 = function() {
		this.___loopingOver___ = true;
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(74).call(this.frame_74).wait(1));

	// Layer_2_obj_
	this.Layer_2 = new lib.Symbol_3_Layer_2();
	this.Layer_2.name = "Layer_2";
	this.Layer_2.parent = this;
	this.Layer_2.setTransform(-28.4,-59.1,1,1,0,0,0,-28.4,-59.1);
	this.Layer_2.depth = 0;
	this.Layer_2.isAttachedToCamera = 0
	this.Layer_2.isAttachedToMask = 0
	this.Layer_2.layerDepth = 0
	this.Layer_2.layerIndex = 0
	this.Layer_2.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_2).wait(75));

	// Layer_1_obj_
	this.Layer_1 = new lib.Symbol_3_Layer_1();
	this.Layer_1.name = "Layer_1";
	this.Layer_1.parent = this;
	this.Layer_1.depth = 0;
	this.Layer_1.isAttachedToCamera = 0
	this.Layer_1.isAttachedToMask = 0
	this.Layer_1.layerDepth = 0
	this.Layer_1.layerIndex = 1
	this.Layer_1.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_1).wait(75));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-86.5,-87.9,139.7,137.8);


(lib.Symbol23 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("AgvC8IAAhaIgViAIADhZIAJhRQAHgmALgOIBCg/IAbBNIAOBlIgbDaIgSDSIg6AZg");
	mask.setTransform(5.075,39.75);

	// Layer_1
	this.instance = new lib.Symbol20();
	this.instance.parent = this;
	this.instance.setTransform(4.6,32.4,1,1,-31.7079,0,0,-13.1,30);

	var maskedShapeInstanceList = [this.instance];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol23, new cjs.Rectangle(-1.8,8.2,13.8,63.2), null);


(lib.Symbol22 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("AoSL6Igi4eIRpB8IAALSIgSBGIgOBPIAFBNIASBsIgDBjIhLFKg");
	mask.setTransform(-54.85,9.825);

	// Layer_1
	this.instance = new lib.Symbol20();
	this.instance.parent = this;
	this.instance.setTransform(4.6,32.4,1,1,-31.7079,0,0,-13.1,30);

	var maskedShapeInstanceList = [this.instance];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol22, new cjs.Rectangle(-111.3,-70.6,112.89999999999999,160.89999999999998), null);


(lib.Symbol21 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("AkpNNIgileIAbkfIgPgiIAdvDII4hCIA2avg");
	mask.setTransform(42,3.825);

	// Layer_1
	this.instance = new lib.Symbol20();
	this.instance.parent = this;
	this.instance.setTransform(4.6,32.4,1,1,-31.7079,0,0,-13.1,30);

	var maskedShapeInstanceList = [this.instance];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol21, new cjs.Rectangle(8.8,-81.7,66.4,171.10000000000002), null);


(lib.Symbol19copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_5
	this.instance = new lib.Symbol22();
	this.instance.parent = this;
	this.instance.setTransform(-13.4,26.8,1,1,0,0,0,4.3,29.2);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({regX:4.1,scaleX:0.3969,skewY:73.9281,x:-13.5,y:26.85},4).to({regX:4.3,scaleX:1,skewY:0,x:-13.4,y:26.8},4).wait(1));

	// Layer_4
	this.instance_1 = new lib.Symbol21();
	this.instance_1.parent = this;
	this.instance_1.setTransform(-13.4,26.8,1,1,0,0,0,4.3,29.2);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).to({regX:4.2,scaleX:0.1316,x:-13.5},4).to({regX:4.3,scaleX:1,x:-13.4},4).wait(1));

	// Layer_1
	this.instance_2 = new lib.Symbol23();
	this.instance_2.parent = this;
	this.instance_2.setTransform(-17.7,-2.4);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(9));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-131.8,-117,228.3,226.2);


(lib.Symbol15 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_74 = function() {
		this.___loopingOver___ = true;
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(74).call(this.frame_74).wait(1));

	// Layer_5_obj_
	this.Layer_5 = new lib.Symbol_15_Layer_5();
	this.Layer_5.name = "Layer_5";
	this.Layer_5.parent = this;
	this.Layer_5.setTransform(7.2,-0.9,1,1,0,0,0,7.2,-0.9);
	this.Layer_5.depth = 0;
	this.Layer_5.isAttachedToCamera = 0
	this.Layer_5.isAttachedToMask = 0
	this.Layer_5.layerDepth = 0
	this.Layer_5.layerIndex = 0
	this.Layer_5.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_5).wait(75));

	// Layer_4_obj_
	this.Layer_4 = new lib.Symbol_15_Layer_4();
	this.Layer_4.name = "Layer_4";
	this.Layer_4.parent = this;
	this.Layer_4.setTransform(7.2,-0.9,1,1,0,0,0,7.2,-0.9);
	this.Layer_4.depth = 0;
	this.Layer_4.isAttachedToCamera = 0
	this.Layer_4.isAttachedToMask = 0
	this.Layer_4.layerDepth = 0
	this.Layer_4.layerIndex = 1
	this.Layer_4.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_4).wait(75));

	// Layer_1_obj_
	this.Layer_1 = new lib.Symbol_15_Layer_1();
	this.Layer_1.name = "Layer_1";
	this.Layer_1.parent = this;
	this.Layer_1.setTransform(7.2,-1,1,1,0,0,0,7.2,-1);
	this.Layer_1.depth = 0;
	this.Layer_1.isAttachedToCamera = 0
	this.Layer_1.isAttachedToMask = 0
	this.Layer_1.layerDepth = 0
	this.Layer_1.layerIndex = 2
	this.Layer_1.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_1).wait(75));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-42.3,-67.5,99,132.4);


(lib.Symbol1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_74 = function() {
		this.___loopingOver___ = true;
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(74).call(this.frame_74).wait(1));

	// Layer_6_obj_
	this.Layer_6 = new lib.Symbol_1_Layer_6();
	this.Layer_6.name = "Layer_6";
	this.Layer_6.parent = this;
	this.Layer_6.setTransform(33,54.8,1,1,0,0,0,33,54.8);
	this.Layer_6.depth = 0;
	this.Layer_6.isAttachedToCamera = 0
	this.Layer_6.isAttachedToMask = 0
	this.Layer_6.layerDepth = 0
	this.Layer_6.layerIndex = 0
	this.Layer_6.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_6).wait(75));

	// Layer_4_obj_
	this.Layer_4 = new lib.Symbol_1_Layer_4();
	this.Layer_4.name = "Layer_4";
	this.Layer_4.parent = this;
	this.Layer_4.setTransform(47.8,-2.6,1,1,0,0,0,47.8,-2.6);
	this.Layer_4.depth = 0;
	this.Layer_4.isAttachedToCamera = 0
	this.Layer_4.isAttachedToMask = 0
	this.Layer_4.layerDepth = 0
	this.Layer_4.layerIndex = 1
	this.Layer_4.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_4).wait(75));

	// Layer_5_obj_
	this.Layer_5 = new lib.Symbol_1_Layer_5();
	this.Layer_5.name = "Layer_5";
	this.Layer_5.parent = this;
	this.Layer_5.setTransform(5.2,-13.2,1,1,0,0,0,5.2,-13.2);
	this.Layer_5.depth = 0;
	this.Layer_5.isAttachedToCamera = 0
	this.Layer_5.isAttachedToMask = 0
	this.Layer_5.layerDepth = 0
	this.Layer_5.layerIndex = 2
	this.Layer_5.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_5).wait(75));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-19.9,-46.2,92.80000000000001,119.10000000000001);


(lib.Symbol_25_Layer_2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.instance = new lib.Symbol19copy();
	this.instance.parent = this;
	this.instance.setTransform(20.85,26.6,1.0699,1.0699,0,0,0,-11.8,28.4);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1).to({regX:-35.8,regY:-14.5,scaleX:1.0625,scaleY:1.0625,x:-4.35,y:-18.95},0).wait(1).to({scaleX:1.035,scaleY:1.035,x:-2.5,y:-17.8},0).wait(1).to({scaleX:1.0074,scaleY:1.0074,x:-0.65,y:-16.6},0).wait(1).to({regX:-12,regY:28.4,scaleX:1,scaleY:1,x:23.75,y:26.55},0).wait(1).to({regX:-35.8,regY:-14.5,scaleX:1.0074,scaleY:1.0074,x:-0.55,y:-16.65},0).wait(1).to({scaleX:1.035,scaleY:1.035,x:-2.4,y:-17.85},0).wait(1).to({scaleX:1.0625,scaleY:1.0625,x:-4.25,y:-19},0).wait(1).to({regX:-11.8,regY:28.4,scaleX:1.0699,scaleY:1.0699,x:20.85,y:26.6},0).wait(1));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_14_Layer_4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_4
	this.instance = new lib.Symbol3();
	this.instance.parent = this;
	this.instance.setTransform(47.1,59,1,1,0,0,0,47.1,43.9);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({_off:true},1).wait(74));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_5_Layer_2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.instance = new lib.Symbol13_1();
	this.instance.parent = this;
	this.instance.setTransform(-53.55,13.1,1,1,6.7397,0,0,15,-0.4);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({regY:-0.5,rotation:0,x:-46.3,y:38},7,cjs.Ease.get(1)).wait(46).to({regY:-0.4,rotation:6.7397,x:-53.55,y:13.1},6,cjs.Ease.get(1)).wait(16));

}).prototype = p = new cjs.MovieClip();


(lib.Scene_1_Layer_6 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_6
	this.instance = new lib.Symbol1();
	this.instance.parent = this;
	this.instance.setTransform(671.5,232.2);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Scene_1_Layer_6, null, null);


(lib.Symbol14_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		/* Stop at This Frame
		The  timeline will stop/pause at the frame where you insert this code.
		Can also be used to stop/pause the timeline of movieclips.
		*/
		
		this.stop();
	}
	this.frame_74 = function() {
		this.___loopingOver___ = true;
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(74).call(this.frame_74).wait(1));

	// Layer_5 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	var mask_graphics_0 = new cjs.Graphics().p("A1QInITR7iIXQK0IqMbDg");

	this.timeline.addTween(cjs.Tween.get(mask).to({graphics:mask_graphics_0,x:-49.475,y:-18.975}).wait(1).to({graphics:null,x:0,y:0}).wait(74));

	// Layer_4_obj_
	this.Layer_4 = new lib.Symbol_14_Layer_4();
	this.Layer_4.name = "Layer_4";
	this.Layer_4.parent = this;
	this.Layer_4.depth = 0;
	this.Layer_4.isAttachedToCamera = 0
	this.Layer_4.isAttachedToMask = 0
	this.Layer_4.layerDepth = 0
	this.Layer_4.layerIndex = 0
	this.Layer_4.maskLayerName = 0

	var maskedShapeInstanceList = [this.Layer_4];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.Layer_4).wait(75));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-185.6,-140.2,272.3,242.5);


(lib.Symbol5_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_74 = function() {
		this.___loopingOver___ = true;
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(74).call(this.frame_74).wait(1));

	// Layer_3 (mask)
	var mask_1 = new cjs.Shape();
	mask_1._off = true;
	mask_1.graphics.p("A2jGAIPj41ISBO7ICblCIJID+IrGX1g");
	mask_1.setTransform(-31.05,-10.15);

	// Layer_2_obj_
	this.Layer_2 = new lib.Symbol_5_Layer_2();
	this.Layer_2.name = "Layer_2";
	this.Layer_2.parent = this;
	this.Layer_2.setTransform(-68.1,3.6,1,1,0,0,0,-68.1,3.6);
	this.Layer_2.depth = 0;
	this.Layer_2.isAttachedToCamera = 0
	this.Layer_2.isAttachedToMask = 0
	this.Layer_2.layerDepth = 0
	this.Layer_2.layerIndex = 0
	this.Layer_2.maskLayerName = 0

	var maskedShapeInstanceList = [this.Layer_2];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask_1;
	}

	this.timeline.addTween(cjs.Tween.get(this.Layer_2).wait(75));

	// Layer_4_obj_
	this.Layer_4 = new lib.Symbol_5_Layer_4();
	this.Layer_4.name = "Layer_4";
	this.Layer_4.parent = this;
	this.Layer_4.setTransform(-2.6,-22.6,1,1,0,0,0,-2.6,-22.6);
	this.Layer_4.depth = 0;
	this.Layer_4.isAttachedToCamera = 0
	this.Layer_4.isAttachedToMask = 0
	this.Layer_4.layerDepth = 0
	this.Layer_4.layerIndex = 1
	this.Layer_4.maskLayerName = 0

	var maskedShapeInstanceList = [this.Layer_4];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask_1;
	}

	this.timeline.addTween(cjs.Tween.get(this.Layer_4).wait(75));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-90.5,-74.4,152,124.80000000000001);


(lib.Symbol25 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_8 = function() {
		this.___loopingOver___ = true;
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(8).call(this.frame_8).wait(1));

	// Layer_2_obj_
	this.Layer_2 = new lib.Symbol_25_Layer_2();
	this.Layer_2.name = "Layer_2";
	this.Layer_2.parent = this;
	this.Layer_2.setTransform(14.6,-6.4,1,1,0,0,0,14.6,-6.4);
	this.Layer_2.depth = 0;
	this.Layer_2.isAttachedToCamera = 0
	this.Layer_2.isAttachedToMask = 0
	this.Layer_2.layerDepth = 0
	this.Layer_2.layerIndex = 0
	this.Layer_2.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_2).wait(1).to({regX:-4.8,regY:-19.4,x:-4.8,y:-19.4},0).wait(3).to({regX:14.6,regY:-6.4,x:14.6,y:-6.4},0).wait(1).to({regX:-4.8,regY:-19.4,x:-4.8,y:-19.4},0).wait(3).to({regX:14.6,regY:-6.4,x:14.6,y:-6.4},0).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-107.6,-125.7,244.29999999999998,238.7);


(lib.Symbol24 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Symbol15();
	this.instance.parent = this;
	this.instance.setTransform(-1.3,13.5,1,1,-29.9999,0,0,5.1,10.8);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol24, new cjs.Rectangle(-72.2,-64.2,133.9,133), null);


(lib.Symbol_10_Layer_6 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_6
	this.instance = new lib.Symbol14_1("single",0);
	this.instance.parent = this;
	this.instance.setTransform(-90.65,-64.25,1,1,-8.9672,0,0,-5,0);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol_10_Layer_6, null, null);


(lib.Symbol_10_Layer_5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_5
	this.instance = new lib.Symbol5_1();
	this.instance.parent = this;
	this.instance.setTransform(124.75,2.45,1,1,-14.9992,0,0,44.6,-46);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol_10_Layer_5, null, null);


(lib.Scene_1_Layer_5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_5
	this.instance = new lib.Symbol25();
	this.instance.parent = this;
	this.instance.setTransform(409.35,443.55,1,1,0,0,0,18.1,-4.3);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Scene_1_Layer_5, null, null);


(lib.Scene_1_Layer_4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_4
	this.instance = new lib.Symbol24();
	this.instance.parent = this;
	this.instance.setTransform(511.35,200.15,1,1,0,0,0,-5.4,2.3);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Scene_1_Layer_4, null, null);


(lib.Symbol10 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_5_obj_
	this.Layer_5 = new lib.Symbol_10_Layer_5();
	this.Layer_5.name = "Layer_5";
	this.Layer_5.parent = this;
	this.Layer_5.setTransform(61,56.7,1,1,0,0,0,61,56.7);
	this.Layer_5.depth = 0;
	this.Layer_5.isAttachedToCamera = 0
	this.Layer_5.isAttachedToMask = 0
	this.Layer_5.layerDepth = 0
	this.Layer_5.layerIndex = 0
	this.Layer_5.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_5).wait(1));

	// Layer_6_obj_
	this.Layer_6 = new lib.Symbol_10_Layer_6();
	this.Layer_6.name = "Layer_6";
	this.Layer_6.parent = this;
	this.Layer_6.setTransform(-137.6,-76.1,1,1,0,0,0,-137.6,-76.1);
	this.Layer_6.depth = 0;
	this.Layer_6.isAttachedToCamera = 0
	this.Layer_6.isAttachedToMask = 0
	this.Layer_6.layerDepth = 0
	this.Layer_6.layerIndex = 1
	this.Layer_6.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_6).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol10, new cjs.Rectangle(-138.6,-124.2,370.2,334.8), null);


(lib.Scene_1_Layer_9 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_9
	this.instance = new lib.Symbol10();
	this.instance.parent = this;
	this.instance.setTransform(632.7,333.95);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Scene_1_Layer_9, null, null);


// stage content:
(lib.June_main = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	this.___GetDepth___ = function(obj) {
		var depth = obj.depth;
		var cameraObj = this.___camera___instance;
		if(cameraObj && cameraObj.depth && obj.isAttachedToCamera)
		{
			depth += depth + cameraObj.depth;
		}
		return depth;
		}
	this.___needSorting___ = function() {
		for (var i = 0; i < this.getNumChildren() - 1; i++)
		{
			var prevDepth = this.___GetDepth___(this.getChildAt(i));
			var nextDepth = this.___GetDepth___(this.getChildAt(i + 1));
			if (prevDepth < nextDepth)
				return true;
		}
		return false;
	}
	this.___sortFunction___ = function(obj1, obj2) {
		return (this.exportRoot.___GetDepth___(obj2) - this.exportRoot.___GetDepth___(obj1));
	}
	this.on('tick', function (event){
		var curTimeline = event.currentTarget;
		if (curTimeline.___needSorting___()){
			this.sortChildren(curTimeline.___sortFunction___);
		}
	});

	// Layer_7_obj_
	this.Layer_7 = new lib.Scene_1_Layer_7();
	this.Layer_7.name = "Layer_7";
	this.Layer_7.parent = this;
	this.Layer_7.setTransform(762.5,202.5,1,1,0,0,0,762.5,202.5);
	this.Layer_7.depth = 0;
	this.Layer_7.isAttachedToCamera = 0
	this.Layer_7.isAttachedToMask = 0
	this.Layer_7.layerDepth = 0
	this.Layer_7.layerIndex = 0
	this.Layer_7.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_7).wait(1));

	// Layer_6_obj_
	this.Layer_6 = new lib.Scene_1_Layer_6();
	this.Layer_6.name = "Layer_6";
	this.Layer_6.parent = this;
	this.Layer_6.setTransform(698,245.5,1,1,0,0,0,698,245.5);
	this.Layer_6.depth = 0;
	this.Layer_6.isAttachedToCamera = 0
	this.Layer_6.isAttachedToMask = 0
	this.Layer_6.layerDepth = 0
	this.Layer_6.layerIndex = 1
	this.Layer_6.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_6).wait(1));

	// Layer_5_obj_
	this.Layer_5 = new lib.Scene_1_Layer_5();
	this.Layer_5.name = "Layer_5";
	this.Layer_5.parent = this;
	this.Layer_5.setTransform(405.8,441.5,1,1,0,0,0,405.8,441.5);
	this.Layer_5.depth = 0;
	this.Layer_5.isAttachedToCamera = 0
	this.Layer_5.isAttachedToMask = 0
	this.Layer_5.layerDepth = 0
	this.Layer_5.layerIndex = 2
	this.Layer_5.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_5).wait(1));

	// Layer_4_obj_
	this.Layer_4 = new lib.Scene_1_Layer_4();
	this.Layer_4.name = "Layer_4";
	this.Layer_4.parent = this;
	this.Layer_4.setTransform(511.4,200.2,1,1,0,0,0,511.4,200.2);
	this.Layer_4.depth = 0;
	this.Layer_4.isAttachedToCamera = 0
	this.Layer_4.isAttachedToMask = 0
	this.Layer_4.layerDepth = 0
	this.Layer_4.layerIndex = 3
	this.Layer_4.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_4).wait(1));

	// Layer_8_obj_
	this.Layer_8 = new lib.Scene_1_Layer_8();
	this.Layer_8.name = "Layer_8";
	this.Layer_8.parent = this;
	this.Layer_8.setTransform(911.5,419.4,1,1,0,0,0,911.5,419.4);
	this.Layer_8.depth = 0;
	this.Layer_8.isAttachedToCamera = 0
	this.Layer_8.isAttachedToMask = 0
	this.Layer_8.layerDepth = 0
	this.Layer_8.layerIndex = 4
	this.Layer_8.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_8).wait(1));

	// Layer_10_obj_
	this.Layer_10 = new lib.Scene_1_Layer_10();
	this.Layer_10.name = "Layer_10";
	this.Layer_10.parent = this;
	this.Layer_10.setTransform(910,470.9,1,1,0,0,0,910,470.9);
	this.Layer_10.depth = 0;
	this.Layer_10.isAttachedToCamera = 0
	this.Layer_10.isAttachedToMask = 0
	this.Layer_10.layerDepth = 0
	this.Layer_10.layerIndex = 5
	this.Layer_10.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_10).wait(1));

	// Layer_9_obj_
	this.Layer_9 = new lib.Scene_1_Layer_9();
	this.Layer_9.name = "Layer_9";
	this.Layer_9.parent = this;
	this.Layer_9.setTransform(603.1,330.7,1,1,0,0,0,603.1,330.7);
	this.Layer_9.depth = 0;
	this.Layer_9.isAttachedToCamera = 0
	this.Layer_9.isAttachedToMask = 0
	this.Layer_9.layerDepth = 0
	this.Layer_9.layerIndex = 6
	this.Layer_9.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_9).wait(1));

	// Layer_2_obj_
	this.Layer_2 = new lib.Scene_1_Layer_2();
	this.Layer_2.name = "Layer_2";
	this.Layer_2.parent = this;
	this.Layer_2.setTransform(540,340,1,1,0,0,0,540,340);
	this.Layer_2.depth = 0;
	this.Layer_2.isAttachedToCamera = 0
	this.Layer_2.isAttachedToMask = 0
	this.Layer_2.layerDepth = 0
	this.Layer_2.layerIndex = 7
	this.Layer_2.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_2).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(540,340,573.5,340);
// library properties:
lib.properties = {
	id: 'D55B5E3E6C6F5A4FBA4088D2C18C4C26',
	width: 1080,
	height: 680,
	fps: 24,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: [
		{src:"assets/images/bg_pack46.jpg", id:"bg_pack"},
		{src:"assets/images/butterfly_white46.png", id:"butterfly_white"},
		{src:"assets/images/butterfly_yellow46.png", id:"butterfly_yellow"},
		{src:"assets/images/leafs46.png", id:"leafs"}
	],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['D55B5E3E6C6F5A4FBA4088D2C18C4C26'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}


// Layer depth API : 

AdobeAn.Layer = new function() {
	this.getLayerZDepth = function(timeline, layerName)
	{
		if(layerName === "Camera")
		layerName = "___camera___instance";
		var script = "if(timeline." + layerName + ") timeline." + layerName + ".depth; else 0;";
		return eval(script);
	}
	this.setLayerZDepth = function(timeline, layerName, zDepth)
	{
		const MAX_zDepth = 10000;
		const MIN_zDepth = -5000;
		if(zDepth > MAX_zDepth)
			zDepth = MAX_zDepth;
		else if(zDepth < MIN_zDepth)
			zDepth = MIN_zDepth;
		if(layerName === "Camera")
		layerName = "___camera___instance";
		var script = "if(timeline." + layerName + ") timeline." + layerName + ".depth = " + zDepth + ";";
		eval(script);
	}
	this.removeLayer = function(timeline, layerName)
	{
		if(layerName === "Camera")
		layerName = "___camera___instance";
		var script = "if(timeline." + layerName + ") timeline.removeChild(timeline." + layerName + ");";
		eval(script);
	}
	this.addNewLayer = function(timeline, layerName, zDepth)
	{
		if(layerName === "Camera")
		layerName = "___camera___instance";
		zDepth = typeof zDepth !== 'undefined' ? zDepth : 0;
		var layer = new createjs.MovieClip();
		layer.name = layerName;
		layer.depth = zDepth;
		layer.layerIndex = 0;
		timeline.addChild(layer);
	}
}


})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;

$(function () {
    var canvas, stage, exportRoot, anim_container, dom_overlay_container, fnStartAnimation;
    function init() {
        canvas = document.getElementById("canvas46");
        anim_container = document.getElementById("animation_container46");
        dom_overlay_container = document.getElementById("dom_overlay_container46");
        var comp=AdobeAn.getComposition("D55B5E3E6C6F5A4FBA4088D2C18C4C26");
        var lib=comp.getLibrary();
        var loader = new createjs.LoadQueue(false);
        loader.addEventListener("fileload", function(evt){handleFileLoad(evt,comp)});
        loader.addEventListener("complete", function(evt){handleComplete(evt,comp)});
        var lib=comp.getLibrary();
        loader.loadManifest(lib.properties.manifest);
    }
    function handleFileLoad(evt, comp) {
        var images=comp.getImages();
        if (evt && (evt.item.type == "image")) { images[evt.item.id] = evt.result; }
    }
    function handleComplete(evt,comp) {
        //This function is always called, irrespective of the content. You can use the variable "stage" after it is created in token create_stage.
        var lib=comp.getLibrary();
        var ss=comp.getSpriteSheet();
        var queue = evt.target;
        var ssMetadata = lib.ssMetadata;
        for(i=0; i<ssMetadata.length; i++) {
            ss[ssMetadata[i].name] = new createjs.SpriteSheet( {"images": [queue.getResult(ssMetadata[i].name)], "frames": ssMetadata[i].frames} )
        }
        var preloaderDiv = document.getElementById("_preload_div_46");
        var elems = document.querySelectorAll("._preload_div_");
        var index = 0, length = elems.length;
        for ( ; index < length; index++) {
            elems[index].style.display = 'none';
        }
        preloaderDiv.style.display = 'none';
        canvas.style.display = 'block';
        exportRoot = new lib.June_main();
        stage = new lib.Stage(canvas);
        //Registers the "tick" event listener.
        fnStartAnimation = function() {
            stage.addChild(exportRoot);
            createjs.Ticker.setFPS(lib.properties.fps);
            createjs.Ticker.addEventListener("tick", stage)
            stage.addEventListener("tick", handleTick)
            function getProjectionMatrix(container, totalDepth) {
                var focalLength = 528.25;
                var projectionCenter = { x : lib.properties.width/2, y : lib.properties.height/2 };
                var scale = (totalDepth + focalLength)/focalLength;
                var scaleMat = new createjs.Matrix2D;
                scaleMat.a = 1/scale;
                scaleMat.d = 1/scale;
                var projMat = new createjs.Matrix2D;
                projMat.tx = -projectionCenter.x;
                projMat.ty = -projectionCenter.y;
                projMat = projMat.prependMatrix(scaleMat);
                projMat.tx += projectionCenter.x;
                projMat.ty += projectionCenter.y;
                return projMat;
            }
            function handleTick(event) {
                var cameraInstance = exportRoot.___camera___instance;
                if(cameraInstance !== undefined && cameraInstance.pinToObject !== undefined)
                {
                    cameraInstance.x = cameraInstance.pinToObject.x + cameraInstance.pinToObject.pinOffsetX;
                    cameraInstance.y = cameraInstance.pinToObject.y + cameraInstance.pinToObject.pinOffsetY;
                    if(cameraInstance.pinToObject.parent !== undefined && cameraInstance.pinToObject.parent.depth !== undefined)
                        cameraInstance.depth = cameraInstance.pinToObject.parent.depth + cameraInstance.pinToObject.pinOffsetZ;
                }
                applyLayerZDepth(exportRoot);
            }
            function applyLayerZDepth(parent)
            {
                var cameraInstance = parent.___camera___instance;
                var focalLength = 528.25;
                var projectionCenter = { 'x' : 0, 'y' : 0};
                if(parent === exportRoot)
                {
                    var stageCenter = { 'x' : lib.properties.width/2, 'y' : lib.properties.height/2 };
                    projectionCenter.x = stageCenter.x;
                    projectionCenter.y = stageCenter.y;
                }
                for(child in parent.children)
                {
                    var layerObj = parent.children[child];
                    if(layerObj == cameraInstance)
                        continue;
                    applyLayerZDepth(layerObj, cameraInstance);
                    if(layerObj.layerDepth === undefined)
                        continue;
                    if(layerObj.currentFrame != layerObj.parent.currentFrame)
                    {
                        layerObj.gotoAndPlay(layerObj.parent.currentFrame);
                    }
                    var matToApply = new createjs.Matrix2D;
                    var cameraMat = new createjs.Matrix2D;
                    var totalDepth = layerObj.layerDepth ? layerObj.layerDepth : 0;
                    var cameraDepth = 0;
                    if(cameraInstance && !layerObj.isAttachedToCamera)
                    {
                        var mat = cameraInstance.getMatrix();
                        mat.tx -= projectionCenter.x;
                        mat.ty -= projectionCenter.y;
                        cameraMat = mat.invert();
                        cameraMat.prependTransform(projectionCenter.x, projectionCenter.y, 1, 1, 0, 0, 0, 0, 0);
                        cameraMat.appendTransform(-projectionCenter.x, -projectionCenter.y, 1, 1, 0, 0, 0, 0, 0);
                        if(cameraInstance.depth)
                            cameraDepth = cameraInstance.depth;
                    }
                    if(layerObj.depth)
                    {
                        totalDepth = layerObj.depth;
                    }
                    //Offset by camera depth
                    totalDepth -= cameraDepth;
                    if(totalDepth < -focalLength)
                    {
                        matToApply.a = 0;
                        matToApply.d = 0;
                    }
                    else
                    {
                        if(layerObj.layerDepth)
                        {
                            var sizeLockedMat = getProjectionMatrix(parent, layerObj.layerDepth);
                            if(sizeLockedMat)
                            {
                                sizeLockedMat.invert();
                                matToApply.prependMatrix(sizeLockedMat);
                            }
                        }
                        matToApply.prependMatrix(cameraMat);
                        var projMat = getProjectionMatrix(parent, totalDepth);
                        if(projMat)
                        {
                            matToApply.prependMatrix(projMat);
                        }
                    }
                    layerObj.transformMatrix = matToApply;
                }
            }
        }
        //Code to support hidpi screens and responsive scaling.
        function makeResponsive(isResp, respDim, isScale, scaleType) {
            var lastW, lastH, lastS=1;
            window.addEventListener('resize', resizeCanvas);
            resizeCanvas();
            function resizeCanvas() {
                var w = lib.properties.width, h = lib.properties.height;
                var iw = window.innerWidth, ih=window.innerHeight;
                var pRatio = window.devicePixelRatio || 1, xRatio=iw/w, yRatio=ih/h, sRatio=1;
                if(isResp) {
                    if((respDim=='width'&&lastW==iw) || (respDim=='height'&&lastH==ih)) {
                        sRatio = lastS;
                    }
                    else if(!isScale) {
                        if(iw<w || ih<h)
                            sRatio = Math.min(xRatio, yRatio);
                    }
                    else if(scaleType==1) {
                        sRatio = Math.min(xRatio, yRatio);
                    }
                    else if(scaleType==2) {
                        sRatio = Math.max(xRatio, yRatio);
                    }
                }
                canvas.width = w*pRatio*sRatio;
                canvas.height = h*pRatio*sRatio;
                canvas.style.width = anim_container.style.width = dom_overlay_container.style.width = preloaderDiv.style.width = w*sRatio+'px';
                canvas.style.height = anim_container.style.height = dom_overlay_container.style.height = preloaderDiv.style.height = h*sRatio+'px';
                stage.scaleX = pRatio*sRatio;
                stage.scaleY = pRatio*sRatio;
                lastW = iw; lastH = ih; lastS = sRatio;
                stage.tickOnUpdate = false;
                stage.update();
                stage.tickOnUpdate = true;
            }
        }
        makeResponsive(true,'both',true,2);
        AdobeAn.compositionLoaded(lib.properties.id);
        fnStartAnimation();
    }
    init();
});