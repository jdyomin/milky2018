(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [];


// symbols:



(lib.clouds = function() {
	this.initialize(img.clouds);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,163,134);


(lib.hand1 = function() {
	this.initialize(img.hand1);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,33,30);


(lib.oprava = function() {
	this.initialize(img.oprava);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,165,100);


(lib.pack1 = function() {
	this.initialize(img.pack1);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,519,469);


(lib.Shadow = function() {
	this.initialize(img.Shadow);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,783,266);// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.Symbol7 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.clouds();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol7, new cjs.Rectangle(0,0,163,134), null);


(lib.Symbol5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.lf(["rgba(255,255,255,0)","#FFFFFF","rgba(255,255,255,0)"],[0,0.478,1],-22.3,0,22.3,0).s().p("AjeIDIAAwFIG9AAIAAQFg");
	this.shape.setTransform(22.3,51.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol5, new cjs.Rectangle(0,0,44.6,103), null);


(lib.Symbol3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(12,1,1).p("AA0CCQgCAGhlkK");
	this.shape.setTransform(5.2,13.1);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol3, new cjs.Rectangle(-6,-6,22.5,38.1), null);


(lib.Symbol2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(12,1,1).p("AgQCLIAhkV");
	this.shape.setTransform(1.7,13.9);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol2, new cjs.Rectangle(-6,-6,15.4,39.9), null);


(lib.Symbol6 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("AkQA6ICdkdIGEDnIluDgg");
	mask.setTransform(27.3,22.8);

	// Layer 4
	this.instance = new lib.Symbol7();
	this.instance.parent = this;
	this.instance.setTransform(98.8,78,1,1,0,0,0,81.5,67);

	var maskedShapeInstanceList = [this.instance];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance).to({x:-49.3,y:-13.9},209).wait(1));

	// Layer 6
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#3374BE").s().p("ApNhdIDcmcIO/JDIlAGwg");
	this.shape.setTransform(50.6,45.7);

	var maskedShapeInstanceList = [this.shape];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(210));

	// Layer 2 (mask)
	var mask_1 = new cjs.Shape();
	mask_1._off = true;
	mask_1.graphics.p("AhdDkQiEgZgviRICdkdIGEDnQieDgjIAAIgIAAg");
	mask_1.setTransform(80.9,58.1);

	// Layer 3
	this.instance_1 = new lib.Symbol7();
	this.instance_1.parent = this;
	this.instance_1.setTransform(164.6,117.6,1,1,0,0,0,81.5,67);

	var maskedShapeInstanceList = [this.instance_1];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask_1;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance_1).to({x:6,y:20},209).wait(1));

	// Layer 5
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#3374BE").s().p("ApNhdIDcmcIO/JDIlAGwg");
	this.shape_1.setTransform(50.6,45.7);

	var maskedShapeInstanceList = [this.shape_1];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask_1;
	}

	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(210));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,108.2,80.9);


(lib.Symbol4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 4 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("AhiDFIgsgPIgkgcIgegfIgQgeIgFgqIALg8IAchCIAgg3IA0hCIFQDPIg+BBIguAqIgxAhIgyAZIhHAVg");
	mask.setTransform(73.1,50.5);

	// Layer 1
	this.instance = new lib.Symbol5();
	this.instance.parent = this;
	this.instance.setTransform(33,21.6,1,1,60,0,0,22.3,51.5);

	var maskedShapeInstanceList = [this.instance];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1).to({x:33.8,y:22.3},0).wait(1).to({x:35,y:23.4},0).wait(1).to({x:36.7,y:24.9},0).wait(1).to({x:38.9,y:26.8},0).wait(1).to({x:41.6,y:29.2},0).wait(1).to({x:44.8,y:32},0).wait(1).to({x:48.6,y:35.3},0).wait(1).to({x:52.8,y:39},0).wait(1).to({x:57.3,y:42.9},0).wait(1).to({x:62.1,y:47},0).wait(1).to({x:66.9,y:51.2},0).wait(1).to({x:71.5,y:55.3},0).wait(1).to({x:76,y:59.2},0).wait(1).to({x:80.1,y:62.8},0).wait(1).to({x:83.8,y:66.1},0).wait(1).to({x:87.1,y:69},0).wait(1).to({x:90,y:71.5},0).wait(1).to({x:92.5,y:73.6},0).wait(1).to({x:94.6,y:75.5},0).wait(1).to({x:96.3,y:77},0).wait(1).to({x:97.7,y:78.2},0).wait(1).to({x:98.8,y:79.2},0).wait(1).to({x:99.6,y:79.9},0).wait(1).to({x:100.2,y:80.4},0).wait(17));

	// Layer 3 (mask)
	var mask_1 = new cjs.Shape();
	mask_1._off = true;
	mask_1.graphics.p("Ah1CLIglgbIgYgmQgGgUAFgcIAHgoIAcg7IA9hSIEJCbIhlBiIg9AmIgvARIg3ADg");
	mask_1.setTransform(18.2,15.6);

	// Layer 2
	this.instance_1 = new lib.Symbol5();
	this.instance_1.parent = this;
	this.instance_1.setTransform(-9.5,-15.9,1,1,60,0,0,22.3,51.5);

	var maskedShapeInstanceList = [this.instance_1];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask_1;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(1).to({x:-9,y:-15.1},0).wait(1).to({x:-8.2,y:-13.9},0).wait(1).to({x:-7.2,y:-12.3},0).wait(1).to({x:-5.8,y:-10.2},0).wait(1).to({x:-4,y:-7.6},0).wait(1).to({x:-2,y:-4.5},0).wait(1).to({x:0.4,y:-0.9},0).wait(1).to({x:3.1,y:3.1},0).wait(1).to({x:5.9,y:7.4},0).wait(1).to({x:9,y:12},0).wait(1).to({x:12,y:16.5},0).wait(1).to({x:15,y:21},0).wait(1).to({x:17.8,y:25.3},0).wait(1).to({x:20.4,y:29.2},0).wait(1).to({x:22.8,y:32.8},0).wait(1).to({x:24.9,y:35.9},0).wait(1).to({x:26.7,y:38.7},0).wait(1).to({x:28.3,y:41.1},0).wait(1).to({x:29.6,y:43.1},0).wait(1).to({x:30.7,y:44.7},0).wait(1).to({x:31.6,y:46},0).wait(1).to({x:32.3,y:47.1},0).wait(1).to({x:32.8,y:47.8},0).wait(1).to({x:33.2,y:48.4},0).wait(17));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,88.8,66.7);


(lib.Symbol1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 3
	this.instance = new lib.Symbol2();
	this.instance.parent = this;
	this.instance.setTransform(21.3,14.1,0.973,0.496,0,-31.4,-6.7,1.7,13.9);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(14).to({_off:false},0).to({scaleX:1,scaleY:1,skewX:0,skewY:0,x:23.9,y:-4.9},9,cjs.Ease.get(1)).wait(46).to({scaleX:0.97,scaleY:0.5,skewX:-31.4,skewY:-6.7,x:21.3,y:14.1},9).wait(17));

	// Layer 2
	this.instance_1 = new lib.Symbol3();
	this.instance_1.parent = this;
	this.instance_1.setTransform(19.1,14.3,0.986,0.47,0,23.5,4.8,5.2,13);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(14).to({_off:false},0).to({regY:13.1,scaleX:1,scaleY:1,skewX:0,skewY:0,x:13.6,y:-3.6},9,cjs.Ease.get(1)).wait(46).to({regY:13,scaleX:0.99,scaleY:0.47,skewX:23.5,skewY:4.8,x:19.1,y:14.3},9).wait(17));

	// Layer 1
	this.instance_2 = new lib.hand1();
	this.instance_2.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(95));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,33,30);


// stage content:
(lib.Sun = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 6
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(12,1,1).p("AiqhWQAUBXA2AuQAvAnA9AAQA5ABAugfQAugfAKgt");
	this.shape.setTransform(777.5,293.7);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(95));

	// Layer 5
	this.instance = new lib.Symbol4();
	this.instance.parent = this;
	this.instance.setTransform(812,247,1,1,0,0,0,48,35.1);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(31).to({_off:false},0).to({_off:true},30).wait(34));

	// Layer 9
	this.instance_1 = new lib.oprava();
	this.instance_1.parent = this;
	this.instance_1.setTransform(750.8,195);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(95));

	// Layer 10
	this.instance_2 = new lib.Symbol6();
	this.instance_2.parent = this;
	this.instance_2.setTransform(783.7,227.4,1,1,0,0,0,27.3,22.8);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(95));

	// Layer 8
	this.instance_3 = new lib.pack1();
	this.instance_3.parent = this;
	this.instance_3.setTransform(499,147);

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(95));

	// Layer 4
	this.instance_4 = new lib.Symbol1();
	this.instance_4.parent = this;
	this.instance_4.setTransform(624.6,344.8,1,1,-90,0,0,22.9,25.6);

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(8).to({rotation:0,x:659.1,y:205.7},15,cjs.Ease.get(1)).wait(46).to({rotation:-90,x:624.6,y:344.8},15).wait(11));

	// Layer 3
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(12,1,1).p("AJqkoQh1CliBBWQiDBVjMBQQjLBRh8AkQh9AkjKAY");
	this.shape_1.setTransform(685.9,311.7);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(12,1,1).p("ApVDYQCwgFB2gNQB1gODCg4QDCg3CEhFQCDhECFiX");
	this.shape_2.setTransform(688.5,302.3);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(12,1,1).p("ApCCBQCYANBvAIQBvAIC5ghQC4ghCGg1QCFg0CTiJ");
	this.shape_3.setTransform(690.9,294.6);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(12,1,1).p("AowAfQCCAfBoAbQBoAbCxgLQCwgMCIgmQCGgmCgh8");
	this.shape_4.setTransform(693.2,289.2);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(12,1,1).p("AoghDQBtAuBjAsQBiAtCqAIQCoAJCKgZQCHgZCrhx");
	this.shape_5.setTransform(695.2,285.2);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(12,1,1).p("AoQhxQBaA8BdA9QBdA9CjAaQChAbCKgNQCKgNC2hm");
	this.shape_6.setTransform(697.1,277.1);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(12,1,1).p("AoDiZQBJBIBZBNQBYBLCcAqQCcArCLgCQCKgBDAhe");
	this.shape_7.setTransform(698.8,269.6);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(12,1,1).p("An3jAQA6BUBVBZQBTBZCWA5QCXA5CMAIQCLAJDJhV");
	this.shape_8.setTransform(700.3,263.2);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(12,1,1).p("AntjjQAtBeBQBlQBQBlCSBFQCRBGCNARQCNASDRhO");
	this.shape_9.setTransform(701.7,257.6);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(12,1,1).p("AnjkCQAhBnBNBvQBMBuCOBRQCNBRCNAZQCNAZDYhI");
	this.shape_10.setTransform(702.8,252.9);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(12,1,1).p("AnbkdQAXBuBKB4QBKB3CJBbQCLBaCNAfQCOAfDehB");
	this.shape_11.setTransform(703.8,248.9);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(12,1,1).p("AnVkzQAPB0BIB/QBIB+CHBiQCHBiCNAkQCPAlDig+");
	this.shape_12.setTransform(704.6,245.7);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(12,1,1).p("AnQlFQAJB5BGCFQBGCDCEBoQCFBoCOApQCPAoDmg6");
	this.shape_13.setTransform(705.2,243.2);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(12,1,1).p("AnNlRQAFB8BFCJQBECHCDBsQCDBsCPAsQCPAsDpg4");
	this.shape_14.setTransform(705.7,241.4);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(12,1,1).p("AnLlZQACB/BECKQBECKCCBvQCCBuCPAuQCPAtDqg2");
	this.shape_15.setTransform(706,240.3);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(12,1,1).p("AHLE/QjrA2iPguQiPguiChwQiBhvhEiLQhEiLgBh/");
	this.shape_16.setTransform(706,240);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(12,1,1).p("AnUk2QAOB1BICAQBHB/CHBiQCGBjCOAlQCPAlDig8");
	this.shape_17.setTransform(704.7,245.3);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#000000").ss(12,1,1).p("AnfkRQAbBrBNB0QBLBzCLBWQCLBWCNAdQCOAcDbhE");
	this.shape_18.setTransform(703.4,250.7);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#000000").ss(12,1,1).p("AnpjtQAoBhBQBoQBPBoCRBJQCPBJCNAUQCNATDShL");
	this.shape_19.setTransform(702,256.2);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#000000").ss(12,1,1).p("An0jJQA2BWBTBdQBTBcCVA8QCVA8CMALQCMALDLhT");
	this.shape_20.setTransform(700.7,261.7);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#000000").ss(12,1,1).p("An/inQBEBNBXBRQBXBQCaAvQCZAwCMACQCLACDDhb");
	this.shape_21.setTransform(699.3,267.4);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#000000").ss(12,1,1).p("AoKiGQBSBDBbBEQBaBFCfAjQCfAjCKgHQCKgHC8hi");
	this.shape_22.setTransform(698,273.1);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#000000").ss(12,1,1).p("AoUhmQBeA4BfA5QBeA5CkAWQCkAWCJgPQCJgQC0ho");
	this.shape_23.setTransform(696.7,279.1);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#000000").ss(12,1,1).p("AofhHQBtAuBiAtQBiAuCpAJQCoAJCJgYQCIgYCshw");
	this.shape_24.setTransform(695.3,285);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f().s("#000000").ss(12,1,1).p("AoqgFQB6AjBmAiQBmAjCugEQCtgECIghQCHghCkh3");
	this.shape_25.setTransform(694,287.5);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f().s("#000000").ss(12,1,1).p("Ao0A4QCHAaBqAXQBpAWC0gQQCygRCHgpQCGgqCch/");
	this.shape_26.setTransform(692.6,290.4);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f().s("#000000").ss(12,1,1).p("Ao/ByQCVAQBuALQBtALC4geQC3gdCGgyQCFgyCViH");
	this.shape_27.setTransform(691.3,293.7);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f().s("#000000").ss(12,1,1).p("ApKCoQCiAGBygBQBxgBC9gqQC8gqCFg7QCFg7CMiP");
	this.shape_28.setTransform(690,297.4);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f().s("#000000").ss(12,1,1).p("ApUDVQCwgEB1gNQB1gMDBg3QDCg3CEhEQCEhECEiW");
	this.shape_29.setTransform(688.6,302);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f().s("#000000").ss(12,1,1).p("ApfD/QC9gOB5gYQB5gZDGhDQDHhECDhMQCDhNB9ie");
	this.shape_30.setTransform(687.3,306.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1}]}).to({state:[{t:this.shape_1}]},8).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_16}]},46).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_27}]},1).to({state:[{t:this.shape_28}]},1).to({state:[{t:this.shape_29}]},1).to({state:[{t:this.shape_30}]},1).to({state:[{t:this.shape_1}]},1).wait(11));

	// Layer 11
	this.instance_5 = new lib.Shadow();
	this.instance_5.parent = this;
	this.instance_5.setTransform(296,413.5);

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(95));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(540,340,1080,680);
// library properties:
lib.properties = {
	id: '7CC5B8CFE208814797223648A4B33F2A',
	width: 1080,
	height: 680,
	fps: 30,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: [
		{src:"assets/images/clouds.png", id:"clouds"},
		{src:"assets/images/hand5.png", id:"hand1"},
		{src:"assets/images/oprava.png", id:"oprava"},
		{src:"assets/images/pack7.png", id:"pack1"},
		{src:"assets/images/Shadow.png", id:"Shadow"}
	],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['7CC5B8CFE208814797223648A4B33F2A'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}



})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;

$(function () {
    var canvas, stage, exportRoot, anim_container, dom_overlay_container, fnStartAnimation;
    function init() {
        canvas = document.getElementById("canvas5");
        anim_container = document.getElementById("animation_container5");
        dom_overlay_container = document.getElementById("dom_overlay_container5");
        var comp=AdobeAn.getComposition("7CC5B8CFE208814797223648A4B33F2A");
        var lib=comp.getLibrary();
        var loader = new createjs.LoadQueue(false);
        loader.addEventListener("fileload", function(evt){handleFileLoad(evt,comp)});
        loader.addEventListener("complete", function(evt){handleComplete(evt,comp)});
        var lib=comp.getLibrary();
        loader.loadManifest(lib.properties.manifest);
    }
    function handleFileLoad(evt, comp) {
        var images=comp.getImages();
        if (evt && (evt.item.type == "image")) { images[evt.item.id] = evt.result; }
    }
    function handleComplete(evt,comp) {
        //This function is always called, irrespective of the content. You can use the variable "stage" after it is created in token create_stage.
        var lib=comp.getLibrary();
        var ss=comp.getSpriteSheet();
        var queue = evt.target;
        var ssMetadata = lib.ssMetadata;
        for(i=0; i<ssMetadata.length; i++) {
            ss[ssMetadata[i].name] = new createjs.SpriteSheet( {"images": [queue.getResult(ssMetadata[i].name)], "frames": ssMetadata[i].frames} )
        }
        exportRoot = new lib.Sun();
        stage = new lib.Stage(canvas);
        //Registers the "tick" event listener.
        fnStartAnimation = function() {
            stage.addChild(exportRoot);
            createjs.Ticker.setFPS(lib.properties.fps);
            createjs.Ticker.addEventListener("tick", stage);
        }
        //Code to support hidpi screens and responsive scaling.
        function makeResponsive(isResp, respDim, isScale, scaleType) {
            var lastW, lastH, lastS=1;
            window.addEventListener('resize', resizeCanvas);
            resizeCanvas();
            function resizeCanvas() {
                var w = lib.properties.width, h = lib.properties.height;
                var iw = window.innerWidth, ih=window.innerHeight;
                var pRatio = window.devicePixelRatio || 1, xRatio=iw/w, yRatio=ih/h, sRatio=1;
                if(isResp) {
                    if((respDim=='width'&&lastW==iw) || (respDim=='height'&&lastH==ih)) {
                        sRatio = lastS;
                    }
                    else if(!isScale) {
                        if(iw<w || ih<h)
                            sRatio = Math.min(xRatio, yRatio);
                    }
                    else if(scaleType==1) {
                        sRatio = Math.min(xRatio, yRatio);
                    }
                    else if(scaleType==2) {
                        sRatio = Math.max(xRatio, yRatio);
                    }
                }
                canvas.width = w*pRatio*sRatio;
                canvas.height = h*pRatio*sRatio;
                canvas.style.width = dom_overlay_container.style.width = anim_container.style.width =  w*sRatio+'px';
                canvas.style.height = anim_container.style.height = dom_overlay_container.style.height = h*sRatio+'px';
                stage.scaleX = pRatio*sRatio;
                stage.scaleY = pRatio*sRatio;
                lastW = iw; lastH = ih; lastS = sRatio;
                stage.tickOnUpdate = false;
                stage.update();
                stage.tickOnUpdate = true;
            }
        }
        makeResponsive(true,'both',true,1);
        AdobeAn.compositionLoaded(lib.properties.id);
        fnStartAnimation();
    }
    init();
});