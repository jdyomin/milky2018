(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [];


// symbols:



(lib.pack_bg = function() {
	this.initialize(img.pack_bg);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,719,561);


(lib.Symbol3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Symbol 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(13,1,1).p("AAwCbQC4kelXgX");
	this.shape.setTransform(13.5,15.5);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(13,1,1).p("AhriLQFAgTibEr");
	this.shape_1.setTransform(12.5,17);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(13,1,1).p("Ahoh3QEqg/h+E4");
	this.shape_2.setTransform(11.5,18);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(13,1,1).p("ABNB7QBhlFkTBq");
	this.shape_3.setTransform(10.6,18.7);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(13,1,1).p("AhgiDQEYgsh+E4");
	this.shape_4.setTransform(12.5,17.3);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(13,1,1).p("AhcieQEdASibEr");
	this.shape_5.setTransform(14.5,15.1);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(13,1,1).p("AARC4QC5kekihR");
	this.shape_6.setTransform(16.6,12.7);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(13,1,1).p("AhfiuQEyA/i4Ee");
	this.shape_7.setTransform(15.6,13.6);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(13,1,1).p("AhlinQFAAxi5Ee");
	this.shape_8.setTransform(14.9,14.3);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(13,1,1).p("AhpihQFJAli4Ee");
	this.shape_9.setTransform(14.3,14.8);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(13,1,1).p("AhsieQFRAei5Ef");
	this.shape_10.setTransform(13.9,15.2);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(13,1,1).p("AhuibQFVAZi4Ee");
	this.shape_11.setTransform(13.6,15.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},9).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_3}]},7).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_6}]},4).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape}]},1).wait(41));

	// Symbol 2
	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(13,1,1).p("ABFBGQAjjdiyCB");
	this.shape_12.setTransform(7.5,18.6);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(13,1,1).p("AhEgPQCmiKgjDd");
	this.shape_13.setTransform(8.1,18.8);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(13,1,1).p("Ag/gGQCbiUgiDd");
	this.shape_14.setTransform(8.6,19);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(13,1,1).p("AA0BBQAijdiPCc");
	this.shape_15.setTransform(9.2,19.2);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(13,1,1).p("Ag6goQCRhqgjDd");
	this.shape_16.setTransform(9.2,18.1);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(13,1,1).p("Ag7hOQCSg2giDd");
	this.shape_17.setTransform(9.1,16.7);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#000000").ss(13,1,1).p("AA2BuQAijdiTAC");
	this.shape_18.setTransform(9,14.6);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#000000").ss(13,1,1).p("AhAhXQCdgogjDd");
	this.shape_19.setTransform(8.6,16.3);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#000000").ss(13,1,1).p("AhDhBQCkhIgjDd");
	this.shape_20.setTransform(8.2,17.3);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#000000").ss(13,1,1).p("AhGgvQCqhhgjDd");
	this.shape_21.setTransform(7.9,17.9);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#000000").ss(13,1,1).p("AhIghQCuhzgjDd");
	this.shape_22.setTransform(7.7,18.3);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#000000").ss(13,1,1).p("AhJgZQCwh9giDd");
	this.shape_23.setTransform(7.5,18.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_12}]}).to({state:[{t:this.shape_12}]},9).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_15}]},7).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_18}]},4).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_12}]},1).wait(41));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-6.5,-6.5,37.8,44.1);


// stage content:
(lib.Family = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 11
	this.instance = new lib.Symbol3();
	this.instance.parent = this;
	this.instance.setTransform(781.9,403.5,1,1,0,0,0,18.1,29.9);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({rotation:-45,x:737.1,y:375.8},9,cjs.Ease.get(1)).wait(3).to({rotation:-25.3,x:760.7,y:321.9},10,cjs.Ease.get(1)).to({rotation:0,x:781.9,y:403.5},10,cjs.Ease.get(-1)).wait(41));

	// Layer 9
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(13,1,1).p("AGBlXQAjEFhPCxQhFCeiOA9QiEA4iRgxQiWgyhiiO");
	this.shape.setTransform(818.5,386.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(13,1,1).p("AmzBvQBdCACMAwQCKAwCWgsQCdguBciOQBiicADkC");
	this.shape_1.setTransform(813.4,383.4);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(13,1,1).p("AnbBeQBYB1CFAuQCCAuClggQCsgiBvh/QB0iLAkj+");
	this.shape_2.setTransform(809.4,380.6);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(13,1,1).p("An+BPQBVBqB9AtQB8AtCzgWQC4gYCAhyQCDh7BAj7");
	this.shape_3.setTransform(805.9,378.3);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(13,1,1).p("AobBBQBRBiB4AsQB2ArDAgNQDBgPCPhoQCPhuBZj3");
	this.shape_4.setTransform(803,376.3);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(13,1,1).p("AozA2QBPBbBzAqQByArDJgGQDKgICZhfQCbhjBsj2");
	this.shape_5.setTransform(800.6,374.8);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(13,1,1).p("ApGAtQBNBVBvAqQBvAqDQgBQDRgCCihYQCjhbB8j0");
	this.shape_6.setTransform(798.7,373.6);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(13,1,1).p("ApTAmQBLBRBsApQBsAqDWADQDWACCphUQCohUCHjz");
	this.shape_7.setTransform(797.4,372.8);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(13,1,1).p("ApbAiQBLBPBqApQBrApDZAFQDYAFCthRQCshRCNjy");
	this.shape_8.setTransform(796.6,372.3);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(13,1,1).p("AJfjFQiQDxitBQQiuBQjZgGQjagGhqgpQhqgphLhO");
	this.shape_9.setTransform(796.3,372.2);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(13,1,1).p("ApeAgQBLBOBqApQBqApDaAGQDZAGCuhQQCthQCQjx");
	this.shape_10.setTransform(796.3,372.2);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(13,1,1).p("ApHgyQA8BiBkA5QBkA4DMARQDKARCwhHQCuhICWjp");
	this.shape_11.setTransform(798.7,370.5);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(13,1,1).p("Aoyh9QAvB2BfBGQBeBGC/AaQC+AbCwhAQCwhACcjg");
	this.shape_12.setTransform(800.9,369.1);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(13,1,1).p("AofiuQAjCIBaBRQBZBTC0AjQCzAjCxg5QCxg5Cgja");
	this.shape_13.setTransform(802.8,366.1);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(13,1,1).p("AoPjGQAZCWBVBcQBVBdCrArQCpAqCygzQCxg0CljT");
	this.shape_14.setTransform(804.4,361.7);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(13,1,1).p("AoBjbQAQCiBTBlQBRBmChAxQCjAxCxgvQCygvCpjN");
	this.shape_15.setTransform(805.8,358.1);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(13,1,1).p("An3jtQAKCtBPBsQBOBtCcA3QCbA2CygrQCygrCsjJ");
	this.shape_16.setTransform(807,355.1);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(13,1,1).p("Anuj7QAFC1BNByQBMBzCWA6QCVA6CzgnQCzgoCujG");
	this.shape_17.setTransform(807.8,352.8);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#000000").ss(13,1,1).p("AnokFQABC6BLB2QBKB3CTA9QCSA+CzgmQCzglCwjF");
	this.shape_18.setTransform(808.5,351.2);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#000000").ss(13,1,1).p("AnkkLQgBC+BKB5QBJB5CQA/QCQA/CzgkQC0glCwjD");
	this.shape_19.setTransform(808.8,350.2);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#000000").ss(13,1,1).p("AHkAaQixDDi0AkQizAjiPg/QiPhAhJh6QhKh5ACi/");
	this.shape_20.setTransform(809,349.9);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#000000").ss(13,1,1).p("AnikKQgBC+BKB5QBKB5CPA+QCPA/CyglQCzgmCvjD");
	this.shape_21.setTransform(809.1,350.4);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#000000").ss(13,1,1).p("AnfkDQACC8BNB3QBLB3CPA7QCPA6CvgoQCvgqCpjF");
	this.shape_22.setTransform(809.4,352);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#000000").ss(13,1,1).p("Anaj4QAHC6BQBzQBQB0COA0QCPA1CpgvQCqgwCejI");
	this.shape_23.setTransform(809.8,354.7);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#000000").ss(13,1,1).p("AnUjpQAOC3BWBuQBUBuCOAtQCOArCig3QCkg7CPjM");
	this.shape_24.setTransform(810.4,358.5);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f().s("#000000").ss(13,1,1).p("AnLjWQAWCyBdBoQBbBnCNAiQCOAgCYhCQCahHB8jT");
	this.shape_25.setTransform(811.2,363.6);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f().s("#000000").ss(13,1,1).p("AnBjCQAhCtBlBgQBkBfCLAVQCOATCMhQQCPhXBlja");
	this.shape_26.setTransform(812.2,370);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f().s("#000000").ss(13,1,1).p("Am1iDQAvCmBvBXQBsBWCLAFQCNADB+hgQCChpBJjj");
	this.shape_27.setTransform(813.4,373.6);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f().s("#000000").ss(13,1,1).p("AmngzQA+CfB6BMQB3BLCIgNQCOgQBuhyQByh+Aqjt");
	this.shape_28.setTransform(814.7,377.1);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f().s("#000000").ss(13,1,1).p("AmXAkQBPCXCHBAQCEA+CGghQCOglBaiHQBiiWAFj5");
	this.shape_29.setTransform(816.2,381.4);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_27}]},1).to({state:[{t:this.shape_28}]},1).to({state:[{t:this.shape_29}]},1).to({state:[{t:this.shape}]},1).wait(41));

	// Layer 5
	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f().s("#000000").ss(13,1,1).p("AjohLQATBKBDAnQA8AlBSABQBPAABBggQBGgjAXg6");
	this.shape_30.setTransform(751.1,304.9);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f().s("#000000").ss(13,1,1).p("AjohBQATBJBDAeQA8AbBSABQBPAABCgWQBFgZAXg6");
	this.shape_31.setTransform(751.1,303.9);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f().s("#000000").ss(13,1,1).p("Ajog3QATBJBCAUQA9AQBSABQBPABBCgNQBFgOAXg6");
	this.shape_32.setTransform(751.1,302.9);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f().s("#000000").ss(13,1,1).p("AjogtQATBJBCAJQA9AHBSABQBPABBCgDQBFgEAXg6");
	this.shape_33.setTransform(751.1,301.9);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f().s("#000000").ss(13,1,1).p("AjognQATBJBBgBQA+gDBSABQBPABBCAHQBFAGAXg6");
	this.shape_34.setTransform(751.1,301.3);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f().s("#000000").ss(13,1,1).p("AjogjQATBIBBgLQA+gMBSABQBPAABDARQBEARAXg6");
	this.shape_35.setTransform(751.1,301);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f().s("#000000").ss(13,1,1).p("AjoggQATBIBAgVQA/gWBRABQBQABBDAaQBEAbAXg6");
	this.shape_36.setTransform(751.1,300.7);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f().s("#000000").ss(13,1,1).p("AjogeQATBIA/ggQBAgfBRABQBQABBDAkQBEAlAXg6");
	this.shape_37.setTransform(751.1,300.5);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f().s("#000000").ss(13,1,1).p("AjoggQATBIBAgXQA/gWBRABQBQAABDAcQBEAcAXg6");
	this.shape_38.setTransform(751.1,300.7);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f().s("#000000").ss(13,1,1).p("AjogiQATBIBBgOQA+gOBSABQBPAABDAUQBEATAXg6");
	this.shape_39.setTransform(751.1,300.9);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f().s("#000000").ss(13,1,1).p("AjoglQATBIBBgFQA+gGBSABQBPABBDALQBEAKAXg6");
	this.shape_40.setTransform(751.1,301.2);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f().s("#000000").ss(13,1,1).p("AjogpQATBIBCAFQA9ACBSABQBPAABCADQBFABAXg6");
	this.shape_41.setTransform(751.1,301.6);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f().s("#000000").ss(13,1,1).p("AjogwQATBIBCAOQA9AKBSABQBPABBCgHQBFgHAXg6");
	this.shape_42.setTransform(751.1,302.3);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f().s("#000000").ss(13,1,1).p("Ajog5QATBIBCAXQA9ATBSABQBPABBCgQQBFgQAXg6");
	this.shape_43.setTransform(751.1,303.2);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f().s("#000000").ss(13,1,1).p("AjohCQATBIBDAgQA8AcBSABQBPAABCgYQBFgZAXg6");
	this.shape_44.setTransform(751.1,304.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_30}]}).to({state:[{t:this.shape_30}]},22).to({state:[{t:this.shape_31}]},1).to({state:[{t:this.shape_32}]},1).to({state:[{t:this.shape_33}]},1).to({state:[{t:this.shape_34}]},1).to({state:[{t:this.shape_35}]},1).to({state:[{t:this.shape_36}]},1).to({state:[{t:this.shape_37}]},1).to({state:[{t:this.shape_38}]},1).to({state:[{t:this.shape_39}]},1).to({state:[{t:this.shape_40}]},1).to({state:[{t:this.shape_41}]},1).to({state:[{t:this.shape_42}]},1).to({state:[{t:this.shape_43}]},1).to({state:[{t:this.shape_44}]},1).to({state:[{t:this.shape_30}]},1).to({state:[{t:this.shape_31}]},1).to({state:[{t:this.shape_32}]},1).to({state:[{t:this.shape_33}]},1).to({state:[{t:this.shape_34}]},1).to({state:[{t:this.shape_35}]},1).to({state:[{t:this.shape_36}]},1).to({state:[{t:this.shape_37}]},1).to({state:[{t:this.shape_38}]},1).to({state:[{t:this.shape_39}]},1).to({state:[{t:this.shape_40}]},1).to({state:[{t:this.shape_41}]},1).to({state:[{t:this.shape_42}]},1).to({state:[{t:this.shape_43}]},1).to({state:[{t:this.shape_44}]},1).to({state:[{t:this.shape_30}]},1).to({state:[{t:this.shape_31}]},1).to({state:[{t:this.shape_32}]},1).to({state:[{t:this.shape_33}]},1).to({state:[{t:this.shape_34}]},1).to({state:[{t:this.shape_35}]},1).to({state:[{t:this.shape_36}]},1).to({state:[{t:this.shape_37}]},1).to({state:[{t:this.shape_38}]},1).to({state:[{t:this.shape_39}]},1).to({state:[{t:this.shape_40}]},1).to({state:[{t:this.shape_41}]},1).to({state:[{t:this.shape_42}]},1).to({state:[{t:this.shape_43}]},1).to({state:[{t:this.shape_44}]},1).to({state:[{t:this.shape_30}]},1).wait(6));
	this.timeline.addTween(cjs.Tween.get(this.shape_30).wait(22).to({_off:true},1).wait(14).to({_off:false},0).to({_off:true},1).wait(14).to({_off:false},0).to({_off:true},1).wait(14).to({_off:false},0).wait(6));

	// Layer 8
	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f("#F93745").s().p("AgiBbQgaghgfgXQgggXAig7QAig9AzgIQAygIAfAaQAgAagCAyQgBAxgQAsQgQAsgqAEIgJAAQgjAAgWgcg");
	this.shape_45.setTransform(781.7,309.4);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.f("#F93745").s().p("AgkBZQgZghgfgXQgfgYAjg7QAig7A0gHQAygIAfAbQAfAagDAyQgCAxgRAsQgRArgqAEIgHAAQglAAgVgeg");
	this.shape_46.setTransform(781.5,309.6);

	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.f("#F93745").s().p("AgmBYQgYghgfgZQgegYAkg6QAkg7AzgGQAygGAeAbQAfAbgEAyQgDAxgSArQgRArgqADIgFAAQgnAAgVgfg");
	this.shape_47.setTransform(781.2,309.9);

	this.shape_48 = new cjs.Shape();
	this.shape_48.graphics.f("#F93745").s().p("AgoBXQgYgigegZQgegZAlg5QAlg6AzgFQAzgGAeAcQAeAcgFAxQgEAxgTArQgSArgqACIgDAAQgoAAgVggg");
	this.shape_48.setTransform(781,310.1);

	this.shape_49 = new cjs.Shape();
	this.shape_49.graphics.f("#F93745").s().p("AgqBWQgXgjgegZQgdgaAng4QAmg6AygDQAzgFAeAdQAdAcgGAyQgFAwgTArQgUAqgqABIgCAAQgoAAgVghg");
	this.shape_49.setTransform(780.8,310.3);

	this.shape_50 = new cjs.Shape();
	this.shape_50.graphics.f("#F93745").s().p("AgsBUQgXgjgdgZQgcgaAng4QAng5AzgDQAzgDAdAdQAdAdgHAxQgGAxgUAqQgVAqgqAAQgpAAgVgjg");
	this.shape_50.setTransform(780.6,310.6);

	this.shape_51 = new cjs.Shape();
	this.shape_51.graphics.f("#F93745").s().p("AAQB3QgqgBgUgiQgWgkgcgaQgcgbAog3QApg4AygCQA0gCAcAdQAcAegIAxQgHAxgVAqQgUAogpAAIgCAAg");
	this.shape_51.setTransform(780.3,310.8);

	this.shape_52 = new cjs.Shape();
	this.shape_52.graphics.f("#F93745").s().p("AANB3QgqgCgTgjQgVgkgcgbQgcgbAqg2QApg4AzAAQA0gCAbAeQAcAfgJAxQgIAwgWAqQgVAngoAAIgDAAg");
	this.shape_52.setTransform(780.1,311);

	this.shape_53 = new cjs.Shape();
	this.shape_53.graphics.f("#F93745").s().p("AAQB3QgqgBgVgjQgVgjgdgbQgcgaApg3QApg5AygBQA0gCAcAdQAcAegIAxQgHAxgVAqQgVAogpAAIgBAAg");
	this.shape_53.setTransform(780.3,310.8);

	this.shape_54 = new cjs.Shape();
	this.shape_54.graphics.f("#F93745").s().p("AgtBUQgWgjgdgaQgcgaAng4QAog4AzgDQAzgDAcAdQAdAdgHAyQgHAwgUAqQgUAqgqAAQgqAAgVgjg");
	this.shape_54.setTransform(780.5,310.6);

	this.shape_55 = new cjs.Shape();
	this.shape_55.graphics.f("#F93745").s().p("AgrBVQgXgigdgaQgdgZAng5QAmg5AzgDQAzgFAdAdQAdAdgGAxQgGAxgTAqQgUAqgqABIgBAAQgpAAgVgig");
	this.shape_55.setTransform(780.7,310.4);

	this.shape_56 = new cjs.Shape();
	this.shape_56.graphics.f("#F93745").s().p("AgpBWQgYgigdgZQgegaAmg4QAlg6AzgEQAzgFAeAcQAeAcgGAyQgFAwgTArQgTArgqABIgCAAQgoAAgVghg");
	this.shape_56.setTransform(780.9,310.2);

	this.shape_57 = new cjs.Shape();
	this.shape_57.graphics.f("#F93745").s().p("AgnBXQgYgigegYQgegZAlg5QAkg7AzgFQAzgGAeAcQAeAbgFAyQgEAxgSArQgSArgqACIgEAAQgnAAgVggg");
	this.shape_57.setTransform(781.1,310);

	this.shape_58 = new cjs.Shape();
	this.shape_58.graphics.f("#F93745").s().p("AgmBYQgYghgfgYQgegZAkg5QAjg8A0gGQAygGAeAbQAfAbgEAxQgDAxgRAsQgSArgqADIgGAAQglAAgWgfg");
	this.shape_58.setTransform(781.3,309.8);

	this.shape_59 = new cjs.Shape();
	this.shape_59.graphics.f("#F93745").s().p("AgkBaQgZghgfgYQgfgYAjg6QAjg8AzgHQAygIAfAbQAfAagDAyQgCAxgRAsQgQAsgqADIgJAAQgkAAgVgdg");
	this.shape_59.setTransform(781.5,309.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_45}]}).to({state:[{t:this.shape_45}]},22).to({state:[{t:this.shape_46}]},1).to({state:[{t:this.shape_47}]},1).to({state:[{t:this.shape_48}]},1).to({state:[{t:this.shape_49}]},1).to({state:[{t:this.shape_50}]},1).to({state:[{t:this.shape_51}]},1).to({state:[{t:this.shape_52}]},1).to({state:[{t:this.shape_53}]},1).to({state:[{t:this.shape_54}]},1).to({state:[{t:this.shape_55}]},1).to({state:[{t:this.shape_56}]},1).to({state:[{t:this.shape_57}]},1).to({state:[{t:this.shape_58}]},1).to({state:[{t:this.shape_59}]},1).to({state:[{t:this.shape_45}]},1).to({state:[{t:this.shape_46}]},1).to({state:[{t:this.shape_47}]},1).to({state:[{t:this.shape_48}]},1).to({state:[{t:this.shape_49}]},1).to({state:[{t:this.shape_50}]},1).to({state:[{t:this.shape_51}]},1).to({state:[{t:this.shape_52}]},1).to({state:[{t:this.shape_53}]},1).to({state:[{t:this.shape_54}]},1).to({state:[{t:this.shape_55}]},1).to({state:[{t:this.shape_56}]},1).to({state:[{t:this.shape_57}]},1).to({state:[{t:this.shape_58}]},1).to({state:[{t:this.shape_59}]},1).to({state:[{t:this.shape_45}]},1).to({state:[{t:this.shape_46}]},1).to({state:[{t:this.shape_47}]},1).to({state:[{t:this.shape_48}]},1).to({state:[{t:this.shape_49}]},1).to({state:[{t:this.shape_50}]},1).to({state:[{t:this.shape_51}]},1).to({state:[{t:this.shape_52}]},1).to({state:[{t:this.shape_53}]},1).to({state:[{t:this.shape_54}]},1).to({state:[{t:this.shape_55}]},1).to({state:[{t:this.shape_56}]},1).to({state:[{t:this.shape_57}]},1).to({state:[{t:this.shape_58}]},1).to({state:[{t:this.shape_59}]},1).to({state:[{t:this.shape_45}]},1).wait(6));
	this.timeline.addTween(cjs.Tween.get(this.shape_45).wait(22).to({_off:true},1).wait(14).to({_off:false},0).to({_off:true},1).wait(14).to({_off:false},0).to({_off:true},1).wait(14).to({_off:false},0).wait(6));

	// Layer 7
	this.shape_60 = new cjs.Shape();
	this.shape_60.graphics.f("#F93745").s().p("AgFBsQg0gKgggYQgggYAEg1QACg1AfgdQAegdA0AFQAzAEAiAcQAjAbAAAyQAAAygkAiQgcAaglAAQgLAAgLgCg");
	this.shape_60.setTransform(793,292.6);

	this.shape_61 = new cjs.Shape();
	this.shape_61.graphics.f("#F93745").s().p("AgFBtQg1gJgggYQgggZADg1QADg2AfgeQAfgdA0AFQA0AEAjAcQAjAbAAAzQAAAzgkAiQgdAbglAAQgMAAgLgDg");
	this.shape_61.setTransform(792.8,292.8);

	this.shape_62 = new cjs.Shape();
	this.shape_62.graphics.f("#F93745").s().p("AgFBvQg2gKgggZQghgZAEg2QACg2AggeQAfgeA1AFQA0AFAkAcQAjAcAAAzQAAAzgkAjQgdAbgnAAQgLAAgLgCg");
	this.shape_62.setTransform(792.6,293.1);

	this.shape_63 = new cjs.Shape();
	this.shape_63.graphics.f("#F93745").s().p("AgFBwQg2gKghgYQghgaADg2QADg4AggeQAfgeA2AFQA1AFAkAcQAkAcAAA0QAAA0glAkQgdAbgnAAQgLAAgMgDg");
	this.shape_63.setTransform(792.3,293.4);

	this.shape_64 = new cjs.Shape();
	this.shape_64.graphics.f("#F93745").s().p("AgFByQg3gKghgaQgigaAEg3QACg3AhgfQAggfA2AGQA1AEAlAdQAlAcAAA1QAAA0gmAkQgeAcgnAAQgMAAgLgCg");
	this.shape_64.setTransform(792.1,293.7);

	this.shape_65 = new cjs.Shape();
	this.shape_65.graphics.f("#F93745").s().p("AgFBzQg4gKgigaQgigaAEg4QADg4AgggQAhgeA3AFQA2AEAlAeQAlAcAAA2QAAA1gmAkQgeAcgnAAQgMAAgMgCg");
	this.shape_65.setTransform(791.9,294);

	this.shape_66 = new cjs.Shape();
	this.shape_66.graphics.f("#F93745").s().p("AgFB1Qg4gLgigaQgjgaAEg5QACg5AigfQAgggA4AGQA3AEAlAeQAmAdAAA2QAAA2gnAlQgfAcgoAAQgLAAgMgCg");
	this.shape_66.setTransform(791.7,294.2);

	this.shape_67 = new cjs.Shape();
	this.shape_67.graphics.f("#F93745").s().p("AgFB2Qg5gLgjgaQgjgbAEg5QADg6AhggQAhgfA5AFQA3AFAmAeQAmAdAAA3QAAA2gnAmQgeAcgpAAQgMAAgMgCg");
	this.shape_67.setTransform(791.5,294.5);

	this.shape_68 = new cjs.Shape();
	this.shape_68.graphics.f("#F93745").s().p("AgFB1Qg5gLgigaQgigaADg5QADg5AhggQAhgfA4AGQA3AEAlAeQAmAdAAA2QAAA2gnAlQgeAcgpAAQgMAAgLgCg");
	this.shape_68.setTransform(791.7,294.3);

	this.shape_69 = new cjs.Shape();
	this.shape_69.graphics.f("#F93745").s().p("AgFBzQg4gKghgZQgjgbAEg4QADg4AhggQAggeA3AFQA3AEAlAeQAlAdAAA1QAAA1gmAlQgeAcgoAAQgMAAgMgDg");
	this.shape_69.setTransform(791.8,294);

	this.shape_70 = new cjs.Shape();
	this.shape_70.graphics.f("#F93745").s().p("AgFByQg3gKghgZQgigaADg4QADg4AggfQAggeA3AFQA2AFAlAdQAkAcAAA1QAAA1glAkQgfAcgnAAQgMAAgLgDg");
	this.shape_70.setTransform(792,293.8);

	this.shape_71 = new cjs.Shape();
	this.shape_71.graphics.f("#F93745").s().p("AgFBxQg3gKghgZQghgaADg3QADg3AggfQAggeA2AFQA1AEAkAdQAlAcAAA1QAAA0gmAkQgdAbgnAAQgMAAgLgCg");
	this.shape_71.setTransform(792.3,293.6);

	this.shape_72 = new cjs.Shape();
	this.shape_72.graphics.f("#F93745").s().p("AgFBwQg2gKghgZQghgZAEg2QADg3AfgeQAggeA1AFQA1AEAjAcQAlAcAAA0QAAA0gmAjQgdAbgmAAQgLAAgMgCg");
	this.shape_72.setTransform(792.4,293.3);

	this.shape_73 = new cjs.Shape();
	this.shape_73.graphics.f("#F93745").s().p("AgFBuQg1gKghgYQgggZADg2QADg2AfgeQAfgdA1AFQA0AEAjAcQAkAcAAAzQAAAzglAjQgdAbgmAAQgLAAgLgDg");
	this.shape_73.setTransform(792.6,293.1);

	this.shape_74 = new cjs.Shape();
	this.shape_74.graphics.f("#F93745").s().p("AgFBtQg1gJgggYQgggZADg1QADg2AfgdQAfgeA0AFQAzAFAjAbQAkAcAAAyQAAAzglAiQgcAbglAAQgLAAgMgDg");
	this.shape_74.setTransform(792.8,292.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_60}]}).to({state:[{t:this.shape_60}]},22).to({state:[{t:this.shape_61}]},1).to({state:[{t:this.shape_62}]},1).to({state:[{t:this.shape_63}]},1).to({state:[{t:this.shape_64}]},1).to({state:[{t:this.shape_65}]},1).to({state:[{t:this.shape_66}]},1).to({state:[{t:this.shape_67}]},1).to({state:[{t:this.shape_68}]},1).to({state:[{t:this.shape_69}]},1).to({state:[{t:this.shape_70}]},1).to({state:[{t:this.shape_71}]},1).to({state:[{t:this.shape_72}]},1).to({state:[{t:this.shape_73}]},1).to({state:[{t:this.shape_74}]},1).to({state:[{t:this.shape_60}]},1).to({state:[{t:this.shape_61}]},1).to({state:[{t:this.shape_62}]},1).to({state:[{t:this.shape_63}]},1).to({state:[{t:this.shape_64}]},1).to({state:[{t:this.shape_65}]},1).to({state:[{t:this.shape_66}]},1).to({state:[{t:this.shape_67}]},1).to({state:[{t:this.shape_68}]},1).to({state:[{t:this.shape_69}]},1).to({state:[{t:this.shape_70}]},1).to({state:[{t:this.shape_71}]},1).to({state:[{t:this.shape_72}]},1).to({state:[{t:this.shape_73}]},1).to({state:[{t:this.shape_74}]},1).to({state:[{t:this.shape_60}]},1).to({state:[{t:this.shape_61}]},1).to({state:[{t:this.shape_62}]},1).to({state:[{t:this.shape_63}]},1).to({state:[{t:this.shape_64}]},1).to({state:[{t:this.shape_65}]},1).to({state:[{t:this.shape_66}]},1).to({state:[{t:this.shape_67}]},1).to({state:[{t:this.shape_68}]},1).to({state:[{t:this.shape_69}]},1).to({state:[{t:this.shape_70}]},1).to({state:[{t:this.shape_71}]},1).to({state:[{t:this.shape_72}]},1).to({state:[{t:this.shape_73}]},1).to({state:[{t:this.shape_74}]},1).to({state:[{t:this.shape_60}]},1).wait(6));
	this.timeline.addTween(cjs.Tween.get(this.shape_60).wait(22).to({_off:true},1).wait(14).to({_off:false},0).to({_off:true},1).wait(14).to({_off:false},0).to({_off:true},1).wait(14).to({_off:false},0).wait(6));

	// Layer 6
	this.shape_75 = new cjs.Shape();
	this.shape_75.graphics.f("#F93745").s().p("AhUBfQgogVAEg5QAEg5AhgjQAhgiAvgBQAtAAAoAhQAnAhAAAxQAAAzgeAXQgdAWg1AIQgNACgMAAQglAAgfgQg");
	this.shape_75.setTransform(719.4,300.9);

	this.shape_76 = new cjs.Shape();
	this.shape_76.graphics.f("#F93745").s().p("AhSBhQgogUADg6QACg4AhgkQAfgjAvgCQAtgBApAgQAnAgABAxQACAygdAYQgdAXg1AJQgPADgPAAQgiAAgdgOg");
	this.shape_76.setTransform(719.7,301.2);

	this.shape_77 = new cjs.Shape();
	this.shape_77.graphics.f("#F93745").s().p("AhQBiQgpgTACg5QABg5AfgkQAfgkAvgDQAtgCApAfQAoAfADAyQACAygcAYQgdAYg0AKQgQADgQAAQghAAgcgNg");
	this.shape_77.setTransform(720,301.4);

	this.shape_78 = new cjs.Shape();
	this.shape_78.graphics.f("#F93745").s().p("AhOBkQgpgSAAg6QAAg4AfgmQAegkAugEQAtgDAqAeQApAeADAyQAEAxgcAZQgcAZg0ALQgSAEgTAAQgeAAgagLg");
	this.shape_78.setTransform(720.3,301.7);

	this.shape_79 = new cjs.Shape();
	this.shape_79.graphics.f("#F93745").s().p("AhLBlQgrgRgBg5QgBg5AeglQAdgmAugEQAtgFArAdQApAdAFAyQAFAygcAZQgbAZg0AMQgUAFgUAAQgcAAgYgKg");
	this.shape_79.setTransform(720.6,302);

	this.shape_80 = new cjs.Shape();
	this.shape_80.graphics.f("#F93745").s().p("AhJBnQgrgQgCg6QgDg4AdgnQAcgmAvgFQAsgGAsAcQAqAcAGAyQAGAxgbAaQgbAagzANQgXAGgWAAQgZAAgXgIg");
	this.shape_80.setTransform(720.9,302.3);

	this.shape_81 = new cjs.Shape();
	this.shape_81.graphics.f("#F93745").s().p("AhHBoQgsgPgDg5QgEg5AcgnQAbgmAugHQAtgHAsAbQArAbAHAyQAHAygaAaQgaAag0APQgXAHgXAAQgYAAgWgIg");
	this.shape_81.setTransform(721.3,302.5);

	this.shape_82 = new cjs.Shape();
	this.shape_82.graphics.f("#F93745").s().p("AhFBqQgsgOgFg6QgFg4AbgoQAbgnAugIQAsgIAtAaQArAaAJAyQAIAxgaAbQgZAbgzAQQgaAIgZAAQgWAAgUgGg");
	this.shape_82.setTransform(721.6,302.8);

	this.shape_83 = new cjs.Shape();
	this.shape_83.graphics.f("#F93745").s().p("AhHBpQgrgPgEg6QgEg4AcgoQAbgmAvgHQAsgHAsAbQArAbAHAyQAHAxgaAbQgaAagzAPQgZAHgXAAQgYAAgVgHg");
	this.shape_83.setTransform(721.3,302.6);

	this.shape_84 = new cjs.Shape();
	this.shape_84.graphics.f("#F93745").s().p("AhJBnQgrgPgCg6QgDg4AdgnQAcgmAugGQAtgGArAbQAqAdAHAyQAGAxgbAaQgaAag0AOQgWAGgWAAQgaAAgXgJg");
	this.shape_84.setTransform(721,302.3);

	this.shape_85 = new cjs.Shape();
	this.shape_85.graphics.f("#F93745").s().p("AhLBmQgqgRgCg5QgCg5AegmQAdglAugFQAtgFArAcQApAdAGAyQAFAygbAZQgbAZg0ANQgVAFgUAAQgcAAgYgJg");
	this.shape_85.setTransform(720.8,302.1);

	this.shape_86 = new cjs.Shape();
	this.shape_86.graphics.f("#F93745").s().p("AhMBkQgqgRgBg6QAAg4AegmQAdgkAvgFQAtgEAqAeQApAdAEAzQAEAxgbAZQgcAZg0ALQgTAFgTAAQgdAAgZgLg");
	this.shape_86.setTransform(720.5,301.9);

	this.shape_87 = new cjs.Shape();
	this.shape_87.graphics.f("#F93745").s().p("AhOBjQgqgSABg6QABg4AfglQAegkAvgEQAtgDApAeQApAfADAyQADAygcAYQgcAYg0ALQgTAEgSAAQgeAAgagMg");
	this.shape_87.setTransform(720.2,301.6);

	this.shape_88 = new cjs.Shape();
	this.shape_88.graphics.f("#F93745").s().p("AhQBiQgpgTACg6QABg4AgglQAfgjAvgDQAtgCApAfQAoAgACAxQACAygcAYQgdAYg0AKQgQADgQAAQghAAgcgNg");
	this.shape_88.setTransform(719.9,301.4);

	this.shape_89 = new cjs.Shape();
	this.shape_89.graphics.f("#F93745").s().p("AhSBhQgpgVAEg5QADg5AggjQAggjAvgCQAtgBAoAgQAoAgABAyQABAygdAYQgdAXg1AIQgPADgOAAQgjAAgdgOg");
	this.shape_89.setTransform(719.6,301.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_75}]}).to({state:[{t:this.shape_75}]},22).to({state:[{t:this.shape_76}]},1).to({state:[{t:this.shape_77}]},1).to({state:[{t:this.shape_78}]},1).to({state:[{t:this.shape_79}]},1).to({state:[{t:this.shape_80}]},1).to({state:[{t:this.shape_81}]},1).to({state:[{t:this.shape_82}]},1).to({state:[{t:this.shape_83}]},1).to({state:[{t:this.shape_84}]},1).to({state:[{t:this.shape_85}]},1).to({state:[{t:this.shape_86}]},1).to({state:[{t:this.shape_87}]},1).to({state:[{t:this.shape_88}]},1).to({state:[{t:this.shape_89}]},1).to({state:[{t:this.shape_75}]},1).to({state:[{t:this.shape_76}]},1).to({state:[{t:this.shape_77}]},1).to({state:[{t:this.shape_78}]},1).to({state:[{t:this.shape_79}]},1).to({state:[{t:this.shape_80}]},1).to({state:[{t:this.shape_81}]},1).to({state:[{t:this.shape_82}]},1).to({state:[{t:this.shape_83}]},1).to({state:[{t:this.shape_84}]},1).to({state:[{t:this.shape_85}]},1).to({state:[{t:this.shape_86}]},1).to({state:[{t:this.shape_87}]},1).to({state:[{t:this.shape_88}]},1).to({state:[{t:this.shape_89}]},1).to({state:[{t:this.shape_75}]},1).to({state:[{t:this.shape_76}]},1).to({state:[{t:this.shape_77}]},1).to({state:[{t:this.shape_78}]},1).to({state:[{t:this.shape_79}]},1).to({state:[{t:this.shape_80}]},1).to({state:[{t:this.shape_81}]},1).to({state:[{t:this.shape_82}]},1).to({state:[{t:this.shape_83}]},1).to({state:[{t:this.shape_84}]},1).to({state:[{t:this.shape_85}]},1).to({state:[{t:this.shape_86}]},1).to({state:[{t:this.shape_87}]},1).to({state:[{t:this.shape_88}]},1).to({state:[{t:this.shape_89}]},1).to({state:[{t:this.shape_75}]},1).wait(6));
	this.timeline.addTween(cjs.Tween.get(this.shape_75).wait(22).to({_off:true},1).wait(14).to({_off:false},0).to({_off:true},1).wait(14).to({_off:false},0).to({_off:true},1).wait(14).to({_off:false},0).wait(6));

	// Layer 4
	this.shape_90 = new cjs.Shape();
	this.shape_90.graphics.f().s("#000000").ss(13,1,1).p("AiiAhQAlhMA9gcQA8gcA5AWQA7AVAdAzQAeAwgLAt");
	this.shape_90.setTransform(790,264);

	this.shape_91 = new cjs.Shape();
	this.shape_91.graphics.f().s("#000000").ss(13,1,1).p("AiiAjQAlhSA9gdQA8gfA5AYQA7AXAdA2QAeA0gLAx");
	this.shape_91.setTransform(790,264);

	this.shape_92 = new cjs.Shape();
	this.shape_92.graphics.f().s("#000000").ss(13,1,1).p("AiiAlQAlhXA9gfQA8ggA5AZQA7AYAdA6QAeA3gLAz");
	this.shape_92.setTransform(790,264);

	this.shape_93 = new cjs.Shape();
	this.shape_93.graphics.f().s("#000000").ss(13,1,1).p("AiiAnQAlhbA9ghQA8giA5AaQA7AaAdA8QAeA6gLA2");
	this.shape_93.setTransform(790,264);

	this.shape_94 = new cjs.Shape();
	this.shape_94.graphics.f().s("#000000").ss(13,1,1).p("AiiAoQAlheA9gjQA8gjA5AbQA7AbAdA/QAeA8gLA5");
	this.shape_94.setTransform(790,264);

	this.shape_95 = new cjs.Shape();
	this.shape_95.graphics.f().s("#000000").ss(13,1,1).p("AiiAqQAlhiA9gkQA8gkA5AcQA7AbAdBCQAeA+gLA6");
	this.shape_95.setTransform(790,264);

	this.shape_96 = new cjs.Shape();
	this.shape_96.graphics.f().s("#000000").ss(13,1,1).p("AiiArQAlhlA9gkQA8gmA5AdQA7AcAdBDQAeBBgLA7");
	this.shape_96.setTransform(790,264);

	this.shape_97 = new cjs.Shape();
	this.shape_97.graphics.f().s("#000000").ss(13,1,1).p("AiiAsQAlhnA9glQA8gnA5AeQA7AdAdBEQAeBCgLA9");
	this.shape_97.setTransform(790,264);

	this.shape_98 = new cjs.Shape();
	this.shape_98.graphics.f().s("#000000").ss(13,1,1).p("AiiAtQAlhpA9glQA8gnA5AeQA7AdAdBFQAeBDgLA+");
	this.shape_98.setTransform(790,263.9);

	this.shape_99 = new cjs.Shape();
	this.shape_99.graphics.f().s("#000000").ss(13,1,1).p("AiiAtQAlhqA9glQA8goA5AfQA7AdAdBGQAeBDgLA+");
	this.shape_99.setTransform(790,264);

	this.shape_100 = new cjs.Shape();
	this.shape_100.graphics.f().s("#000000").ss(13,1,1).p("AiiAtQAlhqA9gmQA8gnA5AeQA7AeAdBGQAeBDgLA/");
	this.shape_100.setTransform(790,264);

	this.shape_101 = new cjs.Shape();
	this.shape_101.graphics.f().s("#000000").ss(13,1,1).p("AiiAqQAlhjA9gkQA8gmA5AdQA7AcAdBCQAeBAgLA7");
	this.shape_101.setTransform(790,264);

	this.shape_102 = new cjs.Shape();
	this.shape_102.graphics.f().s("#000000").ss(13,1,1).p("AiiAnQAlhbA9ggQA8giA5AaQA7AZAdA8QAeA6gLA2");
	this.shape_102.setTransform(790,264);

	this.shape_103 = new cjs.Shape();
	this.shape_103.graphics.f().s("#000000").ss(13,1,1).p("AiiAjQAlhTA9geQA8gfA5AYQA7AXAdA4QAeA1gLAx");
	this.shape_103.setTransform(790,264);

	this.shape_104 = new cjs.Shape();
	this.shape_104.graphics.f().s("#000000").ss(13,1,1).p("AiiAiQAlhRA9gdQA8geA5AXQA7AXAdA2QAeAzgLAw");
	this.shape_104.setTransform(790,264);

	this.shape_105 = new cjs.Shape();
	this.shape_105.graphics.f().s("#000000").ss(13,1,1).p("AiiAiQAlhPA9gcQA8geA5AXQA7AWAdA0QAeAzgLAu");
	this.shape_105.setTransform(790,264);

	this.shape_106 = new cjs.Shape();
	this.shape_106.graphics.f().s("#000000").ss(13,1,1).p("AiiAhQAlhNA9gcQA8gdA5AWQA7AWAdAzQAeAxgLAu");
	this.shape_106.setTransform(790,264);

	this.shape_107 = new cjs.Shape();
	this.shape_107.graphics.f().s("#000000").ss(13,1,1).p("AiiAhQAlhMA9gcQA8gdA5AWQA7AWAdAzQAeAwgLAu");
	this.shape_107.setTransform(790,264);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_90}]}).to({state:[{t:this.shape_90}]},22).to({state:[{t:this.shape_91}]},1).to({state:[{t:this.shape_92}]},1).to({state:[{t:this.shape_93}]},1).to({state:[{t:this.shape_94}]},1).to({state:[{t:this.shape_95}]},1).to({state:[{t:this.shape_96}]},1).to({state:[{t:this.shape_97}]},1).to({state:[{t:this.shape_98}]},1).to({state:[{t:this.shape_99}]},1).to({state:[{t:this.shape_100}]},1).to({state:[{t:this.shape_100}]},30).to({state:[{t:this.shape_101}]},1).to({state:[{t:this.shape_94}]},1).to({state:[{t:this.shape_102}]},1).to({state:[{t:this.shape_92}]},1).to({state:[{t:this.shape_103}]},1).to({state:[{t:this.shape_104}]},1).to({state:[{t:this.shape_105}]},1).to({state:[{t:this.shape_106}]},1).to({state:[{t:this.shape_107}]},1).to({state:[{t:this.shape_90}]},1).wait(1));

	// Layer 3
	this.shape_108 = new cjs.Shape();
	this.shape_108.graphics.f().s("#000000").ss(13,1,1).p("AiHBBQABhCAtgiQAtgiA1AFQA2AFAkAiQAkAgABAl");
	this.shape_108.setTransform(720.3,258.7);

	this.shape_109 = new cjs.Shape();
	this.shape_109.graphics.f().s("#000000").ss(13,1,1).p("AiHBGQABhHAtgkQAtglA1AGQA2AFAkAkQAkAiABAp");
	this.shape_109.setTransform(720.3,258.3);

	this.shape_110 = new cjs.Shape();
	this.shape_110.graphics.f().s("#000000").ss(13,1,1).p("AiHBKQABhLAtgnQAtgnA1AGQA2AGAkAmQAkAlABAq");
	this.shape_110.setTransform(720.3,258);

	this.shape_111 = new cjs.Shape();
	this.shape_111.graphics.f().s("#000000").ss(13,1,1).p("AiHBOQABhPAtgpQAtgpA1AHQA2AGAkAnQAkAnABAt");
	this.shape_111.setTransform(720.3,257.7);

	this.shape_112 = new cjs.Shape();
	this.shape_112.graphics.f().s("#000000").ss(13,1,1).p("AiHBRQABhTAtgqQAtgrA1AHQA2AGAkAqQAkAoABAv");
	this.shape_112.setTransform(720.3,257.4);

	this.shape_113 = new cjs.Shape();
	this.shape_113.graphics.f().s("#000000").ss(13,1,1).p("AiHBUQABhVAtgsQAtgtA1AIQA2AGAkArQAkAqABAw");
	this.shape_113.setTransform(720.3,257.2);

	this.shape_114 = new cjs.Shape();
	this.shape_114.graphics.f().s("#000000").ss(13,1,1).p("AiHBXQABhYAtgtQAtguA1AIQA2AGAkAsQAkArABAy");
	this.shape_114.setTransform(720.3,257);

	this.shape_115 = new cjs.Shape();
	this.shape_115.graphics.f().s("#000000").ss(13,1,1).p("AiHBYQABhZAtguQAtgvA1AIQA2AHAkAtQAkArABAz");
	this.shape_115.setTransform(720.3,256.8);

	this.shape_116 = new cjs.Shape();
	this.shape_116.graphics.f().s("#000000").ss(13,1,1).p("AiHBaQABhbAtgvQAtgvA1AIQA2AGAkAuQAkAsABA0");
	this.shape_116.setTransform(720.3,256.7);

	this.shape_117 = new cjs.Shape();
	this.shape_117.graphics.f().s("#000000").ss(13,1,1).p("AiHBaQABhcAtguQAtgwA1AIQA2AHAkAuQAkAsABA0");
	this.shape_117.setTransform(720.3,256.7);

	this.shape_118 = new cjs.Shape();
	this.shape_118.graphics.f().s("#000000").ss(13,1,1).p("AiHBbQABhcAtgvQAtgwA1AIQA2AHAkAuQAkAtABA0");
	this.shape_118.setTransform(720.3,256.6);

	this.shape_119 = new cjs.Shape();
	this.shape_119.graphics.f().s("#000000").ss(13,1,1).p("AiHBWQABhXAtgtQAtgtA1AHQA2AHAkAsQAkAqABAx");
	this.shape_119.setTransform(720.3,257);

	this.shape_120 = new cjs.Shape();
	this.shape_120.graphics.f().s("#000000").ss(13,1,1).p("AiHBOQABhPAtgoQAtgpA1AGQA2AGAkAoQAkAmABAt");
	this.shape_120.setTransform(720.3,257.7);

	this.shape_121 = new cjs.Shape();
	this.shape_121.graphics.f().s("#000000").ss(13,1,1).p("AiHBIQABhJAtglQAtgmA1AGQA2AGAkAlQAkAjABAp");
	this.shape_121.setTransform(720.3,258.2);

	this.shape_122 = new cjs.Shape();
	this.shape_122.graphics.f().s("#000000").ss(13,1,1).p("AiHBFQABhGAtgkQAtgkA1AFQA2AGAkAjQAkAiABAo");
	this.shape_122.setTransform(720.3,258.4);

	this.shape_123 = new cjs.Shape();
	this.shape_123.graphics.f().s("#000000").ss(13,1,1).p("AiHBEQABhFAtgjQAtgkA1AGQA2AFAkAjQAkAhABAn");
	this.shape_123.setTransform(720.3,258.5);

	this.shape_124 = new cjs.Shape();
	this.shape_124.graphics.f().s("#000000").ss(13,1,1).p("AiHBCQABhDAtgiQAtgjA1AFQA2AGAkAiQAkAgABAm");
	this.shape_124.setTransform(720.3,258.6);

	this.shape_125 = new cjs.Shape();
	this.shape_125.graphics.f().s("#000000").ss(13,1,1).p("AiHBCQABhCAtgjQAtgiA1AFQA2AFAkAiQAkAgABAm");
	this.shape_125.setTransform(720.3,258.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_108}]}).to({state:[{t:this.shape_108}]},22).to({state:[{t:this.shape_109}]},1).to({state:[{t:this.shape_110}]},1).to({state:[{t:this.shape_111}]},1).to({state:[{t:this.shape_112}]},1).to({state:[{t:this.shape_113}]},1).to({state:[{t:this.shape_114}]},1).to({state:[{t:this.shape_115}]},1).to({state:[{t:this.shape_116}]},1).to({state:[{t:this.shape_117}]},1).to({state:[{t:this.shape_118}]},1).to({state:[{t:this.shape_118}]},30).to({state:[{t:this.shape_119}]},1).to({state:[{t:this.shape_112}]},1).to({state:[{t:this.shape_120}]},1).to({state:[{t:this.shape_110}]},1).to({state:[{t:this.shape_121}]},1).to({state:[{t:this.shape_122}]},1).to({state:[{t:this.shape_123}]},1).to({state:[{t:this.shape_124}]},1).to({state:[{t:this.shape_125}]},1).to({state:[{t:this.shape_108}]},1).wait(1));

	// Layer 2
	this.instance_1 = new lib.pack_bg();
	this.instance_1.parent = this;
	this.instance_1.setTransform(361,119);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(73));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(540,340,1080,680);
// library properties:
lib.properties = {
	id: '5BDE3903168FAF41881F114ADC96170F',
	width: 1080,
	height: 680,
	fps: 30,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: [
		{src:"assets/images/pack_bg17.png", id:"pack_bg"}
	],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['5BDE3903168FAF41881F114ADC96170F'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}



})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;

$(function () {
    var canvas, stage, exportRoot, anim_container, dom_overlay_container, fnStartAnimation;
    function init() {
        canvas = document.getElementById("canvas17");
        anim_container = document.getElementById("animation_container17");
        dom_overlay_container = document.getElementById("dom_overlay_container17");
        var comp=AdobeAn.getComposition("5BDE3903168FAF41881F114ADC96170F");
        var lib=comp.getLibrary();
        var loader = new createjs.LoadQueue(false);
        loader.addEventListener("fileload", function(evt){handleFileLoad(evt,comp)});
        loader.addEventListener("complete", function(evt){handleComplete(evt,comp)});
        var lib=comp.getLibrary();
        loader.loadManifest(lib.properties.manifest);
    }
    function handleFileLoad(evt, comp) {
        var images=comp.getImages();
        if (evt && (evt.item.type == "image")) { images[evt.item.id] = evt.result; }
    }
    function handleComplete(evt,comp) {
        //This function is always called, irrespective of the content. You can use the variable "stage" after it is created in token create_stage.
        var lib=comp.getLibrary();
        var ss=comp.getSpriteSheet();
        var queue = evt.target;
        var ssMetadata = lib.ssMetadata;
        for(i=0; i<ssMetadata.length; i++) {
            ss[ssMetadata[i].name] = new createjs.SpriteSheet( {"images": [queue.getResult(ssMetadata[i].name)], "frames": ssMetadata[i].frames} )
        }
        exportRoot = new lib.Family();
        stage = new lib.Stage(canvas);
        //Registers the "tick" event listener.
        fnStartAnimation = function() {
            stage.addChild(exportRoot);
            createjs.Ticker.setFPS(lib.properties.fps);
            createjs.Ticker.addEventListener("tick", stage);
        }
        //Code to support hidpi screens and responsive scaling.
        function makeResponsive(isResp, respDim, isScale, scaleType) {
            var lastW, lastH, lastS=1;
            window.addEventListener('resize', resizeCanvas);
            resizeCanvas();
            function resizeCanvas() {
                var w = lib.properties.width, h = lib.properties.height;
                var iw = window.innerWidth, ih=window.innerHeight;
                var pRatio = window.devicePixelRatio || 1, xRatio=iw/w, yRatio=ih/h, sRatio=1;
                if(isResp) {
                    if((respDim=='width'&&lastW==iw) || (respDim=='height'&&lastH==ih)) {
                        sRatio = lastS;
                    }
                    else if(!isScale) {
                        if(iw<w || ih<h)
                            sRatio = Math.min(xRatio, yRatio);
                    }
                    else if(scaleType==1) {
                        sRatio = Math.min(xRatio, yRatio);
                    }
                    else if(scaleType==2) {
                        sRatio = Math.max(xRatio, yRatio);
                    }
                }
                canvas.width = w*pRatio*sRatio;
                canvas.height = h*pRatio*sRatio;
                canvas.style.width = dom_overlay_container.style.width = anim_container.style.width =  w*sRatio+'px';
                canvas.style.height = anim_container.style.height = dom_overlay_container.style.height = h*sRatio+'px';
                stage.scaleX = pRatio*sRatio;
                stage.scaleY = pRatio*sRatio;
                lastW = iw; lastH = ih; lastS = sRatio;
                stage.tickOnUpdate = false;
                stage.update();
                stage.tickOnUpdate = true;
            }
        }
        makeResponsive(true,'both',true,2);
        AdobeAn.compositionLoaded(lib.properties.id);
        fnStartAnimation();
    }
    init();
});