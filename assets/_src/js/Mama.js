(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [];


// symbols:



(lib.pack_lips = function() {
	this.initialize(img.pack_lips);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,36,36);


(lib.packs1 = function() {
	this.initialize(img.packs1);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,748,808);// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.Symbol9 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(11.5,1,1).p("AjDluQhnFigOClQgOClBFAlQA8AgCag3QAtgRE6iI");
	this.shape.setTransform(31.5389,36.6751);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol9, new cjs.Rectangle(-5.7,-5.7,74.60000000000001,84.8), null);


(lib.Symbol8 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(15,1,1).p("AhZAhQgPgGAIggQAShDBaABQBhATgKBAQgFAggcAc");
	this.shape.setTransform(9.996,7.1993);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol8, new cjs.Rectangle(-7.5,-7.5,35,29.4), null);


(lib.Symbol6 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AgmAqQgQgRgBgZQABgXAQgTQAQgRAWAAQAXAAAQARQAQATAAAXQAAAZgQARQgQASgXAAQgWAAgQgSg");
	this.shape.setTransform(5.55,6);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol6, new cjs.Rectangle(0,0,11.1,12), null);


(lib.Symbol5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AgmAqQgQgRgBgZQABgXAQgTQAQgRAWAAQAXAAAQARQAQATAAAXQAAAZgQARQgQASgXAAQgWAAgQgSg");
	this.shape.setTransform(5.55,6);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol5, new cjs.Rectangle(0,0,11.1,12), null);


(lib.Symbol4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.pack_lips();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol4, new cjs.Rectangle(0,0,36,36), null);


(lib.Symbol_10_Layer_4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_4
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(12,1,1).p("AibAkQA3hbAygQQAxgQAzAQQAzAQAeArQAeAqgGAy");
	this.shape.setTransform(78.5,11.05);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(12,1,1).p("AibAmQA2hgAygQQAxgQAzAQQAzAQAeAuQAfAsgGAz");
	this.shape_1.setTransform(78.467,10.6875);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(12,1,1).p("AibAnQA1hjAygRQAxgRAzARQAzARAfAwQAfAtgGA2");
	this.shape_2.setTransform(78.4664,10.3375);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(12,1,1).p("AicApQA1hnAygSQAxgSAzASQAzASAfAxQAfAvgEA4");
	this.shape_3.setTransform(78.4535,10.0125);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(12,1,1).p("AibAqQAzhrAygSQAxgSAzASQAzASAgAzQAfAygEA5");
	this.shape_4.setTransform(78.4475,9.7125);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(12,1,1).p("AicArQAzhtAygTQAxgTAzATQAzATAgA0QAgAzgEA7");
	this.shape_5.setTransform(78.4222,9.425);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(12,1,1).p("AicAsQAzhwAxgUQAxgTAzATQA0AUAgA2QAgA0gEA9");
	this.shape_6.setTransform(78.4173,9.1875);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(12,1,1).p("AicAuQAyh0AygUQAwgUA0AUQAzAUAhA3QAgA2gDA+");
	this.shape_7.setTransform(78.392,8.9375);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(12,1,1).p("AicAuQAyh2AxgUQAwgVA0AVQA0AUAgA5QAhA3gDA/");
	this.shape_8.setTransform(78.3877,8.725);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(12,1,1).p("AicAvQAxh4AxgVQAwgVA0AVQA0AVAhA6QAgA3gCBB");
	this.shape_9.setTransform(78.4089,8.525);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(12,1,1).p("AicAwQAwh6AygVQAwgWA0AWQAzAVAiA6QAgA5gCBC");
	this.shape_10.setTransform(78.3808,8.3375);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(12,1,1).p("AicAxQAwh9AxgVQAwgWA0AWQA0AVAhA8QAhA6gCBD");
	this.shape_11.setTransform(78.3807,8.2);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(12,1,1).p("AicAxQAvh+AygVQAwgWA0AWQAzAVAiA9QAhA7gCBD");
	this.shape_12.setTransform(78.3807,8.05);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(12,1,1).p("AidAyQAwh/AxgWQAwgWA0AWQA0AWAiA9QAhA7gCBE");
	this.shape_13.setTransform(78.3556,7.9375);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(12,1,1).p("AidAzQAwiBAxgWQAwgWA0AWQA0AWAiA+QAhA7gCBF");
	this.shape_14.setTransform(78.3533,7.8375);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(12,1,1).p("AidAzQAwiCAxgVQAwgXA0AXQA0AVAhA+QAiA8gCBG");
	this.shape_15.setTransform(78.3532,7.775);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(12,1,1).p("AidAzQAviCAxgWQAwgXA0AXQA0AWAiA+QAiA9gCBF");
	this.shape_16.setTransform(78.3532,7.725);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(12,1,1).p("AidAzQAviCAxgWQAwgXA0AXQA0AWAiA+QAiA9gCBG");
	this.shape_17.setTransform(78.3532,7.675);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#000000").ss(12,1,1).p("AicAwQAwh7AxgVQAxgWAzAWQA0AVAhA7QAhA5gCBD");
	this.shape_18.setTransform(78.3807,8.2625);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#000000").ss(12,1,1).p("AicAuQAyh2AxgTQAxgVAzAVQA0ATAhA5QAgA2gDBA");
	this.shape_19.setTransform(78.3879,8.8);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#000000").ss(12,1,1).p("AicAsQAzhwAxgTQAxgUAzAUQA0ATAgA2QAgAzgEA9");
	this.shape_20.setTransform(78.4175,9.275);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#000000").ss(12,1,1).p("AibAqQAzhrAygSQAxgTAzATQAzASAgAzQAfAygEA5");
	this.shape_21.setTransform(78.4475,9.675);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#000000").ss(12,1,1).p("AicApQA1hnAygRQAxgTAzATQAzARAfAxQAfAwgEA3");
	this.shape_22.setTransform(78.4535,10.0375);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#000000").ss(12,1,1).p("AibAnQA1hjAygRQAxgRAzARQAzARAfAwQAfAugGA1");
	this.shape_23.setTransform(78.4664,10.3375);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#000000").ss(12,1,1).p("AibAmQA2hgAygRQAxgRAzARQAzARAeAuQAfAtgGAz");
	this.shape_24.setTransform(78.467,10.6);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f().s("#000000").ss(12,1,1).p("AibAlQA2heAygQQAygQAyAQQAzAQAfAtQAeAsgGAy");
	this.shape_25.setTransform(78.4926,10.7875);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f().s("#000000").ss(12,1,1).p("AibAlQA2hdAygPQAygRAzARQAyAPAfAsQAeArgGAy");
	this.shape_26.setTransform(78.5,10.9375);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f().s("#000000").ss(12,1,1).p("AibAkQA3hbAygQQAxgQAzAQQAzAQAeArQAeArgGAx");
	this.shape_27.setTransform(78.5,11.025);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},16).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_17}]},25).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_27}]},1).to({state:[{t:this.shape}]},1).wait(5));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_10_Layer_3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_3
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(12,1,1).p("Ah+BAQgGg3AmgjQAhggAzgEQAxgEAmAbQAqAcAJAw");
	this.shape.setTransform(12.7898,6.3796);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(12,1,1).p("Ah+BDQgGg5AlglQAhgiAzgEQAxgEAmAcQArAeAJAy");
	this.shape_1.setTransform(12.7331,5.7806);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(12,1,1).p("Ah+BFQgHg7AlgnQAhgjAygEQAxgEAnAdQArAfAKA0");
	this.shape_2.setTransform(12.6514,5.2508);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(12,1,1).p("Ah+BIQgHg+AlgoQAhgkAygFQAxgEAnAeQArAhAKA2");
	this.shape_3.setTransform(12.5937,4.7265);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(12,1,1).p("Ah+BLQgHhAAkgqQAhglAygFQAxgEAnAfQArAhALA5");
	this.shape_4.setTransform(12.543,4.2469);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(12,1,1).p("Ah+BNQgIhCAkgrQAhgnAygFQAxgEAnAgQArAjAMA6");
	this.shape_5.setTransform(12.5023,3.8028);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(12,1,1).p("Ah+BPQgIhEAkgsQAggnAygGQAxgEAoAhQArAjAMA8");
	this.shape_6.setTransform(12.4514,3.3784);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(12,1,1).p("Ah+BRQgIhFAjgtQAggpAzgFQAwgFAoAiQAsAkAMA9");
	this.shape_7.setTransform(12.4006,2.9984);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(12,1,1).p("Ah9BTQgJhHAjguQAggqAygFQAxgFAoAjQAsAlAMA+");
	this.shape_8.setTransform(12.367,2.624);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(12,1,1).p("Ah9BUQgJhIAjgvQAggqAygGQAwgEApAjQAsAmAMA/");
	this.shape_9.setTransform(12.316,2.3244);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(12,1,1).p("Ah9BWQgJhJAigwQAggsAygFQAwgFApAkQAsAmANBB");
	this.shape_10.setTransform(12.3069,2.0247);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(12,1,1).p("Ah9BXQgJhKAigxQAggsAygFQAwgFApAkQAsAnANBC");
	this.shape_11.setTransform(12.2569,1.7949);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(12,1,1).p("Ah9BYQgJhLAigxQAfgtAygGQAxgFApAlQAsAoANBC");
	this.shape_12.setTransform(12.2224,1.5702);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(12,1,1).p("Ah9BZQgKhMAjgyQAfgtAygFQAwgFApAlQAtAoANBD");
	this.shape_13.setTransform(12.2126,1.3952);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(12,1,1).p("Ah9BaQgJhNAigyQAfgtAygGQAxgFAoAlQAtApANBE");
	this.shape_14.setTransform(12.1963,1.2456);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(12,1,1).p("Ah9BaQgJhNAigyQAfguAygGQAwgFApAmQAtAoANBF");
	this.shape_15.setTransform(12.1713,1.1206);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(12,1,1).p("Ah9BbQgKhOAigyQAfguAygGQAxgFApAmQAsAoAOBF");
	this.shape_16.setTransform(12.1614,1.0459);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(12,1,1).p("Ah9BbQgKhOAigzQAfguAygFQAxgFApAlQAsApAOBF");
	this.shape_17.setTransform(12.1614,0.9956);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#000000").ss(12,1,1).p("Ah9BbQgKhOAigzQAfguAygGQAxgFApAmQAsApAOBF");
	this.shape_18.setTransform(12.1614,0.9709);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#000000").ss(12,1,1).p("Ah9BXQgJhLAigwQAggrAygGQAxgFAoAkQAsAnANBC");
	this.shape_19.setTransform(12.2724,1.9);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#000000").ss(12,1,1).p("Ah9BSQgJhGAjguQAggpAygGQAxgEAoAiQAsAlAMA+");
	this.shape_20.setTransform(12.367,2.7737);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#000000").ss(12,1,1).p("Ah+BOQgIhDAkgrQAggoAzgFQAwgEAoAgQArAkAMA7");
	this.shape_21.setTransform(12.4523,3.5226);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#000000").ss(12,1,1).p("Ah+BLQgHhAAkgqQAhgmAygFQAxgEAnAfQArAiALA4");
	this.shape_22.setTransform(12.543,4.2021);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#000000").ss(12,1,1).p("Ah+BIQgHg+AlgoQAggkAzgEQAwgEAoAdQAqAhALA2");
	this.shape_23.setTransform(12.6009,4.7761);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#000000").ss(12,1,1).p("Ah+BFQgHg7AlgnQAhgjAzgEQAwgEAnAdQArAfAKA0");
	this.shape_24.setTransform(12.6514,5.2559);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f().s("#000000").ss(12,1,1).p("Ah+BDQgGg5AlgmQAhgiAygEQAxgEAnAcQAqAeAKAz");
	this.shape_25.setTransform(12.7081,5.6556);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f().s("#000000").ss(12,1,1).p("Ah+BCQgGg4AlglQAighAygEQAxgEAnAbQAqAeAJAx");
	this.shape_26.setTransform(12.7394,5.9799);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f().s("#000000").ss(12,1,1).p("Ah+BAQgGg3AlgkQAiggAygEQAxgEAnAbQAqAdAJAw");
	this.shape_27.setTransform(12.7836,6.2046);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f().s("#000000").ss(12,1,1).p("Ah+BAQgGg3AmgkQAhggAzgEQAxgDAmAaQAqAdAJAw");
	this.shape_28.setTransform(12.7898,6.3546);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},16).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_18}]},25).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_27}]},1).to({state:[{t:this.shape_28}]},1).to({state:[{t:this.shape}]},1).wait(5));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_10_Layer_2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(12,1,1).p("AjZhEQAMA7A9AnQA4AkBLADQBJADA9geQBCghAfg8");
	this.shape.setTransform(42.225,50.068);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(12,1,1).p("AjZhGQAMA9A9AoQA4AlBLADQBJADA9gfQBCgiAfg9");
	this.shape_1.setTransform(42.225,49.7175);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(12,1,1).p("AjZhIQAMA/A9ApQA4AlBLAEQBJADA9ggQBCgjAfg+");
	this.shape_2.setTransform(42.225,49.3923);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(12,1,1).p("AjZhJQAMBAA9ApQA4AnBLADQBJAEA9ghQBCgjAfhB");
	this.shape_3.setTransform(42.225,49.092);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(12,1,1).p("AjZhLQAMBCA9AqQA4AoBLADQBJAEA9giQBCgkAfhC");
	this.shape_4.setTransform(42.225,48.7963);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(12,1,1).p("AjZhMQAMBCA9AsQA4AoBLADQBJAEA9giQBCglAfhD");
	this.shape_5.setTransform(42.225,48.5458);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(12,1,1).p("AjZhOQAMBEA9AsQA4ApBLADQBJAEA9giQBCgmAfhE");
	this.shape_6.setTransform(42.225,48.2708);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(12,1,1).p("AjZhPQAMBFA9AtQA4AqBLADQBJAEA9gjQBCgnAfhE");
	this.shape_7.setTransform(42.225,48.0455);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(12,1,1).p("AjZhQQAMBGA9AtQA4ArBLADQBJAEA9gkQBCgmAfhG");
	this.shape_8.setTransform(42.225,47.85);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(12,1,1).p("AjZhRQAMBGA9AuQA4ArBLAEQBJAEA9gkQBCgoAfhG");
	this.shape_9.setTransform(42.225,47.67);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(12,1,1).p("AjZhSQAMBIA9AuQA4ArBLAEQBJAEA9glQBCgnAfhI");
	this.shape_10.setTransform(42.225,47.4994);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(12,1,1).p("AjZhTQAMBIA9AvQA4AsBLAEQBJAEA9glQBCgoAfhI");
	this.shape_11.setTransform(42.225,47.3448);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(12,1,1).p("AjZhTQAMBJA9AvQA4AsBLADQBJAFA9glQBCgpAfhJ");
	this.shape_12.setTransform(42.225,47.1994);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(12,1,1).p("AjZhUQAMBJA9AwQA4AsBLAEQBJAEA9glQBCgpAfhJ");
	this.shape_13.setTransform(42.225,47.0991);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(12,1,1).p("AjZhUQAMBJA9AwQA4AtBLADQBJAFA9gmQBCgpAfhK");
	this.shape_14.setTransform(42.225,46.9991);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(12,1,1).p("AjZhVQAMBKA9AwQA4AtBLAEQBJAEA9gmQBCgpAfhK");
	this.shape_15.setTransform(42.225,46.9488);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(12,1,1).p("AjZhVQAMBKA9AxQA4AsBLAEQBJAEA9glQBCgqAfhK");
	this.shape_16.setTransform(42.225,46.8741);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(12,1,1).p("AjZhVQAMBKA9AxQA4AtBLADQBJAFA9gmQBCgqAfhK");
	this.shape_17.setTransform(42.225,46.8488);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#000000").ss(12,1,1).p("AjZhSQAMBIA9AvQA4ArBLADQBJAFA9glQBCgoAfhI");
	this.shape_18.setTransform(42.225,47.3994);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#000000").ss(12,1,1).p("AjZhQQAMBGA9AtQA4AqBLADQBJAEA9gjQBCgnAfhF");
	this.shape_19.setTransform(42.225,47.9203);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#000000").ss(12,1,1).p("AjZhNQAMBDA9AsQA4ApBLADQBJAEA9gjQBCglAfhE");
	this.shape_20.setTransform(42.225,48.3708);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#000000").ss(12,1,1).p("AjZhLQAMBBA9ArQA4AnBLAEQBJAEA9giQBCgkAfhC");
	this.shape_21.setTransform(42.225,48.7713);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#000000").ss(12,1,1).p("AjZhJQAMA/A9AqQA4AnBLADQBJAEA9ghQBCgkAfhA");
	this.shape_22.setTransform(42.225,49.1216);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#000000").ss(12,1,1).p("AjZhIQAMA/A9AoQA4AmBLADQBJAEA9ggQBCgjAfg/");
	this.shape_23.setTransform(42.225,49.4173);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#000000").ss(12,1,1).p("AjZhGQAMA9A9AoQA4AlBLADQBJAEA9ggQBCgiAfg9");
	this.shape_24.setTransform(42.225,49.6425);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f().s("#000000").ss(12,1,1).p("AjZhFQAMA8A9AnQA4AlBLADQBJAEA9gfQBCgiAfg9");
	this.shape_25.setTransform(42.225,49.8428);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f().s("#000000").ss(12,1,1).p("AjZhFQAMA8A9AnQA4AkBLADQBJAEA9gfQBCghAfg8");
	this.shape_26.setTransform(42.225,49.968);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f().s("#000000").ss(12,1,1).p("AjZhEQAMA7A9AnQA4AkBLADQBJAEA9gfQBCghAfg8");
	this.shape_27.setTransform(42.225,50.043);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},16).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_17}]},25).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_27}]},1).to({state:[{t:this.shape}]},1).wait(5));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_3_Symbol_2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Symbol_2
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(13,1,1).p("ABFBGQAjjdiyCB");
	this.shape.setTransform(7.4794,18.6195);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(13,1,1).p("AhEgPQCmiKgjDd");
	this.shape_1.setTransform(8.0703,18.805);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(13,1,1).p("Ag/gGQCbiUgiDd");
	this.shape_2.setTransform(8.6374,18.99);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(13,1,1).p("AA0BBQAijdiPCc");
	this.shape_3.setTransform(9.2325,19.1567);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(13,1,1).p("Ag6goQCRhqgjDd");
	this.shape_4.setTransform(9.1547,18.1257);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(13,1,1).p("Ag7hOQCSg2giDd");
	this.shape_5.setTransform(9.1029,16.7055);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(13,1,1).p("AA2BuQAijdiTAC");
	this.shape_6.setTransform(9.0252,14.6245);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(13,1,1).p("AhAhXQCdgogjDd");
	this.shape_7.setTransform(8.56,16.2626);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(13,1,1).p("AhDhBQCkhIgjDd");
	this.shape_8.setTransform(8.1733,17.2684);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(13,1,1).p("AhGgvQCqhhgjDd");
	this.shape_9.setTransform(7.8646,17.9252);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(13,1,1).p("AhIghQCuhzgjDd");
	this.shape_10.setTransform(7.6591,18.327);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(13,1,1).p("AhJgZQCwh9giDd");
	this.shape_11.setTransform(7.5307,18.5486);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},9).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_3}]},7).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_6}]},4).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape}]},1).wait(41));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_3_Symbol_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Symbol_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(13,1,1).p("AAwCbQC4kelXgX");
	this.shape.setTransform(13.5455,15.525);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(13,1,1).p("AhriLQFAgTibEr");
	this.shape_1.setTransform(12.5236,16.9655);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(13,1,1).p("Ahoh3QEqg/h+E4");
	this.shape_2.setTransform(11.545,18.0222);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(13,1,1).p("ABNB7QBhlFkTBq");
	this.shape_3.setTransform(10.6063,18.7456);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(13,1,1).p("AhgiDQEYgsh+E4");
	this.shape_4.setTransform(12.5004,17.3289);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(13,1,1).p("AhcieQEdASibEr");
	this.shape_5.setTransform(14.502,15.125);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(13,1,1).p("AARC4QC5kekihR");
	this.shape_6.setTransform(16.5544,12.675);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(13,1,1).p("AhfiuQEyA/i4Ee");
	this.shape_7.setTransform(15.6379,13.55);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(13,1,1).p("AhlinQFAAxi5Ee");
	this.shape_8.setTransform(14.8703,14.25);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(13,1,1).p("AhpihQFJAli4Ee");
	this.shape_9.setTransform(14.3049,14.825);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(13,1,1).p("AhsieQFRAei5Ef");
	this.shape_10.setTransform(13.8824,15.2);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(13,1,1).p("AhuibQFVAZi4Ee");
	this.shape_11.setTransform(13.6296,15.45);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},9).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_3}]},7).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_6}]},4).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape}]},1).wait(41));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_2_Layer_10 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_10
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(11.5,1,1).p("AjKgpQCDBWBigDQBkgDBMhE");
	this.shape.setTransform(41.075,69.0285);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(11.5,1,1).p("AjIgxQBzBiBmABQBjgBBVhE");
	this.shape_1.setTransform(40.675,69.05);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(11.5,1,1).p("AjGg3QBkBsBqADQBjACBchE");
	this.shape_2.setTransform(40.3,69.0266);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(11.5,1,1).p("AjFg+QBWB3BuAGQBjADBkhD");
	this.shape_3.setTransform(39.95,69.0336);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(11.5,1,1).p("AjDhEQBICAByAJQBiAFBrhE");
	this.shape_4.setTransform(39.625,69.0456);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(11.5,1,1).p("AjBhKQA8CJB0ALQBiAHBxhE");
	this.shape_5.setTransform(39.3,69.0827);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(11.5,1,1).p("AjAhPQAwCRB4ANQBiAJB3hE");
	this.shape_6.setTransform(39,69.1225);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(11.5,1,1).p("Ai+hUQAlCZB6APQBhAKB9hD");
	this.shape_7.setTransform(38.75,69.1207);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(11.5,1,1).p("Ai9hZQAbCgB9ARQBhAMCChD");
	this.shape_8.setTransform(38.5,69.1406);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(11.5,1,1).p("Ai8hdQASCmB/ATQBhANCHhD");
	this.shape_9.setTransform(38.275,69.1733);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(11.5,1,1).p("Ai6hhQAKCsCAAVQBhAOCLhD");
	this.shape_10.setTransform(38.05,69.171);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(11.5,1,1).p("Ai6hlQACCyCDAWQBhAPCPhD");
	this.shape_11.setTransform(37.9,69.2122);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(11.5,1,1).p("Ai5hoQgEC3CEAXQBhAQCShD");
	this.shape_12.setTransform(37.7191,69.2292);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(11.5,1,1).p("Ai4hqQgKC6CGAYQBgARCVhD");
	this.shape_13.setTransform(37.5652,69.239);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(11.5,1,1).p("Ai2htQgPC+CHAZQBgARCXhC");
	this.shape_14.setTransform(37.4,69.2573);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(11.5,1,1).p("Ai2hvQgSDBCHAaQBgASCahD");
	this.shape_15.setTransform(37.2639,69.2667);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(11.5,1,1).p("Ai1hwQgWDDCJAaQBgASCbhC");
	this.shape_16.setTransform(37.1537,69.2513);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(11.5,1,1).p("Ai1hxQgXDECJAbQBgATCchD");
	this.shape_17.setTransform(37.053,69.2861);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#000000").ss(11.5,1,1).p("Ai0hyQgZDGCJAbQBgASCdhC");
	this.shape_18.setTransform(37.0077,69.2763);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#000000").ss(11.5,1,1).p("Ai0hyQgaDGCKAbQBgATCdhD");
	this.shape_19.setTransform(37.0005,69.2861);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#000000").ss(11.5,1,1).p("Ai5hmQgBC0CEAWQBgAQCQhD");
	this.shape_20.setTransform(37.8249,69.1956);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#000000").ss(11.5,1,1).p("Ai8hbQAWCjB+ASQBhAMCEhD");
	this.shape_21.setTransform(38.375,69.1661);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#000000").ss(11.5,1,1).p("Ai/hSQArCVB5AOQBhAKB6hE");
	this.shape_22.setTransform(38.9,69.1082);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#000000").ss(11.5,1,1).p("AjBhJQA9CIB0AKQBiAHBwhE");
	this.shape_23.setTransform(39.375,69.0827);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#000000").ss(11.5,1,1).p("AjEhBQBOB8BxAHQBiAFBohE");
	this.shape_24.setTransform(39.75,69.039);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f().s("#000000").ss(11.5,1,1).p("AjFg7QBcByBsAFQBiACBhhD");
	this.shape_25.setTransform(40.125,69.0544);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f().s("#000000").ss(11.5,1,1).p("AjHg2QBoBpBpADQBjABBbhE");
	this.shape_26.setTransform(40.425,69.0502);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f().s("#000000").ss(11.5,1,1).p("AjIgxQBxBiBnABQBkAABVhE");
	this.shape_27.setTransform(40.65,69.025);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f().s("#000000").ss(11.5,1,1).p("AjJguQB5BdBlAAQBkgCBRhE");
	this.shape_28.setTransform(40.85,69.0251);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f().s("#000000").ss(11.5,1,1).p("AjKgrQB/BZBjgCQBkgCBPhE");
	this.shape_29.setTransform(40.975,69.0262);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f().s("#000000").ss(11.5,1,1).p("AjKgqQCCBXBjgCQBjgDBNhE");
	this.shape_30.setTransform(41.05,69.0285);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},16).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_19}]},24).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_27}]},1).to({state:[{t:this.shape_28}]},1).to({state:[{t:this.shape_29}]},1).to({state:[{t:this.shape_30}]},1).to({state:[{t:this.shape}]},1).wait(4));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_2_Layer_8 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_8
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(11.5,1,1).p("Aiog5QAxBuB3AEQAzACAugVQAugWAagm");
	this.shape.setTransform(80.75,31.4585);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(75));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_2_Layer_7 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_7
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(11.5,1,1).p("Aiog5QAxBuB3AEQAzACAtgVQAvgWAagm");
	this.shape.setTransform(14,31.4585);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(75));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_2_Layer_6 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_6
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#B2B2B2").s().p("ACDAqQgIgEglgnQgkgog4AGQg5AGgYAYQgZAYgPAMQgPAMAngrQAmgqA/AAQA+AAAoAsQAkApgEAAIgBgBg");
	this.shape.setTransform(81.0234,4.0574);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#B2B2B2").s().p("ABlALQgpg1g+AAQhAgBgnA0QgoA1ALgZQALgZAegcQAegdA7gEQA5gFAnApQAmAnAHAUQAAABAAABQAAABAAAAQAAABAAAAQAAABAAAAQgFAAgfgog");
	this.shape_1.setTransform(80.9931,5.3152);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#B2B2B2").s().p("ABnAUQgrg8g/gBQg/gCgoA9QgpA9AIgjQAHgjAjghQAighA8gDQA7gDAoApQApAoAEAgQACALgEAAQgIAAgcgpg");
	this.shape_2.setTransform(80.9269,6.2011);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#B2B2B2").s().p("ABoAcQgshCg/gCQhAgDgoBEQgqBFAFgsQAFgsAmgkQAmgkA9gCQA8gCAqAqQAqAoADAqQABAPgGAAQgJAAgbgpg");
	this.shape_3.setTransform(80.8798,6.9391);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#B2B2B2").s().p("ABpAiQgthHg/gDQhAgDgpBKQgqBKADgzQACgzApgmQAqgnA+gBQA8gBArAqQAsApABAxQABAUgHAAQgKAAgbgqg");
	this.shape_4.setTransform(80.8174,7.5322);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#B2B2B2").s().p("ABqAmQguhLg/gDQhAgDgqBNQgqBPABg4QACg4ArgoQArgpA+AAQA+gBAsArQAsApABA3QAAAXgIAAQgLAAgagrg");
	this.shape_5.setTransform(80.7741,7.9632);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#B2B2B2").s().p("ABrAoQgvhMg/gEQhAgDgqBQQgrBQABg6QABg7AsgpQAsgqA+AAQA/AAAsAqQAtAqAAA6QAAAZgIAAQgLAAgagsg");
	this.shape_6.setTransform(80.75,8.2209);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#B2B2B2").s().p("ABqApQguhNg/gEQhAgDgqBQQgrBSABg8QAAg7AsgqQAtgqA+AAQA/AAAtAqQAtAqAAA7QAAAagJAAQgMAAgagsg");
	this.shape_7.setTransform(80.75,8.3126);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#B2B2B2").s().p("AgFBhQhAgNgpAHQgqAHABg7QAAg7AsgqQAtgrA+AAQA/AAAtArQAtAqAAA7QAAA7gwAGQgOACgQAAQglAAgrgJg");
	this.shape_8.setTransform(80.75,10.3759);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#B2B2B2").s().p("AhrBlQgsgpAAg8QAAg6AsgrQAtgqA+AAQA/AAAtAqQAtArAAA6QAAA8gtApQgtArg/AAQg+AAgtgrg");
	this.shape_9.setTransform(80.75,14.225);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#B2B2B2").s().p("AhrB0QgsgwAAhEQAAhDAsgxQAtgwA+AAQA/AAAtAwQAtAxAABDQAABEgtAwQgtAxg/AAQg+AAgtgxg");
	this.shape_10.setTransform(80.75,16.35);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape}]},4).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_7}]},8).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_7}]},1).to({state:[]},1).wait(48));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_2_Layer_3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_3
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#B2B2B2").s().p("ABkAHQgZgog3gDQg2gDgoAaQgoAagOAKQgOALAogmQAngnA/AAQA+AAAmApQAmAogGAFIgCABQgIAAgWglg");
	this.shape.setTransform(15.3125,4.3274);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#B2B2B2").s().p("ABnAMQglgug+gEQg8gEgsAvQgsAvAKgVQAKgVApgeQApgfA4ADQA5ACAeApQAeApAGALQAAABAAAAQAAABAAAAQAAAAAAAAQAAAAAAAAQgDAAgfglg");
	this.shape_1.setTransform(15.3812,5.3787);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#B2B2B2").s().p("ABrAZQgmgzg8gHQg8gIgvA2QgwA3AHgeQAGgfAqghQAqgiA6ACQA7ABAjAqQAiApAEAZQABAIgDAAQgHAAgZgig");
	this.shape_2.setTransform(15.3974,6.2191);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#B2B2B2").s().p("ABtAiQglg2g7gKQg7gKgyA8QgzA8AEglQADgmArgkQArglA7ABQA9ABAmAqQAmApADAkQAAAPgFAAQgIAAgXgig");
	this.shape_3.setTransform(15.3937,6.9712);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#B2B2B2").s().p("ABvAqQglg6g7gMQg5gMg1BAQg1BCABgrQABgsAsgmQAsgoA9ABQA9ABApAqQApApACAtQAAAUgHAAQgJAAgVghg");
	this.shape_4.setTransform(15.3744,7.577);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#B2B2B2").s().p("ABxAvQglg8g6gNQg5gOg3BEQg2BEgBgvQgBgwAtgnQAsgpA9AAQA/AAArAqQArApABA0QAAAYgIAAQgJAAgUghg");
	this.shape_5.setTransform(15.3739,8.009);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#B2B2B2").s().p("AByAyQglg9g6gOQg4gPg3BGQg4BHgCgyQgBgyAsgpQAtgqA+AAQA/AAAsAqQAsApAAA4QAAAagJAAQgJAAgTghg");
	this.shape_6.setTransform(15.3475,8.2677);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#B2B2B2").s().p("AByAzQglg9g5gPQg5gPg4BGQg3BIgCgzQgCgzAsgpQAtgqA/AAQA+AAAtAqQAtApgBA5QAAAbgIAAQgKAAgTghg");
	this.shape_7.setTransform(15.3334,8.3615);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#B2B2B2").s().p("ABtBlQgsgDg/gHQg+gHguAKQguAJABg8QAAg6AsgqQAtgrA+AAQA/AAAtArQAsAqABA6QAAA6gpAAIgDAAg");
	this.shape_8.setTransform(15.25,10.0301);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#B2B2B2").s().p("AhrBlQgsgpgBg8QABg6AsgqQAtgrA+AAQA/AAAtArQAsAqABA6QgBA8gsApQgtArg/AAQg+AAgtgrg");
	this.shape_9.setTransform(15.25,14.35);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#B2B2B2").s().p("AhrB0QgsgvgBhFQABhDAsgwQAtgxA+AAQA/AAAtAxQAsAwABBDQgBBFgsAvQgtAxg/AAQg+AAgtgxg");
	this.shape_10.setTransform(15.25,16.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape}]},4).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_7}]},8).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_7}]},1).to({state:[]},1).wait(48));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_2_Layer_2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AhrB0QgsgwAAhEQAAhDAsgxQAtgwA+AAQA/AAAtAwQAtAxAABDQAABEgtAwQgtAxg/AAQg+AAgtgxg");
	this.shape.setTransform(80.75,16.35);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(75));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_2_Layer_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AhrB0QgsgvgBhFQABhDAsgwQAtgxA+AAQA/AAAtAxQAsAwABBDQgBBFgsAvQgtAxg/AAQg+AAgtgxg");
	this.shape.setTransform(15.25,16.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(75));

}).prototype = p = new cjs.MovieClip();


(lib.Scene_1_Layer_2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.instance = new lib.packs1();
	this.instance.parent = this;
	this.instance.setTransform(395,151);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Scene_1_Layer_2, null, null);


(lib.Symbol10 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_74 = function() {
		this.___loopingOver___ = true;
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(74).call(this.frame_74).wait(1));

	// Layer_2_obj_
	this.Layer_2 = new lib.Symbol_10_Layer_2();
	this.Layer_2.name = "Layer_2";
	this.Layer_2.parent = this;
	this.Layer_2.setTransform(42.2,50.1,1,1,0,0,0,42.2,50.1);
	this.Layer_2.depth = 0;
	this.Layer_2.isAttachedToCamera = 0
	this.Layer_2.isAttachedToMask = 0
	this.Layer_2.layerDepth = 0
	this.Layer_2.layerIndex = 0
	this.Layer_2.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_2).wait(75));

	// Layer_3_obj_
	this.Layer_3 = new lib.Symbol_10_Layer_3();
	this.Layer_3.name = "Layer_3";
	this.Layer_3.parent = this;
	this.Layer_3.setTransform(12.8,6.4,1,1,0,0,0,12.8,6.4);
	this.Layer_3.depth = 0;
	this.Layer_3.isAttachedToCamera = 0
	this.Layer_3.isAttachedToMask = 0
	this.Layer_3.layerDepth = 0
	this.Layer_3.layerIndex = 1
	this.Layer_3.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_3).wait(75));

	// Layer_4_obj_
	this.Layer_4 = new lib.Symbol_10_Layer_4();
	this.Layer_4.name = "Layer_4";
	this.Layer_4.parent = this;
	this.Layer_4.setTransform(78.5,11.1,1,1,0,0,0,78.5,11.1);
	this.Layer_4.depth = 0;
	this.Layer_4.isAttachedToCamera = 0
	this.Layer_4.isAttachedToMask = 0
	this.Layer_4.layerDepth = 0
	this.Layer_4.layerIndex = 2
	this.Layer_4.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_4).wait(75));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-6.6,-14.1,106.69999999999999,77.1);


(lib.Symbol7 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_4 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	var mask_graphics_18 = new cjs.Graphics().p("AqmD3ICUvpIQHAeICyIbIi5EbQgnBjAkBQQh+BFBYBpQiECjCWCNg");

	this.timeline.addTween(cjs.Tween.get(mask).to({graphics:null,x:0,y:0}).wait(18).to({graphics:mask_graphics_18,x:-47.125,y:45.4}).wait(43).to({graphics:null,x:0,y:0}).wait(14));

	// Layer_5
	this.instance = new lib.Symbol8();
	this.instance.parent = this;
	this.instance.setTransform(-29.9,68.55,1,1,66.725,0,0,1.4,12.8);
	this.instance._off = true;

	var maskedShapeInstanceList = [this.instance];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(18).to({_off:false},0).to({rotation:86.0318,x:-16.7,y:72.65},7,cjs.Ease.get(-1)).wait(30).to({rotation:66.725,x:-29.9,y:68.55},5,cjs.Ease.get(0.5)).to({_off:true},1).wait(14));

	// Layer_2
	this.instance_1 = new lib.Symbol8();
	this.instance_1.parent = this;
	this.instance_1.setTransform(51.25,54.6,1,1,0,0,0,1.4,12.8);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(9).to({rotation:66.725,guide:{path:[51.2,54.6,19.1,84.9,-29.8,68.5]}},9,cjs.Ease.get(1)).to({_off:true},1).wait(41).to({_off:false},0).to({rotation:0,guide:{path:[-29.8,68.5,19.1,84.9,51.2,54.6]}},11,cjs.Ease.get(1)).wait(4));

	// Layer_1
	this.instance_2 = new lib.Symbol9();
	this.instance_2.parent = this;
	this.instance_2.setTransform(-0.1,-0.1,1,1,0,0,0,11.8,-0.1);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(9).to({rotation:66.725,x:0.05,y:-0.2},9,cjs.Ease.get(1)).to({rotation:56.032,x:0,y:-0.45},7,cjs.Ease.get(-1)).wait(30).to({rotation:66.725,x:0.05,y:-0.2},5,cjs.Ease.get(0.5)).to({rotation:0,x:-0.1,y:-0.1},11,cjs.Ease.get(1)).wait(4));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-79.6,-18.5,157,117.8);


(lib.Symbol3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_72 = function() {
		this.___loopingOver___ = true;
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(72).call(this.frame_72).wait(1));

	// Symbol_1_obj_
	this.Symbol_1 = new lib.Symbol_3_Symbol_1();
	this.Symbol_1.name = "Symbol_1";
	this.Symbol_1.parent = this;
	this.Symbol_1.setTransform(13.6,15.5,1,1,0,0,0,13.6,15.5);
	this.Symbol_1.depth = 0;
	this.Symbol_1.isAttachedToCamera = 0
	this.Symbol_1.isAttachedToMask = 0
	this.Symbol_1.layerDepth = 0
	this.Symbol_1.layerIndex = 0
	this.Symbol_1.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Symbol_1).wait(73));

	// Symbol_2_obj_
	this.Symbol_2 = new lib.Symbol_3_Symbol_2();
	this.Symbol_2.name = "Symbol_2";
	this.Symbol_2.parent = this;
	this.Symbol_2.setTransform(7.5,18.6,1,1,0,0,0,7.5,18.6);
	this.Symbol_2.depth = 0;
	this.Symbol_2.isAttachedToCamera = 0
	this.Symbol_2.isAttachedToMask = 0
	this.Symbol_2.layerDepth = 0
	this.Symbol_2.layerIndex = 1
	this.Symbol_2.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Symbol_2).wait(73));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-6.5,-12.2,38.5,49.8);


(lib.Symbol1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Symbol3();
	this.instance.parent = this;
	this.instance.setTransform(11.8,33.35,0.6525,0.6525,0,0,0,18.1,29.8);

	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(8,1,1).p("AD7jfQAWCqgyBzQguBnhcAoQhWAlhfggQhighg/hc");
	this.shape.setTransform(35.7088,22.4493);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape},{t:this.instance}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol1, new cjs.Rectangle(-4.2,-4,69.7,52.9), null);


(lib.Symbol_2_Layer_9 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_9
	this.instance = new lib.Symbol4();
	this.instance.parent = this;
	this.instance.setTransform(42.3,71.7,1,1,-36.193,0,0,18,18.1);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(16).to({regY:18,rotation:0,x:30,y:75},19,cjs.Ease.get(1)).wait(24).to({regY:18.1,rotation:-36.193,x:42.3,y:71.7},12,cjs.Ease.get(1)).wait(4));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_2_Layer_5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_5
	this.instance = new lib.Symbol6();
	this.instance.parent = this;
	this.instance.setTransform(79.2,19.6,1,1,0,0,0,5.5,6);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(4).to({x:86.7},12,cjs.Ease.get(1)).wait(43).to({x:79.2},12,cjs.Ease.get(1)).wait(4));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_2_Layer_4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_4
	this.instance = new lib.Symbol5();
	this.instance.parent = this;
	this.instance.setTransform(13.3,19.1,1,1,0,0,0,5.5,6);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(4).to({x:20.8},12,cjs.Ease.get(1)).wait(43).to({x:13.3},12,cjs.Ease.get(1)).wait(4));

}).prototype = p = new cjs.MovieClip();


(lib.Scene_1_Layer_10 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_10
	this.instance = new lib.Symbol7();
	this.instance.parent = this;
	this.instance.setTransform(618.6,376.7,1,1,0,0,0,25.6,27.3);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Scene_1_Layer_10, null, null);


(lib.Scene_1_Layer_8 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_8
	this.instance = new lib.Symbol10();
	this.instance.parent = this;
	this.instance.setTransform(836.4,354.5,1,1,0,0,0,47.1,28.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Scene_1_Layer_8, null, null);


(lib.Scene_1_Layer_5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_5
	this.instance = new lib.Symbol1();
	this.instance.parent = this;
	this.instance.setTransform(887.75,452.45,1.4423,1.4423,0,0,0,30.6,22.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Scene_1_Layer_5, null, null);


(lib.Symbol2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_74 = function() {
		this.___loopingOver___ = true;
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(74).call(this.frame_74).wait(1));

	// Layer_10_obj_
	this.Layer_10 = new lib.Symbol_2_Layer_10();
	this.Layer_10.name = "Layer_10";
	this.Layer_10.parent = this;
	this.Layer_10.setTransform(41.1,69,1,1,0,0,0,41.1,69);
	this.Layer_10.depth = 0;
	this.Layer_10.isAttachedToCamera = 0
	this.Layer_10.isAttachedToMask = 0
	this.Layer_10.layerDepth = 0
	this.Layer_10.layerIndex = 0
	this.Layer_10.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_10).wait(75));

	// Layer_9_obj_
	this.Layer_9 = new lib.Symbol_2_Layer_9();
	this.Layer_9.name = "Layer_9";
	this.Layer_9.parent = this;
	this.Layer_9.setTransform(42.2,71.7,1,1,0,0,0,42.2,71.7);
	this.Layer_9.depth = 0;
	this.Layer_9.isAttachedToCamera = 0
	this.Layer_9.isAttachedToMask = 0
	this.Layer_9.layerDepth = 0
	this.Layer_9.layerIndex = 1
	this.Layer_9.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_9).wait(75));

	// Layer_8_obj_
	this.Layer_8 = new lib.Symbol_2_Layer_8();
	this.Layer_8.name = "Layer_8";
	this.Layer_8.parent = this;
	this.Layer_8.setTransform(80.8,31.4,1,1,0,0,0,80.8,31.4);
	this.Layer_8.depth = 0;
	this.Layer_8.isAttachedToCamera = 0
	this.Layer_8.isAttachedToMask = 0
	this.Layer_8.layerDepth = 0
	this.Layer_8.layerIndex = 2
	this.Layer_8.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_8).wait(75));

	// Layer_7_obj_
	this.Layer_7 = new lib.Symbol_2_Layer_7();
	this.Layer_7.name = "Layer_7";
	this.Layer_7.parent = this;
	this.Layer_7.setTransform(14,31.4,1,1,0,0,0,14,31.4);
	this.Layer_7.depth = 0;
	this.Layer_7.isAttachedToCamera = 0
	this.Layer_7.isAttachedToMask = 0
	this.Layer_7.layerDepth = 0
	this.Layer_7.layerIndex = 3
	this.Layer_7.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_7).wait(75));

	// Layer_6_obj_
	this.Layer_6 = new lib.Symbol_2_Layer_6();
	this.Layer_6.name = "Layer_6";
	this.Layer_6.parent = this;
	this.Layer_6.depth = 0;
	this.Layer_6.isAttachedToCamera = 0
	this.Layer_6.isAttachedToMask = 0
	this.Layer_6.layerDepth = 0
	this.Layer_6.layerIndex = 4
	this.Layer_6.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_6).wait(75));

	// Layer_3_obj_
	this.Layer_3 = new lib.Symbol_2_Layer_3();
	this.Layer_3.name = "Layer_3";
	this.Layer_3.parent = this;
	this.Layer_3.depth = 0;
	this.Layer_3.isAttachedToCamera = 0
	this.Layer_3.isAttachedToMask = 0
	this.Layer_3.layerDepth = 0
	this.Layer_3.layerIndex = 5
	this.Layer_3.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_3).wait(75));

	// Layer_5_obj_
	this.Layer_5 = new lib.Symbol_2_Layer_5();
	this.Layer_5.name = "Layer_5";
	this.Layer_5.parent = this;
	this.Layer_5.setTransform(79.3,19.6,1,1,0,0,0,79.3,19.6);
	this.Layer_5.depth = 0;
	this.Layer_5.isAttachedToCamera = 0
	this.Layer_5.isAttachedToMask = 0
	this.Layer_5.layerDepth = 0
	this.Layer_5.layerIndex = 6
	this.Layer_5.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_5).wait(75));

	// Layer_4_obj_
	this.Layer_4 = new lib.Symbol_2_Layer_4();
	this.Layer_4.name = "Layer_4";
	this.Layer_4.parent = this;
	this.Layer_4.setTransform(13.3,19.1,1,1,0,0,0,13.3,19.1);
	this.Layer_4.depth = 0;
	this.Layer_4.isAttachedToCamera = 0
	this.Layer_4.isAttachedToMask = 0
	this.Layer_4.layerDepth = 0
	this.Layer_4.layerIndex = 7
	this.Layer_4.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_4).wait(75));

	// Layer_2_obj_
	this.Layer_2 = new lib.Symbol_2_Layer_2();
	this.Layer_2.name = "Layer_2";
	this.Layer_2.parent = this;
	this.Layer_2.setTransform(80.8,16.4,1,1,0,0,0,80.8,16.4);
	this.Layer_2.depth = 0;
	this.Layer_2.isAttachedToCamera = 0
	this.Layer_2.isAttachedToMask = 0
	this.Layer_2.layerDepth = 0
	this.Layer_2.layerIndex = 8
	this.Layer_2.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_2).wait(75));

	// Layer_1_obj_
	this.Layer_1 = new lib.Symbol_2_Layer_1();
	this.Layer_1.name = "Layer_1";
	this.Layer_1.parent = this;
	this.Layer_1.setTransform(15.3,16.5,1,1,0,0,0,15.3,16.5);
	this.Layer_1.depth = 0;
	this.Layer_1.isAttachedToCamera = 0
	this.Layer_1.isAttachedToMask = 0
	this.Layer_1.layerDepth = 0
	this.Layer_1.layerIndex = 9
	this.Layer_1.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_1).wait(75));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-8.6,-0.2,112,97.10000000000001);


(lib.Scene_1_Layer_9 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_9
	this.instance = new lib.Symbol2();
	this.instance.parent = this;
	this.instance.setTransform(645.45,288.85,1,1,0,0,0,15.3,16.1);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Scene_1_Layer_9, null, null);


// stage content:
(lib.Mama = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	this.___GetDepth___ = function(obj) {
		var depth = obj.depth;
		var cameraObj = this.___camera___instance;
		if(cameraObj && cameraObj.depth && obj.isAttachedToCamera)
		{
			depth += depth + cameraObj.depth;
		}
		return depth;
		}
	this.___needSorting___ = function() {
		for (var i = 0; i < this.getNumChildren() - 1; i++)
		{
			var prevDepth = this.___GetDepth___(this.getChildAt(i));
			var nextDepth = this.___GetDepth___(this.getChildAt(i + 1));
			if (prevDepth < nextDepth)
				return true;
		}
		return false;
	}
	this.___sortFunction___ = function(obj1, obj2) {
		return (this.exportRoot.___GetDepth___(obj2) - this.exportRoot.___GetDepth___(obj1));
	}
	this.on('tick', function (event){
		var curTimeline = event.currentTarget;
		if (curTimeline.___needSorting___()){
			this.sortChildren(curTimeline.___sortFunction___);
		}
	});

	// Layer_10_obj_
	this.Layer_10 = new lib.Scene_1_Layer_10();
	this.Layer_10.name = "Layer_10";
	this.Layer_10.parent = this;
	this.Layer_10.setTransform(622.9,386.1,1,1,0,0,0,622.9,386.1);
	this.Layer_10.depth = 0;
	this.Layer_10.isAttachedToCamera = 0
	this.Layer_10.isAttachedToMask = 0
	this.Layer_10.layerDepth = 0
	this.Layer_10.layerIndex = 0
	this.Layer_10.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_10).wait(1));

	// Layer_9_obj_
	this.Layer_9 = new lib.Scene_1_Layer_9();
	this.Layer_9.name = "Layer_9";
	this.Layer_9.parent = this;
	this.Layer_9.setTransform(677.5,321.1,1,1,0,0,0,677.5,321.1);
	this.Layer_9.depth = 0;
	this.Layer_9.isAttachedToCamera = 0
	this.Layer_9.isAttachedToMask = 0
	this.Layer_9.layerDepth = 0
	this.Layer_9.layerIndex = 1
	this.Layer_9.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_9).wait(1));

	// Layer_8_obj_
	this.Layer_8 = new lib.Scene_1_Layer_8();
	this.Layer_8.name = "Layer_8";
	this.Layer_8.parent = this;
	this.Layer_8.setTransform(836.4,354.5,1,1,0,0,0,836.4,354.5);
	this.Layer_8.depth = 0;
	this.Layer_8.isAttachedToCamera = 0
	this.Layer_8.isAttachedToMask = 0
	this.Layer_8.layerDepth = 0
	this.Layer_8.layerIndex = 2
	this.Layer_8.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_8).wait(1));

	// Layer_5_obj_
	this.Layer_5 = new lib.Scene_1_Layer_5();
	this.Layer_5.name = "Layer_5";
	this.Layer_5.parent = this;
	this.Layer_5.setTransform(887.8,452.4,1,1,0,0,0,887.8,452.4);
	this.Layer_5.depth = 0;
	this.Layer_5.isAttachedToCamera = 0
	this.Layer_5.isAttachedToMask = 0
	this.Layer_5.layerDepth = 0
	this.Layer_5.layerIndex = 3
	this.Layer_5.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_5).wait(1));

	// Layer_2_obj_
	this.Layer_2 = new lib.Scene_1_Layer_2();
	this.Layer_2.name = "Layer_2";
	this.Layer_2.parent = this;
	this.Layer_2.setTransform(769,555,1,1,0,0,0,769,555);
	this.Layer_2.depth = 0;
	this.Layer_2.isAttachedToCamera = 0
	this.Layer_2.isAttachedToMask = 0
	this.Layer_2.layerDepth = 0
	this.Layer_2.layerIndex = 4
	this.Layer_2.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_2).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(540,340,603,619);
// library properties:
lib.properties = {
	id: '4E7E0DE10DA2FD4AB2F3788718A09DD7',
	width: 1080,
	height: 680,
	fps: 30,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: [
		{src:"assets/images/pack_lips31.png", id:"pack_lips"},
		{src:"assets/images/packs131.png", id:"packs1"}
	],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['4E7E0DE10DA2FD4AB2F3788718A09DD7'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}


// Layer depth API : 

AdobeAn.Layer = new function() {
	this.getLayerZDepth = function(timeline, layerName)
	{
		if(layerName === "Camera")
		layerName = "___camera___instance";
		var script = "if(timeline." + layerName + ") timeline." + layerName + ".depth; else 0;";
		return eval(script);
	}
	this.setLayerZDepth = function(timeline, layerName, zDepth)
	{
		const MAX_zDepth = 10000;
		const MIN_zDepth = -5000;
		if(zDepth > MAX_zDepth)
			zDepth = MAX_zDepth;
		else if(zDepth < MIN_zDepth)
			zDepth = MIN_zDepth;
		if(layerName === "Camera")
		layerName = "___camera___instance";
		var script = "if(timeline." + layerName + ") timeline." + layerName + ".depth = " + zDepth + ";";
		eval(script);
	}
	this.removeLayer = function(timeline, layerName)
	{
		if(layerName === "Camera")
		layerName = "___camera___instance";
		var script = "if(timeline." + layerName + ") timeline.removeChild(timeline." + layerName + ");";
		eval(script);
	}
	this.addNewLayer = function(timeline, layerName, zDepth)
	{
		if(layerName === "Camera")
		layerName = "___camera___instance";
		zDepth = typeof zDepth !== 'undefined' ? zDepth : 0;
		var layer = new createjs.MovieClip();
		layer.name = layerName;
		layer.depth = zDepth;
		layer.layerIndex = 0;
		timeline.addChild(layer);
	}
}


})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;

$(function () {
    var canvas, stage, exportRoot, anim_container, dom_overlay_container, fnStartAnimation;
    function init() {
        canvas = document.getElementById("canvas31");
        anim_container = document.getElementById("animation_container31");
        dom_overlay_container = document.getElementById("dom_overlay_container31");
        var comp=AdobeAn.getComposition("4E7E0DE10DA2FD4AB2F3788718A09DD7");
        var lib=comp.getLibrary();
        createjs.MotionGuidePlugin.install();
        var loader = new createjs.LoadQueue(false);
        loader.addEventListener("fileload", function(evt){handleFileLoad(evt,comp)});
        loader.addEventListener("complete", function(evt){handleComplete(evt,comp)});
        var lib=comp.getLibrary();
        loader.loadManifest(lib.properties.manifest);
    }
    function handleFileLoad(evt, comp) {
        var images=comp.getImages();
        if (evt && (evt.item.type == "image")) { images[evt.item.id] = evt.result; }
    }
    function handleComplete(evt,comp) {
        //This function is always called, irrespective of the content. You can use the variable "stage" after it is created in token create_stage.
        var lib=comp.getLibrary();
        var ss=comp.getSpriteSheet();
        var queue = evt.target;
        var ssMetadata = lib.ssMetadata;
        for(i=0; i<ssMetadata.length; i++) {
            ss[ssMetadata[i].name] = new createjs.SpriteSheet( {"images": [queue.getResult(ssMetadata[i].name)], "frames": ssMetadata[i].frames} )
        }
        exportRoot = new lib.Mama();
        stage = new lib.Stage(canvas);
        //Registers the "tick" event listener.
        fnStartAnimation = function() {
            stage.addChild(exportRoot);
            createjs.Ticker.setFPS(lib.properties.fps);
            createjs.Ticker.addEventListener("tick", stage)
            stage.addEventListener("tick", handleTick)
            function getProjectionMatrix(container, totalDepth) {
                var focalLength = 528.25;
                var projectionCenter = { x : lib.properties.width/2, y : lib.properties.height/2 };
                var scale = (totalDepth + focalLength)/focalLength;
                var scaleMat = new createjs.Matrix2D;
                scaleMat.a = 1/scale;
                scaleMat.d = 1/scale;
                var projMat = new createjs.Matrix2D;
                projMat.tx = -projectionCenter.x;
                projMat.ty = -projectionCenter.y;
                projMat = projMat.prependMatrix(scaleMat);
                projMat.tx += projectionCenter.x;
                projMat.ty += projectionCenter.y;
                return projMat;
            }
            function handleTick(event) {
                var cameraInstance = exportRoot.___camera___instance;
                if(cameraInstance !== undefined && cameraInstance.pinToObject !== undefined)
                {
                    cameraInstance.x = cameraInstance.pinToObject.x + cameraInstance.pinToObject.pinOffsetX;
                    cameraInstance.y = cameraInstance.pinToObject.y + cameraInstance.pinToObject.pinOffsetY;
                    if(cameraInstance.pinToObject.parent !== undefined && cameraInstance.pinToObject.parent.depth !== undefined)
                        cameraInstance.depth = cameraInstance.pinToObject.parent.depth + cameraInstance.pinToObject.pinOffsetZ;
                }
                applyLayerZDepth(exportRoot);
            }
            function applyLayerZDepth(parent)
            {
                var cameraInstance = parent.___camera___instance;
                var focalLength = 528.25;
                var projectionCenter = { 'x' : 0, 'y' : 0};
                if(parent === exportRoot)
                {
                    var stageCenter = { 'x' : lib.properties.width/2, 'y' : lib.properties.height/2 };
                    projectionCenter.x = stageCenter.x;
                    projectionCenter.y = stageCenter.y;
                }
                for(child in parent.children)
                {
                    var layerObj = parent.children[child];
                    if(layerObj == cameraInstance)
                        continue;
                    applyLayerZDepth(layerObj, cameraInstance);
                    if(layerObj.layerDepth === undefined)
                        continue;
                    if(layerObj.currentFrame != layerObj.parent.currentFrame)
                    {
                        layerObj.gotoAndPlay(layerObj.parent.currentFrame);
                    }
                    var matToApply = new createjs.Matrix2D;
                    var cameraMat = new createjs.Matrix2D;
                    var totalDepth = layerObj.layerDepth ? layerObj.layerDepth : 0;
                    var cameraDepth = 0;
                    if(cameraInstance && !layerObj.isAttachedToCamera)
                    {
                        var mat = cameraInstance.getMatrix();
                        mat.tx -= projectionCenter.x;
                        mat.ty -= projectionCenter.y;
                        cameraMat = mat.invert();
                        cameraMat.prependTransform(projectionCenter.x, projectionCenter.y, 1, 1, 0, 0, 0, 0, 0);
                        cameraMat.appendTransform(-projectionCenter.x, -projectionCenter.y, 1, 1, 0, 0, 0, 0, 0);
                        if(cameraInstance.depth)
                            cameraDepth = cameraInstance.depth;
                    }
                    if(layerObj.depth)
                    {
                        totalDepth = layerObj.depth;
                    }
                    //Offset by camera depth
                    totalDepth -= cameraDepth;
                    if(totalDepth < -focalLength)
                    {
                        matToApply.a = 0;
                        matToApply.d = 0;
                    }
                    else
                    {
                        if(layerObj.layerDepth)
                        {
                            var sizeLockedMat = getProjectionMatrix(parent, layerObj.layerDepth);
                            if(sizeLockedMat)
                            {
                                sizeLockedMat.invert();
                                matToApply.prependMatrix(sizeLockedMat);
                            }
                        }
                        matToApply.prependMatrix(cameraMat);
                        var projMat = getProjectionMatrix(parent, totalDepth);
                        if(projMat)
                        {
                            matToApply.prependMatrix(projMat);
                        }
                    }
                    layerObj.transformMatrix = matToApply;
                }
            }
        }
        //Code to support hidpi screens and responsive scaling.
        function makeResponsive(isResp, respDim, isScale, scaleType) {
            var lastW, lastH, lastS=1;
            window.addEventListener('resize', resizeCanvas);
            resizeCanvas();
            function resizeCanvas() {
                var w = lib.properties.width, h = lib.properties.height;
                var iw = window.innerWidth, ih=window.innerHeight;
                var pRatio = window.devicePixelRatio || 1, xRatio=iw/w, yRatio=ih/h, sRatio=1;
                if(isResp) {
                    if((respDim=='width'&&lastW==iw) || (respDim=='height'&&lastH==ih)) {
                        sRatio = lastS;
                    }
                    else if(!isScale) {
                        if(iw<w || ih<h)
                            sRatio = Math.min(xRatio, yRatio);
                    }
                    else if(scaleType==1) {
                        sRatio = Math.min(xRatio, yRatio);
                    }
                    else if(scaleType==2) {
                        sRatio = Math.max(xRatio, yRatio);
                    }
                }
                canvas.width = w*pRatio*sRatio;
                canvas.height = h*pRatio*sRatio;
                canvas.style.width = dom_overlay_container.style.width = anim_container.style.width =  w*sRatio+'px';
                canvas.style.height = anim_container.style.height = dom_overlay_container.style.height = h*sRatio+'px';
                stage.scaleX = pRatio*sRatio;
                stage.scaleY = pRatio*sRatio;
                lastW = iw; lastH = ih; lastS = sRatio;
                stage.tickOnUpdate = false;
                stage.update();
                stage.tickOnUpdate = true;
            }
        }
        makeResponsive(true,'both',true,2);
        AdobeAn.compositionLoaded(lib.properties.id);
        fnStartAnimation();
    }
    init();
})