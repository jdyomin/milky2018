(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [];


// symbols:



(lib.bg_part = function() {
	this.initialize(img.bg_part);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,685,267);


(lib.hand1 = function() {
	this.initialize(img.hand1);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,39,39);


(lib.packs = function() {
	this.initialize(img.packs);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,580,472);


(lib.rasp = function() {
	this.initialize(img.rasp);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,216,246);


(lib.shadow = function() {
	this.initialize(img.shadow);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,113,182);// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.Symbol17 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 10
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(11,1,1).p("Ag5AgQBUgSAfgs");
	this.shape.setTransform(-29.9,50.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(11,1,1).p("Ag6AfQBVgRAfgs");
	this.shape_1.setTransform(-31,49.4);
	this.shape_1._off = true;

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(11,1,1).p("Ag6AfQBVgRAggs");
	this.shape_2.setTransform(-32,48.9);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(11,1,1).p("Ag6AfQBVgSAfgs");
	this.shape_3.setTransform(-32.8,48.5);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(11,1,1).p("Ag6AgQBVgSAfgs");
	this.shape_4.setTransform(-33,48.3);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(11,1,1).p("Ag5AfQBUgRAfgs");
	this.shape_5.setTransform(-30.7,49.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},20).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2,p:{x:-32}}]},1).to({state:[{t:this.shape_3,p:{x:-32.8,y:48.5}}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_3,p:{x:-34,y:47.7}}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_1}]},15).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_2,p:{x:-32.1}}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape}]},1).wait(26));
	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(21).to({_off:false},0).to({_off:true},1).wait(2).to({_off:false,x:-33.4,y:48.1},0).wait(1).to({x:-33.8,y:47.9},0).to({_off:true},1).wait(1).to({_off:false,x:-34.1,y:47.7},0).wait(15).to({_off:true},1).wait(2).to({_off:false,x:-31.3,y:49.3},0).to({_off:true},1).wait(1).to({_off:false,x:-30.3,y:49.9},0).wait(1).to({x:-30,y:50},0).to({_off:true},1).wait(26));

	// Layer 9
	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(11,1,1).p("ACDAVQisCBhYja");
	this.shape_6.setTransform(-15.9,60);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(11,1,1).p("AiGhFQBbDfCyiI");
	this.shape_7.setTransform(-16.3,60.3);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(11,1,1).p("AiJhFQBcDiC3iO");
	this.shape_8.setTransform(-16.7,60.5);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(11,1,1).p("AiMhFQBdDlC8iT");
	this.shape_9.setTransform(-16.9,60.7);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(11,1,1).p("AiOhGQBeDoC/iX");
	this.shape_10.setTransform(-17.2,60.9);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(11,1,1).p("AiQhGQBgDpDBiZ");
	this.shape_11.setTransform(-17.3,61);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(11,1,1).p("AiRhGQBgDqDDib");
	this.shape_12.setTransform(-17.4,61.1);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(11,1,1).p("ACSAJQjDCbhgjq");
	this.shape_13.setTransform(-17.5,61.1);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(11,1,1).p("AiNhFQBeDlC9iU");
	this.shape_14.setTransform(-17,60.8);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(11,1,1).p("AiKhFQBdDjC4iP");
	this.shape_15.setTransform(-16.7,60.5);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(11,1,1).p("AiHhFQBbDgC0iK");
	this.shape_16.setTransform(-16.4,60.4);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(11,1,1).p("AiFhEQBaDdCxiG");
	this.shape_17.setTransform(-16.2,60.2);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#000000").ss(11,1,1).p("AiDhEQBZDbCuiD");
	this.shape_18.setTransform(-16,60.1);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#000000").ss(11,1,1).p("AiChEQBZDbCsiC");
	this.shape_19.setTransform(-15.9,60);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_6}]}).to({state:[{t:this.shape_6}]},20).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_13}]},15).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_6}]},1).wait(26));

	// Layer 7
	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#000000").ss(10,1,1).p("Aieg0QA2BjBqAGQBkAEA5hL");
	this.shape_20.setTransform(-46.9,29.1);

	this.timeline.addTween(cjs.Tween.get(this.shape_20).wait(75));

	// Layer 5
	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#C4C4C4").s().p("AhiAAQArgrA7AAQA8AAAqArQApApgXgHQgWgIgNgPQgNgPgwgMQgvgNgyAjQgzAjgKACIAAABQgJAAApgsg");
	this.shape_21.setTransform(-47,4.4);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#C4C4C4").s().p("ABgAtQgVgJg2gCQg1gBguAKQguAKgJgHQgKgHArgsQArgsA7AAQA8AAAqAsQArAsgPAHQgHAEgIAAQgKAAgLgFg");
	this.shape_22.setTransform(-46.7,5);
	this.shape_22._off = true;

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#C4C4C4").s().p("ABwBnIhdgQQg5gJg1AQQg1APgBg+QAAg+ArgsQArgsA7AAQA8AAArAsQAqAsAAA+QAAA5gbAAIgGgBg");
	this.shape_23.setTransform(-46.5,10.4);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#C4C4C4").s().p("AhmBqQgrgsAAg+QAAg+ArgsQArgsA7AAQA8AAArAsQAqAsAAA+QAAA+gqAsQgrAtg8AAQg7AAgrgtg");
	this.shape_24.setTransform(-46.5,15.1);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#C4C4C4").s().p("AhmBqQgqgsgBg+QABg+AqgsQArgsA7AAQA8AAArAsQAqAsAAA+QAAA+gqAsQgrAtg8AAQg7AAgrgtg");
	this.shape_25.setTransform(-46.5,15.1);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#C4C4C4").s().p("ABwBnIhdgPQg5gKg1AQQg2AQAAg/QAAg9ArgtQArgsA7AAQA8AAArAsQAqAtAAA9QABA5gcAAIgGgBg");
	this.shape_26.setTransform(-46.5,10.4);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#C4C4C4").s().p("AhhABQArgrA7AAQA8AAApAqQApApgWgHQgXgHgNgQQgMgPgxgMQgvgMgyAiQgyAkgLABIAAAAQgIAAApgqg");
	this.shape_27.setTransform(-47,4.3);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#C4C4C4").s().p("AiDAtQgKgCArgrQArgsA7AAQA8AAAqArQAqAqgVgCQgUgCgPgNQgQgNgygJQgygIgwAaQguAZgMAAIgBAAg");
	this.shape_28.setTransform(-46.9,4.5);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#C4C4C4").s().p("AiEAuQgKgEArgsQArgsA7AAQA8AAAqAsQAqAqgSADQgSABgRgLQgSgLg0gFQgzgGgwATQgoARgNAAIgEgBg");
	this.shape_29.setTransform(-46.8,4.7);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#C4C4C4").s().p("AiFAuQgJgFAqgsQArgsA7AAQA8AAArAsQAqArgQAFQgRAFgTgKQgTgJg2gEQg0gDguAOQgiAKgOAAQgGAAgDgCg");
	this.shape_30.setTransform(-46.8,4.8);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#C4C4C4").s().p("ABfAsQgUgJg2gCQg1gCguALQguALgJgHQgKgGArgsQArgsA7AAQA8AAAqAsQArAsgQAHQgGACgHAAQgLAAgMgFg");
	this.shape_31.setTransform(-46.7,4.9);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#C4C4C4").s().p("AhiAAQArgrA7AAQA8AAAqAqQAqAqgWgFQgWgFgNgOQgPgPgwgKQgxgKgxAfQgyAfgKAAQgKAAAqgsg");
	this.shape_32.setTransform(-47,4.4);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#C4C4C4").s().p("AhiAAQArgrA7AAQA8AAAqArQApApgWgHQgXgGgMgQQgOgOgwgMQgwgMgxAiQgzAigKACIAAAAQgJAAApgsg");
	this.shape_33.setTransform(-47,4.4);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_21}]},4).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_27}]},1).to({state:[]},1).to({state:[{t:this.shape_21}]},10).to({state:[{t:this.shape_28}]},1).to({state:[{t:this.shape_29}]},1).to({state:[{t:this.shape_30}]},1).to({state:[{t:this.shape_31}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_22}]},15).to({state:[{t:this.shape_29}]},1).to({state:[{t:this.shape_28}]},1).to({state:[{t:this.shape_32}]},1).to({state:[{t:this.shape_33}]},1).to({state:[{t:this.shape_21}]},1).to({state:[]},1).to({state:[{t:this.shape_21}]},11).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_27}]},1).to({state:[]},1).wait(8));
	this.timeline.addTween(cjs.Tween.get(this.shape_22).wait(5).to({_off:false},0).to({_off:true},1).wait(4).to({_off:false},0).to({_off:true},1).wait(16).to({_off:false},0).wait(15).to({_off:true},1).wait(17).to({_off:false},0).to({_off:true},1).wait(4).to({_off:false},0).to({_off:true},1).wait(9));

	// Layer 8
	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#000000").s().p("AgkApQgOgRAAgYQAAgXAOgSQAQgRAUAAQAVAAAOARQAQASAAAXQAAAYgQARQgOARgVAAQgUAAgQgRg");
	this.shape_34.setTransform(-41.1,19.5);

	this.timeline.addTween(cjs.Tween.get(this.shape_34).wait(75));

	// Layer 6
	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#FFFFFF").s().p("AhmBqQgrgsAAg+QAAg+ArgsQArgsA7AAQA8AAArAsQAqAsAAA+QAAA+gqAsQgrAtg8AAQg7AAgrgtg");
	this.shape_35.setTransform(-46.5,15.1);

	this.timeline.addTween(cjs.Tween.get(this.shape_35).wait(75));

	// Layer 2
	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f().s("#000000").ss(10,1,1).p("Aieg0QA2BjBqAGQBkAEA5hL");
	this.shape_36.setTransform(14.2,29.1);

	this.timeline.addTween(cjs.Tween.get(this.shape_36).wait(75));

	// Layer 4
	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f("#C4C4C4").s().p("AhhABQArgrA7AAQA8AAApAqQApApgWgHQgXgHgNgQQgMgPgxgMQgvgMgyAiQgyAkgLABIAAAAQgIAAApgqg");
	this.shape_37.setTransform(14.1,4.3);
	this.shape_37._off = true;

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f("#C4C4C4").s().p("ABgAtQgUgJg3gCQg1gBguAKQgtAKgKgHQgKgHArgsQArgsA7AAQA8AAAqAsQArAsgPAHQgIAEgIAAQgJAAgLgFg");
	this.shape_38.setTransform(14.4,5);
	this.shape_38._off = true;

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f("#C4C4C4").s().p("ABwBnIhdgPQg5gKg1AQQg2AQAAg/QAAg9ArgtQArgsA7AAQA8AAArAsQAqAtAAA9QABA5gcAAIgGgBg");
	this.shape_39.setTransform(14.6,10.4);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f("#C4C4C4").s().p("AhmBqQgqgsgBg+QABg+AqgsQArgsA7AAQA8AAArAsQAqAsAAA+QAAA+gqAsQgrAtg8AAQg7AAgrgtg");
	this.shape_40.setTransform(14.6,15.1);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f("#C4C4C4").s().p("AiDAtQgKgCArgrQArgsA7AAQA8AAAqArQAqAqgVgCQgUgCgPgNQgQgNgygJQgygIgwAaQguAZgMAAIgBAAg");
	this.shape_41.setTransform(14.2,4.5);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f("#C4C4C4").s().p("AiEAuQgKgEArgsQArgsA7AAQA8AAAqAsQAqAqgSADQgSABgRgLQgSgLg0gFQgzgGgwATQgoARgNAAIgEgBg");
	this.shape_42.setTransform(14.3,4.7);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f("#C4C4C4").s().p("AiFAuQgJgFAqgsQArgsA7AAQA8AAArAsQAqArgQAFQgRAFgTgKQgTgJg2gEQg0gDguAOQgiAKgOAAQgGAAgDgCg");
	this.shape_43.setTransform(14.3,4.8);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f("#C4C4C4").s().p("ABfAsQgUgJg2gCQg1gCguALQguALgJgHQgKgGArgsQArgsA7AAQA8AAAqAsQArAsgQAHQgGACgHAAQgLAAgMgFg");
	this.shape_44.setTransform(14.4,4.9);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f("#C4C4C4").s().p("AhiAAQArgrA7AAQA8AAAqAqQAqAqgWgFQgWgFgNgOQgPgPgwgKQgxgKgxAfQgyAfgKAAQgKAAAqgsg");
	this.shape_45.setTransform(14.1,4.4);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.f("#C4C4C4").s().p("AhiAAQArgrA7AAQA8AAAqArQApApgWgHQgXgGgMgQQgOgOgwgMQgwgMgxAiQgzAigKACIAAAAQgJAAApgsg");
	this.shape_46.setTransform(14.1,4.4);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_37}]},4).to({state:[{t:this.shape_38}]},1).to({state:[{t:this.shape_39}]},1).to({state:[{t:this.shape_40}]},1).to({state:[{t:this.shape_40}]},1).to({state:[{t:this.shape_39}]},1).to({state:[{t:this.shape_38}]},1).to({state:[{t:this.shape_37}]},1).to({state:[]},1).to({state:[{t:this.shape_37}]},10).to({state:[{t:this.shape_41}]},1).to({state:[{t:this.shape_42}]},1).to({state:[{t:this.shape_43}]},1).to({state:[{t:this.shape_44}]},1).to({state:[{t:this.shape_38}]},1).to({state:[{t:this.shape_38}]},15).to({state:[{t:this.shape_42}]},1).to({state:[{t:this.shape_41}]},1).to({state:[{t:this.shape_45}]},1).to({state:[{t:this.shape_46}]},1).to({state:[{t:this.shape_37}]},1).to({state:[]},1).to({state:[{t:this.shape_37}]},11).to({state:[{t:this.shape_38}]},1).to({state:[{t:this.shape_39}]},1).to({state:[{t:this.shape_40}]},1).to({state:[{t:this.shape_40}]},1).to({state:[{t:this.shape_39}]},1).to({state:[{t:this.shape_38}]},1).to({state:[{t:this.shape_37}]},1).to({state:[]},1).wait(8));
	this.timeline.addTween(cjs.Tween.get(this.shape_37).wait(4).to({_off:false},0).to({_off:true},1).wait(6).to({_off:false},0).to({_off:true},1).wait(10).to({_off:false},0).to({_off:true},1).wait(24).to({_off:false},0).to({_off:true},1).wait(11).to({_off:false},0).to({_off:true},1).wait(6).to({_off:false},0).to({_off:true},1).wait(8));
	this.timeline.addTween(cjs.Tween.get(this.shape_38).wait(5).to({_off:false},0).to({_off:true},1).wait(4).to({_off:false},0).to({_off:true},1).wait(16).to({_off:false},0).wait(15).to({_off:true},1).wait(17).to({_off:false},0).to({_off:true},1).wait(4).to({_off:false},0).to({_off:true},1).wait(9));

	// Layer 3
	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.f("#000000").s().p("AgkApQgOgRAAgYQAAgXAOgSQAQgRAUAAQAVAAAOARQAQASAAAXQAAAYgQARQgOARgVAAQgUAAgQgRg");
	this.shape_47.setTransform(20,19.5);

	this.timeline.addTween(cjs.Tween.get(this.shape_47).wait(75));

	// Layer 1
	this.shape_48 = new cjs.Shape();
	this.shape_48.graphics.f("#FFFFFF").s().p("AhmBqQgqgsgBg+QABg+AqgsQArgsA7AAQA8AAArAsQAqAsAAA+QAAA+gqAsQgrAtg8AAQg7AAgrgtg");
	this.shape_48.setTransform(14.6,15.1);

	this.timeline.addTween(cjs.Tween.get(this.shape_48).wait(75));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-67.8,0,102.9,72.4);


(lib.Symbol15 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(11,1,1).p("AhHgBQBci0AzEB");
	this.shape.setTransform(7.2,7.6);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol15, new cjs.Rectangle(-5.5,-5.5,25.4,26.3), null);


(lib.Symbol14 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(11,1,1).p("Ag+AYQBLjVAyEF");
	this.shape.setTransform(6.3,7.2);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol14, new cjs.Rectangle(-5.5,-5.5,23.5,25.5), null);


(lib.Symbol11 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AgvAwQgTgUAAgcQAAgbATgUQAUgUAbAAQAcAAAUAUQATAUAAAbQAAAcgTAUQgUATgcABQgbgBgUgTg");
	this.shape.setTransform(6.7,6.8);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol11, new cjs.Rectangle(0,0,13.5,13.5), null);


(lib.Symbol10 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AgvAwQgTgUAAgcQAAgbATgUQAUgUAbAAQAcAAAUAUQATAUAAAbQAAAcgTAUQgUATgcABQgbgBgUgTg");
	this.shape.setTransform(6.7,6.8);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol10, new cjs.Rectangle(0,0,13.5,13.5), null);


(lib.Symbol9 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#B7B7B7").s().p("AiMCMQg5g6gBhSQABhSA5g6QA6g5BSgBQBSABA6A5QA6A6AABSQAABSg6A6Qg6A6hSAAQhSAAg6g6g");
	this.shape.setTransform(19.9,19.9);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol9, new cjs.Rectangle(0,0,39.7,39.7), null);


(lib.Symbol7 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#B7B7B7").s().p("AiMCMQg5g6gBhSQABhSA5g6QA6g5BSgBQBSABA6A5QA6A6AABSQAABSg6A6Qg6A6hSAAQhSAAg6g6g");
	this.shape.setTransform(19.9,19.9);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol7, new cjs.Rectangle(0,0,39.7,39.7), null);


(lib.Symbol5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.shadow();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol5, new cjs.Rectangle(0,0,113,182), null);


(lib.Symbol4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.hand1();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol4, new cjs.Rectangle(0,0,39,39), null);


(lib.Symbol3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.rasp();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,216,246);


(lib.Symbol2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(12,1,1).p("AAGhDQgnhKgwgeQgygegmAfQgsBGA5ByQA6B2BzA3QBmhJArhbQAihIgJg8QgEgbgOgOQgPgPgagFQhEgMg2Bzg");
	this.shape.setTransform(25.8,24.4);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(10.9,1,1).p("AipiqQAmgfAyAeQAwAeAnBKQA2hzBEAMQAaAFAPAPQAOAOAEAbQAJA8giBIQgrBbhmBJQhzg3g6h2Qg5hyAshGg");
	this.shape_1.setTransform(25.8,24.4);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(9.9,1,1).p("AipiqQAmgfAyAeQAwAeAnBKQA2hzBEAMQAaAFAPAPQAOAOAEAbQAJA8giBIQgrBbhmBJQhzg3g6h2Qg5hyAshGg");
	this.shape_2.setTransform(25.8,24.4);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(9.1,1,1).p("AipiqQAmgfAyAeQAwAeAnBKQA2hzBEAMQAaAFAPAPQAOAOAEAbQAJA8giBIQgrBbhmBJQhzg3g6h2Qg5hyAshGg");
	this.shape_3.setTransform(25.8,24.4);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(8.5,1,1).p("AipiqQAmgfAyAeQAwAeAnBKQA2hzBEAMQAaAFAPAPQAOAOAEAbQAJA8giBIQgrBbhmBJQhzg3g6h2Qg5hyAshGg");
	this.shape_4.setTransform(25.8,24.4);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(8,1,1).p("AAGhDQgnhKgwgeQgygegmAfQgsBGA5ByQA6B2BzA3QBmhJArhbQAihIgJg8QgEgbgOgOQgPgPgagFQhEgMg2Bzg");
	this.shape_5.setTransform(25.8,24.4);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(8.8,1,1).p("AipiqQAmgfAyAeQAwAeAnBKQA2hzBEAMQAaAFAPAPQAOAOAEAbQAJA8giBIQgrBbhmBJQhzg3g6h2Qg5hyAshGg");
	this.shape_6.setTransform(25.8,24.4);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(9.6,1,1).p("AipiqQAmgfAyAeQAwAeAnBKQA2hzBEAMQAaAFAPAPQAOAOAEAbQAJA8giBIQgrBbhmBJQhzg3g6h2Qg5hyAshGg");
	this.shape_7.setTransform(25.8,24.4);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(10.2,1,1).p("AipiqQAmgfAyAeQAwAeAnBKQA2hzBEAMQAaAFAPAPQAOAOAEAbQAJA8giBIQgrBbhmBJQhzg3g6h2Qg5hyAshGg");
	this.shape_8.setTransform(25.8,24.4);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(10.8,1,1).p("AipiqQAmgfAyAeQAwAeAnBKQA2hzBEAMQAaAFAPAPQAOAOAEAbQAJA8giBIQgrBbhmBJQhzg3g6h2Qg5hyAshGg");
	this.shape_9.setTransform(25.8,24.4);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(11.3,1,1).p("AipiqQAmgfAyAeQAwAeAnBKQA2hzBEAMQAaAFAPAPQAOAOAEAbQAJA8giBIQgrBbhmBJQhzg3g6h2Qg5hyAshGg");
	this.shape_10.setTransform(25.8,24.4);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(11.7,1,1).p("AipiqQAmgfAyAeQAwAeAnBKQA2hzBEAMQAaAFAPAPQAOAOAEAbQAJA8giBIQgrBbhmBJQhzg3g6h2Qg5hyAshGg");
	this.shape_11.setTransform(25.8,24.4);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},6).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},6).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape}]},1).wait(6));
	this.timeline.addTween(cjs.Tween.get(this.shape).to({_off:true},1).wait(11).to({_off:false},0).wait(6).to({_off:true},1).wait(11).to({_off:false},0).wait(6).to({_off:true},1).wait(11).to({_off:false},0).wait(6));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,-0.2,52,49.4);


(lib.Symbol2_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#F51431").s().p("AAAAqQgRAAgNgMQgMgMAAgSQAAgQAMgNQANgNARAAIADAAQAPABALAMQANANAAAQQAAASgNAMQgLAMgPAAIgDAAg");
	this.shape_12.setTransform(4.3,4.3);

	this.timeline.addTween(cjs.Tween.get(this.shape_12).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol2_1, new cjs.Rectangle(0,0,8.5,8.5), null);


(lib.Symbol1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(12,1,1).p("AAGhDQgnhKgwgeQgygegmAfQgsBGA5ByQA6B2BzA3QBmhJArhbQAihIgJg8QgEgbgOgOQgPgPgagFQhEgMg2Bzg");
	this.shape.setTransform(25.8,24.4);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(10.9,1,1).p("AipiqQAmgfAyAeQAwAeAnBKQA2hzBEAMQAaAFAPAPQAOAOAEAbQAJA8giBIQgrBbhmBJQhzg3g6h2Qg5hyAshGg");
	this.shape_1.setTransform(25.8,24.4);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(9.9,1,1).p("AipiqQAmgfAyAeQAwAeAnBKQA2hzBEAMQAaAFAPAPQAOAOAEAbQAJA8giBIQgrBbhmBJQhzg3g6h2Qg5hyAshGg");
	this.shape_2.setTransform(25.8,24.4);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(9.1,1,1).p("AipiqQAmgfAyAeQAwAeAnBKQA2hzBEAMQAaAFAPAPQAOAOAEAbQAJA8giBIQgrBbhmBJQhzg3g6h2Qg5hyAshGg");
	this.shape_3.setTransform(25.8,24.4);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(8.5,1,1).p("AipiqQAmgfAyAeQAwAeAnBKQA2hzBEAMQAaAFAPAPQAOAOAEAbQAJA8giBIQgrBbhmBJQhzg3g6h2Qg5hyAshGg");
	this.shape_4.setTransform(25.8,24.4);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(8,1,1).p("AAGhDQgnhKgwgeQgygegmAfQgsBGA5ByQA6B2BzA3QBmhJArhbQAihIgJg8QgEgbgOgOQgPgPgagFQhEgMg2Bzg");
	this.shape_5.setTransform(25.8,24.4);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(8.8,1,1).p("AipiqQAmgfAyAeQAwAeAnBKQA2hzBEAMQAaAFAPAPQAOAOAEAbQAJA8giBIQgrBbhmBJQhzg3g6h2Qg5hyAshGg");
	this.shape_6.setTransform(25.8,24.4);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(9.6,1,1).p("AipiqQAmgfAyAeQAwAeAnBKQA2hzBEAMQAaAFAPAPQAOAOAEAbQAJA8giBIQgrBbhmBJQhzg3g6h2Qg5hyAshGg");
	this.shape_7.setTransform(25.8,24.4);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(10.2,1,1).p("AipiqQAmgfAyAeQAwAeAnBKQA2hzBEAMQAaAFAPAPQAOAOAEAbQAJA8giBIQgrBbhmBJQhzg3g6h2Qg5hyAshGg");
	this.shape_8.setTransform(25.8,24.4);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(10.8,1,1).p("AipiqQAmgfAyAeQAwAeAnBKQA2hzBEAMQAaAFAPAPQAOAOAEAbQAJA8giBIQgrBbhmBJQhzg3g6h2Qg5hyAshGg");
	this.shape_9.setTransform(25.8,24.4);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(11.3,1,1).p("AipiqQAmgfAyAeQAwAeAnBKQA2hzBEAMQAaAFAPAPQAOAOAEAbQAJA8giBIQgrBbhmBJQhzg3g6h2Qg5hyAshGg");
	this.shape_10.setTransform(25.8,24.4);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(11.7,1,1).p("AipiqQAmgfAyAeQAwAeAnBKQA2hzBEAMQAaAFAPAPQAOAOAEAbQAJA8giBIQgrBbhmBJQhzg3g6h2Qg5hyAshGg");
	this.shape_11.setTransform(25.8,24.4);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},6).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},6).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape}]},1).wait(6));
	this.timeline.addTween(cjs.Tween.get(this.shape).to({_off:true},1).wait(11).to({_off:false},0).wait(6).to({_off:true},1).wait(11).to({_off:false},0).wait(6).to({_off:true},1).wait(11).to({_off:false},0).wait(6));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,-0.2,52,49.4);


(lib.Symbol18 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Symbol 13
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(11,1,1).p("AiVAUQCWhFCVAz");
	this.shape.setTransform(31,2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(11,1,1).p("AiUAWQCVhECUAo");
	this.shape_1.setTransform(31,1.9);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(11,1,1).p("AiUAXQCVhCCUAd");
	this.shape_2.setTransform(31,1.7);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(11,1,1).p("AiUAaQCUhBCVAT");
	this.shape_3.setTransform(30.9,1.5);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(11,1,1).p("AiTAcQCTg/CVAJ");
	this.shape_4.setTransform(30.9,1.2);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(11,1,1).p("AiTAfQCTg+CVAB");
	this.shape_5.setTransform(30.9,0.9);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(11,1,1).p("AiTAiQCTg9CUgG");
	this.shape_6.setTransform(30.9,0.6);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(11,1,1).p("AiTAlQCTg8CUgN");
	this.shape_7.setTransform(30.9,0.3);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(11,1,1).p("AiTAoQCTg8CUgT");
	this.shape_8.setTransform(30.9,0.1);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(11,1,1).p("AiTAqQCTg6CUgZ");
	this.shape_9.setTransform(30.8,-0.2);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(11,1,1).p("AiTAsQCTg6CUgd");
	this.shape_10.setTransform(30.8,-0.4);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(11,1,1).p("AiTAuQCTg6CUgh");
	this.shape_11.setTransform(30.8,-0.5);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(11,1,1).p("AiSAvQCSg5CUgk");
	this.shape_12.setTransform(30.8,-0.6);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(11,1,1).p("AiSAwQCSg5CUgm");
	this.shape_13.setTransform(30.8,-0.7);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(11,1,1).p("AiSAwQCSg4CUgn");
	this.shape_14.setTransform(30.8,-0.8);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(11,1,1).p("AiSAxQCSg5CUgo");
	this.shape_15.setTransform(30.8,-0.8);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(11,1,1).p("AiRA1QCQg9CTgs");
	this.shape_16.setTransform(30.6,-1.3);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(11,1,1).p("AiPA5QCOhBCRgw");
	this.shape_17.setTransform(30.4,-1.7);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#000000").ss(11,1,1).p("AiNA+QCMhGCPg0");
	this.shape_18.setTransform(30.2,-2.2);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#000000").ss(11,1,1).p("AiLBCQCKhKCNg5");
	this.shape_19.setTransform(30,-2.6);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#000000").ss(11,1,1).p("AiKBGQCIhOCNg9");
	this.shape_20.setTransform(29.8,-3.1);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#000000").ss(11,1,1).p("AiMA6QCIhOCRgl");
	this.shape_21.setTransform(30,-1.9);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#000000").ss(11,1,1).p("AiOAtQCIhOCVgL");
	this.shape_22.setTransform(30.2,-0.6);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#000000").ss(11,1,1).p("AiRAiQCJhOCaAN");
	this.shape_23.setTransform(30.5,0.5);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#000000").ss(11,1,1).p("AiRAmQCLhICYgC");
	this.shape_24.setTransform(30.6,0.2);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f().s("#000000").ss(11,1,1).p("AiSAqQCOhDCXgP");
	this.shape_25.setTransform(30.7,-0.1);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f().s("#000000").ss(11,1,1).p("AiSAtQCQg+CVgb");
	this.shape_26.setTransform(30.7,-0.4);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f().s("#000000").ss(11,1,1).p("AiSAvQCQg7CVgi");
	this.shape_27.setTransform(30.8,-0.6);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f().s("#000000").ss(11,1,1).p("AiSAwQCRg5CVgm");
	this.shape_28.setTransform(30.8,-0.7);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f().s("#000000").ss(11,1,1).p("AiTAsQCTg7CUgc");
	this.shape_29.setTransform(30.8,-0.3);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f().s("#000000").ss(11,1,1).p("AiTAnQCTg8CUgR");
	this.shape_30.setTransform(30.9,0.2);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f().s("#000000").ss(11,1,1).p("AiTAeQCTg+CVAD");
	this.shape_31.setTransform(30.9,1);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f().s("#000000").ss(11,1,1).p("AiTAcQCUhACUAL");
	this.shape_32.setTransform(30.9,1.3);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f().s("#000000").ss(11,1,1).p("AiUAYQCUhCCVAa");
	this.shape_33.setTransform(31,1.6);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f().s("#000000").ss(11,1,1).p("AiUAXQCVhDCUAg");
	this.shape_34.setTransform(31,1.8);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f().s("#000000").ss(11,1,1).p("AiUAWQCVhDCUAl");
	this.shape_35.setTransform(31,1.8);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f().s("#000000").ss(11,1,1).p("AiUAVQCVhECUAq");
	this.shape_36.setTransform(31,1.9);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f().s("#000000").ss(11,1,1).p("AiUAVQCVhECUAt");
	this.shape_37.setTransform(31,1.9);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f().s("#000000").ss(11,1,1).p("AiVAVQCWhFCVAw");
	this.shape_38.setTransform(31,2);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f().s("#000000").ss(11,1,1).p("AiVAVQCWhGCVAy");
	this.shape_39.setTransform(31,2);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f().s("#000000").ss(11,1,1).p("AiVAUQCWhFCVAy");
	this.shape_40.setTransform(31,2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},9).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_27}]},1).to({state:[{t:this.shape_28}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_29}]},1).to({state:[{t:this.shape_30}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_31}]},1).to({state:[{t:this.shape_32}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_33}]},1).to({state:[{t:this.shape_34}]},1).to({state:[{t:this.shape_35}]},1).to({state:[{t:this.shape_36}]},1).to({state:[{t:this.shape_37}]},1).to({state:[{t:this.shape_38}]},1).to({state:[{t:this.shape_39}]},1).to({state:[{t:this.shape_40}]},1).to({state:[{t:this.shape}]},1).wait(16));

	// Symbol 14
	this.instance = new lib.Symbol14();
	this.instance.parent = this;
	this.instance.setTransform(16.9,8.7,1,1,0,0,0,6.3,7.2);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(75));

	// Symbol 15
	this.instance_1 = new lib.Symbol15();
	this.instance_1.parent = this;
	this.instance_1.setTransform(7.2,11.5,1,1,0,0,0,7.2,7.6);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(75));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-5.5,-5.5,57,30.1);


(lib.Symbol16 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Symbol 13
	this.instance = new lib.Symbol18();
	this.instance.parent = this;
	this.instance.setTransform(128.9,62.5,1,1,0,0,0,-0.3,12.8);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(9).to({regY:12.7,rotation:10.7,x:178.7,y:29.1},15,cjs.Ease.get(1)).wait(20).to({regY:12.8,rotation:0,x:128.9,y:62.5},15,cjs.Ease.get(1)).wait(16));

	// Layer 5
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(11,1,1).p("Aqrl4QBoFICGCwQCACqCjAzQChAyDMgmQDNgmEMhp");
	this.shape.setTransform(68.4,37.7);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(11,1,1).p("ArLldQCHFBCkCgQCeCaCnApQCkApC5giQC4giEShh");
	this.shape_1.setTransform(71.7,35);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(11,1,1).p("ArqlDQCkE7DACQQC7CLCqAhQCpAgClgfQClgeEZhZ");
	this.shape_2.setTransform(74.9,32.4);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(11,1,1).p("AsGksQC+E1DbCCQDWB+CsAZQCtAYCUgbQCTgcEehS");
	this.shape_3.setTransform(77.7,30.1);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(11,1,1).p("AsgkXQDXEwDyB1QDuBxCwASQCwARCDgYQCEgZEjhL");
	this.shape_4.setTransform(80.4,27.9);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(11,1,1).p("As4kDQDuErEIBpQEEBmCzAKQCzALB1gWQB0gWEohF");
	this.shape_5.setTransform(82.9,26);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(11,1,1).p("AtNjyQEBEmEcBfQEZBbC1AFQC2AEBngTQBngUEshA");
	this.shape_6.setTransform(85.1,24.3);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(11,1,1).p("AthjkQEUEjEuBVQEqBSC4gBQC4gBBbgRQBcgREwg8");
	this.shape_7.setTransform(87,22.8);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(11,1,1).p("AtyjYQElEgE9BMQE6BKC6gGQC6gGBRgOQBRgQEzg3");
	this.shape_8.setTransform(88.7,21.6);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(11,1,1).p("AuBjNQEyEdFLBEQFIBDC9gKQC7gKBIgNQBIgOE2gz");
	this.shape_9.setTransform(90.3,20.5);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(11,1,1).p("AuNjFQE+EbFWA+QFUA8C9gNQC9gNBAgMQBBgME4gx");
	this.shape_10.setTransform(91.5,19.7);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(11,1,1).p("AuXi+QFHEZFgA5QFdA3C/gQQC+gQA6gLQA6gLE6gu");
	this.shape_11.setTransform(92.6,19);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(11,1,1).p("Aufi5QFOEXFoA1QFkA0DAgSQC/gTA1gKQA1gKE8gs");
	this.shape_12.setTransform(93.4,18.5);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(11,1,1).p("Auli1QFUEWFsAyQFqAxDBgUQDAgUAxgJQAxgJE+gr");
	this.shape_13.setTransform(94,18.1);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(11,1,1).p("AuoizQFXEVFwAxQFtAvDBgVQDAgVAvgIQAwgKE9gp");
	this.shape_14.setTransform(94.3,17.9);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(11,1,1).p("AupiyQFYEVFwAvQFvAwDBgWQDAgVAvgJQAugIE+gq");
	this.shape_15.setTransform(94.4,17.8);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(11,1,1).p("AuJjIQE6EcFSBAQFQA/C9gMQC9gMBCgNQBDgME4gy");
	this.shape_16.setTransform(91.1,20);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(11,1,1).p("AtqjeQEdEiE2BQQEyBNC6gDQC5gEBWgPQBVgREyg5");
	this.shape_17.setTransform(87.9,22.2);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#000000").ss(11,1,1).p("As0kHQDqEsEEBrQEABoCyAMQCzAMB3gXQB4gWEnhH");
	this.shape_18.setTransform(82.4,26.4);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#000000").ss(11,1,1).p("AsckbQDTExDuB4QDqBzCwATQCvASCGgZQCHgZEihN");
	this.shape_19.setTransform(79.9,28.3);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#000000").ss(11,1,1).p("Arzk8QCsE5DJCMQDECHCrAeQCqAeCggeQCfgeEahW");
	this.shape_20.setTransform(75.8,31.7);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#000000").ss(11,1,1).p("ArilKQCcE9C5CUQC0CPCpAjQCoAjCqggQCqggEXhb");
	this.shape_21.setTransform(74.1,33.1);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#000000").ss(11,1,1).p("ArTlWQCOE/CsCcQCmCWCnAnQCmAnCzghQCzgiEUhe");
	this.shape_22.setTransform(72.5,34.3);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#000000").ss(11,1,1).p("ArHlgQCDFCCgCiQCaCcCmArQClAqC7gjQC6gjEShh");
	this.shape_23.setTransform(71.3,35.3);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#000000").ss(11,1,1).p("Aq9lpQB5FECXCnQCQChClAuQCjAtDCgkQDBgkEPhk");
	this.shape_24.setTransform(70.2,36.2);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f().s("#000000").ss(11,1,1).p("Aq1lvQByFFCPCsQCKClCjAvQCjAvDGgkQDGglEOhm");
	this.shape_25.setTransform(69.4,36.8);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f().s("#000000").ss(11,1,1).p("Aqvl0QBsFGCKCvQCFCnCiAyQCiAwDKglQDKgmEMhn");
	this.shape_26.setTransform(68.8,37.3);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f().s("#000000").ss(11,1,1).p("Aqsl3QBpFHCHCwQCBCpCjAzQChAxDMgmQDLglENhp");
	this.shape_27.setTransform(68.5,37.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},9).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_15}]},20).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_27}]},1).to({state:[{t:this.shape}]},1).wait(16));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-5.5,-5.5,186.1,86.4);


(lib.Symbol12 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Symbol 6 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("ABcCMQg5g6AAhSQAAhRA5g7QA7g6BSAAQBSAAA6A6QA7A7AABRQAABSg7A6Qg6A7hSAAQhSAAg7g7g");
	mask.setTransform(43.2,18);

	// Symbol 7
	this.instance = new lib.Symbol7();
	this.instance.parent = this;
	this.instance.setTransform(66.7,58.4,1,1,0,0,0,19.9,19.9);

	var maskedShapeInstanceList = [this.instance];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(10).to({x:65.7,y:52.4},0).wait(1).to({x:65.1,y:48.8},0).wait(1).to({x:64.6,y:46.2},0).wait(1).to({x:64.3,y:44.3},0).wait(1).to({x:64.1,y:42.8},0).wait(1).to({x:63.9,y:41.7},0).wait(1).to({x:63.7,y:40.8},0).wait(1).to({x:63.6,y:40.1},0).wait(1).to({x:63.5,y:39.5},0).wait(1).to({x:63.4,y:39.1},0).wait(1).to({y:38.7},0).wait(1).to({x:63.3,y:38.5},0).wait(1).to({y:38.3},0).wait(1).to({y:38.2},0).wait(1).to({y:38.1},0).wait(37).to({x:68.5,y:58.4},8,cjs.Ease.get(1)).wait(6));

	// Symbol 8 (mask)
	var mask_1 = new cjs.Shape();
	mask_1._off = true;
	mask_1.graphics.p("AiMCMQg5g6gBhSQABhSA5g6QA6g5BSgBQBSABA6A5QA6A6AABSQAABSg6A6Qg6A6hSAAQhSAAg6g6g");
	mask_1.setTransform(19.9,19.9);

	// Symbol 9
	this.instance_1 = new lib.Symbol9();
	this.instance_1.parent = this;
	this.instance_1.setTransform(20.2,60.5,1,1,0,0,0,19.9,19.9);

	var maskedShapeInstanceList = [this.instance_1];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask_1;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(10).to({x:19.2,y:54.6},0).wait(1).to({x:18.6,y:50.9},0).wait(1).to({x:18.2,y:48.4},0).wait(1).to({x:17.9,y:46.4},0).wait(1).to({x:17.6,y:45},0).wait(1).to({x:17.4,y:43.8},0).wait(1).to({x:17.3,y:42.9},0).wait(1).to({x:17.1,y:42.2},0).wait(1).to({x:17,y:41.7},0).wait(1).to({y:41.2},0).wait(1).to({x:16.9,y:40.9},0).wait(1).to({y:40.6},0).wait(1).to({x:16.8,y:40.4},0).wait(1).to({y:40.3},0).wait(38).to({x:22,y:60.5},8,cjs.Ease.get(1)).wait(6));

	// Symbol 11
	this.instance_2 = new lib.Symbol11();
	this.instance_2.parent = this;
	this.instance_2.setTransform(61.4,15.9,1,1,0,0,0,6.7,6.8);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(24).to({x:64.9,y:14.1},7,cjs.Ease.get(1)).wait(3).to({x:61.4,y:15.9},7,cjs.Ease.get(1)).wait(3).to({x:64.9,y:14.1},7,cjs.Ease.get(1)).wait(3).to({x:61.4,y:15.9},7,cjs.Ease.get(1)).wait(14));

	// Symbol 10
	this.instance_3 = new lib.Symbol10();
	this.instance_3.parent = this;
	this.instance_3.setTransform(12.5,17.3,1,1,0,0,0,6.7,6.8);

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(24).to({x:16,y:15.5},7,cjs.Ease.get(1)).wait(3).to({x:12.5,y:17.3},7,cjs.Ease.get(1)).wait(3).to({x:16,y:15.5},7,cjs.Ease.get(1)).wait(3).to({x:12.5,y:17.3},7,cjs.Ease.get(1)).wait(14));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,-1.8,86.4,41.6);


(lib.Symbol1copy2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(12,1,1).p("AjOhNQAABBA0ApQA0ApBGAHQBFAGBEgZQBDgZAYgxQAMgZgBgV");
	this.shape.setTransform(245.9,-32.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(54));

	// Layer 2
	this.instance = new lib.Symbol2();
	this.instance.parent = this;
	this.instance.setTransform(287,-70.5,1,1,0,0,0,26,24.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(4).to({regX:25.8,regY:24.4,scaleX:1.17,scaleY:1.17,x:286.8,y:-70.6},0).wait(1).to({scaleX:1.24,scaleY:1.24},0).wait(1).to({scaleX:1.28,scaleY:1.28,x:286.7},0).wait(1).to({scaleX:1.3,scaleY:1.3},0).wait(1).to({regX:26,regY:24.5,scaleX:1.3,scaleY:1.3,x:287,y:-70.5},0).wait(1).to({regX:25.8,regY:24.4,scaleX:1.29,scaleY:1.29,x:286.8,y:-70.6},0).wait(1).to({scaleX:1.26,scaleY:1.26},0).wait(1).to({scaleX:1.22,scaleY:1.22},0).wait(1).to({scaleX:1.15,scaleY:1.15,x:286.7},0).wait(1).to({scaleX:1.08,scaleY:1.08,x:286.8},0).wait(1).to({scaleX:1.02,scaleY:1.02},0).wait(1).to({regX:26,regY:24.5,scaleX:1,scaleY:1,x:287,y:-70.5},0).wait(7).to({regX:25.8,regY:24.4,scaleX:1.17,scaleY:1.17,x:286.8,y:-70.6},0).wait(1).to({scaleX:1.24,scaleY:1.24},0).wait(1).to({scaleX:1.28,scaleY:1.28,x:286.7},0).wait(1).to({scaleX:1.3,scaleY:1.3},0).wait(1).to({regX:26,regY:24.5,scaleX:1.3,scaleY:1.3,x:287,y:-70.5},0).wait(1).to({regX:25.8,regY:24.4,scaleX:1.29,scaleY:1.29,x:286.8,y:-70.6},0).wait(1).to({scaleX:1.26,scaleY:1.26},0).wait(1).to({scaleX:1.22,scaleY:1.22},0).wait(1).to({scaleX:1.15,scaleY:1.15,x:286.7},0).wait(1).to({scaleX:1.08,scaleY:1.08,x:286.8},0).wait(1).to({scaleX:1.02,scaleY:1.02},0).wait(1).to({regX:26,regY:24.5,scaleX:1,scaleY:1,x:287,y:-70.5},0).wait(7).to({regX:25.8,regY:24.4,scaleX:1.17,scaleY:1.17,x:286.8,y:-70.6},0).wait(1).to({scaleX:1.24,scaleY:1.24},0).wait(1).to({scaleX:1.28,scaleY:1.28,x:286.7},0).wait(1).to({scaleX:1.3,scaleY:1.3},0).wait(1).to({regX:26,regY:24.5,scaleX:1.3,scaleY:1.3,x:287,y:-70.5},0).wait(1).to({regX:25.8,regY:24.4,scaleX:1.29,scaleY:1.29,x:286.8,y:-70.6},0).wait(1).to({scaleX:1.26,scaleY:1.26},0).wait(1).to({scaleX:1.22,scaleY:1.22},0).wait(1).to({scaleX:1.15,scaleY:1.15,x:286.7},0).wait(1).to({scaleX:1.08,scaleY:1.08,x:286.8},0).wait(1).to({scaleX:1.02,scaleY:1.02},0).wait(1).to({regX:26,regY:24.5,scaleX:1,scaleY:1,x:287,y:-70.5},0).wait(3));

	// Layer 3
	this.instance_1 = new lib.Symbol1();
	this.instance_1.parent = this;
	this.instance_1.setTransform(205,-69.5,1,1,0,0,0,26,24.5);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(4).to({regX:25.8,regY:24.4,scaleX:1.17,scaleY:1.17,x:204.8,y:-69.6},0).wait(1).to({scaleX:1.24,scaleY:1.24},0).wait(1).to({scaleX:1.28,scaleY:1.28,x:204.7},0).wait(1).to({scaleX:1.3,scaleY:1.3},0).wait(1).to({regX:26,regY:24.5,scaleX:1.3,scaleY:1.3,x:205,y:-69.5},0).wait(1).to({regX:25.8,regY:24.4,scaleX:1.29,scaleY:1.29,x:204.8,y:-69.6},0).wait(1).to({scaleX:1.26,scaleY:1.26},0).wait(1).to({scaleX:1.22,scaleY:1.22},0).wait(1).to({scaleX:1.15,scaleY:1.15,x:204.7},0).wait(1).to({scaleX:1.08,scaleY:1.08,x:204.8},0).wait(1).to({scaleX:1.02,scaleY:1.02},0).wait(1).to({regX:26,regY:24.5,scaleX:1,scaleY:1,x:205,y:-69.5},0).wait(7).to({regX:25.8,regY:24.4,scaleX:1.17,scaleY:1.17,x:204.8,y:-69.6},0).wait(1).to({scaleX:1.24,scaleY:1.24},0).wait(1).to({scaleX:1.28,scaleY:1.28,x:204.7},0).wait(1).to({scaleX:1.3,scaleY:1.3},0).wait(1).to({regX:26,regY:24.5,scaleX:1.3,scaleY:1.3,x:205,y:-69.5},0).wait(1).to({regX:25.8,regY:24.4,scaleX:1.29,scaleY:1.29,x:204.8,y:-69.6},0).wait(1).to({scaleX:1.26,scaleY:1.26},0).wait(1).to({scaleX:1.22,scaleY:1.22},0).wait(1).to({scaleX:1.15,scaleY:1.15,x:204.7},0).wait(1).to({scaleX:1.08,scaleY:1.08,x:204.8},0).wait(1).to({scaleX:1.02,scaleY:1.02},0).wait(1).to({regX:26,regY:24.5,scaleX:1,scaleY:1,x:205,y:-69.5},0).wait(7).to({regX:25.8,regY:24.4,scaleX:1.17,scaleY:1.17,x:204.8,y:-69.6},0).wait(1).to({scaleX:1.24,scaleY:1.24},0).wait(1).to({scaleX:1.28,scaleY:1.28,x:204.7},0).wait(1).to({scaleX:1.3,scaleY:1.3},0).wait(1).to({regX:26,regY:24.5,scaleX:1.3,scaleY:1.3,x:205,y:-69.5},0).wait(1).to({regX:25.8,regY:24.4,scaleX:1.29,scaleY:1.29,x:204.8,y:-69.6},0).wait(1).to({scaleX:1.26,scaleY:1.26},0).wait(1).to({scaleX:1.22,scaleY:1.22},0).wait(1).to({scaleX:1.15,scaleY:1.15,x:204.7},0).wait(1).to({scaleX:1.08,scaleY:1.08,x:204.8},0).wait(1).to({scaleX:1.02,scaleY:1.02},0).wait(1).to({regX:26,regY:24.5,scaleX:1,scaleY:1,x:205,y:-69.5},0).wait(3));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(179.9,-95.3,131.8,76.6);


(lib.Symbol1_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#1B1814").ss(11,1,1).p("AkLhfQBDDEDRgGQBWgDBJgrQBKgtAahG");
	this.shape_12.setTransform(156,-52.6);

	this.timeline.addTween(cjs.Tween.get(this.shape_12).wait(75));

	// Layer 3
	this.instance = new lib.Symbol2_1();
	this.instance.parent = this;
	this.instance.setTransform(137.2,-49.4,2.017,1.042,0,46.1,51.9,5.4,7.5);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(12).to({_off:false},0).to({scaleY:2.02,guide:{path:[137.2,-49.3,137.5,-49,137.9,-48.7]}},4).wait(1).to({regX:4.3,regY:4.3,scaleX:2.02,scaleY:2.02,skewX:45.7,skewY:51.5,x:141.4,y:-54.8},0).wait(1).to({scaleX:2.01,scaleY:2.02,skewX:44.8,skewY:50.4,x:141.8,y:-54.4},0).wait(1).to({scaleX:2.01,scaleY:2.02,skewX:43.2,skewY:48.6,x:142.7,y:-53.7},0).wait(1).to({scaleX:2,scaleY:2.03,skewX:40.7,skewY:45.8,x:144.2,y:-52.8},0).wait(1).to({scaleX:2,scaleY:2.04,skewX:37.3,skewY:41.9,x:146.4,y:-51.7},0).wait(1).to({scaleX:1.98,scaleY:2.05,skewX:32.8,skewY:36.8,x:149.5,y:-50.7},0).wait(1).to({scaleX:1.97,scaleY:2.06,skewX:27.4,skewY:30.6,x:153.3,y:-50.1},0).wait(1).to({scaleX:1.96,scaleY:2.08,skewX:21.5,skewY:23.9,x:157.6},0).wait(1).to({scaleX:1.94,scaleY:2.09,skewX:16,skewY:17.6,x:161.1,y:-50.5},0).wait(1).to({scaleX:1.93,scaleY:2.1,skewX:11.4,skewY:12.4,x:164,y:-51.2},0).wait(1).to({scaleX:1.92,scaleY:2.11,skewX:8,skewY:8.6,x:166.1,y:-52},0).wait(1).to({scaleX:1.92,scaleY:2.11,skewX:5.8,skewY:6,x:167.5,y:-52.6},0).wait(1).to({scaleX:1.91,scaleY:2.12,skewX:4.5,skewY:4.6,x:168.2,y:-53},0).wait(1).to({regX:5.4,regY:7.5,scaleX:1.91,scaleY:2.12,rotation:4,skewX:0,skewY:0,x:170.2,y:-46.3},0).wait(7).to({regX:4.3,regY:4.3,scaleX:1.91,scaleY:2.12,rotation:0,skewX:4.6,skewY:4.7,x:168.2,y:-52.9},0).wait(1).to({scaleX:1.92,scaleY:2.11,skewX:6,skewY:6.2,x:167.3,y:-52.5},0).wait(1).to({scaleX:1.92,scaleY:2.11,skewX:8.3,skewY:8.8,x:165.8,y:-51.9},0).wait(1).to({scaleX:1.93,scaleY:2.1,skewX:11.5,skewY:12.6,x:163.6,y:-51.1},0).wait(1).to({scaleX:1.94,scaleY:2.09,skewX:15.7,skewY:17.4,x:160.6,y:-50.4},0).wait(1).to({scaleX:1.95,scaleY:2.08,skewX:20.7,skewY:23,x:157,y:-50},0).wait(1).to({scaleX:1.97,scaleY:2.06,skewX:26.1,skewY:29.1,x:153.3,y:-50.1},0).wait(1).to({scaleX:1.98,scaleY:2.05,skewX:31.2,skewY:35,x:150.1,y:-50.5},0).wait(1).to({scaleX:1.99,scaleY:2.04,skewX:35.9,skewY:40.2,x:147.3,y:-51.3},0).wait(1).to({scaleX:2,scaleY:2.03,skewX:39.6,skewY:44.5,x:145.1,y:-52.2},0).wait(1).to({scaleX:2.01,scaleY:2.03,skewX:42.5,skewY:47.7,x:143.3,y:-53.2},0).wait(1).to({scaleX:2.01,scaleY:2.02,skewX:44.4,skewY:50,x:142.1,y:-54.1},0).wait(1).to({scaleX:2.02,scaleY:2.02,skewX:45.6,skewY:51.3,x:141.5,y:-54.7},0).wait(1).to({regX:5.4,regY:7.5,scaleX:2.02,scaleY:2.02,skewX:46.1,skewY:51.9,x:137.9,y:-48.7},0).to({regX:5.5,regY:7.4,scaleY:1.23,x:135,y:-45.5},4).to({_off:true},1).wait(20));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(123.8,-67.6,64.6,30.1);


// stage content:
(lib.Russia = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 4
	this.instance = new lib.Symbol17();
	this.instance.parent = this;
	this.instance.setTransform(751.4,280.4,1,1,0,0,0,14.6,15.1);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	// Layer 14
	this.instance_1 = new lib.Symbol16();
	this.instance_1.parent = this;
	this.instance_1.setTransform(707.8,405.9,1,1,0,0,0,87.5,37.7);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(1));

	// Layer 9
	this.instance_2 = new lib.Symbol1copy2();
	this.instance_2.parent = this;
	this.instance_2.setTransform(860.2,304.2,0.896,0.896,0,0,180,245.8,-56.6);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(1));

	// Layer 8
	this.instance_3 = new lib.Symbol1_1();
	this.instance_3.parent = this;
	this.instance_3.setTransform(889.2,411.9,0.685,0.685,15,0,0,156.1,-52.7);

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(1));

	// Layer 10
	this.instance_4 = new lib.Symbol4();
	this.instance_4.parent = this;
	this.instance_4.setTransform(861.5,434.4,1,1,0,0,0,19.5,19.5);

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(1));

	// Layer 7
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(11,1,1).p("AIglhQglEAhCCWQhICjh5BJQh9BLjBgLQjCgLkXhb");
	this.shape.setTransform(925.7,418.4);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	// Layer 6
	this.instance_5 = new lib.Symbol12();
	this.instance_5.parent = this;
	this.instance_5.setTransform(894.8,396.3,0.698,0.698,19,0,0,43.4,34.8);

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(1));

	// Layer 5
	this.instance_6 = new lib.Symbol3();
	this.instance_6.parent = this;
	this.instance_6.setTransform(918.8,399.3,0.693,0.693,0,0,180,108,123);
	new cjs.ButtonHelper(this.instance_6, 0, 1, 1);

	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(1));

	// Layer 11
	this.instance_7 = new lib.Symbol5();
	this.instance_7.parent = this;
	this.instance_7.setTransform(931,419.7,1,1,0,0,0,56.5,91);

	this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(1));

	// Layer 3
	this.instance_8 = new lib.packs();
	this.instance_8.parent = this;
	this.instance_8.setTransform(500,150);

	this.timeline.addTween(cjs.Tween.get(this.instance_8).wait(1));

	// Layer 2
	this.instance_9 = new lib.bg_part();
	this.instance_9.parent = this;
	this.instance_9.setTransform(395,413);

	this.timeline.addTween(cjs.Tween.get(this.instance_9).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(540,340,1080,680);
// library properties:
lib.properties = {
	id: 'C8503C24C662F143AB9FDF6F7E54FF79',
	width: 1080,
	height: 680,
	fps: 30,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: [
		{src:"assets/images/hand128.png", id:"hand1"},
		{src:"assets/images/packs28.png", id:"packs"},
		{src:"assets/images/rasp28.png", id:"rasp"},
		{src:"assets/images/shadow28.png", id:"shadow"}
	],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['C8503C24C662F143AB9FDF6F7E54FF79'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}



})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;

$(function () {
    var canvas, stage, exportRoot, anim_container, dom_overlay_container, fnStartAnimation;
    function init() {
        canvas = document.getElementById("canvas28");
        anim_container = document.getElementById("animation_container28");
        dom_overlay_container = document.getElementById("dom_overlay_container28");
        var comp=AdobeAn.getComposition("C8503C24C662F143AB9FDF6F7E54FF79");
        var lib=comp.getLibrary();
        createjs.MotionGuidePlugin.install();
        var loader = new createjs.LoadQueue(false);
        loader.addEventListener("fileload", function(evt){handleFileLoad(evt,comp)});
        loader.addEventListener("complete", function(evt){handleComplete(evt,comp)});
        var lib=comp.getLibrary();
        loader.loadManifest(lib.properties.manifest);
    }
    function handleFileLoad(evt, comp) {
        var images=comp.getImages();
        if (evt && (evt.item.type == "image")) { images[evt.item.id] = evt.result; }
    }
    function handleComplete(evt,comp) {
        //This function is always called, irrespective of the content. You can use the variable "stage" after it is created in token create_stage.
        var lib=comp.getLibrary();
        var ss=comp.getSpriteSheet();
        var queue = evt.target;
        var ssMetadata = lib.ssMetadata;
        for(i=0; i<ssMetadata.length; i++) {
            ss[ssMetadata[i].name] = new createjs.SpriteSheet( {"images": [queue.getResult(ssMetadata[i].name)], "frames": ssMetadata[i].frames} )
        }
        exportRoot = new lib.Russia();
        stage = new lib.Stage(canvas);
        stage.enableMouseOver();
        //Registers the "tick" event listener.
        fnStartAnimation = function() {
            stage.addChild(exportRoot);
            createjs.Ticker.setFPS(lib.properties.fps);
            createjs.Ticker.addEventListener("tick", stage);
        }
        //Code to support hidpi screens and responsive scaling.
        function makeResponsive(isResp, respDim, isScale, scaleType) {
            var lastW, lastH, lastS=1;
            window.addEventListener('resize', resizeCanvas);
            resizeCanvas();
            function resizeCanvas() {
                var w = lib.properties.width, h = lib.properties.height;
                var iw = window.innerWidth, ih=window.innerHeight;
                var pRatio = window.devicePixelRatio || 1, xRatio=iw/w, yRatio=ih/h, sRatio=1;
                if(isResp) {
                    if((respDim=='width'&&lastW==iw) || (respDim=='height'&&lastH==ih)) {
                        sRatio = lastS;
                    }
                    else if(!isScale) {
                        if(iw<w || ih<h)
                            sRatio = Math.min(xRatio, yRatio);
                    }
                    else if(scaleType==1) {
                        sRatio = Math.min(xRatio, yRatio);
                    }
                    else if(scaleType==2) {
                        sRatio = Math.max(xRatio, yRatio);
                    }
                }
                canvas.width = w*pRatio*sRatio;
                canvas.height = h*pRatio*sRatio;
                canvas.style.width = dom_overlay_container.style.width = anim_container.style.width =  w*sRatio+'px';
                canvas.style.height = anim_container.style.height = dom_overlay_container.style.height = h*sRatio+'px';
                stage.scaleX = pRatio*sRatio;
                stage.scaleY = pRatio*sRatio;
                lastW = iw; lastH = ih; lastS = sRatio;
                stage.tickOnUpdate = false;
                stage.update();
                stage.tickOnUpdate = true;
            }
        }
        makeResponsive(true,'both',true,2);
        AdobeAn.compositionLoaded(lib.properties.id);
        fnStartAnimation();
    }
    init();
});