(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [];


// symbols:



(lib.bg_vert = function() {
	this.initialize(img.bg_vert);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,375,812);


(lib.pack = function() {
	this.initialize(img.pack);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,300,461);


(lib.shadow = function() {
	this.initialize(img.shadow);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,710,229);// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.Symbol10 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Symbol 7
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#141313").ss(6.5,1,1).p("AA2BwQihgqBPi1");
	this.shape.setTransform(5.4552,11.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#141313").ss(6.5,1,1).p("AgZhuQhSCzChAq");
	this.shape_1.setTransform(5.5221,11.275);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#141313").ss(6.5,1,1).p("AgXhtQhVCyChAp");
	this.shape_2.setTransform(5.5767,11.35);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#141313").ss(6.5,1,1).p("AgVhtQhXCxChAq");
	this.shape_3.setTransform(5.6301,11.4);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#141313").ss(6.5,1,1).p("AgThtQhaCxChAq");
	this.shape_4.setTransform(5.6825,11.45);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#141313").ss(6.5,1,1).p("AgShsQhbCvChAq");
	this.shape_5.setTransform(5.7237,11.5);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#141313").ss(6.5,1,1).p("AgRhsQhdCvChAq");
	this.shape_6.setTransform(5.7541,11.55);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#141313").ss(6.5,1,1).p("AgPhrQhfCtChAq");
	this.shape_7.setTransform(5.7942,11.6);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#141313").ss(6.5,1,1).p("AgOhrQhgCtChAq");
	this.shape_8.setTransform(5.814,11.625);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#141313").ss(6.5,1,1).p("AgNhqQhhCsChAq");
	this.shape_9.setTransform(5.8434,11.65);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#141313").ss(6.5,1,1).p("AgMhqQhjCrChAq");
	this.shape_10.setTransform(5.8628,11.675);
	this.shape_10._off = true;

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#141313").ss(6.5,1,1).p("AgLhqQhkCrChAq");
	this.shape_11.setTransform(5.882,11.725);
	this.shape_11._off = true;

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#141313").ss(6.5,1,1).p("AAyBrQihgqBkir");
	this.shape_12.setTransform(5.8916,11.725);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#141313").ss(6.5,1,1).p("AgQhrQheCtChAq");
	this.shape_13.setTransform(5.7642,11.575);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#141313").ss(6.5,1,1).p("AgRhsQhcCvChAq");
	this.shape_14.setTransform(5.7339,11.525);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#141313").ss(6.5,1,1).p("AgThsQhaCvChAq");
	this.shape_15.setTransform(5.7032,11.475);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#141313").ss(6.5,1,1).p("AgUhtQhZCxChAq");
	this.shape_16.setTransform(5.6617,11.45);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#141313").ss(6.5,1,1).p("AgWhtQhWCyChAp");
	this.shape_17.setTransform(5.5982,11.35);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#141313").ss(6.5,1,1).p("AgYhuQhUCzChAq");
	this.shape_18.setTransform(5.5659,11.325);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#141313").ss(6.5,1,1).p("AgahvQhRC1ChAp");
	this.shape_19.setTransform(5.4889,11.25);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#141313").ss(6.5,1,1).p("AgahuQhRCzChAq");
	this.shape_20.setTransform(5.5111,11.275);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#141313").ss(6.5,1,1).p("AgWhtQhWCxChAq");
	this.shape_21.setTransform(5.6089,11.375);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#141313").ss(6.5,1,1).p("AgVhtQhYCxChAq");
	this.shape_22.setTransform(5.6512,11.425);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#141313").ss(6.5,1,1).p("AgQhsQheCvChAq");
	this.shape_23.setTransform(5.7642,11.55);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#141313").ss(6.5,1,1).p("AgNhqQhiCrChAq");
	this.shape_24.setTransform(5.8531,11.675);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9,p:{x:5.8434}}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_12}]},36).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_9,p:{x:5.8336}}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15,p:{x:5.7032}}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},10).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_15,p:{x:5.6929}}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9,p:{x:5.8336}}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_12}]},28).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_9,p:{x:5.8336}}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15,p:{x:5.7032}}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape}]},1).wait(1));
	this.timeline.addTween(cjs.Tween.get(this.shape_10).wait(10).to({_off:false},0).wait(1).to({x:5.8724,y:11.7},0).to({_off:true},1).wait(39).to({_off:false,x:5.8628,y:11.675},0).to({_off:true},1).wait(33).to({_off:false,y:11.7},0).to({_off:true},1).wait(32).to({_off:false,y:11.675},0).to({_off:true},1).wait(12));
	this.timeline.addTween(cjs.Tween.get(this.shape_11).wait(12).to({_off:false},0).wait(1).to({x:5.8916},0).to({_off:true},1).wait(72).to({_off:false,x:5.882,y:11.7},0).wait(1).to({y:11.725},0).wait(1).to({x:5.8916},0).to({_off:true},1).wait(42));

	// Symbol 6
	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f().s("#141313").ss(6.5,1,1).p("AAbBJQhDgYALgxQAJgoAzgg");
	this.shape_25.setTransform(9.0173,15.4);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f().s("#141313").ss(6.5,1,1).p("AAaBJQhDgZALgwQAJgoA1gg");
	this.shape_26.setTransform(9.1173,15.45);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f().s("#141313").ss(6.5,1,1).p("AAaBIQhEgYAMgwQAJgpA2ge");
	this.shape_27.setTransform(9.1923,15.475);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f().s("#141313").ss(6.5,1,1).p("AAZBIQhEgZAMgvQAJgpA4ge");
	this.shape_28.setTransform(9.2673,15.525);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f().s("#141313").ss(6.5,1,1).p("AAYBHQhDgYALgwQAJgpA6gd");
	this.shape_29.setTransform(9.3423,15.55);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f().s("#141313").ss(6.5,1,1).p("AAYBHQhEgYAMgwQAJgpA6gc");
	this.shape_30.setTransform(9.3923,15.575);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f().s("#141313").ss(6.5,1,1).p("AAXBHQhEgYAMgwQAJgpA8gc");
	this.shape_31.setTransform(9.4673,15.6);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f().s("#141313").ss(6.5,1,1).p("AAWBHQhDgZALgvQAJgpA9gc");
	this.shape_32.setTransform(9.5173,15.625);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f().s("#141313").ss(6.5,1,1).p("AAWBHQhDgZALgvQAJgpA+gb");
	this.shape_33.setTransform(9.5423,15.65);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f().s("#141313").ss(6.5,1,1).p("AAWBHQhEgZAMgvQAJgpA+gb");
	this.shape_34.setTransform(9.5923,15.65);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f().s("#141313").ss(6.5,1,1).p("AAVBGQhDgYALgwQAJgpA/ga");
	this.shape_35.setTransform(9.6173,15.675);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f().s("#141313").ss(6.5,1,1).p("AAVBGQhDgYALgwQAJgpBAga");
	this.shape_36.setTransform(9.6423,15.675);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f().s("#141313").ss(6.5,1,1).p("AAVBGQhEgYAMgwQAJgpBAga");
	this.shape_37.setTransform(9.6673,15.7);
	this.shape_37._off = true;

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f().s("#141313").ss(6.5,1,1).p("AAXBHQhDgYALgwQAJgpA7gc");
	this.shape_38.setTransform(9.4173,15.575);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f().s("#141313").ss(6.5,1,1).p("AAYBHQhEgYAMgwQAJgpA6gd");
	this.shape_39.setTransform(9.3673,15.55);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f().s("#141313").ss(6.5,1,1).p("AAYBHQhDgYALgwQAJgpA5gd");
	this.shape_40.setTransform(9.3173,15.55);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f().s("#141313").ss(6.5,1,1).p("AAZBIQhDgYALgwQAJgpA3ge");
	this.shape_41.setTransform(9.2173,15.5);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f().s("#141313").ss(6.5,1,1).p("AAbBJQhEgZAMgwQAJgoA0gg");
	this.shape_42.setTransform(9.0673,15.425);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f().s("#141313").ss(6.5,1,1).p("AAZBIQhDgYALgwQAJgpA4ge");
	this.shape_43.setTransform(9.2423,15.5);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f().s("#141313").ss(6.5,1,1).p("AAWBGQhEgYAMgwQAJgpA+ga");
	this.shape_44.setTransform(9.5923,15.675);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_25}]}).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_27,p:{x:9.1923}}]},1).to({state:[{t:this.shape_28,p:{x:9.2673}}]},1).to({state:[{t:this.shape_29}]},1).to({state:[{t:this.shape_30}]},1).to({state:[{t:this.shape_31}]},1).to({state:[{t:this.shape_32}]},1).to({state:[{t:this.shape_33}]},1).to({state:[{t:this.shape_34,p:{x:9.5923}}]},1).to({state:[{t:this.shape_35}]},1).to({state:[{t:this.shape_36,p:{y:15.675}}]},1).to({state:[{t:this.shape_36,p:{y:15.7}}]},1).to({state:[{t:this.shape_37}]},1).to({state:[{t:this.shape_37}]},1).to({state:[{t:this.shape_37}]},36).to({state:[{t:this.shape_35}]},1).to({state:[{t:this.shape_34,p:{x:9.5673}}]},1).to({state:[{t:this.shape_32}]},1).to({state:[{t:this.shape_31}]},1).to({state:[{t:this.shape_38}]},1).to({state:[{t:this.shape_39}]},1).to({state:[{t:this.shape_40}]},1).to({state:[{t:this.shape_28,p:{x:9.2673}}]},1).to({state:[{t:this.shape_41}]},1).to({state:[{t:this.shape_27,p:{x:9.1673}}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_42,p:{x:9.0673}}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_25}]},10).to({state:[{t:this.shape_42,p:{x:9.0923}}]},1).to({state:[{t:this.shape_27,p:{x:9.1673}}]},1).to({state:[{t:this.shape_43}]},1).to({state:[{t:this.shape_28,p:{x:9.2923}}]},1).to({state:[{t:this.shape_39}]},1).to({state:[{t:this.shape_38}]},1).to({state:[{t:this.shape_31}]},1).to({state:[{t:this.shape_32}]},1).to({state:[{t:this.shape_33}]},1).to({state:[{t:this.shape_34,p:{x:9.5673}}]},1).to({state:[{t:this.shape_44}]},1).to({state:[{t:this.shape_35}]},1).to({state:[{t:this.shape_36,p:{y:15.7}}]},1).to({state:[{t:this.shape_37}]},1).to({state:[{t:this.shape_37}]},1).to({state:[{t:this.shape_37}]},1).to({state:[{t:this.shape_37}]},28).to({state:[{t:this.shape_35}]},1).to({state:[{t:this.shape_34,p:{x:9.5673}}]},1).to({state:[{t:this.shape_32}]},1).to({state:[{t:this.shape_31}]},1).to({state:[{t:this.shape_38}]},1).to({state:[{t:this.shape_39}]},1).to({state:[{t:this.shape_40}]},1).to({state:[{t:this.shape_28,p:{x:9.2673}}]},1).to({state:[{t:this.shape_41}]},1).to({state:[{t:this.shape_27,p:{x:9.1673}}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_42,p:{x:9.0673}}]},1).to({state:[{t:this.shape_25}]},1).wait(1));
	this.timeline.addTween(cjs.Tween.get(this.shape_37).wait(13).to({_off:false},0).wait(37).to({_off:true},1).wait(36).to({_off:false},0).wait(30).to({_off:true},1).wait(13));

	// Symbol 5
	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f().s("#141313").ss(6.5,1,1).p("AgnAxQBOgDABhe");
	this.shape_45.setTransform(15.775,17.825);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.f().s("#141313").ss(6.5,1,1).p("AgmAzQBOgDgChi");
	this.shape_46.setTransform(15.6025,17.6);

	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.f().s("#141313").ss(6.5,1,1).p("AgkA1QBOgCgFhn");
	this.shape_47.setTransform(15.4649,17.4);

	this.shape_48 = new cjs.Shape();
	this.shape_48.graphics.f().s("#141313").ss(6.5,1,1).p("AgjA3QBOgDgIhq");
	this.shape_48.setTransform(15.3368,17.225);

	this.shape_49 = new cjs.Shape();
	this.shape_49.graphics.f().s("#141313").ss(6.5,1,1).p("AgiA4QBOgCgKhu");
	this.shape_49.setTransform(15.2366,17.05);

	this.shape_50 = new cjs.Shape();
	this.shape_50.graphics.f().s("#141313").ss(6.5,1,1).p("AghA6QBOgCgNhx");
	this.shape_50.setTransform(15.1418,16.875);

	this.shape_51 = new cjs.Shape();
	this.shape_51.graphics.f().s("#141313").ss(6.5,1,1).p("AggA7QBOgCgPhz");
	this.shape_51.setTransform(15.0697,16.75);

	this.shape_52 = new cjs.Shape();
	this.shape_52.graphics.f().s("#141313").ss(6.5,1,1).p("AggA9QBOgDgRh2");
	this.shape_52.setTransform(15.0005,16.625);

	this.shape_53 = new cjs.Shape();
	this.shape_53.graphics.f().s("#141313").ss(6.5,1,1).p("AgfA+QBOgDgTh4");
	this.shape_53.setTransform(14.9505,16.525);

	this.shape_54 = new cjs.Shape();
	this.shape_54.graphics.f().s("#141313").ss(6.5,1,1).p("AgfA/QBOgDgUh6");
	this.shape_54.setTransform(14.902,16.425);

	this.shape_55 = new cjs.Shape();
	this.shape_55.graphics.f().s("#141313").ss(6.5,1,1).p("AgeA/QBOgCgVh8");
	this.shape_55.setTransform(14.8705,16.35);

	this.shape_56 = new cjs.Shape();
	this.shape_56.graphics.f().s("#141313").ss(6.5,1,1).p("AgeBAQBOgDgWh8");
	this.shape_56.setTransform(14.8396,16.3);

	this.shape_57 = new cjs.Shape();
	this.shape_57.graphics.f().s("#141313").ss(6.5,1,1).p("AgeBBQBOgDgWh+");
	this.shape_57.setTransform(14.8244,16.25);

	this.shape_58 = new cjs.Shape();
	this.shape_58.graphics.f().s("#141313").ss(6.5,1,1).p("AgeBBQBOgDgXh+");
	this.shape_58.setTransform(14.8093,16.225);
	this.shape_58._off = true;

	this.shape_59 = new cjs.Shape();
	this.shape_59.graphics.f().s("#141313").ss(6.5,1,1).p("AgfA+QBOgCgTh5");
	this.shape_59.setTransform(14.9342,16.475);

	this.shape_60 = new cjs.Shape();
	this.shape_60.graphics.f().s("#141313").ss(6.5,1,1).p("AgfA9QBOgDgSh2");
	this.shape_60.setTransform(14.9837,16.6);

	this.shape_61 = new cjs.Shape();
	this.shape_61.graphics.f().s("#141313").ss(6.5,1,1).p("AggA8QBOgDgQh0");
	this.shape_61.setTransform(15.0521,16.725);

	this.shape_62 = new cjs.Shape();
	this.shape_62.graphics.f().s("#141313").ss(6.5,1,1).p("AghA6QBOgCgNhy");
	this.shape_62.setTransform(15.1235,16.85);

	this.shape_63 = new cjs.Shape();
	this.shape_63.graphics.f().s("#141313").ss(6.5,1,1).p("AghA5QBOgCgMhv");
	this.shape_63.setTransform(15.1981,16.975);

	this.shape_64 = new cjs.Shape();
	this.shape_64.graphics.f().s("#141313").ss(6.5,1,1).p("AgiA4QBOgCgKht");
	this.shape_64.setTransform(15.2562,17.075);

	this.shape_65 = new cjs.Shape();
	this.shape_65.graphics.f().s("#141313").ss(6.5,1,1).p("AgjA3QBOgCgIhr");
	this.shape_65.setTransform(15.3368,17.2);

	this.shape_66 = new cjs.Shape();
	this.shape_66.graphics.f().s("#141313").ss(6.5,1,1).p("AgkA2QBOgDgGho");
	this.shape_66.setTransform(15.4212,17.325);

	this.shape_67 = new cjs.Shape();
	this.shape_67.graphics.f().s("#141313").ss(6.5,1,1).p("AglA1QBOgDgEhm");
	this.shape_67.setTransform(15.5096,17.45);

	this.shape_68 = new cjs.Shape();
	this.shape_68.graphics.f().s("#141313").ss(6.5,1,1).p("AglAzQBOgCgDhj");
	this.shape_68.setTransform(15.5788,17.575);

	this.shape_69 = new cjs.Shape();
	this.shape_69.graphics.f().s("#141313").ss(6.5,1,1).p("AgmAyQBOgCgBhh");
	this.shape_69.setTransform(15.6752,17.7);

	this.shape_70 = new cjs.Shape();
	this.shape_70.graphics.f().s("#141313").ss(6.5,1,1).p("AgmAzQBOgDgBhi");
	this.shape_70.setTransform(15.6264,17.625);

	this.shape_71 = new cjs.Shape();
	this.shape_71.graphics.f().s("#141313").ss(6.5,1,1).p("AgjA2QBOgCgHhp");
	this.shape_71.setTransform(15.3785,17.275);

	this.shape_72 = new cjs.Shape();
	this.shape_72.graphics.f().s("#141313").ss(6.5,1,1).p("AgiA4QBOgDgJhs");
	this.shape_72.setTransform(15.296,17.125);

	this.shape_73 = new cjs.Shape();
	this.shape_73.graphics.f().s("#141313").ss(6.5,1,1).p("AggA8QBOgDgPh0");
	this.shape_73.setTransform(15.0697,16.725);

	this.shape_74 = new cjs.Shape();
	this.shape_74.graphics.f().s("#141313").ss(6.5,1,1).p("AgfA/QBOgDgTh6");
	this.shape_74.setTransform(14.918,16.45);

	this.shape_75 = new cjs.Shape();
	this.shape_75.graphics.f().s("#141313").ss(6.5,1,1).p("AgeA/QBOgCgVh7");
	this.shape_75.setTransform(14.8862,16.375);

	this.shape_76 = new cjs.Shape();
	this.shape_76.graphics.f().s("#141313").ss(6.5,1,1).p("AgeBAQBOgCgWh9");
	this.shape_76.setTransform(14.8396,16.275);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_45}]}).to({state:[{t:this.shape_46}]},1).to({state:[{t:this.shape_47}]},1).to({state:[{t:this.shape_48}]},1).to({state:[{t:this.shape_49}]},1).to({state:[{t:this.shape_50}]},1).to({state:[{t:this.shape_51}]},1).to({state:[{t:this.shape_52}]},1).to({state:[{t:this.shape_53}]},1).to({state:[{t:this.shape_54}]},1).to({state:[{t:this.shape_55}]},1).to({state:[{t:this.shape_56,p:{x:14.8396,y:16.3}}]},1).to({state:[{t:this.shape_57}]},1).to({state:[{t:this.shape_58}]},1).to({state:[{t:this.shape_58}]},1).to({state:[{t:this.shape_58}]},36).to({state:[{t:this.shape_55}]},1).to({state:[{t:this.shape_59}]},1).to({state:[{t:this.shape_60}]},1).to({state:[{t:this.shape_61}]},1).to({state:[{t:this.shape_62}]},1).to({state:[{t:this.shape_63}]},1).to({state:[{t:this.shape_64}]},1).to({state:[{t:this.shape_65}]},1).to({state:[{t:this.shape_66}]},1).to({state:[{t:this.shape_67}]},1).to({state:[{t:this.shape_68}]},1).to({state:[{t:this.shape_69}]},1).to({state:[{t:this.shape_45}]},1).to({state:[{t:this.shape_45}]},10).to({state:[{t:this.shape_70}]},1).to({state:[{t:this.shape_67}]},1).to({state:[{t:this.shape_71}]},1).to({state:[{t:this.shape_72}]},1).to({state:[{t:this.shape_63}]},1).to({state:[{t:this.shape_62}]},1).to({state:[{t:this.shape_73}]},1).to({state:[{t:this.shape_52}]},1).to({state:[{t:this.shape_53}]},1).to({state:[{t:this.shape_74}]},1).to({state:[{t:this.shape_75}]},1).to({state:[{t:this.shape_56,p:{x:14.855,y:16.325}}]},1).to({state:[{t:this.shape_76}]},1).to({state:[{t:this.shape_57}]},1).to({state:[{t:this.shape_58}]},1).to({state:[{t:this.shape_58}]},1).to({state:[{t:this.shape_58}]},28).to({state:[{t:this.shape_55}]},1).to({state:[{t:this.shape_59}]},1).to({state:[{t:this.shape_60}]},1).to({state:[{t:this.shape_61}]},1).to({state:[{t:this.shape_62}]},1).to({state:[{t:this.shape_63}]},1).to({state:[{t:this.shape_64}]},1).to({state:[{t:this.shape_65}]},1).to({state:[{t:this.shape_66}]},1).to({state:[{t:this.shape_67}]},1).to({state:[{t:this.shape_68}]},1).to({state:[{t:this.shape_69}]},1).to({state:[{t:this.shape_45}]},1).wait(1));
	this.timeline.addTween(cjs.Tween.get(this.shape_58).wait(13).to({_off:false},0).wait(37).to({_off:true},1).wait(37).to({_off:false},0).wait(29).to({_off:true},1).wait(13));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-3.2,-3.2,26.3,29.2);


(lib.Symbol9 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Symbol 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#141313").ss(6.5,1,1).p("Ag9BlQCggQgxi5");
	this.shape.setTransform(13.4627,10.05);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#141313").ss(6.5,1,1).p("Ag8BkQCggQgzi3");
	this.shape_1.setTransform(13.3899,10.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#141313").ss(6.5,1,1).p("Ag7BkQCfgQg1i3");
	this.shape_2.setTransform(13.3328,10.125);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#141313").ss(6.5,1,1).p("Ag7BjQCggQg3i1");
	this.shape_3.setTransform(13.2769,10.175);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#141313").ss(6.5,1,1).p("Ag6BjQCfgQg4i1");
	this.shape_4.setTransform(13.2355,10.2);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#141313").ss(6.5,1,1).p("Ag6BjQCggQg6i1");
	this.shape_5.setTransform(13.1948,10.25);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#141313").ss(6.5,1,1).p("Ag6BiQCggQg8iz");
	this.shape_6.setTransform(13.1546,10.275);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#141313").ss(6.5,1,1).p("Ag5BiQCfgQg9iz");
	this.shape_7.setTransform(13.115,10.3);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#141313").ss(6.5,1,1).p("Ag5BiQCggQg+iz");
	this.shape_8.setTransform(13.0888,10.325);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#141313").ss(6.5,1,1).p("Ag5BiQCggQg/iz");
	this.shape_9.setTransform(13.0629,10.325);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#141313").ss(6.5,1,1).p("Ag5BiQCggRhAiy");
	this.shape_10.setTransform(13.0501,10.35);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#141313").ss(6.5,1,1).p("Ag4BiQCfgRhAiy");
	this.shape_11.setTransform(13.0372,10.35);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#141313").ss(6.5,1,1).p("Ag4BhQCfgQhAix");
	this.shape_12.setTransform(13.0245,10.375);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#141313").ss(6.5,1,1).p("Ag4BhQCfgQhBix");
	this.shape_13.setTransform(13.0118,10.375);
	this.shape_13._off = true;

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#141313").ss(6.5,1,1).p("Ag5BiQCfgQg8iz");
	this.shape_14.setTransform(13.1281,10.275);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#141313").ss(6.5,1,1).p("Ag6BjQCggQg7i1");
	this.shape_15.setTransform(13.1813,10.25);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#141313").ss(6.5,1,1).p("Ag8BkQCggQg0i3");
	this.shape_16.setTransform(13.3612,10.125);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#141313").ss(6.5,1,1).p("Ag8BkQCfgQgyi3");
	this.shape_17.setTransform(13.4188,10.075);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#141313").ss(6.5,1,1).p("Ag8BkQCfgQgxi3");
	this.shape_18.setTransform(13.4334,10.075);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#141313").ss(6.5,1,1).p("Ag8BlQCfgQgxi5");
	this.shape_19.setTransform(13.448,10.05);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#141313").ss(6.5,1,1).p("Ag7BkQCfgQg0i3");
	this.shape_20.setTransform(13.347,10.125);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#141313").ss(6.5,1,1).p("Ag7BkQCfgQg2i3");
	this.shape_21.setTransform(13.3047,10.15);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#141313").ss(6.5,1,1).p("Ag7BjQCggQg4i1");
	this.shape_22.setTransform(13.263,10.2);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#141313").ss(6.5,1,1).p("Ag6BjQCfgQg5i1");
	this.shape_23.setTransform(13.2219,10.225);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2,p:{x:13.3328,y:10.125}}]},1).to({state:[{t:this.shape_3,p:{x:13.2769}}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9,p:{x:13.0629}}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_13}]},36).to({state:[{t:this.shape_9,p:{x:13.0758}}]},1).to({state:[{t:this.shape_14,p:{x:13.1281}}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_3,p:{x:13.2908}}]},1).to({state:[{t:this.shape_2,p:{x:13.3188,y:10.15}}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_17,p:{x:13.4188,y:10.075}}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},10).to({state:[{t:this.shape_17,p:{x:13.4043,y:10.1}}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_14,p:{x:13.1413}}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9,p:{x:13.0758}}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_13}]},28).to({state:[{t:this.shape_9,p:{x:13.0758}}]},1).to({state:[{t:this.shape_14,p:{x:13.1281}}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_3,p:{x:13.2908}}]},1).to({state:[{t:this.shape_2,p:{x:13.3188,y:10.15}}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_17,p:{x:13.4188,y:10.075}}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).wait(1));
	this.timeline.addTween(cjs.Tween.get(this.shape).to({_off:true},1).wait(61).to({_off:false},0).wait(11).to({_off:true},1).wait(55).to({_off:false},0).wait(2));
	this.timeline.addTween(cjs.Tween.get(this.shape_13).wait(13).to({_off:false},0).wait(37).to({_off:true},1).wait(37).to({_off:false},0).wait(29).to({_off:true},1).wait(13));

	// Symbol 2
	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#141313").ss(6.5,1,1).p("AggBGQBEgOgEgwQgCgngsgm");
	this.shape_24.setTransform(9.7085,13.3);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f().s("#141313").ss(6.5,1,1).p("AggBGQBEgOgEgwQgCgngtgm");
	this.shape_25.setTransform(9.7085,13.3);
	this.shape_25._off = true;

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f().s("#141313").ss(6.5,1,1).p("AggBGQBEgNgEgwQgCgogtgm");
	this.shape_26.setTransform(9.7085,13.275);
	this.shape_26._off = true;

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f().s("#141313").ss(6.5,1,1).p("AggBGQBEgNgEgwQgCgogugm");
	this.shape_27.setTransform(9.7085,13.275);
	this.shape_27._off = true;

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f().s("#141313").ss(6.5,1,1).p("AggBHQBEgOgEgwQgCgogugn");
	this.shape_28.setTransform(9.7085,13.25);
	this.shape_28._off = true;

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f().s("#141313").ss(6.5,1,1).p("AggBHQBEgOgEgwQgCgogvgn");
	this.shape_29.setTransform(9.7085,13.25);
	this.shape_29._off = true;

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f().s("#141313").ss(6.5,1,1).p("AggBHQBEgOgEgwQgCgngvgo");
	this.shape_30.setTransform(9.7085,13.225);
	this.shape_30._off = true;

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f().s("#141313").ss(6.5,1,1).p("AggBHQBEgOgEgwQgCgngwgo");
	this.shape_31.setTransform(9.7085,13.225);
	this.shape_31._off = true;

	this.timeline.addTween(cjs.Tween.get(this.shape_24).to({_off:true},1).wait(59).to({_off:false},0).wait(13).to({_off:true},1).wait(53).to({_off:false},0).wait(4));
	this.timeline.addTween(cjs.Tween.get(this.shape_25).wait(1).to({_off:false},0).to({_off:true},1).wait(56).to({_off:false},0).wait(1).to({_off:true},1).wait(14).to({_off:false},0).to({_off:true},1).wait(50).to({_off:false},0).wait(1).to({_off:true},1).wait(4));
	this.timeline.addTween(cjs.Tween.get(this.shape_26).wait(2).to({_off:false},0).to({_off:true},1).wait(53).to({_off:false},0).wait(1).to({_off:true},1).wait(17).to({_off:false},0).wait(1).to({_off:true},1).wait(46).to({_off:false},0).wait(1).to({_off:true},1).wait(6));
	this.timeline.addTween(cjs.Tween.get(this.shape_27).wait(3).to({_off:false},0).wait(1).to({_off:true},1).wait(49).to({_off:false},0).wait(1).to({_off:true},1).wait(21).to({_off:false},0).to({_off:true},1).wait(43).to({_off:false},0).wait(1).to({_off:true},1).wait(8));
	this.timeline.addTween(cjs.Tween.get(this.shape_28).wait(5).to({_off:false},0).to({_off:true},1).wait(47).to({_off:false},0).to({_off:true},1).wait(24).to({_off:false},0).wait(1).to({_off:true},1).wait(40).to({_off:false},0).to({_off:true},1).wait(10));
	this.timeline.addTween(cjs.Tween.get(this.shape_29).wait(6).to({_off:false},0).wait(2).to({_off:true},1).wait(43).to({_off:false},0).to({_off:true},1).wait(27).to({_off:false},0).wait(2).to({_off:true},1).wait(36).to({_off:false},0).to({_off:true},1).wait(11));
	this.timeline.addTween(cjs.Tween.get(this.shape_30).wait(9).to({_off:false},0).wait(1).to({_off:true},1).wait(40).to({_off:false},0).to({_off:true},1).wait(31).to({_off:false},0).wait(1).to({_off:true},1).wait(33).to({_off:false},0).to({_off:true},1).wait(12));
	this.timeline.addTween(cjs.Tween.get(this.shape_31).wait(11).to({_off:false},0).wait(39).to({_off:true},1).wait(34).to({_off:false},0).wait(32).to({_off:true},1).wait(13));

	// Symbol 4
	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f().s("#141313").ss(6.5,1,1).p("AAgA1QhKgPANha");
	this.shape_32.setTransform(3.2357,15.05);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f().s("#141313").ss(6.5,1,1).p("Agag1QgRBdBKAO");
	this.shape_33.setTransform(3.3929,14.925);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f().s("#141313").ss(6.5,1,1).p("AgYg2QgVBfBKAO");
	this.shape_34.setTransform(3.5203,14.825);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f().s("#141313").ss(6.5,1,1).p("AgVg3QgZBhBKAO");
	this.shape_35.setTransform(3.6375,14.725);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f().s("#141313").ss(6.5,1,1).p("AgSg4QgdBiBKAP");
	this.shape_36.setTransform(3.7326,14.65);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f().s("#141313").ss(6.5,1,1).p("AgQg4QggBjBKAO");
	this.shape_37.setTransform(3.8092,14.575);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f().s("#141313").ss(6.5,1,1).p("AgOg5QgiBlBKAO");
	this.shape_38.setTransform(3.8815,14.5);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f().s("#141313").ss(6.5,1,1).p("AgMg6QglBnBKAO");
	this.shape_39.setTransform(3.9388,14.425);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f().s("#141313").ss(6.5,1,1).p("AgKg6QgnBnBKAO");
	this.shape_40.setTransform(3.9829,14.375);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f().s("#141313").ss(6.5,1,1).p("AgJg7QgpBpBKAO");
	this.shape_41.setTransform(4.0254,14.325);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f().s("#141313").ss(6.5,1,1).p("AgIg7QgqBpBKAO");
	this.shape_42.setTransform(4.0564,14.3);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f().s("#141313").ss(6.5,1,1).p("AgHg7QgrBpBKAO");
	this.shape_43.setTransform(4.0766,14.275);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f().s("#141313").ss(6.5,1,1).p("AgGg8QgsBqBKAP");
	this.shape_44.setTransform(4.0964,14.25);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f().s("#141313").ss(6.5,1,1).p("AgGg8QgtBrBKAO");
	this.shape_45.setTransform(4.1063,14.225);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.f().s("#141313").ss(6.5,1,1).p("AAXA9QhKgOAthr");
	this.shape_46.setTransform(4.1063,14.225);

	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.f().s("#141313").ss(6.5,1,1).p("AgKg7QgoBoBKAO");
	this.shape_47.setTransform(4.0043,14.35);

	this.shape_48 = new cjs.Shape();
	this.shape_48.graphics.f().s("#141313").ss(6.5,1,1).p("AgNg6QgkBmBKAP");
	this.shape_48.setTransform(3.9162,14.45);

	this.shape_49 = new cjs.Shape();
	this.shape_49.graphics.f().s("#141313").ss(6.5,1,1).p("AgQg5QggBkBKAP");
	this.shape_49.setTransform(3.8092,14.55);

	this.shape_50 = new cjs.Shape();
	this.shape_50.graphics.f().s("#141313").ss(6.5,1,1).p("AgTg4QgcBiBKAP");
	this.shape_50.setTransform(3.7194,14.65);

	this.shape_51 = new cjs.Shape();
	this.shape_51.graphics.f().s("#141313").ss(6.5,1,1).p("AgVg3QgZBgBKAP");
	this.shape_51.setTransform(3.6375,14.75);

	this.shape_52 = new cjs.Shape();
	this.shape_52.graphics.f().s("#141313").ss(6.5,1,1).p("AgXg2QgWBfBKAO");
	this.shape_52.setTransform(3.5505,14.8);

	this.shape_53 = new cjs.Shape();
	this.shape_53.graphics.f().s("#141313").ss(6.5,1,1).p("AgZg1QgTBdBKAO");
	this.shape_53.setTransform(3.4738,14.875);

	this.shape_54 = new cjs.Shape();
	this.shape_54.graphics.f().s("#141313").ss(6.5,1,1).p("Agag1QgSBdBKAO");
	this.shape_54.setTransform(3.4095,14.925);

	this.shape_55 = new cjs.Shape();
	this.shape_55.graphics.f().s("#141313").ss(6.5,1,1).p("Agbg0QgQBbBKAO");
	this.shape_55.setTransform(3.3423,14.975);

	this.shape_56 = new cjs.Shape();
	this.shape_56.graphics.f().s("#141313").ss(6.5,1,1).p("Agcg0QgOBbBKAO");
	this.shape_56.setTransform(3.2899,15);

	this.shape_57 = new cjs.Shape();
	this.shape_57.graphics.f().s("#141313").ss(6.5,1,1).p("Agdg0QgNBbBKAO");
	this.shape_57.setTransform(3.272,15.025);

	this.shape_58 = new cjs.Shape();
	this.shape_58.graphics.f().s("#141313").ss(6.5,1,1).p("Agdg0QgNBaBKAP");
	this.shape_58.setTransform(3.2357,15.05);

	this.shape_59 = new cjs.Shape();
	this.shape_59.graphics.f().s("#141313").ss(6.5,1,1).p("Agbg1QgQBcBKAP");
	this.shape_59.setTransform(3.3762,14.95);

	this.shape_60 = new cjs.Shape();
	this.shape_60.graphics.f().s("#141313").ss(6.5,1,1).p("AgYg2QgUBeBKAO");
	this.shape_60.setTransform(3.4895,14.85);

	this.shape_61 = new cjs.Shape();
	this.shape_61.graphics.f().s("#141313").ss(6.5,1,1).p("AgWg2QgXBfBKAO");
	this.shape_61.setTransform(3.5947,14.775);

	this.shape_62 = new cjs.Shape();
	this.shape_62.graphics.f().s("#141313").ss(6.5,1,1).p("AgUg3QgaBhBKAO");
	this.shape_62.setTransform(3.6791,14.7);

	this.shape_63 = new cjs.Shape();
	this.shape_63.graphics.f().s("#141313").ss(6.5,1,1).p("AgSg4QgdBjBKAO");
	this.shape_63.setTransform(3.7586,14.625);

	this.shape_64 = new cjs.Shape();
	this.shape_64.graphics.f().s("#141313").ss(6.5,1,1).p("AgPg5QghBkBKAP");
	this.shape_64.setTransform(3.8337,14.55);

	this.shape_65 = new cjs.Shape();
	this.shape_65.graphics.f().s("#141313").ss(6.5,1,1).p("AgJg7QgpBoBKAO");
	this.shape_65.setTransform(4.0149,14.35);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_32}]}).to({state:[{t:this.shape_33}]},1).to({state:[{t:this.shape_34}]},1).to({state:[{t:this.shape_35}]},1).to({state:[{t:this.shape_36}]},1).to({state:[{t:this.shape_37}]},1).to({state:[{t:this.shape_38,p:{y:14.5}}]},1).to({state:[{t:this.shape_39}]},1).to({state:[{t:this.shape_40}]},1).to({state:[{t:this.shape_41}]},1).to({state:[{t:this.shape_42,p:{x:4.0564}}]},1).to({state:[{t:this.shape_43,p:{x:4.0766}}]},1).to({state:[{t:this.shape_44,p:{x:4.0964}}]},1).to({state:[{t:this.shape_45}]},1).to({state:[{t:this.shape_46}]},1).to({state:[{t:this.shape_46}]},36).to({state:[{t:this.shape_47}]},1).to({state:[{t:this.shape_48}]},1).to({state:[{t:this.shape_49}]},1).to({state:[{t:this.shape_50}]},1).to({state:[{t:this.shape_51}]},1).to({state:[{t:this.shape_52}]},1).to({state:[{t:this.shape_53}]},1).to({state:[{t:this.shape_54}]},1).to({state:[{t:this.shape_55}]},1).to({state:[{t:this.shape_56}]},1).to({state:[{t:this.shape_57}]},1).to({state:[{t:this.shape_58}]},1).to({state:[{t:this.shape_32}]},1).to({state:[{t:this.shape_32}]},10).to({state:[{t:this.shape_59}]},1).to({state:[{t:this.shape_60}]},1).to({state:[{t:this.shape_61}]},1).to({state:[{t:this.shape_62}]},1).to({state:[{t:this.shape_63}]},1).to({state:[{t:this.shape_64}]},1).to({state:[{t:this.shape_38,p:{y:14.475}}]},1).to({state:[{t:this.shape_39}]},1).to({state:[{t:this.shape_40}]},1).to({state:[{t:this.shape_65}]},1).to({state:[{t:this.shape_42,p:{x:4.0462}}]},1).to({state:[{t:this.shape_43,p:{x:4.0665}}]},1).to({state:[{t:this.shape_44,p:{x:4.0866}}]},1).to({state:[{t:this.shape_44,p:{x:4.0964}}]},1).to({state:[{t:this.shape_45}]},1).to({state:[{t:this.shape_46}]},1).to({state:[{t:this.shape_46}]},28).to({state:[{t:this.shape_47}]},1).to({state:[{t:this.shape_48}]},1).to({state:[{t:this.shape_49}]},1).to({state:[{t:this.shape_50}]},1).to({state:[{t:this.shape_51}]},1).to({state:[{t:this.shape_52}]},1).to({state:[{t:this.shape_53}]},1).to({state:[{t:this.shape_54}]},1).to({state:[{t:this.shape_55}]},1).to({state:[{t:this.shape_56}]},1).to({state:[{t:this.shape_57}]},1).to({state:[{t:this.shape_58}]},1).to({state:[{t:this.shape_32}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-3.2,-3.2,26.099999999999998,26.8);


(lib.Symbol3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.shadow();
	this.instance.parent = this;
	this.instance.setTransform(-10,-21);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol3, new cjs.Rectangle(-10,-21,710,229), null);


(lib.Symbol2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#F51431").s().p("AAAAqQgRAAgNgMQgMgMAAgSQAAgQAMgNQANgNARAAIADAAQAPABALAMQANANAAAQQAAASgNAMQgLAMgPAAIgDAAg");
	this.shape.setTransform(4.25,4.25);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol2, new cjs.Rectangle(0,0,8.5,8.5), null);


(lib.Symbol2copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Symbol 7
	this.instance = new lib.Symbol10();
	this.instance.parent = this;
	this.instance.setTransform(16.6,85,1,1,0,0,0,16.6,23.4);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(9).to({rotation:31.7028,x:25.7,y:78.1},13,cjs.Ease.get(1)).wait(28).to({rotation:0,x:16.6,y:85},13,cjs.Ease.get(1)).wait(10).to({rotation:31.7028,x:25.7,y:78.1},16,cjs.Ease.get(1)).wait(28).to({rotation:0,x:16.6,y:85},13,cjs.Ease.get(1)).wait(1));

	// Layer 3
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#141313").ss(6.5,1,1).p("AEXBbQlkCWjJl2");
	this.shape.setTransform(44.475,98.3199);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#141313").ss(6.5,1,1).p("AkPiJQC7F/FkiW");
	this.shape_1.setTransform(45.15,97.7798);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#141313").ss(6.5,1,1).p("AkJiOQCvGJFkiW");
	this.shape_2.setTransform(45.775,97.268);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#141313").ss(6.5,1,1).p("AkDiSQCjGSFkiW");
	this.shape_3.setTransform(46.35,96.8112);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#141313").ss(6.5,1,1).p("Aj+iVQCZGZFkiW");
	this.shape_4.setTransform(46.875,96.4089);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#141313").ss(6.5,1,1).p("Aj5iYQCPGfFkiW");
	this.shape_5.setTransform(47.325,96.0609);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#141313").ss(6.5,1,1).p("Aj1ibQCHGmFkiW");
	this.shape_6.setTransform(47.75,95.7134);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#141313").ss(6.5,1,1).p("AjxidQCAGrFkiW");
	this.shape_7.setTransform(48.1,95.4464);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#141313").ss(6.5,1,1).p("AjvigQB6GwFkiW");
	this.shape_8.setTransform(48.4,95.2063);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#141313").ss(6.5,1,1).p("AjsihQB1GzFkiW");
	this.shape_9.setTransform(48.65,95.0198);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#141313").ss(6.5,1,1).p("AjqijQBxG2FkiW");
	this.shape_10.setTransform(48.825,94.86);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#141313").ss(6.5,1,1).p("AjpikQBvG4FkiW");
	this.shape_11.setTransform(48.975,94.7536);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#141313").ss(6.5,1,1).p("AjoikQBtG5FkiW");
	this.shape_12.setTransform(49.05,94.7003);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#141313").ss(6.5,1,1).p("ADpB/QlkCWhtm5");
	this.shape_13.setTransform(49.075,94.6737);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#141313").ss(6.5,1,1).p("Aj6iXQCSGdFkiW");
	this.shape_14.setTransform(47.2,96.1679);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#141313").ss(6.5,1,1).p("AkAiUQCdGWFkiW");
	this.shape_15.setTransform(46.675,96.5697);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#141313").ss(6.5,1,1).p("AkEiRQClGQFkiW");
	this.shape_16.setTransform(46.225,96.9186);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#141313").ss(6.5,1,1).p("AkMiLQC1GEFkiW");
	this.shape_17.setTransform(45.45,97.5372);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#141313").ss(6.5,1,1).p("AkSiIQDAF8FkiW");
	this.shape_18.setTransform(44.9,97.9686);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#141313").ss(6.5,1,1).p("AkTiGQDDF5FkiW");
	this.shape_19.setTransform(44.725,98.1307);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#141313").ss(6.5,1,1).p("AkViFQDHF3FkiW");
	this.shape_20.setTransform(44.575,98.2388);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#141313").ss(6.5,1,1).p("AkWiFQDIF2FkiW");
	this.shape_21.setTransform(44.5,98.2929);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#141313").ss(6.5,1,1).p("AkQiIQC9F9FkiW");
	this.shape_22.setTransform(45.025,97.8877);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#141313").ss(6.5,1,1).p("AkLiMQCzGFFkiW");
	this.shape_23.setTransform(45.55,97.4564);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#141313").ss(6.5,1,1).p("AkGiPQCpGMFkiW");
	this.shape_24.setTransform(46.05,97.0798);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f().s("#141313").ss(6.5,1,1).p("AkCiSQCgGTFkiW");
	this.shape_25.setTransform(46.5,96.7307);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f().s("#141313").ss(6.5,1,1).p("Aj+iVQCYGZFkiW");
	this.shape_26.setTransform(46.9,96.3821);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f().s("#141313").ss(6.5,1,1).p("Aj6iYQCRGfFkiW");
	this.shape_27.setTransform(47.275,96.0876);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f().s("#141313").ss(6.5,1,1).p("Aj2iaQCJGkFkiW");
	this.shape_28.setTransform(47.625,95.8202);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f().s("#141313").ss(6.5,1,1).p("AjzicQCDGoFkiW");
	this.shape_29.setTransform(47.925,95.5798);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f().s("#141313").ss(6.5,1,1).p("AjwieQB+GsFkiW");
	this.shape_30.setTransform(48.2,95.3663);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f().s("#141313").ss(6.5,1,1).p("AjuigQB5GwFkiW");
	this.shape_31.setTransform(48.425,95.1797);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f().s("#141313").ss(6.5,1,1).p("AjqiiQByG1FjiW");
	this.shape_32.setTransform(48.8,94.9133);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f().s("#141313").ss(6.5,1,1).p("AjpijQBvG3FkiW");
	this.shape_33.setTransform(48.925,94.8068);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f().s("#141313").ss(6.5,1,1).p("AjpikQBuG5FkiW");
	this.shape_34.setTransform(49,94.7269);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},9).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2,p:{x:45.775}}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6,p:{x:47.75}}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9,p:{x:48.65}}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_13}]},28).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_6,p:{x:47.775}}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_2,p:{x:45.8}}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},10).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_27}]},1).to({state:[{t:this.shape_28}]},1).to({state:[{t:this.shape_29}]},1).to({state:[{t:this.shape_30}]},1).to({state:[{t:this.shape_31}]},1).to({state:[{t:this.shape_9,p:{x:48.625}}]},1).to({state:[{t:this.shape_32}]},1).to({state:[{t:this.shape_33}]},1).to({state:[{t:this.shape_34}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_13}]},28).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_6,p:{x:47.775}}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_2,p:{x:45.8}}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape}]},1).wait(1));
	this.timeline.addTween(cjs.Tween.get(this.shape).wait(9).to({_off:true},1).wait(53).to({_off:false},0).wait(10).to({_off:true},1).wait(56).to({_off:false},0).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-3.2,45.1,78.8,69.9);


(lib.Symbol2_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Symbol 1
	this.instance = new lib.Symbol9();
	this.instance.parent = this;
	this.instance.setTransform(202.2,83.7,1,1,0,0,0,6.9,22.9);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(9).to({rotation:-22.7325,x:200,y:76.55},13,cjs.Ease.get(1)).wait(28).to({rotation:0,x:202.2,y:83.7},13,cjs.Ease.get(1)).wait(10).to({rotation:-22.7325,x:200,y:76.55},16,cjs.Ease.get(1)).wait(28).to({rotation:0,x:202.2,y:83.7},13,cjs.Ease.get(1)).wait(1));

	// Layer 4
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#141313").ss(6.5,1,1).p("AjnBfQD+BmByg3QB6g7gjjz");
	this.shape_1.setTransform(179.5889,98.4384);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#141313").ss(6.5,1,1).p("AjmBmQD9BgByg4QB6g6glj1");
	this.shape_2.setTransform(179.4805,97.695);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#141313").ss(6.5,1,1).p("AjlBtQD7BaB0g4QB5g7gmj3");
	this.shape_3.setTransform(179.3791,97.049);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#141313").ss(6.5,1,1).p("AjkBzQD6BUB0g4QB5g7goj4");
	this.shape_4.setTransform(179.2876,96.432);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#141313").ss(6.5,1,1).p("AjjB4QD5BQB0g5QB5g7gqj6");
	this.shape_5.setTransform(179.2066,95.9108);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#141313").ss(6.5,1,1).p("AjiB9QD4BLB1g5QB4g7grj7");
	this.shape_6.setTransform(179.1325,95.401);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#141313").ss(6.5,1,1).p("AjiCBQD4BIB1g6QB4g7gsj8");
	this.shape_7.setTransform(179.0804,95.0015);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#141313").ss(6.5,1,1).p("AjhCFQD3BEB1g5QB3g7gsj+");
	this.shape_8.setTransform(179.0189,94.637);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#141313").ss(6.5,1,1).p("AjhCIQD3BCB2g6QB2g7gtj/");
	this.shape_9.setTransform(178.9693,94.3315);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#141313").ss(6.5,1,1).p("AjgCKQD2BAB2g6QB3g7gukA");
	this.shape_10.setTransform(178.9432,94.0594);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#141313").ss(6.5,1,1).p("AjgCMQD2A+B2g6QB2g7gukA");
	this.shape_11.setTransform(178.9052,93.87);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#141313").ss(6.5,1,1).p("AjgCOQD2A8B2g6QB2g7gukA");
	this.shape_12.setTransform(178.8822,93.7379);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#141313").ss(6.5,1,1).p("AjgCOQD2A8B2g6QB2g7gvkB");
	this.shape_13.setTransform(178.8694,93.669);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#141313").ss(6.5,1,1).p("AjgCPQD2A7B2g6QB2g6gvkC");
	this.shape_14.setTransform(178.8636,93.6198);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#141313").ss(6.5,1,1).p("AjiCBQD4BIB1g6QB4g6gsj9");
	this.shape_15.setTransform(179.067,94.9565);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#141313").ss(6.5,1,1).p("AjjB7QD5BNB1g5QB4g7grj7");
	this.shape_16.setTransform(179.1558,95.5623);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#141313").ss(6.5,1,1).p("AjjB2QD5BSB1g5QB4g7gpj5");
	this.shape_17.setTransform(179.236,96.0816);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#141313").ss(6.5,1,1).p("AjkBxQD6BWB0g4QB5g7goj4");
	this.shape_18.setTransform(179.3111,96.5951);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#141313").ss(6.5,1,1).p("AjlBtQD7BZB0g4QB5g6gnj3");
	this.shape_19.setTransform(179.3647,97.0027);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#141313").ss(6.5,1,1).p("AjlBpQD8BdBzg4QB5g6gmj2");
	this.shape_20.setTransform(179.4277,97.3773);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#141313").ss(6.5,1,1).p("AjmBjQD8BiBzg3QB6g7gkj0");
	this.shape_21.setTransform(179.5101,97.9783);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#141313").ss(6.5,1,1).p("AjnBhQD9BkBzg3QB6g7gkjz");
	this.shape_22.setTransform(179.5501,98.179);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#141313").ss(6.5,1,1).p("AjnBgQD+BlByg3QB6g7gjjz");
	this.shape_23.setTransform(179.5738,98.3196);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#141313").ss(6.5,1,1).p("AjmBlQD9BhByg4QB6g7gkj0");
	this.shape_24.setTransform(179.4953,97.8349);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f().s("#141313").ss(6.5,1,1).p("AjlBqQD7BcBzg4QB6g6gmj2");
	this.shape_25.setTransform(179.4172,97.2845);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f().s("#141313").ss(6.5,1,1).p("AjkBvQD7BYBzg4QB5g7gnj4");
	this.shape_26.setTransform(179.3361,96.7677);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f().s("#141313").ss(6.5,1,1).p("AjkB0QD6BTB0g4QB5g7goj5");
	this.shape_27.setTransform(179.2734,96.3151);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f().s("#141313").ss(6.5,1,1).p("AjiB8QD4BMB1g5QB4g6gqj8");
	this.shape_28.setTransform(179.1461,95.4714);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f().s("#141313").ss(6.5,1,1).p("AjiCAQD4BJB1g6QB4g6grj9");
	this.shape_29.setTransform(179.0939,95.1168);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f().s("#141313").ss(6.5,1,1).p("AjhCDQD3BGB2g5QB3g7gsj9");
	this.shape_30.setTransform(179.0439,94.7966);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f().s("#141313").ss(6.5,1,1).p("AjhCGQD3BDB1g5QB3g7gsj+");
	this.shape_31.setTransform(179.0056,94.5225);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f().s("#141313").ss(6.5,1,1).p("AjgCKQD2BAB2g6QB3g7guj/");
	this.shape_32.setTransform(178.9432,94.0844);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f().s("#141313").ss(6.5,1,1).p("AjgCNQD2A9B2g6QB3g7gvkA");
	this.shape_33.setTransform(178.8924,93.7819);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f().s("#141313").ss(6.5,1,1).p("AjgCPQD2A7B2g6QB2g7gvkB");
	this.shape_34.setTransform(178.8694,93.6251);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1}]}).to({state:[{t:this.shape_1}]},9).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5,p:{y:95.9108}}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9,p:{y:94.3315}}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11,p:{x:178.9052,y:93.87}}]},1).to({state:[{t:this.shape_12,p:{y:93.7379}}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_14}]},28).to({state:[{t:this.shape_9,p:{y:94.3315}}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_1}]},10).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_27}]},1).to({state:[{t:this.shape_5,p:{y:95.8651}}]},1).to({state:[{t:this.shape_28}]},1).to({state:[{t:this.shape_29}]},1).to({state:[{t:this.shape_30}]},1).to({state:[{t:this.shape_31}]},1).to({state:[{t:this.shape_9,p:{y:94.2869}}]},1).to({state:[{t:this.shape_32}]},1).to({state:[{t:this.shape_11,p:{x:178.9182,y:93.9267}}]},1).to({state:[{t:this.shape_33}]},1).to({state:[{t:this.shape_12,p:{y:93.694}}]},1).to({state:[{t:this.shape_34}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_14}]},28).to({state:[{t:this.shape_9,p:{y:94.3315}}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_1}]},1).wait(1));
	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(9).to({_off:true},1).wait(52).to({_off:false,y:98.3915},0).wait(1).to({y:98.4384},0).wait(10).to({_off:true},1).wait(55).to({_off:false,y:98.3915},0).wait(1).to({y:98.4384},0).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(153.2,46.3,65,71.5);


(lib.Symbol1copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#1B1814").ss(11.5,1,1).p("Ai3hoQgJBhA4A6QAzA1BNAAQBLAAA2gyQA8g2AEhV");
	this.shape.setTransform(187.5066,-93.1);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(40));

	// Layer 2
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#1B1814").ss(11.5,1,1).p("Ai2hsQgJBkA4A9QAyA4BNAAQBKAAA3g1QA7g4AEhY");
	this.shape_1.setTransform(119.1566,-94.85);

	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(40));

	// Layer 3
	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#1B1814").ss(11,1,1).p("AkLhfQBDDEDRgGQBWgDBJgrQBKgtAahG");
	this.shape_2.setTransform(156.025,-52.5925);

	this.timeline.addTween(cjs.Tween.get(this.shape_2).wait(40));

	// Layer 5
	this.instance = new lib.Symbol2();
	this.instance.parent = this;
	this.instance.setTransform(137.85,-48.75,2.0168,2.0168,0,46.0839,51.8739,5.4,7.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1).to({regX:4.3,regY:4.3,scaleX:2.0159,scaleY:2.0177,skewX:45.7195,skewY:51.4594,x:141.35,y:-54.8},0).wait(1).to({scaleX:2.0136,scaleY:2.0199,skewX:44.7878,skewY:50.3997,x:141.8,y:-54.45},0).wait(1).to({scaleX:2.0096,scaleY:2.0237,skewX:43.1697,skewY:48.559,x:142.7,y:-53.7},0).wait(1).to({scaleX:2.0035,scaleY:2.0294,skewX:40.7228,skewY:45.7757,x:144.15,y:-52.85},0).wait(1).to({scaleX:1.995,scaleY:2.0375,skewX:37.2999,skewY:41.8822,x:146.35,y:-51.75},0).wait(1).to({scaleX:1.9839,scaleY:2.0481,skewX:32.8139,skewY:36.7794,x:149.5,y:-50.75},0).wait(1).to({scaleX:1.9705,scaleY:2.0609,skewX:27.3853,skewY:30.6045,x:153.3,y:-50.15},0).wait(1).to({scaleX:1.9559,scaleY:2.0748,skewX:21.5115,skewY:23.9231,x:157.55,y:-50.1},0).wait(1).to({scaleX:1.9422,scaleY:2.0879,skewX:15.9743,skewY:17.6245,x:161.1,y:-50.5},0).wait(1).to({scaleX:1.9309,scaleY:2.0987,skewX:11.4074,skewY:12.4298,x:163.95,y:-51.25},0).wait(1).to({scaleX:1.9225,scaleY:2.1066,skewX:8.0351,skewY:8.5938,x:166.05,y:-52},0).wait(1).to({scaleX:1.917,scaleY:2.1119,skewX:5.788,skewY:6.0378,x:167.45,y:-52.6},0).wait(1).to({scaleX:1.9137,scaleY:2.115,skewX:4.4942,skewY:4.5662,x:168.2,y:-53},0).wait(1).to({regX:5.4,regY:7.5,scaleX:1.9125,scaleY:2.1162,rotation:3.9793,skewX:0,skewY:0,x:170.2,y:-46.35},0).wait(7).to({regX:4.3,regY:4.3,scaleX:1.914,scaleY:2.1148,rotation:0,skewX:4.5733,skewY:4.6561,x:168.2,y:-52.95},0).wait(1).to({scaleX:1.9174,scaleY:2.1115,skewX:5.9626,skewY:6.2364,x:167.3,y:-52.5},0).wait(1).to({scaleX:1.923,scaleY:2.1061,skewX:8.2536,skewY:8.8424,x:165.8,y:-51.9},0).wait(1).to({scaleX:1.9312,scaleY:2.0984,skewX:11.5185,skewY:12.5562,x:163.55,y:-51.1},0).wait(1).to({scaleX:1.9416,scaleY:2.0884,skewX:15.7359,skewY:17.3534,x:160.5,y:-50.4},0).wait(1).to({scaleX:1.9539,scaleY:2.0767,skewX:20.7133,skewY:23.0151,x:156.95,y:-50},0).wait(1).to({scaleX:1.9672,scaleY:2.0641,skewX:26.0541,skewY:29.0903,x:153.3,y:-50.15},0).wait(1).to({scaleX:1.98,scaleY:2.0518,skewX:31.248,skewY:34.9982,x:150.1,y:-50.5},0).wait(1).to({scaleX:1.9914,scaleY:2.0409,skewX:35.8517,skewY:40.2349,x:147.3,y:-51.3},0).wait(1).to({scaleX:2.0007,scaleY:2.0321,skewX:39.611,skewY:44.5111,x:145.05,y:-52.3},0).wait(1).to({scaleX:2.0078,scaleY:2.0254,skewX:42.4537,skewY:47.7446,x:143.25,y:-53.25},0).wait(1).to({scaleX:2.0127,scaleY:2.0207,skewX:44.4197,skewY:49.9809,x:142.1,y:-54.1},0).wait(1).to({scaleX:2.0156,scaleY:2.0179,skewX:45.5968,skewY:51.3198,x:141.45,y:-54.7},0).wait(1).to({regX:5.4,regY:7.5,scaleX:2.0168,scaleY:2.0168,skewX:46.0839,skewY:51.8739,x:137.8,y:-48.7},0).wait(6));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(95,-111.5,116.80000000000001,74);


(lib.Symbol1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 7
	this.instance = new lib.Symbol1copy();
	this.instance.parent = this;
	this.instance.setTransform(64.25,241.25,1.0684,1.0684,0,0,0,80,54);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(120));

	// Layer 6
	this.instance_1 = new lib.Symbol2copy();
	this.instance_1.parent = this;
	this.instance_1.setTransform(185.4,214.55,2,2,0,0,180,104,112);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(120));

	// Layer 1
	this.instance_2 = new lib.pack();
	this.instance_2.parent = this;
	this.instance_2.setTransform(30,-15);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(120));

	// Layer 2
	this.instance_3 = new lib.Symbol2_1();
	this.instance_3.parent = this;
	this.instance_3.setTransform(191.4,214.55,2,2,0,0,180,104,112);

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(120));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-37,-15,436.9,461);


// stage content:
(lib.Oct_2_Vert = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 3
	this.instance = new lib.Symbol1();
	this.instance.parent = this;
	this.instance.setTransform(178.35,395.55,0.7053,0.7053,0,0,0,160.6,215);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1).to({regX:181.5,regY:215.5,x:193.1,y:395.9},0).wait(1).to({y:396},0).wait(1).to({y:396.1},0).wait(1).to({y:396.2},0).wait(1).to({y:396.4},0).wait(1).to({y:396.6},0).wait(1).to({y:396.85},0).wait(1).to({y:397.15},0).wait(1).to({y:397.45},0).wait(1).to({y:397.85},0).wait(1).to({y:398.25},0).wait(1).to({y:398.7},0).wait(1).to({y:399.15},0).wait(1).to({y:399.6},0).wait(1).to({y:400.05},0).wait(1).to({y:400.5},0).wait(1).to({y:400.85},0).wait(1).to({y:401.25},0).wait(1).to({y:401.55},0).wait(1).to({y:401.8},0).wait(1).to({y:402.05},0).wait(1).to({y:402.25},0).wait(1).to({y:402.45},0).wait(1).to({y:402.55},0).wait(1).to({y:402.7},0).wait(1).to({y:402.8},0).wait(1).to({y:402.85},0).wait(1).to({y:402.9},0).wait(1).to({regX:160.6,regY:215,x:178.35,y:402.6},0).wait(1).to({regX:181.5,regY:215.5,x:193.1,y:402.9},0).wait(1).to({y:402.8},0).wait(1).to({y:402.7},0).wait(1).to({y:402.6},0).wait(1).to({y:402.45},0).wait(1).to({y:402.25},0).wait(1).to({y:402},0).wait(1).to({y:401.75},0).wait(1).to({y:401.45},0).wait(1).to({y:401.1},0).wait(1).to({y:400.7},0).wait(1).to({y:400.3},0).wait(1).to({y:399.85},0).wait(1).to({y:399.4},0).wait(1).to({y:398.95},0).wait(1).to({y:398.55},0).wait(1).to({y:398.15},0).wait(1).to({y:397.8},0).wait(1).to({y:397.45},0).wait(1).to({y:397.15},0).wait(1).to({y:396.9},0).wait(1).to({y:396.7},0).wait(1).to({y:396.5},0).wait(1).to({y:396.35},0).wait(1).to({y:396.2},0).wait(1).to({y:396.1},0).wait(1).to({y:396},0).wait(1).to({y:395.95},0).wait(1).to({y:395.9},0).wait(1).to({regX:160.6,regY:215,x:178.35,y:395.55},0).wait(1));

	// Layer 4
	this.instance_1 = new lib.Symbol3();
	this.instance_1.parent = this;
	this.instance_1.setTransform(172.05,559.45,0.7758,0.7758,0,0,0,255,105);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(1).to({regX:345,regY:93.5,scaleX:0.7754,scaleY:0.7754,x:241.85,y:550.5},0).wait(1).to({scaleX:0.7747,scaleY:0.7747,x:241.75,y:550.55},0).wait(1).to({scaleX:0.7737,scaleY:0.7737,x:241.65},0).wait(1).to({scaleX:0.7724,scaleY:0.7724,x:241.55},0).wait(1).to({scaleX:0.7707,scaleY:0.7707,x:241.45},0).wait(1).to({scaleX:0.7686,scaleY:0.7686,x:241.25,y:550.6},0).wait(1).to({scaleX:0.7662,scaleY:0.7662,x:241.05,y:550.65},0).wait(1).to({scaleX:0.7633,scaleY:0.7633,x:240.75},0).wait(1).to({scaleX:0.7599,scaleY:0.7599,x:240.4,y:550.7},0).wait(1).to({scaleX:0.7562,scaleY:0.7562,x:240.15,y:550.75},0).wait(1).to({scaleX:0.752,scaleY:0.752,x:239.75},0).wait(1).to({scaleX:0.7476,scaleY:0.7476,x:239.3,y:550.85},0).wait(1).to({scaleX:0.743,scaleY:0.743,x:238.95},0).wait(1).to({scaleX:0.7384,scaleY:0.7384,x:238.5,y:550.95},0).wait(1).to({scaleX:0.734,scaleY:0.734,x:238.1},0).wait(1).to({scaleX:0.7297,scaleY:0.7297,x:237.7,y:551.05},0).wait(1).to({scaleX:0.7258,scaleY:0.7258,x:237.35},0).wait(1).to({scaleX:0.7223,scaleY:0.7223,x:237.05,y:551.15},0).wait(1).to({scaleX:0.7191,scaleY:0.7191,x:236.75},0).wait(1).to({scaleX:0.7164,scaleY:0.7164,x:236.5,y:551.2},0).wait(1).to({scaleX:0.714,scaleY:0.714,x:236.35},0).wait(1).to({scaleX:0.712,scaleY:0.712,x:236.1,y:551.25},0).wait(1).to({scaleX:0.7102,scaleY:0.7102,x:236},0).wait(1).to({scaleX:0.7088,scaleY:0.7088,x:235.85},0).wait(1).to({scaleX:0.7077,scaleY:0.7077,x:235.75,y:551.3},0).wait(1).to({scaleX:0.7067,scaleY:0.7067,x:235.7},0).wait(1).to({scaleX:0.7061,scaleY:0.7061,x:235.6},0).wait(1).to({scaleX:0.7056,scaleY:0.7056,x:235.5},0).wait(1).to({regX:255.1,regY:105,scaleX:0.7053,scaleY:0.7053,x:172,y:559.45},0).wait(1).to({regX:345,regY:93.5,scaleX:0.7057,scaleY:0.7057,x:235.45,y:551.35},0).wait(1).to({scaleX:0.7064,scaleY:0.7064,x:235.55,y:551.3},0).wait(1).to({scaleX:0.7073,scaleY:0.7073,x:235.6},0).wait(1).to({scaleX:0.7085,scaleY:0.7085,x:235.75},0).wait(1).to({scaleX:0.7101,scaleY:0.7101,x:235.9,y:551.25},0).wait(1).to({scaleX:0.712,scaleY:0.712,x:236.05,y:551.2},0).wait(1).to({scaleX:0.7143,scaleY:0.7143,x:236.3,y:551.25},0).wait(1).to({scaleX:0.717,scaleY:0.717,x:236.5,y:551.2},0).wait(1).to({scaleX:0.7201,scaleY:0.7201,x:236.8,y:551.15},0).wait(1).to({scaleX:0.7236,scaleY:0.7236,x:237.1,y:551.1},0).wait(1).to({scaleX:0.7275,scaleY:0.7275,x:237.45,y:551.05},0).wait(1).to({scaleX:0.7317,scaleY:0.7317,x:237.85,y:551},0).wait(1).to({scaleX:0.7361,scaleY:0.7361,x:238.25,y:550.95},0).wait(1).to({scaleX:0.7405,scaleY:0.7405,x:238.65,y:550.9},0).wait(1).to({scaleX:0.7449,scaleY:0.7449,x:239.05,y:550.85},0).wait(1).to({scaleX:0.7491,scaleY:0.7491,x:239.4,y:550.8},0).wait(1).to({scaleX:0.7531,scaleY:0.7531,x:239.75,y:550.75},0).wait(1).to({scaleX:0.7567,scaleY:0.7567,x:240.1,y:550.7},0).wait(1).to({scaleX:0.76,scaleY:0.76,x:240.4,y:550.65},0).wait(1).to({scaleX:0.7629,scaleY:0.7629,x:240.65},0).wait(1).to({scaleX:0.7655,scaleY:0.7655,x:240.9,y:550.6},0).wait(1).to({scaleX:0.7677,scaleY:0.7677,x:241.1},0).wait(1).to({scaleX:0.7696,scaleY:0.7696,x:241.25,y:550.55},0).wait(1).to({scaleX:0.7712,scaleY:0.7712,x:241.4},0).wait(1).to({scaleX:0.7725,scaleY:0.7725,x:241.5},0).wait(1).to({scaleX:0.7736,scaleY:0.7736,x:241.65},0).wait(1).to({scaleX:0.7744,scaleY:0.7744,y:550.5},0).wait(1).to({scaleX:0.7751,scaleY:0.7751,x:241.75},0).wait(1).to({scaleX:0.7755,scaleY:0.7755,x:241.8},0).wait(1).to({regX:255.1,regY:105.1,scaleX:0.7758,scaleY:0.7758,x:172.05,y:559.5},0).wait(1));

	// Layer_11
	this.instance_2 = new lib.bg_vert();
	this.instance_2.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(60));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(153.9,406,363.4,406);
// library properties:
lib.properties = {
	id: '273F45E4908ABC429A41BE56345D9808',
	width: 375,
	height: 812,
	fps: 30,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: [
		{src:"assets/images/bg_vert55-2.jpg", id:"bg_vert"},
		{src:"assets/images/pack55-2.png", id:"pack"},
		{src:"assets/images/shadow55-2.png", id:"shadow"}
	],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['273F45E4908ABC429A41BE56345D9808'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}



})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;

$(function () {
  var canvas, stage, exportRoot, anim_container, dom_overlay_container, fnStartAnimation;
  function init() {
    canvas = document.getElementById("canvas55-2");
    anim_container = document.getElementById("animation_container55-2");
    dom_overlay_container = document.getElementById("dom_overlay_container55-2");
    var comp=AdobeAn.getComposition("273F45E4908ABC429A41BE56345D9808");
    var lib=comp.getLibrary();
    var loader = new createjs.LoadQueue(false);
    loader.addEventListener("fileload", function(evt){handleFileLoad(evt,comp)});
    loader.addEventListener("complete", function(evt){handleComplete(evt,comp)});
    var lib=comp.getLibrary();
    loader.loadManifest(lib.properties.manifest);
  }
  function handleFileLoad(evt, comp) {
    var images=comp.getImages();
    if (evt && (evt.item.type == "image")) { images[evt.item.id] = evt.result; }
  }
  function handleComplete(evt,comp) {
    //This function is always called, irrespective of the content. You can use the variable "stage" after it is created in token create_stage.
    var lib=comp.getLibrary();
    var ss=comp.getSpriteSheet();
    var queue = evt.target;
    var ssMetadata = lib.ssMetadata;
    for(i=0; i<ssMetadata.length; i++) {
      ss[ssMetadata[i].name] = new createjs.SpriteSheet( {"images": [queue.getResult(ssMetadata[i].name)], "frames": ssMetadata[i].frames} )
    }
    var preloaderDiv = document.getElementById("_preload_div_55");
    preloaderDiv.style.display = 'none';
    canvas.style.display = 'block';
    exportRoot = new lib.Oct_2_Vert();
    stage = new lib.Stage(canvas);
    //Registers the "tick" event listener.
    fnStartAnimation = function() {
      stage.addChild(exportRoot);
      createjs.Ticker.setFPS(lib.properties.fps);
      createjs.Ticker.addEventListener("tick", stage);
    }
    //Code to support hidpi screens and responsive scaling.
    function makeResponsive(isResp, respDim, isScale, scaleType) {
      var lastW, lastH, lastS=1;
      window.addEventListener('resize', resizeCanvas);
      resizeCanvas();
      function resizeCanvas() {
        var w = lib.properties.width, h = lib.properties.height;
        var iw = window.innerWidth, ih=window.innerHeight;
        var pRatio = window.devicePixelRatio || 1, xRatio=iw/w, yRatio=ih/h, sRatio=1;
        if(isResp) {
          if((respDim=='width'&&lastW==iw) || (respDim=='height'&&lastH==ih)) {
            sRatio = lastS;
          }
          else if(!isScale) {
            if(iw<w || ih<h)
              sRatio = Math.min(xRatio, yRatio);
          }
          else if(scaleType==1) {
            sRatio = Math.min(xRatio, yRatio);
          }
          else if(scaleType==2) {
            sRatio = Math.max(xRatio, yRatio);
          }
        }
        canvas.width = w*pRatio*sRatio;
        canvas.height = h*pRatio*sRatio;
        canvas.style.width = anim_container.style.width = dom_overlay_container.style.width = preloaderDiv.style.width = w*sRatio+'px';
        canvas.style.height = anim_container.style.height = dom_overlay_container.style.height = preloaderDiv.style.height = h*sRatio+'px';
        stage.scaleX = pRatio*sRatio;
        stage.scaleY = pRatio*sRatio;
        lastW = iw; lastH = ih; lastS = sRatio;
        stage.tickOnUpdate = false;
        stage.update();
        stage.tickOnUpdate = true;
      }
    }
    makeResponsive(true,'both',true,2);
    AdobeAn.compositionLoaded(lib.properties.id);
    fnStartAnimation();
  }
  init();
});