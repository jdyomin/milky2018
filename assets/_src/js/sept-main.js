(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [];


// symbols:



(lib.bg_part = function() {
	this.initialize(img.bg_part);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,731,234);


(lib.hand1 = function() {
	this.initialize(img.hand1);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,47,45);


(lib.hand2 = function() {
	this.initialize(img.hand2);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,39,48);


(lib.island = function() {
	this.initialize(img.island);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,277,275);


(lib.pack = function() {
	this.initialize(img.pack);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,332,510);


(lib.shadow = function() {
	this.initialize(img.shadow);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,241,39);// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.Symbol14 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(13.5,1,1).p("AjMATQA6AOA+gBQA/AABEgGQBGgGArgZQAsgaABAA");
	this.shape.setTransform(18.4,8.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(13.5,1,1).p("AjHAOQA2AWA+ADQA9ACBEgIQBDgJAsgbQArgcAAgH");
	this.shape_1.setTransform(18.3,8.5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(13.5,1,1).p("AjDAKQAzAcA+AGQA8AEBCgKQBCgJAqgdQArgeABgO");
	this.shape_2.setTransform(18.2,8.3);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(13.5,1,1).p("Ai/AHQAwAiA+AIQA7AGBBgLQBAgMApgfQArgfABgU");
	this.shape_3.setTransform(18.1,8);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(13.5,1,1).p("Ai7AEQAtAnA+AKQA6AIBAgNQA+gMApghQAqghABgZ");
	this.shape_4.setTransform(18,7.8);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(13.5,1,1).p("Ai4ABQAqArA/AMQA5AKA/gOQA9gOAogiQAqgiABgd");
	this.shape_5.setTransform(17.9,7.7);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(13.5,1,1).p("Ai2gBQApAvA/ANQA4AMA+gQQA8gOAogjQAqgjABgi");
	this.shape_6.setTransform(17.8,7.5);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(13.5,1,1).p("Ai0gDQAoAyA+APQA4AMA9gQQA7gQApgkQApgjABgl");
	this.shape_7.setTransform(17.8,7.4);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(13.5,1,1).p("AiygEQAmA0A+APQA4AOA9gRQA6gQAoglQApgkABgn");
	this.shape_8.setTransform(17.8,7.3);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(13.5,1,1).p("AixgFQAlA1A/ARQA3AOA8gSQA6gQAoglQApglABgp");
	this.shape_9.setTransform(17.7,7.3);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(13.5,1,1).p("AiwgGQAkA3A/ARQA2AOA9gSQA5gQAogmQApglABgq");
	this.shape_10.setTransform(17.7,7.2);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(13.5,1,1).p("AiwgGQAkA3A/ARQA3AOA8gSQA6gQAngmQApglABgq");
	this.shape_11.setTransform(17.7,7.2);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(13.5,1,1).p("AizgEQAnAzA+AQQA4ANA9gRQA7gQAogkQApgkABgn");
	this.shape_12.setTransform(17.8,7.4);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(13.5,1,1).p("Ai1gBQAoAvA/AOQA4AMA+gQQA7gPApgjQApgjABgj");
	this.shape_13.setTransform(17.8,7.5);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(13.5,1,1).p("Ai4AAQAqAsA/ANQA4AKA/gPQA9gNApgjQApgiACge");
	this.shape_14.setTransform(17.9,7.7);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(13.5,1,1).p("Ai6ACQAsApA/ALQA5AJBAgOQA9gNAqghQApghABgb");
	this.shape_15.setTransform(18,7.8);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(13.5,1,1).p("Ai9AFQAuAlA/AJQA6AIBBgNQA+gMAqggQAqggABgX");
	this.shape_16.setTransform(18,7.9);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(13.5,1,1).p("Ai/AHQAwAhA+AIQA7AGBBgLQBAgLAqgfQAqgfABgT");
	this.shape_17.setTransform(18.1,8.1);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#000000").ss(13.5,1,1).p("AjCAJQAyAeA/AGQA7AFBCgKQBBgLAqgdQArgeABgQ");
	this.shape_18.setTransform(18.1,8.2);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#000000").ss(13.5,1,1).p("AjEAMQA0AaA+AEQA9AEBCgJQBCgKArgcQArgdAAgM");
	this.shape_19.setTransform(18.2,8.3);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#000000").ss(13.5,1,1).p("AjHAOQA2AWA+ADQA9ACBEgIQBDgIArgcQArgcABgH");
	this.shape_20.setTransform(18.3,8.5);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#000000").ss(13.5,1,1).p("AjJAQQA3ASA/ACQA+ABBDgHQBFgIArgaQAsgbAAgE");
	this.shape_21.setTransform(18.3,8.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},8).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_11}]},34).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape}]},1).wait(16));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-8.8,-1.1,54.5,20);


(lib.Symbol11 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AgvAwQgTgUAAgcQAAgbATgUQAUgUAbAAQAcAAAUAUQATAUAAAbQAAAcgTAUQgUATgcABQgbgBgUgTg");
	this.shape.setTransform(6.7,6.8);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol11, new cjs.Rectangle(0,0,13.5,13.5), null);


(lib.Symbol10 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AgvAwQgTgUAAgcQAAgbATgUQAUgUAbAAQAcAAAUAUQATAUAAAbQAAAcgTAUQgUATgcABQgbgBgUgTg");
	this.shape.setTransform(6.7,6.8);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol10, new cjs.Rectangle(0,0,13.5,13.5), null);


(lib.Symbol9 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#B7B7B7").s().p("AiMCMQg5g6gBhSQABhSA5g6QA6g5BSgBQBSABA6A5QA6A6AABSQAABSg6A6Qg6A6hSAAQhSAAg6g6g");
	this.shape.setTransform(19.9,19.9);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol9, new cjs.Rectangle(0,0,39.7,39.7), null);


(lib.Symbol7 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#B7B7B7").s().p("AiMCMQg5g6gBhSQABhSA5g6QA6g5BSgBQBSABA6A5QA6A6AABSQAABSg6A6Qg6A6hSAAQhSAAg6g6g");
	this.shape.setTransform(19.9,19.9);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol7, new cjs.Rectangle(0,0,39.7,39.7), null);


(lib.Symbol4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.hand2();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol4, new cjs.Rectangle(0,0,39,48), null);


(lib.Symbol3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.hand1();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol3, new cjs.Rectangle(0,0,47,45), null);


(lib.Symbol2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.island();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol2, new cjs.Rectangle(0,0,277,275), null);


(lib.Symbol1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.shadow();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol1, new cjs.Rectangle(0,0,241,39), null);


(lib.Symbol15 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 4 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("AxoOzIFBzJQAAABFAi2IAAqBQZPgPAAABMgEyAi1IAAAAQgLAA+TiogAxoOzIAAAAIAAAAIAAAAg");
	mask.setTransform(59.5,80);

	// Symbol 4
	this.instance = new lib.Symbol4();
	this.instance.parent = this;
	this.instance.setTransform(19.5,102.8,1,1,0,0,0,19.5,24);

	var maskedShapeInstanceList = [this.instance];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(4).to({y:137.8},15,cjs.Ease.get(1)).wait(30).to({y:102.8},15,cjs.Ease.get(1)).wait(16));

	// Layer 3
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(13.5,1,1).p("AlVnVQETAcC/B8QC/B7AXCrQAWCpiDCOQiDCOiMAo");
	this.shape.setTransform(37.8,45.3);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(13.5,1,1).p("AlQnqQETAdC6CBQC6CBAWCyQAXCyh9CUQh+CUiNAq");
	this.shape_1.setTransform(37.2,47.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(13.5,1,1).p("AlLn+QETAfC1CGQC1CGAWC6QAXC3h4CbQh5CaiNAs");
	this.shape_2.setTransform(36.7,49.8);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(13.5,1,1).p("AlGoQQETAgCwCLQCwCLAXDAQAWC+hzCfQh1CgiMAu");
	this.shape_3.setTransform(36.3,51.8);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(13.5,1,1).p("AlCohQETAhCrCQQCtCPAWDGQAXDEhvClQhxCliMAv");
	this.shape_4.setTransform(35.9,53.6);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(13.5,1,1).p("Ak+owQETAiCnCTQCpCUAWDLQAXDKhrCoQhtCqiMAx");
	this.shape_5.setTransform(35.5,55.3);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(13.5,1,1).p("Ak7o+QETAjCkCXQClCXAXDRQAWDPhnCsQhpCuiNAy");
	this.shape_6.setTransform(35.1,56.8);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(13.5,1,1).p("Ak4pKQETAjChCbQCiCaAXDVQAWDUhkCvQhmCyiNAz");
	this.shape_7.setTransform(34.8,58.2);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(13.5,1,1).p("Ak1pVQETAkCeCdQCfCeAXDZQAWDXhhCyQhjC3iNAz");
	this.shape_8.setTransform(34.5,59.4);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(13.5,1,1).p("AkzpfQETAlCcCgQCdCgAWDcQAXDbhfC2QhhC4iNA1");
	this.shape_9.setTransform(34.3,60.5);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(13.5,1,1).p("AkxpmQETAlCaCiQCbCiAWDfQAXDdhdC4QhfC8iNA0");
	this.shape_10.setTransform(34.1,61.3);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(13.5,1,1).p("AkvptQETAlCYCkQCZCkAXDhQAWDghbC6QhdC9iNA2");
	this.shape_11.setTransform(33.9,62.1);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(13.5,1,1).p("AkupyQETAmCXClQCYClAWDjQAXDihaC7QhcC/iNA2");
	this.shape_12.setTransform(33.8,62.6);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(13.5,1,1).p("Aktp2QETAmCWCmQCXCmAWDlQAXDjhZC8QhbDAiNA3");
	this.shape_13.setTransform(33.7,63);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(13.5,1,1).p("Aksp4QETAmCVCnQCWCmAXDlQAWDlhYC9QhbDAiMA3");
	this.shape_14.setTransform(33.7,63.3);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(13.5,1,1).p("Aksp5QETAmCVCoQCWCmAWDmQAXDkhYC9QhbDBiMA3");
	this.shape_15.setTransform(33.7,63.4);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(13.5,1,1).p("AkxpkQETAlCaCiQCcChAWDeQAXDcheC3QhgC7iMA1");
	this.shape_16.setTransform(34.2,61);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(13.5,1,1).p("Ak2pQQETAjCfCdQChCcAWDWQAXDWhjCyQhlC0iMAz");
	this.shape_17.setTransform(34.7,58.9);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#000000").ss(13.5,1,1).p("Ak/otQETAhCpCTQCpCTAWDKQAXDJhsCoQhtCpiNAw");
	this.shape_18.setTransform(35.5,55);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#000000").ss(13.5,1,1).p("AlDoeQETAgCtCQQCtCPAWDEQAXDEhwCjQhxCkiNAv");
	this.shape_19.setTransform(35.9,53.3);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#000000").ss(13.5,1,1).p("AlJoEQETAfCzCIQCzCJAXC7QAWC6h2CbQh4CciMAt");
	this.shape_20.setTransform(36.6,50.4);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#000000").ss(13.5,1,1).p("AlMn5QETAeC2CGQC2CEAXC4QAWC2h6CZQh6CYiMAs");
	this.shape_21.setTransform(36.9,49.2);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#000000").ss(13.5,1,1).p("AlOnvQETAdC4CDQC5CDAWC0QAXCyh8CWQh9CWiMAq");
	this.shape_22.setTransform(37.1,48.2);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#000000").ss(13.5,1,1).p("AlQnnQETAdC6CBQC7CAAWCxQAXCwh+CTQh/CTiMAq");
	this.shape_23.setTransform(37.3,47.3);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#000000").ss(13.5,1,1).p("AlSnhQETAdC8B/QC8B/AXCvQAWCtiACRQiACRiMAq");
	this.shape_24.setTransform(37.5,46.5);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f().s("#000000").ss(13.5,1,1).p("AlTncQETAdC9B+QC+B9AWCtQAXCriBCQQiCCPiMAq");
	this.shape_25.setTransform(37.6,46);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f().s("#000000").ss(13.5,1,1).p("AlUnYQETAcC+B9QC/B8AWCsQAXCqiCCPQiDCOiMAp");
	this.shape_26.setTransform(37.7,45.6);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f().s("#000000").ss(13.5,1,1).p("AlVnWQETAcC/B9QC/B7AXCrQAWCpiCCOQiDCOiNAp");
	this.shape_27.setTransform(37.7,45.3);

	var maskedShapeInstanceList = [this.shape,this.shape_1,this.shape_2,this.shape_3,this.shape_4,this.shape_5,this.shape_6,this.shape_7,this.shape_8,this.shape_9,this.shape_10,this.shape_11,this.shape_12,this.shape_13,this.shape_14,this.shape_15,this.shape_16,this.shape_17,this.shape_18,this.shape_19,this.shape_20,this.shape_21,this.shape_22,this.shape_23,this.shape_24,this.shape_25,this.shape_26,this.shape_27];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},4).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_15}]},30).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_27}]},1).to({state:[{t:this.shape}]},1).wait(16));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-3.2,-8.5,82,135.3);


(lib.Symbol12 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Symbol 6 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("ABVCXQg5g6AAhSQAAhRA5g7QA7g5BTAAQBRAAA6A5QA7A7AABRQAABSg7A6Qg6A7hRAAQhTAAg7g7g");
	mask.setTransform(42.5,21);

	// Symbol 7
	this.instance = new lib.Symbol7();
	this.instance.parent = this;
	this.instance.setTransform(67.2,62.5,1,1,0,0,0,19.9,19.9);

	var maskedShapeInstanceList = [this.instance];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(5).to({y:58.9},0).wait(1).to({y:56.7},0).wait(1).to({y:55.1},0).wait(1).to({y:53.9},0).wait(1).to({y:53.1},0).wait(1).to({y:52.4},0).wait(1).to({y:51.8},0).wait(1).to({y:51.4},0).wait(1).to({y:51.1},0).wait(1).to({y:50.8},0).wait(1).to({y:50.6},0).wait(1).to({y:50.4},0).wait(1).to({y:50.3},0).wait(1).to({y:50.2},0).wait(38).to({y:62.5},8,cjs.Ease.get(1)).wait(16));

	// Symbol 8 (mask)
	var mask_1 = new cjs.Shape();
	mask_1._off = true;
	mask_1.graphics.p("AiMCMQg5g6gBhSQABhSA5g6QA6g5BSgBQBSABA6A5QA6A6AABSQAABSg6A6Qg6A6hSAAQhSAAg6g6g");
	mask_1.setTransform(19.9,19.9);

	// Symbol 9
	this.instance_1 = new lib.Symbol9();
	this.instance_1.parent = this;
	this.instance_1.setTransform(22,60.5,1,1,0,0,0,19.9,19.9);

	var maskedShapeInstanceList = [this.instance_1];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask_1;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(5).to({y:56.9},0).wait(1).to({y:54.7},0).wait(1).to({y:53.2},0).wait(1).to({y:52},0).wait(1).to({y:51.1},0).wait(1).to({y:50.4},0).wait(1).to({y:49.9},0).wait(1).to({y:49.4},0).wait(1).to({y:49.1},0).wait(1).to({y:48.8},0).wait(1).to({y:48.6},0).wait(1).to({y:48.5},0).wait(1).to({y:48.4},0).wait(1).to({y:48.3},0).wait(38).to({y:60.5},8,cjs.Ease.get(1)).wait(16));

	// Symbol 11
	this.instance_2 = new lib.Symbol11();
	this.instance_2.parent = this;
	this.instance_2.setTransform(63.6,25,1,1,0,0,0,6.7,6.8);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(10).to({x:60.1,y:29.6},9,cjs.Ease.get(1)).wait(8).to({x:63.6,y:25},7,cjs.Ease.get(1)).wait(7).to({x:60.1,y:29.6},7,cjs.Ease.get(1)).wait(8).to({x:63.6,y:25},9,cjs.Ease.get(1)).wait(15));

	// Symbol 10
	this.instance_3 = new lib.Symbol10();
	this.instance_3.parent = this;
	this.instance_3.setTransform(20.5,21.4,1,1,0,0,0,6.7,6.8);

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(10).to({x:17,y:26},9,cjs.Ease.get(1)).wait(8).to({x:20.5,y:21.4},7,cjs.Ease.get(1)).wait(7).to({x:17,y:26},7,cjs.Ease.get(1)).wait(8).to({x:20.5,y:21.4},9,cjs.Ease.get(1)).wait(15));

	// Layer 8
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AiMCMQg5g6gBhSQABhSA5g6QA6g5BSgBQBSABA6A5QA6A6AABSQAABSg6A6Qg6A6hSAAQhSAAg6g6g");
	this.shape.setTransform(19.9,19.9);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(80));

	// Layer 9
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AiMCMQg6g6AAhSQAAhSA6g6QA7g5BRgBQBSABA6A5QA7A6gBBSQABBSg7A6Qg6A6hSAAQhRAAg7g6g");
	this.shape_1.setTransform(65.2,22.1);

	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(80));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,85,42);


(lib.Symbol5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("Ai+MrI2ckrILO0yMAnnAEeIo1TJIw3B9QABABgBAAIitgIgA5aIAIAAAAIAAAAIAAAAg");
	mask.setTransform(152.2,40.4);

	// Layer 1
	this.instance = new lib.island();
	this.instance.parent = this;

	this.instance_1 = new lib.Symbol1();
	this.instance_1.parent = this;
	this.instance_1.setTransform(133.5,360.5,1,1,0,0,0,120.5,19.5);

	var maskedShapeInstanceList = [this.instance,this.instance_1];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_1},{t:this.instance}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol5, new cjs.Rectangle(0,0,277,122.2), null);


(lib.Symbol13 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Symbol 5
	this.instance = new lib.Symbol5();
	this.instance.parent = this;
	this.instance.setTransform(138.5,207.5,1,1,0,0,0,138.5,190);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(4).to({y:190},15,cjs.Ease.get(1)).wait(30).to({y:207.5},15,cjs.Ease.get(1)).wait(16));

	// Symbol 3
	this.instance_1 = new lib.Symbol3();
	this.instance_1.parent = this;
	this.instance_1.setTransform(40.6,179,1,1,0,0,0,23.5,22.5);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(4).to({y:161.5},15,cjs.Ease.get(1)).wait(30).to({y:179},15,cjs.Ease.get(1)).wait(16));

	// Layer 6 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("AzVTWMAAAgmrMAmrAAAMAAAAmrg");
	mask.setTransform(38,106.5);

	// Layer 5
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(13.5,1,1).p("AKFl8QoNAfkwDXQkxDVibEu");
	this.shape.setTransform(109.3,120.4);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(13.5,1,1).p("AqCF0QCNkxE3jQQEzjQIOgW");
	this.shape_1.setTransform(109.4,119.4);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(13.5,1,1).p("AqBFsQCCk2E8jKQE1jKIQgN");
	this.shape_2.setTransform(109.4,118.5);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(13.5,1,1).p("AqAFkQB4k5FAjGQE4jDIRgF");
	this.shape_3.setTransform(109.4,117.6);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(13.5,1,1).p("Ap/FdQBtk8FGjCQE5i+ITAD");
	this.shape_4.setTransform(109.5,116.9);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(13.5,1,1).p("Ap+FXQBklAFJi9QE8i5IUAJ");
	this.shape_5.setTransform(109.5,116.2);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(13.5,1,1).p("Ap9FSQBblCFNi6QE9i0IWAP");
	this.shape_6.setTransform(109.6,115.5);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(13.5,1,1).p("Ap8FNQBUlFFQi2QE+iwIXAU");
	this.shape_7.setTransform(109.6,114.9);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(13.5,1,1).p("Ap7FJQBOlHFSi0QFAisIXAZ");
	this.shape_8.setTransform(109.6,114.3);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(13.5,1,1).p("Ap7FFQBIlHFViyQFCiqIYAe");
	this.shape_9.setTransform(109.6,113.8);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(13.5,1,1).p("Ap6FDQBDlJFXiwQFCinIZAh");
	this.shape_10.setTransform(109.6,113.4);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(13.5,1,1).p("Ap5FAQA/lKFZiuQFCilIZAk");
	this.shape_11.setTransform(109.6,113);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(13.5,1,1).p("Ap5E/QA8lMFaitQFDijIaAn");
	this.shape_12.setTransform(109.7,112.8);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(13.5,1,1).p("Ap5E9QA6lMFbisQFEihIaAn");
	this.shape_13.setTransform(109.6,112.6);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(13.5,1,1).p("Ap5E9QA5lMFbisQFEihIbAp");
	this.shape_14.setTransform(109.7,112.4);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(13.5,1,1).p("AJ6kzQobgplECgQlcCsg4FM");
	this.shape_15.setTransform(109.7,112.4);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(13.5,1,1).p("Ap6FEQBFlJFXixQFBinIYAg");
	this.shape_16.setTransform(109.6,113.5);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(13.5,1,1).p("Ap7FLQBQlGFSi1QE/iuIWAX");
	this.shape_17.setTransform(109.6,114.6);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#000000").ss(13.5,1,1).p("Ap+FYQBmk+FIi/QE7i5IUAH");
	this.shape_18.setTransform(109.5,116.3);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#000000").ss(13.5,1,1).p("Ap/FfQBvk8FEjCQE5i/ITAB");
	this.shape_19.setTransform(109.5,117);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#000000").ss(13.5,1,1).p("AqBFqQB/k3E9jJQE2jIIRgK");
	this.shape_20.setTransform(109.4,118.2);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#000000").ss(13.5,1,1).p("AqCFuQCFk0E7jNQE1jLIQgP");
	this.shape_21.setTransform(109.4,118.7);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#000000").ss(13.5,1,1).p("AqCFyQCLkzE4jPQEzjOIPgT");
	this.shape_22.setTransform(109.4,119.2);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#000000").ss(13.5,1,1).p("AqDF1QCQkxE3jRQEyjQIOgX");
	this.shape_23.setTransform(109.4,119.5);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#000000").ss(13.5,1,1).p("AqDF4QCUkwE0jSQEyjTINga");
	this.shape_24.setTransform(109.4,119.8);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f().s("#000000").ss(13.5,1,1).p("AqDF6QCWkuE0jUQEwjVINgc");
	this.shape_25.setTransform(109.3,120);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f().s("#000000").ss(13.5,1,1).p("AqEF8QCZkuEyjVQEwjWIOge");
	this.shape_26.setTransform(109.4,120.2);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f().s("#000000").ss(13.5,1,1).p("AqEF9QCakuEyjVQEwjXINgf");
	this.shape_27.setTransform(109.3,120.3);

	var maskedShapeInstanceList = [this.shape,this.shape_1,this.shape_2,this.shape_3,this.shape_4,this.shape_5,this.shape_6,this.shape_7,this.shape_8,this.shape_9,this.shape_10,this.shape_11,this.shape_12,this.shape_13,this.shape_14,this.shape_15,this.shape_16,this.shape_17,this.shape_18,this.shape_19,this.shape_20,this.shape_21,this.shape_22,this.shape_23,this.shape_24,this.shape_25,this.shape_26,this.shape_27];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},4).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_15}]},30).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_27}]},1).to({state:[{t:this.shape}]},1).wait(16));

	// Symbol 2
	this.instance_2 = new lib.Symbol1();
	this.instance_2.parent = this;
	this.instance_2.setTransform(133.5,360.5,1.2,1.2,0,0,0,120.5,19.5);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(4).to({scaleX:1,scaleY:1},15,cjs.Ease.get(1)).wait(30).to({scaleX:1.2,scaleY:1.2},15,cjs.Ease.get(1)).wait(16));

	// Symbol 2
	this.instance_3 = new lib.Symbol2();
	this.instance_3.parent = this;
	this.instance_3.setTransform(138.5,207.5,1,1,0,0,0,138.5,190);

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(4).to({y:190},15,cjs.Ease.get(1)).wait(30).to({y:207.5},15,cjs.Ease.get(1)).wait(16));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-11.1,-24,325.9,421.5);


// stage content:
(lib.Main = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 17
	this.instance = new lib.Symbol12();
	this.instance.parent = this;
	this.instance.setTransform(877.7,277.5,1,1,0,0,0,43.5,35);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	// Layer 6
	this.instance_1 = new lib.Symbol14();
	this.instance_1.parent = this;
	this.instance_1.setTransform(881.2,303.9,1,1,0,0,0,17.7,7.2);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(1));

	// Layer 18
	this.instance_2 = new lib.Symbol13();
	this.instance_2.parent = this;
	this.instance_2.setTransform(778.2,415.2,1,1,0,0,0,152.2,169.2);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(1));

	// Layer 10
	this.instance_3 = new lib.Symbol15();
	this.instance_3.parent = this;
	this.instance_3.setTransform(977.5,396,1,1,0,0,0,33.6,77.5);

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(1));

	// Layer 3
	this.instance_4 = new lib.pack();
	this.instance_4.parent = this;
	this.instance_4.setTransform(708,128);

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(1));

	// Layer 2
	this.instance_5 = new lib.bg_part();
	this.instance_5.parent = this;
	this.instance_5.setTransform(349,446);

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(540,340,1116.3,680);
// library properties:
lib.properties = {
	id: '5F80515F8B283B45B7806C9D81901A99',
	width: 1080,
	height: 680,
	fps: 30,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: [
		{src:"assets/images/hand123.png", id:"hand1"},
		{src:"assets/images/hand223.png", id:"hand2"},
		{src:"assets/images/island23.png", id:"island"},
		{src:"assets/images/pack23.png", id:"pack"},
		{src:"assets/images/shadow23.png", id:"shadow"}
	],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['5F80515F8B283B45B7806C9D81901A99'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}



})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;

$(function () {
    var canvas, stage, exportRoot, anim_container, dom_overlay_container, fnStartAnimation;
    function init() {
        canvas = document.getElementById("canvas23");
        anim_container = document.getElementById("animation_container23");
        dom_overlay_container = document.getElementById("dom_overlay_container23");
        var comp=AdobeAn.getComposition("5F80515F8B283B45B7806C9D81901A99");
        var lib=comp.getLibrary();
        var loader = new createjs.LoadQueue(false);
        loader.addEventListener("fileload", function(evt){handleFileLoad(evt,comp)});
        loader.addEventListener("complete", function(evt){handleComplete(evt,comp)});
        var lib=comp.getLibrary();
        loader.loadManifest(lib.properties.manifest);
    }
    function handleFileLoad(evt, comp) {
        var images=comp.getImages();
        if (evt && (evt.item.type == "image")) { images[evt.item.id] = evt.result; }
    }
    function handleComplete(evt,comp) {
        //This function is always called, irrespective of the content. You can use the variable "stage" after it is created in token create_stage.
        var lib=comp.getLibrary();
        var ss=comp.getSpriteSheet();
        var queue = evt.target;
        var ssMetadata = lib.ssMetadata;
        for(i=0; i<ssMetadata.length; i++) {
            ss[ssMetadata[i].name] = new createjs.SpriteSheet( {"images": [queue.getResult(ssMetadata[i].name)], "frames": ssMetadata[i].frames} )
        }
        exportRoot = new lib.Main();
        stage = new lib.Stage(canvas);
        //Registers the "tick" event listener.
        fnStartAnimation = function() {
            stage.addChild(exportRoot);
            createjs.Ticker.setFPS(lib.properties.fps);
            createjs.Ticker.addEventListener("tick", stage);
        }
        //Code to support hidpi screens and responsive scaling.
        function makeResponsive(isResp, respDim, isScale, scaleType) {
            var lastW, lastH, lastS=1;
            window.addEventListener('resize', resizeCanvas);
            resizeCanvas();
            function resizeCanvas() {
                var w = lib.properties.width, h = lib.properties.height;
                var iw = window.innerWidth, ih=window.innerHeight;
                var pRatio = window.devicePixelRatio || 1, xRatio=iw/w, yRatio=ih/h, sRatio=1;
                if(isResp) {
                    if((respDim=='width'&&lastW==iw) || (respDim=='height'&&lastH==ih)) {
                        sRatio = lastS;
                    }
                    else if(!isScale) {
                        if(iw<w || ih<h)
                            sRatio = Math.min(xRatio, yRatio);
                    }
                    else if(scaleType==1) {
                        sRatio = Math.min(xRatio, yRatio);
                    }
                    else if(scaleType==2) {
                        sRatio = Math.max(xRatio, yRatio);
                    }
                }
                canvas.width = w*pRatio*sRatio;
                canvas.height = h*pRatio*sRatio;
                canvas.style.width = dom_overlay_container.style.width = anim_container.style.width =  w*sRatio+'px';
                canvas.style.height = anim_container.style.height = dom_overlay_container.style.height = h*sRatio+'px';
                stage.scaleX = pRatio*sRatio;
                stage.scaleY = pRatio*sRatio;
                lastW = iw; lastH = ih; lastS = sRatio;
                stage.tickOnUpdate = false;
                stage.update();
                stage.tickOnUpdate = true;
            }
        }
        makeResponsive(true,'both',true,2);
        AdobeAn.compositionLoaded(lib.properties.id);
        fnStartAnimation();
    }
    init();
});