(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [];


// symbols:



(lib.bg_vert = function() {
	this.initialize(img.bg_vert);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,375,718);


(lib.magnit = function() {
	this.initialize(img.magnit);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,77,116);


(lib.pack = function() {
	this.initialize(img.pack);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,400,541);


(lib.shadow1 = function() {
	this.initialize(img.shadow1);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,109,159);// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.Symbol6 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.bg_vert();
	this.instance.parent = this;
	this.instance.setTransform(-187.5,-359);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol6, new cjs.Rectangle(-187.5,-359,375,718), null);


(lib.Symbol2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.shadow1();
	this.instance.parent = this;
	this.instance.setTransform(-54.5,-79.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol2, new cjs.Rectangle(-54.5,-79.5,109,159), null);


(lib.Symbol_9_Layer_2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(12,1,1).p("Ag7CXQhWiKAyhSQAxhRCgAA");
	this.shape.setTransform(159.6059,55.525);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(12,1,1).p("Ag+CbQhOiGAzhSQAzhRCUgM");
	this.shape_1.setTransform(159.9879,55.05);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(12,1,1).p("AhCCgQhFiEA0hRQA0hRCJgZ");
	this.shape_2.setTransform(160.3657,54.55);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(12,1,1).p("AhGClQg9iCA2hRQA2hQB+gm");
	this.shape_3.setTransform(160.7072,54.075);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(12,1,1).p("AhJCqQg0iAA3hQQA3hQBzgz");
	this.shape_4.setTransform(161.043,53.575);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(12,1,1).p("AhKCTQg6iBBAhKQBAhKBrgQ");
	this.shape_5.setTransform(161.1713,55.925);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(12,1,1).p("AhMB+Qg/iBBJhEQBIhFBkAU");
	this.shape_6.setTransform(161.3158,58.0402);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(12,1,1).p("AhNBxQhEiBBSg/QBRg/BbA4");
	this.shape_7.setTransform(161.4161,59.3018);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(12,1,1).p("AhOBpQhKiCBbg5QBag5BTBc");
	this.shape_8.setTransform(161.5406,60.1347);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(12,1,1).p("AhLBuQhMiCBUg+QBTg9BgBN");
	this.shape_9.setTransform(161.245,59.5969);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(12,1,1).p("AhIB0QhOiEBOhCQBMhBBsA+");
	this.shape_10.setTransform(160.9437,59.005);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(12,1,1).p("AhFB7QhQiFBHhGQBGhFB5Au");
	this.shape_11.setTransform(160.6488,58.33);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(12,1,1).p("AhCCCQhSiGBAhKQA/hJCGAe");
	this.shape_12.setTransform(160.3144,57.5506);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(12,1,1).p("Ag+CMQhUiJA5hNQA4hNCSAP");
	this.shape_13.setTransform(159.9729,56.6451);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},42).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape}]},1).wait(46));
	this.timeline.addTween(cjs.Tween.get(this.shape).wait(42).to({_off:true},1).wait(13).to({_off:false},0).to({_off:true},1).wait(13).to({_off:false},0).to({_off:true},1).wait(13).to({_off:false},0).wait(46));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_9_Layer_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(12,1,1).p("ABWgDQipiMgCDL");
	this.shape.setTransform(164.975,59.0512);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(12,1,1).p("AhVA8QAIi/CjBy");
	this.shape_1.setTransform(165.025,59.0648);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(12,1,1).p("AhWA9QAPi0CeBY");
	this.shape_2.setTransform(165.075,59.0219);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(12,1,1).p("AhWA+QAVipCYA/");
	this.shape_3.setTransform(165.1,58.9116);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(12,1,1).p("ABYg4QiTglgcCd");
	this.shape_4.setTransform(165.15,58.6913);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(12,1,1).p("AhUBNQgDi5CsAn");
	this.shape_5.setTransform(164.8715,57.3877);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(12,1,1).p("AhPBaQgjjVDHAo");
	this.shape_6.setTransform(164.3574,56.0784);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(12,1,1).p("AhHBoQhDjyDhAq");
	this.shape_7.setTransform(163.584,54.7405);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(12,1,1).p("ABbhuQj8grBjEO");
	this.shape_8.setTransform(162.6808,53.4242);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(12,1,1).p("AhDBqQhSkDDuA8");
	this.shape_9.setTransform(163.176,54.5375);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(12,1,1).p("AhHBfQhCj3DgBL");
	this.shape_10.setTransform(163.6366,55.5703);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(12,1,1).p("AhMBWQgwjtDSBc");
	this.shape_11.setTransform(164.0718,56.5406);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(12,1,1).p("AhQBNQgfjiDEBs");
	this.shape_12.setTransform(164.4579,57.4315);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(12,1,1).p("AhTBEQgPjWC3B8");
	this.shape_13.setTransform(164.7683,58.2721);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},42).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape}]},1).wait(46));
	this.timeline.addTween(cjs.Tween.get(this.shape).wait(42).to({_off:true},1).wait(13).to({_off:false},0).to({_off:true},1).wait(13).to({_off:false},0).to({_off:true},1).wait(13).to({_off:false},0).wait(46));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_5_Layer_3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_3
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(12.5,1,1).p("AjxhOQAKBHBBAqQA7AnBWAFQBUAEBHghQBNgjAfhB");
	this.shape.setTransform(32.85,46.2219);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(12.5,1,1).p("Aj6hKQATBFBBAnQA9AkBVAFQBUAEBHgeQBNggAnhB");
	this.shape_1.setTransform(32.8,46.0293);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(12.5,1,1).p("AkChHQAcBDBBAkQA9AjBWAEQBTAEBIgbQBLgdAvhD");
	this.shape_2.setTransform(32.75,45.8504);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(12.5,1,1).p("AkJhEQAkBCBAAhQA/AgBVAFQBUAEBHgZQBLgbA2hD");
	this.shape_3.setTransform(32.7,45.7026);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(12.5,1,1).p("AkQhCQArBBBAAgQA/AeBVAFQBUAEBIgXQBKgZA8hD");
	this.shape_4.setTransform(32.675,45.5612);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(12.5,1,1).p("AkVg/QAwA/BBAeQA/AdBVAFQBUAEBIgWQBJgXBBhD");
	this.shape_5.setTransform(32.65,45.4382);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(12.5,1,1).p("AkZg+QA0A+BBAdQBAAcBVAFQBUAEBHgVQBJgVBFhE");
	this.shape_6.setTransform(32.625,45.3647);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(12.5,1,1).p("Akdg8QA4A9BBAcQBAAbBVAEQBUAFBIgUQBJgUBIhE");
	this.shape_7.setTransform(32.6,45.2672);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(12.5,1,1).p("Akfg7QA7A9BAAaQBBAaBVAFQBUAFBIgTQBIgUBKhE");
	this.shape_8.setTransform(32.575,45.2181);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(12.5,1,1).p("Akgg7QA8A9BAAaQBBAaBVAFQBUAEBIgTQBIgTBLhE");
	this.shape_9.setTransform(32.575,45.2181);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(12.5,1,1).p("Akhg6QA9A8BAAaQBBAZBVAFQBUAFBIgTQBIgSBMhF");
	this.shape_10.setTransform(32.575,45.1885);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(12.5,1,1).p("Akcg8QA4A9BAAcQBAAbBVAFQBUAEBIgUQBJgUBHhE");
	this.shape_11.setTransform(32.6,45.2913);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(12.5,1,1).p("AkXg+QAyA+BBAeQA/AcBVAFQBUAEBIgVQBJgWBDhE");
	this.shape_12.setTransform(32.625,45.3889);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(12.5,1,1).p("AkThAQAuBABBAfQA+AdBWAFQBUAEBIgWQBJgYA/hE");
	this.shape_13.setTransform(32.65,45.4875);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(12.5,1,1).p("AkOhCQApBBBAAgQA/AfBVAFQBUAEBIgYQBKgZA6hD");
	this.shape_14.setTransform(32.675,45.5855);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(12.5,1,1).p("AkJhEQAjBCBBAiQA+AgBVAFQBUAEBIgaQBKgbA2hC");
	this.shape_15.setTransform(32.725,45.7271);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(12.5,1,1).p("AkEhGQAeBDBBAjQA9AiBWAFQBTAEBIgbQBLgdAxhC");
	this.shape_16.setTransform(32.75,45.8258);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(12.5,1,1).p("Aj/hIQAZBEBBAlQA9AjBVAFQBUAEBHgdQBMgeAshC");
	this.shape_17.setTransform(32.775,45.9246);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#000000").ss(12.5,1,1).p("Aj7hKQAUBFBBAnQA9AkBVAFQBUAEBHgeQBNggAohB");
	this.shape_18.setTransform(32.8,46.0239);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#000000").ss(12.5,1,1).p("Aj2hMQAPBGBBAoQA8AmBWAFQBTAEBIgfQBMgiAkhB");
	this.shape_19.setTransform(32.825,46.1229);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},49).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_10}]},30).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape}]},1).wait(31));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_5_Layer_2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(12.5,1,1).p("AjVhhQgFBKA/A4QA6A1BWAKQBVAJA+gpQBFgvAKhb");
	this.shape.setTransform(76.8818,3.2135);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(12.5,1,1).p("AjVhaQgFBFA/A0QA6AxBWAJQBVAJA+gnQBFgrAKhU");
	this.shape_1.setTransform(76.8818,2.2844);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(12.5,1,1).p("AjVhUQgFBAA/AxQA6AtBWAJQBVAIA+gkQBFgoAKhP");
	this.shape_2.setTransform(76.8818,1.4562);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(12.5,1,1).p("AjVhOQgFA8A/AtQA6ArBWAIQBVAHA+ghQBFgmAKhJ");
	this.shape_3.setTransform(76.8818,0.7436);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(12.5,1,1).p("AjVhJQgFA3A/ArQA6AoBWAIQBVAHA+ggQBFgjAKhF");
	this.shape_4.setTransform(76.8818,0.1136);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(12.5,1,1).p("AjVhFQgFA1A/AoQA6AmBWAHQBVAHA+geQBFghAKhC");
	this.shape_5.setTransform(76.8818,-0.4338);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(12.5,1,1).p("AjVhCQgFAzA/AmQA6AkBWAHQBVAGA+gcQBFggAKg+");
	this.shape_6.setTransform(76.8818,-0.8646);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(12.5,1,1).p("AjVg/QgFAwA/AlQA6AjBWAGQBVAHA+gcQBFgeAKg8");
	this.shape_7.setTransform(76.8818,-1.2129);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(12.5,1,1).p("AjVg+QgFAvA/AkQA6AiBWAGQBVAGA+gaQBFgeAKg6");
	this.shape_8.setTransform(76.8818,-1.4446);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(12.5,1,1).p("AjVg9QgFAvA/AjQA6AhBWAGQBVAGA+gaQBFgdAKg5");
	this.shape_9.setTransform(76.8818,-1.5946);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(12.5,1,1).p("AjVg8QgFAuA/AjQA6AhBWAGQBVAGA+gaQBFgdAKg5");
	this.shape_10.setTransform(76.8818,-1.6437);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(12.5,1,1).p("AjVhAQgFAxA/AlQA6AjBWAHQBVAGA+gbQBFgfAKg8");
	this.shape_11.setTransform(76.8818,-1.1629);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(12.5,1,1).p("AjVhDQgFAzA/AnQA6AlBWAHQBVAHA+gdQBFghAKg/");
	this.shape_12.setTransform(76.8818,-0.6655);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(12.5,1,1).p("AjVhHQgFA2A/AqQA6AmBWAIQBVAHA+gfQBFgiAKhD");
	this.shape_13.setTransform(76.8818,-0.1847);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(12.5,1,1).p("AjVhLQgFA5A/AsQA6AoBWAIQBVAHA+ggQBFgkAKhG");
	this.shape_14.setTransform(76.8818,0.3128);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(12.5,1,1).p("AjVhOQgFA8A/AtQA6ArBWAIQBVAIA+giQBFgmAKhJ");
	this.shape_15.setTransform(76.8818,0.7936);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(12.5,1,1).p("AjVhSQgFA+A/AwQA6AtBWAIQBVAJA+gkQBFgnAKhN");
	this.shape_16.setTransform(76.8818,1.2571);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(12.5,1,1).p("AjVhWQgFBBA/AyQA6AvBWAJQBVAIA+glQBFgpAKhQ");
	this.shape_17.setTransform(76.8818,1.7544);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#000000").ss(12.5,1,1).p("AjVhZQgFBEA/A0QA6AxBWAJQBVAJA+gnQBFgrAKhU");
	this.shape_18.setTransform(76.8818,2.2353);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#000000").ss(12.5,1,1).p("AjVhdQgFBHA/A2QA6AzBWAJQBVAJA+goQBFgsAKhY");
	this.shape_19.setTransform(76.8818,2.7327);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},49).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_10}]},30).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape}]},1).wait(31));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_5_Layer_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(12.5,1,1).p("AjVhhQgFBKA/A4QA6A1BWAKQBVAJA+gpQBFgvAKhb");
	this.shape.setTransform(0.0318,0.0135);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(12.5,1,1).p("AjVhaQgFBFA/A0QA6AxBWAJQBVAJA+gnQBFgrAKhU");
	this.shape_1.setTransform(0.0318,-0.6897);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(12.5,1,1).p("AjVhTQgFA/A/AxQA6AuBWAIQBVAIA+gkQBFgoAKhO");
	this.shape_2.setTransform(0.0318,-1.3023);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(12.5,1,1).p("AjVhOQgFA8A/AtQA6ArBWAIQBVAHA+ghQBFgmAKhJ");
	this.shape_3.setTransform(0.0318,-1.8564);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(12.5,1,1).p("AjVhJQgFA4A/AqQA6AoBWAIQBVAHA+gfQBFgkAKhF");
	this.shape_4.setTransform(0.0318,-2.3279);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(12.5,1,1).p("AjVhFQgFA1A/AoQA6AmBWAHQBVAHA+geQBFghAKhC");
	this.shape_5.setTransform(0.0318,-2.7338);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(12.5,1,1).p("AjVhCQgFAzA/AmQA6AkBWAHQBVAGA+gcQBFggAKg+");
	this.shape_6.setTransform(0.0318,-3.0646);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(12.5,1,1).p("AjVg/QgFAwA/AlQA6AjBWAGQBVAHA+gcQBFgeAKg8");
	this.shape_7.setTransform(0.0318,-3.3129);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(12.5,1,1).p("AjVg+QgFAvA/AkQA6AiBWAGQBVAGA+gaQBFgeAKg6");
	this.shape_8.setTransform(0.0318,-3.4946);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(12.5,1,1).p("AjVg8QgFAuA/AjQA6AhBWAGQBVAHA+gbQBFgdAKg5");
	this.shape_9.setTransform(0.0318,-3.611);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(12.5,1,1).p("AjVg8QgFAuA/AjQA6AhBWAGQBVAGA+gaQBFgdAKg5");
	this.shape_10.setTransform(0.0318,-3.6437);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(12.5,1,1).p("AjVhAQgFAxA/AlQA6AjBWAHQBVAGA+gcQBFgeAKg8");
	this.shape_11.setTransform(0.0318,-3.2713);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(12.5,1,1).p("AjVhDQgFAzA/AoQA6AkBWAHQBVAHA+gdQBFghAKg/");
	this.shape_12.setTransform(0.0318,-2.9155);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(12.5,1,1).p("AjVhHQgFA2A/ApQA6AnBWAIQBVAHA+gfQBFgiAKhD");
	this.shape_13.setTransform(0.0318,-2.5355);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(12.5,1,1).p("AjVhLQgFA5A/AsQA6ApBWAHQBVAIA+ghQBFgkAKhG");
	this.shape_14.setTransform(0.0318,-2.1797);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(12.5,1,1).p("AjVhOQgFA8A/AtQA6ArBWAIQBVAIA+giQBFgmAKhJ");
	this.shape_15.setTransform(0.0318,-1.8064);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(12.5,1,1).p("AjVhSQgFA/A/AwQA6AsBWAJQBVAIA+gjQBFgoAKhN");
	this.shape_16.setTransform(0.0318,-1.4506);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(12.5,1,1).p("AjVhWQgFBBA/AzQA6AuBWAJQBVAIA+gkQBFgqAKhQ");
	this.shape_17.setTransform(0.0318,-1.0947);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#000000").ss(12.5,1,1).p("AjVhZQgFBEA/A0QA6AxBWAJQBVAJA+gnQBFgrAKhU");
	this.shape_18.setTransform(0.0318,-0.7147);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#000000").ss(12.5,1,1).p("AjVhdQgFBHA/A2QA6AzBWAJQBVAKA+goQBFgtAKhY");
	this.shape_19.setTransform(0.0318,-0.3588);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},49).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_10}]},30).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape}]},1).wait(31));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_4_Layer_6 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_6
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(12.5,1,1).p("Ag/AbIB/g1");
	this.shape.setTransform(26.075,26.75);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(12.5,1,1).p("Ag+AaIB9gz");
	this.shape_1.setTransform(2.275,48.075);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(12.5,1,1).p("Ag+AYIB9gw");
	this.shape_2.setTransform(1.525,48.2);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(12.5,1,1).p("Ag9AXIB7gt");
	this.shape_3.setTransform(0.725,48.275);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(12.5,1,1).p("Ag9AWIB7gr");
	this.shape_4.setTransform(-0.025,48.35);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(12.5,1,1).p("Ag8AVIB5gp");
	this.shape_5.setTransform(-0.825,48.425);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(12.5,1,1).p("Ag8AUIB5gm");
	this.shape_6.setTransform(-1.575,48.55);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(12.5,1,1).p("Ag7ASIB3gj");
	this.shape_7.setTransform(-2.375,48.625);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(12.5,1,1).p("Ag7AWIB3gq");
	this.shape_8.setTransform(4.725,43.3);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(12.5,1,1).p("Ag7AZIB3gw");
	this.shape_9.setTransform(11.025,38.6);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(12.5,1,1).p("Ag7AbIB3g1");
	this.shape_10.setTransform(16.475,34.525);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(12.5,1,1).p("Ag7AdIB3g5");
	this.shape_11.setTransform(21.075,31.1);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(12.5,1,1).p("Ag7AfIB3g9");
	this.shape_12.setTransform(24.825,28.275);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(12.5,1,1).p("Ag7AhIB3hB");
	this.shape_13.setTransform(27.775,26.075);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(12.5,1,1).p("Ag7AiIB3hD");
	this.shape_14.setTransform(29.875,24.5);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(12.5,1,1).p("AA8ghIh3BD");
	this.shape_15.setTransform(31.525,23.25);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},9).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},3).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_7}]},2).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14,p:{x:29.875,y:24.5}}]},1).to({state:[{t:this.shape_14,p:{x:31.125,y:23.575}}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_15}]},34).to({state:[{t:this.shape_14,p:{x:31.125,y:23.575}}]},1).to({state:[{t:this.shape_14,p:{x:29.875,y:24.5}}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_7}]},3).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).wait(26));
	this.timeline.addTween(cjs.Tween.get(this.shape).wait(10).to({x:25.725,y:27.05},0).wait(1).to({x:24.575,y:28.1},0).wait(1).to({x:22.375,y:30.15},0).wait(1).to({x:18.975,y:33.3},0).wait(1).to({x:14.575,y:37.4},0).wait(1).to({x:10.175,y:41.45},0).wait(1).to({x:6.775,y:44.6},0).wait(1).to({x:4.575,y:46.65},0).wait(1).to({x:3.425,y:47.7},0).wait(1).to({x:3.075,y:48},0).wait(3).to({_off:true},1).wait(70).to({_off:false},0).wait(2).to({x:7.425,y:43.95},0).wait(1).to({x:11.375,y:40.35},0).wait(1).to({x:14.825,y:37.15},0).wait(1).to({x:17.775,y:34.4},0).wait(1).to({x:20.325,y:32.05},0).wait(1).to({x:22.375,y:30.15},0).wait(1).to({x:24.025,y:28.65},0).wait(1).to({x:25.175,y:27.6},0).wait(1).to({x:25.825,y:26.95},0).wait(1).to({x:26.075,y:26.75},0).wait(26));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_4_Layer_5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_5
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(12.5,1,1).p("AgrCKQCqjNiihG");
	this.shape.setTransform(26.356,12.825);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(12.5,1,1).p("AgrCKQCpjNihhG");
	this.shape_1.setTransform(26.0024,13.125);
	this.shape_1._off = true;

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(12.5,1,1).p("AgrCIQCpjIiehH");
	this.shape_2.setTransform(2.7113,34.325);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(12.5,1,1).p("AgsCGQCqjDiahI");
	this.shape_3.setTransform(2.0279,34.525);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(12.5,1,1).p("AgsCEQCqi+iXhJ");
	this.shape_4.setTransform(1.3889,34.775);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(12.5,1,1).p("AgtCCQCri4iUhL");
	this.shape_5.setTransform(0.7205,34.975);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(12.5,1,1).p("AguCAQCsiziRhM");
	this.shape_6.setTransform(0.0837,35.225);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(12.5,1,1).p("AgvB+QCtiuiOhN");
	this.shape_7.setTransform(-0.5945,35.425);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(12.5,1,1).p("AgvB8QCtipiLhO");
	this.shape_8.setTransform(-1.229,35.675);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(12.5,1,1).p("AguB+QCsiwiPhL");
	this.shape_9.setTransform(4.5781,30.975);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(12.5,1,1).p("AgtCAQCri4iShH");
	this.shape_10.setTransform(9.6943,26.85);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(12.5,1,1).p("AgtCBQCri9iVhE");
	this.shape_11.setTransform(14.1313,23.275);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(12.5,1,1).p("AgsCCQCqjCiXhB");
	this.shape_12.setTransform(17.8835,20.225);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(12.5,1,1).p("AgsCDQCqjGiZg/");
	this.shape_13.setTransform(20.9486,17.75);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(12.5,1,1).p("AgsCEQCqjJibg+");
	this.shape_14.setTransform(23.3586,15.825);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(12.5,1,1).p("AgrCEQCpjLibg8");
	this.shape_15.setTransform(25.045,14.425);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(12.5,1,1).p("AgrCFQCpjNicg8");
	this.shape_16.setTransform(26.0757,13.6);
	this.shape_16._off = true;

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},9).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_1}]},3).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_8}]},2).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_16}]},34).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_8}]},3).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape}]},1).wait(26));
	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(10).to({_off:false},0).wait(1).to({x:24.8524,y:14.175},0).wait(1).to({x:22.6524,y:16.225},0).wait(1).to({x:19.2524,y:19.375},0).wait(1).to({x:14.8524,y:23.475},0).wait(1).to({x:10.4524,y:27.525},0).wait(1).to({x:7.0524,y:30.675},0).wait(1).to({x:4.8524,y:32.725},0).wait(1).to({x:3.7024,y:33.775},0).wait(1).to({x:3.3524,y:34.075},0).wait(3).to({_off:true},1).wait(70).to({_off:false},0).wait(2).to({x:7.7024,y:30.025},0).wait(1).to({x:11.6524,y:26.425},0).wait(1).to({x:15.1024,y:23.225},0).wait(1).to({x:18.0524,y:20.475},0).wait(1).to({x:20.6024,y:18.125},0).wait(1).to({x:22.6524,y:16.225},0).wait(1).to({x:24.3024,y:14.725},0).wait(1).to({x:25.4524,y:13.675},0).wait(1).to({x:26.1024,y:13.025},0).to({_off:true},1).wait(26));
	this.timeline.addTween(cjs.Tween.get(this.shape_16).wait(39).to({_off:false},0).wait(1).to({x:26.4225,y:13.325},0).wait(35).to({x:26.0757,y:13.6},0).to({_off:true},1).wait(54));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_4_Layer_4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_4
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(12.5,1,1).p("ABCBMQgGkKh9DJ");
	this.shape.setTransform(12.025,16.2511);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(12.5,1,1).p("AhBAMQB9jKAGEK");
	this.shape_1.setTransform(11.675,16.5457);
	this.shape_1._off = true;

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(12.5,1,1).p("ABCBMQgGkKh9DK");
	this.shape_2.setTransform(-10.975,37.4957);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(12.5,1,1).p("AhAAKQB+jFADEG");
	this.shape_3.setTransform(-11.475,37.5775);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(12.5,1,1).p("Ag/AIQCAjBgBEE");
	this.shape_4.setTransform(-11.9746,37.6846);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(12.5,1,1).p("Ag9AGQCAi8gFEA");
	this.shape_5.setTransform(-12.4657,37.7659);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(12.5,1,1).p("Ag8AFQCBi4gJD8");
	this.shape_6.setTransform(-12.946,37.8725);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(12.5,1,1).p("Ag7ADQCDizgND4");
	this.shape_7.setTransform(-13.4165,37.9532);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(12.5,1,1).p("Ag6ABQCEivgRD2");
	this.shape_8.setTransform(-13.8839,38.0592);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(12.5,1,1).p("AA3BHQAVjxiFCq");
	this.shape_9.setTransform(-14.339,38.1392);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(12.5,1,1).p("Ag9gIQCIikgPD3");
	this.shape_10.setTransform(-9.076,33.2979);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(12.5,1,1).p("AhAgQQCLiegKD8");
	this.shape_11.setTransform(-4.4167,29.0248);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(12.5,1,1).p("AhEgXQCOiYgGEA");
	this.shape_12.setTransform(-0.3898,25.2971);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(12.5,1,1).p("AhHgdQCQiTgCED");
	this.shape_13.setTransform(3.0508,22.1671);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(12.5,1,1).p("AhJghQCRiQACEG");
	this.shape_14.setTransform(5.875,19.5921);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(12.5,1,1).p("AhLglQCTiMADEI");
	this.shape_15.setTransform(8.05,17.5909);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(12.5,1,1).p("AhMgnQCUiKAFEJ");
	this.shape_16.setTransform(9.625,16.1431);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(12.5,1,1).p("AhNgpQCUiJAHEK");
	this.shape_17.setTransform(10.55,15.2771);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#000000").ss(12.5,1,1).p("ABOBZQgHkLiUCJ");
	this.shape_18.setTransform(10.875,14.999);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},9).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_2}]},3).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_9}]},2).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_18}]},34).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_9}]},3).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape}]},1).wait(26));
	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(10).to({_off:false},0).wait(1).to({x:10.525,y:17.5957},0).wait(1).to({x:8.325,y:19.6457},0).wait(1).to({x:4.925,y:22.7957},0).wait(1).to({x:0.525,y:26.8957},0).wait(1).to({x:-3.875,y:30.9457},0).wait(1).to({x:-7.275,y:34.0957},0).wait(1).to({x:-9.475,y:36.1457},0).wait(1).to({x:-10.625,y:37.1957},0).to({_off:true},1).wait(76).to({_off:false,x:-6.625,y:33.4457},0).wait(1).to({x:-2.675,y:29.8457},0).wait(1).to({x:0.775,y:26.6457},0).wait(1).to({x:3.725,y:23.8957},0).wait(1).to({x:6.275,y:21.5457},0).wait(1).to({x:8.325,y:19.6457},0).wait(1).to({x:9.975,y:18.1457},0).wait(1).to({x:11.125,y:17.0957},0).wait(1).to({x:11.775,y:16.4457},0).to({_off:true},1).wait(26));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_4_Layer_2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(12,1,1).p("Am3llQANDBA6CYQA+ChBpBdQDwDWGRi4");
	this.shape.setTransform(64.175,65.9033);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(12,1,1).p("Am5ljQAODAA7CYQA+CgBqBdQDwDVGSi4");
	this.shape_1.setTransform(64.05,66.0694);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(12,1,1).p("Am9lfQAPC/A9CWQBBCfBsBbQDxDSGRi5");
	this.shape_2.setTransform(63.575,66.6628);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(12,1,1).p("AnGlWQASC8BCCUQBFCbByBZQDxDKGRi6");
	this.shape_3.setTransform(62.675,67.7832);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(12,1,1).p("AnUlIQAXC3BJCPQBMCWB7BUQDxC/GSi7");
	this.shape_4.setTransform(61.3,69.5505);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(12,1,1).p("Anmk3QAdCxBTCJQBVCRCEBPQDyCwGSi9");
	this.shape_5.setTransform(59.525,71.8366);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(12,1,1).p("An4kmQAjCrBcCDQBeCLCPBJQDzCjGSjA");
	this.shape_6.setTransform(57.725,74.1297);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(12,1,1).p("AoHkYQApClBjB/QBlCGCXBFQD0CXGTjA");
	this.shape_7.setTransform(56.35,75.9361);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(12,1,1).p("AoQkQQAtCjBnB8QBpCDCeBCQDzCQGTjC");
	this.shape_8.setTransform(55.45,77.097);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(12,1,1).p("AoUkMQAuChBpB7QBsCBCgBAQD0CNGSjC");
	this.shape_9.setTransform(54.975,77.7129);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(12,1,1).p("AoWkKQAuCgBrB6QBsCBChBAQD0CMGTjD");
	this.shape_10.setTransform(54.85,77.8882);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(12,1,1).p("AoZkKQAvCgBnB4QBrCACvBFQD2CFGNi7");
	this.shape_11.setTransform(54.475,77.6096);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(12,1,1).p("AodkJQAwCfBkB2QBpCAC+BJQD5B/GHi1");
	this.shape_12.setTransform(54.125,77.3597);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(12,1,1).p("AohkIQAxCfBhB0QBnB+DMBOQD8B5GCiu");
	this.shape_13.setTransform(53.75,77.0815);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(12,1,1).p("AokkHQAwCeBfByQBlB+DcBSQD9BzF8in");
	this.shape_14.setTransform(53.4,76.8036);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(12,1,1).p("AookGQAyCeBbBwQBkB8DpBYQEABsF3ig");
	this.shape_15.setTransform(53.025,76.5261);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(12,1,1).p("AorkFQAyCdBYBuQBiB8D4BbQEDBnFwia");
	this.shape_16.setTransform(52.675,76.277);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(12,1,1).p("AovkEQAzCcBVBsQBhB7EGBgQEFBhFriT");
	this.shape_17.setTransform(52.3,75.9985);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#000000").ss(12,1,1).p("AoWkYQArCkBPB1QBaCDDlBgQEBB5Fzib");
	this.shape_18.setTransform(54.8,73.815);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#000000").ss(12,1,1).p("AoAkqQAkCrBLB+QBTCKDIBfQD9COF6ih");
	this.shape_19.setTransform(56.975,71.9203);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#000000").ss(12,1,1).p("Ansk5QAeCxBGCEQBMCQCvBfQD6ChGBin");
	this.shape_20.setTransform(58.9,70.3139);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#000000").ss(12,1,1).p("AnclGQAYC1BDCLQBICVCaBeQD2CyGGit");
	this.shape_21.setTransform(60.525,68.9759);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#000000").ss(12,1,1).p("AnPlRQAVC5A/CQQBFCZCIBeQD0C/GKix");
	this.shape_22.setTransform(61.825,67.8634);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#000000").ss(12,1,1).p("AnFlaQARC9A+CTQBBCdB7BdQDyDJGOi0");
	this.shape_23.setTransform(62.85,67.002);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#000000").ss(12,1,1).p("Am9lgQAPDAA8CWQA/CfBxBdQDxDQGPi2");
	this.shape_24.setTransform(63.575,66.3717);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f().s("#000000").ss(12,1,1).p("Am5ljQAODAA7CYQA+CgBqBdQDxDVGRi4");
	this.shape_25.setTransform(64.025,66.0265);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f().s("#000000").ss(12,1,1).p("AoEkbQAoCnBhB/QBkCHCWBGQDzCZGTjA");
	this.shape_26.setTransform(56.625,75.5549);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f().s("#000000").ss(12,1,1).p("An0kqQAiCsBaCEQBbCNCNBLQDyCmGTi/");
	this.shape_27.setTransform(58.2,73.5148);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f().s("#000000").ss(12,1,1).p("Anmk4QAeCxBSCJQBUCSCEBPQDyCxGTi9");
	this.shape_28.setTransform(59.6,71.7174);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f().s("#000000").ss(12,1,1).p("AnZlEQAZC2BLCNQBPCVB9BTQDxC7GSi8");
	this.shape_29.setTransform(60.825,70.1563);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f().s("#000000").ss(12,1,1).p("AnPlOQAVC5BHCRQBJCZB3BVQDxDEGSi7");
	this.shape_30.setTransform(61.85,68.8527);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f().s("#000000").ss(12,1,1).p("AnAlcQAQC+A/CVQBCCeBuBbQDwDPGSi5");
	this.shape_31.setTransform(63.325,66.9654);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f().s("#000000").ss(12,1,1).p("Am7lhQAPDAA8CXQA/CfBsBcQDwDTGRi4");
	this.shape_32.setTransform(63.8,66.3784);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f().s("#000000").ss(12,1,1).p("Am4lkQANDBA7CYQA+CgBqBdQDwDVGRi4");
	this.shape_33.setTransform(64.075,66.014);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},9).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_10}]},3).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_17}]},2).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},34).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_17}]},3).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_27}]},1).to({state:[{t:this.shape_28}]},1).to({state:[{t:this.shape_29}]},1).to({state:[{t:this.shape_30}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_31}]},1).to({state:[{t:this.shape_32}]},1).to({state:[{t:this.shape_33}]},1).to({state:[{t:this.shape}]},1).wait(26));
	this.timeline.addTween(cjs.Tween.get(this.shape).wait(9).to({_off:true},1).wait(30).to({_off:false},0).wait(34).to({_off:true},1).wait(29).to({_off:false},0).wait(26));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_3_Layer_2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.instance = new lib.magnit();
	this.instance.parent = this;
	this.instance.setTransform(-40,-58);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol_3_Layer_2, null, null);


(lib.Symbol_1_Layer_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(12,1,1).p("Ak+n5QhNEsAdDJQAeDIBoCEQBoCECdAkQCpAmCxhl");
	this.shape.setTransform(643.2646,388.8944);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(12,1,1).p("AlKnwQhLE1AlDIQAmDJBsB+QBsB9CdAbQCoAcCqhu");
	this.shape_1.setTransform(644.6,388.0932);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(12,1,1).p("AlXnoQhIE+AtDIQAuDJBxB4QBwB3CcARQCoASCjh3");
	this.shape_2.setTransform(645.8728,387.3745);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(12,1,1).p("AljnhQhGFIA2DIQA1DIB1ByQB1BwCbAIQCoAJCdiA");
	this.shape_3.setTransform(647.1412,386.7046);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(12,1,1).p("AlunbQhEFSA9DIQA+DJB5BqQB4BqCcgBQCnAACWiK");
	this.shape_4.setTransform(648.3771,386.1005);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(12,1,1).p("Al6nVQhBFbBFDIQBFDJB+BkQB9BkCagKQCngLCQiS");
	this.shape_5.setTransform(649.5985,385.5707);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(12,1,1).p("AmFnPQg/FkBNDIQBNDJCCBeQCBBdCagUQCngUCIib");
	this.shape_6.setTransform(650.7749,385.144);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(12,1,1).p("AmRnLQg8FuBVDIQBVDICHBYQCEBWCagcQCmgeCCik");
	this.shape_7.setTransform(651.9586,384.7532);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(12,1,1).p("AmcnHQg5F3BdDIQBdDJCKBRQCJBQCagmQClgnB7iu");
	this.shape_8.setTransform(653.1006,384.4234);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(12,1,1).p("AmHnPQg+FmBODIQBODICDBdQCBBdCagVQCngVCHid");
	this.shape_9.setTransform(650.9271,385.086);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(12,1,1).p("Al0nXQhCFWBBDIQBBDJB8BnQB6BnCbgGQCngFCTiO");
	this.shape_10.setTransform(648.9874,385.839);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(12,1,1).p("AlkngQhGFJA3DIQA2DJB2BwQB0BwCcAHQCoAHCbiB");
	this.shape_11.setTransform(647.2821,386.6206);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(12,1,1).p("AlMnvQhKE2AmDIQAnDJBtB9QBsB9CdAZQCoAbCphv");
	this.shape_12.setTransform(644.7441,387.9891);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(12,1,1).p("AlEn0QhMEwAhDIQAiDJBqCBQBqCBCdAfQCpAhCthq");
	this.shape_13.setTransform(643.9399,388.4931);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(12,1,1).p("Ak/n4QhNEtAeDJQAfDIBpCEQBoCDCdAiQCpAlCwhm");
	this.shape_14.setTransform(643.4423,388.7907);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},34).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_8}]},43).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape}]},1).wait(37));

}).prototype = p = new cjs.MovieClip();


(lib.Scene_1_Layer_12 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_12
	this.shape = new cjs.Shape();
	this.shape.graphics.lf(["#B7E0F5","rgba(183,224,245,0)"],[0.831,1],0,-112.3,0,110).s().p("A+dRvMAAAgjdMA87AAAMAAAAjdg");
	this.shape.setTransform(189.025,66.55);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Scene_1_Layer_12, null, null);


(lib.Scene_1_Layer_4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_4
	this.instance = new lib.pack();
	this.instance.parent = this;
	this.instance.setTransform(70.25,228.75,0.7639,0.7639);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Scene_1_Layer_4, null, null);


(lib.Symbol5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_129 = function() {
		this.___loopingOver___ = true;
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(129).call(this.frame_129).wait(1));

	// Layer_3_obj_
	this.Layer_3 = new lib.Symbol_5_Layer_3();
	this.Layer_3.name = "Layer_3";
	this.Layer_3.parent = this;
	this.Layer_3.setTransform(32.9,46.2,1,1,0,0,0,32.9,46.2);
	this.Layer_3.depth = 0;
	this.Layer_3.isAttachedToCamera = 0
	this.Layer_3.isAttachedToMask = 0
	this.Layer_3.layerDepth = 0
	this.Layer_3.layerIndex = 0
	this.Layer_3.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_3).wait(130));

	// Layer_2_obj_
	this.Layer_2 = new lib.Symbol_5_Layer_2();
	this.Layer_2.name = "Layer_2";
	this.Layer_2.parent = this;
	this.Layer_2.setTransform(76.9,3.2,1,1,0,0,0,76.9,3.2);
	this.Layer_2.depth = 0;
	this.Layer_2.isAttachedToCamera = 0
	this.Layer_2.isAttachedToMask = 0
	this.Layer_2.layerDepth = 0
	this.Layer_2.layerIndex = 1
	this.Layer_2.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_2).wait(130));

	// Layer_1_obj_
	this.Layer_1 = new lib.Symbol_5_Layer_1();
	this.Layer_1.name = "Layer_1";
	this.Layer_1.parent = this;
	this.Layer_1.depth = 0;
	this.Layer_1.isAttachedToCamera = 0
	this.Layer_1.isAttachedToMask = 0
	this.Layer_1.layerDepth = 0
	this.Layer_1.layerIndex = 2
	this.Layer_1.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_1).wait(130));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-27.7,-16,132.29999999999998,76.4);


(lib.Symbol3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2_obj_
	this.Layer_2 = new lib.Symbol_3_Layer_2();
	this.Layer_2.name = "Layer_2";
	this.Layer_2.parent = this;
	this.Layer_2.setTransform(-1.5,0,1,1,0,0,0,-1.5,0);
	this.Layer_2.depth = 0;
	this.Layer_2.isAttachedToCamera = 0
	this.Layer_2.isAttachedToMask = 0
	this.Layer_2.layerDepth = 0
	this.Layer_2.layerIndex = 0
	this.Layer_2.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_2).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol3, new cjs.Rectangle(-40,-58,80,116), null);


(lib.Symbol_4_Layer_3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_3
	this.instance = new lib.Symbol2();
	this.instance.parent = this;
	this.instance.setTransform(-24.45,57.6,1,1,0,0,0,11.3,23.4);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(10).to({regX:0,regY:0,scaleX:0.9991,scaleY:0.9991,rotation:0.081,x:-35.9067,y:34.4534},0).wait(1).to({scaleX:0.9962,scaleY:0.9962,rotation:0.3573,x:-36.4416,y:35.3183},0).wait(1).to({scaleX:0.9906,scaleY:0.9906,rotation:0.8867,x:-37.4673,y:36.9774},0).wait(1).to({scaleX:0.9819,scaleY:0.9819,rotation:1.7113,x:-39.0684,y:39.5684},0).wait(1).to({scaleX:0.9706,scaleY:0.9706,rotation:2.7704,x:-41.13,y:42.9068},0).wait(1).to({scaleX:0.9594,scaleY:0.9594,rotation:3.8294,x:-43.1981,y:46.2573},0).wait(1).to({scaleX:0.9507,scaleY:0.9507,rotation:4.6541,x:-44.8133,y:48.8747},0).wait(1).to({scaleX:0.9451,scaleY:0.9451,rotation:5.1834,x:-45.8522,y:50.5584},0).wait(1).to({scaleX:0.9421,scaleY:0.9421,rotation:5.4597,x:-46.3953,y:51.4385},0).wait(1).to({regX:11.3,regY:23.4,scaleX:0.9413,scaleY:0.9413,rotation:5.5407,x:-38.1,y:74.6},0).wait(3).to({scaleX:0.7106,scaleY:0.7106,rotation:5.5393,x:-10.75,y:37.9},7).wait(57).to({scaleX:0.9413,scaleY:0.9413,rotation:5.5407,x:-38.1,y:74.6},7).wait(1).to({scaleX:1,scaleY:1,rotation:0,x:-24.45,y:57.6},10,cjs.Ease.get(1)).wait(26));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_4_Layer_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Symbol3();
	this.instance.parent = this;
	this.instance.setTransform(20.2,30.2,1,1,0,0,0,20.2,30.2);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(10).to({regX:-1.5,regY:0,rotation:0.0735,x:-1.8,y:0.25},0).wait(1).to({rotation:0.324,x:-2.85,y:1.3},0).wait(1).to({rotation:0.8039,x:-4.85,y:3.2},0).wait(1).to({scaleX:0.9999,scaleY:0.9999,rotation:1.5516,x:-7.95,y:6.2},0).wait(1).to({rotation:2.5118,x:-11.95,y:10.1},0).wait(1).to({scaleX:0.9998,scaleY:0.9998,rotation:3.472,x:-15.95,y:13.95},0).wait(1).to({rotation:4.2197,x:-19.1,y:17},0).wait(1).to({rotation:4.6996,x:-21.1,y:18.95},0).wait(1).to({rotation:4.9501,x:-22.1,y:19.9},0).wait(1).to({regX:20.1,regY:30.2,scaleX:0.9997,scaleY:0.9997,rotation:5.0236,x:-3.55,y:52.25},0).wait(3).to({scaleX:0.9081,scaleY:0.9081,rotation:11.2197,x:-8.75,y:51.75},7).wait(57).to({scaleX:0.9997,scaleY:0.9997,rotation:5.0236,x:-3.55,y:52.25},7).wait(1).to({regX:20.2,scaleX:1,scaleY:1,rotation:0,x:20.2,y:30.2},10,cjs.Ease.get(1)).wait(26));

}).prototype = p = new cjs.MovieClip();


(lib.Scene_1_Layer_11 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_11
	this.instance = new lib.Symbol6();
	this.instance.parent = this;
	this.instance.setTransform(187.55,498.5,1.03,1.03,0,0,0,0.1,0);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Scene_1_Layer_11, null, null);


(lib.Scene_1_Layer_9 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_9
	this.instance = new lib.Symbol5();
	this.instance.parent = this;
	this.instance.setTransform(179.3,334.55,0.7639,0.7639);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Scene_1_Layer_9, null, null);


(lib.Symbol9 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_129 = function() {
		this.___loopingOver___ = true;
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(129).call(this.frame_129).wait(1));

	// Layer_1_obj_
	this.Layer_1 = new lib.Symbol_9_Layer_1();
	this.Layer_1.name = "Layer_1";
	this.Layer_1.parent = this;
	this.Layer_1.setTransform(165,59.1,1,1,0,0,0,165,59.1);
	this.Layer_1.depth = 0;
	this.Layer_1.isAttachedToCamera = 0
	this.Layer_1.isAttachedToMask = 0
	this.Layer_1.layerDepth = 0
	this.Layer_1.layerIndex = 0
	this.Layer_1.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_1).wait(130));

	// Layer_2_obj_
	this.Layer_2 = new lib.Symbol_9_Layer_2();
	this.Layer_2.name = "Layer_2";
	this.Layer_2.parent = this;
	this.Layer_2.setTransform(159.6,55.5,1,1,0,0,0,159.6,55.5);
	this.Layer_2.depth = 0;
	this.Layer_2.isAttachedToCamera = 0
	this.Layer_2.isAttachedToMask = 0
	this.Layer_2.layerDepth = 0
	this.Layer_2.layerIndex = 1
	this.Layer_2.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_2).wait(130));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(142.2,30.6,37.70000000000002,45.99999999999999);


(lib.Symbol4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_129 = function() {
		this.___loopingOver___ = true;
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(129).call(this.frame_129).wait(1));

	// Layer_6_obj_
	this.Layer_6 = new lib.Symbol_4_Layer_6();
	this.Layer_6.name = "Layer_6";
	this.Layer_6.parent = this;
	this.Layer_6.setTransform(26.1,26.8,1,1,0,0,0,26.1,26.8);
	this.Layer_6.depth = 0;
	this.Layer_6.isAttachedToCamera = 0
	this.Layer_6.isAttachedToMask = 0
	this.Layer_6.layerDepth = 0
	this.Layer_6.layerIndex = 0
	this.Layer_6.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_6).wait(130));

	// Layer_1_obj_
	this.Layer_1 = new lib.Symbol_4_Layer_1();
	this.Layer_1.name = "Layer_1";
	this.Layer_1.parent = this;
	this.Layer_1.setTransform(-1.5,0,1,1,0,0,0,-1.5,0);
	this.Layer_1.depth = 0;
	this.Layer_1.isAttachedToCamera = 0
	this.Layer_1.isAttachedToMask = 0
	this.Layer_1.layerDepth = 0
	this.Layer_1.layerIndex = 1
	this.Layer_1.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_1).wait(10).to({regX:-15.1,regY:11.7,x:-15.1,y:11.7},0).wait(9).to({regX:-1.5,regY:0,x:-1.5,y:0},0).wait(111));

	// Layer_2_obj_
	this.Layer_2 = new lib.Symbol_4_Layer_2();
	this.Layer_2.name = "Layer_2";
	this.Layer_2.parent = this;
	this.Layer_2.setTransform(64.2,65.9,1,1,0,0,0,64.2,65.9);
	this.Layer_2.depth = 0;
	this.Layer_2.isAttachedToCamera = 0
	this.Layer_2.isAttachedToMask = 0
	this.Layer_2.layerDepth = 0
	this.Layer_2.layerIndex = 2
	this.Layer_2.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_2).wait(130));

	// Layer_3_obj_
	this.Layer_3 = new lib.Symbol_4_Layer_3();
	this.Layer_3.name = "Layer_3";
	this.Layer_3.parent = this;
	this.Layer_3.setTransform(-35.8,34.2,1,1,0,0,0,-35.8,34.2);
	this.Layer_3.depth = 0;
	this.Layer_3.isAttachedToCamera = 0
	this.Layer_3.isAttachedToMask = 0
	this.Layer_3.layerDepth = 0
	this.Layer_3.layerIndex = 3
	this.Layer_3.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_3).wait(10).to({regX:-39,regY:42.9,x:-39,y:42.9},0).wait(9).to({regX:-35.8,regY:34.2,x:-35.8,y:34.2},0).wait(111));

	// Layer_5_obj_
	this.Layer_5 = new lib.Symbol_4_Layer_5();
	this.Layer_5.name = "Layer_5";
	this.Layer_5.parent = this;
	this.Layer_5.setTransform(26.4,12.8,1,1,0,0,0,26.4,12.8);
	this.Layer_5.depth = 0;
	this.Layer_5.isAttachedToCamera = 0
	this.Layer_5.isAttachedToMask = 0
	this.Layer_5.layerDepth = 0
	this.Layer_5.layerIndex = 4
	this.Layer_5.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_5).wait(130));

	// Layer_4_obj_
	this.Layer_4 = new lib.Symbol_4_Layer_4();
	this.Layer_4.name = "Layer_4";
	this.Layer_4.parent = this;
	this.Layer_4.setTransform(12,16.3,1,1,0,0,0,12,16.3);
	this.Layer_4.depth = 0;
	this.Layer_4.isAttachedToCamera = 0
	this.Layer_4.isAttachedToMask = 0
	this.Layer_4.layerDepth = 0
	this.Layer_4.layerIndex = 5
	this.Layer_4.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_4).wait(130));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-104.8,-58,219.1,189.1);


(lib.Symbol_1_Layer_2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.instance = new lib.Symbol9();
	this.instance.parent = this;
	this.instance.setTransform(675.25,432.8,1,1,29.9992,0,0,153.8,70.6);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(34).to({regX:153.7,rotation:0,x:693.6,y:410.55},8).wait(43).to({regX:153.8,rotation:29.9992,x:675.25,y:432.8},8,cjs.Ease.get(1)).wait(37));

}).prototype = p = new cjs.MovieClip();


(lib.Scene_1_Layer_5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_5
	this.instance = new lib.Symbol4();
	this.instance.parent = this;
	this.instance.setTransform(97.55,346.6,0.7639,0.7639);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Scene_1_Layer_5, null, null);


(lib.Symbol1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_129 = function() {
		this.___loopingOver___ = true;
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(129).call(this.frame_129).wait(1));

	// Layer_1_obj_
	this.Layer_1 = new lib.Symbol_1_Layer_1();
	this.Layer_1.name = "Layer_1";
	this.Layer_1.parent = this;
	this.Layer_1.setTransform(643.3,388.9,1,1,0,0,0,643.3,388.9);
	this.Layer_1.depth = 0;
	this.Layer_1.isAttachedToCamera = 0
	this.Layer_1.isAttachedToMask = 0
	this.Layer_1.layerDepth = 0
	this.Layer_1.layerIndex = 0
	this.Layer_1.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_1).wait(130));

	// Layer_2_obj_
	this.Layer_2 = new lib.Symbol_1_Layer_2();
	this.Layer_2.name = "Layer_2";
	this.Layer_2.parent = this;
	this.Layer_2.setTransform(688.9,423.3,1,1,0,0,0,688.9,423.3);
	this.Layer_2.depth = 0;
	this.Layer_2.isAttachedToCamera = 0
	this.Layer_2.isAttachedToMask = 0
	this.Layer_2.layerDepth = 0
	this.Layer_2.layerIndex = 1
	this.Layer_2.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_2).wait(130));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(599.8,332.3,119.70000000000005,117.30000000000001);


(lib.Scene_1_Layer_7 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_7
	this.instance = new lib.Symbol1();
	this.instance.parent = this;
	this.instance.setTransform(253.5,439.15,0.7628,0.7628,0,28.3285,-151.6715,654.4,405.1);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Scene_1_Layer_7, null, null);


// stage content:
(lib.index = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	this.___GetDepth___ = function(obj) {
		var depth = obj.depth;
		var cameraObj = this.___camera___instance;
		if(cameraObj && cameraObj.depth && obj.isAttachedToCamera)
		{
			depth += depth + cameraObj.depth;
		}
		return depth;
		}
	this.___needSorting___ = function() {
		for (var i = 0; i < this.getNumChildren() - 1; i++)
		{
			var prevDepth = this.___GetDepth___(this.getChildAt(i));
			var nextDepth = this.___GetDepth___(this.getChildAt(i + 1));
			if (prevDepth < nextDepth)
				return true;
		}
		return false;
	}
	this.___sortFunction___ = function(obj1, obj2) {
		return (this.exportRoot.___GetDepth___(obj2) - this.exportRoot.___GetDepth___(obj1));
	}
	this.on('tick', function (event){
		var curTimeline = event.currentTarget;
		if (curTimeline.___needSorting___()){
			this.sortChildren(curTimeline.___sortFunction___);
		}
	});

	// Layer_9_obj_
	this.Layer_9 = new lib.Scene_1_Layer_9();
	this.Layer_9.name = "Layer_9";
	this.Layer_9.parent = this;
	this.Layer_9.setTransform(208.7,351.5,1,1,0,0,0,208.7,351.5);
	this.Layer_9.depth = 0;
	this.Layer_9.isAttachedToCamera = 0
	this.Layer_9.isAttachedToMask = 0
	this.Layer_9.layerDepth = 0
	this.Layer_9.layerIndex = 0
	this.Layer_9.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_9).wait(1));

	// Layer_7_obj_
	this.Layer_7 = new lib.Scene_1_Layer_7();
	this.Layer_7.name = "Layer_7";
	this.Layer_7.parent = this;
	this.Layer_7.setTransform(256.1,428.9,1,1,0,0,0,256.1,428.9);
	this.Layer_7.depth = 0;
	this.Layer_7.isAttachedToCamera = 0
	this.Layer_7.isAttachedToMask = 0
	this.Layer_7.layerDepth = 0
	this.Layer_7.layerIndex = 1
	this.Layer_7.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_7).wait(1));

	// Layer_4_obj_
	this.Layer_4 = new lib.Scene_1_Layer_4();
	this.Layer_4.name = "Layer_4";
	this.Layer_4.parent = this;
	this.Layer_4.setTransform(223,435.4,1,1,0,0,0,223,435.4);
	this.Layer_4.depth = 0;
	this.Layer_4.isAttachedToCamera = 0
	this.Layer_4.isAttachedToMask = 0
	this.Layer_4.layerDepth = 0
	this.Layer_4.layerIndex = 2
	this.Layer_4.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_4).wait(1));

	// Layer_5_obj_
	this.Layer_5 = new lib.Scene_1_Layer_5();
	this.Layer_5.name = "Layer_5";
	this.Layer_5.parent = this;
	this.Layer_5.setTransform(106.7,367.9,1,1,0,0,0,106.7,367.9);
	this.Layer_5.depth = 0;
	this.Layer_5.isAttachedToCamera = 0
	this.Layer_5.isAttachedToMask = 0
	this.Layer_5.layerDepth = 0
	this.Layer_5.layerIndex = 3
	this.Layer_5.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_5).wait(1));

	// Layer_12_obj_
	this.Layer_12 = new lib.Scene_1_Layer_12();
	this.Layer_12.name = "Layer_12";
	this.Layer_12.parent = this;
	this.Layer_12.setTransform(189,66.5,1,1,0,0,0,189,66.5);
	this.Layer_12.depth = 0;
	this.Layer_12.isAttachedToCamera = 0
	this.Layer_12.isAttachedToMask = 0
	this.Layer_12.layerDepth = 0
	this.Layer_12.layerIndex = 4
	this.Layer_12.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_12).wait(1));

	// Layer_11_obj_
	this.Layer_11 = new lib.Scene_1_Layer_11();
	this.Layer_11.name = "Layer_11";
	this.Layer_11.parent = this;
	this.Layer_11.setTransform(187.5,498.5,1,1,0,0,0,187.5,498.5);
	this.Layer_11.depth = 0;
	this.Layer_11.isAttachedToCamera = 0
	this.Layer_11.isAttachedToMask = 0
	this.Layer_11.layerDepth = 0
	this.Layer_11.layerIndex = 5
	this.Layer_11.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_11).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-266.3,359.1,650.3,509.19999999999993);
// library properties:
lib.properties = {
	id: '127A0EC14571894891E827B1D2DA380V',
	width: 375,
	height: 812,
	fps: 30,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: [
		{src:"assets/images/bg_vert50-2.jpg", id:"bg_vert"},
		{src:"assets/images/magnit50-2.png", id:"magnit"},
		{src:"assets/images/pack50-2.png", id:"pack"},
		{src:"assets/images/shadow150-2.png", id:"shadow1"}
	],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['127A0EC14571894891E827B1D2DA380V'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}


// Layer depth API : 

AdobeAn.Layer = new function() {
	this.getLayerZDepth = function(timeline, layerName)
	{
		if(layerName === "Camera")
		layerName = "___camera___instance";
		var script = "if(timeline." + layerName + ") timeline." + layerName + ".depth; else 0;";
		return eval(script);
	}
	this.setLayerZDepth = function(timeline, layerName, zDepth)
	{
		const MAX_zDepth = 10000;
		const MIN_zDepth = -5000;
		if(zDepth > MAX_zDepth)
			zDepth = MAX_zDepth;
		else if(zDepth < MIN_zDepth)
			zDepth = MIN_zDepth;
		if(layerName === "Camera")
		layerName = "___camera___instance";
		var script = "if(timeline." + layerName + ") timeline." + layerName + ".depth = " + zDepth + ";";
		eval(script);
	}
	this.removeLayer = function(timeline, layerName)
	{
		if(layerName === "Camera")
		layerName = "___camera___instance";
		var script = "if(timeline." + layerName + ") timeline.removeChild(timeline." + layerName + ");";
		eval(script);
	}
	this.addNewLayer = function(timeline, layerName, zDepth)
	{
		if(layerName === "Camera")
		layerName = "___camera___instance";
		zDepth = typeof zDepth !== 'undefined' ? zDepth : 0;
		var layer = new createjs.MovieClip();
		layer.name = layerName;
		layer.depth = zDepth;
		layer.layerIndex = 0;
		timeline.addChild(layer);
	}
}


})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;

$(function () {
  var canvas, stage, exportRoot, anim_container, dom_overlay_container, fnStartAnimation;
  function init() {
    canvas = document.getElementById("canvas50-2");
    anim_container = document.getElementById("animation_container50-2");
    dom_overlay_container = document.getElementById("dom_overlay_container50-2");
    var comp=AdobeAn.getComposition("127A0EC14571894891E827B1D2DA380V");
    var lib=comp.getLibrary();
    var loader = new createjs.LoadQueue(false);
    loader.addEventListener("fileload", function(evt){handleFileLoad(evt,comp)});
    loader.addEventListener("complete", function(evt){handleComplete(evt,comp)});
    var lib=comp.getLibrary();
    loader.loadManifest(lib.properties.manifest);
  }
  function handleFileLoad(evt, comp) {
    var images=comp.getImages();
    if (evt && (evt.item.type == "image")) { images[evt.item.id] = evt.result; }
  }
  function handleComplete(evt,comp) {
    //This function is always called, irrespective of the content. You can use the variable "stage" after it is created in token create_stage.
    var lib=comp.getLibrary();
    var ss=comp.getSpriteSheet();
    var queue = evt.target;
    var ssMetadata = lib.ssMetadata;
    for(i=0; i<ssMetadata.length; i++) {
      ss[ssMetadata[i].name] = new createjs.SpriteSheet( {"images": [queue.getResult(ssMetadata[i].name)], "frames": ssMetadata[i].frames} )
    }
    var preloaderDiv = document.getElementById("_preload_div_50");
    preloaderDiv.style.display = 'none';
    canvas.style.display = 'block';
    exportRoot = new lib.index();
    stage = new lib.Stage(canvas);
    //Registers the "tick" event listener.
    fnStartAnimation = function() {
      stage.addChild(exportRoot);
      createjs.Ticker.setFPS(lib.properties.fps);
      createjs.Ticker.addEventListener("tick", stage)
      stage.addEventListener("tick", handleTick)
      function getProjectionMatrix(container, totalDepth) {
        var focalLength = 528.25;
        var projectionCenter = { x : lib.properties.width/2, y : lib.properties.height/2 };
        var scale = (totalDepth + focalLength)/focalLength;
        var scaleMat = new createjs.Matrix2D;
        scaleMat.a = 1/scale;
        scaleMat.d = 1/scale;
        var projMat = new createjs.Matrix2D;
        projMat.tx = -projectionCenter.x;
        projMat.ty = -projectionCenter.y;
        projMat = projMat.prependMatrix(scaleMat);
        projMat.tx += projectionCenter.x;
        projMat.ty += projectionCenter.y;
        return projMat;
      }
      function handleTick(event) {
        var cameraInstance = exportRoot.___camera___instance;
        if(cameraInstance !== undefined && cameraInstance.pinToObject !== undefined)
        {
          cameraInstance.x = cameraInstance.pinToObject.x + cameraInstance.pinToObject.pinOffsetX;
          cameraInstance.y = cameraInstance.pinToObject.y + cameraInstance.pinToObject.pinOffsetY;
          if(cameraInstance.pinToObject.parent !== undefined && cameraInstance.pinToObject.parent.depth !== undefined)
            cameraInstance.depth = cameraInstance.pinToObject.parent.depth + cameraInstance.pinToObject.pinOffsetZ;
        }
        applyLayerZDepth(exportRoot);
      }
      function applyLayerZDepth(parent)
      {
        var cameraInstance = parent.___camera___instance;
        var focalLength = 528.25;
        var projectionCenter = { 'x' : 0, 'y' : 0};
        if(parent === exportRoot)
        {
          var stageCenter = { 'x' : lib.properties.width/2, 'y' : lib.properties.height/2 };
          projectionCenter.x = stageCenter.x;
          projectionCenter.y = stageCenter.y;
        }
        for(child in parent.children)
        {
          var layerObj = parent.children[child];
          if(layerObj == cameraInstance)
            continue;
          applyLayerZDepth(layerObj, cameraInstance);
          if(layerObj.layerDepth === undefined)
            continue;
          if(layerObj.currentFrame != layerObj.parent.currentFrame)
          {
            layerObj.gotoAndPlay(layerObj.parent.currentFrame);
          }
          var matToApply = new createjs.Matrix2D;
          var cameraMat = new createjs.Matrix2D;
          var totalDepth = layerObj.layerDepth ? layerObj.layerDepth : 0;
          var cameraDepth = 0;
          if(cameraInstance && !layerObj.isAttachedToCamera)
          {
            var mat = cameraInstance.getMatrix();
            mat.tx -= projectionCenter.x;
            mat.ty -= projectionCenter.y;
            cameraMat = mat.invert();
            cameraMat.prependTransform(projectionCenter.x, projectionCenter.y, 1, 1, 0, 0, 0, 0, 0);
            cameraMat.appendTransform(-projectionCenter.x, -projectionCenter.y, 1, 1, 0, 0, 0, 0, 0);
            if(cameraInstance.depth)
              cameraDepth = cameraInstance.depth;
          }
          if(layerObj.depth)
          {
            totalDepth = layerObj.depth;
          }
          //Offset by camera depth
          totalDepth -= cameraDepth;
          if(totalDepth < -focalLength)
          {
            matToApply.a = 0;
            matToApply.d = 0;
          }
          else
          {
            if(layerObj.layerDepth)
            {
              var sizeLockedMat = getProjectionMatrix(parent, layerObj.layerDepth);
              if(sizeLockedMat)
              {
                sizeLockedMat.invert();
                matToApply.prependMatrix(sizeLockedMat);
              }
            }
            matToApply.prependMatrix(cameraMat);
            var projMat = getProjectionMatrix(parent, totalDepth);
            if(projMat)
            {
              matToApply.prependMatrix(projMat);
            }
          }
          layerObj.transformMatrix = matToApply;
        }
      }
    }
    //Code to support hidpi screens and responsive scaling.
    function makeResponsive(isResp, respDim, isScale, scaleType) {
      var lastW, lastH, lastS=1;
      window.addEventListener('resize', resizeCanvas);
      resizeCanvas();
      function resizeCanvas() {
        var w = lib.properties.width, h = lib.properties.height;
        var iw = window.innerWidth, ih=window.innerHeight;
        var pRatio = window.devicePixelRatio || 1, xRatio=iw/w, yRatio=ih/h, sRatio=1;
        if(isResp) {
          if((respDim=='width'&&lastW==iw) || (respDim=='height'&&lastH==ih)) {
            sRatio = lastS;
          }
          else if(!isScale) {
            if(iw<w || ih<h)
              sRatio = Math.min(xRatio, yRatio);
          }
          else if(scaleType==1) {
            sRatio = Math.min(xRatio, yRatio);
          }
          else if(scaleType==2) {
            sRatio = Math.max(xRatio, yRatio);
          }
        }
        canvas.width = w*pRatio*sRatio;
        canvas.height = h*pRatio*sRatio;
        canvas.style.width = anim_container.style.width = dom_overlay_container.style.width = preloaderDiv.style.width = w*sRatio+'px';
        canvas.style.height = anim_container.style.height = dom_overlay_container.style.height = preloaderDiv.style.height = h*sRatio+'px';
        stage.scaleX = pRatio*sRatio;
        stage.scaleY = pRatio*sRatio;
        lastW = iw; lastH = ih; lastS = sRatio;
        stage.tickOnUpdate = false;
        stage.update();
        stage.tickOnUpdate = true;
      }
    }
    makeResponsive(true,'both',true,2);
    AdobeAn.compositionLoaded(lib.properties.id);
    fnStartAnimation();
  }
  init();
});