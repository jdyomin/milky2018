(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [];


// symbols:



(lib.pack = function() {
	this.initialize(img.pack);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,366,381);


(lib.pack1 = function() {
	this.initialize(img.pack1);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,366,388);


(lib.rays = function() {
	this.initialize(img.rays);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,210,218);


(lib.Vert_bg = function() {
	this.initialize(img.Vert_bg);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,375,812);// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.Symbol10 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.rays();
	this.instance.parent = this;
	this.instance.setTransform(-105,-109);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol10, new cjs.Rectangle(-105,-109,210,218), null);


(lib.Symbol8 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FD8687").s().p("AgqAqQgRgRAAgZQAAgYARgSQASgRAYAAQAZAAARARQASASAAAYQAAAZgSARQgRASgZAAQgYAAgSgSg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol8, new cjs.Rectangle(-6,-6,12,12), null);


(lib.Symbol_7_Layer_2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(10,1,1).p("ABhAeQg3gbgtgTQhcgngBBR");
	this.shape.setTransform(-6.45,-5.9644);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(10,1,1).p("AhcAcQgHhTBcAnQAtATA3Ab");
	this.shape_1.setTransform(-6.0999,-5.9567);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(10,1,1).p("AhXAdQgOhUBbAnQAtATA3Ab");
	this.shape_2.setTransform(-5.8179,-5.9516);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(10,1,1).p("AhTAdQgUhVBbAoQAtATA3Aa");
	this.shape_3.setTransform(-5.6044,-5.9466);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(10,1,1).p("AhQAeQgZhWBbAnQAtATA3Ab");
	this.shape_4.setTransform(-5.4432,-5.8917);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(10,1,1).p("AhNAeQgdhXBbAoQAtATA3Aa");
	this.shape_5.setTransform(-5.3096,-5.8369);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(10,1,1).p("AhKAfQghhYBbAoQAtATA3Aa");
	this.shape_6.setTransform(-5.2123,-5.8095);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(10,1,1).p("AhIAfQgkhYBbAnQAtATA3Ab");
	this.shape_7.setTransform(-5.1332,-5.7821);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(10,1,1).p("AhHAfQgmhYBcAnQAtATA3Ab");
	this.shape_8.setTransform(-5.0825,-5.7821);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(10,1,1).p("AhGAfQgnhYBcAnQAtATA3Ab");
	this.shape_9.setTransform(-5.0578,-5.7547);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(10,1,1).p("ABSAcQg3gbgtgTQhbgnAnBY");
	this.shape_10.setTransform(-5.0455,-5.7547);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(10,1,1).p("AhLAeQgghXBbAoQAtATA3Aa");
	this.shape_11.setTransform(-5.2396,-5.8369);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(10,1,1).p("AhUAdQgThVBbAoQAtATA3Aa");
	this.shape_12.setTransform(-5.6383,-5.9466);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(10,1,1).p("AhaAcQgKhTBcAnQAtATA3Ab");
	this.shape_13.setTransform(-5.9947,-5.9541);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(10,1,1).p("AhcAbQgGhSBbAnQAtATA3Ab");
	this.shape_14.setTransform(-6.1435,-5.9592);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(10,1,1).p("AheAbQgDhSBcAnQAtATA3Ab");
	this.shape_15.setTransform(-6.2798,-5.9618);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(10,1,1).p("AhfAbQgBhSBcAnQAtATA3Ab");
	this.shape_16.setTransform(-6.3755,-5.9618);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(10,1,1).p("AhfAaQAAhRBbAnQAtATA3Ab");
	this.shape_17.setTransform(-6.425,-5.9644);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},14).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_10}]},35).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape}]},1).wait(11));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_7_Layer_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(10,1,1).p("AhCBYQgoiHA8ghQAagOAiAQQAkARAhAq");
	this.shape.setTransform(0.013,-0.0017);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(10,1,1).p("Ag6BTQgyh+A9ggQAZgOAiAQQAlAQAgAq");
	this.shape_1.setTransform(0.1613,-0.4767);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(10,1,1).p("Ag0BOQg5h0A9ghQAZgOAiAQQAlARAgAq");
	this.shape_2.setTransform(0.2674,-0.9267);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(10,1,1).p("AguBLQhAhtA9ghQAZgOAiAQQAlARAgAq");
	this.shape_3.setTransform(0.355,-1.3017);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(10,1,1).p("AgoBHQhGhmA8ghQAagOAiAQQAkARAhAq");
	this.shape_4.setTransform(0.428,-1.6267);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(10,1,1).p("AgjBFQhMhhA9ghQAZgOAiAQQAlARAgAq");
	this.shape_5.setTransform(0.4793,-1.9017);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(10,1,1).p("AggBCQhPhcA8ggQAagOAiAQQAkAQAhAp");
	this.shape_6.setTransform(0.5178,-2.1517);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(10,1,1).p("AgcBAQhThYA8ghQAagOAiAQQAkARAhAp");
	this.shape_7.setTransform(0.5497,-2.3267);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(10,1,1).p("AgbA/QhVhWA9ggQAZgOAiAQQAlAQAgAp");
	this.shape_8.setTransform(0.5672,-2.4517);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(10,1,1).p("AgZA+QhXhUA9ghQAZgOAiAQQAlARAgAp");
	this.shape_9.setTransform(0.58,-2.5267);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(10,1,1).p("AgZA+QhXhUA9ggQAZgOAiAQQAlAQAgAp");
	this.shape_10.setTransform(0.5842,-2.5517);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(10,1,1).p("AghBDQhOheA8ggQAagOAiAQQAkAQAhAp");
	this.shape_11.setTransform(0.5036,-2.0767);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(10,1,1).p("AguBLQg/huA8ggQAagOAiAQQAkAQAhAq");
	this.shape_12.setTransform(0.3491,-1.2517);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(10,1,1).p("Ag4BRQg0h6A9ghQAZgOAiAQQAlARAgAq");
	this.shape_13.setTransform(0.1982,-0.6267);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(10,1,1).p("Ag7BUQgwh/A8ghQAagOAiAQQAkARAhAq");
	this.shape_14.setTransform(0.1382,-0.4017);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(10,1,1).p("Ag+BVQgtiCA9ghQAZgOAiAQQAlARAgAq");
	this.shape_15.setTransform(0.082,-0.2267);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(10,1,1).p("AhABXQgqiFA8ghQAagOAiAQQAkARAhAq");
	this.shape_16.setTransform(0.0482,-0.1017);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(10,1,1).p("AhBBXQgpiGA8ghQAagOAiAQQAkARAhAq");
	this.shape_17.setTransform(0.0219,-0.0267);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},14).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_10}]},35).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape}]},1).wait(11));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_6_Symbol_5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Symbol_5
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(10,1,1).p("AgkgqQBQgUgIBs");
	this.shape.setTransform(-4.7892,4.2627);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(10,1,1).p("AgtgeQBRgVAKBW");
	this.shape_1.setTransform(-3.9,3.1036);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(10,1,1).p("Ag1gUQBRgUAaBB");
	this.shape_2.setTransform(-3.1,2.0585);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(10,1,1).p("Ag8gKQBQgUApAv");
	this.shape_3.setTransform(-2.375,1.0955);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(10,1,1).p("AhDgBQBRgVA2Ag");
	this.shape_4.setTransform(-1.725,0.2322);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(10,1,1).p("AhIAFQBRgTBAAT");
	this.shape_5.setTransform(-1.2,-0.5188);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(10,1,1).p("AhMAHQBQgTBJAJ");
	this.shape_6.setTransform(-0.775,-0.7004);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(10,1,1).p("AhQAKQBRgUBQAB");
	this.shape_7.setTransform(-0.425,-0.9773);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(10,1,1).p("AhSANQBSgUBTgF");
	this.shape_8.setTransform(-0.2,-1.275);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(10,1,1).p("AhUAPQBSgUBXgI");
	this.shape_9.setTransform(-0.05,-1.45);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(10,1,1).p("AhUAPQBRgTBYgK");
	this.shape_10.setTransform(0,-1.5);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(10,1,1).p("AhLAGQBRgTBGAN");
	this.shape_11.setTransform(-0.925,-0.6272);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(10,1,1).p("Ag8gLQBRgVAoAy");
	this.shape_12.setTransform(-2.45,1.2311);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(10,1,1).p("AgwgaQBRgVAQBP");
	this.shape_13.setTransform(-3.625,2.7399);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(10,1,1).p("AgsggQBRgUAIBZ");
	this.shape_14.setTransform(-4.05,3.2848);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(10,1,1).p("AgogkQBRgVAABi");
	this.shape_15.setTransform(-4.4,3.7234);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(10,1,1).p("AgmgnQBRgVgEBo");
	this.shape_16.setTransform(-4.6156,4.0319);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(10,1,1).p("AglgpQBRgVgHBr");
	this.shape_17.setTransform(-4.7473,4.2115);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},14).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_10}]},35).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape}]},1).wait(11));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_6_Symbol_4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Symbol_4
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(10,1,1).p("AhWAtQB8h6AxAt");
	this.shape.setTransform(0,-4.2893);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(10,1,1).p("AhZAvQB8h6A3Am");
	this.shape_1.setTransform(0.275,-4.5131);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(10,1,1).p("AhbAxQB8h6A7Af");
	this.shape_2.setTransform(0.5,-4.728);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(10,1,1).p("AhdAzQB7h6BAAZ");
	this.shape_3.setTransform(0.725,-4.9285);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(10,1,1).p("AhfA1QB7h6BEAU");
	this.shape_4.setTransform(0.9,-5.1084);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(10,1,1).p("AhhA3QB8h6BHAP");
	this.shape_5.setTransform(1.05,-5.2815);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(10,1,1).p("AhiA4QB8h6BJAL");
	this.shape_6.setTransform(1.175,-5.4451);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(10,1,1).p("AhjA6QB8h6BLAI");
	this.shape_7.setTransform(1.275,-5.5744);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(10,1,1).p("AhjA7QB7h6BNAG");
	this.shape_8.setTransform(1.35,-5.664);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(10,1,1).p("AhkA7QB8h6BNAF");
	this.shape_9.setTransform(1.375,-5.7098);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(10,1,1).p("AhkA7QB7h6BOAF");
	this.shape_10.setTransform(1.4,-5.7329);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(10,1,1).p("AhhA4QB7h6BIAM");
	this.shape_11.setTransform(1.125,-5.3827);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(10,1,1).p("AhdAzQB8h6A/AZ");
	this.shape_12.setTransform(0.675,-4.8939);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(10,1,1).p("AhaAwQB8h6A5Aj");
	this.shape_13.setTransform(0.35,-4.5726);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(10,1,1).p("AhYAvQB7h6A2An");
	this.shape_14.setTransform(0.225,-4.4694);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(10,1,1).p("AhXAuQB7h6A0Aq");
	this.shape_15.setTransform(0.125,-4.3845);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(10,1,1).p("AhXAtQB8h6AzAs");
	this.shape_16.setTransform(0.05,-4.3296);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(10,1,1).p("AhWAtQB7h6AyAt");
	this.shape_17.setTransform(0.025,-4.3027);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},14).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_10}]},35).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape}]},1).wait(11));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_6_Symbol_3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Symbol_3
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(10,1,1).p("AhNgPQAvhUBsCR");
	this.shape.setTransform(0.075,1.7045);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(10,1,1).p("AhTAAQAvhTB4B2");
	this.shape_1.setTransform(0.675,0.1499);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(10,1,1).p("AhZAOQAvhTCEBe");
	this.shape_2.setTransform(1.2,-1.2834);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(10,1,1).p("AhdAWQAvhTCMBJ");
	this.shape_3.setTransform(1.675,-2.0954);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(10,1,1).p("AhiAZQAwhTCVA4");
	this.shape_4.setTransform(2.1,-2.382);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(10,1,1).p("AhlAcQAvhTCcAp");
	this.shape_5.setTransform(2.45,-2.6947);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(10,1,1).p("AhoAgQAvhUCiAc");
	this.shape_6.setTransform(2.725,-3.0234);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(10,1,1).p("AhqAjQAvhUCmAS");
	this.shape_7.setTransform(2.95,-3.333);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(10,1,1).p("AhsAlQAwhTCpAL");
	this.shape_8.setTransform(3.1,-3.5884);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(10,1,1).p("AhtAnQAwhTCrAH");
	this.shape_9.setTransform(3.2,-3.7518);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(10,1,1).p("AhtAoQAvhUCsAG");
	this.shape_10.setTransform(3.225,-3.8168);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(10,1,1).p("AhnAfQAvhUCgAh");
	this.shape_11.setTransform(2.625,-2.9014);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(10,1,1).p("AhdAWQAvhTCMBM");
	this.shape_12.setTransform(1.625,-2.0538);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(10,1,1).p("AhVAFQAvhUB8Bv");
	this.shape_13.setTransform(0.875,-0.3215);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(10,1,1).p("AhSgCQAvhUB2B7");
	this.shape_14.setTransform(0.575,0.4127);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(10,1,1).p("AhQgHQAvhVByCF");
	this.shape_15.setTransform(0.35,0.9911);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(10,1,1).p("AhPgLQAvhVBwCM");
	this.shape_16.setTransform(0.2,1.3918);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(10,1,1).p("AhOgOQAwhUBtCQ");
	this.shape_17.setTransform(0.1,1.6194);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},14).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_10}]},35).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape}]},1).wait(11));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_2_Layer_4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_4
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(10,1,1).p("AFincQBGEugrDcQgnDHh/BxQh0BoimAOQiiANiohK");
	this.shape.setTransform(125.4586,-3.7023);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(10,1,1).p("AmXGaQCiBGCogNQCrgNB1hjQB9hqAwjHQAzjXg5kv");
	this.shape_1.setTransform(123.1542,-4.4738);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(10,1,1).p("AmhGUQCdBFCugNQCwgOB1heQB7hkA3jFQA6jTgtkx");
	this.shape_2.setTransform(121.1501,-5.1213);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(10,1,1).p("AmrGQQCaBCCygNQC0gNB1haQB6heA+jGQBBjOgkky");
	this.shape_3.setTransform(119.4551,-5.718);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(10,1,1).p("Am0GMQCXBBC2gNQC2gOB2hWQB6haBEjFQBFjLgakz");
	this.shape_4.setTransform(118.0658,-6.241);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(10,1,1).p("Am8GJQCUA/C5gNQC6gNB2hUQB5hVBIjFQBKjIgTk0");
	this.shape_5.setTransform(116.9441,-6.6638);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(10,1,1).p("AnDGGQCRA+C9gNQC8gNB2hRQB4hTBNjDQBNjHgNk0");
	this.shape_6.setTransform(116.0434,-7.0123);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(10,1,1).p("AnJGEQCQA9C+gNQC+gNB3hPQB3hQBQjDQBQjFgIk1");
	this.shape_7.setTransform(115.3862,-7.3108);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(10,1,1).p("AnNGDQCOA8DAgNQC/gOB3hOQB3hNBSjEQBSjDgEk1");
	this.shape_8.setTransform(114.9117,-7.4842);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(10,1,1).p("AnQGCQCNA8DBgNQDAgOB3hNQB3hNBTjDQBUjCgDk2");
	this.shape_9.setTransform(114.6536,-7.6092);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(10,1,1).p("AHRmxQACE2hUDCQhTDDh3BMQh3BNjAANQjBANiOg7");
	this.shape_10.setTransform(114.5523,-7.6675);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(10,1,1).p("AnBGHQCSA+C8gNQC7gNB3hSQB4hUBLjDQBMjHgPk1");
	this.shape_11.setTransform(116.323,-6.8881);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(10,1,1).p("AmpGQQCaBDCxgNQCzgNB2hbQB7hfA9jFQA/jQglky");
	this.shape_12.setTransform(119.6945,-5.6444);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(10,1,1).p("AmaGYQCgBGCqgNQCtgOB1hhQB8hoAzjGQA1jWg1kw");
	this.shape_13.setTransform(122.4438,-4.6732);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(10,1,1).p("AmVGaQCjBHCngNQCrgNB0hkQB+hrAujHQAyjXg7kw");
	this.shape_14.setTransform(123.4968,-4.3494);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(10,1,1).p("AmRGdQClBIClgNQCpgOB0hlQB+huArjHQAvjahAku");
	this.shape_15.setTransform(124.3451,-4.0506);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(10,1,1).p("AmPGeQCnBJCjgNQCngOB1hmQB+hwAojHQAujbhEkv");
	this.shape_16.setTransform(124.966,-3.8767);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(10,1,1).p("AmNGfQCnBJCjgNQCmgNB0hoQB/hwAnjIQAsjchFku");
	this.shape_17.setTransform(125.3279,-3.7517);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},14).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_10}]},35).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape}]},1).wait(11));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_2_Layer_2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(10,1,1).p("AjsBaQBLA6DOh7QBog8BYhJ");
	this.shape.setTransform(-6.75,26.5043);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(10,1,1).p("AjuBUQBKA8DQh2QBpg6BahH");
	this.shape_1.setTransform(-3.15,29.3723);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(10,1,1).p("AjvBPQBIA9DShyQBqg4BbhF");
	this.shape_2.setTransform(0.05,31.9238);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(10,1,1).p("AjxBKQBHA+DVhuQBqg2BdhE");
	this.shape_3.setTransform(2.875,34.204);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(10,1,1).p("AjyBGQBGA/DWhrQBsg0BdhC");
	this.shape_4.setTransform(5.325,36.1825);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(10,1,1).p("AjzBDQBFBADXhpQBtgzBehB");
	this.shape_5.setTransform(7.425,37.8404);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(10,1,1).p("Aj0BAQBEBADZhmQBtgyBfg/");
	this.shape_6.setTransform(9.1,39.1993);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(10,1,1).p("Aj1A9QBEBBDahkQBtgxBgg/");
	this.shape_7.setTransform(10.425,40.2553);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(10,1,1).p("Aj1A7QBDBCDahjQBugwBgg/");
	this.shape_8.setTransform(11.375,41.02);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(10,1,1).p("Aj2A7QBEBBDahiQBugwBhg+");
	this.shape_9.setTransform(11.925,41.462);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(10,1,1).p("Aj2A6QBEBCDahiQBugwBhg+");
	this.shape_10.setTransform(12.125,41.628);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(10,1,1).p("Aj0BBQBFBADYhnQBtgyBfhA");
	this.shape_11.setTransform(8.525,38.7419);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(10,1,1).p("AjxBLQBIA+DUhvQBrg2BchE");
	this.shape_12.setTransform(2.5,33.8995);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(10,1,1).p("AjuBTQBJA8DRh1QBpg6BahG");
	this.shape_13.setTransform(-2.025,30.2919);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(10,1,1).p("AjtBVQBKA8DPh3QBpg7BZhH");
	this.shape_14.setTransform(-3.725,28.9167);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(10,1,1).p("AjtBXQBLA7DOh5QBpg7BZhI");
	this.shape_15.setTransform(-5.05,27.8669);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(10,1,1).p("AjsBZQBLA6DOh6QBog8BYhI");
	this.shape_16.setTransform(-6,27.1094);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape,p:{x:-6.75,y:26.5043}}]}).to({state:[{t:this.shape,p:{x:-6.75,y:26.5043}}]},14).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_10}]},35).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape,p:{x:-6.55,y:26.6678}}]},1).to({state:[{t:this.shape,p:{x:-6.75,y:26.5043}}]},1).wait(11));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_2_copy_Layer_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(10,1,1).p("AEylsQgoADhpBPQhsBRhpBtQh5B8hCBwQhOCDAOBa");
	this.shape.setTransform(-0.0065,-0.925);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(10,1,1).p("AkdF4QgShZBIiGQA9hyBziBQBkhwBohWQBlhSAogF");
	this.shape_1.setTransform(1.596,0.175);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(10,1,1).p("AkOGCQgVhYBDiJQA5h0BuiEQBeh1BlhZQBihWAogG");
	this.shape_2.setTransform(3.023,1.175);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(10,1,1).p("AkAGLQgYhXA+iLQA1h2BpiIQBbh3BihdQBfhZAngI");
	this.shape_3.setTransform(4.2651,2.025);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(10,1,1).p("Aj0GSQgahWA6iMQAxh4BmiLQBXh6BghfQBbhcAogJ");
	this.shape_4.setTransform(5.3119,2.775);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(10,1,1).p("AjpGZQgdhWA3iOQAuh5BjiOQBUh8BdhiQBZheAogK");
	this.shape_5.setTransform(6.2107,3.425);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(10,1,1).p("AjgGeQgfhWA0iOQAsh6BgiQQBSh/BbhjQBYhgAngL");
	this.shape_6.setTransform(6.9362,3.95);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(10,1,1).p("AjaGiQgfhVAxiQQArh6BdiSQBQiABahlQBWhiAngL");
	this.shape_7.setTransform(7.4875,4.35);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(10,1,1).p("AjVGlQghhVAxiRQAph7BbiTQBPiABZhnQBVhiAngM");
	this.shape_8.setTransform(7.8819,4.65);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(10,1,1).p("AjSGnQghhVAviRQAoh8BbiTQBOiCBYhnQBVhjAngM");
	this.shape_9.setTransform(8.1194,4.825);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(10,1,1).p("ADgmmQgnAMhVBjQhYBnhNCCQhbCUgoB7QgvCSAiBU");
	this.shape_10.setTransform(8.203,4.875);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(10,1,1).p("AjjGcQgehVA1iPQAth5BgiQQBTh+BchiQBYhgAngK");
	this.shape_11.setTransform(6.6944,3.775);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(10,1,1).p("AkCGJQgXhXA/iKQA1h2BqiHQBbh4BjhbQBehZAogH");
	this.shape_12.setTransform(4.0808,1.925);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(10,1,1).p("AkYF7QgThYBHiHQA7hzByiCQBhhyBohWQBjhUAogF");
	this.shape_13.setTransform(2.0983,0.525);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(10,1,1).p("AkgF2QgRhYBJiGQA+hyB0iAQBkhxBphUQBlhSApgE");
	this.shape_14.setTransform(1.3394,0);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(10,1,1).p("AknFyQgQhZBMiFQA/hxB2h+QBnhvBqhTQBnhQAogE");
	this.shape_15.setTransform(0.7601,-0.4);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(10,1,1).p("AkrFvQgPhZBNiEQBBhxB4h9QBnhuBshSQBnhOApgE");
	this.shape_16.setTransform(0.3345,-0.7);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(10,1,1).p("AkuFtQgOhZBOiEQBBhwB5h8QBohtBshRQBphPAogD");
	this.shape_17.setTransform(0.0864,-0.875);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},14).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_10}]},35).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape}]},1).wait(11));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_1_Layer_11 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_11
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(10,1,1).p("AhuAyIDdhj");
	this.shape.setTransform(59.375,4.125);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(10,1,1).p("Ah4BKIDxiT");
	this.shape_1.setTransform(60.45,1.775);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(10,1,1).p("Ah/BYID/iv");
	this.shape_2.setTransform(61.1,0.35);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(10,1,1).p("AiBBdIEDi5");
	this.shape_3.setTransform(61.325,-0.125);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(10,1,1).p("Ah+BZID9ix");
	this.shape_4.setTransform(60.975,0.25);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(10,1,1).p("Ah0BNIDpiZ");
	this.shape_5.setTransform(59.975,1.425);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(10,1,1).p("AhjA6IDHhz");
	this.shape_6.setTransform(58.25,3.325);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(10,1,1).p("AhLAfICXg9");
	this.shape_7.setTransform(55.875,6.025);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape}]},19).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_3}]},36).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[]},1).wait(17));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_1_Layer_10 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_10
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(10,1,1).p("Ah1gxIDrBj");
	this.shape.setTransform(8.6,5.025);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(10,1,1).p("AhyhJIDlCT");
	this.shape_1.setTransform(8.925,2.575);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(10,1,1).p("AhwhYIDhCx");
	this.shape_2.setTransform(9.125,1.125);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(10,1,1).p("ABwBeIjfi7");
	this.shape_3.setTransform(9.2,0.625);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(10,1,1).p("AhthZIDbCz");
	this.shape_4.setTransform(9.4,1.05);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(10,1,1).p("AhnhMIDPCZ");
	this.shape_5.setTransform(10.05,2.3);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(10,1,1).p("Ahcg3IC5Bv");
	this.shape_6.setTransform(11.1,4.375);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(10,1,1).p("ABOAbIibg1");
	this.shape_7.setTransform(12.575,7.275);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape}]},19).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_3}]},36).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[]},1).wait(17));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_1_Layer_9 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_9
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(10,1,1).p("AjGBlQAIhcA9g4QA2gzBMgCQBLgCA4AuQA+AyAGBM");
	this.shape.setTransform(67.6,-0.9792);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(10,1,1).p("AjGBdQAOhUA7g0QA0gvBLgCQBKgCA2ArQA7AtALBG");
	this.shape_1.setTransform(67.6,-0.1545);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(10,1,1).p("AjGBIQAghCAzgnQAwgkBIgCQBIgCAvAgQAzAiAZA1");
	this.shape_2.setTransform(67.6,2.069);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(10,1,1).p("AjGAuQA1grAsgXQAogXBGgCQBGgCAmATQAoAUArAf");
	this.shape_3.setTransform(67.6,4.8155);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(10,1,1).p("AjGAXQBIgWAkgLQAjgKBDgCQBEgCAfAIQAfAIA6AM");
	this.shape_4.setTransform(67.6,7.2289);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(10,1,1).p("AjGAJQBUgJAggCQAfgDBBgBQBDgCAaAAQAaAABDAB");
	this.shape_5.setTransform(67.6,8.675);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(10,1,1).p("AjGAIQBYgFAeAAQAegBBBgBQBBgBAZgCQAZgCBGgD");
	this.shape_6.setTransform(67.6,8.825);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(10,1,1).p("AjGAiQBAggAngRQAlgQBEgBQBFgCAjAMQAjAOAzAV");
	this.shape_7.setTransform(67.6,6.1362);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(10,1,1).p("AjGA6QAsg2AvgeQArgdBHgCQBHgCAqAZQAtAbAjAo");
	this.shape_8.setTransform(67.6,3.5676);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(10,1,1).p("AjGBNQAchGA1gqQAwgmBJgCQBJgCAxAiQA0AkAWA5");
	this.shape_9.setTransform(67.6,1.5944);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(10,1,1).p("AjGBaQARhSA5gxQA0guBKgCQBKgCA1ApQA6AsANBD");
	this.shape_10.setTransform(67.6,0.1453);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(10,1,1).p("AjGBiQAKhZA8g2QA2gyBLgCQBLgCA3AtQA9AwAIBK");
	this.shape_11.setTransform(67.6,-0.7043);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},14).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_6}]},41).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape}]},1).wait(13));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_1_Layer_8 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_8
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(10,1,1).p("AjHBlQAIhcA9g4QA3gzBMgCQBLgCA4AuQA+AyAGBM");
	this.shape.setTransform(0,0.0208);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(10,1,1).p("AjHBdQANhUA7g0QA1gvBJgBQBHgCA4AqQA9AtANBH");
	this.shape_1.setTransform(0.025,0.8455);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(10,1,1).p("AjHBHQAYhAA2gnQAxgkBAgCQA+gBA3AgQA7AiAgA2");
	this.shape_2.setTransform(0.075,3.0466);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(10,1,1).p("AjIAsQAngnAvgYQAsgXA1gBQA0gBA2AUQA5AUA3Ah");
	this.shape_3.setTransform(0.125,5.7698);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(10,1,1).p("AjIAUQAzgSApgKQAogLAsAAQAqgBA1AIQA3AJBLAP");
	this.shape_4.setTransform(0.175,8.1697);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(10,1,1).p("AjJAGQA8gEAlgDQAlgDAmAAQAkgBA1ACQA1ABBZAE");
	this.shape_5.setTransform(0.225,9.645);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(10,1,1).p("AjJABQA+ABAkgBQAlgBAjAAQAiAAA1AAQA1gBBdAB");
	this.shape_6.setTransform(0.225,10.0785);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(10,1,1).p("AjIAfQAtgbAsgRQAqgQAwgBQAvgBA2AOQA3AOBCAY");
	this.shape_7.setTransform(0.15,7.0466);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(10,1,1).p("AjHA4QAfgyAygfQAvgdA6gBQA5gCA3AaQA5AbAsAq");
	this.shape_8.setTransform(0.1,4.5208);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(10,1,1).p("AjHBMQAVhFA3gqQAygmBCgCQBAgBA4AiQA8AlAbA5");
	this.shape_9.setTransform(0.05,2.5718);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(10,1,1).p("AjHBaQAOhRA7gyQA0guBIgBQBFgCA5ApQA8ArAQBF");
	this.shape_10.setTransform(0.025,1.1453);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(10,1,1).p("AjHBiQAKhZA8g2QA2gyBLgCQBKgCA4AtQA9AwAJBL");
	this.shape_11.setTransform(0,0.2957);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},14).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_6}]},41).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape}]},1).wait(13));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_1_Layer_5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_5
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(10,1,1).p("AhAAzQBdAPAkh2");
	this.shape.setTransform(55.5,34.4032);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(10,1,1).p("AhAAnQBXAWAqhn");
	this.shape_1.setTransform(51.525,36.7751);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(10,1,1).p("AhAAcQBSAdAvhb");
	this.shape_2.setTransform(48.15,38.824);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(10,1,1).p("AhAASQBOAjAzhQ");
	this.shape_3.setTransform(45.4,40.5403);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(10,1,1).p("AhAALQBLAmA2hI");
	this.shape_4.setTransform(43.225,41.8897);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(10,1,1).p("AhAAFQBIApA5hC");
	this.shape_5.setTransform(41.7,42.8687);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(10,1,1).p("AhAABQBHAsA6g/");
	this.shape_6.setTransform(40.775,43.4842);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(10,1,1).p("AhAAAQBHAsA6g9");
	this.shape_7.setTransform(40.475,43.6794);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(10,1,1).p("AhAAPQBNAkA0hM");
	this.shape_8.setTransform(44.45,41.0959);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(10,1,1).p("AhAAbQBRAdAwhZ");
	this.shape_9.setTransform(47.825,38.9891);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(10,1,1).p("AhAAkQBWAYArhk");
	this.shape_10.setTransform(50.575,37.3063);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(10,1,1).p("AhAArQBZAUAohs");
	this.shape_11.setTransform(52.75,36.0306);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(10,1,1).p("AhAAwQBbARAmhy");
	this.shape_12.setTransform(54.275,35.1349);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(10,1,1).p("AhAAzQBcAPAkh1");
	this.shape_13.setTransform(55.2,34.5846);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},18).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_7}]},33).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape}]},1).wait(15));

}).prototype = p = new cjs.MovieClip();


(lib.Scene_1_Layer_8 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_8
	this.instance = new lib.Vert_bg();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Scene_1_Layer_8, null, null);


(lib.Scene_1_Layer_3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_3
	this.instance = new lib.pack();
	this.instance.parent = this;
	this.instance.setTransform(178.3,345.25,0.7841,0.7841);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Scene_1_Layer_3, null, null);


(lib.Symbol9 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.instance = new lib.Symbol10();
	this.instance.parent = this;
	this.instance.setTransform(0,-64);
	this.instance.alpha = 0;
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(9).to({_off:false},0).to({alpha:1},13).wait(35).to({alpha:0},10).to({_off:true},1).wait(12));

	// Layer_1
	this.instance_1 = new lib.pack1();
	this.instance_1.parent = this;
	this.instance_1.setTransform(-183,-194);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(80));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-183,-194,366,388);


(lib.Symbol7 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_79 = function() {
		this.___loopingOver___ = true;
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(79).call(this.frame_79).wait(1));

	// Layer_2_obj_
	this.Layer_2 = new lib.Symbol_7_Layer_2();
	this.Layer_2.name = "Layer_2";
	this.Layer_2.parent = this;
	this.Layer_2.setTransform(-6.5,-6,1,1,0,0,0,-6.5,-6);
	this.Layer_2.depth = 0;
	this.Layer_2.isAttachedToCamera = 0
	this.Layer_2.isAttachedToMask = 0
	this.Layer_2.layerDepth = 0
	this.Layer_2.layerIndex = 0
	this.Layer_2.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_2).wait(80));

	// Layer_1_obj_
	this.Layer_1 = new lib.Symbol_7_Layer_1();
	this.Layer_1.name = "Layer_1";
	this.Layer_1.parent = this;
	this.Layer_1.depth = 0;
	this.Layer_1.isAttachedToCamera = 0
	this.Layer_1.isAttachedToMask = 0
	this.Layer_1.layerDepth = 0
	this.Layer_1.layerIndex = 1
	this.Layer_1.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_1).wait(80));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-21.1,-13.9,34.400000000000006,27.700000000000003);


(lib.Symbol6 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_79 = function() {
		this.___loopingOver___ = true;
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(79).call(this.frame_79).wait(1));

	// Symbol_5_obj_
	this.Symbol_5 = new lib.Symbol_6_Symbol_5();
	this.Symbol_5.name = "Symbol_5";
	this.Symbol_5.parent = this;
	this.Symbol_5.setTransform(-4.8,4.3,1,1,0,0,0,-4.8,4.3);
	this.Symbol_5.depth = 0;
	this.Symbol_5.isAttachedToCamera = 0
	this.Symbol_5.isAttachedToMask = 0
	this.Symbol_5.layerDepth = 0
	this.Symbol_5.layerIndex = 0
	this.Symbol_5.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Symbol_5).wait(80));

	// Symbol_3_obj_
	this.Symbol_3 = new lib.Symbol_6_Symbol_3();
	this.Symbol_3.name = "Symbol_3";
	this.Symbol_3.parent = this;
	this.Symbol_3.setTransform(0.1,1.7,1,1,0,0,0,0.1,1.7);
	this.Symbol_3.depth = 0;
	this.Symbol_3.isAttachedToCamera = 0
	this.Symbol_3.isAttachedToMask = 0
	this.Symbol_3.layerDepth = 0
	this.Symbol_3.layerIndex = 1
	this.Symbol_3.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Symbol_3).wait(80));

	// Symbol_4_obj_
	this.Symbol_4 = new lib.Symbol_6_Symbol_4();
	this.Symbol_4.name = "Symbol_4";
	this.Symbol_4.parent = this;
	this.Symbol_4.setTransform(0,-4.3,1,1,0,0,0,0,-4.3);
	this.Symbol_4.depth = 0;
	this.Symbol_4.isAttachedToCamera = 0
	this.Symbol_4.isAttachedToMask = 0
	this.Symbol_4.layerDepth = 0
	this.Symbol_4.layerIndex = 2
	this.Symbol_4.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Symbol_4).wait(80));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-13.7,-16.6,32.9,30.5);


(lib.Symbol2copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_79 = function() {
		this.___loopingOver___ = true;
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(79).call(this.frame_79).wait(1));

	// Layer_1_obj_
	this.Layer_1 = new lib.Symbol_2_copy_Layer_1();
	this.Layer_1.name = "Layer_1";
	this.Layer_1.parent = this;
	this.Layer_1.setTransform(0,-1,1,1,0,0,0,0,-1);
	this.Layer_1.depth = 0;
	this.Layer_1.isAttachedToCamera = 0
	this.Layer_1.isAttachedToMask = 0
	this.Layer_1.layerDepth = 0
	this.Layer_1.layerIndex = 0
	this.Layer_1.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_1).wait(80));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-35.6,-42.4,71.2,94.6);


(lib.Symbol_2_Layer_5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_5
	this.instance = new lib.Symbol7();
	this.instance.parent = this;
	this.instance.setTransform(90.65,40.85,1,1,0,0,0,8.3,-0.1);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(14).to({rotation:-18.4919,x:70.25,y:33.5},10,cjs.Ease.get(1)).wait(35).to({rotation:0,x:90.65,y:40.85},10,cjs.Ease.get(1)).wait(11));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_2_Layer_3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_3
	this.instance = new lib.Symbol6();
	this.instance.parent = this;
	this.instance.setTransform(18.45,14.75,1,1,0,0,0,-10.7,0);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(14).to({regY:0.1,rotation:4.7483,x:35.55,y:33},10,cjs.Ease.get(1)).wait(35).to({regY:0,rotation:0,x:18.45,y:14.75},10,cjs.Ease.get(1)).wait(11));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_1_Layer_7 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_7
	this.instance = new lib.Symbol8();
	this.instance.parent = this;
	this.instance.setTransform(59.5,35.85,0.9777,0.9777,0,0,0,-6.1,-3.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(14).to({regX:-6,scaleX:0.6478,scaleY:0.6478,x:56,y:32.35},5).to({_off:true},1).wait(44).to({_off:false},0).to({regX:-6.1,scaleX:0.9777,scaleY:0.9777,x:59.5,y:35.85},5,cjs.Ease.get(1)).wait(11));

}).prototype = p = new cjs.MovieClip();


(lib.Scene_1_Layer_7 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_7
	this.instance = new lib.Symbol9();
	this.instance.parent = this;
	this.instance.setTransform(321.65,497.6,0.7841,0.7841);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Scene_1_Layer_7, null, null);


(lib.Scene_1_Layer_6 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_6
	this.instance = new lib.Symbol2copy();
	this.instance.parent = this;
	this.instance.setTransform(222.45,495.05,0.7841,0.7841);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Scene_1_Layer_6, null, null);


(lib.Symbol2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_79 = function() {
		this.___loopingOver___ = true;
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(79).call(this.frame_79).wait(1));

	// Layer_5_obj_
	this.Layer_5 = new lib.Symbol_2_Layer_5();
	this.Layer_5.name = "Layer_5";
	this.Layer_5.parent = this;
	this.Layer_5.setTransform(78.5,40.9,1,1,0,0,0,78.5,40.9);
	this.Layer_5.depth = 0;
	this.Layer_5.isAttachedToCamera = 0
	this.Layer_5.isAttachedToMask = 0
	this.Layer_5.layerDepth = 0
	this.Layer_5.layerIndex = 0
	this.Layer_5.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_5).wait(80));

	// Layer_4_obj_
	this.Layer_4 = new lib.Symbol_2_Layer_4();
	this.Layer_4.name = "Layer_4";
	this.Layer_4.parent = this;
	this.Layer_4.setTransform(125.5,-3.7,1,1,0,0,0,125.5,-3.7);
	this.Layer_4.depth = 0;
	this.Layer_4.isAttachedToCamera = 0
	this.Layer_4.isAttachedToMask = 0
	this.Layer_4.layerDepth = 0
	this.Layer_4.layerIndex = 1
	this.Layer_4.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_4).wait(80));

	// Layer_3_obj_
	this.Layer_3 = new lib.Symbol_2_Layer_3();
	this.Layer_3.name = "Layer_3";
	this.Layer_3.parent = this;
	this.Layer_3.setTransform(29.2,14.8,1,1,0,0,0,29.2,14.8);
	this.Layer_3.depth = 0;
	this.Layer_3.isAttachedToCamera = 0
	this.Layer_3.isAttachedToMask = 0
	this.Layer_3.layerDepth = 0
	this.Layer_3.layerIndex = 2
	this.Layer_3.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_3).wait(80));

	// Layer_2_obj_
	this.Layer_2 = new lib.Symbol_2_Layer_2();
	this.Layer_2.name = "Layer_2";
	this.Layer_2.parent = this;
	this.Layer_2.setTransform(-6.8,26.5,1,1,0,0,0,-6.8,26.5);
	this.Layer_2.depth = 0;
	this.Layer_2.isAttachedToCamera = 0
	this.Layer_2.isAttachedToMask = 0
	this.Layer_2.layerDepth = 0
	this.Layer_2.layerIndex = 3
	this.Layer_2.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_2).wait(80));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-35.6,-56.4,205.79999999999998,111.5);


(lib.Symbol1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_79 = function() {
		this.___loopingOver___ = true;
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(79).call(this.frame_79).wait(1));

	// Layer_11_obj_
	this.Layer_11 = new lib.Symbol_1_Layer_11();
	this.Layer_11.name = "Layer_11";
	this.Layer_11.parent = this;
	this.Layer_11.depth = 0;
	this.Layer_11.isAttachedToCamera = 0
	this.Layer_11.isAttachedToMask = 0
	this.Layer_11.layerDepth = 0
	this.Layer_11.layerIndex = 0
	this.Layer_11.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_11).wait(80));

	// Layer_10_obj_
	this.Layer_10 = new lib.Symbol_1_Layer_10();
	this.Layer_10.name = "Layer_10";
	this.Layer_10.parent = this;
	this.Layer_10.depth = 0;
	this.Layer_10.isAttachedToCamera = 0
	this.Layer_10.isAttachedToMask = 0
	this.Layer_10.layerDepth = 0
	this.Layer_10.layerIndex = 1
	this.Layer_10.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_10).wait(80));

	// Layer_9_obj_
	this.Layer_9 = new lib.Symbol_1_Layer_9();
	this.Layer_9.name = "Layer_9";
	this.Layer_9.parent = this;
	this.Layer_9.setTransform(67.6,-1,1,1,0,0,0,67.6,-1);
	this.Layer_9.depth = 0;
	this.Layer_9.isAttachedToCamera = 0
	this.Layer_9.isAttachedToMask = 0
	this.Layer_9.layerDepth = 0
	this.Layer_9.layerIndex = 2
	this.Layer_9.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_9).wait(80));

	// Layer_8_obj_
	this.Layer_8 = new lib.Symbol_1_Layer_8();
	this.Layer_8.name = "Layer_8";
	this.Layer_8.parent = this;
	this.Layer_8.depth = 0;
	this.Layer_8.isAttachedToCamera = 0
	this.Layer_8.isAttachedToMask = 0
	this.Layer_8.layerDepth = 0
	this.Layer_8.layerIndex = 3
	this.Layer_8.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_8).wait(80));

	// Layer_5_obj_
	this.Layer_5 = new lib.Symbol_1_Layer_5();
	this.Layer_5.name = "Layer_5";
	this.Layer_5.parent = this;
	this.Layer_5.setTransform(55.5,34.4,1,1,0,0,0,55.5,34.4);
	this.Layer_5.depth = 0;
	this.Layer_5.isAttachedToCamera = 0
	this.Layer_5.isAttachedToMask = 0
	this.Layer_5.layerDepth = 0
	this.Layer_5.layerIndex = 4
	this.Layer_5.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_5).wait(80));

	// Layer_7_obj_
	this.Layer_7 = new lib.Symbol_1_Layer_7();
	this.Layer_7.name = "Layer_7";
	this.Layer_7.parent = this;
	this.Layer_7.setTransform(65.4,39.3,1,1,0,0,0,65.4,39.3);
	this.Layer_7.depth = 0;
	this.Layer_7.isAttachedToCamera = 0
	this.Layer_7.isAttachedToMask = 0
	this.Layer_7.layerDepth = 0
	this.Layer_7.layerIndex = 5
	this.Layer_7.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_7).wait(80));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-24.9,-21.8,117.5,75.5);


(lib.Scene_1_Layer_5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_5
	this.instance = new lib.Symbol2();
	this.instance.parent = this;
	this.instance.setTransform(222.45,495.05,0.7841,0.7841);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Scene_1_Layer_5, null, null);


(lib.Scene_1_Layer_4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_4
	this.instance = new lib.Symbol1();
	this.instance.parent = this;
	this.instance.setTransform(252.05,431.1,0.7841,0.7841,0,0,0,0.1,0.1);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Scene_1_Layer_4, null, null);


// stage content:
(lib.Sept_02_Vert = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	this.___GetDepth___ = function(obj) {
		var depth = obj.depth;
		var cameraObj = this.___camera___instance;
		if(cameraObj && cameraObj.depth && obj.isAttachedToCamera)
		{
			depth += depth + cameraObj.depth;
		}
		return depth;
		}
	this.___needSorting___ = function() {
		for (var i = 0; i < this.getNumChildren() - 1; i++)
		{
			var prevDepth = this.___GetDepth___(this.getChildAt(i));
			var nextDepth = this.___GetDepth___(this.getChildAt(i + 1));
			if (prevDepth < nextDepth)
				return true;
		}
		return false;
	}
	this.___sortFunction___ = function(obj1, obj2) {
		return (this.exportRoot.___GetDepth___(obj2) - this.exportRoot.___GetDepth___(obj1));
	}
	this.on('tick', function (event){
		var curTimeline = event.currentTarget;
		if (curTimeline.___needSorting___()){
			this.sortChildren(curTimeline.___sortFunction___);
		}
	});

	// Layer_5_obj_
	this.Layer_5 = new lib.Scene_1_Layer_5();
	this.Layer_5.name = "Layer_5";
	this.Layer_5.parent = this;
	this.Layer_5.setTransform(275.3,494.4,1,1,0,0,0,275.3,494.4);
	this.Layer_5.depth = 0;
	this.Layer_5.isAttachedToCamera = 0
	this.Layer_5.isAttachedToMask = 0
	this.Layer_5.layerDepth = 0
	this.Layer_5.layerIndex = 0
	this.Layer_5.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_5).wait(1));

	// Layer_4_obj_
	this.Layer_4 = new lib.Scene_1_Layer_4();
	this.Layer_4.name = "Layer_4";
	this.Layer_4.parent = this;
	this.Layer_4.setTransform(278.4,442.4,1,1,0,0,0,278.4,442.4);
	this.Layer_4.depth = 0;
	this.Layer_4.isAttachedToCamera = 0
	this.Layer_4.isAttachedToMask = 0
	this.Layer_4.layerDepth = 0
	this.Layer_4.layerIndex = 1
	this.Layer_4.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_4).wait(1));

	// Layer_7_obj_
	this.Layer_7 = new lib.Scene_1_Layer_7();
	this.Layer_7.name = "Layer_7";
	this.Layer_7.parent = this;
	this.Layer_7.setTransform(321.7,497.6,1,1,0,0,0,321.7,497.6);
	this.Layer_7.depth = 0;
	this.Layer_7.isAttachedToCamera = 0
	this.Layer_7.isAttachedToMask = 0
	this.Layer_7.layerDepth = 0
	this.Layer_7.layerIndex = 2
	this.Layer_7.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_7).wait(1));

	// Layer_3_obj_
	this.Layer_3 = new lib.Scene_1_Layer_3();
	this.Layer_3.name = "Layer_3";
	this.Layer_3.parent = this;
	this.Layer_3.setTransform(321.8,494.6,1,1,0,0,0,321.8,494.6);
	this.Layer_3.depth = 0;
	this.Layer_3.isAttachedToCamera = 0
	this.Layer_3.isAttachedToMask = 0
	this.Layer_3.layerDepth = 0
	this.Layer_3.layerIndex = 3
	this.Layer_3.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_3).wait(1));

	// Layer_6_obj_
	this.Layer_6 = new lib.Scene_1_Layer_6();
	this.Layer_6.name = "Layer_6";
	this.Layer_6.parent = this;
	this.Layer_6.setTransform(222.5,494.3,1,1,0,0,0,222.5,494.3);
	this.Layer_6.depth = 0;
	this.Layer_6.isAttachedToCamera = 0
	this.Layer_6.isAttachedToMask = 0
	this.Layer_6.layerDepth = 0
	this.Layer_6.layerIndex = 4
	this.Layer_6.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_6).wait(1));

	// Layer_8_obj_
	this.Layer_8 = new lib.Scene_1_Layer_8();
	this.Layer_8.name = "Layer_8";
	this.Layer_8.parent = this;
	this.Layer_8.setTransform(187.5,406,1,1,0,0,0,187.5,406);
	this.Layer_8.depth = 0;
	this.Layer_8.isAttachedToCamera = 0
	this.Layer_8.isAttachedToMask = 0
	this.Layer_8.layerDepth = 0
	this.Layer_8.layerIndex = 5
	this.Layer_8.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_8).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-162.7,406,659.3,406);
// library properties:
lib.properties = {
	id: 'F3D0D0D08ED9DE449823998BCCD6A8D4',
	width: 375,
	height: 812,
	fps: 24,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: [
		{src:"assets/images/pack53-2.png", id:"pack"},
		{src:"assets/images/pack153-2.png", id:"pack1"},
		{src:"assets/images/rays53-2.png", id:"rays"},
		{src:"assets/images/Vert_bg53-2.png", id:"Vert_bg"}
	],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['F3D0D0D08ED9DE449823998BCCD6A8D4'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}


// Layer depth API : 

AdobeAn.Layer = new function() {
	this.getLayerZDepth = function(timeline, layerName)
	{
		if(layerName === "Camera")
		layerName = "___camera___instance";
		var script = "if(timeline." + layerName + ") timeline." + layerName + ".depth; else 0;";
		return eval(script);
	}
	this.setLayerZDepth = function(timeline, layerName, zDepth)
	{
		const MAX_zDepth = 10000;
		const MIN_zDepth = -5000;
		if(zDepth > MAX_zDepth)
			zDepth = MAX_zDepth;
		else if(zDepth < MIN_zDepth)
			zDepth = MIN_zDepth;
		if(layerName === "Camera")
		layerName = "___camera___instance";
		var script = "if(timeline." + layerName + ") timeline." + layerName + ".depth = " + zDepth + ";";
		eval(script);
	}
	this.removeLayer = function(timeline, layerName)
	{
		if(layerName === "Camera")
		layerName = "___camera___instance";
		var script = "if(timeline." + layerName + ") timeline.removeChild(timeline." + layerName + ");";
		eval(script);
	}
	this.addNewLayer = function(timeline, layerName, zDepth)
	{
		if(layerName === "Camera")
		layerName = "___camera___instance";
		zDepth = typeof zDepth !== 'undefined' ? zDepth : 0;
		var layer = new createjs.MovieClip();
		layer.name = layerName;
		layer.depth = zDepth;
		layer.layerIndex = 0;
		timeline.addChild(layer);
	}
}


})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;

$(function () {
  var canvas, stage, exportRoot, anim_container, dom_overlay_container, fnStartAnimation;
  function init() {
    canvas = document.getElementById("canvas53-2");
    anim_container = document.getElementById("animation_container53-2");
    dom_overlay_container = document.getElementById("dom_overlay_container53-2");
    var comp=AdobeAn.getComposition("F3D0D0D08ED9DE449823998BCCD6A8D4");
    var lib=comp.getLibrary();
    var loader = new createjs.LoadQueue(false);
    loader.addEventListener("fileload", function(evt){handleFileLoad(evt,comp)});
    loader.addEventListener("complete", function(evt){handleComplete(evt,comp)});
    var lib=comp.getLibrary();
    loader.loadManifest(lib.properties.manifest);
  }
  function handleFileLoad(evt, comp) {
    var images=comp.getImages();
    if (evt && (evt.item.type == "image")) { images[evt.item.id] = evt.result; }
  }
  function handleComplete(evt,comp) {
    //This function is always called, irrespective of the content. You can use the variable "stage" after it is created in token create_stage.
    var lib=comp.getLibrary();
    var ss=comp.getSpriteSheet();
    var queue = evt.target;
    var ssMetadata = lib.ssMetadata;
    for(i=0; i<ssMetadata.length; i++) {
      ss[ssMetadata[i].name] = new createjs.SpriteSheet( {"images": [queue.getResult(ssMetadata[i].name)], "frames": ssMetadata[i].frames} )
    }
    var preloaderDiv = document.getElementById("_preload_div_53");
    preloaderDiv.style.display = 'none';
    canvas.style.display = 'block';
    exportRoot = new lib.Sept_02_Vert();
    stage = new lib.Stage(canvas);
    //Registers the "tick" event listener.
    fnStartAnimation = function() {
      stage.addChild(exportRoot);
      createjs.Ticker.setFPS(lib.properties.fps);
      createjs.Ticker.addEventListener("tick", stage)
      stage.addEventListener("tick", handleTick)
      function getProjectionMatrix(container, totalDepth) {
        var focalLength = 528.25;
        var projectionCenter = { x : lib.properties.width/2, y : lib.properties.height/2 };
        var scale = (totalDepth + focalLength)/focalLength;
        var scaleMat = new createjs.Matrix2D;
        scaleMat.a = 1/scale;
        scaleMat.d = 1/scale;
        var projMat = new createjs.Matrix2D;
        projMat.tx = -projectionCenter.x;
        projMat.ty = -projectionCenter.y;
        projMat = projMat.prependMatrix(scaleMat);
        projMat.tx += projectionCenter.x;
        projMat.ty += projectionCenter.y;
        return projMat;
      }
      function handleTick(event) {
        var cameraInstance = exportRoot.___camera___instance;
        if(cameraInstance !== undefined && cameraInstance.pinToObject !== undefined)
        {
          cameraInstance.x = cameraInstance.pinToObject.x + cameraInstance.pinToObject.pinOffsetX;
          cameraInstance.y = cameraInstance.pinToObject.y + cameraInstance.pinToObject.pinOffsetY;
          if(cameraInstance.pinToObject.parent !== undefined && cameraInstance.pinToObject.parent.depth !== undefined)
            cameraInstance.depth = cameraInstance.pinToObject.parent.depth + cameraInstance.pinToObject.pinOffsetZ;
        }
        applyLayerZDepth(exportRoot);
      }
      function applyLayerZDepth(parent)
      {
        var cameraInstance = parent.___camera___instance;
        var focalLength = 528.25;
        var projectionCenter = { 'x' : 0, 'y' : 0};
        if(parent === exportRoot)
        {
          var stageCenter = { 'x' : lib.properties.width/2, 'y' : lib.properties.height/2 };
          projectionCenter.x = stageCenter.x;
          projectionCenter.y = stageCenter.y;
        }
        for(child in parent.children)
        {
          var layerObj = parent.children[child];
          if(layerObj == cameraInstance)
            continue;
          applyLayerZDepth(layerObj, cameraInstance);
          if(layerObj.layerDepth === undefined)
            continue;
          if(layerObj.currentFrame != layerObj.parent.currentFrame)
          {
            layerObj.gotoAndPlay(layerObj.parent.currentFrame);
          }
          var matToApply = new createjs.Matrix2D;
          var cameraMat = new createjs.Matrix2D;
          var totalDepth = layerObj.layerDepth ? layerObj.layerDepth : 0;
          var cameraDepth = 0;
          if(cameraInstance && !layerObj.isAttachedToCamera)
          {
            var mat = cameraInstance.getMatrix();
            mat.tx -= projectionCenter.x;
            mat.ty -= projectionCenter.y;
            cameraMat = mat.invert();
            cameraMat.prependTransform(projectionCenter.x, projectionCenter.y, 1, 1, 0, 0, 0, 0, 0);
            cameraMat.appendTransform(-projectionCenter.x, -projectionCenter.y, 1, 1, 0, 0, 0, 0, 0);
            if(cameraInstance.depth)
              cameraDepth = cameraInstance.depth;
          }
          if(layerObj.depth)
          {
            totalDepth = layerObj.depth;
          }
          //Offset by camera depth
          totalDepth -= cameraDepth;
          if(totalDepth < -focalLength)
          {
            matToApply.a = 0;
            matToApply.d = 0;
          }
          else
          {
            if(layerObj.layerDepth)
            {
              var sizeLockedMat = getProjectionMatrix(parent, layerObj.layerDepth);
              if(sizeLockedMat)
              {
                sizeLockedMat.invert();
                matToApply.prependMatrix(sizeLockedMat);
              }
            }
            matToApply.prependMatrix(cameraMat);
            var projMat = getProjectionMatrix(parent, totalDepth);
            if(projMat)
            {
              matToApply.prependMatrix(projMat);
            }
          }
          layerObj.transformMatrix = matToApply;
        }
      }
    }
    //Code to support hidpi screens and responsive scaling.
    function makeResponsive(isResp, respDim, isScale, scaleType) {
      var lastW, lastH, lastS=1;
      window.addEventListener('resize', resizeCanvas);
      resizeCanvas();
      function resizeCanvas() {
        var w = lib.properties.width, h = lib.properties.height;
        var iw = window.innerWidth, ih=window.innerHeight;
        var pRatio = window.devicePixelRatio || 1, xRatio=iw/w, yRatio=ih/h, sRatio=1;
        if(isResp) {
          if((respDim=='width'&&lastW==iw) || (respDim=='height'&&lastH==ih)) {
            sRatio = lastS;
          }
          else if(!isScale) {
            if(iw<w || ih<h)
              sRatio = Math.min(xRatio, yRatio);
          }
          else if(scaleType==1) {
            sRatio = Math.min(xRatio, yRatio);
          }
          else if(scaleType==2) {
            sRatio = Math.max(xRatio, yRatio);
          }
        }
        canvas.width = w*pRatio*sRatio;
        canvas.height = h*pRatio*sRatio;
        canvas.style.width = anim_container.style.width = dom_overlay_container.style.width = preloaderDiv.style.width = w*sRatio+'px';
        canvas.style.height = anim_container.style.height = dom_overlay_container.style.height = preloaderDiv.style.height = h*sRatio+'px';
        stage.scaleX = pRatio*sRatio;
        stage.scaleY = pRatio*sRatio;
        lastW = iw; lastH = ih; lastS = sRatio;
        stage.tickOnUpdate = false;
        stage.update();
        stage.tickOnUpdate = true;
      }
    }
    makeResponsive(true,'both',true,2);
    AdobeAn.compositionLoaded(lib.properties.id);
    fnStartAnimation();
  }
  init();
});