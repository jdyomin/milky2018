(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [];


// symbols:



(lib.bg_vert = function() {
	this.initialize(img.bg_vert);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,375,812);


(lib.pack1 = function() {
	this.initialize(img.pack1);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,360,506);


(lib.shadow = function() {
	this.initialize(img.shadow);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,196,52);


(lib.stakan = function() {
	this.initialize(img.stakan);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,203,354);


(lib.Z = function() {
	this.initialize(img.Z);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,45,52);// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.Symbol11 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Z();
	this.instance.parent = this;
	this.instance.setTransform(-22.5,-26);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol11, new cjs.Rectangle(-22.5,-26,45,52), null);


(lib.Symbol6 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.shadow();
	this.instance.parent = this;
	this.instance.setTransform(-98,-26);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol6, new cjs.Rectangle(-98,-26,196,52), null);


(lib.Symbol_10_Layer_4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_4
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#DE786D").s().p("AgwAwQgTgUgBgcQABgbATgVQAVgTAbAAQAcAAAUATQAUAVAAAbQAAAcgUAUQgUAVgcgBQgbABgVgVg");
	this.shape.setTransform(28,49.5);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#DE786D").s().p("AgwAwQgTgUgBgcQABgcATgTQAVgVAbAAQAcAAAUAVQAUATAAAcQAAAcgUAUQgUAUgcABQgbgBgVgUg");
	this.shape_1.setTransform(28,53.05);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#DE786D").s().p("AgwAwQgTgUgBgcQABgcATgUQAVgTAbAAQAcAAAUATQAUAUAAAcQAAAcgUAUQgUAVgcAAQgbAAgVgVg");
	this.shape_2.setTransform(28,56.6);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#DE786D").s().p("AgwAwQgTgUgBgcQABgcATgUQAVgTAbAAQAcAAAUATQAUAUAAAcQAAAcgUAUQgUAVgcgBQgbABgVgVg");
	this.shape_3.setTransform(28,60.2);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#DE786D").s().p("AgwAwQgTgUgBgcQABgbATgVQAVgTAbgBQAcABAUATQAUAVAAAbQAAAcgUAUQgUAUgcAAQgbAAgVgUg");
	this.shape_4.setTransform(28,61.95);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#DE786D").s().p("AgwAwQgTgUgBgcQABgcATgTQAVgVAbABQAcgBAUAVQAUATAAAcQAAAcgUAUQgUAVgcAAQgbAAgVgVg");
	this.shape_5.setTransform(28,65.5);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#DE786D").s().p("AgwAyQgTgVgBgdQABgcATgVQAVgWAbABQAcgBAUAWQAUAVAAAcQAAAdgUAVQgUAVgcABQgbgBgVgVg");
	this.shape_6.setTransform(28,68.05);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#DE786D").s().p("AgwA0QgTgWgBgeQABgeATgVQAVgWAbAAQAcAAAUAWQAUAVAAAeQAAAegUAWQgUAWgcAAQgbAAgVgWg");
	this.shape_7.setTransform(28,68.625);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#DE786D").s().p("AgwA1QgTgXgBgeQABgfATgWQAVgVAbAAQAcAAAUAVQAUAWAAAfQAAAegUAXQgUAWgcABQgbgBgVgWg");
	this.shape_8.setTransform(28,69.1);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#DE786D").s().p("AgwA2QgTgXgBgfQABgfATgXQAVgWAbAAQAcAAAUAWQAUAXAAAfQAAAfgUAXQgUAXgcAAQgbAAgVgXg");
	this.shape_9.setTransform(28,69.45);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#DE786D").s().p("AgwA3QgTgXgBggQABgfATgYQAVgWAbAAQAcAAAUAWQAUAYAAAfQAAAggUAXQgUAXgcAAQgbAAgVgXg");
	this.shape_10.setTransform(28,69.75);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#DE786D").s().p("AgwA4QgTgXgBghQABggATgWQAVgYAbAAQAcAAAUAYQAUAWAAAgQAAAhgUAXQgUAXgcAAQgbAAgVgXg");
	this.shape_11.setTransform(28,69.95);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#DE786D").s().p("AgwA4QgTgXgBghQABggATgYQAVgXAbAAQAcAAAUAXQAUAYAAAgQAAAhgUAXQgUAYgcAAQgbAAgVgYg");
	this.shape_12.setTransform(28,70.175);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#DE786D").s().p("AgwA4QgTgXgBghQABghATgXQAVgXAbgBQAcABAUAXQAUAXAAAhQAAAhgUAXQgUAYgcAAQgbAAgVgYg");
	this.shape_13.setTransform(28,70.35);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#DE786D").s().p("AgwA4QgTgXgBghQABghATgYQAVgXAbAAQAcAAAUAXQAUAYAAAhQAAAhgUAXQgUAZgcAAQgbAAgVgZg");
	this.shape_14.setTransform(28,70.5);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#DE786D").s().p("AgwA5QgTgXgBgiQABghATgXQAVgYAbAAQAcAAAUAYQAUAXAAAhQAAAigUAXQgUAYgcABQgbgBgVgYg");
	this.shape_15.setTransform(28,70.6);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#DE786D").s().p("AgwA5QgTgXgBgiQABghATgYQAVgYAbAAQAcAAAUAYQAUAYAAAhQAAAigUAXQgUAZgcAAQgbAAgVgZg");
	this.shape_16.setTransform(28,70.725);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#DE786D").s().p("AgwA6QgTgYgBgiQABghATgYQAVgYAbAAQAcAAAUAYQAUAYAAAhQAAAigUAYQgUAYgcAAQgbAAgVgYg");
	this.shape_17.setTransform(28,70.825);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#DE786D").s().p("AgwA6QgTgYgBgiQABghATgYQAVgZAbAAQAcAAAUAZQAUAYAAAhQAAAigUAYQgUAYgcAAQgbAAgVgYg");
	this.shape_18.setTransform(28,70.9);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#DE786D").s().p("AgwA6QgTgYgBgiQABgiATgYQAVgYAbAAQAcAAAUAYQAUAYAAAiQAAAigUAYQgUAZgcAAQgbAAgVgZg");
	this.shape_19.setTransform(28,70.975);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#DE786D").s().p("AgwA7QgTgZgBgiQABgiATgYQAVgYAbAAQAcAAAUAYQAUAYAAAiQAAAigUAZQgUAYgcAAQgbAAgVgYg");
	this.shape_20.setTransform(28,71.075);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#DE786D").s().p("AgwA7QgTgZgBgiQABgiATgZQAVgXAbAAQAcAAAUAXQAUAZAAAiQAAAigUAZQgUAYgcAAQgbAAgVgYg");
	this.shape_21.setTransform(28,71.15);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#DE786D").s().p("AgwA7QgTgZgBgiQABgiATgYQAVgZAbAAQAcAAAUAZQAUAYAAAiQAAAigUAZQgUAZgcAAQgbAAgVgZg");
	this.shape_22.setTransform(28,71.175);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#DE786D").s().p("AgwA7QgTgZgBgiQABgiATgZQAVgYAbAAQAcAAAUAYQAUAZAAAiQAAAigUAZQgUAZgcAAQgbAAgVgZg");
	this.shape_23.setTransform(28,71.225);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#DE786D").s().p("AgwA3QgTgXgBggQABgfATgXQAVgXAbAAQAcAAAUAXQAUAXAAAfQAAAggUAXQgUAXgcAAQgbAAgVgXg");
	this.shape_24.setTransform(28,62.75);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#DE786D").s().p("AgwA1QgTgWgBgfQABgeATgWQAVgWAbAAQAcAAAUAWQAUAWAAAeQAAAfgUAWQgUAWgcAAQgbAAgVgWg");
	this.shape_25.setTransform(28,59.025);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#DE786D").s().p("AgwAzQgTgWgBgdQABgdATgVQAVgVAbAAQAcAAAUAVQAUAVAAAdQAAAdgUAWQgUAVgcAAQgbAAgVgVg");
	this.shape_26.setTransform(28,54.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},9).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape_1,p:{y:53.05}}]},1).to({state:[{t:this.shape_1,p:{y:54.85}}]},1).to({state:[{t:this.shape_2,p:{y:56.6}}]},1).to({state:[{t:this.shape_2,p:{y:58.4}}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4,p:{y:61.95}}]},1).to({state:[{t:this.shape_4,p:{y:63.75}}]},1).to({state:[{t:this.shape_5,p:{y:65.5}}]},1).to({state:[{t:this.shape_5,p:{y:67.3}}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12,p:{y:70.175}}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17,p:{y:70.825}}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19,p:{y:70.975}}]},1).to({state:[{t:this.shape_19,p:{y:71.025}}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22,p:{y:71.175}}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_22,p:{y:71.25}}]},1).to({state:[{t:this.shape_22,p:{y:70.925}}]},1).to({state:[{t:this.shape_19,p:{y:69.9}}]},1).to({state:[{t:this.shape_17,p:{y:68.175}}]},1).to({state:[{t:this.shape_12,p:{y:65.825}}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape}]},1).wait(53));
	this.timeline.addTween(cjs.Tween.get(this.shape).wait(10).to({y:51.3},0).to({_off:true},1).wait(36).to({_off:false,y:49.5},0).wait(53));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_10_Layer_3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_3
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(12,1,1).p("Ah4AWQgqgFgCgJQgCgIAegJQAegJAxgGQA1gFBCACQA4ABAdAKQAcAKgOAKQgNAMg0AHQgmAFg4ABQhHABhMgH");
	this.shape.setTransform(35.0883,49.78);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(12,1,1).p("AiSAoQBLALBHgBQA4gCAmgJQA1gLAOgVQAOgTgbgQQgcgRg4gDQhCgDg1AJQgxAJggAQQgeAQABAOQACARApAI");
	this.shape_1.setTransform(35.0718,49.773);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(12,1,1).p("AiUA5QBLAQBIgDQA3gCAmgMQA1gRAQgcQAPgcgbgYQgbgYg4gEQhCgFg2AOQgxANggAXQgfAXABAUQABAXAoAM");
	this.shape_2.setTransform(35.0695,49.7573);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(12,1,1).p("AiVBKQBKAVBIgEQA4gCAmgQQA2gWAQglQAQgkgagfQgbgfg3gFQhDgGg1ARQgyARghAdQggAeAAAbQABAeAoAP");
	this.shape_3.setTransform(35.0298,49.75);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(12,1,1).p("AiXBbQBKAaBIgEQA4gEAmgTQA2gbARguQARgsgZgmQgagng3gGQhDgHg2AWQgyAUghAkQghAlAAAhQAAAlAoAS");
	this.shape_4.setTransform(35.019,49.7482);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(12,1,1).p("AiZBrQBKAfBIgFQA4gEAngXQA2ggASg2QASg1gZgtQgZgug3gHQhCgJg3AaQgyAYgiArQgiAsgBAnQgBAsAoAV");
	this.shape_5.setTransform(35.0134,49.755);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(12,1,1).p("AiaB9QBJAjBJgGQA3gFAogaQA3glASg+QATg+gYg0QgYg1g4gJQhCgKg3AeQgyAcgjAyQgiAygCAuQgCAzAoAZ");
	this.shape_6.setTransform(34.9978,49.7184);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(12,1,1).p("AicCNQBJApBJgHQA3gFAogfQA3gpAUhHQAThGgXg8QgXg8g4gKQhCgLg3AiQgzAfgjA6QgjA5gCA0QgDA6AnAc");
	this.shape_7.setTransform(34.9898,49.7166);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(12,1,1).p("AidCeQBIAuBIgIQA4gGAogiQA4guAUhQQAVhPgXhCQgXhDg3gMQhCgMg3AmQg0AjgkBAQgjBAgDA6QgDBBAnAf");
	this.shape_8.setTransform(34.9529,49.7091);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(12,1,1).p("AifCvQBHAzBJgJQA4gGAogmQA5gzAVhYQAVhXgVhKQgXhLg3gMQhBgOg5ArQgzAmglBHQgkBHgEBBQgDBHAmAj");
	this.shape_9.setTransform(34.9541,49.694);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(12,1,1).p("AiHC2QgmgmAEhPQAEhHAlhNQAmhOA0gqQA4gvBCAPQA3ANAVBSQAVBRgWBgQgWBgg5A5QgpApg3AHQhJAKhHg4");
	this.shape_10.setTransform(34.933,49.6865);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(12,1,1).p("AigDJQBHA5BJgJQA3gIApgrQA5g7AWhlQAWhjgVhVQgVhVg3gOQhCgQg4AxQg0AsgmBRQglBQgEBLQgEBSAmAo");
	this.shape_11.setTransform(34.939,49.6797);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(12,1,1).p("AigDPQBHA7BJgKQA3gIApgsQA5g9AWhoQAWhmgVhYQgVhXg3gPQhCgQg4AyQg0AtgmBUQglBTgEBNQgEBVAmAo");
	this.shape_12.setTransform(34.939,49.6672);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(12,1,1).p("AigDUQBHA9BJgLQA3gIApgtQA5g+AWhrQAWhpgVhZQgVhag3gPQhCgRg4A0Qg0AugmBWQglBVgEBPQgEBWAmAq");
	this.shape_13.setTransform(34.939,49.6519);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(12,1,1).p("AigDYQBHA+BJgLQA3gIApguQA5hAAWhsQAWhrgVhbQgVhcg3gPQhCgRg4A0Qg0AvgmBYQglBXgEBQQgEBYAmAr");
	this.shape_14.setTransform(34.939,49.6666);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(12,1,1).p("AigDbQBHA/BJgLQA3gIApgvQA5hBAWhtQAWhtgVhdQgVhdg3gPQhCgRg4A1Qg0AwgmBZQglBYgEBRQgEBZAmAs");
	this.shape_15.setTransform(34.939,49.6578);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(12,1,1).p("AigDeQBHBABJgLQA3gJApgvQA5hBAWhvQAWhvgVhdQgVheg3gQQhCgRg4A1Qg0AxgmBaQglBZgEBSQgEBbAmAs");
	this.shape_16.setTransform(34.939,49.64);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(12,1,1).p("AigDfQBHBBBJgLQA3gIApgwQA5hCAWhwQAWhwgVheQgVhfg3gQQhCgSg4A3Qg0AxgmBbQglBZgEBTQgEBcAmAs");
	this.shape_17.setTransform(34.939,49.6562);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#000000").ss(12,1,1).p("AigDiQBHBBBJgLQA3gJApgwQA5hDAWhxQAWhwgVhfQgVhgg3gQQhCgRg4A2Qg0AxgmBcQglBbgEBUQgEBcAmAs");
	this.shape_18.setTransform(34.939,49.6419);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#000000").ss(12,1,1).p("AigDjQBHBCBJgLQA3gJApgwQA5hDAWhyQAWhygVhfQgVhhg3gQQhCgSg4A3Qg0AygmBdQglBbgEBUQgEBdAmAt");
	this.shape_19.setTransform(34.939,49.6322);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#000000").ss(12,1,1).p("AigDkQBHBCBJgLQA3gJApgxQA5hDAWhzQAWhxgVhhQgVhhg3gQQhCgSg4A3Qg0AygmBdQglBcgEBVQgEBeAmAt");
	this.shape_20.setTransform(34.939,49.6567);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#000000").ss(12,1,1).p("AigDlQBHBDBJgLQA3gJApgxQA5hEAWhzQAWhzgVhhQgVhhg3gQQhCgSg4A3Qg0AygmBeQglBcgEBVQgEBeAmAu");
	this.shape_21.setTransform(34.939,49.6577);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#000000").ss(12,1,1).p("AigDnQBHBDBJgMQA3gJApgxQA5hEAWh0QAWhzgVhhQgVhig3gQQhCgSg4A3Qg0AzgmBeQglBcgEBWQgEBfAmAt");
	this.shape_22.setTransform(34.939,49.6388);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#000000").ss(12,1,1).p("AigDoQBHBDBJgMQA3gIApgyQA5hEAWh0QAWh0gVhiQgVhig3gQQhCgTg4A5Qg0AygmBfQglBcgEBWQgEBfAmAu");
	this.shape_23.setTransform(34.939,49.6311);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#000000").ss(12,1,1).p("AigDpQBHBDBJgMQA3gJApgxQA5hFAWh0QAWh0gVhiQgVhjg3gQQhCgTg4A5Qg0AygmBfQglBdgEBXQgEBfAmAu");
	this.shape_24.setTransform(34.939,49.6311);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f().s("#000000").ss(12,1,1).p("AigDpQBHBEBJgMQA3gJApgyQA5hEAWh1QAWh0gVhjQgVhjg3gQQhCgSg4A4Qg0AzgmBfQglBdgEBXQgEBfAmAu");
	this.shape_25.setTransform(34.939,49.6398);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f().s("#000000").ss(12,1,1).p("AigDqQBHBDBJgLQA3gJApgyQA5hFAWh1QAWh1gVhiQgVhkg3gQQhCgSg4A4Qg0AzgmBgQglBdgEBXQgEBgAmAu");
	this.shape_26.setTransform(34.939,49.6305);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f().s("#000000").ss(12,1,1).p("AigDqQBHBEBJgLQA3gJApgzQA5hFAWh1QAWh1gVhjQgVhjg3gRQhCgSg4A5Qg0AzgmBfQglBegEBXQgEBgAmAv");
	this.shape_27.setTransform(34.939,49.6315);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f().s("#000000").ss(12,1,1).p("AigDrQBHBEBJgMQA3gJApgyQA5hFAWh2QAWh1gVhjQgVhkg3gQQhCgTg4A5Qg0AzgmBgQglBegEBXQgEBhAmAu");
	this.shape_28.setTransform(34.939,49.6315);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f().s("#000000").ss(12,1,1).p("AigDrQBHBEBJgLQA3gJApgzQA5hFAWh2QAWh1gVhjQgVhkg3gRQhCgSg4A5Qg0AzgmBgQglBegEBYQgEBgAmAu");
	this.shape_29.setTransform(34.939,49.6315);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f().s("#000000").ss(12,1,1).p("AiHDfQgmgvAEhgQAEhYAlheQAmhgA0g0QA4g5BCATQA3AQAVBkQAVBkgWB1QgWB2g5BGQgpAyg3AJQhJAMhHhE");
	this.shape_30.setTransform(34.939,49.6315);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f().s("#000000").ss(12,1,1).p("AigDoQBHBDBJgLQA4gJApgyQA4hEAWh0QAWh0gVhiQgWhjg3gQQhBgSg5A4QgzAzgmBeQglBdgEBXQgEBfAmAu");
	this.shape_31.setTransform(34.9392,49.6336);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f().s("#000000").ss(12,1,1).p("AifDfQBHBABJgLQA4gJAogvQA5hCAVhvQAWhvgVheQgWheg3gQQhCgRg4A1Qg0AxglBbQglBZgDBSQgEBbAmAs");
	this.shape_32.setTransform(34.9498,49.64);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f().s("#000000").ss(12,1,1).p("AieDOQBHA7BJgKQA4gHAogtQA4g8AVhnQAVhngWhXQgWhXg4gOQhBgRg4AyQg0AtgkBUQgkBTgDBMQgEBUAnAp");
	this.shape_33.setTransform(34.9732,49.6428);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f().s("#000000").ss(12,1,1).p("AicC3QBIA0BJgJQA3gHAognQA4g2AUhbQAUhbgXhNQgXhNg4gNQhCgOg3AsQgzAogkBKQgjBJgDBEQgCBKAmAk");
	this.shape_34.setTransform(34.9825,49.6719);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f().s("#000000").ss(12,1,1).p("AibCZQBJAsBJgIQA3gFAoghQA3gtAThNQAShLgXhBQgZhBg3gKQhCgMg3AlQgzAhgiA+QgjA9gBA5QgCA+AnAe");
	this.shape_35.setTransform(35.0026,49.6827);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f().s("#000000").ss(12,1,1).p("AiYB0QBKAhBJgFQA3gFAngZQA2giARg6QARg5gYgxQgagyg3gIQhDgJg2AcQgyAagiAvQghAvAAAqQgBAwAoAX");
	this.shape_36.setTransform(35.0229,49.7048);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f().s("#000000").ss(12,1,1).p("AiUBJQBLAVBIgDQA3gDAmgQQA1gVAQglQAPgkgageQgbgfg4gFQhCgGg2ARQgxARggAdQggAeABAaQABAeAoAO");
	this.shape_37.setTransform(35.0391,49.7484);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f().s("#000000").ss(12,1,1).p("AiRAkQBMAKBHgCQA4gBAmgIQA0gKANgTQAOgQgcgPQgdgPg4gDQhCgDg1AJQgxAIgeAOQgeAPACAMQACAOAqAH");
	this.shape_38.setTransform(35.0883,49.8207);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f().s("#000000").ss(12,1,1).p("AiRAuQBMAMBHgCQA4gCAmgJQA0gNANgXQAOgWgcgTQgdgUg4gDQhCgEg1ALQgxALgeASQgeASACAQQACATAqAJ");
	this.shape_39.setTransform(35.0883,49.8105);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f().s("#000000").ss(12,1,1).p("AiRA1QBMAOBHgCQA4gCAmgLQA0gPANgbQAOgZgcgWQgdgWg4gEQhCgFg1ANQgxANgeAUQgeAVACATQACAVAqAK");
	this.shape_40.setTransform(35.0883,49.8293);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f().s("#000000").ss(12,1,1).p("AiRA5QBMAQBHgDQA4gCAmgMQA0gQANgdQAOgbgcgYQgdgYg4gEQhCgFg1AOQgxANgeAWQgeAXACAUQACAYAqAK");
	this.shape_41.setTransform(35.0883,49.8361);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f().s("#000000").ss(12,1,1).p("Ah4A2QgqgLgCgYQgCgUAegYQAegWAxgOQA1gOBCAFQA4AEAdAZQAcAYgOAcQgNAeg0AQQgmAMg4ADQhHAChMgQ");
	this.shape_42.setTransform(35.0883,49.8511);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f().s("#000000").ss(12,1,1).p("AiRA3QBMAPBHgDQA4gCAmgMQA0gPANgcQAOgagcgWQgdgXg4gEQhCgFg1ANQgxANgeAVQgeAWACATQACAXAqAK");
	this.shape_43.setTransform(35.0883,49.835);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f().s("#000000").ss(12,1,1).p("AiRArQBMAMBHgCQA4gCAmgJQA0gMANgWQAOgUgcgSQgdgSg4gDQhCgEg1AKQgxALgeAQQgeARACAPQACASAqAI");
	this.shape_44.setTransform(35.0883,49.8189);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},9).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_27}]},1).to({state:[{t:this.shape_28}]},1).to({state:[{t:this.shape_29}]},1).to({state:[{t:this.shape_30}]},1).to({state:[{t:this.shape_31}]},1).to({state:[{t:this.shape_32}]},1).to({state:[{t:this.shape_33}]},1).to({state:[{t:this.shape_34}]},1).to({state:[{t:this.shape_35}]},1).to({state:[{t:this.shape_36}]},1).to({state:[{t:this.shape_37}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},12).to({state:[{t:this.shape_38}]},1).to({state:[{t:this.shape_39}]},1).to({state:[{t:this.shape_40}]},1).to({state:[{t:this.shape_41}]},1).to({state:[{t:this.shape_42}]},1).to({state:[{t:this.shape_43}]},1).to({state:[{t:this.shape_44}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},4).to({state:[{t:this.shape_38}]},1).to({state:[{t:this.shape_39}]},1).to({state:[{t:this.shape_40}]},1).to({state:[{t:this.shape_41}]},1).to({state:[{t:this.shape_42}]},1).to({state:[{t:this.shape_43}]},1).to({state:[{t:this.shape_44}]},1).to({state:[{t:this.shape}]},1).wait(21));
	this.timeline.addTween(cjs.Tween.get(this.shape).wait(9).to({_off:true},1).wait(37).to({_off:false},0).wait(12).to({_off:true},1).wait(7).to({_off:false},0).wait(4).to({_off:true},1).wait(7).to({_off:false},0).wait(21));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_10_Layer_2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(12,1,1).p("Ai8gZQC2BDDDgW");
	this.shape.setTransform(75.475,7.5647);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(12,1,1).p("Ai8gaQC1BIDEgb");
	this.shape_1.setTransform(75.475,7.6645);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(12,1,1).p("Ai8gbQC0BNDFgg");
	this.shape_2.setTransform(75.475,7.7655);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(12,1,1).p("Ai8gcQCzBSDGgl");
	this.shape_3.setTransform(75.475,7.8598);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(12,1,1).p("Ai8gdQCyBWDHgp");
	this.shape_4.setTransform(75.475,7.9566);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(12,1,1).p("Ai8gdQCxBaDIgt");
	this.shape_5.setTransform(75.475,8.0445);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(12,1,1).p("Ai8geQCwBeDJgx");
	this.shape_6.setTransform(75.475,8.1449);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(12,1,1).p("Ai8gfQCvBiDKg1");
	this.shape_7.setTransform(75.475,8.2354);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(12,1,1).p("Ai8ggQCuBmDLg5");
	this.shape_8.setTransform(75.475,8.3153);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(12,1,1).p("Ai8ghQCtBqDMg9");
	this.shape_9.setTransform(75.475,8.4074);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(12,1,1).p("Ai8giQCsBtDNhA");
	this.shape_10.setTransform(75.475,8.477);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(12,1,1).p("Ai8gjQCrBxDOhE");
	this.shape_11.setTransform(75.475,8.5586);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(12,1,1).p("Ai8gjQCqBzDPhG");
	this.shape_12.setTransform(75.475,8.6289);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(12,1,1).p("Ai8gkQCqB3DPhK");
	this.shape_13.setTransform(75.475,8.6995);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(12,1,1).p("Ai8glQCpB6DQhN");
	this.shape_14.setTransform(75.475,8.7704);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(12,1,1).p("Ai8glQCoB8DRhP");
	this.shape_15.setTransform(75.475,8.8297);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(12,1,1).p("Ai8gmQCoB/DRhS");
	this.shape_16.setTransform(75.475,8.8891);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(12,1,1).p("Ai8gmQCnCADShT");
	this.shape_17.setTransform(75.475,8.9367);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#000000").ss(12,1,1).p("Ai8gnQCnCDDShW");
	this.shape_18.setTransform(75.475,8.9963);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#000000").ss(12,1,1).p("Ai8gnQCmCFDThY");
	this.shape_19.setTransform(75.475,9.0441);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#000000").ss(12,1,1).p("Ai8goQCmCHDTha");
	this.shape_20.setTransform(75.475,9.0801);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#000000").ss(12,1,1).p("Ai8goQClCIDUhb");
	this.shape_21.setTransform(75.475,9.116);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#000000").ss(12,1,1).p("Ai8gpQClCKDUhd");
	this.shape_22.setTransform(75.475,9.152);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#000000").ss(12,1,1).p("Ai8gpQClCLDUhe");
	this.shape_23.setTransform(75.475,9.188);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#000000").ss(12,1,1).p("Ai8gpQCkCMDVhf");
	this.shape_24.setTransform(75.475,9.2121);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f().s("#000000").ss(12,1,1).p("Ai8gpQCkCNDVhg");
	this.shape_25.setTransform(75.475,9.2361);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f().s("#000000").ss(12,1,1).p("Ai8gqQCkCODVhh");
	this.shape_26.setTransform(75.475,9.2602);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f().s("#000000").ss(12,1,1).p("Ai8gqQCkCPDVhi");
	this.shape_27.setTransform(75.475,9.2723);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f().s("#000000").ss(12,1,1).p("Ai8gnQCmCEDThX");
	this.shape_28.setTransform(75.475,9.0322);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f().s("#000000").ss(12,1,1).p("Ai8gjQCrByDOhF");
	this.shape_29.setTransform(75.475,8.582);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f().s("#000000").ss(12,1,1).p("Ai8ggQCuBlDLg4");
	this.shape_30.setTransform(75.475,8.281);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f().s("#000000").ss(12,1,1).p("Ai8gcQCyBVDHgo");
	this.shape_31.setTransform(75.475,7.9349);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},9).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23,p:{y:9.188}}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_25,p:{y:9.2361}}]},1).to({state:[{t:this.shape_25,p:{y:9.2482}}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_27,p:{y:9.2723}}]},1).to({state:[{t:this.shape_27,p:{y:9.2843}}]},1).to({state:[{t:this.shape_27,p:{y:9.2892}}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_23,p:{y:9.176}}]},1).to({state:[{t:this.shape_28}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_29}]},1).to({state:[{t:this.shape_30}]},1).to({state:[{t:this.shape_31}]},1).to({state:[{t:this.shape}]},1).wait(53));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_10_Layer_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(12,1,1).p("Ai8gZQCeAxDbAC");
	this.shape.setTransform(0,0.025);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(12,1,1).p("Ai8gZQCdA2DcgD");
	this.shape_1.setTransform(0,0.0304);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(12,1,1).p("Ai8gaQCcA6DdgG");
	this.shape_2.setTransform(0,0.0573);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(12,1,1).p("Ai8gaQCbA+DegL");
	this.shape_3.setTransform(0,0.1062);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(12,1,1).p("Ai8gbQCaBDDfgP");
	this.shape_4.setTransform(0,0.1614);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(12,1,1).p("Ai8gbQCZBGDggT");
	this.shape_5.setTransform(0,0.2162);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(12,1,1).p("Ai8gcQCYBKDhgW");
	this.shape_6.setTransform(0,0.2846);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(12,1,1).p("Ai8gcQCXBNDiga");
	this.shape_7.setTransform(0,0.3484);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(12,1,1).p("Ai8gdQCXBQDigd");
	this.shape_8.setTransform(0,0.4055);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(12,1,1).p("Ai8geQCWBUDjgg");
	this.shape_9.setTransform(0,0.4745);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(12,1,1).p("Ai8geQCVBXDkgk");
	this.shape_10.setTransform(0,0.5352);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(12,1,1).p("Ai8gfQCUBaDlgm");
	this.shape_11.setTransform(0,0.5973);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(12,1,1).p("Ai8gfQCUBcDlgp");
	this.shape_12.setTransform(0,0.6499);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(12,1,1).p("Ai8ggQCTBfDmgs");
	this.shape_13.setTransform(0,0.714);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(12,1,1).p("Ai8ghQCTBiDmgu");
	this.shape_14.setTransform(0,0.768);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(12,1,1).p("Ai8ghQCSBkDngx");
	this.shape_15.setTransform(0,0.8117);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(12,1,1).p("Ai8giQCSBnDngz");
	this.shape_16.setTransform(0,0.8667);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(12,1,1).p("Ai8giQCRBoDog1");
	this.shape_17.setTransform(0,0.9111);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#000000").ss(12,1,1).p("Ai8giQCRBqDog3");
	this.shape_18.setTransform(0,0.9446);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#000000").ss(12,1,1).p("Ai8gjQCQBsDpg4");
	this.shape_19.setTransform(0,0.9894);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#000000").ss(12,1,1).p("Ai8gjQCQBtDpg6");
	this.shape_20.setTransform(0,1.0232);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#000000").ss(12,1,1).p("Ai8gkQCQBvDpg7");
	this.shape_21.setTransform(0,1.0571);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#000000").ss(12,1,1).p("Ai8gkQCPBwDqg8");
	this.shape_22.setTransform(0,1.0798);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#000000").ss(12,1,1).p("Ai8gkQCPBxDqg+");
	this.shape_23.setTransform(0,1.114);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#000000").ss(12,1,1).p("Ai8gkQCPByDqg/");
	this.shape_24.setTransform(0,1.1368);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f().s("#000000").ss(12,1,1).p("Ai8gkQCPBzDqhA");
	this.shape_25.setTransform(0,1.1482);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f().s("#000000").ss(12,1,1).p("Ai8glQCPB0DqhA");
	this.shape_26.setTransform(0,1.1711);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f().s("#000000").ss(12,1,1).p("Ai8glQCOB1DrhB");
	this.shape_27.setTransform(0,1.1825);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f().s("#000000").ss(12,1,1).p("Ai8glQCPB1DqhB");
	this.shape_28.setTransform(0,1.1966);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f().s("#000000").ss(12,1,1).p("Ai8glQCOB0DrhA");
	this.shape_29.setTransform(0,1.1711);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f().s("#000000").ss(12,1,1).p("Ai8gfQCUBaDlgn");
	this.shape_30.setTransform(0,0.6078);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f().s("#000000").ss(12,1,1).p("Ai8gdQCXBPDigb");
	this.shape_31.setTransform(0,0.3767);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f().s("#000000").ss(12,1,1).p("Ai8gaQCaBBDfgO");
	this.shape_32.setTransform(0,0.1467);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},9).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19,p:{y:0.9894}}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23,p:{y:1.114}}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_27,p:{y:1.1825}}]},1).to({state:[{t:this.shape_27,p:{y:1.1825}}]},1).to({state:[{t:this.shape_27,p:{y:1.194}}]},1).to({state:[{t:this.shape_28}]},1).to({state:[{t:this.shape_29}]},1).to({state:[{t:this.shape_23,p:{y:1.1026}}]},1).to({state:[{t:this.shape_19,p:{y:0.9782}}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_30}]},1).to({state:[{t:this.shape_31}]},1).to({state:[{t:this.shape_32}]},1).to({state:[{t:this.shape}]},1).wait(53));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_9_Layer_3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_3
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(12,1,1).p("AgehqQhJAyAOBcQAFAnAYAXQAYAYAjgBQAhAAAagbQAZgbAHgsQAIgqgQgoQgQgogwgV");
	this.shape.setTransform(55.6477,45.9255);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(12,1,1).p("AgliCQhYA9AQBxQAHAvAdAcQAdAdAqgBQApAAAfghQAfghAJg0QAJg0gTgxQgUgxg6ga");
	this.shape_1.setTransform(49.1839,44.5504);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(12,1,1).p("AgpiPQhiBDASB9QAHAzAhAgQAgAgAugBQAuAAAiglQAjgkAKg6QAJg6gVg2QgVg2hBgc");
	this.shape_2.setTransform(45.2654,43.7254);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(12,1,1).p("AgriYQhpBHATCFQAIA3AjAiQAiAhAxgBQAxAAAkgnQAlgnAKg9QAKg9gWg6QgXg5hEgf");
	this.shape_3.setTransform(42.7101,43.1514);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(12,1,1).p("AgtifQhtBLAUCKQAIA6AkAjQAkAjAzgBQAzgBAmgoQAmgoALhAQAKhAgXg8QgYg8hHgg");
	this.shape_4.setTransform(40.9345,42.7764);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(12,1,1).p("AguikQhwBNAUCPQAIA6AlAlQAlAkA1gBQA0gBAngpQAngqAMhCQALhBgYg+QgZg+hJgg");
	this.shape_5.setTransform(39.6213,42.5014);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(12,1,1).p("AgwinQhyBPAVCRQAIA8AmAlQAmAlA2gBQA1AAAogrQAogqAMhEQALhDgZg/QgZg/hLgh");
	this.shape_6.setTransform(38.6077,42.2763);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(12,1,1).p("AgwiqQh1BQAVCUQAJA9AnAmQAmAlA3gBQA2AAAogrQApgsAMhEQALhFgZg/QgZhBhMgh");
	this.shape_7.setTransform(37.8077,42.1263);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(12,1,1).p("AgxitQh2BRAWCXQAJA9AnAmQAmAmA4AAQA3gBApgsQApgrAMhGQAMhFgahBQgZhBhOgi");
	this.shape_8.setTransform(37.188,42.0003);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(12,1,1).p("AgyiuQh3BSAWCYQAJA+AnAmQAnAnA5gBQA3gBAqgsQApgsANhGQALhGgZhCQgahBhPgj");
	this.shape_9.setTransform(36.6642,41.8513);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(12,1,1).p("AgyivQh4BSAWCZQAJA/AoAnQAnAnA5gBQA4gBAqgsQAqgtAMhHQAMhGgahCQgahDhPgj");
	this.shape_10.setTransform(36.2404,41.7763);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(12,1,1).p("AgyixQh5BTAWCaQAJBAAoAnQAoAnA5gBQA4gBAqgsQArgtAMhIQAMhHgahCQgahDhQgj");
	this.shape_11.setTransform(35.8821,41.7013);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(12,1,1).p("AgziyQh5BUAWCbQAJBAAoAnQAoAnA6gBQA4AAArgtQAqgtANhIQALhIgahCQgahEhQgj");
	this.shape_12.setTransform(35.6077,41.6263);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(12,1,1).p("AgziyQh6BUAWCbQAKBBAoAnQAoAnA6gBQA5AAAqgtQArguANhIQALhIgahDQgahDhRgk");
	this.shape_13.setTransform(35.3768,41.5762);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(12,1,1).p("AgzizQh7BUAXCcQAJBBApAnQAoAoA6gBQA5gBArgtQArguAMhIQAMhIgahEQgbhEhRgj");
	this.shape_14.setTransform(35.188,41.5512);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(12,1,1).p("Agzi0Qh7BVAWCdQAKBAAoAoQApAoA6gBQA5gBArguQArgtANhJQAMhIgbhEQgbhEhRgk");
	this.shape_15.setTransform(35.0386,41.5262);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(12,1,1).p("Agzi0Qh8BUAXCdQAJBBApAoQAoAoA6gBQA6gBArgtQArguANhJQAMhJgbhDQgbhFhRgj");
	this.shape_16.setTransform(34.9136,41.5012);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(12,1,1).p("Agzi1Qh8BVAWCdQAKBBApAoQAoAoA7gBQA5AAArguQAsguAMhJQAMhJgahEQgbhEhSgk");
	this.shape_17.setTransform(34.8142,41.5012);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#000000").ss(12,1,1).p("Agzi1Qh8BVAWCeQAKBBApAoQAoAoA7gBQA6gBArguQArguANhJQAMhJgbhEQgbhEhRgk");
	this.shape_18.setTransform(34.7392,41.4762);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#000000").ss(12,1,1).p("AgzizQh7BUAWCdQAKBAAoAoQAoAnA6gBQA6AAArguQArgtAMhJQAMhIgahEQgbhDhRgk");
	this.shape_19.setTransform(35.113,41.5262);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#000000").ss(12,1,1).p("AgxiuQh4BSAWCYQAJA+AoAnQAnAmA4gBQA4AAApgtQAqgsAMhGQALhGgZhCQgahChPgi");
	this.shape_20.setTransform(36.6327,41.8763);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#000000").ss(12,1,1).p("AguikQhwBNAUCPQAJA7AlAkQAkAkA1gBQA1AAAngqQAngqALhCQALhCgYg9QgYg+hKgh");
	this.shape_21.setTransform(39.4963,42.5003);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#000000").ss(12,1,1).p("AgriXQhnBHATCDQAIA3AiAhQAhAhAxgBQAwAAAkgmQAkgnALg8QAKg9gWg5QgXg5hEge");
	this.shape_22.setTransform(43.2089,43.2514);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#000000").ss(12,1,1).p("AgniLQhfBBARB5QAHAyAgAfQAfAeAsgBQAsAAAhgjQAigjAJg4QAJg4gUg0QgUg0g/gc");
	this.shape_23.setTransform(46.6535,44.0016);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#000000").ss(12,1,1).p("AgliBQhYA9AQBwQAHAvAdAcQAdAcAqAAQApgBAeggQAfghAJg0QAJg0gTgwQgTgxg6ga");
	this.shape_24.setTransform(49.3583,44.5754);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f().s("#000000").ss(12,1,1).p("Agih6QhUA6AQBpQAGAsAcAbQAbAbAngBQAnAAAdgfQAdgfAIgxQAIgxgRgtQgSgvg3gY");
	this.shape_25.setTransform(51.3792,45.0005);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f().s("#000000").ss(12,1,1).p("Aghh1QhPA3AOBlQAGAqAaAaQAbAaAlgBQAlAAAcgeQAcgdAIgvQAIgvgRgsQgSgsg0gX");
	this.shape_26.setTransform(52.8912,45.3505);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f().s("#000000").ss(12,1,1).p("AgghwQhNA0AOBiQAGApAZAZQAaAZAkgBQAkgBAbgcQAbgdAHgtQAIgtgRgqQgQgrgzgW");
	this.shape_27.setTransform(53.9595,45.5519);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f().s("#000000").ss(12,1,1).p("AgfhuQhLA0AOBfQAFAoAZAYQAZAYAjAAQAjgBAagcQAbgcAHgsQAIgsgQgpQgRgqgxgV");
	this.shape_28.setTransform(54.7352,45.7505);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f().s("#000000").ss(12,1,1).p("AgehsQhKAzANBdQAGAnAYAYQAZAYAjAAQAigBAZgbQAagcAIgrQAHgrgQgpQgQgpgxgV");
	this.shape_29.setTransform(55.2297,45.8505);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f().s("#000000").ss(12,1,1).p("AgehqQhJAyANBdQAGAmAYAYQAYAXAjAAQAhgBAagbQAZgbAIgrQAHgrgQgoQgQgogwgW");
	this.shape_30.setTransform(55.5285,45.8755);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18,p:{x:34.7392,y:41.4762}}]},1).to({state:[{t:this.shape_18,p:{x:34.6892,y:41.4512}}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_27}]},1).to({state:[{t:this.shape_28}]},1).to({state:[{t:this.shape_29}]},1).to({state:[{t:this.shape_30}]},1).to({state:[{t:this.shape}]},1).wait(8));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_9_Layer_2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(12,1,1).p("Ai6hkQgEBeAuA2QAtA3BQgCQBOgCA6gwQBAgzAGhR");
	this.shape.setTransform(73.809,-0.5965);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(12,1,1).p("Ai6hdQgEBXAuAyQAtA0BQgCQBOgCA6gsQBAgwAGhL");
	this.shape_1.setTransform(73.809,-2.2463);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(12,1,1).p("Ai6hZQgEBTAuAwQAtAxBQgBQBOgCA6gqQBAguAGhI");
	this.shape_2.setTransform(73.809,-3.2478);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(12,1,1).p("Ai6hWQgEBRAuAuQAtAwBQgCQBOgBA6gpQBAgtAGhF");
	this.shape_3.setTransform(73.809,-3.921);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(12,1,1).p("Ai6hUQgEBPAuAtQAtAvBQgCQBOgBA6goQBAgsAGhE");
	this.shape_4.setTransform(73.809,-4.3709);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(12,1,1).p("Ai6hTQgEBOAuAsQAtAuBQgBQBOgCA6gnQBAgrAGhD");
	this.shape_5.setTransform(73.809,-4.6976);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(12,1,1).p("Ai6hSQgEBNAuAsQAtAuBQgCQBOgCA6gnQBAgqAGhC");
	this.shape_6.setTransform(73.809,-4.9708);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(12,1,1).p("Ai6hRQgEBMAuAsQAtAtBQgCQBOgCA6gmQBAgqAGhB");
	this.shape_7.setTransform(73.809,-5.1726);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(12,1,1).p("Ai6hRQgEBMAuArQAtAtBQgCQBOgBA6gnQBAgpAGhB");
	this.shape_8.setTransform(73.809,-5.3476);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(12,1,1).p("Ai6hQQgEBMAuAqQAtAsBQgBQBOgCA6gmQBAgpAGhA");
	this.shape_9.setTransform(73.809,-5.4725);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(12,1,1).p("Ai6hPQgEBLAuAqQAtAsBQgCQBOgBA6gmQBAgpAGhA");
	this.shape_10.setTransform(73.809,-5.5725);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(12,1,1).p("Ai6hPQgEBKAuAqQAtAsBQgBQBOgCA6gmQBAgoAGhA");
	this.shape_11.setTransform(73.809,-5.6475);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(12,1,1).p("Ai6hPQgEBLAuAqQAtArBQgBQBOgCA6glQBAgpAGg/");
	this.shape_12.setTransform(73.809,-5.7225);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(12,1,1).p("Ai6hPQgEBKAuAqQAtAsBQgCQBOgBA6gmQBAgoAGg/");
	this.shape_13.setTransform(73.809,-5.7975);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(12,1,1).p("Ai6hOQgEBKAuApQAtAsBQgCQBOgCA6glQBAgoAGg/");
	this.shape_14.setTransform(73.809,-5.8956);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(12,1,1).p("Ai6hOQgEBJAuAqQAtArBQgBQBOgCA6glQBAgoAGg/");
	this.shape_15.setTransform(73.809,-5.8975);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(12,1,1).p("Ai6hOQgEBKAuApQAtArBQgBQBOgCA6glQBAgoAGg/");
	this.shape_16.setTransform(73.809,-5.9475);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(12,1,1).p("Ai6hOQgEBKAuApQAtArBQgBQBOgCA6glQBAgoAGg+");
	this.shape_17.setTransform(73.809,-5.9725);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#000000").ss(12,1,1).p("Ai6hOQgEBKAuAqQAtArBQgCQBOgBA6glQBAgpAGg/");
	this.shape_18.setTransform(73.809,-5.8725);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#000000").ss(12,1,1).p("Ai6hTQgEBOAuAtQAtAtBQgBQBOgCA6gnQBAgrAGhD");
	this.shape_19.setTransform(73.809,-4.7476);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#000000").ss(12,1,1).p("Ai6hXQgEBSAuAuQAtAwBQgBQBOgCA6gpQBAgtAGhG");
	this.shape_20.setTransform(73.809,-3.7727);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#000000").ss(12,1,1).p("Ai6hbQgEBVAuAxQAtAyBQgCQBOgCA6grQBAguAGhJ");
	this.shape_21.setTransform(73.809,-2.8978);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#000000").ss(12,1,1).p("Ai6hdQgEBYAuAyQAtAzBQgCQBOgBA6gtQBAgwAGhL");
	this.shape_22.setTransform(73.809,-2.2213);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#000000").ss(12,1,1).p("Ai6hgQgEBaAuA0QAtA0BQgCQBOgCA6gtQBAgxAGhN");
	this.shape_23.setTransform(73.809,-1.6963);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#000000").ss(12,1,1).p("Ai6hhQgEBbAuA0QAtA2BQgCQBOgCA6guQBAgyAGhO");
	this.shape_24.setTransform(73.809,-1.2964);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f().s("#000000").ss(12,1,1).p("Ai6hiQgEBcAuA1QAtA2BQgCQBOgCA6guQBAgzAGhP");
	this.shape_25.setTransform(73.809,-1.0214);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f().s("#000000").ss(12,1,1).p("Ai6hjQgEBdAuA1QAtA3BQgCQBOgCA6gvQBAgzAGhQ");
	this.shape_26.setTransform(73.809,-0.8465);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f().s("#000000").ss(12,1,1).p("Ai6hkQgEBeAuA1QAtA3BQgCQBOgCA6gvQBAgzAGhQ");
	this.shape_27.setTransform(73.809,-0.6965);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f().s("#000000").ss(12,1,1).p("Ai6hkQgEBeAuA2QAtA3BQgCQBOgCA6gwQBAgzAGhQ");
	this.shape_28.setTransform(73.809,-0.6215);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13,p:{y:-5.7975}}]},1).to({state:[{t:this.shape_13,p:{y:-5.8475}}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_27}]},1).to({state:[{t:this.shape_28}]},1).to({state:[{t:this.shape}]},1).wait(8));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_9_Layer_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(12,1,1).p("AjRhxQgFBqA0A9QAzA9BZgCQBYgCBCg1QBHg6AIhb");
	this.shape.setTransform(0.0319,0.0031);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(12,1,1).p("AjRhpQgFBiA0A5QAzA5BZgBQBYgCBCgyQBHg2AIhV");
	this.shape_1.setTransform(0.0319,-1.6981);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(12,1,1).p("AjRhkQgFBeA0A2QAzA3BZgCQBYgCBCgvQBHg0AIhR");
	this.shape_2.setTransform(0.0319,-2.723);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(12,1,1).p("AjRhhQgFBbA0A0QAzA2BZgCQBYgCBCguQBHgyAIhO");
	this.shape_3.setTransform(0.0319,-3.3964);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(12,1,1).p("AjRhfQgFBZA0AzQAzA0BZgBQBYgCBCgtQBHgxAIhN");
	this.shape_4.setTransform(0.0319,-3.8479);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(12,1,1).p("AjRheQgFBYA0AzQAzAzBZgCQBYgCBCgsQBHgwAIhM");
	this.shape_5.setTransform(0.0319,-4.1979);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(12,1,1).p("AjRhcQgFBXA0AxQAzAzBZgCQBYgBBCgtQBHgvAIhK");
	this.shape_6.setTransform(0.0319,-4.4728);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(12,1,1).p("AjRhbQgFBWA0AxQAzAyBZgCQBYgBBCgsQBHgvAIhK");
	this.shape_7.setTransform(0.0319,-4.6728);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(12,1,1).p("AjRhbQgFBWA0AwQAzAyBZgCQBYgBBCgsQBHguAIhJ");
	this.shape_8.setTransform(0.0319,-4.8478);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(12,1,1).p("AjRhaQgFBVA0AwQAzAxBZgBQBYgCBCgrQBHguAIhI");
	this.shape_9.setTransform(0.0319,-4.9728);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(12,1,1).p("AjRhaQgFBVA0AwQAzAxBZgCQBYgBBCgrQBHguAIhI");
	this.shape_10.setTransform(0.0319,-5.0978);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(12,1,1).p("AjRhZQgFBUA0AwQAzAwBZgBQBYgCBCgqQBHguAIhI");
	this.shape_11.setTransform(0.0319,-5.1977);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(12,1,1).p("AjRhZQgFBUA0AvQAzAxBZgCQBYgBBCgrQBHgtAIhI");
	this.shape_12.setTransform(0.0319,-5.2477);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(12,1,1).p("AjRhYQgFBTA0AvQAzAxBZgCQBYgBBCgrQBHgtAIhH");
	this.shape_13.setTransform(0.0319,-5.3227);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(12,1,1).p("AjRhYQgFBTA0AvQAzAwBZgBQBYgCBCgqQBHgtAIhH");
	this.shape_14.setTransform(0.0319,-5.3977);
	this.shape_14._off = true;

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(12,1,1).p("AjRhYQgFBTA0AvQAzAwBZgCQBYgBBCgqQBHgtAIhH");
	this.shape_15.setTransform(0.0319,-5.4977);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(12,1,1).p("AjRhaQgFBVA0AwQAzAxBZgBQBYgCBCgrQBHguAIhJ");
	this.shape_16.setTransform(0.0319,-4.9728);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(12,1,1).p("AjRhdQgFBXA0AzQAzAzBZgCQBYgBBCgtQBHgwAIhL");
	this.shape_17.setTransform(0.0319,-4.2229);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#000000").ss(12,1,1).p("AjRhiQgFBcA0A1QAzA1BZgBQBYgCBCgvQBHgyAIhP");
	this.shape_18.setTransform(0.0319,-3.273);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#000000").ss(12,1,1).p("AjRhmQgFBgA0A3QAzA4BZgCQBYgCBCgwQBHg1AIhS");
	this.shape_19.setTransform(0.0319,-2.3716);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#000000").ss(12,1,1).p("AjRhpQgFBiA0A5QAzA6BZgCQBYgCBCgyQBHg2AIhV");
	this.shape_20.setTransform(0.0319,-1.6466);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#000000").ss(12,1,1).p("AjRhrQgFBkA0A7QAzA6BZgCQBYgBBCg0QBHg3AIhW");
	this.shape_21.setTransform(0.0319,-1.1217);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#000000").ss(12,1,1).p("AjRhtQgFBmA0A7QAzA8BZgCQBYgCBCg0QBHg4AIhY");
	this.shape_22.setTransform(0.0319,-0.7218);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#000000").ss(12,1,1).p("AjRhvQgFBoA0A8QAzA8BZgCQBYgCBCg0QBHg5AIhZ");
	this.shape_23.setTransform(0.0319,-0.4468);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#000000").ss(12,1,1).p("AjRhwQgFBpA0A8QAzA9BZgCQBYgCBCg1QBHg5AIha");
	this.shape_24.setTransform(0.0319,-0.2468);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f().s("#000000").ss(12,1,1).p("AjRhwQgFBpA0A8QAzA+BZgCQBYgCBCg2QBHg5AIha");
	this.shape_25.setTransform(0.0319,-0.0969);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f().s("#000000").ss(12,1,1).p("AjRhwQgFBpA0A9QAzA9BZgCQBYgCBCg1QBHg6AIha");
	this.shape_26.setTransform(0.0319,-0.0219);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13,p:{y:-5.3227}}]},1).to({state:[{t:this.shape_13,p:{y:-5.3727}}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape}]},1).wait(8));
	this.timeline.addTween(cjs.Tween.get(this.shape_14).wait(15).to({_off:false},0).wait(1).to({y:-5.4227},0).wait(1).to({y:-5.4727},0).to({_off:true},1).wait(2).to({_off:false,y:-5.3977},0).to({_off:true},1).wait(19));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_1_Layer_10 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_10
	this.instance = new lib.stakan();
	this.instance.parent = this;
	this.instance.setTransform(63.2,221.65);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(60));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_1_Layer_7 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_7
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(13,1,1).p("AlpDQQIwCHCjpA");
	this.shape.setTransform(222.2,387.3081);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(13,1,1).p("AloDRQIvCGCipA");
	this.shape_1.setTransform(222.275,387.3386);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(13,1,1).p("AlmDTQItCBCgo+");
	this.shape_2.setTransform(222.5,387.4561);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(13,1,1).p("AlhDZQInB4Cco+");
	this.shape_3.setTransform(222.925,387.6689);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(13,1,1).p("AlbDhQIfBrCYo8");
	this.shape_4.setTransform(223.6,388.0418);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(13,1,1).p("AlRDrQIUBZCPo6");
	this.shape_5.setTransform(224.5,388.5443);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(13,1,1).p("AlFD4QIGBBCFo3");
	this.shape_6.setTransform(225.675,389.2791);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(13,1,1).p("Ak3EGQH1AmB5oz");
	this.shape_7.setTransform(227.05,390.1919);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(13,1,1).p("AkoETQHkAKBtov");
	this.shape_8.setTransform(228.375,391.1838);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(13,1,1).p("AkcEeQHWgOBjot");
	this.shape_9.setTransform(229.55,392.175);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(13,1,1).p("AkSEmQHLghBboq");
	this.shape_10.setTransform(230.45,392.925);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(13,1,1).p("AkMErQHDgtBWoo");
	this.shape_11.setTransform(231.125,393.5);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(13,1,1).p("AkHEvQG9g2BSoo");
	this.shape_12.setTransform(231.55,393.85);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(13,1,1).p("AkFEyQG7g8BQon");
	this.shape_13.setTransform(231.775,394.05);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(13,1,1).p("AkEEyQG6g9BPom");
	this.shape_14.setTransform(231.85,394.1);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(13,1,1).p("AkFEyQG6g8BQon");
	this.shape_15.setTransform(231.8,394.05);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(13,1,1).p("AkHEwQG9g3BSoo");
	this.shape_16.setTransform(231.575,393.875);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(13,1,1).p("AkLEsQHCgvBVop");
	this.shape_17.setTransform(231.25,393.6);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#000000").ss(13,1,1).p("AkQEnQHIgkBZoq");
	this.shape_18.setTransform(230.675,393.1);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#000000").ss(13,1,1).p("AkZEgQHSgUBhos");
	this.shape_19.setTransform(229.9,392.45);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#000000").ss(13,1,1).p("AkjEXQHeAABpou");
	this.shape_20.setTransform(228.875,391.6);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#000000").ss(13,1,1).p("AkwEMQHtAZB0oy");
	this.shape_21.setTransform(227.675,390.6511);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#000000").ss(13,1,1).p("Ak9EAQH9AyB+o1");
	this.shape_22.setTransform(226.375,389.7281);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#000000").ss(13,1,1).p("AlKDzQIMBKCJo4");
	this.shape_23.setTransform(225.175,388.9563);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#000000").ss(13,1,1).p("AlVDnQIZBfCSo6");
	this.shape_24.setTransform(224.15,388.352);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f().s("#000000").ss(13,1,1).p("AldDeQIiBvCZo8");
	this.shape_25.setTransform(223.375,387.925);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f().s("#000000").ss(13,1,1).p("AljDXQIpB7Ceo+");
	this.shape_26.setTransform(222.8,387.6007);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f().s("#000000").ss(13,1,1).p("AlmDTQItCCCgo/");
	this.shape_27.setTransform(222.475,387.4394);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f().s("#000000").ss(13,1,1).p("AlpDRQIwCGCipA");
	this.shape_28.setTransform(222.25,387.3386);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_14}]},15).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_27}]},1).to({state:[{t:this.shape_28}]},1).to({state:[{t:this.shape}]},1).wait(16));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_1_Layer_6 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_6
	this.shape = new cjs.Shape();
	this.shape.graphics.f().ls(["#B03F1D","#170E06"],[0.192,0.957],-33.3,0,33.3,0).ss(13,1,1).p("ABAlLQh/AGhfAzQhfAygNBhQgMBfB2B6QB3B7E1B3");
	this.shape.setTransform(78.3404,420.95);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().ls(["#B03F1D","#170E06"],[0.192,0.957],-33.3,0,33.4,0).ss(13,1,1).p("AA/lLQh/AHhfAzQhfAygMBhQgNBgB3B5QB3B6E2B3");
	this.shape_1.setTransform(78.4406,420.925);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().ls(["#B03F1D","#170E06"],[0.192,0.957],-33.5,0,33.5,0).ss(13,1,1).p("AA8lKQh/AIheAzQhfAzgMBhQgLBgB3B4QB4B5E2B0");
	this.shape_2.setTransform(78.7495,420.85);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().ls(["#B03F1D","#170E06"],[0.192,0.957],-33.7,0,33.8,0).ss(13,1,1).p("AA1lHQh+AJheA1QheA0gKBhQgKBgB5B2QB6B3E2Bv");
	this.shape_3.setTransform(79.3118,420.675);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().ls(["#B03F1D","#170E06"],[0.192,0.957],-34.2,0,34.2,0).ss(13,1,1).p("AArlEQh+ANhcA2QhcA2gIBhQgIBgB7BzQB8B0E4Bo");
	this.shape_4.setTransform(80.2007,420.425);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().ls(["#B03F1D","#170E06"],[0.192,0.957],-34.8,0,34.7,0).ss(13,1,1).p("AAdk/Qh8ARhaA5QhbA5gFBgQgFBfB/BvQB/BwE6Be");
	this.shape_5.setTransform(81.4405,420.075);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().ls(["#B03F1D","#170E06"],[0.192,0.957],-35.6,0,35.5,0).ss(13,1,1).p("AALk4Qh7AVhYA9QhXA8gBBgQgBBfCCBqQCDBqE9BR");
	this.shape_6.setTransform(83.0246,419.65);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().ls(["#B03F1D","#170E06"],[0.192,0.957],-36.5,0,36.3,0).ss(13,1,1).p("AgIkxQh7AbhUBBQhVBAADBgQAEBeCHBkQCIBkFABC");
	this.shape_7.setTransform(84.8181,419.15);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().ls(["#B03F1D","#170E06"],[0.192,0.957],-37.3,0,37.2,0).ss(13,1,1).p("AgbkqQh7AhhRBEQhSBFAIBfQAIBeCMBeQCMBeFDAz");
	this.shape_8.setTransform(86.5644,418.65);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().ls(["#B03F1D","#170E06"],[0.192,0.957],-38,0,38,0).ss(13,1,1).p("AgskkQh6AmhOBIQhPBHAMBgQALBeCQBYQCRBYFFAm");
	this.shape_9.setTransform(88.0959,418.225);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().ls(["#B03F1D","#170E06"],[0.192,0.957],-38.6,0,38.6,0).ss(13,1,1).p("Ag6kfQh4AqhNBKQhNBLAPBfQAPBeCTBUQCTBTFIAc");
	this.shape_10.setTransform(89.2777,417.875);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().ls(["#B03F1D","#170E06"],[0.192,0.957],-39,0,39,0).ss(13,1,1).p("AhEkcQh3AthLBMQhMBNARBeQARBfCWBRQCVBQFKAV");
	this.shape_11.setTransform(90.1188,417.625);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().ls(["#B03F1D","#170E06"],[0.192,0.957],-39.2,0,39.3,0).ss(13,1,1).p("AhJkaQh4AvhKBOQhKBNASBeQASBfCYBQQCXBOFKAQ");
	this.shape_12.setTransform(90.641,417.45);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().ls(["#B03F1D","#170E06"],[0.192,0.957],-39.4,0,39.5,0).ss(13,1,1).p("AhNkYQh3AvhJBPQhLBOAUBeQATBfCYBOQCYBNFKAN");
	this.shape_13.setTransform(90.9227,417.375);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().ls(["#B03F1D","#170E06"],[0.192,0.957],-39.4,0,39.5,0).ss(13,1,1).p("AhOkYQh3AwhJBOQhKBPATBeQATBfCZBNQCYBNFLAN");
	this.shape_14.setTransform(91.0217,417.35);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().ls(["#B03F1D","#170E06"],[0.192,0.957],-39.4,0,39.4,0).ss(13,1,1).p("AhNkYQh3AvhKBPQhKBOATBeQATBfCYBOQCYBNFLAN");
	this.shape_15.setTransform(90.9559,417.375);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().ls(["#B03F1D","#170E06"],[0.192,0.957],-39.3,0,39.3,0).ss(13,1,1).p("AhKkaQh3AvhKBOQhLBOATBeQASBfCYBPQCXBOFKAQ");
	this.shape_16.setTransform(90.691,417.45);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().ls(["#B03F1D","#170E06"],[0.192,0.957],-39,0,39.1,0).ss(13,1,1).p("AhFkbQh3AthLBNQhMBMASBfQARBfCWBQQCWBQFKAT");
	this.shape_17.setTransform(90.2353,417.575);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().ls(["#B03F1D","#170E06"],[0.192,0.957],-38.7,0,38.7,0).ss(13,1,1).p("Ag9keQh4ArhMBLQhNBLAQBeQAPBfCUBTQCUBTFJAZ");
	this.shape_18.setTransform(89.5194,417.775);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().ls(["#B03F1D","#170E06"],[0.192,0.957],-38.3,0,38.1,0).ss(13,1,1).p("AgxkiQh5AnhOBJQhPBJANBfQANBeCSBXQCRBWFGAi");
	this.shape_19.setTransform(88.5077,418.075);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().ls(["#B03F1D","#170E06"],[0.192,0.957],-37.6,0,37.5,0).ss(13,1,1).p("AgjkoQh6AjhQBGQhQBGAJBfQAKBfCOBbQCNBbFFAu");
	this.shape_20.setTransform(87.2249,418.475);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().ls(["#B03F1D","#170E06"],[0.192,0.957],-36.9,0,36.7,0).ss(13,1,1).p("AgRkuQh7AehTBDQhTBCAFBfQAGBfCJBhQCKBhFCA6");
	this.shape_21.setTransform(85.6579,418.9);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().ls(["#B03F1D","#170E06"],[0.192,0.957],-36,0,36,0).ss(13,1,1).p("AABk1Qh7AZhVA+QhXA/ACBgQABBeCFBnQCFBnE/BJ");
	this.shape_22.setTransform(83.9487,419.4);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().ls(["#B03F1D","#170E06"],[0.192,0.957],-35.2,0,35.2,0).ss(13,1,1).p("AATk7Qh8AThYA7QhZA7gDBgQgDBfCBBsQCCBtE7BW");
	this.shape_23.setTransform(82.3466,419.825);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().ls(["#B03F1D","#170E06"],[0.192,0.957],-34.6,0,34.5,0).ss(13,1,1).p("AAilBQh9APhbA4QhbA4gGBgQgGBgB9BxQB+BxE6Bi");
	this.shape_24.setTransform(81.0113,420.225);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f().ls(["#B03F1D","#170E06"],[0.192,0.957],-34,0,34.1,0).ss(13,1,1).p("AAulFQh+AMhcA2QhdA1gIBgQgJBgB7B0QB7B1E3Br");
	this.shape_25.setTransform(79.9443,420.525);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f().ls(["#B03F1D","#170E06"],[0.192,0.957],-33.7,0,33.7,0).ss(13,1,1).p("AA3lIQh/AJhdA1QheA0gLBgQgKBgB4B2QB5B4E3Bx");
	this.shape_26.setTransform(79.1829,420.725);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f().ls(["#B03F1D","#170E06"],[0.192,0.957],-33.4,0,33.5,0).ss(13,1,1).p("AA8lKQh+AIhfAzQhfAzgLBgQgMBgB3B5QB4B5E2B0");
	this.shape_27.setTransform(78.6952,420.85);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f().ls(["#B03F1D","#170E06"],[0.192,0.957],-33.3,0,33.4,0).ss(13,1,1).p("AA/lLQh/AHhfAzQhfAygMBgQgNBgB3B6QB3B6E1B3");
	this.shape_28.setTransform(78.4156,420.925);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_14}]},15).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_27}]},1).to({state:[{t:this.shape_28}]},1).to({state:[{t:this.shape}]},1).wait(16));

}).prototype = p = new cjs.MovieClip();


(lib.Scene_1_Layer_11 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_11
	this.instance = new lib.pack1();
	this.instance.parent = this;
	this.instance.setTransform(145.9,279.45,0.6407,0.6407);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Scene_1_Layer_11, null, null);


(lib.Scene_1_Layer_3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_3
	this.instance = new lib.bg_vert();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Scene_1_Layer_3, null, null);


(lib.Symbol13copy2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Symbol11();
	this.instance.parent = this;
	this.instance.setTransform(-0.05,-0.3,0.4642,0.4722,0,-15.4253,-25.978,0.1,-0.6);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1).to({regX:0,regY:0,skewX:-15.425,skewY:-25.9778,x:-0.0001,y:-0.0373},0).wait(1).to({scaleX:0.4643,skewX:-15.4242,skewY:-25.977,x:-0.0004,y:0.0024},0).wait(1).to({skewX:-15.4228,skewY:-25.9757,x:-0.0009,y:0.0718},0).wait(1).to({scaleX:0.4644,skewX:-15.4206,skewY:-25.9738,x:-0.0017,y:0.1742},0).wait(1).to({scaleX:0.4646,skewX:-15.4178,skewY:-25.9711,x:-0.0028,y:0.3126},0).wait(1).to({scaleX:0.4648,scaleY:0.4721,skewX:-15.4141,skewY:-25.9678,x:-0.0041,y:0.4906},0).wait(1).to({scaleX:0.465,skewX:-15.4095,skewY:-25.9636,x:-0.0058,y:0.7116},0).wait(1).to({scaleX:0.4652,skewX:-15.4039,skewY:-25.9586,x:-0.0079,y:0.9787},0).wait(1).to({scaleX:0.4655,scaleY:0.472,skewX:-15.3973,skewY:-25.9526,x:-0.0103,y:1.294},0).wait(1).to({scaleX:0.4659,skewX:-15.3898,skewY:-25.9457,x:-0.0131,y:1.6579},0).wait(1).to({scaleX:0.4663,scaleY:0.4719,skewX:-15.3813,skewY:-25.938,x:-0.0163,y:2.068},0).wait(1).to({scaleX:0.4667,scaleY:0.4718,skewX:-15.3719,skewY:-25.9295,x:-0.0197,y:2.5183},0).wait(1).to({scaleX:0.4672,skewX:-15.3619,skewY:-25.9204,x:-0.0234,y:2.9983},0).wait(1).to({scaleX:0.4677,scaleY:0.4717,skewX:-15.3516,skewY:-25.911,x:-0.0272,y:3.4938},0).wait(1).to({scaleX:0.4682,scaleY:0.4716,skewX:-15.3413,skewY:-25.9017,x:-0.031,y:3.9882},0).wait(1).to({scaleX:0.4687,skewX:-15.3314,skewY:-25.8927,x:-0.0347,y:4.4657},0).wait(1).to({scaleX:0.4691,scaleY:0.4715,skewX:-15.3221,skewY:-25.8842,x:-0.0381,y:4.913},0).wait(1).to({scaleX:0.4695,scaleY:0.4714,skewX:-15.3136,skewY:-25.8765,x:-0.0412,y:5.3213},0).wait(1).to({scaleX:0.4699,skewX:-15.306,skewY:-25.8696,x:-0.044,y:5.6853},0).wait(1).to({scaleX:0.4702,scaleY:0.4713,skewX:-15.2994,skewY:-25.8636,x:-0.0465,y:6.0036},0).wait(1).to({scaleX:0.4704,skewX:-15.2937,skewY:-25.8584,x:-0.0486,y:6.2767},0).wait(1).to({scaleX:0.4707,skewX:-15.2889,skewY:-25.8541,x:-0.0503,y:6.5069},0).wait(1).to({scaleX:0.4709,scaleY:0.4712,skewX:-15.285,skewY:-25.8505,x:-0.0518,y:6.6969},0).wait(1).to({scaleX:0.471,skewX:-15.2818,skewY:-25.8476,x:-0.053,y:6.8499},0).wait(1).to({scaleX:0.4711,skewX:-15.2793,skewY:-25.8453,x:-0.0539,y:6.9691},0).wait(1).to({scaleX:0.4712,skewX:-15.2775,skewY:-25.8437,x:-0.0546,y:7.0573},0).wait(1).to({scaleX:0.4713,skewX:-15.2762,skewY:-25.8425,x:-0.055,y:7.1174},0).wait(1).to({skewX:-15.2755,skewY:-25.8419,x:-0.0553,y:7.1518},0).wait(1).to({regX:-0.3,regY:0.2,skewX:-15.2753,skewY:-25.8417,x:-0.15,y:6.9},0).wait(1).to({regX:0,regY:0,x:0,y:6.7385},0).wait(1).to({x:-0.0001,y:6.7026},0).wait(1).to({y:6.6398},0).wait(1).to({x:-0.0002,y:6.5476},0).wait(1).to({x:-0.0003,y:6.423},0).wait(1).to({x:-0.0005,y:6.263},0).wait(1).to({x:-0.0007,y:6.0646},0).wait(1).to({x:-0.001,y:5.825},0).wait(1).to({skewY:-25.8418,x:-0.0013,y:5.542},0).wait(1).to({x:-0.0016,y:5.2149},0).wait(1).to({x:-0.002,y:4.8447},0).wait(1).to({skewY:-25.8419,x:-0.0024,y:4.4356},0).wait(1).to({scaleX:0.4712,x:-0.0029,y:3.9953},0).wait(1).to({x:-0.0033,y:3.535},0).wait(1).to({skewY:-25.842,x:-0.0038,y:3.0685},0).wait(1).to({x:-0.0043,y:2.6104},0).wait(1).to({x:-0.0047,y:2.1737},0).wait(1).to({skewY:-25.8421,x:-0.0052,y:1.7683},0).wait(1).to({x:-0.0055,y:1.4007},0).wait(1).to({x:-0.0059,y:1.0741},0).wait(1).to({x:-0.0062,y:0.789},0).wait(1).to({skewY:-25.8422,x:-0.0064,y:0.5443},0).wait(1).to({scaleX:0.4711,x:-0.0066,y:0.338},0).wait(1).to({x:-0.0068,y:0.1675},0).wait(1).to({x:-0.007,y:0.0299},0).wait(1).to({x:-0.0071,y:-0.0773},0).wait(1).to({x:-0.0072,y:-0.1569},0).wait(1).to({y:-0.2112},0).wait(1).to({y:-0.2424},0).wait(1).to({regX:-0.2,regY:-0.2,x:-0.1,y:-0.1},0).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-12.8,-16.5,25.6,40.1);


(lib.Symbol13copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Symbol11();
	this.instance.parent = this;
	this.instance.setTransform(-0.05,-0.15,0.5573,0.5573,-7.7445,0,0,-0.1,-0.2);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1).to({regX:0,regY:0,rotation:-7.7442,x:-0.0001,y:-0.0376},0).wait(1).to({rotation:-7.7432,x:-0.0005,y:0.0013},0).wait(1).to({rotation:-7.7415,x:-0.0012,y:0.0695},0).wait(1).to({rotation:-7.7389,x:-0.0021,y:0.1698},0).wait(1).to({rotation:-7.7354,x:-0.0034,y:0.3055},0).wait(1).to({rotation:-7.7309,x:-0.0051,y:0.48},0).wait(1).to({rotation:-7.7253,x:-0.0072,y:0.6967},0).wait(1).to({rotation:-7.7186,x:-0.0097,y:0.9586},0).wait(1).to({rotation:-7.7107,x:-0.0127,y:1.2677},0).wait(1).to({rotation:-7.7015,x:-0.0161,y:1.6244},0).wait(1).to({rotation:-7.6912,x:-0.02,y:2.0265},0).wait(1).to({rotation:-7.6798,x:-0.0243,y:2.468},0).wait(1).to({scaleX:0.5572,scaleY:0.5572,rotation:-7.6677,x:-0.0288,y:2.9386},0).wait(1).to({rotation:-7.6552,x:-0.0335,y:3.4243},0).wait(1).to({rotation:-7.6428,x:-0.0382,y:3.909},0).wait(1).to({rotation:-7.6308,x:-0.0427,y:4.3772},0).wait(1).to({rotation:-7.6195,x:-0.0469,y:4.8158},0).wait(1).to({rotation:-7.6092,x:-0.0508,y:5.216},0).wait(1).to({rotation:-7.6,x:-0.0542,y:5.5729},0).wait(1).to({scaleX:0.5571,scaleY:0.5571,rotation:-7.592,x:-0.0572,y:5.8849},0).wait(1).to({rotation:-7.5851,x:-0.0598,y:6.1527},0).wait(1).to({rotation:-7.5793,x:-0.062,y:6.3784},0).wait(1).to({rotation:-7.5745,x:-0.0638,y:6.5647},0).wait(1).to({rotation:-7.5707,x:-0.0652,y:6.7147},0).wait(1).to({rotation:-7.5677,x:-0.0663,y:6.8315},0).wait(1).to({rotation:-7.5655,x:-0.0672,y:6.918},0).wait(1).to({rotation:-7.5639,x:-0.0677,y:6.9769},0).wait(1).to({rotation:-7.5631,x:-0.0681,y:7.0106},0).wait(1).to({regX:-0.2,rotation:-7.5628,x:-0.1,y:6.9},0).wait(1).to({regX:0,x:0,y:6.8885},0).wait(1).to({x:0.0001,y:6.8525},0).wait(1).to({x:0.0002,y:6.7897},0).wait(1).to({x:0.0004,y:6.6972},0).wait(1).to({x:0.0006,y:6.5724},0).wait(1).to({x:0.0009,y:6.4121},0).wait(1).to({x:0.0013,y:6.2134},0).wait(1).to({x:0.0017,y:5.9734},0).wait(1).to({x:0.0022,y:5.69},0).wait(1).to({x:0.0028,y:5.3623},0).wait(1).to({x:0.0035,y:4.9915},0).wait(1).to({x:0.0043,y:4.5817},0).wait(1).to({x:0.0051,y:4.1406},0).wait(1).to({x:0.0059,y:3.6796},0).wait(1).to({x:0.0068,y:3.2123},0).wait(1).to({x:0.0077,y:2.7535},0).wait(1).to({x:0.0085,y:2.316},0).wait(1).to({x:0.0092,y:1.9099},0).wait(1).to({x:0.0099,y:1.5418},0).wait(1).to({x:0.0105,y:1.2146},0).wait(1).to({x:0.011,y:0.929},0).wait(1).to({x:0.0115,y:0.6839},0).wait(1).to({x:0.0119,y:0.4773},0).wait(1).to({x:0.0122,y:0.3064},0).wait(1).to({x:0.0124,y:0.1686},0).wait(1).to({x:0.0126,y:0.0612},0).wait(1).to({x:0.0128,y:-0.0185},0).wait(1).to({x:0.0129,y:-0.0729},0).wait(1).to({y:-0.1041},0).wait(1).to({regX:-0.1,regY:-0.2,x:-0.05,y:-0.1},0).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-14.4,-16.1,28.8,39);


(lib.Symbol13 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Symbol11();
	this.instance.parent = this;
	this.instance.setTransform(-0.05,-0.05,0.6906,0.6906,0,0,0,-0.1,-0.1);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1).to({regX:0,regY:0,x:0,y:0.0123},0).wait(1).to({y:0.0508},0).wait(1).to({y:0.1182},0).wait(1).to({y:0.2175},0).wait(1).to({y:0.3519},0).wait(1).to({y:0.5247},0).wait(1).to({y:0.7392},0).wait(1).to({y:0.9984},0).wait(1).to({y:1.3043},0).wait(1).to({y:1.6575},0).wait(1).to({y:2.0556},0).wait(1).to({y:2.4926},0).wait(1).to({y:2.9584},0).wait(1).to({y:3.4392},0).wait(1).to({y:3.919},0).wait(1).to({y:4.3824},0).wait(1).to({y:4.8166},0).wait(1).to({y:5.2128},0).wait(1).to({y:5.5661},0).wait(1).to({y:5.875},0).wait(1).to({y:6.14},0).wait(1).to({y:6.3634},0).wait(1).to({y:6.5478},0).wait(1).to({y:6.6963},0).wait(1).to({y:6.812},0).wait(1).to({y:6.8976},0).wait(1).to({y:6.9559},0).wait(1).to({y:6.9893},0).wait(1).to({regX:-0.1,regY:-0.1,x:-0.05,y:6.95},0).wait(1).to({regX:0,regY:0,x:0,y:6.9885},0).wait(1).to({y:6.9526},0).wait(1).to({y:6.8899},0).wait(1).to({y:6.7977},0).wait(1).to({y:6.6731},0).wait(1).to({y:6.5131},0).wait(1).to({y:6.3148},0).wait(1).to({y:6.0753},0).wait(1).to({y:5.7924},0).wait(1).to({y:5.4654},0).wait(1).to({y:5.0953},0).wait(1).to({y:4.6864},0).wait(1).to({y:4.2462},0).wait(1).to({y:3.786},0).wait(1).to({y:3.3198},0).wait(1).to({y:2.8618},0).wait(1).to({y:2.4252},0).wait(1).to({y:2.02},0).wait(1).to({y:1.6525},0).wait(1).to({y:1.326},0).wait(1).to({y:1.041},0).wait(1).to({y:0.7964},0).wait(1).to({y:0.5902},0).wait(1).to({y:0.4197},0).wait(1).to({y:0.2822},0).wait(1).to({y:0.1749},0).wait(1).to({y:0.0954},0).wait(1).to({y:0.0411},0).wait(1).to({y:0.01},0).wait(1).to({regX:-0.1,regY:-0.1,x:-0.05,y:-0.05},0).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-15.5,-17.9,31.1,42.9);


(lib.Symbol12 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_5
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(7,1,1).p("Ag9AAIB7AA");
	this.shape.setTransform(-31.35,-4.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(60));

	// Layer_4
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(6,1,1).p("AgrAKIBXgT");
	this.shape_1.setTransform(-76,0.375);

	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(60));

	// Layer_3
	this.instance = new lib.Symbol13copy2("synched",0,false);
	this.instance.parent = this;
	this.instance.setTransform(-92.75,7.1);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(60));

	// Layer_2
	this.instance_1 = new lib.Symbol13copy("synched",39,false);
	this.instance_1.parent = this;
	this.instance_1.setTransform(-53.35,-1.25);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(19).to({startPosition:0},0).wait(41));

	// Layer_1
	this.instance_2 = new lib.Symbol13("synched",19,false);
	this.instance_2.parent = this;
	this.instance_2.setTransform(-3.35,-1.25);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(39).to({startPosition:0},0).wait(21));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-105.5,-19.2,117.7,49.8);


(lib.Symbol10 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_99 = function() {
		this.___loopingOver___ = true;
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(99).call(this.frame_99).wait(1));

	// Layer_3_obj_
	this.Layer_3 = new lib.Symbol_10_Layer_3();
	this.Layer_3.name = "Layer_3";
	this.Layer_3.parent = this;
	this.Layer_3.setTransform(35.1,49.8,1,1,0,0,0,35.1,49.8);
	this.Layer_3.depth = 0;
	this.Layer_3.isAttachedToCamera = 0
	this.Layer_3.isAttachedToMask = 0
	this.Layer_3.layerDepth = 0
	this.Layer_3.layerIndex = 0
	this.Layer_3.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_3).wait(100));

	// Layer_4_obj_
	this.Layer_4 = new lib.Symbol_10_Layer_4();
	this.Layer_4.name = "Layer_4";
	this.Layer_4.parent = this;
	this.Layer_4.setTransform(28,49.5,1,1,0,0,0,28,49.5);
	this.Layer_4.depth = 0;
	this.Layer_4.isAttachedToCamera = 0
	this.Layer_4.isAttachedToMask = 0
	this.Layer_4.layerDepth = 0
	this.Layer_4.layerIndex = 1
	this.Layer_4.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_4).wait(100));

	// Layer_2_obj_
	this.Layer_2 = new lib.Symbol_10_Layer_2();
	this.Layer_2.name = "Layer_2";
	this.Layer_2.parent = this;
	this.Layer_2.setTransform(75.5,7.5,1,1,0,0,0,75.5,7.5);
	this.Layer_2.depth = 0;
	this.Layer_2.isAttachedToCamera = 0
	this.Layer_2.isAttachedToMask = 0
	this.Layer_2.layerDepth = 0
	this.Layer_2.layerIndex = 2
	this.Layer_2.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_2).wait(100));

	// Layer_1_obj_
	this.Layer_1 = new lib.Symbol_10_Layer_1();
	this.Layer_1.name = "Layer_1";
	this.Layer_1.parent = this;
	this.Layer_1.depth = 0;
	this.Layer_1.isAttachedToCamera = 0
	this.Layer_1.isAttachedToMask = 0
	this.Layer_1.layerDepth = 0
	this.Layer_1.layerIndex = 3
	this.Layer_1.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_1).wait(100));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-24.9,-8.6,125.30000000000001,93.6);


(lib.Symbol9 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_39 = function() {
		this.___loopingOver___ = true;
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(39).call(this.frame_39).wait(1));

	// Layer_3_obj_
	this.Layer_3 = new lib.Symbol_9_Layer_3();
	this.Layer_3.name = "Layer_3";
	this.Layer_3.parent = this;
	this.Layer_3.setTransform(55.6,45.9,1,1,0,0,0,55.6,45.9);
	this.Layer_3.depth = 0;
	this.Layer_3.isAttachedToCamera = 0
	this.Layer_3.isAttachedToMask = 0
	this.Layer_3.layerDepth = 0
	this.Layer_3.layerIndex = 0
	this.Layer_3.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_3).wait(40));

	// Layer_2_obj_
	this.Layer_2 = new lib.Symbol_9_Layer_2();
	this.Layer_2.name = "Layer_2";
	this.Layer_2.parent = this;
	this.Layer_2.setTransform(73.8,-0.6,1,1,0,0,0,73.8,-0.6);
	this.Layer_2.depth = 0;
	this.Layer_2.isAttachedToCamera = 0
	this.Layer_2.isAttachedToMask = 0
	this.Layer_2.layerDepth = 0
	this.Layer_2.layerIndex = 1
	this.Layer_2.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_2).wait(40));

	// Layer_1_obj_
	this.Layer_1 = new lib.Symbol_9_Layer_1();
	this.Layer_1.name = "Layer_1";
	this.Layer_1.parent = this;
	this.Layer_1.depth = 0;
	this.Layer_1.isAttachedToCamera = 0
	this.Layer_1.isAttachedToMask = 0
	this.Layer_1.layerDepth = 0
	this.Layer_1.layerIndex = 2
	this.Layer_1.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_1).wait(40));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-27,-20.3,125.6,88.39999999999999);


(lib.Symbol1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_59 = function() {
		this.___loopingOver___ = true;
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(59).call(this.frame_59).wait(1));

	// Layer_9 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("Aw4qyIPJBOQgmHSABgBIRGgZQABAABCF2IBEF4I9HBxQABAAkr1lg");
	mask.setTransform(88.525,427.35);

	// Layer_6_obj_
	this.Layer_6 = new lib.Symbol_1_Layer_6();
	this.Layer_6.name = "Layer_6";
	this.Layer_6.parent = this;
	this.Layer_6.setTransform(78.4,420.9,1,1,0,0,0,78.4,420.9);
	this.Layer_6.depth = 0;
	this.Layer_6.isAttachedToCamera = 0
	this.Layer_6.isAttachedToMask = 0
	this.Layer_6.layerDepth = 0
	this.Layer_6.layerIndex = 0
	this.Layer_6.maskLayerName = 0

	var maskedShapeInstanceList = [this.Layer_6];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.Layer_6).wait(60));

	// Layer_7_obj_
	this.Layer_7 = new lib.Symbol_1_Layer_7();
	this.Layer_7.name = "Layer_7";
	this.Layer_7.parent = this;
	this.Layer_7.setTransform(222.2,387.3,1,1,0,0,0,222.2,387.3);
	this.Layer_7.depth = 0;
	this.Layer_7.isAttachedToCamera = 0
	this.Layer_7.isAttachedToMask = 0
	this.Layer_7.layerDepth = 0
	this.Layer_7.layerIndex = 1
	this.Layer_7.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_7).wait(60));

	// Layer_10_obj_
	this.Layer_10 = new lib.Symbol_1_Layer_10();
	this.Layer_10.name = "Layer_10";
	this.Layer_10.parent = this;
	this.Layer_10.setTransform(164.7,398.7,1,1,0,0,0,164.7,398.7);
	this.Layer_10.depth = 0;
	this.Layer_10.isAttachedToCamera = 0
	this.Layer_10.isAttachedToMask = 0
	this.Layer_10.layerDepth = 0
	this.Layer_10.layerIndex = 2
	this.Layer_10.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_10).wait(60));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,348,575.7);


(lib.Scene_1_Layer_12 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_12
	this.instance = new lib.Symbol12();
	this.instance.parent = this;
	this.instance.setTransform(366.35,376.75,0.5766,0.5766,-9.4945,0,0,0.1,0.3);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Scene_1_Layer_12, null, null);


(lib.Scene_1_Layer_10 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_10
	this.instance = new lib.Symbol10();
	this.instance.parent = this;
	this.instance.setTransform(122.4,428.05,0.6407,0.6407,0,0,0,0.1,0.1);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Scene_1_Layer_10, null, null);


(lib.Scene_1_Layer_9 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_9
	this.instance = new lib.Symbol9();
	this.instance.parent = this;
	this.instance.setTransform(239.3,367.85,0.6407,0.6407,0,0,0,0.1,0.1);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Scene_1_Layer_9, null, null);


(lib.Scene_1_Layer_7 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_7
	this.instance = new lib.Symbol1();
	this.instance.parent = this;
	this.instance.setTransform(123.05,545.65,0.6407,0.6407,0,0,0,122,531);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Scene_1_Layer_7, null, null);


(lib.Scene_1_Layer_5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_5
	this.instance = new lib.Symbol6();
	this.instance.parent = this;
	this.instance.setTransform(191.6,568.65,0.6407,0.6407,0,0,0,92,-1.6);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Scene_1_Layer_5, null, null);


// stage content:
(lib.Aug2_Vert = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	this.___GetDepth___ = function(obj) {
		var depth = obj.depth;
		var cameraObj = this.___camera___instance;
		if(cameraObj && cameraObj.depth && obj.isAttachedToCamera)
		{
			depth += depth + cameraObj.depth;
		}
		return depth;
		}
	this.___needSorting___ = function() {
		for (var i = 0; i < this.getNumChildren() - 1; i++)
		{
			var prevDepth = this.___GetDepth___(this.getChildAt(i));
			var nextDepth = this.___GetDepth___(this.getChildAt(i + 1));
			if (prevDepth < nextDepth)
				return true;
		}
		return false;
	}
	this.___sortFunction___ = function(obj1, obj2) {
		return (this.exportRoot.___GetDepth___(obj2) - this.exportRoot.___GetDepth___(obj1));
	}
	this.on('tick', function (event){
		var curTimeline = event.currentTarget;
		if (curTimeline.___needSorting___()){
			this.sortChildren(curTimeline.___sortFunction___);
		}
	});

	// Layer_10_obj_
	this.Layer_10 = new lib.Scene_1_Layer_10();
	this.Layer_10.name = "Layer_10";
	this.Layer_10.parent = this;
	this.Layer_10.setTransform(146.5,444.1,1,1,0,0,0,146.5,444.1);
	this.Layer_10.depth = 0;
	this.Layer_10.isAttachedToCamera = 0
	this.Layer_10.isAttachedToMask = 0
	this.Layer_10.layerDepth = 0
	this.Layer_10.layerIndex = 0
	this.Layer_10.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_10).wait(1));

	// Layer_9_obj_
	this.Layer_9 = new lib.Scene_1_Layer_9();
	this.Layer_9.name = "Layer_9";
	this.Layer_9.parent = this;
	this.Layer_9.setTransform(262.1,382.8,1,1,0,0,0,262.1,382.8);
	this.Layer_9.depth = 0;
	this.Layer_9.isAttachedToCamera = 0
	this.Layer_9.isAttachedToMask = 0
	this.Layer_9.layerDepth = 0
	this.Layer_9.layerIndex = 1
	this.Layer_9.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_9).wait(1));

	// Layer_12_obj_
	this.Layer_12 = new lib.Scene_1_Layer_12();
	this.Layer_12.name = "Layer_12";
	this.Layer_12.parent = this;
	this.Layer_12.setTransform(339.9,382.3,1,1,0,0,0,339.9,382.3);
	this.Layer_12.depth = 0;
	this.Layer_12.isAttachedToCamera = 0
	this.Layer_12.isAttachedToMask = 0
	this.Layer_12.layerDepth = 0
	this.Layer_12.layerIndex = 2
	this.Layer_12.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_12).wait(1));

	// Layer_7_obj_
	this.Layer_7 = new lib.Scene_1_Layer_7();
	this.Layer_7.name = "Layer_7";
	this.Layer_7.parent = this;
	this.Layer_7.setTransform(123.9,460.9,1,1,0,0,0,123.9,460.9);
	this.Layer_7.depth = 0;
	this.Layer_7.isAttachedToCamera = 0
	this.Layer_7.isAttachedToMask = 0
	this.Layer_7.layerDepth = 0
	this.Layer_7.layerIndex = 3
	this.Layer_7.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_7).wait(1));

	// Layer_11_obj_
	this.Layer_11 = new lib.Scene_1_Layer_11();
	this.Layer_11.name = "Layer_11";
	this.Layer_11.parent = this;
	this.Layer_11.setTransform(261.2,441.6,1,1,0,0,0,261.2,441.6);
	this.Layer_11.depth = 0;
	this.Layer_11.isAttachedToCamera = 0
	this.Layer_11.isAttachedToMask = 0
	this.Layer_11.layerDepth = 0
	this.Layer_11.layerIndex = 4
	this.Layer_11.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_11).wait(1));

	// Layer_5_obj_
	this.Layer_5 = new lib.Scene_1_Layer_5();
	this.Layer_5.name = "Layer_5";
	this.Layer_5.parent = this;
	this.Layer_5.setTransform(132.7,569.7,1,1,0,0,0,132.7,569.7);
	this.Layer_5.depth = 0;
	this.Layer_5.isAttachedToCamera = 0
	this.Layer_5.isAttachedToMask = 0
	this.Layer_5.layerDepth = 0
	this.Layer_5.layerIndex = 5
	this.Layer_5.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_5).wait(1));

	// Layer_3_obj_
	this.Layer_3 = new lib.Scene_1_Layer_3();
	this.Layer_3.name = "Layer_3";
	this.Layer_3.parent = this;
	this.Layer_3.setTransform(187.5,406,1,1,0,0,0,187.5,406);
	this.Layer_3.depth = 0;
	this.Layer_3.isAttachedToCamera = 0
	this.Layer_3.isAttachedToMask = 0
	this.Layer_3.layerDepth = 0
	this.Layer_3.layerIndex = 6
	this.Layer_3.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_3).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-131.7,406,1217.7,406);
// library properties:
lib.properties = {
	id: '330EA555F22D7642AE1FC6E8BE2D8B68',
	width: 375,
	height: 812,
	fps: 24,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: [
		{src:"assets/images/bg_vert51-2.jpg", id:"bg_vert"},
		{src:"assets/images/pack151-2.png", id:"pack1"},
		{src:"assets/images/shadow51-2.png", id:"shadow"},
		{src:"assets/images/stakan51-2.png", id:"stakan"},
		{src:"assets/images/Z51-2.png", id:"Z"}
	],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['330EA555F22D7642AE1FC6E8BE2D8B68'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}


// Layer depth API : 

AdobeAn.Layer = new function() {
	this.getLayerZDepth = function(timeline, layerName)
	{
		if(layerName === "Camera")
		layerName = "___camera___instance";
		var script = "if(timeline." + layerName + ") timeline." + layerName + ".depth; else 0;";
		return eval(script);
	}
	this.setLayerZDepth = function(timeline, layerName, zDepth)
	{
		const MAX_zDepth = 10000;
		const MIN_zDepth = -5000;
		if(zDepth > MAX_zDepth)
			zDepth = MAX_zDepth;
		else if(zDepth < MIN_zDepth)
			zDepth = MIN_zDepth;
		if(layerName === "Camera")
		layerName = "___camera___instance";
		var script = "if(timeline." + layerName + ") timeline." + layerName + ".depth = " + zDepth + ";";
		eval(script);
	}
	this.removeLayer = function(timeline, layerName)
	{
		if(layerName === "Camera")
		layerName = "___camera___instance";
		var script = "if(timeline." + layerName + ") timeline.removeChild(timeline." + layerName + ");";
		eval(script);
	}
	this.addNewLayer = function(timeline, layerName, zDepth)
	{
		if(layerName === "Camera")
		layerName = "___camera___instance";
		zDepth = typeof zDepth !== 'undefined' ? zDepth : 0;
		var layer = new createjs.MovieClip();
		layer.name = layerName;
		layer.depth = zDepth;
		layer.layerIndex = 0;
		timeline.addChild(layer);
	}
}


})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;

$(function () {
  var canvas, stage, exportRoot, anim_container, dom_overlay_container, fnStartAnimation;
  function init() {
    canvas = document.getElementById("canvas51-2");
    anim_container = document.getElementById("animation_container51-2");
    dom_overlay_container = document.getElementById("dom_overlay_container51-2");
    var comp=AdobeAn.getComposition("330EA555F22D7642AE1FC6E8BE2D8B68");
    var lib=comp.getLibrary();
    var loader = new createjs.LoadQueue(false);
    loader.addEventListener("fileload", function(evt){handleFileLoad(evt,comp)});
    loader.addEventListener("complete", function(evt){handleComplete(evt,comp)});
    var lib=comp.getLibrary();
    loader.loadManifest(lib.properties.manifest);
  }
  function handleFileLoad(evt, comp) {
    var images=comp.getImages();
    if (evt && (evt.item.type == "image")) { images[evt.item.id] = evt.result; }
  }
  function handleComplete(evt,comp) {
    //This function is always called, irrespective of the content. You can use the variable "stage" after it is created in token create_stage.
    var lib=comp.getLibrary();
    var ss=comp.getSpriteSheet();
    var queue = evt.target;
    var ssMetadata = lib.ssMetadata;
    for(i=0; i<ssMetadata.length; i++) {
      ss[ssMetadata[i].name] = new createjs.SpriteSheet( {"images": [queue.getResult(ssMetadata[i].name)], "frames": ssMetadata[i].frames} )
    }
    var preloaderDiv = document.getElementById("_preload_div_51");
    preloaderDiv.style.display = 'none';
    canvas.style.display = 'block';
    exportRoot = new lib.Aug2_Vert();
    stage = new lib.Stage(canvas);
    //Registers the "tick" event listener.
    fnStartAnimation = function() {
      stage.addChild(exportRoot);
      createjs.Ticker.setFPS(lib.properties.fps);
      createjs.Ticker.addEventListener("tick", stage)
      stage.addEventListener("tick", handleTick)
      function getProjectionMatrix(container, totalDepth) {
        var focalLength = 528.25;
        var projectionCenter = { x : lib.properties.width/2, y : lib.properties.height/2 };
        var scale = (totalDepth + focalLength)/focalLength;
        var scaleMat = new createjs.Matrix2D;
        scaleMat.a = 1/scale;
        scaleMat.d = 1/scale;
        var projMat = new createjs.Matrix2D;
        projMat.tx = -projectionCenter.x;
        projMat.ty = -projectionCenter.y;
        projMat = projMat.prependMatrix(scaleMat);
        projMat.tx += projectionCenter.x;
        projMat.ty += projectionCenter.y;
        return projMat;
      }
      function handleTick(event) {
        var cameraInstance = exportRoot.___camera___instance;
        if(cameraInstance !== undefined && cameraInstance.pinToObject !== undefined)
        {
          cameraInstance.x = cameraInstance.pinToObject.x + cameraInstance.pinToObject.pinOffsetX;
          cameraInstance.y = cameraInstance.pinToObject.y + cameraInstance.pinToObject.pinOffsetY;
          if(cameraInstance.pinToObject.parent !== undefined && cameraInstance.pinToObject.parent.depth !== undefined)
            cameraInstance.depth = cameraInstance.pinToObject.parent.depth + cameraInstance.pinToObject.pinOffsetZ;
        }
        applyLayerZDepth(exportRoot);
      }
      function applyLayerZDepth(parent)
      {
        var cameraInstance = parent.___camera___instance;
        var focalLength = 528.25;
        var projectionCenter = { 'x' : 0, 'y' : 0};
        if(parent === exportRoot)
        {
          var stageCenter = { 'x' : lib.properties.width/2, 'y' : lib.properties.height/2 };
          projectionCenter.x = stageCenter.x;
          projectionCenter.y = stageCenter.y;
        }
        for(child in parent.children)
        {
          var layerObj = parent.children[child];
          if(layerObj == cameraInstance)
            continue;
          applyLayerZDepth(layerObj, cameraInstance);
          if(layerObj.layerDepth === undefined)
            continue;
          if(layerObj.currentFrame != layerObj.parent.currentFrame)
          {
            layerObj.gotoAndPlay(layerObj.parent.currentFrame);
          }
          var matToApply = new createjs.Matrix2D;
          var cameraMat = new createjs.Matrix2D;
          var totalDepth = layerObj.layerDepth ? layerObj.layerDepth : 0;
          var cameraDepth = 0;
          if(cameraInstance && !layerObj.isAttachedToCamera)
          {
            var mat = cameraInstance.getMatrix();
            mat.tx -= projectionCenter.x;
            mat.ty -= projectionCenter.y;
            cameraMat = mat.invert();
            cameraMat.prependTransform(projectionCenter.x, projectionCenter.y, 1, 1, 0, 0, 0, 0, 0);
            cameraMat.appendTransform(-projectionCenter.x, -projectionCenter.y, 1, 1, 0, 0, 0, 0, 0);
            if(cameraInstance.depth)
              cameraDepth = cameraInstance.depth;
          }
          if(layerObj.depth)
          {
            totalDepth = layerObj.depth;
          }
          //Offset by camera depth
          totalDepth -= cameraDepth;
          if(totalDepth < -focalLength)
          {
            matToApply.a = 0;
            matToApply.d = 0;
          }
          else
          {
            if(layerObj.layerDepth)
            {
              var sizeLockedMat = getProjectionMatrix(parent, layerObj.layerDepth);
              if(sizeLockedMat)
              {
                sizeLockedMat.invert();
                matToApply.prependMatrix(sizeLockedMat);
              }
            }
            matToApply.prependMatrix(cameraMat);
            var projMat = getProjectionMatrix(parent, totalDepth);
            if(projMat)
            {
              matToApply.prependMatrix(projMat);
            }
          }
          layerObj.transformMatrix = matToApply;
        }
      }
    }
    //Code to support hidpi screens and responsive scaling.
    function makeResponsive(isResp, respDim, isScale, scaleType) {
      var lastW, lastH, lastS=1;
      window.addEventListener('resize', resizeCanvas);
      resizeCanvas();
      function resizeCanvas() {
        var w = lib.properties.width, h = lib.properties.height;
        var iw = window.innerWidth, ih=window.innerHeight;
        var pRatio = window.devicePixelRatio || 1, xRatio=iw/w, yRatio=ih/h, sRatio=1;
        if(isResp) {
          if((respDim=='width'&&lastW==iw) || (respDim=='height'&&lastH==ih)) {
            sRatio = lastS;
          }
          else if(!isScale) {
            if(iw<w || ih<h)
              sRatio = Math.min(xRatio, yRatio);
          }
          else if(scaleType==1) {
            sRatio = Math.min(xRatio, yRatio);
          }
          else if(scaleType==2) {
            sRatio = Math.max(xRatio, yRatio);
          }
        }
        canvas.width = w*pRatio*sRatio;
        canvas.height = h*pRatio*sRatio;
        canvas.style.width = anim_container.style.width = dom_overlay_container.style.width = preloaderDiv.style.width = w*sRatio+'px';
        canvas.style.height = anim_container.style.height = dom_overlay_container.style.height = preloaderDiv.style.height = h*sRatio+'px';
        stage.scaleX = pRatio*sRatio;
        stage.scaleY = pRatio*sRatio;
        lastW = iw; lastH = ih; lastS = sRatio;
        stage.tickOnUpdate = false;
        stage.update();
        stage.tickOnUpdate = true;
      }
    }
    makeResponsive(true,'both',true,2);
    AdobeAn.compositionLoaded(lib.properties.id);
    fnStartAnimation();
  }
  init();
})