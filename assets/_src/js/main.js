var previousScroll = 0;

$(function() {
    $('.lazy').Lazy();

    // object-fit polyfill initialization

    if ($('.js-obj-f').length) {
        $(function () { objectFitImages('.js-obj-f') });
    };

    var arImages = $('.ar-section__images');

    if (arImages.length) {
        var swipeRight = $('.js-swipe-right');

        arImages.on('scroll', function(event){
            var sl = $(this).scrollLeft();

            if (sl > 0) {
                swipeRight.addClass('_hide');
            } else {
                swipeRight.removeClass('_hide');
            }
        });

        swipeRight.on('click', function () {
            arImages.animate({scrollLeft:200}, 300);
        });
    }

    if ($('.js-panorama-nav').length) {
        var panoramaSlider = new Swiper('.js-panorama-nav', {
            slidesPerView: 1,
            loop: true,
            navigation: {
                nextEl: '.js-panorama-next',
                prevEl: '.js-panorama-prev',
            }
        });

        $('.js-panorama-next, .js-panorama-prev').on('click', function() {
          $('.js-panorama-next, .js-panorama-prev').addClass('disabled');
        });

        panoramaSlider.on('transitionEnd', function() {
          var descr = $('.js-panorama-descr');
          var panoramas = $('.js-panoramas');
          var descrItems = descr.find('p');
          var panoramasItems = panoramas.find('iframe');

          descrItems.removeClass('active');
          descrItems.eq(panoramaSlider.realIndex).addClass('active');
          panoramasItems.removeClass('active');
          panoramasItems.eq(panoramaSlider.realIndex).addClass('active');
          setTimeout(function(){
            $('.js-panorama-next, .js-panorama-prev').removeClass('disabled');
          }, 500)
        });
    }

    $('.js-panorama-start-btn').on('click', function(e) {
      e.preventDefault();
      $('.js-panorama-wrap').addClass('_show');
    });

    if ($('.js-parmalat-products').length) {
        var parmalatSlider = new Swiper('.js-parmalat-products', {
            slidesPerView: 3,
            freeMode: true,
            pagination: {
                el: '.swiper-pagination',
                clickable: true
            },
            navigation: {
                nextEl: '.js-slider-next',
                prevEl: '.js-slider-prev',
            },
            slidesPerGroup: 3,
            breakpoints: {
                767: {
                    slidesPerView: 1,
                    slidesPerGroup: 1,
                    freeMode: false
                }
            }
        })
    }

    if ($('.js-scrollable').length) {
        $('.js-scrollable').mCustomScrollbar();
    }

    // if ($('.js-p-video').length) {
    //     console.log($('.js-p-video'))
    //     function playVideo() {
    //         $('.js-p-video').get(0).play();
    //         $('.js-p-video').addClass('playing');
    //         $('.js-p-video-button').addClass('hide');
    //     };
    //     function stopVideo() {
    //         $('.js-p-video').get(0).pause();
    //         $('.js-p-video').removeClass('playing');
    //         $('.js-p-video-button').removeClass('hide');
    //     };
    //     $('.js-p-video').on('click', function(e) {
    //         console.log(e.target)
    //         if (e.target.classList.contains('playing')) {
    //             stopVideo();
    //         } else {
    //             playVideo();
    //         }
    //     })
    // }


    var runPreloader = function () {
        var wrap = $('.js-n-preloader'),
            preloader = $('.n-preloader-block');

        preloader.fadeIn(function () {
            wrap.addClass('in');
        });

        setTimeout(function () {
            preloader.fadeOut();
        }, 2500);
    };

    runPreloader();

    if ($('.js-search').length) {
        $('.js-search-trigger').on('click', function() {
            $('.js-search').toggleClass('show');
        });

        $('.js-search-close').on('click', function() {
            $('.js-search').removeClass('show');
        });
    }

    var steps = [0, 25, 50, 75, 100];

    steps.forEach(function(item, i, steps) {
        setTimeout(function() {
            $('.n-step').text(item);
            $('.n-preloader-icon').removeClass('active');
            $('.n-preloader-icon:eq(' + i +')').addClass('active');
        }, i * 500);
    });


    if($('.sticky').length) {
        var elements = document.querySelectorAll('.sticky');
        Stickyfill.add(elements);
    };

    $('.js-modal-close').on('click', function() {
        $(this).closest('.js-modal').removeClass('_show');
        $('body').removeClass('overflow');
    });

    $('.js-modal-trigger').on('click', function() {
        var target = $(this).data('target');
        var modals = $('.js-modal');
        $.each(modals, function() {
            if ($(this).data('name') === target) {
                modalLazy($(this));
                $(this).addClass('_show');
                $('body').addClass('overflow');

                if (introSlider.length) {
                    introSlider.update();
                }
            }
        });
        return false;
    });

    function modalLazy(modal) {
        modal.find('img.customlazy').each(function () {
            var src = $(this).data('src');
            $(this).attr('src', src);
            $(this).removeClass('customlazy').removeAttr('data-src');
        });
    }

    var canvas = $('.canvas-slide').find('canvas');

    if (canvas.length){
        canvas.each(function () {
            var canvasHeight = $(this).width() / 1.588;

            $(this).css('width', '65%');
            $(this).css('height', canvasHeight);
        });

        $(window).resize(function(){
            canvas.each(function () {
                var canvasHeight;

                canvasHeight = $(this).width() / 1.588;
                $(this).css('height', canvasHeight);
            });
        });
    }

    var mobileSliderInit = function() {
        if ($(window).width() < 768) {
            $('.mobile-slider').addClass('swiper-container');
            $('.mobile-wrapper').addClass('swiper-wrapper');
            $('.mobile-slide').addClass('swiper-slide');
            $('.mobile-pagination').addClass('swiper-pagination');
            var promoSlider = new Swiper('.promo-slider', {
                pagination: {
                    el: '.swiper-pagination',
                    clickable: true
                },
                observer: true
            });
            var giftSlider = new Swiper('.gifts-slider', {
                pagination: {
                    el: '.swiper-pagination',
                    clickable: true
                },
                observer: true
            });
            var prizesSlider = new Swiper('.prizes-slider', {
                pagination: {
                    el: '.swiper-pagination',
                    clickable: true
                },
                observer: true
            });
        } else {
            $('.mobile-slider').removeClass('swiper-container');
            $('.mobile-wrapper').removeClass('swiper-wrapper');
            $('.mobile-slide').removeClass('swiper-slide');
            $('.mobile-pagination').removeClass('swiper-pagination');
        }
    };

    if ($('.mobile-slider').length) {

        mobileSliderInit();
    };

    if ($('.search-select').length) {
        $('.search-select').select2({
            width: '100%'
        });
    };

    if ($('.js-free-field').length) {
        var $input = $('.js-free-field'),
            $buffer = $('.input-hidden');

        $input.on('input', function() {
            $buffer.text($input.val());
            $input.width($buffer.width());
        });
    }

    // var gridCount = function() {
    //     if ($('.grid').length) {
    //         if ($(window).width() < 768) {
    //             var gridItems = $('.grid-item');
    //             $.each(gridItems, function() {
    //                 if ($(this).index() > 3) {
    //                     $(this).hide();
    //                 }
    //             });
    //         } else if ($(window).width() < 1025) {
    //             var gridItems = $('.grid-item');
    //             $.each(gridItems, function() {
    //                 if ($(this).index() > 5) {
    //                     $(this).hide();
    //                 }
    //             });
    //         }
    //     }
    // }
    //
    // gridCount();
    //
    // $('.js-show-more').on('click', function() {
    //     var gridItems = $('.grid-item');
    //     gridItems.fadeIn();
    //     return false;
    // });

    var introSliderOption,
        introSlider,
        introSliderCount = $('.intro-slider').find('.swiper-slide').length;

    introSliderOption = {
        autoplay: {
            delay: 14000,
            waitForTransition: false
        },
        on: {
            init: function () {
                setTimeout(function () {
                    var activeSlide = $('.intro-slider').find('.swiper-slide-active'),
                        header = $('.js-header'),
                        slider = $('.intro-slider .swiper-pagination-bullets');

                    if (activeSlide.is('[data-white]')) {
                        header.addClass('_white-screen');
                        slider.addClass('_white-screen');
                    }
                }, 100);
            }
        },
        pagination: {
            el: '.swiper-pagination',
            clickable: true
        }
    };

    if (introSliderCount > 1) {
        introSliderOption.loop = true;
    }

    introSlider = new Swiper('.js-intro-slider', introSliderOption);

    introSlider.on('slideChangeTransitionStart', function () {
        var activeSlide = $('.intro-slider').find('.swiper-slide-active'),
            header = $('.js-header'),
            slider = $('.intro-slider .swiper-pagination-bullets');

        if (activeSlide.is('[data-white]')) {
            header.addClass('_white-screen');
            slider.addClass('_white-screen');
        } else {
            header.removeClass('_white-screen');
            slider.removeClass('_white-screen');
        }
    });

    $('.js-top').on('click', function() {
        $('html, body').animate({
            scrollTop: 0
        }, 600);
    })

    var toTop = function() {
        if ($('.js-top').length) {
            if ($('.aside-col__inner').length) {
                var top = $('.aside-col__inner').height() + 100;
            } else {
                top = 100;
            }

            if ($(window).scrollTop() < $(window).height()) {
                //console.log('first-screen');
                $('.js-top').css('position', 'absolute')
                            .css('top', top + 'px')
                            .css('bottom', 'auto');
            };
            if (($(window).scrollTop() > $(window).height() - $('.aside-col__inner').height() + 100 ) && $(window).scrollTop() < ($('.grid').offset().top) - $(window).height()) {
                //console.log('middle');
                $('.js-top').css('position', 'fixed')
                            .css('top', 'auto')
                            .css('bottom', '30px');
            };
            if ($(window).scrollTop() > ($('.grid').offset().top) - $(window).height()) {
                //console.log('footer')
                $('.js-top').css('position', 'absolute')
                            .css('top', 'auto')
                            .css('bottom', '30px')
            }
        }
    }

    toTop();

    $('.js-comment-expand').on('click', function() {
        $(this).closest('.comment-block').toggleClass('expanded');
    })

    var lastScrollTop = 0;
    $(window).scroll(function(event){
        toTop();
        var st = $(this).scrollTop();
        var padding = $('.js-header').height();

        if ($(this).scrollTop() > $('.intro').height()) {
            $('.js-header').addClass('_fixed');
        } else {
            $('.js-header').removeClass('_fixed');
        };

        if ($('.history').length) {
            if (($(this).scrollTop() > $('.history').offset().top) && $(this).scrollTop() < ($('.pagination-block').offset().top - 140 )) {
                $('.js-table-header').addClass('fixed');
            } else {
                $('.js-table-header').removeClass('fixed');
            }
        }

        lastScrollTop = st;
    });

    if ($('.intro').length) {
        $('.js-login-link').on('click', function() {
            if ($(window).width() > 1023) {
                $('.js-login-popup').addClass('opened');
                return false;
            } else {
                $('.js-login-popup').addClass('opened');
                $('.js-header').addClass('_full');
                $('body').addClass('overflow');
                return false;
            }
        });

        $('.js-user-block').on('click', function() {
            if ($(window).width() > 1023) {
                $('.js-profile-popup').addClass('opened');
                return false;
            } else {
                $('.js-profile-popup').addClass('opened');
                $('.js-header').addClass('_full');
                $('body').addClass('overflow');
                return false;
            }
        });
    }

    $('.js-popup-close').on('click', function() {
        $(this).closest('.js-popup').removeClass('opened');
        $('body').removeClass('overflow');

        if ($(window).width() < 1025) {
            $('.js-header').removeClass('_full');
        }
    });

    if ($('.winners-slider').length) {
        var winners500 = new Swiper('.winners500', {
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            }
        });

        var winners1000 = new Swiper('.winners1000', {
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            }
        });

        var winners3000 = new Swiper('.winners3000', {
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            }
        });

        var winnersPrizes = new Swiper('.winnersPrizes', {
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            }
        });

        var winners50000 = new Swiper('.winners50000', {
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            }
        });
    };

    if ($('.rules-slider').length) {
        var rulesSlider = new Swiper('.rules-slider', {
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            },
            pagination: {
                el: '.swiper-pagination',
                clickable: true
            },
        });
    }

    var lastScrollTop=0;
    // $(window).scroll(function() {
    //     if ($('.js-aside').length) {
    //         var imageCoordTop = $('.js-aside').offset().top;
    //         var imageCoordBottom = $('.js-cols').next().offset().top;
    //         if ($(window).scrollTop() > imageCoordBottom) {
    //             $('.aside-col__inner').css('top', '0');
    //             $('.aside-col__inner').removeClass('_sticky');
    //             return;
    //         }
    //         if ($(window).scrollTop() >= 60) {
    //             $('.aside-col__inner').css('top', $('.page-header').height());
    //             $('.aside-col__inner').addClass('_sticky');
    //             return;
    //         }
    //
    //         if ($(window).scrollTop() < 60) {
    //             $('.aside-col__inner').css('top', '0');
    //             $('.aside-col__inner').removeClass('_sticky');
    //             return;
    //         }
    //     }
    // });

    if ($('.js-question').length) {
        $('.js-question-trigger').on('click', function() {
            $(this).closest('.js-question').siblings().removeClass('opened');
            $(this).closest('.js-question').toggleClass('opened');
        });
    }

    $('.scrollable').mCustomScrollbar();

    $(window).on('resize', function () {
      if ($('._codes').length && $(window).width() < 1024) {
        $('.aside-col__inner').mCustomScrollbar("disable");
      }
    });

    $('[data-toggle="datepicker"]').datepicker({
        format: 'dd.mm.yyyy',
        autoHide: true
    });

    $('.custom-select').selectric();

    $('.custom-select-multiple').selectric({
        multiple: {
            separator: ', ',
            keepMenuOpen: false,
            maxLabelEntries: false
        }
    });

    var isSafari = /^((?!chrome|android).)*safari/i.test(navigator.userAgent);
    if (isSafari) {
        $('html').addClass('_safari');
    }

    if ($('.intro-slider').length) {
        window.setInterval(function(){
            $('.js-block-banner-1').toggleClass('_hide');
            $('.js-block-banner-2').toggleClass('_hide');
            $('.intro-slider').toggleClass('_block-2');
        }, 7000);
    }

    if ($('.js-wave').length && !isSafari) {
        $('.js-wave')
            .milkyButton();
            //.click(function(){return false;});
    };

    if ($('._video').length) {
        function hoverVideo() {
            $('video', this).get(0).play();
        };

        function hideVideo() {
            $('video', this).get(0).pause();
        };

        $('._video').hover(
            hoverVideo,
            hideVideo
        );
    };

    $('.js-menu-trigger').on('click', function() {
        $('.page-header').toggleClass('_full');
        if ($('body').hasClass('overflow')) {
            $('body').removeClass('overflow');

            if ($(window).width() < 768) {
                $('.logo').show();
            }
        } else {
            $('body').addClass('overflow');

            if ($(window).width() < 768) {
                $('.logo').hide();
            }
        }
    });

    if ($(window).width() < 1025) {
        $('.js-subnav-trigger').on('click', function() {
            $(this).closest('.main-navigation__list-item').siblings().removeClass('expanded');
            $(this).closest('.main-navigation__list-item').toggleClass('expanded');
        })
    };

    $(window).on('resize', function() {
        //gridCount();
        if ($('.mobile-slider').length) {
            mobileSliderInit();
        }
    });

    $(this).keydown(function(eventObject) {
      if (eventObject.which == 27 && !$('.js-modal._show').is('.dontClose')) {
        $('.js-modal').removeClass('_show');
        $('body').removeClass('overflow');
        $('.js-select-wrapper').removeClass('opened');
        $('.datepicker-container').addClass('datepicker-hide');
      }
    });

    $('.js-select-trigger').on('click', function() {
        $(this).closest('.js-select-wrapper').toggleClass('opened');
    });

    var delInterest = function() {
        $('.js-del').on('click', function(event) {
            var content = $(this).parent().find('.text').text();
            var el = $(event.target);
            var selectList = el.closest('.js-select-results').next('.js-select-wrapper').find('ul');

            selectList.append('<li class="js-select-option">');
            selectList.find('li:last-child').text(content);
            $(this).parent().remove();
        });
    };

    var addInterest = function() {
        $('.js-select-results').on('click', '.js-select-option', function(event) {
            var text = $(event.target).text();
            var results = $('.js-select-results').find('.select-result').length;

            if (results >= 4) return false;

            $('.js-select-results').append('<span class="select-result">');
            $('.js-select-results .select-result:last-child').append('<span class="text">');
            $('.js-select-results .select-result:last-child').append('<span class="select-result__del js-del">x</span>');
            $('.js-select-results .select-result:last-child .text').text(text);
            $(this).remove();
        });
    }

    $('.js-select-results').on('click', '.js-del', function(event) {
        var content = $(this).parent().find('.text').text();
        var el = $(event.target);
        var selectList = el.closest('.js-select-results').next('.js-select-wrapper').find('ul');
        console.log(selectList);
        selectList.append('<li class="js-select-option">');
        selectList.find('li:last-child').text(content);
        $(this).parent().remove();
        addInterest();
    });

    $('.select-dropdown').on('click', '.js-select-option', function(event) {
        var text = $(event.target).text();
        var results = $('.js-select-results').find('.select-result').length;
        if (results >= 4) return false;

        $('.js-select-results').append('<span class="select-result">');
        $('.js-select-results .select-result:last-child').append('<span class="text">');
        $('.js-select-results .select-result:last-child').append('<span class="select-result__del js-del">x</span>');
        $('.js-select-results .select-result:last-child .text').text(text);
        $(this).remove();
        delInterest();
    });

    $('.select-dropdown').on('click', function() {
        $(this).closest('.js-select-wrapper').removeClass('opened');
    });

    $(document).mouseup(function (e) {
        var tooltip = $('.js-select-wrapper');
        if (tooltip.has(e.target).length === 0) {
            tooltip.removeClass('opened');
        }
    });

    var prizeLink = $('.js-prize-block'),
        prizeOption = $('.prize-block__option'),
        prizeBack = $('.js-prize-back');

    prizeLink.on('click', function (e) {
        var target = $(this).data('target');

        e.preventDefault();

        $('#'+target).addClass('_show');
    });

    if (prizeOption.length) {
        prizeOption.hover(
            function () {
                prizeOption.not($(this)).find('img').css('opacity', '0.5');
            },
            function () {
                prizeOption.not($(this)).find('img').css('opacity', '1');
            }
        );
    }

    prizeBack.on('click', function (e) {
        e.preventDefault();
        $(this).parent().removeClass('_show');
    });

    var ua = navigator.userAgent,
        iOS = /iPad|iPhone|iPod/.test(ua),
        iOS11 = /OS 11_0|OS 11_1|OS 11_2/.test(ua);

    if ( iOS && iOS11 ) {
        $("body").addClass("iosBugFixCaret");
    }

    if ($('.landings-link-slider').length) {
        var LSlider,
            Lparam = {
            navigation: {
                prevEl: '.landings-link__prev',
                nextEl: '.landings-link__next'
            },
            slidesPerView: 3,
            spaceBetween: 6,
            observer: true
        };

        if ($('.landings-link-slide').length <= 2) {
            $.extend( Lparam, {centeredSlides: true} );
            LSlider = new Swiper('.landings-link-slider', Lparam);
        } else {
            LSlider = new Swiper('.landings-link-slider', Lparam);
        }
    }

    var friendBtn = $('.js-friend-btn'),
        friendInner = $('.js-friend-inner'),
        friendCopy = $('.js-friend-copy');

    friendBtn.on('click', function (e) {
        e.preventDefault();

        $(this).addClass('_hide');
        friendInner.addClass('_show');
    });

    friendCopy.on('click', function (e) {
        var code = $('.friend-block__input:visible').get(0);

        copyToClipboard(code);
    });

    function copyToClipboard(elem) {
        var targetId = "_hiddenCopyText_";
        var isInput = elem.tagName === "INPUT" || elem.tagName === "TEXTAREA";
        var origSelectionStart, origSelectionEnd;
        if (isInput) {

            target = elem;
            origSelectionStart = elem.selectionStart;
            origSelectionEnd = elem.selectionEnd;
        } else {

            target = document.getElementById(targetId);
            if (!target) {
                var target = document.createElement("textarea");
                target.style.position = "absolute";
                target.style.left = "-9999px";
                target.style.top = "0";
                target.id = targetId;
                document.body.appendChild(target);
            }
            target.textContent = elem.textContent;
        }

        var currentFocus = document.activeElement;
        target.focus();
        target.setSelectionRange(0, target.value.length);

        var succeed;
        try {
            succeed = document.execCommand("copy");
        } catch(e) {
            succeed = false;
        }

        if (currentFocus && typeof currentFocus.focus === "function") {
            currentFocus.focus();
        }

        if (isInput) {
            elem.setSelectionRange(origSelectionStart, origSelectionEnd);
        } else {
            target.textContent = "";
        }
        return succeed;
    }
});

$(window).on('load', function () {
    var preloader = $('.js-preloader');

    if (preloader.length) {
        preloader.fadeOut(200);
    }

    if($('.events__slider').length){
        $('.events__slider').slick({
            dots: false,
            prevArrow: '.event__prev',
            nextArrow: '.event__next',
            variableWidth: true,
            swipeToSlide: true,
            slidesToShow: 1,
            centerMode: true,
            centerPadding: '0px',
            speed: 1000,
            responsive: [
                {
                    breakpoint: 992,
                    settings: {
                        swipeToSlide: true
                    }
                }
            ]
        });

        $('.events__bg-slider').each(function () {
            var __this = $(this);
            var pager = __this.closest('.events__item-bl').find('.pager__custom');
            __this.slick({
                dots: true,
                appendDots: pager,
                arrows: false,
                fade: true,
                speed: 500
            });
        });
    }
});

$(window).scroll(function () {
    var dir = 'down';
    var y = $(window).scrollTop();


    var currentScroll = $(this).scrollTop();
    var delta = currentScroll - previousScroll;
    if (currentScroll > previousScroll){
        dir = 'down';
    } else {
        dir = 'up';
    }
    previousScroll = currentScroll;

    $('.splash').each(function () {
        var sp_y = $(this).data('y');
        var koef = $(this).data('koef');
        var shift = delta * koef;
        sp_y += shift;
        $(this).data('y', sp_y);
        $(this).css("transform","translateY(" + sp_y + "px)");
    });

    if ($('.scroll-down').length) {
        if ($(window).scrollTop() > $('.intro').outerHeight() - $(window).height()) {
            $('.scroll-down').fadeOut();
        } else {
            $('.scroll-down').fadeIn();
        };
    };
});

$('.scroll-down').on('click', function() {
    var secondEl = $('main').children()[1].offsetTop;
    $('html, body').animate({
        scrollTop: secondEl
    }, 600);
    return false;
});

var topy = 0;
var delta = 0;
var controller;
var scene2, scene4, scene2, scene3;

$(document).ready(function($){
    if ($('.front').length) {
        animations();
    }
});

$(window).scroll(function() {
    clearTimeout($.data(this, 'scrollTimer'));
});


function wheel(event) {
    var delta = 0;
    if (event.wheelDelta) delta = event.wheelDelta / 120;
    else if (event.detail) delta = -event.detail / 3;

    handle(delta);
    if (event.preventDefault) event.preventDefault();
    event.returnValue = false;
}

function handle(delta) {
    var time = 400;
    var distance = 300;

    $('html, body').stop().animate({
        scrollTop: $(window).scrollTop() - (distance * delta)
    }, time );
}

function animations() {
    if(controller){
        console.log('destroy');
        controller.destroy('reset');
        controller = null;
        scene1.destroy('reset');
        scene2.destroy('reset');
        scene3.destroy('reset');
        scene4.destroy('reset');
        scene2.destroy('reset');
        scene3.destroy('reset');
        var scene1 = null;
        var scene2 = null;
        var scene3 = null;
        var scene4 = null;
        var scene5 = null;
        var scene6 = null;
    }

    if($('body').hasClass('front')){
        controller = new ScrollMagic.Controller();

        var timeline1 = new TimelineMax()
            .to("._step1 .code-step__inner", 1,
                {
                    css: {opacity: 1},
                    onStart: function () {},
                    onComplete: function (){},
                    onReverseComplete: function () {}
                }
            );

        var timeline2 = new TimelineMax()
            .to("._step2 .code-step__inner", 1.3,
                {
                    css: {opacity: 1},
                    onStart: function () {},
                    onComplete: function (){},
                    onReverseComplete: function () {}
                }
            );

        var timeline3 = new TimelineMax()
            .to("._step3 .code-step__inner", 1.3,
                {
                    css: {opacity: 1},
                    onStart: function () {},
                    onComplete: function (){},
                    onReverseComplete: function () {}
                }
            );

        var timeline4 = new TimelineMax()
            .to("._step4 .code-step__inner", 1.3,
                {
                    css: {opacity: 1},
                    onStart: function () {},
                    onComplete: function (){},
                    onReverseComplete: function () {}
                }
            );

        var timeline5 = new TimelineMax()
            .to("._step5 .code-step__inner", 1.3,
                {
                    css: {opacity: 1},
                    onStart: function () {},
                    onComplete: function (){},
                    onReverseComplete: function () {}
                }
            );

        scene1 = new ScrollMagic.Scene({triggerElement: "._step1", duration: 200, offset: -50})
            .setTween(timeline1)
            .on("enter", function(){
            }).on('leave', function(){
            }).addTo(controller);

        scene2 = new ScrollMagic.Scene({triggerElement: ".code-steps", duration: 200, offset: 200})
            .setTween(timeline2)
            .on("enter", function(){
            }).on('leave', function(){
            }).addTo(controller);

        scene3 = new ScrollMagic.Scene({triggerElement: ".code-steps", duration: 200, offset: 550})
            .setTween(timeline3)
            .on("enter", function(){
            }).on('leave', function(){
            }).addTo(controller);

        scene4 = new ScrollMagic.Scene({triggerElement: ".code-steps", duration: 200, offset: 850})
            .setTween(timeline4)
            .on("enter", function(){
            }).on('leave', function(){
            }).addTo(controller);

        scene5 = new ScrollMagic.Scene({triggerElement: ".code-steps", duration: 200, offset: 1150})
            .setTween(timeline5)
            .on("enter", function(){
            }).on('leave', function(){
            }).addTo(controller);
    }
}
