(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [];


// symbols:



(lib.bg_part = function() {
	this.initialize(img.bg_part);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,661,801);


(lib.blue = function() {
	this.initialize(img.blue);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,33,12);


(lib.D = function() {
	this.initialize(img.D);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,21,55);


(lib.orange = function() {
	this.initialize(img.orange);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,33,12);


(lib.pack = function() {
	this.initialize(img.pack);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,368,508);


(lib.pan = function() {
	this.initialize(img.pan);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,424,339);


(lib.pan_shadow = function() {
	this.initialize(img.pan_shadow);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,175,180);


(lib.rays1 = function() {
	this.initialize(img.rays1);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,654,215);


(lib.red = function() {
	this.initialize(img.red);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,33,12);


(lib.rumyanec = function() {
	this.initialize(img.rumyanec);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,46,38);


(lib.shadow1 = function() {
	this.initialize(img.shadow1);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,348,55);


(lib.white = function() {
	this.initialize(img.white);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,33,12);// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.Symbol23 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.blue();
	this.instance.parent = this;
	this.instance.setTransform(-16.5,-6);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol23, new cjs.Rectangle(-16.5,-6,33,12), null);


(lib.Symbol22 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.white();
	this.instance.parent = this;
	this.instance.setTransform(-16.5,-6);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol22, new cjs.Rectangle(-16.5,-6,33,12), null);


(lib.Symbol20 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.orange();
	this.instance.parent = this;
	this.instance.setTransform(-16.5,-6);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol20, new cjs.Rectangle(-16.5,-6,33,12), null);


(lib.Symbol19 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.red();
	this.instance.parent = this;
	this.instance.setTransform(-16.5,-6);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol19, new cjs.Rectangle(-16.5,-6,33,12), null);


(lib.Symbol17 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.D();
	this.instance.parent = this;
	this.instance.setTransform(-10.5,-27.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol17, new cjs.Rectangle(-10.5,-27.5,21,55), null);


(lib.Symbol13 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_3 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("EgcfBGoMgZviNPMBsdAAnMAAACMog");
	mask.setTransform(70.6,1.05);

	// Layer_1
	this.instance = new lib.bg_part();
	this.instance.parent = this;
	this.instance.setTransform(-236.5,-400.5);

	var maskedShapeInstanceList = [this.instance];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	// Layer_2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#12498B").s().p("EhJEBo8MAAAjR3MCSJAAAMAAADR3g");
	this.shape.setTransform(-50.025,220.675);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol13, new cjs.Rectangle(-517.7,-450.9,935.4000000000001,1343.1999999999998), null);


(lib.Symbol12 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#89CA02").ss(3.4,1,1).p("AAkAlIhHhJ");
	this.shape.setTransform(-0.025,0);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol12, new cjs.Rectangle(-5.3,-5.3,10.6,10.7), null);


(lib.Symbol11 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#89CA02").ss(3.4,1,1).p("AgkAjIBJhF");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol11, new cjs.Rectangle(-5.4,-5.2,10.8,10.4), null);


(lib.Symbol9 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.shadow1();
	this.instance.parent = this;
	this.instance.setTransform(-174,-27.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol9, new cjs.Rectangle(-174,-27.5,348,55), null);


(lib.Symbol8 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.pan_shadow();
	this.instance.parent = this;
	this.instance.setTransform(-87.5,-90);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol8, new cjs.Rectangle(-87.5,-90,175,180), null);


(lib.Symbol7 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.pan();
	this.instance.parent = this;
	this.instance.setTransform(-212,-169.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol7, new cjs.Rectangle(-212,-169.5,424,339), null);


(lib.Symbol6 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(13,1,1).p("AAJhPQAGBQgYBP");
	this.shape.setTransform(-0.0079,0);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol6, new cjs.Rectangle(-7.5,-14.4,15,28.9), null);


(lib.Symbol5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(13,1,1).p("ABBg5QhJApg4BK");
	this.shape.setTransform(5.95,7.075);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	// Layer_1
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(13,1,1).p("AB4AWQiMiQhjCn");
	this.shape_1.setTransform(0.025,0.0342);

	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol5, new cjs.Rectangle(-18.5,-11,37.4,30.4), null);


(lib.Symbol4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(13,1,1).p("AA7BWQi1g6Bjhx");
	this.shape.setTransform(-9.5376,-8.275);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	// Layer_1
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(13,1,1).p("AAABXQiMhKDGhj");
	this.shape_1.setTransform(0.0177,0);

	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol4, new cjs.Rectangle(-21.9,-23.3,34.3,38.5), null);


(lib.Symbol2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.rumyanec();
	this.instance.parent = this;
	this.instance.setTransform(-23,-19);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol2, new cjs.Rectangle(-23,-19,46,38), null);


(lib.Symbol1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.rumyanec();
	this.instance.parent = this;
	this.instance.setTransform(-23,-19);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol1, new cjs.Rectangle(-23,-19,46,38), null);


(lib.Symbol_18_Layer_2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.instance = new lib.rays1();
	this.instance.parent = this;
	this.instance.setTransform(-461,-52);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(119));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_15_Layer_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#89CA02").s().p("AgTBHQgCgIAAgXIAAgoIAAgmIAAgdQABgGAFAAQAGAAACAKIAAAUIgBBCIADgLQAKgeAAgUIACgNIACgKQABgIAEgCQAFgCABAFQACAOAAAbIAAArIAAAYIgBAXQAAALgHgCQgDACgDgFQgBgEAAgGIAEhBQgLA4gGAUQgEAEgEAAQgDAAgCgDg");
	this.shape.setTransform(19.8,22.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#89CA02").s().p("AgYBHQgBgUACgtQADgtAAgSQgBgGABgDQABgEADgBQAEgCADADIABAQIAAARQAEgDAFgNQAGgOAEgDIAFgCQABgBABAAQAAABABAAQAAAAABAAQAAABABABQABADgGALIgTAqQAIAQAPAnQAHASgBAGQgBADgEABQgDACgCgCQgEgCgCgIQgIgbgLgeQgCANAAA3QAAADgHABQgGAAAAgDg");
	this.shape_1.setTransform(14.3982,21.9958);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#89CA02").s().p("AgDBLQgHgBgEgDQgEgDAAgQIgCgzIgBgOIAAgQIgBgSIgBgQQACgJANgCQAMgBAJAHQAGAGACAEQAEALgDAKQgDAKgJAHQAHAMACAJQAGAWgCATQgDAbgMAFQgDABgEAAIgEAAgAgGgNIAAAcIABAeQABARACABQADABAEgFQADgEABgEQAEgWgEgWQgEgUgKgCIgBACgAgIg/IABAQIABARQAHABAGgGQAFgHgCgJQgBgGgFgDQgEgDgEAAIgEAAg");
	this.shape_2.setTransform(8.2667,21.7284);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#89CA02").s().p("AgQA/QgEgHgBgEIAAgLIAAgXIABgZIAAgYQAAgZAEgGQAFgIAGgBQAPgDAJAMQADAFAAAOIAAAfQABANgCANIAAAMIgBAOQgBARgFAHQgEAIgHAAIgDABQgKAAgGgKgAgBg7QgGADgBAJIAAATIAAAcIAAAZIgBAMIAAAKQABALAJAAQAFAAAAgKIABgIIABgHQAGghgEgrQgBgIgCgFQAAAAgBAAQAAgBAAAAQgBgBAAAAQgBAAgBgBIgDAAIgBAAg");
	this.shape_3.setTransform(2.45,21.904);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#89CA02").s().p("AgYBHQgBgUACgtQADgtAAgSQgBgGABgDQABgEADgBQAEgCADADIABAQIAAARQAEgDAFgNQAGgOAEgDIAFgCQABgBABAAQAAABABAAQAAAAABAAQAAABABABQABADgGALIgTAqQAIAQAPAnQAHASgBAGQgBADgEABQgDACgCgCQgEgCgCgIQgIgbgLgeQgCANAAA3QAAADgHABQgGAAAAgDg");
	this.shape_4.setTransform(-2.8518,21.9958);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#89CA02").s().p("AgaBHQgDgFABgTIADgSIADgSIAGgdIAGgcQADgNACgFQADgKAEADQADACACAPIAHAtIAJAtQAHAbgCAFQgJAGgDgIIgFgTIgHgiIgFAAIgGAAQgDAMgCARQgEAdgBAAIgFACQAAAAgBgBQAAAAgBAAQAAAAgBAAQAAgBgBAAgAgCgRIgDAQIAGgBQAAgTgCgBIAAAAIgBAFg");
	this.shape_5.setTransform(-8.9493,21.9614);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#89CA02").s().p("AgRBIQgEgBABgLIAAgKIABgKQAAgtgBgOIgBgNIAAgRQAAgVAGAAIANgBIASgBQAGAAgCAQIAAAIIABAwIABAyQAAAQgEAEQgCADgFgCQgCgCAAgDQAAgPgBgfQgCgeABgOIABghIgDAAQgMgBABACQgCASACAkQABAkgBASQgBAVgHAAIgDgBg");
	this.shape_6.setTransform(-14.5972,22.0083);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#89CA02").s().p("AgIBHQgDgBAEgZIAGgkIAFgjIgUgaQgGgIAAgCQAAgEADgBQAEgCADACQAEACAGAJQAGAJADACIACgKQABgIABgDQACgEADgBQADgCAEADQACABgEAeIgFAZIgDAVIgEAZIgFAZQgCAPgBAAIgCABQgFAAgCgCg");
	this.shape_7.setTransform(-20.1554,21.855);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#89CA02").s().p("AgTBUQgCgIABgXIgBgnIABgnIAAgdQABgGADAAQAHAAABALIAAATIAABCIADgLQAKgeABgUIABgMIACgLQABgIAFgCQAEgBABAEQACAPAAAaIAAArIAAAYIgBAXQgBAMgFgCQgEABgDgFQgBgEAAgGIADhCQgJA6gIATQgDAEgEAAQgDAAgCgDgAgChAIgEgDIgEgEIgBgCIgCgDQgEgEABgDQABgBAAgBQABgBAAAAQABAAAAAAQABAAABABIACAEIACACIAEADIABABIAFABIABABIABgBIABAAIABgBIAFgDIABgCIACgEQAGgBgDAGIgBADIgCAGIgCACIgBABIgCABIgBABIgCABQgBAAAAABQgBAAAAAAQgBABAAAAQgBAAgBAAQgCAAgCgCg");
	this.shape_8.setTransform(44.35,-0.4583);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#89CA02").s().p("AgQA/QgEgHgBgEIAAgLIAAgXIABgZIAAgYQABgZADgGQAEgIAHgBQAPgDAJAMQADAFAAAOIAAAfQAAANAAANIgBAMIgBAOQgBARgFAHQgFAIgGAAIgDABQgKAAgGgKgAgBg7QgGADgCAJIAAATIAAAcIAAAZIgBAMIAAAKQACALAJAAQAFAAAAgKIABgIIAAgHQAHghgEgrQgBgIgCgFQAAAAgBAAQAAgBAAAAQgBgBAAAAQgBAAAAgBIgEAAIgBAAg");
	this.shape_9.setTransform(38.7,0.654);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#89CA02").s().p("AgVA8IAAgOIAAgMIABhPIAAgTQAAgDADgCQACgCAEABQADACAAAbQAAAcABABQAAABAHAAIAIgBQABgCgBgYQgBgZACgEQACgFACgBQAFAAABACQADBKAAA+QAAAFgEACQgEACgDgCQgBgEAAgKQAAgdgCgbQgCgCgNACQgDAAACAxQABARgDAEQgBABgFAAQgFAAAAgNg");
	this.shape_10.setTransform(33.4,0.6);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#89CA02").s().p("AgVA8IAAgOIAAgMIABhPIAAgTQAAgDADgCQACgCAEABQADACAAAbQAAAcABABQAAABAHAAIAIgBQABgCgBgYQgBgZACgEQACgFACgBQAFAAABACQADBKAAA+QAAAFgEACQgEACgDgCQgBgEAAgKQAAgdgCgbQgCgCgNACQgDAAACAxQABARgDAEQgBABgFAAQgFAAAAgNg");
	this.shape_11.setTransform(28,0.6);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#89CA02").s().p("AgaBHQgDgFABgTIADgSIADgSIAGgdIAGgcQADgNACgFQADgKAEADQADACACAPIAHAtIAJAtQAHAbgCAFQgJAGgDgIIgFgTIgHgiIgFAAIgGAAQgDAMgCARQgEAdgBAAIgFACQAAAAgBgBQAAAAgBAAQAAAAgBAAQAAgBgBAAgAgCgRIgDAQIAGgBQAAgTgCgBIAAAAIgBAFg");
	this.shape_12.setTransform(22.3007,0.7114);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#89CA02").s().p("AgCBIQgGgDABgZIgBgJIAAgIQAAgqADgoQgBACgMgDQgIgCAAgGQABgHATgBIAZABQAGABABAGQACAHgDAAQgDABgHgBQgGgBAAACQgDAQAAAjIAAA2IAAAJIAAAIQgBAHgFAAIgCgBg");
	this.shape_13.setTransform(16.7235,0.6221);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#89CA02").s().p("AgQA/QgEgHgBgEIAAgLIAAgXIABgZIAAgYQAAgZAEgGQAFgIAGgBQAPgDAJAMQADAFAAAOIAAAfQABANgBANIgBAMIgBAOQgBARgFAHQgFAIgGAAIgDABQgKAAgGgKgAgBg7QgHADAAAJIAAATIAAAcIAAAZIgCAMIAAAKQACALAJAAQAFAAAAgKIABgIIAAgHQAHghgEgrQgBgIgCgFQAAAAgBAAQAAgBAAAAQgBgBAAAAQgBAAgBgBIgDAAIgBAAg");
	this.shape_14.setTransform(10.9,0.654);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#89CA02").s().p("AgDBMQgHgBgDgCQgFgEAAgQIgCgyIAAgOIgBgRIgBgRIgBgTQAAgHAFgBIAGAAIAFgBIAPgCQAKgBAAAGQABAGgLACIgJABIgHABIAAAFIABAKIABAgQAUgJAIAYQAEAXgCASQgEAbgMAFQgDABgEAAIgEAAgAgFgCIgBABIABASIAAAdQABARACABQADABAEgFQAEgEAAgEQAEgVgEgWQgCgNgHAAIgFACg");
	this.shape_15.setTransform(5.3833,0.3396);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#89CA02").s().p("AgaBHQgDgFABgTIADgSIADgSIAGgdIAGgcQADgNACgFQADgKAEADQADACACAPIAHAtIAJAtQAHAbgCAFQgJAGgDgIIgFgTIgHgiIgFAAIgGAAQgDAMgCARQgEAdgBAAIgFACQAAAAgBgBQAAAAgBAAQAAAAgBAAQAAgBgBAAgAgCgRIgDAQIAGgBQAAgTgCgBIAAAAIgBAFg");
	this.shape_16.setTransform(-0.4493,0.7114);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#89CA02").s().p("AgTBMQgEgBgBgIIAAh0QAAgZACgBQAEgCAPACQAQABAGAFQAEADACANQABAPgJAJQgDADgLABQgKABAAABQAEAAgHBKIAAAIIAAAKQgBAIgGAAIgCgBgAgLg+QgCABADAXQAAACAKAAQAJgBADgDQACgCABgIQAAgIgCgCQgDgEgKAAIgEAAQgIAAABACg");
	this.shape_17.setTransform(-5.77,0.4179);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#89CA02").s().p("AgNA+QgEgogCgtIgCgaQgBgPAHgDQABgBAVgDQAFgBADACQADADgCAEQgDAEgIABQgJAAgDACIABAZQACAQAAAIQAJACgBAFQAAADgHADIACAbIACAbQAMgDAIAEQACABgBAEQgCAEgCAAIgZACQgGAAAAgKg");
	this.shape_18.setTransform(-11.4101,0.7107);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#89CA02").s().p("AgTBMQgEgBgBgIIAAh0QAAgZACgBQAEgCAPACQAQABAGAFQAEADACANQABAPgJAJQgDADgLABQgKABAAABQAEAAgHBKIAAAIIAAAKQgBAIgGAAIgCgBgAgLg+QgCABADAXQAAACAKAAQAJgBADgDQACgCABgIQAAgIgCgCQgDgEgKAAIgEAAQgIAAABACg");
	this.shape_19.setTransform(-16.42,0.4179);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#89CA02").s().p("AgNA+QgEgogCgtIgCgaQgBgPAHgDQABgBAVgDQAFgBADACQADADgCAEQgDAEgIABQgJAAgDACIABAZQACAQAAAIQAJACgBAFQAAADgHADIACAbIACAbQAMgDAIAEQACABgBAEQgCAEgCAAIgZACQgGAAAAgKg");
	this.shape_20.setTransform(-22.0601,0.7107);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#89CA02").s().p("AgRBIQgEgBABgLIAAgKIABgKQAAgtgBgOIgBgNIAAgRQAAgVAGAAIANgBIASgBQAGAAgCAQIAAAIIABAwIABAyQAAAQgEAEQgCADgFgCQgCgCAAgDQAAgPgBgfQgCgeABgOIABghIgDAAQgMgBABACQgCASACAkQABAkgBASQgBAVgHAAIgDgBg");
	this.shape_21.setTransform(-27.3972,0.7583);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#89CA02").s().p("AgRBBQgDgCAAgFQABgGAEgCQADgCADADIAFAEIADADQACACAGgDQAFgCABgPIAAgPQgBgKgDgDQgCgBgGABQgGACgDgCQgEgBgCgDQgCgFACgEIAKgHQAIgGADgJQAGgQgDgKQgCgHgIAAQgGAAgFALIgCAGIgCAGQgDAGgFgEQgFgCABgHIABgGQAGgXATgDQAKgCAJALQAIAJgBATIgEARQAAACgEAFQgDAFAAACQAAACAEADIAEAEQAKATgFAZQgBALgGAGQgGAGgJABIgDAAQgLAAgIgIg");
	this.shape_22.setTransform(-38.3577,0.5208);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#89CA02").s().p("AgTBHQgCgIAAgXIAAgoIAAgmIAAgdQABgGAFAAQAGAAACALIAAATIgBBBIADgKQAKgfAAgTIACgNIACgKQABgIAEgCQAFgCABAFQACAPAAAaIAAArIgBAYIAAAXQAAALgHgCQgDACgCgFQgCgEAAgHIAEhAQgLA4gGAUQgDAEgFAAQgEAAgBgDg");
	this.shape_23.setTransform(-44.45,0.85);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#89CA02").s().p("AgaBHQgDgFABgTIADgSIADgSIAGgdIAGgcQADgNACgFQADgKAEADQADACACAPIAHAtIAJAtQAHAbgCAFQgJAGgDgIIgFgTIgHgiIgFAAIgGAAQgDAMgCARQgEAdgBAAIgFACQAAAAgBgBQAAAAgBAAQAAAAgBAAQAAgBgBAAgAgCgRIgDAQIAGgBQAAgTgCgBIAAAAIgBAFg");
	this.shape_24.setTransform(44.0507,-20.5386);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#89CA02").s().p("AgVA8IAAgOIABgMIAAhPIAAgTQABgDABgCQADgCAEABQADACgBAbQAAAcABABQABABAHAAIAIgBQABgBgBgZQgBgZABgEQACgFAEAAQAEAAAAABQAEBKAAA+QAAAFgEACQgEACgDgCQgCgEAAgKQAAgdgBgbQgCgCgNACQgDAAACAxQABARgDAEQAAACgHgBQgEABAAgOg");
	this.shape_25.setTransform(38.6,-20.65);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#89CA02").s().p("AgaBHQgDgFABgTIADgSIADgSIAGgdIAGgcQADgNACgFQADgKAEADQADACACAPIAHAtIAJAtQAHAbgCAFQgJAGgDgIIgFgTIgHgiIgFAAIgGAAQgDAMgCARQgEAdgBAAIgFACQAAAAgBgBQAAAAgBAAQAAAAgBAAQAAgBgBAAgAgCgRIgDAQIAGgBQAAgTgCgBIAAAAIgBAFg");
	this.shape_26.setTransform(32.9007,-20.5386);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#89CA02").s().p("AAMA3QgBgVABgjQgBglgCgRQAAgCgJgBIAAAMIAAAqIABAoQABARgCAIQgDALgJABIgCgBQgEAAgDgBQgFgCABgGQACgGAHABIACAAQADgCAAgWIAAgTIgBgOIAAgVIgBgYIABgPQgIgBgBgCQgCgKAHAAQAGAAAMABIAPABQAGAAgBAWIABAQIAAANIACBPQACALgEABIgDABQgGAAgCgSg");
	this.shape_27.setTransform(26.9642,-20.66);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#89CA02").s().p("AgNA+QgEgogCgtIgCgaQgBgPAHgDQABgBAVgDQAFgBADACQADADgCAEQgDAEgIABQgJAAgDACIABAZQACAQAAAIQAJACgBAFQAAADgHADIACAbIACAbQAMgDAIAEQACABgBAEQgCAEgCAAIgZACQgGAAAAgKg");
	this.shape_28.setTransform(21.9399,-20.5393);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#89CA02").s().p("AgPA/QgEgHgCgYIgCgiQAAgqADgLQAEgPANgCQAHgCAIAGQAFACADAKQACAGgFAEQgFADgCgEQgDgJgFgBQgGgCgCAKIgBARQgBAsACAZQADAbAHgCQAHgBABgEQABgEAEgCQADgBACADIACADQAAACgEAGQgGAJgMAAQgLAAgGgKg");
	this.shape_29.setTransform(10.3242,-20.4911);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#89CA02").s().p("AgaBHQgDgFABgTIADgSIADgSIAGgdIAGgcQADgNACgFQADgKAEADQADACACAPIAHAtIAJAtQAHAbgCAFQgJAGgDgIIgFgTIgHgiIgFAAIgGAAQgDAMgCARQgEAdgBAAIgFACQAAAAgBgBQAAAAgBAAQAAAAgBAAQAAgBgBAAgAgCgRIgDAQIAGgBQAAgTgCgBIAAAAIgBAFg");
	this.shape_30.setTransform(-0.1493,-20.5386);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#89CA02").s().p("AgYBHQgBgUACgtQADgtAAgSQgBgGABgDQABgEADgBQAEgCADADIABAQIAAARQAEgDAFgNQAGgOAEgDIAFgCQABgBABAAQAAABABAAQAAAAABAAQAAABABABQABADgGALIgTAqQAIAQAPAnQAHASgBAGQgBADgEABQgDACgCgCQgEgCgCgIQgIgbgLgeQgCANAAA3QAAADgHABQgGAAAAgDg");
	this.shape_31.setTransform(-5.6018,-20.5042);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#89CA02").s().p("AALBIQgCgEAAgKQAAgdgBgbQgCgCgNACQgNABgBgMIAAgjIAAgUIADgFQADgCADABQAEACAAAbQgBAdABABQAAACAIgCIAIgBQABgBgBgZQgCgZACgEQACgFADAAQACAAAAAAQABAAABAAQAAAAABABQAAAAAAABQADBJAAA+QAAAGgDACIgEAAIgDgBg");
	this.shape_32.setTransform(-11.5,-20.6679);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#89CA02").s().p("AgIBHQgDgBAEgZIAGgkIAFgjIgUgaQgGgIAAgCQAAgEADgBQAEgCADACQAEACAGAJQAGAJADACIACgKQABgIABgDQACgEADgBQADgCAEADQACABgEAeIgFAZIgDAVIgEAZIgFAZQgCAPgBAAIgCABQgFAAgCgCg");
	this.shape_33.setTransform(-16.9054,-20.645);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#89CA02").s().p("AgTBMQgEgBgBgIIAAh0QAAgZACgBQAEgCAPACQAQABAGAFQAEADACANQABAPgJAJQgDADgLABQgKABAAABQAEAAgHBKIAAAIIAAAKQgCAIgFAAIgCgBgAgLg+QgCABADAXQAAACAKAAQAJgBADgDQACgCABgIQAAgIgCgCQgDgEgKAAIgEAAQgIAAABACg");
	this.shape_34.setTransform(-21.87,-20.8321);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#89CA02").s().p("AgaBHQgDgFABgTIADgSIADgSIAGgdIAGgcQADgNACgFQADgKAEADQADACACAPIAHAtIAJAtQAHAbgCAFQgJAGgDgIIgFgTIgHgiIgFAAIgGAAQgDAMgCARQgEAdgBAAIgFACQAAAAgBgBQAAAAgBAAQAAAAgBAAQAAgBgBAAgAgCgRIgDAQIAGgBQAAgTgCgBIAAAAIgBAFg");
	this.shape_35.setTransform(-32.5493,-20.5386);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#89CA02").s().p("AgCBIQgGgDABgZIgBgJIAAgIQAAgqADgoQgBACgMgDQgIgCAAgGQABgHATgBIAZABQAGABABAGQACAHgDAAQgDABgHgBQgGgBAAACQgDAQAAAjIAAA2IAAAJIAAAIQgBAHgFAAIgCgBg");
	this.shape_36.setTransform(-38.1265,-20.6279);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f("#89CA02").s().p("AgNBFQgFgCgDgKQgCgHAFgDQAFgDABAEQAEAJAEABQAHACABgKQACgFAAgMIABgYIgDAAIgKABIgDAAIgCgBQgCAAgCgFIAAgCIAAgCQABgEADAAIAGABIAEAAIAJgBQAAgTgBgNQgCgbgHACQgHABgBAEQgBAEgEACQgBAAgBAAQAAAAgBAAQAAAAgBgBQAAAAgBgBIgCgDIAEgIQAHgJALAAQALAAAGAKQAEAHABAYIABAiQgBArgDAKQgFAPgNACIgDAAQgGAAgFgEg");
	this.shape_37.setTransform(-44.1,-20.5089);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_37},{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol_15_Layer_1, null, null);


(lib.Symbol_10_Layer_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#89CA02").ss(3.4,1,1).p("ACGijQh+AKhMBkQhKBhALB4");
	this.shape.setTransform(-0.0019,0);
	this.shape._off = true;

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(4).to({_off:false},0).wait(55));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_3_Layer_5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_5
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(13,1,1).p("AiPAFQAdAlAtARQAtAQAxgPQAygQAdgrQAdgqALgZ");
	this.shape.setTransform(6.125,25.5759);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(13,1,1).p("AiKAGQAaAnAuARQAtARAxgPQAygQAcgsQAcgsAFge");
	this.shape_1.setTransform(5.925,25.3196);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(13,1,1).p("AiHAHQAYAoAvASQAtASAygQQAygPAagtQAcguABgj");
	this.shape_2.setTransform(5.75,25.1015);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(13,1,1).p("AiDAIQAWApAvATQAuATAxgQQAygQAZgtQAbgvgDgo");
	this.shape_3.setTransform(5.5903,24.8907);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(13,1,1).p("AiAAJQAUAqAwAUQAtATAxgPQAygQAZguQAZgxgHgt");
	this.shape_4.setTransform(5.5004,24.6732);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(13,1,1).p("Ah+AJQATAsAwAUQAuAUAxgQQAygQAXguQAZgygKgx");
	this.shape_5.setTransform(5.4303,24.5056);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(13,1,1).p("Ah8ALQARAsAxAVQAuAUAwgPQAygRAXgvQAZgzgOg0");
	this.shape_6.setTransform(5.4045,24.3384);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(13,1,1).p("Ah6ALQAPAtAyAWQAuAUAwgPQAygRAWgwQAYgzgQg4");
	this.shape_7.setTransform(5.3861,24.2087);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(13,1,1).p("Ah5AMQAOAtAyAXQAvAVAvgQQAzgRAVgwQAYg1gUg6");
	this.shape_8.setTransform(5.3672,24.0709);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(13,1,1).p("Ah4AMQANAuAyAXQAvAWAwgQQAygRAVgxQAXg1gVg9");
	this.shape_9.setTransform(5.3511,23.9663);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(13,1,1).p("Ah3AMQAMAvAzAXQAuAWAwgQQAzgRAUgwQAWg2gXg/");
	this.shape_10.setTransform(5.3563,23.8618);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(13,1,1).p("Ah2ANQAMAvAyAYQAvAWAwgQQAygRAUgxQAWg3gYhA");
	this.shape_11.setTransform(5.3386,23.7868);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(13,1,1).p("Ah2ANQALAwAzAXQAvAXAwgQQAygRATgxQAXg3gahC");
	this.shape_12.setTransform(5.3719,23.7325);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(13,1,1).p("Ah1ANQALAwAzAYQAvAWAvgQQAzgRATgxQAWg4gbhC");
	this.shape_13.setTransform(5.349,23.7075);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(13,1,1).p("Ah1ANQAKAwAzAYQAvAXAwgQQAygRATgyQAWg3gbhE");
	this.shape_14.setTransform(5.3639,23.6575);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(13,1,1).p("Ah4AMQANAvAyAXQAvAWAwgQQAygRAVgxQAXg2gXg+");
	this.shape_15.setTransform(5.3563,23.9118);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(13,1,1).p("Ah6ALQAPAuAyAWQAuAVAwgQQAzgRAVgvQAYg1gSg5");
	this.shape_16.setTransform(5.3654,24.1291);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(13,1,1).p("Ah+AJQATArAwAVQAuAUAwgQQAzgRAXguQAagygKgv");
	this.shape_17.setTransform(5.4408,24.5556);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#000000").ss(13,1,1).p("AiBAJQAVAqAvAUQAuATAxgQQAygQAZguQAagwgHgs");
	this.shape_18.setTransform(5.515,24.7232);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#000000").ss(13,1,1).p("AiFAHQAXApAvASQAuATAwgQQAzgQAagtQAbgugBgl");
	this.shape_19.setTransform(5.6755,25.021);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#000000").ss(13,1,1).p("AiIAGQAZAoAuASQAtASAygPQAygQAagtQAcgtADgi");
	this.shape_20.setTransform(5.8,25.1586);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#000000").ss(13,1,1).p("AiJAGQAZAnAuASQAuARAxgPQAygQAbgsQAcgtAEgf");
	this.shape_21.setTransform(5.875,25.264);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#000000").ss(13,1,1).p("AiLAGQAaAmAuASQAuARAxgQQAygPAcgsQAcgsAGge");
	this.shape_22.setTransform(5.95,25.3696);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#000000").ss(13,1,1).p("AiMAGQAbAmAtARQAtARAygQQAygPAcgsQAdgrAHgc");
	this.shape_23.setTransform(6.025,25.4446);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#000000").ss(13,1,1).p("AiOAFQAcAmAtARQAuAQAxgPQAygQAdgrQAcgrAKgb");
	this.shape_24.setTransform(6.05,25.5004);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f().s("#000000").ss(13,1,1).p("AiOAFQAcAmAtAQQAtARAygQQAygPAdgrQAdgrAJga");
	this.shape_25.setTransform(6.1,25.5254);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape,p:{y:25.5759}}]}).to({state:[{t:this.shape,p:{y:25.5759}}]},19).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_14}]},15).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape,p:{y:25.5754}}]},1).to({state:[{t:this.shape,p:{y:25.5759}}]},1).wait(11));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_3_Layer_4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_4
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(13,1,1).p("AikA0QANhEAxgdQArgZA1AHQAzAHAtAiQAsAgAfA4");
	this.shape.setTransform(-41.425,-18.1504);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(13,1,1).p("AikA1QANhGAxgeQArgaA1AHQAzAIAtAjQAsAhAfA6");
	this.shape_1.setTransform(-41.425,-18.3492);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(13,1,1).p("AikA2QANhIAxgeQArgbA1AIQAzAHAtAkQAsAjAfA7");
	this.shape_2.setTransform(-41.425,-18.5315);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(13,1,1).p("AikA4QANhKAxgfQArgbA1AHQAzAIAtAlQAsAjAfA9");
	this.shape_3.setTransform(-41.425,-18.7054);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(13,1,1).p("AikA5QANhLAxggQArgcA1AIQAzAHAtAmQAsAkAfA+");
	this.shape_4.setTransform(-41.425,-18.8627);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(13,1,1).p("AikA6QANhNAxggQArgdA1AIQAzAIAtAnQAsAlAfA/");
	this.shape_5.setTransform(-41.425,-19.0115);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(13,1,1).p("AikA7QANhOAxghQArgeA1AJQAzAIAtAnQAsAlAfBB");
	this.shape_6.setTransform(-41.425,-19.1451);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(13,1,1).p("AikA8QANhPAxgiQArgeA1AJQAzAIAtAoQAsAmAfBB");
	this.shape_7.setTransform(-41.425,-19.2688);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(13,1,1).p("AikA9QANhRAxghQArgfA1AJQAzAIAtAoQAsAnAfBC");
	this.shape_8.setTransform(-41.425,-19.3676);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(13,1,1).p("AikA+QANhSAxgiQArgfA1AJQAzAIAtApQAsAnAfBD");
	this.shape_9.setTransform(-41.425,-19.4525);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(13,1,1).p("AikA+QANhSAxgjQArgfA1AJQAzAIAtAqQAsAnAfBE");
	this.shape_10.setTransform(-41.425,-19.5262);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(13,1,1).p("AikA+QANhSAxgjQArgfA1AIQAzAJAtApQAsAoAfBE");
	this.shape_11.setTransform(-41.425,-19.5915);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(13,1,1).p("AikA/QANhTAxgkQArgfA1AJQAzAIAtAqQAsAoAfBF");
	this.shape_12.setTransform(-41.425,-19.6415);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(13,1,1).p("AikA/QANhUAxgjQArggA1AJQAzAJAtAqQAsAoAfBF");
	this.shape_13.setTransform(-41.425,-19.7);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(13,1,1).p("AikA+QANhSAxgiQArgfA1AJQAzAIAtApQAsAnAfBE");
	this.shape_14.setTransform(-41.425,-19.5012);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(13,1,1).p("AikA9QANhQAxgiQArgeA1AIQAzAIAtAoQAsAnAfBC");
	this.shape_15.setTransform(-41.425,-19.3188);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(13,1,1).p("AikA6QANhNAxggQArgdA1AIQAzAIAtAmQAsAlAfA/");
	this.shape_16.setTransform(-41.425,-18.9877);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(13,1,1).p("AikA3QANhJAxgeQArgbA1AHQAzAIAtAkQAsAjAfA7");
	this.shape_17.setTransform(-41.425,-18.5815);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#000000").ss(13,1,1).p("AikA2QANhHAxgfQArgaA1AHQAzAHAtAkQAsAiAfA7");
	this.shape_18.setTransform(-41.425,-18.4827);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#000000").ss(13,1,1).p("AikA1QANhGAxgeQArgaA1AHQAzAHAtAjQAsAiAfA6");
	this.shape_19.setTransform(-41.425,-18.3981);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#000000").ss(13,1,1).p("AikA1QANhGAxgdQArgaA1AHQAzAHAtAjQAsAhAfA6");
	this.shape_20.setTransform(-41.425,-18.3242);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#000000").ss(13,1,1).p("AikA1QANhGAxgdQArgaA1AIQAzAHAtAiQAsAhAfA5");
	this.shape_21.setTransform(-41.425,-18.259);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#000000").ss(13,1,1).p("AikA0QANhFAxgcQArgaA1AHQAzAHAtAiQAsAhAfA5");
	this.shape_22.setTransform(-41.425,-18.209);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#000000").ss(13,1,1).p("AikA0QANhFAxgcQArgaA1AHQAzAHAtAiQAsAhAfA4");
	this.shape_23.setTransform(-41.425,-18.1754);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},19).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4,p:{y:-18.8627}}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12,p:{y:-19.6415}}]},1).to({state:[{t:this.shape_12,p:{y:-19.675}}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_13}]},15).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_4,p:{y:-18.8389}}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).wait(11));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_3_Layer_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(13,1,1).p("Ai0BBQAIg+AxgjQAtghBAAAQA/gBA1AhQA5AkAWBA");
	this.shape.setTransform(27.875,-18.5504);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(13,1,1).p("Ai0BDQAIhAAxgkQAtgiBAAAQA/gBA1AiQA5AlAWBC");
	this.shape_1.setTransform(27.875,-18.7504);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(13,1,1).p("Ai0BEQAIhBAxglQAtgjBAAAQA/gBA1AjQA5AmAWBE");
	this.shape_2.setTransform(27.875,-18.9504);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(13,1,1).p("Ai0BGQAIhDAxgmQAtgkBAAAQA/AAA1AjQA5AnAWBF");
	this.shape_3.setTransform(27.875,-19.1503);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(13,1,1).p("Ai0BIQAIhFAxgnQAtglBAAAQA/AAA1AkQA5AoAWBH");
	this.shape_4.setTransform(27.875,-19.3253);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(13,1,1).p("Ai0BJQAIhGAxgoQAtglBAAAQA/gBA1AmQA5AoAWBI");
	this.shape_5.setTransform(27.875,-19.4753);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(13,1,1).p("Ai0BKQAIhHAxgoQAtgmBAAAQA/gBA1AmQA5ApAWBK");
	this.shape_6.setTransform(27.875,-19.6003);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(13,1,1).p("Ai0BLQAIhIAxgpQAtgnBAAAQA/AAA1AmQA5AqAWBL");
	this.shape_7.setTransform(27.875,-19.7253);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(13,1,1).p("Ai0BNQAIhKAxgpQAtgnBAAAQA/gBA1AnQA5AqAWBM");
	this.shape_8.setTransform(27.875,-19.8503);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(13,1,1).p("Ai0BNQAIhKAxgqQAtgnBAAAQA/gBA1AoQA5AqAWBN");
	this.shape_9.setTransform(27.875,-19.9503);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(13,1,1).p("Ai0BOQAIhLAxgrQAtgnBAAAQA/gBA1AoQA5ArAWBN");
	this.shape_10.setTransform(27.875,-20.0253);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(13,1,1).p("Ai0BOQAIhLAxgrQAtgoBAAAQA/AAA1AnQA5AsAWBO");
	this.shape_11.setTransform(27.875,-20.0753);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(13,1,1).p("Ai0BPQAIhMAxgrQAtgoBAAAQA/gBA1AoQA5AsAWBO");
	this.shape_12.setTransform(27.875,-20.1253);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(13,1,1).p("Ai0BPQAIhMAxgrQAtgpBAAAQA/AAA1AoQA5AsAWBP");
	this.shape_13.setTransform(27.875,-20.1753);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(13,1,1).p("Ai0BQQAIhNAxgrQAtgpBAAAQA/AAA1AoQA5AsAWBP");
	this.shape_14.setTransform(27.875,-20.2003);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(13,1,1).p("Ai0BOQAIhLAxgqQAtgoBAAAQA/AAA1AnQA5ArAWBN");
	this.shape_15.setTransform(27.875,-20.0003);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(13,1,1).p("Ai0BMQAIhJAxgpQAtgnBAAAQA/AAA1AmQA5AqAWBL");
	this.shape_16.setTransform(27.875,-19.8003);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(13,1,1).p("Ai0BIQAIhFAxgoQAtglBAAAQA/AAA1AlQA5AoAWBI");
	this.shape_17.setTransform(27.875,-19.4253);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#000000").ss(13,1,1).p("Ai0BHQAIhEAxgnQAtgkBAAAQA/gBA1AkQA5AoAWBG");
	this.shape_18.setTransform(27.875,-19.2753);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#000000").ss(13,1,1).p("Ai0BFQAIhCAxgmQAtgjBAAAQA/AAA1AjQA5AmAWBE");
	this.shape_19.setTransform(27.875,-19.0254);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#000000").ss(13,1,1).p("Ai0BEQAIhBAxglQAtgjBAAAQA/AAA1AiQA5AmAWBD");
	this.shape_20.setTransform(27.875,-18.9004);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#000000").ss(13,1,1).p("Ai0BDQAIhAAxglQAtgiBAAAQA/AAA1AiQA5AlAWBC");
	this.shape_21.setTransform(27.875,-18.8004);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#000000").ss(13,1,1).p("Ai0BCQAIhAAxgjQAtgiBAAAQA/gBA1AiQA5AkAWBC");
	this.shape_22.setTransform(27.875,-18.7254);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#000000").ss(13,1,1).p("Ai0BCQAIg/AxgkQAtgiBAAAQA/AAA1AiQA5AkAWBB");
	this.shape_23.setTransform(27.875,-18.6754);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#000000").ss(13,1,1).p("Ai0BBQAIg+AxgkQAtghBAAAQA/gBA1AiQA5AkAWBA");
	this.shape_24.setTransform(27.875,-18.6254);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f().s("#000000").ss(13,1,1).p("Ai0BBQAIg/AxgjQAtghBAAAQA/AAA1AhQA5AjAWBB");
	this.shape_25.setTransform(27.875,-18.5754);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},19).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_14}]},15).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).wait(11));

}).prototype = p = new cjs.MovieClip();


(lib.Scene_1_Symbol_8 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Symbol_8
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(13,1,1).p("AEdmrQhNJWiXCxQiDCbjSiR");
	this.shape.setTransform(189.275,463.81);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(13,1,1).p("AkdFqQDTCMCEibQCXiwBNpW");
	this.shape_1.setTransform(189.15,463.797);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(13,1,1).p("AkeFtQDVCHCFibQCWiuBNpW");
	this.shape_2.setTransform(189.025,463.7922);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(13,1,1).p("AkgFxQDXCCCGicQCWitBOpV");
	this.shape_3.setTransform(188.875,463.7922);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(13,1,1).p("AkhF1QDYB8CIicQCVisBOpU");
	this.shape_4.setTransform(188.725,463.7908);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(13,1,1).p("AkjF6QDbB1CIicQCViqBPpU");
	this.shape_5.setTransform(188.55,463.7944);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(13,1,1).p("AklF+QDdBvCLidQCUioBPpT");
	this.shape_6.setTransform(188.375,463.7987);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(13,1,1).p("AkmGDQDeBoCNieQCTimBQpS");
	this.shape_7.setTransform(188.2,463.8141);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(13,1,1).p("AkpGIQDiBhCOifQCSikBRpR");
	this.shape_8.setTransform(188,463.8413);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(13,1,1).p("AkrGNQDkBZCQifQCRiiBRpR");
	this.shape_9.setTransform(187.8,463.8835);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(13,1,1).p("AEumsQhSJPiRCgQiRCgjnhR");
	this.shape_10.setTransform(187.575,463.9362);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(13,1,1).p("AkoGOQDiBYCRihQCQihBOpQ");
	this.shape_11.setTransform(188.025,463.9196);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(13,1,1).p("AkkGKQDfBfCQiiQCQiiBLpR");
	this.shape_12.setTransform(188.45,463.9459);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(13,1,1).p("AkhGGQDcBmCPijQCQijBHpS");
	this.shape_13.setTransform(188.85,463.9475);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(13,1,1).p("AkdGCQDZBsCOikQCPikBFpS");
	this.shape_14.setTransform(189.225,463.9601);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(13,1,1).p("AkZF+QDWByCNilQCPikBBpT");
	this.shape_15.setTransform(189.575,464.004);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(13,1,1).p("AkWF7QDTB3CNilQCOimA/pT");
	this.shape_16.setTransform(189.925,464.0053);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(13,1,1).p("AkTF4QDQB8CNimQCOimA8pU");
	this.shape_17.setTransform(190.225,464.0179);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#000000").ss(13,1,1).p("AkQF1QDOCBCMinQCOinA5pU");
	this.shape_18.setTransform(190.5,464.061);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#000000").ss(13,1,1).p("AkOFyQDMCGCMioQCNinA4pV");
	this.shape_19.setTransform(190.775,464.0855);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#000000").ss(13,1,1).p("AkLFwQDJCJCMioQCMioA2pV");
	this.shape_20.setTransform(191,464.0943);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#000000").ss(13,1,1).p("AkJFtQDICNCLioQCMipA0pW");
	this.shape_21.setTransform(191.225,464.1118);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#000000").ss(13,1,1).p("AkIFrQDHCRCLiqQCMioAzpX");
	this.shape_22.setTransform(191.425,464.1249);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#000000").ss(13,1,1).p("AkGFpQDFCTCLipQCLipAypX");
	this.shape_23.setTransform(191.6,464.1637);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#000000").ss(13,1,1).p("AkFFoQDECVCLiqQCLipAxpX");
	this.shape_24.setTransform(191.75,464.1727);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f().s("#000000").ss(13,1,1).p("AkDFnQDDCXCKiqQCLiqAvpX");
	this.shape_25.setTransform(191.875,464.189);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f().s("#000000").ss(13,1,1).p("AkCFmQDCCZCKirQCLiqAupX");
	this.shape_26.setTransform(191.975,464.1931);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f().s("#000000").ss(13,1,1).p("AkCFkQDCCbCKirQCLiqAtpX");
	this.shape_27.setTransform(192.05,464.2098);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f().s("#000000").ss(13,1,1).p("AkBFkQDBCbCKiqQCKirAupX");
	this.shape_28.setTransform(192.1,464.2086);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f().s("#000000").ss(13,1,1).p("AkBFjQDBCdCKirQCLirAtpX");
	this.shape_29.setTransform(192.15,464.2143);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f().s("#000000").ss(13,1,1).p("AECmtQgtJXiLCrQiKCrjBid");
	this.shape_30.setTransform(192.15,464.2143);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f().s("#000000").ss(13,1,1).p("AkBFjQDBCdCKirQCKirAupX");
	this.shape_31.setTransform(192.125,464.2143);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f().s("#000000").ss(13,1,1).p("AkBFjQDBCcCKiqQCKirAupX");
	this.shape_32.setTransform(192.075,464.2018);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f().s("#000000").ss(13,1,1).p("AkCFjQDBCcCKiqQCLirAvpX");
	this.shape_33.setTransform(192,464.2018);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f().s("#000000").ss(13,1,1).p("AkDFkQDCCbCJipQCMisAwpX");
	this.shape_34.setTransform(191.875,464.1826);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f().s("#000000").ss(13,1,1).p("AkFFkQDECbCJipQCMirAypY");
	this.shape_35.setTransform(191.7,464.1451);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f().s("#000000").ss(13,1,1).p("AkGFkQDECaCIinQCOisAzpY");
	this.shape_36.setTransform(191.5,464.1191);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f().s("#000000").ss(13,1,1).p("AkJFkQDGCZCIimQCPisA2pX");
	this.shape_37.setTransform(191.25,464.0941);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f().s("#000000").ss(13,1,1).p("AkLFlQDHCYCIilQCPitA5pX");
	this.shape_38.setTransform(191,464.0499);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f().s("#000000").ss(13,1,1).p("AkOFlQDJCWCHijQCQitA9pX");
	this.shape_39.setTransform(190.75,464.0125);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f().s("#000000").ss(13,1,1).p("AkQFlQDLCWCGiiQCRiuBApX");
	this.shape_40.setTransform(190.45,463.9866);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f().s("#000000").ss(13,1,1).p("AkTFlQDMCVCGigQCTivBCpX");
	this.shape_41.setTransform(190.225,463.9424);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f().s("#000000").ss(13,1,1).p("AkVFlQDNCUCGifQCTivBFpX");
	this.shape_42.setTransform(190,463.9174);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f().s("#000000").ss(13,1,1).p("AkXFmQDPCTCEieQCViwBHpW");
	this.shape_43.setTransform(189.825,463.8983);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f().s("#000000").ss(13,1,1).p("AkYFlQDQCTCDidQCWiwBIpW");
	this.shape_44.setTransform(189.65,463.8666);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f().s("#000000").ss(13,1,1).p("AkZFmQDQCSCEicQCWiwBJpX");
	this.shape_45.setTransform(189.525,463.8291);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.f().s("#000000").ss(13,1,1).p("AkaFmQDRCRCDibQCXixBKpW");
	this.shape_46.setTransform(189.425,463.8225);

	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.f().s("#000000").ss(13,1,1).p("AkbFmQDRCRCEibQCWixBMpW");
	this.shape_47.setTransform(189.375,463.8225);

	this.shape_48 = new cjs.Shape();
	this.shape_48.graphics.f().s("#000000").ss(13,1,1).p("AkbFmQDRCRCDibQCXixBMpW");
	this.shape_48.setTransform(189.325,463.81);

	this.shape_49 = new cjs.Shape();
	this.shape_49.graphics.f().s("#000000").ss(13,1,1).p("AkcFmQDSCRCDibQCXixBMpW");
	this.shape_49.setTransform(189.3,463.81);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},4).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_27}]},1).to({state:[{t:this.shape_28}]},1).to({state:[{t:this.shape_29}]},1).to({state:[{t:this.shape_30}]},1).to({state:[{t:this.shape_30}]},15).to({state:[{t:this.shape_31}]},1).to({state:[{t:this.shape_32}]},1).to({state:[{t:this.shape_33}]},1).to({state:[{t:this.shape_34}]},1).to({state:[{t:this.shape_35}]},1).to({state:[{t:this.shape_36}]},1).to({state:[{t:this.shape_37}]},1).to({state:[{t:this.shape_38}]},1).to({state:[{t:this.shape_39}]},1).to({state:[{t:this.shape_40}]},1).to({state:[{t:this.shape_41}]},1).to({state:[{t:this.shape_42}]},1).to({state:[{t:this.shape_43}]},1).to({state:[{t:this.shape_44}]},1).to({state:[{t:this.shape_45}]},1).to({state:[{t:this.shape_46}]},1).to({state:[{t:this.shape_47}]},1).to({state:[{t:this.shape_48}]},1).to({state:[{t:this.shape_49}]},1).to({state:[{t:this.shape}]},1).wait(6));

}).prototype = p = new cjs.MovieClip();


(lib.Scene_1_Layer_14 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_14
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(13,1,1).p("AFyGHQhHiPgyhhQhfi2hQhyQjzlZjICN");
	this.shape.setTransform(85.85,407.7296);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(13,1,1).p("Al1lfQDMiHDvFQQBYB5BdCyQAzBjBICP");
	this.shape_1.setTransform(85.475,407.7382);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(13,1,1).p("Al5lhQDQiBDqFGQBiCCBbCsQA1BmBHCP");
	this.shape_2.setTransform(85.05,407.7443);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(13,1,1).p("Al9ljQDUh6DmE7QBrCKBYCoQA3BoBHCP");
	this.shape_3.setTransform(84.625,407.747);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(13,1,1).p("AmClmQDZhzDhEwQB1CUBWCjQA5BqBHCP");
	this.shape_4.setTransform(84.15,407.745);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(13,1,1).p("AmHlpQDehrDcEjQB/CeBVCdQA6BtBHCP");
	this.shape_5.setTransform(83.65,407.7617);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(13,1,1).p("AmMlsQDjhjDXEWQCLCpBRCXQA8BwBICP");
	this.shape_6.setTransform(83.1,407.7501);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(13,1,1).p("AmSlvQDphaDREIQCXC0BPCRQA+BzBHCP");
	this.shape_7.setTransform(82.55,407.7559);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(13,1,1).p("AmYlyQDvhRDLD6QCkC/BLCLQBBB2BHCP");
	this.shape_8.setTransform(81.95,407.7574);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(13,1,1).p("Amfl1QD2hHDFDqQCxDMBJCEQBCB6BICP");
	this.shape_9.setTransform(81.3,407.7486);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(13,1,1).p("AGmGHQhHiPhGh9QhFh9i/jZQi+jaj8A9");
	this.shape_10.setTransform(80.65,407.7332);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(13,1,1).p("Ambl1QD5hAC5DWQC6DUBCCAQBCCBBHCP");
	this.shape_11.setTransform(81.625,407.9222);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(13,1,1).p("AmSlyQD2hCC1DQQC1DQA/CEQA/CEBHCP");
	this.shape_12.setTransform(82.55,408.1066);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(13,1,1).p("AmJlvQDzhECwDMQCxDMA8CGQA8CHBHCP");
	this.shape_13.setTransform(83.425,408.2906);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(13,1,1).p("AmBlsQDxhHCsDJQCsDIA6CJQA5CKBHCP");
	this.shape_14.setTransform(84.25,408.4466);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(13,1,1).p("Al5lpQDuhJCoDEQCpDFA2CMQA3CMBHCP");
	this.shape_15.setTransform(85.025,408.5863);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(13,1,1).p("AlylnQDshLCkDBQCmDBA0CPQA0CPBHCP");
	this.shape_16.setTransform(85.725,408.7256);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(13,1,1).p("AlslkQDqhNChC+QCiC9AyCRQAyCSBHCP");
	this.shape_17.setTransform(86.4,408.85);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#000000").ss(13,1,1).p("AllliQDohOCeC6QCeC7AwCTQAwCUBHCP");
	this.shape_18.setTransform(87.025,408.9499);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#000000").ss(13,1,1).p("AlflgQDlhQCcC4QCcC3AuCVQAtCWBICP");
	this.shape_19.setTransform(87.6,409.0629);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#000000").ss(13,1,1).p("AlaleQDkhRCZC1QCZC1AsCXQAsCYBHCP");
	this.shape_20.setTransform(88.125,409.1479);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#000000").ss(13,1,1).p("AlVldQDjhSCWCzQCXCzAqCYQAqCaBICP");
	this.shape_21.setTransform(88.6,409.2236);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#000000").ss(13,1,1).p("AlRlbQDhhUCUCxQCVCxApCaQApCbBHCP");
	this.shape_22.setTransform(89.025,409.3092);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#000000").ss(13,1,1).p("AlOlaQDhhUCSCvQCSCvAoCbQAoCcBHCP");
	this.shape_23.setTransform(89.4,409.3742);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#000000").ss(13,1,1).p("AlKlZQDfhVCRCtQCRCuAmCcQAnCeBHCP");
	this.shape_24.setTransform(89.725,409.4138);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f().s("#000000").ss(13,1,1).p("AlIlYQDfhWCPCsQCQCtAmCdQAlCeBHCP");
	this.shape_25.setTransform(90,409.4667);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f().s("#000000").ss(13,1,1).p("AlFlXQDehXCOCrQCOCsAlCeQAlCfBHCP");
	this.shape_26.setTransform(90.225,409.5081);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f().s("#000000").ss(13,1,1).p("AlElWQDdhXCOCqQCNCqAkCfQAlCgBHCP");
	this.shape_27.setTransform(90.4,409.5384);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f().s("#000000").ss(13,1,1).p("AlClVQDdhYCMCpQCNCqAkCgQAkCgBHCP");
	this.shape_28.setTransform(90.525,409.5463);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f().s("#000000").ss(13,1,1).p("AlBlVQDchYCMCpQCNCpAkCgQAjCgBICP");
	this.shape_29.setTransform(90.6,409.5684);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f().s("#000000").ss(13,1,1).p("AFCF0QhHiPgkigQgjigiNipQiMipjcBY");
	this.shape_30.setTransform(90.625,409.5684);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f().s("#000000").ss(13,1,1).p("AlClVQDdhZCNCrQCMCpAkCfQAjCgBICP");
	this.shape_31.setTransform(90.575,409.563);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f().s("#000000").ss(13,1,1).p("AlDlVQDdhaCOCuQCLCoAlCgQAkCfBHCP");
	this.shape_32.setTransform(90.5,409.5271);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f().s("#000000").ss(13,1,1).p("AlElWQDchbCRCzQCKCnAmCgQAlCdBHCP");
	this.shape_33.setTransform(90.35,409.4665);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f().s("#000000").ss(13,1,1).p("AlGlWQDahdCXC6QCGCkApCiQAlCaBICP");
	this.shape_34.setTransform(90.15,409.3889);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f().s("#000000").ss(13,1,1).p("AlJlWQDahhCcDFQCDChAsCjQAmCWBICP");
	this.shape_35.setTransform(89.875,409.2859);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f().s("#000000").ss(13,1,1).p("AlMlXQDXhkCkDRQB/CdAxCkQAnCSBHCP");
	this.shape_36.setTransform(89.55,409.1569);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f().s("#000000").ss(13,1,1).p("AlQlYQDWhoCsDfQB6CZA1CmQApCNBHCP");
	this.shape_37.setTransform(89.15,409.0103);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f().s("#000000").ss(13,1,1).p("AlUlYQDUhtC1DvQB1CTA7CpQApCHBICP");
	this.shape_38.setTransform(88.7,408.8454);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f().s("#000000").ss(13,1,1).p("AlZlZQDThyC/EAQBuCOBBCqQArCBBHCP");
	this.shape_39.setTransform(88.25,408.6688);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f().s("#000000").ss(13,1,1).p("AldlaQDQh3DIERQBqCJBGCsQAsB7BHCP");
	this.shape_40.setTransform(87.825,408.4914);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f().s("#000000").ss(13,1,1).p("AlhlaQDOh8DREgQBkCEBLCuQAuB2BHCP");
	this.shape_41.setTransform(87.425,408.3445);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f().s("#000000").ss(13,1,1).p("AlllbQDNiADZEtQBfCABQCwQAvBxBHCP");
	this.shape_42.setTransform(87.05,408.2009);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f().s("#000000").ss(13,1,1).p("AlolbQDLiDDgE4QBcB8BTCxQAwBtBHCP");
	this.shape_43.setTransform(86.75,408.0881);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f().s("#000000").ss(13,1,1).p("AlrlcQDLiGDlFCQBZB5BWCzQAwBpBHCP");
	this.shape_44.setTransform(86.5,407.979);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f().s("#000000").ss(13,1,1).p("AltlcQDKiIDpFJQBWB3BaCzQAwBnBICP");
	this.shape_45.setTransform(86.275,407.8987);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.f().s("#000000").ss(13,1,1).p("AlulcQDJiKDtFPQBTB1BcC0QAxBlBHCP");
	this.shape_46.setTransform(86.125,407.8369);

	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.f().s("#000000").ss(13,1,1).p("AlwlcQDJiMDwFUQBSBzBdC1QAxBjBHCP");
	this.shape_47.setTransform(86,407.7937);

	this.shape_48 = new cjs.Shape();
	this.shape_48.graphics.f().s("#000000").ss(13,1,1).p("AlwldQDIiMDxFWQBRBzBeC1QAyBiBHCP");
	this.shape_48.setTransform(85.925,407.7565);

	this.shape_49 = new cjs.Shape();
	this.shape_49.graphics.f().s("#000000").ss(13,1,1).p("AlxldQDIiMDyFYQBRByBeC1QAyBiBICP");
	this.shape_49.setTransform(85.875,407.7378);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},4).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_27}]},1).to({state:[{t:this.shape_28}]},1).to({state:[{t:this.shape_29}]},1).to({state:[{t:this.shape_30}]},1).to({state:[{t:this.shape_30}]},15).to({state:[{t:this.shape_31}]},1).to({state:[{t:this.shape_32}]},1).to({state:[{t:this.shape_33}]},1).to({state:[{t:this.shape_34}]},1).to({state:[{t:this.shape_35}]},1).to({state:[{t:this.shape_36}]},1).to({state:[{t:this.shape_37}]},1).to({state:[{t:this.shape_38}]},1).to({state:[{t:this.shape_39}]},1).to({state:[{t:this.shape_40}]},1).to({state:[{t:this.shape_41}]},1).to({state:[{t:this.shape_42}]},1).to({state:[{t:this.shape_43}]},1).to({state:[{t:this.shape_44}]},1).to({state:[{t:this.shape_45}]},1).to({state:[{t:this.shape_46}]},1).to({state:[{t:this.shape_47}]},1).to({state:[{t:this.shape_48}]},1).to({state:[{t:this.shape_49}]},1).to({state:[{t:this.shape}]},1).wait(6));

}).prototype = p = new cjs.MovieClip();


(lib.Scene_1_bg_png = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// bg_png
	this.instance = new lib.pack();
	this.instance.parent = this;
	this.instance.setTransform(58,278,0.6521,0.6521);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(75));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_18_Layer_75 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_75
	this.instance = new lib.Symbol22();
	this.instance.parent = this;
	this.instance.setTransform(179,-18.4);
	this.instance.alpha = 0;

	this.timeline.addTween(cjs.Tween.get(this.instance).to({guide:{path:[179,-18.4,171.1,-17.6,161.7,-17.6,154.8,-17.6,148.8,-18]},alpha:0.3008},59).to({guide:{path:[148.8,-18,140.4,-18.6,133.7,-19.8,128,-20.9,125.1,-22.2,122.2,-23.5,122.2,-25.1,122.2,-25.6,122.5,-26.2]},alpha:0.0039},58).to({guide:{path:[122.5,-26.1,122.7,-26.4,122.9,-26.6]},alpha:0},1).wait(1));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_18_Layer_74 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_74
	this.instance = new lib.Symbol22();
	this.instance.parent = this;
	this.instance.setTransform(148.75,-13.9,0.7,0.7);
	this.instance.alpha = 0.3008;

	this.timeline.addTween(cjs.Tween.get(this.instance).to({regX:0.1,regY:-0.1,guide:{path:[148.8,-14,132.4,-14.7,119.7,-17.1,103.5,-20.2,102.5,-24.4]},alpha:0.0078},47).to({regX:0,regY:0,guide:{path:[102.5,-24.4,102.4,-24.7,102.4,-25,102.4,-25.2,102.5,-25.4]},alpha:0},1).wait(1).to({x:180.35,y:-14.3},0).to({regX:0.1,regY:-0.1,guide:{path:[180.4,-14.4,171.6,-13.9,161.6,-13.9,155.2,-13.9,149.3,-14.1]},alpha:0.3008},69).wait(1));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_18_Layer_73 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_73
	this.instance = new lib.Symbol22();
	this.instance.parent = this;
	this.instance.setTransform(128.35,-16,0.7,0.7);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({regX:0.1,regY:-0.1,guide:{path:[128.4,-15.7,123.8,-16.3,119.7,-17.1,111.1,-18.8,106.7,-20.8,102.4,-22.7,102.4,-25,102.4,-25.2,102.4,-25.4]},alpha:0.0273},38).to({regX:0,regY:0,guide:{path:[102.5,-25.5,102.5,-25.8,102.7,-26.2]},alpha:0},1).wait(1).to({x:175.55,y:-14.2},0).to({regX:0.1,regY:-0.1,guide:{path:[175.6,-14.1,168.9,-13.8,161.6,-13.8,143.4,-13.8,129.1,-15.6]},alpha:0.9883},78).wait(1));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_18_Layer_72 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_72
	this.instance = new lib.Symbol22();
	this.instance.parent = this;
	this.instance.setTransform(191,-15.3);
	this.instance.alpha = 0;

	this.timeline.addTween(cjs.Tween.get(this.instance).to({guide:{path:[191,-15.2,177.9,-13.8,161.6,-13.8,151.9,-13.8,143.2,-14.3]},alpha:1},59).to({guide:{path:[143.2,-14.4,130.2,-15.2,119.7,-17.2,111.1,-18.8,106.8,-20.8,102.4,-22.7,102.4,-25.1,102.4,-26.5,104,-27.8]},alpha:0.0156},58).to({guide:{path:[104,-27.7,104.3,-28,104.7,-28.2]},alpha:0},1).wait(1));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_18_Layer_71 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_71
	this.instance = new lib.Symbol22();
	this.instance.parent = this;
	this.instance.setTransform(69.2,-20.7);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({guide:{path:[69.2,-20.5,66.6,-22.6,66.6,-24.9,66.6,-26.7,68.4,-28.4]},alpha:0.125},7).to({guide:{path:[68.3,-28.5,68.7,-28.9,69.3,-29.4]},alpha:0},1).wait(1).to({x:195.75,y:-8.3},0).to({guide:{path:[195.8,-8.2,179.9,-7.1,161.6,-7.1,122.2,-7.1,94.5,-12.3,76.6,-15.6,70.3,-19.9]},alpha:0.9883},109).wait(1));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_18_Layer_70 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_70
	this.instance = new lib.Symbol22();
	this.instance.parent = this;
	this.instance.setTransform(154.2,22.1);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({guide:{path:[154.2,22,55.6,21.6,-14.7,8.1,-51.2,1.3,-69.5,-6.9,-87.8,-15.2,-87.8,-24.9,-87.8,-25.3,-87.8,-25.7]},alpha:0.0117},88).to({guide:{path:[-87.8,-25.7,-87.7,-27.2,-87.1,-28.6]},alpha:0},1).wait(1).to({x:239.1,y:20},0).to({guide:{path:[239.1,19.9,202.8,22.1,161.6,22.1,159.4,22.1,157.1,22.1]},alpha:0.9609},28).wait(1));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_18_Layer_69 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_69
	this.instance = new lib.Symbol22();
	this.instance.parent = this;
	this.instance.setTransform(-302.5,38.6);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({guide:{path:[-302.3,38.5,-313.3,35.8,-323.2,33]},alpha:0.1992},4).to({guide:{path:[-323.2,33.1,-325.9,32.3,-328.4,31.6]},alpha:0},1).wait(1).to({x:265,y:81.45},0).to({guide:{path:[265,81.5,215.1,83.1,161.7,83.1,-77.2,83.1,-246.2,50.6,-274.1,45.4,-297.4,39.9]},alpha:0.9883},112).wait(1));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_18_Layer_68 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_68
	this.instance = new lib.Symbol22();
	this.instance.parent = this;
	this.instance.setTransform(-262.5,26.5,0.8,0.8);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({guide:{path:[-262.4,26.5,-339.1,5.1,-344.6,-21.5]},alpha:0.043},22).to({guide:{path:[-344.6,-21.5,-345,-23.5,-345,-25.6,-345,-25.8,-345,-26]},alpha:0},1).wait(1).to({x:284.4,y:67.25},0).to({guide:{path:[284.4,67.2,225.9,69.9,161.6,69.9,-48.2,69.9,-196.7,41.4,-230.6,35.1,-256.8,28.1]},alpha:0.9883},94).wait(1));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_18_Layer_67 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_67
	this.instance = new lib.Symbol22();
	this.instance.parent = this;
	this.instance.setTransform(-212.15,5.05,0.8,0.8);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({guide:{path:[-212,5,-242.6,-7.9,-245,-22.9]},alpha:0.0508},19).to({guide:{path:[-245,-23,-245.2,-24.2,-245.2,-25.4]},alpha:0},1).wait(1).to({x:256.75,y:49.1},0).to({guide:{path:[256.8,49.1,211.4,51.1,161.7,51.1,-6.9,51.1,-126.1,28.2,-178.2,18.5,-207.5,7]},alpha:0.9883},97).wait(1));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_18_Layer_66 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_66
	this.instance = new lib.Symbol22();
	this.instance.parent = this;
	this.instance.setTransform(-133.75,15.45,0.8,0.8);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({guide:{path:[-133.7,15.5,-202.4,-1.4,-205.8,-23.2]},alpha:0.0391},24).to({guide:{path:[-205.7,-23.3,-205.9,-24.4,-205.9,-25.6,-205.9,-26.3,-205.8,-27]},alpha:0},1).wait(1).to({x:265.25,y:41.1},0).to({guide:{path:[265.2,41,216.3,43.7,161.6,43.7,9.4,43.7,-98.3,23,-115.2,20,-129.4,16.6]},alpha:0.9883},92).wait(1));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_18_Layer_65 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_65
	this.instance = new lib.Symbol22();
	this.instance.parent = this;
	this.instance.setTransform(-157.05,-2.2,0.8,0.8);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({guide:{path:[-157,-2.2,-178.6,-12.5,-179.6,-24.4]},alpha:0.0547},17).to({guide:{path:[-179.6,-24.4,-179.6,-25,-179.6,-25.5,-179.6,-26,-179.6,-26.4]},alpha:0},1).wait(1).to({x:289.65,y:34.35},0).to({guide:{path:[289.7,34.2,230.6,38.7,161.7,38.7,20.3,38.7,-79.6,19.6,-127.8,10.6,-152.8,-0.2]},alpha:0.9883},99).wait(1));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_18_Layer_64 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_64
	this.instance = new lib.Symbol22();
	this.instance.parent = this;
	this.instance.setTransform(-130.8,-11.9);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({guide:{path:[-130.7,-11.8,-137.5,-17.1,-138.8,-22.8]},alpha:0.0547},17).to({guide:{path:[-138.7,-22.8,-138.8,-23.2,-138.8,-23.6]},alpha:0},1).wait(1).to({x:238.65,y:29.5},0).to({guide:{path:[238.6,29.3,202,31.1,161.6,31.1,37.1,31.1,-51,14.2,-107.3,3.7,-127.6,-9.6]},alpha:0.9883},99).wait(1));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_18_Layer_62 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_62
	this.instance = new lib.Symbol22();
	this.instance.parent = this;
	this.instance.setTransform(18.4,40.75,0.8,0.8);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({guide:{path:[18.3,41.1,-50.7,35.8,-107.6,24.9,-203,7.1,-216.8,-17.1]},alpha:0.0195},50).to({guide:{path:[-216.8,-17.1,-218.2,-19.4,-218.8,-21.8]},alpha:0},1).wait(1).to({x:295.1,y:41.6},0).to({regX:0.1,regY:0.1,guide:{path:[295.1,41.9,233.2,46.3,161.6,46.3,87,46.3,22.6,41.5]},alpha:0.9805},66).wait(1));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_18_Layer_61 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_61
	this.instance = new lib.Symbol22();
	this.instance.parent = this;
	this.instance.setTransform(-28.15,39.55);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({guide:{path:[-28.2,39.5,-75.6,34.5,-116.8,26.6,-221.5,7,-231.2,-19.9]},alpha:0.0195},55).to({guide:{path:[-231.2,-19.9,-231.9,-21.9,-232.2,-23.8]},alpha:0},1).wait(1).to({x:276.45,y:45.75},0).to({guide:{path:[276.4,45.6,222.3,48.7,161.5,48.7,60,48.7,-23.1,40.1]},alpha:0.9805},61).wait(1));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_18_Layer_60 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_60
	this.instance = new lib.Symbol22();
	this.instance.parent = this;
	this.instance.setTransform(-15.5,24.9);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({guide:{path:[-15.5,24.7,-40.5,21.4,-63.3,17.1,-147.9,1.3,-155.6,-20.6]},alpha:0.0156},61).to({guide:{path:[-155.6,-20.5,-156,-21.8,-156.2,-23]},alpha:0},1).wait(1).to({x:265.5,y:31.75},0).to({guide:{path:[265.5,31.8,217,34.9,161.7,34.9,65.3,34.9,-10.4,25.4]},alpha:0.9805},55).wait(1));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_18_Layer_59 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_59
	this.instance = new lib.Symbol22();
	this.instance.parent = this;
	this.instance.setTransform(118,48.9);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({guide:{path:[118,48.8,-17.3,46.2,-116.8,27.2,-220.9,7.7,-231.1,-19.1]},alpha:0.0117},75).to({guide:{path:[-231.1,-19.1,-232,-21.4,-232.2,-23.9]},alpha:0},1).wait(1).to({x:271.7,y:46.35},0).to({guide:{path:[271.7,46.5,219.7,49.3,161.6,49.3,141.3,49.3,121.8,48.9]},alpha:0.9805},41).wait(1));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_18_Layer_58 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_58
	this.instance = new lib.Symbol22();
	this.instance.parent = this;
	this.instance.setTransform(120.85,29.2,0.7061,0.7061);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({scaleX:0.706,scaleY:0.706,guide:{path:[120.8,29.5,26.1,27.2,-44.3,13.7,-125.8,-1.5,-129.5,-22.8]},alpha:0.0156},71).to({scaleX:0.7061,scaleY:0.7061,guide:{path:[-129.4,-22.8,-129.6,-23.8,-129.6,-24.8,-129.6,-25.7,-129.5,-26.6]},alpha:0},1).wait(1).to({x:276.85,y:25.6},0).to({regX:0.1,regY:0.1,scaleX:0.706,scaleY:0.706,guide:{path:[276.9,25.6,224,29.9,161.6,29.9,142.5,29.9,124.4,29.5]},alpha:0.9805},45).wait(1));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_18_Layer_57 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_57
	this.instance = new lib.Symbol22();
	this.instance.parent = this;
	this.instance.setTransform(56,-4.75);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({guide:{path:[56,-4.8,55.5,-4.9,55,-5,17.6,-11.9,12,-21.4]},alpha:0.0547},17).to({guide:{path:[11.9,-21.3,11.1,-22.7,11,-24.1]},alpha:0},1).wait(1).to({x:222.9,y:1.25},0).to({guide:{path:[222.9,1.1,194.9,3.5,161.6,3.5,101.1,3.5,57.8,-4.4]},alpha:0.9883},99).wait(1));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_18_Layer_46 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_46
	this.instance = new lib.Symbol19();
	this.instance.parent = this;
	this.instance.setTransform(-196.05,10.6);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({guide:{path:[-195.9,10.6,-241.7,-5.2,-241.8,-24.4]},alpha:0.0547},17).to({guide:{path:[-241.8,-24.4,-241.9,-24.5,-241.9,-24.6,-241.9,-26.3,-241.5,-28]},alpha:0},1).wait(1).to({x:231.75,y:50.15},0).to({guide:{path:[231.8,50.2,197.8,51.3,161.5,51.3,-5.6,51.3,-123.6,28.7,-164.9,21,-191.8,12.1]},alpha:0.9883},99).wait(1));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_18_Layer_45 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_45
	this.instance = new lib.Symbol19();
	this.instance.parent = this;
	this.instance.setTransform(-218.15,55.1);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({guide:{path:[-218.1,55.1,-230.1,53,-241.9,50.8,-383.7,24.1,-405.3,-11.4]},alpha:0.0313},30).to({guide:{path:[-405.3,-11.4,-407.2,-14.6,-408.2,-17.8]},alpha:0},1).wait(1).to({x:221.75,y:82.15},0).to({guide:{path:[221.8,82.2,192.2,82.7,161.4,82.7,-54.8,82.7,-213,55.9]},alpha:0.9883},86).wait(1));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_18_Layer_44 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_44
	this.instance = new lib.Symbol19();
	this.instance.parent = this;
	this.instance.setTransform(-50.85,2.75);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({guide:{path:[-50.8,2.8,-95.1,-8.9,-96.4,-23.7]},alpha:0.0508},19).to({guide:{path:[-96.3,-23.8,-96.4,-24.2,-96.4,-24.6,-96.4,-25.7,-96.2,-26.8]},alpha:0},1).wait(1).to({x:213,y:22.9},0).to({guide:{path:[213,22.9,188.2,23.9,161.4,23.9,54.7,23.9,-20.8,9.5,-36,6.5,-48.1,3.5]},alpha:0.9883},97).wait(1));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_18_Layer_41 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_41
	this.instance = new lib.Symbol20();
	this.instance.parent = this;
	this.instance.setTransform(55.55,25.6,0.7758,0.7758);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({scaleX:0.7757,scaleY:0.7757,guide:{path:[55.5,25.5,-1.3,21.6,-47.3,12.8,-121.4,-1,-129.3,-19.9]},alpha:0.0156},58).to({scaleX:0.7758,scaleY:0.7758,guide:{path:[-129.3,-19.9,-130,-21.6,-130.2,-23.3]},alpha:0},1).wait(1).to({x:223.2,y:27},0).to({scaleX:0.7757,scaleY:0.7757,guide:{path:[223.2,27.1,189.6,28.7,152.7,28.7,102.5,28.7,58.5,25.7]},alpha:0.9805},58).wait(1));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_18_Layer_40 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_40
	this.instance = new lib.Symbol20();
	this.instance.parent = this;
	this.instance.setTransform(-109.6,27.55,0.7758,0.7758);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({regX:-0.1,scaleX:0.7757,scaleY:0.7757,guide:{path:[-109.7,27.6,-113.8,26.8,-117.8,26.1,-215.4,7.6,-228,-17]},alpha:0.0234},40).to({regX:0,scaleX:0.7758,scaleY:0.7758,guide:{path:[-228,-17,-228.8,-18.6,-229.2,-20.1]},alpha:0},1).wait(1).to({x:215.85,y:46.95},0).to({scaleX:0.7757,scaleY:0.7757,guide:{path:[215.8,46.4,185.2,47.3,152.6,47.3,2.8,47.3,-105.4,28.3]},alpha:0.9883},76).wait(1));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_18_Layer_39 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_39
	this.instance = new lib.Symbol20();
	this.instance.parent = this;
	this.instance.setTransform(93,42.4);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({guide:{path:[92.9,42.8,-17.9,39.1,-101.8,23.1,-203.3,4.1,-210.6,-22.1]},alpha:0.0117},88).to({guide:{path:[-210.5,-22.1,-211,-23.9,-211,-25.7]},alpha:0},1).wait(1).to({x:236.85,y:42.4},0).to({guide:{path:[236.8,42.6,200.9,43.9,162,43.9,128.9,43.9,98,42.9]},alpha:0.9609},28).wait(1));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_18_Layer_23 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_23
	this.instance = new lib.Symbol23();
	this.instance.parent = this;
	this.instance.setTransform(236.75,59.3,1.1,1.1);
	this.instance.alpha = 0.5;

	this.timeline.addTween(cjs.Tween.get(this.instance).to({scaleX:1.0999,scaleY:1.0999,guide:{path:[236.7,58.8,198.8,59.9,158.4,59.9,-31.4,59.9,-165.4,34.3,-232.4,21.7,-266,6.6,-299.5,-8.5,-299.5,-26.2,-299.5,-28.1,-299.1,-30]},alpha:0.0039},109).to({scaleX:1.1,scaleY:1.1,guide:{path:[-299.2,-30,-298.6,-32.6,-297.4,-35]},alpha:0},1).wait(1).to({x:292.55,y:56.6},0).to({regX:0.1,regY:0.1,scaleX:1.0999,scaleY:1.0999,guide:{path:[292.6,56.3,268.7,57.7,243.8,58.5]},alpha:0.4414},7).wait(1));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_18_Layer_22 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_22
	this.instance = new lib.Symbol23();
	this.instance.parent = this;
	this.instance.setTransform(97.5,-14.85);
	this.instance.alpha = 0.5;

	this.timeline.addTween(cjs.Tween.get(this.instance).to({guide:{path:[97.5,-14.7,97.2,-14.8,96.8,-14.8,84.1,-17.2,77.7,-20.1,71.4,-22.9,71.4,-26.3,71.4,-26.8,71.5,-27.2]},alpha:0.0117},40).to({guide:{path:[71.5,-27.1,71.6,-27.5,71.7,-27.8]},alpha:0},1).wait(1).to({x:240.9,y:-21.25},0).to({guide:{path:[240.9,-21.1,234.8,-17.6,219.6,-14.8,194.2,-10,158.3,-10,124.1,-10,99.5,-14.3]},alpha:0.4883},76).wait(1));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_18_Layer_21 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_21
	this.instance = new lib.Symbol23();
	this.instance.parent = this;
	this.instance.setTransform(153.45,-6.9);
	this.instance.alpha = 0.5;

	this.timeline.addTween(cjs.Tween.get(this.instance).to({guide:{path:[153.4,-7.4,114.7,-7.7,86.9,-13,72.2,-15.8,64.8,-19.1,57.4,-22.4,57.4,-26.3,57.4,-26.4,57.4,-26.4]},alpha:0.0117},45).to({guide:{path:[57.3,-26.5,57.4,-27.6,58,-28.6]},alpha:0},1).wait(1).to({x:258.85,y:-25.45},0).to({guide:{path:[258.9,-25.4,257.2,-18.2,229.4,-13,199.9,-7.4,158.2,-7.4,156.6,-7.4,155,-7.4]},alpha:0.4883},71).wait(1));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_18_Layer_20 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_20
	this.instance = new lib.Symbol23();
	this.instance.parent = this;
	this.instance.setTransform(220.8,48.55,1.197,1.197);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({scaleX:1.1969,scaleY:1.1969,guide:{path:[220.7,48.2,190.3,49,158.1,49,-7.5,49,-124.6,26.6,-222.8,8.3,-238.6,-16.6]},alpha:0.0078},104).to({scaleX:1.197,scaleY:1.197,guide:{path:[-238.6,-16.6,-239.9,-18.6,-240.6,-20.7]},alpha:0},1).wait(1).to({x:315.25,y:43.15},0).to({scaleX:1.1969,scaleY:1.1969,guide:{path:[315.2,43.1,273.7,46.5,228,47.9]},alpha:0.9219},12).wait(1));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_18_Layer_18 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_18
	this.instance = new lib.Symbol23();
	this.instance.parent = this;
	this.instance.setTransform(184.55,3.6);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({guide:{path:[184.6,3.4,173.2,3.7,161.3,3.7,93.7,3.7,46,-5.4,1.2,-13.7,-1.6,-25.4]},alpha:0.0117},97).to({guide:{path:[-1.6,-25.4,-1.8,-26.2,-1.8,-27,-1.8,-27.2,-1.8,-27.4]},alpha:0},1).wait(1).to({x:246.6,y:-0.75},0).to({guide:{path:[246.6,-0.7,219.6,2.4,187.6,3.4]},alpha:0.9492},19).wait(1));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_18_Layer_17 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_17
	this.instance = new lib.Symbol23();
	this.instance.parent = this;
	this.instance.setTransform(195.35,54.8);
	this.instance.alpha = 0;

	this.timeline.addTween(cjs.Tween.get(this.instance).to({guide:{path:[195.3,54.8,143.9,57.7,86,57.7,16.7,57.7,-43.6,53.5]},alpha:1},59).to({guide:{path:[-43.7,53.5,-116.4,48.4,-175.5,37.2,-249.8,23.3,-273.2,5.2]},alpha:0.5078},58).to({guide:{path:[-273.1,5.1,-274.8,3.8,-276.2,2.5]},alpha:0.5},1).wait(1));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_18_Layer_16 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_16
	this.instance = new lib.Symbol23();
	this.instance.parent = this;
	this.instance.setTransform(185.65,33.6,0.82,0.82);
	this.instance.alpha = 0;

	this.timeline.addTween(cjs.Tween.get(this.instance).to({guide:{path:[185.6,34.3,166.6,34.7,146.7,34.7,67.3,34.7,2.2,28.1]},alpha:1},59).to({guide:{path:[2.1,28.1,-37.6,24.1,-71.9,17.6,-117.2,9.1,-139.8,-1.2,-162.4,-11.5,-162.4,-23.6,-162.4,-24.5,-162.3,-25.4]},alpha:0.0156},58).to({guide:{path:[-162.2,-25.5,-162,-27,-161.4,-28.6]},alpha:0},1).wait(1));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_18_Layer_15 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_15
	this.instance = new lib.Symbol23();
	this.instance.parent = this;
	this.instance.setTransform(-184.55,23.7,0.6909,0.6909);
	this.instance.alpha = 0.5;

	this.timeline.addTween(cjs.Tween.get(this.instance).to({guide:{path:[-184.4,23.8,-244.1,8.5,-250,-10.8]},alpha:0.0156},31).to({guide:{path:[-250.1,-10.8,-250.4,-12,-250.6,-13.3]},alpha:0},1).wait(1).to({x:266.7,y:44},0).to({regY:0.1,guide:{path:[266.7,44,192.7,51.5,102.5,51.5,-43.7,51.5,-147.3,31.9,-164.8,28.6,-179.3,25.1]},alpha:0.5},85).wait(1));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_18_Layer_14 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_14
	this.instance = new lib.Symbol23();
	this.instance.parent = this;
	this.instance.setTransform(-207.15,38.95);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({guide:{path:[-207.1,38.9,-295.8,20.9,-306.4,-3.1]},alpha:0.0352},29).to({guide:{path:[-306.4,-3.1,-307.2,-4.9,-307.6,-6.8]},alpha:0},1).wait(1).to({x:240.35,y:55.5},0).to({guide:{path:[240.3,55.3,164.8,62.3,73.9,62.3,-84.1,62.3,-196,41.1,-199.1,40.5,-202,39.9]},alpha:0.9883},87).wait(1));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_18_Layer_13 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_13
	this.instance = new lib.Symbol23();
	this.instance.parent = this;
	this.instance.setTransform(-167.55,39.7);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({guide:{path:[-167.5,39.7,-172.8,38.7,-178.1,37.7,-280.2,18.5,-286.4,-8.2]},alpha:0.0273},38).to({guide:{path:[-286.4,-8.2,-286.8,-9.9,-286.8,-11.7,-286.8,-11.7,-286.8,-11.8]},alpha:0},1).wait(1).to({x:255.25,y:50.7},0).to({guide:{path:[255.3,50.8,178.2,58.5,84.4,58.5,-58.2,58.5,-162.1,40.7]},alpha:0.9883},78).wait(1));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_18_Layer_12 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_12
	this.instance = new lib.Symbol23();
	this.instance.parent = this;
	this.instance.setTransform(-121.9,58.7);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({guide:{path:[-121.9,58.4,-172.4,53.3,-216.2,45,-320.3,25.5,-329.8,-1.6]},alpha:0.0234},42).to({guide:{path:[-329.8,-1.7,-330.7,-4.2,-330.8,-6.9]},alpha:0},1).wait(1).to({x:272.75,y:55.1},0).to({guide:{path:[272.8,55.1,179.5,66.7,60.7,66.7,-36.4,66.7,-116.5,58.9]},alpha:0.9883},74).wait(1));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_18_Layer_11 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_11
	this.instance = new lib.Symbol23();
	this.instance.parent = this;
	this.instance.setTransform(-64.35,13.45,0.6848,0.6848);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({regX:-0.1,guide:{path:[-64.4,13.5,-139.3,-1.5,-144,-22]},alpha:0.0273},34).to({regX:0,guide:{path:[-143.9,-22,-144.2,-23.4,-144.2,-24.8]},alpha:0},1).wait(1).to({x:245.4,y:28.9},0).to({guide:{path:[245.3,28.9,202.4,31.5,153.9,31.5,30.4,31.5,-57,15,-58.8,14.7,-60.6,14.3]},alpha:0.9883},82).wait(1));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_18_Layer_10 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_10
	this.instance = new lib.Symbol23();
	this.instance.parent = this;
	this.instance.setTransform(-88.75,2.4);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({guide:{path:[-88.7,2.4,-129.1,-9.9,-130.4,-25.1]},alpha:0.0313},30).to({guide:{path:[-130.3,-25.1,-130.4,-25.6,-130.4,-26.1,-130.4,-26.6,-130.3,-27]},alpha:0},1).wait(1).to({x:211.3,y:27.8},0).to({guide:{path:[211.3,28.1,186.9,28.9,160.9,28.9,40.3,28.9,-45,12.8,-68.4,8.4,-85.3,3.4]},alpha:0.9883},86).wait(1));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_18_Layer_5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_5
	this.instance = new lib.Symbol22();
	this.instance.parent = this;
	this.instance.setTransform(97.5,0.95);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({guide:{path:[97.5,0.9,74.5,-1.2,55.1,-4.9,15.9,-12.2,11.6,-22.3]},alpha:0.0156},58).to({guide:{path:[11.4,-22.3,11.1,-23,10.9,-23.8]},alpha:0},1).wait(1).to({x:221.95,y:1.15},0).to({guide:{path:[222,1.3,194.3,3.5,161.6,3.5,127.9,3.5,99.7,1.1]},alpha:0.9805},58).wait(1));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_18_Layer_4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_4
	this.instance = new lib.Symbol19();
	this.instance.parent = this;
	this.instance.setTransform(-84.45,45.8);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({guide:{path:[-84.4,45.8,-121.6,41.1,-155.2,34.6,-276.1,12,-285.7,-19.1]},alpha:0.0234},40).to({guide:{path:[-285.6,-19.1,-286.5,-21.8,-286.5,-24.5,-286.5,-24.6,-286.5,-24.6]},alpha:0},1).wait(1).to({x:201,y:59.4},0).to({guide:{path:[201,59.4,181.5,59.7,161.4,59.7,25.9,59.7,-80.6,46.3]},alpha:0.9883},76).wait(1));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_18_Layer_3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_3
	this.instance = new lib.Symbol20();
	this.instance.parent = this;
	this.instance.setTransform(124.5,6);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({guide:{path:[124.5,6.2,75,4.3,37.1,-3,-10.4,-11.8,-14.3,-24]},alpha:0.0156},71).to({guide:{path:[-14.3,-24.1,-14.6,-25.1,-14.6,-26.1]},alpha:0},1).wait(1).to({x:215.45,y:5.35},0).to({guide:{path:[215.4,5.4,190.3,6.9,161.9,6.9,143.5,6.9,126.4,6.3]},alpha:0.9805},45).wait(1));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_18_Layer_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Symbol23();
	this.instance.parent = this;
	this.instance.setTransform(0.15,-0.75);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({guide:{path:[0.2,-0.8,-46.6,-10.8,-50.9,-24.1]},alpha:0.0391},24).to({guide:{path:[-50.9,-24.1,-51.3,-25.3,-51.4,-26.5]},alpha:0},1).wait(1).to({x:208.8,y:12},0).to({guide:{path:[208.8,12.1,185.9,13.1,160.8,13.1,73,13.1,10.8,1.4,6.5,0.6,2.5,-0.2]},alpha:0.9883},92).wait(1));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_15_Layer_4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_4
	this.instance = new lib.Symbol17();
	this.instance.parent = this;
	this.instance.setTransform(15.4,-21.6,0.5,0.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol_15_Layer_4, null, null);


(lib.Symbol_10_Layer_3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_3
	this.instance = new lib.Symbol11();
	this.instance.parent = this;
	this.instance.setTransform(3.4,-13.95,0.9999,0.9999,28.0346,0,0,-3.8,3.4);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(13).to({_off:false},0).to({scaleX:1,scaleY:1,rotation:32.7829,guide:{path:[3.5,-13.8,-2.4,-11,-6.8,-5.4,-7.3,-4.8,-7.7,-4.2]}},5,cjs.Ease.get(-0.5)).wait(1).to({regX:0,regY:0,rotation:28.5433,x:-4.3452,y:-3.03},0).wait(1).to({rotation:24.5961,x:-5.6032,y:-1.1821},0).wait(1).to({rotation:20.9413,x:-6.6524,y:0.6762},0).wait(1).to({rotation:17.5789,x:-7.4926,y:2.497},0).wait(1).to({rotation:14.5089,x:-8.1392,y:4.2413},0).wait(1).to({rotation:11.7312,x:-8.6155,y:5.8734},0).wait(1).to({rotation:9.2459,x:-8.9502,y:7.3648},0).wait(1).to({scaleX:0.9999,scaleY:0.9999,rotation:7.0531,x:-9.173,y:8.6955},0).wait(1).to({scaleX:1,scaleY:1,rotation:5.1526,x:-9.3121,y:9.8532},0).wait(1).to({scaleX:0.9999,scaleY:0.9999,rotation:3.5445,x:-9.3919,y:10.8321},0).wait(1).to({scaleX:1,scaleY:1,rotation:2.2287,x:-9.4324,y:11.6303},0).wait(1).to({rotation:1.2054,x:-9.4491,y:12.2483},0).wait(1).to({rotation:0.4744,x:-9.4534,y:12.6877},0).wait(1).to({regX:-3.8,regY:3.5,scaleX:0.9999,scaleY:0.9999,rotation:0.0358,x:-13.25,y:16.4},0).to({regX:-3.7,scaleX:1,scaleY:1,rotation:0,x:-13.15,y:16.7},1).wait(13).to({x:-13.2,y:16.4},0).wait(1).to({regX:0,regY:0,scaleX:0.9999,scaleY:0.9999,rotation:-1.8004,x:-9.6121,y:12.7853},0).wait(1).to({scaleX:0.9996,scaleY:0.9996,rotation:-7.2015,x:-9.9692,y:12.4633},0).wait(1).to({scaleX:0.9992,scaleY:0.9992,rotation:-16.2034,x:-10.6261,y:12.0059},0).wait(1).to({regX:-3.7,regY:3.5,scaleX:0.9986,scaleY:0.9986,rotation:-28.806,x:-13.2,y:16.4},0).to({regX:-3.8,scaleX:1,scaleY:1,rotation:-44.9994,y:16.75},1).to({_off:true},1).wait(7));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_10_Layer_2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.instance = new lib.Symbol12();
	this.instance.parent = this;
	this.instance.setTransform(3.65,-13.95,1,1,121.4487,0,0,3.4,3.6);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(13).to({_off:false},0).to({regX:3.6,rotation:44.9994,guide:{path:[3.6,-13.8,-2.3,-11,-6.8,-5.3,-7.3,-4.6,-7.8,-3.9]}},5,cjs.Ease.get(-0.5)).wait(1).to({regX:0,regY:0,rotation:39.18,x:-9.8418,y:-6.6787},0).wait(1).to({rotation:33.762,x:-11.4519,y:-4.447},0).wait(1).to({rotation:28.7453,x:-12.8003,y:-2.205},0).wait(1).to({rotation:24.1299,x:-13.8921,y:-0.0088},0).wait(1).to({rotation:19.9159,x:-14.7476,y:2.0956},0).wait(1).to({rotation:16.1032,x:-15.3951,y:4.0668},0).wait(1).to({rotation:12.6919,x:-15.8682,y:5.8716},0).wait(1).to({scaleX:0.9999,scaleY:0.9999,rotation:9.6819,x:-16.201,y:7.4865},0).wait(1).to({rotation:7.0732,x:-16.4259,y:8.8961},0).wait(1).to({scaleX:1,scaleY:1,rotation:4.8659,x:-16.5712,y:10.092},0).wait(1).to({rotation:3.0599,x:-16.6603,y:11.0703},0).wait(1).to({rotation:1.6552,x:-16.7116,y:11.8299},0).wait(1).to({rotation:0.6518,x:-16.7388,y:12.3713},0).wait(1).to({regX:3.5,regY:3.6,scaleX:0.9999,scaleY:0.9999,rotation:0.0498,x:-13.25,y:16.3},0).to({regX:3.6,regY:3.7,scaleX:1,scaleY:1,rotation:0,x:-13.15,y:16.7},1).wait(13).to({x:-13.2,y:16.4},0).wait(1).to({regX:0,regY:0,scaleX:0.9999,scaleY:0.9999,rotation:1.8004,x:-16.6817,y:12.5891},0).wait(1).to({scaleX:0.9996,scaleY:0.9996,rotation:7.2015,x:-16.3067,y:12.2793},0).wait(1).to({scaleX:0.9992,scaleY:0.9992,rotation:16.2034,x:-15.6226,y:11.846},0).wait(1).to({regX:3.6,regY:3.8,scaleX:0.9986,scaleY:0.9986,rotation:28.806,x:-13.25,y:16.5},0).to({scaleX:1,scaleY:1,rotation:44.9994,x:-13.2,y:16.75},1).to({_off:true},1).wait(7));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_3_Symbol_2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Symbol_2
	this.instance = new lib.Symbol2();
	this.instance.parent = this;
	this.instance.setTransform(-58.8,8.1,0.8859,0.8277,0,0,180,-0.1,0.1);
	this.instance.alpha = 0;
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(19).to({_off:false},0).to({alpha:1},15).wait(15).to({alpha:0},15).to({_off:true},1).wait(10));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_3_Symbol_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Symbol_1
	this.instance = new lib.Symbol1();
	this.instance.parent = this;
	this.instance.setTransform(56.25,5.8);
	this.instance.alpha = 0;
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(19).to({_off:false},0).to({alpha:1},15).wait(15).to({alpha:0},15).to({_off:true},1).wait(10));

}).prototype = p = new cjs.MovieClip();


(lib.Scene_1_Symbol_9 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Symbol_9
	this.instance = new lib.Symbol8();
	this.instance.parent = this;
	this.instance.setTransform(154.95,502.55,0.6521,0.6521,0,0,0,0.1,0.1);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(4).to({y:511},10,cjs.Ease.get(-0.3)).to({scaleX:0.6195,scaleY:0.6195,x:158.85,y:478.65},20,cjs.Ease.get(1)).wait(16).to({regX:0,regY:0,scaleX:0.6197,scaleY:0.6197,x:158.7669,y:478.8014},0).wait(1).to({scaleX:0.6203,scaleY:0.6203,x:158.6938,y:479.2453},0).wait(1).to({scaleX:0.6213,scaleY:0.6213,x:158.5755,y:479.9642},0).wait(1).to({scaleX:0.6227,scaleY:0.6227,x:158.4069,y:480.989},0).wait(1).to({scaleX:0.6246,scaleY:0.6246,x:158.1843,y:482.3418},0).wait(1).to({scaleX:0.6269,scaleY:0.6269,x:157.9077,y:484.0229},0).wait(1).to({scaleX:0.6296,scaleY:0.6296,x:157.5831,y:485.9953},0).wait(1).to({scaleX:0.6325,scaleY:0.6325,x:157.2246,y:488.1741},0).wait(1).to({scaleX:0.6356,scaleY:0.6356,x:156.8528,y:490.4342},0).wait(1).to({scaleX:0.6386,scaleY:0.6386,x:156.4899,y:492.6397},0).wait(1).to({scaleX:0.6414,scaleY:0.6414,x:156.1543,y:494.679},0).wait(1).to({scaleX:0.6439,scaleY:0.6439,x:155.8574,y:496.4834},0).wait(1).to({scaleX:0.646,scaleY:0.646,x:155.6038,y:498.0243},0).wait(1).to({scaleX:0.6477,scaleY:0.6477,x:155.3937,y:499.3014},0).wait(1).to({scaleX:0.6491,scaleY:0.6491,x:155.2245,y:500.3295},0).wait(1).to({scaleX:0.6502,scaleY:0.6502,x:155.0928,y:501.1302},0).wait(1).to({scaleX:0.651,scaleY:0.651,x:154.9947,y:501.7266},0).wait(1).to({scaleX:0.6516,scaleY:0.6516,x:154.9265,y:502.1408},0).wait(1).to({scaleX:0.6519,scaleY:0.6519,x:154.8851,y:502.3927},0).wait(1).to({regX:0.1,regY:0.1,scaleX:0.6521,scaleY:0.6521,x:154.95,y:502.55},0).wait(6));

}).prototype = p = new cjs.MovieClip();


(lib.Scene_1_Symbol_7 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Symbol_7
	this.instance = new lib.Symbol7();
	this.instance.parent = this;
	this.instance.setTransform(146.85,469.45,0.6521,0.6521,0,0,0,-18.9,-12.1);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(4).to({regY:-12.2,scaleX:0.6847,scaleY:0.6847,x:141.75,y:470.8},10,cjs.Ease.get(-0.3)).to({regX:-18.7,regY:-12.1,scaleX:0.6194,scaleY:0.6194,x:150.75,y:469.4},20,cjs.Ease.get(1)).wait(16).to({regX:0,regY:0,scaleX:0.6197,scaleY:0.6197,x:162.3219,y:476.9035},0).wait(1).to({scaleX:0.6203,scaleY:0.6203,x:162.2601,y:476.9113},0).wait(1).to({scaleX:0.6213,scaleY:0.6213,x:162.1599,y:476.9238},0).wait(1).to({scaleX:0.6227,scaleY:0.6227,x:162.0171,y:476.9417},0).wait(1).to({scaleX:0.6245,scaleY:0.6245,x:161.8286,y:476.9653},0).wait(1).to({scaleX:0.6268,scaleY:0.6268,x:161.5943,y:476.9947},0).wait(1).to({scaleX:0.6295,scaleY:0.6295,x:161.3195,y:477.0292},0).wait(1).to({scaleX:0.6325,scaleY:0.6325,x:161.0159,y:477.0672},0).wait(1).to({scaleX:0.6356,scaleY:0.6356,x:160.7009,y:477.1067},0).wait(1).to({scaleX:0.6386,scaleY:0.6386,x:160.3936,y:477.1452},0).wait(1).to({scaleX:0.6414,scaleY:0.6414,x:160.1094,y:477.1808},0).wait(1).to({scaleX:0.6439,scaleY:0.6439,x:159.858,y:477.2123},0).wait(1).to({scaleX:0.646,scaleY:0.646,x:159.6433,y:477.2392},0).wait(1).to({scaleX:0.6477,scaleY:0.6477,x:159.4653,y:477.2615},0).wait(1).to({scaleX:0.6491,scaleY:0.6491,x:159.322,y:477.2795},0).wait(1).to({scaleX:0.6502,scaleY:0.6502,x:159.2105,y:477.2935},0).wait(1).to({scaleX:0.651,scaleY:0.651,x:159.1274,y:477.3039},0).wait(1).to({scaleX:0.6516,scaleY:0.6516,x:159.0696,y:477.3111},0).wait(1).to({scaleX:0.6519,scaleY:0.6519,x:159.0345,y:477.3155},0).wait(1).to({regX:-18.9,regY:-12.1,scaleX:0.6521,scaleY:0.6521,x:146.85,y:469.45},0).wait(6));

}).prototype = p = new cjs.MovieClip();


(lib.Scene_1_Symbol_6 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Symbol_6
	this.instance = new lib.Symbol6();
	this.instance.parent = this;
	this.instance.setTransform(52.8,370.5,0.6521,0.6521,0,0,0,4.8,-10.8);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(4).to({scaleX:0.6847,scaleY:0.6847,x:42.35,y:368},10,cjs.Ease.get(-0.3)).to({regX:4.9,scaleX:0.6195,scaleY:0.6195,x:61.65,y:375.45},20,cjs.Ease.get(1)).wait(16).to({regX:0,regY:0,scaleX:0.6197,scaleY:0.6197,x:58.524,y:382.111},0).wait(1).to({scaleX:0.6203,scaleY:0.6203,x:58.3566,y:382.0249},0).wait(1).to({scaleX:0.6213,scaleY:0.6213,x:58.0854,y:381.8856},0).wait(1).to({scaleX:0.6227,scaleY:0.6227,x:57.6989,y:381.6869},0).wait(1).to({scaleX:0.6246,scaleY:0.6246,x:57.1886,y:381.4247},0).wait(1).to({scaleX:0.6269,scaleY:0.6269,x:56.5545,y:381.0989},0).wait(1).to({scaleX:0.6296,scaleY:0.6296,x:55.8105,y:380.7165},0).wait(1).to({scaleX:0.6325,scaleY:0.6325,x:54.9886,y:380.2942},0).wait(1).to({scaleX:0.6356,scaleY:0.6356,x:54.1361,y:379.8562},0).wait(1).to({scaleX:0.6386,scaleY:0.6386,x:53.3042,y:379.4287},0).wait(1).to({scaleX:0.6414,scaleY:0.6414,x:52.5349,y:379.0334},0).wait(1).to({scaleX:0.6439,scaleY:0.6439,x:51.8543,y:378.6836},0).wait(1).to({scaleX:0.646,scaleY:0.646,x:51.273,y:378.3849},0).wait(1).to({scaleX:0.6477,scaleY:0.6477,x:50.7913,y:378.1374},0).wait(1).to({scaleX:0.6491,scaleY:0.6491,x:50.4035,y:377.9381},0).wait(1).to({scaleX:0.6502,scaleY:0.6502,x:50.1015,y:377.7829},0).wait(1).to({scaleX:0.651,scaleY:0.651,x:49.8765,y:377.6673},0).wait(1).to({scaleX:0.6516,scaleY:0.6516,x:49.7203,y:377.587},0).wait(1).to({scaleX:0.6519,scaleY:0.6519,x:49.6253,y:377.5382},0).wait(1).to({regX:4.8,regY:-10.8,scaleX:0.6521,scaleY:0.6521,x:52.8,y:370.5},0).wait(6));

}).prototype = p = new cjs.MovieClip();


(lib.Scene_1_Symbol_5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Symbol_5
	this.instance = new lib.Symbol5();
	this.instance.parent = this;
	this.instance.setTransform(52.75,370.55,0.6521,0.6521,0,0,0,18.4,0.6);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(4).to({regX:18.5,scaleX:0.6847,scaleY:0.6847,x:42.35,y:368},10,cjs.Ease.get(-0.3)).to({scaleX:0.6195,scaleY:0.6195,x:61.6,y:375.5},20,cjs.Ease.get(1)).wait(16).to({regX:0.2,regY:4.2,scaleX:0.6197,scaleY:0.6197,x:50.15,y:377.7},0).wait(1).to({scaleX:0.6203,scaleY:0.6203,x:49.95,y:377.6},0).wait(1).to({scaleX:0.6213,scaleY:0.6213,x:49.7,y:377.45},0).wait(1).to({scaleX:0.6227,scaleY:0.6227,x:49.3,y:377.25},0).wait(1).to({scaleX:0.6246,scaleY:0.6246,x:48.75,y:376.95},0).wait(1).to({scaleX:0.6269,scaleY:0.6269,x:48.15,y:376.65},0).wait(1).to({scaleX:0.6296,scaleY:0.6296,x:47.35,y:376.25},0).wait(1).to({scaleX:0.6325,scaleY:0.6325,x:46.5,y:375.75},0).wait(1).to({scaleX:0.6356,scaleY:0.6356,x:45.6,y:375.3},0).wait(1).to({scaleX:0.6386,scaleY:0.6386,x:44.7,y:374.9},0).wait(1).to({scaleX:0.6414,scaleY:0.6414,x:43.9,y:374.45},0).wait(1).to({scaleX:0.6439,scaleY:0.6439,x:43.2,y:374.1},0).wait(1).to({scaleX:0.646,scaleY:0.646,x:42.6,y:373.75},0).wait(1).to({scaleX:0.6477,scaleY:0.6477,x:42.1,y:373.5},0).wait(1).to({scaleX:0.6491,scaleY:0.6491,x:41.65,y:373.35},0).wait(1).to({scaleX:0.6502,scaleY:0.6502,x:41.35,y:373.15},0).wait(1).to({scaleX:0.651,scaleY:0.651,x:41.1,y:373.05},0).wait(1).to({scaleX:0.6516,scaleY:0.6516,x:40.95,y:372.95},0).wait(1).to({scaleX:0.6519,scaleY:0.6519,x:40.85,y:372.9},0).wait(1).to({regX:18.4,regY:0.6,scaleX:0.6521,scaleY:0.6521,x:52.75,y:370.55},0).wait(6));

}).prototype = p = new cjs.MovieClip();


(lib.Scene_1_Symbol_4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Symbol_4
	this.instance = new lib.Symbol4();
	this.instance.parent = this;
	this.instance.setTransform(163.75,503,0.6521,0.6521,0,0,0,2.6,11.2);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(4).to({scaleX:0.6847,scaleY:0.6847,x:159.5,y:506.3},10,cjs.Ease.get(-0.3)).to({scaleX:0.6195,scaleY:0.6195,x:167,y:501.7},20,cjs.Ease.get(1)).wait(16).to({regX:-4.8,regY:-4.1,scaleX:0.6197,scaleY:0.6197,x:162.4,y:492.2},0).wait(1).to({scaleX:0.6203,scaleY:0.6203,x:162.3},0).wait(1).to({scaleX:0.6213,scaleY:0.6213,x:162.2,y:492.25},0).wait(1).to({scaleX:0.6227,scaleY:0.6227,x:162.05},0).wait(1).to({scaleX:0.6246,scaleY:0.6246,x:161.85,y:492.3},0).wait(1).to({scaleX:0.6269,scaleY:0.6269,x:161.6,y:492.4},0).wait(1).to({scaleX:0.6296,scaleY:0.6296,x:161.35},0).wait(1).to({scaleX:0.6325,scaleY:0.6325,x:161,y:492.5},0).wait(1).to({scaleX:0.6356,scaleY:0.6356,x:160.65,y:492.6},0).wait(1).to({scaleX:0.6386,scaleY:0.6386,x:160.35,y:492.7},0).wait(1).to({scaleX:0.6414,scaleY:0.6414,x:160},0).wait(1).to({scaleX:0.6439,scaleY:0.6439,x:159.75,y:492.8},0).wait(1).to({scaleX:0.646,scaleY:0.646,x:159.55,y:492.85},0).wait(1).to({scaleX:0.6477,scaleY:0.6477,x:159.35,y:492.9},0).wait(1).to({scaleX:0.6491,scaleY:0.6491,x:159.25,y:492.95},0).wait(1).to({scaleX:0.6502,scaleY:0.6502,x:159.1},0).wait(1).to({scaleX:0.651,scaleY:0.651,x:159.05,y:493},0).wait(1).to({scaleX:0.6516,scaleY:0.6516,x:158.95},0).wait(1).to({scaleX:0.6519,scaleY:0.6519,x:158.9},0).wait(1).to({regX:2.6,regY:11.2,scaleX:0.6521,scaleY:0.6521,x:163.75,y:503},0).wait(6));

}).prototype = p = new cjs.MovieClip();


(lib.Scene_1_podloj_png = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// podloj_png
	this.instance = new lib.Symbol9();
	this.instance.parent = this;
	this.instance.setTransform(296.65,587.85,0.6521,0.6521,0,0,0,128.9,-6.6);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(4).to({regY:-6.5,scaleX:0.6847,scaleY:0.6847,x:298.25,y:594.65},10,cjs.Ease.get(-0.3)).to({regX:128.8,regY:-6.6,scaleX:0.6521,scaleY:0.6521,x:292.45,y:581.6},20,cjs.Ease.get(1)).wait(16).to({regX:0,regY:0,x:208.4855,y:585.9527},0).wait(1).to({x:208.5638,y:586.0687},0).wait(1).to({x:208.6906,y:586.2567},0).wait(1).to({x:208.8713,y:586.5247},0).wait(1).to({x:209.1099,y:586.8785},0).wait(1).to({x:209.4064,y:587.3181},0).wait(1).to({x:209.7543,y:587.8339},0).wait(1).to({x:210.1386,y:588.4037},0).wait(1).to({x:210.5372,y:588.9947},0).wait(1).to({x:210.9262,y:589.5715},0).wait(1).to({x:211.2858,y:590.1048},0).wait(1).to({x:211.6041,y:590.5766},0).wait(1).to({x:211.8758,y:590.9796},0).wait(1).to({x:212.1011,y:591.3135},0).wait(1).to({x:212.2824,y:591.5824},0).wait(1).to({x:212.4236,y:591.7918},0).wait(1).to({x:212.5288,y:591.9478},0).wait(1).to({x:212.6018,y:592.0561},0).wait(1).to({x:212.6463,y:592.1219},0).wait(1).to({regX:128.9,regY:-6.6,x:296.65,y:587.85},0).wait(6));

}).prototype = p = new cjs.MovieClip();


(lib.Scene_1_Layer_17 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_17
	this.instance = new lib.Symbol13();
	this.instance.parent = this;
	this.instance.setTransform(121.1,261.35,0.652,0.652,0,0,0,0.1,0.3);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(75));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol18 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_118 = function() {
		this.___loopingOver___ = true;
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(118).call(this.frame_118).wait(1));

	// Layer_2_obj_
	this.Layer_2 = new lib.Symbol_18_Layer_2();
	this.Layer_2.name = "Layer_2";
	this.Layer_2.parent = this;
	this.Layer_2.setTransform(-134,55.5,1,1,0,0,0,-134,55.5);
	this.Layer_2.depth = 0;
	this.Layer_2.isAttachedToCamera = 0
	this.Layer_2.isAttachedToMask = 0
	this.Layer_2.layerDepth = 0
	this.Layer_2.layerIndex = 0
	this.Layer_2.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_2).wait(119));

	// Layer_75_obj_
	this.Layer_75 = new lib.Symbol_18_Layer_75();
	this.Layer_75.name = "Layer_75";
	this.Layer_75.parent = this;
	this.Layer_75.setTransform(179,-18.4,1,1,0,0,0,179,-18.4);
	this.Layer_75.depth = 0;
	this.Layer_75.isAttachedToCamera = 0
	this.Layer_75.isAttachedToMask = 0
	this.Layer_75.layerDepth = 0
	this.Layer_75.layerIndex = 1
	this.Layer_75.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_75).wait(119));

	// Layer_74_obj_
	this.Layer_74 = new lib.Symbol_18_Layer_74();
	this.Layer_74.name = "Layer_74";
	this.Layer_74.parent = this;
	this.Layer_74.setTransform(148.8,-13.9,1,1,0,0,0,148.8,-13.9);
	this.Layer_74.depth = 0;
	this.Layer_74.isAttachedToCamera = 0
	this.Layer_74.isAttachedToMask = 0
	this.Layer_74.layerDepth = 0
	this.Layer_74.layerIndex = 2
	this.Layer_74.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_74).wait(119));

	// Layer_73_obj_
	this.Layer_73 = new lib.Symbol_18_Layer_73();
	this.Layer_73.name = "Layer_73";
	this.Layer_73.parent = this;
	this.Layer_73.setTransform(128.3,-16,1,1,0,0,0,128.3,-16);
	this.Layer_73.depth = 0;
	this.Layer_73.isAttachedToCamera = 0
	this.Layer_73.isAttachedToMask = 0
	this.Layer_73.layerDepth = 0
	this.Layer_73.layerIndex = 3
	this.Layer_73.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_73).wait(119));

	// Layer_72_obj_
	this.Layer_72 = new lib.Symbol_18_Layer_72();
	this.Layer_72.name = "Layer_72";
	this.Layer_72.parent = this;
	this.Layer_72.setTransform(191,-15.3,1,1,0,0,0,191,-15.3);
	this.Layer_72.depth = 0;
	this.Layer_72.isAttachedToCamera = 0
	this.Layer_72.isAttachedToMask = 0
	this.Layer_72.layerDepth = 0
	this.Layer_72.layerIndex = 4
	this.Layer_72.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_72).wait(119));

	// Layer_71_obj_
	this.Layer_71 = new lib.Symbol_18_Layer_71();
	this.Layer_71.name = "Layer_71";
	this.Layer_71.parent = this;
	this.Layer_71.setTransform(69.2,-20.7,1,1,0,0,0,69.2,-20.7);
	this.Layer_71.depth = 0;
	this.Layer_71.isAttachedToCamera = 0
	this.Layer_71.isAttachedToMask = 0
	this.Layer_71.layerDepth = 0
	this.Layer_71.layerIndex = 5
	this.Layer_71.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_71).wait(119));

	// Layer_70_obj_
	this.Layer_70 = new lib.Symbol_18_Layer_70();
	this.Layer_70.name = "Layer_70";
	this.Layer_70.parent = this;
	this.Layer_70.setTransform(154.2,22.1,1,1,0,0,0,154.2,22.1);
	this.Layer_70.depth = 0;
	this.Layer_70.isAttachedToCamera = 0
	this.Layer_70.isAttachedToMask = 0
	this.Layer_70.layerDepth = 0
	this.Layer_70.layerIndex = 6
	this.Layer_70.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_70).wait(119));

	// Layer_69_obj_
	this.Layer_69 = new lib.Symbol_18_Layer_69();
	this.Layer_69.name = "Layer_69";
	this.Layer_69.parent = this;
	this.Layer_69.setTransform(-302.5,38.6,1,1,0,0,0,-302.5,38.6);
	this.Layer_69.depth = 0;
	this.Layer_69.isAttachedToCamera = 0
	this.Layer_69.isAttachedToMask = 0
	this.Layer_69.layerDepth = 0
	this.Layer_69.layerIndex = 7
	this.Layer_69.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_69).wait(119));

	// Layer_68_obj_
	this.Layer_68 = new lib.Symbol_18_Layer_68();
	this.Layer_68.name = "Layer_68";
	this.Layer_68.parent = this;
	this.Layer_68.setTransform(-262.5,26.5,1,1,0,0,0,-262.5,26.5);
	this.Layer_68.depth = 0;
	this.Layer_68.isAttachedToCamera = 0
	this.Layer_68.isAttachedToMask = 0
	this.Layer_68.layerDepth = 0
	this.Layer_68.layerIndex = 8
	this.Layer_68.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_68).wait(119));

	// Layer_67_obj_
	this.Layer_67 = new lib.Symbol_18_Layer_67();
	this.Layer_67.name = "Layer_67";
	this.Layer_67.parent = this;
	this.Layer_67.setTransform(-212.2,5,1,1,0,0,0,-212.2,5);
	this.Layer_67.depth = 0;
	this.Layer_67.isAttachedToCamera = 0
	this.Layer_67.isAttachedToMask = 0
	this.Layer_67.layerDepth = 0
	this.Layer_67.layerIndex = 9
	this.Layer_67.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_67).wait(119));

	// Layer_66_obj_
	this.Layer_66 = new lib.Symbol_18_Layer_66();
	this.Layer_66.name = "Layer_66";
	this.Layer_66.parent = this;
	this.Layer_66.setTransform(-133.8,15.5,1,1,0,0,0,-133.8,15.5);
	this.Layer_66.depth = 0;
	this.Layer_66.isAttachedToCamera = 0
	this.Layer_66.isAttachedToMask = 0
	this.Layer_66.layerDepth = 0
	this.Layer_66.layerIndex = 10
	this.Layer_66.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_66).wait(119));

	// Layer_65_obj_
	this.Layer_65 = new lib.Symbol_18_Layer_65();
	this.Layer_65.name = "Layer_65";
	this.Layer_65.parent = this;
	this.Layer_65.setTransform(-157.1,-2.2,1,1,0,0,0,-157.1,-2.2);
	this.Layer_65.depth = 0;
	this.Layer_65.isAttachedToCamera = 0
	this.Layer_65.isAttachedToMask = 0
	this.Layer_65.layerDepth = 0
	this.Layer_65.layerIndex = 11
	this.Layer_65.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_65).wait(119));

	// Layer_64_obj_
	this.Layer_64 = new lib.Symbol_18_Layer_64();
	this.Layer_64.name = "Layer_64";
	this.Layer_64.parent = this;
	this.Layer_64.setTransform(-130.8,-11.9,1,1,0,0,0,-130.8,-11.9);
	this.Layer_64.depth = 0;
	this.Layer_64.isAttachedToCamera = 0
	this.Layer_64.isAttachedToMask = 0
	this.Layer_64.layerDepth = 0
	this.Layer_64.layerIndex = 12
	this.Layer_64.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_64).wait(119));

	// Layer_62_obj_
	this.Layer_62 = new lib.Symbol_18_Layer_62();
	this.Layer_62.name = "Layer_62";
	this.Layer_62.parent = this;
	this.Layer_62.setTransform(18.35,41.15,1,1,0,0,0,18.4,40.8);
	this.Layer_62.depth = 0;
	this.Layer_62.isAttachedToCamera = 0
	this.Layer_62.isAttachedToMask = 0
	this.Layer_62.layerDepth = 0
	this.Layer_62.layerIndex = 13
	this.Layer_62.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_62).wait(50).to({guide:{path:[18.3,41.1,18.3,41.1,18.3,41.1]}},1).wait(1).to({x:18.35,y:41.15},0).wait(67));

	// Layer_61_obj_
	this.Layer_61 = new lib.Symbol_18_Layer_61();
	this.Layer_61.name = "Layer_61";
	this.Layer_61.parent = this;
	this.Layer_61.setTransform(-28.2,39.6,1,1,0,0,0,-28.2,39.6);
	this.Layer_61.depth = 0;
	this.Layer_61.isAttachedToCamera = 0
	this.Layer_61.isAttachedToMask = 0
	this.Layer_61.layerDepth = 0
	this.Layer_61.layerIndex = 14
	this.Layer_61.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_61).wait(119));

	// Layer_60_obj_
	this.Layer_60 = new lib.Symbol_18_Layer_60();
	this.Layer_60.name = "Layer_60";
	this.Layer_60.parent = this;
	this.Layer_60.setTransform(-15.5,24.9,1,1,0,0,0,-15.5,24.9);
	this.Layer_60.depth = 0;
	this.Layer_60.isAttachedToCamera = 0
	this.Layer_60.isAttachedToMask = 0
	this.Layer_60.layerDepth = 0
	this.Layer_60.layerIndex = 15
	this.Layer_60.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_60).wait(119));

	// Layer_59_obj_
	this.Layer_59 = new lib.Symbol_18_Layer_59();
	this.Layer_59.name = "Layer_59";
	this.Layer_59.parent = this;
	this.Layer_59.setTransform(118,48.9,1,1,0,0,0,118,48.9);
	this.Layer_59.depth = 0;
	this.Layer_59.isAttachedToCamera = 0
	this.Layer_59.isAttachedToMask = 0
	this.Layer_59.layerDepth = 0
	this.Layer_59.layerIndex = 16
	this.Layer_59.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_59).wait(119));

	// Layer_58_obj_
	this.Layer_58 = new lib.Symbol_18_Layer_58();
	this.Layer_58.name = "Layer_58";
	this.Layer_58.parent = this;
	this.Layer_58.setTransform(120.9,29.2,1,1,0,0,0,120.9,29.2);
	this.Layer_58.depth = 0;
	this.Layer_58.isAttachedToCamera = 0
	this.Layer_58.isAttachedToMask = 0
	this.Layer_58.layerDepth = 0
	this.Layer_58.layerIndex = 17
	this.Layer_58.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_58).wait(119));

	// Layer_57_obj_
	this.Layer_57 = new lib.Symbol_18_Layer_57();
	this.Layer_57.name = "Layer_57";
	this.Layer_57.parent = this;
	this.Layer_57.setTransform(56,-4.8,1,1,0,0,0,56,-4.8);
	this.Layer_57.depth = 0;
	this.Layer_57.isAttachedToCamera = 0
	this.Layer_57.isAttachedToMask = 0
	this.Layer_57.layerDepth = 0
	this.Layer_57.layerIndex = 18
	this.Layer_57.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_57).wait(119));

	// Layer_5_obj_
	this.Layer_5 = new lib.Symbol_18_Layer_5();
	this.Layer_5.name = "Layer_5";
	this.Layer_5.parent = this;
	this.Layer_5.setTransform(97.5,1,1,1,0,0,0,97.5,1);
	this.Layer_5.depth = 0;
	this.Layer_5.isAttachedToCamera = 0
	this.Layer_5.isAttachedToMask = 0
	this.Layer_5.layerDepth = 0
	this.Layer_5.layerIndex = 19
	this.Layer_5.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_5).wait(119));

	// Layer_46_obj_
	this.Layer_46 = new lib.Symbol_18_Layer_46();
	this.Layer_46.name = "Layer_46";
	this.Layer_46.parent = this;
	this.Layer_46.setTransform(-196.1,10.6,1,1,0,0,0,-196.1,10.6);
	this.Layer_46.depth = 0;
	this.Layer_46.isAttachedToCamera = 0
	this.Layer_46.isAttachedToMask = 0
	this.Layer_46.layerDepth = 0
	this.Layer_46.layerIndex = 20
	this.Layer_46.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_46).wait(119));

	// Layer_45_obj_
	this.Layer_45 = new lib.Symbol_18_Layer_45();
	this.Layer_45.name = "Layer_45";
	this.Layer_45.parent = this;
	this.Layer_45.setTransform(-218.2,55.1,1,1,0,0,0,-218.2,55.1);
	this.Layer_45.depth = 0;
	this.Layer_45.isAttachedToCamera = 0
	this.Layer_45.isAttachedToMask = 0
	this.Layer_45.layerDepth = 0
	this.Layer_45.layerIndex = 21
	this.Layer_45.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_45).wait(119));

	// Layer_44_obj_
	this.Layer_44 = new lib.Symbol_18_Layer_44();
	this.Layer_44.name = "Layer_44";
	this.Layer_44.parent = this;
	this.Layer_44.setTransform(-50.9,2.8,1,1,0,0,0,-50.9,2.8);
	this.Layer_44.depth = 0;
	this.Layer_44.isAttachedToCamera = 0
	this.Layer_44.isAttachedToMask = 0
	this.Layer_44.layerDepth = 0
	this.Layer_44.layerIndex = 22
	this.Layer_44.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_44).wait(119));

	// Layer_4_obj_
	this.Layer_4 = new lib.Symbol_18_Layer_4();
	this.Layer_4.name = "Layer_4";
	this.Layer_4.parent = this;
	this.Layer_4.setTransform(-84.5,45.8,1,1,0,0,0,-84.5,45.8);
	this.Layer_4.depth = 0;
	this.Layer_4.isAttachedToCamera = 0
	this.Layer_4.isAttachedToMask = 0
	this.Layer_4.layerDepth = 0
	this.Layer_4.layerIndex = 23
	this.Layer_4.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_4).wait(119));

	// Layer_41_obj_
	this.Layer_41 = new lib.Symbol_18_Layer_41();
	this.Layer_41.name = "Layer_41";
	this.Layer_41.parent = this;
	this.Layer_41.setTransform(55.6,25.6,1,1,0,0,0,55.6,25.6);
	this.Layer_41.depth = 0;
	this.Layer_41.isAttachedToCamera = 0
	this.Layer_41.isAttachedToMask = 0
	this.Layer_41.layerDepth = 0
	this.Layer_41.layerIndex = 24
	this.Layer_41.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_41).wait(119));

	// Layer_40_obj_
	this.Layer_40 = new lib.Symbol_18_Layer_40();
	this.Layer_40.name = "Layer_40";
	this.Layer_40.parent = this;
	this.Layer_40.setTransform(-109.6,27.6,1,1,0,0,0,-109.6,27.6);
	this.Layer_40.depth = 0;
	this.Layer_40.isAttachedToCamera = 0
	this.Layer_40.isAttachedToMask = 0
	this.Layer_40.layerDepth = 0
	this.Layer_40.layerIndex = 25
	this.Layer_40.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_40).wait(119));

	// Layer_39_obj_
	this.Layer_39 = new lib.Symbol_18_Layer_39();
	this.Layer_39.name = "Layer_39";
	this.Layer_39.parent = this;
	this.Layer_39.setTransform(93,42.4,1,1,0,0,0,93,42.4);
	this.Layer_39.depth = 0;
	this.Layer_39.isAttachedToCamera = 0
	this.Layer_39.isAttachedToMask = 0
	this.Layer_39.layerDepth = 0
	this.Layer_39.layerIndex = 26
	this.Layer_39.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_39).wait(119));

	// Layer_3_obj_
	this.Layer_3 = new lib.Symbol_18_Layer_3();
	this.Layer_3.name = "Layer_3";
	this.Layer_3.parent = this;
	this.Layer_3.setTransform(124.5,6,1,1,0,0,0,124.5,6);
	this.Layer_3.depth = 0;
	this.Layer_3.isAttachedToCamera = 0
	this.Layer_3.isAttachedToMask = 0
	this.Layer_3.layerDepth = 0
	this.Layer_3.layerIndex = 27
	this.Layer_3.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_3).wait(119));

	// Layer_23_obj_
	this.Layer_23 = new lib.Symbol_18_Layer_23();
	this.Layer_23.name = "Layer_23";
	this.Layer_23.parent = this;
	this.Layer_23.setTransform(236.8,59.3,1,1,0,0,0,236.8,59.3);
	this.Layer_23.depth = 0;
	this.Layer_23.isAttachedToCamera = 0
	this.Layer_23.isAttachedToMask = 0
	this.Layer_23.layerDepth = 0
	this.Layer_23.layerIndex = 28
	this.Layer_23.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_23).wait(119));

	// Layer_22_obj_
	this.Layer_22 = new lib.Symbol_18_Layer_22();
	this.Layer_22.name = "Layer_22";
	this.Layer_22.parent = this;
	this.Layer_22.setTransform(97.5,-14.8,1,1,0,0,0,97.5,-14.8);
	this.Layer_22.depth = 0;
	this.Layer_22.isAttachedToCamera = 0
	this.Layer_22.isAttachedToMask = 0
	this.Layer_22.layerDepth = 0
	this.Layer_22.layerIndex = 29
	this.Layer_22.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_22).wait(119));

	// Layer_21_obj_
	this.Layer_21 = new lib.Symbol_18_Layer_21();
	this.Layer_21.name = "Layer_21";
	this.Layer_21.parent = this;
	this.Layer_21.setTransform(153.5,-6.9,1,1,0,0,0,153.5,-6.9);
	this.Layer_21.depth = 0;
	this.Layer_21.isAttachedToCamera = 0
	this.Layer_21.isAttachedToMask = 0
	this.Layer_21.layerDepth = 0
	this.Layer_21.layerIndex = 30
	this.Layer_21.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_21).wait(119));

	// Layer_20_obj_
	this.Layer_20 = new lib.Symbol_18_Layer_20();
	this.Layer_20.name = "Layer_20";
	this.Layer_20.parent = this;
	this.Layer_20.setTransform(220.8,48.6,1,1,0,0,0,220.8,48.6);
	this.Layer_20.depth = 0;
	this.Layer_20.isAttachedToCamera = 0
	this.Layer_20.isAttachedToMask = 0
	this.Layer_20.layerDepth = 0
	this.Layer_20.layerIndex = 31
	this.Layer_20.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_20).wait(119));

	// Layer_18_obj_
	this.Layer_18 = new lib.Symbol_18_Layer_18();
	this.Layer_18.name = "Layer_18";
	this.Layer_18.parent = this;
	this.Layer_18.setTransform(184.6,3.6,1,1,0,0,0,184.6,3.6);
	this.Layer_18.depth = 0;
	this.Layer_18.isAttachedToCamera = 0
	this.Layer_18.isAttachedToMask = 0
	this.Layer_18.layerDepth = 0
	this.Layer_18.layerIndex = 32
	this.Layer_18.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_18).wait(119));

	// Layer_17_obj_
	this.Layer_17 = new lib.Symbol_18_Layer_17();
	this.Layer_17.name = "Layer_17";
	this.Layer_17.parent = this;
	this.Layer_17.setTransform(195.3,54.8,1,1,0,0,0,195.3,54.8);
	this.Layer_17.depth = 0;
	this.Layer_17.isAttachedToCamera = 0
	this.Layer_17.isAttachedToMask = 0
	this.Layer_17.layerDepth = 0
	this.Layer_17.layerIndex = 33
	this.Layer_17.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_17).wait(119));

	// Layer_16_obj_
	this.Layer_16 = new lib.Symbol_18_Layer_16();
	this.Layer_16.name = "Layer_16";
	this.Layer_16.parent = this;
	this.Layer_16.setTransform(185.7,33.6,1,1,0,0,0,185.7,33.6);
	this.Layer_16.depth = 0;
	this.Layer_16.isAttachedToCamera = 0
	this.Layer_16.isAttachedToMask = 0
	this.Layer_16.layerDepth = 0
	this.Layer_16.layerIndex = 34
	this.Layer_16.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_16).wait(119));

	// Layer_15_obj_
	this.Layer_15 = new lib.Symbol_18_Layer_15();
	this.Layer_15.name = "Layer_15";
	this.Layer_15.parent = this;
	this.Layer_15.setTransform(-184.6,23.7,1,1,0,0,0,-184.6,23.7);
	this.Layer_15.depth = 0;
	this.Layer_15.isAttachedToCamera = 0
	this.Layer_15.isAttachedToMask = 0
	this.Layer_15.layerDepth = 0
	this.Layer_15.layerIndex = 35
	this.Layer_15.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_15).wait(119));

	// Layer_14_obj_
	this.Layer_14 = new lib.Symbol_18_Layer_14();
	this.Layer_14.name = "Layer_14";
	this.Layer_14.parent = this;
	this.Layer_14.setTransform(-207.2,39,1,1,0,0,0,-207.2,39);
	this.Layer_14.depth = 0;
	this.Layer_14.isAttachedToCamera = 0
	this.Layer_14.isAttachedToMask = 0
	this.Layer_14.layerDepth = 0
	this.Layer_14.layerIndex = 36
	this.Layer_14.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_14).wait(119));

	// Layer_13_obj_
	this.Layer_13 = new lib.Symbol_18_Layer_13();
	this.Layer_13.name = "Layer_13";
	this.Layer_13.parent = this;
	this.Layer_13.setTransform(-167.6,39.7,1,1,0,0,0,-167.6,39.7);
	this.Layer_13.depth = 0;
	this.Layer_13.isAttachedToCamera = 0
	this.Layer_13.isAttachedToMask = 0
	this.Layer_13.layerDepth = 0
	this.Layer_13.layerIndex = 37
	this.Layer_13.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_13).wait(119));

	// Layer_12_obj_
	this.Layer_12 = new lib.Symbol_18_Layer_12();
	this.Layer_12.name = "Layer_12";
	this.Layer_12.parent = this;
	this.Layer_12.setTransform(-121.9,58.7,1,1,0,0,0,-121.9,58.7);
	this.Layer_12.depth = 0;
	this.Layer_12.isAttachedToCamera = 0
	this.Layer_12.isAttachedToMask = 0
	this.Layer_12.layerDepth = 0
	this.Layer_12.layerIndex = 38
	this.Layer_12.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_12).wait(119));

	// Layer_11_obj_
	this.Layer_11 = new lib.Symbol_18_Layer_11();
	this.Layer_11.name = "Layer_11";
	this.Layer_11.parent = this;
	this.Layer_11.setTransform(-64.3,13.5,1,1,0,0,0,-64.3,13.5);
	this.Layer_11.depth = 0;
	this.Layer_11.isAttachedToCamera = 0
	this.Layer_11.isAttachedToMask = 0
	this.Layer_11.layerDepth = 0
	this.Layer_11.layerIndex = 39
	this.Layer_11.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_11).wait(119));

	// Layer_10_obj_
	this.Layer_10 = new lib.Symbol_18_Layer_10();
	this.Layer_10.name = "Layer_10";
	this.Layer_10.parent = this;
	this.Layer_10.setTransform(-88.8,2.4,1,1,0,0,0,-88.8,2.4);
	this.Layer_10.depth = 0;
	this.Layer_10.isAttachedToCamera = 0
	this.Layer_10.isAttachedToMask = 0
	this.Layer_10.layerDepth = 0
	this.Layer_10.layerIndex = 40
	this.Layer_10.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_10).wait(119));

	// Layer_1_obj_
	this.Layer_1 = new lib.Symbol_18_Layer_1();
	this.Layer_1.name = "Layer_1";
	this.Layer_1.parent = this;
	this.Layer_1.setTransform(0.1,-0.8,1,1,0,0,0,0.1,-0.8);
	this.Layer_1.depth = 0;
	this.Layer_1.isAttachedToCamera = 0
	this.Layer_1.isAttachedToMask = 0
	this.Layer_1.layerDepth = 0
	this.Layer_1.layerIndex = 41
	this.Layer_1.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_1).wait(119));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-461,-55,796,218);


(lib.Symbol15 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_4_obj_
	this.Layer_4 = new lib.Symbol_15_Layer_4();
	this.Layer_4.name = "Layer_4";
	this.Layer_4.parent = this;
	this.Layer_4.setTransform(15.4,-21.6,1,1,0,0,0,15.4,-21.6);
	this.Layer_4.depth = 0;
	this.Layer_4.isAttachedToCamera = 0
	this.Layer_4.isAttachedToMask = 0
	this.Layer_4.layerDepth = 0
	this.Layer_4.layerIndex = 0
	this.Layer_4.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_4).wait(1));

	// Layer_1_obj_
	this.Layer_1 = new lib.Symbol_15_Layer_1();
	this.Layer_1.name = "Layer_1";
	this.Layer_1.parent = this;
	this.Layer_1.setTransform(0.1,0.5,1,1,0,0,0,0.1,0.5);
	this.Layer_1.depth = 0;
	this.Layer_1.isAttachedToCamera = 0
	this.Layer_1.isAttachedToMask = 0
	this.Layer_1.layerDepth = 0
	this.Layer_1.layerIndex = 1
	this.Layer_1.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_1).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol15, new cjs.Rectangle(-49.2,-35.3,98.5,70.5), null);


(lib.Symbol3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_74 = function() {
		this.___loopingOver___ = true;
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(74).call(this.frame_74).wait(1));

	// Layer_5_obj_
	this.Layer_5 = new lib.Symbol_3_Layer_5();
	this.Layer_5.name = "Layer_5";
	this.Layer_5.parent = this;
	this.Layer_5.setTransform(6.1,25.6,1,1,0,0,0,6.1,25.6);
	this.Layer_5.depth = 0;
	this.Layer_5.isAttachedToCamera = 0
	this.Layer_5.isAttachedToMask = 0
	this.Layer_5.layerDepth = 0
	this.Layer_5.layerIndex = 0
	this.Layer_5.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_5).wait(75));

	// Layer_1_obj_
	this.Layer_1 = new lib.Symbol_3_Layer_1();
	this.Layer_1.name = "Layer_1";
	this.Layer_1.parent = this;
	this.Layer_1.setTransform(27.9,-18.6,1,1,0,0,0,27.9,-18.6);
	this.Layer_1.depth = 0;
	this.Layer_1.isAttachedToCamera = 0
	this.Layer_1.isAttachedToMask = 0
	this.Layer_1.layerDepth = 0
	this.Layer_1.layerIndex = 1
	this.Layer_1.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_1).wait(75));

	// Layer_4_obj_
	this.Layer_4 = new lib.Symbol_3_Layer_4();
	this.Layer_4.name = "Layer_4";
	this.Layer_4.parent = this;
	this.Layer_4.setTransform(-41.5,-18.2,1,1,0,0,0,-41.5,-18.2);
	this.Layer_4.depth = 0;
	this.Layer_4.isAttachedToCamera = 0
	this.Layer_4.isAttachedToMask = 0
	this.Layer_4.layerDepth = 0
	this.Layer_4.layerIndex = 2
	this.Layer_4.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_4).wait(75));

	// Layer_6 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	var mask_graphics_19 = new cjs.Graphics().p("AixibIFUhVIAPGwIkzAxg");

	this.timeline.addTween(cjs.Tween.get(mask).to({graphics:null,x:0,y:0}).wait(19).to({graphics:mask_graphics_19,x:-42.725,y:6.875}).wait(46).to({graphics:null,x:0,y:0}).wait(10));

	// Symbol_2_obj_
	this.Symbol_2 = new lib.Symbol_3_Symbol_2();
	this.Symbol_2.name = "Symbol_2";
	this.Symbol_2.parent = this;
	this.Symbol_2.depth = 0;
	this.Symbol_2.isAttachedToCamera = 0
	this.Symbol_2.isAttachedToMask = 0
	this.Symbol_2.layerDepth = 0
	this.Symbol_2.layerIndex = 3
	this.Symbol_2.maskLayerName = 0

	var maskedShapeInstanceList = [this.Symbol_2];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.Symbol_2).wait(75));

	// Symbol_1_obj_
	this.Symbol_1 = new lib.Symbol_3_Symbol_1();
	this.Symbol_1.name = "Symbol_1";
	this.Symbol_1.parent = this;
	this.Symbol_1.depth = 0;
	this.Symbol_1.isAttachedToCamera = 0
	this.Symbol_1.isAttachedToMask = 0
	this.Symbol_1.layerDepth = 0
	this.Symbol_1.layerIndex = 4
	this.Symbol_1.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Symbol_1).wait(75));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-64.4,-34.9,143.7,74.9);


(lib.Symbol_10_Layer_6 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_6
	this.instance = new lib.Symbol15();
	this.instance.parent = this;
	this.instance.setTransform(67,3);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({scaleX:1.1,scaleY:1.1},4,cjs.Ease.get(0.5)).to({scaleX:1,scaleY:1},10,cjs.Ease.get(1)).wait(45));

}).prototype = p = new cjs.MovieClip();


(lib.Scene_1_Symbol_3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Symbol_3
	this.instance = new lib.Symbol3();
	this.instance.parent = this;
	this.instance.setTransform(142.1,387.15,0.6521,0.6521,0,0,0,0.1,-3.1);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(75));

}).prototype = p = new cjs.MovieClip();


(lib.Scene_1_Layer_15 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_15
	this.instance = new lib.Symbol18();
	this.instance.parent = this;
	this.instance.setTransform(186.05,602.3,0.6521,0.6521,0,0,0,0,0.1);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(75));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol10 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_58 = function() {
		this.___loopingOver___ = true;
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(58).call(this.frame_58).wait(1));

	// Layer_6_obj_
	this.Layer_6 = new lib.Symbol_10_Layer_6();
	this.Layer_6.name = "Layer_6";
	this.Layer_6.parent = this;
	this.Layer_6.setTransform(67.2,0.1,1,1,0,0,0,67.2,0.1);
	this.Layer_6.depth = 0;
	this.Layer_6.isAttachedToCamera = 0
	this.Layer_6.isAttachedToMask = 0
	this.Layer_6.layerDepth = 0
	this.Layer_6.layerIndex = 0
	this.Layer_6.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_6).wait(59));

	// Layer_3_obj_
	this.Layer_3 = new lib.Symbol_10_Layer_3();
	this.Layer_3.name = "Layer_3";
	this.Layer_3.parent = this;
	this.Layer_3.depth = 0;
	this.Layer_3.isAttachedToCamera = 0
	this.Layer_3.isAttachedToMask = 0
	this.Layer_3.layerDepth = 0
	this.Layer_3.layerIndex = 1
	this.Layer_3.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_3).wait(13).to({x:-6.95,y:-5.3},0).wait(6).to({x:-6.9425,y:-5.2943},0).wait(13).to({x:-6.95,y:-5.3},0).to({x:0,y:0},1).wait(13).to({x:-6.95,y:-5.3},0).wait(1).to({x:-6.9425,y:-5.2943},0).wait(3).to({x:-6.95,y:-5.3},0).to({x:0,y:0},1).wait(8));

	// Layer_2_obj_
	this.Layer_2 = new lib.Symbol_10_Layer_2();
	this.Layer_2.name = "Layer_2";
	this.Layer_2.parent = this;
	this.Layer_2.depth = 0;
	this.Layer_2.isAttachedToCamera = 0
	this.Layer_2.isAttachedToMask = 0
	this.Layer_2.layerDepth = 0
	this.Layer_2.layerIndex = 2
	this.Layer_2.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_2).wait(13).to({x:-6.95,y:-5.3},0).wait(6).to({x:-6.9425,y:-5.2943},0).wait(13).to({x:-6.95,y:-5.3},0).to({x:0,y:0},1).wait(13).to({x:-6.95,y:-5.3},0).wait(1).to({x:-6.9425,y:-5.2943},0).wait(3).to({x:-6.95,y:-5.3},0).to({x:0,y:0},1).wait(8));

	// Layer_5 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	var mask_graphics_4 = new cjs.Graphics().p("AitCtQhJhHAAhmQAAhkBJhJQBIhHBlAAQBmAABIBHQBIBJABBkQgBBmhIBHQhIBIhmAAQhlAAhIhIg");
	var mask_graphics_5 = new cjs.Graphics().p("AitCtQhIhIAAhlQAAhlBIhHQBIhIBlAAQBmAABIBIQBJBHgBBlQABBlhJBIQhIBIhmAAQhlAAhIhIg");
	var mask_graphics_6 = new cjs.Graphics().p("AitCtQhIhHAAhmQAAhlBIhHQBIhIBlAAQBmAABIBIQBJBHgBBlQABBmhJBHQhIBIhmAAQhlAAhIhIg");
	var mask_graphics_7 = new cjs.Graphics().p("AitCtQhIhIAAhlQAAhkBIhIQBIhIBlAAQBmAABJBIQBIBIAABkQAABlhIBIQhJBIhmAAQhlAAhIhIg");
	var mask_graphics_8 = new cjs.Graphics().p("AitCtQhIhIAAhlQAAhlBIhHQBIhIBlAAQBmAABJBIQBIBHAABlQAABlhIBIQhJBIhmAAQhlAAhIhIg");
	var mask_graphics_9 = new cjs.Graphics().p("AitCtQhJhIAAhlQAAhlBJhIQBIhHBlAAQBmAABJBHQBHBIABBlQgBBlhHBIQhJBIhmAAQhlAAhIhIg");
	var mask_graphics_10 = new cjs.Graphics().p("AitCtQhJhIAAhlQAAhkBJhIQBIhIBlAAQBmAABJBIQBHBIABBkQgBBlhHBIQhJBIhmAAQhlAAhIhIg");
	var mask_graphics_11 = new cjs.Graphics().p("AitCtQhJhIAAhlQAAhlBJhIQBIhHBlAAQBmAABJBHQBHBIABBlQgBBlhHBIQhJBIhmAAQhlAAhIhIg");
	var mask_graphics_12 = new cjs.Graphics().p("AitCtQhIhIgBhlQABhkBIhIQBIhIBlAAQBmAABIBIQBJBIgBBkQABBlhJBIQhIBIhmAAQhlAAhIhIg");
	var mask_graphics_13 = new cjs.Graphics().p("AitCtQhIhIgBhlQABhkBIhIQBIhIBlAAQBmAABIBIQBJBIgBBkQABBlhJBIQhIBIhmAAQhlAAhIhIg");
	var mask_graphics_14 = new cjs.Graphics().p("AitCtQhJhHAAhmQAAhkBJhJQBIhHBlAAQBmAABJBHQBHBJABBkQgBBmhHBHQhJBIhmAAQhlAAhIhIg");
	var mask_graphics_15 = new cjs.Graphics().p("AitCtQhIhIAAhlQAAhlBIhIQBIhHBlAAQBmAABIBHQBJBIgBBlQABBlhJBIQhIBIhmAAQhlAAhIhIg");
	var mask_graphics_16 = new cjs.Graphics().p("AitCtQhIhHgBhmQABhkBIhJQBIhHBlAAQBmAABIBHQBIBJAABkQAABmhIBHQhIBIhmAAQhlAAhIhIg");
	var mask_graphics_17 = new cjs.Graphics().p("AitCtQhJhHAAhmQAAhkBJhJQBIhHBlAAQBmAABJBHQBHBJABBkQgBBmhHBHQhJBIhmAAQhlAAhIhIg");
	var mask_graphics_18 = new cjs.Graphics().p("AitCtQhIhHAAhmQAAhkBIhJQBIhHBlAAQBmAABJBHQBIBJAABkQAABmhIBHQhJBIhmAAQhlAAhIhIg");
	var mask_graphics_19 = new cjs.Graphics().p("AitCtQhJhHAAhmQAAhlBJhHQBIhIBlAAQBmAABJBIQBHBHABBlQgBBmhHBHQhJBIhmAAQhlAAhIhIg");
	var mask_graphics_20 = new cjs.Graphics().p("AitCtQhIhIgBhlQABhlBIhHQBIhIBlAAQBmAABIBIQBIBHAABlQAABlhIBIQhIBIhmAAQhlAAhIhIg");
	var mask_graphics_21 = new cjs.Graphics().p("AitCtQhIhIgBhlQABhlBIhIQBIhHBlAAQBmAABIBHQBIBIAABlQAABlhIBIQhIBIhmAAQhlAAhIhIg");
	var mask_graphics_22 = new cjs.Graphics().p("AitCtQhIhIAAhlQAAhlBIhHQBIhIBlAAQBmAABJBIQBIBHAABlQAABlhIBIQhJBIhmAAQhlAAhIhIg");
	var mask_graphics_23 = new cjs.Graphics().p("AitCtQhIhHAAhmQAAhkBIhJQBIhHBlAAQBmAABIBHQBJBJgBBkQABBmhJBHQhIBIhmAAQhlAAhIhIg");
	var mask_graphics_24 = new cjs.Graphics().p("AitCtQhIhHAAhmQAAhkBIhJQBIhHBlAAQBmAABJBHQBIBJAABkQAABmhIBHQhJBIhmAAQhlAAhIhIg");
	var mask_graphics_25 = new cjs.Graphics().p("AitCtQhIhIAAhlQAAhkBIhIQBIhIBlAAQBmAABIBIQBJBIgBBkQABBlhJBIQhIBIhmAAQhlAAhIhIg");
	var mask_graphics_26 = new cjs.Graphics().p("AitCtQhIhHAAhmQAAhkBIhJQBIhHBlAAQBmAABIBHQBJBJgBBkQABBmhJBHQhIBIhmAAQhlAAhIhIg");
	var mask_graphics_27 = new cjs.Graphics().p("AitCtQhIhIAAhlQAAhlBIhHQBIhIBlAAQBmAABJBIQBIBHAABlQAABlhIBIQhJBIhmAAQhlAAhIhIg");
	var mask_graphics_28 = new cjs.Graphics().p("AitCtQhIhIgBhlQABhkBIhIQBIhIBlAAQBmAABIBIQBJBIgBBkQABBlhJBIQhIBIhmAAQhlAAhIhIg");
	var mask_graphics_29 = new cjs.Graphics().p("AitCtQhIhIAAhlQAAhlBIhIQBIhHBlAAQBmAABJBHQBIBIAABlQAABlhIBIQhJBIhmAAQhlAAhIhIg");
	var mask_graphics_30 = new cjs.Graphics().p("AitCtQhIhIAAhlQAAhkBIhIQBIhIBlAAQBmAABIBIQBJBIgBBkQABBlhJBIQhIBIhmAAQhlAAhIhIg");
	var mask_graphics_31 = new cjs.Graphics().p("AitCtQhIhHAAhmQAAhkBIhJQBIhHBlAAQBmAABIBHQBJBJgBBkQABBmhJBHQhIBIhmAAQhlAAhIhIg");
	var mask_graphics_32 = new cjs.Graphics().p("AitCtQhIhIgBhlQABhkBIhIQBIhIBlAAQBmAABIBIQBIBIAABkQAABlhIBIQhIBIhmAAQhlAAhIhIg");
	var mask_graphics_33 = new cjs.Graphics().p("AitCtQhIhIAAhlQAAhlBIhIQBIhHBlAAQBmAABIBHQBJBIgBBlQABBlhJBIQhIBIhmAAQhlAAhIhIg");
	var mask_graphics_34 = new cjs.Graphics().p("AitCtQhIhIAAhlQAAhkBIhIQBIhIBlAAQBmAABJBIQBIBIAABkQAABlhIBIQhJBIhmAAQhlAAhIhIg");
	var mask_graphics_35 = new cjs.Graphics().p("AitCtQhIhHgBhmQABhkBIhJQBIhHBlAAQBmAABIBHQBIBJAABkQAABmhIBHQhIBIhmAAQhlAAhIhIg");
	var mask_graphics_38 = new cjs.Graphics().p("AitCtQhIhIAAhlQAAhkBIhIQBIhIBlAAQBmAABJBIQBIBIAABkQAABlhIBIQhJBIhmAAQhlAAhIhIg");
	var mask_graphics_39 = new cjs.Graphics().p("AitCtQhIhHAAhmQAAhkBIhJQBIhHBlAAQBmAABIBHQBJBJgBBkQABBmhJBHQhIBIhmAAQhlAAhIhIg");
	var mask_graphics_40 = new cjs.Graphics().p("AitCtQhIhHgBhmQABhkBIhJQBIhHBlAAQBmAABIBHQBJBJgBBkQABBmhJBHQhIBIhmAAQhlAAhIhIg");
	var mask_graphics_41 = new cjs.Graphics().p("AitCtQhIhHgBhmQABhkBIhJQBIhHBlAAQBmAABIBHQBJBJgBBkQABBmhJBHQhIBIhmAAQhlAAhIhIg");
	var mask_graphics_42 = new cjs.Graphics().p("AitCtQhJhIAAhlQAAhlBJhIQBIhHBlAAQBmAABJBHQBHBIABBlQgBBlhHBIQhJBIhmAAQhlAAhIhIg");
	var mask_graphics_43 = new cjs.Graphics().p("AitCtQhIhIAAhlQAAhlBIhHQBIhIBlAAQBmAABIBIQBJBHgBBlQABBlhJBIQhIBIhmAAQhlAAhIhIg");
	var mask_graphics_44 = new cjs.Graphics().p("AitCtQhIhHAAhmQAAhlBIhHQBIhIBlAAQBmAABJBIQBIBHAABlQAABmhIBHQhJBIhmAAQhlAAhIhIg");
	var mask_graphics_45 = new cjs.Graphics().p("AitCtQhJhHAAhmQAAhkBJhJQBIhHBlAAQBmAABIBHQBIBJAABkQAABmhIBHQhIBIhmAAQhlAAhIhIg");
	var mask_graphics_46 = new cjs.Graphics().p("AitCtQhJhHAAhmQAAhlBJhHQBIhIBlAAQBmAABJBIQBHBHABBlQgBBmhHBHQhJBIhmAAQhlAAhIhIg");
	var mask_graphics_47 = new cjs.Graphics().p("AitCtQhJhHAAhmQAAhlBJhHQBIhIBlAAQBmAABJBIQBHBHABBlQgBBmhHBHQhJBIhmAAQhlAAhIhIg");
	var mask_graphics_48 = new cjs.Graphics().p("AitCtQhJhIAAhlQAAhlBJhIQBIhHBlAAQBmAABIBHQBIBIAABlQAABlhIBIQhIBIhmAAQhlAAhIhIg");
	var mask_graphics_49 = new cjs.Graphics().p("AitCtQhIhIAAhlQAAhlBIhIQBIhHBlAAQBmAABIBHQBJBIgBBlQABBlhJBIQhIBIhmAAQhlAAhIhIg");
	var mask_graphics_50 = new cjs.Graphics().p("AitCtQhJhHAAhmQAAhlBJhHQBIhIBlAAQBmAABJBIQBHBHABBlQgBBmhHBHQhJBIhmAAQhlAAhIhIg");
	var mask_graphics_51 = new cjs.Graphics().p("AitCtQhIhHgBhmQABhlBIhHQBIhIBlAAQBmAABIBIQBJBHgBBlQABBmhJBHQhIBIhmAAQhlAAhIhIg");
	var mask_graphics_52 = new cjs.Graphics().p("AitCtQhIhIAAhlQAAhkBIhIQBIhIBlAAQBmAABIBIQBJBIgBBkQABBlhJBIQhIBIhmAAQhlAAhIhIg");
	var mask_graphics_53 = new cjs.Graphics().p("AitCtQhIhHAAhmQAAhkBIhJQBIhHBlAAQBmAABJBHQBIBJAABkQAABmhIBHQhJBIhmAAQhlAAhIhIg");
	var mask_graphics_54 = new cjs.Graphics().p("AitCtQhJhIAAhlQAAhkBJhIQBIhIBlAAQBmAABJBIQBHBIABBkQgBBlhHBIQhJBIhmAAQhlAAhIhIg");
	var mask_graphics_55 = new cjs.Graphics().p("AitCtQhIhIgBhlQABhlBIhIQBIhHBlAAQBmAABIBHQBIBIAABlQAABlhIBIQhIBIhmAAQhlAAhIhIg");
	var mask_graphics_56 = new cjs.Graphics().p("AitCtQhIhHgBhmQABhkBIhJQBIhHBlAAQBmAABIBHQBJBJgBBkQABBmhJBHQhIBIhmAAQhlAAhIhIg");
	var mask_graphics_57 = new cjs.Graphics().p("AitCtQhIhIgBhlQABhkBIhIQBIhIBlAAQBmAABIBIQBIBIAABkQAABlhIBIQhIBIhmAAQhlAAhIhIg");
	var mask_graphics_58 = new cjs.Graphics().p("AitCtQhIhHgBhmQABhkBIhJQBIhHBlAAQBmAABIBHQBJBJgBBkQABBmhJBHQhIBIhmAAQhlAAhIhIg");

	this.timeline.addTween(cjs.Tween.get(mask).to({graphics:null,x:0,y:0}).wait(4).to({graphics:mask_graphics_4,x:38.35,y:-25.2}).wait(1).to({graphics:mask_graphics_5,x:38.2,y:-25.15}).wait(1).to({graphics:mask_graphics_6,x:37.85,y:-25.05}).wait(1).to({graphics:mask_graphics_7,x:37.15,y:-24.9}).wait(1).to({graphics:mask_graphics_8,x:36.25,y:-24.7}).wait(1).to({graphics:mask_graphics_9,x:35.1,y:-24.4}).wait(1).to({graphics:mask_graphics_10,x:33.65,y:-24}).wait(1).to({graphics:mask_graphics_11,x:31.95,y:-23.6}).wait(1).to({graphics:mask_graphics_12,x:30,y:-23.1}).wait(1).to({graphics:mask_graphics_13,x:27.75,y:-22.55}).wait(1).to({graphics:mask_graphics_14,x:25.3,y:-21.95}).wait(1).to({graphics:mask_graphics_15,x:22.55,y:-21.25}).wait(1).to({graphics:mask_graphics_16,x:19.55,y:-20.5}).wait(1).to({graphics:mask_graphics_17,x:16.3,y:-19.7}).wait(1).to({graphics:mask_graphics_18,x:12.75,y:-18.8}).wait(1).to({graphics:mask_graphics_19,x:11.15,y:-16.95}).wait(1).to({graphics:mask_graphics_20,x:9.65,y:-15.25}).wait(1).to({graphics:mask_graphics_21,x:8.3,y:-13.6}).wait(1).to({graphics:mask_graphics_22,x:7,y:-12.1}).wait(1).to({graphics:mask_graphics_23,x:5.8,y:-10.7}).wait(1).to({graphics:mask_graphics_24,x:4.65,y:-9.45}).wait(1).to({graphics:mask_graphics_25,x:3.65,y:-8.25}).wait(1).to({graphics:mask_graphics_26,x:2.75,y:-7.2}).wait(1).to({graphics:mask_graphics_27,x:1.95,y:-6.25}).wait(1).to({graphics:mask_graphics_28,x:1.2,y:-5.45}).wait(1).to({graphics:mask_graphics_29,x:0.6,y:-4.7}).wait(1).to({graphics:mask_graphics_30,x:0.05,y:-4.1}).wait(1).to({graphics:mask_graphics_31,x:-0.4,y:-3.6}).wait(1).to({graphics:mask_graphics_32,x:-0.7,y:-3.2}).wait(1).to({graphics:mask_graphics_33,x:-0.95,y:-2.9}).wait(1).to({graphics:mask_graphics_34,x:-1.1,y:-2.75}).wait(1).to({graphics:mask_graphics_35,x:-1.15,y:-2.7}).wait(3).to({graphics:mask_graphics_38,x:-5.25,y:-3.2}).wait(1).to({graphics:mask_graphics_39,x:-5.45,y:-3.05}).wait(1).to({graphics:mask_graphics_40,x:-6,y:-2.7}).wait(1).to({graphics:mask_graphics_41,x:-6.9,y:-2.05}).wait(1).to({graphics:mask_graphics_42,x:-8.2,y:-1.1}).wait(1).to({graphics:mask_graphics_43,x:-9.85,y:0.05}).wait(1).to({graphics:mask_graphics_44,x:-11.9,y:1.5}).wait(1).to({graphics:mask_graphics_45,x:-14.3,y:3.15}).wait(1).to({graphics:mask_graphics_46,x:-17.1,y:5.1}).wait(1).to({graphics:mask_graphics_47,x:-20.25,y:7.35}).wait(1).to({graphics:mask_graphics_48,x:-23.75,y:9.8}).wait(1).to({graphics:mask_graphics_49,x:-21.65,y:16.1}).wait(1).to({graphics:mask_graphics_50,x:-19.8,y:21.75}).wait(1).to({graphics:mask_graphics_51,x:-18.15,y:26.7}).wait(1).to({graphics:mask_graphics_52,x:-16.7,y:31}).wait(1).to({graphics:mask_graphics_53,x:-15.5,y:34.65}).wait(1).to({graphics:mask_graphics_54,x:-14.5,y:37.65}).wait(1).to({graphics:mask_graphics_55,x:-13.75,y:39.95}).wait(1).to({graphics:mask_graphics_56,x:-13.2,y:41.6}).wait(1).to({graphics:mask_graphics_57,x:-12.85,y:42.6}).wait(1).to({graphics:mask_graphics_58,x:-12.75,y:42.95}).wait(1));

	// Layer_1_obj_
	this.Layer_1 = new lib.Symbol_10_Layer_1();
	this.Layer_1.name = "Layer_1";
	this.Layer_1.parent = this;
	this.Layer_1.depth = 0;
	this.Layer_1.isAttachedToCamera = 0
	this.Layer_1.isAttachedToMask = 0
	this.Layer_1.layerDepth = 0
	this.Layer_1.layerIndex = 3
	this.Layer_1.maskLayerName = 0

	var maskedShapeInstanceList = [this.Layer_1];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.Layer_1).wait(59));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-22.1,-35.8,143.3,77.5);


(lib.Scene_1_Layer_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Symbol10();
	this.instance.parent = this;
	this.instance.setTransform(262.55,520.65,0.6521,0.6521,0,0,0,0,0.1);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(75));

}).prototype = p = new cjs.MovieClip();


// stage content:
(lib.March_01_vert = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	this.___GetDepth___ = function(obj) {
		var depth = obj.depth;
		var cameraObj = this.___camera___instance;
		if(cameraObj && cameraObj.depth && obj.isAttachedToCamera)
		{
			depth += depth + cameraObj.depth;
		}
		return depth;
		}
	this.___needSorting___ = function() {
		for (var i = 0; i < this.getNumChildren() - 1; i++)
		{
			var prevDepth = this.___GetDepth___(this.getChildAt(i));
			var nextDepth = this.___GetDepth___(this.getChildAt(i + 1));
			if (prevDepth < nextDepth)
				return true;
		}
		return false;
	}
	this.___sortFunction___ = function(obj1, obj2) {
		return (this.exportRoot.___GetDepth___(obj2) - this.exportRoot.___GetDepth___(obj1));
	}
	this.on('tick', function (event){
		var curTimeline = event.currentTarget;
		if (curTimeline.___needSorting___()){
			this.sortChildren(curTimeline.___sortFunction___);
		}
	});

	// timeline functions:
	this.frame_74 = function() {
		this.___loopingOver___ = true;
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(74).call(this.frame_74).wait(1));

	// Layer_1_obj_
	this.Layer_1 = new lib.Scene_1_Layer_1();
	this.Layer_1.name = "Layer_1";
	this.Layer_1.parent = this;
	this.Layer_1.setTransform(299.7,520.6,1,1,0,0,0,299.7,520.6);
	this.Layer_1.depth = 0;
	this.Layer_1.isAttachedToCamera = 0
	this.Layer_1.isAttachedToMask = 0
	this.Layer_1.layerDepth = 0
	this.Layer_1.layerIndex = 0
	this.Layer_1.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_1).wait(75));

	// Symbol_3_obj_
	this.Symbol_3 = new lib.Scene_1_Symbol_3();
	this.Symbol_3.name = "Symbol_3";
	this.Symbol_3.parent = this;
	this.Symbol_3.setTransform(138.2,391.4,1,1,0,0,0,138.2,391.4);
	this.Symbol_3.depth = 0;
	this.Symbol_3.isAttachedToCamera = 0
	this.Symbol_3.isAttachedToMask = 0
	this.Symbol_3.layerDepth = 0
	this.Symbol_3.layerIndex = 1
	this.Symbol_3.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Symbol_3).wait(75));

	// Symbol_5_obj_
	this.Symbol_5 = new lib.Scene_1_Symbol_5();
	this.Symbol_5.name = "Symbol_5";
	this.Symbol_5.parent = this;
	this.Symbol_5.setTransform(40.9,372.9,1,1,0,0,0,40.9,372.9);
	this.Symbol_5.depth = 0;
	this.Symbol_5.isAttachedToCamera = 0
	this.Symbol_5.isAttachedToMask = 0
	this.Symbol_5.layerDepth = 0
	this.Symbol_5.layerIndex = 2
	this.Symbol_5.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Symbol_5).wait(50).to({regX:39.4,regY:373.6,x:39.4,y:373.6},0).wait(19).to({regX:40.9,regY:372.9,x:40.9,y:372.9},0).wait(6));

	// Symbol_4_obj_
	this.Symbol_4 = new lib.Scene_1_Symbol_4();
	this.Symbol_4.name = "Symbol_4";
	this.Symbol_4.parent = this;
	this.Symbol_4.setTransform(158.9,493,1,1,0,0,0,158.9,493);
	this.Symbol_4.depth = 0;
	this.Symbol_4.isAttachedToCamera = 0
	this.Symbol_4.isAttachedToMask = 0
	this.Symbol_4.layerDepth = 0
	this.Symbol_4.layerIndex = 3
	this.Symbol_4.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Symbol_4).wait(50).to({regX:157.9,regY:494.7,x:157.9,y:494.7},0).wait(19).to({regX:158.9,regY:493,x:158.9,y:493},0).wait(6));

	// Symbol_7_obj_
	this.Symbol_7 = new lib.Scene_1_Symbol_7();
	this.Symbol_7.name = "Symbol_7";
	this.Symbol_7.parent = this;
	this.Symbol_7.setTransform(159.2,477.4,1,1,0,0,0,159.2,477.4);
	this.Symbol_7.depth = 0;
	this.Symbol_7.isAttachedToCamera = 0
	this.Symbol_7.isAttachedToMask = 0
	this.Symbol_7.layerDepth = 0
	this.Symbol_7.layerIndex = 4
	this.Symbol_7.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Symbol_7).wait(50).to({regX:154.7,regY:479.2,x:154.7,y:479.2},0).wait(19).to({regX:159.2,regY:477.4,x:159.2,y:477.4},0).wait(6));

	// Symbol_6_obj_
	this.Symbol_6 = new lib.Scene_1_Symbol_6();
	this.Symbol_6.name = "Symbol_6";
	this.Symbol_6.parent = this;
	this.Symbol_6.setTransform(49.6,377.6,1,1,0,0,0,49.6,377.6);
	this.Symbol_6.depth = 0;
	this.Symbol_6.isAttachedToCamera = 0
	this.Symbol_6.isAttachedToMask = 0
	this.Symbol_6.layerDepth = 0
	this.Symbol_6.layerIndex = 5
	this.Symbol_6.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Symbol_6).wait(50).to({regX:48.6,regY:378.3,x:48.6,y:378.3},0).wait(19).to({regX:49.6,regY:377.6,x:49.6,y:377.6},0).wait(6));

	// Symbol_8_obj_
	this.Symbol_8 = new lib.Scene_1_Symbol_8();
	this.Symbol_8.name = "Symbol_8";
	this.Symbol_8.parent = this;
	this.Symbol_8.setTransform(189.3,463.8,1,1,0,0,0,189.3,463.8);
	this.Symbol_8.depth = 0;
	this.Symbol_8.isAttachedToCamera = 0
	this.Symbol_8.isAttachedToMask = 0
	this.Symbol_8.layerDepth = 0
	this.Symbol_8.layerIndex = 6
	this.Symbol_8.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Symbol_8).wait(75));

	// pack_png (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("ApvDoQAWomg8n2IUlFLIAGRjQiiC7jUAAQl4ABoXpOg");
	mask.setTransform(173.15,497.8489);

	// Symbol_9_obj_
	this.Symbol_9 = new lib.Scene_1_Symbol_9();
	this.Symbol_9.name = "Symbol_9";
	this.Symbol_9.parent = this;
	this.Symbol_9.setTransform(154.9,502.5,1,1,0,0,0,154.9,502.5);
	this.Symbol_9.depth = 0;
	this.Symbol_9.isAttachedToCamera = 0
	this.Symbol_9.isAttachedToMask = 0
	this.Symbol_9.layerDepth = 0
	this.Symbol_9.layerIndex = 7
	this.Symbol_9.maskLayerName = 0

	var maskedShapeInstanceList = [this.Symbol_9];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.Symbol_9).wait(50).to({regX:155.4,regY:496.2,x:155.4,y:496.2},0).wait(19).to({regX:154.9,regY:502.5,x:154.9,y:502.5},0).wait(6));

	// bg_png_obj_
	this.bg_png = new lib.Scene_1_bg_png();
	this.bg_png.name = "bg_png";
	this.bg_png.parent = this;
	this.bg_png.setTransform(178,443.6,1,1,0,0,0,178,443.6);
	this.bg_png.depth = 0;
	this.bg_png.isAttachedToCamera = 0
	this.bg_png.isAttachedToMask = 0
	this.bg_png.layerDepth = 0
	this.bg_png.layerIndex = 8
	this.bg_png.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.bg_png).wait(75));

	// podloj_png_obj_
	this.podloj_png = new lib.Scene_1_podloj_png();
	this.podloj_png.name = "podloj_png";
	this.podloj_png.parent = this;
	this.podloj_png.setTransform(212.6,592.1,1,1,0,0,0,212.6,592.1);
	this.podloj_png.depth = 0;
	this.podloj_png.isAttachedToCamera = 0
	this.podloj_png.isAttachedToMask = 0
	this.podloj_png.layerDepth = 0
	this.podloj_png.layerIndex = 9
	this.podloj_png.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.podloj_png).wait(50).to({regX:210,regY:592.9,x:210,y:592.9},0).wait(19).to({regX:212.6,regY:592.1,x:212.6,y:592.1},0).wait(6));

	// Layer_14_obj_
	this.Layer_14 = new lib.Scene_1_Layer_14();
	this.Layer_14.name = "Layer_14";
	this.Layer_14.parent = this;
	this.Layer_14.setTransform(85.9,407.7,1,1,0,0,0,85.9,407.7);
	this.Layer_14.depth = 0;
	this.Layer_14.isAttachedToCamera = 0
	this.Layer_14.isAttachedToMask = 0
	this.Layer_14.layerDepth = 0
	this.Layer_14.layerIndex = 10
	this.Layer_14.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_14).wait(75));

	// Layer_15_obj_
	this.Layer_15 = new lib.Scene_1_Layer_15();
	this.Layer_15.name = "Layer_15";
	this.Layer_15.parent = this;
	this.Layer_15.setTransform(118.9,638.5,1,1,0,0,0,118.9,638.5);
	this.Layer_15.depth = 0;
	this.Layer_15.isAttachedToCamera = 0
	this.Layer_15.isAttachedToMask = 0
	this.Layer_15.layerDepth = 0
	this.Layer_15.layerIndex = 11
	this.Layer_15.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_15).wait(75));

	// Layer_17_obj_
	this.Layer_17 = new lib.Scene_1_Layer_17();
	this.Layer_17.name = "Layer_17";
	this.Layer_17.parent = this;
	this.Layer_17.setTransform(90.7,405,1,1,0,0,0,90.7,405);
	this.Layer_17.depth = 0;
	this.Layer_17.isAttachedToCamera = 0
	this.Layer_17.isAttachedToMask = 0
	this.Layer_17.layerDepth = 0
	this.Layer_17.layerIndex = 12
	this.Layer_17.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_17).wait(75));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-121.5,373.1,519.4,469.9);
// library properties:
lib.properties = {
	id: '8AC9ECFD30851A4D8FBF2A1DE96DB1D9',
	width: 375,
	height: 812,
	fps: 30,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: [
		{src:"assets/images/bg_part39-2.jpg", id:"bg_part"},
		{src:"assets/images/blue39-2.png", id:"blue"},
		{src:"assets/images/D39-2.png", id:"D"},
		{src:"assets/images/orange39-2.png", id:"orange"},
		{src:"assets/images/pack39-2.png", id:"pack"},
		{src:"assets/images/pan39-2.png", id:"pan"},
		{src:"assets/images/pan_shadow39-2.png", id:"pan_shadow"},
		{src:"assets/images/rays139-2.png", id:"rays1"},
		{src:"assets/images/red39-2.png", id:"red"},
		{src:"assets/images/rumyanec39-2.png", id:"rumyanec"},
		{src:"assets/images/shadow139-2.png", id:"shadow1"},
		{src:"assets/images/white39-2.png", id:"white"}
	],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['8AC9ECFD30851A4D8FBF2A1DE96DB1D9'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}


// Layer depth API : 

AdobeAn.Layer = new function() {
	this.getLayerZDepth = function(timeline, layerName)
	{
		if(layerName === "Camera")
		layerName = "___camera___instance";
		var script = "if(timeline." + layerName + ") timeline." + layerName + ".depth; else 0;";
		return eval(script);
	}
	this.setLayerZDepth = function(timeline, layerName, zDepth)
	{
		const MAX_zDepth = 10000;
		const MIN_zDepth = -5000;
		if(zDepth > MAX_zDepth)
			zDepth = MAX_zDepth;
		else if(zDepth < MIN_zDepth)
			zDepth = MIN_zDepth;
		if(layerName === "Camera")
		layerName = "___camera___instance";
		var script = "if(timeline." + layerName + ") timeline." + layerName + ".depth = " + zDepth + ";";
		eval(script);
	}
	this.removeLayer = function(timeline, layerName)
	{
		if(layerName === "Camera")
		layerName = "___camera___instance";
		var script = "if(timeline." + layerName + ") timeline.removeChild(timeline." + layerName + ");";
		eval(script);
	}
	this.addNewLayer = function(timeline, layerName, zDepth)
	{
		if(layerName === "Camera")
		layerName = "___camera___instance";
		zDepth = typeof zDepth !== 'undefined' ? zDepth : 0;
		var layer = new createjs.MovieClip();
		layer.name = layerName;
		layer.depth = zDepth;
		layer.layerIndex = 0;
		timeline.addChild(layer);
	}
}


})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;

$(function () {
    var canvas, stage, exportRoot, anim_container, dom_overlay_container, fnStartAnimation;
    function init() {
        canvas = document.getElementById("canvas39-2");
        anim_container = document.getElementById("animation_container39-2");
        dom_overlay_container = document.getElementById("dom_overlay_container39-2");
        var comp=AdobeAn.getComposition("8AC9ECFD30851A4D8FBF2A1DE96DB1D9");
        var lib=comp.getLibrary();
        createjs.MotionGuidePlugin.install();
        var loader = new createjs.LoadQueue(false);
        loader.addEventListener("fileload", function(evt){handleFileLoad(evt,comp)});
        loader.addEventListener("complete", function(evt){handleComplete(evt,comp)});
        var lib=comp.getLibrary();
        loader.loadManifest(lib.properties.manifest);
    }
    function handleFileLoad(evt, comp) {
        var images=comp.getImages();
        if (evt && (evt.item.type == "image")) { images[evt.item.id] = evt.result; }
    }
    function handleComplete(evt,comp) {
        //This function is always called, irrespective of the content. You can use the variable "stage" after it is created in token create_stage.
        var lib=comp.getLibrary();
        var ss=comp.getSpriteSheet();
        var queue = evt.target;
        var ssMetadata = lib.ssMetadata;
        for(i=0; i<ssMetadata.length; i++) {
            ss[ssMetadata[i].name] = new createjs.SpriteSheet( {"images": [queue.getResult(ssMetadata[i].name)], "frames": ssMetadata[i].frames} )
        }
        var preloaderDiv = document.getElementById("_preload_div_39");
        preloaderDiv.style.display = 'none';
        canvas.style.display = 'block';
        exportRoot = new lib.March_01_vert();
        stage = new lib.Stage(canvas);
        //Registers the "tick" event listener.
        fnStartAnimation = function() {
            stage.addChild(exportRoot);
            createjs.Ticker.setFPS(lib.properties.fps);
            createjs.Ticker.addEventListener("tick", stage)
            stage.addEventListener("tick", handleTick)
            function getProjectionMatrix(container, totalDepth) {
                var focalLength = 528.25;
                var projectionCenter = { x : lib.properties.width/2, y : lib.properties.height/2 };
                var scale = (totalDepth + focalLength)/focalLength;
                var scaleMat = new createjs.Matrix2D;
                scaleMat.a = 1/scale;
                scaleMat.d = 1/scale;
                var projMat = new createjs.Matrix2D;
                projMat.tx = -projectionCenter.x;
                projMat.ty = -projectionCenter.y;
                projMat = projMat.prependMatrix(scaleMat);
                projMat.tx += projectionCenter.x;
                projMat.ty += projectionCenter.y;
                return projMat;
            }
            function handleTick(event) {
                var cameraInstance = exportRoot.___camera___instance;
                if(cameraInstance !== undefined && cameraInstance.pinToObject !== undefined)
                {
                    cameraInstance.x = cameraInstance.pinToObject.x + cameraInstance.pinToObject.pinOffsetX;
                    cameraInstance.y = cameraInstance.pinToObject.y + cameraInstance.pinToObject.pinOffsetY;
                    if(cameraInstance.pinToObject.parent !== undefined && cameraInstance.pinToObject.parent.depth !== undefined)
                        cameraInstance.depth = cameraInstance.pinToObject.parent.depth + cameraInstance.pinToObject.pinOffsetZ;
                }
                applyLayerZDepth(exportRoot);
            }
            function applyLayerZDepth(parent)
            {
                var cameraInstance = parent.___camera___instance;
                var focalLength = 528.25;
                var projectionCenter = { 'x' : 0, 'y' : 0};
                if(parent === exportRoot)
                {
                    var stageCenter = { 'x' : lib.properties.width/2, 'y' : lib.properties.height/2 };
                    projectionCenter.x = stageCenter.x;
                    projectionCenter.y = stageCenter.y;
                }
                for(child in parent.children)
                {
                    var layerObj = parent.children[child];
                    if(layerObj == cameraInstance)
                        continue;
                    applyLayerZDepth(layerObj, cameraInstance);
                    if(layerObj.layerDepth === undefined)
                        continue;
                    if(layerObj.currentFrame != layerObj.parent.currentFrame)
                    {
                        layerObj.gotoAndPlay(layerObj.parent.currentFrame);
                    }
                    var matToApply = new createjs.Matrix2D;
                    var cameraMat = new createjs.Matrix2D;
                    var totalDepth = layerObj.layerDepth ? layerObj.layerDepth : 0;
                    var cameraDepth = 0;
                    if(cameraInstance && !layerObj.isAttachedToCamera)
                    {
                        var mat = cameraInstance.getMatrix();
                        mat.tx -= projectionCenter.x;
                        mat.ty -= projectionCenter.y;
                        cameraMat = mat.invert();
                        cameraMat.prependTransform(projectionCenter.x, projectionCenter.y, 1, 1, 0, 0, 0, 0, 0);
                        cameraMat.appendTransform(-projectionCenter.x, -projectionCenter.y, 1, 1, 0, 0, 0, 0, 0);
                        if(cameraInstance.depth)
                            cameraDepth = cameraInstance.depth;
                    }
                    if(layerObj.depth)
                    {
                        totalDepth = layerObj.depth;
                    }
                    //Offset by camera depth
                    totalDepth -= cameraDepth;
                    if(totalDepth < -focalLength)
                    {
                        matToApply.a = 0;
                        matToApply.d = 0;
                    }
                    else
                    {
                        if(layerObj.layerDepth)
                        {
                            var sizeLockedMat = getProjectionMatrix(parent, layerObj.layerDepth);
                            if(sizeLockedMat)
                            {
                                sizeLockedMat.invert();
                                matToApply.prependMatrix(sizeLockedMat);
                            }
                        }
                        matToApply.prependMatrix(cameraMat);
                        var projMat = getProjectionMatrix(parent, totalDepth);
                        if(projMat)
                        {
                            matToApply.prependMatrix(projMat);
                        }
                    }
                    layerObj.transformMatrix = matToApply;
                }
            }
        }
        //Code to support hidpi screens and responsive scaling.
        function makeResponsive(isResp, respDim, isScale, scaleType) {
            var lastW, lastH, lastS=1;
            window.addEventListener('resize', resizeCanvas);
            resizeCanvas();
            function resizeCanvas() {
                var w = lib.properties.width, h = lib.properties.height;
                var iw = window.innerWidth, ih=window.innerHeight;
                var pRatio = window.devicePixelRatio || 1, xRatio=iw/w, yRatio=ih/h, sRatio=1;
                if(isResp) {
                    if((respDim=='width'&&lastW==iw) || (respDim=='height'&&lastH==ih)) {
                        sRatio = lastS;
                    }
                    else if(!isScale) {
                        if(iw<w || ih<h)
                            sRatio = Math.min(xRatio, yRatio);
                    }
                    else if(scaleType==1) {
                        sRatio = Math.min(xRatio, yRatio);
                    }
                    else if(scaleType==2) {
                        sRatio = Math.max(xRatio, yRatio);
                    }
                }
                canvas.width = w*pRatio*sRatio;
                canvas.height = h*pRatio*sRatio;
                canvas.style.width = anim_container.style.width = dom_overlay_container.style.width = preloaderDiv.style.width = w*sRatio+'px';
                canvas.style.height = anim_container.style.height = dom_overlay_container.style.height = preloaderDiv.style.height = h*sRatio+'px';
                stage.scaleX = pRatio*sRatio;
                stage.scaleY = pRatio*sRatio;
                lastW = iw; lastH = ih; lastS = sRatio;
                stage.tickOnUpdate = false;
                stage.update();
                stage.tickOnUpdate = true;
            }
        }
        makeResponsive(true,'both',true,2);
        AdobeAn.compositionLoaded(lib.properties.id);
        fnStartAnimation();
    }
    init();
})