(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [];


// symbols:



(lib.bg_vert = function() {
	this.initialize(img.bg_vert);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,861,1318);


(lib.eggs = function() {
	this.initialize(img.eggs);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,158,145);


(lib.pack = function() {
	this.initialize(img.pack);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,382,527);// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.Symbol7 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.bg_vert();
	this.instance.parent = this;
	this.instance.setTransform(-281.05,-430.25,0.6529,0.6529);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol7, new cjs.Rectangle(-281,-430.2,562.1,860.5), null);


(lib.Symbol_4_pack_png = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// pack_png
	this.instance = new lib.pack();
	this.instance.parent = this;
	this.instance.setTransform(-208.5,-263.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(145));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_4_Layer_19 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_19
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(8.5,1,1).p("AgygGQAwA7A1hI");
	this.shape.setTransform(133.4,139.9459);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(8.5,1,1).p("AgygDQAxAuA0g7");
	this.shape_1.setTransform(133.4,139.1744);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(8.5,1,1).p("AgygBQAxAlA0gx");
	this.shape_2.setTransform(133.4,138.5286);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(8.5,1,1).p("AgyAAQAyAdAzgo");
	this.shape_3.setTransform(133.4,138.0715);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(8.5,1,1).p("AgyABQAyAYAzgj");
	this.shape_4.setTransform(133.4,137.701);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(8.5,1,1).p("AgyACQAyAUAzgf");
	this.shape_5.setTransform(133.4,137.506);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(8.5,1,1).p("AgyADQAyATAzgf");
	this.shape_6.setTransform(133.4,137.4361);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(8.5,1,1).p("AgyAAQAyAgAzgs");
	this.shape_7.setTransform(133.4,138.1943);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(8.5,1,1).p("AgygCQAxApA0g1");
	this.shape_8.setTransform(133.4,138.839);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(8.5,1,1).p("AgygEQAxAxA0g9");
	this.shape_9.setTransform(133.4,139.3361);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(8.5,1,1).p("AgygFQAwA2A1hD");
	this.shape_10.setTransform(133.4,139.6721);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(8.5,1,1).p("AgygGQAwA6A1hH");
	this.shape_11.setTransform(133.4,139.8711);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},78).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_6}]},39).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape}]},1).wait(16));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_4_Layer_18 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_18
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(8.5,1,1).p("AgygGQAwA7A1hI");
	this.shape.setTransform(162.3,138.9959);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(8.5,1,1).p("AgygEQAyAwAzg8");
	this.shape_1.setTransform(162.3,138.2614);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(8.5,1,1).p("AgygBQA1AnAwg0");
	this.shape_2.setTransform(162.3,137.6399);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(8.5,1,1).p("AgyAAQA2AgAvgs");
	this.shape_3.setTransform(162.3,137.2065);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(8.5,1,1).p("AgyABQA4AcAtgo");
	this.shape_4.setTransform(162.3,136.8473);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(8.5,1,1).p("AgyABQA4AZAtgk");
	this.shape_5.setTransform(162.3,136.663);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(8.5,1,1).p("AgyACQA5AXAsgj");
	this.shape_6.setTransform(162.3,136.595);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(8.5,1,1).p("AgyAAQA2AiAvgv");
	this.shape_7.setTransform(162.3,137.3174);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(8.5,1,1).p("AgygCQAzArAyg4");
	this.shape_8.setTransform(162.3,137.9382);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(8.5,1,1).p("AgygEQAyAyAzg/");
	this.shape_9.setTransform(162.3,138.4108);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(8.5,1,1).p("AgygFQAxA3A0hE");
	this.shape_10.setTransform(162.3,138.7344);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(8.5,1,1).p("AgygGQAxA6A0hH");
	this.shape_11.setTransform(162.3,138.9211);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},78).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_6}]},39).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape}]},1).wait(16));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_4_Layer_17 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_17
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(8.5,1,1).p("AiLgIQBIA5BSgHQBWgGAnhO");
	this.shape.setTransform(149.85,158.5828);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(8.5,1,1).p("AiUgJQBSA7BSgIQBVgIAwhM");
	this.shape_1.setTransform(149.975,158.8967);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(8.5,1,1).p("AibgLQBaA+BSgJQBUgJA3hL");
	this.shape_2.setTransform(150.05,159.1813);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(8.5,1,1).p("AiggLQBgA/BSgKQBUgKA7hK");
	this.shape_3.setTransform(150.125,159.3921);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(8.5,1,1).p("AikgMQBkBBBTgLQBTgLA/hK");
	this.shape_4.setTransform(150.175,159.5536);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(8.5,1,1).p("AingMQBoBBBTgLQBTgLBBhK");
	this.shape_5.setTransform(150.175,159.6098);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(8.5,1,1).p("AingNQBoBCBSgLQBTgLBDhK");
	this.shape_6.setTransform(150.2,159.6579);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(8.5,1,1).p("AifgLQBfA/BSgKQBUgKA6hK");
	this.shape_7.setTransform(150.075,159.3421);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(8.5,1,1).p("AiYgKQBXA9BSgJQBVgJAzhL");
	this.shape_8.setTransform(150,159.057);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(8.5,1,1).p("AiSgJQBQA7BSgIQBVgHAuhN");
	this.shape_9.setTransform(149.95,158.8467);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(8.5,1,1).p("AiOgJQBMA6BRgHQBWgHAqhN");
	this.shape_10.setTransform(149.875,158.6871);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(8.5,1,1).p("AiMgJQBJA6BRgHQBWgHAphN");
	this.shape_11.setTransform(149.875,158.6325);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},78).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_6}]},39).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape}]},1).wait(16));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_4_Layer_15 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_15
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(8.5,1,1).p("AgWgMIAtAZ");
	this.shape.setTransform(222.425,137.575);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(90).to({y:138.425},0).wait(1).to({y:138.925},0).wait(1).to({y:139.075},0).wait(1).to({y:138.425},0).wait(1).to({y:137.975},0).wait(1).to({y:137.675},0).wait(1).to({y:137.575},0).wait(3).to({y:138.425},0).wait(1).to({y:138.925},0).wait(1).to({y:139.075},0).wait(1).to({y:138.425},0).wait(1).to({y:137.975},0).wait(1).to({y:137.675},0).wait(1).to({y:137.575},0).wait(3).to({y:138.425},0).wait(1).to({y:138.925},0).wait(1).to({y:139.075},0).wait(1).to({y:138.425},0).wait(1).to({y:137.975},0).wait(1).to({y:137.675},0).wait(1).to({y:137.575},0).wait(3).to({y:138.425},0).wait(1).to({y:138.925},0).wait(1).to({y:139.075},0).wait(1).to({y:138.425},0).wait(1).to({y:137.975},0).wait(1).to({y:137.675},0).wait(1).to({y:137.575},0).wait(22));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_4_Layer_14 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_14
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(8.5,1,1).p("AARgUIghAp");
	this.shape.setTransform(242.475,135.425);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(8.5,1,1).p("AgQAVIAhgp");
	this.shape_1.setTransform(242.475,136.275);
	this.shape_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(89).to({_off:true},1).wait(2).to({_off:false,y:136.925},0).to({_off:true},1).wait(3).to({_off:false,y:135.425},0).wait(2).to({_off:true},1).wait(2).to({_off:false,y:136.925},0).to({_off:true},1).wait(3).to({_off:false,y:135.425},0).wait(2).to({_off:true},1).wait(2).to({_off:false,y:136.925},0).to({_off:true},1).wait(3).to({_off:false,y:135.425},0).wait(2).to({_off:true},1).wait(2).to({_off:false,y:136.925},0).to({_off:true},1).wait(3).to({_off:false,y:135.425},0).wait(22));
	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(90).to({_off:false},0).wait(1).to({y:136.775},0).to({_off:true},1).wait(1).to({_off:false,y:136.275},0).wait(1).to({y:135.825},0).wait(1).to({y:135.525},0).to({_off:true},1).wait(3).to({_off:false,y:136.275},0).wait(1).to({y:136.775},0).to({_off:true},1).wait(1).to({_off:false,y:136.275},0).wait(1).to({y:135.825},0).wait(1).to({y:135.525},0).to({_off:true},1).wait(3).to({_off:false,y:136.275},0).wait(1).to({y:136.775},0).to({_off:true},1).wait(1).to({_off:false,y:136.275},0).wait(1).to({y:135.825},0).wait(1).to({y:135.525},0).to({_off:true},1).wait(3).to({_off:false,y:136.275},0).wait(1).to({y:136.775},0).to({_off:true},1).wait(1).to({_off:false,y:136.275},0).wait(1).to({y:135.825},0).wait(1).to({y:135.525},0).to({_off:true},1).wait(22));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_4_Layer_13 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_13
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(8.5,1,1).p("AhbgBQA5BGA5gUQA5gUAMhS");
	this.shape.setTransform(237.8,154.8534);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(92).to({y:155.7034},0).wait(1).to({y:156.2034},0).wait(1).to({y:156.3534},0).wait(1).to({y:155.7034},0).wait(1).to({y:155.2534},0).wait(1).to({y:154.9534},0).wait(1).to({y:154.8534},0).wait(3).to({y:155.7034},0).wait(1).to({y:156.2034},0).wait(1).to({y:156.3534},0).wait(1).to({y:155.7034},0).wait(1).to({y:155.2534},0).wait(1).to({y:154.9534},0).wait(1).to({y:154.8534},0).wait(3).to({y:155.7034},0).wait(1).to({y:156.2034},0).wait(1).to({y:156.3534},0).wait(1).to({y:155.7034},0).wait(1).to({y:155.2534},0).wait(1).to({y:154.9534},0).wait(1).to({y:154.8534},0).wait(3).to({y:155.7034},0).wait(1).to({y:156.2034},0).wait(1).to({y:156.3534},0).wait(1).to({y:155.7034},0).wait(1).to({y:155.2534},0).wait(1).to({y:154.9534},0).wait(1).to({y:154.8534},0).wait(20));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_4_Layer_11 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_11
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(8.5,1,1).p("AgygFQAtA8A4hL");
	this.shape.setTransform(283.5,127.9457);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(8.5,1,1).p("AgygBQA4AsAtg7");
	this.shape_1.setTransform(283.5,125.9643);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(8.5,1,1).p("AgyAAQA9AmAog1");
	this.shape_2.setTransform(283.5,125.2177);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(8.5,1,1).p("AgyAAQBAAkAlgy");
	this.shape_3.setTransform(283.5,124.7827);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(8.5,1,1).p("AgyABQBCAhAjgv");
	this.shape_4.setTransform(283.5,124.5343);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(8.5,1,1).p("AgyABQBCAgAjgu");
	this.shape_5.setTransform(283.5,124.3481);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(8.5,1,1).p("AgyABQBDAfAigt");
	this.shape_6.setTransform(283.5,124.274);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(8.5,1,1).p("AgyAAQBAAjAlgx");
	this.shape_7.setTransform(283.5,124.7706);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(8.5,1,1).p("AgyAAQA9AnAog2");
	this.shape_8.setTransform(283.5,125.2799);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(8.5,1,1).p("AgygBQA6ArArg6");
	this.shape_9.setTransform(283.5,125.8276);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(8.5,1,1).p("AgygCQA2AvAvg+");
	this.shape_10.setTransform(283.5,126.3378);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(8.5,1,1).p("AgygDQAzAzAyhC");
	this.shape_11.setTransform(283.5,126.8861);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(8.5,1,1).p("AgygEQAwA4A1hH");
	this.shape_12.setTransform(283.5,127.397);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},88).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6,p:{y:124.274}}]},1).to({state:[{t:this.shape_6,p:{y:124.2289}}]},1).to({state:[{t:this.shape_6,p:{y:124.2289}}]},33).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape}]},1).wait(10));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_4_Layer_10 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_10
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(8.5,1,1).p("AgygFQAtA8A5hL");
	this.shape.setTransform(305.55,123.7957);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(8.5,1,1).p("AgygBQAwAqA2g5");
	this.shape_1.setTransform(305.55,121.7654);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(8.5,1,1).p("AgyAAQAyAkA0gy");
	this.shape_2.setTransform(305.55,120.9827);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(8.5,1,1).p("AgyABQAyAgA0gu");
	this.shape_3.setTransform(305.55,120.5361);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(8.5,1,1).p("AgyACQAyAdA0gr");
	this.shape_4.setTransform(305.55,120.276);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(8.5,1,1).p("AgyACQAyAcA0gq");
	this.shape_5.setTransform(305.55,120.1021);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(8.5,1,1).p("AgyACQAyAbA0gp");
	this.shape_6.setTransform(305.55,120.0283);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(8.5,1,1).p("AgyABQAyAfA0gt");
	this.shape_7.setTransform(305.55,120.524);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(8.5,1,1).p("AgyAAQAxAoA1g3");
	this.shape_8.setTransform(305.55,121.6165);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(8.5,1,1).p("AgygBQAwAtA2g8");
	this.shape_9.setTransform(305.55,122.1388);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(8.5,1,1).p("AgygCQAvAxA3hA");
	this.shape_10.setTransform(305.55,122.6993);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(8.5,1,1).p("AgygEQAuA3A4hG");
	this.shape_11.setTransform(305.55,123.2347);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},88).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2,p:{y:120.9827}}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6,p:{y:120.0283}}]},1).to({state:[{t:this.shape_6,p:{y:119.9683}}]},1).to({state:[{t:this.shape_6,p:{y:119.9683}}]},33).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_2,p:{y:121.057}}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape}]},1).wait(10));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_4_Layer_9 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_9
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(8.5,1,1).p("AhIAhQAshIBlAH");
	this.shape.setTransform(301.075,144.9153);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(8.5,1,1).p("AhOAoQAjhfB6AT");
	this.shape_1.setTransform(301.3,142.2861);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(8.5,1,1).p("AhRArQAhhpCCAZ");
	this.shape_2.setTransform(301.375,141.245);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(8.5,1,1).p("AhTAtQAghuCHAb");
	this.shape_3.setTransform(301.425,140.6359);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(8.5,1,1).p("AhUAuQAfhxCKAc");
	this.shape_4.setTransform(301.425,140.29);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(8.5,1,1).p("AhUAuQAehzCLAe");
	this.shape_5.setTransform(301.475,140.076);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(8.5,1,1).p("AhVAvQAeh0CNAe");
	this.shape_6.setTransform(301.475,139.9439);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(8.5,1,1).p("AhVAvQAeh1CNAf");
	this.shape_7.setTransform(301.475,139.8827);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(8.5,1,1).p("AhTAtQAghvCHAc");
	this.shape_8.setTransform(301.425,140.628);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(8.5,1,1).p("AhRArQAhhoCCAY");
	this.shape_9.setTransform(301.35,141.3267);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(8.5,1,1).p("AhPApQAkhiB7AV");
	this.shape_10.setTransform(301.3,142.0663);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(8.5,1,1).p("AhOAnQAmhbB3AR");
	this.shape_11.setTransform(301.25,142.7674);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(8.5,1,1).p("AhMAlQAohVBwAO");
	this.shape_12.setTransform(301.2,143.502);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(8.5,1,1).p("AhKAjQAqhOBrAK");
	this.shape_13.setTransform(301.125,144.1888);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},88).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_7}]},33).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape}]},1).wait(10));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_4_Layer_8 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_8
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(13,1,1).p("An0JcQERhACVjIQBiiGBMkIQA3jAACgGQAhhrAlhAQBdilC5gL");
	this.shape.setTransform(124.45,-103.525);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(13,1,1).p("An5IyQBXgUBJghQBcgrBNg/QAxgqApgzQA7hLA0h2QAmhRAhhiQAjhsAQgsQAJgdACgCQAjhiAmg7QATgeAXgZQBZhhCQgH");
	this.shape_1.setTransform(124.925,-98.9);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(13,1,1).p("AoHGzQBTgUBEgdQBTglBRg2QAwgiArgqQA/g9A8hfQAtg/AnhOQAqhTAXgeQAKgWAEAAQAohIAqgpQAUgWAXgRQBZhECFgB");
	this.shape_2.setTransform(126.4,-84.975);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(13,1,1).p("AofDhQBOgUA7gWQBDgbBaglQAugXAwgaQBCgmBKg3QA6gmAygoQA0gsAjgFQALgKAHACQAxgcAwgMQAWgHAXgFQBZgVBzAK");
	this.shape_3.setTransform(128.825,-61.9389);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(13,1,1).p("ApBgiQBHgSAugNQAugNBmgPQBlgPCggCQCfgDBkA2QBlA2BZAtQBZAuBbAZ");
	this.shape_4.setTransform(132.225,-33.1306);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(13,1,1).p("AoBkqQBNACA5AFQBDALBdAJQB5AfB2AlQANAGANAGQBOAiA2AuQAxAhAkA1QAsA1AjA2QAaAkAYAiQA9BXA9A8");
	this.shape_5.setTransform(125.875,-4.625);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(13,1,1).p("AnemjQBRAMA+APQBNAXBaAWQCFA4BsA6QANAKANAKQBLA0AwA/QAqAsAdBDQAiBAAaBFQATAtASApQAsBtAtBP");
	this.shape_6.setTransform(122.425,8.675);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(13,1,1).p("AnIn0QBUAUBCAVQBUAgBXAeQCNBIBmBJQAOALAMANQBJBAAsBLQAlA0AYBMQAcBIATBOQAOAzAPAuQAiB8AjBb");
	this.shape_7.setTransform(120.15,17.5);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(13,1,1).p("Am3ouQBVAZBFAaQBZAmBWAkQCSBUBiBSQANAOAMAOQBHBJAqBTQAhA5AVBTQAXBNAPBWQALA2ALAzQAaCFAcBl");
	this.shape_8.setTransform(118.525,23.875);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(13,1,1).p("AmqpbQBWAdBHAeQBdAqBUApQCXBdBeBaQANAPAMAPQBGBQAoBaQAeA9ATBZQAUBRALBaQAIA6AJA1QAUCOAWBs");
	this.shape_9.setTransform(117.275,28.7);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(13,1,1).p("Amhp8QBXAfBJAhQBgAuBTAsQCbBkBbBgQANAQAMAQQBEBVAnBfQAcBAARBcQARBUAJBfQAHA8AGA4QAQCTASBx");
	this.shape_10.setTransform(116.3,32.425);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(13,1,1).p("AmZqXQBZAiBKAjQBkAyBUAwQCZBnBYBkQAQATAOAUQBABUAkBjQAdBEAPBlQANBPAHBiQAFBAAFA8QAMCUAPB1");
	this.shape_11.setTransform(115.55,35.375);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(13,1,1).p("AmdqXQBZAjBJAjQBkAyBUAxQCXBmBYBkQAPATAPAVQBABUAlBiQAcBEAQBkQAPBPAHBhQAGBAAGA8QANCTAUB3");
	this.shape_12.setTransform(115.925,35.375);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(13,1,1).p("AmpqXQBWAkBJAmQBiA0BSAyQCSBlBVBhQASAVAQAWQBABUAmBfQAdBDASBjQASBOAJBeQAIA/AKA9QARCOAkB/");
	this.shape_13.setTransform(117.15,35.375);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(13,1,1).p("AnBqXQBTAnBHArQBdA3BQA1QCJBiBQBbQATAYAWAbQA/BSAnBbQAgBBAXBfQAWBNAOBZQAMA9ASA9QAWCHBFCN");
	this.shape_14.setTransform(119.525,35.375);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(13,1,1).p("AnlqXQBNAsBEAyQBWA8BMA5QB7BdBIBUQAXAbAcAjQA/BPAqBVQAjA+AfBaQAeBKAUBRQATA5AcA/QAgB8B2Ci");
	this.shape_15.setTransform(123.175,35.375);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(13,1,1).p("AoUqYQBGAzBBA7QBNBDBHA9QBpBYA/BJQAbAhAlArQA9BNAsBMQAoA8ApBSQAnBGAdBHQAcA2AqBBQAtBsC1C+");
	this.shape_16.setTransform(127.85,35.4);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(13,1,1).p("ApDqYQA/A5A9BEQBFBJBBBCQBXBTA2A/QAfAlAuA1QA9BKAvBDQAsA6AyBKQAxBDAlA8QAkAyA4BDQA6BdD1Da");
	this.shape_17.setTransform(132.525,35.4);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#000000").ss(13,1,1).p("ApnqYQA4A9A7BMQA+BOA9BGQBJBOAuA3QAjAqA1A8QA9BHAxA8QAwA4A5BEQA5BBArA0QArAvBCBEQBEBREmDw");
	this.shape_18.setTransform(136.15,35.4);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#000000").ss(13,1,1).p("Ap/qYQA1BAA5BRQA6BSA7BHQA+BMArAxQAkAsA5BBQA9BGAzA5QAyA1A+BBQA+A+AvAwQAwAtBJBFQBKBKFFD9");
	this.shape_19.setTransform(138.5,35.4);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#000000").ss(13,1,1).p("AqLqYQAzBCA4BTQA3BTA5BKQA7BJAnAvQAlAuA8BDQA9BFA0A2QA0A1BBA/QA/A9AyAtQAwAsBOBFQBNBGFXEF");
	this.shape_20.setTransform(139.75,35.4);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#000000").ss(13,1,1).p("AqPqYQAzBDA3BTQA3BUA5BJQA5BKAmAuQAnAuA8BEQA8BEA1A2QAzA0BCA+QBAA+AyAsQAyArBPBGQBOBFFcEH");
	this.shape_21.setTransform(140.1,35.4);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#000000").ss(13,1,1).p("AqQqXQAzBCA4BUQA3BUA4BJQA5BJAnAuQAmAuA9BEQA8BFA0A1QA1A0BBA/QBAA9AzAsQAyArBOBGQBPBFFbEH");
	this.shape_22.setTransform(140.3,35.35);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#000000").ss(13,1,1).p("AqTqSQAyBBA5BUQA3BTA5BJQA6BJAnAuQAmAtA9BEQA9BEA0A1QA1A0BCA9QBBA9AyAsQAyArBQBFQBOBEFdEF");
	this.shape_23.setTransform(141.05,35.175);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#000000").ss(13,1,1).p("AqZqLQAyBBA5BTQA5BSA6BJQA6BIAoAtQAnAtA9BEQA+BDA1AzQA1A1BCA9QBBA8AzArQAzAqBQBEQBPBEFgEB");
	this.shape_24.setTransform(142.45,34.825);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f().s("#000000").ss(13,1,1).p("Aqkp/QAzBAA7BSQA6BRA7BIQA8BHAoAtQAnAsA/BCQA/BCA2AzQA1AzBEA8QBCA7A0AqQAzAqBRBCQBQBCFkD7");
	this.shape_25.setTransform(144.6,34.275);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f().s("#000000").ss(13,1,1).p("AqypwQA0A/A9BRQA8BQA9BGQA9BGApAsQApArBABBQBABAA3AyQA3AyBEA6QBEA6A0ApQA1AoBSBBQBRBAFqDz");
	this.shape_26.setTransform(147.5,33.55);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f().s("#000000").ss(13,1,1).p("ArBpeQA0A9A/BPQA+BOA/BFQA/BFArAqQAqAqBBBAQBCA/A4AwQA4AxBGA4QBEA4A2AoQA2AnBUA/QBTA9FvDq");
	this.shape_27.setTransform(150.75,32.75);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f().s("#000000").ss(13,1,1).p("ArPpPQA1A9BBBNQBBBNBABEQBBBDArAqQArApBDA+QBDA9A5AvQA5AvBIA3QBGA3A3AmQA2AmBVA9QBUA8F1Di");
	this.shape_28.setTransform(153.625,32.05);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f().s("#000000").ss(13,1,1).p("ArZpDQA1A7BDBNQBCBMBCBCQBCBCAsApQArApBEA8QBFA9A5AuQA6AuBIA2QBHA2A4AlQA3AmBWA7QBWA7F4Db");
	this.shape_29.setTransform(155.775,31.5);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f().s("#000000").ss(13,1,1).p("Argo8QA2A7BDBMQBDBLBDBCQBDBCAsAoQAsAoBFA8QBGA8A5AuQA7AtBHA2QBJA0A4AkQA3AmBYA6QBWA6F7DY");
	this.shape_30.setTransform(157.15,31.15);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f().s("#000000").ss(13,1,1).p("Arjo3QA1A6BEBLQBEBLBDBCQBDBBAtAoQAsAoBGA8QBFA7A6AtQA7AuBIA0QBJA0A4AlQA4AlBXA6QBWA5F9DV");
	this.shape_31.setTransform(157.9,30.975);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f().s("#000000").ss(13,1,1).p("Arko2QA2A6BEBLQBDBLBEBBQBDBCAtAoQAsAoBGA7QBFA7A7AtQA6AuBJA0QBJA0A4AlQA4AkBXA6QBXA5F8DV");
	this.shape_32.setTransform(158.1,30.925);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f().s("#000000").ss(13,1,1).p("ArXo6QA3A5BFBKQBFBKBEBAQBHBDAuAqQArAnBDA6QBGA8A5AwQA6AuBGA3QBGA1A3AmQA1AnBVA6QBTA8FuDR");
	this.shape_33.setTransform(156.375,31.1);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f().s("#000000").ss(13,1,1).p("AqvpGQA7A3BFBFQBJBHBGA+QBRBHA0AyQAnAlA9A1QBFA/A3A1QA2AxA/A9QA/A4AwAuQAwApBKA7QBLBHFCDG");
	this.shape_34.setTransform(151.3,31.625);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f().s("#000000").ss(13,1,1).p("AptpZQBDAyBGA8QBPBCBKA8QBiBOA9A+QAhAgAyAtQBDBFAyBAQAwA1A0BGQA0A+AmA7QAmAuA5A7QA8BaD5Cy");
	this.shape_35.setTransform(142.775,32.525);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f().s("#000000").ss(13,1,1).p("AoQp0QBMArBIAxQBZA7BOA2QB6BZBJBPQAZAbAiAiQBBBLAtBQQAnA7AkBUQAjBFAZBMQAXA2AjA8QAnByCSCY");
	this.shape_36.setTransform(130.875,33.775);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f().s("#000000").ss(13,1,1).p("AmQq3QBaAmBMAlQBoA1BTA1QCaBtBTBlQAUAaAOAUQA8BXAkBkQABADABADQAZBGANBqQALBPAFBiQAAADAAADQADBCAEA+QAICYAKB6");
	this.shape_37.setTransform(114.675,38.8);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f().s("#000000").ss(13,1,1).p("AmKrNQBbAoBNAnQBqA4BTA3QCbBwBTBrQAUAbAOAUQA7BaAiBoQABACABADQAYBJAMBtQAJBRADBkQAAADAAADQABBEADA/QAFCbAIB9");
	this.shape_38.setTransform(114.05,41.225);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f().s("#000000").ss(13,1,1).p("AmGrcQBdAoBNApQBsA6BTA5QCbByBTBvQATAbAPAVQA6BcAiBqQABADABADQAXBKAKBvQAIBSACBmQAAADAAADQAABGACA/QADCdAGB/");
	this.shape_39.setTransform(113.6,42.9);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f().s("#000000").ss(13,1,1).p("AmDrnQBdAqBOAqQBtA6BSA7QCcBzBTByQAUAcAOAVQA6BdAgBrQABADABADQAXBMAKBwQAHBUAABmQAAADAAADQAABGABBBQACCeAFCB");
	this.shape_40.setTransform(113.3,44.05);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f().s("#000000").ss(13,1,1).p("AmBrtQBdAqBOAqQBvA8BSA7QCbB0BTB0QAUAcAOAVQA6BfAgBsQABADABADQAWBMAJBxQAHBUAABnQAAADAAAEQAABGAABBQABCfAECB");
	this.shape_41.setTransform(113.125,44.75);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f().s("#000000").ss(13,1,1).p("AmArxQDsBtCOBrQCOBqBTB0QBTB0AoCJQAoCJgBD3QgBD3AFC5");
	this.shape_42.setTransform(113.025,45.15);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f().s("#000000").ss(13,1,1).p("AmArxQDsBtCPBrQCNBqBTB0QBTB0AoCJQAoCJgGD9QgGD8APCu");
	this.shape_43.setTransform(113.025,45.15);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f().s("#000000").ss(13,1,1).p("AmArxQDsBtCPBrQCNBqBTB0QBTB0AoCJQAoCJgLECQgLECAZCj");
	this.shape_44.setTransform(113.025,45.15);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f().s("#000000").ss(13,1,1).p("AmArxQDsBtCPBrQCNBqBTB0QBTB0AoCJQAoCJgQEHQgQEIAjCY");
	this.shape_45.setTransform(113.025,45.15);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.f().s("#000000").ss(13,1,1).p("AmArxQDsBtCOBrQCOBqBTB0QBTB0AoCJQAoCJgVEMQgWENAuCO");
	this.shape_46.setTransform(113.025,45.15);

	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.f().s("#000000").ss(13,1,1).p("AmArxQDsBtCPBrQCNBqBTB0QBTB0AoCJQAoCJgMEDQgNEEAcCg");
	this.shape_47.setTransform(113.025,45.15);

	this.shape_48 = new cjs.Shape();
	this.shape_48.graphics.f().s("#000000").ss(13,1,1).p("AmArxQDsBtCPBrQCNBqBTB0QBTB0AoCJQAoCJgED7QgED7ALCx");
	this.shape_48.setTransform(113.025,45.15);

	this.shape_49 = new cjs.Shape();
	this.shape_49.graphics.f().s("#000000").ss(13,1,1).p("AmCrxQDsBtCPBrQCNBqBTB0QBTB0AoCJQAoCJAFDyQAFDygHDD");
	this.shape_49.setTransform(113.238,45.15);

	this.shape_50 = new cjs.Shape();
	this.shape_50.graphics.f().s("#000000").ss(13,1,1).p("AmIrxQDsBtCPBrQCNBqBTB0QBTB0AoCJQAoCJAODpQANDpgYDV");
	this.shape_50.setTransform(113.8034,45.15);

	this.shape_51 = new cjs.Shape();
	this.shape_51.graphics.f().s("#000000").ss(13,1,1).p("AmNrxQDsBtCOBrQCOBqBTB0QBSB0ApCJQAoCJAWDgQAWDggqDn");
	this.shape_51.setTransform(114.3811,45.15);

	this.shape_52 = new cjs.Shape();
	this.shape_52.graphics.f().s("#000000").ss(13,1,1).p("AmTrxQDsBtCOBrQCOBqBTB0QBSB0ApCJQAoCJAfDXQAfDXg8D5");
	this.shape_52.setTransform(114.9685,45.15);

	this.shape_53 = new cjs.Shape();
	this.shape_53.graphics.f().s("#000000").ss(13,1,1).p("AmOrxQDsBtCOBrQCOBqBTB0QBSB0ApCJQAoCJAXDfQAXDfgsDp");
	this.shape_53.setTransform(114.4698,45.15);

	this.shape_54 = new cjs.Shape();
	this.shape_54.graphics.f().s("#000000").ss(13,1,1).p("AmJrxQDsBtCOBrQCOBqBTB0QBSB0ApCJQAoCJAQDnQAPDmgdDa");
	this.shape_54.setTransform(113.9699,45.15);

	this.shape_55 = new cjs.Shape();
	this.shape_55.graphics.f().s("#000000").ss(13,1,1).p("AmErxQDsBtCOBrQCOBqBTB0QBSB0ApCJQAoCJAIDuQAIDugODL");
	this.shape_55.setTransform(113.4705,45.15);

	this.shape_56 = new cjs.Shape();
	this.shape_56.graphics.f().s("#000000").ss(13,1,1).p("AmArxQDsBtCPBrQCNBqBTB0QBTB0AoCJQAoCJABD2QABD1ABC8");
	this.shape_56.setTransform(113.025,45.15);

	this.shape_57 = new cjs.Shape();
	this.shape_57.graphics.f().s("#000000").ss(13,1,1).p("AmArxQDsBtCPBrQCNBqBTB0QBTB0AoCJQAoCJgGD9QgHD+AQCs");
	this.shape_57.setTransform(113.025,45.15);

	this.shape_58 = new cjs.Shape();
	this.shape_58.graphics.f().s("#000000").ss(13,1,1).p("AmArxQDsBtCPBrQCNBqBTB0QBTB0AoCJQAoCJgOEFQgOEFAfCd");
	this.shape_58.setTransform(113.025,45.15);

	this.shape_59 = new cjs.Shape();
	this.shape_59.graphics.f().s("#000000").ss(13,1,1).p("AmOrxQDsBtCPBrQCNBqBTB0QBTB0AoCJQAoCJAXDfQAXDfgrDp");
	this.shape_59.setTransform(114.4365,45.15);

	this.shape_60 = new cjs.Shape();
	this.shape_60.graphics.f().s("#000000").ss(13,1,1).p("AmJrxQDsBtCPBrQCNBqBTB0QBTB0AoCJQAoCJAPDnQAPDngbDZ");
	this.shape_60.setTransform(113.9147,45.15);

	this.shape_61 = new cjs.Shape();
	this.shape_61.graphics.f().s("#000000").ss(13,1,1).p("AmDrxQDsBtCOBrQCOBqBTB0QBSB0ApCJQAoCJAHDvQAHDvgMDJ");
	this.shape_61.setTransform(113.3824,45.15);

	this.shape_62 = new cjs.Shape();
	this.shape_62.graphics.f().s("#000000").ss(13,1,1).p("AmIrVQBbAoBNAoQBrA4BRA3QCeByBVBwQAQAVANAVQA+BeAjBrQAXBKAMBtQAJBVABBpQACBFACBAQAECeAHB/");
	this.shape_62.setTransform(113.8,42.175);

	this.shape_63 = new cjs.Shape();
	this.shape_63.graphics.f().s("#000000").ss(13,1,1).p("AmfqEQBZAhBJAhQBjAwBUAuQCXBjBaBhQAPASAPATQBABSAmBgQAdBCAQBiQAPBOAJBfQAGA/AGA7QAOCQASBz");
	this.shape_63.setTransform(116.1,33.225);

	this.shape_64 = new cjs.Shape();
	this.shape_64.graphics.f().s("#000000").ss(13,1,1).p("AnFn8QBUAVBDAWQBWAhBZAgQCLBIBiBJQAQAOAPAOQBDA+AtBMQAmA2AYBRQAZBDATBPQAOA1AOAxQAhB6AiBd");
	this.shape_64.setTransform(119.925,18.35);

	this.shape_65 = new cjs.Shape();
	this.shape_65.graphics.f().s("#000000").ss(13,1,1).p("An7k9QBOAEA5AHQBFAMBeAMQB9AjBtAnQAPAIAQAIQBKAiA1AxQAyAkAjA5QAoAyAiA5QAZAnAZAlQA5BYA7A/");
	this.shape_65.setTransform(125.325,-2.525);

	this.shape_66 = new cjs.Shape();
	this.shape_66.graphics.f().s("#000000").ss(13,1,1).p("AoXEjQBQgUA+gYQBIgeBXgrQAvgaAugfQBBgtBGhFQA2gtAug0QAxg4AfgOQALgNAGABQAugqAugVQAVgMAXgJQBZgjB4AG");
	this.shape_66.setTransform(127.975,-69.1689);

	this.shape_67 = new cjs.Shape();
	this.shape_67.graphics.f().s("#000000").ss(13,1,1).p("AoFG/QBUgUBFgdQBTglBRg3QAwgjArgrQA+g+A8hiQAshCAnhOQAphXAWgfQAKgWAEgBQAohKApgrQAUgXAXgRQBYhHCFgC");
	this.shape_67.setTransform(126.125,-86.35);

	this.shape_68 = new cjs.Shape();
	this.shape_68.graphics.f().s("#000000").ss(13,1,1).p("An7ITQBWgUBIggQBagpBOg+QAwgoAqgwQA8hIA2hwQAnhNAjhdQAlhmASgpQAJgbACgCQAlhbAmg3QAUgcAXgXQBYhZCLgG");
	this.shape_68.setTransform(125.15,-95.525);

	this.shape_69 = new cjs.Shape();
	this.shape_69.graphics.f().s("#000000").ss(13,1,1).p("An2JAQBYgUBJgiQBdgrBMhBQAxgqApg0QA7hNA0h4QAkhTAghlQAjhvAPgtQAJgeACgCQAihlAmg9QATgfAWgaQBYhjCQgI");
	this.shape_69.setTransform(124.625,-100.475);

	this.shape_70 = new cjs.Shape();
	this.shape_70.graphics.f().s("#000000").ss(13,1,1).p("AnzJWQBYgVBKghQBfgtBLhDQAxgrAog1QA7hQAyh9QAjhVAfhpQAihzAOgvQAJgfABgDQAihpAlhAQATghAWgbQBYhoCRgJ");
	this.shape_70.setTransform(124.375,-102.85);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},14).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_11}]},8).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_21}]},8).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_27}]},1).to({state:[{t:this.shape_28}]},1).to({state:[{t:this.shape_29}]},1).to({state:[{t:this.shape_30}]},1).to({state:[{t:this.shape_31}]},1).to({state:[{t:this.shape_32}]},1).to({state:[{t:this.shape_32}]},8).to({state:[{t:this.shape_33}]},1).to({state:[{t:this.shape_34}]},1).to({state:[{t:this.shape_35}]},1).to({state:[{t:this.shape_36}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_37}]},1).to({state:[{t:this.shape_38}]},1).to({state:[{t:this.shape_39}]},1).to({state:[{t:this.shape_40}]},1).to({state:[{t:this.shape_41}]},1).to({state:[{t:this.shape_42}]},1).to({state:[{t:this.shape_42}]},2).to({state:[{t:this.shape_43}]},1).to({state:[{t:this.shape_44}]},1).to({state:[{t:this.shape_45}]},1).to({state:[{t:this.shape_46}]},1).to({state:[{t:this.shape_47}]},1).to({state:[{t:this.shape_48}]},1).to({state:[{t:this.shape_49}]},1).to({state:[{t:this.shape_50}]},1).to({state:[{t:this.shape_51}]},1).to({state:[{t:this.shape_52}]},1).to({state:[{t:this.shape_53}]},1).to({state:[{t:this.shape_54}]},1).to({state:[{t:this.shape_55}]},1).to({state:[{t:this.shape_56}]},1).to({state:[{t:this.shape_57}]},1).to({state:[{t:this.shape_58}]},1).to({state:[{t:this.shape_46}]},1).to({state:[{t:this.shape_47}]},1).to({state:[{t:this.shape_48}]},1).to({state:[{t:this.shape_49}]},1).to({state:[{t:this.shape_50}]},1).to({state:[{t:this.shape_51}]},1).to({state:[{t:this.shape_52}]},1).to({state:[{t:this.shape_59}]},1).to({state:[{t:this.shape_60}]},1).to({state:[{t:this.shape_61}]},1).to({state:[{t:this.shape_42}]},1).to({state:[{t:this.shape_42}]},8).to({state:[{t:this.shape_62}]},1).to({state:[{t:this.shape_63}]},1).to({state:[{t:this.shape_64}]},1).to({state:[{t:this.shape_65}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_66}]},1).to({state:[{t:this.shape_67}]},1).to({state:[{t:this.shape_68}]},1).to({state:[{t:this.shape_69}]},1).to({state:[{t:this.shape_70}]},1).to({state:[{t:this.shape}]},1).wait(16));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_4_Layer_7 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_7
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(13,1,1).p("Aoxm4QAsFfA5DLQA5DLB4BSQB3BSDFhOQDFhOFMkc");
	this.shape.setTransform(-28.875,33.2828);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(13,1,1).p("An3m9QgBFTAdDHQAdDIBoBcQBnBdDFgyQDEgxFejm");
	this.shape_1.setTransform(-34.7036,33.7628);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(13,1,1).p("AmvnEQgrFIAEDEQAEDEBaBmQBYBmDFgXQDDgZFviz");
	this.shape_2.setTransform(-41.8922,34.495);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(13,1,1).p("AlhnOQhPE+gTDBQgSDCBMBuQBMBvDEgBQDDgBF9iH");
	this.shape_3.setTransform(-49.7301,35.4754);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(13,1,1).p("AkYnaQhvE0gnC/QgmDABBB2QBAB2DDATQDDATGKhg");
	this.shape_4.setTransform(-57.0161,36.7076);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(13,1,1).p("AjWnpQiMEtg4C8Qg3C+A3B9QA2B8DDAlQDCAkGWg9");
	this.shape_5.setTransform(-63.5688,38.168);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(13,1,1).p("Aidn5QikEmhGC8QhGC7AuCCQAuCCDDAzQDCAzGfgg");
	this.shape_6.setTransform(-69.253,39.8419);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(13,1,1).p("AhuoMQi3EhhSC6QhTC6AoCGQAnCHDCA/QDCA/GngI");
	this.shape_7.setTransform(-73.9781,41.7005);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(13,1,1).p("AhJofQjGEdhcC5QhbC5AiCKQAhCKDDBJQDBBJGtAK");
	this.shape_8.setTransform(-77.6856,43.55);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(13,1,1).p("AguosQjREahiC4QhiC4AeCMQAeCNDCBQQDCBPGwAX");
	this.shape_9.setTransform(-80.362,44.9);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(13,1,1).p("Ageo0QjYEYhmC4QhmC3AcCOQAcCPDCBTQDCBTGzAf");
	this.shape_10.setTransform(-81.9765,45.7);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(13,1,1).p("AgZo3QjZEYhoC3QhnC4AbCOQAbCPDCBVQDCBUG0Ai");
	this.shape_11.setTransform(-82.5136,45.975);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(13,1,1).p("AiJoBQisEkhMC7QhLC7ArCEQArCEDDA5QDCA4GigW");
	this.shape_12.setTransform(-71.3451,40.5982);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(13,1,1).p("AjqnkQiDEvgzC9QgyC/A6B7QA5B6DDAfQDDAfGShH");
	this.shape_13.setTransform(-61.5502,37.6688);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(13,1,1).p("Ak/nTQhfE5gbDAQgcDBBHByQBGByDEAJQDCAHGEh0");
	this.shape_14.setTransform(-53.096,36.0228);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(13,1,1).p("AmGnJQg+FCgIDDQgIDDBSBrQBSBqDEgLQDDgNF3ib");
	this.shape_15.setTransform(-45.9854,34.981);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(13,1,1).p("AnAnCQgiFJAJDFQAJDGBdBkQBcBkDEgeQDDgdFsi+");
	this.shape_16.setTransform(-40.1787,34.311);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(13,1,1).p("Anrm+QgKFQAYDHQAYDHBlBfQBkBeDFgsQDDgsFijb");
	this.shape_17.setTransform(-35.9471,33.8989);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#000000").ss(13,1,1).p("AoFm7QAJFVAkDIQAkDJBrBaQBsBaDFg4QDEg5Fajy");
	this.shape_18.setTransform(-33.3,33.624);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#000000").ss(13,1,1).p("AoYm6QAYFaAtDJQAtDKByBWQBwBXDEhCQDFhCFUkF");
	this.shape_19.setTransform(-31.375,33.4613);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#000000").ss(13,1,1).p("Aomm5QAjFdA0DKQAzDKB1BUQB1BUDEhIQDFhJFQkS");
	this.shape_20.setTransform(-29.975,33.3543);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#000000").ss(13,1,1).p("Aovm4QAqFfA4DKQA3DLB4BSQB2BTDFhMQDFhNFOka");
	this.shape_21.setTransform(-29.15,33.2971);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},14).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_11}]},96).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape}]},1).wait(13));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_4_eggs_png = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// eggs_png
	this.instance = new lib.eggs();
	this.instance.parent = this;
	this.instance.setTransform(50.5,106.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(145));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_3_Layer_3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_3
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(13,1,1).p("AhcgIQBvAeBKgX");
	this.shape.setTransform(15.575,10.2645);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(13,1,1).p("AhZgIQBwAqBDgt");
	this.shape_1.setTransform(15.325,10.3196);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(13,1,1).p("AhXgFQByA1A9hB");
	this.shape_2.setTransform(15.075,10.0451);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(13,1,1).p("AhVgDQB0A/A3hS");
	this.shape_3.setTransform(14.875,9.8432);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(13,1,1).p("AhTgBQB1BIAyhi");
	this.shape_4.setTransform(14.7,9.6491);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(13,1,1).p("AhSAAQB2BQAvhw");
	this.shape_5.setTransform(14.55,9.4788);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(13,1,1).p("AhQABQB3BXAqh7");
	this.shape_6.setTransform(14.4,9.3436);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(13,1,1).p("AhPACQB4BcAniE");
	this.shape_7.setTransform(14.3,9.243);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(13,1,1).p("AhOADQB4BgAliM");
	this.shape_8.setTransform(14.2,9.1355);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(13,1,1).p("AhOADQB6BkAjiR");
	this.shape_9.setTransform(14.15,9.0913);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(13,1,1).p("AhNADQB6BmAhiV");
	this.shape_10.setTransform(14.1,9.0522);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(13,1,1).p("AhNAEQB6BmAhiW");
	this.shape_11.setTransform(14.1,9.0323);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(13,1,1).p("AhQABQB4BaAoiA");
	this.shape_12.setTransform(14.35,9.2872);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(13,1,1).p("AhSAAQB2BNAvhr");
	this.shape_13.setTransform(14.6,9.5477);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(13,1,1).p("AhUgCQB1BDA0ha");
	this.shape_14.setTransform(14.8,9.7459);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(13,1,1).p("AhWgEQBzA6A6hK");
	this.shape_15.setTransform(14.975,9.9411);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(13,1,1).p("AhXgGQByAzA9g9");
	this.shape_16.setTransform(15.125,10.1162);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(13,1,1).p("AhZgIQBxAtBCgx");
	this.shape_17.setTransform(15.275,10.2589);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#000000").ss(13,1,1).p("AhagJQBwAnBFgn");
	this.shape_18.setTransform(15.375,10.3688);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#000000").ss(13,1,1).p("AhbgIQBwAjBHgg");
	this.shape_19.setTransform(15.475,10.3391);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#000000").ss(13,1,1).p("AhbgIQBvAgBIgb");
	this.shape_20.setTransform(15.525,10.2926);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape,p:{y:10.2645}}]}).to({state:[{t:this.shape,p:{y:10.2645}}]},14).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_11}]},96).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape,p:{y:10.2658}}]},1).to({state:[{t:this.shape,p:{y:10.2645}}]},1).wait(13));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_3_Layer_2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(13,1,1).p("AgYCRQgGiRA4iQ");
	this.shape.setTransform(11.9251,-4.25);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(13,1,1).p("AgZCCQgFiQA4hz");
	this.shape_1.setTransform(11.976,-2.75);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(13,1,1).p("AgZB0QgGiPA5hY");
	this.shape_2.setTransform(12.0012,-1.4);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(13,1,1).p("AgZBoQgGiPA6hA");
	this.shape_3.setTransform(12.0264,-0.175);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(13,1,1).p("AgaBdQgFiPA6gq");
	this.shape_4.setTransform(12.0516,0.875);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(13,1,1).p("AgaBUQgFiPA6gY");
	this.shape_5.setTransform(12.0767,1.8);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(13,1,1).p("AgaBMQgGiPA7gI");
	this.shape_6.setTransform(12.1019,2.6);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(13,1,1).p("AgaBGQgGiPA8AE");
	this.shape_7.setTransform(12.1271,3.2182);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(13,1,1).p("AgaBBQgGiPA8AP");
	this.shape_8.setTransform(12.1271,3.6587);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(13,1,1).p("AgbA+QgFiPA8AW");
	this.shape_9.setTransform(12.1523,3.9542);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(13,1,1).p("AgbA9QgFiPA8Aa");
	this.shape_10.setTransform(12.1523,4.1012);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(13,1,1).p("AgbA8QgFiPA8Ac");
	this.shape_11.setTransform(12.1518,4.1511);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(13,1,1).p("AgaBJQgGiPA7gC");
	this.shape_12.setTransform(12.1019,2.875);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(13,1,1).p("AgaBXQgFiPA6ge");
	this.shape_13.setTransform(12.0767,1.525);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(13,1,1).p("AgaBjQgFiPA6g2");
	this.shape_14.setTransform(12.0516,0.3);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(13,1,1).p("AgZBuQgGiQA6hL");
	this.shape_15.setTransform(12.0264,-0.75);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(13,1,1).p("AgZB3QgGiPA5he");
	this.shape_16.setTransform(12.0012,-1.675);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(13,1,1).p("AgZB/QgFiPA4hu");
	this.shape_17.setTransform(11.976,-2.475);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#000000").ss(13,1,1).p("AgZCFQgFiPA4h6");
	this.shape_18.setTransform(11.9508,-3.1);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#000000").ss(13,1,1).p("AgZCKQgFiPA4iE");
	this.shape_19.setTransform(11.9508,-3.6);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#000000").ss(13,1,1).p("AgYCOQgGiPA4iM");
	this.shape_20.setTransform(11.9256,-3.975);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#000000").ss(13,1,1).p("AgYCQQgGiQA4iP");
	this.shape_21.setTransform(11.9256,-4.175);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},14).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_11}]},96).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape}]},1).wait(13));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_3_Layer_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(13,1,1).p("AAhCPQhMhrAMiy");
	this.shape.setTransform(0.0132,0.025);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(13,1,1).p("AgUiOQgcCyBMBr");
	this.shape_1.setTransform(0.5169,0.025);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(13,1,1).p("AgJiOQgqCyBMBr");
	this.shape_2.setTransform(0.8369,0.05);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(13,1,1).p("AAAiOQg1CyBMBr");
	this.shape_3.setTransform(1.0488,0.05);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(13,1,1).p("AAGiOQg9CyBNBr");
	this.shape_4.setTransform(1.1972,0.05);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(13,1,1).p("AAUCPQhMhrBEiy");
	this.shape_5.setTransform(1.3001,0.05);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(13,1,1).p("AAMhzQhFCVBOBS");
	this.shape_6.setTransform(1.4362,0.275);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(13,1,1).p("AALhdQhFB9BQA+");
	this.shape_7.setTransform(1.5347,0.475);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(13,1,1).p("AALhMQhGBqBRAv");
	this.shape_8.setTransform(1.6098,0.625);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(13,1,1).p("AALhAQhHBdBSAk");
	this.shape_9.setTransform(1.6706,0.725);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(13,1,1).p("AALg4QhHBVBSAc");
	this.shape_10.setTransform(1.7081,0.775);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(13,1,1).p("AALg2QhHBSBSAb");
	this.shape_11.setTransform(1.7081,0.8);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(13,1,1).p("AALhPQhGBtBRAy");
	this.shape_12.setTransform(1.6026,0.6);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(13,1,1).p("AALhkQhFCEBPBE");
	this.shape_13.setTransform(1.5042,0.4);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(13,1,1).p("AAMh1QhFCXBOBU");
	this.shape_14.setTransform(1.4057,0.25);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(13,1,1).p("AAMiDQhECmBNBh");
	this.shape_15.setTransform(1.3376,0.15);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(13,1,1).p("AgCiOQgzCyBMBr");
	this.shape_16.setTransform(1.022,0.05);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(13,1,1).p("AgNiOQglCyBMBr");
	this.shape_17.setTransform(0.7386,0.025);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#000000").ss(13,1,1).p("AgViOQgbCyBNBr");
	this.shape_18.setTransform(0.4624,0.025);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#000000").ss(13,1,1).p("AgaiOQgTCyBMBr");
	this.shape_19.setTransform(0.238,0.025);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#000000").ss(13,1,1).p("AgeiOQgOCyBNBr");
	this.shape_20.setTransform(0.0679,0.025);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},14).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_11}]},96).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape}]},1).wait(13));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_2_Layer_3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_3
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(13,1,1).p("AA0gaQg0AUgyAh");
	this.shape.setTransform(-8.45,1.525);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(13,1,1).p("AgwAdQAugkAzgV");
	this.shape_1.setTransform(-8.225,1.775);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(13,1,1).p("AgnAjQAdgvAygW");
	this.shape_2.setTransform(-7.5,2.575);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(13,1,1).p("AgZAtQABhBAygY");
	this.shape_3.setTransform(-6.325,3.85);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(13,1,1).p("AAOg5QgxAaAlBZ");
	this.shape_4.setTransform(-5.4816,5.675);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(13,1,1).p("AgUAwQgIhHAygY");
	this.shape_5.setTransform(-5.9797,4.3);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(13,1,1).p("AgjAmQAWg0AygX");
	this.shape_6.setTransform(-7.2,2.9);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(13,1,1).p("AgoAfQAkgiAtgb");
	this.shape_7.setTransform(-11.075,2.975);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(13,1,1).p("AgcAjQATgkAmgh");
	this.shape_8.setTransform(-13.7,4.475);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(13,1,1).p("AATgmQggAmgFAn");
	this.shape_9.setTransform(-16.325,5.925);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(13,1,1).p("AggAiQAZgkAogf");
	this.shape_10.setTransform(-12.875,4);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(13,1,1).p("AgqAeQAogiAtgZ");
	this.shape_11.setTransform(-10.425,2.625);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(13,1,1).p("AgxAcQAxghAxgW");
	this.shape_12.setTransform(-8.95,1.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},83).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape}]},1).wait(35));
	this.timeline.addTween(cjs.Tween.get(this.shape).wait(83).to({_off:true},1).wait(6).to({_off:false},0).to({_off:true},1).wait(5).to({_off:false},0).to({_off:true},1).wait(6).to({_off:false},0).to({_off:true},1).wait(6).to({_off:false},0).wait(35));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_2_Layer_2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(13,1,1).p("AhihNQCqgQAbCt");
	this.shape.setTransform(-3.725,6.2316);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(13,1,1).p("AhlhLQCrgSAgCr");
	this.shape_1.setTransform(-3.45,6.019);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(13,1,1).p("AhthFQCugVAtCi");
	this.shape_2.setTransform(-2.625,5.3751);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(13,1,1).p("Ah7g5QCzgdBECV");
	this.shape_3.setTransform(-1.225,4.2475);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(13,1,1).p("AiPgpQC6gnBlCD");
	this.shape_4.setTransform(0.7,2.6469);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(13,1,1).p("AiAg2QC1geBMCQ");
	this.shape_5.setTransform(-0.775,3.8857);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(13,1,1).p("AhxhCQCvgXA0Cf");
	this.shape_6.setTransform(-2.25,5.0745);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(13,1,1).p("AhOhRQCmgYgKC+");
	this.shape_7.setTransform(-5.7939,6.5907);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(13,1,1).p("Ag+hVQCigfgwDO");
	this.shape_8.setTransform(-7.3777,6.967);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(13,1,1).p("AgyhYQCdgohWDf");
	this.shape_9.setTransform(-8.519,7.3023);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(13,1,1).p("AhChTQCjgdglDI");
	this.shape_10.setTransform(-6.9428,6.8426);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(13,1,1).p("AhThQQCngWgBC5");
	this.shape_11.setTransform(-5.2997,6.5089);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(13,1,1).p("AhehOQCpgRAUCw");
	this.shape_12.setTransform(-4.125,6.2963);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},83).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape}]},1).wait(35));
	this.timeline.addTween(cjs.Tween.get(this.shape).wait(83).to({_off:true},1).wait(6).to({_off:false},0).to({_off:true},1).wait(5).to({_off:false},0).to({_off:true},1).wait(6).to({_off:false},0).to({_off:true},1).wait(6).to({_off:false},0).wait(35));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_2_Layer_1copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(13,1,1).p("AiMgSQD3g7AiB0");
	this.shape.setTransform(0.025,0.1889);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(13,1,1).p("AiOgOQD5hAAkB0");
	this.shape_1.setTransform(0.2,-0.2711);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(13,1,1).p("AiTAAQD+hQApBy");
	this.shape_2.setTransform(0.725,-1.7116);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(13,1,1).p("AicAZQEHhqAyBt");
	this.shape_3.setTransform(1.6,-4.238);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(13,1,1).p("AioAqQETiPA+Bm");
	this.shape_4.setTransform(2.825,-5.9482);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(13,1,1).p("AifAeQEKhzA1Br");
	this.shape_5.setTransform(1.9,-4.7469);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(13,1,1).p("AiWAGQEBhWAsBw");
	this.shape_6.setTransform(0.95,-2.3843);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(13,1,1).p("AiEggQDphDAgCZ");
	this.shape_7.setTransform(-0.775,1.5844);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(13,1,1).p("Ah8gvQDchKAdC9");
	this.shape_8.setTransform(-1.6,3.0028);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(13,1,1).p("Ah0g8QDOhTAbDi");
	this.shape_9.setTransform(-2.4,4.3762);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(13,1,1).p("Ah+gqQDghIAeCy");
	this.shape_10.setTransform(-1.35,2.5673);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(13,1,1).p("AiGgdQDthBAgCQ");
	this.shape_11.setTransform(-0.575,1.244);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(13,1,1).p("AiLgVQD1g9AiB8");
	this.shape_12.setTransform(-0.125,0.4607);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},83).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape}]},1).wait(35));
	this.timeline.addTween(cjs.Tween.get(this.shape).wait(83).to({_off:true},1).wait(6).to({_off:false},0).to({_off:true},1).wait(5).to({_off:false},0).to({_off:true},1).wait(6).to({_off:false},0).to({_off:true},1).wait(6).to({_off:false},0).wait(35));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_1_Layer_5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_5
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(13,1,1).p("AC6hUQABBZg6AwQg6AwhKgIQhMgHg2gwQg4gvAEhV");
	this.shape.setTransform(124.1874,-104.0989);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(13,1,1).p("Ai5hWQgEBOA4AsQA2AsBMAHQBKAHA6gsQA6gtgBhT");
	this.shape_1.setTransform(124.1874,-104.802);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(13,1,1).p("Ai5hQQgEBJA4AoQA2ApBMAHQBKAGA6gpQA6gpgBhN");
	this.shape_2.setTransform(124.1874,-105.4055);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(13,1,1).p("Ai5hLQgEBEA4AmQA2AmBMAGQBKAGA6gmQA6gngBhH");
	this.shape_3.setTransform(124.1874,-105.9841);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(13,1,1).p("Ai5hGQgEBAA4AjQA2AkBMAGQBKAFA6gjQA6glgBhD");
	this.shape_4.setTransform(124.1874,-106.4631);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(13,1,1).p("Ai5hCQgEA9A4AhQA2AiBMAFQBKAFA6ghQA6gjgBg/");
	this.shape_5.setTransform(124.1874,-106.8612);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(13,1,1).p("Ai5g/QgEA6A4AfQA2AgBMAFQBKAFA6ggQA6ghgBg8");
	this.shape_6.setTransform(124.1874,-107.1912);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(13,1,1).p("Ai5g9QgEA4A4AeQA2AfBMAFQBKAFA6gfQA6gggBg5");
	this.shape_7.setTransform(124.1874,-107.4403);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(13,1,1).p("Ai5g7QgEA2A4AdQA2AfBMAEQBKAFA6geQA6gfgBg4");
	this.shape_8.setTransform(124.1874,-107.6202);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(13,1,1).p("Ai5g6QgEA1A4AdQA2AdBMAFQBKAFA6geQA6gegBg3");
	this.shape_9.setTransform(124.1874,-107.7388);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(13,1,1).p("AC6gzQABA2g6AeQg6AdhKgEQhMgFg2gdQg4gdAEg0");
	this.shape_10.setTransform(124.1874,-107.7698);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(13,1,1).p("Ai5hAQgEA6A4AgQA2AhBMAFQBKAFA6ggQA6ghgBg9");
	this.shape_11.setTransform(124.1874,-107.1412);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(13,1,1).p("Ai5hFQgEA/A4AjQA2AjBMAGQBKAFA6gjQA6gkgBhC");
	this.shape_12.setTransform(124.1874,-106.5626);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(13,1,1).p("Ai5hKQgEBEA4AlQA2AmBMAGQBKAGA6gmQA6gmgBhH");
	this.shape_13.setTransform(124.1874,-106.0586);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(13,1,1).p("Ai5hPQgEBIA4AnQA2ApBMAGQBKAGA6goQA6gpgBhL");
	this.shape_14.setTransform(124.1874,-105.5796);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(13,1,1).p("Ai5hTQgEBLA4AqQA2AqBMAHQBKAGA6gqQA6gqgBhP");
	this.shape_15.setTransform(124.1874,-105.1814);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(13,1,1).p("Ai5hWQgEBOA4AsQA2ArBMAHQBKAHA6gsQA6gsgBhS");
	this.shape_16.setTransform(124.1874,-104.852);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(13,1,1).p("Ai5hZQgEBRA4AsQA2AuBMAHQBKAHA6gtQA6gugBhV");
	this.shape_17.setTransform(124.1874,-104.5779);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#000000").ss(13,1,1).p("Ai5hbQgEBTA4AtQA2AvBMAHQBKAHA6guQA6gvgBhX");
	this.shape_18.setTransform(124.1874,-104.3724);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#000000").ss(13,1,1).p("Ai5hcQgEBUA4AuQA2AvBMAHQBKAIA6gvQA6gwgBhY");
	this.shape_19.setTransform(124.1874,-104.2234);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#000000").ss(13,1,1).p("Ai5hdQgEBUA4AvQA2AwBMAHQBKAIA6gwQA6gwgBhZ");
	this.shape_20.setTransform(124.1874,-104.1239);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},84).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_10}]},21).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape}]},1).wait(19));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_1_Layer_4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_4
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(13,1,1).p("ADFhcQACBig+A0Qg+A0hPgIQhQgIg6g0Qg6gzAEhd");
	this.shape.setTransform(199.214,-98.4293);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(13,1,1).p("AjEhfQgEBWA6AwQA6AwBQAIQBPAHA+gwQA+gxgCha");
	this.shape_1.setTransform(199.214,-99.5248);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(13,1,1).p("AjEhYQgEBQA6AsQA6AtBQAHQBPAHA+gtQA+gtgChU");
	this.shape_2.setTransform(199.214,-100.5279);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(13,1,1).p("AjEhSQgEBKA6ApQA6AqBQAHQBPAGA+gpQA+grgChO");
	this.shape_3.setTransform(199.214,-101.3871);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(13,1,1).p("AjEhNQgEBGA6AnQA6AnBQAGQBPAGA+gnQA+gngChK");
	this.shape_4.setTransform(199.214,-102.16);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(13,1,1).p("AjEhJQgEBCA6AkQA6AlBQAHQBPAFA+glQA+glgChF");
	this.shape_5.setTransform(199.214,-102.7894);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(13,1,1).p("AjEhFQgEA/A6AiQA6AjBQAGQBPAGA+gjQA+gkgChC");
	this.shape_6.setTransform(199.214,-103.3376);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(13,1,1).p("AjEhDQgEA9A6AhQA6AiBQAGQBPAFA+giQA+gigChA");
	this.shape_7.setTransform(199.214,-103.7429);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(13,1,1).p("AjEhAQgEA6A6AgQA6AhBQAGQBPAFA+ghQA+ghgCg+");
	this.shape_8.setTransform(199.214,-104.0171);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(13,1,1).p("AjEg/QgEA6A6AfQA6AgBQAGQBPAFA+ggQA+ghgCg9");
	this.shape_9.setTransform(199.214,-104.2162);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(13,1,1).p("ADFg4QACA8g+AgQg+AghPgFQhQgFg6ggQg6gfAEg6");
	this.shape_10.setTransform(199.214,-104.2662);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(13,1,1).p("AjEhGQgEBAA6AiQA6AkBQAGQBPAFA+gjQA+gkgChD");
	this.shape_11.setTransform(199.214,-103.2381);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(13,1,1).p("AjEhMQgEBFA6AmQA6AmBQAHQBPAGA+gnQA+gngChI");
	this.shape_12.setTransform(199.214,-102.3345);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(13,1,1).p("AjEhRQgEBKA6AoQA6AqBQAHQBPAGA+gpQA+gqgChO");
	this.shape_13.setTransform(199.214,-101.506);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(13,1,1).p("AjEhWQgEBOA6ArQA6AsBQAHQBPAHA+gsQA+gsgChT");
	this.shape_14.setTransform(199.214,-100.777);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(13,1,1).p("AjEhaQgEBSA6AtQA6AuBQAIQBPAHA+guQA+gvgChW");
	this.shape_15.setTransform(199.214,-100.1538);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(13,1,1).p("AjEheQgEBVA6AvQA6AwBQAIQBPAHA+gvQA+gxgCha");
	this.shape_16.setTransform(199.214,-99.63);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(13,1,1).p("AjEhhQgEBZA6AwQA6AxBQAIQBPAIA+gxQA+gygChd");
	this.shape_17.setTransform(199.214,-99.2002);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#000000").ss(13,1,1).p("AjEhjQgEBaA6AyQA6AyBQAIQBPAIA+gyQA+gzgChf");
	this.shape_18.setTransform(199.214,-98.8703);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#000000").ss(13,1,1).p("AjEhlQgEBcA6AyQA6A0BQAIQBPAIA+gzQA+g0gChh");
	this.shape_19.setTransform(199.214,-98.6019);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#000000").ss(13,1,1).p("AjEhmQgEBdA6AzQA6AzBQAJQBPAIA+g0QA+g0gChh");
	this.shape_20.setTransform(199.214,-98.4712);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},84).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_10}]},21).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape}]},1).wait(19));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_1_Layer_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000601").ss(13,1,1).p("AkchVQBYCfCSALQCRAKBGgoQBGgpAyhk");
	this.shape.setTransform(156.625,-52.5544);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(145));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FB0200").s().p("AAKA0IgDAAQgRgCgPgRQgPgQgDgVQgDgVAKgOQAMgOASACIADABQAQADANAPQAQARADAUQADAWgLANQgKAMgPAAIgCAAg");
	this.shape.setTransform(3.5988,2.7909);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol2, new cjs.Rectangle(-0.9,-2.4,9,10.4), null);


(lib.Symbol3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_144 = function() {
		this.___loopingOver___ = true;
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(144).call(this.frame_144).wait(1));

	// Layer_3_obj_
	this.Layer_3 = new lib.Symbol_3_Layer_3();
	this.Layer_3.name = "Layer_3";
	this.Layer_3.parent = this;
	this.Layer_3.setTransform(15.6,10.3,1,1,0,0,0,15.6,10.3);
	this.Layer_3.depth = 0;
	this.Layer_3.isAttachedToCamera = 0
	this.Layer_3.isAttachedToMask = 0
	this.Layer_3.layerDepth = 0
	this.Layer_3.layerIndex = 0
	this.Layer_3.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_3).wait(145));

	// Layer_2_obj_
	this.Layer_2 = new lib.Symbol_3_Layer_2();
	this.Layer_2.name = "Layer_2";
	this.Layer_2.parent = this;
	this.Layer_2.setTransform(11.9,-4.3,1,1,0,0,0,11.9,-4.3);
	this.Layer_2.depth = 0;
	this.Layer_2.isAttachedToCamera = 0
	this.Layer_2.isAttachedToMask = 0
	this.Layer_2.layerDepth = 0
	this.Layer_2.layerIndex = 1
	this.Layer_2.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_2).wait(145));

	// Layer_1_obj_
	this.Layer_1 = new lib.Symbol_3_Layer_1();
	this.Layer_1.name = "Layer_1";
	this.Layer_1.parent = this;
	this.Layer_1.depth = 0;
	this.Layer_1.isAttachedToCamera = 0
	this.Layer_1.isAttachedToMask = 0
	this.Layer_1.layerDepth = 0
	this.Layer_1.layerIndex = 2
	this.Layer_1.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_1).wait(145));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-9.8,-25.2,41.2,46);


(lib.Symbol2_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_144 = function() {
		this.___loopingOver___ = true;
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(144).call(this.frame_144).wait(1));

	// Layer_3_obj_
	this.Layer_3 = new lib.Symbol_2_Layer_3();
	this.Layer_3.name = "Layer_3";
	this.Layer_3.parent = this;
	this.Layer_3.setTransform(-8.4,1.5,1,1,0,0,0,-8.4,1.5);
	this.Layer_3.depth = 0;
	this.Layer_3.isAttachedToCamera = 0
	this.Layer_3.isAttachedToMask = 0
	this.Layer_3.layerDepth = 0
	this.Layer_3.layerIndex = 0
	this.Layer_3.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_3).wait(145));

	// Layer_2_obj_
	this.Layer_2 = new lib.Symbol_2_Layer_2();
	this.Layer_2.name = "Layer_2";
	this.Layer_2.parent = this;
	this.Layer_2.setTransform(-3.8,6.2,1,1,0,0,0,-3.8,6.2);
	this.Layer_2.depth = 0;
	this.Layer_2.isAttachedToCamera = 0
	this.Layer_2.isAttachedToMask = 0
	this.Layer_2.layerDepth = 0
	this.Layer_2.layerIndex = 1
	this.Layer_2.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_2).wait(145));

	// Layer_1_obj_
	this.Layer_1 = new lib.Symbol_2_Layer_1copy();
	this.Layer_1.name = "Layer_1";
	this.Layer_1.parent = this;
	this.Layer_1.setTransform(0,0.2,1,1,0,0,0,0,0.2);
	this.Layer_1.depth = 0;
	this.Layer_1.isAttachedToCamera = 0
	this.Layer_1.isAttachedToMask = 0
	this.Layer_1.layerDepth = 0
	this.Layer_1.layerIndex = 2
	this.Layer_1.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_1).wait(145));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-24.7,-16.6,51,39.900000000000006);


(lib.Symbol_4_Symbol_3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Symbol_3
	this.instance = new lib.Symbol3();
	this.instance.parent = this;
	this.instance.setTransform(21.55,42.25,1,1,0,0,0,1.7,15.7);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(14).to({regX:1.8,regY:15.8,rotation:44.9994,x:-47.2,y:102.2},11,cjs.Ease.get(1)).wait(96).to({regX:1.7,regY:15.7,rotation:0,x:21.55,y:42.25},11,cjs.Ease.get(1)).wait(13));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_4_Symbol_2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Symbol_2
	this.instance = new lib.Symbol2_1();
	this.instance.parent = this;
	this.instance.setTransform(170.4,-164.35,1,1,0,0,0,-15.3,-2.2);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(14).to({scaleX:0.9995,scaleY:0.9995,rotation:9.063,x:190,y:-21.95},4,cjs.Ease.get(-1)).wait(1).to({regX:0.8,regY:3.3,scaleX:0.9993,scaleY:0.9993,rotation:12.1207,x:191.8,y:33.95},0).wait(1).to({scaleX:0.9992,scaleY:0.9992,rotation:13.7676,x:184.7,y:59.85},0).wait(1).to({rotation:14.8618,x:179.9,y:77.05},0).wait(1).to({scaleX:0.9991,scaleY:0.9991,rotation:15.6518,x:176.45,y:89.4},0).wait(1).to({rotation:16.2481,x:173.9,y:98.75},0).wait(1).to({scaleX:0.999,scaleY:0.999,rotation:16.7103,x:171.9,y:106.05},0).wait(1).to({regX:-15.3,regY:-2.2,rotation:17.0744,x:156.6,y:101.8},0).wait(9).to({regX:0.8,regY:3.3,rotation:17.2965,x:171,y:111.85},0).wait(1).to({rotation:18.0525,x:173.35,y:112},0).wait(1).to({rotation:19.4957,x:177.8,y:112.3},0).wait(1).to({rotation:21.7327,x:184.75,y:112.85},0).wait(1).to({rotation:24.5896,x:193.55,y:113.5},0).wait(1).to({rotation:27.4408,x:202.25,y:114.1},0).wait(1).to({rotation:29.6668,x:209,y:114.55},0).wait(1).to({rotation:31.1013,x:213.4,y:114.8},0).wait(1).to({rotation:31.8528,x:215.7,y:114.95},0).wait(1).to({regX:-15.3,regY:-2.2,rotation:32.0736,x:205.6,y:101.85},0).wait(9).to({regX:0.8,regY:3.3,rotation:31.8916,x:216.7,y:114.75},0).wait(1).to({rotation:31.2793,x:218,y:114.05},0).wait(1).to({rotation:30.1224,x:220.4,y:112.8},0).wait(1).to({rotation:28.3192,x:224.15,y:110.8},0).wait(1).to({rotation:25.9021,x:229.15,y:108},0).wait(1).to({rotation:23.2152,x:234.75,y:104.95},0).wait(1).to({rotation:20.8065,x:239.75,y:102.2},0).wait(1).to({rotation:19.0131,x:243.4,y:100.05},0).wait(1).to({rotation:17.8632,x:245.75,y:98.75},0).wait(1).to({rotation:17.2547,x:246.95,y:98.05},0).wait(1).to({regX:-15.2,regY:-2.2,rotation:17.0738,x:233.7,y:87.9},0).wait(8).to({regX:-15.3,rotation:17.0744,x:156.6,y:101.8},5,cjs.Ease.get(-1)).wait(1).to({regX:0.8,regY:3.3,scaleX:0.9994,scaleY:0.9994,rotation:17.5628,x:168.45,y:118.45},0).wait(1).to({scaleX:0.9996,scaleY:0.9996,rotation:17.907,x:167.15,y:123.15},0).wait(1).to({scaleX:0.9998,scaleY:0.9998,rotation:18.1469,x:166.2,y:126.45},0).wait(1).to({scaleX:0.9999,scaleY:0.9999,rotation:18.3089,x:165.6,y:128.65},0).wait(1).to({scaleX:1,scaleY:1,rotation:18.4113,x:165.2,y:130.05},0).wait(1).to({regX:-15.3,regY:-2.1,rotation:18.467,x:151.5,y:120.55},0).wait(2).to({mode:"synched",startPosition:83,loop:false},0).wait(35).to({mode:"independent"},0).to({regY:-2.2,scaleX:0.9995,scaleY:0.9995,rotation:9.063,x:190,y:-21.95},5,cjs.Ease.get(-1)).wait(1).to({regX:0.8,regY:3.3,scaleX:0.9998,scaleY:0.9998,rotation:4.2019,x:195.1,y:-91.65},0).wait(1).to({scaleX:0.9999,scaleY:0.9999,rotation:2.0967,x:190.8,y:-125.3},0).wait(1).to({scaleX:1,scaleY:1,rotation:0.9769,x:188.45,y:-143.2},0).wait(1).to({rotation:0.3721,x:187.2,y:-152.9},0).wait(1).to({rotation:0.0815,x:186.6,y:-157.5},0).wait(1).to({regX:-15.3,regY:-2.2,rotation:0,x:170.4,y:-164.35},0).wait(16));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_1_Layer_3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_3
	this.instance = new lib.Symbol2();
	this.instance.parent = this;
	this.instance.setTransform(137.2,-49.4,2.0168,1.0421,0,46.0839,51.8739,5.4,7.5);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(29).to({_off:false},0).to({scaleX:2.0135,scaleY:1.6081,skewX:57.4327,skewY:51.8977,guide:{path:[137.2,-49.3,137.6,-48.9,138.1,-48.6]}},3).to({regX:5.5,regY:7.4,scaleX:2.0168,scaleY:1.8,skewX:61.2341,skewY:51.8739,guide:{path:[138.1,-48.5,138.2,-48.4,138.3,-48.4]}},1).wait(59).to({regX:3.6,regY:2.8,scaleX:2.0155,scaleY:1.8012,skewX:60.3326,skewY:50.9877,x:143.35,y:-55.05},0).wait(1).to({scaleX:2.0118,scaleY:1.8044,skewX:57.7949,skewY:48.493,x:144.3,y:-54.45},0).wait(1).to({scaleX:2.005,scaleY:1.8102,skewX:53.125,skewY:43.9021,x:146.1,y:-53.45},0).wait(1).to({scaleX:1.9942,scaleY:1.8194,skewX:45.8017,skewY:36.7027,x:149.2,y:-52.25},0).wait(1).to({scaleX:1.9796,scaleY:1.832,skewX:35.8072,skewY:26.8774,x:153.8,y:-51.6},0).wait(1).to({scaleX:1.9634,scaleY:1.846,skewX:24.6936,skewY:15.9518,x:158.8,y:-51.75},0).wait(1).to({scaleX:1.9495,scaleY:1.8579,skewX:15.1942,skewY:6.6132,x:162.55,y:-52.6},0).wait(1).to({scaleX:1.9401,scaleY:1.866,skewX:8.7674,skewY:0.2952,x:165.05,y:-53.45},0).wait(1).to({scaleX:1.935,scaleY:1.8704,skewX:5.2433,skewY:-3.1692,x:166.35,y:-54.1},0).wait(1).to({regX:5.5,regY:7.5,scaleX:1.9331,scaleY:1.872,skewX:3.9791,skewY:-4.4121,x:169.95,y:-46.05},0).to({guide:{path:[169.9,-46,169.9,-46,169.9,-46]}},5).wait(1).to({regX:3.6,regY:2.8,scaleX:1.9352,scaleY:1.8714,skewX:5.507,skewY:-3.011,x:166.3,y:-54.2},0).wait(1).to({scaleX:1.9406,scaleY:1.8697,skewX:9.4309,skewY:0.5871,x:164.85,y:-53.6},0).wait(1).to({scaleX:1.9497,scaleY:1.8669,skewX:16.0935,skewY:6.6965,x:162.2,y:-52.65},0).wait(1).to({scaleX:1.9623,scaleY:1.863,skewX:25.3702,skewY:15.203,x:158.35,y:-51.9},0).wait(1).to({scaleX:1.977,scaleY:1.8585,skewX:36.1611,skewY:25.0978,x:154,y:-51.7},0).wait(1).to({scaleX:1.9911,scaleY:1.8541,skewX:46.5484,skewY:34.6228,x:150.35,y:-52.1},0).wait(1).to({scaleX:2.0026,scaleY:1.8506,skewX:54.9355,skewY:42.3135,x:147.35,y:-52.9},0).wait(1).to({scaleX:2.0105,scaleY:1.8482,skewX:60.7412,skewY:47.6372,x:145.3,y:-53.9},0).wait(1).to({scaleX:2.0151,scaleY:1.8467,skewX:64.0938,skewY:50.7114,x:144.1,y:-54.7},0).wait(1).to({regX:5.5,regY:7.3,scaleX:2.0168,scaleY:1.8462,skewX:65.3617,skewY:51.8739,x:138.2,y:-48.5},0).to({scaleX:2.0135,scaleY:1.3804,skewX:50.8998,skewY:51.8977,guide:{path:[138.2,-48.4,138.1,-48.6,137.9,-48.7]}},3).to({regY:7.4,scaleX:2.0168,scaleY:1.2283,skewX:46.0842,skewY:51.8739,x:134.95,y:-45.55},1).to({_off:true},1).wait(24));

}).prototype = p = new cjs.MovieClip();


(lib.Scene_1_Layer_4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_4
	this.instance = new lib.Symbol7();
	this.instance.parent = this;
	this.instance.setTransform(94.05,381.85);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Scene_1_Layer_4, null, null);


(lib.Symbol1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_144 = function() {
		this.___loopingOver___ = true;
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(144).call(this.frame_144).wait(1));

	// Layer_5_obj_
	this.Layer_5 = new lib.Symbol_1_Layer_5();
	this.Layer_5.name = "Layer_5";
	this.Layer_5.parent = this;
	this.Layer_5.setTransform(124.2,-104.1,1,1,0,0,0,124.2,-104.1);
	this.Layer_5.depth = 0;
	this.Layer_5.isAttachedToCamera = 0
	this.Layer_5.isAttachedToMask = 0
	this.Layer_5.layerDepth = 0
	this.Layer_5.layerIndex = 0
	this.Layer_5.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_5).wait(145));

	// Layer_4_obj_
	this.Layer_4 = new lib.Symbol_1_Layer_4();
	this.Layer_4.name = "Layer_4";
	this.Layer_4.parent = this;
	this.Layer_4.setTransform(199.2,-98.5,1,1,0,0,0,199.2,-98.5);
	this.Layer_4.depth = 0;
	this.Layer_4.isAttachedToCamera = 0
	this.Layer_4.isAttachedToMask = 0
	this.Layer_4.layerDepth = 0
	this.Layer_4.layerIndex = 1
	this.Layer_4.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_4).wait(145));

	// Layer_1_obj_
	this.Layer_1 = new lib.Symbol_1_Layer_1();
	this.Layer_1.name = "Layer_1";
	this.Layer_1.parent = this;
	this.Layer_1.setTransform(156.6,-52.6,1,1,0,0,0,156.6,-52.6);
	this.Layer_1.depth = 0;
	this.Layer_1.isAttachedToCamera = 0
	this.Layer_1.isAttachedToMask = 0
	this.Layer_1.layerDepth = 0
	this.Layer_1.layerIndex = 2
	this.Layer_1.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_1).wait(145));

	// Layer_3_obj_
	this.Layer_3 = new lib.Symbol_1_Layer_3();
	this.Layer_3.name = "Layer_3";
	this.Layer_3.parent = this;
	this.Layer_3.depth = 0;
	this.Layer_3.isAttachedToCamera = 0
	this.Layer_3.isAttachedToMask = 0
	this.Layer_3.layerDepth = 0
	this.Layer_3.layerIndex = 3
	this.Layer_3.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_3).wait(29).to({x:130.6,y:-58.65},0).wait(3).to({x:0,y:0},1).wait(58).to({x:130.6,y:-58.65},0).wait(1).to({x:130.6166,y:-58.6575},0).wait(9).to({x:130.6,y:-58.65},0).to({x:0,y:0},5).wait(1).to({x:3.2509,y:-1.4599},0).wait(1).to({x:11.5996,y:-5.2092},0).wait(1).to({x:25.7752,y:-11.5751},0).wait(1).to({x:45.5127,y:-20.4389},0).wait(1).to({x:68.4716,y:-30.7493},0).wait(1).to({x:90.5723,y:-40.6743},0).wait(1).to({x:108.417,y:-48.688},0).wait(1).to({x:120.7694,y:-54.2353},0).wait(1).to({x:127.9025,y:-57.4386},0).wait(1).to({x:130.6,y:-58.65},0).wait(3).to({x:0,y:0},1).wait(25));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(99.1,-120.1,126.4,82.8);


(lib.Symbol_4_Symbol_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Symbol_1
	this.instance = new lib.Symbol1();
	this.instance.parent = this;
	this.instance.setTransform(34.5,-58.9,0.9997,0.9997,0,4.2549,-175.7451,156,-52.6);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(145));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_144 = function() {
		this.___loopingOver___ = true;
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(144).call(this.frame_144).wait(1));

	// Layer_9_obj_
	this.Layer_9 = new lib.Symbol_4_Layer_9();
	this.Layer_9.name = "Layer_9";
	this.Layer_9.parent = this;
	this.Layer_9.setTransform(301.1,144.9,1,1,0,0,0,301.1,144.9);
	this.Layer_9.depth = 0;
	this.Layer_9.isAttachedToCamera = 0
	this.Layer_9.isAttachedToMask = 0
	this.Layer_9.layerDepth = 0
	this.Layer_9.layerIndex = 0
	this.Layer_9.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_9).wait(145));

	// Layer_10_obj_
	this.Layer_10 = new lib.Symbol_4_Layer_10();
	this.Layer_10.name = "Layer_10";
	this.Layer_10.parent = this;
	this.Layer_10.setTransform(305.6,123.8,1,1,0,0,0,305.6,123.8);
	this.Layer_10.depth = 0;
	this.Layer_10.isAttachedToCamera = 0
	this.Layer_10.isAttachedToMask = 0
	this.Layer_10.layerDepth = 0
	this.Layer_10.layerIndex = 1
	this.Layer_10.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_10).wait(145));

	// Layer_11_obj_
	this.Layer_11 = new lib.Symbol_4_Layer_11();
	this.Layer_11.name = "Layer_11";
	this.Layer_11.parent = this;
	this.Layer_11.setTransform(283.5,128,1,1,0,0,0,283.5,128);
	this.Layer_11.depth = 0;
	this.Layer_11.isAttachedToCamera = 0
	this.Layer_11.isAttachedToMask = 0
	this.Layer_11.layerDepth = 0
	this.Layer_11.layerIndex = 2
	this.Layer_11.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_11).wait(145));

	// Layer_13_obj_
	this.Layer_13 = new lib.Symbol_4_Layer_13();
	this.Layer_13.name = "Layer_13";
	this.Layer_13.parent = this;
	this.Layer_13.setTransform(237.8,154.8,1,1,0,0,0,237.8,154.8);
	this.Layer_13.depth = 0;
	this.Layer_13.isAttachedToCamera = 0
	this.Layer_13.isAttachedToMask = 0
	this.Layer_13.layerDepth = 0
	this.Layer_13.layerIndex = 3
	this.Layer_13.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_13).wait(145));

	// Layer_14_obj_
	this.Layer_14 = new lib.Symbol_4_Layer_14();
	this.Layer_14.name = "Layer_14";
	this.Layer_14.parent = this;
	this.Layer_14.setTransform(242.5,135.4,1,1,0,0,0,242.5,135.4);
	this.Layer_14.depth = 0;
	this.Layer_14.isAttachedToCamera = 0
	this.Layer_14.isAttachedToMask = 0
	this.Layer_14.layerDepth = 0
	this.Layer_14.layerIndex = 4
	this.Layer_14.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_14).wait(145));

	// Layer_15_obj_
	this.Layer_15 = new lib.Symbol_4_Layer_15();
	this.Layer_15.name = "Layer_15";
	this.Layer_15.parent = this;
	this.Layer_15.setTransform(222.4,137.6,1,1,0,0,0,222.4,137.6);
	this.Layer_15.depth = 0;
	this.Layer_15.isAttachedToCamera = 0
	this.Layer_15.isAttachedToMask = 0
	this.Layer_15.layerDepth = 0
	this.Layer_15.layerIndex = 5
	this.Layer_15.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_15).wait(145));

	// Layer_17_obj_
	this.Layer_17 = new lib.Symbol_4_Layer_17();
	this.Layer_17.name = "Layer_17";
	this.Layer_17.parent = this;
	this.Layer_17.setTransform(149.8,158.6,1,1,0,0,0,149.8,158.6);
	this.Layer_17.depth = 0;
	this.Layer_17.isAttachedToCamera = 0
	this.Layer_17.isAttachedToMask = 0
	this.Layer_17.layerDepth = 0
	this.Layer_17.layerIndex = 6
	this.Layer_17.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_17).wait(145));

	// Layer_18_obj_
	this.Layer_18 = new lib.Symbol_4_Layer_18();
	this.Layer_18.name = "Layer_18";
	this.Layer_18.parent = this;
	this.Layer_18.setTransform(162.3,139,1,1,0,0,0,162.3,139);
	this.Layer_18.depth = 0;
	this.Layer_18.isAttachedToCamera = 0
	this.Layer_18.isAttachedToMask = 0
	this.Layer_18.layerDepth = 0
	this.Layer_18.layerIndex = 7
	this.Layer_18.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_18).wait(145));

	// Layer_19_obj_
	this.Layer_19 = new lib.Symbol_4_Layer_19();
	this.Layer_19.name = "Layer_19";
	this.Layer_19.parent = this;
	this.Layer_19.setTransform(133.4,140,1,1,0,0,0,133.4,140);
	this.Layer_19.depth = 0;
	this.Layer_19.isAttachedToCamera = 0
	this.Layer_19.isAttachedToMask = 0
	this.Layer_19.layerDepth = 0
	this.Layer_19.layerIndex = 8
	this.Layer_19.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_19).wait(145));

	// Symbol_3_obj_
	this.Symbol_3 = new lib.Symbol_4_Symbol_3();
	this.Symbol_3.name = "Symbol_3";
	this.Symbol_3.parent = this;
	this.Symbol_3.setTransform(30.6,24.4,1,1,0,0,0,30.6,24.4);
	this.Symbol_3.depth = 0;
	this.Symbol_3.isAttachedToCamera = 0
	this.Symbol_3.isAttachedToMask = 0
	this.Symbol_3.layerDepth = 0
	this.Symbol_3.layerIndex = 9
	this.Symbol_3.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Symbol_3).wait(145));

	// Layer_7_obj_
	this.Layer_7 = new lib.Symbol_4_Layer_7();
	this.Layer_7.name = "Layer_7";
	this.Layer_7.parent = this;
	this.Layer_7.setTransform(-28.9,33.3,1,1,0,0,0,-28.9,33.3);
	this.Layer_7.depth = 0;
	this.Layer_7.isAttachedToCamera = 0
	this.Layer_7.isAttachedToMask = 0
	this.Layer_7.layerDepth = 0
	this.Layer_7.layerIndex = 10
	this.Layer_7.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_7).wait(145));

	// Symbol_1_obj_
	this.Symbol_1 = new lib.Symbol_4_Symbol_1();
	this.Symbol_1.name = "Symbol_1";
	this.Symbol_1.parent = this;
	this.Symbol_1.setTransform(78.2,-63.1,1,1,0,0,0,78.2,-63.1);
	this.Symbol_1.depth = 0;
	this.Symbol_1.isAttachedToCamera = 0
	this.Symbol_1.isAttachedToMask = 0
	this.Symbol_1.layerDepth = 0
	this.Symbol_1.layerIndex = 11
	this.Symbol_1.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Symbol_1).wait(145));

	// Symbol_2_obj_
	this.Symbol_2 = new lib.Symbol_4_Symbol_2();
	this.Symbol_2.name = "Symbol_2";
	this.Symbol_2.parent = this;
	this.Symbol_2.setTransform(185.7,-156.9,1,1,0,0,0,185.7,-156.9);
	this.Symbol_2.depth = 0;
	this.Symbol_2.isAttachedToCamera = 0
	this.Symbol_2.isAttachedToMask = 0
	this.Symbol_2.layerDepth = 0
	this.Symbol_2.layerIndex = 12
	this.Symbol_2.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Symbol_2).wait(19).to({regX:206,regY:-10.5,x:206,y:-10.5},0).wait(6).to({regX:185.7,regY:-156.9,x:185.7,y:-156.9},0).wait(9).to({regX:206,regY:-10.5,x:206,y:-10.5},0).wait(9).to({regX:185.7,regY:-156.9,x:185.7,y:-156.9},0).wait(9).to({regX:206,regY:-10.5,x:206,y:-10.5},0).wait(10).to({regX:185.7,regY:-156.9,x:185.7,y:-156.9},0).wait(14).to({regX:206,regY:-10.5,x:206,y:-10.5},0).wait(5).to({regX:185.7,regY:-156.9,x:185.7,y:-156.9},0).wait(43).to({regX:206,regY:-10.5,x:206,y:-10.5},0).wait(5).to({regX:185.7,regY:-156.9,x:185.7,y:-156.9},0).wait(16));

	// Layer_20 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	var mask_graphics_0 = new cjs.Graphics().p("AtvX6Qgj22E52eIXMxEMAAABKvI2pCOg");
	var mask_graphics_75 = new cjs.Graphics().p("AtvX6Qgj22E52eIXMxEMAAABKvI2pCOg");

	this.timeline.addTween(cjs.Tween.get(mask).to({graphics:mask_graphics_0,x:177.3525,y:-40.375}).wait(75).to({graphics:mask_graphics_75,x:177.3525,y:-40.375}).wait(70));

	// Layer_8_obj_
	this.Layer_8 = new lib.Symbol_4_Layer_8();
	this.Layer_8.name = "Layer_8";
	this.Layer_8.parent = this;
	this.Layer_8.setTransform(124.5,-103.5,1,1,0,0,0,124.5,-103.5);
	this.Layer_8.depth = 0;
	this.Layer_8.isAttachedToCamera = 0
	this.Layer_8.isAttachedToMask = 0
	this.Layer_8.layerDepth = 0
	this.Layer_8.layerIndex = 13
	this.Layer_8.maskLayerName = 0

	var maskedShapeInstanceList = [this.Layer_8];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.Layer_8).wait(145));

	// eggs_png_obj_
	this.eggs_png = new lib.Symbol_4_eggs_png();
	this.eggs_png.name = "eggs_png";
	this.eggs_png.parent = this;
	this.eggs_png.setTransform(129.5,179,1,1,0,0,0,129.5,179);
	this.eggs_png.depth = 0;
	this.eggs_png.isAttachedToCamera = 0
	this.eggs_png.isAttachedToMask = 0
	this.eggs_png.layerDepth = 0
	this.eggs_png.layerIndex = 14
	this.eggs_png.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.eggs_png).wait(145));

	// pack_png_obj_
	this.pack_png = new lib.Symbol_4_pack_png();
	this.pack_png.name = "pack_png";
	this.pack_png.parent = this;
	this.pack_png.setTransform(-17.5,0,1,1,0,0,0,-17.5,0);
	this.pack_png.depth = 0;
	this.pack_png.isAttachedToCamera = 0
	this.pack_png.isAttachedToMask = 0
	this.pack_png.layerDepth = 0
	this.pack_png.layerIndex = 15
	this.pack_png.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.pack_png).wait(145));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-208.5,-263.5,523.5,527);


(lib.Scene_1_Layer_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Symbol4();
	this.instance.parent = this;
	this.instance.setTransform(167.9,405.75,0.6529,0.6529,0,0,0,0.1,0.1);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Scene_1_Layer_1, null, null);


// stage content:
(lib.Feb_2_Vert = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	this.___GetDepth___ = function(obj) {
		var depth = obj.depth;
		var cameraObj = this.___camera___instance;
		if(cameraObj && cameraObj.depth && obj.isAttachedToCamera)
		{
			depth += depth + cameraObj.depth;
		}
		return depth;
		}
	this.___needSorting___ = function() {
		for (var i = 0; i < this.getNumChildren() - 1; i++)
		{
			var prevDepth = this.___GetDepth___(this.getChildAt(i));
			var nextDepth = this.___GetDepth___(this.getChildAt(i + 1));
			if (prevDepth < nextDepth)
				return true;
		}
		return false;
	}
	this.___sortFunction___ = function(obj1, obj2) {
		return (this.exportRoot.___GetDepth___(obj2) - this.exportRoot.___GetDepth___(obj1));
	}
	this.on('tick', function (event){
		var curTimeline = event.currentTarget;
		if (curTimeline.___needSorting___()){
			this.sortChildren(curTimeline.___sortFunction___);
		}
	});

	// Layer_1_obj_
	this.Layer_1 = new lib.Scene_1_Layer_1();
	this.Layer_1.name = "Layer_1";
	this.Layer_1.parent = this;
	this.Layer_1.setTransform(202.6,398.1,1,1,0,0,0,202.6,398.1);
	this.Layer_1.depth = 0;
	this.Layer_1.isAttachedToCamera = 0
	this.Layer_1.isAttachedToMask = 0
	this.Layer_1.layerDepth = 0
	this.Layer_1.layerIndex = 0
	this.Layer_1.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_1).wait(1));

	// Layer_4_obj_
	this.Layer_4 = new lib.Scene_1_Layer_4();
	this.Layer_4.name = "Layer_4";
	this.Layer_4.parent = this;
	this.Layer_4.setTransform(94,381.9,1,1,0,0,0,94,381.9);
	this.Layer_4.depth = 0;
	this.Layer_4.isAttachedToCamera = 0
	this.Layer_4.isAttachedToMask = 0
	this.Layer_4.layerDepth = 0
	this.Layer_4.layerIndex = 1
	this.Layer_4.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_4).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0.5,357.6,374.7,454.5);
// library properties:
lib.properties = {
	id: '3A4C4E099F291C4FBCE56C6E2E36194C',
	width: 375,
	height: 812,
	fps: 30,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: [
		{src:"assets/images/bg_vert38-2.png", id:"bg_vert"},
		{src:"assets/images/eggs38-2.png", id:"eggs"},
		{src:"assets/images/pack38-2.png", id:"pack"}
	],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['3A4C4E099F291C4FBCE56C6E2E36194C'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}


// Layer depth API : 

AdobeAn.Layer = new function() {
	this.getLayerZDepth = function(timeline, layerName)
	{
		if(layerName === "Camera")
		layerName = "___camera___instance";
		var script = "if(timeline." + layerName + ") timeline." + layerName + ".depth; else 0;";
		return eval(script);
	}
	this.setLayerZDepth = function(timeline, layerName, zDepth)
	{
		const MAX_zDepth = 10000;
		const MIN_zDepth = -5000;
		if(zDepth > MAX_zDepth)
			zDepth = MAX_zDepth;
		else if(zDepth < MIN_zDepth)
			zDepth = MIN_zDepth;
		if(layerName === "Camera")
		layerName = "___camera___instance";
		var script = "if(timeline." + layerName + ") timeline." + layerName + ".depth = " + zDepth + ";";
		eval(script);
	}
	this.removeLayer = function(timeline, layerName)
	{
		if(layerName === "Camera")
		layerName = "___camera___instance";
		var script = "if(timeline." + layerName + ") timeline.removeChild(timeline." + layerName + ");";
		eval(script);
	}
	this.addNewLayer = function(timeline, layerName, zDepth)
	{
		if(layerName === "Camera")
		layerName = "___camera___instance";
		zDepth = typeof zDepth !== 'undefined' ? zDepth : 0;
		var layer = new createjs.MovieClip();
		layer.name = layerName;
		layer.depth = zDepth;
		layer.layerIndex = 0;
		timeline.addChild(layer);
	}
}


})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;

$(function () {
    var canvas, stage, exportRoot, anim_container, dom_overlay_container, fnStartAnimation;
    function init() {
        canvas = document.getElementById("canvas38-2");
        anim_container = document.getElementById("animation_container38-2");
        dom_overlay_container = document.getElementById("dom_overlay_container38-2");
        var comp=AdobeAn.getComposition("3A4C4E099F291C4FBCE56C6E2E36194C");
        var lib=comp.getLibrary();
        createjs.MotionGuidePlugin.install();
        var loader = new createjs.LoadQueue(false);
        loader.addEventListener("fileload", function(evt){handleFileLoad(evt,comp)});
        loader.addEventListener("complete", function(evt){handleComplete(evt,comp)});
        var lib=comp.getLibrary();
        loader.loadManifest(lib.properties.manifest);
    }
    function handleFileLoad(evt, comp) {
        var images=comp.getImages();
        if (evt && (evt.item.type == "image")) { images[evt.item.id] = evt.result; }
    }
    function handleComplete(evt,comp) {
        //This function is always called, irrespective of the content. You can use the variable "stage" after it is created in token create_stage.
        var lib=comp.getLibrary();
        var ss=comp.getSpriteSheet();
        var queue = evt.target;
        var ssMetadata = lib.ssMetadata;
        for(i=0; i<ssMetadata.length; i++) {
            ss[ssMetadata[i].name] = new createjs.SpriteSheet( {"images": [queue.getResult(ssMetadata[i].name)], "frames": ssMetadata[i].frames} )
        }
        var preloaderDiv = document.getElementById("_preload_div_");
        preloaderDiv.style.display = 'none';
        canvas.style.display = 'block';
        exportRoot = new lib.Feb_2_Vert();
        stage = new lib.Stage(canvas);
        //Registers the "tick" event listener.
        fnStartAnimation = function() {
            stage.addChild(exportRoot);
            createjs.Ticker.setFPS(lib.properties.fps);
            createjs.Ticker.addEventListener("tick", stage)
            stage.addEventListener("tick", handleTick)
            function getProjectionMatrix(container, totalDepth) {
                var focalLength = 528.25;
                var projectionCenter = { x : lib.properties.width/2, y : lib.properties.height/2 };
                var scale = (totalDepth + focalLength)/focalLength;
                var scaleMat = new createjs.Matrix2D;
                scaleMat.a = 1/scale;
                scaleMat.d = 1/scale;
                var projMat = new createjs.Matrix2D;
                projMat.tx = -projectionCenter.x;
                projMat.ty = -projectionCenter.y;
                projMat = projMat.prependMatrix(scaleMat);
                projMat.tx += projectionCenter.x;
                projMat.ty += projectionCenter.y;
                return projMat;
            }
            function handleTick(event) {
                var cameraInstance = exportRoot.___camera___instance;
                if(cameraInstance !== undefined && cameraInstance.pinToObject !== undefined)
                {
                    cameraInstance.x = cameraInstance.pinToObject.x + cameraInstance.pinToObject.pinOffsetX;
                    cameraInstance.y = cameraInstance.pinToObject.y + cameraInstance.pinToObject.pinOffsetY;
                    if(cameraInstance.pinToObject.parent !== undefined && cameraInstance.pinToObject.parent.depth !== undefined)
                        cameraInstance.depth = cameraInstance.pinToObject.parent.depth + cameraInstance.pinToObject.pinOffsetZ;
                }
                applyLayerZDepth(exportRoot);
            }
            function applyLayerZDepth(parent)
            {
                var cameraInstance = parent.___camera___instance;
                var focalLength = 528.25;
                var projectionCenter = { 'x' : 0, 'y' : 0};
                if(parent === exportRoot)
                {
                    var stageCenter = { 'x' : lib.properties.width/2, 'y' : lib.properties.height/2 };
                    projectionCenter.x = stageCenter.x;
                    projectionCenter.y = stageCenter.y;
                }
                for(child in parent.children)
                {
                    var layerObj = parent.children[child];
                    if(layerObj == cameraInstance)
                        continue;
                    applyLayerZDepth(layerObj, cameraInstance);
                    if(layerObj.layerDepth === undefined)
                        continue;
                    if(layerObj.currentFrame != layerObj.parent.currentFrame)
                    {
                        layerObj.gotoAndPlay(layerObj.parent.currentFrame);
                    }
                    var matToApply = new createjs.Matrix2D;
                    var cameraMat = new createjs.Matrix2D;
                    var totalDepth = layerObj.layerDepth ? layerObj.layerDepth : 0;
                    var cameraDepth = 0;
                    if(cameraInstance && !layerObj.isAttachedToCamera)
                    {
                        var mat = cameraInstance.getMatrix();
                        mat.tx -= projectionCenter.x;
                        mat.ty -= projectionCenter.y;
                        cameraMat = mat.invert();
                        cameraMat.prependTransform(projectionCenter.x, projectionCenter.y, 1, 1, 0, 0, 0, 0, 0);
                        cameraMat.appendTransform(-projectionCenter.x, -projectionCenter.y, 1, 1, 0, 0, 0, 0, 0);
                        if(cameraInstance.depth)
                            cameraDepth = cameraInstance.depth;
                    }
                    if(layerObj.depth)
                    {
                        totalDepth = layerObj.depth;
                    }
                    //Offset by camera depth
                    totalDepth -= cameraDepth;
                    if(totalDepth < -focalLength)
                    {
                        matToApply.a = 0;
                        matToApply.d = 0;
                    }
                    else
                    {
                        if(layerObj.layerDepth)
                        {
                            var sizeLockedMat = getProjectionMatrix(parent, layerObj.layerDepth);
                            if(sizeLockedMat)
                            {
                                sizeLockedMat.invert();
                                matToApply.prependMatrix(sizeLockedMat);
                            }
                        }
                        matToApply.prependMatrix(cameraMat);
                        var projMat = getProjectionMatrix(parent, totalDepth);
                        if(projMat)
                        {
                            matToApply.prependMatrix(projMat);
                        }
                    }
                    layerObj.transformMatrix = matToApply;
                }
            }
        }
        //Code to support hidpi screens and responsive scaling.
        function makeResponsive(isResp, respDim, isScale, scaleType) {
            var lastW, lastH, lastS=1;
            window.addEventListener('resize', resizeCanvas);
            resizeCanvas();
            function resizeCanvas() {
                var w = lib.properties.width, h = lib.properties.height;
                var iw = window.innerWidth, ih=window.innerHeight;
                var pRatio = window.devicePixelRatio || 1, xRatio=iw/w, yRatio=ih/h, sRatio=1;
                if(isResp) {
                    if((respDim=='width'&&lastW==iw) || (respDim=='height'&&lastH==ih)) {
                        sRatio = lastS;
                    }
                    else if(!isScale) {
                        if(iw<w || ih<h)
                            sRatio = Math.min(xRatio, yRatio);
                    }
                    else if(scaleType==1) {
                        sRatio = Math.min(xRatio, yRatio);
                    }
                    else if(scaleType==2) {
                        sRatio = Math.max(xRatio, yRatio);
                    }
                }
                canvas.width = w*pRatio*sRatio;
                canvas.height = h*pRatio*sRatio;
                canvas.style.width = anim_container.style.width = dom_overlay_container.style.width = preloaderDiv.style.width = w*sRatio+'px';
                canvas.style.height = anim_container.style.height = dom_overlay_container.style.height = preloaderDiv.style.height = h*sRatio+'px';
                stage.scaleX = pRatio*sRatio;
                stage.scaleY = pRatio*sRatio;
                lastW = iw; lastH = ih; lastS = sRatio;
                stage.tickOnUpdate = false;
                stage.update();
                stage.tickOnUpdate = true;
            }
        }
        makeResponsive(true,'both',true,2);
        AdobeAn.compositionLoaded(lib.properties.id);
        fnStartAnimation();
    }
    init();
});