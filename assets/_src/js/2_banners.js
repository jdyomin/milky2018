(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [];


// symbols:



(lib._2_banners_img1 = function() {
	this.initialize(img._2_banners_img1);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,1080,680);


(lib.bg = function() {
	this.initialize(img.bg);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,1080,680);


(lib.bg2_1 = function() {
	this.initialize(img.bg2_1);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,1080,680);


(lib.bg2_2 = function() {
	this.initialize(img.bg2_2);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,1079,680);


(lib.bg_pack = function() {
	this.initialize(img.bg_pack);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,1080,680);


(lib.butt = function() {
	this.initialize(img.butt);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,232,54);


(lib.butt1 = function() {
	this.initialize(img.butt1);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,215,50);


(lib.butterfly_white = function() {
	this.initialize(img.butterfly_white);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,75,68);


(lib.butterfly_yellow = function() {
	this.initialize(img.butterfly_yellow);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,172,156);


(lib.leafs = function() {
	this.initialize(img.leafs);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,635,405);


(lib.milk_part = function() {
	this.initialize(img.milk_part);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,119,36);


(lib.pack = function() {
	this.initialize(img.pack);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,334,460);


(lib.txt1 = function() {
	this.initialize(img.txt1);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,354,139);


(lib.txt2 = function() {
	this.initialize(img.txt2);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,329,136);


(lib.txts1 = function() {
	this.initialize(img.txts1);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,264,66);


(lib.txts2 = function() {
	this.initialize(img.txts2);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,280,60);// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.Symbol39 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.txt1();
	this.instance.parent = this;
	this.instance.setTransform(-177,-69.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol39, new cjs.Rectangle(-177,-69.5,354,139), null);


(lib.Symbol38 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.butt1();
	this.instance.parent = this;
	this.instance.setTransform(-107.5,-25);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol38, new cjs.Rectangle(-107.5,-25,215,50), null);


(lib.Symbol37copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#03A2E3").s().p("AgrArQgRgSAAgZQAAgZARgRQASgTAZABQAZgBASATQASARAAAZQAAAZgSASQgSASgZAAQgZAAgSgSg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol37copy, new cjs.Rectangle(-6.1,-6.1,12.3,12.3), null);


(lib.Symbol37 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgrArQgRgSAAgZQAAgZARgRQASgTAZABQAZgBASATQASARAAAZQAAAZgSASQgSASgZAAQgZAAgSgSg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol37, new cjs.Rectangle(-6.1,-6.1,12.3,12.3), null);


(lib.Symbol34 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.txts2();
	this.instance.parent = this;
	this.instance.setTransform(-140,-30);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol34, new cjs.Rectangle(-140,-30,280,60), null);


(lib.Symbol33 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.txts1();
	this.instance.parent = this;
	this.instance.setTransform(-132,-33);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol33, new cjs.Rectangle(-132,-33,264,66), null);


(lib.Symbol32 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.butt();
	this.instance.parent = this;
	this.instance.setTransform(-116,-27);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol32, new cjs.Rectangle(-116,-27,232,54), null);


(lib.Symbol31copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_4
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#03A2E3").ss(2.5,1,1).p("AAyAAQAAAVgPAOQgOAPgVAAQgUAAgPgPQgOgOAAgVQAAgUAOgOQAPgPAUAAQAVAAAOAPQAPAOAAAUg");
	this.shape.setTransform(60.025,0.025);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	// Layer_3
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#03A2E3").ss(2.5,1,1).p("AAyAAQAAAVgPAOQgOAPgVAAQgUAAgPgPQgOgOAAgVQAAgUAOgOQAPgPAUAAQAVAAAOAPQAPAOAAAUg");
	this.shape_1.setTransform(39.925,0.025);

	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(1));

	// Layer_2
	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#03A2E3").ss(2.5,1,1).p("AAyAAQAAAVgPAOQgOAPgVAAQgUAAgPgPQgOgOAAgVQAAgUAOgOQAPgPAUAAQAVAAAOAPQAPAOAAAUg");
	this.shape_2.setTransform(19.975,0.025);

	this.timeline.addTween(cjs.Tween.get(this.shape_2).wait(1));

	// Layer_1
	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#03A2E3").ss(2.5,1,1).p("AAyAAQAAAVgPAOQgOAPgVAAQgUAAgPgPQgOgOAAgVQAAgUAOgOQAPgPAUAAQAVAAAOAPQAPAOAAAUg");
	this.shape_3.setTransform(0.025,0.025);

	this.timeline.addTween(cjs.Tween.get(this.shape_3).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol31copy, new cjs.Rectangle(-6.2,-6.2,72.5,12.5), null);


(lib.Symbol31 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_4
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#FFFFFF").ss(2.5,1,1).p("AAyAAQAAAVgPAOQgOAPgVAAQgUAAgPgPQgOgOAAgVQAAgUAOgOQAPgPAUAAQAVAAAOAPQAPAOAAAUg");
	this.shape.setTransform(60.025,0.025);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	// Layer_3
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#FFFFFF").ss(2.5,1,1).p("AAyAAQAAAVgPAOQgOAPgVAAQgUAAgPgPQgOgOAAgVQAAgUAOgOQAPgPAUAAQAVAAAOAPQAPAOAAAUg");
	this.shape_1.setTransform(39.925,0.025);

	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(1));

	// Layer_2
	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#FFFFFF").ss(2.5,1,1).p("AAyAAQAAAVgPAOQgOAPgVAAQgUAAgPgPQgOgOAAgVQAAgUAOgOQAPgPAUAAQAVAAAOAPQAPAOAAAUg");
	this.shape_2.setTransform(19.975,0.025);

	this.timeline.addTween(cjs.Tween.get(this.shape_2).wait(1));

	// Layer_1
	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#FFFFFF").ss(2.5,1,1).p("AAyAAQAAAVgPAOQgOAPgVAAQgUAAgPgPQgOgOAAgVQAAgUAOgOQAPgPAUAAQAVAAAOAPQAPAOAAAUg");
	this.shape_3.setTransform(0.025,0.025);

	this.timeline.addTween(cjs.Tween.get(this.shape_3).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol31, new cjs.Rectangle(-6.2,-6.2,72.5,12.5), null);


(lib.Symbol30 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib._2_banners_img1();
	this.instance.parent = this;
	this.instance.setTransform(-540,-340);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol30, new cjs.Rectangle(-540,-340,1080,680), null);


(lib.Symbol29 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.bg2_2();
	this.instance.parent = this;
	this.instance.setTransform(-539,-340);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol29, new cjs.Rectangle(-539,-340,1079,680), null);


(lib.Symbol28 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.bg2_1();
	this.instance.parent = this;
	this.instance.setTransform(-540,-340);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol28, new cjs.Rectangle(-540,-340,1080,680), null);


(lib.Symbol27 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.leafs();
	this.instance.parent = this;
	this.instance.setTransform(-317.5,-202.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol27, new cjs.Rectangle(-317.5,-202.5,635,405), null);


(lib.Symbol26 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.bg_pack();
	this.instance.parent = this;
	this.instance.setTransform(-540,-340);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol26, new cjs.Rectangle(-540,-340,1080,680), null);


(lib.Symbol18 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("AiomgIE8A6IAJIIIAMCwIkoBPg");
	mask.setTransform(-19.925,-1.9);

	// Layer_1
	this.instance = new lib.butterfly_white();
	this.instance.parent = this;
	this.instance.setTransform(-15.45,-48.2,1,1,29.9992);

	var maskedShapeInstanceList = [this.instance];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol18, new cjs.Rectangle(-36.8,-43.5,33.8,83.3), null);


(lib.Symbol17 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("Aj2FHIAAjRIAYgwIADgkIgNggIgJgtIHolFIgHLig");
	mask.setTransform(23.675,7.95);

	// Layer_1
	this.instance = new lib.butterfly_white();
	this.instance.parent = this;
	this.instance.setTransform(-15.45,-48.2,1,1,29.9992);

	var maskedShapeInstanceList = [this.instance];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol17, new cjs.Rectangle(-1,-29,49.4,73.9), null);


(lib.Symbol16 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("AgpgXIgshZIAHgLIAtABIA1A4IAJBCIA5BqIgdASg");
	mask.setTransform(6.775,17.225);

	// Layer_1
	this.instance = new lib.butterfly_white();
	this.instance.parent = this;
	this.instance.setTransform(-37.5,-34);

	var maskedShapeInstanceList = [this.instance];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol16, new cjs.Rectangle(-1.8,4.9,17.2,24.700000000000003), null);


(lib.Symbol14 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.butterfly_yellow();
	this.instance.parent = this;
	this.instance.setTransform(-86,-78);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol14, new cjs.Rectangle(-86,-78,172,156), null);


(lib.Symbol7 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_4
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(11,1,1).p("ABvgKIjdAV");
	this.shape.setTransform(-36.825,-30);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	// Layer_3
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(11,1,1).p("ABlAMIjJgX");
	this.shape_1.setTransform(-35.425,-38.525);

	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(1));

	// Layer_2
	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(11,1,1).p("AgIhVQAUBLgEBh");
	this.shape_2.setTransform(-24.9446,-26.1);

	this.timeline.addTween(cjs.Tween.get(this.shape_2).wait(1));

	// Layer_1
	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(11,1,1).p("AlCHQQERhTC2jKQCkizAXi7QALhYgchAQgehEhDgfQiZhIkjCG");
	this.shape_3.setTransform(0.0051,0.0061);

	this.timeline.addTween(cjs.Tween.get(this.shape_3).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol7, new cjs.Rectangle(-53.4,-51.9,91.19999999999999,103.8), null);


(lib.Symbol6 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#9AADA9").s().p("AiMCNQg7g7AAhSQAAhRA7g7QA6g7BSAAQBSAAA7A7QA7A7AABRQAABSg7A7Qg7A7hSAAQhSAAg6g7g");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol6, new cjs.Rectangle(-19.9,-19.9,39.9,39.9), null);


(lib.Symbol4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AiMCNQg7g7AAhSQAAhRA7g7QA6g7BSAAQBSAAA7A7QA7A7AABRQAABSg7A7Qg7A7hSAAQhSAAg6g7g");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol4, new cjs.Rectangle(-19.9,-19.9,39.9,39.9), null);


(lib.Symbol2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AgvAwQgUgUAAgcQAAgbAUgUQAUgTAbgBQAcABAUATQATAUABAbQgBAcgTAUQgUATgcABQgbgBgUgTg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol2, new cjs.Rectangle(-6.7,-6.7,13.5,13.5), null);


(lib.Symbol_35_Layer_2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.instance = new lib.txt2();
	this.instance.parent = this;
	this.instance.setTransform(-166,-67);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol_35_Layer_2, null, null);


(lib.Symbol_13_Layer_3copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_3
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#1A1714").ss(11,1,1).p("AkegHQA9BiBmAZQBYAXBjgkQBbggBBhCQBBhBAChA");
	this.shape.setTransform(48.225,37.4114);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#1A1714").ss(11,1,1).p("AkoAIQBGBVBjAVQBYASBigiQBcgfBCg8QBCg7AOhD");
	this.shape_1.setTransform(48.425,36.6396);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#1A1714").ss(11,1,1).p("AkxAWQBOBJBhAQQBYAOBhggQBcgfBDg2QBDg1AZhG");
	this.shape_2.setTransform(48.625,35.9608);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#1A1714").ss(11,1,1).p("Ak4AiQBVA/BeAMQBYALBggfQBdgeBEgyQBEgwAhhI");
	this.shape_3.setTransform(48.775,35.3924);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#1A1714").ss(11,1,1).p("Ak/AsQBcA2BcAJQBYAIBggeQBdgeBEgtQBFgtAohJ");
	this.shape_4.setTransform(48.9,34.9081);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#1A1714").ss(11,1,1).p("AlEA1QBgAuBbAHQBYAFBfgdQBdgdBFgqQBFgqAwhL");
	this.shape_5.setTransform(49.025,34.5182);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#1A1714").ss(11,1,1).p("AlIA7QBkApBaAEQBYAEBegdQBegdBFgnQBGgnA0hM");
	this.shape_6.setTransform(49.075,34.2242);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#1A1714").ss(11,1,1).p("AlLA/QBmAlBaADQBYADBegdQBegcBFgmQBGglA4hN");
	this.shape_7.setTransform(49.15,34.0101);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#1A1714").ss(11,1,1).p("AlMBCQBnAjBYACQBZACBegcQBegdBGgkQBGgkA6hO");
	this.shape_8.setTransform(49.2,33.8817);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#1A1714").ss(11,1,1).p("AlNBDQBoAiBYABQBZACBdgcQBfgcBGglQBGgjA6hN");
	this.shape_9.setTransform(49.2,33.8423);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#1A1714").ss(11,1,1).p("AlDA0QBfAvBbAHQBYAGBfgeQBegdBEgqQBGgqAuhL");
	this.shape_10.setTransform(49,34.55);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#1A1714").ss(11,1,1).p("Ak7AmQBYA8BeALQBYAKBggfQBdgeBEgwQBEgvAkhJ");
	this.shape_11.setTransform(48.8,35.197);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#1A1714").ss(11,1,1).p("AkzAaQBQBGBgAPQBYANBhggQBcgfBDg0QBEg0AbhH");
	this.shape_12.setTransform(48.65,35.7628);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#1A1714").ss(11,1,1).p("AktAQQBLBOBhASQBZAQBhghQBcgfBCg4QBDg4AUhF");
	this.shape_13.setTransform(48.525,36.2612);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#1A1714").ss(11,1,1).p("AkoAHQBGBWBjAVQBZASBigiQBbgfBCg8QBDg7ANhD");
	this.shape_14.setTransform(48.4,36.676);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#1A1714").ss(11,1,1).p("AkjABQBCBbBkAXQBYAUBigiQBcggBBg/QBCg9AJhC");
	this.shape_15.setTransform(48.35,36.992);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#1A1714").ss(11,1,1).p("AkhgDQBABeBlAZQBYAVBjgjQBbggBBhAQBChAAFhB");
	this.shape_16.setTransform(48.275,37.2341);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#1A1714").ss(11,1,1).p("AkfgGQA+BhBmAZQBYAWBjgjQBbggBBhCQBBhAADhB");
	this.shape_17.setTransform(48.225,37.3709);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},34).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_9}]},17).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape}]},1).wait(6));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_13_Layer_3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_3
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(12,1,1).p("AAbBlQA9h0iFhV");
	this.shape.setTransform(10.3136,-12.05);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(12,1,1).p("AgzhhQCYBMhJB3");
	this.shape_1.setTransform(10.1681,-11.825);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(12,1,1).p("Ag4hfQCpBEhTB7");
	this.shape_2.setTransform(10.0225,-11.6);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(12,1,1).p("Ag+hdQC8A8heB/");
	this.shape_3.setTransform(9.8389,-11.4);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(12,1,1).p("AAhBcQBpiDjNg0");
	this.shape_4.setTransform(9.6895,-11.175);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(12,1,1).p("AhAhSQDAAmheB/");
	this.shape_5.setTransform(9.5617,-10.3);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(12,1,1).p("Ag9hJQC0AYhTB7");
	this.shape_6.setTransform(9.443,-9.4);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(12,1,1).p("Ag6hAQCnAKhIB3");
	this.shape_7.setTransform(9.3178,-8.525);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(12,1,1).p("AAmA5QA9h0iZAE");
	this.shape_8.setTransform(9.1973,-7.6544);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(12,1,1).p("Ag1hAQCWAOg9Bz");
	this.shape_9.setTransform(9.4149,-8.525);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(12,1,1).p("AgzhJQCSAgg9Bz");
	this.shape_10.setTransform(9.632,-9.4);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(12,1,1).p("AgxhSQCOAyg+Bz");
	this.shape_11.setTransform(9.877,-10.3);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(12,1,1).p("AgvhbQCKBEg+Bz");
	this.shape_12.setTransform(10.0956,-11.175);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},59).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape}]},1).wait(3));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_13_Layer_2copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#1A1714").ss(11,1,1).p("AjDBtQgHhPA2g9QAyg4BLgQQBMgQA7AkQBDAoARBa");
	this.shape.setTransform(73.2598,-14.1712);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#1A1714").ss(11,1,1).p("AjDBvQgHhPA1g/QAxg7BMgPQBMgPA8AlQBDApARBa");
	this.shape_1.setTransform(73.2592,-14.3143);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#1A1714").ss(11,1,1).p("AjDBzQgHhPAyhFQAwhDBNgMQBMgMA/AqQBDAtARBa");
	this.shape_2.setTransform(73.257,-14.792);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#1A1714").ss(11,1,1).p("AjDB8QgGhPAthPQAthQBOgIQBOgHBDAyQBDAyARBa");
	this.shape_3.setTransform(73.2536,-15.6243);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#1A1714").ss(11,1,1).p("AjDB4QgHhPAwhLQAuhKBOgJQBNgKBBAvQBDAvARBa");
	this.shape_4.setTransform(73.255,-15.2377);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#1A1714").ss(11,1,1).p("AjDB0QgHhPAyhGQAvhEBNgMQBNgLA/ArQBDAtARBa");
	this.shape_5.setTransform(73.257,-14.8713);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#1A1714").ss(11,1,1).p("AjDBxQgHhPA0hCQAxg+BMgOQBMgOA9AoQBDAqARBa");
	this.shape_6.setTransform(73.2585,-14.5082);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#1A1714").ss(11,1,1).p("AjDB1QgHhPAxhHQAvhGBOgLQBMgLBAAsQBDAuARBa");
	this.shape_7.setTransform(73.2563,-14.9681);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#1A1714").ss(11,1,1).p("AjDBuQgHhPA2g+QAxg6BMgPQBLgQA8AlQBDApARBa");
	this.shape_8.setTransform(73.2595,-14.2538);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},34).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape}]},1).wait(27));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_13_Layer_2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(12,1,1).p("AB+A0QhMi0ivCG");
	this.shape.setTransform(-3.5,-10.3147);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(12,1,1).p("Ah/gKQC4h3BHC6");
	this.shape_1.setTransform(-3.725,-10.8851);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(12,1,1).p("AiBgbQDBhnBCDC");
	this.shape_2.setTransform(-3.975,-11.5193);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(12,1,1).p("AiEgrQDLhXA+DJ");
	this.shape_3.setTransform(-4.2,-12.2095);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(12,1,1).p("ACHBOQg5jQjUBH");
	this.shape_4.setTransform(-4.45,-12.9575);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(12,1,1).p("Ah3ghQCyhlA9DJ");
	this.shape_5.setTransform(-2.95,-11.8945);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(12,1,1).p("AhogGQCPiCBCDD");
	this.shape_6.setTransform(-1.45,-11);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(12,1,1).p("AhZAWQBsieBHC7");
	this.shape_7.setTransform(0.05,-10.2231);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(12,1,1).p("ABLAoQhLizhKC7");
	this.shape_8.setTransform(1.55,-9.1661);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(12,1,1).p("AhUArQBeixBLCz");
	this.shape_9.setTransform(0.55,-9.694);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(12,1,1).p("AheAhQByimBLC0");
	this.shape_10.setTransform(-0.475,-9.8378);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(12,1,1).p("AhoAYQCGibBLCz");
	this.shape_11.setTransform(-1.475,-9.9839);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(12,1,1).p("AhzAPQCciRBLC0");
	this.shape_12.setTransform(-2.5,-10.147);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},59).to({state:[{t:this.shape}]},2).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape}]},1).wait(1));
	this.timeline.addTween(cjs.Tween.get(this.shape).wait(61).to({_off:true},1).wait(12).to({_off:false},0).wait(1));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_13_Layer_1copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#1A1714").ss(11,1,1).p("AjDBtQgHhPA2g9QAyg4BLgQQBMgQA7AkQBDAoARBa");
	this.shape.setTransform(0.0098,0.0288);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#1A1714").ss(11,1,1).p("AjDBvQgHhPA2g/QAyg7BLgQQBMgPA8AmQBCAqARBa");
	this.shape_1.setTransform(0.0098,-0.1623);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#1A1714").ss(11,1,1).p("AjDB1QgHhPA1hGQAzhEBLgNQBMgNA9AsQBBAuARBa");
	this.shape_2.setTransform(0.0092,-0.7232);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#1A1714").ss(11,1,1).p("AjDB/QgHhPA0hRQA0hTBMgIQBLgJA/A1QA/A2ARBa");
	this.shape_3.setTransform(0.0083,-1.7129);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#1A1714").ss(11,1,1).p("AjDB6QgHhPA1hMQAzhLBMgLQBLgLA+AyQBAAyARBa");
	this.shape_4.setTransform(0.0088,-1.2669);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#1A1714").ss(11,1,1).p("AjDB2QgHhPA1hHQAzhFBMgNQBLgMA9AtQBBAuARBa");
	this.shape_5.setTransform(0.0092,-0.8109);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#1A1714").ss(11,1,1).p("AjDBxQgHhPA2hCQAyg/BLgOQBMgOA8AoQBCAsARBa");
	this.shape_6.setTransform(0.0095,-0.3911);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#1A1714").ss(11,1,1).p("AjDB3QgHhPA1hIQAzhHBMgMQBLgMA9AuQBBAvARBa");
	this.shape_7.setTransform(0.0092,-0.9241);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#1A1714").ss(11,1,1).p("AjDBuQgHhPA2g+QAyg6BLgQQBMgPA7AlQBDApARBa");
	this.shape_8.setTransform(0.0098,-0.0788);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},34).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape}]},1).wait(27));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_13_Layer_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(12,1,1).p("AB6gYQhzhiiAC1");
	this.shape.setTransform(0,0.0069);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(12,1,1).p("Ah9AtQCJiXByBo");
	this.shape_1.setTransform(-0.4,-2.1746);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(12,1,1).p("AiBAhQCRh7ByBu");
	this.shape_2.setTransform(-0.775,-4.4215);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(12,1,1).p("AiFAKQCaheBxB1");
	this.shape_3.setTransform(-1.175,-5.7109);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(12,1,1).p("ACKApQhwh8ijBD");
	this.shape_4.setTransform(-1.575,-6.504);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(12,1,1).p("Ah1AEQB6hXBxB1");
	this.shape_5.setTransform(0.45,-5.8296);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(12,1,1).p("AhgAaQBQhsBxBu");
	this.shape_6.setTransform(2.5,-5.2502);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(12,1,1).p("AhMAkQAoiBBxBn");
	this.shape_7.setTransform(4.525,-3.4735);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(12,1,1).p("AA5gHQhyhiACCX");
	this.shape_8.setTransform(6.5483,-1.6708);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(12,1,1).p("AhFAxQAYidBzBi");
	this.shape_9.setTransform(5.25,-1.3258);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(12,1,1).p("AhSAzQAzijByBi");
	this.shape_10.setTransform(3.925,-0.9828);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(12,1,1).p("AhfA2QBNipByBi");
	this.shape_11.setTransform(2.625,-0.6701);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(12,1,1).p("AhsA4QBniuByBi");
	this.shape_12.setTransform(1.3,-0.3308);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},59).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape}]},1).wait(3));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_8_Symbol_7 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Symbol_7
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(11.5,1,1).p("AF2HYQkRgXitjkQihjTiMnh");
	this.shape.setTransform(-121.775,-78.825);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(11.5,1,1).p("AmXmfQC+HaC5CwQDDC9D1gI");
	this.shape_1.setTransform(-125.45,-72.4568);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(11.5,1,1).p("Am2luQDsHUDPCOQDYCZDagl");
	this.shape_2.setTransform(-128.875,-66.1819);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(11.5,1,1).p("AnTlFQEXHODjBwQDrB3DChA");
	this.shape_3.setTransform(-132.05,-59.9695);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(11.5,1,1).p("AnukkQE9HKD3BTQD9BYCrhZ");
	this.shape_4.setTransform(-134.95,-53.8187);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(11.5,1,1).p("AoGkJQFhHFEIA5QENA8CXhv");
	this.shape_5.setTransform(-137.6,-47.836);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(11.5,1,1).p("Aocj0QGCHAEYAiQEaAjCFiE");
	this.shape_6.setTransform(-140.025,-42.0749);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(11.5,1,1).p("AowjlQGfG9EmANQEnAMB1iW");
	this.shape_7.setTransform(-142.175,-36.6263);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(11.5,1,1).p("ApBi/QG0FzEgAMQEhALCOiF");
	this.shape_8.setTransform(-144.075,-34.1542);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(11.5,1,1).p("ApQifQHGE0EbAKQEbALClh2");
	this.shape_9.setTransform(-145.725,-32.0283);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(11.5,1,1).p("ApdiEQHWD+EWAKQEXAKC4hp");
	this.shape_10.setTransform(-147.15,-30.2067);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(11.5,1,1).p("ApohuQHjDSETAKQETAJDIhf");
	this.shape_11.setTransform(-148.25,-28.7318);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(11.5,1,1).p("ApvhcQHsCvEQAJQEQAJDUhX");
	this.shape_12.setTransform(-149.15,-27.5828);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(11.5,1,1).p("Ap1hQQH0CXENAJQEOAJDdhR");
	this.shape_13.setTransform(-149.8,-26.7596);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(11.5,1,1).p("Ap5hIQH4CIEMAJQENAIDihO");
	this.shape_14.setTransform(-150.175,-26.2582);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(11.5,1,1).p("AJ7ACQjjBNkNgJQkMgJn5iD");
	this.shape_15.setTransform(-150.3,-26.1059);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(11.5,1,1).p("Ap5hJQH2CJENAJQEOAJDihO");
	this.shape_16.setTransform(-150.15,-26.3782);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(11.5,1,1).p("Ap0hRQHqCXEQALQERAKDehQ");
	this.shape_17.setTransform(-149.65,-27.1641);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#000000").ss(11.5,1,1).p("ApsheQHWCvEWANQEWANDYhU");
	this.shape_18.setTransform(-148.8,-28.5124);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#000000").ss(11.5,1,1).p("ApihxQG7DREdAQQEeAQDPhZ");
	this.shape_19.setTransform(-147.625,-30.371);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#000000").ss(11.5,1,1).p("ApUiKQGYD9EnAUQEnAVDDhg");
	this.shape_20.setTransform(-146.15,-32.7615);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#000000").ss(11.5,1,1).p("ApEioQFuEyEzAaQEzAaC1ho");
	this.shape_21.setTransform(-144.35,-35.682);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#000000").ss(11.5,1,1).p("AowjMQE7FxFBAhQFBAgCkhy");
	this.shape_22.setTransform(-142.175,-39.1141);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#000000").ss(11.5,1,1).p("An3kOQEGGTEPBXQEVBcDFhI");
	this.shape_23.setTransform(-135.925,-52.7572);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#000000").ss(11.5,1,1).p("AnIlQQDaGvDpCDQDuCODggn");
	this.shape_24.setTransform(-130.85,-62.7633);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f().s("#000000").ss(11.5,1,1).p("AmkmJQC4HEDJCnQDSCzD2gL");
	this.shape_25.setTransform(-126.875,-69.9654);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f().s("#000000").ss(11.5,1,1).p("AmKm0QCgHUCzC/QC9DPEFAH");
	this.shape_26.setTransform(-124.05,-74.9);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f().s("#000000").ss(11.5,1,1).p("Al7nOQCSHeCmDOQCxDfEOAS");
	this.shape_27.setTransform(-122.35,-77.85);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},17).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_15}]},32).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_27}]},1).to({state:[{t:this.shape}]},1).wait(8));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_8_Symbol_6 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Symbol_6
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(11.5,1,1).p("AkOoOQEwC0CHCSQCECOgQB9QgPB3iXB3QiFBqkPB0");
	this.shape.setTransform(91.3917,5.575);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(11.5,1,1).p("AksIUQEShpCNhjQCbhtAZh3QAbh8hyiZQh2ickfjG");
	this.shape_1.setTransform(83.5576,6.45);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(11.5,1,1).p("Ak7IZQEVhgCThaQCghjAkh3QAmh8hiiiQhkinkOjY");
	this.shape_2.setTransform(75.8708,7.35);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(11.5,1,1).p("AlLIdQEXhVCbhTQCkhYAvh4QAwh6hQitQhSiwj9jr");
	this.shape_3.setTransform(68.2848,8.25);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(11.5,1,1).p("AlcIiQEahLCihKQCohPA6h4QA6h5g/i4QhAi6jtj8");
	this.shape_4.setTransform(60.7901,9.125);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(11.5,1,1).p("AlvInQEchBCphDQCthFBFh3QBEh5gujCQgujEjckO");
	this.shape_5.setTransform(53.499,10.025);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(11.5,1,1).p("ACWorQDLEhAdDOQAcDMhPB4QhPB4ixA6QiwA7kgA3");
	this.shape_6.setTransform(46.3517,10.9);
	this.shape_6._off = true;

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(11.5,1,1).p("Al6InQEagnCuhAQCuhBBMh6QBLh6gjjLQgkjNjVkZ");
	this.shape_7.setTransform(49.4068,10.425);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(11.5,1,1).p("AlwIjQEVgXCrhGQCthHBHh8QBHh8gqjKQgqjMjekT");
	this.shape_8.setTransform(52.5055,9.9);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(11.5,1,1).p("AA6odQDnEMAxDKQAxDIhDB/QhDB/iqBNQiqBLkPAH");
	this.shape_9.setTransform(55.6183,9.425);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(11.5,1,1).p("AlwICQEJALCwhBQCxhCBKh3QBKh4gwjIQgxjLjokL");
	this.shape_10.setTransform(54.4601,6.8546);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(11.5,1,1).p("Al6HjQECAdC3g2QC4g4BShwQBRhxgxjIQgxjKjnkM");
	this.shape_11.setTransform(53.3332,4.6036);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(11.5,1,1).p("ABcnZQDoEMAxDKQAwDIhYBqQhYBpjAAsQi+Asj8gu");
	this.shape_12.setTransform(52.1837,2.6374);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(11.5,1,1).p("AmQHZQEUAgC7gsQC8gtBYhsQBYhsgljKQgljMjXkY");
	this.shape_13.setTransform(47.5726,3.9237);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(11.5,1,1).p("AmcHwQEsASC4gtQC4guBYhuQBYhvgZjMQgZjOjGkk");
	this.shape_14.setTransform(43.0265,5.295);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(11.5,1,1).p("ADloEQC2EwANDQQANDNhYByQhYBxi0AuQi1AtlEgD");
	this.shape_15.setTransform(38.5398,6.8594);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(11.5,1,1).p("AmcISQE4gQCzgyQCzgzBVhzQBVh0gSjNQgSjPi9kr");
	this.shape_16.setTransform(41.1153,8.2);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(11.5,1,1).p("AmQIfQEsgkCxg2QCyg2BTh2QBSh1gYjNQgXjPjEkm");
	this.shape_17.setTransform(43.7367,9.55);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#000000").ss(11.5,1,1).p("AlgIjQEbhJCjhKQCphNA8h3QA7h6g8i5Qg9i8jqj/");
	this.shape_18.setTransform(59.5563,9.275);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#000000").ss(11.5,1,1).p("AlFIcQEWhZCYhVQCjhdArh3QAth7hWipQhYitkDjk");
	this.shape_19.setTransform(70.7837,7.95);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#000000").ss(11.5,1,1).p("AkzIXQEThlCQheQCdhoAfh3QAhh8hrieQhsihkXjQ");
	this.shape_20.setTransform(79.7049,6.9);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#000000").ss(11.5,1,1).p("AknITQERhuCKhlQCahwAWh3QAXh8h4iWQh8iYkkjB");
	this.shape_21.setTransform(86.1668,6.15);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#000000").ss(11.5,1,1).p("AkgIQQEQhyCHhpQCXh2ARh2QASh9iBiQQiFiUkti3");
	this.shape_22.setTransform(90.0852,5.725);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},32).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_6}]},2).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_6}]},13).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape}]},1).wait(2));
	this.timeline.addTween(cjs.Tween.get(this.shape_6).wait(38).to({_off:false},0).wait(2).to({_off:true},1).wait(11).to({_off:false},0).to({_off:true},1).wait(11).to({_off:false},0).wait(13).to({_off:true},1).wait(7));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_8_pack_png = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// pack_png
	this.instance = new lib.pack();
	this.instance.parent = this;
	this.instance.setTransform(-161.9,-220.8);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(85));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_5_Layer_4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_4
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(11,1,1).p("AG8nPQCMEGhVDlQhJDGjYB9QjKB1jWgEQjggEhkiL");
	this.shape.setTransform(-2.5577,-22.5667);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(11,1,1).p("AoQF1QB5BzDegKQDWgJDHh/QDRiFBBjIQBOjoiWj/");
	this.shape_1.setTransform(-0.977,-20.5388);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(11,1,1).p("AoOGgQCLBfDcgVQDWgVDEiHQDLiMA7jJQBHjqiej7");
	this.shape_2.setTransform(0.365,-18.7091);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(11,1,1).p("AoNHCQCaBODbgeQDVgfDCiNQDGiRA3jKQBAjtikj3");
	this.shape_3.setTransform(1.5002,-17.1213);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(11,1,1).p("AoMHcQCmBBDZgmQDWgmDAiTQDCiUAzjMQA7juioj0");
	this.shape_4.setTransform(2.3781,-15.774);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(11,1,1).p("AoMHuQCvA4DYgrQDVgsC/iWQDAiYAwjMQA4jvirjy");
	this.shape_5.setTransform(3.0251,-14.7662);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(11,1,1).p("AoMH4QC0AzDXgvQDWguC+iZQC+iaAujMQA3jwiujx");
	this.shape_6.setTransform(3.4014,-14.1354);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(11,1,1).p("AGHoTQCvDwg2DwQguDNi+CZQi+CajVAwQjXAvi1gw");
	this.shape_7.setTransform(3.5381,-13.9206);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(11,1,1).p("AoNHGQCdBMDZgfQDWggDCiOQDFiSA2jKQBAjtikj3");
	this.shape_8.setTransform(1.6362,-16.9135);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(11,1,1).p("AoPGXQCIBjDcgTQDWgSDFiGQDMiKA9jIQBHjribj8");
	this.shape_9.setTransform(0.101,-19.097);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(11,1,1).p("AoQFyQB4B1DegJQDWgJDHh+QDRiFBCjIQBOjniVkA");
	this.shape_10.setTransform(-1.0741,-20.6677);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(11,1,1).p("AoRFXQBtCBDfgCQDWgBDIh5QDWiBBFjHQBSjmiQkD");
	this.shape_11.setTransform(-1.9124,-21.7485);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(11,1,1).p("AoSFGQBmCJDgACQDWADDJh2QDYh+BIjGQBUjliNkF");
	this.shape_12.setTransform(-2.3953,-22.3713);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_7}]},46).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape}]},1).wait(16));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_3_Layer_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(12,1,1).p("AHXG3QhEAAitgrQjmg4ilhYQjdh1g+iYQhLi9Ctjo");
	this.shape.setTransform(0.0054,0.025);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(12,1,1).p("Aljm2QioDoBMC8QA+CZDdB2QCkBXDnA5QCtAqBEAA");
	this.shape_1.setTransform(-0.0562,0);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(12,1,1).p("Alzm3QiZDqBNC7QBACaDdB1QCkBYDnA4QCtArBEAA");
	this.shape_2.setTransform(-0.2528,-0.05);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(12,1,1).p("AmOm3Qh/DsBOC3QBDCcDdB2QClBXDmA5QCtAqBFAA");
	this.shape_3.setTransform(-0.6125,-0.125);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(12,1,1).p("Amzm5QhaDvBQC0QBHCgDdB1QClBYDmA4QCtArBFAA");
	this.shape_4.setTransform(-1.2199,-0.25);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(12,1,1).p("Anfm6QgrDzBSCvQBNCjDdB2QClBXDmA5QCtAqBFAA");
	this.shape_5.setTransform(-2.3283,-0.4);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(12,1,1).p("AIJG9QhEAAitgrQjng4ilhYQjch1hVioQhVipgOj4");
	this.shape_6.setTransform(-4.95,-0.575);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(12,1,1).p("Alim2QipDoBLC8QA+CZDdB1QClBYDmA4QCtArBFAA");
	this.shape_7.setTransform(-0.0383,0.025);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(12,1,1).p("Alum2QieDpBMC7QBACZDdB2QCkBXDnA5QCtAqBEAA");
	this.shape_8.setTransform(-0.1842,-0.025);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(12,1,1).p("AmCm3QiLDrBOC5QBBCbDdB1QClBYDmA4QCtArBFAA");
	this.shape_9.setTransform(-0.4371,-0.075);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(12,1,1).p("Amdm4QhwDtBOC3QBFCdDdB1QClBYDmA4QCtArBFAA");
	this.shape_10.setTransform(-0.8424,-0.175);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(12,1,1).p("Am/m5QhODwBRCzQBJCgDdB1QCkBYDnA4QCtArBEAA");
	this.shape_11.setTransform(-1.4692,-0.275);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(12,1,1).p("Anlm6QgkDzBSCuQBPCkDdB2QCkBXDnA5QCtAqBEAA");
	this.shape_12.setTransform(-2.5613,-0.425);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(12,1,1).p("Anqm6QgeDzBTCuQBPCkDdB2QCkBXDnA5QCtAqBEAA");
	this.shape_13.setTransform(-2.7635,-0.425);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(12,1,1).p("AnIm5QhEDwBQCyQBLChDdB2QCkBXDnA5QCtAqBEAA");
	this.shape_14.setTransform(-1.6819,-0.3);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(12,1,1).p("Ampm4QhkDuBPC1QBGCeDdB2QClBXDmA4QCtArBFAA");
	this.shape_15.setTransform(-1.0433,-0.2);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(12,1,1).p("Al5m3QiTDqBNC7QBACaDdB1QClBYDmA4QCtArBFAA");
	this.shape_16.setTransform(-0.3234,-0.05);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(12,1,1).p("Alqm2QiiDoBMC8QA/CZDdB2QClBXDmA5QCtAqBFAA");
	this.shape_17.setTransform(-0.1384,0);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#000000").ss(12,1,1).p("Alhm2QiqDoBLC9QA+CYDdB1QClBYDmA4QCtArBFAA");
	this.shape_18.setTransform(-0.0336,0.025);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},19).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape}]},1).wait(29));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_1_Layer_6 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_6
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(11,1,1).p("AjehRQAHBpBMA6QBEA0BZgJQBagJA6hBQBAhHgIhp");
	this.shape.setTransform(33.0194,54.7484);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(11,1,1).p("AjehLQAHBpBUA1QBMAwBWgOQBWgOA0g8QA6hBgEhh");
	this.shape_1.setTransform(32.988,54.1331);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(11,1,1).p("AjehGQAHBpBaAwQBVAsBSgSQBSgRAwg5QAzg8AAhZ");
	this.shape_2.setTransform(32.9752,53.6113);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(11,1,1).p("AjehBQAHBoBgAsQBcAqBOgWQBPgVAsg1QAug4ADhS");
	this.shape_3.setTransform(32.975,53.1919);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(11,1,1).p("Ajeg+QAHBpBlAoQBiAnBLgZQBNgYAngzQAqgzAGhN");
	this.shape_4.setTransform(32.975,52.8264);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(11,1,1).p("Ajeg7QAHBpBpAlQBnAkBJgbQBKgbAlgwQAmgwAIhI");
	this.shape_5.setTransform(32.975,52.5286);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(11,1,1).p("Ajeg4QAHBoBtAjQBqAiBIgdQBIgdAiguQAjguAKhD");
	this.shape_6.setTransform(32.975,52.2852);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(11,1,1).p("Ajeg3QAHBpBvAhQBuAgBHgfQBGgfAggsQAhgrALhB");
	this.shape_7.setTransform(32.975,52.1067);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(11,1,1).p("Ajeg1QAHBoBxAgQBwAfBGggQBFggAfgrQAfgqAMg+");
	this.shape_8.setTransform(32.975,51.9813);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(11,1,1).p("Ajeg1QAHBpByAfQBxAeBGghQBEggAegrQAegpANg9");
	this.shape_9.setTransform(32.975,51.9075);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(11,1,1).p("Ajeg0QAHBoBzAfQBxAeBFghQBFghAdgqQAegpANg8");
	this.shape_10.setTransform(32.975,51.8813);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(11,1,1).p("Ajeg5QAHBoBrAkQBqAjBIgdQBJgcAjgvQAkguAJhF");
	this.shape_11.setTransform(32.975,52.3696);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(11,1,1).p("AjehCQAHBpBfAtQBbApBPgVQBPgVAsg2QAvg4ADhT");
	this.shape_12.setTransform(32.975,53.2306);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(11,1,1).p("AjehJQAHBoBWA0QBQAuBUgPQBUgPAzg7QA4g/gDhe");
	this.shape_13.setTransform(32.9827,53.9579);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(11,1,1).p("AjehMQAHBpBSA2QBMAwBWgNQBWgNA2g9QA6hCgEhi");
	this.shape_14.setTransform(32.9911,54.2374);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(11,1,1).p("AjehOQAHBoBPA4QBIAyBYgLQBYgLA3g/QA9hEgGhl");
	this.shape_15.setTransform(33.0019,54.4575);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(11,1,1).p("AjehQQAHBpBNA5QBGAzBZgKQBZgKA5hAQA+hGgHhn");
	this.shape_16.setTransform(33.0103,54.6146);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(11,1,1).p("AjehRQAHBpBMA6QBFA0BZgKQBagJA5hBQBAhGgIhp");
	this.shape_17.setTransform(33.0194,54.7234);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},14).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_10}]},30).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape}]},1).wait(11));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_1_Layer_3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_3
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(11.5,1,1).p("AiPgaQARAmAxAXQAuAWAzgEQAzgEAjgdQAlgeABgr");
	this.shape.setTransform(42.65,37.1058);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(11.5,1,1).p("AiWgXQAZAmAxAVQAtAUAygFQA0gFAjgbQAmgdAHgp");
	this.shape_1.setTransform(42.575,36.825);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(11.5,1,1).p("AiegVQAhAmAwAUQAtASAzgGQA0gGAlgaQAmgbANgo");
	this.shape_2.setTransform(42.5,36.5735);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(11.5,1,1).p("AikgTQAoAnAwASQAtAQAygGQA0gHAlgZQAngaASgm");
	this.shape_3.setTransform(42.425,36.3418);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(11.5,1,1).p("AiqgRQAvAnAvAQQAsAPAzgHQA0gIAmgYQAogYAWgl");
	this.shape_4.setTransform(42.375,36.1473);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(11.5,1,1).p("AiwgPQA2AnAuAOQAsAOAzgIQA0gJAngWQAogXAbgk");
	this.shape_5.setTransform(42.3,35.9356);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(11.5,1,1).p("Ai1gOQA7AnAuAOQAsAMAzgJQAzgJAogWQApgWAfgi");
	this.shape_6.setTransform(42.25,35.7762);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(11.5,1,1).p("Ai5gMQBAAnAtAMQAsALAzgKQA0gJAogVQApgVAjgh");
	this.shape_7.setTransform(42.2,35.6076);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(11.5,1,1).p("Ai+gLQBFAnAtALQAsAKAygKQA0gKApgUQAqgUAlgg");
	this.shape_8.setTransform(42.15,35.475);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(11.5,1,1).p("AjBgKQBIAnAtAKQAsAJAzgKQAzgLAqgUQAqgSAogg");
	this.shape_9.setTransform(42.125,35.3577);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(11.5,1,1).p("AjFgJQBNAnAsAJQAsAJAzgMQA0gLApgTQArgSArge");
	this.shape_10.setTransform(42.075,35.2556);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(11.5,1,1).p("AjIgIQBQAnAsAIQArAIAzgLQA0gMArgSQAqgSAuge");
	this.shape_11.setTransform(42.075,35.152);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(11.5,1,1).p("AjKgIQBSAoAsAHQArAHAzgMQA0gMArgRQArgRAvgd");
	this.shape_12.setTransform(42.025,35.1039);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(11.5,1,1).p("AjMgHQBUAoAsAHQArAGAzgMQA0gMArgSQArgQAxgd");
	this.shape_13.setTransform(42.025,35.0392);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(11.5,1,1).p("AjOgHQBXAoArAHQAsAFAygLQA0gOAsgQQArgRAygb");
	this.shape_14.setTransform(41.975,35);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(11.5,1,1).p("AjPgGQBYAoArAGQArAGAzgNQA0gNAsgQQArgQAzgc");
	this.shape_15.setTransform(41.975,34.9473);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(11.5,1,1).p("AjQgGQBZAoArAGQArAFAzgMQA0gNAsgRQArgPA0gc");
	this.shape_16.setTransform(41.975,34.934);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(11.5,1,1).p("AjFgJQBNAoAsAJQAsAIAzgLQAzgMArgTQAqgRArgf");
	this.shape_17.setTransform(42.075,35.2434);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#000000").ss(11.5,1,1).p("Ai8gLQBDAnAtALQAsALAzgKQA0gLAogUQAqgUAkgg");
	this.shape_18.setTransform(42.2,35.5439);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#000000").ss(11.5,1,1).p("AizgOQA5AnAuANQAsANAzgJQA0gJAngWQApgWAdgi");
	this.shape_19.setTransform(42.275,35.8091);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#000000").ss(11.5,1,1).p("AisgQQAxAmAvAQQAsAPAzgIQAzgIAngYQAogXAYgl");
	this.shape_20.setTransform(42.35,36.0641);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#000000").ss(11.5,1,1).p("AilgTQApAnAwARQAsARA0gHQAzgHAmgZQAngZASgm");
	this.shape_21.setTransform(42.4,36.3043);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#000000").ss(11.5,1,1).p("AifgUQAjAmAwATQAtASAygGQA0gHAlgZQAmgbAOgn");
	this.shape_22.setTransform(42.475,36.525);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#000000").ss(11.5,1,1).p("AiagWQAdAmAxAVQAtATAygGQA0gFAkgbQAmgcAKgo");
	this.shape_23.setTransform(42.525,36.6867);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#000000").ss(11.5,1,1).p("AiWgXQAZAmAxAVQAtAUAygEQA0gFAjgcQAmgdAHgp");
	this.shape_24.setTransform(42.575,36.8413);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f().s("#000000").ss(11.5,1,1).p("AiTgYQAVAlAxAXQAuAVAygFQA0gEAjgdQAmgdAEgq");
	this.shape_25.setTransform(42.6,36.9647);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f().s("#000000").ss(11.5,1,1).p("AiRgZQATAmAyAXQAtAVAzgEQAzgEAjgdQAlgeADgr");
	this.shape_26.setTransform(42.625,37.0314);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f().s("#000000").ss(11.5,1,1).p("AiQgZQASAlAxAYQAuAWAzgFQAzgEAjgdQAlgeACgr");
	this.shape_27.setTransform(42.65,37.0882);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},32).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16,p:{y:34.934}}]},1).to({state:[{t:this.shape_16,p:{y:34.9239}}]},1).to({state:[{t:this.shape_16,p:{y:34.9239}}]},18).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_27}]},1).to({state:[{t:this.shape}]},1).wait(6));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_1_Layer_2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(11.5,1,1).p("AithGQAFBMA8AuQA3ApBFgBQBHgCAsgtQAwgygFhW");
	this.shape.setTransform(65.578,0.1026);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(11.5,1,1).p("AithAQAFBMA9AoQA5AjBFgCQBGgBArgoQAvgsgFhV");
	this.shape_1.setTransform(65.5788,-0.5447);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(11.5,1,1).p("Aitg6QAFBMA+AiQA6AdBFgCQBGgCAqgiQAuglgFhV");
	this.shape_2.setTransform(65.5794,-1.1437);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(11.5,1,1).p("Aitg0QAFBMA/AcQA7AYBFgCQBGgDAqgcQAsgggFhU");
	this.shape_3.setTransform(65.5803,-1.7134);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(11.5,1,1).p("AitgvQAFBMBAAWQA9AUBEgDQBGgCApgYQArgagFhU");
	this.shape_4.setTransform(65.5809,-2.2358);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(11.5,1,1).p("AitgqQAFBMBBARQA+APBEgCQBFgDApgTQArgWgGhT");
	this.shape_5.setTransform(65.5867,-2.7071);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(11.5,1,1).p("AitgmQAFBMBBANQA/ALBEgDQBGgEAogOQAqgRgGhT");
	this.shape_6.setTransform(65.5875,-3.1429);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(11.5,1,1).p("AitghQAFBMBCAIQBAAHBEgDQBFgEAogKQApgNgGhS");
	this.shape_7.setTransform(65.5883,-3.5526);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(11.5,1,1).p("AitgeQAFBMBCAEQBBAEBEgEQBFgDAngHQApgJgGhS");
	this.shape_8.setTransform(65.5887,-3.8875);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(11.5,1,1).p("AitgcQAFBMBDABQBCABBEgEQBEgEAngDQAogGgGhS");
	this.shape_9.setTransform(65.5896,-4.1469);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(11.5,1,1).p("AitgaQAFBMBDgCQBDgDBEgDQBEgEAngBQAngCgGhS");
	this.shape_10.setTransform(65.59,-4.2975);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(11.5,1,1).p("AitgZQAFBMBEgEQBDgFBEgEQBEgEAmACQAnAAgGhS");
	this.shape_11.setTransform(65.5904,-4.4126);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(11.5,1,1).p("AitgYQAFBMBEgHQBEgHBDgDQBFgFAmAEQAmACgGhR");
	this.shape_12.setTransform(65.5909,-4.4997);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(11.5,1,1).p("AitgXQAFBMBEgJQBFgIBDgEQBEgEAmAFQAmAEgGhR");
	this.shape_13.setTransform(65.5914,-4.5827);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(11.5,1,1).p("AitgXQAFBMBEgJQBFgKBEgEQBEgFAlAHQAmAFgGhR");
	this.shape_14.setTransform(65.5914,-4.6228);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(11.5,1,1).p("AitgWQAFBMBFgLQBEgLBEgEQBEgEAmAHQAlAHgGhR");
	this.shape_15.setTransform(65.5919,-4.662);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(11.5,1,1).p("AitgWQAFBMBFgLQBFgLBDgEQBEgFAmAIQAlAHgGhR");
	this.shape_16.setTransform(65.5919,-4.7003);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(11.5,1,1).p("AitgWQAFBMBFgLQBEgMBEgEQBEgFAlAIQAmAIgGhR");
	this.shape_17.setTransform(65.5913,-4.701);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#000000").ss(11.5,1,1).p("AitgaQAFBMBDgCQBDgDBEgEQBEgEAnAAQAngCgGhS");
	this.shape_18.setTransform(65.59,-4.3211);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#000000").ss(11.5,1,1).p("AitgfQAFBMBCAGQBBAFBEgEQBFgDAngJQApgKgGhS");
	this.shape_19.setTransform(65.5887,-3.7529);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#000000").ss(11.5,1,1).p("AitgnQAFBMBBAOQA/AMBEgDQBFgDApgQQAqgSgGhT");
	this.shape_20.setTransform(65.5875,-3.045);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#000000").ss(11.5,1,1).p("AitgtQAFBMBAAUQA9ASBFgCQBFgDApgWQArgZgFhT");
	this.shape_21.setTransform(65.5812,-2.3848);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#000000").ss(11.5,1,1).p("AitgzQAFBMA/AbQA8AXBEgCQBGgDAqgbQAsgfgFhU");
	this.shape_22.setTransform(65.5803,-1.813);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#000000").ss(11.5,1,1).p("Aitg4QAFBMA+AfQA7AdBFgCQBGgDAqggQAtgkgFhU");
	this.shape_23.setTransform(65.5797,-1.2934);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#000000").ss(11.5,1,1).p("Aitg8QAFBMA+AkQA5AgBFgCQBGgCArgkQAugpgFhU");
	this.shape_24.setTransform(65.5791,-0.8692);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f().s("#000000").ss(11.5,1,1).p("AithAQAFBMA9AoQA5AjBFgBQBGgCArgoQAvgsgFhV");
	this.shape_25.setTransform(65.5788,-0.522);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f().s("#000000").ss(11.5,1,1).p("AithDQAFBMA9ArQA4AmBFgCQBGgBAsgrQAvgugFhW");
	this.shape_26.setTransform(65.5785,-0.2472);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f().s("#000000").ss(11.5,1,1).p("AithFQAFBMA8AtQA4AoBFgCQBHgBArgsQAwgwgFhX");
	this.shape_27.setTransform(65.5783,-0.0473);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f().s("#000000").ss(11.5,1,1).p("AithGQAFBMA8AuQA3ApBFgCQBHgBAsgtQAwgxgFhX");
	this.shape_28.setTransform(65.578,0.0526);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},32).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_17}]},18).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_27}]},1).to({state:[{t:this.shape_28}]},1).to({state:[{t:this.shape}]},1).wait(6));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_1_Layer_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(11.5,1,1).p("AihhAQAFBCA4AqQAzAnBAgCQBBgBApgqQAtgugFhJ");
	this.shape.setTransform(0,0.0028);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(11.5,1,1).p("Aihg6QAFBBA4AlQAzAhBAgBQBCgCAoglQAtgogFhI");
	this.shape_1.setTransform(0.0003,-0.5468);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(11.5,1,1).p("Aihg1QAFBCA4AfQA0AcBAgBQBBgCApggQAsgjgFhI");
	this.shape_2.setTransform(0.0005,-1.0712);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(11.5,1,1).p("AihgwQAFBCA4AaQA0AXBAgBQBCgCAogbQAsgegFhI");
	this.shape_3.setTransform(0.0008,-1.5705);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(11.5,1,1).p("AihgrQAFBBA4AWQA1ATBAgCQBBgBApgXQArgagFhH");
	this.shape_4.setTransform(0.001,-2.0195);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(11.5,1,1).p("AihgnQAFBBA4ARQA1APBAgBQBCgCAogSQArgWgFhH");
	this.shape_5.setTransform(0.0013,-2.4432);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(11.5,1,1).p("AihgjQAFBBA4ANQA1AMBBgCQBBgBAogPQArgSgFhH");
	this.shape_6.setTransform(0.0013,-2.8163);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(11.5,1,1).p("AihggQAFBCA4AJQA2AIBAgCQBBgBApgMQAqgNgFhI");
	this.shape_7.setTransform(0.0016,-3.1875);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(11.5,1,1).p("AihgdQAFBCA4AGQA2AFBAgCQBBgBApgJQAqgKgFhI");
	this.shape_8.setTransform(0.0016,-3.4827);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(11.5,1,1).p("AihgaQAFBCA4ADQA2ACBBgCQBBgBAogGQAqgHgFhI");
	this.shape_9.setTransform(0.0019,-3.7679);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(11.5,1,1).p("AihgYQAFBCA4AAQA2AABBgCQBBgBAogEQAqgEgFhI");
	this.shape_10.setTransform(0.0019,-3.95);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(11.5,1,1).p("AihgXQAFBBA4gBQA3gDBAgCQBBgBApgBQApgDgFhH");
	this.shape_11.setTransform(0.0022,-4.0483);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(11.5,1,1).p("AihgWQAFBBA4gDQA3gFBAgBQBBgCApABQApgBgFhH");
	this.shape_12.setTransform(0.0022,-4.1412);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(11.5,1,1).p("AihgVQAFBBA4gFQA3gGBAgBQBCgCAoACQApABgFhH");
	this.shape_13.setTransform(0.0025,-4.2075);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(11.5,1,1).p("AihgVQAFBCA4gHQA3gHBBgBQBBgCAoADQApADgFhI");
	this.shape_14.setTransform(0.0025,-4.2711);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(11.5,1,1).p("AihgUQAFBBA4gHQA3gIBBgBQBBgCAoAEQApADgFhH");
	this.shape_15.setTransform(0.0025,-4.312);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(11.5,1,1).p("AihgUQAFBBA4gIQA3gIBBgBQBBgCAoAEQApAEgFhH");
	this.shape_16.setTransform(0.0025,-4.332);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(11.5,1,1).p("AihgUQAFBBA4gIQA3gIBBgCQBBgBAoAEQApAEgFhH");
	this.shape_17.setTransform(0.0025,-4.342);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#000000").ss(11.5,1,1).p("AihgYQAFBCA4AAQA3gBBAgCQBBgBApgDQApgEgFhI");
	this.shape_18.setTransform(0.0022,-3.975);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#000000").ss(11.5,1,1).p("AihgeQAFBCA4AHQA2AGBAgBQBBgCApgKQAqgLgFhI");
	this.shape_19.setTransform(0.0016,-3.36);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#000000").ss(11.5,1,1).p("AihgkQAFBBA4AOQA1AMBBgBQBBgCAogPQArgTgFhH");
	this.shape_20.setTransform(0.0013,-2.7417);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#000000").ss(11.5,1,1).p("AihgqQAFBCA4AUQA1ARBAgBQBBgCApgVQArgYgFhI");
	this.shape_21.setTransform(0.001,-2.1691);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#000000").ss(11.5,1,1).p("AihgvQAFBBA4AaQA0AWBAgBQBCgCAogaQAsgegFhH");
	this.shape_22.setTransform(0.0008,-1.6453);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#000000").ss(11.5,1,1).p("AihgzQAFBBA4AeQA0AbBAgCQBBgBApgeQAsgjgFhH");
	this.shape_23.setTransform(0.0005,-1.221);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#000000").ss(11.5,1,1).p("Aihg3QAFBBA4AiQA0AeBAgBQBBgCApgiQAsglgFhI");
	this.shape_24.setTransform(0.0005,-0.8465);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f().s("#000000").ss(11.5,1,1).p("Aihg6QAFBBA4AlQAzAhBAgBQBCgCAogkQAtgpgFhI");
	this.shape_25.setTransform(0.0003,-0.5467);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f().s("#000000").ss(11.5,1,1).p("Aihg9QAFBCA4AnQAzAkBAgCQBCgBAognQAtgrgFhJ");
	this.shape_26.setTransform(0.0003,-0.297);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f().s("#000000").ss(11.5,1,1).p("Aihg+QAFBBA4ApQAzAmBAgCQBBgBApgpQAtgtgFhI");
	this.shape_27.setTransform(0,-0.1221);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f().s("#000000").ss(11.5,1,1).p("Aihg/QAFBBA4AqQAzAnBAgCQBBgBApgqQAtgugFhI");
	this.shape_28.setTransform(0,-0.0222);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},32).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_17}]},18).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_27}]},1).to({state:[{t:this.shape_28}]},1).to({state:[{t:this.shape}]},1).wait(6));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol9 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_6 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	var mask_graphics_0 = new cjs.Graphics().p("EgmTAQqIAAmhIAMAAQBcAABmgOQA0gHB/gYQDxgrC6gCQChABD4AtQB8AYA/AIQBrAOBjgBIAVgBIAMAAQBcAABlgOQA0gHB/gYQDygrC5gCQCfABD5AtQB8AYA/AIQBsAOBjgBIAUgBIALAAQBdAABlgOQA0gHCAgYQDygrC4gCQChABD5AtQB8AYA/AIQBrAOBigBIAWgBIAAGhg");
	var mask_graphics_1 = new cjs.Graphics().p("EgmTAQqIAAmhIAMAAQBcAABmgOQA0gHB/gYQDxgrC6gCQChABD4AtQB8AYA/AIQBsAOBigBIAVgBIAMAAQBcAABlgOQA0gHB/gYQDygrC5gCQCfABD5AtQB8AYA/AIQBsAOBjgBIAUgBIALAAQBdAABlgOQA0gHCAgYQDygrC4gCQChABD5AtQB8AYA/AIQBrAOBigBIAWgBIAAGhg");
	var mask_graphics_2 = new cjs.Graphics().p("EgmTAQqIAAmhIAMAAQBcAABmgOQA0gHB/gYQDxgrC6gCQChABD4AtQB8AYA/AIQBsAOBigBIAVgBIAMAAQBcAABlgOQA0gHB/gYQDygrC5gCQCfABD5AtQB8AYA/AIQBsAOBjgBIAUgBIALAAQBdAABlgOQA0gHCAgYQDygrC4gCQChABD5AtQB8AYA/AIQBrAOBigBIAWgBIAAGhg");
	var mask_graphics_3 = new cjs.Graphics().p("EgmTAQqIAAmhIAMAAQBcAABmgOQA0gHB/gYQDxgrC6gCQChABD4AtQB8AYBAAIQBqAOBjgBIAVgBIAMAAQBcAABlgOQA0gHB/gYQDygrC5gCQCfABD5AtQB8AYA/AIQBsAOBjgBIAUgBIALAAQBdAABlgOQA0gHCAgYQDygrC4gCQChABD5AtQB8AYA/AIQBrAOBigBIAWgBIAAGhg");
	var mask_graphics_4 = new cjs.Graphics().p("EgmTAQqIAAmhIAMAAQBcAABmgOQA0gHB/gYQDxgrC6gCQChABD4AtQB8AYBAAIQBqAOBjgBIAVgBIAMAAQBcAABlgOQA0gHB/gYQDygrC5gCQCfABD5AtQB8AYA/AIQBsAOBjgBIAUgBIAMAAQBcAABlgOQA0gHCAgYQDygrC4gCQChABD5AtQB8AYA/AIQBrAOBigBIAWgBIAAGhg");
	var mask_graphics_5 = new cjs.Graphics().p("EgmTAQqIAAmhIAMAAQBcAABmgOQA0gHB/gYQDxgrC6gCQChABD4AtQB8AYBAAIQBqAOBjgBIAVgBIAMAAQBcAABlgOQA0gHB/gYQDygrC5gCQCfABD5AtQB8AYA/AIQBsAOBigBIAVgBIAMAAQBcAABlgOQA0gHB/gYQDzgrC4gCQChABD5AtQB8AYA/AIQBrAOBjgBIAVgBIAAGhg");
	var mask_graphics_6 = new cjs.Graphics().p("EgmTAQqIAAmhIAMAAQBcAABmgOQA0gHB/gYQDxgrC6gCQChABD4AtQB8AYBAAIQBqAOBjgBIAVgBIALAAQBdAABlgOQA0gHB/gYQDygrC5gCQCfABD5AtQB8AYA/AIQBsAOBigBIAVgBIAMAAQBcAABlgOQA0gHB/gYQDzgrC4gCQChABD5AtQB8AYA/AIQBrAOBjgBIAVgBIAAGhg");
	var mask_graphics_7 = new cjs.Graphics().p("EgmTAQqIAAmhIAMAAQBcAABmgOQA0gHB/gYQDxgrC6gCQChABD4AtQB8AYBAAIQBrAOBigBIAVgBIALAAQBdAABlgOQA0gHB/gYQDygrC5gCQCfABD5AtQB8AYA/AIQBsAOBigBIAVgBIAMAAQBcAABlgOQA0gHB/gYQDzgrC4gCQChABD5AtQB8AYA/AIQBrAOBjgBIAVgBIAAGhg");
	var mask_graphics_8 = new cjs.Graphics().p("EgmTAQqIAAmhIAMAAQBcAABmgOQA0gHB/gYQDxgrC6gCQChABD4AtQB8AYBAAIQBrAOBigBIAVgBIALAAQBdAABlgOQA0gHB/gYQDygrC5gCQCfABD5AtQB9AYA+AIQBsAOBigBIAVgBIAMAAQBcAABlgOQA0gHB/gYQDzgrC4gCQChABD5AtQB8AYA/AIQBrAOBjgBIAVgBIAAGhg");
	var mask_graphics_9 = new cjs.Graphics().p("EgmTAQqIAAmhIAMAAQBcAABmgOQA0gHB/gYQDxgrC6gCQChABD4AtQB8AYBAAIQBrAOBigBIAVgBIALAAQBdAABlgOQA0gHB/gYQDygrC5gCQCfABD5AtQB9AYA+AIQBsAOBigBIAVgBIAMAAQBdAABkgOQA0gHB/gYQDzgrC5gCQCgABD5AtQB8AYA/AIQBrAOBjgBIAVgBIAAGhg");
	var mask_graphics_10 = new cjs.Graphics().p("EgmTAQqIAAmhIAMAAQBdAABlgOQA0gHB/gYQDxgrC6gCQChABD4AtQB8AYBAAIQBrAOBigBIAVgBIALAAQBdAABlgOQA0gHB/gYQDygrC5gCQCfABD5AtQB9AYA+AIQBsAOBigBIAVgBIAMAAQBdAABkgOQA0gHB/gYQDzgrC5gCQCgABD5AtQB8AYA/AIQBrAOBjgBIAVgBIAAGhg");
	var mask_graphics_11 = new cjs.Graphics().p("EgmTAQqIAAmhIAMAAQBdAABkgOQA1gHB/gYQDxgrC6gCQChABD4AtQB8AYBAAIQBrAOBigBIAVgBIALAAQBdAABlgOQA0gHB/gYQDygrC5gCQCfABD5AtQB9AYA+AIQBsAOBigBIAVgBIAMAAQBcAABlgOQA0gHB/gYQDygrC6gCQCgABD4AtQB9AYA/AIQBrAOBjgBIAVgBIAAGhg");
	var mask_graphics_12 = new cjs.Graphics().p("EgmTAQqIAAmhIAMAAQBdAABkgOQA1gHB/gYQDxgrC6gCQChABD4AtQB8AYBAAIQBrAOBigBIAVgBIALAAQBdAABlgOQA0gHB/gYQDygrC5gCQCgABD4AtQB9AYA+AIQBsAOBigBIAVgBIAMAAQBcAABlgOQA0gHB/gYQDygrC6gCQCgABD4AtQB9AYA/AIQBrAOBjgBIAUgBIAAGhg");
	var mask_graphics_13 = new cjs.Graphics().p("EgmTAQqIAAmhIAMAAQBdAABkgOQA1gHB/gYQDxgrC6gCQChABD4AtQB8AYBAAIQBrAOBigBIAVgBIALAAQBdAABlgOQA0gHB/gYQDygrC5gCQCgABD4AtQB9AYA+AIQBsAOBigBIAVgBIAMAAQBcAABlgOQA0gHB/gYQDygrC6gCQCgABD4AtQB9AYA/AIQBrAOBjgBIAUgBIAAGhg");
	var mask_graphics_14 = new cjs.Graphics().p("EgmTAQqIAAmhIAMAAQBdAABkgOQA0gHCAgYQDxgrC6gCQChABD4AtQB8AYBAAIQBrAOBigBIAVgBIALAAQBdAABlgOQA0gHB/gYQDygrC5gCQCfABD5AtQB9AYA+AIQBsAOBigBIAWgBIALAAQBcAABlgOQA0gHB/gYQDygrC6gCQCgABD4AtQB9AYA/AIQBrAOBjgBIAUgBIAAGhg");
	var mask_graphics_15 = new cjs.Graphics().p("EgmTAQqIAAmhIAMAAQBdAABkgOQA0gHCAgYQDxgrC6gCQChABD4AtQB8AYBAAIQBrAOBigBIAVgBIALAAQBdAABlgOQA0gHB/gYQDygrC5gCQCfABD5AtQB9AYA+AIQBsAOBigBIAWgBIALAAQBcAABlgOQA0gHB/gYQDygrC6gCQCgABD4AtQB9AYA/AIQBrAOBjgBIAUgBIAAGhg");
	var mask_graphics_16 = new cjs.Graphics().p("EgmTAQqIAAmhIAMAAQBdAABkgOQA0gHCAgYQDxgrC6gCQChABD4AtQB8AYBAAIQBrAOBigBIAVgBIALAAQBdAABlgOQA0gHB/gYQDygrC5gCQCfABD5AtQB9AYA+AIQBsAOBigBIAWgBIALAAQBcAABlgOQA0gHB/gYQDygrC6gCQCgABD4AtQB9AYA/AIQBrAOBjgBIAUgBIAAGhg");
	var mask_graphics_17 = new cjs.Graphics().p("EgmTAQqIAAmhIAMAAQBdAABkgOQA0gHCAgYQDxgrC6gCQChABD4AtQB8AYBAAIQBrAOBigBIAVgBIALAAQBdAABlgOQA0gHB/gYQDygrC5gCQCfABD5AtQB9AYA+AIQBrAOBjgBIAWgBIALAAQBcAABlgOQA0gHB/gYQDygrC6gCQCgABD4AtQB9AYA/AIQBrAOBjgBIAUgBIAAGhg");
	var mask_graphics_18 = new cjs.Graphics().p("EgmTAQqIAAmhIAMAAQBdAABkgOQA0gHCAgYQDxgrC6gCQChABD4AtQB8AYBAAIQBrAOBigBIAVgBIALAAQBdAABlgOQA0gHB/gYQDygrC5gCQCfABD5AtQB9AYA+AIQBrAOBjgBIAWgBIALAAQBcAABlgOQA0gHB/gYQDygrC6gCQCgABD4AtQB9AYA/AIQBrAOBjgBIAUgBIAAGhg");
	var mask_graphics_19 = new cjs.Graphics().p("EgmTAQqIAAmhIAMAAQBdAABkgOQA0gHCAgYQDygrC5gCQCgABD5AtQB8AYBAAIQBrAOBigBIAVgBIALAAQBdAABlgOQA0gHB/gYQDygrC5gCQCgABD4AtQB9AYA+AIQBrAOBjgBIAWgBIALAAQBcAABlgOQA0gHB/gYQDygrC6gCQCgABD4AtQB9AYA/AIQBrAOBjgBIAUgBIAAGhg");
	var mask_graphics_20 = new cjs.Graphics().p("EgmTAQqIAAmhIAMAAQBdAABkgOQA0gHCAgYQDygrC5gCQCgABD5AtQB8AYBAAIQBrAOBigBIAVgBIALAAQBdAABlgOQA0gHB/gYQDygrC5gCQCgABD4AtQB9AYA+AIQBrAOBjgBIAWgBIALAAQBcAABlgOQA0gHB/gYQDygrC6gCQCgABD4AtQB9AYA/AIQBrAOBjgBIAUgBIAAGhg");
	var mask_graphics_21 = new cjs.Graphics().p("EgmTAQqIAAmhIAMAAQBdAABkgOQA0gHCAgYQDygrC5gCQCgABD5AtQB8AYBAAIQBrAOBigBIAVgBIALAAQBdAABlgOQA0gHB/gYQDygrC5gCQCgABD4AtQB9AYA+AIQBrAOBjgBIAWgBIALAAQBcAABlgOQA0gHB/gYQDygrC6gCQCgABD4AtQB9AYA/AIQBrAOBjgBIAUgBIAAGhg");
	var mask_graphics_22 = new cjs.Graphics().p("EgmTAQqIAAmhIAMAAQBdAABkgOQA0gHCAgYQDxgrC6gCQCgABD5AtQB8AYBAAIQBrAOBigBIAVgBIALAAQBdAABlgOQA0gHB/gYQDygrC5gCQCgABD4AtQB9AYA+AIQBrAOBjgBIAWgBIALAAQBcAABlgOQA0gHB/gYQDygrC6gCQCgABD4AtQB9AYA/AIQBrAOBjgBIAUgBIAAGhg");
	var mask_graphics_23 = new cjs.Graphics().p("EgmTAQqIAAmhIAMAAQBdAABkgOQA0gHCAgYQDxgrC6gCQCgABD5AtQB8AYBAAIQBrAOBigBIAVgBIALAAQBdAABlgOQA0gHB/gYQDygrC5gCQCgABD4AtQB9AYA+AIQBrAOBjgBIAWgBIALAAQBcAABlgOQA0gHB/gYQDygrC6gCQCgABD4AtQB9AYA/AIQBrAOBjgBIAUgBIAAGhg");
	var mask_graphics_24 = new cjs.Graphics().p("EgmSAQqIAAmhIALAAQBdAABkgOQA0gHCAgYQDxgrC6gCQCgABD5AtQB8AYBAAIQBrAOBigBIAVgBIALAAQBdAABlgOQA0gHB/gYQDygrC5gCQCgABD4AtQB9AYA+AIQBrAOBjgBIAWgBIALAAQBcAABlgOQA0gHB/gYQDygrC6gCQCgABD4AtQB9AYA/AIQBrAOBjgBIAUgBIAAGhg");
	var mask_graphics_25 = new cjs.Graphics().p("EgmSAQqIAAmhIALAAQBdAABkgOQA0gHCAgYQDxgrC6gCQCgABD5AtQB8AYBAAIQBrAOBigBIAVgBIALAAQBdAABlgOQA0gHB/gYQDygrC5gCQCgABD4AtQB8AYBAAIQBqAOBjgBIAWgBIALAAQBdAABkgOQA0gHB/gYQDygrC6gCQCgABD4AtQB9AYA/AIQBrAOBjgBIAUgBIAAGhg");
	var mask_graphics_26 = new cjs.Graphics().p("EgmSAQqIAAmhIALAAQBdAABkgOQA0gHCAgYQDxgrC6gCQCgABD5AtQB8AYBAAIQBrAOBigBIAVgBIALAAQBdAABlgOQA0gHB/gYQDygrC5gCQCgABD4AtQB8AYBAAIQBqAOBjgBIAWgBIALAAQBdAABkgOQA0gHB/gYQDygrC6gCQCgABD4AtQB9AYA/AIQBrAOBjgBIAUgBIAAGhg");
	var mask_graphics_27 = new cjs.Graphics().p("EgmSAQqIAAmhIALAAQBdAABkgOQA0gHCAgYQDxgrC6gCQCgABD5AtQB8AYA/AIQBsAOBigBIAVgBIALAAQBdAABlgOQA0gHB/gYQDygrC5gCQCgABD4AtQB8AYBAAIQBqAOBjgBIAWgBIAKAAQBeAABkgOQA0gHB/gYQDygrC6gCQCgABD4AtQB9AYA/AIQBrAOBigBIAVgBIAAGhg");
	var mask_graphics_28 = new cjs.Graphics().p("EgmSAQqIAAmhIALAAQBdAABkgOQA0gHCAgYQDygrC5gCQCgABD5AtQB8AYA/AIQBsAOBigBIAVgBIALAAQBdAABlgOQA0gHB/gYQDygrC5gCQCgABD4AtQB8AYBAAIQBrAOBigBIAWgBIAKAAQBeAABkgOQA0gHB/gYQDygrC6gCQCgABD4AtQB9AYA/AIQBrAOBigBIAVgBIAAGhg");
	var mask_graphics_29 = new cjs.Graphics().p("EgmSAQqIAAmhIALAAQBdAABkgOQA0gHCAgYQDygrC5gCQCgABD5AtQB8AYA/AIQBsAOBigBIAVgBIALAAQBdAABlgOQA0gHB/gYQDygrC5gCQCgABD4AtQB8AYBAAIQBrAOBigBIAWgBIAKAAQBeAABkgOQA0gHB/gYQDygrC6gCQCgABD4AtQB9AYA/AIQBrAOBigBIAVgBIAAGhg");
	var mask_graphics_30 = new cjs.Graphics().p("EgmSAQqIAAmhIALAAQBdAABkgOQA0gHCAgYQDygrC5gCQCgABD5AtQB8AYA/AIQBsAOBigBIAVgBIAMAAQBcAABlgOQA0gHB/gYQDygrC5gCQCgABD4AtQB9AYA/AIQBqAOBkgBIAVgBIAKAAQBeAABkgOQA0gHCAgYQDxgrC6gCQCgABD4AtQB9AYA/AIQBrAOBigBIAVgBIAAGhg");
	var mask_graphics_31 = new cjs.Graphics().p("EgmSAQqIAAmhIALAAQBdAABkgOQA0gHCAgYQDygrC5gCQCgABD5AtQB8AYA/AIQBsAOBigBIAVgBIAMAAQBcAABlgOQA0gHB/gYQDygrC5gCQCgABD4AtQB9AYA/AIQBqAOBkgBIAVgBIAKAAQBeAABkgOQA0gHCAgYQDxgrC6gCQCgABD4AtQB9AYA/AIQBrAOBigBIAVgBIAAGhg");
	var mask_graphics_32 = new cjs.Graphics().p("EgmSAQqIAAmhIALAAQBdAABkgOQA0gHCAgYQDygrC5gCQCgABD5AtQB8AYA/AIQBsAOBigBIAVgBIAMAAQBcAABlgOQA0gHB/gYQDygrC5gCQCgABD4AtQB9AYA/AIQBqAOBkgBIAVgBIAKAAQBeAABkgOQA0gHCAgYQDxgrC5gCQChABD4AtQB9AYA/AIQBrAOBigBIAVgBIAAGhg");
	var mask_graphics_33 = new cjs.Graphics().p("EgmSAQqIAAmhIALAAQBcAABlgOQA0gHCAgYQDygrC5gCQCgABD5AtQB8AYA/AIQBrAOBjgBIAVgBIAMAAQBcAABlgOQA0gHB/gYQDygrC5gCQCgABD4AtQB9AYA/AIQBqAOBkgBIAVgBIAKAAQBeAABkgOQA0gHCAgYQDxgrC5gCQChABD4AtQB9AYA/AIQBrAOBigBIAVgBIAAGhg");
	var mask_graphics_34 = new cjs.Graphics().p("EgmSAQqIAAmhIALAAQBcAABlgOQA0gHCAgYQDygrC5gCQCgABD5AtQB8AYA/AIQBrAOBjgBIAVgBIAMAAQBcAABlgOQA0gHB/gYQDygrC5gCQCgABD4AtQB9AYA/AIQBqAOBkgBIAVgBIAKAAQBeAABkgOQA0gHCAgYQDygrC4gCQChABD4AtQB9AYA/AIQBrAOBigBIAVgBIAAGhg");
	var mask_graphics_35 = new cjs.Graphics().p("EgmSAQqIAAmhIALAAQBcAABmgOQAzgHCAgYQDygrC5gCQCgABD5AtQB8AYA/AIQBrAOBjgBIAVgBIAMAAQBcAABlgOQA0gHB/gYQDygrC5gCQCgABD4AtQB9AYA/AIQBqAOBkgBIAVgBIAKAAQBeAABkgOQA0gHCAgYQDxgrC5gCQChABD4AtQB9AYA/AIQBrAOBigBIAVgBIAAGhg");
	var mask_graphics_36 = new cjs.Graphics().p("EgmSAQqIAAmhIALAAQBcAABmgOQAzgHCAgYQDygrC5gCQCgABD5AtQB8AYA/AIQBrAOBjgBIAVgBIAMAAQBcAABlgOQA0gHB/gYQDygrC5gCQCgABD4AtQB9AYA/AIQBqAOBkgBIAVgBIAKAAQBeAABkgOQA0gHCAgYQDxgrC5gCQChABD5AtQB8AYA/AIQBrAOBigBIAVgBIAAGhg");
	var mask_graphics_37 = new cjs.Graphics().p("EgmSAQqIAAmhIALAAQBcAABmgOQAzgHCAgYQDygrC5gCQCgABD5AtQB8AYA/AIQBrAOBjgBIAVgBIAMAAQBcAABlgOQA0gHB/gYQDygrC5gCQCgABD4AtQB9AYA/AIQBqAOBkgBIAVgBIAKAAQBeAABkgOQA0gHCAgYQDxgrC5gCQChABD5AtQB8AYA/AIQBrAOBigBIAWgBIAAGhg");
	var mask_graphics_38 = new cjs.Graphics().p("EgmSAQqIAAmhIALAAQBcAABmgOQAzgHCAgYQDygrC5gCQCgABD5AtQB8AYA/AIQBrAOBjgBIAVgBIAMAAQBcAABlgOQA0gHB/gYQDygrC5gCQCgABD4AtQB9AYA/AIQBqAOBkgBIAUgBIALAAQBeAABkgOQA0gHCAgYQDxgrC5gCQChABD5AtQB8AYA/AIQBrAOBigBIAWgBIAAGhg");
	var mask_graphics_39 = new cjs.Graphics().p("EgmSAQqIAAmhIALAAQBcAABmgOQAzgHCAgYQDygrC5gCQChABD4AtQB8AYA/AIQBrAOBjgBIAVgBIAMAAQBcAABlgOQA0gHB/gYQDygrC5gCQCgABD4AtQB9AYA/AIQBqAOBkgBIAUgBIALAAQBeAABkgOQA0gHCAgYQDxgrC5gCQChABD5AtQB8AYA/AIQBrAOBigBIAWgBIAAGhg");
	var mask_graphics_40 = new cjs.Graphics().p("EgmSAQqIAAmhIALAAQBcAABmgOQAzgHCAgYQDygrC5gCQCgABD5AtQB8AYA/AIQBrAOBjgBIAVgBIAMAAQBcAABlgOQA0gHB/gYQDygrC5gCQCgABD4AtQB9AYA/AIQBqAOBkgBIAUgBIALAAQBeAABkgOQA0gHCAgYQDxgrC5gCQChABD5AtQB8AYA/AIQBrAOBigBIAWgBIAAGhg");
	var mask_graphics_41 = new cjs.Graphics().p("EgmSAQqIAAmhIALAAQBcAABmgOQAzgHCAgYQDygrC5gCQCgABD5AtQB8AYA/AIQBrAOBjgBIAVgBIAMAAQBcAABlgOQA0gHB/gYQDygrC5gCQCfABD5AtQB8AYBAAIQBqAOBkgBIAUgBIALAAQBeAABkgOQA0gHCAgYQDxgrC5gCQChABD5AtQB8AYA/AIQBrAOBigBIAWgBIAAGhg");
	var mask_graphics_42 = new cjs.Graphics().p("EgmSAQqIAAmhIALAAQBdAABlgOQAzgHCAgYQDygrC5gCQCgABD5AtQB8AYA/AIQBrAOBjgBIAVgBIAMAAQBcAABlgOQA0gHB/gYQDygrC5gCQCfABD5AtQB8AYBAAIQBqAOBkgBIAUgBIALAAQBeAABkgOQA0gHCAgYQDxgrC5gCQChABD5AtQB8AYA/AIQBrAOBigBIAWgBIAAGhg");
	var mask_graphics_43 = new cjs.Graphics().p("EgmSAQqIAAmhIALAAQBdAABlgOQA0gHB/gYQDygrC5gCQCgABD5AtQB8AYA/AIQBrAOBjgBIAVgBIAMAAQBcAABlgOQA0gHB/gYQDygrC5gCQCfABD5AtQB8AYBAAIQBqAOBkgBIAUgBIALAAQBeAABkgOQA0gHCAgYQDygrC4gCQChABD5AtQB8AYA/AIQBrAOBigBIAWgBIAAGhg");
	var mask_graphics_44 = new cjs.Graphics().p("EgmSAQqIAAmhIALAAQBdAABlgOQA0gHB/gYQDygrC5gCQCgABD5AtQB8AYA/AIQBrAOBjgBIAVgBIAMAAQBcAABlgOQA0gHB/gYQDygrC5gCQCfABD5AtQB8AYBAAIQBqAOBkgBIAUgBIALAAQBeAABkgOQA0gHCAgYQDygrC4gCQChABD5AtQB8AYA/AIQBrAOBigBIAWgBIAAGhg");
	var mask_graphics_45 = new cjs.Graphics().p("EgmSAQqIAAmhIALAAQBcAABmgOQA0gHB/gYQDygrC5gCQCgABD5AtQB8AYA/AIQBrAOBjgBIAVgBIAMAAQBcAABlgOQA0gHB/gYQDygrC5gCQCfABD5AtQB8AYBAAIQBqAOBkgBIAUgBIALAAQBeAABkgOQA0gHCAgYQDygrC4gCQChABD5AtQB8AYA/AIQBrAOBigBIAWgBIAAGhg");
	var mask_graphics_46 = new cjs.Graphics().p("EgmSAQqIAAmhIALAAQBcAABmgOQA0gHB/gYQDxgrC6gCQChABD4AtQB8AYA/AIQBrAOBjgBIAVgBIAMAAQBcAABlgOQA0gHB/gYQDygrC5gCQCfABD5AtQB8AYBAAIQBrAOBjgBIAUgBIALAAQBeAABkgOQA0gHCAgYQDygrC4gCQChABD5AtQB8AYA/AIQBrAOBigBIAWgBIAAGhg");
	var mask_graphics_47 = new cjs.Graphics().p("EgmSAQqIAAmhIALAAQBcAABmgOQA0gHB/gYQDxgrC6gCQChABD4AtQB8AYA/AIQBrAOBjgBIAVgBIAMAAQBcAABlgOQA0gHB/gYQDygrC5gCQCfABD5AtQB8AYBAAIQBrAOBjgBIAUgBIALAAQBdAABlgOQA0gHCAgYQDygrC4gCQChABD5AtQB8AYA/AIQBrAOBigBIAWgBIAAGhg");
	var mask_graphics_48 = new cjs.Graphics().p("EgmTAQqIAAmhIAMAAQBcAABmgOQA0gHB/gYQDxgrC6gCQChABD4AtQB8AYA/AIQBrAOBjgBIAVgBIAMAAQBcAABlgOQA0gHB/gYQDygrC5gCQCfABD5AtQB8AYBAAIQBrAOBjgBIAUgBIALAAQBdAABlgOQA0gHCAgYQDygrC4gCQChABD5AtQB8AYA/AIQBrAOBigBIAWgBIAAGhg");
	var mask_graphics_49 = new cjs.Graphics().p("EgmTAQqIAAmhIAMAAQBcAABmgOQA0gHB/gYQDxgrC6gCQChABD4AtQB8AYA/AIQBrAOBjgBIAVgBIAMAAQBcAABlgOQA0gHB/gYQDygrC5gCQCfABD5AtQB8AYA/AIQBsAOBjgBIAUgBIALAAQBdAABlgOQA0gHCAgYQDygrC4gCQChABD5AtQB8AYA/AIQBrAOBigBIAWgBIAAGhg");
	var mask_graphics_50 = new cjs.Graphics().p("EgmTAQqIAAmhIAMAAQBcAABmgOQA0gHB/gYQDxgrC6gCQChABD4AtQB8AYA/AIQBsAOBigBIAVgBIAMAAQBcAABlgOQA0gHB/gYQDygrC5gCQCfABD5AtQB8AYA/AIQBsAOBjgBIAUgBIALAAQBdAABlgOQA0gHCAgYQDygrC4gCQChABD5AtQB8AYA/AIQBrAOBigBIAWgBIAAGhg");
	var mask_graphics_51 = new cjs.Graphics().p("EgmTAQqIAAmhIAMAAQBcAABmgOQA0gHB/gYQDxgrC6gCQChABD4AtQB8AYBAAIQBqAOBjgBIAVgBIAMAAQBcAABlgOQA0gHB/gYQDygrC5gCQCfABD5AtQB8AYA/AIQBsAOBjgBIAUgBIAMAAQBcAABlgOQA0gHCAgYQDygrC4gCQChABD5AtQB8AYA/AIQBrAOBigBIAWgBIAAGhg");
	var mask_graphics_52 = new cjs.Graphics().p("EgmTAQqIAAmhIAMAAQBcAABmgOQA0gHB/gYQDxgrC6gCQChABD4AtQB8AYBAAIQBqAOBjgBIAVgBIAMAAQBcAABlgOQA0gHB/gYQDygrC5gCQCfABD5AtQB8AYA/AIQBsAOBjgBIAUgBIAMAAQBcAABlgOQA0gHCAgYQDygrC4gCQChABD5AtQB8AYA/AIQBrAOBjgBIAVgBIAAGhg");
	var mask_graphics_53 = new cjs.Graphics().p("EgmTAQqIAAmhIAMAAQBcAABmgOQA0gHB/gYQDxgrC6gCQChABD4AtQB8AYBAAIQBqAOBjgBIAVgBIAMAAQBcAABlgOQA0gHB/gYQDygrC5gCQCfABD5AtQB8AYA/AIQBsAOBjgBIAUgBIAMAAQBcAABlgOQA0gHCAgYQDygrC4gCQChABD5AtQB8AYA/AIQBrAOBjgBIAVgBIAAGhg");
	var mask_graphics_54 = new cjs.Graphics().p("EgmTAQqIAAmhIAMAAQBcAABmgOQA0gHB/gYQDxgrC6gCQChABD4AtQB8AYBAAIQBqAOBjgBIAVgBIALAAQBdAABlgOQA0gHB/gYQDygrC5gCQCfABD5AtQB8AYA/AIQBsAOBigBIAVgBIAMAAQBcAABlgOQA0gHB/gYQDzgrC4gCQChABD5AtQB8AYA/AIQBrAOBjgBIAVgBIAAGhg");
	var mask_graphics_55 = new cjs.Graphics().p("EgmTAQqIAAmhIAMAAQBcAABmgOQA0gHB/gYQDxgrC6gCQChABD4AtQB8AYBAAIQBrAOBigBIAVgBIALAAQBdAABlgOQA0gHB/gYQDygrC5gCQCfABD5AtQB8AYA/AIQBsAOBigBIAVgBIAMAAQBcAABlgOQA0gHB/gYQDzgrC4gCQChABD5AtQB8AYA/AIQBrAOBjgBIAVgBIAAGhg");
	var mask_graphics_56 = new cjs.Graphics().p("EgmTAQqIAAmhIAMAAQBcAABmgOQA0gHB/gYQDxgrC6gCQChABD4AtQB8AYBAAIQBrAOBigBIAVgBIALAAQBdAABlgOQA0gHB/gYQDygrC5gCQCfABD5AtQB8AYA/AIQBsAOBigBIAVgBIAMAAQBcAABlgOQA0gHB/gYQDzgrC4gCQChABD5AtQB8AYA/AIQBrAOBjgBIAVgBIAAGhg");
	var mask_graphics_57 = new cjs.Graphics().p("EgmTAQqIAAmhIAMAAQBcAABmgOQA0gHB/gYQDxgrC6gCQChABD4AtQB8AYBAAIQBrAOBigBIAVgBIALAAQBdAABlgOQA0gHB/gYQDygrC5gCQCfABD5AtQB9AYA+AIQBsAOBigBIAVgBIAMAAQBdAABkgOQA0gHB/gYQDzgrC5gCQCgABD5AtQB8AYA/AIQBrAOBjgBIAVgBIAAGhg");
	var mask_graphics_58 = new cjs.Graphics().p("EgmTAQqIAAmhIAMAAQBcAABmgOQA0gHB/gYQDxgrC6gCQChABD4AtQB8AYBAAIQBrAOBigBIAVgBIALAAQBdAABlgOQA0gHB/gYQDygrC5gCQCfABD5AtQB9AYA+AIQBsAOBigBIAVgBIAMAAQBdAABkgOQA0gHB/gYQDzgrC5gCQCgABD5AtQB8AYA/AIQBrAOBjgBIAVgBIAAGhg");
	var mask_graphics_59 = new cjs.Graphics().p("EgmTAQqIAAmhIAMAAQBdAABkgOQA1gHB/gYQDxgrC6gCQChABD4AtQB8AYBAAIQBrAOBigBIAVgBIALAAQBdAABlgOQA0gHB/gYQDygrC5gCQCfABD5AtQB9AYA+AIQBsAOBigBIAVgBIAMAAQBcAABlgOQA0gHB/gYQDzgrC5gCQCgABD5AtQB8AYA/AIQBrAOBjgBIAUgBIAAGhg");

	this.timeline.addTween(cjs.Tween.get(mask).to({graphics:mask_graphics_0,x:179.9,y:106.6}).wait(1).to({graphics:mask_graphics_1,x:177.1271,y:106.6}).wait(1).to({graphics:mask_graphics_2,x:174.3542,y:106.6}).wait(1).to({graphics:mask_graphics_3,x:171.5814,y:106.6}).wait(1).to({graphics:mask_graphics_4,x:168.8085,y:106.6}).wait(1).to({graphics:mask_graphics_5,x:166.0356,y:106.6}).wait(1).to({graphics:mask_graphics_6,x:163.2627,y:106.6}).wait(1).to({graphics:mask_graphics_7,x:160.4898,y:106.6}).wait(1).to({graphics:mask_graphics_8,x:157.717,y:106.6}).wait(1).to({graphics:mask_graphics_9,x:154.9441,y:106.6}).wait(1).to({graphics:mask_graphics_10,x:152.1712,y:106.6}).wait(1).to({graphics:mask_graphics_11,x:149.3983,y:106.6}).wait(1).to({graphics:mask_graphics_12,x:146.6254,y:106.6}).wait(1).to({graphics:mask_graphics_13,x:143.8525,y:106.6}).wait(1).to({graphics:mask_graphics_14,x:141.0797,y:106.6}).wait(1).to({graphics:mask_graphics_15,x:138.3068,y:106.6}).wait(1).to({graphics:mask_graphics_16,x:135.5339,y:106.6}).wait(1).to({graphics:mask_graphics_17,x:132.761,y:106.6}).wait(1).to({graphics:mask_graphics_18,x:129.9881,y:106.6}).wait(1).to({graphics:mask_graphics_19,x:127.2153,y:106.6}).wait(1).to({graphics:mask_graphics_20,x:124.4424,y:106.6}).wait(1).to({graphics:mask_graphics_21,x:121.6695,y:106.6}).wait(1).to({graphics:mask_graphics_22,x:118.8966,y:106.6}).wait(1).to({graphics:mask_graphics_23,x:116.1237,y:106.6}).wait(1).to({graphics:mask_graphics_24,x:113.3509,y:106.6}).wait(1).to({graphics:mask_graphics_25,x:110.578,y:106.6}).wait(1).to({graphics:mask_graphics_26,x:107.8051,y:106.6}).wait(1).to({graphics:mask_graphics_27,x:105.0322,y:106.6}).wait(1).to({graphics:mask_graphics_28,x:102.2593,y:106.6}).wait(1).to({graphics:mask_graphics_29,x:99.4864,y:106.6}).wait(1).to({graphics:mask_graphics_30,x:96.7136,y:106.6}).wait(1).to({graphics:mask_graphics_31,x:93.9407,y:106.6}).wait(1).to({graphics:mask_graphics_32,x:91.1678,y:106.6}).wait(1).to({graphics:mask_graphics_33,x:88.3949,y:106.6}).wait(1).to({graphics:mask_graphics_34,x:85.622,y:106.6}).wait(1).to({graphics:mask_graphics_35,x:82.8492,y:106.6}).wait(1).to({graphics:mask_graphics_36,x:80.0763,y:106.6}).wait(1).to({graphics:mask_graphics_37,x:77.3034,y:106.6}).wait(1).to({graphics:mask_graphics_38,x:74.5305,y:106.6}).wait(1).to({graphics:mask_graphics_39,x:71.7576,y:106.6}).wait(1).to({graphics:mask_graphics_40,x:68.9847,y:106.6}).wait(1).to({graphics:mask_graphics_41,x:66.2119,y:106.6}).wait(1).to({graphics:mask_graphics_42,x:63.439,y:106.6}).wait(1).to({graphics:mask_graphics_43,x:60.6661,y:106.6}).wait(1).to({graphics:mask_graphics_44,x:57.8932,y:106.6}).wait(1).to({graphics:mask_graphics_45,x:55.1203,y:106.6}).wait(1).to({graphics:mask_graphics_46,x:52.3475,y:106.6}).wait(1).to({graphics:mask_graphics_47,x:49.5746,y:106.6}).wait(1).to({graphics:mask_graphics_48,x:46.8017,y:106.6}).wait(1).to({graphics:mask_graphics_49,x:44.0288,y:106.6}).wait(1).to({graphics:mask_graphics_50,x:41.2559,y:106.6}).wait(1).to({graphics:mask_graphics_51,x:38.4831,y:106.6}).wait(1).to({graphics:mask_graphics_52,x:35.7102,y:106.6}).wait(1).to({graphics:mask_graphics_53,x:32.9373,y:106.6}).wait(1).to({graphics:mask_graphics_54,x:30.1644,y:106.6}).wait(1).to({graphics:mask_graphics_55,x:27.3915,y:106.6}).wait(1).to({graphics:mask_graphics_56,x:24.6186,y:106.6}).wait(1).to({graphics:mask_graphics_57,x:21.8458,y:106.6}).wait(1).to({graphics:mask_graphics_58,x:19.0729,y:106.6}).wait(1).to({graphics:mask_graphics_59,x:16.3,y:106.6}).wait(1));

	// Layer_2
	this.instance = new lib.milk_part();
	this.instance.parent = this;
	this.instance.setTransform(-48,159);

	var maskedShapeInstanceList = [this.instance];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(60));

	// bg_png
	this.instance_1 = new lib.bg();
	this.instance_1.parent = this;
	this.instance_1.setTransform(-540,-340);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(60));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-540,-340,1080,680);


(lib.Symbol5copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_3 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("Axdv/ITMAAII7jeMAIoAkBMgmjAC6g");
	mask.setTransform(14.825,92.825);

	// pack_png
	this.instance = new lib.pack();
	this.instance.parent = this;
	this.instance.setTransform(-167,-231);

	var maskedShapeInstanceList = [this.instance];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol5copy, new cjs.Rectangle(-108.6,-31.8,246.9,249.3), null);


(lib.Symbol4_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_3
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(12,1,1).p("AhFAAICLAA");
	this.shape_1.setTransform(13.3,14.95);

	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(1));

	// Layer_2
	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(12,1,1).p("AgiCEQgeiSBqh1");
	this.shape_2.setTransform(11.8777,-0.1);

	this.timeline.addTween(cjs.Tween.get(this.shape_2).wait(1));

	// Layer_1
	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(12,1,1).p("AAbCWQhjiMBSif");
	this.shape_3.setTransform(-0.0322,-0.025);

	this.timeline.addTween(cjs.Tween.get(this.shape_3).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol4_1, new cjs.Rectangle(-8.7,-21,35,42), null);


(lib.Symbol3copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_3
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(11.5,1,1).p("AAlAxQAQg2hbgr");
	this.shape.setTransform(3.1679,-11.8);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	// Layer_2
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(11.5,1,1).p("AAIhvQAHBVgYCK");
	this.shape_1.setTransform(7.7166,3.375);

	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(1));

	// Layer_1
	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(11.5,1,1).p("ABZhWQg0Bjh9BK");
	this.shape_2.setTransform(0.025,0);

	this.timeline.addTween(cjs.Tween.get(this.shape_2).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol3copy, new cjs.Rectangle(-14.6,-22.4,29.299999999999997,42.8), null);


(lib.Symbol2_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_3
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(11.5,1,1).p("AAzg6QglBBhAA0");
	this.shape_1.setTransform(-0.525,17.2);

	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(1));

	// Layer_2
	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(11.5,1,1).p("AgcBnQBWhtgthg");
	this.shape_2.setTransform(7.8056,1.375);

	this.timeline.addTween(cjs.Tween.get(this.shape_2).wait(1));

	// Layer_1
	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(11.5,1,1).p("AAjB0QAlithzg6");
	this.shape_3.setTransform(0.0239,0.025);

	this.timeline.addTween(cjs.Tween.get(this.shape_3).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol2_1, new cjs.Rectangle(-11.3,-17.3,27.7,46.2), null);


(lib.Symbol35 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2_obj_
	this.Layer_2 = new lib.Symbol_35_Layer_2();
	this.Layer_2.name = "Layer_2";
	this.Layer_2.parent = this;
	this.Layer_2.setTransform(-1.5,1,1,1,0,0,0,-1.5,1);
	this.Layer_2.depth = 0;
	this.Layer_2.isAttachedToCamera = 0
	this.Layer_2.isAttachedToMask = 0
	this.Layer_2.layerDepth = 0
	this.Layer_2.layerIndex = 0
	this.Layer_2.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_2).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol35, new cjs.Rectangle(-168.3,-78.1,336.70000000000005,156.2), null);


(lib.Symbol20 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Symbol14();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol20, new cjs.Rectangle(-86,-78,172,156), null);


(lib.Symbol13 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_74 = function() {
		this.___loopingOver___ = true;
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(74).call(this.frame_74).wait(1));

	// Layer_3_obj_
	this.Layer_3 = new lib.Symbol_13_Layer_3copy();
	this.Layer_3.name = "Layer_3";
	this.Layer_3.parent = this;
	this.Layer_3.setTransform(48.2,37.4,1,1,0,0,0,48.2,37.4);
	this.Layer_3.depth = 0;
	this.Layer_3.isAttachedToCamera = 0
	this.Layer_3.isAttachedToMask = 0
	this.Layer_3.layerDepth = 0
	this.Layer_3.layerIndex = 0
	this.Layer_3.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_3).wait(75));

	// Layer_2_obj_
	this.Layer_2 = new lib.Symbol_13_Layer_2copy();
	this.Layer_2.name = "Layer_2";
	this.Layer_2.parent = this;
	this.Layer_2.setTransform(73.3,-14.2,1,1,0,0,0,73.3,-14.2);
	this.Layer_2.depth = 0;
	this.Layer_2.isAttachedToCamera = 0
	this.Layer_2.isAttachedToMask = 0
	this.Layer_2.layerDepth = 0
	this.Layer_2.layerIndex = 1
	this.Layer_2.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_2).wait(75));

	// Layer_1_obj_
	this.Layer_1 = new lib.Symbol_13_Layer_1copy();
	this.Layer_1.name = "Layer_1";
	this.Layer_1.parent = this;
	this.Layer_1.depth = 0;
	this.Layer_1.isAttachedToCamera = 0
	this.Layer_1.isAttachedToMask = 0
	this.Layer_1.layerDepth = 0
	this.Layer_1.layerIndex = 2
	this.Layer_1.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_1).wait(75));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-25.1,-33.5,123.5,88.9);


(lib.Symbol8 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_4 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("AsBpNIT3j6IEMUmI3vFpg");
	mask.setTransform(-244.925,53.95);

	// Layer_2
	this.instance = new lib.Symbol7();
	this.instance.parent = this;
	this.instance.setTransform(-208.45,43.55,1,1,0,-27.7252,152.2748,0.1,0.1);

	var maskedShapeInstanceList = [this.instance];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	// Layer_3 (mask)
	var mask_1 = new cjs.Shape();
	mask_1._off = true;
	mask_1.graphics.p("ApXmWIRpneIBGY/IurCqg");
	mask_1.setTransform(25.025,-23.475);

	// Layer_1
	this.instance_1 = new lib.Symbol7();
	this.instance_1.parent = this;
	this.instance_1.setTransform(7.8,0);

	var maskedShapeInstanceList = [this.instance_1];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask_1;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol8, new cjs.Rectangle(-266,-51.9,311.6,158.9), null);


(lib.Symbol5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Symbol_3 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("AiMCNQg7g7AAhSQAAhRA7g7QA6g7BSAAQBTAAA6A7QA7A7gBBRQABBSg7A7Qg6A6hTABQhSgBg6g6g");
	mask.setTransform(-5.15,13.15);

	// Layer_5
	this.instance = new lib.Symbol6();
	this.instance.parent = this;
	this.instance.setTransform(5.15,-13.15);

	var maskedShapeInstanceList = [this.instance];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(14).to({x:8.15,y:-24.55},10,cjs.Ease.get(1)).wait(30).to({x:5.15,y:-13.15},11,cjs.Ease.get(1)).wait(10));

	// Symbol_2
	this.instance_1 = new lib.Symbol2();
	this.instance_1.parent = this;
	this.instance_1.setTransform(-6.3,7.05);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(14).to({x:-11.7,y:17.85},10,cjs.Ease.get(1)).wait(10).to({x:-5.4,y:14.55},10,cjs.Ease.get(1)).wait(10).to({x:-6.3,y:7.05},11,cjs.Ease.get(1)).wait(10));

	// Symbol_4
	this.instance_2 = new lib.Symbol4();
	this.instance_2.parent = this;
	this.instance_2.setTransform(-5.15,13.15);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(75));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-25.1,-6.8,39.900000000000006,39.9);


(lib.Symbol_36_Layer_2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.instance = new lib.Symbol39();
	this.instance.parent = this;
	this.instance.setTransform(-2.05,4.45,0.925,0.925);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol_36_Layer_2, null, null);


(lib.Symbol_15_Layer_5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_5
	this.instance = new lib.Symbol18();
	this.instance.parent = this;
	this.instance.setTransform(4.7,10.8,1,1,0,0,0,-2.5,11.7);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(50).to({scaleX:0.1391,x:4.75},3).to({scaleX:1,x:4.7},3).to({scaleX:0.1391,x:4.75},3).to({scaleX:1,x:4.7},3).to({scaleX:0.1391,x:4.75},3).to({scaleX:1,x:4.7},5).wait(5));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_15_Layer_4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_4
	this.instance = new lib.Symbol17();
	this.instance.parent = this;
	this.instance.setTransform(5.6,11.2,1,1,0,0,0,-1.6,12.1);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(50).to({regX:-1.2,regY:12,scaleX:0.3744,skewY:-76.7409,x:5.8,y:11.15},3).to({regX:-2.5,regY:11.7,scaleX:1,skewY:0,x:4.7,y:10.8},3).to({regX:-1.2,regY:12,scaleX:0.3744,skewY:-76.7409,x:5.8,y:11.15},3).to({regX:-2.5,regY:11.7,scaleX:1,skewY:0,x:4.7,y:10.8},3).to({regX:-1.2,regY:12,scaleX:0.3744,skewY:-76.7409,x:5.8,y:11.15},3).to({regX:-2.5,regY:11.7,scaleX:1,skewY:0,x:4.7,y:10.8},5).wait(5));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_15_Layer_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Symbol16();
	this.instance.parent = this;
	this.instance.setTransform(5.4,13,1,1,29.9992,0,0,5.4,13);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(75));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_8_Symbol_3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Symbol_3
	this.instance = new lib.Symbol3copy();
	this.instance.parent = this;
	this.instance.setTransform(64.05,57.75,1,1,0,0,0,12.3,-8.9);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(32).to({rotation:19.9563,x:7.4,y:66.5},6).wait(2).to({rotation:34.9552,x:19.05,y:64},3).to({regY:-8.8,rotation:41.9229,x:14.9,y:49.95},3).to({x:0.1,y:58.85},3).to({regY:-8.9,rotation:19.9563,x:7.4,y:66.5},3).to({rotation:34.9552,x:19.05,y:64},3).to({regY:-8.8,rotation:41.9229,x:14.9,y:49.95},3).to({x:0.1,y:58.85},3).to({regY:-8.9,rotation:19.9563,x:7.4,y:66.5},3).wait(13).to({rotation:0,x:64.05,y:57.75},6,cjs.Ease.get(1)).wait(2));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_8_Symbol_2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Symbol_2
	this.instance = new lib.Symbol2_1();
	this.instance.parent = this;
	this.instance.setTransform(-158.45,-121.9,1,1,0,0,0,7.2,17.4);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(18).to({regX:2.5,regY:5.8,scaleX:0.9998,scaleY:0.9998,rotation:2.8987,x:-169.55,y:-122.25},0).wait(1).to({scaleX:0.9995,scaleY:0.9995,rotation:5.5975,x:-175.5,y:-111.85},0).wait(1).to({scaleX:0.9993,scaleY:0.9993,rotation:8.0963,x:-181.05,y:-102.15},0).wait(1).to({scaleX:0.9991,scaleY:0.9991,rotation:10.3953,x:-186.1,y:-93.2},0).wait(1).to({scaleX:0.9989,scaleY:0.9989,rotation:12.4944,x:-190.7,y:-85},0).wait(1).to({scaleX:0.9987,scaleY:0.9987,rotation:14.3935,x:-194.95,y:-77.65},0).wait(1).to({regX:7.2,regY:17.3,scaleX:0.9985,scaleY:0.9985,rotation:16.0927,x:-197.4,y:-58.6},0).to({scaleX:1,scaleY:1,rotation:-82.2098,x:-210.45,y:-36.45},8,cjs.Ease.get(1)).wait(32).to({scaleX:0.9985,scaleY:0.9985,rotation:-13.9065,x:-197.35,y:-58.6},7,cjs.Ease.get(-1)).to({regY:17.4,scaleX:1,scaleY:1,rotation:0,x:-158.45,y:-121.9},6,cjs.Ease.get(1)).wait(8));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_3_Layer_2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.instance = new lib.Symbol4_1();
	this.instance.parent = this;
	this.instance.setTransform(-37.3,-40.7,1,1,0,0,0,-0.2,18.3);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(19).to({rotation:-34.2068,x:-57.3,y:-44.3},6,cjs.Ease.get(-1)).to({rotation:0,x:-37.3,y:-40.7},6,cjs.Ease.get(1)).to({rotation:-34.2068,x:-57.3,y:-44.3},7,cjs.Ease.get(-1)).to({rotation:0,x:-37.3,y:-40.7},8,cjs.Ease.get(1)).wait(29));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_1_Layer_5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_5
	this.instance = new lib.Symbol5();
	this.instance.parent = this;
	this.instance.setTransform(5.15,-13.15);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(75));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_1_Layer_4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_4
	this.instance = new lib.Symbol5();
	this.instance.parent = this;
	this.instance.setTransform(47.75,-2.65);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(75));

}).prototype = p = new cjs.MovieClip();


(lib.Scene_1_Layer_29 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_29
	this.instance = new lib.Symbol38();
	this.instance.parent = this;
	this.instance.setTransform(186.95,554.75);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(310).to({_off:false},0).to({_off:true},300).wait(5));

}).prototype = p = new cjs.MovieClip();


(lib.Scene_1_Layer_28 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_28
	this.instance = new lib.Symbol37();
	this.instance.parent = this;
	this.instance.setTransform(85.05,635.05,0.0325,0.0325);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(605).to({_off:false},0).to({scaleX:1,scaleY:1},9,cjs.Ease.get(1)).wait(1));

}).prototype = p = new cjs.MovieClip();


(lib.Scene_1_Layer_27 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_27
	this.instance = new lib.Symbol37copy();
	this.instance.parent = this;
	this.instance.setTransform(145.05,635.05,0.0325,0.0325);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(455).to({_off:false},0).to({scaleX:1,scaleY:1},9,cjs.Ease.get(1)).wait(135).to({scaleX:0.0325,scaleY:0.0325},9,cjs.Ease.get(-1)).to({_off:true},1).wait(6));

}).prototype = p = new cjs.MovieClip();


(lib.Scene_1_Layer_26 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_26
	this.instance = new lib.Symbol37copy();
	this.instance.parent = this;
	this.instance.setTransform(124.85,635.05,0.0325,0.0325);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(305).to({_off:false},0).to({scaleX:1,scaleY:1},9,cjs.Ease.get(1)).wait(135).to({scaleX:0.0325,scaleY:0.0325},9,cjs.Ease.get(-1)).to({_off:true},1).wait(156));

}).prototype = p = new cjs.MovieClip();


(lib.Scene_1_Layer_25 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_25
	this.instance = new lib.Symbol37();
	this.instance.parent = this;
	this.instance.setTransform(104.85,635.05,0.0325,0.0325);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(155).to({_off:false},0).to({scaleX:1,scaleY:1},9,cjs.Ease.get(1)).wait(135).to({scaleX:0.0325,scaleY:0.0325},9,cjs.Ease.get(-1)).to({_off:true},1).wait(306));

}).prototype = p = new cjs.MovieClip();


(lib.Scene_1_Layer_24 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_24
	this.instance = new lib.Symbol37();
	this.instance.parent = this;
	this.instance.setTransform(85.05,635.05);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(149).to({scaleX:0.0325,scaleY:0.0325},9,cjs.Ease.get(-1)).to({_off:true},1).wait(456));

}).prototype = p = new cjs.MovieClip();


(lib.Scene_1_Layer_23 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_23
	this.instance = new lib.Symbol33();
	this.instance.parent = this;
	this.instance.setTransform(1272.95,395.95,0.92,0.92);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(599).to({_off:false},0).wait(1).to({x:1262.5859},0).wait(1).to({x:1230.147},0).wait(1).to({x:1174.128},0).wait(1).to({x:1094.3519},0).wait(1).to({x:992.9377},0).wait(1).to({x:875.0682},0).wait(1).to({x:748.8242},0).wait(1).to({x:623.6585},0).wait(1).to({x:508.1503},0).wait(1).to({x:408.3604},0).wait(1).to({x:327.4729},0).wait(1).to({x:266.3763},0).wait(1).to({x:224.5098},0).wait(1).to({x:200.5662},0).wait(1).to({x:192.95},0).wait(1));

}).prototype = p = new cjs.MovieClip();


(lib.Scene_1_Layer_21 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_21
	this.instance = new lib.Symbol35();
	this.instance.parent = this;
	this.instance.setTransform(1323.35,283.65);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(299).to({_off:false},0).wait(1).to({regX:-1.5,regY:1,x:1311.45,y:284.65},0).wait(1).to({x:1279},0).wait(1).to({x:1223},0).wait(1).to({x:1143.25},0).wait(1).to({x:1041.8},0).wait(1).to({x:923.95},0).wait(1).to({x:797.7},0).wait(1).to({x:672.55},0).wait(1).to({x:557.05},0).wait(1).to({x:457.25},0).wait(1).to({x:376.35},0).wait(1).to({x:315.25},0).wait(1).to({x:273.4},0).wait(1).to({x:249.45},0).wait(1).to({regX:0,regY:0,x:243.35,y:283.65},0).wait(301));

}).prototype = p = new cjs.MovieClip();


(lib.Scene_1_Layer_19 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_19
	this.instance = new lib.Symbol33();
	this.instance.parent = this;
	this.instance.setTransform(192.95,395.95,0.92,0.92);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({_off:true},165).wait(450));

}).prototype = p = new cjs.MovieClip();


(lib.Scene_1_Layer_18 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_18
	this.instance = new lib.Symbol32();
	this.instance.parent = this;
	this.instance.setTransform(187,555,0.92,0.92,0,0,0,0.1,0.1);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({_off:true},310).wait(300).to({_off:false},0).wait(5));

}).prototype = p = new cjs.MovieClip();


(lib.Scene_1_Layer_17 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_17
	this.instance = new lib.Symbol31();
	this.instance.parent = this;
	this.instance.setTransform(85,635);

	this.instance_1 = new lib.Symbol31copy();
	this.instance_1.parent = this;
	this.instance_1.setTransform(85,635);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},311).to({state:[{t:this.instance}]},299).wait(5));

}).prototype = p = new cjs.MovieClip();


(lib.Scene_1_Layer_14 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_14
	this.instance = new lib.Symbol30();
	this.instance.parent = this;
	this.instance.setTransform(1620,340);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(599).to({_off:false},0).wait(1).to({x:1609.6359},0).wait(1).to({x:1577.197},0).wait(1).to({x:1521.178},0).wait(1).to({x:1441.4019},0).wait(1).to({x:1339.9877},0).wait(1).to({x:1222.1182},0).wait(1).to({x:1095.8742},0).wait(1).to({x:970.7085},0).wait(1).to({x:855.2003},0).wait(1).to({x:755.4104},0).wait(1).to({x:674.5229},0).wait(1).to({x:613.4263},0).wait(1).to({x:571.5598},0).wait(1).to({x:547.6162},0).wait(1).to({x:540},0).wait(1));

}).prototype = p = new cjs.MovieClip();


(lib.Scene_1_Layer_13 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_13
	this.instance = new lib.Symbol34();
	this.instance.parent = this;
	this.instance.setTransform(1301,408);
	this.instance._off = true;

	this.instance_1 = new lib.Symbol28();
	this.instance_1.parent = this;
	this.instance_1.setTransform(540,340);
	this.instance_1.alpha = 0;
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(299).to({_off:false},0).wait(1).to({x:1290.5399},0).wait(1).to({x:1257.8007},0).wait(1).to({x:1201.2629},0).wait(1).to({x:1120.7482},0).wait(1).to({x:1018.395},0).wait(1).to({x:899.4341},0).wait(1).to({x:772.0212},0).wait(1).to({x:645.6965},0).wait(1).to({x:529.1188},0).wait(1).to({x:428.405},0).wait(1).to({x:346.7685},0).wait(1).to({x:285.1062},0).wait(1).to({x:242.852},0).wait(1).to({x:218.6867},0).wait(1).to({x:211},0).to({_off:true},135).wait(166));
	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(449).to({_off:false},0).to({alpha:1},15).wait(136).to({x:529.6359},0).wait(1).to({x:497.197},0).wait(1).to({x:441.178},0).wait(1).to({x:361.4019},0).wait(1).to({x:259.9877},0).wait(1).to({x:142.1182},0).wait(1).to({x:15.8742},0).wait(1).to({x:-109.2915},0).wait(1).to({x:-224.7997},0).wait(1).to({x:-324.5896},0).wait(1).to({x:-405.4771},0).wait(1).to({x:-466.5737},0).wait(1).to({x:-508.4402},0).wait(1).to({x:-532.3838},0).wait(1).to({x:-540},0).wait(1));

}).prototype = p = new cjs.MovieClip();


(lib.Scene_1_Layer_12 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_12
	this.instance = new lib.Symbol29();
	this.instance.parent = this;
	this.instance.setTransform(540,340);
	this.instance.alpha = 0;
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(149).to({_off:false},0).to({alpha:1},15).wait(136).to({regX:0.5,x:530.1},0).wait(1).to({x:497.65},0).wait(1).to({x:441.65},0).wait(1).to({x:361.9},0).wait(1).to({x:260.45},0).wait(1).to({x:142.6},0).wait(1).to({x:16.35},0).wait(1).to({x:-108.75},0).wait(1).to({x:-224.25},0).wait(1).to({x:-324.05},0).wait(1).to({x:-404.95},0).wait(1).to({x:-466.05},0).wait(1).to({x:-507.9},0).wait(1).to({x:-531.85},0).wait(1).to({regX:0,x:-540},0).wait(1));

}).prototype = p = new cjs.MovieClip();


(lib.Scene_1_Layer_11 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_11
	this.instance = new lib.Symbol9();
	this.instance.parent = this;
	this.instance.setTransform(1620,340);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(299).to({_off:false},0).wait(1).to({x:1609.6359},0).wait(1).to({x:1577.197},0).wait(1).to({x:1521.178},0).wait(1).to({x:1441.4019},0).wait(1).to({x:1339.9877},0).wait(1).to({x:1222.1182},0).wait(1).to({x:1095.8742},0).wait(1).to({x:970.7085},0).wait(1).to({x:855.2003},0).wait(1).to({x:755.4104},0).wait(1).to({x:674.5229},0).wait(1).to({x:613.4263},0).wait(1).to({x:571.5598},0).wait(1).to({x:547.6162},0).wait(1).to({x:540},0).wait(151));

}).prototype = p = new cjs.MovieClip();


(lib.Scene_1_Layer_10 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_10
	this.instance = new lib.Symbol8();
	this.instance.parent = this;
	this.instance.setTransform(1028.5,457.95);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({_off:true},165).wait(1));

}).prototype = p = new cjs.MovieClip();


(lib.Scene_1_Layer_8 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_8
	this.instance = new lib.Symbol13();
	this.instance.parent = this;
	this.instance.setTransform(874.85,407);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({_off:true},165).wait(1));

}).prototype = p = new cjs.MovieClip();


(lib.Scene_1_Layer_7 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_7
	this.instance = new lib.Symbol27();
	this.instance.parent = this;
	this.instance.setTransform(762.5,202.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({_off:true},165).wait(1));

}).prototype = p = new cjs.MovieClip();


(lib.Scene_1_Layer_2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.instance = new lib.Symbol26();
	this.instance.parent = this;
	this.instance.setTransform(540,340);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({_off:true},165).wait(1));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol13_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_74 = function() {
		this.___loopingOver___ = true;
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(74).call(this.frame_74).wait(1));

	// Layer_2_obj_
	this.Layer_2_1 = new lib.Symbol_13_Layer_2();
	this.Layer_2_1.name = "Layer_2_1";
	this.Layer_2_1.parent = this;
	this.Layer_2_1.setTransform(-3.5,-10.3,1,1,0,0,0,-3.5,-10.3);
	this.Layer_2_1.depth = 0;
	this.Layer_2_1.isAttachedToCamera = 0
	this.Layer_2_1.isAttachedToMask = 0
	this.Layer_2_1.layerDepth = 0
	this.Layer_2_1.layerIndex = 0
	this.Layer_2_1.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_2_1).wait(75));

	// Layer_1_obj_
	this.Layer_1_1 = new lib.Symbol_13_Layer_1();
	this.Layer_1_1.name = "Layer_1_1";
	this.Layer_1_1.parent = this;
	this.Layer_1_1.depth = 0;
	this.Layer_1_1.isAttachedToCamera = 0
	this.Layer_1_1.isAttachedToMask = 0
	this.Layer_1_1.layerDepth = 0
	this.Layer_1_1.layerIndex = 1
	this.Layer_1_1.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_1_1).wait(75));

	// Layer_3_obj_
	this.Layer_3_1 = new lib.Symbol_13_Layer_3();
	this.Layer_3_1.name = "Layer_3_1";
	this.Layer_3_1.parent = this;
	this.Layer_3_1.setTransform(10.3,-12.1,1,1,0,0,0,10.3,-12.1);
	this.Layer_3_1.depth = 0;
	this.Layer_3_1.isAttachedToCamera = 0
	this.Layer_3_1.isAttachedToMask = 0
	this.Layer_3_1.layerDepth = 0
	this.Layer_3_1.layerIndex = 2
	this.Layer_3_1.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_3_1).wait(75));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-23.9,-28.1,46.5,40);


(lib.Symbol3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		/* Stop at This Frame
		The  timeline will stop/pause at the frame where you insert this code.
		Can also be used to stop/pause the timeline of movieclips.
		*/
		
		this.stop();
	}
	this.frame_74 = function() {
		this.___loopingOver___ = true;
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(74).call(this.frame_74).wait(1));

	// Layer_2_obj_
	this.Layer_2 = new lib.Symbol_3_Layer_2();
	this.Layer_2.name = "Layer_2";
	this.Layer_2.parent = this;
	this.Layer_2.setTransform(-28.4,-59.1,1,1,0,0,0,-28.4,-59.1);
	this.Layer_2.depth = 0;
	this.Layer_2.isAttachedToCamera = 0
	this.Layer_2.isAttachedToMask = 0
	this.Layer_2.layerDepth = 0
	this.Layer_2.layerIndex = 0
	this.Layer_2.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_2).wait(75));

	// Layer_1_obj_
	this.Layer_1 = new lib.Symbol_3_Layer_1();
	this.Layer_1.name = "Layer_1";
	this.Layer_1.parent = this;
	this.Layer_1.depth = 0;
	this.Layer_1.isAttachedToCamera = 0
	this.Layer_1.isAttachedToMask = 0
	this.Layer_1.layerDepth = 0
	this.Layer_1.layerIndex = 1
	this.Layer_1.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_1).wait(75));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-86.5,-87.9,139.7,137.8);


(lib.Symbol1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_84 = function() {
		this.___loopingOver___ = true;
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(84).call(this.frame_84).wait(1));

	// Layer_3_obj_
	this.Layer_3 = new lib.Symbol_1_Layer_3();
	this.Layer_3.name = "Layer_3";
	this.Layer_3.parent = this;
	this.Layer_3.setTransform(42.6,37.1,1,1,0,0,0,42.6,37.1);
	this.Layer_3.depth = 0;
	this.Layer_3.isAttachedToCamera = 0
	this.Layer_3.isAttachedToMask = 0
	this.Layer_3.layerDepth = 0
	this.Layer_3.layerIndex = 0
	this.Layer_3.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_3).wait(85));

	// Layer_2_obj_
	this.Layer_2 = new lib.Symbol_1_Layer_2();
	this.Layer_2.name = "Layer_2";
	this.Layer_2.parent = this;
	this.Layer_2.setTransform(65.5,0.1,1,1,0,0,0,65.5,0.1);
	this.Layer_2.depth = 0;
	this.Layer_2.isAttachedToCamera = 0
	this.Layer_2.isAttachedToMask = 0
	this.Layer_2.layerDepth = 0
	this.Layer_2.layerIndex = 1
	this.Layer_2.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_2).wait(85));

	// Layer_1_obj_
	this.Layer_1 = new lib.Symbol_1_Layer_1();
	this.Layer_1.name = "Layer_1";
	this.Layer_1.parent = this;
	this.Layer_1.depth = 0;
	this.Layer_1.isAttachedToCamera = 0
	this.Layer_1.isAttachedToMask = 0
	this.Layer_1.layerDepth = 0
	this.Layer_1.layerIndex = 2
	this.Layer_1.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_1).wait(85));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-21.9,-14.8,110.69999999999999,63.099999999999994);


(lib.Symbol36 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2_obj_
	this.Layer_2 = new lib.Symbol_36_Layer_2();
	this.Layer_2.name = "Layer_2";
	this.Layer_2.parent = this;
	this.Layer_2.setTransform(-2,4.5,1,1,0,0,0,-2,4.5);
	this.Layer_2.depth = 0;
	this.Layer_2.isAttachedToCamera = 0
	this.Layer_2.isAttachedToMask = 0
	this.Layer_2.layerDepth = 0
	this.Layer_2.layerIndex = 0
	this.Layer_2.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_2).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol36, new cjs.Rectangle(-166.8,-78.1,333.6,156.2), null);


(lib.Symbol23 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("AgvC8IAAhaIgViAIADhZIAJhRQAHgmALgOIBCg/IAbBNIAOBlIgbDaIgSDSIg6AZg");
	mask.setTransform(5.075,39.75);

	// Layer_1
	this.instance = new lib.Symbol20();
	this.instance.parent = this;
	this.instance.setTransform(4.6,32.4,1,1,-31.7079,0,0,-13.1,30);

	var maskedShapeInstanceList = [this.instance];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol23, new cjs.Rectangle(-1.8,8.2,13.8,63.2), null);


(lib.Symbol22 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("AoSL6Igi4eIRpB8IAALSIgSBGIgOBPIAFBNIASBsIgDBjIhLFKg");
	mask.setTransform(-54.85,9.825);

	// Layer_1
	this.instance = new lib.Symbol20();
	this.instance.parent = this;
	this.instance.setTransform(4.6,32.4,1,1,-31.7079,0,0,-13.1,30);

	var maskedShapeInstanceList = [this.instance];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol22, new cjs.Rectangle(-111.3,-70.6,112.89999999999999,160.89999999999998), null);


(lib.Symbol21 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("AkpNNIgileIAbkfIgPgiIAdvDII4hCIA2avg");
	mask.setTransform(42,3.825);

	// Layer_1
	this.instance = new lib.Symbol20();
	this.instance.parent = this;
	this.instance.setTransform(4.6,32.4,1,1,-31.7079,0,0,-13.1,30);

	var maskedShapeInstanceList = [this.instance];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol21, new cjs.Rectangle(8.8,-81.7,66.4,171.10000000000002), null);


(lib.Symbol19copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_5
	this.instance = new lib.Symbol22();
	this.instance.parent = this;
	this.instance.setTransform(-13.4,26.8,1,1,0,0,0,4.3,29.2);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({regX:4.1,scaleX:0.3969,skewY:73.9281,x:-13.5,y:26.85},4).to({regX:4.3,scaleX:1,skewY:0,x:-13.4,y:26.8},4).wait(1));

	// Layer_4
	this.instance_1 = new lib.Symbol21();
	this.instance_1.parent = this;
	this.instance_1.setTransform(-13.4,26.8,1,1,0,0,0,4.3,29.2);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).to({regX:4.2,scaleX:0.1316,x:-13.5},4).to({regX:4.3,scaleX:1,x:-13.4},4).wait(1));

	// Layer_1
	this.instance_2 = new lib.Symbol23();
	this.instance_2.parent = this;
	this.instance_2.setTransform(-17.7,-2.4);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(9));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-131.8,-117,228.3,226.2);


(lib.Symbol15 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_74 = function() {
		this.___loopingOver___ = true;
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(74).call(this.frame_74).wait(1));

	// Layer_5_obj_
	this.Layer_5 = new lib.Symbol_15_Layer_5();
	this.Layer_5.name = "Layer_5";
	this.Layer_5.parent = this;
	this.Layer_5.setTransform(7.2,-0.9,1,1,0,0,0,7.2,-0.9);
	this.Layer_5.depth = 0;
	this.Layer_5.isAttachedToCamera = 0
	this.Layer_5.isAttachedToMask = 0
	this.Layer_5.layerDepth = 0
	this.Layer_5.layerIndex = 0
	this.Layer_5.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_5).wait(75));

	// Layer_4_obj_
	this.Layer_4 = new lib.Symbol_15_Layer_4();
	this.Layer_4.name = "Layer_4";
	this.Layer_4.parent = this;
	this.Layer_4.setTransform(7.2,-0.9,1,1,0,0,0,7.2,-0.9);
	this.Layer_4.depth = 0;
	this.Layer_4.isAttachedToCamera = 0
	this.Layer_4.isAttachedToMask = 0
	this.Layer_4.layerDepth = 0
	this.Layer_4.layerIndex = 1
	this.Layer_4.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_4).wait(75));

	// Layer_1_obj_
	this.Layer_1 = new lib.Symbol_15_Layer_1();
	this.Layer_1.name = "Layer_1";
	this.Layer_1.parent = this;
	this.Layer_1.setTransform(7.2,-1,1,1,0,0,0,7.2,-1);
	this.Layer_1.depth = 0;
	this.Layer_1.isAttachedToCamera = 0
	this.Layer_1.isAttachedToMask = 0
	this.Layer_1.layerDepth = 0
	this.Layer_1.layerIndex = 2
	this.Layer_1.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_1).wait(75));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-42.3,-67.5,99,132.4);


(lib.Symbol1_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_74 = function() {
		this.___loopingOver___ = true;
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(74).call(this.frame_74).wait(1));

	// Layer_6_obj_
	this.Layer_6 = new lib.Symbol_1_Layer_6();
	this.Layer_6.name = "Layer_6";
	this.Layer_6.parent = this;
	this.Layer_6.setTransform(33,54.8,1,1,0,0,0,33,54.8);
	this.Layer_6.depth = 0;
	this.Layer_6.isAttachedToCamera = 0
	this.Layer_6.isAttachedToMask = 0
	this.Layer_6.layerDepth = 0
	this.Layer_6.layerIndex = 0
	this.Layer_6.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_6).wait(75));

	// Layer_4_obj_
	this.Layer_4 = new lib.Symbol_1_Layer_4();
	this.Layer_4.name = "Layer_4";
	this.Layer_4.parent = this;
	this.Layer_4.setTransform(47.8,-2.6,1,1,0,0,0,47.8,-2.6);
	this.Layer_4.depth = 0;
	this.Layer_4.isAttachedToCamera = 0
	this.Layer_4.isAttachedToMask = 0
	this.Layer_4.layerDepth = 0
	this.Layer_4.layerIndex = 1
	this.Layer_4.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_4).wait(75));

	// Layer_5_obj_
	this.Layer_5 = new lib.Symbol_1_Layer_5();
	this.Layer_5.name = "Layer_5";
	this.Layer_5.parent = this;
	this.Layer_5.setTransform(5.2,-13.2,1,1,0,0,0,5.2,-13.2);
	this.Layer_5.depth = 0;
	this.Layer_5.isAttachedToCamera = 0
	this.Layer_5.isAttachedToMask = 0
	this.Layer_5.layerDepth = 0
	this.Layer_5.layerIndex = 2
	this.Layer_5.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_5).wait(75));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-19.9,-46.2,92.80000000000001,119.10000000000001);


(lib.Symbol_25_Layer_2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.instance = new lib.Symbol19copy();
	this.instance.parent = this;
	this.instance.setTransform(20.85,26.6,1.0699,1.0699,0,0,0,-11.8,28.4);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1).to({regX:-35.8,regY:-14.5,scaleX:1.0625,scaleY:1.0625,x:-4.35,y:-18.95},0).wait(1).to({scaleX:1.035,scaleY:1.035,x:-2.5,y:-17.8},0).wait(1).to({scaleX:1.0074,scaleY:1.0074,x:-0.65,y:-16.6},0).wait(1).to({regX:-12,regY:28.4,scaleX:1,scaleY:1,x:23.75,y:26.55},0).wait(1).to({regX:-35.8,regY:-14.5,scaleX:1.0074,scaleY:1.0074,x:-0.55,y:-16.65},0).wait(1).to({scaleX:1.035,scaleY:1.035,x:-2.4,y:-17.85},0).wait(1).to({scaleX:1.0625,scaleY:1.0625,x:-4.25,y:-19},0).wait(1).to({regX:-11.8,regY:28.4,scaleX:1.0699,scaleY:1.0699,x:20.85,y:26.6},0).wait(1));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_14_Layer_4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_4
	this.instance = new lib.Symbol3();
	this.instance.parent = this;
	this.instance.setTransform(47.1,59,1,1,0,0,0,47.1,43.9);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({_off:true},1).wait(74));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_8_Symbol_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Symbol_1
	this.instance = new lib.Symbol1();
	this.instance.parent = this;
	this.instance.setTransform(-83.9,-65.15);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(85));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_5_Layer_2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.instance = new lib.Symbol13_1();
	this.instance.parent = this;
	this.instance.setTransform(-53.55,13.1,1,1,6.7397,0,0,15,-0.4);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({regY:-0.5,rotation:0,x:-46.3,y:38},7,cjs.Ease.get(1)).wait(46).to({regY:-0.4,rotation:6.7397,x:-53.55,y:13.1},6,cjs.Ease.get(1)).wait(16));

}).prototype = p = new cjs.MovieClip();


(lib.Scene_1_Layer_22 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_22
	this.instance = new lib.Symbol36();
	this.instance.parent = this;
	this.instance.setTransform(1321.85,270.65);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(599).to({_off:false},0).wait(1).to({regX:-2.1,regY:4.5,x:1309.35,y:275.15},0).wait(1).to({x:1276.9},0).wait(1).to({x:1220.9},0).wait(1).to({x:1141.15},0).wait(1).to({x:1039.7},0).wait(1).to({x:921.85},0).wait(1).to({x:795.6},0).wait(1).to({x:670.45},0).wait(1).to({x:554.95},0).wait(1).to({x:455.15},0).wait(1).to({x:374.25},0).wait(1).to({x:313.15},0).wait(1).to({x:271.3},0).wait(1).to({x:247.35},0).wait(1).to({regX:0,regY:0,x:241.85,y:270.65},0).wait(1));

}).prototype = p = new cjs.MovieClip();


(lib.Scene_1_Layer_16 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_16
	this.instance = new lib.Symbol36();
	this.instance.parent = this;
	this.instance.setTransform(241.85,270.65);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({_off:true},165).wait(450));

}).prototype = p = new cjs.MovieClip();


(lib.Scene_1_Layer_6 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_6
	this.instance = new lib.Symbol1_1();
	this.instance.parent = this;
	this.instance.setTransform(671.5,232.2);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({_off:true},165).wait(1));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol14_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		/* Stop at This Frame
		The  timeline will stop/pause at the frame where you insert this code.
		Can also be used to stop/pause the timeline of movieclips.
		*/
		
		this.stop();
	}
	this.frame_74 = function() {
		this.___loopingOver___ = true;
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(74).call(this.frame_74).wait(1));

	// Layer_5 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	var mask_graphics_0 = new cjs.Graphics().p("A1QInITR7iIXQK0IqMbDg");

	this.timeline.addTween(cjs.Tween.get(mask).to({graphics:mask_graphics_0,x:-49.475,y:-18.975}).wait(1).to({graphics:null,x:0,y:0}).wait(74));

	// Layer_4_obj_
	this.Layer_4 = new lib.Symbol_14_Layer_4();
	this.Layer_4.name = "Layer_4";
	this.Layer_4.parent = this;
	this.Layer_4.depth = 0;
	this.Layer_4.isAttachedToCamera = 0
	this.Layer_4.isAttachedToMask = 0
	this.Layer_4.layerDepth = 0
	this.Layer_4.layerIndex = 0
	this.Layer_4.maskLayerName = 0

	var maskedShapeInstanceList = [this.Layer_4];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.Layer_4).wait(75));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-185.6,-140.2,272.3,242.5);


(lib.Symbol8_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_84 = function() {
		this.___loopingOver___ = true;
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(84).call(this.frame_84).wait(1));

	// Symbol_3_obj_
	this.Symbol_3 = new lib.Symbol_8_Symbol_3();
	this.Symbol_3.name = "Symbol_3";
	this.Symbol_3.parent = this;
	this.Symbol_3.setTransform(51.8,65.6,1,1,0,0,0,51.8,65.6);
	this.Symbol_3.depth = 0;
	this.Symbol_3.isAttachedToCamera = 0
	this.Symbol_3.isAttachedToMask = 0
	this.Symbol_3.layerDepth = 0
	this.Symbol_3.layerIndex = 0
	this.Symbol_3.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Symbol_3).wait(85));

	// Symbol_6_obj_
	this.Symbol_6 = new lib.Symbol_8_Symbol_6();
	this.Symbol_6.name = "Symbol_6";
	this.Symbol_6.parent = this;
	this.Symbol_6.setTransform(91.4,5.5,1,1,0,0,0,91.4,5.5);
	this.Symbol_6.depth = 0;
	this.Symbol_6.isAttachedToCamera = 0
	this.Symbol_6.isAttachedToMask = 0
	this.Symbol_6.layerDepth = 0
	this.Symbol_6.layerIndex = 1
	this.Symbol_6.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Symbol_6).wait(85));

	// Symbol_2_obj_
	this.Symbol_2 = new lib.Symbol_8_Symbol_2();
	this.Symbol_2.name = "Symbol_2";
	this.Symbol_2.parent = this;
	this.Symbol_2.setTransform(-163.2,-133.5,1,1,0,0,0,-163.2,-133.5);
	this.Symbol_2.depth = 0;
	this.Symbol_2.isAttachedToCamera = 0
	this.Symbol_2.isAttachedToMask = 0
	this.Symbol_2.layerDepth = 0
	this.Symbol_2.layerIndex = 2
	this.Symbol_2.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Symbol_2).wait(18).to({regX:-198.3,regY:-86.5,x:-198.3,y:-86.5},0).wait(6).to({regX:-163.2,regY:-133.5,x:-163.2,y:-133.5},0).wait(61));

	// Symbol_1_obj_
	this.Symbol_1 = new lib.Symbol_8_Symbol_1();
	this.Symbol_1.name = "Symbol_1";
	this.Symbol_1.parent = this;
	this.Symbol_1.setTransform(-50.5,-48.5,1,1,0,0,0,-50.5,-48.5);
	this.Symbol_1.depth = 0;
	this.Symbol_1.isAttachedToCamera = 0
	this.Symbol_1.isAttachedToMask = 0
	this.Symbol_1.layerDepth = 0
	this.Symbol_1.layerIndex = 3
	this.Symbol_1.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Symbol_1).wait(85));

	// Layer_8 (mask)
	var mask_2 = new cjs.Shape();
	mask_2._off = true;
	mask_2.graphics.p("An/S5MgMjglkMApFgANMgAbAiTIo7Deg");
	mask_2.setTransform(-40.675,-120.275);

	// pack_png_obj_
	this.pack_png = new lib.Symbol_8_pack_png();
	this.pack_png.name = "pack_png";
	this.pack_png.parent = this;
	this.pack_png.setTransform(5.1,9.2,1,1,0,0,0,5.1,9.2);
	this.pack_png.depth = 0;
	this.pack_png.isAttachedToCamera = 0
	this.pack_png.isAttachedToMask = 0
	this.pack_png.layerDepth = 0
	this.pack_png.layerIndex = 4
	this.pack_png.maskLayerName = 0

	var maskedShapeInstanceList = [this.pack_png];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask_2;
	}

	this.timeline.addTween(cjs.Tween.get(this.pack_png).wait(85));

	// Symbol_7_obj_
	this.Symbol_7 = new lib.Symbol_8_Symbol_7();
	this.Symbol_7.name = "Symbol_7";
	this.Symbol_7.parent = this;
	this.Symbol_7.setTransform(-121.8,-78.9,1,1,0,0,0,-121.8,-78.9);
	this.Symbol_7.depth = 0;
	this.Symbol_7.isAttachedToCamera = 0
	this.Symbol_7.isAttachedToMask = 0
	this.Symbol_7.layerDepth = 0
	this.Symbol_7.layerIndex = 5
	this.Symbol_7.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Symbol_7).wait(85));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-247.1,-220.8,372.9,315.6);


(lib.Symbol5_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_74 = function() {
		this.___loopingOver___ = true;
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(74).call(this.frame_74).wait(1));

	// Layer_3 (mask)
	var mask_1 = new cjs.Shape();
	mask_1._off = true;
	mask_1.graphics.p("A2jGAIPj41ISBO7ICblCIJID+IrGX1g");
	mask_1.setTransform(-31.05,-10.15);

	// Layer_2_obj_
	this.Layer_2 = new lib.Symbol_5_Layer_2();
	this.Layer_2.name = "Layer_2";
	this.Layer_2.parent = this;
	this.Layer_2.setTransform(-68.1,3.6,1,1,0,0,0,-68.1,3.6);
	this.Layer_2.depth = 0;
	this.Layer_2.isAttachedToCamera = 0
	this.Layer_2.isAttachedToMask = 0
	this.Layer_2.layerDepth = 0
	this.Layer_2.layerIndex = 0
	this.Layer_2.maskLayerName = 0

	var maskedShapeInstanceList = [this.Layer_2];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask_1;
	}

	this.timeline.addTween(cjs.Tween.get(this.Layer_2).wait(75));

	// Layer_4_obj_
	this.Layer_4 = new lib.Symbol_5_Layer_4();
	this.Layer_4.name = "Layer_4";
	this.Layer_4.parent = this;
	this.Layer_4.setTransform(-2.6,-22.6,1,1,0,0,0,-2.6,-22.6);
	this.Layer_4.depth = 0;
	this.Layer_4.isAttachedToCamera = 0
	this.Layer_4.isAttachedToMask = 0
	this.Layer_4.layerDepth = 0
	this.Layer_4.layerIndex = 1
	this.Layer_4.maskLayerName = 0

	var maskedShapeInstanceList = [this.Layer_4];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask_1;
	}

	this.timeline.addTween(cjs.Tween.get(this.Layer_4).wait(75));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-90.5,-74.4,152,124.80000000000001);


(lib.Symbol4copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Symbol_3
	this.instance = new lib.Symbol8_1();
	this.instance.parent = this;
	this.instance.setTransform(-91.9,-9.5,1,1,0,0,0,-91.5,0.7);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(9).to({rotation:3.4528,x:-91.45},15,cjs.Ease.get(1)).wait(40).to({rotation:0,x:-91.9},12,cjs.Ease.get(1)).wait(9));

	// Layer_8
	this.instance_1 = new lib.Symbol5copy();
	this.instance_1.parent = this;
	this.instance_1.setTransform(4.7,0);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(85));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-177.4,-255.8,362.5,500.20000000000005);


(lib.Symbol25 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_8 = function() {
		this.___loopingOver___ = true;
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(8).call(this.frame_8).wait(1));

	// Layer_2_obj_
	this.Layer_2 = new lib.Symbol_25_Layer_2();
	this.Layer_2.name = "Layer_2";
	this.Layer_2.parent = this;
	this.Layer_2.setTransform(14.6,-6.4,1,1,0,0,0,14.6,-6.4);
	this.Layer_2.depth = 0;
	this.Layer_2.isAttachedToCamera = 0
	this.Layer_2.isAttachedToMask = 0
	this.Layer_2.layerDepth = 0
	this.Layer_2.layerIndex = 0
	this.Layer_2.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_2).wait(1).to({regX:-4.8,regY:-19.4,x:-4.8,y:-19.4},0).wait(3).to({regX:14.6,regY:-6.4,x:14.6,y:-6.4},0).wait(1).to({regX:-4.8,regY:-19.4,x:-4.8,y:-19.4},0).wait(3).to({regX:14.6,regY:-6.4,x:14.6,y:-6.4},0).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-107.6,-125.7,244.29999999999998,238.7);


(lib.Symbol24 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Symbol15();
	this.instance.parent = this;
	this.instance.setTransform(-1.3,13.5,1,1,-29.9999,0,0,5.1,10.8);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol24, new cjs.Rectangle(-72.2,-64.2,133.9,133), null);


(lib.Symbol_10_Layer_6 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_6
	this.instance = new lib.Symbol14_1("single",0);
	this.instance.parent = this;
	this.instance.setTransform(-90.65,-64.25,1,1,-8.9672,0,0,-5,0);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol_10_Layer_6, null, null);


(lib.Symbol_10_Layer_5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_5
	this.instance = new lib.Symbol5_1();
	this.instance.parent = this;
	this.instance.setTransform(124.75,2.45,1,1,-14.9992,0,0,44.6,-46);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol_10_Layer_5, null, null);


(lib.Scene_1_Layer_5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_5
	this.instance = new lib.Symbol25();
	this.instance.parent = this;
	this.instance.setTransform(409.35,443.55,1,1,0,0,0,18.1,-4.3);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({_off:true},165).wait(1));

}).prototype = p = new cjs.MovieClip();


(lib.Scene_1_Layer_4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_4
	this.instance = new lib.Symbol24();
	this.instance.parent = this;
	this.instance.setTransform(511.35,200.15,1,1,0,0,0,-5.4,2.3);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({_off:true},165).wait(1));

}).prototype = p = new cjs.MovieClip();


(lib.Scene_1_Layer_3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_3
	this.instance = new lib.Symbol4copy();
	this.instance.parent = this;
	this.instance.setTransform(1960.4,449,1,1,0,0,0,-2.9,0);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(299).to({_off:false},0).wait(1).to({regX:-54.8,regY:-11.3,x:1898.1,y:437.7},0).wait(1).to({x:1865.65},0).wait(1).to({x:1809.65},0).wait(1).to({x:1729.9},0).wait(1).to({x:1628.45},0).wait(1).to({x:1510.6},0).wait(1).to({x:1384.35},0).wait(1).to({x:1259.2},0).wait(1).to({x:1143.7},0).wait(1).to({x:1043.9},0).wait(1).to({x:963},0).wait(1).to({x:901.9},0).wait(1).to({x:860.05},0).wait(1).to({x:836.1},0).wait(1).to({regX:-2.9,regY:0,x:880.4,y:449},0).wait(151));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol10 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_5_obj_
	this.Layer_5 = new lib.Symbol_10_Layer_5();
	this.Layer_5.name = "Layer_5";
	this.Layer_5.parent = this;
	this.Layer_5.setTransform(61,56.7,1,1,0,0,0,61,56.7);
	this.Layer_5.depth = 0;
	this.Layer_5.isAttachedToCamera = 0
	this.Layer_5.isAttachedToMask = 0
	this.Layer_5.layerDepth = 0
	this.Layer_5.layerIndex = 0
	this.Layer_5.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_5).wait(1));

	// Layer_6_obj_
	this.Layer_6 = new lib.Symbol_10_Layer_6();
	this.Layer_6.name = "Layer_6";
	this.Layer_6.parent = this;
	this.Layer_6.setTransform(-137.6,-76.1,1,1,0,0,0,-137.6,-76.1);
	this.Layer_6.depth = 0;
	this.Layer_6.isAttachedToCamera = 0
	this.Layer_6.isAttachedToMask = 0
	this.Layer_6.layerDepth = 0
	this.Layer_6.layerIndex = 1
	this.Layer_6.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_6).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol10, new cjs.Rectangle(-138.6,-124.2,370.2,334.8), null);


(lib.Scene_1_Layer_9 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_9
	this.instance = new lib.Symbol10();
	this.instance.parent = this;
	this.instance.setTransform(632.7,333.95);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({_off:true},165).wait(1));

}).prototype = p = new cjs.MovieClip();


// stage content:
(lib._2_banners = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	this.___GetDepth___ = function(obj) {
		var depth = obj.depth;
		var cameraObj = this.___camera___instance;
		if(cameraObj && cameraObj.depth && obj.isAttachedToCamera)
		{
			depth += depth + cameraObj.depth;
		}
		return depth;
		}
	this.___needSorting___ = function() {
		for (var i = 0; i < this.getNumChildren() - 1; i++)
		{
			var prevDepth = this.___GetDepth___(this.getChildAt(i));
			var nextDepth = this.___GetDepth___(this.getChildAt(i + 1));
			if (prevDepth < nextDepth)
				return true;
		}
		return false;
	}
	this.___sortFunction___ = function(obj1, obj2) {
		return (this.exportRoot.___GetDepth___(obj2) - this.exportRoot.___GetDepth___(obj1));
	}
	this.on('tick', function (event){
		var curTimeline = event.currentTarget;
		if (curTimeline.___needSorting___()){
			this.sortChildren(curTimeline.___sortFunction___);
		}
	});

	// timeline functions:
	this.frame_614 = function() {
		this.___loopingOver___ = true;
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(614).call(this.frame_614).wait(1));

	// Layer_29_obj_
	this.Layer_29 = new lib.Scene_1_Layer_29();
	this.Layer_29.name = "Layer_29";
	this.Layer_29.parent = this;
	this.Layer_29.depth = 0;
	this.Layer_29.isAttachedToCamera = 0
	this.Layer_29.isAttachedToMask = 0
	this.Layer_29.layerDepth = 0
	this.Layer_29.layerIndex = 0
	this.Layer_29.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_29).wait(615));

	// Layer_18_obj_
	this.Layer_18 = new lib.Scene_1_Layer_18();
	this.Layer_18.name = "Layer_18";
	this.Layer_18.parent = this;
	this.Layer_18.setTransform(186.9,554.9,1,1,0,0,0,186.9,554.9);
	this.Layer_18.depth = 0;
	this.Layer_18.isAttachedToCamera = 0
	this.Layer_18.isAttachedToMask = 0
	this.Layer_18.layerDepth = 0
	this.Layer_18.layerIndex = 1
	this.Layer_18.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_18).wait(615));

	// Layer_28_obj_
	this.Layer_28 = new lib.Scene_1_Layer_28();
	this.Layer_28.name = "Layer_28";
	this.Layer_28.parent = this;
	this.Layer_28.depth = 0;
	this.Layer_28.isAttachedToCamera = 0
	this.Layer_28.isAttachedToMask = 0
	this.Layer_28.layerDepth = 0
	this.Layer_28.layerIndex = 2
	this.Layer_28.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_28).wait(615));

	// Layer_27_obj_
	this.Layer_27 = new lib.Scene_1_Layer_27();
	this.Layer_27.name = "Layer_27";
	this.Layer_27.parent = this;
	this.Layer_27.depth = 0;
	this.Layer_27.isAttachedToCamera = 0
	this.Layer_27.isAttachedToMask = 0
	this.Layer_27.layerDepth = 0
	this.Layer_27.layerIndex = 3
	this.Layer_27.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_27).wait(615));

	// Layer_26_obj_
	this.Layer_26 = new lib.Scene_1_Layer_26();
	this.Layer_26.name = "Layer_26";
	this.Layer_26.parent = this;
	this.Layer_26.depth = 0;
	this.Layer_26.isAttachedToCamera = 0
	this.Layer_26.isAttachedToMask = 0
	this.Layer_26.layerDepth = 0
	this.Layer_26.layerIndex = 4
	this.Layer_26.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_26).wait(615));

	// Layer_25_obj_
	this.Layer_25 = new lib.Scene_1_Layer_25();
	this.Layer_25.name = "Layer_25";
	this.Layer_25.parent = this;
	this.Layer_25.depth = 0;
	this.Layer_25.isAttachedToCamera = 0
	this.Layer_25.isAttachedToMask = 0
	this.Layer_25.layerDepth = 0
	this.Layer_25.layerIndex = 5
	this.Layer_25.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_25).wait(615));

	// Layer_24_obj_
	this.Layer_24 = new lib.Scene_1_Layer_24();
	this.Layer_24.name = "Layer_24";
	this.Layer_24.parent = this;
	this.Layer_24.setTransform(85,635.1,1,1,0,0,0,85,635.1);
	this.Layer_24.depth = 0;
	this.Layer_24.isAttachedToCamera = 0
	this.Layer_24.isAttachedToMask = 0
	this.Layer_24.layerDepth = 0
	this.Layer_24.layerIndex = 6
	this.Layer_24.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_24).wait(615));

	// Layer_17_obj_
	this.Layer_17 = new lib.Scene_1_Layer_17();
	this.Layer_17.name = "Layer_17";
	this.Layer_17.parent = this;
	this.Layer_17.setTransform(115,635,1,1,0,0,0,115,635);
	this.Layer_17.depth = 0;
	this.Layer_17.isAttachedToCamera = 0
	this.Layer_17.isAttachedToMask = 0
	this.Layer_17.layerDepth = 0
	this.Layer_17.layerIndex = 7
	this.Layer_17.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_17).wait(615));

	// Layer_23_obj_
	this.Layer_23 = new lib.Scene_1_Layer_23();
	this.Layer_23.name = "Layer_23";
	this.Layer_23.parent = this;
	this.Layer_23.depth = 0;
	this.Layer_23.isAttachedToCamera = 0
	this.Layer_23.isAttachedToMask = 0
	this.Layer_23.layerDepth = 0
	this.Layer_23.layerIndex = 8
	this.Layer_23.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_23).wait(600).to({regX:733,regY:395.9,x:733,y:395.9},0).wait(14).to({regX:0,regY:0,x:0,y:0},0).wait(1));

	// Layer_22_obj_
	this.Layer_22 = new lib.Scene_1_Layer_22();
	this.Layer_22.name = "Layer_22";
	this.Layer_22.parent = this;
	this.Layer_22.depth = 0;
	this.Layer_22.isAttachedToCamera = 0
	this.Layer_22.isAttachedToMask = 0
	this.Layer_22.layerDepth = 0
	this.Layer_22.layerIndex = 9
	this.Layer_22.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_22).wait(600).to({regX:779.8,regY:275.1,x:779.8,y:275.1},0).wait(14).to({regX:0,regY:0,x:0,y:0},0).wait(1));

	// Layer_14_obj_
	this.Layer_14 = new lib.Scene_1_Layer_14();
	this.Layer_14.name = "Layer_14";
	this.Layer_14.parent = this;
	this.Layer_14.depth = 0;
	this.Layer_14.isAttachedToCamera = 0
	this.Layer_14.isAttachedToMask = 0
	this.Layer_14.layerDepth = 0
	this.Layer_14.layerIndex = 10
	this.Layer_14.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_14).wait(600).to({regX:1080,regY:340,x:1080,y:340},0).wait(14).to({regX:0,regY:0,x:0,y:0},0).wait(1));

	// Layer_13_obj_
	this.Layer_13 = new lib.Scene_1_Layer_13();
	this.Layer_13.name = "Layer_13";
	this.Layer_13.parent = this;
	this.Layer_13.depth = 0;
	this.Layer_13.isAttachedToCamera = 0
	this.Layer_13.isAttachedToMask = 0
	this.Layer_13.layerDepth = 0
	this.Layer_13.layerIndex = 11
	this.Layer_13.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_13).wait(300).to({regX:180.5,regY:340,x:180.5,y:340},0).wait(14).to({regX:0,regY:0,x:0,y:0},0).wait(286).to({regX:180.5,regY:340,x:180.5,y:340},0).wait(14).to({regX:0,regY:0,x:0,y:0},0).wait(1));

	// Layer_21_obj_
	this.Layer_21 = new lib.Scene_1_Layer_21();
	this.Layer_21.name = "Layer_21";
	this.Layer_21.parent = this;
	this.Layer_21.depth = 0;
	this.Layer_21.isAttachedToCamera = 0
	this.Layer_21.isAttachedToMask = 0
	this.Layer_21.layerDepth = 0
	this.Layer_21.layerIndex = 12
	this.Layer_21.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_21).wait(300).to({regX:781.9,regY:284.7,x:781.9,y:284.7},0).wait(14).to({regX:0,regY:0,x:0,y:0},0).wait(301));

	// Layer_3_obj_
	this.Layer_3 = new lib.Scene_1_Layer_3();
	this.Layer_3.name = "Layer_3";
	this.Layer_3.parent = this;
	this.Layer_3.depth = 0;
	this.Layer_3.isAttachedToCamera = 0
	this.Layer_3.isAttachedToMask = 0
	this.Layer_3.layerDepth = 0
	this.Layer_3.layerIndex = 13
	this.Layer_3.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_3).wait(300).to({regX:1368.5,regY:437.7,x:1368.5,y:437.7},0).wait(14).to({regX:0,regY:0,x:0,y:0},0).to({_off:true},151).wait(150));

	// Layer_11_obj_
	this.Layer_11 = new lib.Scene_1_Layer_11();
	this.Layer_11.name = "Layer_11";
	this.Layer_11.parent = this;
	this.Layer_11.depth = 0;
	this.Layer_11.isAttachedToCamera = 0
	this.Layer_11.isAttachedToMask = 0
	this.Layer_11.layerDepth = 0
	this.Layer_11.layerIndex = 14
	this.Layer_11.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_11).wait(300).to({regX:1080,regY:340,x:1080,y:340},0).wait(14).to({regX:0,regY:0,x:0,y:0},0).to({_off:true},151).wait(150));

	// Layer_12_obj_
	this.Layer_12 = new lib.Scene_1_Layer_12();
	this.Layer_12.name = "Layer_12";
	this.Layer_12.parent = this;
	this.Layer_12.depth = 0;
	this.Layer_12.isAttachedToCamera = 0
	this.Layer_12.isAttachedToMask = 0
	this.Layer_12.layerDepth = 0
	this.Layer_12.layerIndex = 15
	this.Layer_12.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_12).wait(300).to({regX:0.5,regY:340,x:0.5,y:340},0).wait(14).to({regX:0,regY:0,x:0,y:0},0).to({_off:true},1).wait(300));

	// Layer_19_obj_
	this.Layer_19 = new lib.Scene_1_Layer_19();
	this.Layer_19.name = "Layer_19";
	this.Layer_19.parent = this;
	this.Layer_19.setTransform(193,395.9,1,1,0,0,0,193,395.9);
	this.Layer_19.depth = 0;
	this.Layer_19.isAttachedToCamera = 0
	this.Layer_19.isAttachedToMask = 0
	this.Layer_19.layerDepth = 0
	this.Layer_19.layerIndex = 16
	this.Layer_19.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_19).wait(615));

	// Layer_16_obj_
	this.Layer_16 = new lib.Scene_1_Layer_16();
	this.Layer_16.name = "Layer_16";
	this.Layer_16.parent = this;
	this.Layer_16.setTransform(239.8,275.1,1,1,0,0,0,239.8,275.1);
	this.Layer_16.depth = 0;
	this.Layer_16.isAttachedToCamera = 0
	this.Layer_16.isAttachedToMask = 0
	this.Layer_16.layerDepth = 0
	this.Layer_16.layerIndex = 17
	this.Layer_16.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_16).wait(615));

	// Layer_7_obj_
	this.Layer_7 = new lib.Scene_1_Layer_7();
	this.Layer_7.name = "Layer_7";
	this.Layer_7.parent = this;
	this.Layer_7.setTransform(762.5,202.5,1,1,0,0,0,762.5,202.5);
	this.Layer_7.depth = 0;
	this.Layer_7.isAttachedToCamera = 0
	this.Layer_7.isAttachedToMask = 0
	this.Layer_7.layerDepth = 0
	this.Layer_7.layerIndex = 18
	this.Layer_7.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_7).wait(165).to({_off:true},1).wait(449));

	// Layer_6_obj_
	this.Layer_6 = new lib.Scene_1_Layer_6();
	this.Layer_6.name = "Layer_6";
	this.Layer_6.parent = this;
	this.Layer_6.setTransform(698,245.5,1,1,0,0,0,698,245.5);
	this.Layer_6.depth = 0;
	this.Layer_6.isAttachedToCamera = 0
	this.Layer_6.isAttachedToMask = 0
	this.Layer_6.layerDepth = 0
	this.Layer_6.layerIndex = 19
	this.Layer_6.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_6).wait(165).to({_off:true},1).wait(449));

	// Layer_5_obj_
	this.Layer_5 = new lib.Scene_1_Layer_5();
	this.Layer_5.name = "Layer_5";
	this.Layer_5.parent = this;
	this.Layer_5.setTransform(405.8,441.5,1,1,0,0,0,405.8,441.5);
	this.Layer_5.depth = 0;
	this.Layer_5.isAttachedToCamera = 0
	this.Layer_5.isAttachedToMask = 0
	this.Layer_5.layerDepth = 0
	this.Layer_5.layerIndex = 20
	this.Layer_5.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_5).wait(165).to({_off:true},1).wait(449));

	// Layer_4_obj_
	this.Layer_4 = new lib.Scene_1_Layer_4();
	this.Layer_4.name = "Layer_4";
	this.Layer_4.parent = this;
	this.Layer_4.setTransform(511.4,200.2,1,1,0,0,0,511.4,200.2);
	this.Layer_4.depth = 0;
	this.Layer_4.isAttachedToCamera = 0
	this.Layer_4.isAttachedToMask = 0
	this.Layer_4.layerDepth = 0
	this.Layer_4.layerIndex = 21
	this.Layer_4.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_4).wait(165).to({_off:true},1).wait(449));

	// Layer_8_obj_
	this.Layer_8 = new lib.Scene_1_Layer_8();
	this.Layer_8.name = "Layer_8";
	this.Layer_8.parent = this;
	this.Layer_8.setTransform(911.5,419.4,1,1,0,0,0,911.5,419.4);
	this.Layer_8.depth = 0;
	this.Layer_8.isAttachedToCamera = 0
	this.Layer_8.isAttachedToMask = 0
	this.Layer_8.layerDepth = 0
	this.Layer_8.layerIndex = 22
	this.Layer_8.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_8).wait(165).to({_off:true},1).wait(449));

	// Layer_10_obj_
	this.Layer_10 = new lib.Scene_1_Layer_10();
	this.Layer_10.name = "Layer_10";
	this.Layer_10.parent = this;
	this.Layer_10.setTransform(910,470.9,1,1,0,0,0,910,470.9);
	this.Layer_10.depth = 0;
	this.Layer_10.isAttachedToCamera = 0
	this.Layer_10.isAttachedToMask = 0
	this.Layer_10.layerDepth = 0
	this.Layer_10.layerIndex = 23
	this.Layer_10.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_10).wait(165).to({_off:true},1).wait(449));

	// Layer_9_obj_
	this.Layer_9 = new lib.Scene_1_Layer_9();
	this.Layer_9.name = "Layer_9";
	this.Layer_9.parent = this;
	this.Layer_9.setTransform(603.1,330.7,1,1,0,0,0,603.1,330.7);
	this.Layer_9.depth = 0;
	this.Layer_9.isAttachedToCamera = 0
	this.Layer_9.isAttachedToMask = 0
	this.Layer_9.layerDepth = 0
	this.Layer_9.layerIndex = 24
	this.Layer_9.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_9).wait(165).to({_off:true},1).wait(449));

	// Layer_2_obj_
	this.Layer_2 = new lib.Scene_1_Layer_2();
	this.Layer_2.name = "Layer_2";
	this.Layer_2.parent = this;
	this.Layer_2.setTransform(540,340,1,1,0,0,0,540,340);
	this.Layer_2.depth = 0;
	this.Layer_2.isAttachedToCamera = 0
	this.Layer_2.isAttachedToMask = 0
	this.Layer_2.layerDepth = 0
	this.Layer_2.layerIndex = 25
	this.Layer_2.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_2).wait(165).to({_off:true},1).wait(449));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-540,340,2700,340);
// library properties:
lib.properties = {
	id: 'D55B5E3E6C6F5A4FBA4088D2C18C4C26',
	width: 1080,
	height: 680,
	fps: 24,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: [
		{src:"assets/images/_2_banners_img1t.jpg", id:"_2_banners_img1"},
		{src:"assets/images/bgt.jpg", id:"bg"},
		{src:"assets/images/bg2_1t.jpg", id:"bg2_1"},
		{src:"assets/images/bg2_2t.jpg", id:"bg2_2"},
		{src:"assets/images/bg_packt.jpg", id:"bg_pack"},
		{src:"assets/images/buttt.png", id:"butt"},
		{src:"assets/images/butt1t.png", id:"butt1"},
		{src:"assets/images/butterfly_whitet.png", id:"butterfly_white"},
		{src:"assets/images/butterfly_yellowt.png", id:"butterfly_yellow"},
		{src:"assets/images/leafst.png", id:"leafs"},
		{src:"assets/images/milk_partt.png", id:"milk_part"},
		{src:"assets/images/packt.png", id:"pack"},
		{src:"assets/images/txt1t.png", id:"txt1"},
		{src:"assets/images/txt2t.png", id:"txt2"},
		{src:"assets/images/txts1t.png", id:"txts1"},
		{src:"assets/images/txts2t.png", id:"txts2"}
	],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['D55B5E3E6C6F5A4FBA4088D2C18C4C26'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}


// Layer depth API : 

AdobeAn.Layer = new function() {
	this.getLayerZDepth = function(timeline, layerName)
	{
		if(layerName === "Camera")
		layerName = "___camera___instance";
		var script = "if(timeline." + layerName + ") timeline." + layerName + ".depth; else 0;";
		return eval(script);
	}
	this.setLayerZDepth = function(timeline, layerName, zDepth)
	{
		const MAX_zDepth = 10000;
		const MIN_zDepth = -5000;
		if(zDepth > MAX_zDepth)
			zDepth = MAX_zDepth;
		else if(zDepth < MIN_zDepth)
			zDepth = MIN_zDepth;
		if(layerName === "Camera")
		layerName = "___camera___instance";
		var script = "if(timeline." + layerName + ") timeline." + layerName + ".depth = " + zDepth + ";";
		eval(script);
	}
	this.removeLayer = function(timeline, layerName)
	{
		if(layerName === "Camera")
		layerName = "___camera___instance";
		var script = "if(timeline." + layerName + ") timeline.removeChild(timeline." + layerName + ");";
		eval(script);
	}
	this.addNewLayer = function(timeline, layerName, zDepth)
	{
		if(layerName === "Camera")
		layerName = "___camera___instance";
		zDepth = typeof zDepth !== 'undefined' ? zDepth : 0;
		var layer = new createjs.MovieClip();
		layer.name = layerName;
		layer.depth = zDepth;
		layer.layerIndex = 0;
		timeline.addChild(layer);
	}
}


})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;

$(function () {
    var canvas, stage, exportRoot, anim_container, dom_overlay_container, fnStartAnimation;
    function init() {
        canvas = document.getElementById("canvas48");
        anim_container = document.getElementById("animation_container48");
        dom_overlay_container = document.getElementById("dom_overlay_container48");
        var comp=AdobeAn.getComposition("D55B5E3E6C6F5A4FBA4088D2C18C4C26");
        var lib=comp.getLibrary();
        var loader = new createjs.LoadQueue(false);
        loader.addEventListener("fileload", function(evt){handleFileLoad(evt,comp)});
        loader.addEventListener("complete", function(evt){handleComplete(evt,comp)});
        var lib=comp.getLibrary();
        loader.loadManifest(lib.properties.manifest);
    }
    function handleFileLoad(evt, comp) {
        var images=comp.getImages();
        if (evt && (evt.item.type == "image")) { images[evt.item.id] = evt.result; }
    }
    function handleComplete(evt,comp) {
        //This function is always called, irrespective of the content. You can use the variable "stage" after it is created in token create_stage.
        var lib=comp.getLibrary();
        var ss=comp.getSpriteSheet();
        var queue = evt.target;
        var ssMetadata = lib.ssMetadata;
        for(i=0; i<ssMetadata.length; i++) {
            ss[ssMetadata[i].name] = new createjs.SpriteSheet( {"images": [queue.getResult(ssMetadata[i].name)], "frames": ssMetadata[i].frames} )
        }
        var preloaderDiv = document.getElementById("_preload_div_48");
        preloaderDiv.style.display = 'none';
        canvas.style.display = 'block';
        exportRoot = new lib._2_banners();
        stage = new lib.Stage(canvas);
        //Registers the "tick" event listener.
        fnStartAnimation = function() {
            stage.addChild(exportRoot);
            createjs.Ticker.setFPS(lib.properties.fps);
            createjs.Ticker.addEventListener("tick", stage)
            stage.addEventListener("tick", handleTick)
            function getProjectionMatrix(container, totalDepth) {
                var focalLength = 528.25;
                var projectionCenter = { x : lib.properties.width/2, y : lib.properties.height/2 };
                var scale = (totalDepth + focalLength)/focalLength;
                var scaleMat = new createjs.Matrix2D;
                scaleMat.a = 1/scale;
                scaleMat.d = 1/scale;
                var projMat = new createjs.Matrix2D;
                projMat.tx = -projectionCenter.x;
                projMat.ty = -projectionCenter.y;
                projMat = projMat.prependMatrix(scaleMat);
                projMat.tx += projectionCenter.x;
                projMat.ty += projectionCenter.y;
                return projMat;
            }
            function handleTick(event) {
                var cameraInstance = exportRoot.___camera___instance;
                if(cameraInstance !== undefined && cameraInstance.pinToObject !== undefined)
                {
                    cameraInstance.x = cameraInstance.pinToObject.x + cameraInstance.pinToObject.pinOffsetX;
                    cameraInstance.y = cameraInstance.pinToObject.y + cameraInstance.pinToObject.pinOffsetY;
                    if(cameraInstance.pinToObject.parent !== undefined && cameraInstance.pinToObject.parent.depth !== undefined)
                        cameraInstance.depth = cameraInstance.pinToObject.parent.depth + cameraInstance.pinToObject.pinOffsetZ;
                }
                applyLayerZDepth(exportRoot);
            }
            function applyLayerZDepth(parent)
            {
                var cameraInstance = parent.___camera___instance;
                var focalLength = 528.25;
                var projectionCenter = { 'x' : 0, 'y' : 0};
                if(parent === exportRoot)
                {
                    var stageCenter = { 'x' : lib.properties.width/2, 'y' : lib.properties.height/2 };
                    projectionCenter.x = stageCenter.x;
                    projectionCenter.y = stageCenter.y;
                }
                for(child in parent.children)
                {
                    var layerObj = parent.children[child];
                    if(layerObj == cameraInstance)
                        continue;
                    applyLayerZDepth(layerObj, cameraInstance);
                    if(layerObj.layerDepth === undefined)
                        continue;
                    if(layerObj.currentFrame != layerObj.parent.currentFrame)
                    {
                        layerObj.gotoAndPlay(layerObj.parent.currentFrame);
                    }
                    var matToApply = new createjs.Matrix2D;
                    var cameraMat = new createjs.Matrix2D;
                    var totalDepth = layerObj.layerDepth ? layerObj.layerDepth : 0;
                    var cameraDepth = 0;
                    if(cameraInstance && !layerObj.isAttachedToCamera)
                    {
                        var mat = cameraInstance.getMatrix();
                        mat.tx -= projectionCenter.x;
                        mat.ty -= projectionCenter.y;
                        cameraMat = mat.invert();
                        cameraMat.prependTransform(projectionCenter.x, projectionCenter.y, 1, 1, 0, 0, 0, 0, 0);
                        cameraMat.appendTransform(-projectionCenter.x, -projectionCenter.y, 1, 1, 0, 0, 0, 0, 0);
                        if(cameraInstance.depth)
                            cameraDepth = cameraInstance.depth;
                    }
                    if(layerObj.depth)
                    {
                        totalDepth = layerObj.depth;
                    }
                    //Offset by camera depth
                    totalDepth -= cameraDepth;
                    if(totalDepth < -focalLength)
                    {
                        matToApply.a = 0;
                        matToApply.d = 0;
                    }
                    else
                    {
                        if(layerObj.layerDepth)
                        {
                            var sizeLockedMat = getProjectionMatrix(parent, layerObj.layerDepth);
                            if(sizeLockedMat)
                            {
                                sizeLockedMat.invert();
                                matToApply.prependMatrix(sizeLockedMat);
                            }
                        }
                        matToApply.prependMatrix(cameraMat);
                        var projMat = getProjectionMatrix(parent, totalDepth);
                        if(projMat)
                        {
                            matToApply.prependMatrix(projMat);
                        }
                    }
                    layerObj.transformMatrix = matToApply;
                }
            }
        }
        //Code to support hidpi screens and responsive scaling.
        function makeResponsive(isResp, respDim, isScale, scaleType) {
            var lastW, lastH, lastS=1;
            window.addEventListener('resize', resizeCanvas);
            resizeCanvas();
            function resizeCanvas() {
                var w = lib.properties.width, h = lib.properties.height;
                var iw = window.innerWidth, ih=window.innerHeight;
                var pRatio = window.devicePixelRatio || 1, xRatio=iw/w, yRatio=ih/h, sRatio=1;
                if(isResp) {
                    if((respDim=='width'&&lastW==iw) || (respDim=='height'&&lastH==ih)) {
                        sRatio = lastS;
                    }
                    else if(!isScale) {
                        if(iw<w || ih<h)
                            sRatio = Math.min(xRatio, yRatio);
                    }
                    else if(scaleType==1) {
                        sRatio = Math.min(xRatio, yRatio);
                    }
                    else if(scaleType==2) {
                        sRatio = Math.max(xRatio, yRatio);
                    }
                }
                canvas.width = w*pRatio*sRatio;
                canvas.height = h*pRatio*sRatio;
                canvas.style.width = anim_container.style.width = dom_overlay_container.style.width = preloaderDiv.style.width = w*sRatio+'px';
                canvas.style.height = anim_container.style.height = dom_overlay_container.style.height = preloaderDiv.style.height = h*sRatio+'px';
                stage.scaleX = pRatio*sRatio;
                stage.scaleY = pRatio*sRatio;
                lastW = iw; lastH = ih; lastS = sRatio;
                stage.tickOnUpdate = false;
                stage.update();
                stage.tickOnUpdate = true;
            }
        }
        makeResponsive(true,'both',true,2);
        AdobeAn.compositionLoaded(lib.properties.id);
        fnStartAnimation();
    }
    init();
});