(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [];


// symbols:



(lib.bg_vert = function() {
	this.initialize(img.bg_vert);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,375,812);


(lib.pack = function() {
	this.initialize(img.pack);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,287,339);


(lib.shadow1 = function() {
	this.initialize(img.shadow1);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,133,165);// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.Symbol_5_Layer_2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(8,1,1).p("AiRgmQASA0AvAdQArAbAygDQA1gDAjghQAogjAFg/");
	this.shape.setTransform(3.425,27.0106);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(8,1,1).p("AiZghQAZAyAvAaQAsAYAxgDQA0gFAkgdQAngfAPg+");
	this.shape_1.setTransform(3.525,26.7973);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(8,1,1).p("AifgdQAfAxAuAXQAsAVAxgEQAzgGAkgZQAngcAXg9");
	this.shape_2.setTransform(3.625,26.6139);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(8,1,1).p("AilgaQAlAwAuAUQAsAUAxgGQAxgGAlgWQAngZAeg8");
	this.shape_3.setTransform(3.675,26.4692);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(8,1,1).p("AiqgWQApAuAuASQAtATAwgHQAxgHAmgUQAmgWAkg7");
	this.shape_4.setTransform(3.75,26.3461);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(8,1,1).p("AiugVQAtAuAuARQAtAQAvgHQAxgHAlgSQAngUApg7");
	this.shape_5.setTransform(3.8,26.2543);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(8,1,1).p("AixgTQAwAtAuAQQAsAPAwgHQAwgIAmgRQAmgRAtg7");
	this.shape_6.setTransform(3.85,26.1723);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(8,1,1).p("AizgRQAyAsAuAPQAtAOAvgIQAvgIAmgPQAmgQAwg7");
	this.shape_7.setTransform(3.875,26.1172);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(8,1,1).p("Ai0gRQAzAsAtAOQAtAPAvgJQAwgIAmgPQAmgPAxg7");
	this.shape_8.setTransform(3.9,26.1071);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(8,1,1).p("Ai1gQQA0AsAtAOQAtAOAvgJQAwgIAmgPQAmgPAyg6");
	this.shape_9.setTransform(3.9,26.0856);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(8,1,1).p("AitgVQAsAuAuARQAtARAwgIQAwgHAmgSQAmgUAog7");
	this.shape_10.setTransform(3.8,26.2648);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(8,1,1).p("AingYQAnAuAuAUQAtAUAwgHQAxgGAlgWQAngXAgg8");
	this.shape_11.setTransform(3.7,26.4313);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(8,1,1).p("AihgcQAhAwAuAXQAsAVAxgFQAzgGAkgZQAngbAZg8");
	this.shape_12.setTransform(3.65,26.5731);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(8,1,1).p("AicgfQAcAxAvAZQAsAWAxgEQAzgFAkgbQAngdATg9");
	this.shape_13.setTransform(3.575,26.7052);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(8,1,1).p("AiYghQAYAyAvAaQAsAYAxgEQA0gEAkgdQAngfAOg+");
	this.shape_14.setTransform(3.525,26.8031);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(8,1,1).p("AiVgjQAWAzAuAbQAsAaAygEQA0gDAjgfQAoghAKg+");
	this.shape_15.setTransform(3.475,26.8961);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(8,1,1).p("AiTglQATAzAvAdQAsAaAygDQA1gDAjggQAogjAHg+");
	this.shape_16.setTransform(3.45,26.9653);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(8,1,1).p("AiSglQATAzAvAdQArAbAygDQA1gDAjggQAogkAGg+");
	this.shape_17.setTransform(3.425,26.9858);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},64).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_9}]},17).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_4_Layer_3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_3
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(8,1,1).p("AATgnIglBO");
	this.shape.setTransform(-0.525,10.9);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(8,1,1).p("AgSAoIAlhP");
	this.shape_1.setTransform(-0.5,10.95);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(8,1,1).p("AgRApIAjhR");
	this.shape_2.setTransform(-0.475,11.075);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(8,1,1).p("AgQAsIAihW");
	this.shape_3.setTransform(-0.4,11.3);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(8,1,1).p("AgPAvIAghc");
	this.shape_4.setTransform(-0.3,11.6);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(8,1,1).p("AAPgyIgdBl");
	this.shape_5.setTransform(-0.175,12);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(8,1,1).p("AgPAvIAfhd");
	this.shape_6.setTransform(-0.275,11.675);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(8,1,1).p("AgQAsIAhhY");
	this.shape_7.setTransform(-0.375,11.4);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(8,1,1).p("AgRAqIAjhT");
	this.shape_8.setTransform(-0.425,11.175);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},58).to({state:[{t:this.shape_1,p:{x:-0.5,y:10.95}}]},1).to({state:[{t:this.shape_2,p:{y:11.075}}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_5}]},18).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_2,p:{y:11.025}}]},1).to({state:[{t:this.shape_1,p:{x:-0.525,y:10.925}}]},1).to({state:[{t:this.shape}]},1).wait(13));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_4_Layer_2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(8,1,1).p("AA8A6QhDjHg0CO");
	this.shape.setTransform(-4.275,3.8024);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(8,1,1).p("Ag7gDQA0iJBDDH");
	this.shape_1.setTransform(-4.275,3.7032);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(8,1,1).p("Ag7gRQA0h4BDDI");
	this.shape_2.setTransform(-4.275,3.3687);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(8,1,1).p("Ag6gpQAzhZBDDH");
	this.shape_3.setTransform(-4.25,2.707);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(8,1,1).p("Ag6hHQAzgvBCDH");
	this.shape_4.setTransform(-4.225,1.5287);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(8,1,1).p("AA7BoQhDjIgygH");
	this.shape_5.setTransform(-4.2,-0.75);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(8,1,1).p("Ag6hNQAzgmBCDH");
	this.shape_6.setTransform(-4.225,1.2399);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(8,1,1).p("Ag6gyQAzhNBDDI");
	this.shape_7.setTransform(-4.25,2.3936);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(8,1,1).p("Ag6gcQAzhqBDDI");
	this.shape_8.setTransform(-4.25,3.092);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(8,1,1).p("Ag7gMQA0h+BDDH");
	this.shape_9.setTransform(-4.275,3.5032);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(8,1,1).p("Ag7gBQA0iLBDDH");
	this.shape_10.setTransform(-4.275,3.7382);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},58).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_5}]},18).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape}]},1).wait(13));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_4_Layer_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(8,1,1).p("AAgBmQAyh2h/hV");
	this.shape.setTransform(-0.0078,-0.025);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(8,1,1).p("AgshmQB9BWgyB3");
	this.shape_1.setTransform(0.0731,-0.075);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(8,1,1).p("AgqhnQB5BZgyB2");
	this.shape_2.setTransform(0.2894,-0.225);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(8,1,1).p("AgnhpQByBdgxB2");
	this.shape_3.setTransform(0.6425,-0.45);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(8,1,1).p("AgjhtQBpBkgyB3");
	this.shape_4.setTransform(1.1904,-0.775);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(8,1,1).p("AANByQAyh2hdht");
	this.shape_5.setTransform(1.8567,-1.2);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(8,1,1).p("AgihuQBnBmgyB3");
	this.shape_6.setTransform(1.2731,-0.85);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(8,1,1).p("AgmhrQBvBggxB3");
	this.shape_7.setTransform(0.8063,-0.55);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(8,1,1).p("AgphoQB2BbgyB2");
	this.shape_8.setTransform(0.4521,-0.325);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(8,1,1).p("AgrhnQB6BYgxB3");
	this.shape_9.setTransform(0.2082,-0.15);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(8,1,1).p("AgshmQB9BWgxB3");
	this.shape_10.setTransform(0.0461,-0.05);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},58).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_5}]},18).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape}]},1).wait(13));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_3_Layer_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(9,1,1).p("AjUAvQA6CvByBPQBpBJBPgzQBTg3gQifQgUi6igkJ");
	this.shape.setTransform(0.0361,-0.0012);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(9,1,1).p("AjVA1QA9CtBzBMQBpBHBOg0QBTg4gRigQgUi5iekG");
	this.shape_1.setTransform(0.1452,-0.6341);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(9,1,1).p("AjYBIQBFCnB0BFQBrBABMg5QBQg7gSihQgUi4iZj8");
	this.shape_2.setTransform(0.5403,-2.4754);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(9,1,1).p("AjeBmQBTCdB4A5QBuA1BIg/QBLhCgUiiQgVi2iRjr");
	this.shape_3.setTransform(1.1512,-5.4818);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(9,1,1).p("AjlCRQBlCOB9AoQByAlBDhJQBFhKgWikQgYiziEjU");
	this.shape_4.setTransform(2.0409,-9.6137);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(9,1,1).p("AjvDEQB9B7CCATQB5ARA8hVQA8hWgZinQgaivh0i2");
	this.shape_5.setTransform(3.1655,-14.6333);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(9,1,1).p("Aj7D+QCaBlCKgIQCAgIAyhkQAzhkgdipQgdiqhhiS");
	this.shape_6.setTransform(4.6141,-20.317);
	this.shape_6._off = true;

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(9,1,1).p("Aj9D/QCaBlCJgJQCAgHA0hlQA1hlgYirQgXishYiO");
	this.shape_7.setTransform(4.8883,-20.392);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(9,1,1).p("AkKECQCaBlCJgIQCAgIA7hnQA7hngEizQgFi0g0iA");
	this.shape_8.setTransform(6.1628,-20.717);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(9,1,1).p("AkdEFQCaBlCJgIQCAgIBChqQBAhpAQi7QAOi8gQhy");
	this.shape_9.setTransform(8.0891,-21.042);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(9,1,1).p("AkmEGQCaBlCLgIQB/gIBDhqQBChrAVi9QAUi9gHhu");
	this.shape_10.setTransform(8.9163,-21.117);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(9,1,1).p("AkkEGQCaBlCKgJQB/gHBDhrQBChqAUi8QATi+gJhu");
	this.shape_11.setTransform(8.7564,-21.092);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(9,1,1).p("AkfEFQCaBlCJgIQCAgIBChqQBBhqARi7QAQi8gPhx");
	this.shape_12.setTransform(8.2697,-21.042);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(9,1,1).p("AkXEEQCaBlCJgIQCAgIBAhpQA/hpALi5QAKi6gah1");
	this.shape_13.setTransform(7.4908,-20.942);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(9,1,1).p("AkOEDQCaBlCJgIQCAgIA9hoQA8hoABi1QABi2gqh8");
	this.shape_14.setTransform(6.5761,-20.817);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(9,1,1).p("AkGEBQCaBlCJgIQCAgIA5hmQA5hngJiwQgKizg+iD");
	this.shape_15.setTransform(5.7618,-20.617);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(9,1,1).p("AkAEAQCaBlCJgJQCAgHA2hmQA2hlgTitQgTiuhPiL");
	this.shape_16.setTransform(5.1627,-20.492);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(9,1,1).p("Aj9D/QCaBlCKgJQCAgHA0hlQAzhkgYirQgZishaiP");
	this.shape_17.setTransform(4.8199,-20.392);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#000000").ss(9,1,1).p("Aj7D+QCaBlCJgIQCAgIAzhkQAzhkgcipQgcirhgiS");
	this.shape_18.setTransform(4.6598,-20.342);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#000000").ss(9,1,1).p("AjzDaQCIBzCFAJQB7AHA4haQA5hcgainQgbithuip");
	this.shape_19.setTransform(3.7097,-16.8463);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#000000").ss(9,1,1).p("AjsC5QB3CACBAXQB4AVA9hSQA+hUgYimQgaivh4i9");
	this.shape_20.setTransform(2.9349,-13.5778);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#000000").ss(9,1,1).p("AjnCaQBqCLB9AkQB0AiBBhLQBEhNgXilQgYixiBjP");
	this.shape_21.setTransform(2.2313,-10.5578);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#000000").ss(9,1,1).p("AjiB+QBdCVB7AvQBxAsBEhEQBIhHgVikQgXi0iJje");
	this.shape_22.setTransform(1.6371,-7.8396);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#000000").ss(9,1,1).p("AjaBSQBKCkB2BBQBsA8BKg7QBOg+gSihQgVi3iWj2");
	this.shape_23.setTransform(0.7273,-3.5374);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#000000").ss(9,1,1).p("AjYBDQBECoB0BIQBqBBBNg3QBQg7gRigQgVi5iaj+");
	this.shape_24.setTransform(0.4313,-2.0022);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f().s("#000000").ss(9,1,1).p("AjWA4QA+CsBzBLQBqBGBOg1QBSg4gRigQgUi5iekF");
	this.shape_25.setTransform(0.221,-0.9081);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f().s("#000000").ss(9,1,1).p("AjVAxQA8CuByBOQBpBIBOgzQBUg3gRigQgUi6ifkH");
	this.shape_26.setTransform(0.0861,-0.2262);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},54).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape}]},1).wait(7));
	this.timeline.addTween(cjs.Tween.get(this.shape_6).wait(60).to({_off:false},0).wait(1).to({_off:true},1).wait(7).to({_off:false},0).to({_off:true},1).wait(12).to({_off:false},0).wait(1).to({_off:true},1).wait(16));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_2_Layer_3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_3
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(8,1,1).p("AAWgxIgrBi");
	this.shape.setTransform(-4.3,11.05);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(8,1,1).p("AgWAyQAcgwASgz");
	this.shape_1.setTransform(-4.45,11.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(8,1,1).p("AgYAzQAjguAOg2");
	this.shape_2.setTransform(-4.6,11.15);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(8,1,1).p("AgaAzQAsgrAJg6");
	this.shape_3.setTransform(-4.75,11.2);
	this.shape_3._off = true;

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(8,1,1).p("AAcgzQgFA9gyAp");
	this.shape_4.setTransform(-4.9,11.25);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(8,1,1).p("AgZAzQAmgtAMg4");
	this.shape_5.setTransform(-4.65,11.175);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(8,1,1).p("AgYAyQAhguAPg1");
	this.shape_6.setTransform(-4.55,11.125);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(8,1,1).p("AgWAyQAbgwASgz");
	this.shape_7.setTransform(-4.425,11.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},19).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},5).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},5).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape}]},1).wait(44));
	this.timeline.addTween(cjs.Tween.get(this.shape).wait(19).to({_off:true},1).wait(8).to({_off:false},0).wait(5).to({_off:true},1).wait(8).to({_off:false},0).wait(5).to({_off:true},1).wait(8).to({_off:false},0).wait(44));
	this.timeline.addTween(cjs.Tween.get(this.shape_3).wait(22).to({_off:false},0).to({_off:true},1).wait(1).to({_off:false,x:-4.775},0).to({_off:true},1).wait(11).to({_off:false,x:-4.75},0).to({_off:true},1).wait(1).to({_off:false,x:-4.775},0).to({_off:true},1).wait(11).to({_off:false,x:-4.75},0).to({_off:true},1).wait(1).to({_off:false,x:-4.775},0).to({_off:true},1).wait(47));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_2_Layer_2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(8,1,1).p("AArBTQg9hIgYhd");
	this.shape.setTransform(-4.9,1.125);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(8,1,1).p("AgnhTQAaBbA1BM");
	this.shape_1.setTransform(-4.65,1.05);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(8,1,1).p("AglhUQAdBZAuBQ");
	this.shape_2.setTransform(-4.4,0.975);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(8,1,1).p("AgihVQAfBYAmBT");
	this.shape_3.setTransform(-4.175,0.875);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(8,1,1).p("AAhBWIhBir");
	this.shape_4.setTransform(-3.925,0.8);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(8,1,1).p("AgihVQAfBXAmBU");
	this.shape_5.setTransform(-4.125,0.875);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(8,1,1).p("AgkhUQAeBYArBR");
	this.shape_6.setTransform(-4.325,0.925);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(8,1,1).p("AgmhUQAbBaAyBP");
	this.shape_7.setTransform(-4.5,1);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(8,1,1).p("AgohTQAaBcA3BL");
	this.shape_8.setTransform(-4.7,1.05);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},19).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},5).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},5).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape}]},1).wait(44));
	this.timeline.addTween(cjs.Tween.get(this.shape).wait(19).to({_off:true},1).wait(8).to({_off:false},0).wait(5).to({_off:true},1).wait(8).to({_off:false},0).wait(5).to({_off:true},1).wait(8).to({_off:false},0).wait(44));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_2_Layer_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(8,1,1).p("AAIBVQgWhXAJhS");
	this.shape.setTransform(0.0103,0);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(8,1,1).p("AgFhTQgFBSARBV");
	this.shape_1.setTransform(0.137,0.025);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(8,1,1).p("AgFhTQgBBTAMBU");
	this.shape_2.setTransform(0.2425,0.075);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(8,1,1).p("AgEhTQABBTAIBU");
	this.shape_3.setTransform(0.325,0.1);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(8,1,1).p("AAFBTIgJil");
	this.shape_4.setTransform(0.375,0.125);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(8,1,1).p("AgEhTQACBTAHBU");
	this.shape_5.setTransform(0.325,0.1);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(8,1,1).p("AgFhTQAABTALBU");
	this.shape_6.setTransform(0.274,0.075);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(8,1,1).p("AgFhTQgDBTAOBU");
	this.shape_7.setTransform(0.2007,0.05);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(8,1,1).p("AgFhTQgGBSASBV");
	this.shape_8.setTransform(0.1044,0.025);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},19).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},5).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},5).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape}]},1).wait(44));
	this.timeline.addTween(cjs.Tween.get(this.shape).wait(19).to({_off:true},1).wait(8).to({_off:false},0).wait(5).to({_off:true},1).wait(8).to({_off:false},0).wait(5).to({_off:true},1).wait(8).to({_off:false},0).wait(44));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_1_Layer_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(9,1,1).p("AFYE5Qpxl0g+j9");

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(9,1,1).p("Ak/k7QAzEHJMFw");
	this.shape_1.setTransform(2.45,-0.325);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(9,1,1).p("Akqk+QAoEPItFu");
	this.shape_2.setTransform(4.55,-0.6);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(9,1,1).p("AkZlAQAgEVITFs");
	this.shape_3.setTransform(6.25,-0.8);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(9,1,1).p("AkLlCQAZEbH+Fq");
	this.shape_4.setTransform(7.575,-0.975);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(9,1,1).p("AkClDQAUEfHxFo");
	this.shape_5.setTransform(8.525,-1.1);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(9,1,1).p("Aj8lEQAREhHoFo");
	this.shape_6.setTransform(9.075,-1.175);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(9,1,1).p("AD7FFQnllogQkh");
	this.shape_7.setTransform(9.275,-1.2);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(9,1,1).p("AkAlDQATEfHuFo");
	this.shape_8.setTransform(8.7,-1.125);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(9,1,1).p("AkSlBQAcEZIJFq");
	this.shape_9.setTransform(6.95,-0.9);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(9,1,1).p("Akvk9QArENI0Fu");
	this.shape_10.setTransform(4.05,-0.525);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(9,1,1).p("AlZk0QBGDNJtGc");
	this.shape_11.setTransform(-0.175,0.45);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(9,1,1).p("AlakwQBLCrJqG2");
	this.shape_12.setTransform(-0.3,0.8);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(9,1,1).p("AlbkuQBOCWJpHH");
	this.shape_13.setTransform(-0.375,0.975);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(9,1,1).p("AFcEvQponOhPiO");
	this.shape_14.setTransform(-0.4,1.05);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(9,1,1).p("AlakwQBMCmJpG7");
	this.shape_15.setTransform(-0.325,0.85);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(9,1,1).p("AlakyQBJC7JsGq");
	this.shape_16.setTransform(-0.25,0.625);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(9,1,1).p("AlZk0QBFDRJuGY");
	this.shape_17.setTransform(-0.15,0.425);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#000000").ss(9,1,1).p("AlYk2QBCDnJvGG");
	this.shape_18.setTransform(-0.075,0.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},4).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},4).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},5).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},5).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape}]},1).wait(44));
	this.timeline.addTween(cjs.Tween.get(this.shape).wait(4).to({_off:true},1).wait(10).to({_off:false},0).wait(4).to({_off:true},1).wait(8).to({_off:false},0).wait(5).to({_off:true},1).wait(8).to({_off:false},0).wait(5).to({_off:true},1).wait(8).to({_off:false},0).wait(44));

}).prototype = p = new cjs.MovieClip();


(lib.Scene_1_Layer_9 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_9
	this.instance = new lib.shadow1();
	this.instance.parent = this;
	this.instance.setTransform(161.85,613.15);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Scene_1_Layer_9, null, null);


(lib.Scene_1_Layer_8 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_8
	this.instance = new lib.bg_vert();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Scene_1_Layer_8, null, null);


(lib.Scene_1_Layer_3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_3
	this.instance = new lib.pack();
	this.instance.parent = this;
	this.instance.setTransform(92,315.9);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Scene_1_Layer_3, null, null);


(lib.Symbol9 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AgpArQgRgRAAgaQAAgZARgRQARgTAYABQAYgBASATQARARAAAZQAAAagRARQgSATgYgBQgYABgRgTg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol9, new cjs.Rectangle(-5.9,-6.1,11.9,12.3), null);


(lib.Symbol10 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#C7C7C7").s().p("AhjAIQApgpA8AAQA9AAAjAiQAjAjgSgNQgSgNglgQQglgRgcACQgdACgPAJQgPAJgmAZQgPAKgDAAQgEAAAZgag");
	this.shape.setTransform(-68.9275,-22.0279);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#C7C7C7").s().p("AiTA1QADgdArgrQArgsA8AAQA9AAArAsQArArgBAXQgBAXg3gxQg3gvg2AKQg4AKgmAsQgYAagIAAQgFAAABgLg");
	this.shape_1.setTransform(-68.9097,-19.0359);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#C7C7C7").s().p("ABwBLQgkgphAAHQg/AHgwAgQgxAgACg6QABg4ArgtQArgsA8AAQA9AAArAsQArAtAAA6QgBAkgMAAQgJAAgOgRg");
	this.shape_2.setTransform(-68.8036,-16.2799);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#C7C7C7").s().p("AAAB7Qg7gFgrgNQgsgOAAg/QAAg+ArgsQArgsA8AAQA9AAArAsQArAsAAA+QAAA/gqASQgjAOguAAIgYAAg");
	this.shape_3.setTransform(-68.7,-13.105);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#C7C7C7").s().p("AhnBrQgrgsAAg/QAAg+ArgtQArgrA8gBQA9ABArArQArAtAAA+QAAA/grAsQgrAtg9AAQg8AAgrgtg");
	this.shape_4.setTransform(-68.7,-10.3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape}]},12).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape}]},1).to({state:[]},1).to({state:[{t:this.shape}]},44).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape}]},1).to({state:[]},1).wait(24));

	// Symbol_9
	this.instance = new lib.Symbol9();
	this.instance.parent = this;
	this.instance.setTransform(-72.5,-14.25);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(4).to({x:-77,y:-9.75},6,cjs.Ease.get(1)).wait(43).to({x:-68,y:-14.25},6,cjs.Ease.get(1)).wait(27).to({x:-72.5},6,cjs.Ease.get(1)).wait(8));

	// Layer_3
	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AhnBrQgrgsAAg/QAAg+ArgtQArgrA8gBQA9ABArArQArAtAAA+QAAA/grAsQgrAtg9AAQg8AAgrgtg");
	this.shape_5.setTransform(-68.7,-10.3);

	this.timeline.addTween(cjs.Tween.get(this.shape_5).wait(100));

	// Layer_5
	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#C7C7C7").s().p("AhjAIQApgpA8AAQA9AAAjAiQAjAjgSgNQgSgNglgQQglgRgcACQgdACgPAJQgPAJgmAZQgPAKgDAAQgEAAAZgag");
	this.shape_6.setTransform(-8.3306,-21.9285);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#C7C7C7").s().p("AiTA1QADgdArgrQArgsA8AAQA9AAArAsQArArgBAXQgBAXg3gxQg3gvg3AKQg3AKgnAsQgXAagIAAQgFAAABgLg");
	this.shape_7.setTransform(-8.3213,-18.9323);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#C7C7C7").s().p("ABwBLQgkgphAAHQg/AHgwAgQgxAgACg6QABg4ArgtQArgsA8AAQA9AAArAsQArAtAAA6QgBAkgMAAQgJAAgOgRg");
	this.shape_8.setTransform(-8.2036,-16.1799);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#C7C7C7").s().p("AAAB7Qg7gFgsgNQgrgOAAg/QAAg+ArgsQArgsA8AAQA9AAArAsQArAsAAA+QAAA/grASQgiAOgtAAIgZAAg");
	this.shape_9.setTransform(-8.1,-13.0028);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#C7C7C7").s().p("AhnBrQgrgsAAg/QAAg+ArgtQArgsA8AAQA9AAArAsQArAtAAA+QAAA/grAsQgrAsg9AAQg8AAgrgsg");
	this.shape_10.setTransform(-8.1,-10.2);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#C7C7C7").s().p("AABB7Qg9gFgrgNQgrgOAAg/QAAg+ArgsQArgsA8AAQA9AAArAsQArAsAAA+QAAA/grASQgiAOguAAIgXAAg");
	this.shape_11.setTransform(-8.1,-13.005);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#C7C7C7").s().p("ABvBLQgjgphAAHQg/AHgxAgQgwAgABg6QACg4ArgtQArgsA8AAQA9AAArAsQArAtAAA6QgBAkgMAAQgJAAgPgRg");
	this.shape_12.setTransform(-8.2156,-16.1805);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#C7C7C7").s().p("AiTA1QADgdArgrQArgsA8AAQA9AAArAsQArArgBAXQgBAXg3gxQg3gvg2AKQg4AKgmAsQgYAagIAAQgFAAABgLg");
	this.shape_13.setTransform(-8.3097,-18.9359);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_6,p:{x:-8.3306,y:-21.9285}}]},12).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_6,p:{x:-8.3306,y:-21.9285}}]},1).to({state:[]},1).to({state:[{t:this.shape_6,p:{x:-8.3275,y:-21.9279}}]},44).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_6,p:{x:-8.3306,y:-21.9285}}]},1).to({state:[]},1).wait(24));

	// Symbol_9
	this.instance_1 = new lib.Symbol9();
	this.instance_1.parent = this;
	this.instance_1.setTransform(-11.9,-14.15);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(4).to({x:-16.4,y:-9.65},6,cjs.Ease.get(1)).wait(43).to({x:-7.4,y:-14.15},6,cjs.Ease.get(1)).wait(27).to({x:-11.9},6,cjs.Ease.get(1)).wait(8));

	// Layer_7
	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FFFFFF").s().p("AhnBrQgrgsAAg/QAAg+ArgtQArgsA8AAQA9AAArAsQArAtAAA+QAAA/grAsQgrAsg9AAQg8AAgrgsg");
	this.shape_14.setTransform(-8.1,-10.2);

	this.timeline.addTween(cjs.Tween.get(this.shape_14).wait(100));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-83.7,-25.4,90.3,30.4);


(lib.Symbol4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_99 = function() {
		this.___loopingOver___ = true;
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(99).call(this.frame_99).wait(1));

	// Layer_3_obj_
	this.Layer_3 = new lib.Symbol_4_Layer_3();
	this.Layer_3.name = "Layer_3";
	this.Layer_3.parent = this;
	this.Layer_3.setTransform(-0.6,10.9,1,1,0,0,0,-0.6,10.9);
	this.Layer_3.depth = 0;
	this.Layer_3.isAttachedToCamera = 0
	this.Layer_3.isAttachedToMask = 0
	this.Layer_3.layerDepth = 0
	this.Layer_3.layerIndex = 0
	this.Layer_3.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_3).wait(100));

	// Layer_2_obj_
	this.Layer_2 = new lib.Symbol_4_Layer_2();
	this.Layer_2.name = "Layer_2";
	this.Layer_2.parent = this;
	this.Layer_2.setTransform(-4.3,3.8,1,1,0,0,0,-4.3,3.8);
	this.Layer_2.depth = 0;
	this.Layer_2.isAttachedToCamera = 0
	this.Layer_2.isAttachedToMask = 0
	this.Layer_2.layerDepth = 0
	this.Layer_2.layerIndex = 1
	this.Layer_2.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_2).wait(100));

	// Layer_1_obj_
	this.Layer_1 = new lib.Symbol_4_Layer_1();
	this.Layer_1.name = "Layer_1";
	this.Layer_1.parent = this;
	this.Layer_1.depth = 0;
	this.Layer_1.isAttachedToCamera = 0
	this.Layer_1.isAttachedToMask = 0
	this.Layer_1.layerDepth = 0
	this.Layer_1.layerIndex = 2
	this.Layer_1.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_1).wait(100));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-14.2,-16.6,23.1,37.7);


(lib.Symbol2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_99 = function() {
		this.___loopingOver___ = true;
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(99).call(this.frame_99).wait(1));

	// Layer_3_obj_
	this.Layer_3 = new lib.Symbol_2_Layer_3();
	this.Layer_3.name = "Layer_3";
	this.Layer_3.parent = this;
	this.Layer_3.setTransform(-4.3,11.1,1,1,0,0,0,-4.3,11.1);
	this.Layer_3.depth = 0;
	this.Layer_3.isAttachedToCamera = 0
	this.Layer_3.isAttachedToMask = 0
	this.Layer_3.layerDepth = 0
	this.Layer_3.layerIndex = 0
	this.Layer_3.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_3).wait(100));

	// Layer_2_obj_
	this.Layer_2 = new lib.Symbol_2_Layer_2();
	this.Layer_2.name = "Layer_2";
	this.Layer_2.parent = this;
	this.Layer_2.setTransform(-4.9,1.1,1,1,0,0,0,-4.9,1.1);
	this.Layer_2.depth = 0;
	this.Layer_2.isAttachedToCamera = 0
	this.Layer_2.isAttachedToMask = 0
	this.Layer_2.layerDepth = 0
	this.Layer_2.layerIndex = 1
	this.Layer_2.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_2).wait(100));

	// Layer_1_obj_
	this.Layer_1 = new lib.Symbol_2_Layer_1();
	this.Layer_1.name = "Layer_1";
	this.Layer_1.parent = this;
	this.Layer_1.depth = 0;
	this.Layer_1.isAttachedToCamera = 0
	this.Layer_1.isAttachedToMask = 0
	this.Layer_1.layerDepth = 0
	this.Layer_1.layerIndex = 2
	this.Layer_1.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_1).wait(100));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-13.1,-12.4,18,32.8);


(lib.Symbol_5_Layer_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Symbol10();
	this.instance.parent = this;
	this.instance.setTransform(27.5,7.35,0.715,0.715);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(100));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_3_Layer_2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.instance = new lib.Symbol4();
	this.instance.parent = this;
	this.instance.setTransform(4.65,-31.9,1,1,0,0,0,4,12.7);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(54).to({x:15.15,y:-54.4},6,cjs.Ease.get(-1)).wait(2).to({regX:-2.7,regY:2.2,rotation:2.1147,x:11.2,y:-65.1},0).wait(1).to({rotation:9.9853,x:21.6,y:-65.75},0).wait(1).to({rotation:17.8559,x:32.15,y:-66.2},0).wait(1).to({regX:4,regY:12.6,rotation:19.9706,x:37.7,y:-54.2},0).wait(1).to({regX:-2.7,regY:2.2,rotation:17.8559,x:32.15,y:-66.15},0).wait(1).to({rotation:9.9853,x:21.6,y:-65.7},0).wait(1).to({rotation:2.1147,x:11.2,y:-65.05},0).wait(1).to({regX:4,regY:12.7,rotation:0,x:15.15,y:-54.4},0).wait(1).to({regX:-2.7,regY:2.2,rotation:2.1147,x:11.2,y:-65.1},0).wait(1).to({rotation:9.9853,x:21.6,y:-65.75},0).wait(1).to({rotation:17.8559,x:32.15,y:-66.2},0).wait(1).to({regX:4,regY:12.6,rotation:19.9706,x:37.7,y:-54.2},0).wait(1).to({regX:-2.7,regY:2.2,rotation:19.6063,x:34.45,y:-66.25},0).wait(1).to({rotation:18.345,x:32.75,y:-66.15},0).wait(1).to({rotation:15.9035,x:29.5,y:-66.1},0).wait(1).to({rotation:12.1725,x:24.5,y:-65.85},0).wait(1).to({rotation:7.7981,x:18.7,y:-65.5},0).wait(1).to({rotation:4.0671,x:13.75,y:-65.2},0).wait(1).to({rotation:1.6256,x:10.55,y:-65},0).wait(1).to({rotation:0.3644,x:8.9,y:-64.85},0).wait(1).to({regX:4,regY:12.7,rotation:0,x:15.15,y:-54.4},0).wait(1).to({x:4.65,y:-31.9},10,cjs.Ease.get(1)).wait(7));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_1_Layer_2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.instance = new lib.Symbol2();
	this.instance.parent = this;
	this.instance.setTransform(-33.8,-29.15,1,1,0,0,0,0.9,10.7);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(4).to({rotation:24.445,x:-16.2,y:-33.7},7,cjs.Ease.get(1)).to({rotation:0,x:-33.8,y:-29.15},4,cjs.Ease.get(-1)).wait(4).to({x:-35.2},4,cjs.Ease.get(1)).to({x:-33.8},5).wait(5).to({x:-35.2},4,cjs.Ease.get(1)).to({x:-33.8},5).wait(5).to({x:-35.2},4,cjs.Ease.get(1)).to({x:-33.8},5).wait(44));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_99 = function() {
		this.___loopingOver___ = true;
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(99).call(this.frame_99).wait(1));

	// Layer_2_obj_
	this.Layer_2 = new lib.Symbol_5_Layer_2();
	this.Layer_2.name = "Layer_2";
	this.Layer_2.parent = this;
	this.Layer_2.setTransform(3.4,27,1,1,0,0,0,3.4,27);
	this.Layer_2.depth = 0;
	this.Layer_2.isAttachedToCamera = 0
	this.Layer_2.isAttachedToMask = 0
	this.Layer_2.layerDepth = 0
	this.Layer_2.layerIndex = 0
	this.Layer_2.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_2).wait(100));

	// Layer_1_obj_
	this.Layer_1 = new lib.Symbol_5_Layer_1();
	this.Layer_1.name = "Layer_1";
	this.Layer_1.parent = this;
	this.Layer_1.depth = 0;
	this.Layer_1.isAttachedToCamera = 0
	this.Layer_1.isAttachedToMask = 0
	this.Layer_1.layerDepth = 0
	this.Layer_1.layerIndex = 1
	this.Layer_1.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_1).wait(100));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-32.1,-10.8,64.30000000000001,48.599999999999994);


(lib.Symbol3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_99 = function() {
		this.___loopingOver___ = true;
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(99).call(this.frame_99).wait(1));

	// Layer_2_obj_
	this.Layer_2 = new lib.Symbol_3_Layer_2();
	this.Layer_2.name = "Layer_2";
	this.Layer_2.parent = this;
	this.Layer_2.setTransform(-2.2,-42.3,1,1,0,0,0,-2.2,-42.3);
	this.Layer_2.depth = 0;
	this.Layer_2.isAttachedToCamera = 0
	this.Layer_2.isAttachedToMask = 0
	this.Layer_2.layerDepth = 0
	this.Layer_2.layerIndex = 0
	this.Layer_2.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_2).wait(62).to({regX:19.3,regY:-55.7,x:19.3,y:-55.7},0).wait(3).to({regX:-2.2,regY:-42.3,x:-2.2,y:-42.3},0).wait(1).to({regX:19.3,regY:-55.7,x:19.3,y:-55.7},0).wait(3).to({regX:-2.2,regY:-42.3,x:-2.2,y:-42.3},0).wait(1).to({regX:19.3,regY:-55.7,x:19.3,y:-55.7},0).wait(3).to({regX:-2.2,regY:-42.3,x:-2.2,y:-42.3},0).wait(1).to({regX:19.3,regY:-55.7,x:19.3,y:-55.7},0).wait(8).to({regX:-2.2,regY:-42.3,x:-2.2,y:-42.3},0).wait(18));

	// Layer_1_obj_
	this.Layer_1 = new lib.Symbol_3_Layer_1();
	this.Layer_1.name = "Layer_1";
	this.Layer_1.parent = this;
	this.Layer_1.depth = 0;
	this.Layer_1.isAttachedToCamera = 0
	this.Layer_1.isAttachedToMask = 0
	this.Layer_1.layerDepth = 0
	this.Layer_1.layerIndex = 1
	this.Layer_1.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_1).wait(100));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-25.8,-83.7,77,122.6);


(lib.Symbol1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_99 = function() {
		this.___loopingOver___ = true;
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(99).call(this.frame_99).wait(1));

	// Layer_2_obj_
	this.Layer_2 = new lib.Symbol_1_Layer_2();
	this.Layer_2.name = "Layer_2";
	this.Layer_2.parent = this;
	this.Layer_2.setTransform(-38.9,-36.1,1,1,0,0,0,-38.9,-36.1);
	this.Layer_2.depth = 0;
	this.Layer_2.isAttachedToCamera = 0
	this.Layer_2.isAttachedToMask = 0
	this.Layer_2.layerDepth = 0
	this.Layer_2.layerIndex = 0
	this.Layer_2.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_2).wait(100));

	// Layer_1_obj_
	this.Layer_1 = new lib.Symbol_1_Layer_1();
	this.Layer_1.name = "Layer_1";
	this.Layer_1.parent = this;
	this.Layer_1.depth = 0;
	this.Layer_1.isAttachedToCamera = 0
	this.Layer_1.isAttachedToMask = 0
	this.Layer_1.layerDepth = 0
	this.Layer_1.layerIndex = 1
	this.Layer_1.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_1).wait(100));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-49.2,-59.4,88.1,95.19999999999999);


(lib.Scene_1_Layer_7 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_7
	this.instance = new lib.Symbol3();
	this.instance.parent = this;
	this.instance.setTransform(301.05,447.9);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Scene_1_Layer_7, null, null);


(lib.Scene_1_Layer_6 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_6
	this.instance = new lib.Symbol1();
	this.instance.parent = this;
	this.instance.setTransform(136.25,434.8);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Scene_1_Layer_6, null, null);


(lib.Scene_1_Layer_5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_5
	this.instance = new lib.Symbol5();
	this.instance.parent = this;
	this.instance.setTransform(209.25,400.85);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Scene_1_Layer_5, null, null);


// stage content:
(lib.Oct_1_Vert = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	this.___GetDepth___ = function(obj) {
		var depth = obj.depth;
		var cameraObj = this.___camera___instance;
		if(cameraObj && cameraObj.depth && obj.isAttachedToCamera)
		{
			depth += depth + cameraObj.depth;
		}
		return depth;
		}
	this.___needSorting___ = function() {
		for (var i = 0; i < this.getNumChildren() - 1; i++)
		{
			var prevDepth = this.___GetDepth___(this.getChildAt(i));
			var nextDepth = this.___GetDepth___(this.getChildAt(i + 1));
			if (prevDepth < nextDepth)
				return true;
		}
		return false;
	}
	this.___sortFunction___ = function(obj1, obj2) {
		return (this.exportRoot.___GetDepth___(obj2) - this.exportRoot.___GetDepth___(obj1));
	}
	this.on('tick', function (event){
		var curTimeline = event.currentTarget;
		if (curTimeline.___needSorting___()){
			this.sortChildren(curTimeline.___sortFunction___);
		}
	});

	// Layer_5_obj_
	this.Layer_5 = new lib.Scene_1_Layer_5();
	this.Layer_5.name = "Layer_5";
	this.Layer_5.parent = this;
	this.Layer_5.setTransform(209.3,414.3,1,1,0,0,0,209.3,414.3);
	this.Layer_5.depth = 0;
	this.Layer_5.isAttachedToCamera = 0
	this.Layer_5.isAttachedToMask = 0
	this.Layer_5.layerDepth = 0
	this.Layer_5.layerIndex = 0
	this.Layer_5.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_5).wait(1));

	// Layer_7_obj_
	this.Layer_7 = new lib.Scene_1_Layer_7();
	this.Layer_7.name = "Layer_7";
	this.Layer_7.parent = this;
	this.Layer_7.setTransform(301.1,437.9,1,1,0,0,0,301.1,437.9);
	this.Layer_7.depth = 0;
	this.Layer_7.isAttachedToCamera = 0
	this.Layer_7.isAttachedToMask = 0
	this.Layer_7.layerDepth = 0
	this.Layer_7.layerIndex = 1
	this.Layer_7.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_7).wait(1));

	// Layer_3_obj_
	this.Layer_3 = new lib.Scene_1_Layer_3();
	this.Layer_3.name = "Layer_3";
	this.Layer_3.parent = this;
	this.Layer_3.setTransform(235.5,485.4,1,1,0,0,0,235.5,485.4);
	this.Layer_3.depth = 0;
	this.Layer_3.isAttachedToCamera = 0
	this.Layer_3.isAttachedToMask = 0
	this.Layer_3.layerDepth = 0
	this.Layer_3.layerIndex = 2
	this.Layer_3.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_3).wait(1));

	// Layer_6_obj_
	this.Layer_6 = new lib.Scene_1_Layer_6();
	this.Layer_6.name = "Layer_6";
	this.Layer_6.parent = this;
	this.Layer_6.setTransform(131.8,426.6,1,1,0,0,0,131.8,426.6);
	this.Layer_6.depth = 0;
	this.Layer_6.isAttachedToCamera = 0
	this.Layer_6.isAttachedToMask = 0
	this.Layer_6.layerDepth = 0
	this.Layer_6.layerIndex = 3
	this.Layer_6.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_6).wait(1));

	// Layer_9_obj_
	this.Layer_9 = new lib.Scene_1_Layer_9();
	this.Layer_9.name = "Layer_9";
	this.Layer_9.parent = this;
	this.Layer_9.setTransform(228.3,695.6,1,1,0,0,0,228.3,695.6);
	this.Layer_9.depth = 0;
	this.Layer_9.isAttachedToCamera = 0
	this.Layer_9.isAttachedToMask = 0
	this.Layer_9.layerDepth = 0
	this.Layer_9.layerIndex = 4
	this.Layer_9.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_9).wait(1));

	// Layer_8_obj_
	this.Layer_8 = new lib.Scene_1_Layer_8();
	this.Layer_8.name = "Layer_8";
	this.Layer_8.parent = this;
	this.Layer_8.setTransform(187.5,406,1,1,0,0,0,187.5,406);
	this.Layer_8.depth = 0;
	this.Layer_8.isAttachedToCamera = 0
	this.Layer_8.isAttachedToMask = 0
	this.Layer_8.layerDepth = 0
	this.Layer_8.layerIndex = 5
	this.Layer_8.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_8).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(187.5,406,898.5,406);
// library properties:
lib.properties = {
	id: 'B2EE0A6DACBEC548A9162DD1E75C08D4',
	width: 375,
	height: 812,
	fps: 30,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: [
		{src:"assets/images/bg_vert54-2.jpg", id:"bg_vert"},
		{src:"assets/images/pack52-2.png", id:"pack"},
		{src:"assets/images/shadow152-2.png", id:"shadow1"}
	],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['B2EE0A6DACBEC548A9162DD1E75C08D4'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}


// Layer depth API : 

AdobeAn.Layer = new function() {
	this.getLayerZDepth = function(timeline, layerName)
	{
		if(layerName === "Camera")
		layerName = "___camera___instance";
		var script = "if(timeline." + layerName + ") timeline." + layerName + ".depth; else 0;";
		return eval(script);
	}
	this.setLayerZDepth = function(timeline, layerName, zDepth)
	{
		const MAX_zDepth = 10000;
		const MIN_zDepth = -5000;
		if(zDepth > MAX_zDepth)
			zDepth = MAX_zDepth;
		else if(zDepth < MIN_zDepth)
			zDepth = MIN_zDepth;
		if(layerName === "Camera")
		layerName = "___camera___instance";
		var script = "if(timeline." + layerName + ") timeline." + layerName + ".depth = " + zDepth + ";";
		eval(script);
	}
	this.removeLayer = function(timeline, layerName)
	{
		if(layerName === "Camera")
		layerName = "___camera___instance";
		var script = "if(timeline." + layerName + ") timeline.removeChild(timeline." + layerName + ");";
		eval(script);
	}
	this.addNewLayer = function(timeline, layerName, zDepth)
	{
		if(layerName === "Camera")
		layerName = "___camera___instance";
		zDepth = typeof zDepth !== 'undefined' ? zDepth : 0;
		var layer = new createjs.MovieClip();
		layer.name = layerName;
		layer.depth = zDepth;
		layer.layerIndex = 0;
		timeline.addChild(layer);
	}
}


})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;

$(function () {
  var canvas, stage, exportRoot, anim_container, dom_overlay_container, fnStartAnimation;
  function init() {
    canvas = document.getElementById("canvas54-2");
    anim_container = document.getElementById("animation_container54-2");
    dom_overlay_container = document.getElementById("dom_overlay_container54-2");
    var comp=AdobeAn.getComposition("B2EE0A6DACBEC548A9162DD1E75C08D4");
    var lib=comp.getLibrary();
    var loader = new createjs.LoadQueue(false);
    loader.addEventListener("fileload", function(evt){handleFileLoad(evt,comp)});
    loader.addEventListener("complete", function(evt){handleComplete(evt,comp)});
    var lib=comp.getLibrary();
    loader.loadManifest(lib.properties.manifest);
  }
  function handleFileLoad(evt, comp) {
    var images=comp.getImages();
    if (evt && (evt.item.type == "image")) { images[evt.item.id] = evt.result; }
  }
  function handleComplete(evt,comp) {
    //This function is always called, irrespective of the content. You can use the variable "stage" after it is created in token create_stage.
    var lib=comp.getLibrary();
    var ss=comp.getSpriteSheet();
    var queue = evt.target;
    var ssMetadata = lib.ssMetadata;
    for(i=0; i<ssMetadata.length; i++) {
      ss[ssMetadata[i].name] = new createjs.SpriteSheet( {"images": [queue.getResult(ssMetadata[i].name)], "frames": ssMetadata[i].frames} )
    }
    var preloaderDiv = document.getElementById("_preload_div_54");
    preloaderDiv.style.display = 'none';
    canvas.style.display = 'block';
    exportRoot = new lib.Oct_1_Vert();
    stage = new lib.Stage(canvas);
    //Registers the "tick" event listener.
    fnStartAnimation = function() {
      stage.addChild(exportRoot);
      createjs.Ticker.setFPS(lib.properties.fps);
      createjs.Ticker.addEventListener("tick", stage)
      stage.addEventListener("tick", handleTick)
      function getProjectionMatrix(container, totalDepth) {
        var focalLength = 528.25;
        var projectionCenter = { x : lib.properties.width/2, y : lib.properties.height/2 };
        var scale = (totalDepth + focalLength)/focalLength;
        var scaleMat = new createjs.Matrix2D;
        scaleMat.a = 1/scale;
        scaleMat.d = 1/scale;
        var projMat = new createjs.Matrix2D;
        projMat.tx = -projectionCenter.x;
        projMat.ty = -projectionCenter.y;
        projMat = projMat.prependMatrix(scaleMat);
        projMat.tx += projectionCenter.x;
        projMat.ty += projectionCenter.y;
        return projMat;
      }
      function handleTick(event) {
        var cameraInstance = exportRoot.___camera___instance;
        if(cameraInstance !== undefined && cameraInstance.pinToObject !== undefined)
        {
          cameraInstance.x = cameraInstance.pinToObject.x + cameraInstance.pinToObject.pinOffsetX;
          cameraInstance.y = cameraInstance.pinToObject.y + cameraInstance.pinToObject.pinOffsetY;
          if(cameraInstance.pinToObject.parent !== undefined && cameraInstance.pinToObject.parent.depth !== undefined)
            cameraInstance.depth = cameraInstance.pinToObject.parent.depth + cameraInstance.pinToObject.pinOffsetZ;
        }
        applyLayerZDepth(exportRoot);
      }
      function applyLayerZDepth(parent)
      {
        var cameraInstance = parent.___camera___instance;
        var focalLength = 528.25;
        var projectionCenter = { 'x' : 0, 'y' : 0};
        if(parent === exportRoot)
        {
          var stageCenter = { 'x' : lib.properties.width/2, 'y' : lib.properties.height/2 };
          projectionCenter.x = stageCenter.x;
          projectionCenter.y = stageCenter.y;
        }
        for(child in parent.children)
        {
          var layerObj = parent.children[child];
          if(layerObj == cameraInstance)
            continue;
          applyLayerZDepth(layerObj, cameraInstance);
          if(layerObj.layerDepth === undefined)
            continue;
          if(layerObj.currentFrame != layerObj.parent.currentFrame)
          {
            layerObj.gotoAndPlay(layerObj.parent.currentFrame);
          }
          var matToApply = new createjs.Matrix2D;
          var cameraMat = new createjs.Matrix2D;
          var totalDepth = layerObj.layerDepth ? layerObj.layerDepth : 0;
          var cameraDepth = 0;
          if(cameraInstance && !layerObj.isAttachedToCamera)
          {
            var mat = cameraInstance.getMatrix();
            mat.tx -= projectionCenter.x;
            mat.ty -= projectionCenter.y;
            cameraMat = mat.invert();
            cameraMat.prependTransform(projectionCenter.x, projectionCenter.y, 1, 1, 0, 0, 0, 0, 0);
            cameraMat.appendTransform(-projectionCenter.x, -projectionCenter.y, 1, 1, 0, 0, 0, 0, 0);
            if(cameraInstance.depth)
              cameraDepth = cameraInstance.depth;
          }
          if(layerObj.depth)
          {
            totalDepth = layerObj.depth;
          }
          //Offset by camera depth
          totalDepth -= cameraDepth;
          if(totalDepth < -focalLength)
          {
            matToApply.a = 0;
            matToApply.d = 0;
          }
          else
          {
            if(layerObj.layerDepth)
            {
              var sizeLockedMat = getProjectionMatrix(parent, layerObj.layerDepth);
              if(sizeLockedMat)
              {
                sizeLockedMat.invert();
                matToApply.prependMatrix(sizeLockedMat);
              }
            }
            matToApply.prependMatrix(cameraMat);
            var projMat = getProjectionMatrix(parent, totalDepth);
            if(projMat)
            {
              matToApply.prependMatrix(projMat);
            }
          }
          layerObj.transformMatrix = matToApply;
        }
      }
    }
    //Code to support hidpi screens and responsive scaling.
    function makeResponsive(isResp, respDim, isScale, scaleType) {
      var lastW, lastH, lastS=1;
      window.addEventListener('resize', resizeCanvas);
      resizeCanvas();
      function resizeCanvas() {
        var w = lib.properties.width, h = lib.properties.height;
        var iw = window.innerWidth, ih=window.innerHeight;
        var pRatio = window.devicePixelRatio || 1, xRatio=iw/w, yRatio=ih/h, sRatio=1;
        if(isResp) {
          if((respDim=='width'&&lastW==iw) || (respDim=='height'&&lastH==ih)) {
            sRatio = lastS;
          }
          else if(!isScale) {
            if(iw<w || ih<h)
              sRatio = Math.min(xRatio, yRatio);
          }
          else if(scaleType==1) {
            sRatio = Math.min(xRatio, yRatio);
          }
          else if(scaleType==2) {
            sRatio = Math.max(xRatio, yRatio);
          }
        }
        canvas.width = w*pRatio*sRatio;
        canvas.height = h*pRatio*sRatio;
        canvas.style.width = anim_container.style.width = dom_overlay_container.style.width = preloaderDiv.style.width = w*sRatio+'px';
        canvas.style.height = anim_container.style.height = dom_overlay_container.style.height = preloaderDiv.style.height = h*sRatio+'px';
        stage.scaleX = pRatio*sRatio;
        stage.scaleY = pRatio*sRatio;
        lastW = iw; lastH = ih; lastS = sRatio;
        stage.tickOnUpdate = false;
        stage.update();
        stage.tickOnUpdate = true;
      }
    }
    makeResponsive(true,'both',true,2);
    AdobeAn.compositionLoaded(lib.properties.id);
    fnStartAnimation();
  }
  init();
});