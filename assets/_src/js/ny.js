(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [];


// symbols:



(lib.beard = function() {
	this.initialize(img.beard);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,160,263);


(lib.pack = function() {
	this.initialize(img.pack);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,1080,680);// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.Symbol5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.instance = new lib.beard();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	// Layer_1
	this.instance_1 = new lib.beard();
	this.instance_1.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol5, new cjs.Rectangle(0,0,160,263), null);


(lib.Symbol2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(10.5,1,1).p("AgcApQhHhsCXAk");
	this.shape.setTransform(7.2864,11.082);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	// Layer_1
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(10.5,1,1).p("AAABMQhriOCYgJ");
	this.shape_1.setTransform(4.4983,7.575);

	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol2, new cjs.Rectangle(-5.2,-5.2,22.9,25.599999999999998), null);


(lib.Symbol_6_Layer_4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_4
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(10.5,1,1).p("ABrAzQAAgrgcgaQgbgcgpgEQgogDgiAVQgjAWgHAo");
	this.shape.setTransform(68.7,5.1005);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(10.5,1,1).p("AhpAfQAHgqAjgWQAigWAoADQApAEAbAdQAcAcAAAs");
	this.shape_1.setTransform(68.7,4.651);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(10.5,1,1).p("AhpAhQAHgsAjgXQAigXAoAEQApAEAbAeQAcAcAAAu");
	this.shape_2.setTransform(68.7,4.2459);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(10.5,1,1).p("AhpAiQAHgtAjgYQAigYAoAEQApAEAbAfQAcAeAAAv");
	this.shape_3.setTransform(68.7,3.8773);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(10.5,1,1).p("AhpAiQAHgtAjgZQAigYAoADQApAEAbAgQAcAfAAAw");
	this.shape_4.setTransform(68.7,3.5777);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(10.5,1,1).p("AhpAjQAHgvAjgZQAigZAoAEQApAEAbAhQAcAfAAAx");
	this.shape_5.setTransform(68.7,3.3224);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(10.5,1,1).p("AhpAkQAHgwAjgZQAigaAoAEQApAFAbAhQAcAfAAAz");
	this.shape_6.setTransform(68.7,3.0979);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(10.5,1,1).p("AhpAkQAHgwAjgaQAigaAoAEQApAFAbAhQAcAgAAAz");
	this.shape_7.setTransform(68.7,2.9233);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(10.5,1,1).p("AhpAkQAHgwAjgbQAigZAoAEQApAEAbAiQAcAgAAA0");
	this.shape_8.setTransform(68.7,2.8229);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(10.5,1,1).p("AhpAlQAHgxAjgaQAigaAoAEQApAEAbAiQAcAgAAA0");
	this.shape_9.setTransform(68.7,2.7483);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(10.5,1,1).p("ABrA+QAAg0gcggQgbgigpgFQgogEgiAaQgjAbgHAx");
	this.shape_10.setTransform(68.7,2.7233);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(10.5,1,1).p("AhpAjQAHgvAjgZQAigaAoAEQApAFAbAhQAcAfAAAy");
	this.shape_11.setTransform(68.7,3.1729);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(10.5,1,1).p("AhpAhQAHgsAjgYQAigYAoAEQApAEAbAfQAcAdAAAw");
	this.shape_12.setTransform(68.7,3.9464);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(10.5,1,1).p("AhpAgQAHgrAjgWQAigXAoAEQApADAbAeQAcAcAAAt");
	this.shape_13.setTransform(68.7,4.5204);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(10.5,1,1).p("AhpAfQAHgpAjgXQAigWAoAEQApADAbAdQAcAcAAAs");
	this.shape_14.setTransform(68.7,4.726);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(10.5,1,1).p("AhpAeQAHgpAjgWQAigVAoADQApAEAbAcQAcAbAAAs");
	this.shape_15.setTransform(68.7,4.9005);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(10.5,1,1).p("AhpAeQAHgoAjgWQAigWAoAEQApADAbAdQAcAaAAAr");
	this.shape_16.setTransform(68.7,5.001);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(10.5,1,1).p("AhpAeQAHgoAjgWQAigVAoADQApAEAbAcQAcAbAAAq");
	this.shape_17.setTransform(68.7,5.0755);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},14).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_10}]},20).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape}]},1).wait(21));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_6_Layer_3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_3
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(10.5,1,1).p("AiBBEQADg9AighQAigjAtgFQArgEArAbQArAbAOAs");
	this.shape.setTransform(12.95,9.0929);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(10.5,1,1).p("AiBBGQADg/AigjQAigkAtgFQArgFArAdQArAcAOAu");
	this.shape_1.setTransform(12.95,8.8127);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(10.5,1,1).p("AiBBJQADhCAigkQAigmAtgEQArgFArAdQArAdAOAw");
	this.shape_2.setTransform(12.95,8.5632);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(10.5,1,1).p("AiBBLQADhDAiglQAignAtgFQArgFArAeQArAeAOAx");
	this.shape_3.setTransform(12.95,8.3393);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(10.5,1,1).p("AiBBNQADhFAigmQAigoAtgFQArgFArAfQArAeAOAy");
	this.shape_4.setTransform(12.95,8.1403);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(10.5,1,1).p("AiBBPQADhHAignQAigpAtgFQArgFArAfQArAgAOAz");
	this.shape_5.setTransform(12.95,7.9841);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(10.5,1,1).p("AiBBQQADhIAignQAigqAtgFQArgFArAgQArAfAOA1");
	this.shape_6.setTransform(12.95,7.8412);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(10.5,1,1).p("AiBBRQADhJAigoQAigqAtgFQArgGArAhQArAgAOA1");
	this.shape_7.setTransform(12.95,7.7352);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(10.5,1,1).p("AiBBSQADhKAigoQAigrAtgFQArgGArAhQArAhAOA1");
	this.shape_8.setTransform(12.95,7.6607);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(10.5,1,1).p("AiBBSQADhKAigpQAigqAtgFQArgGArAhQArAgAOA2");
	this.shape_9.setTransform(12.95,7.6352);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(10.5,1,1).p("AiBBSQADhKAigpQAigqAtgGQArgFArAhQArAgAOA2");
	this.shape_10.setTransform(12.95,7.6107);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(10.5,1,1).p("AiBBQQADhIAignQAigpAtgGQArgFArAgQArAfAOA0");
	this.shape_11.setTransform(12.95,7.8908);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(10.5,1,1).p("AiBBLQADhDAigmQAigmAtgFQArgFArAeQArAdAOAx");
	this.shape_12.setTransform(12.95,8.3643);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(10.5,1,1).p("AiBBHQADhAAigjQAiglAtgFQArgEArAcQArAdAOAu");
	this.shape_13.setTransform(12.95,8.7193);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(10.5,1,1).p("AiBBGQADg/AigjQAigkAtgEQArgFArAcQArAcAOAt");
	this.shape_14.setTransform(12.95,8.8621);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(10.5,1,1).p("AiBBFQADg+AigiQAigkAtgEQArgFArAcQArAbAOAt");
	this.shape_15.setTransform(12.95,8.9684);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(10.5,1,1).p("AiBBEQADg9AigiQAigjAtgEQArgFArAbQArAbAOAt");
	this.shape_16.setTransform(12.95,9.0429);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(10.5,1,1).p("AiBBEQADg9AighQAigkAtgEQArgFArAcQArAbAOAs");
	this.shape_17.setTransform(12.95,9.0684);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},14).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_10}]},20).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape}]},1).wait(21));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_4_Layer_3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_3
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(10.5,1,1).p("AmhhGQGODaG1h1");
	this.shape.setTransform(41.8,10.9477);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(10.5,1,1).p("AmhhGQGODZG1h1");
	this.shape_1.setTransform(41.8,10.9037);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(10.5,1,1).p("AmhhFQGODXG1h1");
	this.shape_2.setTransform(41.825,10.7835);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(10.5,1,1).p("AmhhDQGPDSG0h1");
	this.shape_3.setTransform(41.875,10.5753);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(10.5,1,1).p("AmihAQGQDLG1h1");
	this.shape_4.setTransform(41.9,10.2822);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(10.5,1,1).p("Amig8QGSDDGzh2");
	this.shape_5.setTransform(41.975,9.927);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(10.5,1,1).p("Amig4QGTC5Gyh2");
	this.shape_6.setTransform(42.075,9.4783);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(10.5,1,1).p("AmigzQGVCtGwh2");
	this.shape_7.setTransform(42.15,8.9633);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(10.5,1,1).p("AmjgtQGYCfGvh1");
	this.shape_8.setTransform(42.275,8.3961);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(10.5,1,1).p("AmkgmQGbCPGuh2");
	this.shape_9.setTransform(42.4,7.7422);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(10.5,1,1).p("AmkggQGdB+Gsh3");
	this.shape_10.setTransform(42.55,7.0504);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(10.5,1,1).p("AmjgoQGZCTGuh2");
	this.shape_11.setTransform(42.375,7.9096);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(10.5,1,1).p("AmjgwQGXCmGwh2");
	this.shape_12.setTransform(42.225,8.6928);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(10.5,1,1).p("Amig3QGTC2Gyh2");
	this.shape_13.setTransform(42.1,9.363);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(10.5,1,1).p("AmihBQGQDNG1h1");
	this.shape_14.setTransform(41.9,10.369);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(10.5,1,1).p("AmhhEQGODUG1h0");
	this.shape_15.setTransform(41.85,10.6845);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(10.5,1,1).p("AmhhDQGODTG1h1");
	this.shape_16.setTransform(41.875,10.6408);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(10.5,1,1).p("AmihBQGQDOG1h1");
	this.shape_17.setTransform(41.9,10.4125);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#000000").ss(10.5,1,1).p("Amhg+QGQDHG0h1");
	this.shape_18.setTransform(41.95,10.1091);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#000000").ss(10.5,1,1).p("Amig6QGSC+Gzh1");
	this.shape_19.setTransform(42.025,9.734);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#000000").ss(10.5,1,1).p("Amig2QGTC0Gyh1");
	this.shape_20.setTransform(42.125,9.3087);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#000000").ss(10.5,1,1).p("AmjgxQGWCpGxh2");
	this.shape_21.setTransform(42.2,8.8174);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#000000").ss(10.5,1,1).p("AmjgsQGYCcGvh1");
	this.shape_22.setTransform(42.3,8.2735);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#000000").ss(10.5,1,1).p("AmkgmQGbCOGuh2");
	this.shape_23.setTransform(42.4,7.6826);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},34).to({state:[{t:this.shape_1,p:{y:10.9037}}]},1).to({state:[{t:this.shape_2,p:{y:10.7835}}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_1,p:{y:10.8818}}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape_1,p:{y:10.9037}}]},1).to({state:[{t:this.shape_2,p:{y:10.8159}}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_1,p:{y:10.8818}}]},1).to({state:[{t:this.shape}]},1).wait(3));
	this.timeline.addTween(cjs.Tween.get(this.shape).wait(34).to({_off:true},1).wait(17).to({_off:false},0).wait(1).to({_off:true},1).wait(18).to({_off:false},0).wait(3));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_3_Symbol_2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Symbol_2
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(13,1,1).p("ABFBGQAjjdiyCB");
	this.shape.setTransform(7.4794,18.6195);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(13,1,1).p("AhEgPQCmiKgjDd");
	this.shape_1.setTransform(8.0703,18.805);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(13,1,1).p("Ag/gGQCbiUgiDd");
	this.shape_2.setTransform(8.6374,18.99);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(13,1,1).p("AA0BBQAijdiPCc");
	this.shape_3.setTransform(9.2325,19.1567);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(13,1,1).p("Ag6goQCRhqgjDd");
	this.shape_4.setTransform(9.1547,18.1257);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(13,1,1).p("Ag7hOQCSg2giDd");
	this.shape_5.setTransform(9.1029,16.7055);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(13,1,1).p("AA2BuQAijdiTAC");
	this.shape_6.setTransform(9.0252,14.6245);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(13,1,1).p("AhAhXQCdgogjDd");
	this.shape_7.setTransform(8.56,16.2626);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(13,1,1).p("AhDhBQCkhIgjDd");
	this.shape_8.setTransform(8.1733,17.2684);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(13,1,1).p("AhGgvQCqhhgjDd");
	this.shape_9.setTransform(7.8646,17.9252);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(13,1,1).p("AhIghQCuhzgjDd");
	this.shape_10.setTransform(7.6591,18.327);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(13,1,1).p("AhJgZQCwh9giDd");
	this.shape_11.setTransform(7.5307,18.5486);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},9).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_3}]},7).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_6}]},4).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape}]},1).wait(43));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_3_Symbol_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Symbol_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(13,1,1).p("AAwCWQC4kTlXgY");
	this.shape.setTransform(13.5455,14.95);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(13,1,1).p("AAwCUQC4kQlXgX");
	this.shape_1.setTransform(13.5667,14.825);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(13,1,1).p("AhsiEQFAgTiYEd");
	this.shape_2.setTransform(12.6038,16.2879);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(13,1,1).p("AhphwQEpg/h4Er");
	this.shape_3.setTransform(11.6954,17.3276);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(13,1,1).p("ABTB1QBYk4kSBq");
	this.shape_4.setTransform(10.8446,18.0552);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(13,1,1).p("ABQB3QBdk9kTBr");
	this.shape_5.setTransform(10.7171,18.2945);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(13,1,1).p("Ahhh9QEYgsh6Et");
	this.shape_6.setTransform(12.6088,16.7702);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(13,1,1).p("AhdiYQEdATiWEe");
	this.shape_7.setTransform(14.619,14.475);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(13,1,1).p("AAWCwQCzkPkihQ");
	this.shape_8.setTransform(16.6682,11.9);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(13,1,1).p("AAVCyQC0kTkihQ");
	this.shape_9.setTransform(16.63,12.125);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(13,1,1).p("AhgipQEyA/i0EU");
	this.shape_10.setTransform(15.7166,13.05);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(13,1,1).p("AhmiiQFAAwi0EW");
	this.shape_11.setTransform(14.9618,13.8);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(13,1,1).p("AhqidQFKAli0EW");
	this.shape_12.setTransform(14.3984,14.425);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(13,1,1).p("AhtiaQFRAei0EX");
	this.shape_13.setTransform(13.9774,14.825);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(13,1,1).p("AhviYQFVAZi0EY");
	this.shape_14.setTransform(13.7255,15.1);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(13,1,1).p("AAzCYQC0kXlXgY");
	this.shape_15.setTransform(13.6416,15.175);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape_1}]},9).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},7).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},4).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).wait(43));

}).prototype = p = new cjs.MovieClip();


(lib.Scene_1_Layer_2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.instance = new lib.pack();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Scene_1_Layer_2, null, null);


(lib.Symbol2_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FB0200").s().p("AAKA0IgDAAQgRgCgPgRQgPgQgDgVQgDgVAKgOQAMgOASACIADABQAQADANAPQAQARADAUQADAWgLANQgKAMgPAAIgCAAg");
	this.shape_2.setTransform(3.5988,2.7909);

	this.timeline.addTween(cjs.Tween.get(this.shape_2).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol2_1, new cjs.Rectangle(-0.9,-2.4,9,10.4), null);


(lib.Symbol3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_74 = function() {
		this.___loopingOver___ = true;
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(74).call(this.frame_74).wait(1));

	// Symbol_1_obj_
	this.Symbol_1 = new lib.Symbol_3_Symbol_1();
	this.Symbol_1.name = "Symbol_1";
	this.Symbol_1.parent = this;
	this.Symbol_1.setTransform(13.6,15,1,1,0,0,0,13.6,15);
	this.Symbol_1.depth = 0;
	this.Symbol_1.isAttachedToCamera = 0
	this.Symbol_1.isAttachedToMask = 0
	this.Symbol_1.layerDepth = 0
	this.Symbol_1.layerIndex = 0
	this.Symbol_1.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Symbol_1).wait(75));

	// Symbol_2_obj_
	this.Symbol_2 = new lib.Symbol_3_Symbol_2();
	this.Symbol_2.name = "Symbol_2";
	this.Symbol_2.parent = this;
	this.Symbol_2.setTransform(7.5,18.6,1,1,0,0,0,7.5,18.6);
	this.Symbol_2.depth = 0;
	this.Symbol_2.isAttachedToCamera = 0
	this.Symbol_2.isAttachedToMask = 0
	this.Symbol_2.layerDepth = 0
	this.Symbol_2.layerIndex = 1
	this.Symbol_2.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Symbol_2).wait(75));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-6.5,-12.2,38.7,49.099999999999994);


(lib.Symbol1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000601").ss(13,1,1).p("AkNhWQBZCgCSALQCRAKBGgoQBGgpAShO");
	this.shape.setTransform(155.05,-52.5294);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(75));

	// Layer_3
	this.instance = new lib.Symbol2_1();
	this.instance.parent = this;
	this.instance.setTransform(137.2,-49.4,2.0168,1.0421,0,46.0839,51.8739,5.4,7.5);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(12).to({_off:false},0).to({scaleY:2.0168,guide:{path:[137.2,-49.3,137.5,-49,137.9,-48.7]}},4).wait(1).to({regX:3.6,regY:2.8,scaleX:2.0159,scaleY:2.0177,skewX:45.7195,skewY:51.4594,x:142.6,y:-58},0).wait(1).to({scaleX:2.0136,scaleY:2.0199,skewX:44.7878,skewY:50.3997,x:143,y:-57.65},0).wait(1).to({scaleX:2.0096,scaleY:2.0237,skewX:43.1697,skewY:48.559,x:143.85,y:-57},0).wait(1).to({scaleX:2.0035,scaleY:2.0294,skewX:40.7228,skewY:45.7757,x:145.2,y:-56.15},0).wait(1).to({scaleX:1.995,scaleY:2.0375,skewX:37.2999,skewY:41.8822,x:147.15,y:-55.1},0).wait(1).to({scaleX:1.9839,scaleY:2.0481,skewX:32.8139,skewY:36.7794,x:150,y:-54.15},0).wait(1).to({scaleX:1.9705,scaleY:2.0609,skewX:27.3853,skewY:30.6045,x:153.55,y:-53.6},0).wait(1).to({scaleX:1.9559,scaleY:2.0748,skewX:21.5115,skewY:23.9231,x:157.4,y:-53.55},0).wait(1).to({scaleX:1.9422,scaleY:2.0879,skewX:15.9743,skewY:17.6245,x:160.65,y:-54},0).wait(1).to({scaleX:1.9309,scaleY:2.0987,skewX:11.4074,skewY:12.4298,x:163.3,y:-54.65},0).wait(1).to({scaleX:1.9225,scaleY:2.1066,skewX:8.0351,skewY:8.5938,x:165.2,y:-55.3},0).wait(1).to({scaleX:1.917,scaleY:2.1119,skewX:5.788,skewY:6.0378,x:166.4,y:-55.85},0).wait(1).to({scaleX:1.9137,scaleY:2.115,skewX:4.4942,skewY:4.5662,x:167.1,y:-56.25},0).wait(1).to({regX:5.4,regY:7.5,scaleX:1.9125,scaleY:2.1162,rotation:3.9793,skewX:0,skewY:0,x:170.2,y:-46.35},0).wait(7).to({regX:3.6,regY:2.8,scaleX:1.914,scaleY:2.1148,rotation:0,skewX:4.5733,skewY:4.6561,x:167.15,y:-56.2},0).wait(1).to({scaleX:1.9174,scaleY:2.1115,skewX:5.9626,skewY:6.2364,x:166.3,y:-55.8},0).wait(1).to({scaleX:1.923,scaleY:2.1061,skewX:8.2536,skewY:8.8424,x:164.95,y:-55.2},0).wait(1).to({scaleX:1.9312,scaleY:2.0984,skewX:11.5185,skewY:12.5562,x:162.9,y:-54.5},0).wait(1).to({scaleX:1.9416,scaleY:2.0884,skewX:15.7359,skewY:17.3534,x:160.1,y:-53.8},0).wait(1).to({scaleX:1.9539,scaleY:2.0767,skewX:20.7133,skewY:23.0151,x:156.75,y:-53.45},0).wait(1).to({scaleX:1.9672,scaleY:2.0641,skewX:26.0541,skewY:29.0903,x:153.45,y:-53.55},0).wait(1).to({scaleX:1.98,scaleY:2.0518,skewX:31.248,skewY:34.9982,x:150.6,y:-53.95},0).wait(1).to({scaleX:1.9914,scaleY:2.0409,skewX:35.8517,skewY:40.2349,x:148,y:-54.65},0).wait(1).to({scaleX:2.0007,scaleY:2.0321,skewX:39.611,skewY:44.5111,x:145.95,y:-55.6},0).wait(1).to({scaleX:2.0078,scaleY:2.0254,skewX:42.4537,skewY:47.7446,x:144.35,y:-56.55},0).wait(1).to({scaleX:2.0127,scaleY:2.0207,skewX:44.4197,skewY:49.9809,x:143.35,y:-57.35},0).wait(1).to({scaleX:2.0156,scaleY:2.0179,skewX:45.5968,skewY:51.3198,x:142.75,y:-57.9},0).wait(1).to({regX:5.4,regY:7.5,scaleX:2.0168,scaleY:2.0168,skewX:46.0839,skewY:51.8739,x:137.85,y:-48.75},0).to({regX:5.5,regY:7.4,scaleY:1.2283,skewX:46.0842,x:134.95,y:-45.55},4).to({_off:true},1).wait(20));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(121.6,-72.7,66.9,35.400000000000006);


(lib.Symbol_6_Symbol_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Symbol_1
	this.instance = new lib.Symbol1();
	this.instance.parent = this;
	this.instance.setTransform(44.8,41.85,0.6848,0.6848,-5.1876,0,0,156.1,-52.7);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(75));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_4_Symbol_2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Symbol_2
	this.instance = new lib.Symbol2();
	this.instance.parent = this;
	this.instance.setTransform(79.2,7.5,1,1,0,0,0,4.5,7.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(34).to({regX:4.6,rotation:8.5171,x:83.2,y:-2.2},10,cjs.Ease.get(-1)).to({regX:4.5,rotation:0,x:79.2,y:7.5},8,cjs.Ease.get(1)).wait(1).to({regX:4.6,rotation:8.5171,x:83.2,y:-2.2},11,cjs.Ease.get(-1)).to({regX:4.5,rotation:0,x:79.2,y:7.5},8,cjs.Ease.get(1)).wait(3));

}).prototype = p = new cjs.MovieClip();


(lib.Scene_1_Layer_3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_3
	this.instance = new lib.Symbol5();
	this.instance.parent = this;
	this.instance.setTransform(768,318.5,1,1,0,0,0,80,131.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Scene_1_Layer_3, null, null);


(lib.Symbol1_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Symbol_3
	this.instance_1 = new lib.Symbol3();
	this.instance_1.parent = this;
	this.instance_1.setTransform(11.8,33.35,0.6525,0.6525,0,0,0,18.1,29.8);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(1));

	// Layer_3
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(8,1,1).p("AD7jfQAWCqgyBzQguBnhcAoQhWAlhfggQhighg/hc");
	this.shape_1.setTransform(35.7088,22.4493);

	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol1_1, new cjs.Rectangle(-4.2,-4,69.7,52.9), null);


(lib.Symbol6 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_74 = function() {
		this.___loopingOver___ = true;
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(74).call(this.frame_74).wait(1));

	// Layer_4_obj_
	this.Layer_4 = new lib.Symbol_6_Layer_4();
	this.Layer_4.name = "Layer_4";
	this.Layer_4.parent = this;
	this.Layer_4.setTransform(68.7,5.1,1,1,0,0,0,68.7,5.1);
	this.Layer_4.depth = 0;
	this.Layer_4.isAttachedToCamera = 0
	this.Layer_4.isAttachedToMask = 0
	this.Layer_4.layerDepth = 0
	this.Layer_4.layerIndex = 0
	this.Layer_4.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_4).wait(75));

	// Layer_3_obj_
	this.Layer_3 = new lib.Symbol_6_Layer_3();
	this.Layer_3.name = "Layer_3";
	this.Layer_3.parent = this;
	this.Layer_3.setTransform(13,9.1,1,1,0,0,0,13,9.1);
	this.Layer_3.depth = 0;
	this.Layer_3.isAttachedToCamera = 0
	this.Layer_3.isAttachedToMask = 0
	this.Layer_3.layerDepth = 0
	this.Layer_3.layerIndex = 1
	this.Layer_3.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_3).wait(75));

	// Symbol_1_obj_
	this.Symbol_1 = new lib.Symbol_6_Symbol_1();
	this.Symbol_1.name = "Symbol_1";
	this.Symbol_1.parent = this;
	this.Symbol_1.setTransform(3.8,58.6,1,1,0,0,0,3.8,58.6);
	this.Symbol_1.depth = 0;
	this.Symbol_1.isAttachedToCamera = 0
	this.Symbol_1.isAttachedToMask = 0
	this.Symbol_1.layerDepth = 0
	this.Symbol_1.layerIndex = 2
	this.Symbol_1.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Symbol_1).wait(75));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-5.2,-8.7,89.8,63.2);


(lib.Symbol4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_74 = function() {
		this.___loopingOver___ = true;
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(74).call(this.frame_74).wait(1));

	// Layer_1 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("Aq6K1IAA1pIV1AAIAAVpg");
	mask.setTransform(83.275,12.35);

	// Symbol_2_obj_
	this.Symbol_2 = new lib.Symbol_4_Symbol_2();
	this.Symbol_2.name = "Symbol_2";
	this.Symbol_2.parent = this;
	this.Symbol_2.setTransform(80.9,7.5,1,1,0,0,0,80.9,7.5);
	this.Symbol_2.depth = 0;
	this.Symbol_2.isAttachedToCamera = 0
	this.Symbol_2.isAttachedToMask = 0
	this.Symbol_2.layerDepth = 0
	this.Symbol_2.layerIndex = 0
	this.Symbol_2.maskLayerName = 0

	var maskedShapeInstanceList = [this.Symbol_2];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.Symbol_2).wait(75));

	// Layer_3_obj_
	this.Layer_3 = new lib.Symbol_4_Layer_3();
	this.Layer_3.name = "Layer_3";
	this.Layer_3.parent = this;
	this.Layer_3.setTransform(41.8,11,1,1,0,0,0,41.8,11);
	this.Layer_3.depth = 0;
	this.Layer_3.isAttachedToCamera = 0
	this.Layer_3.isAttachedToMask = 0
	this.Layer_3.layerDepth = 0
	this.Layer_3.layerIndex = 1
	this.Layer_3.maskLayerName = 0

	var maskedShapeInstanceList = [this.Layer_3];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.Layer_3).wait(75));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(13.4,-16.3,83.6,39.7);


(lib.Scene_1_Layer_10 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_10
	this.instance = new lib.Symbol4();
	this.instance.parent = this;
	this.instance.setTransform(678.9,333.8,1,1,0,0,0,44.2,9.1);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Scene_1_Layer_10, null, null);


(lib.Scene_1_Layer_8 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_8
	this.instance = new lib.Symbol6();
	this.instance.parent = this;
	this.instance.setTransform(606.9,265.55,1,1,0,0,0,39.6,24.6);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Scene_1_Layer_8, null, null);


(lib.Scene_1_Layer_5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_5
	this.instance = new lib.Symbol1_1();
	this.instance.parent = this;
	this.instance.setTransform(563.65,348.25,1.23,1.23,0,0,180,30.6,22.6);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Scene_1_Layer_5, null, null);


// stage content:
(lib.Dec = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	this.___GetDepth___ = function(obj) {
		var depth = obj.depth;
		var cameraObj = this.___camera___instance;
		if(cameraObj && cameraObj.depth && obj.isAttachedToCamera)
		{
			depth += depth + cameraObj.depth;
		}
		return depth;
		}
	this.___needSorting___ = function() {
		for (var i = 0; i < this.getNumChildren() - 1; i++)
		{
			var prevDepth = this.___GetDepth___(this.getChildAt(i));
			var nextDepth = this.___GetDepth___(this.getChildAt(i + 1));
			if (prevDepth < nextDepth)
				return true;
		}
		return false;
	}
	this.___sortFunction___ = function(obj1, obj2) {
		return (this.exportRoot.___GetDepth___(obj2) - this.exportRoot.___GetDepth___(obj1));
	}
	this.on('tick', function (event){
		var curTimeline = event.currentTarget;
		if (curTimeline.___needSorting___()){
			this.sortChildren(curTimeline.___sortFunction___);
		}
	});

	// Layer_8_obj_
	this.Layer_8 = new lib.Scene_1_Layer_8();
	this.Layer_8.name = "Layer_8";
	this.Layer_8.parent = this;
	this.Layer_8.setTransform(578.3,282.1,1,1,0,0,0,578.3,282.1);
	this.Layer_8.depth = 0;
	this.Layer_8.isAttachedToCamera = 0
	this.Layer_8.isAttachedToMask = 0
	this.Layer_8.layerDepth = 0
	this.Layer_8.layerIndex = 0
	this.Layer_8.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_8).wait(1));

	// Layer_5_obj_
	this.Layer_5 = new lib.Scene_1_Layer_5();
	this.Layer_5.name = "Layer_5";
	this.Layer_5.parent = this;
	this.Layer_5.setTransform(563.6,348.1,1,1,0,0,0,563.6,348.1);
	this.Layer_5.depth = 0;
	this.Layer_5.isAttachedToCamera = 0
	this.Layer_5.isAttachedToMask = 0
	this.Layer_5.layerDepth = 0
	this.Layer_5.layerIndex = 1
	this.Layer_5.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_5).wait(1));

	// Layer_3_obj_
	this.Layer_3 = new lib.Scene_1_Layer_3();
	this.Layer_3.name = "Layer_3";
	this.Layer_3.parent = this;
	this.Layer_3.setTransform(768,318.5,1,1,0,0,0,768,318.5);
	this.Layer_3.depth = 0;
	this.Layer_3.isAttachedToCamera = 0
	this.Layer_3.isAttachedToMask = 0
	this.Layer_3.layerDepth = 0
	this.Layer_3.layerIndex = 2
	this.Layer_3.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_3).wait(1));

	// Layer_10_obj_
	this.Layer_10 = new lib.Scene_1_Layer_10();
	this.Layer_10.name = "Layer_10";
	this.Layer_10.parent = this;
	this.Layer_10.setTransform(708.6,337.1,1,1,0,0,0,708.6,337.1);
	this.Layer_10.depth = 0;
	this.Layer_10.isAttachedToCamera = 0
	this.Layer_10.isAttachedToMask = 0
	this.Layer_10.layerDepth = 0
	this.Layer_10.layerIndex = 3
	this.Layer_10.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_10).wait(1));

	// Layer_2_obj_
	this.Layer_2 = new lib.Scene_1_Layer_2();
	this.Layer_2.name = "Layer_2";
	this.Layer_2.parent = this;
	this.Layer_2.setTransform(540,340,1,1,0,0,0,540,340);
	this.Layer_2.depth = 0;
	this.Layer_2.isAttachedToCamera = 0
	this.Layer_2.isAttachedToMask = 0
	this.Layer_2.layerDepth = 0
	this.Layer_2.layerIndex = 4
	this.Layer_2.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_2).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(540,340,540,340);
// library properties:
lib.properties = {
	id: 'C2736FE4F936C04B8982C61D9ABAEC19',
	width: 1080,
	height: 680,
	fps: 30,
	color: "#00366B",
	opacity: 1.00,
	manifest: [
		{src:"assets/images/beard34.png", id:"beard"},
		{src:"assets/images/pack34.jpg", id:"pack"}
	],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['C2736FE4F936C04B8982C61D9ABAEC19'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}


// Layer depth API : 

AdobeAn.Layer = new function() {
	this.getLayerZDepth = function(timeline, layerName)
	{
		if(layerName === "Camera")
		layerName = "___camera___instance";
		var script = "if(timeline." + layerName + ") timeline." + layerName + ".depth; else 0;";
		return eval(script);
	}
	this.setLayerZDepth = function(timeline, layerName, zDepth)
	{
		const MAX_zDepth = 10000;
		const MIN_zDepth = -5000;
		if(zDepth > MAX_zDepth)
			zDepth = MAX_zDepth;
		else if(zDepth < MIN_zDepth)
			zDepth = MIN_zDepth;
		if(layerName === "Camera")
		layerName = "___camera___instance";
		var script = "if(timeline." + layerName + ") timeline." + layerName + ".depth = " + zDepth + ";";
		eval(script);
	}
	this.removeLayer = function(timeline, layerName)
	{
		if(layerName === "Camera")
		layerName = "___camera___instance";
		var script = "if(timeline." + layerName + ") timeline.removeChild(timeline." + layerName + ");";
		eval(script);
	}
	this.addNewLayer = function(timeline, layerName, zDepth)
	{
		if(layerName === "Camera")
		layerName = "___camera___instance";
		zDepth = typeof zDepth !== 'undefined' ? zDepth : 0;
		var layer = new createjs.MovieClip();
		layer.name = layerName;
		layer.depth = zDepth;
		layer.layerIndex = 0;
		timeline.addChild(layer);
	}
}


})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;

$(function () {
    var canvas, stage, exportRoot, anim_container, dom_overlay_container, fnStartAnimation;
    function init() {
        canvas = document.getElementById("canvas34");
        anim_container = document.getElementById("animation_container34");
        dom_overlay_container = document.getElementById("dom_overlay_container34");
        var comp=AdobeAn.getComposition("C2736FE4F936C04B8982C61D9ABAEC19");
        var lib=comp.getLibrary();
        createjs.MotionGuidePlugin.install();
        var loader = new createjs.LoadQueue(false);
        loader.addEventListener("fileload", function(evt){handleFileLoad(evt,comp)});
        loader.addEventListener("complete", function(evt){handleComplete(evt,comp)});
        var lib=comp.getLibrary();
        loader.loadManifest(lib.properties.manifest);
    }
    function handleFileLoad(evt, comp) {
        var images=comp.getImages();
        if (evt && (evt.item.type == "image")) { images[evt.item.id] = evt.result; }
    }
    function handleComplete(evt,comp) {
        //This function is always called, irrespective of the content. You can use the variable "stage" after it is created in token create_stage.
        var lib=comp.getLibrary();
        var ss=comp.getSpriteSheet();
        var queue = evt.target;
        var ssMetadata = lib.ssMetadata;
        for(i=0; i<ssMetadata.length; i++) {
            ss[ssMetadata[i].name] = new createjs.SpriteSheet( {"images": [queue.getResult(ssMetadata[i].name)], "frames": ssMetadata[i].frames} )
        }
        var preloaderDiv = document.getElementById("_preload_div_34");
        preloaderDiv.style.display = 'none';
        canvas.style.display = 'block';
        exportRoot = new lib.Dec();
        stage = new lib.Stage(canvas);
        //Registers the "tick" event listener.
        fnStartAnimation = function() {
            stage.addChild(exportRoot);
            createjs.Ticker.setFPS(lib.properties.fps);
            createjs.Ticker.addEventListener("tick", stage)
            stage.addEventListener("tick", handleTick)
            function getProjectionMatrix(container, totalDepth) {
                var focalLength = 528.25;
                var projectionCenter = { x : lib.properties.width/2, y : lib.properties.height/2 };
                var scale = (totalDepth + focalLength)/focalLength;
                var scaleMat = new createjs.Matrix2D;
                scaleMat.a = 1/scale;
                scaleMat.d = 1/scale;
                var projMat = new createjs.Matrix2D;
                projMat.tx = -projectionCenter.x;
                projMat.ty = -projectionCenter.y;
                projMat = projMat.prependMatrix(scaleMat);
                projMat.tx += projectionCenter.x;
                projMat.ty += projectionCenter.y;
                return projMat;
            }
            function handleTick(event) {
                var cameraInstance = exportRoot.___camera___instance;
                if(cameraInstance !== undefined && cameraInstance.pinToObject !== undefined)
                {
                    cameraInstance.x = cameraInstance.pinToObject.x + cameraInstance.pinToObject.pinOffsetX;
                    cameraInstance.y = cameraInstance.pinToObject.y + cameraInstance.pinToObject.pinOffsetY;
                    if(cameraInstance.pinToObject.parent !== undefined && cameraInstance.pinToObject.parent.depth !== undefined)
                        cameraInstance.depth = cameraInstance.pinToObject.parent.depth + cameraInstance.pinToObject.pinOffsetZ;
                }
                applyLayerZDepth(exportRoot);
            }
            function applyLayerZDepth(parent)
            {
                var cameraInstance = parent.___camera___instance;
                var focalLength = 528.25;
                var projectionCenter = { 'x' : 0, 'y' : 0};
                if(parent === exportRoot)
                {
                    var stageCenter = { 'x' : lib.properties.width/2, 'y' : lib.properties.height/2 };
                    projectionCenter.x = stageCenter.x;
                    projectionCenter.y = stageCenter.y;
                }
                for(child in parent.children)
                {
                    var layerObj = parent.children[child];
                    if(layerObj == cameraInstance)
                        continue;
                    applyLayerZDepth(layerObj, cameraInstance);
                    if(layerObj.layerDepth === undefined)
                        continue;
                    if(layerObj.currentFrame != layerObj.parent.currentFrame)
                    {
                        layerObj.gotoAndPlay(layerObj.parent.currentFrame);
                    }
                    var matToApply = new createjs.Matrix2D;
                    var cameraMat = new createjs.Matrix2D;
                    var totalDepth = layerObj.layerDepth ? layerObj.layerDepth : 0;
                    var cameraDepth = 0;
                    if(cameraInstance && !layerObj.isAttachedToCamera)
                    {
                        var mat = cameraInstance.getMatrix();
                        mat.tx -= projectionCenter.x;
                        mat.ty -= projectionCenter.y;
                        cameraMat = mat.invert();
                        cameraMat.prependTransform(projectionCenter.x, projectionCenter.y, 1, 1, 0, 0, 0, 0, 0);
                        cameraMat.appendTransform(-projectionCenter.x, -projectionCenter.y, 1, 1, 0, 0, 0, 0, 0);
                        if(cameraInstance.depth)
                            cameraDepth = cameraInstance.depth;
                    }
                    if(layerObj.depth)
                    {
                        totalDepth = layerObj.depth;
                    }
                    //Offset by camera depth
                    totalDepth -= cameraDepth;
                    if(totalDepth < -focalLength)
                    {
                        matToApply.a = 0;
                        matToApply.d = 0;
                    }
                    else
                    {
                        if(layerObj.layerDepth)
                        {
                            var sizeLockedMat = getProjectionMatrix(parent, layerObj.layerDepth);
                            if(sizeLockedMat)
                            {
                                sizeLockedMat.invert();
                                matToApply.prependMatrix(sizeLockedMat);
                            }
                        }
                        matToApply.prependMatrix(cameraMat);
                        var projMat = getProjectionMatrix(parent, totalDepth);
                        if(projMat)
                        {
                            matToApply.prependMatrix(projMat);
                        }
                    }
                    layerObj.transformMatrix = matToApply;
                }
            }
        }
        //Code to support hidpi screens and responsive scaling.
        function makeResponsive(isResp, respDim, isScale, scaleType) {
            var lastW, lastH, lastS=1;
            window.addEventListener('resize', resizeCanvas);
            resizeCanvas();
            function resizeCanvas() {
                var w = lib.properties.width, h = lib.properties.height;
                var iw = window.innerWidth, ih=window.innerHeight;
                var pRatio = window.devicePixelRatio || 1, xRatio=iw/w, yRatio=ih/h, sRatio=1;
                if(isResp) {
                    if((respDim=='width'&&lastW==iw) || (respDim=='height'&&lastH==ih)) {
                        sRatio = lastS;
                    }
                    else if(!isScale) {
                        if(iw<w || ih<h)
                            sRatio = Math.min(xRatio, yRatio);
                    }
                    else if(scaleType==1) {
                        sRatio = Math.min(xRatio, yRatio);
                    }
                    else if(scaleType==2) {
                        sRatio = Math.max(xRatio, yRatio);
                    }
                }
                canvas.width = w*pRatio*sRatio;
                canvas.height = h*pRatio*sRatio;
                canvas.style.width = anim_container.style.width = dom_overlay_container.style.width = preloaderDiv.style.width = w*sRatio+'px';
                canvas.style.height = anim_container.style.height = dom_overlay_container.style.height = preloaderDiv.style.height = h*sRatio+'px';
                stage.scaleX = pRatio*sRatio;
                stage.scaleY = pRatio*sRatio;
                lastW = iw; lastH = ih; lastS = sRatio;
                stage.tickOnUpdate = false;
                stage.update();
                stage.tickOnUpdate = true;
            }
        }
        makeResponsive(true,'both',true,2);
        AdobeAn.compositionLoaded(lib.properties.id);
        fnStartAnimation();
    }
    init();
});