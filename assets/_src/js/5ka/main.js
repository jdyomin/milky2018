function isiOS() {
  return [
    'iPad Simulator',
    'iPhone Simulator',
    'iPod Simulator',
    'iPad',
    'iPhone',
    'iPod'
  ].includes(navigator.platform)
  || (navigator.userAgent.includes("Mac") && "ontouchend" in document)
}

$(document).ready(function(){
  if ($('.js-select').length) {
    $('.js-select').select2({
      width: 'style',
      minimumResultsForSearch: -1
    });
  }
  if (isiOS()) {
    $('html').addClass('ios');
  }
  Inputmask().mask('input[name="birthday"]');
  $('input[name="phone"]').inputmask({"mask": "+7 (999) 999-99-99"});

  $('.js-menu-trigger').on('click', function() {
      $('.page-header').toggleClass('_full');
      if ($('body').hasClass('overflow')) {
          $('body').removeClass('overflow');

          if ($(window).width() < 768) {
              $('.logo').show();
          }
      } else {
          $('body').addClass('overflow');

          if ($(window).width() < 768) {
              $('.logo').hide();
          }
      }
  });

  if ($(window).width() < 1025) {
      $('.js-subnav-trigger').on('click', function() {
          $(this).closest('.main-navigation__list-item').siblings().removeClass('expanded');
          $(this).closest('.main-navigation__list-item').toggleClass('expanded');
      })
  };

  if ($('.js-rus-carousel').length) {
    var SwiperCarousel = new Swiper('.js-rus-carousel', {
      slidesPerView: 1,
      pagination: {
        el: '.js-rus-carousel .swiper-pagination',
        clickable: true
      }
    });
  }
});
