function setActiveClass() {
   $('.tabs').each(function() {
     let active = $(this).find('.tab-ctrl.active').length ? $(this).find('.tab-ctrl.active') : $(this).find('.tab-ctrl').eq(0);
     active.addClass('active');
     $(active.data('tab')).fadeIn(500).addClass('active');
   })
 }

$(document).ready(function(){
  if ($('.tabs').length) {
    setActiveClass();
  }

  if ($('.custom-scroll').length) {
    $(".custom-scroll").mCustomScrollbar({
      theme: 'minimal'
    });
  }

  $(document).on('click', '.tab-ctrl', function(){
    if (!$(this).hasClass('active')) {
      $(this).siblings('.tab-ctrl').removeClass('active');
      $(this).closest('.tabs').find('.tab.active').fadeOut(50).removeClass('active');
      $(this).addClass('active');
      $($(this).data('tab')).fadeIn(500).addClass('active');
    }
  });

  $(document).on('click', '.btn-tab-ctrl', function() {
    let active = $(this).data('tab');
    $(this).closest('.tabs').find('.tab-ctrl').removeClass('active');
    $(this).closest('.tabs').find('.tab.active').fadeOut(50).removeClass('active');
    $(this).closest('.tabs').find(`.tab-ctrl[data-tab="${active}"]`).addClass('active');
    $(active).fadeIn(500).addClass('active');
  });
});
