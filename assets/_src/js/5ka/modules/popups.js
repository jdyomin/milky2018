function openPopup(popupID) {
  closeAllPopups();
  if (popupID) {
    let popup = $(popupID);
    let win = popup.find('.popup__window');
    popup.fadeIn(500);
    if ($(window).width() < $(window).height()) {
      $('html, body').css('overflow', 'hidden');
    }
    $('html').addClass('popup-opened');
    win.fadeIn(500);
    popup.trigger('popupopened');
    $(window).trigger('popupopened');

    if (popup.attr('id') == 'crop-popup') {
      $(window).trigger('crop-popupopened');
    }
  } else {
    console.error('Which popup?');
  }
}

function closePopup(popupID) {
  if (popupID) {
    let popup = $(popupID);
    let win = popup.find('.popup__window');
    win.fadeOut(500);
    popup.fadeOut(500);
    $('html').removeClass('popup-opened');
    if ($(window).width() < $(window).height()) {
      $('html, body').css('overflow', 'visible');
    }
    popup.trigger('popupclosed')
    $(window).trigger('popupclosed');
  } else {
    console.error('Which popup?');
  }
  if (popupID == '#qr-popup') {
    $(window).trigger('scan-stop');
  }
}

function closeAllPopups() {
  let popup = $('.popup');
  let win = popup.find('.popup__window');
  win.fadeOut(200);
  popup.fadeOut(200);
  $('html').removeClass('popup-opened');
  if ($(window).width() < $(window).height()) {
    $('html, body').css('overflow', 'visible');
  }
  popup.trigger('popupclosed');
  $(window).trigger('popupclosed');
  var event = new Event('scan-stop');
  document.dispatchEvent(event);
}

$(document).ready(function(){
  $(document).on('click', '.btn-close-popup', function(){
    let popup = !!$(this).data('target') ? $($(this).data('target')) : $(this).closest('.popup').attr('id');
    closePopup(`#${popup}`);
  });

  $(document).on('click', '.popup', function() {
    if (!$(this).hasClass('unclosed')) {
      closePopup(`#${$(this).attr('id')}`);
    }
  });

  $(document).on('click', '.popup__window, .popup .mCSB_scrollTools', function(e) {
    e.stopPropagation();
  });

  $(document).on('click', '.btn-popup', function(e) {
    let target = $(this).data('target') === 'self' ? $(this).parent() : $($(this).data('target'));
    e.preventDefault();
    openPopup(target);
  });

  // apm-widjet
  // https://qr.apmcheck.ru/readme
  if (!window.sys) {
    window.sys = {};
    window.sys.userUuid = "test@mail.ru";
  }
  var widgetParams = {
    api: 'https://api.apmcheck.ru',
    apiKey: '08fa945b-b911-45a6-babe-26e54f57b4cc',
    userUuid: window.sys.userUuid,
    i18n: {
      labels: {
        mainButton: 'Сканировать QR-код чека',
        manualQrButton: 'Ввести данные вручную',
        uploadPhotosButton: 'Загрузить фото чека',
        submitManualQrButton: 'Отправить',
        submitPhotosButton: 'Отправить',
        addPhotoButton: '<svg class="icon"><use xlink:href="#upload"/></svg> <span class="text">Загрузить фото</span>'
      },
      screens: {
        cameraNotAvailable: {
          header: "Загрузка чека",
          subheader: 'Мы не&nbsp;можем получить доступ к&nbsp;камере&nbsp;устройства.<br>Разрешите браузеру обращаться к&nbsp;камере или введите данные с&nbsp;чека вручную'
        }
      }
    }
  };

  if ( $('#apm-widget').length ) {
    qrWidget.init('apm-widget', widgetParams);
    $(document).on('click', '.js-scanner-btn', function (ev) {
      ev.preventDefault();
      var e = document.createEvent('HTMLEvents');
      e.initEvent('click', true, true);
      $('#apm-scan-qr')[0].dispatchEvent(e);
    });
    $(document).on('click', '.apm-btn-link ', function (ev) {
      ev.preventDefault();
    });
  }
});
