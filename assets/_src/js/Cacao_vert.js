(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [];


// symbols:



(lib.bg_pack_vert = function() {
	this.initialize(img.bg_pack_vert);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,517,371);// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.Symbol10 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 4
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(12,1,1).p("AB0BlQARiBhQgxQhkgnhHAb");
	this.shape.setTransform(18.4,107.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(12,1,1).p("Ah8hQQBEgYBjAjQBUAqgCB2");
	this.shape_1.setTransform(14.5,106.9);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(12,1,1).p("AiDhIQBCgVBjAfQBWAkAMBs");
	this.shape_2.setTransform(11.2,106.6);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(12,1,1).p("AiJhBQA/gTBjAcQBZAfAYBj");
	this.shape_3.setTransform(8.3,106.4);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(12,1,1).p("AiOg7QA9gQBiAYQBcAbAiBb");
	this.shape_4.setTransform(5.8,106.2);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(12,1,1).p("AiSg3QA7gNBiAVQBeAXAqBV");
	this.shape_5.setTransform(3.7,106);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(12,1,1).p("AiWgyQA6gNBiATQBfAUAyBQ");
	this.shape_6.setTransform(1.9,105.9);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(12,1,1).p("AiYgvQA4gLBiARQBgASA3BL");
	this.shape_7.setTransform(0.5,105.8);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(12,1,1).p("AiagtQA3gKBjAQQBgAQA7BJ");
	this.shape_8.setTransform(-0.4,105.7);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(12,1,1).p("AibgsQA3gJBiAQQBhAOA9BH");
	this.shape_9.setTransform(-1,105.6);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(12,1,1).p("ACdAvQg+hGhigOQhigPg3AJ");
	this.shape_10.setTransform(-1.2,105.6);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(12,1,1).p("AiTgnQAtgWBiAQQBgAQA4BS");
	this.shape_11.setTransform(-0.3,105.1);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(12,1,1).p("AiKgjQAigiBhASQBgARAyBd");
	this.shape_12.setTransform(0.6,104.5);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(12,1,1).p("AiBgeQAXgvBhATQBfATAsBo");
	this.shape_13.setTransform(1.5,103.9);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(12,1,1).p("Ah4gaQANg8BgAVQBeAUAmB0");
	this.shape_14.setTransform(2.4,103.3);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(12,1,1).p("ABvBNQgfh/hegVQhegWgCBI");
	this.shape_15.setTransform(3.3,102.6);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(12,1,1).p("AhpAAQgGhPBdAQQBcARAhBw");
	this.shape_16.setTransform(3.8,103.7);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(12,1,1).p("AhkAVQgNhWBbAMQBaALAiBi");
	this.shape_17.setTransform(4.2,104.8);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#000000").ss(12,1,1).p("AhdAqQgWhcBaAGQBZAGAjBT");
	this.shape_18.setTransform(4.6,105.8);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#000000").ss(12,1,1).p("AhXAyQgdhjBYABQBXAAAkBF");
	this.shape_19.setTransform(4.9,108.2);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#000000").ss(12,1,1).p("AhQA4QglhqBXgFQBVgFAlA4");
	this.shape_20.setTransform(5.2,110.8);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#000000").ss(12,1,1).p("AhJA/QgthxBVgKQBUgKAmAp");
	this.shape_21.setTransform(5.5,113.3);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#000000").ss(12,1,1).p("ABXg1QgngbhTAQQhTAPA0B4");
	this.shape_22.setTransform(5.8,115.7);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#000000").ss(12,1,1).p("AhMA8QgphuBWgIQBUgIAmAv");
	this.shape_23.setTransform(5.4,112.3);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#000000").ss(12,1,1).p("AhWAzQgehlBYAAQBWgBAlBD");
	this.shape_24.setTransform(5,108.8);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f().s("#000000").ss(12,1,1).p("AhfAmQgUhbBbAHQBZAHAiBW");
	this.shape_25.setTransform(4.5,105.6);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f().s("#000000").ss(12,1,1).p("AhnAIQgJhRBcAOQBcAOAhBr");
	this.shape_26.setTransform(4,104.1);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f().s("#000000").ss(12,1,1).p("AiRg3QA7gOBiAWQBdAXApBW");
	this.shape_27.setTransform(3.9,106);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f().s("#000000").ss(12,1,1).p("AiBhKQBCgVBiAfQBWAmAJBu");
	this.shape_28.setTransform(11.9,106.7);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f().s("#000000").ss(12,1,1).p("Ah8hQQBFgYBjAjQBTAqgCB2");
	this.shape_29.setTransform(14.6,106.9);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f().s("#000000").ss(12,1,1).p("Ah4hVQBGgaBjAmQBSAtgLB9");
	this.shape_30.setTransform(16.7,107.1);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f().s("#000000").ss(12,1,1).p("Ah3hYQBIgbBjAnQBQAwgPCA");
	this.shape_31.setTransform(17.9,107.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},9).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_27}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_28}]},1).to({state:[{t:this.shape_29}]},1).to({state:[{t:this.shape_30}]},1).to({state:[{t:this.shape_31}]},1).to({state:[{t:this.shape}]},1).wait(1));

	// Layer 3
	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f().s("#000000").ss(12,1,1).p("ABgAdQhNg0hPgLQgiAVgBAw");
	this.shape_32.setTransform(19.8,113.2);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f().s("#000000").ss(12,1,1).p("AhqAcQALgmArgRQBOAIBQAp");
	this.shape_33.setTransform(15.8,113.2);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f().s("#000000").ss(12,1,1).p("AhzAVQAUgbAzgOQBOAFBSAe");
	this.shape_34.setTransform(12.2,113.2);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f().s("#000000").ss(12,1,1).p("Ah8APQAcgSA7gLQBNACBVAV");
	this.shape_35.setTransform(9,113.2);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f().s("#000000").ss(12,1,1).p("AiEAKQAkgLBAgIQBNgBBYAO");
	this.shape_36.setTransform(6.3,113.2);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f().s("#000000").ss(12,1,1).p("AiKAGQApgFBGgFQBMgDBaAI");
	this.shape_37.setTransform(4,113.1);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f().s("#000000").ss(12,1,1).p("AiPAEQAuAABKgEQBMgEBbAD");
	this.shape_38.setTransform(2,113);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f().s("#000000").ss(12,1,1).p("AiTACQAyAEBNgDQBMgFBcgB");
	this.shape_39.setTransform(0.6,112.9);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f().s("#000000").ss(12,1,1).p("AiWAAQA0AHBQgCQBNgGBcgE");
	this.shape_40.setTransform(-0.5,112.9);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f().s("#000000").ss(12,1,1).p("AiYAAQA2AIBSgCQBMgGBdgG");
	this.shape_41.setTransform(-1.1,112.9);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f().s("#000000").ss(12,1,1).p("ACagGQheAGhMAHQhRABg4gJ");
	this.shape_42.setTransform(-1.3,112.9);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f().s("#000000").ss(12,1,1).p("AiMANQAegMBSgBQBMgGBdgH");
	this.shape_43.setTransform(-0.1,113.6);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f().s("#000000").ss(12,1,1).p("AiAAYQAGggBRgBQBMgIBegG");
	this.shape_44.setTransform(1.1,114.6);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f().s("#000000").ss(12,1,1).p("AhyAjQgTg2BSgBQBMgHBdgH");
	this.shape_45.setTransform(2.2,115.7);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.f().s("#000000").ss(12,1,1).p("AhfAtQgshKBRgCQBMgHBegG");
	this.shape_46.setTransform(2.8,116.8);

	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.f().s("#000000").ss(12,1,1).p("ABrg3QhdAGhMAIQhRABBEBg");
	this.shape_47.setTransform(3.3,117.8);

	this.shape_48 = new cjs.Shape();
	this.shape_48.graphics.f().s("#000000").ss(12,1,1).p("AhQA1QhDheBbgIQBIADBdgG");
	this.shape_48.setTransform(3.1,117.5);

	this.shape_49 = new cjs.Shape();
	this.shape_49.graphics.f().s("#000000").ss(12,1,1).p("AhVA2QhBhbBkgQQBEAPBegG");
	this.shape_49.setTransform(2.8,116.7);

	this.shape_50 = new cjs.Shape();
	this.shape_50.graphics.f().s("#000000").ss(12,1,1).p("AhaA5Qg/hZBtgYQBAAbBegG");
	this.shape_50.setTransform(2.6,115.8);

	this.shape_51 = new cjs.Shape();
	this.shape_51.graphics.f().s("#000000").ss(12,1,1).p("AhfA8Qg8hXB2ggQA8AnBdgG");
	this.shape_51.setTransform(2.4,114.9);

	this.shape_52 = new cjs.Shape();
	this.shape_52.graphics.f().s("#000000").ss(12,1,1).p("AhjA+Qg7hUCAgnQA3AyBegG");
	this.shape_52.setTransform(2.1,114);

	this.shape_53 = new cjs.Shape();
	this.shape_53.graphics.f().s("#000000").ss(12,1,1).p("AhoBBQg5hSCJgvQA0A+BdgG");
	this.shape_53.setTransform(1.9,113.1);

	this.shape_54 = new cjs.Shape();
	this.shape_54.graphics.f().s("#000000").ss(12,1,1).p("AB8AAQhdAGgwhIQiSA2A3BP");
	this.shape_54.setTransform(1.6,112.2);

	this.shape_55 = new cjs.Shape();
	this.shape_55.graphics.f().s("#000000").ss(12,1,1).p("AhmBAQg6hTCGgsQA1A6BdgH");
	this.shape_55.setTransform(2,113.5);

	this.shape_56 = new cjs.Shape();
	this.shape_56.graphics.f().s("#000000").ss(12,1,1).p("AhgA8Qg8hXB4ggQA7ApBegG");
	this.shape_56.setTransform(2.3,114.8);

	this.shape_57 = new cjs.Shape();
	this.shape_57.graphics.f().s("#000000").ss(12,1,1).p("AhZA5Qg/haBrgXQBBAZBdgG");
	this.shape_57.setTransform(2.7,116);

	this.shape_58 = new cjs.Shape();
	this.shape_58.graphics.f().s("#000000").ss(12,1,1).p("AhSA1QhChdBegMQBHAJBdgH");
	this.shape_58.setTransform(3,117.3);

	this.shape_59 = new cjs.Shape();
	this.shape_59.graphics.f().s("#000000").ss(12,1,1).p("AiJAHQAogGBFgGQBNgCBZAI");
	this.shape_59.setTransform(4.3,113.1);

	this.shape_60 = new cjs.Shape();
	this.shape_60.graphics.f().s("#000000").ss(12,1,1).p("AhyAWQATgdAxgOQBOAFBTAh");
	this.shape_60.setTransform(12.9,113.2);

	this.shape_61 = new cjs.Shape();
	this.shape_61.graphics.f().s("#000000").ss(12,1,1).p("AhpAcQALgmAqgRQBOAIBQAp");
	this.shape_61.setTransform(15.9,113.2);

	this.shape_62 = new cjs.Shape();
	this.shape_62.graphics.f().s("#000000").ss(12,1,1).p("AhjAgQAFgsAmgTQBOAKBOAv");
	this.shape_62.setTransform(18.1,113.2);

	this.shape_63 = new cjs.Shape();
	this.shape_63.graphics.f().s("#000000").ss(12,1,1).p("AhgAiQACgvAjgUQBOALBOAy");
	this.shape_63.setTransform(19.4,113.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_32}]}).to({state:[{t:this.shape_32}]},9).to({state:[{t:this.shape_33}]},1).to({state:[{t:this.shape_34}]},1).to({state:[{t:this.shape_35}]},1).to({state:[{t:this.shape_36}]},1).to({state:[{t:this.shape_37}]},1).to({state:[{t:this.shape_38}]},1).to({state:[{t:this.shape_39}]},1).to({state:[{t:this.shape_40}]},1).to({state:[{t:this.shape_41}]},1).to({state:[{t:this.shape_42}]},1).to({state:[{t:this.shape_43}]},1).to({state:[{t:this.shape_44}]},1).to({state:[{t:this.shape_45}]},1).to({state:[{t:this.shape_46}]},1).to({state:[{t:this.shape_47}]},1).to({state:[{t:this.shape_48}]},1).to({state:[{t:this.shape_49}]},1).to({state:[{t:this.shape_50}]},1).to({state:[{t:this.shape_51}]},1).to({state:[{t:this.shape_52}]},1).to({state:[{t:this.shape_53}]},1).to({state:[{t:this.shape_54}]},1).to({state:[{t:this.shape_55}]},1).to({state:[{t:this.shape_56}]},1).to({state:[{t:this.shape_57}]},1).to({state:[{t:this.shape_58}]},1).to({state:[{t:this.shape_47}]},1).to({state:[{t:this.shape_46}]},1).to({state:[{t:this.shape_45}]},1).to({state:[{t:this.shape_44}]},1).to({state:[{t:this.shape_43}]},1).to({state:[{t:this.shape_42}]},1).to({state:[{t:this.shape_59}]},1).to({state:[{t:this.shape_35}]},1).to({state:[{t:this.shape_60}]},1).to({state:[{t:this.shape_61}]},1).to({state:[{t:this.shape_62}]},1).to({state:[{t:this.shape_63}]},1).to({state:[{t:this.shape_32}]},1).wait(1));

	// Layer 2
	this.shape_64 = new cjs.Shape();
	this.shape_64.graphics.f().s("#000000").ss(12,1,1).p("AG9pTQBODrgND0QgOD+htCzQh4DGjVA7QjuBClHiD");
	this.shape_64.setTransform(80.5,65.2);

	this.shape_65 = new cjs.Shape();
	this.shape_65.graphics.f().s("#000000").ss(12,1,1).p("AoCIFQE5BnDgg/QDLg5CBi2QB3inAfj7QAfjyhDju");
	this.shape_65.setTransform(78.1,63.7);

	this.shape_66 = new cjs.Shape();
	this.shape_66.graphics.f().s("#000000").ss(12,1,1).p("AoHILQEtBODTg7QDCg3CIipQCBicAvj3QAvjyg5jw");
	this.shape_66.setTransform(76.1,62.4);

	this.shape_67 = new cjs.Shape();
	this.shape_67.graphics.f().s("#000000").ss(12,1,1).p("AoNIRQEiA4DIg5QC7g1COidQCJiTA9j0QA+jwgxjz");
	this.shape_67.setTransform(74.5,61.3);

	this.shape_68 = new cjs.Shape();
	this.shape_68.graphics.f().s("#000000").ss(12,1,1).p("AoUIVQEaAlC+g2QC0g0CUiSQCPiMBKjyQBKjugpj1");
	this.shape_68.setTransform(73.2,60.4);

	this.shape_69 = new cjs.Shape();
	this.shape_69.graphics.f().s("#000000").ss(12,1,1).p("AoaIYQESAUC2g0QCugzCZiJQCViFBVjvQBUjugjj2");
	this.shape_69.setTransform(72.2,59.8);

	this.shape_70 = new cjs.Shape();
	this.shape_70.graphics.f().s("#000000").ss(12,1,1).p("AoeIYQELAHCvgyQCqgyCciCQCbh/BdjuQBcjtgej3");
	this.shape_70.setTransform(71.4,59.4);

	this.shape_71 = new cjs.Shape();
	this.shape_71.graphics.f().s("#000000").ss(12,1,1).p("AojIYQEGgECqgxQCngwCfh9QCeh8BkjsQBjjtgaj4");
	this.shape_71.setTransform(70.8,59.2);

	this.shape_72 = new cjs.Shape();
	this.shape_72.graphics.f().s("#000000").ss(12,1,1).p("AomIXQECgLCngwQCjgxCih4QChh4BojsQBpjsgYj5");
	this.shape_72.setTransform(70.4,59.1);

	this.shape_73 = new cjs.Shape();
	this.shape_73.graphics.f().s("#000000").ss(12,1,1).p("AooIWQEAgPCkgvQCjgwCjh3QCih2BrjrQBrjsgVj5");
	this.shape_73.setTransform(70.1,59.1);

	this.shape_74 = new cjs.Shape();
	this.shape_74.graphics.f().s("#000000").ss(12,1,1).p("AImoVQAVD6hsDrQhsDrijB2QikB1iiAwQijAvj/AR");
	this.shape_74.setTransform(70,59);

	this.shape_75 = new cjs.Shape();
	this.shape_75.graphics.f().s("#000000").ss(12,1,1).p("AoYIXQESAXC3g0QCvgzCYiLQCViGBTjvQBTjvgkj1");
	this.shape_75.setTransform(72.3,59.9);

	this.shape_76 = new cjs.Shape();
	this.shape_76.graphics.f().s("#000000").ss(12,1,1).p("AoNIRQEiA4DIg5QC6g1CPidQCIiTA+j0QA+jwgxjz");
	this.shape_76.setTransform(74.5,61.3);

	this.shape_77 = new cjs.Shape();
	this.shape_77.graphics.f().s("#000000").ss(12,1,1).p("AoGIKQEwBTDWg8QDDg3CHisQB+ieAtj4QAsjyg8jv");
	this.shape_77.setTransform(76.5,62.7);

	this.shape_78 = new cjs.Shape();
	this.shape_78.graphics.f().s("#000000").ss(12,1,1).p("AoCIEQE6BoDgg+QDMg5CAi3QB3ioAfj6QAejzhDjt");
	this.shape_78.setTransform(78.1,63.8);

	this.shape_79 = new cjs.Shape();
	this.shape_79.graphics.f().s("#000000").ss(12,1,1).p("AoAIAQFBB3DohAQDRg6B8i/QBxiuAVj9QAVjzhJjs");
	this.shape_79.setTransform(79.4,64.6);

	this.shape_80 = new cjs.Shape();
	this.shape_80.graphics.f().s("#000000").ss(12,1,1).p("An/H9QFGCADshBQDUg7B5jEQBuiyAQj9QAPj0hNjr");
	this.shape_80.setTransform(80.2,65.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_64}]}).to({state:[{t:this.shape_64}]},9).to({state:[{t:this.shape_65}]},1).to({state:[{t:this.shape_66}]},1).to({state:[{t:this.shape_67}]},1).to({state:[{t:this.shape_68}]},1).to({state:[{t:this.shape_69}]},1).to({state:[{t:this.shape_70}]},1).to({state:[{t:this.shape_71}]},1).to({state:[{t:this.shape_72}]},1).to({state:[{t:this.shape_73}]},1).to({state:[{t:this.shape_74}]},1).to({state:[{t:this.shape_74}]},22).to({state:[{t:this.shape_75}]},1).to({state:[{t:this.shape_76}]},1).to({state:[{t:this.shape_77}]},1).to({state:[{t:this.shape_78}]},1).to({state:[{t:this.shape_79}]},1).to({state:[{t:this.shape_80}]},1).to({state:[{t:this.shape_64}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,-0.3,138,131.4);


(lib.Symbol9 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 6
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(9.5,1,1).p("AiUgpQB2AWCzA9");
	this.shape.setTransform(78.2,19.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(9.5,1,1).p("AiWghQB3AQC1Az");
	this.shape_1.setTransform(78.7,18.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(9.5,1,1).p("AiWgbQB3ALC2As");
	this.shape_2.setTransform(79.2,18.1);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(9.5,1,1).p("AiYgUQB4AGC5Ak");
	this.shape_3.setTransform(79.6,17.7);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(9.5,1,1).p("AiYgPQB4ABC5Ae");
	this.shape_4.setTransform(80,17.3);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(9.5,1,1).p("AiZgLQB4gBC7AY");
	this.shape_5.setTransform(80.3,16.9);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(9.5,1,1).p("AiZgHQB4gEC7AU");
	this.shape_6.setTransform(80.5,16.6);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(9.5,1,1).p("AiagEQB5gGC8AQ");
	this.shape_7.setTransform(80.7,16.4);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(9.5,1,1).p("AiagBQB5gIC8AO");
	this.shape_8.setTransform(80.9,16.2);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(9.5,1,1).p("AiaAAQB4gJC9AN");
	this.shape_9.setTransform(81,16);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(9.5,1,1).p("AiaAAQB4gIC9AM");
	this.shape_10.setTransform(81,16);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(9.5,1,1).p("AiZgJQB4gDC7AW");
	this.shape_11.setTransform(80.4,16.8);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(9.5,1,1).p("AiYgRQB4ADC5Ag");
	this.shape_12.setTransform(79.9,17.4);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(9.5,1,1).p("AiXgXQB3AIC4An");
	this.shape_13.setTransform(79.4,17.9);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(9.5,1,1).p("AiWgcQB3AMC2At");
	this.shape_14.setTransform(79.1,18.3);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(9.5,1,1).p("AiVghQB2AQC1Az");
	this.shape_15.setTransform(78.7,18.6);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(9.5,1,1).p("AiVglQB2ATC1A4");
	this.shape_16.setTransform(78.5,18.9);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(9.5,1,1).p("AiUgnQB2AVC0A6");
	this.shape_17.setTransform(78.3,19.1);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#000000").ss(9.5,1,1).p("AiUgoQB2AVCzA9");
	this.shape_18.setTransform(78.2,19.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},9).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_10}]},20).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape}]},1).wait(1));

	// Layer 3
	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#000000").ss(9.5,1,1).p("AiUBDQB0hLC1g6");
	this.shape_19.setTransform(78.2,8.3);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#000000").ss(9.5,1,1).p("AiTBFQBzhMC0g9");
	this.shape_20.setTransform(78.6,8.3);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#000000").ss(9.5,1,1).p("AiTBHQBzhOC0g/");
	this.shape_21.setTransform(79,8.3);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#000000").ss(9.5,1,1).p("AiSBJQByhQCzhB");
	this.shape_22.setTransform(79.3,8.3);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#000000").ss(9.5,1,1).p("AiRBKQBxhQCyhD");
	this.shape_23.setTransform(79.5,8.2);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#000000").ss(9.5,1,1).p("AiQBLQBwhRCxhE");
	this.shape_24.setTransform(79.8,8.2);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f().s("#000000").ss(9.5,1,1).p("AiQBMQBvhRCyhG");
	this.shape_25.setTransform(80,8.2);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f().s("#000000").ss(9.5,1,1).p("AiQBNQBwhSCxhH");
	this.shape_26.setTransform(80.1,8.2);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f().s("#000000").ss(9.5,1,1).p("AiQBNQBvhSCyhH");
	this.shape_27.setTransform(80.2,8.2);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f().s("#000000").ss(9.5,1,1).p("AiQBOQBwhTCxhI");
	this.shape_28.setTransform(80.3,8.2);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f().s("#000000").ss(9.5,1,1).p("AiPBOQBvhTCwhI");
	this.shape_29.setTransform(80.3,8.2);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f().s("#000000").ss(9.5,1,1).p("AiQBMQBwhSCxhE");
	this.shape_30.setTransform(79.8,8.2);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f().s("#000000").ss(9.5,1,1).p("AiSBIQByhPCzhA");
	this.shape_31.setTransform(79.1,8.2);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f().s("#000000").ss(9.5,1,1).p("AiTBGQBzhNC0g+");
	this.shape_32.setTransform(78.8,8.3);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f().s("#000000").ss(9.5,1,1).p("AiUBEQB0hMC1g7");
	this.shape_33.setTransform(78.4,8.3);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f().s("#000000").ss(9.5,1,1).p("AiUBDQB1hLC0g6");
	this.shape_34.setTransform(78.2,8.3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_19}]}).to({state:[{t:this.shape_19}]},9).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23,p:{x:79.5}}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_27}]},1).to({state:[{t:this.shape_28}]},1).to({state:[{t:this.shape_29}]},1).to({state:[{t:this.shape_29}]},20).to({state:[{t:this.shape_30}]},1).to({state:[{t:this.shape_23,p:{x:79.4}}]},1).to({state:[{t:this.shape_31}]},1).to({state:[{t:this.shape_32}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_33,p:{x:78.4}}]},1).to({state:[{t:this.shape_33,p:{x:78.3}}]},1).to({state:[{t:this.shape_34}]},1).to({state:[{t:this.shape_19}]},1).wait(1));

	// Layer 1
	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f().s("#000000").ss(9.5,1,1).p("AiPhJQCYAvCIBk");
	this.shape_35.setTransform(13.9,7.5);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f().s("#000000").ss(9.5,1,1).p("AiRhGQCZAsCKBh");
	this.shape_36.setTransform(13.5,7.9);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f().s("#000000").ss(9.5,1,1).p("AiShDQCaApCLBe");
	this.shape_37.setTransform(13.2,8.3);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f().s("#000000").ss(9.5,1,1).p("AiThBQCaAnCNBc");
	this.shape_38.setTransform(12.9,8.6);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f().s("#000000").ss(9.5,1,1).p("AiUg/QCbAkCOBb");
	this.shape_39.setTransform(12.7,8.9);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f().s("#000000").ss(9.5,1,1).p("AiVg9QCcAiCPBZ");
	this.shape_40.setTransform(12.5,9.2);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f().s("#000000").ss(9.5,1,1).p("AiWg8QCcAhCRBY");
	this.shape_41.setTransform(12.3,9.4);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f().s("#000000").ss(9.5,1,1).p("AiWg7QCcAgCRBW");
	this.shape_42.setTransform(12.2,9.5);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f().s("#000000").ss(9.5,1,1).p("AiXg6QCdAfCSBW");
	this.shape_43.setTransform(12.1,9.6);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f().s("#000000").ss(9.5,1,1).p("AiXg5QCcAeCTBV");
	this.shape_44.setTransform(12.1,9.7);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f().s("#000000").ss(9.5,1,1).p("AiXg5QCdAeCSBV");
	this.shape_45.setTransform(12,9.7);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.f().s("#000000").ss(9.5,1,1).p("AiVg8QCbAhCQBY");
	this.shape_46.setTransform(12.4,9.2);

	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.f().s("#000000").ss(9.5,1,1).p("AiUg/QCbAlCOBa");
	this.shape_47.setTransform(12.8,8.8);

	this.shape_48 = new cjs.Shape();
	this.shape_48.graphics.f().s("#000000").ss(9.5,1,1).p("AiThCQCbAoCMBd");
	this.shape_48.setTransform(13,8.5);

	this.shape_49 = new cjs.Shape();
	this.shape_49.graphics.f().s("#000000").ss(9.5,1,1).p("AiShEQCaAqCLBf");
	this.shape_49.setTransform(13.3,8.2);

	this.shape_50 = new cjs.Shape();
	this.shape_50.graphics.f().s("#000000").ss(9.5,1,1).p("AiRhHQCZAtCJBi");
	this.shape_50.setTransform(13.7,7.8);

	this.shape_51 = new cjs.Shape();
	this.shape_51.graphics.f().s("#000000").ss(9.5,1,1).p("AiQhIQCZAuCIBj");
	this.shape_51.setTransform(13.8,7.7);

	this.shape_52 = new cjs.Shape();
	this.shape_52.graphics.f().s("#000000").ss(9.5,1,1).p("AiQhIQCZAvCIBj");
	this.shape_52.setTransform(13.8,7.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_35}]}).to({state:[{t:this.shape_35}]},9).to({state:[{t:this.shape_36,p:{y:7.9}}]},1).to({state:[{t:this.shape_37}]},1).to({state:[{t:this.shape_38}]},1).to({state:[{t:this.shape_39}]},1).to({state:[{t:this.shape_40}]},1).to({state:[{t:this.shape_41}]},1).to({state:[{t:this.shape_42}]},1).to({state:[{t:this.shape_43}]},1).to({state:[{t:this.shape_44}]},1).to({state:[{t:this.shape_45}]},1).to({state:[{t:this.shape_45}]},20).to({state:[{t:this.shape_46}]},1).to({state:[{t:this.shape_47}]},1).to({state:[{t:this.shape_48}]},1).to({state:[{t:this.shape_49}]},1).to({state:[{t:this.shape_36,p:{y:8}}]},1).to({state:[{t:this.shape_50}]},1).to({state:[{t:this.shape_51}]},1).to({state:[{t:this.shape_52}]},1).to({state:[{t:this.shape_35}]},1).wait(1));

	// Layer 2
	this.shape_53 = new cjs.Shape();
	this.shape_53.graphics.f().s("#000000").ss(9.5,1,1).p("AiUAiQB+gsCrgX");
	this.shape_53.setTransform(13.4,18.3);

	this.shape_54 = new cjs.Shape();
	this.shape_54.graphics.f().s("#000000").ss(9.5,1,1).p("AiVAdQCAgnCrgS");
	this.shape_54.setTransform(13.1,18);

	this.shape_55 = new cjs.Shape();
	this.shape_55.graphics.f().s("#000000").ss(9.5,1,1).p("AiVAaQCAglCrgN");
	this.shape_55.setTransform(12.8,17.7);

	this.shape_56 = new cjs.Shape();
	this.shape_56.graphics.f().s("#000000").ss(9.5,1,1).p("AiWAWQCBgiCsgJ");
	this.shape_56.setTransform(12.6,17.4);

	this.shape_57 = new cjs.Shape();
	this.shape_57.graphics.f().s("#000000").ss(9.5,1,1).p("AiXATQCDgfCsgF");
	this.shape_57.setTransform(12.4,17.2);

	this.shape_58 = new cjs.Shape();
	this.shape_58.graphics.f().s("#000000").ss(9.5,1,1).p("AiXAQQCDgcCsgD");
	this.shape_58.setTransform(12.2,17);

	this.shape_59 = new cjs.Shape();
	this.shape_59.graphics.f().s("#000000").ss(9.5,1,1).p("AiXAOQCDgbCsAA");
	this.shape_59.setTransform(12.1,16.8);

	this.shape_60 = new cjs.Shape();
	this.shape_60.graphics.f().s("#000000").ss(9.5,1,1).p("AiXAMQCDgZCsAC");
	this.shape_60.setTransform(12,16.7);

	this.shape_61 = new cjs.Shape();
	this.shape_61.graphics.f().s("#000000").ss(9.5,1,1).p("AiXALQCDgYCtAD");
	this.shape_61.setTransform(11.9,16.6);

	this.shape_62 = new cjs.Shape();
	this.shape_62.graphics.f().s("#000000").ss(9.5,1,1).p("AiYALQCEgYCtAE");
	this.shape_62.setTransform(11.9,16.5);

	this.shape_63 = new cjs.Shape();
	this.shape_63.graphics.f().s("#000000").ss(9.5,1,1).p("AiYAKQCEgXCtAE");
	this.shape_63.setTransform(11.9,16.5);

	this.shape_64 = new cjs.Shape();
	this.shape_64.graphics.f().s("#000000").ss(9.5,1,1).p("AiXAPQCDgcCsgB");
	this.shape_64.setTransform(12.2,16.9);

	this.shape_65 = new cjs.Shape();
	this.shape_65.graphics.f().s("#000000").ss(9.5,1,1).p("AiWATQCCgfCrgG");
	this.shape_65.setTransform(12.5,17.2);

	this.shape_66 = new cjs.Shape();
	this.shape_66.graphics.f().s("#000000").ss(9.5,1,1).p("AiWAYQCBgkCsgK");
	this.shape_66.setTransform(12.7,17.5);

	this.shape_67 = new cjs.Shape();
	this.shape_67.graphics.f().s("#000000").ss(9.5,1,1).p("AiVAaQCAglCrgP");
	this.shape_67.setTransform(12.9,17.8);

	this.shape_68 = new cjs.Shape();
	this.shape_68.graphics.f().s("#000000").ss(9.5,1,1).p("AiVAfQB/gqCsgU");
	this.shape_68.setTransform(13.2,18.1);

	this.shape_69 = new cjs.Shape();
	this.shape_69.graphics.f().s("#000000").ss(9.5,1,1).p("AiVAhQB/grCsgW");
	this.shape_69.setTransform(13.3,18.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_53}]}).to({state:[{t:this.shape_53}]},9).to({state:[{t:this.shape_54,p:{y:18}}]},1).to({state:[{t:this.shape_55}]},1).to({state:[{t:this.shape_56}]},1).to({state:[{t:this.shape_57}]},1).to({state:[{t:this.shape_58}]},1).to({state:[{t:this.shape_59}]},1).to({state:[{t:this.shape_60}]},1).to({state:[{t:this.shape_61}]},1).to({state:[{t:this.shape_62}]},1).to({state:[{t:this.shape_63}]},1).to({state:[{t:this.shape_63}]},20).to({state:[{t:this.shape_64}]},1).to({state:[{t:this.shape_65}]},1).to({state:[{t:this.shape_66}]},1).to({state:[{t:this.shape_67}]},1).to({state:[{t:this.shape_54,p:{y:17.9}}]},1).to({state:[{t:this.shape_68}]},1).to({state:[{t:this.shape_69}]},1).to({state:[{t:this.shape_53}]},1).to({state:[{t:this.shape_53}]},1).wait(1));

	// Layer 7
	this.shape_70 = new cjs.Shape();
	this.shape_70.graphics.f().s("#000000").ss(9.5,1,1).p("AjegoQAOAYAVATQBDBBBvADQBuAABLhLQAlgmAKgc");
	this.shape_70.setTransform(44.8,43.3);

	this.shape_71 = new cjs.Shape();
	this.shape_71.graphics.f().s("#000000").ss(9.5,1,1).p("AjkgpQAUAfAVAUQBDBABvAEQBugBBLhLQAlglAQgq");
	this.shape_71.setTransform(44.8,42.7);

	this.shape_72 = new cjs.Shape();
	this.shape_72.graphics.f().s("#000000").ss(9.5,1,1).p("AjpgqQAaAmAVATQBDBBBvAEQBugBBLhLQAlgmAUg1");
	this.shape_72.setTransform(44.7,42.1);

	this.shape_73 = new cjs.Shape();
	this.shape_73.graphics.f().s("#000000").ss(9.5,1,1).p("AjugrQAfArAVAUQBDBBBvAEQBugBBLhLQAlgmAZg/");
	this.shape_73.setTransform(44.7,41.6);

	this.shape_74 = new cjs.Shape();
	this.shape_74.graphics.f().s("#000000").ss(9.5,1,1).p("AjygsQAjAwAVAVQBDBABvAEQBugBBLhLQAlglAdhI");
	this.shape_74.setTransform(44.7,41.2);

	this.shape_75 = new cjs.Shape();
	this.shape_75.graphics.f().s("#000000").ss(9.5,1,1).p("Aj1gsQAmA0AVAVQBDBABvAEQBugBBLhLQAmglAfhQ");
	this.shape_75.setTransform(44.6,40.8);

	this.shape_76 = new cjs.Shape();
	this.shape_76.graphics.f().s("#000000").ss(9.5,1,1).p("Aj4gtQApA4AVAVQBDBABvAEQBvgBBLhLQAlglAihW");
	this.shape_76.setTransform(44.6,40.5);

	this.shape_77 = new cjs.Shape();
	this.shape_77.graphics.f().s("#000000").ss(9.5,1,1).p("Aj6gtQArA7AVAUQBDBBBvADQBvAABLhMQAlglAkha");
	this.shape_77.setTransform(44.6,40.2);

	this.shape_78 = new cjs.Shape();
	this.shape_78.graphics.f().s("#000000").ss(9.5,1,1).p("Aj7gtQAtA9AVAUQBDBBBvADQBuAABLhMQAlglAlhe");
	this.shape_78.setTransform(44.6,40.1);

	this.shape_79 = new cjs.Shape();
	this.shape_79.graphics.f().s("#000000").ss(9.5,1,1).p("Aj8guQAuA+AVAVQBDBABvAEQBugBBLhLQAlglAmhg");
	this.shape_79.setTransform(44.6,40);

	this.shape_80 = new cjs.Shape();
	this.shape_80.graphics.f().s("#000000").ss(9.5,1,1).p("Aj8guQAuA/AVAUQBDBBBvADQBuAABLhMQAlglAmhg");
	this.shape_80.setTransform(44.6,39.9);

	this.shape_81 = new cjs.Shape();
	this.shape_81.graphics.f().s("#000000").ss(9.5,1,1).p("Aj2gtQAoA2AVAVQBDBABvAEQBugBBLhLQAlglAghS");
	this.shape_81.setTransform(44.6,40.7);

	this.shape_82 = new cjs.Shape();
	this.shape_82.graphics.f().s("#000000").ss(9.5,1,1).p("AjxgsQAiAvAVAVQBDBABvAEQBugBBLhLQAlglAchG");
	this.shape_82.setTransform(44.7,41.3);

	this.shape_83 = new cjs.Shape();
	this.shape_83.graphics.f().s("#000000").ss(9.5,1,1).p("AjrgrQAcAqAVATQBDBBBvADQBuAABLhMQAlglAXg6");
	this.shape_83.setTransform(44.7,41.8);

	this.shape_84 = new cjs.Shape();
	this.shape_84.graphics.f().s("#000000").ss(9.5,1,1).p("AjogqQAYAkAVAUQBDBABvAEQBugBBMhLQAlglATgy");
	this.shape_84.setTransform(44.7,42.3);

	this.shape_85 = new cjs.Shape();
	this.shape_85.graphics.f().s("#000000").ss(9.5,1,1).p("AjkgqQAUAgAVAUQBDBABvAEQBugBBLhLQAmglAPgq");
	this.shape_85.setTransform(44.7,42.7);

	this.shape_86 = new cjs.Shape();
	this.shape_86.graphics.f().s("#000000").ss(9.5,1,1).p("AjigpQASAcAVAUQBDBABvAEQBugBBLhKQAlgmAOgk");
	this.shape_86.setTransform(44.8,43);

	this.shape_87 = new cjs.Shape();
	this.shape_87.graphics.f().s("#000000").ss(9.5,1,1).p("AjggpQAQAaAVAUQBDBABvAEQBugBBLhKQAlgmAMgg");
	this.shape_87.setTransform(44.8,43.2);

	this.shape_88 = new cjs.Shape();
	this.shape_88.graphics.f().s("#000000").ss(9.5,1,1).p("AjfgoQAPAYAVAUQBDBABvAEQBugBBLhKQAlgmALge");
	this.shape_88.setTransform(44.8,43.3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_70}]}).to({state:[{t:this.shape_70}]},9).to({state:[{t:this.shape_71}]},1).to({state:[{t:this.shape_72}]},1).to({state:[{t:this.shape_73}]},1).to({state:[{t:this.shape_74}]},1).to({state:[{t:this.shape_75}]},1).to({state:[{t:this.shape_76}]},1).to({state:[{t:this.shape_77}]},1).to({state:[{t:this.shape_78}]},1).to({state:[{t:this.shape_79}]},1).to({state:[{t:this.shape_80}]},1).to({state:[{t:this.shape_80}]},20).to({state:[{t:this.shape_81}]},1).to({state:[{t:this.shape_82}]},1).to({state:[{t:this.shape_83}]},1).to({state:[{t:this.shape_84}]},1).to({state:[{t:this.shape_85}]},1).to({state:[{t:this.shape_86}]},1).to({state:[{t:this.shape_87}]},1).to({state:[{t:this.shape_88}]},1).to({state:[{t:this.shape_70}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-6.3,-4.6,104.2,59.8);


(lib.Символ1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Symbol 9
	this.instance = new lib.Symbol9();
	this.instance.parent = this;
	this.instance.setTransform(730.4,274.5,1.33,1.33,0,0,0,14.5,7.4);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	// Symbol 10
	this.instance_1 = new lib.Symbol10();
	this.instance_1.parent = this;
	this.instance_1.setTransform(832.9,384.7,1.07,1.07,1.8,0,0,69,65.5);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(1));

}).prototype = getMCSymbolPrototype(lib.Символ1, new cjs.Rectangle(702.7,258.6,205.8,198.2), null);


// stage content:
(lib.Cacao_vert = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой_1
	this.instance = new lib.Символ1();
	this.instance.parent = this;
	this.instance.setTransform(68,435.1,0.51,0.51,0,0,0,539.6,340.1);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	// Слой_6
	this.instance_1 = new lib.bg_pack_vert();
	this.instance_1.parent = this;
	this.instance_1.setTransform(-33,246);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(1));

	// Слой_7
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("EhWkBIhMAAAiRBMCtJAAAMAAACRBg");
	this.shape.setTransform(282.1,396.1);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-84.6,338,1108.3,928.3);
// library properties:
lib.properties = {
	id: 'BD16DB2468AB1D4B8B08B54DFE0A7311',
	width: 375,
	height: 812,
	fps: 30,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: [
		{src:"assets/images/bg_pack_vert30.jpg", id:"bg_pack_vert"}
	],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['BD16DB2468AB1D4B8B08B54DFE0A7311'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}



})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;

$(function () {
    var canvas, stage, exportRoot, anim_container, dom_overlay_container, fnStartAnimation;
    function init() {
        canvas = document.getElementById("canvas30-2");
        anim_container = document.getElementById("animation_container30-2");
        dom_overlay_container = document.getElementById("dom_overlay_container30-2");
        var comp=AdobeAn.getComposition("BD16DB2468AB1D4B8B08B54DFE0A7311");
        var lib=comp.getLibrary();
        var loader = new createjs.LoadQueue(false);
        loader.addEventListener("fileload", function(evt){handleFileLoad(evt,comp)});
        loader.addEventListener("complete", function(evt){handleComplete(evt,comp)});
        var lib=comp.getLibrary();
        loader.loadManifest(lib.properties.manifest);
    }
    function handleFileLoad(evt, comp) {
        var images=comp.getImages();
        if (evt && (evt.item.type == "image")) { images[evt.item.id] = evt.result; }
    }
    function handleComplete(evt,comp) {
        //This function is always called, irrespective of the content. You can use the variable "stage" after it is created in token create_stage.
        var lib=comp.getLibrary();
        var ss=comp.getSpriteSheet();
        var queue = evt.target;
        var ssMetadata = lib.ssMetadata;
        for(i=0; i<ssMetadata.length; i++) {
            ss[ssMetadata[i].name] = new createjs.SpriteSheet( {"images": [queue.getResult(ssMetadata[i].name)], "frames": ssMetadata[i].frames} )
        }
        exportRoot = new lib.Cacao_vert();
        stage = new lib.Stage(canvas);
        //Registers the "tick" event listener.
        fnStartAnimation = function() {
            stage.addChild(exportRoot);
            createjs.Ticker.setFPS(lib.properties.fps);
            createjs.Ticker.addEventListener("tick", stage);
        }
        //Code to support hidpi screens and responsive scaling.
        function makeResponsive(isResp, respDim, isScale, scaleType) {
            var lastW, lastH, lastS=1;
            window.addEventListener('resize', resizeCanvas);
            resizeCanvas();
            function resizeCanvas() {
                var w = lib.properties.width, h = lib.properties.height;
                var iw = window.innerWidth, ih=window.innerHeight;
                var pRatio = window.devicePixelRatio || 1, xRatio=iw/w, yRatio=ih/h, sRatio=1;
                if(isResp) {
                    if((respDim=='width'&&lastW==iw) || (respDim=='height'&&lastH==ih)) {
                        sRatio = lastS;
                    }
                    else if(!isScale) {
                        if(iw<w || ih<h)
                            sRatio = Math.min(xRatio, yRatio);
                    }
                    else if(scaleType==1) {
                        sRatio = Math.min(xRatio, yRatio);
                    }
                    else if(scaleType==2) {
                        sRatio = Math.max(xRatio, yRatio);
                    }
                }
                canvas.width = w*pRatio*sRatio;
                canvas.height = h*pRatio*sRatio;
                canvas.style.width = dom_overlay_container.style.width = anim_container.style.width =  w*sRatio+'px';
                canvas.style.height = anim_container.style.height = dom_overlay_container.style.height = h*sRatio+'px';
                stage.scaleX = pRatio*sRatio;
                stage.scaleY = pRatio*sRatio;
                lastW = iw; lastH = ih; lastS = sRatio;
                stage.tickOnUpdate = false;
                stage.update();
                stage.tickOnUpdate = true;
            }
        }
        makeResponsive(true,'both',true,2);
        AdobeAn.compositionLoaded(lib.properties.id);
        fnStartAnimation();
    }
    init();
});