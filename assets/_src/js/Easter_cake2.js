(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [];


// symbols:



(lib.cake = function() {
	this.initialize(img.cake);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,248,289);


(lib.eggs = function() {
	this.initialize(img.eggs);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,320,147);


(lib.kroshka1 = function() {
	this.initialize(img.kroshka1);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,9,9);


(lib.kroshka2 = function() {
	this.initialize(img.kroshka2);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,8,8);


(lib.kroshka3 = function() {
	this.initialize(img.kroshka3);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,5,5);


(lib.kroshka4 = function() {
	this.initialize(img.kroshka4);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,5,5);


(lib.kroshka5 = function() {
	this.initialize(img.kroshka5);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,5,5);


(lib.pack = function() {
	this.initialize(img.pack);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,239,450);


(lib.yazik = function() {
	this.initialize(img.yazik);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,16,21);// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.Symbol9 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(12,1,1).p("ABWgDQipiMgCDL");
	this.shape.setTransform(165,59.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(12,1,1).p("AhVA8QAIi/CjBy");
	this.shape_1.setTransform(165,59.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(12,1,1).p("AhWA9QAPi0CeBY");
	this.shape_2.setTransform(165.1,59);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(12,1,1).p("AhWA+QAVipCYA/");
	this.shape_3.setTransform(165.1,58.9);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(12,1,1).p("ABYg4QiTglgcCd");
	this.shape_4.setTransform(165.2,58.7);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(12,1,1).p("AhUBNQgDi5CsAn");
	this.shape_5.setTransform(164.9,57.4);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(12,1,1).p("AhPBaQgjjVDHAo");
	this.shape_6.setTransform(164.4,56.1);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(12,1,1).p("AhHBoQhDjyDhAq");
	this.shape_7.setTransform(163.6,54.7);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(12,1,1).p("ABbhuQj8grBjEO");
	this.shape_8.setTransform(162.7,53.4);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(12,1,1).p("AhDBqQhSkDDuA8");
	this.shape_9.setTransform(163.2,54.5);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(12,1,1).p("AhHBfQhCj3DgBL");
	this.shape_10.setTransform(163.6,55.6);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(12,1,1).p("AhMBWQgwjtDSBc");
	this.shape_11.setTransform(164.1,56.5);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(12,1,1).p("AhQBNQgfjiDEBs");
	this.shape_12.setTransform(164.5,57.4);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(12,1,1).p("AhTBEQgPjWC3B8");
	this.shape_13.setTransform(164.8,58.3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},18).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape}]},1).wait(17));
	this.timeline.addTween(cjs.Tween.get(this.shape).wait(18).to({_off:true},1).wait(13).to({_off:false},0).to({_off:true},1).wait(13).to({_off:false},0).to({_off:true},1).wait(13).to({_off:false},0).wait(17));

	// Layer 2
	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(12,1,1).p("Ag7CXQhWiKAyhSQAxhRCgAA");
	this.shape_14.setTransform(159.6,55.5);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(12,1,1).p("Ag+CbQhOiGAzhSQAzhRCUgM");
	this.shape_15.setTransform(160,55.1);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(12,1,1).p("AhCCgQhFiEA0hRQA0hRCJgZ");
	this.shape_16.setTransform(160.4,54.6);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(12,1,1).p("AhGClQg9iCA2hRQA2hQB+gm");
	this.shape_17.setTransform(160.7,54.1);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#000000").ss(12,1,1).p("AhJCqQg0iAA3hQQA3hQBzgz");
	this.shape_18.setTransform(161,53.6);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#000000").ss(12,1,1).p("AhKCTQg6iBBAhKQBAhKBrgQ");
	this.shape_19.setTransform(161.2,55.9);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#000000").ss(12,1,1).p("AhMB+Qg/iBBJhEQBIhFBkAU");
	this.shape_20.setTransform(161.3,58);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#000000").ss(12,1,1).p("AhNBxQhEiBBSg/QBRg/BbA4");
	this.shape_21.setTransform(161.4,59.3);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#000000").ss(12,1,1).p("AhOBpQhKiCBbg5QBag5BTBc");
	this.shape_22.setTransform(161.5,60.1);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#000000").ss(12,1,1).p("AhLBuQhMiCBUg+QBTg9BgBN");
	this.shape_23.setTransform(161.2,59.6);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#000000").ss(12,1,1).p("AhIB0QhOiEBOhCQBMhBBsA+");
	this.shape_24.setTransform(160.9,59);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f().s("#000000").ss(12,1,1).p("AhFB7QhQiFBHhGQBGhFB5Au");
	this.shape_25.setTransform(160.6,58.3);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f().s("#000000").ss(12,1,1).p("AhCCCQhSiGBAhKQA/hJCGAe");
	this.shape_26.setTransform(160.3,57.6);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f().s("#000000").ss(12,1,1).p("Ag+CMQhUiJA5hNQA4hNCSAP");
	this.shape_27.setTransform(160,56.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_14}]}).to({state:[{t:this.shape_14}]},18).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_27}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_27}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_27}]},1).to({state:[{t:this.shape_14}]},1).wait(17));
	this.timeline.addTween(cjs.Tween.get(this.shape_14).wait(18).to({_off:true},1).wait(13).to({_off:false},0).to({_off:true},1).wait(13).to({_off:false},0).to({_off:true},1).wait(13).to({_off:false},0).wait(17));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(142.2,34.5,37.4,42.1);


(lib.Symbol8 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.kroshka5();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol8, new cjs.Rectangle(0,0,5,5), null);


(lib.Symbol7 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.kroshka4();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol7, new cjs.Rectangle(0,0,5,5), null);


(lib.Symbol6 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.kroshka3();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol6, new cjs.Rectangle(0,0,5,5), null);


(lib.Symbol5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.kroshka2();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol5, new cjs.Rectangle(0,0,8,8), null);


(lib.Symbol4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.kroshka1();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol4, new cjs.Rectangle(0,0,9,9), null);


(lib.Symbol2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.yazik();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol2, new cjs.Rectangle(0,0,16,21), null);


(lib.Symbol1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 5
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(12,1,1).p("AC0BRQjfk3iIEe");
	this.shape.setTransform(18,8.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(12,1,1).p("Ai0A8QCHkeDiEx");
	this.shape_1.setTransform(18.1,7.7);
	this.shape_1._off = true;

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(12,1,1).p("Ai1A/QCIkeDjEs");
	this.shape_2.setTransform(18.2,7.4);
	this.shape_2._off = true;

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(12,1,1).p("Ai2BDQCHkeDmEm");
	this.shape_3.setTransform(18.3,7);
	this.shape_3._off = true;

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(12,1,1).p("Ai3BGQCIkdDnEg");
	this.shape_4.setTransform(18.4,6.7);
	this.shape_4._off = true;

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(12,1,1).p("Ai4BIQCIkdDpEa");
	this.shape_5.setTransform(18.5,6.5);
	this.shape_5._off = true;

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(12,1,1).p("Ai5BJQCHkeDsEU");
	this.shape_6.setTransform(18.6,6.4);
	this.shape_6._off = true;

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(12,1,1).p("Ai6BKQCIkeDtEP");
	this.shape_7.setTransform(18.7,6.3);
	this.shape_7._off = true;

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(12,1,1).p("AC8A1QjwkIiHEd");
	this.shape_8.setTransform(18.8,6.3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},16).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape}]},1).wait(13));
	this.timeline.addTween(cjs.Tween.get(this.shape).wait(16).to({_off:true},1).wait(15).to({_off:false},0).to({_off:true},1).wait(15).to({_off:false},0).to({_off:true},1).wait(15).to({_off:false},0).wait(13));
	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(17).to({_off:false},0).to({_off:true},1).wait(13).to({_off:false},0).to({_off:true},1).wait(1).to({_off:false},0).to({_off:true},1).wait(13).to({_off:false},0).to({_off:true},1).wait(1).to({_off:false},0).to({_off:true},1).wait(13).to({_off:false},0).to({_off:true},1).wait(13));
	this.timeline.addTween(cjs.Tween.get(this.shape_2).wait(18).to({_off:false},0).to({_off:true},1).wait(11).to({_off:false},0).to({_off:true},1).wait(3).to({_off:false},0).to({_off:true},1).wait(11).to({_off:false},0).to({_off:true},1).wait(3).to({_off:false},0).to({_off:true},1).wait(11).to({_off:false},0).to({_off:true},1).wait(14));
	this.timeline.addTween(cjs.Tween.get(this.shape_3).wait(19).to({_off:false},0).to({_off:true},1).wait(9).to({_off:false},0).to({_off:true},1).wait(5).to({_off:false},0).to({_off:true},1).wait(9).to({_off:false},0).to({_off:true},1).wait(5).to({_off:false},0).to({_off:true},1).wait(9).to({_off:false},0).to({_off:true},1).wait(15));
	this.timeline.addTween(cjs.Tween.get(this.shape_4).wait(20).to({_off:false},0).to({_off:true},1).wait(7).to({_off:false},0).to({_off:true},1).wait(7).to({_off:false},0).to({_off:true},1).wait(7).to({_off:false},0).to({_off:true},1).wait(7).to({_off:false},0).to({_off:true},1).wait(7).to({_off:false},0).to({_off:true},1).wait(16));
	this.timeline.addTween(cjs.Tween.get(this.shape_5).wait(21).to({_off:false},0).to({_off:true},1).wait(5).to({_off:false},0).to({_off:true},1).wait(9).to({_off:false},0).to({_off:true},1).wait(5).to({_off:false},0).to({_off:true},1).wait(9).to({_off:false},0).to({_off:true},1).wait(5).to({_off:false},0).to({_off:true},1).wait(17));
	this.timeline.addTween(cjs.Tween.get(this.shape_6).wait(22).to({_off:false},0).to({_off:true},1).wait(3).to({_off:false},0).to({_off:true},1).wait(11).to({_off:false},0).to({_off:true},1).wait(3).to({_off:false},0).to({_off:true},1).wait(11).to({_off:false},0).to({_off:true},1).wait(3).to({_off:false},0).to({_off:true},1).wait(18));
	this.timeline.addTween(cjs.Tween.get(this.shape_7).wait(23).to({_off:false},0).to({_off:true},1).wait(1).to({_off:false},0).to({_off:true},1).wait(13).to({_off:false},0).to({_off:true},1).wait(1).to({_off:false},0).to({_off:true},1).wait(13).to({_off:false},0).to({_off:true},1).wait(1).to({_off:false},0).to({_off:true},1).wait(19));

	// Layer 6
	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(12,1,1).p("AAthNQAfDHh+g6");
	this.shape_9.setTransform(17.3,11.8);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(12,1,1).p("Ag0BAQB3AwgPi5");
	this.shape_10.setTransform(16.7,11.4);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(12,1,1).p("Ag4A/QBwAnABir");
	this.shape_11.setTransform(16.2,11);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(12,1,1).p("Ag8A/QBoAdARie");
	this.shape_12.setTransform(15.8,10.7);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(12,1,1).p("AhBA+QBiATAhiQ");
	this.shape_13.setTransform(15.3,10.4);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(12,1,1).p("AhFA8QBbAKAwiC");
	this.shape_14.setTransform(14.9,10.1);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(12,1,1).p("ABKg6QhAB1hTgB");
	this.shape_15.setTransform(14.5,9.9);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(12,1,1).p("AhFA8QBZAIAyiA");
	this.shape_16.setTransform(14.8,10.1);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(12,1,1).p("AhCA9QBgARAliM");
	this.shape_17.setTransform(15.2,10.3);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#000000").ss(12,1,1).p("Ag+A+QBlAZAYiY");
	this.shape_18.setTransform(15.6,10.6);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#000000").ss(12,1,1).p("Ag6A/QBsAhAJik");
	this.shape_19.setTransform(15.9,10.9);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#000000").ss(12,1,1).p("Ag3BAQByApgEiv");
	this.shape_20.setTransform(16.3,11.1);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#000000").ss(12,1,1).p("Ag0BAQB4AygRi7");
	this.shape_21.setTransform(16.8,11.4);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_9}]}).to({state:[{t:this.shape_9}]},16).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_9}]},3).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_9}]},3).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_9}]},1).wait(16));
	this.timeline.addTween(cjs.Tween.get(this.shape_9).wait(16).to({_off:true},1).wait(12).to({_off:false},0).wait(3).to({_off:true},1).wait(12).to({_off:false},0).wait(3).to({_off:true},1).wait(12).to({_off:false},0).wait(16));

	// Layer 7
	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#000000").ss(12,1,1).p("ABNhUQgoDAhxga");
	this.shape_22.setTransform(9.6,10.6);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#000000").ss(12,1,1).p("AhBBUQB5AVAKi+");
	this.shape_23.setTransform(10.8,10.7);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#000000").ss(12,1,1).p("Ag2BVQB+ARgUi8");
	this.shape_24.setTransform(12.1,10.8);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f().s("#000000").ss(12,1,1).p("AgwBXQCFAMgyi6");
	this.shape_25.setTransform(13.9,10.9);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f().s("#000000").ss(12,1,1).p("AgsBZQCMAHhRi4");
	this.shape_26.setTransform(15.8,11);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f().s("#000000").ss(12,1,1).p("AgpBaQCSADhui2");
	this.shape_27.setTransform(17.9,11.1);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f().s("#000000").ss(12,1,1).p("AgbhaQCNC0iZAB");
	this.shape_28.setTransform(20.1,11.2);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f().s("#000000").ss(12,1,1).p("AgpBaQCTADhyi2");
	this.shape_29.setTransform(18.2,11.1);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f().s("#000000").ss(12,1,1).p("AgrBZQCNAGhZi3");
	this.shape_30.setTransform(16.4,11);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f().s("#000000").ss(12,1,1).p("AguBYQCIAKg/i5");
	this.shape_31.setTransform(14.7,10.9);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f().s("#000000").ss(12,1,1).p("AgyBWQCCAPgli7");
	this.shape_32.setTransform(13.1,10.8);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f().s("#000000").ss(12,1,1).p("Ag4BVQB8ASgLi9");
	this.shape_33.setTransform(11.7,10.8);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f().s("#000000").ss(12,1,1).p("AhCBTQB3AWAOi+");
	this.shape_34.setTransform(10.7,10.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_22}]}).to({state:[{t:this.shape_22}]},16).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_27}]},1).to({state:[{t:this.shape_28}]},1).to({state:[{t:this.shape_29}]},1).to({state:[{t:this.shape_30}]},1).to({state:[{t:this.shape_31}]},1).to({state:[{t:this.shape_32}]},1).to({state:[{t:this.shape_33}]},1).to({state:[{t:this.shape_34}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_22}]},3).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_27}]},1).to({state:[{t:this.shape_28}]},1).to({state:[{t:this.shape_29}]},1).to({state:[{t:this.shape_30}]},1).to({state:[{t:this.shape_31}]},1).to({state:[{t:this.shape_32}]},1).to({state:[{t:this.shape_33}]},1).to({state:[{t:this.shape_34}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_22}]},3).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_27}]},1).to({state:[{t:this.shape_28}]},1).to({state:[{t:this.shape_29}]},1).to({state:[{t:this.shape_30}]},1).to({state:[{t:this.shape_31}]},1).to({state:[{t:this.shape_32}]},1).to({state:[{t:this.shape_33}]},1).to({state:[{t:this.shape_34}]},1).to({state:[{t:this.shape_22}]},1).wait(16));
	this.timeline.addTween(cjs.Tween.get(this.shape_22).wait(16).to({_off:true},1).wait(12).to({_off:false},0).wait(3).to({_off:true},1).wait(12).to({_off:false},0).wait(3).to({_off:true},1).wait(12).to({_off:false},0).wait(16));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-6,-6,48,31.5);


(lib.Symbol3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 12
	this.instance = new lib.Symbol4();
	this.instance.parent = this;
	this.instance.setTransform(56.4,14.3,0.6,0.6,0,0,0,4.5,4.5);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(33).to({_off:false},0).to({y:104.3},11).to({_off:true},1).wait(59));

	// Layer 20
	this.instance_1 = new lib.Symbol5();
	this.instance_1.parent = this;
	this.instance_1.setTransform(45.7,18.3,1,1,0,0,0,4,4);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(31).to({_off:false},0).to({y:105.3},8).to({_off:true},1).wait(64));

	// Layer 19
	this.instance_2 = new lib.Symbol8();
	this.instance_2.parent = this;
	this.instance_2.setTransform(53.5,14.4,0.7,0.7,0,0,0,2.5,2.5);
	this.instance_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(29).to({_off:false},0).to({y:104.4},18).to({_off:true},1).wait(56));

	// Layer 18
	this.instance_3 = new lib.Symbol4();
	this.instance_3.parent = this;
	this.instance_3.setTransform(42.9,23.9,0.5,0.5,0,0,0,4.5,4.5);
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(27).to({_off:false},0).to({y:101.9},14).to({_off:true},1).wait(62));

	// Layer 17
	this.instance_4 = new lib.Symbol4();
	this.instance_4.parent = this;
	this.instance_4.setTransform(57.9,14.9,0.5,0.5,0,0,0,4.5,4.5);
	this.instance_4._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(26).to({_off:false},0).to({y:104.9},17).to({_off:true},1).wait(60));

	// Layer 16
	this.instance_5 = new lib.Symbol8();
	this.instance_5.parent = this;
	this.instance_5.setTransform(32.5,21.3,1,1,0,0,0,2.5,2.5);
	this.instance_5._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(24).to({_off:false},0).to({y:102.3},14).to({_off:true},1).wait(65));

	// Layer 15
	this.instance_6 = new lib.Symbol7();
	this.instance_6.parent = this;
	this.instance_6.setTransform(38.5,22.5,1,1,0,0,0,2.5,2.5);
	this.instance_6._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(22).to({_off:false},0).to({y:103.5},16).to({_off:true},1).wait(65));

	// Layer 14
	this.instance_7 = new lib.Symbol6();
	this.instance_7.parent = this;
	this.instance_7.setTransform(51.4,16.2,1,1,0,0,0,2.5,2.5);
	this.instance_7._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(20).to({_off:false},0).to({y:103.2},15).to({_off:true},1).wait(68));

	// Layer 13
	this.instance_8 = new lib.Symbol5();
	this.instance_8.parent = this;
	this.instance_8.setTransform(39.6,17.7,1,1,0,0,0,4,4);
	this.instance_8._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_8).wait(24).to({_off:false},0).to({y:104.7},10).to({_off:true},1).wait(69));

	// Layer 11
	this.instance_9 = new lib.Symbol4();
	this.instance_9.parent = this;
	this.instance_9.setTransform(42.3,22.4,1,1,0,0,0,4.5,4.5);
	this.instance_9._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_9).wait(38).to({_off:false},0).to({y:103.4},8).to({_off:true},1).wait(57));

	// Layer 10
	this.instance_10 = new lib.Symbol4();
	this.instance_10.parent = this;
	this.instance_10.setTransform(56.4,14.3,0.6,0.6,0,0,0,4.5,4.5);
	this.instance_10._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_10).wait(15).to({_off:false},0).to({y:104.3},11).to({_off:true},1).wait(77));

	// Layer 9
	this.instance_11 = new lib.Symbol5();
	this.instance_11.parent = this;
	this.instance_11.setTransform(45.7,18.3,1,1,0,0,0,4,4);
	this.instance_11._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_11).wait(13).to({_off:false},0).to({y:105.3},8).to({_off:true},1).wait(82));

	// Layer 8
	this.instance_12 = new lib.Symbol8();
	this.instance_12.parent = this;
	this.instance_12.setTransform(53.5,14.4,0.7,0.7,0,0,0,2.5,2.5);
	this.instance_12._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_12).wait(11).to({_off:false},0).to({y:104.4},18).to({_off:true},1).wait(74));

	// Layer 7
	this.instance_13 = new lib.Symbol4();
	this.instance_13.parent = this;
	this.instance_13.setTransform(42.9,23.9,0.5,0.5,0,0,0,4.5,4.5);
	this.instance_13._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_13).wait(9).to({_off:false},0).to({y:101.9},14).to({_off:true},1).wait(80));

	// Layer 6
	this.instance_14 = new lib.Symbol4();
	this.instance_14.parent = this;
	this.instance_14.setTransform(57.9,14.9,0.5,0.5,0,0,0,4.5,4.5);
	this.instance_14._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_14).wait(8).to({_off:false},0).to({y:104.9},17).to({_off:true},1).wait(78));

	// Layer 5
	this.instance_15 = new lib.Symbol8();
	this.instance_15.parent = this;
	this.instance_15.setTransform(32.5,21.3,1,1,0,0,0,2.5,2.5);
	this.instance_15._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_15).wait(6).to({_off:false},0).to({y:102.3},14).to({_off:true},1).wait(83));

	// Layer 4
	this.instance_16 = new lib.Symbol7();
	this.instance_16.parent = this;
	this.instance_16.setTransform(38.5,22.5,1,1,0,0,0,2.5,2.5);
	this.instance_16._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_16).wait(4).to({_off:false},0).to({y:103.5},16).to({_off:true},1).wait(83));

	// Layer 3
	this.instance_17 = new lib.Symbol6();
	this.instance_17.parent = this;
	this.instance_17.setTransform(51.4,16.2,1,1,0,0,0,2.5,2.5);
	this.instance_17._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_17).wait(2).to({_off:false},0).to({y:103.2},15).to({_off:true},1).wait(86));

	// Layer 2
	this.instance_18 = new lib.Symbol5();
	this.instance_18.parent = this;
	this.instance_18.setTransform(49.6,17.7,1,1,0,0,0,4,4);

	this.timeline.addTween(cjs.Tween.get(this.instance_18).to({y:104.7},10).to({_off:true},1).wait(93));

	// Layer 1
	this.instance_19 = new lib.Symbol4();
	this.instance_19.parent = this;
	this.instance_19.setTransform(42.3,22.4,1,1,0,0,0,4.5,4.5);
	this.instance_19._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_19).wait(20).to({_off:false},0).to({y:103.4},8).to({_off:true},1).wait(75));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(45.6,13.7,8,8);


(lib.Symbol10 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.eggs();
	this.instance.parent = this;
	this.instance.setTransform(374.3,482.9);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(77));

	// Layer 2
	this.instance_1 = new lib.cake();
	this.instance_1.parent = this;
	this.instance_1.setTransform(766,356);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(77));

	// Layer 3
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(12,1,1).p("AipAJQA3ABA0gEQA1gEApgDQAqgEAkAAQAmgBAWgD");
	this.shape.setTransform(732,274);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(12,1,1).p("AiqAJQA4ABA0gEQA1gEApgDQApgEAlAAQAmgBAWgD");
	this.shape_1.setTransform(732,274);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(12,1,1).p("AinAQQAygOA1gIQA0gJAsAAQAsgBAlAIQAlAIASAL");
	this.shape_2.setTransform(731.8,273.3);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(12,1,1).p("AikAXQAtgaA2gOQAzgOAvADQAuACAkAQQAmARAMAW");
	this.shape_3.setTransform(731.5,272.7);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(12,1,1).p("AiiAaQApglA2gSQAzgRAxAFQAxAEAjAYQAmAXAJAh");
	this.shape_4.setTransform(731.3,272.3);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(12,1,1).p("AihAeQAmgvA3gWQAygUAzAHQAyAHAkAdQAlAeAGAp");
	this.shape_5.setTransform(731.1,272);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(12,1,1).p("AifAgQAjg3A3gZQAygWA0AJQA0AIAjAiQAmAjACAx");
	this.shape_6.setTransform(731,271.8);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(12,1,1).p("AifAiQAhg9A3gbQAzgZA1ALQA1AKAjAlQAlAnABA2");
	this.shape_7.setTransform(730.9,271.6);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(12,1,1).p("AieAjQAghBA3gcQAygaA2ALQA2ALAjAoQAlApgBA7");
	this.shape_8.setTransform(730.8,271.4);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(12,1,1).p("AidAkQAehEA4gdQAygbA2AMQA2ALAjAqQAmArgCA9");
	this.shape_9.setTransform(730.8,271.3);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(12,1,1).p("AidAkQAehFA3gdQAygbA3AMQA2ALAjArQAmArgCA+");
	this.shape_10.setTransform(730.8,271.3);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(12,1,1).p("AiiAcQAngrA3gVQAzgSAyAGQAyAGAjAbQAlAbAIAn");
	this.shape_11.setTransform(731.2,272.1);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(12,1,1).p("AilAWQAvgXA1gNQA0gNAuACQAuACAkAPQAlAOAOAU");
	this.shape_12.setTransform(731.6,272.7);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(12,1,1).p("AinAOQAzgLA1gHQA0gIArgBQAsgBAlAGQAlAHATAG");
	this.shape_13.setTransform(731.8,273.6);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(12,1,1).p("AipAJQA2gCA1gFQA0gFAqgCQAqgEAlABQAlACAWgB");
	this.shape_14.setTransform(732,274.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape_1}]},16).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_10}]},43).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape}]},1).wait(4));

	// Layer 4
	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(12,1,1).p("AibgZQAdABAjAFQAiAFAuAHQAsAIAjAGQAiAFA2AP");
	this.shape_15.setTransform(671.2,269.9);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(12,1,1).p("AibgZQAdABAjAFQAiAFAuAIQAsAHAjAGQAiAFA2AP");
	this.shape_16.setTransform(671.2,269.9);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(12,1,1).p("AiWgKQAYgKAjgEQAigCAuAFQAtAFAjAKQAlAJAtAW");
	this.shape_17.setTransform(671.2,269.5);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#000000").ss(12,1,1).p("AiRAIQATgTAjgKQAigKAvADQAtADAkANQAlANAmAd");
	this.shape_18.setTransform(671.2,268.6);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#000000").ss(12,1,1).p("AiNAaQAPgcAjgRQAigPAvABQAtABAlAQQAnAQAfAj");
	this.shape_19.setTransform(671.2,267.7);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#000000").ss(12,1,1).p("AiKAoQAMgkAjgWQAigUAvgBQAugBAlATQAoAUAaAn");
	this.shape_20.setTransform(671.2,267);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#000000").ss(12,1,1).p("AiHAwQAJgrAkgZQAhgYAvgDQAugCAmAUQApAWAVAs");
	this.shape_21.setTransform(671.2,266.9);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#000000").ss(12,1,1).p("AiFA2QAHgvAkgdQAhgbAwgEQAugEAmAWQApAZASAu");
	this.shape_22.setTransform(671.2,266.7);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#000000").ss(12,1,1).p("AiDA7QAFgzAkggQAhgdAwgEQAugFAmAXQAqAaAPAw");
	this.shape_23.setTransform(671.2,266.6);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#000000").ss(12,1,1).p("AiDA+QAFg1AkghQAhgfAwgFQAugFAnAYQAqAbAOAy");
	this.shape_24.setTransform(671.2,266.5);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f().s("#000000").ss(12,1,1).p("AiCA+QAEg1AkgiQAhgfAwgFQAugFAnAYQAqAbANAy");
	this.shape_25.setTransform(671.2,266.5);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f().s("#000000").ss(12,1,1).p("AiLAjQANghAjgUQAigSAvAAQAuAAAlARQAnATAcAl");
	this.shape_26.setTransform(671.2,267.2);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f().s("#000000").ss(12,1,1).p("AiSAFQAUgSAjgJQAigIAuADQAtADAlANQAlAMAnAc");
	this.shape_27.setTransform(671.2,268.8);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f().s("#000000").ss(12,1,1).p("AiXgOQAZgIAjgBQAigBAuAGQAsAGAkAIQAkAIAvAV");
	this.shape_28.setTransform(671.2,269.7);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f().s("#000000").ss(12,1,1).p("AiagYQAcAAAjADQAiADAuAIQAsAHAjAGQAjAGA0AQ");
	this.shape_29.setTransform(671.2,269.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_15}]}).to({state:[{t:this.shape_16}]},16).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_25}]},43).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_27}]},1).to({state:[{t:this.shape_28}]},1).to({state:[{t:this.shape_29}]},1).to({state:[{t:this.shape_15}]},1).wait(4));

	// Layer 5
	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f().s("#000000").ss(12,1,1).p("AjrAmQA/AEBFgGQBFgHBAgNQBAgNAygIQAygJAqgZ");
	this.shape_30.setTransform(701.3,309);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f().s("#000000").ss(12,1,1).p("AjsAmQBAAEBFgGQBFgHBAgNQBAgNAygIQAxgJArgZ");
	this.shape_31.setTransform(701.3,309);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f().s("#000000").ss(12,1,1).p("AjpAVQA1APBEADQBDACBDgKQBDgKA1gOQA2gNAngg");
	this.shape_32.setTransform(701.3,309);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f().s("#000000").ss(12,1,1).p("AjoABQAsAZBEALQBBAKBFgHQBGgIA4gSQA5gRAkgn");
	this.shape_33.setTransform(701.4,309.5);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f().s("#000000").ss(12,1,1).p("AjmgRQAkAhBCATQA/AQBJgFQBIgFA6gWQA8gVAhgs");
	this.shape_34.setTransform(701.4,310);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f().s("#000000").ss(12,1,1).p("AjlghQAdApBBAZQA+AWBLgDQBKgDA8gZQA/gZAfgy");
	this.shape_35.setTransform(701.5,310.4);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f().s("#000000").ss(12,1,1).p("AjkgvQAXAvBBAeQA9AbBNgBQBLgCA+gbQBBgcAdg2");
	this.shape_36.setTransform(701.5,310.9);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f().s("#000000").ss(12,1,1).p("Ajjg7QASA2BBAiQA7AfBPAAQBNAAA/geQBDgeAbg5");
	this.shape_37.setTransform(701.6,311.2);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f().s("#000000").ss(12,1,1).p("AjihBQAOA6BAAlQA7AjBQABQBOABBBgfQBEgiAZg7");
	this.shape_38.setTransform(701.6,311.1);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f().s("#000000").ss(12,1,1).p("AjhhFQAKA9BBAnQA6AlBQACQBPACBCghQBFgiAYg9");
	this.shape_39.setTransform(701.6,311.1);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f().s("#000000").ss(12,1,1).p("AjhhHQAJA+BAApQA6AmBRACQBPADBCghQBHgkAXg+");
	this.shape_40.setTransform(701.6,311.1);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f().s("#000000").ss(12,1,1).p("AjhhIQAIA/BBApQA5AnBSACQBPADBCgiQBHgjAXg/");
	this.shape_41.setTransform(701.6,311.1);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f().s("#000000").ss(12,1,1).p("AjqAYQA4ANBEACQBDABBCgLQBDgLA0gNQA1gMAogf");
	this.shape_42.setTransform(701.3,309);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f().s("#000000").ss(12,1,1).p("AjrAkQA+AFBFgEQBEgEBBgNQBAgNAzgIQAygLAqga");
	this.shape_43.setTransform(701.3,308.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_30}]}).to({state:[{t:this.shape_31}]},20).to({state:[{t:this.shape_32}]},1).to({state:[{t:this.shape_33}]},1).to({state:[{t:this.shape_34}]},1).to({state:[{t:this.shape_35}]},1).to({state:[{t:this.shape_36}]},1).to({state:[{t:this.shape_37}]},1).to({state:[{t:this.shape_38}]},1).to({state:[{t:this.shape_39}]},1).to({state:[{t:this.shape_40}]},1).to({state:[{t:this.shape_41}]},1).to({state:[{t:this.shape_41}]},38).to({state:[{t:this.shape_35}]},1).to({state:[{t:this.shape_33}]},1).to({state:[{t:this.shape_42}]},1).to({state:[{t:this.shape_43}]},1).to({state:[{t:this.shape_30}]},1).wait(4));

	// Layer 7
	this.instance_2 = new lib.Symbol2();
	this.instance_2.parent = this;
	this.instance_2.setTransform(683.7,312.3,1,0.287,50.5,0,0,10.9,18.7);
	this.instance_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(29).to({_off:false},0).to({regX:10.8,scaleY:1},6).wait(1).to({regX:8,regY:10.5,rotation:49.8,x:688.4,y:305.2},0).wait(1).to({rotation:48.4,x:688.9,y:305.6},0).wait(1).to({rotation:46.1,x:689.8,y:306.4},0).wait(1).to({rotation:42.7,x:691.2,y:307.3},0).wait(1).to({rotation:37.7,x:693.5,y:308.4},0).wait(1).to({rotation:31.2,x:696.8,y:309.4},0).wait(1).to({rotation:23.7,x:700.7,y:309.8},0).wait(1).to({rotation:16.6,x:704.1,y:309.6},0).wait(1).to({rotation:10.7,x:706.7,y:309.2},0).wait(1).to({rotation:6.5,x:708.6,y:308.6},0).wait(1).to({rotation:3.5,x:710,y:308.1},0).wait(1).to({rotation:1.6,x:710.9,y:307.7},0).wait(1).to({rotation:0.5,x:711.3,y:307.4},0).wait(1).to({regX:10.9,regY:18.7,rotation:0,x:714.4,y:315.6},0).to({scaleY:0.18,guide:{path:[714.4,315.5,714.6,315.4,714.8,315.3,716.3,314.6,717.5,313.7]}},5).to({_off:true},1).wait(22));

	// Layer 8
	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f().s("#000000").ss(12,1,1).p("AjlmuQh6DRABDGQAAC1BkB9QBjB8CdAVQCpAWCxh2");
	this.shape_44.setTransform(637.8,400.9);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f().s("#000000").ss(12,1,1).p("Aj9mkQhwDUAKDFQAIC0BpB3QBoB3CcANQCpAOCqh9");
	this.shape_45.setTransform(640.2,399.8);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.f().s("#000000").ss(12,1,1).p("AkUmbQhmDYATDDQAQCyBuByQBsBxCcAGQCoAHCkiE");
	this.shape_46.setTransform(642.6,398.8);

	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.f().s("#000000").ss(12,1,1).p("AkqmSQhcDcAcDAQAYCyBzBtQBxBrCbgBQCogBCdiL");
	this.shape_47.setTransform(644.8,397.8);

	this.shape_48 = new cjs.Shape();
	this.shape_48.graphics.f().s("#000000").ss(12,1,1).p("AlAmJQhRDfAkC+QAhCxB3BnQB2BmCbgJQCngICWiS");
	this.shape_48.setTransform(647,396.9);

	this.shape_49 = new cjs.Shape();
	this.shape_49.graphics.f().s("#000000").ss(12,1,1).p("AlVmBQhHDjAtC9QApCvB8BhQB6BhCbgQQCngRCPiY");
	this.shape_49.setTransform(649.2,396);

	this.shape_50 = new cjs.Shape();
	this.shape_50.graphics.f().s("#000000").ss(12,1,1).p("Alql6Qg8DnA2C7QAxCuCBBcQB/BbCagXQCngYCIig");
	this.shape_50.setTransform(651.2,395.2);

	this.shape_51 = new cjs.Shape();
	this.shape_51.graphics.f().s("#000000").ss(12,1,1).p("Al9lyQgyDqA/C6QA5CsCFBWQCEBWCbgfQCmggCBim");
	this.shape_51.setTransform(653.2,394.4);

	this.shape_52 = new cjs.Shape();
	this.shape_52.graphics.f().s("#000000").ss(12,1,1).p("AmQlrQgnDuBHC4QBBCqCLBRQCIBQCaglQCmgoB7it");
	this.shape_52.setTransform(655.1,393.6);

	this.shape_53 = new cjs.Shape();
	this.shape_53.graphics.f().s("#000000").ss(12,1,1).p("Alsl5Qg7DnA3C7QAyCuCCBbQB/BaCbgYQCmgZCIig");
	this.shape_53.setTransform(651.4,395.1);

	this.shape_54 = new cjs.Shape();
	this.shape_54.graphics.f().s("#000000").ss(12,1,1).p("AlLmFQhMDhApC9QAkCwB6BkQB4BkCbgMQCngNCTiV");
	this.shape_54.setTransform(648.1,396.5);

	this.shape_55 = new cjs.Shape();
	this.shape_55.graphics.f().s("#000000").ss(12,1,1).p("AktmRQhaDcAcDAQAaCyBzBsQByBrCbgDQCogCCciL");
	this.shape_55.setTransform(645.1,397.7);

	this.shape_56 = new cjs.Shape();
	this.shape_56.graphics.f().s("#000000").ss(12,1,1).p("AkAmjQhuDVAKDEQAJC0BqB3QBoB2CdAMQCoANCph+");
	this.shape_56.setTransform(640.5,399.7);

	this.shape_57 = new cjs.Shape();
	this.shape_57.graphics.f().s("#000000").ss(12,1,1).p("AjxmpQh1DSAFDFQAEC1BnB6QBlB6CdARQCoASCuh6");
	this.shape_57.setTransform(639,400.4);

	this.shape_58 = new cjs.Shape();
	this.shape_58.graphics.f().s("#000000").ss(12,1,1).p("AjomtQh4DRABDGQABC1BlB9QBkB7CcATQCpAVCwh2");
	this.shape_58.setTransform(638.1,400.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_44}]}).to({state:[{t:this.shape_44}]},11).to({state:[{t:this.shape_45}]},1).to({state:[{t:this.shape_46}]},1).to({state:[{t:this.shape_47}]},1).to({state:[{t:this.shape_48}]},1).to({state:[{t:this.shape_49}]},1).to({state:[{t:this.shape_50}]},1).to({state:[{t:this.shape_51}]},1).to({state:[{t:this.shape_52}]},1).to({state:[{t:this.shape_52}]},43).to({state:[{t:this.shape_53}]},1).to({state:[{t:this.shape_54}]},1).to({state:[{t:this.shape_55}]},1).to({state:[{t:this.shape_46}]},1).to({state:[{t:this.shape_56}]},1).to({state:[{t:this.shape_57}]},1).to({state:[{t:this.shape_58}]},1).to({state:[{t:this.shape_44}]},1).wait(7));

	// Layer 9
	this.instance_3 = new lib.Symbol9();
	this.instance_3.parent = this;
	this.instance_3.setTransform(675.3,432.8,1,1,30,0,0,153.8,70.6);

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(11).to({regX:153.7,rotation:0,x:693.6,y:410.6},8).wait(43).to({regX:153.8,rotation:30,x:675.3,y:432.8},8,cjs.Ease.get(1)).wait(7));

	// Layer 11
	this.instance_4 = new lib.pack();
	this.instance_4.parent = this;
	this.instance_4.setTransform(595.9,147);

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(77));

	// Layer 12
	this.shape_59 = new cjs.Shape();
	this.shape_59.graphics.f().s("#000000").ss(12,1,1).p("AnGl0QABCCBAB/QBAB+C5B/QC5B/GaBs");
	this.shape_59.setTransform(822.5,367.6);

	this.shape_60 = new cjs.Shape();
	this.shape_60.graphics.f().s("#000000").ss(12,1,1).p("AnGl0QABCCBBB/QA/B+C5B/QC5B/GaBs");
	this.shape_60.setTransform(822.5,367.6);

	this.shape_61 = new cjs.Shape();
	this.shape_61.graphics.f().s("#000000").ss(12,1,1).p("AnTkRQASB3BLBvQBQBwCyBmQDZBdFvAK");
	this.shape_61.setTransform(824.3,357.8);

	this.shape_62 = new cjs.Shape();
	this.shape_62.graphics.f().s("#000000").ss(12,1,1).p("AnfjNQAiBtBVBgQBdBjCtBPQD1A/FJhP");
	this.shape_62.setTransform(826,351.1);

	this.shape_63 = new cjs.Shape();
	this.shape_63.graphics.f().s("#000000").ss(12,1,1).p("AnqinQAwBkBeBUQBqBXCoA6QEPAjEmif");
	this.shape_63.setTransform(827.5,347.3);

	this.shape_64 = new cjs.Shape();
	this.shape_64.graphics.f().s("#000000").ss(12,1,1).p("AnziLQA8BcBmBIQB2BMCiAnQEnALEHjm");
	this.shape_64.setTransform(828.9,344.6);

	this.shape_65 = new cjs.Shape();
	this.shape_65.graphics.f().s("#000000").ss(12,1,1).p("An8hUQBHBVBtA+QCBBDCdAXQE9gLDqkm");
	this.shape_65.setTransform(830.1,339.1);

	this.shape_66 = new cjs.Shape();
	this.shape_66.graphics.f().s("#000000").ss(12,1,1).p("AoDgJQBRBOByA2QCJA7CbAIQFOgdDSle");
	this.shape_66.setTransform(831.1,331.7);

	this.shape_67 = new cjs.Shape();
	this.shape_67.graphics.f().s("#000000").ss(12,1,1).p("AoKA1QBaBKB3AuQCRA0CXgEQFdgtC/mM");
	this.shape_67.setTransform(832,325.4);

	this.shape_68 = new cjs.Shape();
	this.shape_68.graphics.f().s("#000000").ss(12,1,1).p("AoPBnQBhBGB7AoQCXAuCVgOQFpg6Cumz");
	this.shape_68.setTransform(832.7,320.5);

	this.shape_69 = new cjs.Shape();
	this.shape_69.graphics.f().s("#000000").ss(12,1,1).p("AoTCNQBmBDB/AjQCbAqCTgWQFzhFChnP");
	this.shape_69.setTransform(833.2,316.7);

	this.shape_70 = new cjs.Shape();
	this.shape_70.graphics.f().s("#000000").ss(12,1,1).p("AoWCoQBqBACBAgQCfAmCQgaQF7hNCYnl");
	this.shape_70.setTransform(833.6,314);

	this.shape_71 = new cjs.Shape();
	this.shape_71.graphics.f().s("#000000").ss(12,1,1).p("AoXC4QBrA/CDAdQChAlCPgeQGAhQCRny");
	this.shape_71.setTransform(833.9,312.4);

	this.shape_72 = new cjs.Shape();
	this.shape_72.graphics.f().s("#000000").ss(12,1,1).p("AoYC9QBtA+CCAdQCiAkCQgfQGAhSCQn2");
	this.shape_72.setTransform(834,311.9);

	this.shape_73 = new cjs.Shape();
	this.shape_73.graphics.f().s("#000000").ss(12,1,1).p("AoXCyQBrBACCAeQChAlCQgdQF9hPCUnt");
	this.shape_73.setTransform(833.8,313);

	this.shape_74 = new cjs.Shape();
	this.shape_74.graphics.f().s("#000000").ss(12,1,1).p("AoUCSQBnBCB/AjQCdApCSgWQF1hHCfnU");
	this.shape_74.setTransform(833.3,316.2);

	this.shape_75 = new cjs.Shape();
	this.shape_75.graphics.f().s("#000000").ss(12,1,1).p("AoOBcQBfBHB7ApQCVAvCWgLQFmg4Cymp");
	this.shape_75.setTransform(832.5,321.6);

	this.shape_76 = new cjs.Shape();
	this.shape_76.graphics.f().s("#000000").ss(12,1,1).p("AoGANQBVBNB0AzQCMA4CaAEQFTgjDLlu");
	this.shape_76.setTransform(831.4,329.4);

	this.shape_77 = new cjs.Shape();
	this.shape_77.graphics.f().s("#000000").ss(12,1,1).p("An8hZQBIBWBrA+QCABDCfAYQE6gJDski");
	this.shape_77.setTransform(830,339.7);

	this.shape_78 = new cjs.Shape();
	this.shape_78.graphics.f().s("#000000").ss(12,1,1).p("AnviYQA3BgBiBNQBwBSClAwQEcAWEVjE");
	this.shape_78.setTransform(828.2,345.9);

	this.shape_79 = new cjs.Shape();
	this.shape_79.graphics.f().s("#000000").ss(12,1,1).p("AnjjAQAnBrBXBcQBiBfCrBIQD+A3E9ho");
	this.shape_79.setTransform(826.5,349.8);

	this.shape_80 = new cjs.Shape();
	this.shape_80.graphics.f().s("#000000").ss(12,1,1).p("AnYjuQAZByBPBqQBWBqCvBcQDlBQFfgc");
	this.shape_80.setTransform(825,354.4);

	this.shape_81 = new cjs.Shape();
	this.shape_81.graphics.f().s("#000000").ss(12,1,1).p("AnQknQAOB6BJByQBMBzC0BsQDSBkF4Ag");
	this.shape_81.setTransform(823.9,360);

	this.shape_82 = new cjs.Shape();
	this.shape_82.graphics.f().s("#000000").ss(12,1,1).p("AnKlRQAHB9BEB6QBFB5C3B2QDDB0GMBJ");
	this.shape_82.setTransform(823.1,364.2);

	this.shape_83 = new cjs.Shape();
	this.shape_83.graphics.f().s("#000000").ss(12,1,1).p("AnHlrQACCABCB+QBBB9C4B9QC8B8GWBj");
	this.shape_83.setTransform(822.6,366.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_59}]}).to({state:[{t:this.shape_60}]},4).to({state:[{t:this.shape_61}]},1).to({state:[{t:this.shape_62}]},1).to({state:[{t:this.shape_63}]},1).to({state:[{t:this.shape_64}]},1).to({state:[{t:this.shape_65}]},1).to({state:[{t:this.shape_66}]},1).to({state:[{t:this.shape_67}]},1).to({state:[{t:this.shape_68}]},1).to({state:[{t:this.shape_69}]},1).to({state:[{t:this.shape_70}]},1).to({state:[{t:this.shape_71}]},1).to({state:[{t:this.shape_72}]},1).to({state:[{t:this.shape_72}]},48).to({state:[{t:this.shape_73}]},1).to({state:[{t:this.shape_74}]},1).to({state:[{t:this.shape_75}]},1).to({state:[{t:this.shape_76}]},1).to({state:[{t:this.shape_77}]},1).to({state:[{t:this.shape_78}]},1).to({state:[{t:this.shape_79}]},1).to({state:[{t:this.shape_80}]},1).to({state:[{t:this.shape_81}]},1).to({state:[{t:this.shape_82}]},1).to({state:[{t:this.shape_83}]},1).to({state:[{t:this.shape_59}]},1).wait(1));

	// Layer 13
	this.instance_5 = new lib.Symbol1();
	this.instance_5.parent = this;
	this.instance_5.setTransform(868.7,404,1,1,69,0,0,0,9.8);

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(4).to({rotation:0,x:887.7,y:278.1},12,cjs.Ease.get(1)).wait(48).to({regX:0.1,scaleX:1,scaleY:1,rotation:34.6,x:878.3,y:341.1},6,cjs.Ease.get(-1)).to({regX:0,scaleX:1,scaleY:1,rotation:69,x:868.7,y:404},6,cjs.Ease.get(1)).wait(1));

	// Layer 14
	this.instance_6 = new lib.Symbol3("synched",0,false);
	this.instance_6.parent = this;
	this.instance_6.setTransform(904.7,329.7,1,1,0,0,0,43.5,63.6);
	this.instance_6._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(16).to({_off:false},0).to({_off:true},48).wait(13));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(374.3,147,639.7,498);


// stage2 content:
(lib.Easter_cake2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		var page_canvas = document.getElementsByTagName("canvas")[0];
		var r = this;

		page_canvas.style.margin = "0 auto";
		var viewport = document.querySelector('meta[name=viewport]');
		var viewportContent = 'width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0';

		if (viewport === null) {
		 var head = document.getElementsByTagName('head')[0];
		 viewport = document.createElement('meta');
		 viewport.setAttribute('name', 'viewport');
		 head.appendChild(viewport);
		}

		viewport.setAttribute('content', viewportContent);
		var page_body = document.getElementsByTagName("body")[0];
		page_body.style.backgroundColor = "#FFFFFF";


		function onResize() {
		 var newWidth = window.innerWidth;
		 var newHeight = window.innerHeight;
		 stage2.width = newWidth;
		 stage2.height = newHeight;
		 //r.pack1.x = window.innerWidth/2;
		 //r.pack1.y = window.innerHeight/2;
		 r.pack1.width = stage2.width;
		 r.pack1.height = stage2.height;
		}

		window.onresize = function () {
		 onResize();
		}

		onResize();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(1));

	// Layer 19
	this.pack1 = new lib.Symbol10();
	this.pack1.name = "pack1";
	this.pack1.parent = this;
	this.pack1.setTransform(540,340,1,1,0,0,0,540,340);

	this.timeline.addTween(cjs.Tween.get(this.pack1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(540,340,1080,680);
// library properties:
lib.properties = {
	id: '9336C9FBB403CF40AFC219E4F9D64F1C',
	width: 1080,
	height: 680,
	fps: 30,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: [
		{src:"assets/images/cake.png", id:"cake"},
		{src:"assets/images/eggs.png", id:"eggs"},
		{src:"assets/images/kroshka1.png", id:"kroshka1"},
		{src:"assets/images/kroshka2.png", id:"kroshka2"},
		{src:"assets/images/kroshka3.png", id:"kroshka3"},
		{src:"assets/images/kroshka4.png", id:"kroshka4"},
		{src:"assets/images/kroshka5.png", id:"kroshka5"},
		{src:"assets/images/pack.png", id:"pack"},
		{src:"assets/images/yazik.png", id:"yazik"}
	],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['9336C9FBB403CF40AFC219E4F9D64F1C'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}



})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;

$(function () {
    var canvas, stage2, exportRoot, anim_container, dom_overlay_container, fnStartAnimation;
    function init() {
        canvas = document.getElementById("canvas1");
        anim_container = document.getElementById("animation_container1");
        dom_overlay_container = document.getElementById("dom_overlay_container1");
        var comp=AdobeAn.getComposition("9336C9FBB403CF40AFC219E4F9D64F1C");
        var lib=comp.getLibrary();
        createjs.MotionGuidePlugin.install();
        var loader = new createjs.LoadQueue(false);
        loader.addEventListener("fileload", function(evt){handleFileLoad(evt,comp)});
        loader.addEventListener("complete", function(evt){handleComplete(evt,comp)});
        var lib=comp.getLibrary();
        loader.loadManifest(lib.properties.manifest);
    }
    function handleFileLoad(evt, comp) {
        var images=comp.getImages();
        if (evt && (evt.item.type == "image")) { images[evt.item.id] = evt.result; }
    }
    function handleComplete(evt,comp) {
        //This function is always called, irrespective of the content. You can use the variable "stage2" after it is created in token create_stage2.
        var lib=comp.getLibrary();
        var ss=comp.getSpriteSheet();
        var queue = evt.target;
        var ssMetadata = lib.ssMetadata;
        for(i=0; i<ssMetadata.length; i++) {
            ss[ssMetadata[i].name] = new createjs.SpriteSheet( {"images": [queue.getResult(ssMetadata[i].name)], "frames": ssMetadata[i].frames} )
        }
        exportRoot = new lib.Easter_cake2();
        window.stage2 = new lib.Stage(canvas);
        //Registers the "tick" event listener.
        fnStartAnimation = function() {
            window.stage2.addChild(exportRoot);
            createjs.Ticker.setFPS(lib.properties.fps);
            createjs.Ticker.addEventListener("tick", window.stage2);
        }
        //Code to support hidpi screens and responsive scaling.
        function makeResponsive(isResp, respDim, isScale, scaleType) {
            var lastW, lastH, lastS=1;
            window.addEventListener('resize', resizeCanvas);
            resizeCanvas();
            function resizeCanvas() {
                var w = lib.properties.width, h = lib.properties.height;
                var iw = window.innerWidth, ih=window.innerHeight;
                var pRatio = window.devicePixelRatio || 1, xRatio=iw/w, yRatio=ih/h, sRatio=1;
                if(isResp) {
                    if((respDim=='width'&&lastW==iw) || (respDim=='height'&&lastH==ih)) {
                        sRatio = lastS;
                    }
                    else if(!isScale) {
                        if(iw<w || ih<h)
                            sRatio = Math.min(xRatio, yRatio);
                    }
                    else if(scaleType==1) {
                        sRatio = Math.min(xRatio, yRatio);
                    }
                    else if(scaleType==2) {
                        sRatio = Math.max(xRatio, yRatio);
                    }
                }
                canvas.width = w*pRatio*sRatio;
                canvas.height = h*pRatio*sRatio;
                canvas.style.width = dom_overlay_container.style.width = anim_container.style.width =  w*sRatio+'px';
                canvas.style.height = anim_container.style.height = dom_overlay_container.style.height = h*sRatio+'px';
                window.stage2.scaleX = pRatio*sRatio;
                window.stage2.scaleY = pRatio*sRatio;
                lastW = iw; lastH = ih; lastS = sRatio;
                window.stage2.tickOnUpdate = false;
                window.stage2.update();
                window.stage2.tickOnUpdate = true;
            }
        }
        makeResponsive(true,'both',true,1);
        AdobeAn.compositionLoaded(lib.properties.id);
        fnStartAnimation();
    }

    init();
});