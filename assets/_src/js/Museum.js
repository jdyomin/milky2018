(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [];


// symbols:



(lib.hand1 = function() {
	this.initialize(img.hand1);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,34,37);


(lib.hand1_1 = function() {
	this.initialize(img.hand1_1);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,159,92);


(lib.pack = function() {
	this.initialize(img.pack);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,448,476);


(lib.paint1 = function() {
	this.initialize(img.paint1);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,80,69);


(lib.paint2 = function() {
	this.initialize(img.paint2);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,151,126);


(lib.paint3 = function() {
	this.initialize(img.paint3);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,101,34);


(lib.paint4 = function() {
	this.initialize(img.paint4);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,60,106);


(lib.paint5 = function() {
	this.initialize(img.paint5);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,68,155);


(lib.paint6 = function() {
	this.initialize(img.paint6);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,103,107);


(lib.paint7 = function() {
	this.initialize(img.paint7);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,97,101);


(lib.ramka = function() {
	this.initialize(img.ramka);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,427,433);// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.Symbol11 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.paint6();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol11, new cjs.Rectangle(0,0,103,107), null);


(lib.Symbol10 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.paint5();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol10, new cjs.Rectangle(0,0,68,155), null);


(lib.Symbol9 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.paint4();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol9, new cjs.Rectangle(0,0,60,106), null);


(lib.Symbol8 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.paint3();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol8, new cjs.Rectangle(0,0,101,34), null);


(lib.Symbol7 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.paint2();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol7, new cjs.Rectangle(0,0,151,126), null);


(lib.Symbol6 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.paint1();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol6, new cjs.Rectangle(0,0,80,69), null);


(lib.Symbol5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#1B1815").ss(5.5,1,1).p("AAJhZQAKBHgKAyQgEATgGARAgDBHQgEAKgGAJ");
	this.shape.setTransform(1.4,9);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#1B1815").ss(5.5,1,1).p("AgNBbQAGgJAFgKAgBBFQAGgRAEgUQAKgxgLhJ");
	this.shape_1.setTransform(1.4,9);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#1B1815").ss(5.5,1,1).p("AgNBcQAGgJAFgKAgBBGQAGgRAEgUQAKgxgMhL");
	this.shape_2.setTransform(1.4,8.9);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#1B1815").ss(5.5,1,1).p("AgNBdQAGgKAFgKAgBBGQAGgQAEgUQAKgygOhM");
	this.shape_3.setTransform(1.4,8.8);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#1B1815").ss(5.5,1,1).p("AgMBdQAGgJAEgKAAABHQAGgRAEgTQAKgygPhN");
	this.shape_4.setTransform(1.4,8.7);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#1B1815").ss(5.5,1,1).p("AADhdQARBPgKAxQgEAUgGARAgCBLQgFAKgFAJ");
	this.shape_5.setTransform(1.3,8.7);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#1B1815").ss(5.5,1,1).p("AgNBeQAGgKAEgKAgBBHQAGgRAEgTQAKgygJhO");
	this.shape_6.setTransform(1.4,8.7);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#1B1815").ss(5.5,1,1).p("AgPBdQAGgJAEgKAgDBHQAGgRAEgTQAKgygBhN");
	this.shape_7.setTransform(1.6,8.7);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#1B1815").ss(5.5,1,1).p("AgTBdQAGgJAEgKAgHBHQAHgRADgUQAKgxAHhN");
	this.shape_8.setTransform(2,8.8);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#1B1815").ss(5.5,1,1).p("AgXBdQAGgKAFgKAgLBGQAHgQAEgUQAJgyAPhM");
	this.shape_9.setTransform(2.4,8.8);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#1B1815").ss(5.5,1,1).p("AgbBcQAGgJAFgKAgPBGQAHgRAEgTQAJgyAXhL");
	this.shape_10.setTransform(2.8,8.8);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#1B1815").ss(5.5,1,1).p("AAghbQgfBLgJAxQgEAUgHARAgUBJQgFAKgGAJ");
	this.shape_11.setTransform(3.2,8.9);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#1B1815").ss(5.5,1,1).p("AgcBcQAGgKAEgKAgQBGQAHgRAEgUQAJgyAZhK");
	this.shape_12.setTransform(2.9,8.9);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#1B1815").ss(5.5,1,1).p("AgaBbQAGgJAFgKAgOBFQAHgRAEgTQAJgyAVhJ");
	this.shape_13.setTransform(2.7,8.9);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#1B1815").ss(5.5,1,1).p("AgXBbQAGgJAEgKAgLBFQAHgRAEgTQAJgyAPhJ");
	this.shape_14.setTransform(2.4,8.9);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#1B1815").ss(5.5,1,1).p("AgVBbQAGgJAFgKAgJBFQAHgRADgUQAKgxALhJ");
	this.shape_15.setTransform(2.2,9);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#1B1815").ss(5.5,1,1).p("AgSBbQAGgJAFgKAgGBFQAGgRAEgUQAKgxAFhJ");
	this.shape_16.setTransform(1.9,9);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#1B1815").ss(5.5,1,1).p("AgQBbQAHgKAEgKAgEBFQAHgRADgUQALgxAAhJ");
	this.shape_17.setTransform(1.7,9);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#1B1815").ss(5.5,1,1).p("AgOBbQAGgKAFgKAgCBFQAGgRAEgUQAKgxgFhJ");
	this.shape_18.setTransform(1.5,9);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#1B1815").ss(5.5,1,1).p("AgDBHQgEAKgGAJAAJhZQAKBHgKAyQgEATgGAR");
	this.shape_19.setTransform(1.4,9);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#1B1815").ss(5.5,1,1).p("AgCBLQgFAKgFAJAADhdQARBPgKAxQgEAUgGAR");
	this.shape_20.setTransform(1.3,8.7);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#1B1815").ss(5.5,1,1).p("AgUBJQgFAKgGAJAAghbQgfBLgJAxQgEAUgHAR");
	this.shape_21.setTransform(3.2,8.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape_19}]},2).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).wait(2));

	// Layer 2
	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#1B1815").ss(5.5,1,1).p("AA3gKQhNglggBG");
	this.shape_22.setTransform(7.8,9.9);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#1B1815").ss(5.5,1,1).p("Ag2AXQAghGBNAl");
	this.shape_23.setTransform(7.8,9.9);
	this.shape_23._off = true;

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#1B1815").ss(5.5,1,1).p("Ag0AWQAghGBJAq");
	this.shape_24.setTransform(7.6,10);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f().s("#1B1815").ss(5.5,1,1).p("AgyAWQAghHBGAv");
	this.shape_25.setTransform(7.5,10);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f().s("#1B1815").ss(5.5,1,1).p("AgxAVQAghHBDAz");
	this.shape_26.setTransform(7.3,10.1);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f().s("#1B1815").ss(5.5,1,1).p("AgvAUQAghHA/A4");
	this.shape_27.setTransform(7.1,10.2);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f().s("#1B1815").ss(5.5,1,1).p("AgtATQAghGA7A8");
	this.shape_28.setTransform(6.9,10.3);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f().s("#1B1815").ss(5.5,1,1).p("AAsANQg3hBggBG");
	this.shape_29.setTransform(6.7,10.4);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f().s("#1B1815").ss(5.5,1,1).p("AgtATQAhhGA6A+");
	this.shape_30.setTransform(6.9,10.3);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f().s("#1B1815").ss(5.5,1,1).p("AguAUQAghHA9A7");
	this.shape_31.setTransform(7,10.2);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f().s("#1B1815").ss(5.5,1,1).p("AgvAUQAghGA/A2");
	this.shape_32.setTransform(7.1,10.2);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f().s("#1B1815").ss(5.5,1,1).p("AgyAVQAghGBFAw");
	this.shape_33.setTransform(7.4,10.1);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f().s("#1B1815").ss(5.5,1,1).p("Ag0AWQAghHBJAt");
	this.shape_34.setTransform(7.6,10);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f().s("#1B1815").ss(5.5,1,1).p("Ag1AXQAghHBLAp");
	this.shape_35.setTransform(7.7,9.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_22}]}).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_27}]},1).to({state:[{t:this.shape_28}]},1).to({state:[{t:this.shape_29}]},1).to({state:[{t:this.shape_30}]},1).to({state:[{t:this.shape_31}]},1).to({state:[{t:this.shape_32}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_33}]},1).to({state:[{t:this.shape_34}]},1).to({state:[{t:this.shape_35}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_22}]},2).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_27}]},1).to({state:[{t:this.shape_28}]},1).to({state:[{t:this.shape_29}]},1).to({state:[{t:this.shape_30}]},1).to({state:[{t:this.shape_31}]},1).to({state:[{t:this.shape_32}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_33}]},1).to({state:[{t:this.shape_34}]},1).to({state:[{t:this.shape_35}]},1).to({state:[{t:this.shape_22}]},1).wait(2));
	this.timeline.addTween(cjs.Tween.get(this.shape_22).to({_off:true},1).wait(4).to({_off:false},0).to({_off:true},1).wait(13).to({_off:false},0).wait(2).to({_off:true},1).wait(4).to({_off:false},0).to({_off:true},1).wait(13).to({_off:false},0).wait(2));
	this.timeline.addTween(cjs.Tween.get(this.shape_23).wait(1).to({_off:false},0).wait(3).to({_off:true},1).wait(17).to({_off:false},0).wait(3).to({_off:true},1).wait(16));

	// Layer 3
	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f().s("#1B1815").ss(5.5,1,1).p("AA1ATQhIhLghBI");
	this.shape_36.setTransform(6.5,14.2);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f().s("#1B1815").ss(5.5,1,1).p("Ag0AQQAhhIBIBL");
	this.shape_37.setTransform(6.5,14.2);
	this.shape_37._off = true;

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f().s("#1B1815").ss(5.5,1,1).p("AgyAQQAihIBDBM");
	this.shape_38.setTransform(6.3,14.2);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f().s("#1B1815").ss(5.5,1,1).p("AgvAPQAihIA9BN");
	this.shape_39.setTransform(6,14.3);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f().s("#1B1815").ss(5.5,1,1).p("AgsAPQAhhIA4BN");
	this.shape_40.setTransform(5.7,14.3);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f().s("#1B1815").ss(5.5,1,1).p("AgqAPQAihIAzBO");
	this.shape_41.setTransform(5.5,14.3);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f().s("#1B1815").ss(5.5,1,1).p("AgnAOQAihIAtBP");
	this.shape_42.setTransform(5.2,14.4);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f().s("#1B1815").ss(5.5,1,1).p("AAmAVQgphPghBI");
	this.shape_43.setTransform(5,14.4);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f().s("#1B1815").ss(5.5,1,1).p("AgmAOQAhhIAtBP");
	this.shape_44.setTransform(5.2,14.4);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f().s("#1B1815").ss(5.5,1,1).p("AgoAOQAhhIAxBP");
	this.shape_45.setTransform(5.4,14.4);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.f().s("#1B1815").ss(5.5,1,1).p("AgqAPQAhhIA0BO");
	this.shape_46.setTransform(5.5,14.3);

	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.f().s("#1B1815").ss(5.5,1,1).p("AguAPQAhhIA8BN");
	this.shape_47.setTransform(5.9,14.3);

	this.shape_48 = new cjs.Shape();
	this.shape_48.graphics.f().s("#1B1815").ss(5.5,1,1).p("AgwAQQAhhIBABM");
	this.shape_48.setTransform(6.1,14.2);

	this.shape_49 = new cjs.Shape();
	this.shape_49.graphics.f().s("#1B1815").ss(5.5,1,1).p("AgyAQQAhhIBEBM");
	this.shape_49.setTransform(6.3,14.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_36}]}).to({state:[{t:this.shape_37}]},1).to({state:[{t:this.shape_37}]},1).to({state:[{t:this.shape_37}]},1).to({state:[{t:this.shape_37}]},1).to({state:[{t:this.shape_36}]},1).to({state:[{t:this.shape_38}]},1).to({state:[{t:this.shape_39}]},1).to({state:[{t:this.shape_40}]},1).to({state:[{t:this.shape_41}]},1).to({state:[{t:this.shape_42}]},1).to({state:[{t:this.shape_43}]},1).to({state:[{t:this.shape_44}]},1).to({state:[{t:this.shape_45}]},1).to({state:[{t:this.shape_46}]},1).to({state:[{t:this.shape_40}]},1).to({state:[{t:this.shape_47}]},1).to({state:[{t:this.shape_48}]},1).to({state:[{t:this.shape_49}]},1).to({state:[{t:this.shape_36}]},1).to({state:[{t:this.shape_36}]},2).to({state:[{t:this.shape_37}]},1).to({state:[{t:this.shape_37}]},1).to({state:[{t:this.shape_37}]},1).to({state:[{t:this.shape_37}]},1).to({state:[{t:this.shape_36}]},1).to({state:[{t:this.shape_38}]},1).to({state:[{t:this.shape_39}]},1).to({state:[{t:this.shape_40}]},1).to({state:[{t:this.shape_41}]},1).to({state:[{t:this.shape_42}]},1).to({state:[{t:this.shape_43}]},1).to({state:[{t:this.shape_44}]},1).to({state:[{t:this.shape_45}]},1).to({state:[{t:this.shape_46}]},1).to({state:[{t:this.shape_40}]},1).to({state:[{t:this.shape_47}]},1).to({state:[{t:this.shape_48}]},1).to({state:[{t:this.shape_49}]},1).to({state:[{t:this.shape_36}]},1).wait(2));
	this.timeline.addTween(cjs.Tween.get(this.shape_36).to({_off:true},1).wait(4).to({_off:false},0).to({_off:true},1).wait(13).to({_off:false},0).wait(2).to({_off:true},1).wait(4).to({_off:false},0).to({_off:true},1).wait(13).to({_off:false},0).wait(2));
	this.timeline.addTween(cjs.Tween.get(this.shape_37).wait(1).to({_off:false},0).wait(3).to({_off:true},1).wait(17).to({_off:false},0).wait(3).to({_off:true},1).wait(16));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-2.7,-2.7,18.9,23.6);


(lib.Symbol4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.paint7();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol4, new cjs.Rectangle(0,0,97,101), null);


(lib.Symbol3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.hand1_1();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol3, new cjs.Rectangle(0,0,159,92), null);


(lib.Symbol2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#F51431").s().p("AAAAqQgRAAgNgMQgMgMAAgSQAAgQAMgNQANgNARAAIADAAQAPABALAMQANANAAAQQAAASgNAMQgLAMgPAAIgDAAg");
	this.shape.setTransform(4.3,4.3);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol2, new cjs.Rectangle(0,0,8.5,8.5), null);


(lib.Symbol1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.hand1();
	this.instance.parent = this;
	this.instance.setTransform(0,187);

	this.instance_1 = new lib.ramka();
	this.instance_1.parent = this;
	this.instance_1.setTransform(9,0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_1},{t:this.instance}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol1, new cjs.Rectangle(0,0,436,433), null);


(lib.Symbol19 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.Symbol11();
	this.instance.parent = this;
	this.instance.setTransform(51.5,53.5,2.398,2.398,0,0,0,51.5,53.5);
	this.instance.alpha = 0;
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(16).to({_off:false},0).to({scaleX:0.95,scaleY:0.95,alpha:1},5,cjs.Ease.get(-1)).to({scaleX:1,scaleY:1},2,cjs.Ease.get(1)).to({y:63.5},42).to({y:70.5,alpha:0},25).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = null;


(lib.Symbol18 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.Symbol10();
	this.instance.parent = this;
	this.instance.setTransform(73.8,77.5,2.212,2.212,0,0,0,33.9,77.5);
	this.instance.alpha = 0;
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(13).to({_off:false},0).to({regX:34,scaleX:0.95,scaleY:0.95,x:34,alpha:1},5,cjs.Ease.get(-1)).to({scaleX:1,scaleY:1},2,cjs.Ease.get(1)).to({y:87.5},42).to({y:94.5,alpha:0},25).to({_off:true},1).wait(3));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = null;


(lib.Symbol17 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.Symbol9();
	this.instance.parent = this;
	this.instance.setTransform(30,53,2.396,2.396,0,0,0,30,53);
	this.instance.alpha = 0;
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(14).to({_off:false},0).to({scaleX:0.95,scaleY:0.95,alpha:1},5,cjs.Ease.get(-1)).to({scaleX:1,scaleY:1},2,cjs.Ease.get(1)).to({y:63},42).to({y:70,alpha:0},25).to({_off:true},1).wait(2));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = null;


(lib.Symbol16 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.Symbol8();
	this.instance.parent = this;
	this.instance.setTransform(50.5,17,2.465,2.465,0,0,0,50.5,17);
	this.instance.alpha = 0;
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(9).to({_off:false},0).to({scaleX:0.95,scaleY:0.95,alpha:1},5,cjs.Ease.get(-1)).to({scaleX:1,scaleY:1},2,cjs.Ease.get(1)).to({y:20},42).to({y:24,alpha:0},25).to({_off:true},1).wait(7));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = null;


(lib.Symbol15 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.Symbol7();
	this.instance.parent = this;
	this.instance.setTransform(75.5,53,2.086,2.086,0,0,0,75.5,63);
	this.instance.alpha = 0;
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(6).to({_off:false},0).to({scaleX:0.95,scaleY:0.95,alpha:1},5,cjs.Ease.get(-1)).to({scaleX:1,scaleY:1},2,cjs.Ease.get(1)).to({y:61},42).to({y:67,alpha:0},25).to({_off:true},1).wait(10));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = null;


(lib.Symbol14 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.Symbol6();
	this.instance.parent = this;
	this.instance.setTransform(52,34.5,3.786,3.786,0,0,0,52,34.5);
	this.instance.alpha = 0;
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(4).to({_off:false},0).to({scaleX:0.95,scaleY:0.95,alpha:1},5,cjs.Ease.get(-1)).to({scaleX:1,scaleY:1},2,cjs.Ease.get(1)).to({y:44.5},42).to({y:51.5,alpha:0},25).to({_off:true},1).wait(12));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = null;


(lib.Symbol13 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.Symbol4();
	this.instance.parent = this;
	this.instance.setTransform(48.5,50.5,2.781,2.781,0,0,0,48.5,50.5);
	this.instance.alpha = 0;

	this.timeline.addTween(cjs.Tween.get(this.instance).to({scaleX:0.95,scaleY:0.95,alpha:1},5,cjs.Ease.get(-1)).to({scaleX:1,scaleY:1},2,cjs.Ease.get(1)).to({y:60.5},42).to({y:67.5,alpha:0},25).to({_off:true},1).wait(16));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-86.4,-89.9,269.8,280.9);


(lib.Symbol12 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Symbol 4
	this.instance = new lib.Symbol13();
	this.instance.parent = this;
	this.instance.setTransform(59.5,110.5,1,1,0,0,0,48.5,50.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	// Symbol 6
	this.instance_1 = new lib.Symbol14();
	this.instance_1.parent = this;
	this.instance_1.setTransform(60,35.5,1,1,0,0,0,40,34.5);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(1));

	// Symbol 7
	this.instance_2 = new lib.Symbol15();
	this.instance_2.parent = this;
	this.instance_2.setTransform(93.5,154,1,1,0,0,0,75.5,63);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(1));

	// Symbol 8
	this.instance_3 = new lib.Symbol16();
	this.instance_3.parent = this;
	this.instance_3.setTransform(116.5,237,1,1,0,0,0,50.5,17);

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(1));

	// Symbol 9
	this.instance_4 = new lib.Symbol17();
	this.instance_4.parent = this;
	this.instance_4.setTransform(103,53,1,1,0,0,0,30,53);

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(1));

	// Symbol 10
	this.instance_5 = new lib.Symbol18();
	this.instance_5.parent = this;
	this.instance_5.setTransform(151,138.5,1,1,0,0,0,34,77.5);

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(1));

	// Symbol 11
	this.instance_6 = new lib.Symbol19();
	this.instance_6.parent = this;
	this.instance_6.setTransform(51.5,97.5,1,1,0,0,0,51.5,53.5);

	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol12, new cjs.Rectangle(-75.4,-29.9,269.8,280.9), null);


// stage content:
(lib.Museum = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 11
	this.instance = new lib.Symbol5();
	this.instance.parent = this;
	this.instance.setTransform(983.6,331.6,2.167,2.167,10,0,0,0,18.1);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({regY:18,rotation:-0.8,x:976.7,y:320.3},5).to({regY:18.1,rotation:41.9,x:991.1,y:354.2},6).to({rotation:10,x:983.6,y:331.6},8).wait(2).to({regY:18,rotation:-0.8,x:976.7,y:320.3},5).to({regY:18.1,rotation:41.9,x:991.1,y:354.2},6).to({rotation:10,x:983.6,y:331.6},8).wait(2));

	// Layer 12
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#1B1815").ss(12,1,1).p("Aq5D5QPYgtGbnE");
	this.shape.setTransform(913.7,356.3);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#1B1815").ss(12,1,1).p("Aq0EFQPWg+GTnM");
	this.shape_1.setTransform(913.1,354.9);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#1B1815").ss(12,1,1).p("AqvESQPVhQGKnT");
	this.shape_2.setTransform(912.5,353.5);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#1B1815").ss(12,1,1).p("AqqEeQPThiGCna");
	this.shape_3.setTransform(912,352.1);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#1B1815").ss(12,1,1).p("AqlErQPRh0F6nh");
	this.shape_4.setTransform(911.4,350.7);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#1B1815").ss(12,1,1).p("AqgE4QPQiGFxnp");
	this.shape_5.setTransform(910.8,349.3);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#1B1815").ss(12,1,1).p("AqqEbQPRhwGEnF");
	this.shape_6.setTransform(911.9,352.4);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#1B1815").ss(12,1,1).p("Aq0D9QPThZGWmg");
	this.shape_7.setTransform(913,355.5);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#1B1815").ss(12,1,1).p("Aq/DgQPVhDGql8");
	this.shape_8.setTransform(914.2,358.6);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#1B1815").ss(12,1,1).p("ArJDDQPWgtG9lY");
	this.shape_9.setTransform(915.3,361.6);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#1B1815").ss(12,1,1).p("ArTCmQPYgYHPkz");
	this.shape_10.setTransform(916.4,364.7);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#1B1815").ss(12,1,1).p("ArdCJQPZgCHikP");
	this.shape_11.setTransform(917.5,367.8);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#1B1815").ss(12,1,1).p("ArYCXQPZgHHZkm");
	this.shape_12.setTransform(917.1,366.3);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#1B1815").ss(12,1,1).p("ArUClQPZgMHQk9");
	this.shape_13.setTransform(916.6,364.9);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#1B1815").ss(12,1,1).p("ArPCzQPYgSHIlT");
	this.shape_14.setTransform(916.1,363.5);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#1B1815").ss(12,1,1).p("ArLDBQPZgXG+lq");
	this.shape_15.setTransform(915.7,362);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#1B1815").ss(12,1,1).p("ArGDPQPYgcG1mB");
	this.shape_16.setTransform(915.2,360.6);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#1B1815").ss(12,1,1).p("ArCDdQPYgiGtmX");
	this.shape_17.setTransform(914.7,359.2);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#1B1815").ss(12,1,1).p("Aq+DrQPZgnGkmu");
	this.shape_18.setTransform(914.2,357.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},2).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape}]},1).wait(2));

	// Layer 7
	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#1B1815").ss(11,1,1).p("AkghmQBIDTDigGQBdgDBOgvQBQgwAchM");
	this.shape_19.setTransform(739.9,309.6);

	this.timeline.addTween(cjs.Tween.get(this.shape_19).wait(42));

	// Layer 10
	this.instance_1 = new lib.Symbol2();
	this.instance_1.parent = this;
	this.instance_1.setTransform(720.4,313.7,2.175,2.175,0,46.1,51.9,5.4,7.5);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(1).to({regX:4.3,regY:4.3,scaleX:2.17,scaleY:2.18,skewX:45.7,skewY:51.5,x:724.1,y:307.2},0).wait(1).to({scaleX:2.17,scaleY:2.18,skewX:44.8,skewY:50.4,x:724.6,y:307.6},0).wait(1).to({scaleX:2.17,scaleY:2.18,skewX:43.2,skewY:48.6,x:725.6,y:308.4},0).wait(1).to({scaleX:2.16,scaleY:2.19,skewX:40.7,skewY:45.8,x:727.1,y:309.3},0).wait(1).to({scaleX:2.15,scaleY:2.2,skewX:37.3,skewY:41.9,x:729.5,y:310.5},0).wait(1).to({scaleX:2.14,scaleY:2.21,skewX:32.8,skewY:36.8,x:732.8,y:311.6},0).wait(1).to({scaleX:2.13,scaleY:2.22,skewX:27.4,skewY:30.6,x:737,y:312.2},0).wait(1).to({scaleX:2.11,scaleY:2.24,skewX:21.5,skewY:23.9,x:741.6,y:312.3},0).wait(1).to({scaleX:2.1,scaleY:2.25,skewX:16,skewY:17.6,x:745.5,y:311.8},0).wait(1).to({scaleX:2.08,scaleY:2.26,skewX:11.4,skewY:12.4,x:748.6,y:311},0).wait(1).to({scaleX:2.07,scaleY:2.27,skewX:8,skewY:8.6,x:750.8,y:310.2},0).wait(1).to({scaleX:2.07,scaleY:2.28,skewX:5.8,skewY:6,x:752.3,y:309.5},0).wait(1).to({scaleX:2.06,scaleY:2.28,skewX:4.5,skewY:4.6,x:753.1,y:309.1},0).wait(1).to({regX:5.4,regY:7.5,scaleX:2.06,scaleY:2.28,rotation:4,skewX:0,skewY:0,x:755.2,y:316.3},0).wait(7).to({regX:4.3,regY:4.3,scaleX:2.06,scaleY:2.28,rotation:0,skewX:4.6,skewY:4.7,x:753,y:309.1},0).wait(1).to({scaleX:2.07,scaleY:2.28,skewX:6,skewY:6.2,x:752.1,y:309.6},0).wait(1).to({scaleX:2.07,scaleY:2.27,skewX:8.3,skewY:8.8,x:750.4,y:310.3},0).wait(1).to({scaleX:2.08,scaleY:2.26,skewX:11.5,skewY:12.6,x:748,y:311.1},0).wait(1).to({scaleX:2.09,scaleY:2.25,skewX:15.7,skewY:17.4,x:744.8,y:311.9},0).wait(1).to({scaleX:2.11,scaleY:2.24,skewX:20.7,skewY:23,x:740.9,y:312.2},0).wait(1).to({scaleX:2.12,scaleY:2.23,skewX:26.1,skewY:29.1,x:736.9},0).wait(1).to({scaleX:2.13,scaleY:2.21,skewX:31.2,skewY:35,x:733.5,y:311.7},0).wait(1).to({scaleX:2.15,scaleY:2.2,skewX:35.9,skewY:40.2,x:730.5,y:310.8},0).wait(1).to({scaleX:2.16,scaleY:2.19,skewX:39.6,skewY:44.5,x:728,y:309.8},0).wait(1).to({scaleX:2.17,scaleY:2.18,skewX:42.5,skewY:47.7,x:726.1,y:308.7},0).wait(1).to({scaleX:2.17,scaleY:2.18,skewX:44.4,skewY:50,x:724.9,y:307.8},0).wait(1).to({scaleX:2.17,scaleY:2.18,skewX:45.6,skewY:51.3,x:724.2,y:307.2},0).wait(1).to({regX:5.4,regY:7.5,scaleX:2.17,scaleY:2.17,skewX:46.1,skewY:51.9,x:720.3,y:313.8},0).wait(8));

	// Layer 4
	this.instance_2 = new lib.Symbol1();
	this.instance_2.parent = this;
	this.instance_2.setTransform(549.6,307.6,1,1,0,0,0,13.6,224.6);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).to({rotation:-3.2,x:559.2,y:296.2},10,cjs.Ease.get(-1)).to({rotation:0,x:549.6,y:307.6},10,cjs.Ease.get(1)).wait(1).to({rotation:-3.2,x:559.2,y:296.2},10,cjs.Ease.get(-1)).to({rotation:0,x:549.6,y:307.6},10,cjs.Ease.get(1)).wait(1));

	// Layer 19
	this.instance_3 = new lib.Symbol12();
	this.instance_3.parent = this;
	this.instance_3.setTransform(762.5,481,1,1,0,0,0,92.5,127);

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(42));

	// Layer 3
	this.instance_4 = new lib.pack();
	this.instance_4.parent = this;
	this.instance_4.setTransform(580,173);

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(42));

	// Layer 5
	this.instance_5 = new lib.Symbol3();
	this.instance_5.parent = this;
	this.instance_5.setTransform(682.5,370.5,1,1,0,0,0,155.5,86.5);

	this.timeline.addTween(cjs.Tween.get(this.instance_5).to({rotation:5.7},10,cjs.Ease.get(-1)).to({rotation:0},10,cjs.Ease.get(1)).wait(1).to({rotation:5.7},10,cjs.Ease.get(-1)).to({rotation:0},10,cjs.Ease.get(1)).wait(1));

	// Layer 6
	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.rf(["#0E94D1","rgba(14,148,209,0)"],[0.071,1],-0.1,0,0,-0.1,0,300.6).s().p("Egg6Ag8QtptqAAzSQAAzRNptpQNptoTRgBQTSABNoNoQNpNpAATRQAATStpNqQtoNnzSAAQzRAAtptng");
	this.shape_20.setTransform(715,306.8);

	this.timeline.addTween(cjs.Tween.get(this.shape_20).wait(42));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(540,340,1080,680);
// library properties:
lib.properties = {
	id: 'A21F7ED90511814B82538375916BEDC5',
	width: 1080,
	height: 680,
	fps: 30,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: [
		{src:"assets/images/hand10.png", id:"hand1"},
		{src:"assets/images/hand1_1.png", id:"hand1_1"},
		{src:"assets/images/pack10.png", id:"pack"},
		{src:"assets/images/paint1.png", id:"paint1"},
		{src:"assets/images/paint2.png", id:"paint2"},
		{src:"assets/images/paint3.png", id:"paint3"},
		{src:"assets/images/paint4.png", id:"paint4"},
		{src:"assets/images/paint5.png", id:"paint5"},
		{src:"assets/images/paint6.png", id:"paint6"},
		{src:"assets/images/paint7.png", id:"paint7"},
		{src:"assets/images/ramka.png", id:"ramka"}
	],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['A21F7ED90511814B82538375916BEDC5'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}



})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;

$(function () {
    var canvas, stage, exportRoot, anim_container, dom_overlay_container, fnStartAnimation;
    function init() {
        canvas = document.getElementById("canvas10");
        anim_container = document.getElementById("animation_container10");
        dom_overlay_container = document.getElementById("dom_overlay_container10");
        var comp=AdobeAn.getComposition("A21F7ED90511814B82538375916BEDC5");
        var lib=comp.getLibrary();
        var loader = new createjs.LoadQueue(false);
        loader.addEventListener("fileload", function(evt){handleFileLoad(evt,comp)});
        loader.addEventListener("complete", function(evt){handleComplete(evt,comp)});
        var lib=comp.getLibrary();
        loader.loadManifest(lib.properties.manifest);
    }
    function handleFileLoad(evt, comp) {
        var images=comp.getImages();
        if (evt && (evt.item.type == "image")) { images[evt.item.id] = evt.result; }
    }
    function handleComplete(evt,comp) {
        //This function is always called, irrespective of the content. You can use the variable "stage" after it is created in token create_stage.
        var lib=comp.getLibrary();
        var ss=comp.getSpriteSheet();
        var queue = evt.target;
        var ssMetadata = lib.ssMetadata;
        for(i=0; i<ssMetadata.length; i++) {
            ss[ssMetadata[i].name] = new createjs.SpriteSheet( {"images": [queue.getResult(ssMetadata[i].name)], "frames": ssMetadata[i].frames} )
        }
        exportRoot = new lib.Museum();
        stage = new lib.Stage(canvas);
        //Registers the "tick" event listener.
        fnStartAnimation = function() {
            stage.addChild(exportRoot);
            createjs.Ticker.setFPS(lib.properties.fps);
            createjs.Ticker.addEventListener("tick", stage);
        }
        //Code to support hidpi screens and responsive scaling.
        function makeResponsive(isResp, respDim, isScale, scaleType) {
            var lastW, lastH, lastS=1;
            window.addEventListener('resize', resizeCanvas);
            resizeCanvas();
            function resizeCanvas() {
                var w = lib.properties.width, h = lib.properties.height;
                var iw = window.innerWidth, ih=window.innerHeight;
                var pRatio = window.devicePixelRatio || 1, xRatio=iw/w, yRatio=ih/h, sRatio=1;
                if(isResp) {
                    if((respDim=='width'&&lastW==iw) || (respDim=='height'&&lastH==ih)) {
                        sRatio = lastS;
                    }
                    else if(!isScale) {
                        if(iw<w || ih<h)
                            sRatio = Math.min(xRatio, yRatio);
                    }
                    else if(scaleType==1) {
                        sRatio = Math.min(xRatio, yRatio);
                    }
                    else if(scaleType==2) {
                        sRatio = Math.max(xRatio, yRatio);
                    }
                }
                canvas.width = w*pRatio*sRatio;
                canvas.height = h*pRatio*sRatio;
                canvas.style.width = dom_overlay_container.style.width = anim_container.style.width =  w*sRatio+'px';
                canvas.style.height = anim_container.style.height = dom_overlay_container.style.height = h*sRatio+'px';
                stage.scaleX = pRatio*sRatio;
                stage.scaleY = pRatio*sRatio;
                lastW = iw; lastH = ih; lastS = sRatio;
                stage.tickOnUpdate = false;
                stage.update();
                stage.tickOnUpdate = true;
            }
        }
        makeResponsive(true,'both',true,2);
        AdobeAn.compositionLoaded(lib.properties.id);
        fnStartAnimation();
    }
    init();
});