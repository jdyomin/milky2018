(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [];


// symbols:



(lib.bg = function() {
	this.initialize(img.bg);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,703,196);


(lib.flower = function() {
	this.initialize(img.flower);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,178,259);


(lib.leefs_1 = function() {
	this.initialize(img.leefs_1);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,228,209);


(lib.leefs_2 = function() {
	this.initialize(img.leefs_2);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,186,172);


(lib.pack = function() {
	this.initialize(img.pack);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,352,488);// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.Symbol13 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#16120C").ss(12.5,1,1).p("Ai1g6QALA5A0AfQAvAdA+gBQA8AAA0gbQA3gdAYgw");
	this.shape.setTransform(51.1,49.7);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#16120C").ss(12.5,1,1).p("Ai/g7QAVA4A0AgQAvAfA/AAQA8gBA0gdQA3geAhgy");
	this.shape_1.setTransform(51.1,49.9);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#16120C").ss(12.5,1,1).p("AjIg8QAfA3AyAhQAxAhA+AAQA8gBA1gfQA2gfApg0");
	this.shape_2.setTransform(51,50.1);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#16120C").ss(12.5,1,1).p("AjPg9QAmA2AyAjQAxAjA+gBQA8AAA1ghQA2ggAxg2");
	this.shape_3.setTransform(51,50.3);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#16120C").ss(12.5,1,1).p("AjVg+QAsA2AyAkQAxAjA+AAQA9gBA0ghQA3giA1g2");
	this.shape_4.setTransform(51,50.4);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#16120C").ss(12.5,1,1).p("AjZg/QAwA2AyAkQAxAlA+AAQA9gBA1gjQA2giA6g3");
	this.shape_5.setTransform(50.9,50.5);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#16120C").ss(12.5,1,1).p("Ajdg/QA1A1AxAlQAyAlA9AAQA9gBA1gjQA2gjA+g4");
	this.shape_6.setTransform(50.9,50.6);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#16120C").ss(12.5,1,1).p("AjehAQA2A1AxAmQAyAmA9gBQA9AAA1gkQA2gjBAg5");
	this.shape_7.setTransform(50.9,50.7);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#16120C").ss(12.5,1,1).p("AjfhAQA3A1AxAmQAyAmA9gBQA9AAA1gkQA2gjBAg5");
	this.shape_8.setTransform(50.9,50.7);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#16120C").ss(12.5,1,1).p("AjVg+QAsA2AyAkQAxAkA+gBQA9AAA1giQA2giA2g2");
	this.shape_9.setTransform(50.9,50.4);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#16120C").ss(12.5,1,1).p("AjNg9QAkA3AyAiQAxAiA+AAQA8gBA1ggQA2ggAvg1");
	this.shape_10.setTransform(51,50.2);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#16120C").ss(12.5,1,1).p("AjGg8QAdA3AyAhQAxAhA+AAQA8gBA1gfQA2geAng0");
	this.shape_11.setTransform(51,50.1);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#16120C").ss(12.5,1,1).p("AjAg7QAWA4A0AgQAvAfA/AAQA8gBA0gdQA3geAigy");
	this.shape_12.setTransform(51,49.9);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#16120C").ss(12.5,1,1).p("Ai7g7QARA5A0AfQAvAfA/gBQA8AAA0gdQA3geAdgw");
	this.shape_13.setTransform(51.1,49.8);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#16120C").ss(12.5,1,1).p("Ai4g6QAOA5A0AfQAvAdA+AAQA8gBA0gbQA3geAbgw");
	this.shape_14.setTransform(51.1,49.7);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#16120C").ss(12.5,1,1).p("Ai2g6QAMA5A0AfQAvAdA+AAQA8gBA0gbQA3gdAZgw");
	this.shape_15.setTransform(51.1,49.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},19).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_8}]},17).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape}]},1).wait(18));

	// Layer 3
	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#16120C").ss(12.5,1,1).p("AimA2QAShLA7gkQA7glBBAIQBBAIAnAuQAmAsgOBM");
	this.shape_16.setTransform(16.7,8.8);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#16120C").ss(12.5,1,1).p("AimA8QAQhTA6gnQA6goBBAIQBCAJAnAyQAoAxgMBS");
	this.shape_17.setTransform(16.8,7.6);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#16120C").ss(12.5,1,1).p("AimBAQAPhZA5gqQA5grBBAJQBCAKApA1QAoA0gKBZ");
	this.shape_18.setTransform(16.9,6.5);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#16120C").ss(12.5,1,1).p("AimBEQAOheA4gtQA5guBAAKQBDAKApA5QApA3gIBe");
	this.shape_19.setTransform(16.9,5.6);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#16120C").ss(12.5,1,1).p("AimBGQAMhiA4gvQA4gvBBAKQBDALAqA7QAqA6gIBi");
	this.shape_20.setTransform(17,4.9);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#16120C").ss(12.5,1,1).p("AimBJQALhmA4gwQA4gxBAAKQBDALArA9QAqA8gGBm");
	this.shape_21.setTransform(17,4.3);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#16120C").ss(12.5,1,1).p("AimBLQALhoA3gyQA3gyBBALQBCALAsA+QAqA+gFBo");
	this.shape_22.setTransform(17.1,3.9);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#16120C").ss(12.5,1,1).p("AinBMQALhqA3gyQA3gzBBALQBCAMAsA/QArA+gFBq");
	this.shape_23.setTransform(17.1,3.7);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#16120C").ss(12.5,1,1).p("AinBMQALhqA3gyQA3gzBBALQBCALAsBAQArA+gFBq");
	this.shape_24.setTransform(17.1,3.6);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f().s("#16120C").ss(12.5,1,1).p("AimBHQAMhjA4gvQA4gwBBALQBCAKArA8QApA6gHBj");
	this.shape_25.setTransform(17,4.8);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f().s("#16120C").ss(12.5,1,1).p("AimBDQAOhdA4gsQA5gtBBAKQBCAKApA3QApA3gJBd");
	this.shape_26.setTransform(16.9,5.9);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f().s("#16120C").ss(12.5,1,1).p("AimA/QAPhYA5gpQA6gqBBAJQBCAJAoA1QAoAzgKBY");
	this.shape_27.setTransform(16.9,6.8);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f().s("#16120C").ss(12.5,1,1).p("AimA8QAQhTA6goQA6goBBAJQBCAIAnAyQAoAxgMBU");
	this.shape_28.setTransform(16.8,7.5);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f().s("#16120C").ss(12.5,1,1).p("AimA5QARhPA7gmQA6gnBBAJQBBAIAnAwQAnAvgMBQ");
	this.shape_29.setTransform(16.8,8.1);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f().s("#16120C").ss(12.5,1,1).p("AimA4QAShOA7gkQA6gmBBAIQBCAJAmAuQAnAugNBN");
	this.shape_30.setTransform(16.7,8.5);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f().s("#16120C").ss(12.5,1,1).p("AimA3QAShMA7gkQA7glBBAIQBBAIAnAuQAmAtgOBL");
	this.shape_31.setTransform(16.7,8.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_16}]}).to({state:[{t:this.shape_16}]},19).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_24}]},17).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_27}]},1).to({state:[{t:this.shape_28}]},1).to({state:[{t:this.shape_29}]},1).to({state:[{t:this.shape_30}]},1).to({state:[{t:this.shape_31}]},1).to({state:[{t:this.shape_16}]},1).wait(18));

	// Layer 4
	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f().s("#16120C").ss(12.5,1,1).p("Ai5BBQAWhdBCgoQBBgpBHAJQBIAJArAyQAqAygNBe");
	this.shape_32.setTransform(81.1,11.6);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f().s("#16120C").ss(12.5,1,1).p("Ai5BHQAUhmBAgsQBAgtBHAKQBJAKAsA3QArA2gKBn");
	this.shape_33.setTransform(81.3,10.6);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f().s("#16120C").ss(12.5,1,1).p("Ai5BNQAShuA/gvQA/gxBIALQBJALAtA7QAsA6gIBv");
	this.shape_34.setTransform(81.5,9.8);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f().s("#16120C").ss(12.5,1,1).p("Ai5BRQAQh1A+gxQA/gzBHALQBJALAuA/QAuA+gGB1");
	this.shape_35.setTransform(81.6,9.1);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f().s("#16120C").ss(12.5,1,1).p("Ai5BVQAOh6A/g0QA9g1BIAMQBJALAvBCQAuBBgFB6");
	this.shape_36.setTransform(81.7,8.5);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f().s("#16120C").ss(12.5,1,1).p("Ai6BYQAOh+A9g2QA9g3BIAMQBKAMAvBEQAvBDgEB+");
	this.shape_37.setTransform(81.9,8);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f().s("#16120C").ss(12.5,1,1).p("Ai6BaQANiBA9g3QA9g5BIANQBJAMAwBFQAvBFgCCB");
	this.shape_38.setTransform(81.9,7.7);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f().s("#16120C").ss(12.5,1,1).p("Ai6BbQAMiDA9g4QA9g5BIANQBJAMAwBHQAwBFgCCD");
	this.shape_39.setTransform(82,7.5);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f().s("#16120C").ss(12.5,1,1).p("Ai6BbQAMiDA9g4QA9g6BIANQBJANAwBGQAwBGgCCE");
	this.shape_40.setTransform(82,7.5);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f().s("#16120C").ss(12.5,1,1).p("Ai5BVQAOh6A+g1QA+g1BIAMQBJAMAvBBQAuBCgFB6");
	this.shape_41.setTransform(81.7,8.4);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f().s("#16120C").ss(12.5,1,1).p("Ai5BQQAQhzA/gxQA/gyBHALQBJALAuA+QAtA9gGBz");
	this.shape_42.setTransform(81.6,9.3);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f().s("#16120C").ss(12.5,1,1).p("Ai5BLQAShsBAguQA/gwBIALQBJAKAsA7QAsA5gIBt");
	this.shape_43.setTransform(81.4,10);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f().s("#16120C").ss(12.5,1,1).p("Ai5BIQAUhnBAgsQBAgtBHAKQBJAKAsA3QArA3gKBn");
	this.shape_44.setTransform(81.3,10.5);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f().s("#16120C").ss(12.5,1,1).p("Ai5BFQAVhjBBgqQBAgrBHAJQBJAKArA1QArA0gLBk");
	this.shape_45.setTransform(81.2,11);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.f().s("#16120C").ss(12.5,1,1).p("Ai5BDQAWhgBBgpQBBgqBHAJQBIAJArA0QAqAzgLBg");
	this.shape_46.setTransform(81.2,11.3);

	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.f().s("#16120C").ss(12.5,1,1).p("Ai5BCQAWheBCgoQBBgqBHAJQBIAKArAyQAqAygNBf");
	this.shape_47.setTransform(81.1,11.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_32}]}).to({state:[{t:this.shape_32}]},19).to({state:[{t:this.shape_33}]},1).to({state:[{t:this.shape_34}]},1).to({state:[{t:this.shape_35}]},1).to({state:[{t:this.shape_36}]},1).to({state:[{t:this.shape_37}]},1).to({state:[{t:this.shape_38}]},1).to({state:[{t:this.shape_39}]},1).to({state:[{t:this.shape_40}]},1).to({state:[{t:this.shape_40}]},17).to({state:[{t:this.shape_41}]},1).to({state:[{t:this.shape_42}]},1).to({state:[{t:this.shape_43}]},1).to({state:[{t:this.shape_44}]},1).to({state:[{t:this.shape_45}]},1).to({state:[{t:this.shape_46}]},1).to({state:[{t:this.shape_47}]},1).to({state:[{t:this.shape_32}]},1).wait(18));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-6.2,-6.2,112.3,68);


(lib.Symbol12 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.leefs_2();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol12, new cjs.Rectangle(0,0,186,172), null);


(lib.Symbol10 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.leefs_1();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol10, new cjs.Rectangle(0,0,228,209), null);


(lib.Symbol7 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Symbol 4
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(12.5,1,1).p("AgPCYQBPjohWhH");
	this.shape.setTransform(21.9,15.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(12.5,1,1).p("AgWiXQBWBHhPDo");
	this.shape_1.setTransform(21.9,15.2);
	this.shape_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(19).to({_off:true},1).wait(3).to({_off:false},0).to({_off:true},1).wait(5).to({_off:false},0).wait(3).to({_off:true},1).wait(3).to({_off:false},0).to({_off:true},1).wait(5).to({_off:false},0).wait(28));
	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(20).to({_off:false},0).wait(2).to({_off:true},1).wait(1).to({_off:false},0).wait(4).to({_off:true},1).wait(4).to({_off:false},0).wait(2).to({_off:true},1).wait(1).to({_off:false},0).wait(4).to({_off:true},1).wait(28));

	// Symbol 5
	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(12.5,1,1).p("ABQBbQAskFjTB1");
	this.shape_2.setTransform(8.8,20.3);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(12.5,1,1).p("AhWgxQDUhvgxD1");
	this.shape_3.setTransform(9,20.9);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(12.5,1,1).p("AhVguQDWhpg2Dn");
	this.shape_4.setTransform(9.2,21.4);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(12.5,1,1).p("AhVgrQDXhlg4Dc");
	this.shape_5.setTransform(9.4,21.7);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(12.5,1,1).p("ABJBJQA7jVjYBi");
	this.shape_6.setTransform(9.5,22);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(12.5,1,1).p("AhVgtQDWhog2Dk");
	this.shape_7.setTransform(9.3,21.5);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(12.5,1,1).p("AhVgwQDUhtgyDw");
	this.shape_8.setTransform(9.1,21.1);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(12.5,1,1).p("AhWgyQDUhxgwD5");
	this.shape_9.setTransform(8.9,20.7);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(12.5,1,1).p("AhWg0QDThzguEA");
	this.shape_10.setTransform(8.8,20.5);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(12.5,1,1).p("AhWg1QDSh0gsED");
	this.shape_11.setTransform(8.8,20.3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2}]}).to({state:[{t:this.shape_2}]},19).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_2}]},3).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_2}]},1).wait(28));
	this.timeline.addTween(cjs.Tween.get(this.shape_2).wait(19).to({_off:true},1).wait(9).to({_off:false},0).wait(3).to({_off:true},1).wait(9).to({_off:false},0).wait(28));

	// Symbol 6
	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(12.5,1,1).p("AA1gVQgygGg2Ay");
	this.shape_12.setTransform(13.2,30.3);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(12.5,1,1).p("Ag2ARQA8gmAxAG");
	this.shape_13.setTransform(12.9,29.7);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(12.5,1,1).p("Ag4AMQBAgcAxAG");
	this.shape_14.setTransform(12.7,29.2);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(12.5,1,1).p("Ag6AIQBDgUAyAG");
	this.shape_15.setTransform(12.5,28.8);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(12.5,1,1).p("AA8gDQgxgGhGAP");
	this.shape_16.setTransform(12.4,28.5);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(12.5,1,1).p("Ag5ALQBBgaAyAH");
	this.shape_17.setTransform(12.6,29.1);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#000000").ss(12.5,1,1).p("Ag3APQA9giAyAG");
	this.shape_18.setTransform(12.8,29.5);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#000000").ss(12.5,1,1).p("Ag1ASQA6gpAxAH");
	this.shape_19.setTransform(13,29.9);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#000000").ss(12.5,1,1).p("Ag0AVQA4guAxAG");
	this.shape_20.setTransform(13.1,30.1);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#000000").ss(12.5,1,1).p("Ag0AWQA3gxAyAH");
	this.shape_21.setTransform(13.1,30.3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_12}]}).to({state:[{t:this.shape_12}]},19).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_12}]},3).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_12}]},1).wait(28));
	this.timeline.addTween(cjs.Tween.get(this.shape_12).wait(19).to({_off:true},1).wait(9).to({_off:false},0).wait(3).to({_off:true},1).wait(9).to({_off:false},0).wait(28));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-6.2,-6.2,36.7,45.1);


(lib.Symbol3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(12.5,1,1).p("AAqgsIhTBZ");
	this.shape.setTransform(4.2,4.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol3, new cjs.Rectangle(-6.2,-6.2,20.8,21.5), null);


(lib.Symbol2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(12.5,1,1).p("AAvB+Ihdj7");
	this.shape.setTransform(4.7,12.6);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol2, new cjs.Rectangle(-6.2,-6.2,21.8,37.7), null);


(lib.Symbol1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(12.5,1,1).p("AgoB2IBRjr");
	this.shape.setTransform(4.1,11.8);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol1, new cjs.Rectangle(-6.2,-6.2,20.6,36.2), null);


(lib.Symbol11 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 5 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("AD0QcIolovIvUjtQAEAAATgqQASgrAFgcQAHgtgBgcQAAgQgEgXIgEgTQAIgRheBEIguAKQiTBCDOnlIJNpVIZFkyIIrOKIg6M/IokL6g");
	mask.setTransform(127.6,76.9);

	// Symbol 7
	this.instance = new lib.Symbol7();
	this.instance.parent = this;
	this.instance.setTransform(21.7,84.3,1,1,0,0,0,21.7,31.1);

	var maskedShapeInstanceList = [this.instance];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(4).to({regX:21.8,regY:31.2,rotation:-15,x:16.8},15,cjs.Ease.get(1)).wait(20).to({regX:21.7,regY:31.1,rotation:0,x:21.7},15,cjs.Ease.get(1)).wait(16));

	// leefs_2.png
	this.instance_1 = new lib.Symbol12();
	this.instance_1.parent = this;
	this.instance_1.setTransform(105,75,1,1,0,0,0,30.7,75);

	var maskedShapeInstanceList = [this.instance_1];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(4).to({rotation:2.5,x:101.8,y:75.6},15,cjs.Ease.get(1)).wait(20).to({rotation:0,x:105,y:75},15,cjs.Ease.get(1)).wait(16));

	// Layer 4
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(12.5,1,1).p("AFgmxQBzEIgJDLQgIDCh2BqQh2BqjJgGQjRgGkGiA");
	this.shape.setTransform(63.7,52.4);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(12.5,1,1).p("AnNEtQEKB8DRAHQDJAIB3hpQB2hpAJjDQAKjLhxkI");
	this.shape_1.setTransform(63.1,52.3);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(12.5,1,1).p("AnQEuQENB5DSAIQDJAJB3hoQB3hpAKjCQALjLhvkJ");
	this.shape_2.setTransform(62.5,52.2);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(12.5,1,1).p("AnTEwQERB1DRAJQDJAKB4hoQB3hoAMjCQAMjLhukJ");
	this.shape_3.setTransform(62,52.1);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(12.5,1,1).p("AnWExQEUByDSAKQDJALB4hnQB3hoANjCQANjLhtkJ");
	this.shape_4.setTransform(61.6,52.1);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(12.5,1,1).p("AnYEyQEXBvDRAMQDJALB5hnQB4hnANjCQAOjLhrkK");
	this.shape_5.setTransform(61.2,52);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(12.5,1,1).p("AnaEzQEZBsDSANQDIAMB6hmQB4hnAOjCQAPjLhqkK");
	this.shape_6.setTransform(60.8,51.9);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(12.5,1,1).p("AncE0QEcBqDRANQDJANB5hmQB5hmAPjBQAQjLhqkL");
	this.shape_7.setTransform(60.4,51.8);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(12.5,1,1).p("AneE1QEeBoDRAOQDJANB6hlQB5hmAQjCQAQjLhpkL");
	this.shape_8.setTransform(60.1,51.8);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(12.5,1,1).p("AnfE1QEgBmDRAPQDIAOB6hlQB6hmAQjBQARjLhokM");
	this.shape_9.setTransform(59.9,51.8);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(12.5,1,1).p("AnhE1QEiBlDRAPQDIAPB7hlQB5hlARjCQARjLhnkL");
	this.shape_10.setTransform(59.7,51.7);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(12.5,1,1).p("AniE2QEjBkDRAPQDJAPB6hlQB6hkARjCQASjLhnkM");
	this.shape_11.setTransform(59.5,51.7);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(12.5,1,1).p("AnjE2QEkBjDRAPQDIAQB7hlQB6hkASjCQARjLhmkM");
	this.shape_12.setTransform(59.4,51.7);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(12.5,1,1).p("AnjE3QEkBiDRAQQDIAPB7hkQB6hlASjBQASjLhmkM");
	this.shape_13.setTransform(59.3,51.6);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(12.5,1,1).p("AnjE3QEkBhDRAQQDJAQB7hkQB6hlASjBQASjLhmkM");
	this.shape_14.setTransform(59.2,51.6);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(12.5,1,1).p("AGOmpQBmEMgSDLQgSDBh6BlQh7BkjJgQQjRgQklhh");
	this.shape_15.setTransform(59.2,51.6);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(12.5,1,1).p("AngE1QEhBmDRAPQDIAOB7hlQB5hlARjCQARjLhokL");
	this.shape_16.setTransform(59.7,51.7);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(12.5,1,1).p("AndE0QEdBpDRAOQDJANB6hmQB4hlAQjCQAQjLhpkL");
	this.shape_17.setTransform(60.3,51.8);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#000000").ss(12.5,1,1).p("AnYEyQEXBvDRAMQDJALB5hnQB4hnANjCQAOjLhskK");
	this.shape_18.setTransform(61.2,52);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#000000").ss(12.5,1,1).p("AnVExQEUByDRAKQDJALB4hnQB3hoANjCQANjLhtkJ");
	this.shape_19.setTransform(61.6,52.1);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#000000").ss(12.5,1,1).p("AnREvQEPB3DRAJQDJAIB3hnQB3hpALjCQALjLhvkJ");
	this.shape_20.setTransform(62.4,52.2);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#000000").ss(12.5,1,1).p("AnQEuQENB5DSAIQDIAJB4hpQB2hpALjCQAKjLhwkI");
	this.shape_21.setTransform(62.7,52.3);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#000000").ss(12.5,1,1).p("AnOEtQELB7DRAIQDJAHB3hoQB2hpAKjDQAKjLhwkI");
	this.shape_22.setTransform(62.9,52.3);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#000000").ss(12.5,1,1).p("AnNEtQEKB9DRAHQDJAHB3hpQB2hpAJjDQAKjLhxkI");
	this.shape_23.setTransform(63.1,52.3);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#000000").ss(12.5,1,1).p("AnMEsQEIB+DSAHQDJAHB2hpQB2hqAJjCQAJjLhxkI");
	this.shape_24.setTransform(63.3,52.4);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f().s("#000000").ss(12.5,1,1).p("AnLEsQEHB/DSAGQDJAGB2hpQB2hqAIjCQAJjLhykI");
	this.shape_25.setTransform(63.5,52.4);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f().s("#000000").ss(12.5,1,1).p("AnLEsQEHB/DSAGQDJAHB2hqQB1hqAJjCQAJjLhzkI");
	this.shape_26.setTransform(63.6,52.4);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f().s("#000000").ss(12.5,1,1).p("AnKEsQEGCADSAGQDJAGB2hqQB1hqAJjCQAIjLhykI");
	this.shape_27.setTransform(63.6,52.4);

	var maskedShapeInstanceList = [this.shape,this.shape_1,this.shape_2,this.shape_3,this.shape_4,this.shape_5,this.shape_6,this.shape_7,this.shape_8,this.shape_9,this.shape_10,this.shape_11,this.shape_12,this.shape_13,this.shape_14,this.shape_15,this.shape_16,this.shape_17,this.shape_18,this.shape_19,this.shape_20,this.shape_21,this.shape_22,this.shape_23,this.shape_24,this.shape_25,this.shape_26,this.shape_27];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},4).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_15}]},20).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_27}]},1).to({state:[{t:this.shape}]},1).wait(16));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-6.2,0,266.6,172);


(lib.Symbol8 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.Symbol1();
	this.instance.parent = this;
	this.instance.setTransform(12,12.5,1,1,0,0,0,4,11.8);

	this.instance_1 = new lib.Symbol2();
	this.instance_1.parent = this;
	this.instance_1.setTransform(4.7,12.6,1,1,0,0,0,4.7,12.6);

	this.instance_2 = new lib.Symbol3();
	this.instance_2.parent = this;
	this.instance_2.setTransform(5.9,26.6,1,1,0,0,0,4.2,4.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_2},{t:this.instance_1},{t:this.instance}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol8, new cjs.Rectangle(-6.2,-6.2,28.6,43.6), null);


(lib.Symbol9 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Symbol 8
	this.instance = new lib.Symbol8();
	this.instance.parent = this;
	this.instance.setTransform(72.2,156.5,1,1,0,0,0,12.1,26.6);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(4).to({regY:26.5,rotation:-53.2,x:50.8,y:226.9},5,cjs.Ease.get(-1)).to({regY:26.6,rotation:0,x:72.2,y:156.5},8,cjs.Ease.get(1)).to({regY:26.5,rotation:-53.2,x:50.8,y:226.9},5,cjs.Ease.get(-1)).to({regY:26.6,rotation:0,x:72.2,y:156.5},8,cjs.Ease.get(1)).to({regY:26.5,rotation:-53.2,x:50.8,y:226.9},5,cjs.Ease.get(-1)).to({regY:26.6,rotation:0,x:72.2,y:156.5},8,cjs.Ease.get(1)).wait(27));

	// Layer 4
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(12.5,1,1).p("AKPGzQiWgTjHhCQjGhBjIh2QjIh2iRiaQiRidhIis");
	this.shape.setTransform(135.9,196);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(12.5,1,1).p("AqWmkQBPCmCUCWQCUCVDKByQDIByDHBBQDHA/CWAU");
	this.shape_1.setTransform(135.9,197.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(12.5,1,1).p("Aqsl6QBiCSCdCGQCdCFDNBnQDNBmDIA8QDHA7CVAU");
	this.shape_2.setTransform(136,202.4);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(12.5,1,1).p("ArSk0QCCBxCsBqQCrBoDVBVQDTBUDJA0QDJA0CSAV");
	this.shape_3.setTransform(136.1,210.4);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(12.5,1,1).p("AsHjSQCwBCDABEQDBBDDcA5QDeA6DLApQDLApCOAX");
	this.shape_4.setTransform(136.2,221.6);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(12.5,1,1).p("ANMBVQiKgZjNgbQjNgbjqgYQjpgYjagSQjbgRjrgH");
	this.shape_5.setTransform(136.4,236.1);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(12.5,1,1).p("AsfimQDFAuDJAxQDJAyDhAuQDiAuDMAkQDMAkCNAY");
	this.shape_6.setTransform(136.3,226.7);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(12.5,1,1).p("Ar4jtQCjBPC7BOQC6BODbBAQDbBCDKAsQDKAsCQAW");
	this.shape_7.setTransform(136.2,218.5);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(12.5,1,1).p("ArYkpQCHBrCvBmQCtBlDVBRQDVBSDKAyQDJAzCRAV");
	this.shape_8.setTransform(136.1,211.6);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(12.5,1,1).p("Aq+lbQBxCDCkB6QCjB4DRBfQDQBdDIA5QDJA4CTAV");
	this.shape_9.setTransform(136,206);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(12.5,1,1).p("AqpmBQBfCWCcCIQCbCHDNBpQDMBoDIA9QDHA8CVAU");
	this.shape_10.setTransform(136,201.6);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(12.5,1,1).p("AqamcQBSCiCWCUQCVCSDLBwQDJBvDHBAQDIA/CVAT");
	this.shape_11.setTransform(135.9,198.5);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(12.5,1,1).p("AqRmtQBKCqCTCaQCSCZDJB0QDIB0DGBBQDHBBCWAU");
	this.shape_12.setTransform(135.9,196.6);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(12.5,1,1).p("ANMBVQiKgZjNgbQjNgbjqgYQjpgYjbgSQjagRjrgH");
	this.shape_13.setTransform(136.4,236.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},4).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape}]},1).wait(27));
	this.timeline.addTween(cjs.Tween.get(this.shape).wait(4).to({_off:true},1).wait(12).to({_off:false},0).to({_off:true},1).wait(12).to({_off:false},0).to({_off:true},1).wait(12).to({_off:false},0).wait(27));

	// leefs_1.png
	this.instance_1 = new lib.Symbol10();
	this.instance_1.parent = this;
	this.instance_1.setTransform(119.7,208.3,1,1,0,0,0,119.7,208.3);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(4).to({rotation:-40.4,x:118.3,y:233},5,cjs.Ease.get(-1)).to({rotation:0,x:119.7,y:208.3},8,cjs.Ease.get(1)).to({rotation:-40.4,x:118.3,y:233},5,cjs.Ease.get(-1)).to({rotation:0,x:119.7,y:208.3},8,cjs.Ease.get(1)).to({rotation:-40.4,x:118.3,y:233},5,cjs.Ease.get(-1)).to({rotation:0,x:119.7,y:208.3},8,cjs.Ease.get(1)).wait(27));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,228,245.7);


// stage content:
(lib.Forest = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 24
	this.instance = new lib.Symbol13();
	this.instance.parent = this;
	this.instance.setTransform(726.6,312.3,1,1,0,0,0,49.9,27.8);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(80));

	// Layer 20
	this.instance_1 = new lib.Symbol11();
	this.instance_1.parent = this;
	this.instance_1.setTransform(863.4,415.6,1,1,0,0,0,127,86);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(80));

	// Layer 6
	this.instance_2 = new lib.flower();
	this.instance_2.parent = this;
	this.instance_2.setTransform(588,351);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(80));

	// Layer 3
	this.instance_3 = new lib.pack();
	this.instance_3.parent = this;
	this.instance_3.setTransform(609,131);

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(80));

	// Layer 16
	this.instance_4 = new lib.Symbol9();
	this.instance_4.parent = this;
	this.instance_4.setTransform(613,271.9,1,1,0,0,0,114,122.9);

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(80));

	// Layer 2
	this.instance_5 = new lib.bg();
	this.instance_5.parent = this;
	this.instance_5.setTransform(377,484);

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(80));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(540,340,1080,680);
// library properties:
lib.properties = {
	id: '0B7CB6B393E34D4AA5D2C4725AE803AC',
	width: 1080,
	height: 680,
	fps: 30,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: [
		{src:"assets/images/flower24.png", id:"flower"},
		{src:"assets/images/leefs_124.png", id:"leefs_1"},
		{src:"assets/images/leefs_224.png", id:"leefs_2"},
		{src:"assets/images/pack24.png", id:"pack"}
	],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['0B7CB6B393E34D4AA5D2C4725AE803AC'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}



})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;

$(function () {
    var canvas, stage, exportRoot, anim_container, dom_overlay_container, fnStartAnimation;
    function init() {
        canvas = document.getElementById("canvas24");
        anim_container = document.getElementById("animation_container24");
        dom_overlay_container = document.getElementById("dom_overlay_container24");
        var comp=AdobeAn.getComposition("0B7CB6B393E34D4AA5D2C4725AE803AC");
        var lib=comp.getLibrary();
        var loader = new createjs.LoadQueue(false);
        loader.addEventListener("fileload", function(evt){handleFileLoad(evt,comp)});
        loader.addEventListener("complete", function(evt){handleComplete(evt,comp)});
        var lib=comp.getLibrary();
        loader.loadManifest(lib.properties.manifest);
    }
    function handleFileLoad(evt, comp) {
        var images=comp.getImages();
        if (evt && (evt.item.type == "image")) { images[evt.item.id] = evt.result; }
    }
    function handleComplete(evt,comp) {
        //This function is always called, irrespective of the content. You can use the variable "stage" after it is created in token create_stage.
        var lib=comp.getLibrary();
        var ss=comp.getSpriteSheet();
        var queue = evt.target;
        var ssMetadata = lib.ssMetadata;
        for(i=0; i<ssMetadata.length; i++) {
            ss[ssMetadata[i].name] = new createjs.SpriteSheet( {"images": [queue.getResult(ssMetadata[i].name)], "frames": ssMetadata[i].frames} )
        }
        exportRoot = new lib.Forest();
        stage = new lib.Stage(canvas);
        //Registers the "tick" event listener.
        fnStartAnimation = function() {
            stage.addChild(exportRoot);
            createjs.Ticker.setFPS(lib.properties.fps);
            createjs.Ticker.addEventListener("tick", stage);
        }
        //Code to support hidpi screens and responsive scaling.
        function makeResponsive(isResp, respDim, isScale, scaleType) {
            var lastW, lastH, lastS=1;
            window.addEventListener('resize', resizeCanvas);
            resizeCanvas();
            function resizeCanvas() {
                var w = lib.properties.width, h = lib.properties.height;
                var iw = window.innerWidth, ih=window.innerHeight;
                var pRatio = window.devicePixelRatio || 1, xRatio=iw/w, yRatio=ih/h, sRatio=1;
                if(isResp) {
                    if((respDim=='width'&&lastW==iw) || (respDim=='height'&&lastH==ih)) {
                        sRatio = lastS;
                    }
                    else if(!isScale) {
                        if(iw<w || ih<h)
                            sRatio = Math.min(xRatio, yRatio);
                    }
                    else if(scaleType==1) {
                        sRatio = Math.min(xRatio, yRatio);
                    }
                    else if(scaleType==2) {
                        sRatio = Math.max(xRatio, yRatio);
                    }
                }
                canvas.width = w*pRatio*sRatio;
                canvas.height = h*pRatio*sRatio;
                canvas.style.width = dom_overlay_container.style.width = anim_container.style.width =  w*sRatio+'px';
                canvas.style.height = anim_container.style.height = dom_overlay_container.style.height = h*sRatio+'px';
                stage.scaleX = pRatio*sRatio;
                stage.scaleY = pRatio*sRatio;
                lastW = iw; lastH = ih; lastS = sRatio;
                stage.tickOnUpdate = false;
                stage.update();
                stage.tickOnUpdate = true;
            }
        }
        makeResponsive(true,'both',true,2);
        AdobeAn.compositionLoaded(lib.properties.id);
        fnStartAnimation();
    }
    init();
});