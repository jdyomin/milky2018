(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [];


// symbols:



(lib.bg = function() {
	this.initialize(img.bg);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,1080,680);


(lib._case = function() {
	this.initialize(img._case);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,207,369);


(lib.hand2 = function() {
	this.initialize(img.hand2);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,39,30);


(lib.pack = function() {
	this.initialize(img.pack);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,270,373);// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.Symbol7 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(11.5,1,1).p("AhpAZQApgpCqgI");
	this.shape.setTransform(10.625,2.525);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol7, new cjs.Rectangle(-5.7,-5.7,32.7,16.5), null);


(lib.Symbol6 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(11.5,1,1).p("AgPCMQgTiCA3iV");
	this.shape.setTransform(2.0688,14.025);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol6, new cjs.Rectangle(-5.7,-5.7,15.600000000000001,39.5), null);


(lib.Symbol5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib._case();
	this.instance.parent = this;
	this.instance.setTransform(-103.5,-184.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol5, new cjs.Rectangle(-103.5,-184.5,207,369), null);


(lib.Symbol4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.hand2();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol4, new cjs.Rectangle(0,0,39,30), null);


(lib.Symbol2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#F51431").s().p("AAAAqQgRAAgNgMQgMgMAAgSQAAgQAMgNQANgNARAAIADAAQAPABALAMQANANAAAQQAAASgNAMQgLAMgPAAIgDAAg");
	this.shape.setTransform(4.25,4.25);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol2, new cjs.Rectangle(0,0,8.5,8.5), null);


(lib.Symbol_8_Layer_5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_5
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(11.5,1,1).p("AnumQQBRKyDZBeQDYBeHbmb");
	this.shape.setTransform(49.475,40.149);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(11.5,1,1).p("An8mJQBlKqDaBYQDaBZHfmG");
	this.shape_1.setTransform(51,39.2193);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(11.5,1,1).p("AoJmCQB3KiDcBTQDcBTHklx");
	this.shape_2.setTransform(52.4,38.3612);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(11.5,1,1).p("AoVl7QCIKaDeBPQDdBOHolf");
	this.shape_3.setTransform(53.7,37.5599);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(11.5,1,1).p("Aogl1QCYKTDfBLQDfBJHrlN");
	this.shape_4.setTransform(54.9,36.8395);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(11.5,1,1).p("AoqlvQClKNDhBGQDgBFHvk9");
	this.shape_5.setTransform(56.025,36.1741);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(11.5,1,1).p("AozlqQCyKHDiBCQDhBCHykv");
	this.shape_6.setTransform(57.025,35.5652);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(11.5,1,1).p("Ao7lmQC9KDDkA/QDhA+H1ki");
	this.shape_7.setTransform(57.9,35.0283);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(11.5,1,1).p("ApCliQDIJ+DjA8QDjA8H3kX");
	this.shape_8.setTransform(58.7,34.531);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(11.5,1,1).p("ApIleQDQJ6DlA5QDjA5H5kN");
	this.shape_9.setTransform(59.375,34.1233);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(11.5,1,1).p("ApOlbQDYJ2DlA4QDlA3H7kF");
	this.shape_10.setTransform(59.95,33.7786);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(11.5,1,1).p("ApSlZQDeJ0DmA2QDlA1H8j+");
	this.shape_11.setTransform(60.425,33.4904);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(11.5,1,1).p("ApVlXQDiJyDnA0QDlA0H9j5");
	this.shape_12.setTransform(60.775,33.267);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(11.5,1,1).p("ApYlWQDmJxDnAzQDlAzH/j1");
	this.shape_13.setTransform(61.05,33.0894);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(11.5,1,1).p("ApZlVQDoJwDmAyQDmAyH/jy");
	this.shape_14.setTransform(61.2,33.0024);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(11.5,1,1).p("ApalVQDpJwDnAyQDmAyH/jy");
	this.shape_15.setTransform(61.25,32.9695);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(11.5,1,1).p("ApMldQDVJ4DmA5QDkA3H6kI");
	this.shape_16.setTransform(59.725,33.907);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(11.5,1,1).p("Ao/lkQDDKADkA+QDiA9H2kc");
	this.shape_17.setTransform(58.325,34.7643);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#000000").ss(11.5,1,1).p("AoolwQCjKODgBHQDfBGHvlA");
	this.shape_18.setTransform(55.825,36.2856);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#000000").ss(11.5,1,1).p("Aoel2QCVKUDfBLQDeBLHrlQ");
	this.shape_19.setTransform(54.7,36.951);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#000000").ss(11.5,1,1).p("AoMl/QB8KfDdBSQDbBRHllr");
	this.shape_20.setTransform(52.825,38.0972);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#000000").ss(11.5,1,1).p("AoFmDQByKjDcBVQDbBUHil2");
	this.shape_21.setTransform(52.025,38.5945);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#000000").ss(11.5,1,1).p("An/mHQBpKnDbBYQDaBXHhmB");
	this.shape_22.setTransform(51.35,39.0027);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#000000").ss(11.5,1,1).p("An6mKQBiKrDbBaQDZBZHfmJ");
	this.shape_23.setTransform(50.775,39.3475);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#000000").ss(11.5,1,1).p("An2mMQBcKtDbBcQDYBaHemP");
	this.shape_24.setTransform(50.3,39.6357);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f().s("#000000").ss(11.5,1,1).p("AnymOQBXKvDaBdQDYBcHcmV");
	this.shape_25.setTransform(49.95,39.8599);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f().s("#000000").ss(11.5,1,1).p("AnwmPQBUKwDZBeQDYBdHcmY");
	this.shape_26.setTransform(49.675,40.0373);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f().s("#000000").ss(11.5,1,1).p("AnumQQBRKxDaBfQDXBeHbmb");
	this.shape_27.setTransform(49.525,40.1249);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},14).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_15}]},10).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_27}]},1).to({state:[{t:this.shape}]},1).wait(27));

}).prototype = p = new cjs.MovieClip();


(lib.Scene_1_Layer_14 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_14
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#E21E2B").s().p("EgmyAbuMAAAg3bMBNlAAAMAAAA3bg");
	this.shape.setTransform(167.15,649.65);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(45));

}).prototype = p = new cjs.MovieClip();


(lib.Scene_1_Layer_12 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_12
	this.instance = new lib.pack();
	this.instance.parent = this;
	this.instance.setTransform(30.75,327.7,0.6223,0.6223);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(45));

}).prototype = p = new cjs.MovieClip();


(lib.Scene_1_Layer_11 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_11
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(5.5,1,1).p("AjIEHQBkgdA6hZQAmg6Ahh1QAUhGALgeQAKgdAbgoQAcgoBMgX");
	this.shape.setTransform(179.175,397.475);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(5.5,1,1).p("AjPEHQBfgjA8hZQArg8AghqQAXhCAOgeQAOggAdgnQAcgoBNgc");
	this.shape_1.setTransform(179.85,397.55);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(5.5,1,1).p("AjVEGQBbgpA9hWQAvg/AhhgQAXg+ASggQASggAdgoQAdgoBOgf");
	this.shape_2.setTransform(180.475,397.625);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(5.5,1,1).p("AjaEGQBVgvBAhUQAyhBAhhXQAZg7AVghQAVgiAegnQAegnBOgk");
	this.shape_3.setTransform(181.05,397.7);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(5.5,1,1).p("AjfEGQBSg0BAhTQA2hDAhhOQAbg3AYgjQAYgjAfgnQAegnBOgo");
	this.shape_4.setTransform(181.575,397.725);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(5.5,1,1).p("AjkEGQBOg5BChSQA5hFAhhGQAcg0AbgkQAbgkAfgnQAfgnBPgq");
	this.shape_5.setTransform(182.05,397.8);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(5.5,1,1).p("AjoEFQBLg8BDhRQA8hGAhhAQAdgxAdglQAeglAggnQAfgmBPgu");
	this.shape_6.setTransform(182.475,397.825);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(5.5,1,1).p("AjsEFQBJhABDhQQBAhIAgg5QAegvAggmQAfgmAhgmQAggnBPgw");
	this.shape_7.setTransform(182.85,397.9);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(5.5,1,1).p("AjvEFQBGhEBFhOQBBhKAhg0QAegsAignQAhgnAhgmQAhgmBPgz");
	this.shape_8.setTransform(183.175,397.925);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(5.5,1,1).p("AjyEFQBEhGBGhOQBDhKAhgxQAfgqAjgoQAjgnAhgmQAhgmBQg1");
	this.shape_9.setTransform(183.45,397.95);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(5.5,1,1).p("AjzEFQBBhJBHhNQBEhLAhgtQAfgpAlgoQAkgoAigmQAhgmBPg2");
	this.shape_10.setTransform(183.675,398);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(5.5,1,1).p("Aj1EFQBAhLBHhNQBGhLAhgqQAggoAlgoQAmgoAhgnQAigmBPg3");
	this.shape_11.setTransform(183.85,398);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(5.5,1,1).p("Aj2EEQA/hLBHhNQBHhMAhgnQAggnAmgpQAmgpAigmQAigmBPg3");
	this.shape_12.setTransform(183.975,398.025);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(5.5,1,1).p("Aj3EEQA+hMBIhMQBHhMAhgnQAggnAngoQAmgpAigmQAigmBQg4");
	this.shape_13.setTransform(184.05,398.025);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(5.5,1,1).p("Aj3EEQA+hMBIhMQBHhNAhgmQAggmAngpQAngpAigmQAigmBPg4");
	this.shape_14.setTransform(184.075,398.025);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(5.5,1,1).p("AjxEFQBDhGBGhOQBDhKAhgxQAfgqAjgoQAjgnAhgmQAhgmBPg1");
	this.shape_15.setTransform(183.425,397.95);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(5.5,1,1).p("AjmEFQBMg7BChRQA8hGAghCQAcgyAdglQAcgkAggnQAggnBOgs");
	this.shape_16.setTransform(182.325,397.825);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(5.5,1,1).p("AjiEGQBQg3BChSQA3hEAhhKQAbg2AagjQAZgkAggnQAegnBOgp");
	this.shape_17.setTransform(181.8,397.775);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#000000").ss(5.5,1,1).p("AjdEGQBTgyBAhUQA1hCAghSQAag4AYgiQAWgjAfgnQAegnBOgm");
	this.shape_18.setTransform(181.35,397.725);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#000000").ss(5.5,1,1).p("AjZEGQBXguA/hVQAxhAAhhYQAZg7AVgiQAUghAegoQAegnBNgj");
	this.shape_19.setTransform(180.925,397.675);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#000000").ss(5.5,1,1).p("AjWEGQBagqA+hWQAvg/AhheQAYg+ASggQATghAdgnQAdgoBOgg");
	this.shape_20.setTransform(180.575,397.625);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#000000").ss(5.5,1,1).p("AjTEHQBdgoA9hXQAtg+AhhjQAWhAARggQARgfAdgoQAcgoBOge");
	this.shape_21.setTransform(180.25,397.6);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#000000").ss(5.5,1,1).p("AjQEHQBeglA9hXQArg9AghoQAXhCAPgfQAPgfAcgnQAdgpBNgc");
	this.shape_22.setTransform(179.975,397.575);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#000000").ss(5.5,1,1).p("AjNEHQBggjA8hXQApg8AhhtQAWhDANgeQAOgfAcgnQAcgpBNga");
	this.shape_23.setTransform(179.7,397.55);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#000000").ss(5.5,1,1).p("AjMEHQBiggA7hYQAog8AhhvQAVhEANgfQAMgeAcgnQAcgpBNgZ");
	this.shape_24.setTransform(179.525,397.5);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f().s("#000000").ss(5.5,1,1).p("AjKEHQBjgfA6hZQAng7AhhxQAVhFAMgeQALgeAcgoQAbgoBNgY");
	this.shape_25.setTransform(179.375,397.5);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f().s("#000000").ss(5.5,1,1).p("AjJEHQBkgeA6hZQAmg6AhhzQAVhGALgeQALgdAbgoQAcgpBMgX");
	this.shape_26.setTransform(179.25,397.475);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f().s("#000000").ss(5.5,1,1).p("AjIEHQBkgdA6hZQAmg6Ahh1QAUhGALgeQAKgdAcgoQAbgoBNgX");
	this.shape_27.setTransform(179.2,397.475);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_14}]},15).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_27}]},1).to({state:[{t:this.shape}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();


(lib.Scene_1_Layer_3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_3
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#3479C8").s().p("AtNFdIAAq5IabAAIAAK5g");
	this.shape.setTransform(362.075,26.075);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(45));

}).prototype = p = new cjs.MovieClip();


(lib.Scene_1_Layer_2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.instance = new lib.bg();
	this.instance.parent = this;
	this.instance.setTransform(-343.15,-0.4,0.7049,0.7049);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(45));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol9 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Symbol_7
	this.instance = new lib.Symbol7();
	this.instance.parent = this;
	this.instance.setTransform(14.75,29.15,1,1,0,0,0,10.6,2.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	// Symbol_6
	this.instance_1 = new lib.Symbol6();
	this.instance_1.parent = this;
	this.instance_1.setTransform(14,14,1,1,0,0,0,2,14);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(1));

	// Layer_4
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(11.5,1,1).p("AAdCGQhAh1AIiW");
	this.shape.setTransform(2.9062,17.9);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol9, new cjs.Rectangle(-5.7,-5.7,36.9,43.2), null);


(lib.Symbol_8_Symbol_7 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Symbol_7
	this.instance = new lib.Symbol9();
	this.instance.parent = this;
	this.instance.setTransform(97.2,48.3,1,1,0,0,0,3.7,32.1);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(14).to({regX:3.8,rotation:29.9992,x:121.75,y:45.85},15,cjs.Ease.get(1)).wait(10).to({regX:3.7,rotation:0,x:97.2,y:48.3},15,cjs.Ease.get(1)).wait(27));

}).prototype = p = new cjs.MovieClip();


(lib.Scene_1_Layer_13 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_13
	this.instance = new lib.Symbol5();
	this.instance.parent = this;
	this.instance.setTransform(239.6,441.15,0.6223,0.6223,0,0,0,0.1,0.1);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({regX:0,x:247.05},14,cjs.Ease.get(1)).wait(15).to({regX:0.1,x:239.6},15,cjs.Ease.get(1)).wait(1));

}).prototype = p = new cjs.MovieClip();


(lib.Scene_1_Layer_4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_4
	this.instance = new lib.Symbol4();
	this.instance.parent = this;
	this.instance.setTransform(195.15,372.75,0.4857,0.4857,0,0,0,0.1,11.6);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({regX:0,rotation:-8.1978,x:204.75,y:373.95},14,cjs.Ease.get(1)).wait(15).to({regX:0.1,rotation:0,x:195.15,y:372.75},15,cjs.Ease.get(1)).wait(1));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#1B1814").ss(11.5,1,1).p("AimhjQgJBcA0A4QAuAzBGAAQBEAAAxgwQA3g0ADhR");
	this.shape.setTransform(188.8148,-96.875);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(40));

	// Layer_2
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#1B1814").ss(11.5,1,1).p("Ai2hsQgJBlA4A9QAyA3BNAAQBKAAA3g0QA7g5AEhY");
	this.shape_1.setTransform(120.6066,-96.55);

	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(40));

	// Layer_3
	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#1B1814").ss(11,1,1).p("AkLhfQBDDEDRgGQBWgDBJgrQBKgtAahG");
	this.shape_2.setTransform(156.025,-52.5925);

	this.timeline.addTween(cjs.Tween.get(this.shape_2).wait(40));

	// Layer_5
	this.instance = new lib.Symbol2();
	this.instance.parent = this;
	this.instance.setTransform(137.85,-48.75,2.0168,2.0168,0,46.0839,51.8739,5.4,7.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1).to({regX:4.3,regY:4.3,scaleX:2.0159,scaleY:2.0177,skewX:45.7195,skewY:51.4594,x:141.35,y:-54.8},0).wait(1).to({scaleX:2.0136,scaleY:2.0199,skewX:44.7878,skewY:50.3997,x:141.8,y:-54.45},0).wait(1).to({scaleX:2.0096,scaleY:2.0237,skewX:43.1697,skewY:48.559,x:142.7,y:-53.7},0).wait(1).to({scaleX:2.0035,scaleY:2.0294,skewX:40.7228,skewY:45.7757,x:144.15,y:-52.85},0).wait(1).to({scaleX:1.995,scaleY:2.0375,skewX:37.2999,skewY:41.8822,x:146.35,y:-51.75},0).wait(1).to({scaleX:1.9839,scaleY:2.0481,skewX:32.8139,skewY:36.7794,x:149.5,y:-50.75},0).wait(1).to({scaleX:1.9705,scaleY:2.0609,skewX:27.3853,skewY:30.6045,x:153.3,y:-50.15},0).wait(1).to({scaleX:1.9559,scaleY:2.0748,skewX:21.5115,skewY:23.9231,x:157.55,y:-50.1},0).wait(1).to({scaleX:1.9422,scaleY:2.0879,skewX:15.9743,skewY:17.6245,x:161.1,y:-50.5},0).wait(1).to({scaleX:1.9309,scaleY:2.0987,skewX:11.4074,skewY:12.4298,x:163.95,y:-51.25},0).wait(1).to({scaleX:1.9225,scaleY:2.1066,skewX:8.0351,skewY:8.5938,x:166.05,y:-52},0).wait(1).to({scaleX:1.917,scaleY:2.1119,skewX:5.788,skewY:6.0378,x:167.45,y:-52.6},0).wait(1).to({scaleX:1.9137,scaleY:2.115,skewX:4.4942,skewY:4.5662,x:168.2,y:-53},0).wait(1).to({regX:5.4,regY:7.5,scaleX:1.9125,scaleY:2.1162,rotation:3.9793,skewX:0,skewY:0,x:170.2,y:-46.35},0).wait(7).to({regX:4.3,regY:4.3,scaleX:1.914,scaleY:2.1148,rotation:0,skewX:4.5733,skewY:4.6561,x:168.2,y:-52.95},0).wait(1).to({scaleX:1.9174,scaleY:2.1115,skewX:5.9626,skewY:6.2364,x:167.3,y:-52.5},0).wait(1).to({scaleX:1.923,scaleY:2.1061,skewX:8.2536,skewY:8.8424,x:165.8,y:-51.9},0).wait(1).to({scaleX:1.9312,scaleY:2.0984,skewX:11.5185,skewY:12.5562,x:163.55,y:-51.1},0).wait(1).to({scaleX:1.9416,scaleY:2.0884,skewX:15.7359,skewY:17.3534,x:160.5,y:-50.4},0).wait(1).to({scaleX:1.9539,scaleY:2.0767,skewX:20.7133,skewY:23.0151,x:156.95,y:-50},0).wait(1).to({scaleX:1.9672,scaleY:2.0641,skewX:26.0541,skewY:29.0903,x:153.3,y:-50.15},0).wait(1).to({scaleX:1.98,scaleY:2.0518,skewX:31.248,skewY:34.9982,x:150.1,y:-50.5},0).wait(1).to({scaleX:1.9914,scaleY:2.0409,skewX:35.8517,skewY:40.2349,x:147.3,y:-51.3},0).wait(1).to({scaleX:2.0007,scaleY:2.0321,skewX:39.611,skewY:44.5111,x:145.05,y:-52.3},0).wait(1).to({scaleX:2.0078,scaleY:2.0254,skewX:42.4537,skewY:47.7446,x:143.25,y:-53.25},0).wait(1).to({scaleX:2.0127,scaleY:2.0207,skewX:44.4197,skewY:49.9809,x:142.1,y:-54.1},0).wait(1).to({scaleX:2.0156,scaleY:2.0179,skewX:45.5968,skewY:51.3198,x:141.45,y:-54.7},0).wait(1).to({regX:5.4,regY:7.5,scaleX:2.0168,scaleY:2.0168,skewX:46.0839,skewY:51.8739,x:137.8,y:-48.7},0).wait(6));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(96.5,-113.2,114.9,75.7);


(lib.Symbol8 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_80 = function() {
		this.___loopingOver___ = true;
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(80).call(this.frame_80).wait(1));

	// Symbol_7_obj_
	this.Symbol_7 = new lib.Symbol_8_Symbol_7();
	this.Symbol_7.name = "Symbol_7";
	this.Symbol_7.parent = this;
	this.Symbol_7.setTransform(106.2,32,1,1,0,0,0,106.2,32);
	this.Symbol_7.depth = 0;
	this.Symbol_7.isAttachedToCamera = 0
	this.Symbol_7.isAttachedToMask = 0
	this.Symbol_7.layerDepth = 0
	this.Symbol_7.layerIndex = 0
	this.Symbol_7.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Symbol_7).wait(81));

	// Layer_5_obj_
	this.Layer_5 = new lib.Symbol_8_Layer_5();
	this.Layer_5.name = "Layer_5";
	this.Layer_5.parent = this;
	this.Layer_5.setTransform(49.5,40.1,1,1,0,0,0,49.5,40.1);
	this.Layer_5.depth = 0;
	this.Layer_5.isAttachedToCamera = 0
	this.Layer_5.isAttachedToMask = 0
	this.Layer_5.layerDepth = 0
	this.Layer_5.layerIndex = 1
	this.Layer_5.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_5).wait(81));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-5.7,-6.9,162,93);


(lib.Scene_1_Layer_8 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_8
	this.instance = new lib.Symbol1();
	this.instance.parent = this;
	this.instance.setTransform(101.15,469.55,0.4857,0.4857,0,0,0,80.1,54.1);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(45));

}).prototype = p = new cjs.MovieClip();


(lib.Scene_1_Layer_7 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_7
	this.instance = new lib.Symbol8();
	this.instance.parent = this;
	this.instance.setTransform(84.7,439.2,0.4857,0.4857,0,0,0,0.1,0.1);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(45));

}).prototype = p = new cjs.MovieClip();


// stage content:
(lib.Prom1_vert = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	this.___GetDepth___ = function(obj) {
		var depth = obj.depth;
		var cameraObj = this.___camera___instance;
		if(cameraObj && cameraObj.depth && obj.isAttachedToCamera)
		{
			depth += depth + cameraObj.depth;
		}
		return depth;
		}
	this.___needSorting___ = function() {
		for (var i = 0; i < this.getNumChildren() - 1; i++)
		{
			var prevDepth = this.___GetDepth___(this.getChildAt(i));
			var nextDepth = this.___GetDepth___(this.getChildAt(i + 1));
			if (prevDepth < nextDepth)
				return true;
		}
		return false;
	}
	this.___sortFunction___ = function(obj1, obj2) {
		return (this.exportRoot.___GetDepth___(obj2) - this.exportRoot.___GetDepth___(obj1));
	}
	this.on('tick', function (event){
		var curTimeline = event.currentTarget;
		if (curTimeline.___needSorting___()){
			this.sortChildren(curTimeline.___sortFunction___);
		}
	});

	// timeline functions:
	this.frame_44 = function() {
		this.___loopingOver___ = true;
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(44).call(this.frame_44).wait(1));

	// Layer_4_obj_
	this.Layer_4 = new lib.Scene_1_Layer_4();
	this.Layer_4.name = "Layer_4";
	this.Layer_4.parent = this;
	this.Layer_4.setTransform(204.6,374.4,1,1,0,0,0,204.6,374.4);
	this.Layer_4.depth = 0;
	this.Layer_4.isAttachedToCamera = 0
	this.Layer_4.isAttachedToMask = 0
	this.Layer_4.layerDepth = 0
	this.Layer_4.layerIndex = 0
	this.Layer_4.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_4).wait(45));

	// Layer_7_obj_
	this.Layer_7 = new lib.Scene_1_Layer_7();
	this.Layer_7.name = "Layer_7";
	this.Layer_7.parent = this;
	this.Layer_7.setTransform(113.5,458.7,1,1,0,0,0,113.5,458.7);
	this.Layer_7.depth = 0;
	this.Layer_7.isAttachedToCamera = 0
	this.Layer_7.isAttachedToMask = 0
	this.Layer_7.layerDepth = 0
	this.Layer_7.layerIndex = 1
	this.Layer_7.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_7).wait(45));

	// Layer_8_obj_
	this.Layer_8 = new lib.Scene_1_Layer_8();
	this.Layer_8.name = "Layer_8";
	this.Layer_8.parent = this;
	this.Layer_8.setTransform(133.1,428.9,1,1,0,0,0,133.1,428.9);
	this.Layer_8.depth = 0;
	this.Layer_8.isAttachedToCamera = 0
	this.Layer_8.isAttachedToMask = 0
	this.Layer_8.layerDepth = 0
	this.Layer_8.layerIndex = 2
	this.Layer_8.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_8).wait(45));

	// Layer_13_obj_
	this.Layer_13 = new lib.Scene_1_Layer_13();
	this.Layer_13.name = "Layer_13";
	this.Layer_13.parent = this;
	this.Layer_13.setTransform(239.6,441.1,1,1,0,0,0,239.6,441.1);
	this.Layer_13.depth = 0;
	this.Layer_13.isAttachedToCamera = 0
	this.Layer_13.isAttachedToMask = 0
	this.Layer_13.layerDepth = 0
	this.Layer_13.layerIndex = 3
	this.Layer_13.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_13).wait(45));

	// Layer_10 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("AnoJJQAUlqBJlxIASmuIMngIIA7SRg");
	mask.setTransform(211.9,388.25);

	// Layer_11_obj_
	this.Layer_11 = new lib.Scene_1_Layer_11();
	this.Layer_11.name = "Layer_11";
	this.Layer_11.parent = this;
	this.Layer_11.setTransform(179.2,397.4,1,1,0,0,0,179.2,397.4);
	this.Layer_11.depth = 0;
	this.Layer_11.isAttachedToCamera = 0
	this.Layer_11.isAttachedToMask = 0
	this.Layer_11.layerDepth = 0
	this.Layer_11.layerIndex = 4
	this.Layer_11.maskLayerName = 0

	var maskedShapeInstanceList = [this.Layer_11];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.Layer_11).wait(45));

	// Layer_12_obj_
	this.Layer_12 = new lib.Scene_1_Layer_12();
	this.Layer_12.name = "Layer_12";
	this.Layer_12.parent = this;
	this.Layer_12.setTransform(114.8,443.8,1,1,0,0,0,114.8,443.8);
	this.Layer_12.depth = 0;
	this.Layer_12.isAttachedToCamera = 0
	this.Layer_12.isAttachedToMask = 0
	this.Layer_12.layerDepth = 0
	this.Layer_12.layerIndex = 5
	this.Layer_12.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_12).wait(45));

	// Layer_3_obj_
	this.Layer_3 = new lib.Scene_1_Layer_3();
	this.Layer_3.name = "Layer_3";
	this.Layer_3.parent = this;
	this.Layer_3.setTransform(362.1,26.1,1,1,0,0,0,362.1,26.1);
	this.Layer_3.depth = 0;
	this.Layer_3.isAttachedToCamera = 0
	this.Layer_3.isAttachedToMask = 0
	this.Layer_3.layerDepth = 0
	this.Layer_3.layerIndex = 6
	this.Layer_3.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_3).wait(45));

	// Layer_2_obj_
	this.Layer_2 = new lib.Scene_1_Layer_2();
	this.Layer_2.name = "Layer_2";
	this.Layer_2.parent = this;
	this.Layer_2.setTransform(37.5,239.3,1,1,0,0,0,37.5,239.3);
	this.Layer_2.depth = 0;
	this.Layer_2.isAttachedToCamera = 0
	this.Layer_2.isAttachedToMask = 0
	this.Layer_2.layerDepth = 0
	this.Layer_2.layerIndex = 7
	this.Layer_2.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_2).wait(45));

	// Layer_14_obj_
	this.Layer_14 = new lib.Scene_1_Layer_14();
	this.Layer_14.name = "Layer_14";
	this.Layer_14.parent = this;
	this.Layer_14.setTransform(167.2,649.6,1,1,0,0,0,167.2,649.6);
	this.Layer_14.depth = 0;
	this.Layer_14.isAttachedToCamera = 0
	this.Layer_14.isAttachedToMask = 0
	this.Layer_14.layerDepth = 0
	this.Layer_14.layerIndex = 8
	this.Layer_14.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_14).wait(45));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-155.6,397.2,1235.5,429.90000000000003);
// library properties:
lib.properties = {
	id: '02BB8AA354C1F648AE126FBFC1E86FA7',
	width: 375,
	height: 812,
	fps: 24,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: [
		{src:"assets/images/bgpr-2.png", id:"bg"},
		{src:"assets/images/_casepr-2.png", id:"_case"},
		{src:"assets/images/hand2pr-2.png", id:"hand2"},
		{src:"assets/images/packpr-2.png", id:"pack"}
	],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['02BB8AA354C1F648AE126FBFC1E86FA7'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}


// Layer depth API : 

AdobeAn.Layer = new function() {
	this.getLayerZDepth = function(timeline, layerName)
	{
		if(layerName === "Camera")
		layerName = "___camera___instance";
		var script = "if(timeline." + layerName + ") timeline." + layerName + ".depth; else 0;";
		return eval(script);
	}
	this.setLayerZDepth = function(timeline, layerName, zDepth)
	{
		const MAX_zDepth = 10000;
		const MIN_zDepth = -5000;
		if(zDepth > MAX_zDepth)
			zDepth = MAX_zDepth;
		else if(zDepth < MIN_zDepth)
			zDepth = MIN_zDepth;
		if(layerName === "Camera")
		layerName = "___camera___instance";
		var script = "if(timeline." + layerName + ") timeline." + layerName + ".depth = " + zDepth + ";";
		eval(script);
	}
	this.removeLayer = function(timeline, layerName)
	{
		if(layerName === "Camera")
		layerName = "___camera___instance";
		var script = "if(timeline." + layerName + ") timeline.removeChild(timeline." + layerName + ");";
		eval(script);
	}
	this.addNewLayer = function(timeline, layerName, zDepth)
	{
		if(layerName === "Camera")
		layerName = "___camera___instance";
		zDepth = typeof zDepth !== 'undefined' ? zDepth : 0;
		var layer = new createjs.MovieClip();
		layer.name = layerName;
		layer.depth = zDepth;
		layer.layerIndex = 0;
		timeline.addChild(layer);
	}
}


})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;

$(function () {
  var canvas, stage, exportRoot, anim_container, dom_overlay_container, fnStartAnimation;
  function init() {
    canvas = document.getElementById("canvaspr-2");
    anim_container = document.getElementById("animation_containerpr-2");
    dom_overlay_container = document.getElementById("dom_overlay_containerpr-2");
    var comp=AdobeAn.getComposition("02BB8AA354C1F648AE126FBFC1E86FA7");
    var lib=comp.getLibrary();
    var loader = new createjs.LoadQueue(false);
    loader.addEventListener("fileload", function(evt){handleFileLoad(evt,comp)});
    loader.addEventListener("complete", function(evt){handleComplete(evt,comp)});
    var lib=comp.getLibrary();
    loader.loadManifest(lib.properties.manifest);
  }
  function handleFileLoad(evt, comp) {
    var images=comp.getImages();
    if (evt && (evt.item.type == "image")) { images[evt.item.id] = evt.result; }
  }
  function handleComplete(evt,comp) {
    //This function is always called, irrespective of the content. You can use the variable "stage" after it is created in token create_stage.
    var lib=comp.getLibrary();
    var ss=comp.getSpriteSheet();
    var queue = evt.target;
    var ssMetadata = lib.ssMetadata;
    for(i=0; i<ssMetadata.length; i++) {
      ss[ssMetadata[i].name] = new createjs.SpriteSheet( {"images": [queue.getResult(ssMetadata[i].name)], "frames": ssMetadata[i].frames} )
    }
    var preloaderDiv = document.getElementById("_preload_div_pr");
    preloaderDiv.style.display = 'none';
    canvas.style.display = 'block';
    exportRoot = new lib.Prom1_vert();
    stage = new lib.Stage(canvas);
    //Registers the "tick" event listener.
    fnStartAnimation = function() {
      stage.addChild(exportRoot);
      createjs.Ticker.setFPS(lib.properties.fps);
      createjs.Ticker.addEventListener("tick", stage)
      stage.addEventListener("tick", handleTick)
      function getProjectionMatrix(container, totalDepth) {
        var focalLength = 528.25;
        var projectionCenter = { x : lib.properties.width/2, y : lib.properties.height/2 };
        var scale = (totalDepth + focalLength)/focalLength;
        var scaleMat = new createjs.Matrix2D;
        scaleMat.a = 1/scale;
        scaleMat.d = 1/scale;
        var projMat = new createjs.Matrix2D;
        projMat.tx = -projectionCenter.x;
        projMat.ty = -projectionCenter.y;
        projMat = projMat.prependMatrix(scaleMat);
        projMat.tx += projectionCenter.x;
        projMat.ty += projectionCenter.y;
        return projMat;
      }
      function handleTick(event) {
        var cameraInstance = exportRoot.___camera___instance;
        if(cameraInstance !== undefined && cameraInstance.pinToObject !== undefined)
        {
          cameraInstance.x = cameraInstance.pinToObject.x + cameraInstance.pinToObject.pinOffsetX;
          cameraInstance.y = cameraInstance.pinToObject.y + cameraInstance.pinToObject.pinOffsetY;
          if(cameraInstance.pinToObject.parent !== undefined && cameraInstance.pinToObject.parent.depth !== undefined)
            cameraInstance.depth = cameraInstance.pinToObject.parent.depth + cameraInstance.pinToObject.pinOffsetZ;
        }
        applyLayerZDepth(exportRoot);
      }
      function applyLayerZDepth(parent)
      {
        var cameraInstance = parent.___camera___instance;
        var focalLength = 528.25;
        var projectionCenter = { 'x' : 0, 'y' : 0};
        if(parent === exportRoot)
        {
          var stageCenter = { 'x' : lib.properties.width/2, 'y' : lib.properties.height/2 };
          projectionCenter.x = stageCenter.x;
          projectionCenter.y = stageCenter.y;
        }
        for(child in parent.children)
        {
          var layerObj = parent.children[child];
          if(layerObj == cameraInstance)
            continue;
          applyLayerZDepth(layerObj, cameraInstance);
          if(layerObj.layerDepth === undefined)
            continue;
          if(layerObj.currentFrame != layerObj.parent.currentFrame)
          {
            layerObj.gotoAndPlay(layerObj.parent.currentFrame);
          }
          var matToApply = new createjs.Matrix2D;
          var cameraMat = new createjs.Matrix2D;
          var totalDepth = layerObj.layerDepth ? layerObj.layerDepth : 0;
          var cameraDepth = 0;
          if(cameraInstance && !layerObj.isAttachedToCamera)
          {
            var mat = cameraInstance.getMatrix();
            mat.tx -= projectionCenter.x;
            mat.ty -= projectionCenter.y;
            cameraMat = mat.invert();
            cameraMat.prependTransform(projectionCenter.x, projectionCenter.y, 1, 1, 0, 0, 0, 0, 0);
            cameraMat.appendTransform(-projectionCenter.x, -projectionCenter.y, 1, 1, 0, 0, 0, 0, 0);
            if(cameraInstance.depth)
              cameraDepth = cameraInstance.depth;
          }
          if(layerObj.depth)
          {
            totalDepth = layerObj.depth;
          }
          //Offset by camera depth
          totalDepth -= cameraDepth;
          if(totalDepth < -focalLength)
          {
            matToApply.a = 0;
            matToApply.d = 0;
          }
          else
          {
            if(layerObj.layerDepth)
            {
              var sizeLockedMat = getProjectionMatrix(parent, layerObj.layerDepth);
              if(sizeLockedMat)
              {
                sizeLockedMat.invert();
                matToApply.prependMatrix(sizeLockedMat);
              }
            }
            matToApply.prependMatrix(cameraMat);
            var projMat = getProjectionMatrix(parent, totalDepth);
            if(projMat)
            {
              matToApply.prependMatrix(projMat);
            }
          }
          layerObj.transformMatrix = matToApply;
        }
      }
    }
    //Code to support hidpi screens and responsive scaling.
    function makeResponsive(isResp, respDim, isScale, scaleType) {
      var lastW, lastH, lastS=1;
      window.addEventListener('resize', resizeCanvas);
      resizeCanvas();
      function resizeCanvas() {
        var w = lib.properties.width, h = lib.properties.height;
        var iw = window.innerWidth, ih=window.innerHeight;
        var pRatio = window.devicePixelRatio || 1, xRatio=iw/w, yRatio=ih/h, sRatio=1;
        if(isResp) {
          if((respDim=='width'&&lastW==iw) || (respDim=='height'&&lastH==ih)) {
            sRatio = lastS;
          }
          else if(!isScale) {
            if(iw<w || ih<h)
              sRatio = Math.min(xRatio, yRatio);
          }
          else if(scaleType==1) {
            sRatio = Math.min(xRatio, yRatio);
          }
          else if(scaleType==2) {
            sRatio = Math.max(xRatio, yRatio);
          }
        }
        canvas.width = w*pRatio*sRatio;
        canvas.height = h*pRatio*sRatio;
        canvas.style.width = anim_container.style.width = dom_overlay_container.style.width = preloaderDiv.style.width = w*sRatio+'px';
        canvas.style.height = anim_container.style.height = dom_overlay_container.style.height = preloaderDiv.style.height = h*sRatio+'px';
        stage.scaleX = pRatio*sRatio;
        stage.scaleY = pRatio*sRatio;
        lastW = iw; lastH = ih; lastS = sRatio;
        stage.tickOnUpdate = false;
        stage.update();
        stage.tickOnUpdate = true;
      }
    }
    makeResponsive(true,'both',true,2);
    AdobeAn.compositionLoaded(lib.properties.id);
    fnStartAnimation();
  }
  init();
});