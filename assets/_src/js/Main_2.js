(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [];


// symbols:



(lib.fire = function() {
	this.initialize(img.fire);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,241,166);


(lib.mouth = function() {
	this.initialize(img.mouth);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,20,56);


(lib.pack_plita = function() {
	this.initialize(img.pack_plita);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,731,554);


(lib.teeth = function() {
	this.initialize(img.teeth);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,10,8);// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.Symbol13 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.fire();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol13, new cjs.Rectangle(0,0,241,166), null);


(lib.Symbol11 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 5
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(9.5,1,1).p("ABZg9QgTCKhNgQQgegGgbgfQgRgSgHgT");
	this.shape.setTransform(156.5,7.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(60));

	// Layer 4
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(9.5,1,1).p("AC9goQgYAEgxAIQg4AKg2ALQhkAUheAc");
	this.shape_1.setTransform(158,1.5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(9.5,1,1).p("Ai7AsQBdgdBjgWQA2gMA4gKQAxgKAYgE");
	this.shape_2.setTransform(158.1,1.9);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(9.5,1,1).p("Ai7AuQBdgeBkgXQA0gNA5gLQAxgKAYgE");
	this.shape_3.setTransform(158.3,2.2);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(9.5,1,1).p("Ai6AwQBcgfBjgYQA1gOA5gMQAwgKAYgE");
	this.shape_4.setTransform(158.4,2.5);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(9.5,1,1).p("Ai6AyQBcggBjgZQA1gOA4gNQAxgLAYgE");
	this.shape_5.setTransform(158.5,2.7);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(9.5,1,1).p("Ai5A0QBbghBjgbQA1gOA4gNQAxgLAXgF");
	this.shape_6.setTransform(158.6,2.9);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(9.5,1,1).p("Ai5A1QBbghBjgbQA1gPA4gOQAwgLAYgF");
	this.shape_7.setTransform(158.6,3.1);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(9.5,1,1).p("Ai5A2QBbgiBjgcQA0gOA5gOQAwgMAYgF");
	this.shape_8.setTransform(158.7,3.2);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(9.5,1,1).p("Ai4A2QBbgiBigbQA1gPA4gOQAwgMAYgF");
	this.shape_9.setTransform(158.7,3.2);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(9.5,1,1).p("AC6g1QgYAFgwAMQg4AOg1APQhiAbhbAi");
	this.shape_10.setTransform(158.7,3.3);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(9.5,1,1).p("Ai5AzQBbggBjgaQA1gOA4gNQAxgLAXgF");
	this.shape_11.setTransform(158.5,2.8);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(9.5,1,1).p("Ai6AxQBcggBjgYQA1gOA5gMQAwgKAYgF");
	this.shape_12.setTransform(158.4,2.5);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(9.5,1,1).p("Ai7AuQBdgeBjgXQA1gNA5gLQAxgKAYgE");
	this.shape_13.setTransform(158.3,2.2);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(9.5,1,1).p("Ai7AsQBdgdBkgWQA1gMA5gLQAwgJAYgE");
	this.shape_14.setTransform(158.2,2);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(9.5,1,1).p("Ai8ArQBdgdBkgVQA2gMA4gKQAxgJAYgE");
	this.shape_15.setTransform(158.1,1.8);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(9.5,1,1).p("Ai8AqQBdgcBlgVQA1gLA5gKQAxgJAYgE");
	this.shape_16.setTransform(158,1.6);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(9.5,1,1).p("Ai8ApQBegcBkgUQA2gMA4gJQAxgJAYgD");
	this.shape_17.setTransform(158,1.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1}]}).to({state:[{t:this.shape_1}]},15).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_10}]},25).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_1}]},1).wait(3));

	// Layer 2
	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#000000").ss(9.5,1,1).p("Ahag5QAhCHBKgXQAegJAYghQAPgTAFgU");
	this.shape_18.setTransform(25,8.3);

	this.timeline.addTween(cjs.Tween.get(this.shape_18).wait(60));

	// Layer 1
	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#000000").ss(9.5,1,1).p("AjNgWQA0ACAyAEQA5AEA2AGQBmALBgAS");
	this.shape_19.setTransform(20.6,2.1);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#000000").ss(9.5,1,1).p("AjNgYQA0ACAyAFQA5AFA2AGQBnAMBeAT");
	this.shape_20.setTransform(20.5,2.4);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#000000").ss(9.5,1,1).p("AjMgaQA0ADAyAFQA4AFA2AHQBmANBfAU");
	this.shape_21.setTransform(20.4,2.6);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#000000").ss(9.5,1,1).p("AjMgcQA0ADAyAGQA4AGA2AIQBmAOBfAU");
	this.shape_22.setTransform(20.4,2.9);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#000000").ss(9.5,1,1).p("AjMgeQA0AFAyAFQA4AHA2AIQBmAOBfAW");
	this.shape_23.setTransform(20.3,3);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#000000").ss(9.5,1,1).p("AjMgfQA0AEAyAGQA5AHA1AIQBmAQBfAW");
	this.shape_24.setTransform(20.2,3.2);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f().s("#000000").ss(9.5,1,1).p("AjMggQA0AFAyAGQA5AHA1AJQBmAPBfAX");
	this.shape_25.setTransform(20.2,3.3);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f().s("#000000").ss(9.5,1,1).p("AjMghQA0AFAyAGQA4AIA2AJQBmAQBfAX");
	this.shape_26.setTransform(20.2,3.4);
	this.shape_26._off = true;

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f().s("#000000").ss(9.5,1,1).p("AjMgfQA0AEAyAGQA4AHA2AIQBmAPBfAW");
	this.shape_27.setTransform(20.3,3.2);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f().s("#000000").ss(9.5,1,1).p("AjMgcQA0ADAyAGQA4AGA2AHQBmAOBfAV");
	this.shape_28.setTransform(20.4,2.9);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f().s("#000000").ss(9.5,1,1).p("AjNgZQA0ADAyAFQA5AFA2AGQBnAMBeAU");
	this.shape_29.setTransform(20.5,2.4);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f().s("#000000").ss(9.5,1,1).p("AjNgXQA0ACAyAEQA5AFA2AGQBnAMBfAS");
	this.shape_30.setTransform(20.5,2.3);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f().s("#000000").ss(9.5,1,1).p("AjNgXQA0ADAyAEQA5AEA2AHQBmALBgAS");
	this.shape_31.setTransform(20.6,2.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_19}]}).to({state:[{t:this.shape_19}]},15).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_26}]},25).to({state:[{t:this.shape_27}]},1).to({state:[{t:this.shape_28}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_29}]},1).to({state:[{t:this.shape_30}]},1).to({state:[{t:this.shape_31}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_19}]},1).wait(3));
	this.timeline.addTween(cjs.Tween.get(this.shape_26).wait(22).to({_off:false},0).wait(1).to({y:3.5},0).wait(26).to({_off:true},1).wait(10));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-4.7,-7.3,186.4,26.2);


(lib.Symbol10 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("AobCiIAAlDIQ2AAIAAFDg");
	mask.setTransform(33.9,-2.2);

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#F1214C").s().p("AgbBoQgngWgTguQgTguALgrQAMgrAkgOQAjgPAmAWQAmAWAUAuQATAtgLArQgMArgjAPQgOAGgPAAQgVAAgYgNg");
	this.shape.setTransform(-3.2,26.3);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#F1214C").s().p("AgVBoQgngSgVgrQgVgrAJgqQAKgrAigSQAigSAlASQAnARAVAsQAUAqgIAqQgJArgiATQgRAJgSAAQgSAAgTgJg");
	this.shape_1.setTransform(15.2,11.7);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#F1214C").s().p("AgQBoQgngNgWgoQgXgoAHgqQAIgrAggVQAggVAlANQAnANAXAoQAWAogGAqQgHArghAVQgTANgWAAQgOAAgPgFg");
	this.shape_2.setTransform(20.6,11.7);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#F1214C").s().p("AgKBoQgngJgYgkQgYglAEgqQAFgrAfgYQAegZAmAJQAmAJAZAlQAYAkgFAqQgEArgfAYQgWASgbAAQgJAAgKgCg");
	this.shape_3.setTransform(26,11.7);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#F1214C").s().p("AgEBoQgngEgZgiQgagiABgqQADgrAdgbQAdgbAlAEQAnAFAaAhQAaAhgCAqQgDArgdAcQgZAYggAAIgJgBg");
	this.shape_4.setTransform(31.4,11.7);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#F1214C").s().p("AhBBKQgbgfgBgrQABgqAbgeQAbgfAmAAQAmAAAcAfQAbAeAAAqQABArgcAeQgbAfgnAAQgmAAgbgeg");
	this.shape_5.setTransform(36.8,11.7);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#F1214C").s().p("Ag9BRQgdgbgDgrQgCgqAagiQAZgiAngEQAlgFAeAcQAdAbACArQADAqgaAiQgaAhgmAFIgJABQghAAgZgYg");
	this.shape_6.setTransform(42.2,11.7);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#F1214C").s().p("Ag5BZQgfgYgEgrQgFgrAYgkQAXglAngJQAmgJAfAYQAfAYAFArQAEAqgYAlQgYAlgmAJQgLADgIAAQgbAAgXgSg");
	this.shape_7.setTransform(47.6,11.7);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#F1214C").s().p("Ag1BhQgggVgHgrQgHgrAWgnQAWgpAngNQAmgNAgAVQAgAVAHAqQAIAqgXAoQgWAogmAOQgPAFgOAAQgWAAgUgMg");
	this.shape_8.setTransform(53,11.7);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#F1214C").s().p("AgxBoQgigSgJgrQgJgrAUgqQAUgrAngSQAmgSAiASQAiASAJArQAKAqgVArQgVArgmASQgTAJgSAAQgSAAgRgJg");
	this.shape_9.setTransform(58.4,11.7);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#F1214C").s().p("AgtBwQgjgPgMgrQgMgrATgtQATgvAmgWQAmgWAkAPQAkAOALArQAMArgTAtQgTAugmAXQgYANgVAAQgPAAgOgFg");
	this.shape_10.setTransform(63.8,11.7);
	this.shape_10._off = true;

	var maskedShapeInstanceList = [this.shape,this.shape_1,this.shape_2,this.shape_3,this.shape_4,this.shape_5,this.shape_6,this.shape_7,this.shape_8,this.shape_9,this.shape_10];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},28).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_10}]},1).wait(10));
	this.timeline.addTween(cjs.Tween.get(this.shape).wait(29).to({x:-2.8,y:25.9},0).wait(1).to({x:-1.7,y:24.7},0).wait(1).to({x:0.1,y:22.7},0).wait(1).to({x:2.6,y:19.8},0).wait(1).to({x:5.9,y:16.2},0).wait(1).to({x:9.8,y:11.7},0).to({_off:true},1).wait(25));
	this.timeline.addTween(cjs.Tween.get(this.shape_10).wait(44).to({_off:false},0).wait(1).to({x:67.7,y:16.2},0).wait(1).to({x:71,y:19.9},0).wait(1).to({x:73.5,y:22.8},0).wait(1).to({x:75.3,y:24.8},0).wait(1).to({x:76.4,y:26},0).wait(1).to({x:76.8,y:26.4},0).wait(10));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = null;


(lib.Symbol8 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(11.5,1,1).p("ABIC2QAujPjGic");
	this.shape.setTransform(8.1,18.2);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol8, new cjs.Rectangle(-5.7,-5.7,27.7,48), null);


(lib.Symbol7 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(11.5,1,1).p("AAKBnQBehbiWhy");
	this.shape.setTransform(4.7,10.3);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol7, new cjs.Rectangle(-5.7,-5.7,20.8,32.1), null);


(lib.Symbol6 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AgZAaQgLgLAAgPQAAgOALgMQALgLAOAAQAPAAALALQALAMAAAOQAAAPgLALQgLALgPAAQgOAAgLgLg");
	this.shape.setTransform(3.7,3.8);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol6, new cjs.Rectangle(0,0,7.5,7.5), null);


(lib.Symbol5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AgZAaQgLgLAAgPQAAgOALgMQALgLAOAAQAPAAALALQALAMAAAOQAAAPgLALQgLALgPAAQgOAAgLgLg");
	this.shape.setTransform(3.7,3.8);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol5, new cjs.Rectangle(0,0,7.5,7.5), null);


(lib.Symbol4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.mouth();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol4, new cjs.Rectangle(0,0,20,56), null);


(lib.Symbol3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.teeth();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol3, new cjs.Rectangle(0,0,10,8), null);


(lib.Symbol5_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Symbol 2
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(9,1,1).p("ABMBKQjNAHBKia");
	this.shape_1.setTransform(7.6,10.4);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(9,1,1).p("AgqhKQhaCFDKAQ");
	this.shape_2.setTransform(8,10.7);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(9,1,1).p("AgdhLQhqBzDJAk");
	this.shape_3.setTransform(8.3,10.9);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(9,1,1).p("AgShMQh2BjDHA2");
	this.shape_4.setTransform(8.5,11.1);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(9,1,1).p("AgIhNQiBBUDFBH");
	this.shape_5.setTransform(8.7,11.3);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(9,1,1).p("AAAhOQiLBKDFBT");
	this.shape_6.setTransform(8.9,11.4);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(9,1,1).p("AAHhPQiSBBDDBe");
	this.shape_7.setTransform(8.9,11.5);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(9,1,1).p("AAMhPQiYA4DDBn");
	this.shape_8.setTransform(9,11.6);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(9,1,1).p("AAQhPQicAzDCBs");
	this.shape_9.setTransform(9.1,11.7);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(9,1,1).p("AAShQQieAwDBBx");
	this.shape_10.setTransform(9.1,11.7);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(9,1,1).p("AA1BRQjBhyCfgv");
	this.shape_11.setTransform(9.1,11.7);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(9,1,1).p("AAEhOQiPBDDEBa");
	this.shape_12.setTransform(8.9,11.5);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(9,1,1).p("AgThMQh1BlDHA0");
	this.shape_13.setTransform(8.5,11.1);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(9,1,1).p("AglhLQhgCADKAX");
	this.shape_14.setTransform(8.1,10.8);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(9,1,1).p("AgshKQhYCJDLAM");
	this.shape_15.setTransform(7.9,10.6);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(9,1,1).p("AgxhKQhSCRDMAE");
	this.shape_16.setTransform(7.8,10.6);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(9,1,1).p("Ag0hJQhOCWDMgD");
	this.shape_17.setTransform(7.7,10.5);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#000000").ss(9,1,1).p("Ag2hJQhMCZDNgG");
	this.shape_18.setTransform(7.6,10.4);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1}]}).to({state:[{t:this.shape_1}]},9).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_11}]},16).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_1}]},1).wait(15));

	// Symbol 3
	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#000000").ss(9,1,1).p("AAtBgQiUhnBghY");
	this.shape_19.setTransform(12.3,9.6);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#000000").ss(9,1,1).p("AAAhaQhoBPCUBm");
	this.shape_20.setTransform(12.5,10);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#000000").ss(9,1,1).p("AAHhVQhxBFCUBm");
	this.shape_21.setTransform(12.6,10.5);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#000000").ss(9,1,1).p("AAOhSQh5A+CUBn");
	this.shape_22.setTransform(12.7,10.9);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#000000").ss(9,1,1).p("AAThOQh/A3CUBm");
	this.shape_23.setTransform(12.8,11.2);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#000000").ss(9,1,1).p("AAYhLQiFAxCUBm");
	this.shape_24.setTransform(12.9,11.5);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f().s("#000000").ss(9,1,1).p("AAchJQiJAtCUBm");
	this.shape_25.setTransform(13,11.7);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f().s("#000000").ss(9,1,1).p("AAfhHQiNApCUBm");
	this.shape_26.setTransform(13,11.9);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f().s("#000000").ss(9,1,1).p("AAhhGQiPAnCUBm");
	this.shape_27.setTransform(13.1,12);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f().s("#000000").ss(9,1,1).p("AAihFQiQAlCUBm");
	this.shape_28.setTransform(13.1,12.1);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f().s("#000000").ss(9,1,1).p("AAmBGQiUhmCRgl");
	this.shape_29.setTransform(13.1,12.1);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f().s("#000000").ss(9,1,1).p("AAahKQiHAvCUBm");
	this.shape_30.setTransform(13,11.6);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f().s("#000000").ss(9,1,1).p("AAMhSQh3A/CUBm");
	this.shape_31.setTransform(12.7,10.8);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f().s("#000000").ss(9,1,1).p("AAChYQhrBLCUBm");
	this.shape_32.setTransform(12.5,10.2);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f().s("#000000").ss(9,1,1).p("AgBhbQhnBQCUBn");
	this.shape_33.setTransform(12.5,10);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f().s("#000000").ss(9,1,1).p("AgDhcQhkBTCUBm");
	this.shape_34.setTransform(12.4,9.8);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f().s("#000000").ss(9,1,1).p("AgFheQhiBXCUBm");
	this.shape_35.setTransform(12.3,9.7);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f().s("#000000").ss(9,1,1).p("AgHheQhgBXCUBm");
	this.shape_36.setTransform(12.3,9.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_19}]}).to({state:[{t:this.shape_19}]},9).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_27}]},1).to({state:[{t:this.shape_28}]},1).to({state:[{t:this.shape_29}]},1).to({state:[{t:this.shape_29}]},16).to({state:[{t:this.shape_30}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_31}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_32}]},1).to({state:[{t:this.shape_33}]},1).to({state:[{t:this.shape_34}]},1).to({state:[{t:this.shape_35}]},1).to({state:[{t:this.shape_36}]},1).to({state:[{t:this.shape_19}]},1).wait(15));

	// Symbol 4
	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f().s("#000000").ss(9,1,1).p("AgZBAQBWhXg8go");
	this.shape_37.setTransform(19,14.8);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f().s("#000000").ss(9,1,1).p("AgYA+QBWhWg/gl");
	this.shape_38.setTransform(19,14.9);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f().s("#000000").ss(9,1,1).p("AgYA9QBWhWhCgj");
	this.shape_39.setTransform(18.9,15);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f().s("#000000").ss(9,1,1).p("AgXA9QBWhXhFgi");
	this.shape_40.setTransform(18.9,15.1);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f().s("#000000").ss(9,1,1).p("AgXA8QBWhXhGgg");
	this.shape_41.setTransform(18.8,15.1);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f().s("#000000").ss(9,1,1).p("AgXA7QBWhXhIge");
	this.shape_42.setTransform(18.8,15.2);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f().s("#000000").ss(9,1,1).p("AgWA7QBWhXhKge");
	this.shape_43.setTransform(18.8,15.3);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f().s("#000000").ss(9,1,1).p("AgWA6QBWhXhMgc");
	this.shape_44.setTransform(18.8,15.3);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f().s("#000000").ss(9,1,1).p("AgWA6QBWhXhNgc");
	this.shape_45.setTransform(18.8,15.3);
	this.shape_45._off = true;

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.f().s("#000000").ss(9,1,1).p("AgXA9QBWhXhEgi");
	this.shape_46.setTransform(18.9,15.1);

	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.f().s("#000000").ss(9,1,1).p("AgYA+QBWhXhAgk");
	this.shape_47.setTransform(18.9,14.9);

	this.shape_48 = new cjs.Shape();
	this.shape_48.graphics.f().s("#000000").ss(9,1,1).p("AgYA/QBWhXg/gm");
	this.shape_48.setTransform(19,14.9);

	this.shape_49 = new cjs.Shape();
	this.shape_49.graphics.f().s("#000000").ss(9,1,1).p("AgYA/QBWhXg+gm");
	this.shape_49.setTransform(19,14.8);

	this.shape_50 = new cjs.Shape();
	this.shape_50.graphics.f().s("#000000").ss(9,1,1).p("AgZA/QBWhWg9gn");
	this.shape_50.setTransform(19,14.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_37}]}).to({state:[{t:this.shape_37}]},9).to({state:[{t:this.shape_38}]},1).to({state:[{t:this.shape_39}]},1).to({state:[{t:this.shape_40}]},1).to({state:[{t:this.shape_41}]},1).to({state:[{t:this.shape_42}]},1).to({state:[{t:this.shape_43,p:{y:15.3}}]},1).to({state:[{t:this.shape_44}]},1).to({state:[{t:this.shape_45}]},1).to({state:[{t:this.shape_45}]},1).to({state:[{t:this.shape_45}]},1).to({state:[{t:this.shape_45}]},16).to({state:[{t:this.shape_43,p:{y:15.2}}]},1).to({state:[{t:this.shape_41}]},1).to({state:[{t:this.shape_46}]},1).to({state:[{t:this.shape_39}]},1).to({state:[{t:this.shape_47}]},1).to({state:[{t:this.shape_48}]},1).to({state:[{t:this.shape_49}]},1).to({state:[{t:this.shape_50}]},1).to({state:[{t:this.shape_37}]},1).to({state:[{t:this.shape_37}]},1).wait(15));
	this.timeline.addTween(cjs.Tween.get(this.shape_45).wait(17).to({_off:false},0).wait(1).to({x:18.7,y:15.4},0).wait(17).to({_off:true},1).wait(24));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-4.5,-4.5,30.6,30.1);


(lib.Symbol14 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.Symbol13();
	this.instance.parent = this;
	this.instance.setTransform(120.5,109,1,1,0,0,0,120.5,109);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({scaleY:0.95},4).to({scaleY:1},3).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,241,166);


(lib.Symbol12 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Symbol 5
	this.instance = new lib.Symbol5_1();
	this.instance.parent = this;
	this.instance.setTransform(15.6,44.9,1,1,0,0,0,15.6,22.9);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(9).to({regY:22.8,rotation:30,x:10.6,y:17.4},10,cjs.Ease.get(1)).wait(16).to({regY:22.9,rotation:0,x:15.6,y:44.9},10,cjs.Ease.get(1)).wait(15));

	// Layer 3
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(9,1,1).p("ADmmEQAWEdg6DRQg0C7hfBEQhbBBhQhSQhWhbgZjk");
	this.shape.setTransform(38.8,38.9);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(9,1,1).p("AjrgBQAKDhBLBfQBEBZBdg4QBfg4A/i2QBHjMgEkb");
	this.shape_1.setTransform(38,37);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(9,1,1).p("AjugZQgCDfA/BjQA7BeBeguQBfgvBJiyQBTjGAMkZ");
	this.shape_2.setTransform(37.5,35.3);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(9,1,1).p("AjwguQgNDdA2BmQAzBjBegmQBfgnBSitQBdjBAbkZ");
	this.shape_3.setTransform(36.9,33.9);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(9,1,1).p("AjvhBQgXDcAuBpQArBnBfgfQBfggBaipQBmi9AnkY");
	this.shape_4.setTransform(36.3,32.6);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(9,1,1).p("AjuhRQgfDaAmBsQAmBqBfgZQBggaBgimQBti6AykW");
	this.shape_5.setTransform(35.7,31.6);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(9,1,1).p("AjshfQgmDaAhBuQAgBsBggUQBggUBlikQBzi3A7kV");
	this.shape_6.setTransform(35.1,30.8);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(9,1,1).p("AjrhqQgrDZAdBwQAcBvBggRQBggQBqiiQB3i1BCkU");
	this.shape_7.setTransform(34.6,30.2);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(9,1,1).p("AjphxQgvDYAZBxQAZBwBhgNQBggOBsihQB8izBGkU");
	this.shape_8.setTransform(34.2,29.7);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(9,1,1).p("Ajoh2QgyDYAYByQAYBxBggMQBhgMBuigQB9iyBJkU");
	this.shape_9.setTransform(34,29.5);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(9,1,1).p("AELk4QhKEUh+CxQhuCghhALQhgAMgXhyQgXhyAyjX");
	this.shape_10.setTransform(33.9,29.4);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(9,1,1).p("AjthaQgkDZAjBuQAiBrBggVQBggXBjikQBxi4A4kW");
	this.shape_11.setTransform(35.3,31.1);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(9,1,1).p("AjvgrQgMDdA3BmQA0BiBfgnQBegoBRiuQBcjCAZkY");
	this.shape_12.setTransform(37,34.1);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(9,1,1).p("AjsgJQAGDgBHBhQBBBaBeg0QBeg2BCi0QBMjJACkb");
	this.shape_13.setTransform(37.8,36.4);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(9,1,1).p("AjrADQANDiBMBeQBHBYBcg5QBfg7A9i2QBFjNgGkb");
	this.shape_14.setTransform(38.1,37.3);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(9,1,1).p("AjrANQASDjBRBcQBLBVBbg8QBfg/A5i4QBBjPgNkc");
	this.shape_15.setTransform(38.4,38);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(9,1,1).p("AjrAUQAWDjBUBcQBNBTBcg/QBehBA3i6QA9jQgSkd");
	this.shape_16.setTransform(38.6,38.5);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(9,1,1).p("AjrAXQAYDkBWBbQBPBTBbhBQBfhDA0i7QA8jRgVkd");
	this.shape_17.setTransform(38.8,38.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},9).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_10}]},16).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape}]},1).wait(15));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-4.5,-4.5,71.5,86.9);


(lib.Symbol9 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.Symbol7();
	this.instance.parent = this;
	this.instance.setTransform(14.2,10.6,1,1,0,0,0,4.7,10.3);

	this.instance_1 = new lib.Symbol8();
	this.instance_1.parent = this;
	this.instance_1.setTransform(8.1,18.2,1,1,0,0,0,8.1,18.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_1},{t:this.instance}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol9, new cjs.Rectangle(-5.7,-5.7,30.3,48), null);


(lib.Symbol2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 4 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("AjGJ+QgEizAorkIpkqcQYMAFABABIjmOyIjmOzIAAAAIoBk4g");
	mask.setTransform(26.2,39);

	// Layer 3
	this.instance = new lib.Symbol9();
	this.instance.parent = this;
	this.instance.setTransform(36.6,13,1,1,0,0,0,13.8,32.3);

	var maskedShapeInstanceList = [this.instance];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(19).to({rotation:-15,x:33.6,y:16},6,cjs.Ease.get(-1)).to({rotation:0,x:36.6,y:13},3,cjs.Ease.get(1)).wait(1).to({rotation:-15,x:33.6,y:16},4).to({rotation:0,x:36.6,y:13},4).wait(23));

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(11.5,1,1).p("Ajnj/QBSHDB6CzQAzBMAzANQAwAMAmgtQBThggNj0QgPkTiMka");
	this.shape.setTransform(23.2,46.7);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(11.5,1,1).p("Ajnj/QBSHCB6C0QAzBMAzAMQAwANAmgtQBThhgNjzQgPkTiOkZ");
	this.shape_1.setTransform(23.2,46.7);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(11.5,1,1).p("AjnkAQBSHCB6C0QAzBMAzAMQAwANAmgtQBThhgNjzQgPkTiUkX");
	this.shape_2.setTransform(23.2,46.8);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(11.5,1,1).p("AjnkCQBSHCB6C0QAzBMAzAMQAwANAmgtQBThhgNjzQgPkTidkT");
	this.shape_3.setTransform(23.2,47);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(11.5,1,1).p("AjnkFQBSHDB6CzQAzBMAzANQAwAMAmgtQBThggNj0QgPkTiqkN");
	this.shape_4.setTransform(23.2,47.3);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(11.5,1,1).p("AjnkJQBSHDB6CzQAzBMAzANQAwAMAmgtQBThggNj0QgPkTi7kG");
	this.shape_5.setTransform(23.2,47.7);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(11.5,1,1).p("AjnkNQBSHDB6CzQAzBMAzANQAwAMAmgtQBThggNj0QgPkTjQj9");
	this.shape_6.setTransform(23.2,48.1);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(11.5,1,1).p("AjnkGQBSHDB6CzQAzBMAzANQAwAMAmgtQBThggNj0QgPkTiukL");
	this.shape_7.setTransform(23.2,47.4);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(11.5,1,1).p("AjnkJQBSHCB6C0QAzBMAzAMQAwANAmgtQBThhgNjzQgPkTi/kF");
	this.shape_8.setTransform(23.2,47.7);

	var maskedShapeInstanceList = [this.shape,this.shape_1,this.shape_2,this.shape_3,this.shape_4,this.shape_5,this.shape_6,this.shape_7,this.shape_8];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},19).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape}]},1).wait(23));
	this.timeline.addTween(cjs.Tween.get(this.shape).wait(19).to({_off:true},1).wait(8).to({_off:false},0).wait(1).to({_off:true},1).wait(7).to({_off:false},0).wait(23));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-5.7,-25.1,58,124.2);


(lib.Symbol1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 3
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#C8C8C8").s().p("ABjAfQgYghgKgFQgLgGgZgKQgZgIgUgCQgTgCgzAQQgzAQA2gbQA0gdA7AYQA7AXARAmQAKAVgCAAQgCAAgLgQg");
	this.shape.setTransform(674,206.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#C8C8C8").s().p("ACEBGQgXgNgYgRQgXgPhKgIQhMgHgdANQgfAOAIgeQAIgfA5gfQA5gdA7AXQA7AXAbA8QAYAygNAAIgGgCg");
	this.shape_1.setTransform(672.7,209.3);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#C8C8C8").s().p("ABTCCQgngZg5gdQg4gegwgBQgwgCAVhBQAVhCA5gfQA5geA7AYQA7AXAbA+QAcA+gWBCQgQAxgYAAQgJAAgKgHg");
	this.shape_2.setTransform(671.8,215.8);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#C8C8C8").s().p("ABNCOIhsgRQg/gJgjgoQgkgnAWhBQAVhCA5gfQA4geA7AYQA8AXAbA+QAbA+gWBCQgTA9gmAAIgIgBg");
	this.shape_3.setTransform(671.7,216.4);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#C8C8C8").s().p("AgxCgQg9gXgbhAQgbg/AWhBQAVhCA5gfQA4geA7AYQA8AXAbA+QAbA/gWBBQgVBDg5AeQggASghAAQgYAAgZgKg");
	this.shape_4.setTransform(671.6,219.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape}]},19).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape}]},1).to({state:[]},1).wait(31));

	// Layer 4
	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#C8C8C8").s().p("ABgAbQgYgegLgFQgLgGgYgHQgagHgRgBQgUgBgxATQgwASAygdQAxgeA7ASQA6ATAVAlQAKAUgDAAQgCAAgMgPg");
	this.shape_5.setTransform(639.7,196.9);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#C8C8C8").s().p("ACCBAQgYgLgXgPQgYgPhIgCQhIgDgeAQQgdAPAHgeQAHgeA1ghQA0ggA7ASQA6ATAfA5QAZAwgNAAIgFgCg");
	this.shape_6.setTransform(638.7,199.3);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#C8C8C8").s().p("ABXB8QgpgWg2gYQg6gagtACQgvACAPhBQAShCA1ghQA1ggA6ATQA7ASAeA7QAeA7gRBCQgMAwgYAAQgJAAgJgFg");
	this.shape_7.setTransform(638.2,205.6);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#C8C8C8").s().p("ABSCHIhpgKQg+gFgkgkQglgjAQhBQAShBA1ghQA0ggA7ASQA6ATAfA6QAeA7gRBCQgQA9gnABIgFgBg");
	this.shape_8.setTransform(638.1,206.3);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#C8C8C8").s().p("AgmCdQg7gTgfg7Qgdg7APhBQAShBA1ghQA1ggA6ASQA7ATAeA6QAeA8gRBBQgQBBg2AgQgiAWgkAAQgTAAgVgHg");
	this.shape_9.setTransform(638.1,209.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_5}]},19).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_5}]},1).to({state:[]},1).wait(31));

	// Layer 5
	this.instance = new lib.Symbol5();
	this.instance.parent = this;
	this.instance.setTransform(674.9,214.3,1.456,1.456,23.7,0,0,3.8,3.8);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(9).to({regX:3.7,x:664.8,y:222.1},5,cjs.Ease.get(1)).wait(21).to({regX:3.8,x:674.9,y:214.3},5,cjs.Ease.get(1)).wait(20));

	// Layer 6
	this.instance_1 = new lib.Symbol6();
	this.instance_1.parent = this;
	this.instance_1.setTransform(642.5,206.3,1.411,1.411,20.2,0,0,3.7,3.6);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(9).to({regX:3.8,regY:3.7,x:633.2,y:214.2},5,cjs.Ease.get(1)).wait(21).to({regX:3.7,regY:3.6,x:642.5,y:206.3},5,cjs.Ease.get(1)).wait(20));

	// Layer 7
	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("Ag4CcQg7gagYg/QgXg/AXhBQAZhAA5gcQA5gbA6AZQA5AaAaA/QAYA/gbBAQgWBCg6AbQgeAOgdAAQgcAAgbgMg");
	this.shape_10.setTransform(671.8,219);

	this.timeline.addTween(cjs.Tween.get(this.shape_10).wait(60));

	// Layer 8
	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("AgsCbQg6gWgdg8Qgag9ATg+QAUhAA2gfQA2geA5AWQA5AUAcA8QAbA9gUA+QgTBBg3AeQggASghAAQgWAAgWgIg");
	this.shape_11.setTransform(638.1,209);

	this.timeline.addTween(cjs.Tween.get(this.shape_11).wait(60));

	// Layer 1
	this.instance_2 = new lib.Symbol3();
	this.instance_2.parent = this;
	this.instance_2.setTransform(642.9,246.5,1.451,1.492,0,12.9,13.7,5,4.1);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(14).to({x:643.4,y:244.4},0).to({regY:4,x:641,y:255.1},4).to({x:644.5,y:240.1},3).to({regY:4.1,x:644.4,y:240.8},1).to({regY:4,x:644.5,y:240.1},1).to({regY:4.1,x:644.4,y:240.8},1).to({regY:4,x:644.5,y:240.1},1).to({regY:4.1,x:644.4,y:240.8},1).to({regY:4,x:644.5,y:240.1},1).to({regY:4.1,x:644.4,y:240.8},1).to({regY:4,x:644.5,y:240.1},1).to({regY:4.1,x:644.4,y:240.8},1).to({regY:4,x:644.5,y:240.1},1).to({regY:4.1,x:642.9,y:246.5},4,cjs.Ease.get(1)).wait(25));

	// Layer 2
	this.instance_3 = new lib.Symbol4();
	this.instance_3.parent = this;
	this.instance_3.setTransform(638.9,275.3,1.451,1.39,0,12.9,13.7,9.9,27.9);

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(14).to({regX:9.8,scaleY:1.49,x:638.8,y:275.2},0).to({regX:10,scaleY:1.02,x:639.1},4).to({scaleY:1.69},3).to({regX:9.8,scaleX:1.45,scaleY:1.67,skewX:12.8,skewY:13.6,x:638.8,y:275.3},1).to({regX:10,scaleX:1.45,scaleY:1.69,skewX:12.9,skewY:13.7,x:639.1,y:275.2},1).to({regX:9.8,scaleX:1.45,scaleY:1.67,skewX:12.8,skewY:13.6,x:638.8,y:275.3},1).to({regX:10,scaleX:1.45,scaleY:1.69,skewX:12.9,skewY:13.7,x:639.1,y:275.2},1).to({regX:9.8,scaleX:1.45,scaleY:1.67,skewX:12.8,skewY:13.6,x:638.8,y:275.3},1).to({regX:10,scaleX:1.45,scaleY:1.69,skewX:12.9,skewY:13.7,x:639.1,y:275.2},1).to({regX:9.8,scaleX:1.45,scaleY:1.67,skewX:12.8,skewY:13.6,x:638.8,y:275.3},1).to({regX:10,scaleX:1.45,scaleY:1.69,skewX:12.9,skewY:13.7,x:639.1,y:275.2},1).to({regX:9.8,scaleX:1.45,scaleY:1.67,skewX:12.8,skewY:13.6,x:638.8,y:275.3},1).to({regX:10,scaleX:1.45,scaleY:1.69,skewX:12.9,skewY:13.7,x:639.1,y:275.2},1).to({regX:9.9,scaleY:1.39,x:638.9,y:275.3},4,cjs.Ease.get(1)).wait(25));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(616.3,192.7,70.8,124.2);


// stage content:
(lib.Main_2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 11
	this.instance = new lib.Symbol14();
	this.instance.parent = this;
	this.instance.setTransform(886.5,491,1,1,0,0,0,120.5,83);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	// Layer 9
	this.instance_1 = new lib.Symbol11();
	this.instance_1.parent = this;
	this.instance_1.setTransform(811.8,559.8,1,1,0,0,0,20.6,2.3);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(1));

	// Layer 8
	this.instance_2 = new lib.Symbol10();
	this.instance_2.parent = this;
	this.instance_2.setTransform(856.7,590.6,1,1,0,0,0,9.8,11.7);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(1));

	// Layer 7
	this.instance_3 = new lib.Symbol2();
	this.instance_3.parent = this;
	this.instance_3.setTransform(720.4,389.1,1,1,0,0,0,23.3,46.6);

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(1));

	// Layer 6
	this.instance_4 = new lib.Symbol12();
	this.instance_4.parent = this;
	this.instance_4.setTransform(611.6,403.9,1.4,1.4,0,0,180,31.2,38.9);

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(1));

	// Layer 4
	this.instance_5 = new lib.Symbol1();
	this.instance_5.parent = this;
	this.instance_5.setTransform(640.3,306.6,1,1,0,12.4,-167.6,673.3,254.8);

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(1));

	// Layer 2
	this.instance_6 = new lib.pack_plita();
	this.instance_6.parent = this;
	this.instance_6.setTransform(349,126);

	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(540,340,1080,680);
// library properties:
lib.properties = {
	id: '363BBB0E3EF9A9419B12734A0734D24A',
	width: 1080,
	height: 680,
	fps: 30,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: [
		{src:"assets/images/fire18.png", id:"fire"},
		{src:"assets/images/mouth18.png", id:"mouth"},
		{src:"assets/images/pack_plita18.png", id:"pack_plita"},
		{src:"assets/images/teeth18.png", id:"teeth"}
	],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['363BBB0E3EF9A9419B12734A0734D24A'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}



})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;

$(function () {
    var canvas, stage, exportRoot, anim_container, dom_overlay_container, fnStartAnimation;
    function init() {
        canvas = document.getElementById("canvas18");
        anim_container = document.getElementById("animation_container18");
        dom_overlay_container = document.getElementById("dom_overlay_container18");
        var comp=AdobeAn.getComposition("363BBB0E3EF9A9419B12734A0734D24A");
        var lib=comp.getLibrary();
        var loader = new createjs.LoadQueue(false);
        loader.addEventListener("fileload", function(evt){handleFileLoad(evt,comp)});
        loader.addEventListener("complete", function(evt){handleComplete(evt,comp)});
        var lib=comp.getLibrary();
        loader.loadManifest(lib.properties.manifest);
    }
    function handleFileLoad(evt, comp) {
        var images=comp.getImages();
        if (evt && (evt.item.type == "image")) { images[evt.item.id] = evt.result; }
    }
    function handleComplete(evt,comp) {
        //This function is always called, irrespective of the content. You can use the variable "stage" after it is created in token create_stage.
        var lib=comp.getLibrary();
        var ss=comp.getSpriteSheet();
        var queue = evt.target;
        var ssMetadata = lib.ssMetadata;
        for(i=0; i<ssMetadata.length; i++) {
            ss[ssMetadata[i].name] = new createjs.SpriteSheet( {"images": [queue.getResult(ssMetadata[i].name)], "frames": ssMetadata[i].frames} )
        }
        exportRoot = new lib.Main_2();
        stage = new lib.Stage(canvas);
        //Registers the "tick" event listener.
        fnStartAnimation = function() {
            stage.addChild(exportRoot);
            createjs.Ticker.setFPS(lib.properties.fps);
            createjs.Ticker.addEventListener("tick", stage);
        }
        //Code to support hidpi screens and responsive scaling.
        function makeResponsive(isResp, respDim, isScale, scaleType) {
            var lastW, lastH, lastS=1;
            window.addEventListener('resize', resizeCanvas);
            resizeCanvas();
            function resizeCanvas() {
                var w = lib.properties.width, h = lib.properties.height;
                var iw = window.innerWidth, ih=window.innerHeight;
                var pRatio = window.devicePixelRatio || 1, xRatio=iw/w, yRatio=ih/h, sRatio=1;
                if(isResp) {
                    if((respDim=='width'&&lastW==iw) || (respDim=='height'&&lastH==ih)) {
                        sRatio = lastS;
                    }
                    else if(!isScale) {
                        if(iw<w || ih<h)
                            sRatio = Math.min(xRatio, yRatio);
                    }
                    else if(scaleType==1) {
                        sRatio = Math.min(xRatio, yRatio);
                    }
                    else if(scaleType==2) {
                        sRatio = Math.max(xRatio, yRatio);
                    }
                }
                canvas.width = w*pRatio*sRatio;
                canvas.height = h*pRatio*sRatio;
                canvas.style.width = dom_overlay_container.style.width = anim_container.style.width =  w*sRatio+'px';
                canvas.style.height = anim_container.style.height = dom_overlay_container.style.height = h*sRatio+'px';
                stage.scaleX = pRatio*sRatio;
                stage.scaleY = pRatio*sRatio;
                lastW = iw; lastH = ih; lastS = sRatio;
                stage.tickOnUpdate = false;
                stage.update();
                stage.tickOnUpdate = true;
            }
        }
        makeResponsive(true,'both',true,2);
        AdobeAn.compositionLoaded(lib.properties.id);
        fnStartAnimation();
    }
    init();
});