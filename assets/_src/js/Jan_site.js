(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [];


// symbols:



(lib.bg = function() {
	this.initialize(img.bg);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,1080,680);


(lib.pack = function() {
	this.initialize(img.pack);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,360,496);


(lib.star_lights = function() {
	this.initialize(img.star_lights);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,301,304);// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.Symbol17copy4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("A5eg1IKW2TIekBnIKDUSIl1VlMgjYACzgAkZwJIrdG/IivFLIhCEIIHKPzIBrBaIBUA+IRJgTIDzhuICPigIEFwTIg5irIqFp6IoQhog");
	mask.setTransform(148.325,150.025);

	// Layer_1
	this.instance = new lib.star_lights();
	this.instance.parent = this;

	var maskedShapeInstanceList = [this.instance];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol17copy4, new cjs.Rectangle(0,2,301,296.1), null);


(lib.Symbol17copy3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("Ap3QjIhqhaInLvyIBCkJICwlLILdm/IC8gkIIQBoIKFJ6IA5CrIkEQTIiPCgIj0BuIxJATgAiVs1IpiFkIiRGpID0LwID4C7IO5gJICDhoIBah+ICPrvIg6i8IoInLIlChrg");
	mask.setTransform(142.4,155.125);

	// Layer_1
	this.instance = new lib.star_lights();
	this.instance.parent = this;

	var maskedShapeInstanceList = [this.instance];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol17copy3, new cjs.Rectangle(22.7,43,239.40000000000003,224.3), null);


(lib.Symbol17copy2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("AqUKuIj0rxICRmoIJillICagXIFCBqIIIHMIA6C8IiPLvIhaB+IiDBoIu5AIgAjpnWIj4B9IhxB3IgeDGICPGnIB2CoICfBcIJAgjIB/g/IBbimIAwklIASimIgjiDIiKh4IkZjfIjwgrg");
	mask.setTransform(142.4,157.775);

	// Layer_1
	this.instance = new lib.star_lights();
	this.instance.parent = this;

	var maskedShapeInstanceList = [this.instance];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol17copy2, new cjs.Rectangle(51.9,70.6,181,174.4), null);


(lib.Symbol17copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("Al7IQIh2ioIiOmnIAdjFIBxh3ID4h9IDDhzIDxArIEYDeICKB4IAjCEIgSCmIgvEkIhcCmIh/A/Io/AjgAi4lMIiGBzIgsBnQgCAGgEBLIgDBNQgBADAYBWIAYBVIBVBjIB8BEICpAJICTgjIBvhHIAph6IgRhdIAMhMIAdhoIgxhSIiAhUIhvhbIiEgPg");
	mask.setTransform(143.975,161.225);

	// Layer_1
	this.instance = new lib.star_lights();
	this.instance.parent = this;

	var maskedShapeInstanceList = [this.instance];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol17copy, new cjs.Rectangle(80,99.2,128,124.10000000000001), null);


(lib.Symbol17 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("Ah1F6Ih8hEIhVhjIgYhVQgYhVABgDIADhNQADhMADgGIAshnICGhzICNgvICEAPIBvBbICABVIAxBSIgdBnIgMBMIARBdIgpB7IhvBGIiTAjg");
	mask.setTransform(144.1995,161.9);

	// Layer_1
	this.instance = new lib.star_lights();
	this.instance.parent = this;

	var maskedShapeInstanceList = [this.instance];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol17, new cjs.Rectangle(106.8,123.2,74.89999999999999,77.39999999999999), null);


(lib.Symbol13 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(12.5,1,1).p("AAkg1IhIBs");
	this.shape.setTransform(3.65,5.45);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol13, new cjs.Rectangle(-6.2,-6.2,19.8,23.4), null);


(lib.Symbol11 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AgvAwQgTgUAAgcQAAgbATgUQAUgUAbAAQAcAAAUAUQATAUAAAbQAAAcgTAUQgUATgcABQgbgBgUgTg");
	this.shape.setTransform(6.725,6.75);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol11, new cjs.Rectangle(0,0,13.5,13.5), null);


(lib.Symbol10 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AgvAwQgTgUAAgcQAAgbATgUQAUgUAbAAQAcAAAUAUQATAUAAAbQAAAcgTAUQgUATgcABQgbgBgUgTg");
	this.shape.setTransform(6.725,6.75);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol10, new cjs.Rectangle(0,0,13.5,13.5), null);


(lib.Symbol9 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#B7B7B7").s().p("AiMCMQg5g6gBhSQABhSA5g6QA6g5BSgBQBSABA6A5QA6A6AABSQAABSg6A6Qg6A6hSAAQhSAAg6g6g");
	this.shape.setTransform(19.85,19.85);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol9, new cjs.Rectangle(0,0,39.7,39.7), null);


(lib.Symbol7 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#B7B7B7").s().p("AiMCMQg5g6gBhSQABhSA5g6QA6g5BSgBQBSABA6A5QA6A6AABSQAABSg6A6Qg6A6hSAAQhSAAg6g6g");
	this.shape.setTransform(19.85,19.85);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol7, new cjs.Rectangle(0,0,39.7,39.7), null);


(lib.Symbol5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(12.5,1,1).p("Ahgg9QCVgrAsCu");
	this.shape.setTransform(9.675,7.018);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol5, new cjs.Rectangle(-6.2,-6.2,31.8,26.5), null);


(lib.Symbol4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(12.5,1,1).p("Ah1AbQBlhmCGBc");
	this.shape.setTransform(11.8,2.7312);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol4, new cjs.Rectangle(-6.2,-6.2,36.1,17.9), null);


(lib.Symbol_18_Symbol_17 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Symbol_17
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(12.5,1,1).p("AhBAUQAhg7BhAd");
	this.shape.setTransform(6.55,25.1665);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(12.5,1,1).p("AhCATQAig3BjAa");
	this.shape_1.setTransform(6.65,25.2878);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(12.5,1,1).p("AhEAQQAigqBnAQ");
	this.shape_2.setTransform(6.9,25.6462);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(12.5,1,1).p("AhJALQAlgVBuAB");
	this.shape_3.setTransform(7.35,26.1494);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(12.5,1,1).p("AhPAFQAoAJB3gV");
	this.shape_4.setTransform(7.95,26.6716);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(12.5,1,1).p("AhLAJQAmgJBxgI");
	this.shape_5.setTransform(7.575,26.3);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(12.5,1,1).p("AhIALQAlgYBsAD");
	this.shape_6.setTransform(7.275,26.0789);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(12.5,1,1).p("AhFAOQAjglBoAN");
	this.shape_7.setTransform(7,25.7843);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(12.5,1,1).p("AhDARQAigvBlAU");
	this.shape_8.setTransform(6.8,25.5182);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(12.5,1,1).p("AhCATQAig2BjAZ");
	this.shape_9.setTransform(6.675,25.3211);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(12.5,1,1).p("AhBAUQAhg6BiAc");
	this.shape_10.setTransform(6.575,25.2109);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},19).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_4}]},2).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape}]},1).wait(43));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_18_Symbol_16 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Symbol_16
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(12.5,1,1).p("AhXBbICvi1");
	this.shape.setTransform(10.225,12.875);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(12.5,1,1).p("AhXBYQBXheBYhR");
	this.shape_1.setTransform(10.225,13.225);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(12.5,1,1).p("AhYBNQBXhoBagx");
	this.shape_2.setTransform(10.25,14.275);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(12.5,1,1).p("AhYA8QBVh7BcAE");
	this.shape_3.setTransform(10.275,16.0187);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(12.5,1,1).p("AhYAxQBRiUBgBO");
	this.shape_4.setTransform(10.3,17.118);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(12.5,1,1).p("AhYA1QBTiEBeAh");
	this.shape_5.setTransform(10.275,16.664);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(12.5,1,1).p("AhYA/QBVh4BcgF");
	this.shape_6.setTransform(10.275,15.725);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(12.5,1,1).p("AhYBJQBWhtBbgk");
	this.shape_7.setTransform(10.25,14.7);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(12.5,1,1).p("AhYBRQBXhlBag8");
	this.shape_8.setTransform(10.25,13.9);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(12.5,1,1).p("AhXBXQBXhfBYhO");
	this.shape_9.setTransform(10.225,13.325);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(12.5,1,1).p("AhXBaQBXhbBYhY");
	this.shape_10.setTransform(10.225,13);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},19).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_4}]},2).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape}]},1).wait(43));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_18_Symbol_15 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Symbol_15
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(12.5,1,1).p("AgbBzQAvhpAJh8");
	this.shape.setTransform(3.85,11.45);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(12.5,1,1).p("AgbBzQAwhpAHh8");
	this.shape_1.setTransform(3.8,11.45);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(12.5,1,1).p("AgbByQAwhpAHh6");
	this.shape_2.setTransform(3.75,11.475);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(12.5,1,1).p("AgaByQAwhpAEh6");
	this.shape_3.setTransform(3.65,11.5);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(12.5,1,1).p("AgZByQAwhpADh6");
	this.shape_4.setTransform(3.55,11.525);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(12.5,1,1).p("AgXByQAvhpAAh6");
	this.shape_5.setTransform(3.425,11.55);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(12.5,1,1).p("AgYByQAwhpABh6");
	this.shape_6.setTransform(3.475,11.55);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(12.5,1,1).p("AgYByQAvhpACh6");
	this.shape_7.setTransform(3.525,11.525);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(12.5,1,1).p("AgZByQAvhpAEh6");
	this.shape_8.setTransform(3.625,11.5);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(12.5,1,1).p("AgaByQAvhpAGh6");
	this.shape_9.setTransform(3.7,11.475);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},19).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4,p:{x:3.55}}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_5}]},2).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_4,p:{x:3.575}}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape}]},1).wait(39));
	this.timeline.addTween(cjs.Tween.get(this.shape).wait(20).to({_off:true},1).wait(15).to({_off:false},0).wait(39));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_14_Layer_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(12.5,1,1).p("AnzGbQGWAxDwjCQD7jJBmnl");
	this.shape.setTransform(49.975,42.094);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(12.5,1,1).p("AnxGrQGPAVDxjEQD6jJBpnf");
	this.shape_1.setTransform(49.8,40.4723);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(12.5,1,1).p("AnvG3QGIgFDyjFQD5jJBsna");
	this.shape_2.setTransform(49.625,39.3);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(12.5,1,1).p("AnuHBQGCgcDzjGQD5jJBvnW");
	this.shape_3.setTransform(49.5,38.325);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(12.5,1,1).p("AntHJQF+gvDzjHQD4jJBynS");
	this.shape_4.setTransform(49.375,37.5);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(12.5,1,1).p("AnsHQQF5hAD1jHQD3jJB0nP");
	this.shape_5.setTransform(49.275,36.8);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(12.5,1,1).p("AnrHWQF2hOD1jIQD3jIB1nN");
	this.shape_6.setTransform(49.175,36.225);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(12.5,1,1).p("AnqHaQFzhYD1jIQD4jIB1nL");
	this.shape_7.setTransform(49.1,35.775);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(12.5,1,1).p("AnqHeQFxhhD2jHQD3jJB3nK");
	this.shape_8.setTransform(49.075,35.45);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(12.5,1,1).p("AnpHfQFwhkD2jJQD2jIB3nI");
	this.shape_9.setTransform(49.025,35.275);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(12.5,1,1).p("AnpHgQFvhmD2jJQD3jHB3nJ");
	this.shape_10.setTransform(49.025,35.2);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(12.5,1,1).p("AnrHUQF3hKD0jHQD4jIB0nO");
	this.shape_11.setTransform(49.2,36.425);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(12.5,1,1).p("AnuG/QGDgZDzjFQD5jJBunW");
	this.shape_12.setTransform(49.5,38.475);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(12.5,1,1).p("AnxGwQGNALDyjEQD5jJBrnd");
	this.shape_13.setTransform(49.75,40.0319);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(12.5,1,1).p("AnxGpQGQAZDxjDQD5jKBpng");
	this.shape_14.setTransform(49.825,40.717);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(12.5,1,1).p("AnyGjQGTAkDwjDQD6jJBoni");
	this.shape_15.setTransform(49.9,41.299);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(12.5,1,1).p("AnyGfQGUArDwjDQD7jJBmnj");
	this.shape_16.setTransform(49.925,41.7393);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(12.5,1,1).p("AnzGcQGWAwDwjCQD6jKBnnk");
	this.shape_17.setTransform(49.975,41.9921);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},9).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_10}]},13).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape}]},1).wait(33));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_3_Layer_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(12.5,1,1).p("AhipMQibErgxCgQg3C3AuB2QAuB1CkBmQCMBVElBx");
	this.shape.setTransform(33.2525,58.85);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(12.5,1,1).p("AhPpUQicErgwCgQg3C3ApB4QAqB2CbBnQCFBaEaB4");
	this.shape_1.setTransform(31.4076,59.675);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(12.5,1,1).p("Ag+pcQibErgxCgQg3C3AmB5QAlB4CSBpQB/BeERB/");
	this.shape_2.setTransform(29.6899,60.45);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(12.5,1,1).p("AgupjQibErgxCgQg3C3AiB6QAiB5CJBrQB6BhEHCG");
	this.shape_3.setTransform(28.0756,61.175);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(12.5,1,1).p("AgfppQicErgwCgQg3C3AeB7QAfB6CCBtQB0BjD+CM");
	this.shape_4.setTransform(26.606,61.825);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(12.5,1,1).p("AgRpvQicErgwCgQg3C3AbB8QAbB7B7BuQBwBnD2CR");
	this.shape_5.setTransform(25.2417,62.425);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(12.5,1,1).p("AgFp1QicErgwCgQg3C3AYB9QAZB9B1BvQBrBqDvCW");
	this.shape_6.setTransform(24.0225,62.95);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(12.5,1,1).p("AAFp6QibErgwCgQg3C3AWB/QAWB9BvBwQBnBsDpCb");
	this.shape_7.setTransform(22.9107,63.45);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(12.5,1,1).p("AAPp+QibErgwCgQg3C3AUB/QATB+BqBxQBkBuDkCf");
	this.shape_8.setTransform(21.9333,63.875);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(12.5,1,1).p("AAXqCQiaErgxCgQg3C3ASCAQASB/BmByQBhBwDeCi");
	this.shape_9.setTransform(21.0781,64.25);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(12.5,1,1).p("AAfqFQibErgwCgQg3C4AQB/QAQCABiBzQBgBxDZCl");
	this.shape_10.setTransform(20.3447,64.55);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(12.5,1,1).p("AAkqHQiaErgxCgQg3C4APB/QAPCABfB0QBeByDWCn");
	this.shape_11.setTransform(19.7643,64.825);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(12.5,1,1).p("AApqJQibErgwCgQg3C4AOB/QANCABdB1QBcBzDUCp");
	this.shape_12.setTransform(19.308,65.025);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(12.5,1,1).p("AAsqLQiaErgxCgQg3C4ANCAQANCABcB2QBbBzDRCr");
	this.shape_13.setTransform(18.9757,65.15);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(12.5,1,1).p("AAuqMQiaErgxCgQg3C4ANCBQANCABaB1QBaB0DQCs");
	this.shape_14.setTransform(18.7593,65.25);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(12.5,1,1).p("AAvqMQibErgwCgQg3C4AMCAQANCBBaB0QBaB1DQCs");
	this.shape_15.setTransform(18.7088,65.275);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(12.5,1,1).p("AAcqEQibErgwCgQg3C4ARB/QAQB/BkBzQBgBxDbCk");
	this.shape_16.setTransform(20.6243,64.45);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(12.5,1,1).p("AAKp8QiaErgxCgQg3C3AVB/QAVB9BsBxQBmBtDmCd");
	this.shape_17.setTransform(22.3849,63.675);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#000000").ss(12.5,1,1).p("AgUpuQicErgwCgQg3C3AcB8QAbB7B9BuQBwBmD4CQ");
	this.shape_18.setTransform(25.5027,62.3);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#000000").ss(12.5,1,1).p("AgipoQibErgxCgQg3C3AfB7QAfB6CEBsQB1BjEACL");
	this.shape_19.setTransform(26.8663,61.7);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#000000").ss(12.5,1,1).p("Ag5peQibErgxCgQg3C3AlB5QAkB5CPBpQB9BfEOCB");
	this.shape_20.setTransform(29.172,60.675);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#000000").ss(12.5,1,1).p("AhCpaQicErgwCgQg3C3AmB5QAnB4CUBoQCBBcETB+");
	this.shape_21.setTransform(30.1324,60.25);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#000000").ss(12.5,1,1).p("AhLpWQibErgxCgQg3C3ApB4QAoB3CZBnQCDBbEYB6");
	this.shape_22.setTransform(30.9662,59.875);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#000000").ss(12.5,1,1).p("AhSpTQibErgxCgQg3C3ArB3QApB3CdBnQCFBYEdB4");
	this.shape_23.setTransform(31.6737,59.575);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#000000").ss(12.5,1,1).p("AhXpQQicErgwCgQg3C3ArB3QAsB2CfBmQCIBYEfB0");
	this.shape_24.setTransform(32.2395,59.3);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f().s("#000000").ss(12.5,1,1).p("AhcpOQibErgxCgQg3C3AtB3QAsB1CiBlQCJBYEiBy");
	this.shape_25.setTransform(32.6799,59.1);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f().s("#000000").ss(12.5,1,1).p("AhfpNQibErgxCgQg3C3AuB2QAtB2CjBlQCKBWEkBy");
	this.shape_26.setTransform(32.995,58.975);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f().s("#000000").ss(12.5,1,1).p("AhhpMQicErgwCgQg3C3AuB2QAtB1CkBlQCLBWEmBx");
	this.shape_27.setTransform(33.2025,58.875);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},9).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_15}]},25).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_27}]},1).to({state:[{t:this.shape}]},1).wait(11));

}).prototype = p = new cjs.MovieClip();


(lib.Scene_1_Layer_3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_3
	this.instance = new lib.pack();
	this.instance.parent = this;
	this.instance.setTransform(501,149);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Scene_1_Layer_3, null, null);


(lib.Scene_1_Layer_2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.instance = new lib.bg();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Scene_1_Layer_2, null, null);


(lib.Symbol2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#F51431").s().p("AAAAqQgRAAgNgMQgMgMAAgSQAAgQAMgNQANgNARAAIADAAQAPABALAMQANANAAAQQAAASgNAMQgLAMgPAAIgDAAg");
	this.shape.setTransform(4.25,4.25);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol2, new cjs.Rectangle(0,0,8.5,8.5), null);


(lib.Symbol19 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Symbol13();
	this.instance.parent = this;
	this.instance.setTransform(7.15,11.55,1,1,0,0,0,3.6,5.5);

	this.instance_1 = new lib.Symbol5();
	this.instance_1.parent = this;
	this.instance_1.setTransform(12.3,13.4,1,1,0,0,0,9.7,7);

	this.instance_2 = new lib.Symbol4();
	this.instance_2.parent = this;
	this.instance_2.setTransform(11.8,2.7,1,1,0,0,0,11.8,2.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_2},{t:this.instance_1},{t:this.instance}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol19, new cjs.Rectangle(-6.2,-6.2,36.1,32.9), null);


(lib.Symbol18 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_74 = function() {
		this.___loopingOver___ = true;
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(74).call(this.frame_74).wait(1));

	// Symbol_17_obj_
	this.Symbol_17 = new lib.Symbol_18_Symbol_17();
	this.Symbol_17.name = "Symbol_17";
	this.Symbol_17.parent = this;
	this.Symbol_17.setTransform(6.5,25.2,1,1,0,0,0,6.5,25.2);
	this.Symbol_17.depth = 0;
	this.Symbol_17.isAttachedToCamera = 0
	this.Symbol_17.isAttachedToMask = 0
	this.Symbol_17.layerDepth = 0
	this.Symbol_17.layerIndex = 0
	this.Symbol_17.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Symbol_17).wait(75));

	// Symbol_16_obj_
	this.Symbol_16 = new lib.Symbol_18_Symbol_16();
	this.Symbol_16.name = "Symbol_16";
	this.Symbol_16.parent = this;
	this.Symbol_16.setTransform(10.2,12.8,1,1,0,0,0,10.2,12.8);
	this.Symbol_16.depth = 0;
	this.Symbol_16.isAttachedToCamera = 0
	this.Symbol_16.isAttachedToMask = 0
	this.Symbol_16.layerDepth = 0
	this.Symbol_16.layerIndex = 1
	this.Symbol_16.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Symbol_16).wait(75));

	// Symbol_15_obj_
	this.Symbol_15 = new lib.Symbol_18_Symbol_15();
	this.Symbol_15.name = "Symbol_15";
	this.Symbol_15.parent = this;
	this.Symbol_15.setTransform(3.9,11.5,1,1,0,0,0,3.9,11.5);
	this.Symbol_15.depth = 0;
	this.Symbol_15.isAttachedToCamera = 0
	this.Symbol_15.isAttachedToMask = 0
	this.Symbol_15.layerDepth = 0
	this.Symbol_15.layerIndex = 2
	this.Symbol_15.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Symbol_15).wait(75));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-6.2,-6.2,31.7,39.900000000000006);


(lib.Symbol15 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_5
	this.instance = new lib.Symbol17copy4();
	this.instance.parent = this;
	this.instance.setTransform(15.75,45.55,1,1,0,0,0,150.5,152);
	this.instance.alpha = 0;
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(12).to({_off:false},0).to({alpha:1},10).wait(9).to({alpha:0},10).wait(1));

	// Layer_4
	this.instance_1 = new lib.Symbol17copy3();
	this.instance_1.parent = this;
	this.instance_1.setTransform(15.75,45.55,1,1,0,0,0,150.5,152);
	this.instance_1.alpha = 0;
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(9).to({_off:false},0).to({alpha:1},10).wait(9).to({alpha:0},10).to({_off:true},1).wait(3));

	// Layer_3
	this.instance_2 = new lib.Symbol17copy2();
	this.instance_2.parent = this;
	this.instance_2.setTransform(15.75,45.55,1,1,0,0,0,150.5,152);
	this.instance_2.alpha = 0;
	this.instance_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(6).to({_off:false},0).to({alpha:1},10).wait(9).to({alpha:0},10).to({_off:true},1).wait(6));

	// Layer_2
	this.instance_3 = new lib.Symbol17copy();
	this.instance_3.parent = this;
	this.instance_3.setTransform(15.75,45.55,1,1,0,0,0,150.5,152);
	this.instance_3.alpha = 0;
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(3).to({_off:false},0).to({alpha:1},10).wait(9).to({alpha:0},10).to({_off:true},1).wait(9));

	// Layer_1
	this.instance_4 = new lib.Symbol17();
	this.instance_4.parent = this;
	this.instance_4.setTransform(15.75,45.55,1,1,0,0,0,150.5,152);
	this.instance_4.alpha = 0;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).to({alpha:1},10).wait(9).to({alpha:0},10).to({_off:true},1).wait(12));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-149.5,-106.4,326.2,304);


(lib.Symbol12 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Symbol_6 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("ABZCMQg6g6AAhSQAAhSA6g6QA6g5BTAAQBSAAA5A5QA7A6AABSQAABSg7A6Qg5A6hSABQhTgBg6g6g");
	mask.setTransform(42.8,18.4);

	// Symbol_7
	this.instance = new lib.Symbol7();
	this.instance.parent = this;
	this.instance.setTransform(65.95,58.75,1,1,0,0,0,19.9,19.9);

	var maskedShapeInstanceList = [this.instance];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1).to({y:54.4},0).wait(1).to({y:51.75},0).wait(1).to({y:49.9},0).wait(1).to({y:48.5},0).wait(1).to({y:47.4},0).wait(1).to({y:46.6},0).wait(1).to({y:45.95},0).wait(1).to({y:45.4},0).wait(1).to({y:45},0).wait(1).to({y:44.7},0).wait(1).to({y:44.45},0).wait(1).to({y:44.25},0).wait(1).to({y:44.1},0).wait(1).to({y:44.05},0).wait(1).to({y:44},0).wait(37).to({x:67.75,y:46.5},0).to({y:58.75},8,cjs.Ease.get(1)).wait(15));

	// Symbol_8 (mask)
	var mask_1 = new cjs.Shape();
	mask_1._off = true;
	mask_1.graphics.p("AiMCMQg5g6gBhSQABhSA5g6QA6g5BSgBQBSABA6A5QA6A6AABSQAABSg6A6Qg6A6hSAAQhSAAg6g6g");
	mask_1.setTransform(19.85,19.85);

	// Symbol_9
	this.instance_1 = new lib.Symbol9();
	this.instance_1.parent = this;
	this.instance_1.setTransform(20.2,60.5,1,1,0,0,0,19.9,19.9);

	var maskedShapeInstanceList = [this.instance_1];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask_1;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(1).to({y:56.15},0).wait(1).to({y:53.5},0).wait(1).to({y:51.65},0).wait(1).to({y:50.25},0).wait(1).to({y:49.15},0).wait(1).to({y:48.35},0).wait(1).to({y:47.7},0).wait(1).to({y:47.15},0).wait(1).to({y:46.75},0).wait(1).to({y:46.45},0).wait(1).to({y:46.2},0).wait(1).to({y:46},0).wait(1).to({y:45.85},0).wait(1).to({y:45.8},0).wait(1).to({y:45.75},0).wait(37).to({x:22,y:48.25},0).to({y:60.5},8,cjs.Ease.get(1)).wait(15));

	// Symbol_11
	this.instance_2 = new lib.Symbol11();
	this.instance_2.parent = this;
	this.instance_2.setTransform(60.65,23.15,1,1,0,0,0,6.7,6.8);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(15).to({x:64.15,y:21.3},7,cjs.Ease.get(1)).wait(3).to({x:60.65,y:23.15},7,cjs.Ease.get(1)).wait(3).to({x:64.15,y:21.3},7,cjs.Ease.get(1)).wait(3).to({x:60.65,y:23.15},7,cjs.Ease.get(1)).wait(23));

	// Symbol_10
	this.instance_3 = new lib.Symbol10();
	this.instance_3.parent = this;
	this.instance_3.setTransform(12.5,24.15,1,1,0,0,0,6.7,6.8);

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(15).to({x:16,y:22.3},7,cjs.Ease.get(1)).wait(3).to({x:12.5,y:24.15},7,cjs.Ease.get(1)).wait(3).to({x:16,y:22.3},7,cjs.Ease.get(1)).wait(3).to({x:12.5,y:24.15},7,cjs.Ease.get(1)).wait(23));

	// Layer_8
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AiMCMQg5g6gBhSQABhSA5g6QA6g5BSgBQBSABA6A5QA6A6AABSQAABSg6A6Qg6A6hSAAQhSAAg6g6g");
	this.shape.setTransform(19.85,19.85);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(75));

	// Layer_9
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AiMCMQg5g6gBhSQABhSA5g6QA6g5BSAAQBSAAA6A5QA6A6AABSQAABSg6A6Qg6A6hSABQhSgBg6g6g");
	this.shape_1.setTransform(65.75,18.4);

	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(75));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,-1.4,85.6,41.1);


(lib.Symbol1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#1B1814").ss(11,1,1).p("AkLhfQBDDEDRgGQBWgDBJgrQBKgtAahG");
	this.shape.setTransform(156.025,-52.5925);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(75));

	// Layer_3
	this.instance = new lib.Symbol2();
	this.instance.parent = this;
	this.instance.setTransform(137.2,-49.4,2.0168,1.0421,0,46.0839,51.8739,5.4,7.5);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(12).to({_off:false},0).to({scaleY:2.0168,guide:{path:[137.2,-49.3,137.5,-49,137.9,-48.7]}},4).wait(1).to({regX:4.3,regY:4.3,scaleX:2.0159,scaleY:2.0177,skewX:45.7195,skewY:51.4594,x:141.35,y:-54.8},0).wait(1).to({scaleX:2.0136,scaleY:2.0199,skewX:44.7878,skewY:50.3997,x:141.8,y:-54.45},0).wait(1).to({scaleX:2.0096,scaleY:2.0237,skewX:43.1697,skewY:48.559,x:142.7,y:-53.7},0).wait(1).to({scaleX:2.0035,scaleY:2.0294,skewX:40.7228,skewY:45.7757,x:144.15,y:-52.85},0).wait(1).to({scaleX:1.995,scaleY:2.0375,skewX:37.2999,skewY:41.8822,x:146.35,y:-51.75},0).wait(1).to({scaleX:1.9839,scaleY:2.0481,skewX:32.8139,skewY:36.7794,x:149.5,y:-50.75},0).wait(1).to({scaleX:1.9705,scaleY:2.0609,skewX:27.3853,skewY:30.6045,x:153.3,y:-50.15},0).wait(1).to({scaleX:1.9559,scaleY:2.0748,skewX:21.5115,skewY:23.9231,x:157.55,y:-50.1},0).wait(1).to({scaleX:1.9422,scaleY:2.0879,skewX:15.9743,skewY:17.6245,x:161.1,y:-50.5},0).wait(1).to({scaleX:1.9309,scaleY:2.0987,skewX:11.4074,skewY:12.4298,x:163.95,y:-51.25},0).wait(1).to({scaleX:1.9225,scaleY:2.1066,skewX:8.0351,skewY:8.5938,x:166.05,y:-52},0).wait(1).to({scaleX:1.917,scaleY:2.1119,skewX:5.788,skewY:6.0378,x:167.45,y:-52.6},0).wait(1).to({scaleX:1.9137,scaleY:2.115,skewX:4.4942,skewY:4.5662,x:168.2,y:-53},0).wait(1).to({regX:5.4,regY:7.5,scaleX:1.9125,scaleY:2.1162,rotation:3.9793,skewX:0,skewY:0,x:170.2,y:-46.35},0).wait(7).to({regX:4.3,regY:4.3,scaleX:1.914,scaleY:2.1148,rotation:0,skewX:4.5733,skewY:4.6561,x:168.2,y:-52.95},0).wait(1).to({scaleX:1.9174,scaleY:2.1115,skewX:5.9626,skewY:6.2364,x:167.3,y:-52.5},0).wait(1).to({scaleX:1.923,scaleY:2.1061,skewX:8.2536,skewY:8.8424,x:165.8,y:-51.9},0).wait(1).to({scaleX:1.9312,scaleY:2.0984,skewX:11.5185,skewY:12.5562,x:163.55,y:-51.1},0).wait(1).to({scaleX:1.9416,scaleY:2.0884,skewX:15.7359,skewY:17.3534,x:160.55,y:-50.4},0).wait(1).to({scaleX:1.9539,scaleY:2.0767,skewX:20.7133,skewY:23.0151,x:156.95,y:-50},0).wait(1).to({scaleX:1.9672,scaleY:2.0641,skewX:26.0541,skewY:29.0903,x:153.3,y:-50.15},0).wait(1).to({scaleX:1.98,scaleY:2.0518,skewX:31.248,skewY:34.9982,x:150.1,y:-50.5},0).wait(1).to({scaleX:1.9914,scaleY:2.0409,skewX:35.8517,skewY:40.2349,x:147.3,y:-51.3},0).wait(1).to({scaleX:2.0007,scaleY:2.0321,skewX:39.611,skewY:44.5111,x:145.05,y:-52.25},0).wait(1).to({scaleX:2.0078,scaleY:2.0254,skewX:42.4537,skewY:47.7446,x:143.25,y:-53.25},0).wait(1).to({scaleX:2.0127,scaleY:2.0207,skewX:44.4197,skewY:49.9809,x:142.1,y:-54.1},0).wait(1).to({scaleX:2.0156,scaleY:2.0179,skewX:45.5968,skewY:51.3198,x:141.45,y:-54.7},0).wait(1).to({regX:5.4,regY:7.5,scaleX:2.0168,scaleY:2.0168,skewX:46.0839,skewY:51.8739,x:137.85,y:-48.75},0).to({regX:5.5,regY:7.4,scaleY:1.2283,skewX:46.0842,x:134.95,y:-45.55},4).to({_off:true},1).wait(20));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(123.8,-67.8,64.50000000000001,30.299999999999997);


(lib.Symbol_14_Layer_4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_4
	this.instance = new lib.Symbol18();
	this.instance.parent = this;
	this.instance.setTransform(98.55,6.2,1,1,0,0,0,0,27.2);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(9).to({regX:-0.1,rotation:39.9471,x:98.3,y:-13.05},10,cjs.Ease.get(1)).wait(13).to({regX:0,rotation:0,x:98.55,y:6.2},10,cjs.Ease.get(1)).wait(33));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_3_Layer_4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_4
	this.instance = new lib.Symbol19();
	this.instance.parent = this;
	this.instance.setTransform(66.55,117.75,1,1,0,0,0,0.1,6.2);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(9).to({rotation:44.9994,x:40.35,y:134.4},15,cjs.Ease.get(1)).wait(25).to({rotation:0,x:66.55,y:117.75},15,cjs.Ease.get(1)).wait(11));

}).prototype = p = new cjs.MovieClip();


(lib.Scene_1_Layer_7 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_7
	this.instance = new lib.Symbol15();
	this.instance.parent = this;
	this.instance.setTransform(890.95,407.4,1,1,0,0,0,16.2,13);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Scene_1_Layer_7, null, null);


(lib.Symbol14 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_74 = function() {
		this.___loopingOver___ = true;
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(74).call(this.frame_74).wait(1));

	// Layer_4_obj_
	this.Layer_4 = new lib.Symbol_14_Layer_4();
	this.Layer_4.name = "Layer_4";
	this.Layer_4.parent = this;
	this.Layer_4.setTransform(108,-7.4,1,1,0,0,0,108,-7.4);
	this.Layer_4.depth = 0;
	this.Layer_4.isAttachedToCamera = 0
	this.Layer_4.isAttachedToMask = 0
	this.Layer_4.layerDepth = 0
	this.Layer_4.layerIndex = 0
	this.Layer_4.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_4).wait(75));

	// Layer_1_obj_
	this.Layer_1 = new lib.Symbol_14_Layer_1();
	this.Layer_1.name = "Layer_1";
	this.Layer_1.parent = this;
	this.Layer_1.setTransform(50,42.1,1,1,0,0,0,50,42.1);
	this.Layer_1.depth = 0;
	this.Layer_1.isAttachedToCamera = 0
	this.Layer_1.isAttachedToMask = 0
	this.Layer_1.layerDepth = 0
	this.Layer_1.layerIndex = 1
	this.Layer_1.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_1).wait(75));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-6.2,-42,143.1,132.5);


(lib.Symbol3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_74 = function() {
		this.___loopingOver___ = true;
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(74).call(this.frame_74).wait(1));

	// Layer_4_obj_
	this.Layer_4 = new lib.Symbol_3_Layer_4();
	this.Layer_4.name = "Layer_4";
	this.Layer_4.parent = this;
	this.Layer_4.setTransform(78.3,121.8,1,1,0,0,0,78.3,121.8);
	this.Layer_4.depth = 0;
	this.Layer_4.isAttachedToCamera = 0
	this.Layer_4.isAttachedToMask = 0
	this.Layer_4.layerDepth = 0
	this.Layer_4.layerIndex = 0
	this.Layer_4.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_4).wait(75));

	// Layer_1_obj_
	this.Layer_1 = new lib.Symbol_3_Layer_1();
	this.Layer_1.name = "Layer_1";
	this.Layer_1.parent = this;
	this.Layer_1.setTransform(33.3,58.9,1,1,0,0,0,33.3,58.9);
	this.Layer_1.depth = 0;
	this.Layer_1.isAttachedToCamera = 0
	this.Layer_1.isAttachedToMask = 0
	this.Layer_1.layerDepth = 0
	this.Layer_1.layerIndex = 1
	this.Layer_1.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_1).wait(75));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-7.7,-6.2,104,175);


(lib.Symbol2_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.instance = new lib.Symbol12();
	this.instance.parent = this;
	this.instance.setTransform(30.35,-14.4,1.0998,1.0998,0,0.0485,-179.9507,43.4,34.9);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	// Layer_1
	this.instance_1 = new lib.Symbol1();
	this.instance_1.parent = this;
	this.instance_1.setTransform(27.55,15.65,1.03,1.03,0,0,0,156,-52.6);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol2_1, new cjs.Rectangle(-16.2,-54.4,94.4,90), null);


(lib.Scene_1_Layer_6 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_6
	this.instance = new lib.Symbol2_1();
	this.instance.parent = this;
	this.instance.setTransform(730.35,341.25,1,1,0,0,0,27.6,15.6);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Scene_1_Layer_6, null, null);


(lib.Scene_1_Layer_5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_5
	this.instance = new lib.Symbol3();
	this.instance.parent = this;
	this.instance.setTransform(641.05,438.55,1,1,0,0,0,33.3,58.9);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Scene_1_Layer_5, null, null);


(lib.Scene_1_Layer_4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_4
	this.instance = new lib.Symbol14();
	this.instance.parent = this;
	this.instance.setTransform(777.6,391.85,1,1,12.7181,0,0,50,42.1);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Scene_1_Layer_4, null, null);


// stage content:
(lib.Jan_site = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	this.___GetDepth___ = function(obj) {
		var depth = obj.depth;
		var cameraObj = this.___camera___instance;
		if(cameraObj && cameraObj.depth && obj.isAttachedToCamera)
		{
			depth += depth + cameraObj.depth;
		}
		return depth;
		}
	this.___needSorting___ = function() {
		for (var i = 0; i < this.getNumChildren() - 1; i++)
		{
			var prevDepth = this.___GetDepth___(this.getChildAt(i));
			var nextDepth = this.___GetDepth___(this.getChildAt(i + 1));
			if (prevDepth < nextDepth)
				return true;
		}
		return false;
	}
	this.___sortFunction___ = function(obj1, obj2) {
		return (this.exportRoot.___GetDepth___(obj2) - this.exportRoot.___GetDepth___(obj1));
	}
	this.on('tick', function (event){
		var curTimeline = event.currentTarget;
		if (curTimeline.___needSorting___()){
			this.sortChildren(curTimeline.___sortFunction___);
		}
	});

	// Layer_6_obj_
	this.Layer_6 = new lib.Scene_1_Layer_6();
	this.Layer_6.name = "Layer_6";
	this.Layer_6.parent = this;
	this.Layer_6.setTransform(675.3,333.4,1,1,0,0,0,675.3,333.4);
	this.Layer_6.depth = 0;
	this.Layer_6.isAttachedToCamera = 0
	this.Layer_6.isAttachedToMask = 0
	this.Layer_6.layerDepth = 0
	this.Layer_6.layerIndex = 0
	this.Layer_6.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_6).wait(1));

	// Layer_5_obj_
	this.Layer_5 = new lib.Scene_1_Layer_5();
	this.Layer_5.name = "Layer_5";
	this.Layer_5.parent = this;
	this.Layer_5.setTransform(652.8,445.7,1,1,0,0,0,652.8,445.7);
	this.Layer_5.depth = 0;
	this.Layer_5.isAttachedToCamera = 0
	this.Layer_5.isAttachedToMask = 0
	this.Layer_5.layerDepth = 0
	this.Layer_5.layerIndex = 1
	this.Layer_5.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_5).wait(1));

	// Layer_3_obj_
	this.Layer_3 = new lib.Scene_1_Layer_3();
	this.Layer_3.name = "Layer_3";
	this.Layer_3.parent = this;
	this.Layer_3.setTransform(681,397,1,1,0,0,0,681,397);
	this.Layer_3.depth = 0;
	this.Layer_3.isAttachedToCamera = 0
	this.Layer_3.isAttachedToMask = 0
	this.Layer_3.layerDepth = 0
	this.Layer_3.layerIndex = 2
	this.Layer_3.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_3).wait(1));

	// Layer_4_obj_
	this.Layer_4 = new lib.Scene_1_Layer_4();
	this.Layer_4.name = "Layer_4";
	this.Layer_4.parent = this;
	this.Layer_4.setTransform(788.5,383.6,1,1,0,0,0,788.5,383.6);
	this.Layer_4.depth = 0;
	this.Layer_4.isAttachedToCamera = 0
	this.Layer_4.isAttachedToMask = 0
	this.Layer_4.layerDepth = 0
	this.Layer_4.layerIndex = 3
	this.Layer_4.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_4).wait(1));

	// Layer_7_obj_
	this.Layer_7 = new lib.Scene_1_Layer_7();
	this.Layer_7.name = "Layer_7";
	this.Layer_7.parent = this;
	this.Layer_7.setTransform(890.5,439.9,1,1,0,0,0,890.5,439.9);
	this.Layer_7.depth = 0;
	this.Layer_7.isAttachedToCamera = 0
	this.Layer_7.isAttachedToMask = 0
	this.Layer_7.layerDepth = 0
	this.Layer_7.layerIndex = 4
	this.Layer_7.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_7).wait(1));

	// Layer_2_obj_
	this.Layer_2 = new lib.Scene_1_Layer_2();
	this.Layer_2.name = "Layer_2";
	this.Layer_2.parent = this;
	this.Layer_2.setTransform(540,340,1,1,0,0,0,540,340);
	this.Layer_2.depth = 0;
	this.Layer_2.isAttachedToCamera = 0
	this.Layer_2.isAttachedToMask = 0
	this.Layer_2.layerDepth = 0
	this.Layer_2.layerIndex = 5
	this.Layer_2.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_2).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(540,340,540,340);
// library properties:
lib.properties = {
	id: '54A88281C7816D4BA4406DADABC039A0',
	width: 1080,
	height: 680,
	fps: 24,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: [
		{src:"assets/images/bg35.jpg", id:"bg"},
		{src:"assets/images/pack35.png", id:"pack"},
		{src:"assets/images/star_lights35.png", id:"star_lights"}
	],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['54A88281C7816D4BA4406DADABC039A0'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}


// Layer depth API : 

AdobeAn.Layer = new function() {
	this.getLayerZDepth = function(timeline, layerName)
	{
		if(layerName === "Camera")
		layerName = "___camera___instance";
		var script = "if(timeline." + layerName + ") timeline." + layerName + ".depth; else 0;";
		return eval(script);
	}
	this.setLayerZDepth = function(timeline, layerName, zDepth)
	{
		const MAX_zDepth = 10000;
		const MIN_zDepth = -5000;
		if(zDepth > MAX_zDepth)
			zDepth = MAX_zDepth;
		else if(zDepth < MIN_zDepth)
			zDepth = MIN_zDepth;
		if(layerName === "Camera")
		layerName = "___camera___instance";
		var script = "if(timeline." + layerName + ") timeline." + layerName + ".depth = " + zDepth + ";";
		eval(script);
	}
	this.removeLayer = function(timeline, layerName)
	{
		if(layerName === "Camera")
		layerName = "___camera___instance";
		var script = "if(timeline." + layerName + ") timeline.removeChild(timeline." + layerName + ");";
		eval(script);
	}
	this.addNewLayer = function(timeline, layerName, zDepth)
	{
		if(layerName === "Camera")
		layerName = "___camera___instance";
		zDepth = typeof zDepth !== 'undefined' ? zDepth : 0;
		var layer = new createjs.MovieClip();
		layer.name = layerName;
		layer.depth = zDepth;
		layer.layerIndex = 0;
		timeline.addChild(layer);
	}
}


})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;

$(function () {
    var canvas, stage, exportRoot, anim_container, dom_overlay_container, fnStartAnimation;
    function init() {
        canvas = document.getElementById("canvas35");
        anim_container = document.getElementById("animation_container35");
        dom_overlay_container = document.getElementById("dom_overlay_container35");
        var comp=AdobeAn.getComposition("54A88281C7816D4BA4406DADABC039A0");
        var lib=comp.getLibrary();
        createjs.MotionGuidePlugin.install();
        var loader = new createjs.LoadQueue(false);
        loader.addEventListener("fileload", function(evt){handleFileLoad(evt,comp)});
        loader.addEventListener("complete", function(evt){handleComplete(evt,comp)});
        var lib=comp.getLibrary();
        loader.loadManifest(lib.properties.manifest);
    }
    function handleFileLoad(evt, comp) {
        var images=comp.getImages();
        if (evt && (evt.item.type == "image")) { images[evt.item.id] = evt.result; }
    }
    function handleComplete(evt,comp) {
        //This function is always called, irrespective of the content. You can use the variable "stage" after it is created in token create_stage.
        var lib=comp.getLibrary();
        var ss=comp.getSpriteSheet();
        var queue = evt.target;
        var ssMetadata = lib.ssMetadata;
        for(i=0; i<ssMetadata.length; i++) {
            ss[ssMetadata[i].name] = new createjs.SpriteSheet( {"images": [queue.getResult(ssMetadata[i].name)], "frames": ssMetadata[i].frames} )
        }
        var preloaderDiv = document.getElementById("_preload_div_35");
        preloaderDiv.style.display = 'none';
        canvas.style.display = 'block';
        exportRoot = new lib.Jan_site();
        stage = new lib.Stage(canvas);
        //Registers the "tick" event listener.
        fnStartAnimation = function() {
            stage.addChild(exportRoot);
            createjs.Ticker.setFPS(lib.properties.fps);
            createjs.Ticker.addEventListener("tick", stage)
            stage.addEventListener("tick", handleTick)
            function getProjectionMatrix(container, totalDepth) {
                var focalLength = 528.25;
                var projectionCenter = { x : lib.properties.width/2, y : lib.properties.height/2 };
                var scale = (totalDepth + focalLength)/focalLength;
                var scaleMat = new createjs.Matrix2D;
                scaleMat.a = 1/scale;
                scaleMat.d = 1/scale;
                var projMat = new createjs.Matrix2D;
                projMat.tx = -projectionCenter.x;
                projMat.ty = -projectionCenter.y;
                projMat = projMat.prependMatrix(scaleMat);
                projMat.tx += projectionCenter.x;
                projMat.ty += projectionCenter.y;
                return projMat;
            }
            function handleTick(event) {
                var cameraInstance = exportRoot.___camera___instance;
                if(cameraInstance !== undefined && cameraInstance.pinToObject !== undefined)
                {
                    cameraInstance.x = cameraInstance.pinToObject.x + cameraInstance.pinToObject.pinOffsetX;
                    cameraInstance.y = cameraInstance.pinToObject.y + cameraInstance.pinToObject.pinOffsetY;
                    if(cameraInstance.pinToObject.parent !== undefined && cameraInstance.pinToObject.parent.depth !== undefined)
                        cameraInstance.depth = cameraInstance.pinToObject.parent.depth + cameraInstance.pinToObject.pinOffsetZ;
                }
                applyLayerZDepth(exportRoot);
            }
            function applyLayerZDepth(parent)
            {
                var cameraInstance = parent.___camera___instance;
                var focalLength = 528.25;
                var projectionCenter = { 'x' : 0, 'y' : 0};
                if(parent === exportRoot)
                {
                    var stageCenter = { 'x' : lib.properties.width/2, 'y' : lib.properties.height/2 };
                    projectionCenter.x = stageCenter.x;
                    projectionCenter.y = stageCenter.y;
                }
                for(child in parent.children)
                {
                    var layerObj = parent.children[child];
                    if(layerObj == cameraInstance)
                        continue;
                    applyLayerZDepth(layerObj, cameraInstance);
                    if(layerObj.layerDepth === undefined)
                        continue;
                    if(layerObj.currentFrame != layerObj.parent.currentFrame)
                    {
                        layerObj.gotoAndPlay(layerObj.parent.currentFrame);
                    }
                    var matToApply = new createjs.Matrix2D;
                    var cameraMat = new createjs.Matrix2D;
                    var totalDepth = layerObj.layerDepth ? layerObj.layerDepth : 0;
                    var cameraDepth = 0;
                    if(cameraInstance && !layerObj.isAttachedToCamera)
                    {
                        var mat = cameraInstance.getMatrix();
                        mat.tx -= projectionCenter.x;
                        mat.ty -= projectionCenter.y;
                        cameraMat = mat.invert();
                        cameraMat.prependTransform(projectionCenter.x, projectionCenter.y, 1, 1, 0, 0, 0, 0, 0);
                        cameraMat.appendTransform(-projectionCenter.x, -projectionCenter.y, 1, 1, 0, 0, 0, 0, 0);
                        if(cameraInstance.depth)
                            cameraDepth = cameraInstance.depth;
                    }
                    if(layerObj.depth)
                    {
                        totalDepth = layerObj.depth;
                    }
                    //Offset by camera depth
                    totalDepth -= cameraDepth;
                    if(totalDepth < -focalLength)
                    {
                        matToApply.a = 0;
                        matToApply.d = 0;
                    }
                    else
                    {
                        if(layerObj.layerDepth)
                        {
                            var sizeLockedMat = getProjectionMatrix(parent, layerObj.layerDepth);
                            if(sizeLockedMat)
                            {
                                sizeLockedMat.invert();
                                matToApply.prependMatrix(sizeLockedMat);
                            }
                        }
                        matToApply.prependMatrix(cameraMat);
                        var projMat = getProjectionMatrix(parent, totalDepth);
                        if(projMat)
                        {
                            matToApply.prependMatrix(projMat);
                        }
                    }
                    layerObj.transformMatrix = matToApply;
                }
            }
        }
        //Code to support hidpi screens and responsive scaling.
        function makeResponsive(isResp, respDim, isScale, scaleType) {
            var lastW, lastH, lastS=1;
            window.addEventListener('resize', resizeCanvas);
            resizeCanvas();
            function resizeCanvas() {
                var w = lib.properties.width, h = lib.properties.height;
                var iw = window.innerWidth, ih=window.innerHeight;
                var pRatio = window.devicePixelRatio || 1, xRatio=iw/w, yRatio=ih/h, sRatio=1;
                if(isResp) {
                    if((respDim=='width'&&lastW==iw) || (respDim=='height'&&lastH==ih)) {
                        sRatio = lastS;
                    }
                    else if(!isScale) {
                        if(iw<w || ih<h)
                            sRatio = Math.min(xRatio, yRatio);
                    }
                    else if(scaleType==1) {
                        sRatio = Math.min(xRatio, yRatio);
                    }
                    else if(scaleType==2) {
                        sRatio = Math.max(xRatio, yRatio);
                    }
                }
                canvas.width = w*pRatio*sRatio;
                canvas.height = h*pRatio*sRatio;
                canvas.style.width = anim_container.style.width = dom_overlay_container.style.width = preloaderDiv.style.width = w*sRatio+'px';
                canvas.style.height = anim_container.style.height = dom_overlay_container.style.height = preloaderDiv.style.height = h*sRatio+'px';
                stage.scaleX = pRatio*sRatio;
                stage.scaleY = pRatio*sRatio;
                lastW = iw; lastH = ih; lastS = sRatio;
                stage.tickOnUpdate = false;
                stage.update();
                stage.tickOnUpdate = true;
            }
        }
        makeResponsive(true,'both',true,2);
        AdobeAn.compositionLoaded(lib.properties.id);
        fnStartAnimation();
    }
    init();
});