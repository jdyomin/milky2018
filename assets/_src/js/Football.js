(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [];


// symbols:



(lib.bg = function() {
	this.initialize(img.bg);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,1080,680);// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.Symbol10 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Symbol 9
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#1B1814").ss(11.5,1,1).p("AhOBdQDAg9gsh8");
	this.shape.setTransform(7.9,12.7);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#1B1814").ss(11.5,1,1).p("AhMBaQC9g3gsh8");
	this.shape_1.setTransform(8,12.5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#1B1814").ss(11.5,1,1).p("AhLBXQC7gxgsh8");
	this.shape_2.setTransform(8.1,12.2);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#1B1814").ss(11.5,1,1).p("AhKBUQC4grgsh8");
	this.shape_3.setTransform(8.3,11.9);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#1B1814").ss(11.5,1,1).p("AhIBSQC1gmgsh8");
	this.shape_4.setTransform(8.4,11.6);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#1B1814").ss(11.5,1,1).p("AhHBPQCygggsh8");
	this.shape_5.setTransform(8.6,11.3);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#1B1814").ss(11.5,1,1).p("AhGBMQCwgbgsh8");
	this.shape_6.setTransform(8.7,11);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#1B1814").ss(11.5,1,1).p("AhEBJQCtgVgsh8");
	this.shape_7.setTransform(8.8,10.8);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#1B1814").ss(11.5,1,1).p("AhDBGQCqgPgsh8");
	this.shape_8.setTransform(9,10.5);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#1B1814").ss(11.5,1,1).p("AhCBDQCogJgsh8");
	this.shape_9.setTransform(9.1,10.2);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#1B1814").ss(11.5,1,1).p("AhBBBQClgFgsh8");
	this.shape_10.setTransform(9.3,9.9);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#1B1814").ss(11.5,1,1).p("Ag/A+QCiABgsh8");
	this.shape_11.setTransform(9.4,9.6);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#1B1814").ss(11.5,1,1).p("Ag+A7QCgAHgsh8");
	this.shape_12.setTransform(9.5,9.3);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#1B1814").ss(11.5,1,1).p("Ag9A3QCdANgsh8");
	this.shape_13.setTransform(9.7,9.1);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#1B1814").ss(11.5,1,1).p("Ag/A9QCiACgsh8");
	this.shape_14.setTransform(9.4,9.6);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#1B1814").ss(11.5,1,1).p("AhCBDQCngIgsh9");
	this.shape_15.setTransform(9.2,10.1);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#1B1814").ss(11.5,1,1).p("AhEBHQCsgRgsh8");
	this.shape_16.setTransform(8.9,10.6);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#1B1814").ss(11.5,1,1).p("AhFBLQCvgZgsh8");
	this.shape_17.setTransform(8.7,11);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#1B1814").ss(11.5,1,1).p("AhHBPQCyghgsh8");
	this.shape_18.setTransform(8.6,11.3);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#1B1814").ss(11.5,1,1).p("AhJBSQC2gngsh8");
	this.shape_19.setTransform(8.4,11.7);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#1B1814").ss(11.5,1,1).p("AhKBVQC5gtgsh8");
	this.shape_20.setTransform(8.2,12);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#1B1814").ss(11.5,1,1).p("AhMBZQC9g1gsh8");
	this.shape_21.setTransform(8,12.4);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#1B1814").ss(11.5,1,1).p("AhNBbQC+g5gsh8");
	this.shape_22.setTransform(8,12.5);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#1B1814").ss(11.5,1,1).p("AhNBcQC/g7gsh8");
	this.shape_23.setTransform(7.9,12.7);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#1B1814").ss(11.5,1,1).p("AhOBdQDAg8gsh9");
	this.shape_24.setTransform(7.9,12.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},9).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_13}]},23).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape}]},1).wait(17));

	// Symbol 4
	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f().s("#1B1814").ss(11.5,1,1).p("AgQiRQA8CkgvB/");
	this.shape_25.setTransform(15.4,14.6);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f().s("#1B1814").ss(11.5,1,1).p("AgOiPQA8Ckg7B6");
	this.shape_26.setTransform(15.2,14.4);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f().s("#1B1814").ss(11.5,1,1).p("AgJiNQA8CkhFB3");
	this.shape_27.setTransform(14.7,14.2);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f().s("#1B1814").ss(11.5,1,1).p("AgDiLQA8ClhPBy");
	this.shape_28.setTransform(14.1,14);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f().s("#1B1814").ss(11.5,1,1).p("AAAiKQA9CkhVBw");
	this.shape_29.setTransform(13.7,13.9);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f().s("#1B1814").ss(11.5,1,1).p("AAFiJQA9ClheBu");
	this.shape_30.setTransform(13.2,13.8);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f().s("#1B1814").ss(11.5,1,1).p("AAJiIQA9ClhkBs");
	this.shape_31.setTransform(12.8,13.7);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f().s("#1B1814").ss(11.5,1,1).p("AAMiFQA9CkhrBm");
	this.shape_32.setTransform(12.5,13.4);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f().s("#1B1814").ss(11.5,1,1).p("AAOiDQA9CkhuBj");
	this.shape_33.setTransform(12.3,13.2);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f().s("#1B1814").ss(11.5,1,1).p("AAQiCQA9CkhyBh");
	this.shape_34.setTransform(12.1,13.1);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f().s("#1B1814").ss(11.5,1,1).p("AATiAQA9Ckh3Bd");
	this.shape_35.setTransform(11.8,12.9);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f().s("#1B1814").ss(11.5,1,1).p("AAVh/QA9Clh6Ba");
	this.shape_36.setTransform(11.6,12.8);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f().s("#1B1814").ss(11.5,1,1).p("AAXh9QA9Ckh+BX");
	this.shape_37.setTransform(11.4,12.6);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f().s("#1B1814").ss(11.5,1,1).p("AAXh1QA9Ckh9BH");
	this.shape_38.setTransform(11.4,11.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_25}]}).to({state:[{t:this.shape_25}]},9).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_27}]},1).to({state:[{t:this.shape_28}]},1).to({state:[{t:this.shape_29}]},1).to({state:[{t:this.shape_30}]},1).to({state:[{t:this.shape_31}]},1).to({state:[{t:this.shape_32}]},1).to({state:[{t:this.shape_33}]},1).to({state:[{t:this.shape_34}]},1).to({state:[{t:this.shape_35}]},1).to({state:[{t:this.shape_36}]},1).to({state:[{t:this.shape_37}]},1).to({state:[{t:this.shape_38}]},1).to({state:[{t:this.shape_38}]},23).to({state:[{t:this.shape_37}]},1).to({state:[{t:this.shape_36}]},1).to({state:[{t:this.shape_35}]},1).to({state:[{t:this.shape_34}]},1).to({state:[{t:this.shape_33}]},1).to({state:[{t:this.shape_32}]},1).to({state:[{t:this.shape_31}]},1).to({state:[{t:this.shape_30}]},1).to({state:[{t:this.shape_29}]},1).to({state:[{t:this.shape_28}]},1).to({state:[{t:this.shape_27}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_25}]},1).wait(17));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-5.7,-5.7,28.6,40.6);


(lib.Symbol7 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#1B1815").ss(6.5,1,1).p("ABMgLQilAKAPAN");
	this.shape.setTransform(7.6,1.2);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol7, new cjs.Rectangle(-3.2,-3.2,21.7,9), null);


(lib.Symbol6 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#1B1815").ss(7.5,1,1).p("ABeANQh5gghCAI");
	this.shape.setTransform(9.4,1.3);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol6, new cjs.Rectangle(-3.7,-3.7,26.2,10.2), null);


(lib.Symbol5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#1B1815").ss(6.5,1,1).p("AAbA6QAZgyhUhB");
	this.shape.setTransform(3.3,5.8);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol5, new cjs.Rectangle(-3.2,-3.2,13.1,18), null);


(lib.Symbol3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#1B1814").ss(11.5,1,1).p("ADwFIQgxhJi7jdQi6jdg5iM");
	this.shape.setTransform(24,32.8);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol3, new cjs.Rectangle(-5.7,-5.7,59.4,77.2), null);


(lib.Symbol2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#F51431").s().p("AAAAqQgRAAgNgMQgMgMAAgSQAAgQAMgNQANgNARAAIADAAQAPABALAMQANANAAAQQAAASgNAMQgLAMgPAAIgDAAg");
	this.shape.setTransform(4.3,4.3);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol2, new cjs.Rectangle(0,0,8.5,8.5), null);


(lib.Symbol1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#1B1814").ss(11.5,1,1).p("AFfiuQkIDXm1CG");
	this.shape.setTransform(35.1,17.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol1, new cjs.Rectangle(-5.7,-5.7,81.6,46.5), null);


(lib.Symbol11 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Symbol 10
	this.instance = new lib.Symbol10();
	this.instance.parent = this;
	this.instance.setTransform(47.9,100.7,1,1,0,0,0,13.7,0);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(9).to({y:76.7},13,cjs.Ease.get(1)).wait(47).to({y:100.7},13,cjs.Ease.get(1)).wait(57));

	// Symbol 3
	this.instance_1 = new lib.Symbol3();
	this.instance_1.parent = this;
	this.instance_1.setTransform(0,35);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(9).to({rotation:-6.2,x:-6.7,y:16.6},13,cjs.Ease.get(1)).wait(47).to({rotation:0,x:0,y:35},13,cjs.Ease.get(1)).wait(57));

	// Symbol 1
	this.instance_2 = new lib.Symbol1();
	this.instance_2.parent = this;
	this.instance_2.setTransform(70.1,0,1,1,0,0,0,70.1,0);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(9).to({rotation:15},13,cjs.Ease.get(1)).wait(47).to({rotation:0},13,cjs.Ease.get(1)).wait(57));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-5.7,-5.7,81.6,141.3);


(lib.Symbol8 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.Symbol5();
	this.instance.parent = this;
	this.instance.setTransform(3.3,8.4,1,1,0,0,0,3.3,5.8);

	this.instance_1 = new lib.Symbol6();
	this.instance_1.parent = this;
	this.instance_1.setTransform(10.8,3.6,1,1,0,0,0,9.3,1.3);

	this.instance_2 = new lib.Symbol7();
	this.instance_2.parent = this;
	this.instance_2.setTransform(9,1.2,1,1,0,0,0,7.6,1.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_2},{t:this.instance_1},{t:this.instance}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol8, new cjs.Rectangle(-3.2,-3.2,27.2,20.6), null);


// stage content:
(lib.Football = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 12
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#1B1814").ss(11.5,1,1).p("Ai2hsQgJBkA4A9QAyA4BNAAQBKAAA3g1QA7g4AEhY");
	this.shape.setTransform(727.3,247.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(40));

	// Layer 11
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#1B1814").ss(11.5,1,1).p("Ai2hsQgJBkA4A9QAyA4BNAAQBKAAA3g1QA7g4AEhY");
	this.shape_1.setTransform(659.6,245.6);

	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(40));

	// Layer 9
	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#1B1814").ss(11,1,1).p("AkLhfQBDDEDRgGQBWgDBJgrQBKgtAahG");
	this.shape_2.setTransform(696,287.4);

	this.timeline.addTween(cjs.Tween.get(this.shape_2).wait(40));

	// Layer 15
	this.instance = new lib.Symbol2();
	this.instance.parent = this;
	this.instance.setTransform(677.9,291.3,2.017,2.017,0,46.1,51.9,5.4,7.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1).to({regX:4.3,regY:4.3,scaleX:2.02,scaleY:2.02,skewX:45.7,skewY:51.5,x:681.4,y:285.2},0).wait(1).to({scaleX:2.01,scaleY:2.02,skewX:44.8,skewY:50.4,x:681.8,y:285.5},0).wait(1).to({scaleX:2.01,scaleY:2.02,skewX:43.2,skewY:48.6,x:682.7,y:286.3},0).wait(1).to({scaleX:2,scaleY:2.03,skewX:40.7,skewY:45.8,x:684.2,y:287.1},0).wait(1).to({scaleX:2,scaleY:2.04,skewX:37.3,skewY:41.9,x:686.4,y:288.2},0).wait(1).to({scaleX:1.98,scaleY:2.05,skewX:32.8,skewY:36.8,x:689.5,y:289.2},0).wait(1).to({scaleX:1.97,scaleY:2.06,skewX:27.4,skewY:30.6,x:693.3,y:289.8},0).wait(1).to({scaleX:1.96,scaleY:2.08,skewX:21.5,skewY:23.9,x:697.6,y:289.9},0).wait(1).to({scaleX:1.94,scaleY:2.09,skewX:16,skewY:17.6,x:701.1,y:289.5},0).wait(1).to({scaleX:1.93,scaleY:2.1,skewX:11.4,skewY:12.4,x:704,y:288.7},0).wait(1).to({scaleX:1.92,scaleY:2.11,skewX:8,skewY:8.6,x:706.1,y:288},0).wait(1).to({scaleX:1.92,scaleY:2.11,skewX:5.8,skewY:6,x:707.5,y:287.4},0).wait(1).to({scaleX:1.91,scaleY:2.12,skewX:4.5,skewY:4.6,x:708.2,y:287},0).wait(1).to({regX:5.4,regY:7.5,scaleX:1.91,scaleY:2.12,rotation:4,skewX:0,skewY:0,x:710.2,y:293.7},0).wait(7).to({regX:4.3,regY:4.3,scaleX:1.91,scaleY:2.12,rotation:0,skewX:4.6,skewY:4.7,x:708.2,y:287},0).wait(1).to({scaleX:1.92,scaleY:2.11,skewX:6,skewY:6.2,x:707.3,y:287.5},0).wait(1).to({scaleX:1.92,scaleY:2.11,skewX:8.3,skewY:8.8,x:705.8,y:288.1},0).wait(1).to({scaleX:1.93,scaleY:2.1,skewX:11.5,skewY:12.6,x:703.6,y:288.9},0).wait(1).to({scaleX:1.94,scaleY:2.09,skewX:15.7,skewY:17.4,x:700.5,y:289.6},0).wait(1).to({scaleX:1.95,scaleY:2.08,skewX:20.7,skewY:23,x:697,y:290},0).wait(1).to({scaleX:1.97,scaleY:2.06,skewX:26.1,skewY:29.1,x:693.3,y:289.8},0).wait(1).to({scaleX:1.98,scaleY:2.05,skewX:31.2,skewY:35,x:690.1,y:289.5},0).wait(1).to({scaleX:1.99,scaleY:2.04,skewX:35.9,skewY:40.2,x:687.3,y:288.7},0).wait(1).to({scaleX:2,scaleY:2.03,skewX:39.6,skewY:44.5,x:685.1,y:287.7},0).wait(1).to({scaleX:2.01,scaleY:2.03,skewX:42.5,skewY:47.7,x:683.3,y:286.7},0).wait(1).to({scaleX:2.01,scaleY:2.02,skewX:44.4,skewY:50,x:682.1,y:285.9},0).wait(1).to({scaleX:2.02,scaleY:2.02,skewX:45.6,skewY:51.3,x:681.5,y:285.3},0).wait(1).to({regX:5.4,regY:7.5,scaleX:2.02,scaleY:2.02,skewX:46.1,skewY:51.9,x:677.8,y:291.3},0).wait(6));

	// Layer 16 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("AsBPZMAFVggjISuGbQg5OVAONlg");
	mask.setTransform(557,365.2);

	// Layer 7
	this.instance_1 = new lib.Symbol11();
	this.instance_1.parent = this;
	this.instance_1.setTransform(609.1,371.8,1,1,0,0,0,35.1,64.8);

	var maskedShapeInstanceList = [this.instance_1];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(40));

	// Layer 3
	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#1B1815").ss(11.5,1,1).p("AmykCQEXH2JOAP");
	this.shape_3.setTransform(836.9,315.3);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#1B1815").ss(11.5,1,1).p("AmykBQEXH2JOAN");
	this.shape_4.setTransform(836.9,315.2);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#1B1815").ss(11.5,1,1).p("AmykAQEXH2JOAL");
	this.shape_5.setTransform(836.9,315.1);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#1B1815").ss(11.5,1,1).p("Amyj/QEXH2JOAJ");
	this.shape_6.setTransform(836.9,315);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#1B1815").ss(11.5,1,1).p("Amyj+QEXH2JOAH");
	this.shape_7.setTransform(836.9,314.9);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#1B1815").ss(11.5,1,1).p("Amyj9QEXH2JOAF");
	this.shape_8.setTransform(836.9,314.8);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#1B1815").ss(11.5,1,1).p("Amxj8QEXH3JNAC");
	this.shape_9.setTransform(836.9,314.7);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#1B1815").ss(11.5,1,1).p("Amxj6QEXH2JNAA");
	this.shape_10.setTransform(836.9,314.6);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#1B1815").ss(11.5,1,1).p("AmykBQEXH3JOAM");
	this.shape_11.setTransform(836.9,315.2);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#1B1815").ss(11.5,1,1).p("Amyj+QEXH2JOAG");
	this.shape_12.setTransform(836.9,314.9);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#1B1815").ss(11.5,1,1).p("Amyj8QEXH2JOAD");
	this.shape_13.setTransform(836.9,314.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_3}]}).to({state:[{t:this.shape_3}]},4).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_3}]},1).wait(18));
	this.timeline.addTween(cjs.Tween.get(this.shape_3).wait(4).to({_off:true},1).wait(9).to({_off:false},0).wait(1).to({_off:true},1).wait(6).to({_off:false},0).wait(18));

	// Layer 8
	this.instance_2 = new lib.Symbol8();
	this.instance_2.parent = this;
	this.instance_2.setTransform(880.5,341.2,1.588,1.588,0,0,0,-0.8,2.6);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(4).to({rotation:-30},8,cjs.Ease.get(-1)).to({rotation:0},2).wait(1).to({rotation:-30},5).to({rotation:0},2).wait(18));

	// Layer 2
	this.instance_3 = new lib.bg();
	this.instance_3.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(40));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(540,340,1080,680);
// library properties:
lib.properties = {
	id: 'D7DF931991409644B92E9328A60773C7',
	width: 1080,
	height: 680,
	fps: 30,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: [
		{src:"assets/images/bg13.png", id:"bg"}
	],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['D7DF931991409644B92E9328A60773C7'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}



})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;

$(function () {
    var canvas, stage, exportRoot, anim_container, dom_overlay_container, fnStartAnimation;
    function init() {
        canvas = document.getElementById("canvas13");
        anim_container = document.getElementById("animation_container13");
        dom_overlay_container = document.getElementById("dom_overlay_container13");
        var comp=AdobeAn.getComposition("D7DF931991409644B92E9328A60773C7");
        var lib=comp.getLibrary();
        var loader = new createjs.LoadQueue(false);
        loader.addEventListener("fileload", function(evt){handleFileLoad(evt,comp)});
        loader.addEventListener("complete", function(evt){handleComplete(evt,comp)});
        var lib=comp.getLibrary();
        loader.loadManifest(lib.properties.manifest);
    }
    function handleFileLoad(evt, comp) {
        var images=comp.getImages();
        if (evt && (evt.item.type == "image")) { images[evt.item.id] = evt.result; }
    }
    function handleComplete(evt,comp) {
        //This function is always called, irrespective of the content. You can use the variable "stage" after it is created in token create_stage.
        var lib=comp.getLibrary();
        var ss=comp.getSpriteSheet();
        var queue = evt.target;
        var ssMetadata = lib.ssMetadata;
        for(i=0; i<ssMetadata.length; i++) {
            ss[ssMetadata[i].name] = new createjs.SpriteSheet( {"images": [queue.getResult(ssMetadata[i].name)], "frames": ssMetadata[i].frames} )
        }
        exportRoot = new lib.Football();
        stage = new lib.Stage(canvas);
        //Registers the "tick" event listener.
        fnStartAnimation = function() {
            stage.addChild(exportRoot);
            createjs.Ticker.setFPS(lib.properties.fps);
            createjs.Ticker.addEventListener("tick", stage);
        }
        //Code to support hidpi screens and responsive scaling.
        function makeResponsive(isResp, respDim, isScale, scaleType) {
            var lastW, lastH, lastS=1;
            window.addEventListener('resize', resizeCanvas);
            resizeCanvas();
            function resizeCanvas() {
                var w = lib.properties.width, h = lib.properties.height;
                var iw = window.innerWidth, ih=window.innerHeight;
                var pRatio = window.devicePixelRatio || 1, xRatio=iw/w, yRatio=ih/h, sRatio=1;
                if(isResp) {
                    if((respDim=='width'&&lastW==iw) || (respDim=='height'&&lastH==ih)) {
                        sRatio = lastS;
                    }
                    else if(!isScale) {
                        if(iw<w || ih<h)
                            sRatio = Math.min(xRatio, yRatio);
                    }
                    else if(scaleType==1) {
                        sRatio = Math.min(xRatio, yRatio);
                    }
                    else if(scaleType==2) {
                        sRatio = Math.max(xRatio, yRatio);
                    }
                }
                canvas.width = w*pRatio*sRatio;
                canvas.height = h*pRatio*sRatio;
                canvas.style.width = dom_overlay_container.style.width = anim_container.style.width =  w*sRatio+'px';
                canvas.style.height = anim_container.style.height = dom_overlay_container.style.height = h*sRatio+'px';
                stage.scaleX = pRatio*sRatio;
                stage.scaleY = pRatio*sRatio;
                lastW = iw; lastH = ih; lastS = sRatio;
                stage.tickOnUpdate = false;
                stage.update();
                stage.tickOnUpdate = true;
            }
        }
        makeResponsive(true,'both',true,2);
        AdobeAn.compositionLoaded(lib.properties.id);
        fnStartAnimation();
    }
    init();
});