(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [];


// symbols:



(lib.miska = function() {
	this.initialize(img.miska);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,269,200);


(lib.pack = function() {
	this.initialize(img.pack);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,444,479);


(lib.yazik = function() {
	this.initialize(img.yazik);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,16,21);// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.Symbol2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(11.5,1,1).p("AhFgTQCXhrgNC5");
	this.shape.setTransform(7,17.5);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(11.5,1,1).p("AhCgjQCRhXgNC5");
	this.shape_1.setTransform(7.3,17.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(11.5,1,1).p("Ag/gyQCLhCgNC5");
	this.shape_2.setTransform(7.6,16.5);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(11.5,1,1).p("Ag8hBQCFgtgOC5");
	this.shape_3.setTransform(8,15.9);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(11.5,1,1).p("Ag5hOQB/gZgOC5");
	this.shape_4.setTransform(8.3,15.2);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(11.5,1,1).p("Ag1hZQB4gFgOC5");
	this.shape_5.setTransform(8.6,14.3);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(11.5,1,1).p("AgyhkQByAQgOC5");
	this.shape_6.setTransform(8.9,13.3);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(11.5,1,1).p("AgvhuQBrAkgNC5");
	this.shape_7.setTransform(9.2,12.3);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(11.5,1,1).p("Agsh4QBlA4gNC5");
	this.shape_8.setTransform(9.5,11.3);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(11.5,1,1).p("Agth+QBoBDgPC7");
	this.shape_9.setTransform(9.5,10.7);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(11.5,1,1).p("AgtiFQBpBOgPC9");
	this.shape_10.setTransform(9.4,10);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(11.5,1,1).p("AguiLQBrBYgQC/");
	this.shape_11.setTransform(9.4,9.4);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(11.5,1,1).p("AgviSQBtBkgRDB");
	this.shape_12.setTransform(9.4,8.7);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(11.5,1,1).p("AgviYQBuBugRDD");
	this.shape_13.setTransform(9.3,8.1);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(11.5,1,1).p("AgwifQBwB6gSDF");
	this.shape_14.setTransform(9.3,7.5);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(11.5,1,1).p("AgwilQByCEgTDH");
	this.shape_15.setTransform(9.2,6.8);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(11.5,1,1).p("AgziVQB3BmgSDF");
	this.shape_16.setTransform(8.9,8.4);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(11.5,1,1).p("Ag1iFQB7BIgSDD");
	this.shape_17.setTransform(8.7,10);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#000000").ss(11.5,1,1).p("Ag4h2QCAArgRDC");
	this.shape_18.setTransform(8.4,11.6);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#000000").ss(11.5,1,1).p("Ag7hlQCFAMgQDA");
	this.shape_19.setTransform(8.1,13.2);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#000000").ss(11.5,1,1).p("Ag9hVQCJgSgPC+");
	this.shape_20.setTransform(7.8,14.7);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#000000").ss(11.5,1,1).p("AhAhBQCOgwgPC9");
	this.shape_21.setTransform(7.6,15.8);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#000000").ss(11.5,1,1).p("AhCgrQCShNgOC6");
	this.shape_22.setTransform(7.3,16.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},29).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape}]},1).wait(43));

	// Layer 3
	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#000000").ss(11.5,1,1).p("AhniKQE+AUisEB");
	this.shape_23.setTransform(13.2,13.9);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#000000").ss(11.5,1,1).p("AhhiSQEvAminD/");
	this.shape_24.setTransform(13.6,13.2);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f().s("#000000").ss(11.5,1,1).p("AhciZQEgA3ihD8");
	this.shape_25.setTransform(14,12.5);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f().s("#000000").ss(11.5,1,1).p("AhWihQERBIicD7");
	this.shape_26.setTransform(14.4,11.7);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f().s("#000000").ss(11.5,1,1).p("AhRioQECBZiXD4");
	this.shape_27.setTransform(14.9,11);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f().s("#000000").ss(11.5,1,1).p("AhMivQD0BqiSD1");
	this.shape_28.setTransform(15.3,10.2);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f().s("#000000").ss(11.5,1,1).p("AhGi3QDkB8iMDz");
	this.shape_29.setTransform(15.7,9.5);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f().s("#000000").ss(11.5,1,1).p("AhBi+QDWCMiHDx");
	this.shape_30.setTransform(16.1,8.8);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f().s("#000000").ss(11.5,1,1).p("Ag8jGQDHCeiCDv");
	this.shape_31.setTransform(16.6,8);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f().s("#000000").ss(11.5,1,1).p("Ag/i+QDFCShsDr");
	this.shape_32.setTransform(14.9,8.8);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f().s("#000000").ss(11.5,1,1).p("AhEi2QDFCFhXDo");
	this.shape_33.setTransform(13.3,9.6);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f().s("#000000").ss(11.5,1,1).p("AhJiuQDEB5hCDk");
	this.shape_34.setTransform(11.8,10.3);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f().s("#000000").ss(11.5,1,1).p("AhPinQDDBugtDh");
	this.shape_35.setTransform(10.4,11.1);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f().s("#000000").ss(11.5,1,1).p("AhWifQDBBhgXDe");
	this.shape_36.setTransform(9.1,11.9);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f().s("#000000").ss(11.5,1,1).p("AhfiXQDBBVgCDa");
	this.shape_37.setTransform(7.9,12.6);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f().s("#000000").ss(11.5,1,1).p("AhpiQQC/BKAUDX");
	this.shape_38.setTransform(6.9,13.4);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f().s("#000000").ss(11.5,1,1).p("AhliPQDPBDgEDc");
	this.shape_39.setTransform(7.3,13.5);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f().s("#000000").ss(11.5,1,1).p("AhiiOQDfA8gdDh");
	this.shape_40.setTransform(7.9,13.5);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f().s("#000000").ss(11.5,1,1).p("AhhiOQDvA2g1Dn");
	this.shape_41.setTransform(8.6,13.6);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f().s("#000000").ss(11.5,1,1).p("AhhiNQD+AvhMDs");
	this.shape_42.setTransform(9.4,13.7);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f().s("#000000").ss(11.5,1,1).p("AhiiMQEOAohkDx");
	this.shape_43.setTransform(10.3,13.7);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f().s("#000000").ss(11.5,1,1).p("AhjiMQEeAih8D3");
	this.shape_44.setTransform(11.2,13.8);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f().s("#000000").ss(11.5,1,1).p("AhliLQEuAbiUD8");
	this.shape_45.setTransform(12.2,13.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_23}]}).to({state:[{t:this.shape_23}]},29).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_27}]},1).to({state:[{t:this.shape_28}]},1).to({state:[{t:this.shape_29}]},1).to({state:[{t:this.shape_30}]},1).to({state:[{t:this.shape_31}]},1).to({state:[{t:this.shape_32}]},1).to({state:[{t:this.shape_33}]},1).to({state:[{t:this.shape_34}]},1).to({state:[{t:this.shape_35}]},1).to({state:[{t:this.shape_36}]},1).to({state:[{t:this.shape_37}]},1).to({state:[{t:this.shape_38}]},1).to({state:[{t:this.shape_39}]},1).to({state:[{t:this.shape_40}]},1).to({state:[{t:this.shape_41}]},1).to({state:[{t:this.shape_42}]},1).to({state:[{t:this.shape_43}]},1).to({state:[{t:this.shape_44}]},1).to({state:[{t:this.shape_45}]},1).to({state:[{t:this.shape_23}]},1).wait(43));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-5.7,-5.7,35.1,39.4);


(lib.Symbol1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.miska();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol1, new cjs.Rectangle(0,0,269,200), null);


(lib.Symbol2_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.yazik();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol2_1, new cjs.Rectangle(0,0,16,21), null);


// stage content:
(lib.Dacha = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 11
	this.instance = new lib.Symbol2();
	this.instance.parent = this;
	this.instance.setTransform(781.2,421.6,1,1,0,0,0,15.1,27);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(14).to({rotation:-50.2,x:739.2,y:431.4},12,cjs.Ease.get(1)).wait(31).to({rotation:0,x:781.2,y:421.6},12,cjs.Ease.get(1)).wait(26));

	// Layer 10
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(11.5,1,1).p("AFSk0QAdDKg2CYQgzCPhtBGQhsBEiCgYQiJgZh9h7");
	this.shape.setTransform(817.6,406.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(11.5,1,1).p("Al5CwQCZBnCCAUQB8ASBrg+QBug+BFiNQBIiVgLjN");
	this.shape_1.setTransform(813.7,405.4);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(11.5,1,1).p("AmZDDQCzBVB8APQB2ANBrg4QBug4BWiLQBaiRAEjQ");
	this.shape_2.setTransform(810.5,404.8);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(11.5,1,1).p("Am1DUQDKBEB2AKQBxAJBrgyQBtgyBniJQBoiOATjT");
	this.shape_3.setTransform(807.6,404.3);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(11.5,1,1).p("AnPDjQDgA2BvAGQBtAEBsgsQBtguB1iHQB1iMAgjU");
	this.shape_4.setTransform(805,403.9);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(11.5,1,1).p("AnmDwQDyApBsACQBpACBrgpQBtgpCBiGQCCiJArjW");
	this.shape_5.setTransform(802.7,403.5);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(11.5,1,1).p("An5D7QECAeBngBQBlgCBsgkQBtglCMiFQCMiHA0jY");
	this.shape_6.setTransform(800.8,403.2);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(11.5,1,1).p("AoKEFQEQAUBkgEQBigEBsgiQBsghCViEQCViFA9ja");
	this.shape_7.setTransform(799.1,402.9);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(11.5,1,1).p("AoYEMQEbANBhgHQBggGBsgfQBsgfCdiDQCciDBEjb");
	this.shape_8.setTransform(797.7,402.8);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(11.5,1,1).p("AoiERQEjAGBfgHQBegJBsgcQBsgdCiiCQCiiDBJjc");
	this.shape_9.setTransform(796.7,402.7);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(11.5,1,1).p("AoqETQEqADBdgJQBdgKBsgbQBsgbCmiCQCmiCBNjc");
	this.shape_10.setTransform(795.9,402.8);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(11.5,1,1).p("AovEVQEugBBcgJQBcgLBsgaQBsgaCpiCQCoiBBPjd");
	this.shape_11.setTransform(795.5,402.8);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(11.5,1,1).p("AIxkUQhQDdipCBQiqCBhsAaQhrAbhcAKQhcAKkvAB");
	this.shape_12.setTransform(795.3,402.9);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(11.5,1,1).p("AoNEGQESATBkgEQBhgFBsghQBsghCXiEQCXiEA+ja");
	this.shape_13.setTransform(798.8,402.9);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(11.5,1,1).p("AnuD0QD5AlBqABQBnAABsgnQBsgnCGiGQCGiIAujX");
	this.shape_14.setTransform(802,403.4);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(11.5,1,1).p("AnQDkQDgA1BwAGQBtAEBrgsQBtgtB2iIQB2iLAhjV");
	this.shape_15.setTransform(804.9,403.8);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(11.5,1,1).p("Am3DVQDMBEB1AJQByAJBrgyQBtgyBniJQBqiOATjS");
	this.shape_16.setTransform(807.4,404.3);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(11.5,1,1).p("AmgDHQC5BRB6ANQB1AMBsg2QBtg2BbiLQBdiQAIjR");
	this.shape_17.setTransform(809.7,404.7);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#000000").ss(11.5,1,1).p("AmNC7QCpBcB/ARQB4APBrg6QBug6BQiMQBTiSgCjP");
	this.shape_18.setTransform(811.7,405.1);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#000000").ss(11.5,1,1).p("Al8CxQCbBmCCATQB7ASBrg9QBug+BHiNQBKiUgKjN");
	this.shape_19.setTransform(813.4,405.4);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#000000").ss(11.5,1,1).p("AlwCpQCRBtCEAWQB+AUBrhAQBuhAA/iOQBDiWgQjM");
	this.shape_20.setTransform(814.8,405.6);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#000000").ss(11.5,1,1).p("AlmCjQCHBzCHAXQB/AWBshCQBthCA6iPQA9iXgVjL");
	this.shape_21.setTransform(816,405.8);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#000000").ss(11.5,1,1).p("AlgCeQCCB4CIAYQCBAXBrhDQBuhEA2iPQA5iYgajK");
	this.shape_22.setTransform(816.9,406);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#000000").ss(11.5,1,1).p("AlcCbQB9B7CKAZQCBAYBshFQBthFA0iPQA2iYgbjK");
	this.shape_23.setTransform(817.4,406.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},14).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_12}]},31).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape}]},1).wait(26));

	// Layer 6
	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#000000").ss(11.5,1,1).p("AjWAZQAXAJA8gIQA8gIBIgJQBJgJA+gHQA9gIASgO");
	this.shape_24.setTransform(756,334.6);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f().s("#000000").ss(11.5,1,1).p("AjVAOQAXASA9gCQA7gDBJgIQBIgIA9gLQA9gMARgR");
	this.shape_25.setTransform(756,334.3);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f().s("#000000").ss(11.5,1,1).p("AjVACQAYAbA9ADQA7ACBJgIQBIgIA9gOQA8gOARgW");
	this.shape_26.setTransform(756,334.1);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f().s("#000000").ss(11.5,1,1).p("AjUgJQAYAiA9AIQA7AGBJgHQBIgIA8gRQA8gRAQgZ");
	this.shape_27.setTransform(756,334.1);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f().s("#000000").ss(11.5,1,1).p("AjUgUQAYApA/AMQA5ALBKgHQBIgIA7gUQA7gTARgd");
	this.shape_28.setTransform(756,334.1);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f().s("#000000").ss(11.5,1,1).p("AjUgfQAYAwBAAQQA5APBKgHQBHgHA8gXQA6gWARgf");
	this.shape_29.setTransform(756,334.2);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f().s("#000000").ss(11.5,1,1).p("AjTgoQAYA1A/AUQA6ASBKgHQBHgGA7gZQA6gYAQgi");
	this.shape_30.setTransform(756,334.2);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f().s("#000000").ss(11.5,1,1).p("AjTgxQAYA7BAAXQA5AVBLgGQBGgHA7gaQA6gbAQgk");
	this.shape_31.setTransform(756,334.3);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f().s("#000000").ss(11.5,1,1).p("AjSg1QAYA/BAAaQA5AYBKgHQBGgGA7gcQA6gcAPgn");
	this.shape_32.setTransform(756,334);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f().s("#000000").ss(11.5,1,1).p("AjSg5QAXBCBBAdQA5AaBLgHQBGgGA6gdQA6geAPgo");
	this.shape_33.setTransform(756.1,333.9);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f().s("#000000").ss(11.5,1,1).p("AjSg9QAYBGBAAfQA6AbBKgGQBGgGA6gfQA6geAPgq");
	this.shape_34.setTransform(756.1,333.7);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f().s("#000000").ss(11.5,1,1).p("AjShAQAYBIBBAgQA5AdBLgGQBFgGA6gfQA6ggAPgq");
	this.shape_35.setTransform(756,333.7);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f().s("#000000").ss(11.5,1,1).p("AjRhCQAYBKBBAiQA4AdBLgGQBGgGA5ggQA6ggAPgr");
	this.shape_36.setTransform(756.1,333.6);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f().s("#000000").ss(11.5,1,1).p("AjRhDQAYBLBBAiQA4AfBLgGQBGgHA5ggQA6ggAPgs");
	this.shape_37.setTransform(756.1,333.5);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f().s("#000000").ss(11.5,1,1).p("AjRhDQAYBLBBAjQA4AeBLgGQBGgGA5ggQA6ghAPgs");
	this.shape_38.setTransform(756.1,333.5);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f().s("#000000").ss(11.5,1,1).p("AjSg5QAYBCBAAcQA5AaBLgHQBGgGA6gdQA6gdAPgo");
	this.shape_39.setTransform(756,333.9);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f().s("#000000").ss(11.5,1,1).p("AjTgvQAYA5BAAXQA5AUBKgGQBHgHA6gaQA7gaAQgk");
	this.shape_40.setTransform(756,334.3);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f().s("#000000").ss(11.5,1,1).p("AjTgiQAXAxBAATQA5APBKgHQBHgHA7gXQA7gXAQgg");
	this.shape_41.setTransform(756,334.2);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f().s("#000000").ss(11.5,1,1).p("AjUgWQAYAqA/AOQA5ALBKgHQBIgHA7gVQA7gUARgd");
	this.shape_42.setTransform(756,334.1);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f().s("#000000").ss(11.5,1,1).p("AjUgMQAXAjA+AKQA7AIBJgIQBIgHA7gSQA8gSARga");
	this.shape_43.setTransform(756,334.1);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f().s("#000000").ss(11.5,1,1).p("AjVgDQAYAeA9AGQA7AEBKgHQBHgIA8gQQA8gQASgX");
	this.shape_44.setTransform(756,334.1);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f().s("#000000").ss(11.5,1,1).p("AjVAEQAYAaA9ACQA7ABBJgHQBIgIA9gOQA8gOARgV");
	this.shape_45.setTransform(756,334.1);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.f().s("#000000").ss(11.5,1,1).p("AjVAKQAXAVA9AAQA7gCBJgIQBJgIA8gMQA9gMARgT");
	this.shape_46.setTransform(756,334.2);

	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.f().s("#000000").ss(11.5,1,1).p("AjWAPQAYASA9gDQA7gEBJgIQBIgIA9gKQA9gMARgR");
	this.shape_47.setTransform(756,334.3);

	this.shape_48 = new cjs.Shape();
	this.shape_48.graphics.f().s("#000000").ss(11.5,1,1).p("AjWATQAYAOA8gEQA7gGBKgIQBIgIA9gJQA9gLARgQ");
	this.shape_48.setTransform(756,334.4);

	this.shape_49 = new cjs.Shape();
	this.shape_49.graphics.f().s("#000000").ss(11.5,1,1).p("AjWAVQAYANA8gGQA7gHBJgJQBJgIA9gIQA9gKASgP");
	this.shape_49.setTransform(756,334.5);

	this.shape_50 = new cjs.Shape();
	this.shape_50.graphics.f().s("#000000").ss(11.5,1,1).p("AjWAYQAXAKA8gHQA8gIBIgIQBJgJA+gHQA9gJASgO");
	this.shape_50.setTransform(756,334.5);

	this.shape_51 = new cjs.Shape();
	this.shape_51.graphics.f().s("#000000").ss(11.5,1,1).p("AjWAYQAXAKA8gIQA8gJBIgIQBJgIA+gIQA9gIASgO");
	this.shape_51.setTransform(756,334.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_24}]}).to({state:[{t:this.shape_24}]},6).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_27}]},1).to({state:[{t:this.shape_28}]},1).to({state:[{t:this.shape_29}]},1).to({state:[{t:this.shape_30}]},1).to({state:[{t:this.shape_31}]},1).to({state:[{t:this.shape_32}]},1).to({state:[{t:this.shape_33}]},1).to({state:[{t:this.shape_34}]},1).to({state:[{t:this.shape_35}]},1).to({state:[{t:this.shape_36}]},1).to({state:[{t:this.shape_37}]},1).to({state:[{t:this.shape_38}]},1).to({state:[{t:this.shape_38}]},41).to({state:[{t:this.shape_39}]},1).to({state:[{t:this.shape_40}]},1).to({state:[{t:this.shape_41}]},1).to({state:[{t:this.shape_42}]},1).to({state:[{t:this.shape_43}]},1).to({state:[{t:this.shape_44}]},1).to({state:[{t:this.shape_45}]},1).to({state:[{t:this.shape_46}]},1).to({state:[{t:this.shape_47}]},1).to({state:[{t:this.shape_48}]},1).to({state:[{t:this.shape_49}]},1).to({state:[{t:this.shape_50}]},1).to({state:[{t:this.shape_51}]},1).to({state:[{t:this.shape_24}]},1).wait(20));

	// Layer 5
	this.shape_52 = new cjs.Shape();
	this.shape_52.graphics.f().s("#000000").ss(11.5,1,1).p("AiXAVQAogPAsgGQAtgHAagDQAagEArgFQArgFAkAM");
	this.shape_52.setTransform(791,286.9);

	this.shape_53 = new cjs.Shape();
	this.shape_53.graphics.f().s("#000000").ss(11.5,1,1).p("AiXAXQAmgWAtgJQAtgKAdgCQAegCAqAAQAqACAgAU");
	this.shape_53.setTransform(791,288.2);

	this.shape_54 = new cjs.Shape();
	this.shape_54.graphics.f().s("#000000").ss(11.5,1,1).p("AiXAbQAlgbAtgMQAtgNAggBQAggBApAGQArAIAcAa");
	this.shape_54.setTransform(790.9,289.2);

	this.shape_55 = new cjs.Shape();
	this.shape_55.graphics.f().s("#000000").ss(11.5,1,1).p("AiWAfQAjggAtgPQAtgOAjAAQAjAAAoAKQArANAXAh");
	this.shape_55.setTransform(790.9,290);

	this.shape_56 = new cjs.Shape();
	this.shape_56.graphics.f().s("#000000").ss(11.5,1,1).p("AiWAiQAiglAtgRQAtgRAlABQAmABAoAQQAqARAUAn");
	this.shape_56.setTransform(790.8,290.9);

	this.shape_57 = new cjs.Shape();
	this.shape_57.graphics.f().s("#000000").ss(11.5,1,1).p("AiWAiQAhgpAtgUQAugTAnACQAoACAnAUQAqAWARAt");
	this.shape_57.setTransform(790.8,291.9);

	this.shape_58 = new cjs.Shape();
	this.shape_58.graphics.f().s("#000000").ss(11.5,1,1).p("AiWAjQAgguAtgVQAugVApADQAqADAnAXQApAZAPAz");
	this.shape_58.setTransform(790.8,292.8);

	this.shape_59 = new cjs.Shape();
	this.shape_59.graphics.f().s("#000000").ss(11.5,1,1).p("AiWAjQAfgxAugXQAtgXArAEQAsAEAnAaQApAdAMA3");
	this.shape_59.setTransform(790.7,293.6);

	this.shape_60 = new cjs.Shape();
	this.shape_60.graphics.f().s("#000000").ss(11.5,1,1).p("AiWAjQAfg0AugYQAtgYAtAEQAtAEAmAeQApAfAKA7");
	this.shape_60.setTransform(790.7,294.3);

	this.shape_61 = new cjs.Shape();
	this.shape_61.graphics.f().s("#000000").ss(11.5,1,1).p("AiWAjQAeg3AugZQAtgZAuAFQAvAEAmAgQApAiAIA+");
	this.shape_61.setTransform(790.7,294.9);

	this.shape_62 = new cjs.Shape();
	this.shape_62.graphics.f().s("#000000").ss(11.5,1,1).p("AiWAjQAeg5AugaQAtgaAvAFQAwAFAlAiQAqAkAGBB");
	this.shape_62.setTransform(790.6,295.4);

	this.shape_63 = new cjs.Shape();
	this.shape_63.graphics.f().s("#000000").ss(11.5,1,1).p("AiWAkQAdg7AugbQAugbAwAFQAwAGAlAkQAqAlAFBD");
	this.shape_63.setTransform(790.6,295.8);

	this.shape_64 = new cjs.Shape();
	this.shape_64.graphics.f().s("#000000").ss(11.5,1,1).p("AiVAkQAdg8AugcQAtgbAxAGQAxAFAlAlQApAnAEBE");
	this.shape_64.setTransform(790.6,296);

	this.shape_65 = new cjs.Shape();
	this.shape_65.graphics.f().s("#000000").ss(11.5,1,1).p("AiVAkQAcg9AugcQAugcAxAGQAxAGAlAlQApAoAEBF");
	this.shape_65.setTransform(790.6,296.2);

	this.shape_66 = new cjs.Shape();
	this.shape_66.graphics.f().s("#000000").ss(11.5,1,1).p("AiVAkQAcg9AugcQAugcAwAGQAyAGAlAlQApAoAEBF");
	this.shape_66.setTransform(790.6,296.3);

	this.shape_67 = new cjs.Shape();
	this.shape_67.graphics.f().s("#000000").ss(11.5,1,1).p("AiWAjQAeg2AugZQAtgZAuAEQAuAFAmAgQAqAhAIA9");
	this.shape_67.setTransform(790.7,294.8);

	this.shape_68 = new cjs.Shape();
	this.shape_68.graphics.f().s("#000000").ss(11.5,1,1).p("AiWAjQAfgxAugWQAtgWArADQAsADAnAbQApAbAMA2");
	this.shape_68.setTransform(790.7,293.5);

	this.shape_69 = new cjs.Shape();
	this.shape_69.graphics.f().s("#000000").ss(11.5,1,1).p("AiWAjQAhgrAtgUQAtgUAoACQApACAnAWQAqAWAQAv");
	this.shape_69.setTransform(790.8,292.2);

	this.shape_70 = new cjs.Shape();
	this.shape_70.graphics.f().s("#000000").ss(11.5,1,1).p("AiWAiQAigmAtgSQAugRAlABQAmABAoARQAqASAUAp");
	this.shape_70.setTransform(790.8,291.1);

	this.shape_71 = new cjs.Shape();
	this.shape_71.graphics.f().s("#000000").ss(11.5,1,1).p("AiWAhQAjgiAtgQQAtgPAjAAQAkAAAoANQArAOAWAj");
	this.shape_71.setTransform(790.9,290.2);

	this.shape_72 = new cjs.Shape();
	this.shape_72.graphics.f().s("#000000").ss(11.5,1,1).p("AiXAdQAlgeAtgNQAtgOAhAAQAigBApAJQAqAKAaAe");
	this.shape_72.setTransform(790.9,289.6);

	this.shape_73 = new cjs.Shape();
	this.shape_73.graphics.f().s("#000000").ss(11.5,1,1).p("AiXAaQAlgaAtgMQAtgMAggBQAggBApAFQArAHAcAZ");
	this.shape_73.setTransform(790.9,289.1);

	this.shape_74 = new cjs.Shape();
	this.shape_74.graphics.f().s("#000000").ss(11.5,1,1).p("AiXAYQAmgYAsgKQAugKAdgCQAggCApADQAqADAfAW");
	this.shape_74.setTransform(791,288.6);

	this.shape_75 = new cjs.Shape();
	this.shape_75.graphics.f().s("#000000").ss(11.5,1,1).p("AiXAXQAmgWAtgJQAtgJAdgCQAdgDAqABQArABAgAT");
	this.shape_75.setTransform(791,288.1);

	this.shape_76 = new cjs.Shape();
	this.shape_76.graphics.f().s("#000000").ss(11.5,1,1).p("AiXAWQAngTAsgIQAtgIAcgDQAdgDApgCQArgBAiAR");
	this.shape_76.setTransform(791,287.7);

	this.shape_77 = new cjs.Shape();
	this.shape_77.graphics.f().s("#000000").ss(11.5,1,1).p("AiXAWQAngSAtgHQAtgHAbgDQAbgEArgDQAqgDAjAP");
	this.shape_77.setTransform(791,287.3);

	this.shape_78 = new cjs.Shape();
	this.shape_78.graphics.f().s("#000000").ss(11.5,1,1).p("AiXAVQAngQAtgGQAtgHAagEQAbgDAqgFQArgEAkAO");
	this.shape_78.setTransform(791,287.1);

	this.shape_79 = new cjs.Shape();
	this.shape_79.graphics.f().s("#000000").ss(11.5,1,1).p("AiXAVQAogPAsgHQAtgGAagEQAagEArgEQArgFAkAN");
	this.shape_79.setTransform(791,286.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_52}]}).to({state:[{t:this.shape_52}]},6).to({state:[{t:this.shape_53}]},1).to({state:[{t:this.shape_54}]},1).to({state:[{t:this.shape_55}]},1).to({state:[{t:this.shape_56}]},1).to({state:[{t:this.shape_57}]},1).to({state:[{t:this.shape_58}]},1).to({state:[{t:this.shape_59}]},1).to({state:[{t:this.shape_60}]},1).to({state:[{t:this.shape_61}]},1).to({state:[{t:this.shape_62}]},1).to({state:[{t:this.shape_63}]},1).to({state:[{t:this.shape_64}]},1).to({state:[{t:this.shape_65}]},1).to({state:[{t:this.shape_66}]},1).to({state:[{t:this.shape_66}]},46).to({state:[{t:this.shape_67}]},1).to({state:[{t:this.shape_68}]},1).to({state:[{t:this.shape_69}]},1).to({state:[{t:this.shape_70}]},1).to({state:[{t:this.shape_71}]},1).to({state:[{t:this.shape_72}]},1).to({state:[{t:this.shape_73}]},1).to({state:[{t:this.shape_74}]},1).to({state:[{t:this.shape_75}]},1).to({state:[{t:this.shape_76}]},1).to({state:[{t:this.shape_77}]},1).to({state:[{t:this.shape_78}]},1).to({state:[{t:this.shape_79}]},1).to({state:[{t:this.shape_52}]},1).wait(15));

	// Layer 4
	this.shape_80 = new cjs.Shape();
	this.shape_80.graphics.f().s("#000000").ss(11.5,1,1).p("AiQAFQAzgGAcgBQAcgBARgBQASgBAhABQAhAABRAK");
	this.shape_80.setTransform(729,286.2);

	this.shape_81 = new cjs.Shape();
	this.shape_81.graphics.f().s("#000000").ss(11.5,1,1).p("AiNANQAsgNAdgFQAcgFAVgCQAVgCAjAEQAiAEBHAQ");
	this.shape_81.setTransform(728.8,287);

	this.shape_82 = new cjs.Shape();
	this.shape_82.graphics.f().s("#000000").ss(11.5,1,1).p("AiLAVQAmgTAdgJQAcgJAZgDQAZgCAjAHQAjAGA/AW");
	this.shape_82.setTransform(728.7,287.7);

	this.shape_83 = new cjs.Shape();
	this.shape_83.graphics.f().s("#000000").ss(11.5,1,1).p("AiJAbQAggXAegOQAdgNAcgDQAcgDAjAKQAlAKA4Aa");
	this.shape_83.setTransform(728.5,288.4);

	this.shape_84 = new cjs.Shape();
	this.shape_84.graphics.f().s("#000000").ss(11.5,1,1).p("AiGAiQAagcAegRQAdgRAggEQAfgDAjAMQAmAMAwAg");
	this.shape_84.setTransform(728.4,289);

	this.shape_85 = new cjs.Shape();
	this.shape_85.graphics.f().s("#000000").ss(11.5,1,1).p("AiEAnQAVghAfgUQAdgTAjgEQAhgFAkAOQAnAQApAj");
	this.shape_85.setTransform(728.2,289.6);

	this.shape_86 = new cjs.Shape();
	this.shape_86.graphics.f().s("#000000").ss(11.5,1,1).p("AiDAsQARgkAggXQAdgXAlgEQAkgFAlAQQAnARAkAo");
	this.shape_86.setTransform(728.1,290.1);

	this.shape_87 = new cjs.Shape();
	this.shape_87.graphics.f().s("#000000").ss(11.5,1,1).p("AiBAxQAMgoAhgZQAegZAngFQAmgGAlASQAnAUAfAr");
	this.shape_87.setTransform(728.1,290.5);

	this.shape_88 = new cjs.Shape();
	this.shape_88.graphics.f().s("#000000").ss(11.5,1,1).p("AiAA1QAJgrAhgcQAegbApgFQAogGAlATQApAWAaAu");
	this.shape_88.setTransform(728,290.9);

	this.shape_89 = new cjs.Shape();
	this.shape_89.graphics.f().s("#000000").ss(11.5,1,1).p("Ah/A4QAHgtAhgeQAegdAqgFQAqgHAlAVQApAXAXAw");
	this.shape_89.setTransform(727.9,291.3);

	this.shape_90 = new cjs.Shape();
	this.shape_90.graphics.f().s("#000000").ss(11.5,1,1).p("Ah+A6QAEgvAhgfQAfgeArgGQArgGAmAVQApAYAUAz");
	this.shape_90.setTransform(727.8,291.5);

	this.shape_91 = new cjs.Shape();
	this.shape_91.graphics.f().s("#000000").ss(11.5,1,1).p("Ah9A8QACgwAhghQAfgfAtgGQAsgHAlAXQAqAZARA0");
	this.shape_91.setTransform(727.8,291.7);

	this.shape_92 = new cjs.Shape();
	this.shape_92.graphics.f().s("#000000").ss(11.5,1,1).p("Ah8A+QAAgyAighQAeggAugGQAsgHAmAXQAqAZAPA1");
	this.shape_92.setTransform(727.8,291.9);

	this.shape_93 = new cjs.Shape();
	this.shape_93.graphics.f().s("#000000").ss(11.5,1,1).p("Ah8A/QAAgzAhghQAfghAugGQAtgHAmAXQAqAaAOA2");
	this.shape_93.setTransform(727.7,292);

	this.shape_94 = new cjs.Shape();
	this.shape_94.graphics.f().s("#000000").ss(11.5,1,1).p("Ah8A/QAAgyAhgiQAfghAugGQAtgHAmAXQAqAaAOA2");
	this.shape_94.setTransform(727.7,292);

	this.shape_95 = new cjs.Shape();
	this.shape_95.graphics.f().s("#000000").ss(11.5,1,1).p("Ah/A3QAHgsAggeQAfgcAqgFQApgHAlAVQApAWAYAw");
	this.shape_95.setTransform(727.9,291.2);

	this.shape_96 = new cjs.Shape();
	this.shape_96.graphics.f().s("#000000").ss(11.5,1,1).p("AiBAwQANgnAggZQAegZAngFQAmgFAkARQAoAUAgAq");
	this.shape_96.setTransform(728.1,290.5);

	this.shape_97 = new cjs.Shape();
	this.shape_97.graphics.f().s("#000000").ss(11.5,1,1).p("AiEApQAUgiAfgVQAdgUAjgFQAjgFAkAPQAnAQAoAm");
	this.shape_97.setTransform(728.2,289.8);

	this.shape_98 = new cjs.Shape();
	this.shape_98.graphics.f().s("#000000").ss(11.5,1,1).p("AiGAjQAZgeAfgRQAdgRAggEQAggEAkAMQAlAOAvAg");
	this.shape_98.setTransform(728.3,289.2);

	this.shape_99 = new cjs.Shape();
	this.shape_99.graphics.f().s("#000000").ss(11.5,1,1).p("AiIAdQAegYAegPQAdgOAdgDQAdgEAkALQAkAKA2Ad");
	this.shape_99.setTransform(728.5,288.6);

	this.shape_100 = new cjs.Shape();
	this.shape_100.graphics.f().s("#000000").ss(11.5,1,1).p("AiJAYQAigVAdgLQAdgLAbgDQAagDAjAIQAkAJA7AY");
	this.shape_100.setTransform(728.6,288.1);

	this.shape_101 = new cjs.Shape();
	this.shape_101.graphics.f().s("#000000").ss(11.5,1,1).p("AiLAUQAmgSAdgJQAdgJAZgCQAYgDAjAHQAjAGBAAV");
	this.shape_101.setTransform(728.7,287.7);

	this.shape_102 = new cjs.Shape();
	this.shape_102.graphics.f().s("#000000").ss(11.5,1,1).p("AiMAQQApgPAdgHQAcgGAXgCQAXgDAiAGQAjAEBEAS");
	this.shape_102.setTransform(728.8,287.3);

	this.shape_103 = new cjs.Shape();
	this.shape_103.graphics.f().s("#000000").ss(11.5,1,1).p("AiNANQAsgNAcgEQAdgFAVgCQAUgCAjAEQAiADBIAQ");
	this.shape_103.setTransform(728.8,286.9);

	this.shape_104 = new cjs.Shape();
	this.shape_104.graphics.f().s("#000000").ss(11.5,1,1).p("AiOAKQAvgKAcgEQAcgDAUgCQATgBAiADQAiACBLAN");
	this.shape_104.setTransform(728.9,286.7);

	this.shape_105 = new cjs.Shape();
	this.shape_105.graphics.f().s("#000000").ss(11.5,1,1).p("AiPAIQAxgIAcgDQAcgCASgCQAUgBAhACQAhABBOAM");
	this.shape_105.setTransform(728.9,286.5);

	this.shape_106 = new cjs.Shape();
	this.shape_106.graphics.f().s("#000000").ss(11.5,1,1).p("AiQAHQAygHAcgCQAcgCASgBQATgBAhABQAhABBPAL");
	this.shape_106.setTransform(729,286.3);

	this.shape_107 = new cjs.Shape();
	this.shape_107.graphics.f().s("#000000").ss(11.5,1,1).p("AiQAGQAzgGAcgDQAcAAARgCQASAAAhAAQAhAABRAL");
	this.shape_107.setTransform(729,286.3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_80}]}).to({state:[{t:this.shape_80}]},6).to({state:[{t:this.shape_81}]},1).to({state:[{t:this.shape_82}]},1).to({state:[{t:this.shape_83}]},1).to({state:[{t:this.shape_84}]},1).to({state:[{t:this.shape_85}]},1).to({state:[{t:this.shape_86}]},1).to({state:[{t:this.shape_87}]},1).to({state:[{t:this.shape_88}]},1).to({state:[{t:this.shape_89}]},1).to({state:[{t:this.shape_90}]},1).to({state:[{t:this.shape_91}]},1).to({state:[{t:this.shape_92}]},1).to({state:[{t:this.shape_93}]},1).to({state:[{t:this.shape_94}]},1).to({state:[{t:this.shape_94}]},46).to({state:[{t:this.shape_95}]},1).to({state:[{t:this.shape_96}]},1).to({state:[{t:this.shape_97}]},1).to({state:[{t:this.shape_98}]},1).to({state:[{t:this.shape_99}]},1).to({state:[{t:this.shape_100}]},1).to({state:[{t:this.shape_101}]},1).to({state:[{t:this.shape_102}]},1).to({state:[{t:this.shape_103}]},1).to({state:[{t:this.shape_104}]},1).to({state:[{t:this.shape_105}]},1).to({state:[{t:this.shape_106}]},1).to({state:[{t:this.shape_107}]},1).to({state:[{t:this.shape_80}]},1).wait(15));

	// Layer 9
	this.instance_1 = new lib.Symbol2_1();
	this.instance_1.parent = this;
	this.instance_1.setTransform(737.5,333.9,1,0.287,50.5,0,0,10.9,18.7);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(25).to({_off:false},0).to({regX:10.8,scaleY:1,guide:{path:[737.5,333.9,737.5,333.9,737.5,333.9]}},6).wait(1).to({regX:8,regY:10.5,rotation:49.8,x:742.1,y:326.8},0).wait(1).to({rotation:48.4,x:742.6,y:327.2},0).wait(1).to({rotation:46.1,x:743.6,y:328},0).wait(1).to({rotation:42.7,x:745,y:328.9},0).wait(1).to({rotation:37.7,x:747.3,y:330},0).wait(1).to({rotation:31.2,x:750.5,y:331},0).wait(1).to({rotation:23.7,x:754.4,y:331.4},0).wait(1).to({rotation:16.6,x:757.8,y:331.2},0).wait(1).to({rotation:10.7,x:760.5,y:330.8},0).wait(1).to({rotation:6.5,x:762.4,y:330.2},0).wait(1).to({rotation:3.5,x:763.8,y:329.7},0).wait(1).to({rotation:1.6,x:764.6,y:329.3},0).wait(1).to({rotation:0.5,x:765.1,y:329},0).wait(1).to({regX:10.9,regY:18.7,rotation:0,x:768.2,y:337.2},0).to({scaleY:0.18,guide:{path:[768.1,337.1,768.3,337,768.5,336.9,770,336.2,771.3,335.3]}},5).to({_off:true},1).wait(44));

	// Layer 3
	this.instance_2 = new lib.Symbol1();
	this.instance_2.parent = this;
	this.instance_2.setTransform(605.5,425,1,1,0,0,0,134.5,100);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(14).to({x:611,y:445},10,cjs.Ease.get(1)).wait(35).to({x:605.5,y:425},10,cjs.Ease.get(1)).wait(26));

	// Layer 2
	this.instance_3 = new lib.pack();
	this.instance_3.parent = this;
	this.instance_3.setTransform(595,143);

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(95));

	// Layer 7
	this.shape_108 = new cjs.Shape();
	this.shape_108.graphics.f().s("#000000").ss(11.5,1,1).p("ADdhNQinBwkRAr");
	this.shape_108.setTransform(704.1,355.9);

	this.shape_109 = new cjs.Shape();
	this.shape_109.graphics.f().s("#000000").ss(11.5,1,1).p("AjRBgQEDhICgh3");
	this.shape_109.setTransform(705.4,358);

	this.shape_110 = new cjs.Shape();
	this.shape_110.graphics.f().s("#000000").ss(11.5,1,1).p("AjHBwQD1hiCah9");
	this.shape_110.setTransform(706.7,359.8);

	this.shape_111 = new cjs.Shape();
	this.shape_111.graphics.f().s("#000000").ss(11.5,1,1).p("Ai/B+QDqh4CViD");
	this.shape_111.setTransform(707.8,361.4);

	this.shape_112 = new cjs.Shape();
	this.shape_112.graphics.f().s("#000000").ss(11.5,1,1).p("Ai4CKQDgiKCRiJ");
	this.shape_112.setTransform(708.7,362.8);

	this.shape_113 = new cjs.Shape();
	this.shape_113.graphics.f().s("#000000").ss(11.5,1,1).p("AixCVQDXicCNiN");
	this.shape_113.setTransform(709.6,364);

	this.shape_114 = new cjs.Shape();
	this.shape_114.graphics.f().s("#000000").ss(11.5,1,1).p("AitCdQDQipCKiQ");
	this.shape_114.setTransform(710.2,365);

	this.shape_115 = new cjs.Shape();
	this.shape_115.graphics.f().s("#000000").ss(11.5,1,1).p("AipCkQDLizCHiU");
	this.shape_115.setTransform(710.7,365.7);

	this.shape_116 = new cjs.Shape();
	this.shape_116.graphics.f().s("#000000").ss(11.5,1,1).p("AimCpQDHi7CGiW");
	this.shape_116.setTransform(711.1,366.3);

	this.shape_117 = new cjs.Shape();
	this.shape_117.graphics.f().s("#000000").ss(11.5,1,1).p("AikCrQDFi/CEiW");
	this.shape_117.setTransform(711.3,366.6);

	this.shape_118 = new cjs.Shape();
	this.shape_118.graphics.f().s("#000000").ss(11.5,1,1).p("AClirQiECXjEDA");
	this.shape_118.setTransform(711.4,366.7);

	this.shape_119 = new cjs.Shape();
	this.shape_119.graphics.f().s("#000000").ss(11.5,1,1).p("AiuCaQDSikCLiP");
	this.shape_119.setTransform(710,364.6);

	this.shape_120 = new cjs.Shape();
	this.shape_120.graphics.f().s("#000000").ss(11.5,1,1).p("AjAB8QDrh1CWiC");
	this.shape_120.setTransform(707.6,361.2);

	this.shape_121 = new cjs.Shape();
	this.shape_121.graphics.f().s("#000000").ss(11.5,1,1).p("AjNBlQD9hRCeh4");
	this.shape_121.setTransform(705.9,358.6);

	this.shape_122 = new cjs.Shape();
	this.shape_122.graphics.f().s("#000000").ss(11.5,1,1).p("AjTBdQEFhDChh2");
	this.shape_122.setTransform(705.2,357.6);

	this.shape_123 = new cjs.Shape();
	this.shape_123.graphics.f().s("#000000").ss(11.5,1,1).p("AjXBWQEKg5Ckhy");
	this.shape_123.setTransform(704.7,356.9);

	this.shape_124 = new cjs.Shape();
	this.shape_124.graphics.f().s("#000000").ss(11.5,1,1).p("AjZBRQEOgxClhw");
	this.shape_124.setTransform(704.3,356.3);

	this.shape_125 = new cjs.Shape();
	this.shape_125.graphics.f().s("#000000").ss(11.5,1,1).p("AjbBPQEQgtCnhw");
	this.shape_125.setTransform(704.1,356);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_108}]}).to({state:[{t:this.shape_108}]},14).to({state:[{t:this.shape_109}]},1).to({state:[{t:this.shape_110}]},1).to({state:[{t:this.shape_111}]},1).to({state:[{t:this.shape_112}]},1).to({state:[{t:this.shape_113}]},1).to({state:[{t:this.shape_114}]},1).to({state:[{t:this.shape_115}]},1).to({state:[{t:this.shape_116}]},1).to({state:[{t:this.shape_117}]},1).to({state:[{t:this.shape_118}]},1).to({state:[{t:this.shape_118}]},35).to({state:[{t:this.shape_119}]},1).to({state:[{t:this.shape_112}]},1).to({state:[{t:this.shape_120}]},1).to({state:[{t:this.shape_110}]},1).to({state:[{t:this.shape_121}]},1).to({state:[{t:this.shape_122}]},1).to({state:[{t:this.shape_123}]},1).to({state:[{t:this.shape_124}]},1).to({state:[{t:this.shape_125}]},1).to({state:[{t:this.shape_108}]},1).wait(26));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(540,340,1080,680);
// library properties:
lib.properties = {
	id: '58A3E3E35346A0459E13ED56260D2A1A',
	width: 1080,
	height: 680,
	fps: 30,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: [
		{src:"assets/images/miska_1.png", id:"miska"},
		{src:"assets/images/pack_1.png", id:"pack"},
		{src:"assets/images/yazik.png", id:"yazik"}
	],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['58A3E3E35346A0459E13ED56260D2A1A'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}



})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;

$(function () {
    var canvas, stage, exportRoot, anim_container, dom_overlay_container, fnStartAnimation;
    function init() {
        canvas = document.getElementById("canvas6");
        anim_container = document.getElementById("animation_container6");
        dom_overlay_container = document.getElementById("dom_overlay_container6");
        var comp=AdobeAn.getComposition("58A3E3E35346A0459E13ED56260D2A1A");
        var lib=comp.getLibrary();
        createjs.MotionGuidePlugin.install();
        var loader = new createjs.LoadQueue(false);
        loader.addEventListener("fileload", function(evt){handleFileLoad(evt,comp)});
        loader.addEventListener("complete", function(evt){handleComplete(evt,comp)});
        var lib=comp.getLibrary();
        loader.loadManifest(lib.properties.manifest);
    }
    function handleFileLoad(evt, comp) {
        var images=comp.getImages();
        if (evt && (evt.item.type == "image")) { images[evt.item.id] = evt.result; }
    }
    function handleComplete(evt,comp) {
        //This function is always called, irrespective of the content. You can use the variable "stage" after it is created in token create_stage.
        var lib=comp.getLibrary();
        var ss=comp.getSpriteSheet();
        var queue = evt.target;
        var ssMetadata = lib.ssMetadata;
        for(i=0; i<ssMetadata.length; i++) {
            ss[ssMetadata[i].name] = new createjs.SpriteSheet( {"images": [queue.getResult(ssMetadata[i].name)], "frames": ssMetadata[i].frames} )
        }
        exportRoot = new lib.Dacha();
        stage = new lib.Stage(canvas);
        //Registers the "tick" event listener.
        fnStartAnimation = function() {
            stage.addChild(exportRoot);
            createjs.Ticker.setFPS(lib.properties.fps);
            createjs.Ticker.addEventListener("tick", stage);
        }
        //Code to support hidpi screens and responsive scaling.
        function makeResponsive(isResp, respDim, isScale, scaleType) {
            var lastW, lastH, lastS=1;
            window.addEventListener('resize', resizeCanvas);
            resizeCanvas();
            function resizeCanvas() {
                var w = lib.properties.width, h = lib.properties.height;
                var iw = window.innerWidth, ih=window.innerHeight;
                var pRatio = window.devicePixelRatio || 1, xRatio=iw/w, yRatio=ih/h, sRatio=1;
                if(isResp) {
                    if((respDim=='width'&&lastW==iw) || (respDim=='height'&&lastH==ih)) {
                        sRatio = lastS;
                    }
                    else if(!isScale) {
                        if(iw<w || ih<h)
                            sRatio = Math.min(xRatio, yRatio);
                    }
                    else if(scaleType==1) {
                        sRatio = Math.min(xRatio, yRatio);
                    }
                    else if(scaleType==2) {
                        sRatio = Math.max(xRatio, yRatio);
                    }
                }
                canvas.width = w*pRatio*sRatio;
                canvas.height = h*pRatio*sRatio;
                canvas.style.width = dom_overlay_container.style.width = anim_container.style.width =  w*sRatio+'px';
                canvas.style.height = anim_container.style.height = dom_overlay_container.style.height = h*sRatio+'px';
                stage.scaleX = pRatio*sRatio;
                stage.scaleY = pRatio*sRatio;
                lastW = iw; lastH = ih; lastS = sRatio;
                stage.tickOnUpdate = false;
                stage.update();
                stage.tickOnUpdate = true;
            }
        }
        makeResponsive(true,'both',true,1);
        AdobeAn.compositionLoaded(lib.properties.id);
        fnStartAnimation();
    }

	init();
});