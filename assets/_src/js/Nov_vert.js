(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [];


// symbols:



(lib.bg_part1_vert = function() {
	this.initialize(img.bg_part1_vert);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,375,718);


(lib.bg_part2_vert = function() {
	this.initialize(img.bg_part2_vert);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,375,418);


(lib.pack = function() {
	this.initialize(img.pack);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,377,469);


(lib.shadow = function() {
	this.initialize(img.shadow);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,391,223);// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.Symbol8 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FD8687").s().p("AgqAqQgRgRAAgZQAAgYARgSQASgRAYAAQAZAAARARQASASAAAYQAAAZgSARQgRASgZAAQgYAAgSgSg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol8, new cjs.Rectangle(-6,-6,12,12), null);


(lib.Symbol_7_Layer_2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(10,1,1).p("ABhAeQg3gbgtgTQhcgngBBR");
	this.shape.setTransform(-6.45,-5.9644);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(10,1,1).p("AhmAOQANhFBbAoQAuATA3Aa");
	this.shape_1.setTransform(-7.075,-6.0374);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(10,1,1).p("AhsAEQAZg6BbAoQAuATA3Aa");
	this.shape_2.setTransform(-7.65,-6.117);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(10,1,1).p("AhxgDQAjgyBbAoQAuATA3Aa");
	this.shape_3.setTransform(-8.15,-6.2015);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(10,1,1).p("Ah1gLQArgpBbAnQAuATA3Ab");
	this.shape_4.setTransform(-8.6,-6.2941);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(10,1,1).p("Ah5gRQAzgiBbAnQAuATA3Ab");
	this.shape_5.setTransform(-8.95,-6.3864);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(10,1,1).p("Ah8gWQA5gcBbAnQAuATA3Ab");
	this.shape_6.setTransform(-9.275,-6.4807);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(10,1,1).p("Ah+gaQA9gXBbAnQAuATA3Ab");
	this.shape_7.setTransform(-9.5,-6.5633);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(10,1,1).p("AiAgdQBBgUBbAoQAuATA3Aa");
	this.shape_8.setTransform(-9.675,-6.6361);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(10,1,1).p("AiBgeQBDgSBbAnQAuATA3Ab");
	this.shape_9.setTransform(-9.775,-6.6817);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(10,1,1).p("ACCAlQg3gbgugTQhbgnhDAR");
	this.shape_10.setTransform(-9.8,-6.6936);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(10,1,1).p("Ah7gVQA3geBbAoQAuATA3Aa");
	this.shape_11.setTransform(-9.175,-6.4475);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(10,1,1).p("AhwgDQAhgyBbAnQAuATA3Ab");
	this.shape_12.setTransform(-8.1,-6.1918);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(10,1,1).p("AhoALQARhBBbAnQAuATA3Ab");
	this.shape_13.setTransform(-7.275,-6.061);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(10,1,1).p("AhlAQQALhHBbAoQAuATA3Aa");
	this.shape_14.setTransform(-6.975,-6.0247);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(10,1,1).p("AhjAUQAHhLBbAnQAuATA3Ab");
	this.shape_15.setTransform(-6.75,-5.9975);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(10,1,1).p("AhhAYQADhPBcAnQAtATA3Ab");
	this.shape_16.setTransform(-6.575,-5.9778);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(10,1,1).p("AhgAaQABhRBcAnQAtATA3Ab");
	this.shape_17.setTransform(-6.475,-5.9671);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},29).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_10}]},35).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape}]},1).wait(11));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_7_Layer_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(10,1,1).p("AhCBYQgoiHA8ghQAagOAiAQQAkARAhAq");
	this.shape.setTransform(0.013,-0.0017);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(10,1,1).p("AhVBKQgPhyA6gcQAdgNAjAQQAkARAfAk");
	this.shape_1.setTransform(-0.5884,-0.7914);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(10,1,1).p("AhiA/QAHhfA4gYQAhgMAjAQQAkAQAeAg");
	this.shape_2.setTransform(-1.55,-1.5071);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(10,1,1).p("AhsA0QAbhOA2gVQAkgLAjAQQAkAQAdAb");
	this.shape_3.setTransform(-2.55,-2.1362);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(10,1,1).p("Ah0AsQAshBA0gRQAngLAkAQQAkAQAaAX");
	this.shape_4.setTransform(-3.425,-2.67);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(10,1,1).p("Ah8AkQA8g0AzgPQApgKAjAQQAkAQAaAU");
	this.shape_5.setTransform(-4.15,-3.1387);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(10,1,1).p("AiCAeQBIgqAxgNQArgKAkARQAkAQAZAR");
	this.shape_6.setTransform(-4.75,-3.5236);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(10,1,1).p("AiGAZQBQgjAwgLQAtgJAkAQQAjAPAZAQ");
	this.shape_7.setTransform(-5.2,-3.7986);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(10,1,1).p("AiJAWQBWgdAwgKQAugJAkAQQAjAPAYAP");
	this.shape_8.setTransform(-5.525,-4.0088);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(10,1,1).p("AiLATQBagaAwgJQAugJAkAQQAkAPAXAO");
	this.shape_9.setTransform(-5.725,-4.0736);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(10,1,1).p("AiMASQBcgZAvgJQAvgJAkAQQAkAQAXAN");
	this.shape_10.setTransform(-5.8,-4.0604);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(10,1,1).p("AiAAgQBDguAygNQArgKAjAQQAkAQAaAS");
	this.shape_11.setTransform(-4.55,-3.3887);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(10,1,1).p("AhqA2QAYhRA2gVQAkgMAjAQQAkARAcAb");
	this.shape_12.setTransform(-2.425,-2.0405);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(10,1,1).p("AhaBGQgHhrA5gbQAfgNAiARQAlAQAeAj");
	this.shape_13.setTransform(-0.8683,-1.0364);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(10,1,1).p("AhSBNQgTh2A6gcQAdgOAiAQQAlARAfAl");
	this.shape_14.setTransform(-0.4687,-0.6589);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(10,1,1).p("AhLBRQgdh9A8geQAbgOAiAQQAlARAfAn");
	this.shape_15.setTransform(-0.2365,-0.3839);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(10,1,1).p("AhGBVQgjiDA8gfQAagOAiAQQAlAQAgAp");
	this.shape_16.setTransform(-0.0947,-0.1767);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(10,1,1).p("AhDBXQgniGA9ghQAZgNAiAQQAlAQAgAq");
	this.shape_17.setTransform(-0.0143,-0.0339);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},29).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_10}]},35).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape}]},1).wait(11));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_5_Layer_7 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_7
	this.instance = new lib.shadow();
	this.instance.parent = this;
	this.instance.setTransform(-537,121,0.9117,0.9117);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(95));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_5_Layer_3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_3
	this.instance = new lib.bg_part2_vert();
	this.instance.parent = this;
	this.instance.setTransform(-539.5,54.2);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(95));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_5_Layer_2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.instance = new lib.bg_part1_vert();
	this.instance.parent = this;
	this.instance.setTransform(-539.5,-339.8);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(95));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_3_Layer_3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_3
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(9.5,1,1).p("AhNgZQCYg8ADCC");
	this.shape.setTransform(10.3,-6.9299);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(9.5,1,1).p("AhNgWQCLg2AQB0");
	this.shape_1.setTransform(10.325,-7.2144);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(9.5,1,1).p("AhOgUQCAgwAdBo");
	this.shape_2.setTransform(10.375,-7.4529);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(9.5,1,1).p("AhOgSQB2grAnBd");
	this.shape_3.setTransform(10.4,-7.6474);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(9.5,1,1).p("AhOgQQBtgnAwBU");
	this.shape_4.setTransform(10.4,-7.8358);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(9.5,1,1).p("AhOgOQBlgkA4BN");
	this.shape_5.setTransform(10.425,-8.0076);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(9.5,1,1).p("AhPgNQBgghA/BG");
	this.shape_6.setTransform(10.45,-8.1353);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(9.5,1,1).p("AhPgMQBbgfBEBC");
	this.shape_7.setTransform(10.45,-8.2295);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(9.5,1,1).p("AhPgLQBXgdBIA+");
	this.shape_8.setTransform(10.475,-8.3071);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(9.5,1,1).p("AhPgLQBVgcBKA8");
	this.shape_9.setTransform(10.475,-8.3404);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(9.5,1,1).p("AhPgLQBVgbBKA7");
	this.shape_10.setTransform(10.475,-8.3535);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(9.5,1,1).p("AhPgOQBighA9BI");
	this.shape_11.setTransform(10.45,-8.0744);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(9.5,1,1).p("AhOgSQB3gsAmBf");
	this.shape_12.setTransform(10.375,-7.6414);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(9.5,1,1).p("AhOgWQCIgzAVBw");
	this.shape_13.setTransform(10.35,-7.2812);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(9.5,1,1).p("AhNgXQCNg2AOB2");
	this.shape_14.setTransform(10.325,-7.1535);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(9.5,1,1).p("AhNgYQCSg4AJB7");
	this.shape_15.setTransform(10.325,-7.0593);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(9.5,1,1).p("AhNgZQCWg6AFB/");
	this.shape_16.setTransform(10.3,-6.9817);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(9.5,1,1).p("AhNgZQCYg7ADCA");
	this.shape_17.setTransform(10.3,-6.9483);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},33).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_10}]},30).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape}]},1).wait(12));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_3_Layer_2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(9.5,1,1).p("AgqhnQBaCDgFBM");
	this.shape.setTransform(6.8151,0.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(9.5,1,1).p("AgnhnQBECHALBI");
	this.shape_1.setTransform(6.525,0.85);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(9.5,1,1).p("AglhoQAxCMAaBF");
	this.shape_2.setTransform(6.25,0.9);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(9.5,1,1).p("AgihoQAgCQAlBB");
	this.shape_3.setTransform(6.025,0.95);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(9.5,1,1).p("AghhoQASCTAxA+");
	this.shape_4.setTransform(5.85,0.975);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(9.5,1,1).p("AgfhpQAECXA7A8");
	this.shape_5.setTransform(5.675,1);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(9.5,1,1).p("AgdhpQgHCZBDA6");
	this.shape_6.setTransform(5.5215,1.025);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(9.5,1,1).p("AgbhpQgPCbBIA4");
	this.shape_7.setTransform(5.3049,1.05);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(9.5,1,1).p("AgZhpQgVCcBNA3");
	this.shape_8.setTransform(5.1356,1.075);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(9.5,1,1).p("AgYhpQgYCcBPA3");
	this.shape_9.setTransform(5.0494,1.075);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(9.5,1,1).p("AgYhpQgZCdBQA2");
	this.shape_10.setTransform(5.0028,1.075);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(9.5,1,1).p("AgehpQgDCYBAA7");
	this.shape_11.setTransform(5.5683,1.025);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(9.5,1,1).p("AgjhoQAjCPAkBC");
	this.shape_12.setTransform(6.075,0.925);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(9.5,1,1).p("AgmhnQA9CJAQBG");
	this.shape_13.setTransform(6.425,0.875);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(9.5,1,1).p("AgohnQBJCHAIBI");
	this.shape_14.setTransform(6.55,0.85);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(9.5,1,1).p("AgphnQBQCFADBK");
	this.shape_15.setTransform(6.675,0.825);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(9.5,1,1).p("AgqhnQBWCEgCBL");
	this.shape_16.setTransform(6.7522,0.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape,p:{x:6.8151}}]}).to({state:[{t:this.shape,p:{x:6.8151}}]},33).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_10}]},30).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape,p:{x:6.7881}}]},1).to({state:[{t:this.shape,p:{x:6.8151}}]},1).wait(12));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_3_Layer_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(9.5,1,1).p("AgIhbQBOCAhfA3");
	this.shape.setTransform(-0.0045,0);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(9.5,1,1).p("AgUBeQBGg6gxiB");
	this.shape_1.setTransform(-1.0083,0.15);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(9.5,1,1).p("AgPBfQAvg8gXiB");
	this.shape_2.setTransform(-1.866,0.3);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(9.5,1,1).p("AgOBgQAbg9ACiC");
	this.shape_3.setTransform(-2.4,0.425);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(9.5,1,1).p("AgPBhQALg/AUiC");
	this.shape_4.setTransform(-2.55,0.525);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(9.5,1,1).p("AgQBiQgFhBAmiC");
	this.shape_5.setTransform(-2.6983,0.625);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(9.5,1,1).p("AgPBjQgRhCA0iD");
	this.shape_6.setTransform(-2.9709,0.7);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(9.5,1,1).p("AgPBkQgahDBAiE");
	this.shape_7.setTransform(-3.2485,0.75);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(9.5,1,1).p("AgOBkQgghEBHiD");
	this.shape_8.setTransform(-3.4304,0.8);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(9.5,1,1).p("AgNBkQglhEBMiD");
	this.shape_9.setTransform(-3.5619,0.825);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(9.5,1,1).p("AAahjQhNCDAmBE");
	this.shape_10.setTransform(-3.5931,0.825);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(9.5,1,1).p("AgQBjQgMhCAviD");
	this.shape_11.setTransform(-2.8781,0.675);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(9.5,1,1).p("AgNBgQAdg9gCiC");
	this.shape_12.setTransform(-2.3688,0.4);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(9.5,1,1).p("AgSBeQA+g6goiB");
	this.shape_13.setTransform(-1.3233,0.2);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(9.5,1,1).p("AgVBdQBKg5g1iA");
	this.shape_14.setTransform(-0.8698,0.125);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(9.5,1,1).p("AgXBdQBUg4hBiB");
	this.shape_15.setTransform(-0.4699,0.075);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(9.5,1,1).p("AgYBcQBag3hIiA");
	this.shape_16.setTransform(-0.2253,0.025);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(9.5,1,1).p("AgZBcQBeg3hNiA");
	this.shape_17.setTransform(-0.0416,0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},33).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_10}]},30).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape}]},1).wait(12));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_2_Layer_6 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_6
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(9.5,1,1).p("AGSjEQw+hhF9H6");
	this.shape.setTransform(-7.8945,-8.8565);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(9.5,1,1).p("AlgDEQkYn5QUCU");
	this.shape_1.setTransform(-9.1824,-12.6159);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(9.5,1,1).p("AmJC3Qi+n4PvDA");
	this.shape_2.setTransform(-10.631,-16.1714);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(9.5,1,1).p("AmqCsQhun2PPDn");
	this.shape_3.setTransform(-12.2571,-19.4457);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(9.5,1,1).p("AnDCkQgpn1OyEK");
	this.shape_4.setTransform(-14.01,-22.3785);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(9.5,1,1).p("AnWCeQASn1ObEn");
	this.shape_5.setTransform(-15.8,-24.9095);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(9.5,1,1).p("AnkCZQBBn0OIE+");
	this.shape_6.setTransform(-17.35,-26.9855);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(9.5,1,1).p("AnvCWQBmn1N5FS");
	this.shape_7.setTransform(-18.55,-28.6382);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(9.5,1,1).p("An2CTQCBn0NsFf");
	this.shape_8.setTransform(-19.425,-29.8388);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(9.5,1,1).p("An7CSQCRn0NmFl");
	this.shape_9.setTransform(-19.925,-30.5394);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(9.5,1,1).p("AH+AGQtkloiXHz");
	this.shape_10.setTransform(-20.1,-30.7893);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(9.5,1,1).p("AnuCEQDPnuMOGw");
	this.shape_11.setTransform(-18.625,-37.1589);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(9.5,1,1).p("AniB7QD7nqLKHn");
	this.shape_12.setTransform(-17.45,-42.2126);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(9.5,1,1).p("AnaBhQEbnmKaIO");
	this.shape_13.setTransform(-16.625,-43.9072);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(9.5,1,1).p("AnVBRQEunlJ9In");
	this.shape_14.setTransform(-16.15,-44.8579);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(9.5,1,1).p("AHUCWQpzovk0Hl");
	this.shape_15.setTransform(-15.975,-45.1815);
	this.shape_15._off = true;

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(9.5,1,1).p("AnKBuQDxoMKkIu");
	this.shape_16.setTransform(-17.025,-48.1442);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(9.5,1,1).p("AnDCHQDBooLGIt");
	this.shape_17.setTransform(-17.8,-50.2628);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#000000").ss(9.5,1,1).p("Am/CQQCko5LbIt");
	this.shape_18.setTransform(-18.25,-50.958);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#000000").ss(9.5,1,1).p("Am+CSQCbo+LiIs");
	this.shape_19.setTransform(-18.4,-51.1151);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#000000").ss(9.5,1,1).p("AnfCbQAxn1OOE3");
	this.shape_20.setTransform(-16.825,-26.2866);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#000000").ss(9.5,1,1).p("AmmCuQh5n3PTDj");
	this.shape_21.setTransform(-12.0149,-19.0139);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#000000").ss(9.5,1,1).p("AlvC/Qj4n4QHCj");
	this.shape_22.setTransform(-9.6266,-13.8387);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#000000").ss(9.5,1,1).p("AlZDGQkon4QbCL");
	this.shape_23.setTransform(-8.9499,-12.0085);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#000000").ss(9.5,1,1).p("AlGDMQlNn4QqB4");
	this.shape_24.setTransform(-8.4603,-10.6098);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f().s("#000000").ss(9.5,1,1).p("Ak5DRQlon5Q1Br");
	this.shape_25.setTransform(-8.1297,-9.6168);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f().s("#000000").ss(9.5,1,1).p("AkxDTQl4n5Q8Bk");
	this.shape_26.setTransform(-7.9521,-9.0494);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},34).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_15}]},2).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_15}]},6).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape}]},1).wait(6));
	this.timeline.addTween(cjs.Tween.get(this.shape_15).wait(49).to({_off:false},0).wait(2).to({_off:true},1).wait(7).to({_off:false},0).wait(1).to({_off:true},1).wait(7).to({_off:false},0).wait(6).to({_off:true},1).wait(20));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_2_Layer_4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_4
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(9.5,1,1).p("AFincQBGEugrDcQgnDHh/BxQh0BoimAOQiiANiohK");
	this.shape.setTransform(125.4586,-3.7023);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(10,1,1).p("AFincQBGEugrDcQgnDHh/BxQh0BoimAOQiiANiohK");
	this.shape_1.setTransform(125.4586,-3.7023);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(10,1,1).p("AmvGdQCiBHCngOQCsgOB/hlQCIhrA9jKQBBjagvkr");
	this.shape_2.setTransform(119.7528,-4.184);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(10,1,1).p("AnSGcQCdBECtgPQCwgPCJhhQCQhnBSjLQBVjZgdko");
	this.shape_3.setTransform(115.0078,-4.5645);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(10,1,1).p("AnzGaQCYBCCygQQCzgQCTheQCXhiBjjNQBmjXgKkm");
	this.shape_4.setTransform(111.124,-4.9276);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(10,1,1).p("AoSGZQCUBAC2gQQC4gRCYhbQCehfBzjOQB1jVAFkm");
	this.shape_5.setTransform(107.975,-5.265);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(10,1,1).p("AosGYQCRA+C5gRQC6gRCfhZQCjhcCAjPQCCjUARkk");
	this.shape_6.setTransform(105.325,-5.5182);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(10,1,1).p("ApCGXQCOA9C8gSQC9gRCkhYQCohZCKjQQCNjTAbki");
	this.shape_7.setTransform(103.15,-5.7299);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(10,1,1).p("ApTGWQCMA8C+gRQC/gSCohWQCrhXCTjRQCUjTAkkh");
	this.shape_8.setTransform(101.5,-5.9024);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(10,1,1).p("ApfGWQCLA7C/gSQDAgSCshVQCshWCZjRQCajSAqkh");
	this.shape_9.setTransform(100.3,-6.0146);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(10,1,1).p("ApmGVQCJA7DBgSQDAgSCuhVQCuhVCdjSQCdjRAtkg");
	this.shape_10.setTransform(99.575,-6.0882);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(10,1,1).p("AJqnBQgvEgieDRQieDTivBUQiuBVjAASQjBASiKg7");
	this.shape_11.setTransform(99.325,-6.1146);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(10,1,1).p("ApZGOQCKA6DBgLQDBgMCrhSQCrhTCTjRQCTjRArko");
	this.shape_12.setTransform(97.725,-5.3805);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(10,1,1).p("ApMGIQCKA7DCgHQDBgHCohRQCphQCKjRQCJjQAokv");
	this.shape_13.setTransform(96.45,-4.8376);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(10,1,1).p("ApDGEQCJA6DDgDQDBgECnhPQCnhPCDjRQCDjQAmky");
	this.shape_14.setTransform(95.575,-4.3901);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(10,1,1).p("Ao+GBQCKA6DCgBQDCgCClhOQCmhPCAjQQB/jQAlk1");
	this.shape_15.setTransform(95.025,-4.0992);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(10,1,1).p("AI9m5QgkE2h+DPQh/DRilBOQilBOjCABQjDABiJg7");
	this.shape_16.setTransform(94.85,-4.0371);
	this.shape_16._off = true;

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(10,1,1).p("AoyGgQCQAzDOgTQDNgUCnhiQCnhhBojMQBojMAckV");
	this.shape_17.setTransform(92.8,-7.8553);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#000000").ss(10,1,1).p("AoqG1QCUAuDWgiQDVghCphvQCohwBZjIQBYjJAUj+");
	this.shape_18.setTransform(91.35,-10.3692);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#000000").ss(10,1,1).p("AomHBQCWAqDbgqQDbgpCph4QCph3BPjGQBPjIARjw");
	this.shape_19.setTransform(90.5,-11.8062);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#000000").ss(10,1,1).p("AokHEQCXAqDdgtQDbgsCph6QCqh6BMjGQBMjHAPjr");
	this.shape_20.setTransform(90.2,-12.2737);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#000000").ss(10,1,1).p("Ao8GAQCJA7DDgBQDCgBClhOQCmhOB+jRQB+jPAkk2");
	this.shape_21.setTransform(94.85,-4.0248);
	this.shape_21._off = true;

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#000000").ss(10,1,1).p("AoXGGQCPA+C9gDQC9gECchTQCdhVBujOQBujSARk1");
	this.shape_22.setTransform(100.25,-3.968);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#000000").ss(10,1,1).p("An1GLQCUBAC3gFQC3gFCUhXQCYhbBejOQBhjUgCkz");
	this.shape_23.setTransform(105.102,-3.9319);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#000000").ss(10,1,1).p("AnZGQQCZBCCygHQCzgHCNhbQCRhgBSjMQBUjWgSky");
	this.shape_24.setTransform(109.5088,-3.8914);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f().s("#000000").ss(10,1,1).p("AnDGUQCdBECugIQCwgJCGhfQCMhlBHjKQBJjYggkx");
	this.shape_25.setTransform(113.5248,-3.8278);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f().s("#000000").ss(10,1,1).p("AmxGXQCgBGCqgKQCtgKCBhiQCIhoA9jKQBAjZgskw");
	this.shape_26.setTransform(117.0551,-3.7875);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f().s("#000000").ss(10,1,1).p("AmjGaQCiBHCogLQCqgLB8hkQCFhrA1jJQA5jag1kw");
	this.shape_27.setTransform(120.0327,-3.7762);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f().s("#000000").ss(10,1,1).p("AmZGcQClBIClgMQCpgMB4hmQCChtAvjJQAzjbg9ku");
	this.shape_28.setTransform(122.3664,-3.7393);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f().s("#000000").ss(10,1,1).p("AmSGeQCmBJCkgNQCngNB2hnQCAhvArjIQAujchBku");
	this.shape_29.setTransform(124.0653,-3.7086);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f().s("#000000").ss(10,1,1).p("AmOGfQCoBJCigNQCngNB0hoQB/hwAojIQAsjchFku");
	this.shape_30.setTransform(125.0974,-3.7023);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape_1}]},29).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_11}]},5).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_16}]},2).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_16}]},6).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_27}]},1).to({state:[{t:this.shape_28}]},1).to({state:[{t:this.shape_29}]},1).to({state:[{t:this.shape_30}]},1).to({state:[{t:this.shape_1}]},1).wait(6));
	this.timeline.addTween(cjs.Tween.get(this.shape_16).wait(49).to({_off:false},0).wait(2).to({_off:true},1).wait(7).to({_off:false},0).wait(1).to({_off:true},1).wait(7).to({_off:false},0).wait(6).to({_off:true},1).wait(4).to({_off:false},0).to({_off:true},1).wait(15));
	this.timeline.addTween(cjs.Tween.get(this.shape_21).wait(75).to({_off:false},0).wait(3).to({_off:true},1).wait(16));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_1_Layer_6 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_6
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(10,1,1).p("AhAAzQBdAPAkh2");
	this.shape.setTransform(55.5,34.4032);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(10,1,1).p("AhEA2QBhANAoh5");
	this.shape_1.setTransform(55.475,34.267);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(10,1,1).p("AhJA4QBmANAsh9");
	this.shape_2.setTransform(55.45,34.136);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(10,1,1).p("AhNA6QBqAMAxiA");
	this.shape_3.setTransform(55.425,34.0011);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(10,1,1).p("AhSA8QBvAMA1iE");
	this.shape_4.setTransform(55.4,33.8459);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(10,1,1).p("AhWA+QB0ALA5iH");
	this.shape_5.setTransform(55.375,33.7378);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(10,1,1).p("AhbBAQB5AKA+iK");
	this.shape_6.setTransform(55.35,33.6054);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(10,1,1).p("AhfBCQB9AJBCiN");
	this.shape_7.setTransform(55.325,33.4517);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(10,1,1).p("AhkBFQCCAIBHiR");
	this.shape_8.setTransform(55.3,33.3208);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(10,1,1).p("AhoBHQCHAHBKiU");
	this.shape_9.setTransform(55.275,33.193);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(10,1,1).p("AhtBJQCMAGBPiX");
	this.shape_10.setTransform(55.25,33.063);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(10,1,1).p("AhkBFQCDAIBHiR");
	this.shape_11.setTransform(55.3,33.3208);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(10,1,1).p("AhdBBQB7AKBAiM");
	this.shape_12.setTransform(55.35,33.5301);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(10,1,1).p("AhQA7QBuAMAziC");
	this.shape_13.setTransform(55.4,33.9214);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(10,1,1).p("AhLA5QBoAMAvh+");
	this.shape_14.setTransform(55.45,34.0558);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(10,1,1).p("AhHA3QBkANArh7");
	this.shape_15.setTransform(55.475,34.1865);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(10,1,1).p("AhEA1QBhAOAoh5");
	this.shape_16.setTransform(55.475,34.2721);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(10,1,1).p("AhBA1QBeAOAlh4");
	this.shape_17.setTransform(55.475,34.3479);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#000000").ss(10,1,1).p("AhAA0QBdAOAkh2");
	this.shape_18.setTransform(55.5,34.3785);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_10}]},35).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape}]},1).wait(40));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_1_Layer_2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(10,1,1).p("AjGBlQAIhcA9g4QA2gzBMgCQBLgCA4AuQA+AyAGBM");
	this.shape.setTransform(67.6,-0.9792);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(10,1,1).p("AjGBqQAIhgA9g7QA2g2BMgCQBLgCA4AxQA+A0AGBQ");
	this.shape_1.setTransform(67.6,-1.554);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(10,1,1).p("AjGBvQAIhlA9g9QA2g4BMgDQBLgCA4AzQA+A2AGBU");
	this.shape_2.setTransform(67.6,-2.0788);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(10,1,1).p("AjGBzQAIhoA9hAQA2g7BMgCQBLgCA4A0QA+A5AGBX");
	this.shape_3.setTransform(67.6,-2.5307);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(10,1,1).p("AjGB3QAIhsA9hCQA2g8BMgCQBLgDA4A2QA+A7AGBa");
	this.shape_4.setTransform(67.6,-2.9055);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(10,1,1).p("AjGB6QAIhuA9hEQA2g+BMgCQBLgDA4A4QA+A8AGBc");
	this.shape_5.setTransform(67.6,-3.2554);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(10,1,1).p("AjGB8QAIhwA9hGQA2g/BMgCQBLgCA4A4QA+A9AGBf");
	this.shape_6.setTransform(67.6,-3.5053);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(10,1,1).p("AjGB+QAIhyA9hGQA2hABMgDQBLgCA4A5QA+A/AGBf");
	this.shape_7.setTransform(67.6,-3.7534);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(10,1,1).p("AjGB/QAIhzA9hIQA2hABMgCQBLgDA4A6QA+A/AGBh");
	this.shape_8.setTransform(67.6,-3.8802);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(10,1,1).p("AjGCAQAIh0A9hIQA2hBBMgCQBLgDA4A7QA+A/AGBh");
	this.shape_9.setTransform(67.6,-3.9801);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(10,1,1).p("AjGCBQAIh1A9hIQA2hBBMgCQBLgDA4A7QA+A/AGBi");
	this.shape_10.setTransform(67.6,-4.0051);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(10,1,1).p("AjGB7QAIhwA9hEQA2g/BMgCQBLgDA4A4QA+A9AGBe");
	this.shape_11.setTransform(67.6,-3.4303);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(10,1,1).p("AjGBzQAIhoA9hAQA2g6BMgCQBLgCA4A0QA+A4AGBX");
	this.shape_12.setTransform(67.6,-2.4537);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(10,1,1).p("AjGBsQAIhiA9g8QA2g3BMgCQBLgCA4AxQA+A1AGBS");
	this.shape_13.setTransform(67.6,-1.7289);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(10,1,1).p("AjGBpQAIhfA9g7QA2g1BMgCQBLgCA4AwQA+A0AGBP");
	this.shape_14.setTransform(67.6,-1.479);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(10,1,1).p("AjGBnQAIheA9g5QA2g0BMgCQBLgDA4AwQA+AyAGBP");
	this.shape_15.setTransform(67.6,-1.2312);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(10,1,1).p("AjGBmQAIhcA9g5QA2g0BMgCQBLgCA4AvQA+AyAGBN");
	this.shape_16.setTransform(67.6,-1.1041);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(10,1,1).p("AjGBlQAIhcA9g4QA2gzBMgCQBLgCA4AuQA+AyAGBN");
	this.shape_17.setTransform(67.6,-1.0042);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_10}]},35).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape}]},1).wait(40));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_1_Layer_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(10,1,1).p("AjHBlQAIhcA9g4QA3gzBMgCQBLgCA4AuQA+AyAGBM");
	this.shape.setTransform(0,0.0208);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(10,1,1).p("AjHBqQAIhgA9g7QA3g2BMgCQBLgCA4AxQA+A0AGBQ");
	this.shape_1.setTransform(0,-0.504);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(10,1,1).p("AjHBvQAIhlA9g9QA3g4BMgDQBLgCA4AzQA+A2AGBU");
	this.shape_2.setTransform(0,-0.9788);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(10,1,1).p("AjHBzQAIhpA9g/QA3g7BMgCQBLgCA4A0QA+A5AGBX");
	this.shape_3.setTransform(0,-1.3787);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(10,1,1).p("AjHB3QAIhsA9hCQA3g8BMgDQBLgCA4A2QA+A6AGBb");
	this.shape_4.setTransform(0,-1.7305);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(10,1,1).p("AjHB6QAIhvA9hEQA3g9BMgDQBLgCA4A4QA+A7AGBd");
	this.shape_5.setTransform(0,-2.0303);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(10,1,1).p("AjHB8QAIhxA9hFQA3g/BMgCQBLgDA4A5QA+A9AGBf");
	this.shape_6.setTransform(0,-2.2803);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(10,1,1).p("AjHB+QAIhzA9hGQA3g/BMgDQBLgDA4A6QA+A+AGBg");
	this.shape_7.setTransform(0,-2.4802);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(10,1,1).p("AjHCAQAIh0A9hHQA3hBBMgDQBLgCA4A6QA+A/AGBh");
	this.shape_8.setTransform(0,-2.6301);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(10,1,1).p("AjHCBQAIh1A9hHQA3hBBMgDQBLgDA4A7QA+A/AGBi");
	this.shape_9.setTransform(0,-2.7051);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(10,1,1).p("AjHCBQAIh1A9hIQA3hBBMgDQBLgCA4A7QA+A/AGBi");
	this.shape_10.setTransform(0,-2.7301);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(10,1,1).p("AjHB8QAIhwA9hFQA3g+BMgDQBLgDA4A5QA+A8AGBe");
	this.shape_11.setTransform(0,-2.2053);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(10,1,1).p("AjHByQAIhnA9hAQA3g6BMgCQBLgDA4A1QA+A4AGBX");
	this.shape_12.setTransform(0,-1.3307);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(10,1,1).p("AjHBsQAIhiA9g8QA3g2BMgDQBLgCA4AyQA+A0AGBS");
	this.shape_13.setTransform(0,-0.6789);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(10,1,1).p("AjHBpQAIhfA9g7QA3g1BMgCQBLgCA4AwQA+A0AGBP");
	this.shape_14.setTransform(0,-0.429);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(10,1,1).p("AjHBnQAIhdA9g6QA3g0BMgCQBLgCA4AvQA+AzAGBO");
	this.shape_15.setTransform(0,-0.2291);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(10,1,1).p("AjHBmQAIhdA9g4QA3g0BMgCQBLgCA4AvQA+AyAGBN");
	this.shape_16.setTransform(0,-0.0791);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(10,1,1).p("AjHBlQAIhbA9g5QA3gzBMgCQBLgCA4AuQA+AyAGBN");
	this.shape_17.setTransform(0,-0.0042);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_10}]},35).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape}]},1).wait(40));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol7 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_94 = function() {
		this.___loopingOver___ = true;
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(94).call(this.frame_94).wait(1));

	// Layer_2_obj_
	this.Layer_2 = new lib.Symbol_7_Layer_2();
	this.Layer_2.name = "Layer_2";
	this.Layer_2.parent = this;
	this.Layer_2.setTransform(-6.5,-6,1,1,0,0,0,-6.5,-6);
	this.Layer_2.depth = 0;
	this.Layer_2.isAttachedToCamera = 0
	this.Layer_2.isAttachedToMask = 0
	this.Layer_2.layerDepth = 0
	this.Layer_2.layerIndex = 0
	this.Layer_2.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_2).wait(95));

	// Layer_1_obj_
	this.Layer_1 = new lib.Symbol_7_Layer_1();
	this.Layer_1.name = "Layer_1";
	this.Layer_1.parent = this;
	this.Layer_1.depth = 0;
	this.Layer_1.isAttachedToCamera = 0
	this.Layer_1.isAttachedToMask = 0
	this.Layer_1.layerDepth = 0
	this.Layer_1.layerIndex = 1
	this.Layer_1.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_1).wait(95));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-27.8,-15.4,41.1,29.200000000000003);


(lib.Symbol3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_94 = function() {
		this.___loopingOver___ = true;
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(94).call(this.frame_94).wait(1));

	// Layer_3_obj_
	this.Layer_3 = new lib.Symbol_3_Layer_3();
	this.Layer_3.name = "Layer_3";
	this.Layer_3.parent = this;
	this.Layer_3.setTransform(10.3,-7,1,1,0,0,0,10.3,-7);
	this.Layer_3.depth = 0;
	this.Layer_3.isAttachedToCamera = 0
	this.Layer_3.isAttachedToMask = 0
	this.Layer_3.layerDepth = 0
	this.Layer_3.layerIndex = 0
	this.Layer_3.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_3).wait(95));

	// Layer_2_obj_
	this.Layer_2 = new lib.Symbol_3_Layer_2();
	this.Layer_2.name = "Layer_2";
	this.Layer_2.parent = this;
	this.Layer_2.setTransform(6.8,0.8,1,1,0,0,0,6.8,0.8);
	this.Layer_2.depth = 0;
	this.Layer_2.isAttachedToCamera = 0
	this.Layer_2.isAttachedToMask = 0
	this.Layer_2.layerDepth = 0
	this.Layer_2.layerIndex = 1
	this.Layer_2.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_2).wait(95));

	// Layer_1_obj_
	this.Layer_1 = new lib.Symbol_3_Layer_1();
	this.Layer_1.name = "Layer_1";
	this.Layer_1.parent = this;
	this.Layer_1.depth = 0;
	this.Layer_1.isAttachedToCamera = 0
	this.Layer_1.isAttachedToMask = 0
	this.Layer_1.layerDepth = 0
	this.Layer_1.layerIndex = 2
	this.Layer_1.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_1).wait(95));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-11,-16.1,34.2,32.6);


(lib.Symbol_2_Layer_7 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_7
	this.instance = new lib.Symbol3();
	this.instance.parent = this;
	this.instance.setTransform(-40.8,9.85,1,1,0,0,0,-1.2,-12.3);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(34).to({rotation:50.4689,x:-70.3,y:-18.6},10,cjs.Ease.get(1)).to({x:-62.8,y:-37.6},5,cjs.Ease.get(1)).wait(2).to({regX:-1.4,scaleX:0.9983,scaleY:0.9983,rotation:51.4079,x:-62.4,y:-37.35},4,cjs.Ease.get(1)).to({regX:-1.2,scaleX:1,scaleY:1,rotation:50.4689,x:-62.8,y:-37.6},4,cjs.Ease.get(-1)).wait(1).to({regX:-1.4,scaleX:0.9983,scaleY:0.9983,rotation:51.4079,x:-62.4,y:-37.35},4,cjs.Ease.get(1)).to({regX:-1.2,scaleX:1,scaleY:1,rotation:50.4689,x:-62.8,y:-37.6},4,cjs.Ease.get(-1)).wait(6).to({x:-70.3,y:-18.6},5,cjs.Ease.get(-1)).to({rotation:0,x:-40.8,y:9.85},10,cjs.Ease.get(1)).wait(6));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_2_Layer_5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_5
	this.instance = new lib.Symbol7();
	this.instance.parent = this;
	this.instance.setTransform(90.65,40.85,1,1,0,0,0,8.3,-0.1);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(29).to({rotation:4.7133,x:41,y:37.3},10,cjs.Ease.get(1)).wait(12).to({regX:8.4,scaleX:0.9998,scaleY:0.9998,rotation:3.7682,x:40.5,y:36.5},4,cjs.Ease.get(1)).to({regX:8.3,scaleX:1,scaleY:1,rotation:4.7133,x:41,y:37.3},4,cjs.Ease.get(-1)).wait(1).to({regX:8.4,scaleX:0.9998,scaleY:0.9998,rotation:3.7682,x:40.5,y:36.5},4,cjs.Ease.get(1)).to({regX:8.3,scaleX:1,scaleY:1,rotation:4.7133,x:41,y:37.3},4,cjs.Ease.get(-1)).wait(11).to({rotation:0,x:90.65,y:40.85},10,cjs.Ease.get(1)).wait(6));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_1_Layer_4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_4
	this.instance = new lib.Symbol8();
	this.instance.parent = this;
	this.instance.setTransform(63.55,30.25,0.2983,0.2983,0,0,0,-5.9,-3.4);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(5).to({_off:false},0).to({regX:-6.1,regY:-3.5,scaleX:0.8926,scaleY:0.8926,guide:{path:[63.5,30.3,63.8,29.8,64.1,29.4]}},5,cjs.Ease.get(-1)).to({regX:-6,scaleX:0.8907,scaleY:0.8907,rotation:27.5471,guide:{path:[64.1,29.4,61,34.2,57,36.9]}},6,cjs.Ease.get(-1)).to({regX:-6.2,scaleX:0.8925,scaleY:0.8925,rotation:50.998,guide:{path:[57.1,36.9,53.6,39.2,49.6,39.9]}},6,cjs.Ease.get(1)).to({regX:-6,scaleX:0.8907,scaleY:0.8907,rotation:27.5471,guide:{path:[49.6,39.9,53.6,39.2,57.1,36.9]}},6,cjs.Ease.get(-1)).to({regX:-6.1,scaleX:0.8926,scaleY:0.8926,rotation:0,guide:{path:[57,36.9,61,34.2,64.1,29.4]}},6,cjs.Ease.get(1)).wait(1).to({regX:0,regY:0,scaleX:0.8821,scaleY:0.8821,x:69.5293,y:32.4096},0).wait(1).to({scaleX:0.8477,scaleY:0.8477,x:69.3192,y:32.2916},0).wait(1).to({scaleX:0.7843,scaleY:0.7843,x:68.9316,y:32.0739},0).wait(1).to({scaleX:0.6866,scaleY:0.6866,x:68.3342,y:31.7383},0).wait(1).to({regX:-6,regY:-3.4,scaleX:0.5513,scaleY:0.5513,x:64.2,y:29.4},0).to({scaleX:0.3825,scaleY:0.3825,x:64.15,y:29.35},1).to({_off:true},1).wait(54));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_94 = function() {
		this.___loopingOver___ = true;
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(94).call(this.frame_94).wait(1));

	// Layer_9 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	var mask_graphics_0 = new cjs.Graphics().p("AD6NhI3N0oQSRspUWIcIgLPwQAQKRomAAQi7AAj+hMg");
	var mask_graphics_44 = new cjs.Graphics().p("AD6NhI3N0oQSRspUWIcIgLPwQAQKRomAAQi7AAj+hMg");
	var mask_graphics_45 = new cjs.Graphics().p("AD7NhI3N0oQSQsqUVIdIgKPwQAQKRomAAQi7AAj9hMg");
	var mask_graphics_46 = new cjs.Graphics().p("AD6NhI3N0oQSRsqUWIdIgLPwQAQKRomAAQi7AAj+hMg");
	var mask_graphics_47 = new cjs.Graphics().p("AD6NhI3N0oQSRsqUWIdIgLPwQAQKRomAAQi7AAj+hMg");
	var mask_graphics_48 = new cjs.Graphics().p("AD6NhI3M0oQSQsqUVIdIgKPwQAQKRomAAQi7AAj+hMg");
	var mask_graphics_49 = new cjs.Graphics().p("AD7NhI3N0oQSQsqUVIdIgKPwQAQKRomAAQi7AAj9hMg");
	var mask_graphics_51 = new cjs.Graphics().p("AD7NhI3N0oQSQsqUVIdIgKPwQAQKRomAAQi7AAj9hMg");
	var mask_graphics_52 = new cjs.Graphics().p("AEANkI3N0oQSQsqT/IPIAMP+QAQKRomAAQi7AAj9hMg");
	var mask_graphics_53 = new cjs.Graphics().p("AEANlI3N0nQSQsqTwIEIAbQJQAQKRomAAQi7AAj9hNg");
	var mask_graphics_54 = new cjs.Graphics().p("AEANnI3N0oQSQsqTmH+IAlQPQAQKRomAAQi7AAj9hMg");
	var mask_graphics_55 = new cjs.Graphics().p("AEANnI3N0nQSQsqTjH8IAoQRQAQKQomAAQi7AAj9hMg");
	var mask_graphics_56 = new cjs.Graphics().p("AEANnI3N0oQSQsqTmH+IAlQPQAQKRomAAQi7AAj9hMg");
	var mask_graphics_57 = new cjs.Graphics().p("AEANlI3N0nQSQsqTwIEIAbQJQAQKRomAAQi7AAj9hNg");
	var mask_graphics_58 = new cjs.Graphics().p("AEANkI3N0oQSQsqT/IPIAMP+QAQKRomAAQi7AAj9hMg");
	var mask_graphics_59 = new cjs.Graphics().p("AD7NhI3N0oQSQsqUVIdIgKPwQAQKRomAAQi7AAj9hMg");
	var mask_graphics_60 = new cjs.Graphics().p("AD7NhI3N0oQSQsqUVIdIgKPwQAQKRomAAQi7AAj9hMg");
	var mask_graphics_61 = new cjs.Graphics().p("AEANkI3N0oQSQsqT/IPIAMP+QAQKRomAAQi7AAj9hMg");
	var mask_graphics_62 = new cjs.Graphics().p("AEANlI3N0nQSQsqTwIEIAbQJQAQKRomAAQi7AAj9hNg");
	var mask_graphics_63 = new cjs.Graphics().p("AEANnI3N0oQSQsqTmH+IAlQPQAQKRomAAQi7AAj9hMg");
	var mask_graphics_64 = new cjs.Graphics().p("AEANnI3N0nQSQsqTjH8IAoQRQAQKQomAAQi7AAj9hMg");
	var mask_graphics_65 = new cjs.Graphics().p("AEANnI3N0oQSQsqTmH+IAlQPQAQKRomAAQi7AAj9hMg");
	var mask_graphics_66 = new cjs.Graphics().p("AEANlI3N0nQSQsqTwIEIAbQJQAQKRomAAQi7AAj9hNg");
	var mask_graphics_67 = new cjs.Graphics().p("AEANkI3N0oQSQsqT/IPIAMP+QAQKRomAAQi7AAj9hMg");
	var mask_graphics_68 = new cjs.Graphics().p("AD7NhI3N0oQSQsqUVIdIgKPwQAQKRomAAQi7AAj9hMg");
	var mask_graphics_74 = new cjs.Graphics().p("AD7NhI3N0oQSQsqUVIdIgKPwQAQKRomAAQi7AAj9hMg");
	var mask_graphics_75 = new cjs.Graphics().p("AD6NhI3M0oQSQsqUVIdIgKPwQAQKRomAAQi7AAj+hMg");
	var mask_graphics_76 = new cjs.Graphics().p("AD6NhI3N0oQSRsqUWIdIgLPwQAQKRomAAQi7AAj+hMg");
	var mask_graphics_77 = new cjs.Graphics().p("AD6NhI3N0oQSRsqUWIdIgLPwQAQKRomAAQi7AAj+hMg");
	var mask_graphics_78 = new cjs.Graphics().p("AD7NhI3N0oQSQsqUVIdIgKPwQAQKRomAAQi7AAj9hMg");
	var mask_graphics_79 = new cjs.Graphics().p("AD6NhI3N0oQSRspUWIcIgLPwQAQKRomAAQi7AAj+hMg");

	this.timeline.addTween(cjs.Tween.get(mask).to({graphics:mask_graphics_0,x:-101.7,y:-28.1458}).wait(44).to({graphics:mask_graphics_44,x:-101.7,y:-28.1458}).wait(1).to({graphics:mask_graphics_45,x:-103.65,y:-28.2451}).wait(1).to({graphics:mask_graphics_46,x:-105.2,y:-28.2951}).wait(1).to({graphics:mask_graphics_47,x:-106.3,y:-28.3451}).wait(1).to({graphics:mask_graphics_48,x:-106.95,y:-28.3951}).wait(1).to({graphics:mask_graphics_49,x:-107.15,y:-28.3951}).wait(2).to({graphics:mask_graphics_51,x:-107.15,y:-28.3951}).wait(1).to({graphics:mask_graphics_52,x:-109.9512,y:-28.8585}).wait(1).to({graphics:mask_graphics_53,x:-111.6012,y:-29.1935}).wait(1).to({graphics:mask_graphics_54,x:-112.5512,y:-29.3653}).wait(1).to({graphics:mask_graphics_55,x:-112.9012,y:-29.453}).wait(1).to({graphics:mask_graphics_56,x:-112.5512,y:-29.3653}).wait(1).to({graphics:mask_graphics_57,x:-111.6012,y:-29.1935}).wait(1).to({graphics:mask_graphics_58,x:-109.9512,y:-28.8585}).wait(1).to({graphics:mask_graphics_59,x:-107.15,y:-28.3951}).wait(1).to({graphics:mask_graphics_60,x:-107.15,y:-28.3951}).wait(1).to({graphics:mask_graphics_61,x:-109.9512,y:-28.8585}).wait(1).to({graphics:mask_graphics_62,x:-111.6012,y:-29.1935}).wait(1).to({graphics:mask_graphics_63,x:-112.5512,y:-29.3653}).wait(1).to({graphics:mask_graphics_64,x:-112.9012,y:-29.453}).wait(1).to({graphics:mask_graphics_65,x:-112.5512,y:-29.3653}).wait(1).to({graphics:mask_graphics_66,x:-111.6012,y:-29.1935}).wait(1).to({graphics:mask_graphics_67,x:-109.9512,y:-28.8585}).wait(1).to({graphics:mask_graphics_68,x:-107.15,y:-28.3951}).wait(6).to({graphics:mask_graphics_74,x:-107.15,y:-28.3951}).wait(1).to({graphics:mask_graphics_75,x:-106.95,y:-28.3951}).wait(1).to({graphics:mask_graphics_76,x:-106.3,y:-28.3451}).wait(1).to({graphics:mask_graphics_77,x:-105.2,y:-28.2951}).wait(1).to({graphics:mask_graphics_78,x:-103.65,y:-28.2451}).wait(1).to({graphics:mask_graphics_79,x:-101.7,y:-28.1458}).wait(16));

	// Layer_7_obj_
	this.Layer_7 = new lib.Symbol_2_Layer_7();
	this.Layer_7.name = "Layer_7";
	this.Layer_7.parent = this;
	this.Layer_7.setTransform(-31.9,22,1,1,0,0,0,-31.9,22);
	this.Layer_7.depth = 0;
	this.Layer_7.isAttachedToCamera = 0
	this.Layer_7.isAttachedToMask = 0
	this.Layer_7.layerDepth = 0
	this.Layer_7.layerIndex = 0
	this.Layer_7.maskLayerName = 0

	var maskedShapeInstanceList = [this.Layer_7];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.Layer_7).wait(95));

	// Layer_6_obj_
	this.Layer_6 = new lib.Symbol_2_Layer_6();
	this.Layer_6.name = "Layer_6";
	this.Layer_6.parent = this;
	this.Layer_6.setTransform(-7.9,-8.8,1,1,0,0,0,-7.9,-8.8);
	this.Layer_6.depth = 0;
	this.Layer_6.isAttachedToCamera = 0
	this.Layer_6.isAttachedToMask = 0
	this.Layer_6.layerDepth = 0
	this.Layer_6.layerIndex = 1
	this.Layer_6.maskLayerName = 0

	var maskedShapeInstanceList = [this.Layer_6];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.Layer_6).wait(95));

	// Layer_5_obj_
	this.Layer_5 = new lib.Symbol_2_Layer_5();
	this.Layer_5.name = "Layer_5";
	this.Layer_5.parent = this;
	this.Layer_5.setTransform(78.5,40.9,1,1,0,0,0,78.5,40.9);
	this.Layer_5.depth = 0;
	this.Layer_5.isAttachedToCamera = 0
	this.Layer_5.isAttachedToMask = 0
	this.Layer_5.layerDepth = 0
	this.Layer_5.layerIndex = 2
	this.Layer_5.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_5).wait(95));

	// Layer_4_obj_
	this.Layer_4 = new lib.Symbol_2_Layer_4();
	this.Layer_4.name = "Layer_4";
	this.Layer_4.parent = this;
	this.Layer_4.setTransform(125.5,-3.7,1,1,0,0,0,125.5,-3.7);
	this.Layer_4.depth = 0;
	this.Layer_4.isAttachedToCamera = 0
	this.Layer_4.isAttachedToMask = 0
	this.Layer_4.layerDepth = 0
	this.Layer_4.layerIndex = 3
	this.Layer_4.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_4).wait(95));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-94.5,-70.5,264.7,125.2);


(lib.Symbol1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_94 = function() {
		this.___loopingOver___ = true;
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(94).call(this.frame_94).wait(1));

	// Layer_6_obj_
	this.Layer_6 = new lib.Symbol_1_Layer_6();
	this.Layer_6.name = "Layer_6";
	this.Layer_6.parent = this;
	this.Layer_6.setTransform(55.5,34.4,1,1,0,0,0,55.5,34.4);
	this.Layer_6.depth = 0;
	this.Layer_6.isAttachedToCamera = 0
	this.Layer_6.isAttachedToMask = 0
	this.Layer_6.layerDepth = 0
	this.Layer_6.layerIndex = 0
	this.Layer_6.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_6).wait(95));

	// Layer_4_obj_
	this.Layer_4 = new lib.Symbol_1_Layer_4();
	this.Layer_4.name = "Layer_4";
	this.Layer_4.parent = this;
	this.Layer_4.depth = 0;
	this.Layer_4.isAttachedToCamera = 0
	this.Layer_4.isAttachedToMask = 0
	this.Layer_4.layerDepth = 0
	this.Layer_4.layerIndex = 1
	this.Layer_4.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_4).wait(5).to({x:44.25,y:40.35},0).wait(30).to({x:42.8946,y:39.1141},0).wait(1).to({x:38.4372,y:35.0496},0).wait(1).to({x:30.2127,y:27.5499},0).wait(1).to({x:17.5358,y:15.9903},0).wait(1).to({x:0,y:0},0).wait(56));

	// Layer_2_obj_
	this.Layer_2 = new lib.Symbol_1_Layer_2();
	this.Layer_2.name = "Layer_2";
	this.Layer_2.parent = this;
	this.Layer_2.setTransform(67.6,-1,1,1,0,0,0,67.6,-1);
	this.Layer_2.depth = 0;
	this.Layer_2.isAttachedToCamera = 0
	this.Layer_2.isAttachedToMask = 0
	this.Layer_2.layerDepth = 0
	this.Layer_2.layerIndex = 2
	this.Layer_2.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_2).wait(95));

	// Layer_1_obj_
	this.Layer_1 = new lib.Symbol_1_Layer_1();
	this.Layer_1.name = "Layer_1";
	this.Layer_1.parent = this;
	this.Layer_1.depth = 0;
	this.Layer_1.isAttachedToCamera = 0
	this.Layer_1.isAttachedToMask = 0
	this.Layer_1.layerDepth = 0
	this.Layer_1.layerIndex = 3
	this.Layer_1.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_1).wait(95));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-24.9,-21.8,117.5,75.5);


(lib.Symbol_5_Symbol_2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Symbol_2
	this.instance = new lib.Symbol2();
	this.instance.parent = this;
	this.instance.setTransform(-382.7,150.3,1.1684,1.1648,6.972,0,0,32,34.1);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(44).to({regY:34.3,scaleX:1.1683,rotation:-4.2199,x:-382.65,y:150.55},5,cjs.Ease.get(1)).wait(25).to({regX:32.1,regY:34.1,x:-382.6,y:150.3},0).to({regX:32,scaleX:1.1684,rotation:6.972,x:-382.7},5,cjs.Ease.get(-1)).wait(16));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Symbol1();
	this.instance.parent = this;
	this.instance.setTransform(-36.5,-103.8,1.301,1.301,7,0,0,0.1,0);

	this.instance_1 = new lib.pack();
	this.instance_1.parent = this;
	this.instance_1.setTransform(-188.5,-234.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_1},{t:this.instance}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol4, new cjs.Rectangle(-188.5,-234.5,377,469), null);


(lib.Symbol_5_Symbol_4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Symbol_4
	this.instance = new lib.Symbol4();
	this.instance.parent = this;
	this.instance.setTransform(-327.45,110.15,0.9117,0.9117,0,0,0,-0.1,0.1);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(44).to({regX:-60.6,regY:44.2,x:-382.6,y:150.35},0).to({rotation:-10.9616,x:-389.45},5,cjs.Ease.get(1)).wait(2).to({regX:-60.7,regY:44.3,rotation:-13.4534,x:-394.85,y:143.15},4,cjs.Ease.get(1)).to({regX:-60.6,regY:44.2,rotation:-10.9616,x:-389.45,y:150.35},4,cjs.Ease.get(-1)).wait(1).to({regX:-60.7,regY:44.3,rotation:-13.4534,x:-394.85,y:143.15},4,cjs.Ease.get(1)).to({regX:-60.6,regY:44.2,rotation:-10.9616,x:-389.45,y:150.35},4,cjs.Ease.get(-1)).wait(6).to({rotation:0,x:-382.6},5,cjs.Ease.get(-1)).wait(16));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_94 = function() {
		this.___loopingOver___ = true;
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(94).call(this.frame_94).wait(1));

	// Symbol_2_obj_
	this.Symbol_2 = new lib.Symbol_5_Symbol_2();
	this.Symbol_2.name = "Symbol_2";
	this.Symbol_2.parent = this;
	this.Symbol_2.setTransform(-443.1,69.8,1,1,0,0,0,-443.1,69.8);
	this.Symbol_2.depth = 0;
	this.Symbol_2.isAttachedToCamera = 0
	this.Symbol_2.isAttachedToMask = 0
	this.Symbol_2.layerDepth = 0
	this.Symbol_2.layerIndex = 0
	this.Symbol_2.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Symbol_2).wait(95));

	// Layer_3_obj_
	this.Layer_3 = new lib.Symbol_5_Layer_3();
	this.Layer_3.name = "Layer_3";
	this.Layer_3.parent = this;
	this.Layer_3.setTransform(-352,263.2,1,1,0,0,0,-352,263.2);
	this.Layer_3.depth = 0;
	this.Layer_3.isAttachedToCamera = 0
	this.Layer_3.isAttachedToMask = 0
	this.Layer_3.layerDepth = 0
	this.Layer_3.layerIndex = 1
	this.Layer_3.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_3).wait(95));

	// Layer_7_obj_
	this.Layer_7 = new lib.Symbol_5_Layer_7();
	this.Layer_7.name = "Layer_7";
	this.Layer_7.parent = this;
	this.Layer_7.setTransform(-358.8,222.7,1,1,0,0,0,-358.8,222.7);
	this.Layer_7.depth = 0;
	this.Layer_7.isAttachedToCamera = 0
	this.Layer_7.isAttachedToMask = 0
	this.Layer_7.layerDepth = 0
	this.Layer_7.layerIndex = 2
	this.Layer_7.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_7).wait(95));

	// Symbol_4_obj_
	this.Symbol_4 = new lib.Symbol_5_Symbol_4();
	this.Symbol_4.name = "Symbol_4";
	this.Symbol_4.parent = this;
	this.Symbol_4.setTransform(-327.4,110,1,1,0,0,0,-327.4,110);
	this.Symbol_4.depth = 0;
	this.Symbol_4.isAttachedToCamera = 0
	this.Symbol_4.isAttachedToMask = 0
	this.Symbol_4.layerDepth = 0
	this.Symbol_4.layerIndex = 3
	this.Symbol_4.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Symbol_4).wait(95));

	// Layer_2_obj_
	this.Layer_2 = new lib.Symbol_5_Layer_2();
	this.Layer_2.name = "Layer_2";
	this.Layer_2.parent = this;
	this.Layer_2.setTransform(-352,19.2,1,1,0,0,0,-352,19.2);
	this.Layer_2.depth = 0;
	this.Layer_2.isAttachedToCamera = 0
	this.Layer_2.isAttachedToMask = 0
	this.Layer_2.layerDepth = 0
	this.Layer_2.layerIndex = 4
	this.Layer_2.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_2).wait(95));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-695.8,-339.9,922.1999999999999,812.0999999999999);


(lib.Scene_1_Layer_6 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_6
	this.instance = new lib.Symbol5();
	this.instance.parent = this;
	this.instance.setTransform(540,340.2);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Scene_1_Layer_6, null, null);


// stage content:
(lib.Nov_vert = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	this.___GetDepth___ = function(obj) {
		var depth = obj.depth;
		var cameraObj = this.___camera___instance;
		if(cameraObj && cameraObj.depth && obj.isAttachedToCamera)
		{
			depth += depth + cameraObj.depth;
		}
		return depth;
		}
	this.___needSorting___ = function() {
		for (var i = 0; i < this.getNumChildren() - 1; i++)
		{
			var prevDepth = this.___GetDepth___(this.getChildAt(i));
			var nextDepth = this.___GetDepth___(this.getChildAt(i + 1));
			if (prevDepth < nextDepth)
				return true;
		}
		return false;
	}
	this.___sortFunction___ = function(obj1, obj2) {
		return (this.exportRoot.___GetDepth___(obj2) - this.exportRoot.___GetDepth___(obj1));
	}
	this.on('tick', function (event){
		var curTimeline = event.currentTarget;
		if (curTimeline.___needSorting___()){
			this.sortChildren(curTimeline.___sortFunction___);
		}
	});

	// Layer_6_obj_
	this.Layer_6 = new lib.Scene_1_Layer_6();
	this.Layer_6.name = "Layer_6";
	this.Layer_6.parent = this;
	this.Layer_6.setTransform(197.2,406.4,1,1,0,0,0,197.2,406.4);
	this.Layer_6.depth = 0;
	this.Layer_6.isAttachedToCamera = 0
	this.Layer_6.isAttachedToMask = 0
	this.Layer_6.layerDepth = 0
	this.Layer_6.layerIndex = 0
	this.Layer_6.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_6).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(41.9,406,342.6,406.4);
// library properties:
lib.properties = {
	id: 'AD8A6DF1819D4B41BE0B89F97E9B12F6',
	width: 375,
	height: 812,
	fps: 24,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: [
		{src:"assets/images/bg_part1_vert56-2.png", id:"bg_part1_vert"},
		{src:"assets/images/bg_part2_vert56-2.png", id:"bg_part2_vert"},
		{src:"assets/images/pack56-2.png", id:"pack"},
		{src:"assets/images/shadow56-2.png", id:"shadow"}
	],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['AD8A6DF1819D4B41BE0B89F97E9B12F6'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}


// Layer depth API : 

AdobeAn.Layer = new function() {
	this.getLayerZDepth = function(timeline, layerName)
	{
		if(layerName === "Camera")
		layerName = "___camera___instance";
		var script = "if(timeline." + layerName + ") timeline." + layerName + ".depth; else 0;";
		return eval(script);
	}
	this.setLayerZDepth = function(timeline, layerName, zDepth)
	{
		const MAX_zDepth = 10000;
		const MIN_zDepth = -5000;
		if(zDepth > MAX_zDepth)
			zDepth = MAX_zDepth;
		else if(zDepth < MIN_zDepth)
			zDepth = MIN_zDepth;
		if(layerName === "Camera")
		layerName = "___camera___instance";
		var script = "if(timeline." + layerName + ") timeline." + layerName + ".depth = " + zDepth + ";";
		eval(script);
	}
	this.removeLayer = function(timeline, layerName)
	{
		if(layerName === "Camera")
		layerName = "___camera___instance";
		var script = "if(timeline." + layerName + ") timeline.removeChild(timeline." + layerName + ");";
		eval(script);
	}
	this.addNewLayer = function(timeline, layerName, zDepth)
	{
		if(layerName === "Camera")
		layerName = "___camera___instance";
		zDepth = typeof zDepth !== 'undefined' ? zDepth : 0;
		var layer = new createjs.MovieClip();
		layer.name = layerName;
		layer.depth = zDepth;
		layer.layerIndex = 0;
		timeline.addChild(layer);
	}
}


})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;

$(function () {
  var canvas, stage, exportRoot, anim_container, dom_overlay_container, fnStartAnimation;
  function init() {
    canvas = document.getElementById("canvas56-2");
    anim_container = document.getElementById("animation_container56-2");
    dom_overlay_container = document.getElementById("dom_overlay_container56-2");
    var comp=AdobeAn.getComposition("AD8A6DF1819D4B41BE0B89F97E9B12F6");
    var lib=comp.getLibrary();
    createjs.MotionGuidePlugin.install();
    var loader = new createjs.LoadQueue(false);
    loader.addEventListener("fileload", function(evt){handleFileLoad(evt,comp)});
    loader.addEventListener("complete", function(evt){handleComplete(evt,comp)});
    var lib=comp.getLibrary();
    loader.loadManifest(lib.properties.manifest);
  }
  function handleFileLoad(evt, comp) {
    var images=comp.getImages();
    if (evt && (evt.item.type == "image")) { images[evt.item.id] = evt.result; }
  }
  function handleComplete(evt,comp) {
    //This function is always called, irrespective of the content. You can use the variable "stage" after it is created in token create_stage.
    var lib=comp.getLibrary();
    var ss=comp.getSpriteSheet();
    var queue = evt.target;
    var ssMetadata = lib.ssMetadata;
    for(i=0; i<ssMetadata.length; i++) {
      ss[ssMetadata[i].name] = new createjs.SpriteSheet( {"images": [queue.getResult(ssMetadata[i].name)], "frames": ssMetadata[i].frames} )
    }
    var preloaderDiv = document.getElementById("_preload_div_56");
    preloaderDiv.style.display = 'none';
    canvas.style.display = 'block';
    exportRoot = new lib.Nov_vert();
    stage = new lib.Stage(canvas);
    //Registers the "tick" event listener.
    fnStartAnimation = function() {
      stage.addChild(exportRoot);
      createjs.Ticker.setFPS(lib.properties.fps);
      createjs.Ticker.addEventListener("tick", stage)
      stage.addEventListener("tick", handleTick)
      function getProjectionMatrix(container, totalDepth) {
        var focalLength = 528.25;
        var projectionCenter = { x : lib.properties.width/2, y : lib.properties.height/2 };
        var scale = (totalDepth + focalLength)/focalLength;
        var scaleMat = new createjs.Matrix2D;
        scaleMat.a = 1/scale;
        scaleMat.d = 1/scale;
        var projMat = new createjs.Matrix2D;
        projMat.tx = -projectionCenter.x;
        projMat.ty = -projectionCenter.y;
        projMat = projMat.prependMatrix(scaleMat);
        projMat.tx += projectionCenter.x;
        projMat.ty += projectionCenter.y;
        return projMat;
      }
      function handleTick(event) {
        var cameraInstance = exportRoot.___camera___instance;
        if(cameraInstance !== undefined && cameraInstance.pinToObject !== undefined)
        {
          cameraInstance.x = cameraInstance.pinToObject.x + cameraInstance.pinToObject.pinOffsetX;
          cameraInstance.y = cameraInstance.pinToObject.y + cameraInstance.pinToObject.pinOffsetY;
          if(cameraInstance.pinToObject.parent !== undefined && cameraInstance.pinToObject.parent.depth !== undefined)
            cameraInstance.depth = cameraInstance.pinToObject.parent.depth + cameraInstance.pinToObject.pinOffsetZ;
        }
        applyLayerZDepth(exportRoot);
      }
      function applyLayerZDepth(parent)
      {
        var cameraInstance = parent.___camera___instance;
        var focalLength = 528.25;
        var projectionCenter = { 'x' : 0, 'y' : 0};
        if(parent === exportRoot)
        {
          var stageCenter = { 'x' : lib.properties.width/2, 'y' : lib.properties.height/2 };
          projectionCenter.x = stageCenter.x;
          projectionCenter.y = stageCenter.y;
        }
        for(child in parent.children)
        {
          var layerObj = parent.children[child];
          if(layerObj == cameraInstance)
            continue;
          applyLayerZDepth(layerObj, cameraInstance);
          if(layerObj.layerDepth === undefined)
            continue;
          if(layerObj.currentFrame != layerObj.parent.currentFrame)
          {
            layerObj.gotoAndPlay(layerObj.parent.currentFrame);
          }
          var matToApply = new createjs.Matrix2D;
          var cameraMat = new createjs.Matrix2D;
          var totalDepth = layerObj.layerDepth ? layerObj.layerDepth : 0;
          var cameraDepth = 0;
          if(cameraInstance && !layerObj.isAttachedToCamera)
          {
            var mat = cameraInstance.getMatrix();
            mat.tx -= projectionCenter.x;
            mat.ty -= projectionCenter.y;
            cameraMat = mat.invert();
            cameraMat.prependTransform(projectionCenter.x, projectionCenter.y, 1, 1, 0, 0, 0, 0, 0);
            cameraMat.appendTransform(-projectionCenter.x, -projectionCenter.y, 1, 1, 0, 0, 0, 0, 0);
            if(cameraInstance.depth)
              cameraDepth = cameraInstance.depth;
          }
          if(layerObj.depth)
          {
            totalDepth = layerObj.depth;
          }
          //Offset by camera depth
          totalDepth -= cameraDepth;
          if(totalDepth < -focalLength)
          {
            matToApply.a = 0;
            matToApply.d = 0;
          }
          else
          {
            if(layerObj.layerDepth)
            {
              var sizeLockedMat = getProjectionMatrix(parent, layerObj.layerDepth);
              if(sizeLockedMat)
              {
                sizeLockedMat.invert();
                matToApply.prependMatrix(sizeLockedMat);
              }
            }
            matToApply.prependMatrix(cameraMat);
            var projMat = getProjectionMatrix(parent, totalDepth);
            if(projMat)
            {
              matToApply.prependMatrix(projMat);
            }
          }
          layerObj.transformMatrix = matToApply;
        }
      }
    }
    //Code to support hidpi screens and responsive scaling.
    function makeResponsive(isResp, respDim, isScale, scaleType) {
      var lastW, lastH, lastS=1;
      window.addEventListener('resize', resizeCanvas);
      resizeCanvas();
      function resizeCanvas() {
        var w = lib.properties.width, h = lib.properties.height;
        var iw = window.innerWidth, ih=window.innerHeight;
        var pRatio = window.devicePixelRatio || 1, xRatio=iw/w, yRatio=ih/h, sRatio=1;
        if(isResp) {
          if((respDim=='width'&&lastW==iw) || (respDim=='height'&&lastH==ih)) {
            sRatio = lastS;
          }
          else if(!isScale) {
            if(iw<w || ih<h)
              sRatio = Math.min(xRatio, yRatio);
          }
          else if(scaleType==1) {
            sRatio = Math.min(xRatio, yRatio);
          }
          else if(scaleType==2) {
            sRatio = Math.max(xRatio, yRatio);
          }
        }
        canvas.width = w*pRatio*sRatio;
        canvas.height = h*pRatio*sRatio;
        canvas.style.width = anim_container.style.width = dom_overlay_container.style.width = preloaderDiv.style.width = w*sRatio+'px';
        canvas.style.height = anim_container.style.height = dom_overlay_container.style.height = preloaderDiv.style.height = h*sRatio+'px';
        stage.scaleX = pRatio*sRatio;
        stage.scaleY = pRatio*sRatio;
        lastW = iw; lastH = ih; lastS = sRatio;
        stage.tickOnUpdate = false;
        stage.update();
        stage.tickOnUpdate = true;
      }
    }
    makeResponsive(true,'both',true,2);
    AdobeAn.compositionLoaded(lib.properties.id);
    fnStartAnimation();
  }
  init();
});