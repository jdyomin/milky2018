(function (lib, img, cjs, ss) {

var p; // shortcut to reference prototypes
lib.webFontTxtFilters = {}; 

// library properties:
lib.properties = {
	width: 1080,
	height: 680,
	fps: 30,
	color: "#FFFFFF",
	webfonts: {},
	manifest: [
		{src:"assets/images/clock_125.png", id:"clock_1"},
		{src:"assets/images/dec25.png", id:"dec"},
		{src:"assets/images/feb25.png", id:"feb"},
		{src:"assets/images/jen25.png", id:"jen"},
		{src:"assets/images/mar25.png", id:"mar"},
		{src:"assets/images/nov25.png", id:"nov"},
		{src:"assets/images/oct25.png", id:"oct"},
		{src:"assets/images/pack25.png", id:"pack"}
	]
};



lib.webfontAvailable = function(family) { 
	lib.properties.webfonts[family] = true;
	var txtFilters = lib.webFontTxtFilters && lib.webFontTxtFilters[family] || [];
	for(var f = 0; f < txtFilters.length; ++f) {
		txtFilters[f].updateCache();
	}
};
// symbols:



(lib.bg = function() {
	this.initialize(img.bg);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,1080,680);


(lib.clock_1 = function() {
	this.initialize(img.clock_1);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,426,343);


(lib.dec = function() {
	this.initialize(img.dec);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,83,44);


(lib.feb = function() {
	this.initialize(img.feb);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,88,39);


(lib.jen = function() {
	this.initialize(img.jen);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,72,38);


(lib.mar = function() {
	this.initialize(img.mar);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,49,38);


(lib.nov = function() {
	this.initialize(img.nov);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,76,39);


(lib.oct = function() {
	this.initialize(img.oct);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,85,39);


(lib.pack = function() {
	this.initialize(img.pack);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,360,496);


(lib.Symbol30 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.dec();

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,83,44);


(lib.Symbol29 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.jen();

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,72,38);


(lib.Symbol28 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.feb();

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,88,39);


(lib.Symbol27 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.mar();

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,49,38);


(lib.Symbol25copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("rgba(81,55,36,0.647)").s().p("Ak+FgIJtrMIAQAOIpwLLg");
	this.shape.setTransform(31.9,36.6);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,63.8,73.1);


(lib.Symbol25 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("Ak+FgIJtrMIAQAOIpwLLg");
	this.shape.setTransform(31.9,36.6);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,63.8,73.1);


(lib.Symbol24 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.oct();

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,85,39);


(lib.Symbol23 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.nov();

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,76,39);


(lib.Symbol22copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AgYrXIAxgDIgFWxIgkAEgAAULXIAAAAIAAAAIAAAAg");
	this.shape.setTransform(2.5,73.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#000000").s().p("AgYrPIAxgDIgFWgIgkAFgAAULOIAAAAIAAAAIAAAAg");
	this.shape_1.setTransform(2.5,74);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("AgYrHIAxgDIgFWQIgkAFgAAULGIAAAAIAAAAIAAAAg");
	this.shape_2.setTransform(2.5,74.8);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#000000").s().p("AgYq/IAxgDIgFWAIgkAFgAAUK+IAAAAIAAAAIAAAAg");
	this.shape_3.setTransform(2.5,75.6);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#000000").s().p("AgYq3IAxgCIgFVvIgkAEgAAUK2IAAAAIAAAAIAAAAg");
	this.shape_4.setTransform(2.5,76.5);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#000000").s().p("AgYquIAxgDIgFVfIgkAEgAAUKuIAAAAIAAAAIAAAAg");
	this.shape_5.setTransform(2.5,77.3);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#000000").s().p("AgYqnIAxgCIgFVOIgkAFgAAUKlIAAAAIAAAAIAAAAg");
	this.shape_6.setTransform(2.5,78.1);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#000000").s().p("AgYqeIAxgDIgFU/IgkAEgAAUKeIAAAAIAAAAIAAAAg");
	this.shape_7.setTransform(2.5,78.9);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#000000").s().p("AgYqWIAxgDIgFUvIgkAEgAAUKWIAAAAIAAAAIAAAAg");
	this.shape_8.setTransform(2.5,79.7);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#000000").s().p("AgYqOIAxgCIgFUeIgkADgAAUKOIAAAAIAAAAIAAAAg");
	this.shape_9.setTransform(2.5,80.6);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#000000").s().p("AgYqGIAxgCIgFUNIgkAEgAAUKFIAAAAIAAAAIAAAAg");
	this.shape_10.setTransform(2.5,81.4);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#000000").s().p("AgYp+IAxgCIgFT9IgkAEgAAUJ9IAAAAIAAAAIAAAAg");
	this.shape_11.setTransform(2.5,82.2);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#000000").s().p("AgYp1IAxgDIgFTtIgkAEgAAUJ1IAAAAIAAAAIAAAAg");
	this.shape_12.setTransform(2.5,83);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#000000").s().p("AgYptIAxgDIgFTdIgkAEgAAUJtIAAAAIAAAAIAAAAg");
	this.shape_13.setTransform(2.5,83.8);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#000000").s().p("AgYplIAxgDIgFTNIgkADgAAUJlIAAAAIAAAAIAAAAg");
	this.shape_14.setTransform(2.5,84.7);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#000000").s().p("AgYppIAxgDIgFTVIgkAEgAAUJpIAAAAIAAAAIAAAAg");
	this.shape_15.setTransform(2.5,84.2);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#000000").s().p("AgYpuIAxgDIgFTfIgkAEgAAUJuIAAAAIAAAAIAAAAg");
	this.shape_16.setTransform(2.5,83.7);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#000000").s().p("AgYp1IAxgCIgFTrIgkAEgAAUJ0IAAAAIAAAAIAAAAg");
	this.shape_17.setTransform(2.5,83.1);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#000000").s().p("AgYp8IAxgCIgFT6IgkADgAAUJ8IAAAAIAAAAIAAAAg");
	this.shape_18.setTransform(2.5,82.4);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#000000").s().p("AgYqEIAxgDIgFULIgkADgAAUKEIAAAAIAAAAIAAAAg");
	this.shape_19.setTransform(2.5,81.6);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#000000").s().p("AgYqNIAxgDIgFUdIgkAEgAAUKNIAAAAIAAAAIAAAAg");
	this.shape_20.setTransform(2.5,80.6);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#000000").s().p("AgYqXIAxgDIgFUxIgkAEgAAUKXIAAAAIAAAAIAAAAg");
	this.shape_21.setTransform(2.5,79.6);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#000000").s().p("AgYqiIAxgDIgFVGIgkAFgAAUKhIAAAAIAAAAIAAAAg");
	this.shape_22.setTransform(2.5,78.5);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#000000").s().p("AgYquIAxgDIgFVeIgkAFgAAUKtIAAAAIAAAAIAAAAg");
	this.shape_23.setTransform(2.5,77.3);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#000000").s().p("AgYq7IAxgCIgFV3IgkAEgAAUK6IAAAAIAAAAIAAAAg");
	this.shape_24.setTransform(2.5,76.1);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#000000").s().p("AgYrJIAxgCIgFWTIgkAEgAAULIIAAAAIAAAAIAAAAg");
	this.shape_25.setTransform(2.5,74.7);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#000000").s().p("AgYrUIAxgDIgFWqIgkAFgAAULTIAAAAIAAAAIAAAAg");
	this.shape_26.setTransform(2.5,73.5);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#000000").s().p("AgYrRIAxgDIgFWlIgkAEgAAULRIAAAAIAAAAIAAAAg");
	this.shape_27.setTransform(2.5,73.8);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#000000").s().p("AgYrOIAxgEIgFWgIgkAEgAAULOIAAAAIAAAAIAAAAg");
	this.shape_28.setTransform(2.5,74.1);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#000000").s().p("AgYrMIAxgDIgFWbIgkAEgAAULMIAAAAIAAAAIAAAAg");
	this.shape_29.setTransform(2.5,74.3);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#000000").s().p("AgYrKIAxgDIgFWWIgkAFgAAULJIAAAAIAAAAIAAAAg");
	this.shape_30.setTransform(2.5,74.5);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#000000").s().p("AgYrIIAxgDIgFWTIgkAEgAAULIIAAAAIAAAAIAAAAg");
	this.shape_31.setTransform(2.5,74.7);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#000000").s().p("AgYrFIAxgEIgFWOIgkAEgAAULFIAAAAIAAAAIAAAAg");
	this.shape_32.setTransform(2.5,75);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#000000").s().p("AgYrFIAxgCIgFWLIgkAEgAAULEIAAAAIAAAAIAAAAg");
	this.shape_33.setTransform(2.5,75.1);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#000000").s().p("AgYrEIAxgDIgFWKIgkAFgAAULDIAAAAIAAAAIAAAAg");
	this.shape_34.setTransform(2.5,75.1);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#000000").s().p("AgYrEIAxgCIgFWJIgkAEgAAULDIAAAAIAAAAIAAAAg");
	this.shape_35.setTransform(2.5,75.2);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#000000").s().p("AgYrDIAxgDIgFWJIgkAEgAAULDIAAAAIAAAAIAAAAg");
	this.shape_36.setTransform(2.5,75.2);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f("#000000").s().p("AgYrFIAxgDIgFWMIgkAFgAAULEIAAAAIAAAAIAAAAg");
	this.shape_37.setTransform(2.5,75);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f("#000000").s().p("AgYrGIAxgEIgFWQIgkAEgAAULGIAAAAIAAAAIAAAAg");
	this.shape_38.setTransform(2.5,74.9);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f("#000000").s().p("AgYrMIAxgDIgFWaIgkAEgAAULLIAAAAIAAAAIAAAAg");
	this.shape_39.setTransform(2.5,74.4);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f("#000000").s().p("AgYrNIAxgDIgFWdIgkAEgAAULNIAAAAIAAAAIAAAAg");
	this.shape_40.setTransform(2.5,74.2);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f("#000000").s().p("AgYrRIAxgCIgFWjIgkAEgAAULQIAAAAIAAAAIAAAAg");
	this.shape_41.setTransform(2.5,73.9);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f("#000000").s().p("AgYrSIAxgDIgFWnIgkAEgAAULSIAAAAIAAAAIAAAAg");
	this.shape_42.setTransform(2.5,73.7);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f("#000000").s().p("AgYrWIAxgCIgFWtIgkAEgAAULVIAAAAIAAAAIAAAAg");
	this.shape_43.setTransform(2.5,73.4);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[]},25).to({state:[{t:this.shape}]},44).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_27}]},1).to({state:[{t:this.shape_28}]},1).to({state:[{t:this.shape_29}]},1).to({state:[{t:this.shape_30}]},1).to({state:[{t:this.shape_31}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_32}]},1).to({state:[{t:this.shape_33}]},1).to({state:[{t:this.shape_34}]},1).to({state:[{t:this.shape_35}]},1).to({state:[{t:this.shape_36}]},1).to({state:[{t:this.shape_37}]},1).to({state:[{t:this.shape_38}]},1).to({state:[{t:this.shape_31}]},1).to({state:[{t:this.shape_30}]},1).to({state:[{t:this.shape_39}]},1).to({state:[{t:this.shape_40}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_41}]},1).to({state:[{t:this.shape_42}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_43}]},1).to({state:[]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,5,146.4);


(lib.Symbol22 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("rgba(81,55,36,0.647)").s().p("AgYrXIAxgDIgFWxIgkAEgAAULXIAAAAIAAAAIAAAAg");
	this.shape.setTransform(2.5,73.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("rgba(81,55,36,0.647)").s().p("AgYrPIAxgDIgFWgIgkAFgAAULOIAAAAIAAAAIAAAAg");
	this.shape_1.setTransform(2.5,74);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("rgba(81,55,36,0.647)").s().p("AgYrHIAxgDIgFWQIgkAFgAAULGIAAAAIAAAAIAAAAg");
	this.shape_2.setTransform(2.5,74.8);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("rgba(81,55,36,0.647)").s().p("AgYq/IAxgDIgFWAIgkAFgAAUK+IAAAAIAAAAIAAAAg");
	this.shape_3.setTransform(2.5,75.6);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("rgba(81,55,36,0.647)").s().p("AgYq3IAxgCIgFVvIgkAEgAAUK2IAAAAIAAAAIAAAAg");
	this.shape_4.setTransform(2.5,76.5);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("rgba(81,55,36,0.647)").s().p("AgYquIAxgDIgFVfIgkAEgAAUKuIAAAAIAAAAIAAAAg");
	this.shape_5.setTransform(2.5,77.3);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("rgba(81,55,36,0.647)").s().p("AgYqnIAxgCIgFVOIgkAFgAAUKlIAAAAIAAAAIAAAAg");
	this.shape_6.setTransform(2.5,78.1);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("rgba(81,55,36,0.647)").s().p("AgYqeIAxgDIgFU/IgkAEgAAUKeIAAAAIAAAAIAAAAg");
	this.shape_7.setTransform(2.5,78.9);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("rgba(81,55,36,0.647)").s().p("AgYqWIAxgDIgFUvIgkAEgAAUKWIAAAAIAAAAIAAAAg");
	this.shape_8.setTransform(2.5,79.7);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("rgba(81,55,36,0.647)").s().p("AgYqOIAxgCIgFUeIgkADgAAUKOIAAAAIAAAAIAAAAg");
	this.shape_9.setTransform(2.5,80.6);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("rgba(81,55,36,0.647)").s().p("AgYqGIAxgCIgFUNIgkAEgAAUKFIAAAAIAAAAIAAAAg");
	this.shape_10.setTransform(2.5,81.4);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("rgba(81,55,36,0.647)").s().p("AgYp+IAxgCIgFT9IgkAEgAAUJ9IAAAAIAAAAIAAAAg");
	this.shape_11.setTransform(2.5,82.2);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("rgba(81,55,36,0.647)").s().p("AgYp1IAxgDIgFTtIgkAEgAAUJ1IAAAAIAAAAIAAAAg");
	this.shape_12.setTransform(2.5,83);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("rgba(81,55,36,0.647)").s().p("AgYptIAxgDIgFTdIgkAEgAAUJtIAAAAIAAAAIAAAAg");
	this.shape_13.setTransform(2.5,83.8);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("rgba(81,55,36,0.647)").s().p("AgYplIAxgDIgFTNIgkADgAAUJlIAAAAIAAAAIAAAAg");
	this.shape_14.setTransform(2.5,84.7);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("rgba(81,55,36,0.647)").s().p("AgYppIAxgDIgFTVIgkAEgAAUJpIAAAAIAAAAIAAAAg");
	this.shape_15.setTransform(2.5,84.2);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("rgba(81,55,36,0.647)").s().p("AgYpuIAxgDIgFTfIgkAEgAAUJuIAAAAIAAAAIAAAAg");
	this.shape_16.setTransform(2.5,83.7);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("rgba(81,55,36,0.647)").s().p("AgYp1IAxgCIgFTrIgkAEgAAUJ0IAAAAIAAAAIAAAAg");
	this.shape_17.setTransform(2.5,83.1);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("rgba(81,55,36,0.647)").s().p("AgYp8IAxgCIgFT6IgkADgAAUJ8IAAAAIAAAAIAAAAg");
	this.shape_18.setTransform(2.5,82.4);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("rgba(81,55,36,0.647)").s().p("AgYqEIAxgDIgFULIgkADgAAUKEIAAAAIAAAAIAAAAg");
	this.shape_19.setTransform(2.5,81.6);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("rgba(81,55,36,0.647)").s().p("AgYqNIAxgDIgFUdIgkAEgAAUKNIAAAAIAAAAIAAAAg");
	this.shape_20.setTransform(2.5,80.6);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("rgba(81,55,36,0.647)").s().p("AgYqXIAxgDIgFUxIgkAEgAAUKXIAAAAIAAAAIAAAAg");
	this.shape_21.setTransform(2.5,79.6);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("rgba(81,55,36,0.647)").s().p("AgYqiIAxgDIgFVGIgkAFgAAUKhIAAAAIAAAAIAAAAg");
	this.shape_22.setTransform(2.5,78.5);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("rgba(81,55,36,0.647)").s().p("AgYquIAxgDIgFVeIgkAFgAAUKtIAAAAIAAAAIAAAAg");
	this.shape_23.setTransform(2.5,77.3);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("rgba(81,55,36,0.647)").s().p("AgYq7IAxgCIgFV3IgkAEgAAUK6IAAAAIAAAAIAAAAg");
	this.shape_24.setTransform(2.5,76.1);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("rgba(81,55,36,0.647)").s().p("AgYrJIAxgCIgFWTIgkAEgAAULIIAAAAIAAAAIAAAAg");
	this.shape_25.setTransform(2.5,74.7);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("rgba(81,55,36,0.647)").s().p("AgYrUIAxgDIgFWqIgkAFgAAULTIAAAAIAAAAIAAAAg");
	this.shape_26.setTransform(2.5,73.5);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("rgba(81,55,36,0.647)").s().p("AgYrRIAxgDIgFWlIgkAEgAAULRIAAAAIAAAAIAAAAg");
	this.shape_27.setTransform(2.5,73.8);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("rgba(81,55,36,0.647)").s().p("AgYrOIAxgEIgFWgIgkAEgAAULOIAAAAIAAAAIAAAAg");
	this.shape_28.setTransform(2.5,74.1);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("rgba(81,55,36,0.647)").s().p("AgYrMIAxgDIgFWbIgkAEgAAULMIAAAAIAAAAIAAAAg");
	this.shape_29.setTransform(2.5,74.3);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("rgba(81,55,36,0.647)").s().p("AgYrKIAxgDIgFWWIgkAFgAAULJIAAAAIAAAAIAAAAg");
	this.shape_30.setTransform(2.5,74.5);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("rgba(81,55,36,0.647)").s().p("AgYrIIAxgDIgFWTIgkAEgAAULIIAAAAIAAAAIAAAAg");
	this.shape_31.setTransform(2.5,74.7);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("rgba(81,55,36,0.647)").s().p("AgYrFIAxgEIgFWOIgkAEgAAULFIAAAAIAAAAIAAAAg");
	this.shape_32.setTransform(2.5,75);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("rgba(81,55,36,0.647)").s().p("AgYrFIAxgCIgFWLIgkAEgAAULEIAAAAIAAAAIAAAAg");
	this.shape_33.setTransform(2.5,75.1);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("rgba(81,55,36,0.647)").s().p("AgYrEIAxgDIgFWKIgkAFgAAULDIAAAAIAAAAIAAAAg");
	this.shape_34.setTransform(2.5,75.1);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("rgba(81,55,36,0.647)").s().p("AgYrEIAxgCIgFWJIgkAEgAAULDIAAAAIAAAAIAAAAg");
	this.shape_35.setTransform(2.5,75.2);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("rgba(81,55,36,0.647)").s().p("AgYrDIAxgDIgFWJIgkAEgAAULDIAAAAIAAAAIAAAAg");
	this.shape_36.setTransform(2.5,75.2);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f("rgba(81,55,36,0.647)").s().p("AgYrFIAxgDIgFWMIgkAFgAAULEIAAAAIAAAAIAAAAg");
	this.shape_37.setTransform(2.5,75);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f("rgba(81,55,36,0.647)").s().p("AgYrGIAxgEIgFWQIgkAEgAAULGIAAAAIAAAAIAAAAg");
	this.shape_38.setTransform(2.5,74.9);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f("rgba(81,55,36,0.647)").s().p("AgYrMIAxgDIgFWaIgkAEgAAULLIAAAAIAAAAIAAAAg");
	this.shape_39.setTransform(2.5,74.4);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f("rgba(81,55,36,0.647)").s().p("AgYrNIAxgDIgFWdIgkAEgAAULNIAAAAIAAAAIAAAAg");
	this.shape_40.setTransform(2.5,74.2);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f("rgba(81,55,36,0.647)").s().p("AgYrRIAxgCIgFWjIgkAEgAAULQIAAAAIAAAAIAAAAg");
	this.shape_41.setTransform(2.5,73.9);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f("rgba(81,55,36,0.647)").s().p("AgYrSIAxgDIgFWnIgkAEgAAULSIAAAAIAAAAIAAAAg");
	this.shape_42.setTransform(2.5,73.7);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f("rgba(81,55,36,0.647)").s().p("AgYrWIAxgCIgFWtIgkAEgAAULVIAAAAIAAAAIAAAAg");
	this.shape_43.setTransform(2.5,73.4);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[]},25).to({state:[{t:this.shape}]},44).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_27}]},1).to({state:[{t:this.shape_28}]},1).to({state:[{t:this.shape_29}]},1).to({state:[{t:this.shape_30}]},1).to({state:[{t:this.shape_31}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_32}]},1).to({state:[{t:this.shape_33}]},1).to({state:[{t:this.shape_34}]},1).to({state:[{t:this.shape_35}]},1).to({state:[{t:this.shape_36}]},1).to({state:[{t:this.shape_37}]},1).to({state:[{t:this.shape_38}]},1).to({state:[{t:this.shape_31}]},1).to({state:[{t:this.shape_30}]},1).to({state:[{t:this.shape_39}]},1).to({state:[{t:this.shape_40}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_41}]},1).to({state:[{t:this.shape_42}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_43}]},1).to({state:[]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,5,146.4);


(lib.Symbol18 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Symbol 17
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(12.5,1,1).p("AhAAUQAhg7BgAe");
	this.shape.setTransform(6.6,25.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(12.5,1,1).p("AhBATQAhg2BiAa");
	this.shape_1.setTransform(6.7,25.3);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(12.5,1,1).p("AhEAPQAjgpBmAR");
	this.shape_2.setTransform(6.9,25.6);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(12.5,1,1).p("AhIAKQAkgTBtAA");
	this.shape_3.setTransform(7.4,26.1);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(12.5,1,1).p("AhOAFQAoAIB1gT");
	this.shape_4.setTransform(8,26.7);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(12.5,1,1).p("AhKAIQAmgIBvgI");
	this.shape_5.setTransform(7.6,26.3);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(12.5,1,1).p("AhHALQAkgYBrAE");
	this.shape_6.setTransform(7.3,26.1);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(12.5,1,1).p("AhFAOQAjgkBoAN");
	this.shape_7.setTransform(7,25.8);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(12.5,1,1).p("AhDAQQAiguBkAV");
	this.shape_8.setTransform(6.8,25.5);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(12.5,1,1).p("AhBASQAhg1BiAa");
	this.shape_9.setTransform(6.7,25.3);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(12.5,1,1).p("AhAATQAhg5BgAd");
	this.shape_10.setTransform(6.6,25.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},19).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_4}]},2).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape}]},1).wait(43));

	// Symbol 16
	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(12.5,1,1).p("AhXBbICvi1");
	this.shape_11.setTransform(10.2,12.9);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(12.5,1,1).p("AhXBXQBXhcBYhR");
	this.shape_12.setTransform(10.2,13.2);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(12.5,1,1).p("AhXBNQBXhoBYgx");
	this.shape_13.setTransform(10.3,14.3);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(12.5,1,1).p("AhXA7QBUh5BbAE");
	this.shape_14.setTransform(10.3,16);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(12.5,1,1).p("AhYAwQBRiTBfBP");
	this.shape_15.setTransform(10.3,17.1);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(12.5,1,1).p("AhXA1QBTiEBcAi");
	this.shape_16.setTransform(10.3,16.7);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(12.5,1,1).p("AhXA+QBUh2BbgF");
	this.shape_17.setTransform(10.3,15.7);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#000000").ss(12.5,1,1).p("AhXBJQBWhsBZgk");
	this.shape_18.setTransform(10.3,14.7);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#000000").ss(12.5,1,1).p("AhXBRQBXhkBYg9");
	this.shape_19.setTransform(10.3,13.9);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#000000").ss(12.5,1,1).p("AhXBWQBXhdBYhO");
	this.shape_20.setTransform(10.2,13.3);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#000000").ss(12.5,1,1).p("AhXBaQBXhaBYhZ");
	this.shape_21.setTransform(10.2,13);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_11}]}).to({state:[{t:this.shape_11}]},19).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_15}]},2).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_11}]},1).wait(43));

	// Symbol 15
	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#000000").ss(12.5,1,1).p("AgbByQAvhpAIh6");
	this.shape_22.setTransform(3.9,11.5);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#000000").ss(12.5,1,1).p("AgaByQAuhpAHh6");
	this.shape_23.setTransform(3.8,11.5);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#000000").ss(12.5,1,1).p("AgZBxQAuhoAFh6");
	this.shape_24.setTransform(3.7,11.5);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f().s("#000000").ss(12.5,1,1).p("AgYBxQAvhpACh4");
	this.shape_25.setTransform(3.6,11.5);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f().s("#000000").ss(12.5,1,1).p("AgXBxQAvhpAAh4");
	this.shape_26.setTransform(3.4,11.6);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f().s("#000000").ss(12.5,1,1).p("AgXBxQAuhpABh4");
	this.shape_27.setTransform(3.5,11.6);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f().s("#000000").ss(12.5,1,1).p("AgYBxQAuhpADh4");
	this.shape_28.setTransform(3.6,11.5);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f().s("#000000").ss(12.5,1,1).p("AgZBxQAvhoAEh6");
	this.shape_29.setTransform(3.6,11.5);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f().s("#000000").ss(12.5,1,1).p("AgaByQAvhpAGh6");
	this.shape_30.setTransform(3.7,11.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_22}]}).to({state:[{t:this.shape_22}]},19).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_25,p:{x:3.6}}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_26}]},2).to({state:[{t:this.shape_27}]},1).to({state:[{t:this.shape_25,p:{x:3.5}}]},1).to({state:[{t:this.shape_28}]},1).to({state:[{t:this.shape_29}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_30}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_22}]},1).wait(39));
	this.timeline.addTween(cjs.Tween.get(this.shape_22).wait(21).to({x:3.8},0).to({_off:true},1).wait(13).to({_off:false},0).wait(1).to({x:3.9},0).wait(39));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-6.2,-6.2,31.6,39.7);


(lib.Symbol13 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(12.5,1,1).p("AAkg1IhHBr");
	this.shape.setTransform(3.7,5.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-6.2,-6.2,19.8,23.4);


(lib.Symbol11 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AguAvQgUgUAAgbQAAgbAUgTQATgUAbAAQAbAAAUAUQAUATAAAbQAAAbgUAUQgUAUgbAAQgbAAgTgUg");
	this.shape.setTransform(6.7,6.8);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,13.5,13.5);


(lib.Symbol10 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AguAvQgUgUAAgbQAAgbAUgTQATgUAbAAQAbAAAUAUQAUATAAAbQAAAbgUAUQgUAUgbAAQgbAAgTgUg");
	this.shape.setTransform(6.7,6.8);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,13.5,13.5);


(lib.Symbol9 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#B7B7B7").s().p("AiLCLQg6g5AAhSQAAhRA6g6QA6g6BRAAQBSAAA5A6QA7A6AABRQAABSg7A5Qg5A7hSAAQhRAAg6g7g");
	this.shape.setTransform(19.9,19.9);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,39.7,39.7);


(lib.Symbol7 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#B7B7B7").s().p("AiLCLQg6g5AAhSQAAhRA6g6QA6g6BRAAQBSAAA5A6QA7A6AABRQAABSg7A5Qg5A7hSAAQhRAAg6g7g");
	this.shape.setTransform(19.9,19.9);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,39.7,39.7);


(lib.Symbol5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(12.5,1,1).p("Ahfg8QCTgsAsCu");
	this.shape.setTransform(9.7,7);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-6.2,-6.2,31.9,26.6);


(lib.Symbol4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(12.5,1,1).p("Ah1AbQBlhmCGBc");
	this.shape.setTransform(11.8,2.7);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-6.2,-6.2,36.1,18);


(lib.Symbol2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#F51431").s().p("AAAAqQgQAAgNgMQgMgMAAgSQAAgQAMgMQANgNAQAAIADAAQAPABALAMQANAMAAAQQAAASgNAMQgLALgPABIgDAAg");
	this.shape.setTransform(4.3,4.3);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,8.5,8.5);


(lib.Symbol20 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_150 = function() {
		this.gotoAndPlay(75);
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(150).call(this.frame_150).wait(1));

	// Layer 4
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AgiAoQgPgRAAgXQAAgXAPgQQAPgQATgBQAUABAQAQQAOAQAAAXQAAAXgOARQgQAQgUAAQgTAAgPgQg");
	this.shape.setTransform(295.6,161.8);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(151));

	// Layer 5
	this.instance = new lib.Symbol22copy("synched",0,false);
	this.instance.setTransform(295.5,161.8,1,1,-35.7,0,0,2.4,133.9);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(24).to({startPosition:69},0).to({regX:2.5,rotation:90,x:295.6,startPosition:83},14).to({regX:2.4,rotation:180,y:161.7,startPosition:95},12).to({regX:2.5,rotation:270,x:295.5,startPosition:107},12).to({regX:2.4,rotation:324.3,y:161.8,startPosition:0},12).wait(25).to({startPosition:69},0).to({regX:2.5,rotation:450,x:295.6,startPosition:83},14).to({regX:2.4,rotation:540,y:161.7,startPosition:95},12).to({regX:2.5,rotation:630,x:295.5,startPosition:107},12).to({regX:2.4,rotation:684.3,y:161.8,startPosition:0},12).wait(2));

	// Layer 2
	this.instance_1 = new lib.Symbol25();
	this.instance_1.setTransform(295.5,161.8,1,1,0,0,0,1.4,71.7);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(41).to({rotation:2},3,cjs.Ease.get(-1)).to({regX:1.5,regY:71.8,rotation:-0.8,x:295.6,y:161.9},3).to({regX:1.4,regY:71.7,rotation:0,x:295.5,y:161.8},4).wait(65).to({rotation:2},3,cjs.Ease.get(-1)).to({regX:1.5,regY:71.8,rotation:-0.8,x:295.6,y:161.9},3).to({regX:1.4,regY:71.7,rotation:0,x:295.5,y:161.8},4).wait(25));

	// Layer 12
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("rgba(81,55,36,0.749)").s().p("AgiAoQgPgRAAgXQAAgXAPgQQAOgQAUAAQAUAAAQAQQAOAQAAAXQAAAXgOARQgQAQgUAAQgUAAgOgQg");
	this.shape_1.setTransform(293.6,162.8);

	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(151));

	// Layer 13
	this.instance_2 = new lib.Symbol22("synched",0,false);
	this.instance_2.setTransform(293.5,162.8,1,1,-35.7,0,0,2.4,133.9);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(24).to({startPosition:69},0).to({regX:2.5,rotation:90,x:293.6,startPosition:83},14).to({regX:2.4,rotation:180,y:162.7,startPosition:95},12).to({regX:2.5,rotation:270,x:293.5,startPosition:107},12).to({regX:2.4,rotation:324.3,y:162.8,startPosition:0},12).wait(25).to({startPosition:69},0).to({regX:2.5,rotation:450,x:293.6,startPosition:83},14).to({regX:2.4,rotation:540,y:162.7,startPosition:95},12).to({regX:2.5,rotation:630,x:293.5,startPosition:107},12).to({regX:2.4,rotation:684.3,y:162.8,startPosition:0},12).wait(2));

	// Layer 11
	this.instance_3 = new lib.Symbol25copy();
	this.instance_3.setTransform(292.5,162.8,1,1,0,0,0,1.4,71.7);

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(41).to({rotation:2},3,cjs.Ease.get(-1)).to({regX:1.5,regY:71.8,rotation:-0.8,x:292.6,y:162.9},3).to({regX:1.4,regY:71.7,rotation:0,x:292.5,y:162.8},4).wait(65).to({rotation:2},3,cjs.Ease.get(-1)).to({regX:1.5,regY:71.8,rotation:-0.8,x:292.6,y:162.9},3).to({regX:1.4,regY:71.7,rotation:0,x:292.5,y:162.8},4).wait(25));

	// Layer 8 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("AoJjMIQTgVIAAGWIwTAtg");
	mask.setTransform(293.3,204.5);

	// Layer 17
	this.instance_4 = new lib.Symbol27();
	this.instance_4.setTransform(295.5,160.5,1,1,0,0,0,24.5,19);

	this.instance_4.mask = mask;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(67).to({y:207.5},4).to({y:204.5},2,cjs.Ease.get(1)).wait(6).to({y:161.5},5,cjs.Ease.get(-1)).wait(16).to({y:160.5},0).wait(42).to({y:207.5},4).to({y:204.5},2,cjs.Ease.get(1)).wait(3));

	// Layer 16
	this.instance_5 = new lib.Symbol28();
	this.instance_5.setTransform(292.5,160.5,1,1,0,0,0,44,19.5);

	this.instance_5.mask = mask;

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(57).to({y:207.5},4).to({y:204.5},2,cjs.Ease.get(1)).wait(2).to({y:245.5},4).wait(13).to({y:202.5},2).to({y:160.5},2).wait(46).to({y:207.5},4).to({y:204.5},2,cjs.Ease.get(1)).wait(2).to({y:245.5},4).wait(7));

	// Layer 15
	this.instance_6 = new lib.Symbol29();
	this.instance_6.setTransform(295.5,160.5,1,1,0,0,0,36,19);

	this.instance_6.mask = mask;

	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(47).to({y:207.5},4).to({y:204.5},2,cjs.Ease.get(1)).wait(2).to({y:245.5},4).wait(25).to({y:202.5},2).to({y:160.5},2).wait(34).to({y:207.5},4).to({y:204.5},2,cjs.Ease.get(1)).wait(2).to({y:245.5},4).wait(17));

	// Layer 14
	this.instance_7 = new lib.Symbol30();
	this.instance_7.setTransform(294.5,162.5,1,1,0,0,0,41.5,22);

	this.instance_7.mask = mask;

	this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(37).to({y:209.5},4).to({y:206.5},2,cjs.Ease.get(1)).wait(2).to({y:247.5},4).wait(37).to({y:204.5},2).to({y:162.5},2).wait(22).to({y:209.5},4).to({y:206.5},2,cjs.Ease.get(1)).wait(2).to({y:247.5},4).wait(27));

	// Layer 7
	this.instance_8 = new lib.Symbol23();
	this.instance_8.setTransform(294,164,1,1,0,0,0,38,19.5);

	this.instance_8.mask = mask;

	this.timeline.addTween(cjs.Tween.get(this.instance_8).wait(27).to({y:207.5},4).to({y:204.5},2,cjs.Ease.get(1)).wait(2).to({y:245.5},4).wait(49).to({y:202.5},2).to({y:160.5},2).wait(8).to({y:164},0).wait(2).to({y:207.5},4).to({y:204.5},2,cjs.Ease.get(1)).wait(2).to({y:245.5},4).wait(37));

	// Layer 6
	this.instance_9 = new lib.Symbol24();
	this.instance_9.setTransform(293.5,204.5,1,1,0,0,0,42.5,19.5);

	this.instance_9.mask = mask;

	this.timeline.addTween(cjs.Tween.get(this.instance_9).wait(25).to({y:245},4).wait(61).to({y:202},4,cjs.Ease.get(1)).wait(6).to({y:204.5},0).to({y:245},4).wait(47));

	// Layer 1
	this.instance_10 = new lib.clock_1();

	this.timeline.addTween(cjs.Tween.get(this.instance_10).wait(151));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,426,343);


(lib.Symbol19 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.Symbol13();
	this.instance.setTransform(7.2,11.6,1,1,0,0,0,3.6,5.5);

	this.instance_1 = new lib.Symbol5();
	this.instance_1.setTransform(12.3,13.4,1,1,0,0,0,9.7,7);

	this.instance_2 = new lib.Symbol4();
	this.instance_2.setTransform(11.8,2.7,1,1,0,0,0,11.8,2.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_2},{t:this.instance_1},{t:this.instance}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-6.2,-6.2,36.1,33);


(lib.Symbol14 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 4
	this.instance = new lib.Symbol18();
	this.instance.setTransform(98.6,6.2,1,1,0,0,0,0,27.2);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(9).to({regX:-0.1,rotation:39.9,x:98.3,y:-13},10,cjs.Ease.get(1)).wait(13).to({regX:0,rotation:0,x:98.6,y:6.2},10,cjs.Ease.get(1)).wait(33));

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(12.5,1,1).p("AnyGbQGWAxDvjCQD6jKBmnj");
	this.shape.setTransform(50,42.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(12.5,1,1).p("AnxGrQGPAUDwjDQD6jKBqne");
	this.shape_1.setTransform(49.8,40.5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(12.5,1,1).p("AnvG3QGIgGDyjEQD5jKBsnZ");
	this.shape_2.setTransform(49.6,39.3);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(12.5,1,1).p("AnuHAQGDgcDyjFQD5jJBvnV");
	this.shape_3.setTransform(49.5,38.3);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(12.5,1,1).p("AnsHJQF9gwDzjGQD4jJBxnS");
	this.shape_4.setTransform(49.4,37.5);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(12.5,1,1).p("AnrHQQF5hBDzjHQD4jIBznP");
	this.shape_5.setTransform(49.3,36.8);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(12.5,1,1).p("AnqHVQF1hOD0jHQD4jHB0nN");
	this.shape_6.setTransform(49.2,36.2);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(12.5,1,1).p("AnqHaQFzhZD1jIQD3jHB2nL");
	this.shape_7.setTransform(49.1,35.8);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(12.5,1,1).p("AnpHdQFxhgD1jIQD2jHB3nK");
	this.shape_8.setTransform(49.1,35.5);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(12.5,1,1).p("AnpHfQFwhlD1jIQD3jHB3nJ");
	this.shape_9.setTransform(49,35.3);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(12.5,1,1).p("AnpHgQFwhnD0jIQD3jHB4nJ");
	this.shape_10.setTransform(49,35.2);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(12.5,1,1).p("AnrHTQF3hJD0jHQD3jHB1nO");
	this.shape_11.setTransform(49.2,36.4);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(12.5,1,1).p("AnuG/QGEgZDyjFQD4jKBvnV");
	this.shape_12.setTransform(49.5,38.5);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(12.5,1,1).p("AnwGvQGMAMDxjEQD5jJBrnd");
	this.shape_13.setTransform(49.8,40);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(12.5,1,1).p("AnxGoQGQAZDwjDQD6jJBpnf");
	this.shape_14.setTransform(49.8,40.7);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(12.5,1,1).p("AnyGjQGTAjDwjCQD6jKBonh");
	this.shape_15.setTransform(49.9,41.3);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(12.5,1,1).p("AnyGeQGVArDvjCQD6jKBnni");
	this.shape_16.setTransform(49.9,41.7);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(12.5,1,1).p("AnyGcQGVAvDvjCQD7jJBmnk");
	this.shape_17.setTransform(50,42);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},9).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_10}]},13).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape}]},1).wait(33));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-6.2,-27.2,130.1,117.7);


(lib.Symbol12 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Symbol 6 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("ABYCLQg5g5gBhSQABhRA5g6QA6g6BTAAQBSAAA6A6QA6A6AABRQAABSg6A5Qg6A7hSAAQhTAAg6g7g");
	mask.setTransform(42.8,18.4);

	// Symbol 7
	this.instance = new lib.Symbol7();
	this.instance.setTransform(66,58.8,1,1,0,0,0,19.9,19.9);

	this.instance.mask = mask;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1).to({y:54.4},0).wait(1).to({y:51.8},0).wait(1).to({y:49.9},0).wait(1).to({y:48.5},0).wait(1).to({y:47.4},0).wait(1).to({y:46.6},0).wait(1).to({y:46},0).wait(1).to({y:45.4},0).wait(1).to({y:45},0).wait(1).to({y:44.7},0).wait(1).to({y:44.5},0).wait(1).to({y:44.3},0).wait(1).to({y:44.1},0).wait(2).to({y:44},0).wait(37).to({x:67.8,y:46.5},0).to({y:58.8},8,cjs.Ease.get(1)).wait(15));

	// Symbol 8 (mask)
	var mask_1 = new cjs.Shape();
	mask_1._off = true;
	mask_1.graphics.p("AiLCLQg6g5AAhSQAAhRA6g6QA6g6BRAAQBSAAA5A6QA7A6AABRQAABSg7A5Qg5A7hSAAQhRAAg6g7g");
	mask_1.setTransform(19.9,19.9);

	// Symbol 9
	this.instance_1 = new lib.Symbol9();
	this.instance_1.setTransform(20.2,60.5,1,1,0,0,0,19.9,19.9);

	this.instance_1.mask = mask_1;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(1).to({y:56.2},0).wait(1).to({y:53.5},0).wait(1).to({y:51.7},0).wait(1).to({y:50.3},0).wait(1).to({y:49.2},0).wait(1).to({y:48.4},0).wait(1).to({y:47.7},0).wait(1).to({y:47.2},0).wait(1).to({y:46.8},0).wait(1).to({y:46.5},0).wait(1).to({y:46.2},0).wait(1).to({y:46},0).wait(1).to({y:45.9},0).wait(1).to({y:45.8},0).wait(38).to({x:22,y:48.3},0).to({y:60.5},8,cjs.Ease.get(1)).wait(15));

	// Symbol 11
	this.instance_2 = new lib.Symbol11();
	this.instance_2.setTransform(60.7,23.2,1,1,0,0,0,6.7,6.8);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(15).to({x:64.2,y:21.3},7,cjs.Ease.get(1)).wait(3).to({x:60.7,y:23.2},7,cjs.Ease.get(1)).wait(3).to({x:64.2,y:21.3},7,cjs.Ease.get(1)).wait(3).to({x:60.7,y:23.2},7,cjs.Ease.get(1)).wait(23));

	// Symbol 10
	this.instance_3 = new lib.Symbol10();
	this.instance_3.setTransform(12.5,24.2,1,1,0,0,0,6.7,6.8);

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(15).to({x:16,y:22.3},7,cjs.Ease.get(1)).wait(3).to({x:12.5,y:24.2},7,cjs.Ease.get(1)).wait(3).to({x:16,y:22.3},7,cjs.Ease.get(1)).wait(3).to({x:12.5,y:24.2},7,cjs.Ease.get(1)).wait(23));

	// Layer 8
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AiLCLQg6g5AAhSQAAhRA6g6QA6g6BRAAQBSAAA5A6QA7A6AABRQAABSg7A5Qg5A7hSAAQhRAAg6g7g");
	this.shape.setTransform(19.9,19.9);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(75));

	// Layer 9
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AiLCLQg6g5AAhSQAAhRA6g6QA6g6BRAAQBSAAA5A6QA7A6AABRQAABSg7A5Qg5A7hSAAQhRAAg6g7g");
	this.shape_1.setTransform(65.8,18.4);

	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(75));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,-1.4,85.6,41.2);


(lib.Symbol3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 4
	this.instance = new lib.Symbol19();
	this.instance.setTransform(66.6,117.8,1,1,0,0,0,0.1,6.2);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(9).to({rotation:45,x:40.4,y:134.4},15,cjs.Ease.get(1)).wait(25).to({rotation:0,x:66.6,y:117.8},15,cjs.Ease.get(1)).wait(11));

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(12.5,1,1).p("AhhpLQicErgwCgQg3C2AuB2QAtB1ClBlQCKBWEmBw");
	this.shape.setTransform(33.3,58.9);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(12.5,1,1).p("AhPpTQibErgxCgQg3C2AqB3QApB3CbBnQCEBZEbB4");
	this.shape_1.setTransform(31.4,59.7);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(12.5,1,1).p("Ag9pbQicErgwCgQg3C2AlB4QAmB4CSBpQB+BeEQB/");
	this.shape_2.setTransform(29.7,60.5);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(12.5,1,1).p("AgtpiQicErgwCgQg3C2AiB6QAhB5CKBrQB4BgEHCG");
	this.shape_3.setTransform(28.1,61.2);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(12.5,1,1).p("AgfppQibErgxCgQg3C2AfB7QAeB7CCBsQBzBkD/CM");
	this.shape_4.setTransform(26.6,61.8);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(12.5,1,1).p("AgRpvQibErgxCgQg3C2AcB8QAbB8B7BuQBuBmD3CS");
	this.shape_5.setTransform(25.2,62.4);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(12.5,1,1).p("AgFp0QibErgxCgQg3C2AZB9QAYB9B1BuQBqBqDwCW");
	this.shape_6.setTransform(24,63);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(12.5,1,1).p("AAEp5QiZErgxCgQg3C2AWB+QAWB9BvBxQBnBrDpCb");
	this.shape_7.setTransform(22.9,63.5);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(12.5,1,1).p("AAOp9QiZErgxCgQg3C2AUB+QAUB+BqByQBjBuDjCe");
	this.shape_8.setTransform(21.9,63.9);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(12.5,1,1).p("AAXqBQiaErgwCgQg3C2ASB/QARB/BmBzQBhBvDeCi");
	this.shape_9.setTransform(21.1,64.3);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(12.5,1,1).p("AAeqEQiZErgxCgQg3C4AQB+QARB/BiBzQBgByDYCk");
	this.shape_10.setTransform(20.3,64.6);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(12.5,1,1).p("AAkqHQiaErgwCgQg3C4APB+QAOCABgB0QBdByDVCo");
	this.shape_11.setTransform(19.8,64.8);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(12.5,1,1).p("AAoqJQiZErgxCgQg3C4AOB/QAOCABdB0QBcBzDSCq");
	this.shape_12.setTransform(19.3,65);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(12.5,1,1).p("AAsqKQiaErgwCgQg3C4ANB/QANCABbB1QBbB0DRCq");
	this.shape_13.setTransform(19,65.2);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(12.5,1,1).p("AAuqLQiaErgwCgQg3C4AMB/QANCABaB2QBbBzDPCs");
	this.shape_14.setTransform(18.8,65.3);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(12.5,1,1).p("AAuqLQiZErgxCgQg3C4ANB+QANCBBZB1QBaB1DPCr");
	this.shape_15.setTransform(18.7,65.3);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(12.5,1,1).p("AAbqDQiZErgxCgQg3C4ARB9QARB/BjB0QBhBwDaCk");
	this.shape_16.setTransform(20.6,64.5);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(12.5,1,1).p("AAKp7QiaErgwCgQg3C2AVB+QAUB+BtBxQBkBsDmCd");
	this.shape_17.setTransform(22.4,63.7);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#000000").ss(12.5,1,1).p("AgUpuQibErgxCgQg3C3AcB8QAcB7B8BtQBwBnD4CQ");
	this.shape_18.setTransform(25.5,62.3);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#000000").ss(12.5,1,1).p("AghpoQicErgwCgQg3C2AfB7QAfB6CDBsQB0BkEACL");
	this.shape_19.setTransform(26.9,61.7);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#000000").ss(12.5,1,1).p("Ag4pdQicErgwCgQg3C2AkB5QAkB4CQBqQB8BeENCB");
	this.shape_20.setTransform(29.2,60.7);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#000000").ss(12.5,1,1).p("AhCpZQibErgxCgQg3C2AnB4QAmB4CVBpQB/BcETB9");
	this.shape_21.setTransform(30.1,60.3);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#000000").ss(12.5,1,1).p("AhKpVQicErgwCgQg3C2AoB4QApB2CYBoQCDBaEYB6");
	this.shape_22.setTransform(31,59.9);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#000000").ss(12.5,1,1).p("AhRpSQicErgwCgQg3C2AqB3QAqB2CcBnQCFBZEcB3");
	this.shape_23.setTransform(31.7,59.6);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#000000").ss(12.5,1,1).p("AhXpQQibErgxCgQg3C3AsB3QArB1CfBmQCHBYEgB1");
	this.shape_24.setTransform(32.2,59.3);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f().s("#000000").ss(12.5,1,1).p("AhbpOQicErgwCgQg3C2AsB3QAtB2ChBlQCJBXEiBz");
	this.shape_25.setTransform(32.7,59.1);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f().s("#000000").ss(12.5,1,1).p("AhepMQicErgwCgQg3C2AtB2QAtB1CjBlQCKBWEkBy");
	this.shape_26.setTransform(33,59);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f().s("#000000").ss(12.5,1,1).p("AhhpLQibErgxCgQg3C2AuB2QAtB1ClBlQCKBWElBw");
	this.shape_27.setTransform(33.2,58.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},9).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_15}]},25).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_27}]},1).to({state:[{t:this.shape}]},1).wait(11));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-6.2,-6.2,102.6,144.5);


(lib.Symbol1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#1B1814").ss(11,1,1).p("AkKheQBCDCDQgFQBXgDBIgsQBKgsAahG");
	this.shape.setTransform(156,-52.6);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(75));

	// Layer 3
	this.instance = new lib.Symbol2();
	this.instance.setTransform(137.2,-49.4,2.017,1.042,0,46.1,51.9,5.4,7.5);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(12).to({_off:false},0).to({scaleY:2.02,guide:{path:[137.2,-49.2,137.5,-48.9,137.8,-48.7]}},4).wait(1).to({regX:4.3,regY:4.3,scaleX:2.02,scaleY:2.02,skewX:45.7,skewY:51.5,x:141.4,y:-54.8},0).wait(1).to({scaleX:2.01,scaleY:2.02,skewX:44.8,skewY:50.4,x:141.8,y:-54.4},0).wait(1).to({scaleX:2.01,scaleY:2.02,skewX:43.2,skewY:48.6,x:142.7,y:-53.7},0).wait(1).to({scaleX:2,scaleY:2.03,skewX:40.7,skewY:45.8,x:144.2,y:-52.8},0).wait(1).to({scaleX:2,scaleY:2.04,skewX:37.3,skewY:41.9,x:146.4,y:-51.7},0).wait(1).to({scaleX:1.98,scaleY:2.05,skewX:32.8,skewY:36.8,x:149.5,y:-50.7},0).wait(1).to({scaleX:1.97,scaleY:2.06,skewX:27.4,skewY:30.6,x:153.3,y:-50.1},0).wait(1).to({scaleX:1.96,scaleY:2.08,skewX:21.5,skewY:23.9,x:157.6},0).wait(1).to({scaleX:1.94,scaleY:2.09,skewX:16,skewY:17.6,x:161.1,y:-50.5},0).wait(1).to({scaleX:1.93,scaleY:2.1,skewX:11.4,skewY:12.4,x:164,y:-51.2},0).wait(1).to({scaleX:1.92,scaleY:2.11,skewX:8,skewY:8.6,x:166.1,y:-52},0).wait(1).to({scaleX:1.92,scaleY:2.11,skewX:5.8,skewY:6,x:167.5,y:-52.6},0).wait(1).to({scaleX:1.91,scaleY:2.12,skewX:4.5,skewY:4.6,x:168.2,y:-53},0).wait(1).to({regX:5.4,regY:7.5,scaleX:1.91,scaleY:2.12,rotation:4,skewX:0,skewY:0,x:170.2,y:-46.3},0).wait(7).to({regX:4.3,regY:4.3,scaleX:1.91,scaleY:2.12,rotation:0,skewX:4.6,skewY:4.7,x:168.2,y:-52.9},0).wait(1).to({scaleX:1.92,scaleY:2.11,skewX:6,skewY:6.2,x:167.3,y:-52.5},0).wait(1).to({scaleX:1.92,scaleY:2.11,skewX:8.3,skewY:8.8,x:165.8,y:-51.9},0).wait(1).to({scaleX:1.93,scaleY:2.1,skewX:11.5,skewY:12.6,x:163.6,y:-51.1},0).wait(1).to({scaleX:1.94,scaleY:2.09,skewX:15.7,skewY:17.4,x:160.6,y:-50.4},0).wait(1).to({scaleX:1.95,scaleY:2.08,skewX:20.7,skewY:23,x:157,y:-50},0).wait(1).to({scaleX:1.97,scaleY:2.06,skewX:26.1,skewY:29.1,x:153.3,y:-50.1},0).wait(1).to({scaleX:1.98,scaleY:2.05,skewX:31.2,skewY:35,x:150.1,y:-50.5},0).wait(1).to({scaleX:1.99,scaleY:2.04,skewX:35.9,skewY:40.2,x:147.3,y:-51.3},0).wait(1).to({scaleX:2,scaleY:2.03,skewX:39.6,skewY:44.5,x:145.1,y:-52.2},0).wait(1).to({scaleX:2.01,scaleY:2.03,skewX:42.5,skewY:47.7,x:143.3,y:-53.2},0).wait(1).to({scaleX:2.01,scaleY:2.02,skewX:44.4,skewY:50,x:142.1,y:-54.1},0).wait(1).to({scaleX:2.02,scaleY:2.02,skewX:45.6,skewY:51.3,x:141.5,y:-54.7},0).wait(1).to({regX:5.4,regY:7.5,scaleX:2.02,scaleY:2.02,skewX:46.1,skewY:51.9,x:137.9,y:-48.7},0).to({scaleY:0.61,guide:{path:[137.8,-48.6,137.8,-48.6,137.8,-48.6]}},4).to({_off:true},1).wait(20));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(123.8,-67.6,64.6,30.1);


(lib.Symbol2_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2
	this.instance = new lib.Symbol12();
	this.instance.setTransform(30.4,-14.4,1.1,1.1,0,0,-180,43.4,34.9);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	// Layer 1
	this.instance_1 = new lib.Symbol1();
	this.instance_1.setTransform(27.6,15.7,1.03,1.03,0,0,0,156,-52.6);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-16.2,-54.4,94.4,90);


// stage content:
(lib.Main_site = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 7
	this.instance = new lib.Symbol2_1();
	this.instance.setTransform(670.2,319.1,1,1,0,0,0,27.6,15.6);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	// Layer 6
	this.instance_1 = new lib.Symbol3();
	this.instance_1.setTransform(581.7,416,1,1,0,0,0,33.3,58.9);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(1));

	// Layer 4
	this.instance_2 = new lib.pack();
	this.instance_2.setTransform(441,127);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(1));

	// Layer 9
	this.instance_3 = new lib.Symbol14();
	this.instance_3.setTransform(740.8,351.2,1,1,0,0,0,50,42.1);

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(1));

	// Layer 2
	this.instance_4 = new lib.Symbol20();
	this.instance_4.setTransform(793,408.5,1,1,0,0,0,213,171.5);

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(1));

	// Layer 1
	this.instance_5 = new lib.bg();

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(540,340,1080,680);

})(lib = lib||{}, images = images||{}, createjs = createjs||{}, ss = ss||{});
var lib, images, createjs, ss;

$(function () {
    var canvas, stage, exportRoot;
    function init() {
        // --- write your JS code here ---

        createjs.MotionGuidePlugin.install();

        canvas = document.getElementById("canvas25");
        images = images||{};

        var loader = new createjs.LoadQueue(false);
        loader.addEventListener("fileload", handleFileLoad);
        loader.addEventListener("complete", handleComplete);
        loader.loadManifest(lib.properties.manifest);
    }

    function handleFileLoad(evt) {
        if (evt.item.type == "image") { images[evt.item.id] = evt.result; }
    }

    function handleComplete(evt) {
        exportRoot = new lib.Main_site();

        stage = new createjs.Stage(canvas);
        stage.addChild(exportRoot);
        stage.update();

        createjs.Ticker.setFPS(lib.properties.fps);
        createjs.Ticker.addEventListener("tick", stage);
    }
    init();
});