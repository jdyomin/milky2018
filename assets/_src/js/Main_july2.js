(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [];


// symbols:



(lib.bg_part1 = function() {
	this.initialize(img.bg_part1);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,369,271);


(lib.bg_part2 = function() {
	this.initialize(img.bg_part2);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,833,333);


(lib.glasses = function() {
	this.initialize(img.glasses);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,284,90);


(lib.pack = function() {
	this.initialize(img.pack);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,547,680);// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.Symbol7 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.bg_part2();
	this.instance.parent = this;
	this.instance.setTransform(-416.5,-166.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol7, new cjs.Rectangle(-416.5,-166.5,833,333), null);


(lib.Symbol4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.glasses();
	this.instance.parent = this;
	this.instance.setTransform(-142,-45);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol4, new cjs.Rectangle(-142,-45,284,90), null);


(lib.Symbol1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.bg_part1();
	this.instance.parent = this;
	this.instance.setTransform(-184.5,-135.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol1, new cjs.Rectangle(-184.5,-135.5,369,271), null);


(lib.Symbol_9_Layer_2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(12,1,1).p("Ag7CXQhWiKAyhSQAxhRCgAA");
	this.shape.setTransform(159.6059,55.525);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(12,1,1).p("Ag+CbQhOiGAzhSQAzhRCUgM");
	this.shape_1.setTransform(159.9879,55.05);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(12,1,1).p("AhCCgQhFiEA0hRQA0hRCJgZ");
	this.shape_2.setTransform(160.3657,54.55);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(12,1,1).p("AhGClQg9iCA2hRQA2hQB+gm");
	this.shape_3.setTransform(160.7072,54.075);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(12,1,1).p("AhJCqQg0iAA3hQQA3hQBzgz");
	this.shape_4.setTransform(161.043,53.575);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(12,1,1).p("AhKCTQg6iBBAhKQBAhKBrgQ");
	this.shape_5.setTransform(161.1713,55.925);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(12,1,1).p("AhMB+Qg/iBBJhEQBIhFBkAU");
	this.shape_6.setTransform(161.3158,58.0402);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(12,1,1).p("AhNBxQhEiBBSg/QBRg/BbA4");
	this.shape_7.setTransform(161.4161,59.3018);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(12,1,1).p("AhOBpQhKiCBbg5QBag5BTBc");
	this.shape_8.setTransform(161.5406,60.1347);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(12,1,1).p("AhLBuQhMiCBUg+QBTg9BgBN");
	this.shape_9.setTransform(161.245,59.5969);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(12,1,1).p("AhIB0QhOiEBOhCQBMhBBsA+");
	this.shape_10.setTransform(160.9437,59.005);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(12,1,1).p("AhFB7QhQiFBHhGQBGhFB5Au");
	this.shape_11.setTransform(160.6488,58.33);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(12,1,1).p("AhCCCQhSiGBAhKQA/hJCGAe");
	this.shape_12.setTransform(160.3144,57.5506);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(12,1,1).p("Ag+CMQhUiJA5hNQA4hNCSAP");
	this.shape_13.setTransform(159.9729,56.6451);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},9).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape}]},1).wait(53));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_9_Layer_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(12,1,1).p("ABWgDQipiMgCDL");
	this.shape.setTransform(164.975,59.0512);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(12,1,1).p("AhVA8QAIi/CjBy");
	this.shape_1.setTransform(165.025,59.0648);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(12,1,1).p("AhWA9QAPi0CeBY");
	this.shape_2.setTransform(165.075,59.0219);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(12,1,1).p("AhWA+QAVipCYA/");
	this.shape_3.setTransform(165.1,58.9116);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(12,1,1).p("ABYg4QiTglgcCd");
	this.shape_4.setTransform(165.15,58.6913);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(12,1,1).p("AhUBNQgDi5CsAn");
	this.shape_5.setTransform(164.8715,57.3877);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(12,1,1).p("AhPBaQgjjVDHAo");
	this.shape_6.setTransform(164.3574,56.0784);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(12,1,1).p("AhHBoQhDjyDhAq");
	this.shape_7.setTransform(163.584,54.7405);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(12,1,1).p("ABbhuQj8grBjEO");
	this.shape_8.setTransform(162.6808,53.4242);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(12,1,1).p("AhDBqQhSkDDuA8");
	this.shape_9.setTransform(163.176,54.5375);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(12,1,1).p("AhHBfQhCj3DgBL");
	this.shape_10.setTransform(163.6366,55.5703);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(12,1,1).p("AhMBWQgwjtDSBc");
	this.shape_11.setTransform(164.0718,56.5406);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(12,1,1).p("AhQBNQgfjiDEBs");
	this.shape_12.setTransform(164.4579,57.4315);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(12,1,1).p("AhTBEQgPjWC3B8");
	this.shape_13.setTransform(164.7683,58.2721);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},9).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape}]},1).wait(53));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_5_Layer_2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(16,1,1).p("Akgh8QAGBUBBBDQA+BBBbAXQBcAWBbgeQBkggBGhN");
	this.shape.setTransform(-48.15,58.0571);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(16,1,1).p("Akgh4QAGBUBCA/QBAA8BbAXQBbAWBbgZQBigcBGhO");
	this.shape_1.setTransform(-48.15,56.9523);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(16,1,1).p("Akgh0QAGBUBDA6QBBA5BbAWQBcAXBagWQBggXBGhO");
	this.shape_2.setTransform(-48.15,55.9753);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(16,1,1).p("AkghxQAGBUBEA2QBCA2BbAWQBbAXBagSQBfgUBGhO");
	this.shape_3.setTransform(-48.15,55.075);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(16,1,1).p("AkghuQAGBTBFA0QBDAyBbAXQBbAWBagPQBdgQBGhP");
	this.shape_4.setTransform(-48.15,54.3161);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(16,1,1).p("AkghsQAGBUBFAwQBEAwBcAWQBbAXBZgNQBcgNBGhO");
	this.shape_5.setTransform(-48.15,53.6732);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(16,1,1).p("AkghqQAGBUBGAtQBEAuBcAWQBbAXBZgKQBbgLBGhO");
	this.shape_6.setTransform(-48.15,53.1288);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(16,1,1).p("AkghoQAGBTBGAsQBFArBdAXQBaAWBZgIQBagIBGhP");
	this.shape_7.setTransform(-48.15,52.6799);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(16,1,1).p("AkghnQAGBTBGAqQBGArBcAWQBbAXBYgHQBagHBGhP");
	this.shape_8.setTransform(-48.15,52.3331);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(16,1,1).p("AkghmQAGBTBHApQBGApBcAXQBaAWBZgGQBZgFBGhP");
	this.shape_9.setTransform(-48.15,52.0882);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(16,1,1).p("AkghmQAGBTBHAoQBGApBcAXQBbAWBYgFQBZgFBGhP");
	this.shape_10.setTransform(-48.15,51.954);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(16,1,1).p("AkghmQAGBUBHAnQBGApBcAXQBbAWBYgFQBZgFBGhO");
	this.shape_11.setTransform(-48.15,51.8955);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(16,1,1).p("AkghpQAGBTBGAtQBFAtBcAWQBaAXBagKQBagJBGhP");
	this.shape_12.setTransform(-48.15,52.916);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(16,1,1).p("AkghtQAGBUBFAxQBEAxBbAWQBcAXBZgOQBcgOBGhO");
	this.shape_13.setTransform(-48.15,53.8531);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(16,1,1).p("AkghvQAGBTBFA1QBCA0BcAXQBaAWBagRQBegRBGhP");
	this.shape_14.setTransform(-48.15,54.7408);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(16,1,1).p("AkghyQAGBTBDA4QBCA3BbAXQBcAWBagTQBfgVBGhP");
	this.shape_15.setTransform(-48.15,55.5027);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(16,1,1).p("Akgh1QAGBUBDA7QBAA6BcAWQBbAXBbgXQBggYBGhO");
	this.shape_16.setTransform(-48.15,56.1625);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(16,1,1).p("Akgh3QAGBUBCA+QBAA8BcAWQBbAXBbgZQBhgbBGhO");
	this.shape_17.setTransform(-48.15,56.7329);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#000000").ss(16,1,1).p("Akgh5QAGBUBCBAQA/A+BbAWQBcAXBbgbQBigdBGhO");
	this.shape_18.setTransform(-48.15,57.2114);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#000000").ss(16,1,1).p("Akgh6QAGBUBCBBQA+A/BbAXQBcAWBbgcQBjgeBGhN");
	this.shape_19.setTransform(-48.15,57.5762);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#000000").ss(16,1,1).p("Akgh7QAGBUBBBDQA/BABbAWQBcAXBbgdQBjggBGhN");
	this.shape_20.setTransform(-48.15,57.8415);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#000000").ss(16,1,1).p("Akgh7QAGBTBBBEQA+BABbAXQBcAWBbgdQBkggBGhO");
	this.shape_21.setTransform(-48.15,57.9868);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},29).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_11}]},38).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_3_Layer_3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_3
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(16,1,1).p("AidBhQFGATgLjW");
	this.shape.setTransform(15.643,23.8283);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(16,1,1).p("AieBkQE6AIADjP");
	this.shape_1.setTransform(15.725,23.5648);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(16,1,1).p("AifBlQEwAAAPjJ");
	this.shape_2.setTransform(15.825,23.4);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(16,1,1).p("AigBnQEpgHAYjF");
	this.shape_3.setTransform(15.875,23.3);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(16,1,1).p("AigBnQElgLAcjC");
	this.shape_4.setTransform(15.925,23.225);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(16,1,1).p("AigBnQEjgMAejC");
	this.shape_5.setTransform(15.925,23.2);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(16,1,1).p("AieBjQFBAOgEjT");
	this.shape_6.setTransform(15.6781,23.6934);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(16,1,1).p("AidBiQFFASgKjW");
	this.shape_7.setTransform(15.6385,23.7947);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},49).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_5}]},16).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape}]},1).wait(15));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_3_Layer_2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(16,1,1).p("AiFCXQALmeEACa");
	this.shape.setTransform(15.325,10.3531);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(16,1,1).p("AiHCVQALmeEECj");
	this.shape_1.setTransform(15.55,10.5915);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(16,1,1).p("AiJCTQALmeEHCq");
	this.shape_2.setTransform(15.7,10.7696);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(16,1,1).p("AiKCSQALmeEKCv");
	this.shape_3.setTransform(15.825,10.8942);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(16,1,1).p("AiLCRQALmeEMCy");
	this.shape_4.setTransform(15.9,10.968);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(16,1,1).p("AiLCRQALmfEMC0");
	this.shape_5.setTransform(15.925,10.9892);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(16,1,1).p("AiGCWQALmeECCe");
	this.shape_6.setTransform(15.425,10.4617);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(16,1,1).p("AiFCXQALmeEACb");
	this.shape_7.setTransform(15.35,10.3826);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},49).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_5}]},16).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape}]},1).wait(15));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_3_Layer_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(16,1,1).p("AgFD3QkBlPF0ie");
	this.shape.setTransform(0.008,0);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(16,1,1).p("AAAD9QkAlPFqiq");
	this.shape_1.setTransform(-0.5765,-0.575);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(16,1,1).p("AAFEBQkAlOFiiz");
	this.shape_2.setTransform(-1.0168,-1.025);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(16,1,1).p("AAIEFQkAlPFdi6");
	this.shape_3.setTransform(-1.3406,-1.35);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(16,1,1).p("AAKEGQkAlOFZi9");
	this.shape_4.setTransform(-1.5472,-1.525);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(16,1,1).p("AALEHQkAlPFYi+");
	this.shape_5.setTransform(-1.6062,-1.6);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(16,1,1).p("AgCD6QkBlPFwik");
	this.shape_6.setTransform(-0.2547,-0.25);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(16,1,1).p("AgED4QkBlPFzig");
	this.shape_7.setTransform(-0.0503,-0.075);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},49).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_5}]},16).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape}]},1).wait(15));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_2_Layer_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(16,1,1).p("AIQGuQkHAljWgoQjFgliOiFQiGh9g9i7Qg9i6AZjN");
	this.shape.setTransform(-0.0031,-0.0042);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(16,1,1).p("AoFnEQgYDSA9C4QA/C4CECBQCKCHDFAoQDUAqEHgi");
	this.shape_1.setTransform(0.3632,-0.8271);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(16,1,1).p("AoCnJQgYDWA+C2QA/C1CDCFQCHCJDFArQDRAsEHgg");
	this.shape_2.setTransform(0.6921,-1.5641);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(16,1,1).p("AoAnOQgYDaBAC0QBAC0CACHQCFCMDFAtQDPAuEHgf");
	this.shape_3.setTransform(0.9727,-2.201);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(16,1,1).p("An+nSQgYDdBBCyQBACyB/CKQCCCNDGAwQDNAwEHge");
	this.shape_4.setTransform(1.2362,-2.7731);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(16,1,1).p("An9nWQgXDhBBCwQBBCwB+CNQCACPDFAxQDMAxEHgd");
	this.shape_5.setTransform(1.4647,-3.2643);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(16,1,1).p("An8nZQgXDjBCCvQBCCvB8CPQB/CQDFAyQDKAzEHgc");
	this.shape_6.setTransform(1.6528,-3.6756);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(16,1,1).p("An7nbQgXDkBDCvQBCCtB8CRQB8CRDGAzQDJA0EHgb");
	this.shape_7.setTransform(1.8045,-4.0136);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(16,1,1).p("An5ndQgYDmBDCuQBDCsB7CSQB7CSDGA0QDIA1EHga");
	this.shape_8.setTransform(1.9199,-4.2694);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(16,1,1).p("An5nfQgXDoBDCtQBDCsB6CSQB7CTDGA1QDHA1EIga");
	this.shape_9.setTransform(2.0311,-4.4722);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(16,1,1).p("An5ngQgXDoBECtQBDCsB6CTQB6CTDGA1QDHA2EHga");
	this.shape_10.setTransform(2.0577,-4.5884);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(16,1,1).p("AH/HZQkHAZjHg2QjGg1h6iTQh6iUhDirQhDitAXjo");
	this.shape_11.setTransform(2.0816,-4.6153);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(16,1,1).p("An7naQgXDjBCCwQBCCuB8CPQB+CRDFAzQDKAzEHgc");
	this.shape_12.setTransform(1.7182,-3.8183);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(16,1,1).p("An9nVQgYDfBCCyQBBCwB+CMQCACODGAxQDMAxEHge");
	this.shape_13.setTransform(1.3897,-3.0957);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(16,1,1).p("AoAnQQgXDcBACzQBACyCACJQCDCMDGAuQDOAwEHgf");
	this.shape_14.setTransform(1.1094,-2.4629);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(16,1,1).p("AoBnMQgYDYA/C1QBAC0CBCHQCGCKDFAsQDQAtEHgg");
	this.shape_15.setTransform(0.8459,-1.8894);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(16,1,1).p("AoDnIQgYDVA+C2QA/C2CDCEQCICJDFAqQDSAsEHgh");
	this.shape_16.setTransform(0.6171,-1.3929);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(16,1,1).p("AoEnFQgZDTA+C3QA/C4CECBQCJCIDFAoQDTArEHgi");
	this.shape_17.setTransform(0.428,-0.9744);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#000000").ss(16,1,1).p("AoFnDQgZDRA+C5QA9C4CFCAQCMCHDEAnQDVAqEGgj");
	this.shape_18.setTransform(0.276,-0.6243);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#000000").ss(16,1,1).p("AoHnBQgYDQA9C5QA+C5CFB/QCNCGDEAmQDWApEGgk");
	this.shape_19.setTransform(0.1612,-0.3584);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#000000").ss(16,1,1).p("AoHm/QgYDOA9C6QA9C6CGB+QCNCFDFAlQDWApEGgk");
	this.shape_20.setTransform(0.049,-0.1529);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#000000").ss(16,1,1).p("AoHm/QgZDOA9C6QA9C6CGB+QCOCEDEAmQDXAoEGgl");
	this.shape_21.setTransform(0.0219,-0.0292);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},36).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_11}]},31).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_1_Layer_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(12,1,1).p("Aiem7QiqEMgFClQgFClBVB6QBUB6CeAjQCoAmCxhl");
	this.shape.setTransform(647.3498,395.0444);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(12,1,1).p("AitmzQinERACCmQACCnBaB1QBaB0CcAaQCpAdCqhu");
	this.shape_1.setTransform(648.8488,394.2182);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(12,1,1).p("Ai8mrQikEWAKCoQAJCoBfBvQBfBvCcARQCoATCjh3");
	this.shape_2.setTransform(650.299,393.4495);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(12,1,1).p("AjKmlQihEcAQCpQAQCpBkBqQBlBqCbAIQCoAJCdiA");
	this.shape_3.setTransform(651.7277,392.7546);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(12,1,1).p("AjYmeQifEhAYCqQAWCrBqBkQBqBkCbgBQCnAACWiK");
	this.shape_4.setTransform(653.1233,392.1255);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(12,1,1).p("AjmmZQicEnAeCrQAeCsBvBfQBvBfCbgKQCngLCPiS");
	this.shape_5.setTransform(654.5074,391.5729);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(12,1,1).p("AjzmUQiZEsAlCtQAlCtB0BaQB0BZCbgTQCmgUCJic");
	this.shape_6.setTransform(655.8397,391.0994);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(12,1,1).p("AkBmQQiWExAsCuQArCvB6BUQB6BUCagcQCmgeCCik");
	this.shape_7.setTransform(657.201,390.686);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(12,1,1).p("AkOmMQiTE2AyCvQAzCwB/BPQB/BPCagmQCmgnB7iu");
	this.shape_8.setTransform(658.4919,390.3299);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(12,1,1).p("Aj1mTQiZEsAmCtQAmCtB1BaQB1BYCagUQCngWCHic");
	this.shape_9.setTransform(656.021,391.0428);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(12,1,1).p("AjfmbQidEjAaCrQAbCsBsBiQBsBhCbgGQCogFCSiO");
	this.shape_10.setTransform(653.8086,391.8398);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(12,1,1).p("AjMmkQihEdASCpQAQCpBlBqQBlBoCcAHQCoAICbiB");
	this.shape_11.setTransform(651.8892,392.6706);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(12,1,1).p("AivmyQinESAECmQACCnBbB0QBaB0CdAZQCoAbCphv");
	this.shape_12.setTransform(649.0214,394.1141);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(12,1,1).p("Aimm3QioEOgBCnQgCClBXB4QBXB2CeAfQCoAhCthp");
	this.shape_13.setTransform(648.1237,394.6181);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(12,1,1).p("Aigm6QipEMgECnQgECkBVB6QBVB4CeAjQCoAlCwhn");
	this.shape_14.setTransform(647.541,394.9407);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_8}]},28).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape}]},1).wait(45));

}).prototype = p = new cjs.MovieClip();


(lib.Scene_1_Layer_9 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_9
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("EhUXA1IMAAAhqPMCovAAAMAAABqPg");
	this.shape.setTransform(540,340);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Scene_1_Layer_9, null, null);


(lib.Symbol9 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_89 = function() {
		this.___loopingOver___ = true;
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(89).call(this.frame_89).wait(1));

	// Layer_1_obj_
	this.Layer_1 = new lib.Symbol_9_Layer_1();
	this.Layer_1.name = "Layer_1";
	this.Layer_1.parent = this;
	this.Layer_1.setTransform(165,59.1,1,1,0,0,0,165,59.1);
	this.Layer_1.depth = 0;
	this.Layer_1.isAttachedToCamera = 0
	this.Layer_1.isAttachedToMask = 0
	this.Layer_1.layerDepth = 0
	this.Layer_1.layerIndex = 0
	this.Layer_1.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_1).wait(90));

	// Layer_2_obj_
	this.Layer_2 = new lib.Symbol_9_Layer_2();
	this.Layer_2.name = "Layer_2";
	this.Layer_2.parent = this;
	this.Layer_2.setTransform(159.6,55.5,1,1,0,0,0,159.6,55.5);
	this.Layer_2.depth = 0;
	this.Layer_2.isAttachedToCamera = 0
	this.Layer_2.isAttachedToMask = 0
	this.Layer_2.layerDepth = 0
	this.Layer_2.layerIndex = 1
	this.Layer_2.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_2).wait(90));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(142.2,30.6,37.70000000000002,45.99999999999999);


(lib.Symbol3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_89 = function() {
		this.___loopingOver___ = true;
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(89).call(this.frame_89).wait(1));

	// Layer_3_obj_
	this.Layer_3 = new lib.Symbol_3_Layer_3();
	this.Layer_3.name = "Layer_3";
	this.Layer_3.parent = this;
	this.Layer_3.setTransform(15.7,23.8,1,1,0,0,0,15.7,23.8);
	this.Layer_3.depth = 0;
	this.Layer_3.isAttachedToCamera = 0
	this.Layer_3.isAttachedToMask = 0
	this.Layer_3.layerDepth = 0
	this.Layer_3.layerIndex = 0
	this.Layer_3.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_3).wait(90));

	// Layer_2_obj_
	this.Layer_2 = new lib.Symbol_3_Layer_2();
	this.Layer_2.name = "Layer_2";
	this.Layer_2.parent = this;
	this.Layer_2.setTransform(15.3,10.3,1,1,0,0,0,15.3,10.3);
	this.Layer_2.depth = 0;
	this.Layer_2.isAttachedToCamera = 0
	this.Layer_2.isAttachedToMask = 0
	this.Layer_2.layerDepth = 0
	this.Layer_2.layerIndex = 1
	this.Layer_2.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_2).wait(90));

	// Layer_1_obj_
	this.Layer_1 = new lib.Symbol_3_Layer_1();
	this.Layer_1.name = "Layer_1";
	this.Layer_1.parent = this;
	this.Layer_1.depth = 0;
	this.Layer_1.isAttachedToCamera = 0
	this.Layer_1.isAttachedToMask = 0
	this.Layer_1.layerDepth = 0
	this.Layer_1.layerIndex = 2
	this.Layer_1.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_1).wait(90));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-19.5,-35.9,59.6,77.6);


(lib.Symbol_5_Layer_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Symbol4();
	this.instance.parent = this;
	this.instance.setTransform(106,17,1,1,0,0,0,106,17);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(29).to({regX:106.1,regY:16.9,scaleX:0.9973,scaleY:1.0056,skewX:-0.0774,skewY:4.2792,x:106.1,y:17.05},11,cjs.Ease.get(1)).wait(38).to({regX:106,regY:17,scaleX:1,scaleY:1,skewX:0,skewY:0,x:106,y:17},11,cjs.Ease.get(1)).wait(1));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_2_Layer_2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.instance = new lib.Symbol3();
	this.instance.parent = this;
	this.instance.setTransform(-52.65,-36.45,1,1,0,0,0,0.1,34.6);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(36).to({x:-48.65,y:-46.45},11,cjs.Ease.get(1)).wait(2).to({regX:0,rotation:35.19,x:-49.1},6,cjs.Ease.get(-0.5)).to({regX:0.1,rotation:0,x:-48.65},5,cjs.Ease.get(1)).wait(2).to({regX:0,rotation:35.19,x:-49.1},6,cjs.Ease.get(-0.5)).to({regX:0.1,rotation:0,x:-48.65},5,cjs.Ease.get(1)).wait(5).to({x:-52.65,y:-36.45},11,cjs.Ease.get(1)).wait(1));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_1_Layer_2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.instance = new lib.Symbol9();
	this.instance.parent = this;
	this.instance.setTransform(675.25,432.8,1,1,29.9992,0,0,153.8,70.6);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1).to({regX:153.7,rotation:0,x:693.6,y:410.55},8).wait(28).to({regX:153.8,rotation:29.9992,x:675.25,y:432.8},8,cjs.Ease.get(1)).wait(45));

}).prototype = p = new cjs.MovieClip();


(lib.Scene_1_Layer_8 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_8
	this.bg3 = new lib.Symbol7();
	this.bg3.name = "bg3";
	this.bg3.parent = this;
	this.bg3.setTransform(761,680,1,1,0,0,0,-1.5,8.5);

	this.timeline.addTween(cjs.Tween.get(this.bg3).wait(1));

}).prototype = getMCSymbolPrototype(lib.Scene_1_Layer_8, null, null);


(lib.Scene_1_Layer_3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_3
	this.bg1 = new lib.Symbol1();
	this.bg1.name = "bg1";
	this.bg1.parent = this;
	this.bg1.setTransform(0,568.9,0.6895,0.6895,-11.5618,0,0,-33,-5.4);

	this.timeline.addTween(cjs.Tween.get(this.bg1).wait(1));

}).prototype = getMCSymbolPrototype(lib.Scene_1_Layer_3, null, null);


(lib.Scene_1_Layer_2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.bg2 = new lib.Symbol1();
	this.bg2.name = "bg2";
	this.bg2.parent = this;
	this.bg2.setTransform(1080,238.5,1,1,0,0,0,24.5,-3);

	this.timeline.addTween(cjs.Tween.get(this.bg2).wait(1));

}).prototype = getMCSymbolPrototype(lib.Scene_1_Layer_2, null, null);


(lib.Symbol1_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_89 = function() {
		this.___loopingOver___ = true;
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(89).call(this.frame_89).wait(1));

	// Layer_1_obj_
	this.Layer_1 = new lib.Symbol_1_Layer_1();
	this.Layer_1.name = "Layer_1";
	this.Layer_1.parent = this;
	this.Layer_1.setTransform(647.4,395.1,1,1,0,0,0,647.4,395.1);
	this.Layer_1.depth = 0;
	this.Layer_1.isAttachedToCamera = 0
	this.Layer_1.isAttachedToMask = 0
	this.Layer_1.layerDepth = 0
	this.Layer_1.layerIndex = 0
	this.Layer_1.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_1).wait(90));

	// Layer_2_obj_
	this.Layer_2 = new lib.Symbol_1_Layer_2();
	this.Layer_2.name = "Layer_2";
	this.Layer_2.parent = this;
	this.Layer_2.setTransform(688.9,423.3,1,1,0,0,0,688.9,423.3);
	this.Layer_2.depth = 0;
	this.Layer_2.isAttachedToCamera = 0
	this.Layer_2.isAttachedToMask = 0
	this.Layer_2.layerDepth = 0
	this.Layer_2.layerIndex = 1
	this.Layer_2.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_2).wait(90));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(608,344.6,111.5,105);


(lib.Symbol5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_89 = function() {
		this.___loopingOver___ = true;
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(89).call(this.frame_89).wait(1));

	// Layer_2_obj_
	this.Layer_2 = new lib.Symbol_5_Layer_2();
	this.Layer_2.name = "Layer_2";
	this.Layer_2.parent = this;
	this.Layer_2.setTransform(-48.1,58.1,1,1,0,0,0,-48.1,58.1);
	this.Layer_2.depth = 0;
	this.Layer_2.isAttachedToCamera = 0
	this.Layer_2.isAttachedToMask = 0
	this.Layer_2.layerDepth = 0
	this.Layer_2.layerIndex = 0
	this.Layer_2.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_2).wait(90));

	// Layer_1_obj_
	this.Layer_1 = new lib.Symbol_5_Layer_1();
	this.Layer_1.name = "Layer_1";
	this.Layer_1.parent = this;
	this.Layer_1.depth = 0;
	this.Layer_1.isAttachedToCamera = 0
	this.Layer_1.isAttachedToMask = 0
	this.Layer_1.layerDepth = 0
	this.Layer_1.layerIndex = 1
	this.Layer_1.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_1).wait(90));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-142,-63.6,284.1,142.1);


(lib.Symbol2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_89 = function() {
		this.___loopingOver___ = true;
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(89).call(this.frame_89).wait(1));

	// Layer_2_obj_
	this.Layer_2 = new lib.Symbol_2_Layer_2();
	this.Layer_2.name = "Layer_2";
	this.Layer_2.parent = this;
	this.Layer_2.setTransform(-42.6,-66.5,1,1,0,0,0,-42.6,-66.5);
	this.Layer_2.depth = 0;
	this.Layer_2.isAttachedToCamera = 0
	this.Layer_2.isAttachedToMask = 0
	this.Layer_2.layerDepth = 0
	this.Layer_2.layerIndex = 0
	this.Layer_2.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_2).wait(90));

	// Layer_1_obj_
	this.Layer_1 = new lib.Symbol_2_Layer_1();
	this.Layer_1.name = "Layer_1";
	this.Layer_1.parent = this;
	this.Layer_1.depth = 0;
	this.Layer_1.isAttachedToCamera = 0
	this.Layer_1.isAttachedToMask = 0
	this.Layer_1.layerDepth = 0
	this.Layer_1.layerIndex = 1
	this.Layer_1.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_1).wait(90));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-71.8,-113.7,133,166.5);


(lib.Symbol6 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Symbol1_1();
	this.instance.parent = this;
	this.instance.setTransform(-64.6,241.35,1.2681,1.268,0,32.8253,-147.1735,654.3,405.2);

	this.instance_1 = new lib.Symbol5();
	this.instance_1.parent = this;
	this.instance_1.setTransform(-91.05,90);

	this.instance_2 = new lib.pack();
	this.instance_2.parent = this;
	this.instance_2.setTransform(-246.05,-340);

	this.instance_3 = new lib.Symbol2();
	this.instance_3.parent = this;
	this.instance_3.setTransform(-237.15,157.75);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_3},{t:this.instance_2},{t:this.instance_1},{t:this.instance}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol6, new cjs.Rectangle(-308.9,-340,609.9,680), null);


(lib.Scene_1_Layer_6 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_6
	this.pack1 = new lib.Symbol6();
	this.pack1.name = "pack1";
	this.pack1.parent = this;
	this.pack1.setTransform(779.05,680,1,1,0,0,0,0,340);

	this.timeline.addTween(cjs.Tween.get(this.pack1).wait(1));

}).prototype = getMCSymbolPrototype(lib.Scene_1_Layer_6, null, null);


// stage content:
(lib.Main_july2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	this.___GetDepth___ = function(obj) {
		var depth = obj.depth;
		var cameraObj = this.___camera___instance;
		if(cameraObj && cameraObj.depth && obj.isAttachedToCamera)
		{
			depth += depth + cameraObj.depth;
		}
		return depth;
		}
	this.___needSorting___ = function() {
		for (var i = 0; i < this.getNumChildren() - 1; i++)
		{
			var prevDepth = this.___GetDepth___(this.getChildAt(i));
			var nextDepth = this.___GetDepth___(this.getChildAt(i + 1));
			if (prevDepth < nextDepth)
				return true;
		}
		return false;
	}
	this.___sortFunction___ = function(obj1, obj2) {
		return (this.exportRoot.___GetDepth___(obj2) - this.exportRoot.___GetDepth___(obj1));
	}
	this.on('tick', function (event){
		var curTimeline = event.currentTarget;
		if (curTimeline.___needSorting___()){
			this.sortChildren(curTimeline.___sortFunction___);
		}
	});

	// timeline functions:
	this.frame_0 = function() {
		this.pack1 = this.Layer_6.pack1;
		this.bg1 = this.Layer_3.bg1;
		this.bg2 = this.Layer_2.bg2;
		this.bg3 = this.Layer_8.bg3;
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(1));

	// Layer_6_obj_
	this.Layer_6 = new lib.Scene_1_Layer_6();
	this.Layer_6.name = "Layer_6";
	this.Layer_6.parent = this;
	this.Layer_6.setTransform(775.1,340,1,1,0,0,0,775.1,340);
	this.Layer_6.depth = 0;
	this.Layer_6.isAttachedToCamera = 0
	this.Layer_6.isAttachedToMask = 0
	this.Layer_6.layerDepth = 0
	this.Layer_6.layerIndex = 0
	this.Layer_6.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_6).wait(1));

	// Layer_3_obj_
	this.Layer_3 = new lib.Scene_1_Layer_3();
	this.Layer_3.name = "Layer_3";
	this.Layer_3.parent = this;
	this.Layer_3.setTransform(23.1,568,1,1,0,0,0,23.1,568);
	this.Layer_3.depth = 0;
	this.Layer_3.isAttachedToCamera = 0
	this.Layer_3.isAttachedToMask = 0
	this.Layer_3.layerDepth = 0
	this.Layer_3.layerIndex = 1
	this.Layer_3.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_3).wait(1));

	// Layer_2_obj_
	this.Layer_2 = new lib.Scene_1_Layer_2();
	this.Layer_2.name = "Layer_2";
	this.Layer_2.parent = this;
	this.Layer_2.setTransform(1055.5,241.5,1,1,0,0,0,1055.5,241.5);
	this.Layer_2.depth = 0;
	this.Layer_2.isAttachedToCamera = 0
	this.Layer_2.isAttachedToMask = 0
	this.Layer_2.layerDepth = 0
	this.Layer_2.layerIndex = 2
	this.Layer_2.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_2).wait(1));

	// Layer_8_obj_
	this.Layer_8 = new lib.Scene_1_Layer_8();
	this.Layer_8.name = "Layer_8";
	this.Layer_8.parent = this;
	this.Layer_8.setTransform(762.5,671.5,1,1,0,0,0,762.5,671.5);
	this.Layer_8.depth = 0;
	this.Layer_8.isAttachedToCamera = 0
	this.Layer_8.isAttachedToMask = 0
	this.Layer_8.layerDepth = 0
	this.Layer_8.layerIndex = 3
	this.Layer_8.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_8).wait(1));

	// Layer_9_obj_
	this.Layer_9 = new lib.Scene_1_Layer_9();
	this.Layer_9.name = "Layer_9";
	this.Layer_9.parent = this;
	this.Layer_9.setTransform(540,340,1,1,0,0,0,540,340);
	this.Layer_9.depth = 0;
	this.Layer_9.isAttachedToCamera = 0
	this.Layer_9.isAttachedToMask = 0
	this.Layer_9.layerDepth = 0
	this.Layer_9.layerIndex = 4
	this.Layer_9.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_9).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(419.7,340,820.3,498);
// library properties:
lib.properties = {
	id: 'A7C51EFBC7C3F74A9B966B7FC30EF222',
	width: 1080,
	height: 680,
	fps: 24,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: [
		{src:"assets/images/bg_part149.jpg", id:"bg_part1"},
		{src:"assets/images/bg_part249.jpg", id:"bg_part2"},
		{src:"assets/images/glasses49.png", id:"glasses"},
		{src:"assets/images/pack49.png", id:"pack"}
	],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['A7C51EFBC7C3F74A9B966B7FC30EF222'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}


// Layer depth API : 

AdobeAn.Layer = new function() {
	this.getLayerZDepth = function(timeline, layerName)
	{
		if(layerName === "Camera")
		layerName = "___camera___instance";
		var script = "if(timeline." + layerName + ") timeline." + layerName + ".depth; else 0;";
		return eval(script);
	}
	this.setLayerZDepth = function(timeline, layerName, zDepth)
	{
		const MAX_zDepth = 10000;
		const MIN_zDepth = -5000;
		if(zDepth > MAX_zDepth)
			zDepth = MAX_zDepth;
		else if(zDepth < MIN_zDepth)
			zDepth = MIN_zDepth;
		if(layerName === "Camera")
		layerName = "___camera___instance";
		var script = "if(timeline." + layerName + ") timeline." + layerName + ".depth = " + zDepth + ";";
		eval(script);
	}
	this.removeLayer = function(timeline, layerName)
	{
		if(layerName === "Camera")
		layerName = "___camera___instance";
		var script = "if(timeline." + layerName + ") timeline.removeChild(timeline." + layerName + ");";
		eval(script);
	}
	this.addNewLayer = function(timeline, layerName, zDepth)
	{
		if(layerName === "Camera")
		layerName = "___camera___instance";
		zDepth = typeof zDepth !== 'undefined' ? zDepth : 0;
		var layer = new createjs.MovieClip();
		layer.name = layerName;
		layer.depth = zDepth;
		layer.layerIndex = 0;
		timeline.addChild(layer);
	}
}


})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;

$(function () {
    var canvas, stage, exportRoot, anim_container, dom_overlay_container, fnStartAnimation;
    function init() {
        canvas = document.getElementById("canvas49");
        anim_container = document.getElementById("animation_container49");
        dom_overlay_container = document.getElementById("dom_overlay_container49");
        var comp=AdobeAn.getComposition("A7C51EFBC7C3F74A9B966B7FC30EF222");
        var lib=comp.getLibrary();
        var loader = new createjs.LoadQueue(false);
        loader.addEventListener("fileload", function(evt){handleFileLoad(evt,comp)});
        loader.addEventListener("complete", function(evt){handleComplete(evt,comp)});
        var lib=comp.getLibrary();
        loader.loadManifest(lib.properties.manifest);
    }
    function handleFileLoad(evt, comp) {
        var images=comp.getImages();
        if (evt && (evt.item.type == "image")) { images[evt.item.id] = evt.result; }
    }
    function handleComplete(evt,comp) {
        //This function is always called, irrespective of the content. You can use the variable "stage" after it is created in token create_stage.
        var lib=comp.getLibrary();
        var ss=comp.getSpriteSheet();
        var queue = evt.target;
        var ssMetadata = lib.ssMetadata;
        for(i=0; i<ssMetadata.length; i++) {
            ss[ssMetadata[i].name] = new createjs.SpriteSheet( {"images": [queue.getResult(ssMetadata[i].name)], "frames": ssMetadata[i].frames} )
        }
        var preloaderDiv = document.getElementById("_preload_div_49");
        preloaderDiv.style.display = 'none';
        canvas.style.display = 'block';
        exportRoot = new lib.Main_july2();
        stage = new lib.Stage(canvas);
        //Registers the "tick" event listener.
        fnStartAnimation = function() {
            stage.addChild(exportRoot);
            createjs.Ticker.setFPS(lib.properties.fps);
            createjs.Ticker.addEventListener("tick", stage)
            stage.addEventListener("tick", handleTick)
            function getProjectionMatrix(container, totalDepth) {
                var focalLength = 528.25;
                var projectionCenter = { x : lib.properties.width/2, y : lib.properties.height/2 };
                var scale = (totalDepth + focalLength)/focalLength;
                var scaleMat = new createjs.Matrix2D;
                scaleMat.a = 1/scale;
                scaleMat.d = 1/scale;
                var projMat = new createjs.Matrix2D;
                projMat.tx = -projectionCenter.x;
                projMat.ty = -projectionCenter.y;
                projMat = projMat.prependMatrix(scaleMat);
                projMat.tx += projectionCenter.x;
                projMat.ty += projectionCenter.y;
                return projMat;
            }
            function handleTick(event) {
                var cameraInstance = exportRoot.___camera___instance;
                if(cameraInstance !== undefined && cameraInstance.pinToObject !== undefined)
                {
                    cameraInstance.x = cameraInstance.pinToObject.x + cameraInstance.pinToObject.pinOffsetX;
                    cameraInstance.y = cameraInstance.pinToObject.y + cameraInstance.pinToObject.pinOffsetY;
                    if(cameraInstance.pinToObject.parent !== undefined && cameraInstance.pinToObject.parent.depth !== undefined)
                        cameraInstance.depth = cameraInstance.pinToObject.parent.depth + cameraInstance.pinToObject.pinOffsetZ;
                }
                applyLayerZDepth(exportRoot);
            }
            function applyLayerZDepth(parent)
            {
                var cameraInstance = parent.___camera___instance;
                var focalLength = 528.25;
                var projectionCenter = { 'x' : 0, 'y' : 0};
                if(parent === exportRoot)
                {
                    var stageCenter = { 'x' : lib.properties.width/2, 'y' : lib.properties.height/2 };
                    projectionCenter.x = stageCenter.x;
                    projectionCenter.y = stageCenter.y;
                }
                for(child in parent.children)
                {
                    var layerObj = parent.children[child];
                    if(layerObj == cameraInstance)
                        continue;
                    applyLayerZDepth(layerObj, cameraInstance);
                    if(layerObj.layerDepth === undefined)
                        continue;
                    if(layerObj.currentFrame != layerObj.parent.currentFrame)
                    {
                        layerObj.gotoAndPlay(layerObj.parent.currentFrame);
                    }
                    var matToApply = new createjs.Matrix2D;
                    var cameraMat = new createjs.Matrix2D;
                    var totalDepth = layerObj.layerDepth ? layerObj.layerDepth : 0;
                    var cameraDepth = 0;
                    if(cameraInstance && !layerObj.isAttachedToCamera)
                    {
                        var mat = cameraInstance.getMatrix();
                        mat.tx -= projectionCenter.x;
                        mat.ty -= projectionCenter.y;
                        cameraMat = mat.invert();
                        cameraMat.prependTransform(projectionCenter.x, projectionCenter.y, 1, 1, 0, 0, 0, 0, 0);
                        cameraMat.appendTransform(-projectionCenter.x, -projectionCenter.y, 1, 1, 0, 0, 0, 0, 0);
                        if(cameraInstance.depth)
                            cameraDepth = cameraInstance.depth;
                    }
                    if(layerObj.depth)
                    {
                        totalDepth = layerObj.depth;
                    }
                    //Offset by camera depth
                    totalDepth -= cameraDepth;
                    if(totalDepth < -focalLength)
                    {
                        matToApply.a = 0;
                        matToApply.d = 0;
                    }
                    else
                    {
                        if(layerObj.layerDepth)
                        {
                            var sizeLockedMat = getProjectionMatrix(parent, layerObj.layerDepth);
                            if(sizeLockedMat)
                            {
                                sizeLockedMat.invert();
                                matToApply.prependMatrix(sizeLockedMat);
                            }
                        }
                        matToApply.prependMatrix(cameraMat);
                        var projMat = getProjectionMatrix(parent, totalDepth);
                        if(projMat)
                        {
                            matToApply.prependMatrix(projMat);
                        }
                    }
                    layerObj.transformMatrix = matToApply;
                }
            }
        }
        //Code to support hidpi screens and responsive scaling.
        function makeResponsive(isResp, respDim, isScale, scaleType) {
            var lastW, lastH, lastS=1;
            window.addEventListener('resize', resizeCanvas);
            resizeCanvas();
            function resizeCanvas() {
                var w = lib.properties.width, h = lib.properties.height;
                var iw = window.innerWidth, ih=window.innerHeight;
                var pRatio = window.devicePixelRatio || 1, xRatio=iw/w, yRatio=ih/h, sRatio=1;
                if(isResp) {
                    if((respDim=='width'&&lastW==iw) || (respDim=='height'&&lastH==ih)) {
                        sRatio = lastS;
                    }
                    else if(!isScale) {
                        if(iw<w || ih<h)
                            sRatio = Math.min(xRatio, yRatio);
                    }
                    else if(scaleType==1) {
                        sRatio = Math.min(xRatio, yRatio);
                    }
                    else if(scaleType==2) {
                        sRatio = Math.max(xRatio, yRatio);
                    }
                }
                canvas.width = w*pRatio*sRatio;
                canvas.height = h*pRatio*sRatio;
                canvas.style.width = anim_container.style.width = dom_overlay_container.style.width = preloaderDiv.style.width = w*sRatio+'px';
                canvas.style.height = anim_container.style.height = dom_overlay_container.style.height = preloaderDiv.style.height = h*sRatio+'px';
                stage.scaleX = pRatio*sRatio;
                stage.scaleY = pRatio*sRatio;
                lastW = iw; lastH = ih; lastS = sRatio;
                stage.tickOnUpdate = false;
                stage.update();
                stage.tickOnUpdate = true;
            }
        }
        makeResponsive(true,'both',true,2);
        AdobeAn.compositionLoaded(lib.properties.id);
        fnStartAnimation();
    }
    init();
});