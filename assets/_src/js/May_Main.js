(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [];


// symbols:



(lib.banan = function() {
	this.initialize(img.banan);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,226,462);


(lib.banan_shadow = function() {
	this.initialize(img.banan_shadow);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,165,100);


(lib.banan_shadow1 = function() {
	this.initialize(img.banan_shadow1);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,117,359);


(lib.bg_part = function() {
	this.initialize(img.bg_part);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,745,287);


(lib.hand1 = function() {
	this.initialize(img.hand1);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,36,49);


(lib.hand2 = function() {
	this.initialize(img.hand2);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,43,47);


(lib.leaf = function() {
	this.initialize(img.leaf);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,240,216);


(lib.leaf2 = function() {
	this.initialize(img.leaf2);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,245,158);


(lib.leaf3 = function() {
	this.initialize(img.leaf3);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,151,127);


(lib.pack = function() {
	this.initialize(img.pack);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,354,565);// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.Symbol5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.banan();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol5, new cjs.Rectangle(0,0,226,462), null);


(lib.Symbol4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.banan_shadow1();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol4, new cjs.Rectangle(0,0,117,359), null);


(lib.Symbol3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.hand2();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol3, new cjs.Rectangle(0,0,43,47), null);


(lib.Symbol2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.hand1();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol2, new cjs.Rectangle(0,0,36,49), null);


(lib.Symbol1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.banan_shadow();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol1, new cjs.Rectangle(0,0,165,100), null);


(lib.Символ3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой_1
	this.instance = new lib.leaf();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Символ3, new cjs.Rectangle(0,0,240,216), null);


(lib.Символ2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой_1
	this.instance = new lib.leaf3();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Символ2, new cjs.Rectangle(0,0,151,127), null);


(lib.Символ1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой_1
	this.instance = new lib.leaf2();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Символ1, new cjs.Rectangle(0,0,245,158), null);


(lib.Символ6 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой_1
	this.instance = new lib.Символ3();
	this.instance.parent = this;
	this.instance.setTransform(120,-402,1,1,0,0,0,120,108);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({y:448},249).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,-510,240,216);


(lib.Символ5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой_1
	this.instance = new lib.Символ2();
	this.instance.parent = this;
	this.instance.setTransform(75.5,-148.5,1,1,0,0,0,75.5,63.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({y:653.5},449).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,-212,151,127);


(lib.Символ4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой_1
	this.instance = new lib.Символ1();
	this.instance.parent = this;
	this.instance.setTransform(122.5,-331,1,1,0,0,0,122.5,79);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({y:509},299).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,-410,245,158);


// stage content:
(lib.May_Main = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 5
	this.instance = new lib.Символ6();
	this.instance.parent = this;
	this.instance.setTransform(1043.5,452.5,1,1,0,0,0,120,108);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(59));

	// Layer 14
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(12,1,1).p("ABmh6Qgwg3g5gDQg2gCgjAhQgkAgAJAtQAJAvA/AmQgzApgLA2QgJAxAoAPQApAPA1gKQA8gLAugh");
	this.shape.setTransform(750.2,318.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(12,1,1).p("ABvh+Qg1g4g+gDQg6gDgmAhQgnAhAJAtQAKAxBFAmQg4ApgMA3QgJAxAoATQApATA6gKQBBgLA5gp");
	this.shape_1.setTransform(747.6,318.5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(12,1,1).p("AB4iCQg5g5hDgDQg/gEgpAiQgqAhAKAuQAKAxBLAnQg9AqgMA2QgLAzApAWQAqAXA+gLQBHgLBCgw");
	this.shape_2.setTransform(744.9,318.8);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(12,1,1).p("ACBiHQg+g4hJgFQhCgEgsAiQgtAiAKAuQALAzBQAnQhBAqgNA3QgLAzApAaQAqAbBDgLQBMgLBNg4");
	this.shape_3.setTransform(742.3,319.1);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(12,1,1).p("ACKiLQhCg5hOgFQhHgEgvAiQgwAhALAvQAMA1BVAnQhFAqgOA4QgNAzAqAeQArAeBIgKQBRgLBWhA");
	this.shape_4.setTransform(739.7,319.4);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(12,1,1).p("ACTiPQhGg6hUgFQhLgFgyAiQgzAiALAwQANA1BbApQhKAqgOA4QgOAzAqAiQAsAiBMgKQBWgMBhhH");
	this.shape_5.setTransform(737,319.7);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(12,1,1).p("ACLiMQhCg5hPgFQhIgEgwAiQgwAiALAvQAMA0BXAoQhHAqgNA4QgNAzAqAeQArAfBIgKQBSgLBYhB");
	this.shape_6.setTransform(738.2,319.4);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(12,1,1).p("ACEiIQg/g5hLgEQhEgFgtAiQguAiALAvQALAzBSAnQhCAqgNA4QgNAyAqAcQAqAcBFgLQBNgLBQg6");
	this.shape_7.setTransform(739.4,319.2);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(12,1,1).p("AB8iEQg7g5hGgEQhAgEgrAiQgsAhAKAuQALAzBNAnQg+ApgNA3QgLAzApAYQAqAZBAgLQBKgLBIg0");
	this.shape_8.setTransform(740.6,318.9);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(12,1,1).p("AB1iBQg4g4hBgEQg9gDgoAiQgpAgAJAuQAKAxBJAnQg7AqgMA2QgKAyAoAVQAqAWA9gKQBEgMBAgt");
	this.shape_9.setTransform(741.8,318.7);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(12,1,1).p("ABth9Qg0g4g9gDQg5gDgmAhQgmAhAJAtQAJAwBEAnQg3ApgLA2QgKAyApASQApASA5gKQBAgLA3gn");
	this.shape_10.setTransform(743,318.4);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(12,1,1).p("ACIiKQhBg5hNgFQhGgEgvAiQgvAhAKAvQAMA0BVAoQhFAqgNA3QgNAzAqAeQArAdBHgKQBPgLBWg+");
	this.shape_11.setTransform(740.1,319.3);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(12,1,1).p("AB/iGQg9g5hHgEQhCgEgsAiQgsAiAKAuQALAzBPAnQhAApgNA4QgLAyApAaQAqAaBBgLQBMgLBLg2");
	this.shape_12.setTransform(742.8,319);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(12,1,1).p("AB3iCQg5g5hDgDQg+gEgpAiQgqAhAKAuQAKAxBLAnQg9AqgMA2QgKAzAoAWQAqAWA+gKQBGgLBDgw");
	this.shape_13.setTransform(745.1,318.8);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(12,1,1).p("ABxh/Qg2g4g/gDQg7gDgnAhQgoAhAJAtQAKAxBHAnQg5ApgMA2QgKAyAoAUQAqAUA7gLQBCgKA7gr");
	this.shape_14.setTransform(746.9,318.5);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(12,1,1).p("ABth9Qg0g3g8gDQg5gDglAhQgnAhAJAtQAKAwBDAmQg2ApgLA2QgKAyAoASQApARA5gKQA/gLA2gm");
	this.shape_15.setTransform(748.3,318.4);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(12,1,1).p("ABph7Qgyg4g6gCQg4gDgjAhQglAhAIAsQAKAwBBAmQg1ApgLA2QgJAyAoAQQApAQA2gKQA+gLAxgj");
	this.shape_16.setTransform(749.4,318.3);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(12,1,1).p("ABnh6Qgxg4g5gCQg2gDgjAhQglAhAJAsQAJAwBAAmQg0AogLA2QgJAyAoAPQApAPA2gKQA8gLAvgh");
	this.shape_17.setTransform(750,318.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},9).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape}]},1).wait(27));

	// Layer 13
	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#000000").ss(12,1,1).p("AicApQBLgCBjgfQBigeApgS");
	this.shape_18.setTransform(911.9,566.5);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#000000").ss(12,1,1).p("AicApQBLAEBjgfQBigeApgZ");
	this.shape_19.setTransform(911.9,565.9);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#000000").ss(12,1,1).p("AibApQBKAKBjggQBigeAogf");
	this.shape_20.setTransform(912,565.3);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#000000").ss(12,1,1).p("AibAnQBKAQBjggQBigeAogl");
	this.shape_21.setTransform(912,564.9);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#000000").ss(12,1,1).p("AiaAmQBIAVBkggQBhgeAogr");
	this.shape_22.setTransform(912,564.5);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#000000").ss(12,1,1).p("AiaAlQBIAZBkgfQBhgfAogv");
	this.shape_23.setTransform(912,564.2);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#000000").ss(12,1,1).p("AiaAkQBIAeBjggQBigfAog0");
	this.shape_24.setTransform(912,563.8);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f().s("#000000").ss(12,1,1).p("AiZAiQBHAiBjgfQBigfAng5");
	this.shape_25.setTransform(912,563.6);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f().s("#000000").ss(12,1,1).p("AiZAhQBHAmBjgfQBigfAng+");
	this.shape_26.setTransform(912.1,563.3);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f().s("#000000").ss(12,1,1).p("AiYAfQBGArBjggQBigfAnhC");
	this.shape_27.setTransform(912.1,563);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f().s("#000000").ss(12,1,1).p("AiYAeQBGAuBjggQBigfAmhF");
	this.shape_28.setTransform(912.1,562.8);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f().s("#000000").ss(12,1,1).p("AiYAdQBGAxBjggQBigfAmhJ");
	this.shape_29.setTransform(912.1,562.6);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f().s("#000000").ss(12,1,1).p("AiYAcQBGA0BjggQBigfAmhM");
	this.shape_30.setTransform(912.1,562.4);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f().s("#000000").ss(12,1,1).p("AiYAbQBGA2BjgfQBigfAmhQ");
	this.shape_31.setTransform(912.1,562.3);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f().s("#000000").ss(12,1,1).p("AiYAaQBFA5BjggQBigfAnhS");
	this.shape_32.setTransform(912.1,562.1);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f().s("#000000").ss(12,1,1).p("AiYAZQBFA7BjgfQBigfAnhV");
	this.shape_33.setTransform(912.1,562);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f().s("#000000").ss(12,1,1).p("AiXAYQBEA9BjgfQBigfAmhX");
	this.shape_34.setTransform(912.1,561.9);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f().s("#000000").ss(12,1,1).p("AiXAYQBFA+BjgfQBigfAlhY");
	this.shape_35.setTransform(912.1,561.8);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f().s("#000000").ss(12,1,1).p("AiXAXQBEBABjgfQBigfAmha");
	this.shape_36.setTransform(912.1,561.7);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f().s("#000000").ss(12,1,1).p("AiXAXQBEBBBjggQBigfAmha");
	this.shape_37.setTransform(912.1,561.6);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f().s("#000000").ss(12,1,1).p("AiXAXQBEBBBjgfQBigfAmhc");
	this.shape_38.setTransform(912.1,561.6);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f().s("#000000").ss(12,1,1).p("AiXAWQBEBCBjgfQBigfAmhc");
	this.shape_39.setTransform(912.1,561.6);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f().s("#000000").ss(12,1,1).p("AiXAZQBEA8BjggQBigfAmhV");
	this.shape_40.setTransform(912.1,561.9);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f().s("#000000").ss(12,1,1).p("AiYAbQBGA2BjgfQBigfAmhP");
	this.shape_41.setTransform(912.1,562.3);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f().s("#000000").ss(12,1,1).p("AiYAfQBGArBjgfQBigfAnhD");
	this.shape_42.setTransform(912.1,563);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f().s("#000000").ss(12,1,1).p("AiaAkQBIAdBjgfQBigfAog0");
	this.shape_43.setTransform(912,563.9);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f().s("#000000").ss(12,1,1).p("AiaAlQBIAaBkggQBhgfAogv");
	this.shape_44.setTransform(912,564.1);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f().s("#000000").ss(12,1,1).p("AiaAmQBIAWBkggQBhgeAogs");
	this.shape_45.setTransform(912,564.4);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.f().s("#000000").ss(12,1,1).p("AibAnQBKASBjgfQBigeAogp");
	this.shape_46.setTransform(912,564.7);

	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.f().s("#000000").ss(12,1,1).p("AibAoQBKAPBjggQBigeAogk");
	this.shape_47.setTransform(912,564.9);

	this.shape_48 = new cjs.Shape();
	this.shape_48.graphics.f().s("#000000").ss(12,1,1).p("AibAoQBKAMBjgfQBigeAogi");
	this.shape_48.setTransform(912,565.2);

	this.shape_49 = new cjs.Shape();
	this.shape_49.graphics.f().s("#000000").ss(12,1,1).p("AibApQBKAJBjgfQBigeAogf");
	this.shape_49.setTransform(912,565.4);

	this.shape_50 = new cjs.Shape();
	this.shape_50.graphics.f().s("#000000").ss(12,1,1).p("AicApQBLAHBjgfQBigeAogc");
	this.shape_50.setTransform(912,565.6);

	this.shape_51 = new cjs.Shape();
	this.shape_51.graphics.f().s("#000000").ss(12,1,1).p("AicApQBLAFBjgfQBigeAoga");
	this.shape_51.setTransform(912,565.8);

	this.shape_52 = new cjs.Shape();
	this.shape_52.graphics.f().s("#000000").ss(12,1,1).p("AicApQBLADBjgfQBigeApgY");
	this.shape_52.setTransform(911.9,566);

	this.shape_53 = new cjs.Shape();
	this.shape_53.graphics.f().s("#000000").ss(12,1,1).p("AicApQBLACBjggQBigeApgW");
	this.shape_53.setTransform(912,566.1);

	this.shape_54 = new cjs.Shape();
	this.shape_54.graphics.f().s("#000000").ss(12,1,1).p("AicApQBLAABjgfQBigeApgU");
	this.shape_54.setTransform(911.9,566.3);

	this.shape_55 = new cjs.Shape();
	this.shape_55.graphics.f().s("#000000").ss(12,1,1).p("AicApQBLAABjggQBigeApgT");
	this.shape_55.setTransform(911.9,566.3);

	this.shape_56 = new cjs.Shape();
	this.shape_56.graphics.f().s("#000000").ss(12,1,1).p("AicApQBLgBBjggQBigeApgS");
	this.shape_56.setTransform(911.9,566.4);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_18}]}).to({state:[{t:this.shape_18}]},7).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_27}]},1).to({state:[{t:this.shape_28}]},1).to({state:[{t:this.shape_29}]},1).to({state:[{t:this.shape_30}]},1).to({state:[{t:this.shape_31}]},1).to({state:[{t:this.shape_32}]},1).to({state:[{t:this.shape_33}]},1).to({state:[{t:this.shape_34}]},1).to({state:[{t:this.shape_35}]},1).to({state:[{t:this.shape_36}]},1).to({state:[{t:this.shape_37}]},1).to({state:[{t:this.shape_38}]},1).to({state:[{t:this.shape_39}]},1).to({state:[{t:this.shape_39}]},1).to({state:[{t:this.shape_39}]},1).to({state:[{t:this.shape_40}]},1).to({state:[{t:this.shape_41}]},1).to({state:[{t:this.shape_29}]},1).to({state:[{t:this.shape_42}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_43}]},1).to({state:[{t:this.shape_44}]},1).to({state:[{t:this.shape_45}]},1).to({state:[{t:this.shape_46}]},1).to({state:[{t:this.shape_47}]},1).to({state:[{t:this.shape_48}]},1).to({state:[{t:this.shape_49}]},1).to({state:[{t:this.shape_50}]},1).to({state:[{t:this.shape_51}]},1).to({state:[{t:this.shape_52}]},1).to({state:[{t:this.shape_53}]},1).to({state:[{t:this.shape_54}]},1).to({state:[{t:this.shape_55}]},1).to({state:[{t:this.shape_56}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_18}]},1).wait(7));

	// Layer 12
	this.shape_57 = new cjs.Shape();
	this.shape_57.graphics.f().s("#000000").ss(12,1,1).p("ApqA0QAfhLBHgkQA/ggBLAJQBKAJAyAsQA2AtAEBEADJA7QARg7A9gpQA7gnBHgDQBMgEA3AnQA/AsAQBW");
	this.shape_57.setTransform(799.9,258.3);

	this.timeline.addTween(cjs.Tween.get(this.shape_57).wait(59));

	// Layer 9
	this.instance_1 = new lib.Symbol3();
	this.instance_1.parent = this;
	this.instance_1.setTransform(890.2,479.3,1,1,0,0,0,39.2,13.3);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(6).to({regX:39.1,rotation:15,x:890.1},7).to({regX:39.2,rotation:0,x:890.2},5).wait(2).to({regX:39.1,rotation:15,x:890.1},7).to({regX:39.2,rotation:0,x:890.2},5).wait(27));

	// Layer 8
	this.instance_2 = new lib.Symbol2();
	this.instance_2.parent = this;
	this.instance_2.setTransform(606,455.5,1,1,0,0,0,18,24.5);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(6).to({x:626},8,cjs.Ease.get(1)).wait(18).to({x:606},9,cjs.Ease.get(1)).wait(18));

	// Layer 4
	this.instance_3 = new lib.Symbol5();
	this.instance_3.parent = this;
	this.instance_3.setTransform(597,407,1,1,0,0,0,113,231);

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(6).to({x:617},8,cjs.Ease.get(1)).wait(18).to({x:597},9,cjs.Ease.get(1)).wait(18));

	// Layer 18
	this.shape_58 = new cjs.Shape();
	this.shape_58.graphics.f().s("#000000").ss(12,1,1).p("Akkr7QDzAiCgDKQCYC+AZEGQAaEMh8DiQiID6jwBf");
	this.shape_58.setTransform(910.2,401.7);

	this.timeline.addTween(cjs.Tween.get(this.shape_58).wait(59));

	// Layer 16
	this.instance_4 = new lib.Symbol4();
	this.instance_4.parent = this;
	this.instance_4.setTransform(679.6,470.4,1,1,0,0,0,58.5,179.5);

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(6).to({x:699.6},8,cjs.Ease.get(1)).wait(18).to({x:679.6},9,cjs.Ease.get(1)).wait(18));

	// Layer 3
	this.instance_5 = new lib.pack();
	this.instance_5.parent = this;
	this.instance_5.setTransform(625,115);

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(59));

	// Layer 17
	this.shape_59 = new cjs.Shape();
	this.shape_59.graphics.f().s("#000000").ss(12,1,1).p("AmcGnQgbtSNUAF");
	this.shape_59.setTransform(641.2,413.2);

	this.shape_60 = new cjs.Shape();
	this.shape_60.graphics.f().s("#000000").ss(12,1,1).p("AmFGpQhCs2NSgb");
	this.shape_60.setTransform(643.7,412.6);

	this.shape_61 = new cjs.Shape();
	this.shape_61.graphics.f().s("#000000").ss(12,1,1).p("AlxGrQhjsdNQg4");
	this.shape_61.setTransform(645.7,412);

	this.shape_62 = new cjs.Shape();
	this.shape_62.graphics.f().s("#000000").ss(12,1,1).p("AlfGsQh/sINPhP");
	this.shape_62.setTransform(647.3,411.6);

	this.shape_63 = new cjs.Shape();
	this.shape_63.graphics.f().s("#000000").ss(12,1,1).p("AlQGtQiVr3NNhi");
	this.shape_63.setTransform(648.6,411.2);

	this.shape_64 = new cjs.Shape();
	this.shape_64.graphics.f().s("#000000").ss(12,1,1).p("AlEGuQinrqNMhx");
	this.shape_64.setTransform(649.6,410.9);

	this.shape_65 = new cjs.Shape();
	this.shape_65.graphics.f().s("#000000").ss(12,1,1).p("Ak7GvQi0rhNLh8");
	this.shape_65.setTransform(650.3,410.7);

	this.shape_66 = new cjs.Shape();
	this.shape_66.graphics.f().s("#000000").ss(12,1,1).p("Ak2GvQi7rbNKiC");
	this.shape_66.setTransform(650.7,410.6);

	this.shape_67 = new cjs.Shape();
	this.shape_67.graphics.f().s("#000000").ss(12,1,1).p("Ak0GvQi+rZNKiE");
	this.shape_67.setTransform(650.8,410.5);

	this.shape_68 = new cjs.Shape();
	this.shape_68.graphics.f().s("#000000").ss(12,1,1).p("AlMGtQicryNNho");
	this.shape_68.setTransform(649,411.1);

	this.shape_69 = new cjs.Shape();
	this.shape_69.graphics.f().s("#000000").ss(12,1,1).p("AlgGsQh9sJNOhO");
	this.shape_69.setTransform(647.3,411.6);

	this.shape_70 = new cjs.Shape();
	this.shape_70.graphics.f().s("#000000").ss(12,1,1).p("AlxGrQhjscNQg5");
	this.shape_70.setTransform(645.8,412);

	this.shape_71 = new cjs.Shape();
	this.shape_71.graphics.f().s("#000000").ss(12,1,1).p("Al+GqQhOstNSgm");
	this.shape_71.setTransform(644.4,412.4);

	this.shape_72 = new cjs.Shape();
	this.shape_72.graphics.f().s("#000000").ss(12,1,1).p("AmJGpQg8s6NTgX");
	this.shape_72.setTransform(643.3,412.7);

	this.shape_73 = new cjs.Shape();
	this.shape_73.graphics.f().s("#000000").ss(12,1,1).p("AmRGoQgutENUgL");
	this.shape_73.setTransform(642.4,412.9);

	this.shape_74 = new cjs.Shape();
	this.shape_74.graphics.f().s("#000000").ss(12,1,1).p("AmXGnQgktMNUgC");
	this.shape_74.setTransform(641.8,413.1);

	this.shape_75 = new cjs.Shape();
	this.shape_75.graphics.f().s("#000000").ss(12,1,1).p("AmbGoQgdtRNUAD");
	this.shape_75.setTransform(641.4,413.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_59}]}).to({state:[{t:this.shape_59}]},6).to({state:[{t:this.shape_60}]},1).to({state:[{t:this.shape_61}]},1).to({state:[{t:this.shape_62}]},1).to({state:[{t:this.shape_63}]},1).to({state:[{t:this.shape_64}]},1).to({state:[{t:this.shape_65}]},1).to({state:[{t:this.shape_66}]},1).to({state:[{t:this.shape_67}]},1).to({state:[{t:this.shape_67}]},18).to({state:[{t:this.shape_68}]},1).to({state:[{t:this.shape_69}]},1).to({state:[{t:this.shape_70}]},1).to({state:[{t:this.shape_71}]},1).to({state:[{t:this.shape_72}]},1).to({state:[{t:this.shape_73}]},1).to({state:[{t:this.shape_74}]},1).to({state:[{t:this.shape_75}]},1).to({state:[{t:this.shape_59}]},1).wait(18));

	// Слой_2
	this.instance_6 = new lib.Символ5();
	this.instance_6.parent = this;
	this.instance_6.setTransform(936.5,150.5,1,1,0,0,0,75.5,63.5);

	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(59));

	// Слой_1
	this.instance_7 = new lib.Символ4();
	this.instance_7.parent = this;
	this.instance_7.setTransform(457.5,331,1,1,0,0,0,122.5,79);

	this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(59));

	// Layer 7
	this.instance_8 = new lib.Symbol1();
	this.instance_8.parent = this;
	this.instance_8.setTransform(533,635.9,1.085,1,0,0,0,10.5,96);

	this.timeline.addTween(cjs.Tween.get(this.instance_8).wait(6).to({scaleX:1,x:553},8,cjs.Ease.get(1)).wait(18).to({scaleX:1.09,x:533},9,cjs.Ease.get(1)).wait(18));

	// Layer 2
	this.instance_9 = new lib.bg_part();
	this.instance_9.parent = this;
	this.instance_9.setTransform(390,393);

	this.timeline.addTween(cjs.Tween.get(this.instance_9).wait(59));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(540,174.5,1163.5,845.6);
// library properties:
lib.properties = {
	id: '3F54AC98F156434A827AB1307B55EFC0',
	width: 1080,
	height: 680,
	fps: 30,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: [
		{src:"assets/images/banan.png", id:"banan"},
		{src:"assets/images/banan_shadow.png", id:"banan_shadow"},
		{src:"assets/images/banan_shadow1.png", id:"banan_shadow1"},
		{src:"assets/images/bg_part.png", id:"bg_part"},
		{src:"assets/images/hand8.png", id:"hand1"},
		{src:"assets/images/hand9.png", id:"hand2"},
		{src:"assets/images/leaf.png", id:"leaf"},
		{src:"assets/images/leaf2.png", id:"leaf2"},
		{src:"assets/images/leaf3.png", id:"leaf3"},
		{src:"assets/images/pack8.png", id:"pack"}
	],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['3F54AC98F156434A827AB1307B55EFC0'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}



})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;

$(function () {
    var canvas, stage, exportRoot, anim_container, dom_overlay_container, fnStartAnimation;
    function init() {
        canvas = document.getElementById("canvas8");
        anim_container = document.getElementById("animation_container8");
        dom_overlay_container = document.getElementById("dom_overlay_container8");
        var comp=AdobeAn.getComposition("3F54AC98F156434A827AB1307B55EFC0");
        var lib=comp.getLibrary();
        var loader = new createjs.LoadQueue(false);
        loader.addEventListener("fileload", function(evt){handleFileLoad(evt,comp)});
        loader.addEventListener("complete", function(evt){handleComplete(evt,comp)});
        var lib=comp.getLibrary();
        loader.loadManifest(lib.properties.manifest);
    }
    function handleFileLoad(evt, comp) {
        var images=comp.getImages();
        if (evt && (evt.item.type == "image")) { images[evt.item.id] = evt.result; }
    }
    function handleComplete(evt,comp) {
        //This function is always called, irrespective of the content. You can use the variable "stage" after it is created in token create_stage.
        var lib=comp.getLibrary();
        var ss=comp.getSpriteSheet();
        var queue = evt.target;
        var ssMetadata = lib.ssMetadata;
        for(i=0; i<ssMetadata.length; i++) {
            ss[ssMetadata[i].name] = new createjs.SpriteSheet( {"images": [queue.getResult(ssMetadata[i].name)], "frames": ssMetadata[i].frames} )
        }
        exportRoot = new lib.May_Main();
        stage = new lib.Stage(canvas);
        //Registers the "tick" event listener.
        fnStartAnimation = function() {
            stage.addChild(exportRoot);
            createjs.Ticker.setFPS(lib.properties.fps);
            createjs.Ticker.addEventListener("tick", stage);
        }
        //Code to support hidpi screens and responsive scaling.
        function makeResponsive(isResp, respDim, isScale, scaleType) {
            var lastW, lastH, lastS=1;
            window.addEventListener('resize', resizeCanvas);
            resizeCanvas();
            function resizeCanvas() {
                var w = lib.properties.width, h = lib.properties.height;
                var iw = window.innerWidth, ih=window.innerHeight;
                var pRatio = window.devicePixelRatio || 1, xRatio=iw/w, yRatio=ih/h, sRatio=1;
                if(isResp) {
                    if((respDim=='width'&&lastW==iw) || (respDim=='height'&&lastH==ih)) {
                        sRatio = lastS;
                    }
                    else if(!isScale) {
                        if(iw<w || ih<h)
                            sRatio = Math.min(xRatio, yRatio);
                    }
                    else if(scaleType==1) {
                        sRatio = Math.min(xRatio, yRatio);
                    }
                    else if(scaleType==2) {
                        sRatio = Math.max(xRatio, yRatio);
                    }
                }
                canvas.width = w*pRatio*sRatio;
                canvas.height = h*pRatio*sRatio;
                canvas.style.width = dom_overlay_container.style.width = anim_container.style.width =  w*sRatio+'px';
                canvas.style.height = anim_container.style.height = dom_overlay_container.style.height = h*sRatio+'px';
                stage.scaleX = pRatio*sRatio;
                stage.scaleY = pRatio*sRatio;
                lastW = iw; lastH = ih; lastS = sRatio;
                stage.tickOnUpdate = false;
                stage.update();
                stage.tickOnUpdate = true;
            }
        }
        makeResponsive(true,'both',true,2);
        AdobeAn.compositionLoaded(lib.properties.id);
        fnStartAnimation();
    }
    init();
});