(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [];


// symbols:



(lib.bat = function() {
	this.initialize(img.bat);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,122,51);


(lib.bg = function() {
	this.initialize(img.bg);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,1080,680);


(lib.pack = function() {
	this.initialize(img.pack);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,603,499);// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.Symbol12 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.bat();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol12, new cjs.Rectangle(0,0,122,51), null);


(lib.Symbol9 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(12,1,1).p("ABZhOQgmB3iLAm");
	this.shape.setTransform(8.9,7.9);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol9, new cjs.Rectangle(-6,-6,29.8,27.9), null);


(lib.Symbol8 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(12,1,1).p("AgEh6QAsB4g2B9");
	this.shape.setTransform(1.5,12.3);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol8, new cjs.Rectangle(-6,-6,15.1,36.6), null);


(lib.Symbol7 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(12,1,1).p("AGfjKQmnEnmWBu");
	this.shape.setTransform(41.5,20.3);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol7, new cjs.Rectangle(-6,-6,94.9,52.6), null);


(lib.Symbol6 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(12,1,1).p("AEXGUQgUhRhChaQglgzhsh3Qhsh5g8hQQhfiBg/iI");
	this.shape.setTransform(27.9,40.4);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol6, new cjs.Rectangle(-6,-6,67.8,92.7), null);


(lib.Symbol4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 4
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(12,1,1).p("ABhgrQhugjhTCB");
	this.shape.setTransform(68.8,102.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(12,1,1).p("AhgAzQBTiABuAi");
	this.shape_1.setTransform(68.8,102.8);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(12,1,1).p("AhiAyQBWh9BvAg");
	this.shape_2.setTransform(68.7,102.9);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(12,1,1).p("AhkAxQBZh4BwAc");
	this.shape_3.setTransform(68.6,103);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(12,1,1).p("AhoAwQBghxBxAV");
	this.shape_4.setTransform(68.4,103.1);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(12,1,1).p("ABtgsQhzgNhmBn");
	this.shape_5.setTransform(68.2,103.2);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(12,1,1).p("AhpAvQBihsByAS");
	this.shape_6.setTransform(68.4,103.1);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(12,1,1).p("AhnAwQBehxBxAW");
	this.shape_7.setTransform(68.5,103);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(12,1,1).p("AhlAxQBbh2BwAZ");
	this.shape_8.setTransform(68.6,103);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(12,1,1).p("AhkAyQBZh6BvAd");
	this.shape_9.setTransform(68.7,102.9);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(12,1,1).p("AhiAyQBWh8BvAf");
	this.shape_10.setTransform(68.7,102.9);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(12,1,1).p("AhhAzQBVh/BuAh");
	this.shape_11.setTransform(68.8,102.8);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(12,1,1).p("AhhAzQBUiABvAi");
	this.shape_12.setTransform(68.8,102.8);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(12,1,1).p("AhgAzQBTiBBuAj");
	this.shape_13.setTransform(68.8,102.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},17).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape}]},1).wait(30));

	// Layer 3
	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(12,1,1).p("AhWhlQDUAkgwCn");
	this.shape_14.setTransform(87.1,104.5);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(12,1,1).p("AhXhkQDUAjgsCm");
	this.shape_15.setTransform(87.2,104.3);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(12,1,1).p("AhdhfQDUAdgdCi");
	this.shape_16.setTransform(87.8,103.9);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(12,1,1).p("AhnhXQDVAUgGCb");
	this.shape_17.setTransform(88.8,103.2);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#000000").ss(12,1,1).p("Ah4hMQDWAIAbCR");
	this.shape_18.setTransform(90.4,102.2);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#000000").ss(12,1,1).p("AiNg+QDWgHBFCE");
	this.shape_19.setTransform(92.5,100.9);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#000000").ss(12,1,1).p("AiBhGQDWACAtCL");
	this.shape_20.setTransform(91.3,101.7);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#000000").ss(12,1,1).p("Ah2hNQDWAKAXCR");
	this.shape_21.setTransform(90.2,102.3);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#000000").ss(12,1,1).p("AhshUQDVARAECX");
	this.shape_22.setTransform(89.3,102.9);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#000000").ss(12,1,1).p("AhkhZQDVAXgMCc");
	this.shape_23.setTransform(88.5,103.4);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#000000").ss(12,1,1).p("AhfhdQDVAbgZCg");
	this.shape_24.setTransform(87.9,103.8);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f().s("#000000").ss(12,1,1).p("AhahhQDUAfgjCk");
	this.shape_25.setTransform(87.5,104.1);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f().s("#000000").ss(12,1,1).p("AhYhjQDUAigqCl");
	this.shape_26.setTransform(87.3,104.3);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f().s("#000000").ss(12,1,1).p("AhWhlQDUAkgvCn");
	this.shape_27.setTransform(87.1,104.4);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_14}]}).to({state:[{t:this.shape_14}]},17).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_27}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_27}]},1).to({state:[{t:this.shape_14}]},1).wait(30));

	// Layer 2
	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f().s("#000000").ss(12,1,1).p("Ag1hoQCZA0hCCd");
	this.shape_28.setTransform(80.4,107.4);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f().s("#000000").ss(12,1,1).p("Ag2hmQCZAyg9Cc");
	this.shape_29.setTransform(80.6,107.4);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f().s("#000000").ss(12,1,1).p("Ag6hjQCaAvgwCY");
	this.shape_30.setTransform(81,107.1);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f().s("#000000").ss(12,1,1).p("AhChdQCbApgZCS");
	this.shape_31.setTransform(81.9,106.7);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f().s("#000000").ss(12,1,1).p("AhShVQCeAfAHCM");
	this.shape_32.setTransform(83.6,106.2);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f().s("#000000").ss(12,1,1).p("AhnhKQCgAUAvCB");
	this.shape_33.setTransform(85.9,105.5);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f().s("#000000").ss(12,1,1).p("AhbhQQCfAaAYCH");
	this.shape_34.setTransform(84.6,105.9);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f().s("#000000").ss(12,1,1).p("AhPhWQCdAhADCM");
	this.shape_35.setTransform(83.4,106.2);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f().s("#000000").ss(12,1,1).p("AhGhbQCcAmgQCQ");
	this.shape_36.setTransform(82.4,106.6);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f().s("#000000").ss(12,1,1).p("AhAheQCbAqgfCT");
	this.shape_37.setTransform(81.7,106.8);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f().s("#000000").ss(12,1,1).p("Ag8hiQCbAugsCX");
	this.shape_38.setTransform(81.2,107);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f().s("#000000").ss(12,1,1).p("Ag5hkQCaAwg1Ca");
	this.shape_39.setTransform(80.9,107.2);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f().s("#000000").ss(12,1,1).p("Ag3hmQCaAyg9Cb");
	this.shape_40.setTransform(80.6,107.3);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f().s("#000000").ss(12,1,1).p("Ag1hoQCZAzhBCd");
	this.shape_41.setTransform(80.5,107.4);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_28}]}).to({state:[{t:this.shape_28}]},17).to({state:[{t:this.shape_29}]},1).to({state:[{t:this.shape_30}]},1).to({state:[{t:this.shape_31}]},1).to({state:[{t:this.shape_32}]},1).to({state:[{t:this.shape_33}]},1).to({state:[{t:this.shape_34}]},1).to({state:[{t:this.shape_35}]},1).to({state:[{t:this.shape_36}]},1).to({state:[{t:this.shape_37}]},1).to({state:[{t:this.shape_38}]},1).to({state:[{t:this.shape_39}]},1).to({state:[{t:this.shape_40}]},1).to({state:[{t:this.shape_41}]},1).to({state:[{t:this.shape_28}]},1).to({state:[{t:this.shape_29}]},1).to({state:[{t:this.shape_30}]},1).to({state:[{t:this.shape_31}]},1).to({state:[{t:this.shape_32}]},1).to({state:[{t:this.shape_33}]},1).to({state:[{t:this.shape_34}]},1).to({state:[{t:this.shape_35}]},1).to({state:[{t:this.shape_36}]},1).to({state:[{t:this.shape_37}]},1).to({state:[{t:this.shape_38}]},1).to({state:[{t:this.shape_39}]},1).to({state:[{t:this.shape_40}]},1).to({state:[{t:this.shape_41}]},1).to({state:[{t:this.shape_28}]},1).wait(30));

	// Layer 1
	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f().s("#000000").ss(12,1,1).p("Al4nTQDjLxIOC2");
	this.shape_42.setTransform(37.7,46.8);

	this.timeline.addTween(cjs.Tween.get(this.shape_42).wait(75));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-6,-6,107.8,129.9);


(lib.Symbol2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#F51431").s().p("AAAAqQgRAAgNgMQgMgMAAgSQAAgQAMgNQANgNARAAIADAAQAPABALAMQANANAAAQQAAASgNAMQgLAMgPAAIgDAAg");
	this.shape.setTransform(4.3,4.3);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol2, new cjs.Rectangle(0,0,8.5,8.5), null);


(lib.Symbol10 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.Symbol8();
	this.instance.parent = this;
	this.instance.setTransform(18.2,12.3,1,1,0,0,0,1.5,12.3);

	this.instance_1 = new lib.Symbol9();
	this.instance_1.parent = this;
	this.instance_1.setTransform(8.8,7.9,1,1,0,0,0,8.8,7.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_1},{t:this.instance}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol10, new cjs.Rectangle(-6,-6,31.8,36.6), null);


(lib.Symbol5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 4
	this.instance = new lib.Symbol10();
	this.instance.parent = this;
	this.instance.setTransform(56.1,121.8,1,1,0,0,0,17.8,0);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(4).to({x:56.8,y:98.7},15,cjs.Ease.get(1)).wait(25).to({x:56.1,y:121.8},15,cjs.Ease.get(1)).wait(16));

	// Layer 2
	this.instance_1 = new lib.Symbol6();
	this.instance_1.parent = this;
	this.instance_1.setTransform(0.3,41.1);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(4).to({rotation:-5.5,x:-7,y:21.8},15,cjs.Ease.get(1)).wait(25).to({rotation:0,x:0.3,y:41.1},15,cjs.Ease.get(1)).wait(16));

	// Layer 1
	this.instance_2 = new lib.Symbol7();
	this.instance_2.parent = this;
	this.instance_2.setTransform(82.9,0,1,1,0,0,0,82.9,0);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(4).to({rotation:12.7},15,cjs.Ease.get(1)).wait(25).to({rotation:0},15,cjs.Ease.get(1)).wait(16));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-6,-6,94.9,158.4);


(lib.Symbol3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.Symbol12();
	this.instance.parent = this;
	this.instance.setTransform(61,25.5,1,1,0,0,0,61,25.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(2).to({y:25.6},0).wait(1).to({y:25.7},0).wait(1).to({y:25.8},0).wait(1).to({y:26},0).wait(1).to({y:26.2},0).wait(1).to({y:26.6},0).wait(1).to({y:26.9},0).wait(1).to({y:27.4},0).wait(1).to({y:27.9},0).wait(1).to({y:28.4},0).wait(1).to({y:29.1},0).wait(1).to({y:29.7},0).wait(1).to({y:30.4},0).wait(1).to({y:31.1},0).wait(1).to({y:31.8},0).wait(1).to({y:32.4},0).wait(1).to({y:32.9},0).wait(1).to({y:33.5},0).wait(1).to({y:33.9},0).wait(1).to({y:34.3},0).wait(1).to({y:34.6},0).wait(1).to({y:34.9},0).wait(1).to({y:35.1},0).wait(1).to({y:35.2},0).wait(1).to({y:35.4},0).wait(2).to({y:35.5},0).wait(3).to({y:35.4},0).wait(1).to({y:35.3},0).wait(1).to({y:35.2},0).wait(1).to({y:35},0).wait(1).to({y:34.8},0).wait(1).to({y:34.5},0).wait(1).to({y:34.2},0).wait(1).to({y:33.8},0).wait(1).to({y:33.3},0).wait(1).to({y:32.8},0).wait(1).to({y:32.2},0).wait(1).to({y:31.6},0).wait(1).to({y:30.9},0).wait(1).to({y:30.2},0).wait(1).to({y:29.6},0).wait(1).to({y:29},0).wait(1).to({y:28.4},0).wait(1).to({y:27.9},0).wait(1).to({y:27.4},0).wait(1).to({y:27},0).wait(1).to({y:26.6},0).wait(1).to({y:26.3},0).wait(1).to({y:26.1},0).wait(1).to({y:25.9},0).wait(1).to({y:25.7},0).wait(1).to({y:25.6},0).wait(2).to({y:25.5},0).wait(2));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,122,51);


(lib.Symbol2_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 6
	this.instance = new lib.Symbol3("synched",39,false);
	this.instance.parent = this;
	this.instance.setTransform(392,153.3,0.697,0.8,0,-6.9,-7.5,60.7,25.7);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(19).to({startPosition:0},0).wait(41));

	// Layer 5
	this.instance_1 = new lib.Symbol3("synched",49,false);
	this.instance_1.parent = this;
	this.instance_1.setTransform(431.6,114.5,0.8,0.8,6.6,0,0,61.1,25.5);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(9).to({startPosition:0},0).wait(51));

	// Layer 4
	this.instance_2 = new lib.Symbol3("synched",28,false);
	this.instance_2.parent = this;
	this.instance_2.setTransform(482.3,84.2,1.02,1.02,-5,0,0,61.1,25.4);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(30).to({startPosition:0},0).wait(30));

	// Layer 2
	this.instance_3 = new lib.Symbol3("synched",19,false);
	this.instance_3.parent = this;
	this.instance_3.setTransform(393,39.9,1,1,0,0,180,61,25.5);

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(39).to({startPosition:0},0).wait(21));

	// Layer 1
	this.instance_4 = new lib.Symbol3("synched",0,false);
	this.instance_4.parent = this;
	this.instance_4.setTransform(61,25.5,1,1,0,0,0,61,25.5);

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(60));

	// Layer 3
	this.instance_5 = new lib.Symbol3("synched",8,false);
	this.instance_5.parent = this;
	this.instance_5.setTransform(30.7,104.4,0.78,0.78,0,-7.3,172.7,60.9,25.6);

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(50).to({startPosition:0},0).wait(10));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-19,0,566.4,185.1);


(lib.Symbol1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#130F09").ss(11,1,1).p("AkLhfQBDDEDRgGQBWgDBJgrQBKgtAahG");
	this.shape.setTransform(156,-52.6);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(75));

	// Layer 3
	this.instance = new lib.Symbol2();
	this.instance.parent = this;
	this.instance.setTransform(137.2,-49.4,2.017,1.042,0,46.1,51.9,5.4,7.5);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(12).to({_off:false},0).to({scaleY:2.02,guide:{path:[137.2,-49.3,137.5,-49,137.9,-48.7]}},4).wait(1).to({regX:4.3,regY:4.3,scaleX:2.02,scaleY:2.02,skewX:45.7,skewY:51.5,x:141.4,y:-54.8},0).wait(1).to({scaleX:2.01,scaleY:2.02,skewX:44.8,skewY:50.4,x:141.8,y:-54.4},0).wait(1).to({scaleX:2.01,scaleY:2.02,skewX:43.2,skewY:48.6,x:142.7,y:-53.7},0).wait(1).to({scaleX:2,scaleY:2.03,skewX:40.7,skewY:45.8,x:144.2,y:-52.8},0).wait(1).to({scaleX:2,scaleY:2.04,skewX:37.3,skewY:41.9,x:146.4,y:-51.7},0).wait(1).to({scaleX:1.98,scaleY:2.05,skewX:32.8,skewY:36.8,x:149.5,y:-50.7},0).wait(1).to({scaleX:1.97,scaleY:2.06,skewX:27.4,skewY:30.6,x:153.3,y:-50.1},0).wait(1).to({scaleX:1.96,scaleY:2.08,skewX:21.5,skewY:23.9,x:157.6},0).wait(1).to({scaleX:1.94,scaleY:2.09,skewX:16,skewY:17.6,x:161.1,y:-50.5},0).wait(1).to({scaleX:1.93,scaleY:2.1,skewX:11.4,skewY:12.4,x:164,y:-51.2},0).wait(1).to({scaleX:1.92,scaleY:2.11,skewX:8,skewY:8.6,x:166.1,y:-52},0).wait(1).to({scaleX:1.92,scaleY:2.11,skewX:5.8,skewY:6,x:167.5,y:-52.6},0).wait(1).to({scaleX:1.91,scaleY:2.12,skewX:4.5,skewY:4.6,x:168.2,y:-53},0).wait(1).to({regX:5.4,regY:7.5,scaleX:1.91,scaleY:2.12,rotation:4,skewX:0,skewY:0,x:170.2,y:-46.3},0).wait(7).to({regX:4.3,regY:4.3,scaleX:1.91,scaleY:2.12,rotation:0,skewX:4.6,skewY:4.7,x:168.2,y:-52.9},0).wait(1).to({scaleX:1.92,scaleY:2.11,skewX:6,skewY:6.2,x:167.3,y:-52.5},0).wait(1).to({scaleX:1.92,scaleY:2.11,skewX:8.3,skewY:8.8,x:165.8,y:-51.9},0).wait(1).to({scaleX:1.93,scaleY:2.1,skewX:11.5,skewY:12.6,x:163.6,y:-51.1},0).wait(1).to({scaleX:1.94,scaleY:2.09,skewX:15.7,skewY:17.4,x:160.6,y:-50.4},0).wait(1).to({scaleX:1.95,scaleY:2.08,skewX:20.7,skewY:23,x:157,y:-50},0).wait(1).to({scaleX:1.97,scaleY:2.06,skewX:26.1,skewY:29.1,x:153.3,y:-50.1},0).wait(1).to({scaleX:1.98,scaleY:2.05,skewX:31.2,skewY:35,x:150.1,y:-50.5},0).wait(1).to({scaleX:1.99,scaleY:2.04,skewX:35.9,skewY:40.2,x:147.3,y:-51.3},0).wait(1).to({scaleX:2,scaleY:2.03,skewX:39.6,skewY:44.5,x:145.1,y:-52.2},0).wait(1).to({scaleX:2.01,scaleY:2.03,skewX:42.5,skewY:47.7,x:143.3,y:-53.2},0).wait(1).to({scaleX:2.01,scaleY:2.02,skewX:44.4,skewY:50,x:142.1,y:-54.1},0).wait(1).to({scaleX:2.02,scaleY:2.02,skewX:45.6,skewY:51.3,x:141.5,y:-54.7},0).wait(1).to({regX:5.4,regY:7.5,scaleX:2.02,scaleY:2.02,skewX:46.1,skewY:51.9,x:137.9,y:-48.7},0).to({regX:5.5,regY:7.4,scaleY:1.23,x:135,y:-45.5},4).to({_off:true},1).wait(20));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(123.8,-67.6,64.6,30.1);


(lib.Symbol11 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Symbol 1
	this.instance = new lib.Symbol1();
	this.instance.parent = this;
	this.instance.setTransform(61.2,59.1,1.114,1.069,1.5,0,0,156.2,-52.6);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(75));

	// Layer 3
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#130F09").ss(12,1,1).p("AjNh8QgGB1A+BEQA4A+BVACQBUABA9g5QBDhAADhr");
	this.shape.setTransform(20.6,12.5);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#130F09").ss(12,1,1).p("AjNh1QgGBuA+BAQA4A7BVACQBUABA9g2QBDg8ADhm");
	this.shape_1.setTransform(20.6,11.8);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#130F09").ss(12,1,1).p("AjNhvQgGBoA+A9QA4A4BVACQBUABA9gzQBDg5ADhh");
	this.shape_2.setTransform(20.6,11.2);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#130F09").ss(12,1,1).p("AjNhqQgGBkA+A6QA4A1BVACQBUABA9gxQBDg2ADhc");
	this.shape_3.setTransform(20.6,10.7);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#130F09").ss(12,1,1).p("AjNhlQgGBfA+A4QA4AyBVACQBUABA9gvQBDg0ADhY");
	this.shape_4.setTransform(20.6,10.2);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#130F09").ss(12,1,1).p("AjNhhQgGBcA+A1QA4AxBVABQBUACA9guQBDgyADhU");
	this.shape_5.setTransform(20.6,9.8);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#130F09").ss(12,1,1).p("AjNheQgGBZA+AzQA4AvBVACQBUABA9gsQBDgwADhS");
	this.shape_6.setTransform(20.6,9.5);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#130F09").ss(12,1,1).p("AjNhcQgGBXA+AyQA4AuBVACQBUABA9grQBDgvADhQ");
	this.shape_7.setTransform(20.6,9.3);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#130F09").ss(12,1,1).p("AjNhaQgGBVA+AxQA4AtBVACQBUABA9gqQBDguADhP");
	this.shape_8.setTransform(20.6,9.1);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#130F09").ss(12,1,1).p("AjNhZQgGBUA+AxQA4AsBVACQBUABA9gpQBDguADhN");
	this.shape_9.setTransform(20.6,9);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#130F09").ss(12,1,1).p("AjNhZQgGBUA+AwQA4AtBVABQBUABA9gpQBDgtADhN");
	this.shape_10.setTransform(20.6,9);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#130F09").ss(12,1,1).p("AjNhfQgGBaA+A0QA4AwBVABQBUABA9gsQBDgxADhT");
	this.shape_11.setTransform(20.6,9.6);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#130F09").ss(12,1,1).p("AjNhrQgGBlA+A6QA4A1BVACQBUABA9gxQBDg3ADhc");
	this.shape_12.setTransform(20.6,10.8);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#130F09").ss(12,1,1).p("AjNhzQgGBsA+A/QA4A6BVACQBUABA9g1QBDg7ADhk");
	this.shape_13.setTransform(20.6,11.6);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#130F09").ss(12,1,1).p("AjNh2QgGBvA+BBQA4A7BVACQBUABA9g2QBDg9ADhn");
	this.shape_14.setTransform(20.6,11.9);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#130F09").ss(12,1,1).p("AjNh5QgGByA+BCQA4A8BVACQBUACA9g4QBDg+ADhp");
	this.shape_15.setTransform(20.6,12.2);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#130F09").ss(12,1,1).p("AjNh6QgGBzA+BDQA4A9BVACQBUACA9g5QBDg/ADhq");
	this.shape_16.setTransform(20.6,12.3);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#130F09").ss(12,1,1).p("AjNh8QgGB0A+BEQA4A+BVACQBUACA9g6QBDg/ADhr");
	this.shape_17.setTransform(20.6,12.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},12).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_10}]},22).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape}]},1).wait(21));

	// Layer 4
	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#130F09").ss(12,1,1).p("AjNh8QgGB1A+BEQA4A+BVACQBUABA9g5QBDhAADhr");
	this.shape_18.setTransform(96,14.6);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#130F09").ss(12,1,1).p("AjNh1QgGBuA+BAQA4A7BVACQBUABA9g2QBDg8ADhm");
	this.shape_19.setTransform(96,13.9);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#130F09").ss(12,1,1).p("AjNhvQgGBoA+A9QA4A4BVACQBUABA9g0QBDg5ADhg");
	this.shape_20.setTransform(96,13.2);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#130F09").ss(12,1,1).p("AjNhqQgGBkA+A6QA4A1BVACQBUABA9gxQBDg3ADhb");
	this.shape_21.setTransform(96,12.5);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#130F09").ss(12,1,1).p("AjNhlQgGBfA+A4QA4AzBVABQBUABA9guQBDg1ADhX");
	this.shape_22.setTransform(96,12);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#130F09").ss(12,1,1).p("AjNhhQgGBcA+A1QA4AxBVABQBUACA9guQBDgyADhU");
	this.shape_23.setTransform(96,11.5);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#130F09").ss(12,1,1).p("AjNheQgGBYA+A0QA4AvBVACQBUABA9gsQBDgwADhS");
	this.shape_24.setTransform(96,11.2);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f().s("#130F09").ss(12,1,1).p("AjNhcQgGBXA+AyQA4AuBVACQBUABA9grQBDgvADhQ");
	this.shape_25.setTransform(96,10.9);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f().s("#130F09").ss(12,1,1).p("AjNhaQgGBVA+AxQA4AtBVACQBUABA9gqQBDgvADhN");
	this.shape_26.setTransform(96,10.7);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f().s("#130F09").ss(12,1,1).p("AjNhZQgGBUA+AwQA4AtBVACQBUABA9gqQBDguADhN");
	this.shape_27.setTransform(96,10.6);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f().s("#130F09").ss(12,1,1).p("AjNhZQgGBUA+AwQA4AtBVABQBUABA9gpQBDguADhM");
	this.shape_28.setTransform(96,10.5);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f().s("#130F09").ss(12,1,1).p("AjNhfQgGBaA+A0QA4AwBVABQBUABA9gsQBDgxADhS");
	this.shape_29.setTransform(96,11.3);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f().s("#130F09").ss(12,1,1).p("AjNhrQgGBlA+A6QA4A2BVABQBUACA9gyQBDg3ADhc");
	this.shape_30.setTransform(96,12.6);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f().s("#130F09").ss(12,1,1).p("AjNhzQgGBsA+A/QA4A6BVACQBUABA9g1QBDg7ADhk");
	this.shape_31.setTransform(96,13.6);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f().s("#130F09").ss(12,1,1).p("AjNh2QgGBvA+BBQA4A7BVACQBUABA9g2QBDg9ADhn");
	this.shape_32.setTransform(96,14);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f().s("#130F09").ss(12,1,1).p("AjNh5QgGByA+BCQA4A8BVACQBUACA9g4QBDg+ADhp");
	this.shape_33.setTransform(96,14.3);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f().s("#130F09").ss(12,1,1).p("AjNh6QgGBzA+BDQA4A9BVACQBUACA9g5QBDg/ADhq");
	this.shape_34.setTransform(96,14.5);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f().s("#130F09").ss(12,1,1).p("AjNh7QgGB0A+BEQA4A9BVACQBUACA9g5QBDhAADhr");
	this.shape_35.setTransform(96,14.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_18}]}).to({state:[{t:this.shape_18}]},12).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_27}]},1).to({state:[{t:this.shape_28}]},1).to({state:[{t:this.shape_28}]},22).to({state:[{t:this.shape_29}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_30}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_31}]},1).to({state:[{t:this.shape_32}]},1).to({state:[{t:this.shape_33}]},1).to({state:[{t:this.shape_34}]},1).to({state:[{t:this.shape_35}]},1).to({state:[{t:this.shape_18}]},1).wait(21));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-6,-6,128.7,82.1);


// stage content:
(lib.Helloween = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 7
	this.instance = new lib.Symbol11();
	this.instance.parent = this;
	this.instance.setTransform(685.4,242.2,1,1,0,0,0,58.4,35);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	// Layer 6
	this.instance_1 = new lib.Symbol2_1();
	this.instance_1.parent = this;
	this.instance_1.setTransform(537,168.5,1,1,0,0,0,61,25.5);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(1));

	// Layer 5
	this.instance_2 = new lib.Symbol4();
	this.instance_2.parent = this;
	this.instance_2.setTransform(832.4,350.9,1,1,0,0,0,37.6,46.8);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(1));

	// Layer 3
	this.instance_3 = new lib.pack();
	this.instance_3.parent = this;
	this.instance_3.setTransform(468,130);

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(1));

	// Layer 11
	this.instance_4 = new lib.Symbol5();
	this.instance_4.parent = this;
	this.instance_4.setTransform(593.7,306.7,1,1,0,0,0,41.5,20.3);

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(1));

	// Layer 2
	this.instance_5 = new lib.bg();
	this.instance_5.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(540,340,1080,680);
// library properties:
lib.properties = {
	id: 'A3D3E5204FE73E408EE741C307264568',
	width: 1080,
	height: 680,
	fps: 30,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: [
		{src:"assets/images/bat27.png", id:"bat"},
		{src:"assets/images/pack27.png", id:"pack"}
	],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['A3D3E5204FE73E408EE741C307264568'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}



})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;

$(function () {
    var canvas, stage, exportRoot, anim_container, dom_overlay_container, fnStartAnimation;
    function init() {
        canvas = document.getElementById("canvas27");
        anim_container = document.getElementById("animation_container27");
        dom_overlay_container = document.getElementById("dom_overlay_container27");
        var comp=AdobeAn.getComposition("A3D3E5204FE73E408EE741C307264568");
        var lib=comp.getLibrary();
        createjs.MotionGuidePlugin.install();
        var loader = new createjs.LoadQueue(false);
        loader.addEventListener("fileload", function(evt){handleFileLoad(evt,comp)});
        loader.addEventListener("complete", function(evt){handleComplete(evt,comp)});
        var lib=comp.getLibrary();
        loader.loadManifest(lib.properties.manifest);
    }
    function handleFileLoad(evt, comp) {
        var images=comp.getImages();
        if (evt && (evt.item.type == "image")) { images[evt.item.id] = evt.result; }
    }
    function handleComplete(evt,comp) {
        //This function is always called, irrespective of the content. You can use the variable "stage" after it is created in token create_stage.
        var lib=comp.getLibrary();
        var ss=comp.getSpriteSheet();
        var queue = evt.target;
        var ssMetadata = lib.ssMetadata;
        for(i=0; i<ssMetadata.length; i++) {
            ss[ssMetadata[i].name] = new createjs.SpriteSheet( {"images": [queue.getResult(ssMetadata[i].name)], "frames": ssMetadata[i].frames} )
        }
        exportRoot = new lib.Helloween();
        stage = new lib.Stage(canvas);
        //Registers the "tick" event listener.
        fnStartAnimation = function() {
            stage.addChild(exportRoot);
            createjs.Ticker.setFPS(lib.properties.fps);
            createjs.Ticker.addEventListener("tick", stage);
        }
        //Code to support hidpi screens and responsive scaling.
        function makeResponsive(isResp, respDim, isScale, scaleType) {
            var lastW, lastH, lastS=1;
            window.addEventListener('resize', resizeCanvas);
            resizeCanvas();
            function resizeCanvas() {
                var w = lib.properties.width, h = lib.properties.height;
                var iw = window.innerWidth, ih=window.innerHeight;
                var pRatio = window.devicePixelRatio || 1, xRatio=iw/w, yRatio=ih/h, sRatio=1;
                if(isResp) {
                    if((respDim=='width'&&lastW==iw) || (respDim=='height'&&lastH==ih)) {
                        sRatio = lastS;
                    }
                    else if(!isScale) {
                        if(iw<w || ih<h)
                            sRatio = Math.min(xRatio, yRatio);
                    }
                    else if(scaleType==1) {
                        sRatio = Math.min(xRatio, yRatio);
                    }
                    else if(scaleType==2) {
                        sRatio = Math.max(xRatio, yRatio);
                    }
                }
                canvas.width = w*pRatio*sRatio;
                canvas.height = h*pRatio*sRatio;
                canvas.style.width = dom_overlay_container.style.width = anim_container.style.width =  w*sRatio+'px';
                canvas.style.height = anim_container.style.height = dom_overlay_container.style.height = h*sRatio+'px';
                stage.scaleX = pRatio*sRatio;
                stage.scaleY = pRatio*sRatio;
                lastW = iw; lastH = ih; lastS = sRatio;
                stage.tickOnUpdate = false;
                stage.update();
                stage.tickOnUpdate = true;
            }
        }
        makeResponsive(true,'both',true,2);
        AdobeAn.compositionLoaded(lib.properties.id);
        fnStartAnimation();
    }
    init();
});