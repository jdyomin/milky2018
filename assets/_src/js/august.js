(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [];


// symbols:



(lib.bg_part = function() {
	this.initialize(img.bg_part);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,701,286);


(lib.pack = function() {
	this.initialize(img.pack);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,421,473);// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.Symbol7 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#121111").ss(12,1,1).p("AAEhIQgOBEAOBN");
	this.shape.setTransform(0.4,7.3);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol7, new cjs.Rectangle(-6,-6,12.8,26.7), null);


(lib.Symbol6 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#121111").ss(12,1,1).p("Ag8CCQAmiYBThr");
	this.shape.setTransform(6.1,13);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol6, new cjs.Rectangle(-6,-6,24.2,38), null);


(lib.Symbol5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#121111").ss(12,1,1).p("AhaBiQA4iBB9hC");
	this.shape.setTransform(9.1,9.8);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol5, new cjs.Rectangle(-6,-6,30.3,31.5), null);


(lib.Symbol3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#121111").ss(12,1,1).p("ABBAlQhWAbgrhp");
	this.shape.setTransform(6.5,4.2);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol3, new cjs.Rectangle(-6,-6,24.9,20.5), null);


(lib.Symbol2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#121111").ss(12,1,1).p("ABBCMQANg5iPje");
	this.shape.setTransform(6.6,14);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol2, new cjs.Rectangle(-6,-6,25.2,39.9), null);


(lib.Symbol1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#121111").ss(12,1,1).p("ABlCRQiIirhBh2");
	this.shape.setTransform(10.1,14.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol1, new cjs.Rectangle(-6,-6,32.3,41), null);


(lib.Символ2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#121111").ss(12,1,1).p("AAEh0Qg1CMBFBd");
	this.shape.setTransform(2,11.7);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Символ2, new cjs.Rectangle(-6,-6,16,35.3), null);


(lib.Символ1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#121111").ss(12,1,1).p("AgdheQgrDOBxgT");
	this.shape.setTransform(4.2,9.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Символ1, new cjs.Rectangle(-6,-6,20.3,31), null);


(lib.Symbol8 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.Symbol7();
	this.instance.parent = this;
	this.instance.setTransform(0.7,27.4,1,1,0,0,0,0.4,0);

	this.instance_1 = new lib.Symbol5();
	this.instance_1.parent = this;
	this.instance_1.setTransform(2,28.7,1,1,0,0,0,0,19.5);

	this.instance_2 = new lib.Symbol6();
	this.instance_2.parent = this;
	this.instance_2.setTransform(0,25.9,1,1,0,0,0,0,25.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_2},{t:this.instance_1},{t:this.instance}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol8, new cjs.Rectangle(-6,-6,32.3,54.1), null);


(lib.Symbol4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.Symbol3();
	this.instance.parent = this;
	this.instance.setTransform(11,27.3,1,1,0,0,0,6.5,4.3);

	this.instance_1 = new lib.Symbol1();
	this.instance_1.parent = this;
	this.instance_1.setTransform(10.1,15.8,1,1,0,0,0,10.1,14.5);

	this.instance_2 = new lib.Symbol2();
	this.instance_2.parent = this;
	this.instance_2.setTransform(15.3,14,1,1,0,0,0,6.6,14);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_2},{t:this.instance_1},{t:this.instance}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol4, new cjs.Rectangle(-6,-6,33.9,43.5), null);


(lib.Символ3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой_1
	this.instance = new lib.Символ1();
	this.instance.parent = this;
	this.instance.setTransform(8.2,9.5,1,1,0,0,0,4.2,9.5);

	this.instance_1 = new lib.Символ2();
	this.instance_1.parent = this;
	this.instance_1.setTransform(1.9,14,1,1,0,0,0,1.9,11.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_1},{t:this.instance}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Символ3, new cjs.Rectangle(-6,-6,24.3,37.6), null);


// stage content:
(lib.Main = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 18
	this.instance = new lib.Symbol4();
	this.instance.parent = this;
	this.instance.setTransform(1021.9,244.9,1,1,0,0,0,21.3,33.2);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(9).to({rotation:45,x:1056,y:244.3},8,cjs.Ease.get(1)).to({rotation:-3,x:1008.1},5,cjs.Ease.get(-1)).to({rotation:0,x:1021.9,y:244.9},3,cjs.Ease.get(1)).wait(58));

	// Layer 15
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#121111").ss(12,1,1).p("AkNJpQFzAMB2kkQCDlCjTp4");
	this.shape.setTransform(1006.3,301.9);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#121111").ss(12,1,1).p("AkUJqQFqgSB7keQCDk0h6pu");
	this.shape_1.setTransform(1007,301.9);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#121111").ss(12,1,1).p("AkiJqQFjgrB+kZQCFkogtpm");
	this.shape_2.setTransform(1008.4,301.9);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#121111").ss(12,1,1).p("Ak7JqQFdhBCBkUQCGkfATpe");
	this.shape_3.setTransform(1010.9,301.9);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#121111").ss(12,1,1).p("AlUJqQFYhSCDkRQCHkXBHpY");
	this.shape_4.setTransform(1013.5,301.9);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#121111").ss(12,1,1).p("AloJqQFVhgCEkOQCIkRBwpT");
	this.shape_5.setTransform(1015.5,301.9);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#121111").ss(12,1,1).p("Al3JqQFShqCGkMQCIkMCPpQ");
	this.shape_6.setTransform(1016.9,301.9);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#121111").ss(12,1,1).p("Al/JqQFQhwCHkKQCIkLCgpN");
	this.shape_7.setTransform(1017.7,301.9);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#121111").ss(12,1,1).p("AmCJqQFQhyCHkKQCIkJCmpN");
	this.shape_8.setTransform(1018,301.9);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#121111").ss(12,1,1).p("Al4JpQFRhtCGkLQCIkLCSpO");
	this.shape_9.setTransform(1017,302);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#121111").ss(12,1,1).p("AlbJmQFVhdCFkOQCHkSBWpO");
	this.shape_10.setTransform(1014.1,302.2);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#121111").ss(12,1,1).p("AkrJjQFchECBkTQCGkegNpQ");
	this.shape_11.setTransform(1009.3,302.6);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#121111").ss(12,1,1).p("AkQJeQFmghB9kbQCEksiYpT");
	this.shape_12.setTransform(1006.6,303.1);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#121111").ss(12,1,1).p("AkGJXQFyAMB3kkQCClBlMpV");
	this.shape_13.setTransform(1005.6,303.7);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#121111").ss(12,1,1).p("AkKJhQFzAMB2kkQCDlCkJpo");
	this.shape_14.setTransform(1006,302.7);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#121111").ss(12,1,1).p("AkMJnQFyAMB3kkQCClCjgp0");
	this.shape_15.setTransform(1006.2,302.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},9).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape}]},1).wait(58));

	// Layer 10
	this.instance_1 = new lib.Символ3();
	this.instance_1.parent = this;
	this.instance_1.setTransform(778.2,449.9,1,1,0,0,0,6.1,0);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(2).to({x:777.1,y:423.7},10,cjs.Ease.get(1)).wait(14).to({x:778.2,y:449.9},10,cjs.Ease.get(1)).wait(47));

	// Layer 7
	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#121111").ss(12,1,1).p("AgkoOQFcLTmuFK");
	this.shape_16.setTransform(786.3,399.5);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#121111").ss(12,1,1).p("AiCH0QHflsmOp8");
	this.shape_17.setTransform(787.6,396.9);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#121111").ss(12,1,1).p("AiNHdQIKmLm6ou");
	this.shape_18.setTransform(788.8,394.5);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#121111").ss(12,1,1).p("AiWHIQIxmlninq");
	this.shape_19.setTransform(789.8,392.4);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#121111").ss(12,1,1).p("AieG2QJSm8oEmv");
	this.shape_20.setTransform(790.7,390.6);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#121111").ss(12,1,1).p("AilGnQJunQohl9");
	this.shape_21.setTransform(791.5,389.1);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#121111").ss(12,1,1).p("AirGaQKFngo4lT");
	this.shape_22.setTransform(792.1,387.8);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#121111").ss(12,1,1).p("AivGQQKXntpLkz");
	this.shape_23.setTransform(792.6,386.9);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#121111").ss(12,1,1).p("AiyGKQKkn2pYkc");
	this.shape_24.setTransform(792.9,386.2);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f().s("#121111").ss(12,1,1).p("Ai0GFQKrn7pgkO");
	this.shape_25.setTransform(793.2,385.8);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f().s("#121111").ss(12,1,1).p("AhpmDQJiEKquH9");
	this.shape_26.setTransform(793.2,385.6);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f().s("#121111").ss(12,1,1).p("AipGeQJ9naowlh");
	this.shape_27.setTransform(791.9,388.3);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f().s("#121111").ss(12,1,1).p("AiVHLQIsmjndny");
	this.shape_28.setTransform(789.7,392.7);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f().s("#121111").ss(12,1,1).p("AiGHsQHul3mdpg");
	this.shape_29.setTransform(788,396);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f().s("#121111").ss(12,1,1).p("AiAH5QHXlnmGqK");
	this.shape_30.setTransform(787.4,397.3);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f().s("#121111").ss(12,1,1).p("Ah8ICQHFlZlzqq");
	this.shape_31.setTransform(786.9,398.3);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f().s("#121111").ss(12,1,1).p("Ah4IJQG4lRlnrB");
	this.shape_32.setTransform(786.6,399);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f().s("#121111").ss(12,1,1).p("Ah3INQGxlLlfrP");
	this.shape_33.setTransform(786.4,399.4);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_16}]}).to({state:[{t:this.shape_16}]},2).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_26}]},14).to({state:[{t:this.shape_27}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_28}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_29}]},1).to({state:[{t:this.shape_30}]},1).to({state:[{t:this.shape_31}]},1).to({state:[{t:this.shape_32}]},1).to({state:[{t:this.shape_33}]},1).to({state:[{t:this.shape_16}]},1).wait(47));

	// Layer 21
	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f().s("#121111").ss(12,1,1).p("AiygdQAdA/A9AYQA2AWA9gOQA5gNArgmQArgmAJgv");
	this.shape_34.setTransform(882.7,286.9);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f().s("#121111").ss(12,1,1).p("AiygZQAdA+A9AVQA2ASA8gPQA5gOArgjQAsgjAJgs");
	this.shape_35.setTransform(882.7,286.5);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f().s("#121111").ss(12,1,1).p("AiygWQAdA+A8ARQA2APA8gPQA6gPArghQArghAKgo");
	this.shape_36.setTransform(882.7,286.2);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f().s("#121111").ss(12,1,1).p("AiygTQAdA+A7AOQA3AMA7gQQA5gQAsgeQAsgfAKgl");
	this.shape_37.setTransform(882.7,285.9);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f().s("#121111").ss(12,1,1).p("AiygRQAdA+A7ALQA3AJA7gQQA5gRArgcQAsgcALgj");
	this.shape_38.setTransform(882.7,285.7);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f().s("#121111").ss(12,1,1).p("AiygPQAdA/A6AIQA3AGA7gRQA5gRAsgaQAsgbALgf");
	this.shape_39.setTransform(882.7,285.5);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f().s("#121111").ss(12,1,1).p("AiygNQAdA+A6AGQA3AEA7gRQA5gSAsgYQAsgZALge");
	this.shape_40.setTransform(882.7,285.3);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f().s("#121111").ss(12,1,1).p("AiygMQAdA/A5ADQA4ACA6gSQA5gSAsgWQAtgYALgb");
	this.shape_41.setTransform(882.7,285.2);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f().s("#121111").ss(12,1,1).p("AiygLQAdA/A5ABQA4ABA5gTQA6gSAsgVQAtgXALgZ");
	this.shape_42.setTransform(882.7,285.1);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f().s("#121111").ss(12,1,1).p("AiygKQAdA/A5gBQA3gBA6gSQA5gTAtgUQAsgVAMgY");
	this.shape_43.setTransform(882.7,285);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f().s("#121111").ss(12,1,1).p("AiygJQAdA+A4gBQA4gDA6gTQA5gTAtgTQAsgUAMgX");
	this.shape_44.setTransform(882.7,284.9);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f().s("#121111").ss(12,1,1).p("AiygJQAdA/A4gDQA4gEA5gTQA6gTAtgSQAsgUAMgV");
	this.shape_45.setTransform(882.7,284.9);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.f().s("#121111").ss(12,1,1).p("AiygIQAdA+A4gEQA4gEA5gTQA6gUAsgRQAsgTANgV");
	this.shape_46.setTransform(882.7,284.8);

	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.f().s("#121111").ss(12,1,1).p("AiygIQAdA/A4gFQA4gFA5gTQA6gUAsgRQAsgSANgU");
	this.shape_47.setTransform(882.7,284.8);

	this.shape_48 = new cjs.Shape();
	this.shape_48.graphics.f().s("#121111").ss(12,1,1).p("AiygIQAdA/A4gFQA4gFA5gUQA6gUAsgRQAsgSANgT");
	this.shape_48.setTransform(882.7,284.8);

	this.shape_49 = new cjs.Shape();
	this.shape_49.graphics.f().s("#121111").ss(12,1,1).p("AiygMQAdA+A5AEQA4ADA6gSQA5gSAsgWQAtgZALgc");
	this.shape_49.setTransform(882.7,285.2);

	this.shape_50 = new cjs.Shape();
	this.shape_50.graphics.f().s("#121111").ss(12,1,1).p("AiygRQAdA+A7AMQA3AJA7gQQA5gQAsgdQArgdALgj");
	this.shape_50.setTransform(882.7,285.7);

	this.shape_51 = new cjs.Shape();
	this.shape_51.graphics.f().s("#121111").ss(12,1,1).p("AiygaQAdA/A9AVQA2ASA8gOQA5gOArgkQAsgjAJgs");
	this.shape_51.setTransform(882.7,286.6);

	this.shape_52 = new cjs.Shape();
	this.shape_52.graphics.f().s("#121111").ss(12,1,1).p("AiygcQAdA+A9AYQA2AVA9gOQA5gOArglQArgmAJgu");
	this.shape_52.setTransform(882.7,286.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_34}]}).to({state:[{t:this.shape_34}]},25).to({state:[{t:this.shape_35}]},1).to({state:[{t:this.shape_36}]},1).to({state:[{t:this.shape_37}]},1).to({state:[{t:this.shape_38}]},1).to({state:[{t:this.shape_39}]},1).to({state:[{t:this.shape_40}]},1).to({state:[{t:this.shape_41}]},1).to({state:[{t:this.shape_42}]},1).to({state:[{t:this.shape_43}]},1).to({state:[{t:this.shape_44}]},1).to({state:[{t:this.shape_45}]},1).to({state:[{t:this.shape_46}]},1).to({state:[{t:this.shape_47}]},1).to({state:[{t:this.shape_48}]},1).to({state:[{t:this.shape_48}]},1).to({state:[{t:this.shape_48}]},17).to({state:[{t:this.shape_49}]},1).to({state:[{t:this.shape_50}]},1).to({state:[{t:this.shape_36}]},1).to({state:[{t:this.shape_51}]},1).to({state:[{t:this.shape_52}]},1).to({state:[{t:this.shape_34}]},1).wait(20));

	// Layer 20
	this.shape_53 = new cjs.Shape();
	this.shape_53.graphics.f().s("#121111").ss(11.5,1,1).p("Ai7A5QAPhTBEgqQA9glBLALQBLAKAsA0QAsAzgIBP");
	this.shape_53.setTransform(917.5,243.4);

	this.shape_54 = new cjs.Shape();
	this.shape_54.graphics.f().s("#121111").ss(11.5,1,1).p("Ai7AvQAPhFBEgiQA9gfBLAJQBLAJAsArQAsAqgIBB");
	this.shape_54.setTransform(917.5,245.1);

	this.shape_55 = new cjs.Shape();
	this.shape_55.graphics.f().s("#121111").ss(11.5,1,1).p("Ai7AnQAPg4BEgcQA9gZBLAHQBLAHAsAjQAsAigIA1");
	this.shape_55.setTransform(917.5,246.6);

	this.shape_56 = new cjs.Shape();
	this.shape_56.graphics.f().s("#121111").ss(11.5,1,1).p("Ai7AfQAPgtBEgXQA9gUBLAGQBLAGAsAcQAsAbgIAr");
	this.shape_56.setTransform(917.5,248);

	this.shape_57 = new cjs.Shape();
	this.shape_57.graphics.f().s("#121111").ss(11.5,1,1).p("Ai7AZQAPgkBEgSQA9gQBLAFQBLAEAsAXQAsAWgIAh");
	this.shape_57.setTransform(917.5,249.1);

	this.shape_58 = new cjs.Shape();
	this.shape_58.graphics.f().s("#121111").ss(11.5,1,1).p("Ai7AUQAPgcBEgPQA9gMBLADQBLAEAsASQAsARgIAa");
	this.shape_58.setTransform(917.5,250);

	this.shape_59 = new cjs.Shape();
	this.shape_59.graphics.f().s("#121111").ss(11.5,1,1).p("Ai7AQQAPgWBEgMQA9gKBLADQBLADAsAOQAsANgIAV");
	this.shape_59.setTransform(917.5,250.7);

	this.shape_60 = new cjs.Shape();
	this.shape_60.graphics.f().s("#121111").ss(11.5,1,1).p("Ai7ANQAPgSBEgJQA9gIBLACQBLACAsAMQAsAKgIAS");
	this.shape_60.setTransform(917.5,251.2);

	this.shape_61 = new cjs.Shape();
	this.shape_61.graphics.f().s("#121111").ss(11.5,1,1).p("Ai7ALQAPgQBEgHQA9gHBLACQBLABAsAKQAsAJgIAP");
	this.shape_61.setTransform(917.5,251.6);

	this.shape_62 = new cjs.Shape();
	this.shape_62.graphics.f().s("#121111").ss(11.5,1,1).p("Ai7AKQAPgOBEgIQA9gGBLACQBLABAsAKQAsAIgIAO");
	this.shape_62.setTransform(917.5,251.7);

	this.shape_63 = new cjs.Shape();
	this.shape_63.graphics.f().s("#121111").ss(11.5,1,1).p("Ai7AZQAPgkBEgSQA9gQBLAFQBLAEAsAXQAsAVgIAi");
	this.shape_63.setTransform(917.5,249.1);

	this.shape_64 = new cjs.Shape();
	this.shape_64.graphics.f().s("#121111").ss(11.5,1,1).p("Ai7AkQAPg1BEgaQA9gXBLAGQBLAHAsAhQAsAggIAy");
	this.shape_64.setTransform(917.5,247.1);

	this.shape_65 = new cjs.Shape();
	this.shape_65.graphics.f().s("#121111").ss(11.5,1,1).p("Ai7AtQAPhCBEghQA9gdBLAIQBLAIAsAqQAsAogIA+");
	this.shape_65.setTransform(917.5,245.5);

	this.shape_66 = new cjs.Shape();
	this.shape_66.graphics.f().s("#121111").ss(11.5,1,1).p("Ai7A0QAPhMBEgmQA9ghBLAJQBLAKAsAvQAsAugIBI");
	this.shape_66.setTransform(917.5,244.3);

	this.shape_67 = new cjs.Shape();
	this.shape_67.graphics.f().s("#121111").ss(11.5,1,1).p("Ai7A4QAPhSBEgoQA9glBLALQBLAKAsAzQAsAygIBM");
	this.shape_67.setTransform(917.5,243.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_53}]}).to({state:[{t:this.shape_53}]},25).to({state:[{t:this.shape_54}]},1).to({state:[{t:this.shape_55}]},1).to({state:[{t:this.shape_56}]},1).to({state:[{t:this.shape_57}]},1).to({state:[{t:this.shape_58}]},1).to({state:[{t:this.shape_59}]},1).to({state:[{t:this.shape_60}]},1).to({state:[{t:this.shape_61}]},1).to({state:[{t:this.shape_62}]},1).to({state:[{t:this.shape_62}]},23).to({state:[{t:this.shape_63}]},1).to({state:[{t:this.shape_64}]},1).to({state:[{t:this.shape_65}]},1).to({state:[{t:this.shape_66}]},1).to({state:[{t:this.shape_67}]},1).to({state:[{t:this.shape_53}]},1).wait(20));

	// Layer 19
	this.shape_68 = new cjs.Shape();
	this.shape_68.graphics.f().s("#121111").ss(11.5,1,1).p("Ai7A5QAPhTBEgqQA9glBLALQBLAKAsA0QAsAzgIBP");
	this.shape_68.setTransform(846.4,237.5);

	this.timeline.addTween(cjs.Tween.get(this.shape_68).wait(83));

	// Layer 6
	this.shape_69 = new cjs.Shape();
	this.shape_69.graphics.f().s("#121111").ss(12,1,1).p("AiygdQAdA/A9AYQA2AWA9gOQA5gNArgmQArgmAJgv");
	this.shape_69.setTransform(680.9,286.9);

	this.shape_70 = new cjs.Shape();
	this.shape_70.graphics.f().s("#121111").ss(12,1,1).p("AizgZQAdA/A9AYQA2AWA9gQQA6gOArgnQArgnAKgx");
	this.shape_70.setTransform(681,286.5);

	this.shape_71 = new cjs.Shape();
	this.shape_71.graphics.f().s("#121111").ss(12,1,1).p("Ai0gWQAdA/A+AYQA2AWA9gRQA7gQArgoQArgnAKg0");
	this.shape_71.setTransform(681,286.2);

	this.shape_72 = new cjs.Shape();
	this.shape_72.graphics.f().s("#121111").ss(12,1,1).p("Ai0gSQAcA+A+AZQA2AVA+gSQA7gSArgoQAqgnALg2");
	this.shape_72.setTransform(681.1,285.8);

	this.shape_73 = new cjs.Shape();
	this.shape_73.graphics.f().s("#121111").ss(12,1,1).p("Ai1gPQAdA+A9AZQA2AVA+gTQA9gTAqgpQArgoALg4");
	this.shape_73.setTransform(681.2,285.5);

	this.shape_74 = new cjs.Shape();
	this.shape_74.graphics.f().s("#121111").ss(12,1,1).p("Ai2gMQAdA+A9AZQA2AVA/gVQA9gUArgpQApgoANg6");
	this.shape_74.setTransform(681.3,285.2);

	this.shape_75 = new cjs.Shape();
	this.shape_75.graphics.f().s("#121111").ss(12,1,1).p("Ai2gKQAdA/A9AYQA2AWA/gWQA+gWAqgpQAqgoANg9");
	this.shape_75.setTransform(681.3,285);

	this.shape_76 = new cjs.Shape();
	this.shape_76.graphics.f().s("#121111").ss(12,1,1).p("Ai3gHQAdA+A9AZQA2AVA/gXQA/gWAqgqQApgpAOg+");
	this.shape_76.setTransform(681.4,284.7);

	this.shape_77 = new cjs.Shape();
	this.shape_77.graphics.f().s("#121111").ss(12,1,1).p("Ai4gFQAdA+A+AZQA2AVA/gXQA/gYAqgqQAqgpANhA");
	this.shape_77.setTransform(681.4,284.5);

	this.shape_78 = new cjs.Shape();
	this.shape_78.graphics.f().s("#121111").ss(12,1,1).p("Ai4gEQAdA/A9AYQA2AWBAgZQBAgYApgqQApgqAPhB");
	this.shape_78.setTransform(681.5,284.4);

	this.shape_79 = new cjs.Shape();
	this.shape_79.graphics.f().s("#121111").ss(12,1,1).p("Ai4gCQAdA/A9AYQA2AWBAgZQBAgaApgqQApgqAPhC");
	this.shape_79.setTransform(681.5,284.2);

	this.shape_80 = new cjs.Shape();
	this.shape_80.graphics.f().s("#121111").ss(12,1,1).p("Ai5AAQAdA+A+AZQA2AVBAgaQBAgZApgrQAqgqAPhD");
	this.shape_80.setTransform(681.5,284);

	this.shape_81 = new cjs.Shape();
	this.shape_81.graphics.f().s("#121111").ss(12,1,1).p("Ai5AAQAdA/A+AZQA2AVBAgaQBAgaAqgrQApgqAPhE");
	this.shape_81.setTransform(681.6,283.9);

	this.shape_82 = new cjs.Shape();
	this.shape_82.graphics.f().s("#121111").ss(12,1,1).p("Ai5ABQAdA/A9AZQA2AVBBgbQBAgaAqgrQApgrAPhE");
	this.shape_82.setTransform(681.6,283.8);

	this.shape_83 = new cjs.Shape();
	this.shape_83.graphics.f().s("#121111").ss(12,1,1).p("Ai5ABQAcBAA+AYQA2AWBBgbQBAgbApgsQAqgqAPhF");
	this.shape_83.setTransform(681.6,283.8);

	this.shape_84 = new cjs.Shape();
	this.shape_84.graphics.f().s("#121111").ss(12,1,1).p("Ai6ACQAdBAA+AYQA2AWBAgcQBBgbAqgrQApgrAQhF");
	this.shape_84.setTransform(681.6,283.7);

	this.shape_85 = new cjs.Shape();
	this.shape_85.graphics.f().s("#121111").ss(12,1,1).p("Ai6ADQAdA/A+AZQA2AVBAgbQBBgcAqgrQApgrAQhG");
	this.shape_85.setTransform(681.6,283.6);

	this.shape_86 = new cjs.Shape();
	this.shape_86.graphics.f().s("#121111").ss(12,1,1).p("Ai3gIQAdA+A9AZQA2AVA/gWQA+gXArgpQApgpAOg+");
	this.shape_86.setTransform(681.4,284.8);

	this.shape_87 = new cjs.Shape();
	this.shape_87.graphics.f().s("#121111").ss(12,1,1).p("Ai1gRQAdA/A+AYQA2AWA+gTQA8gSAqgoQArgoALg3");
	this.shape_87.setTransform(681.1,285.7);

	this.shape_88 = new cjs.Shape();
	this.shape_88.graphics.f().s("#121111").ss(12,1,1).p("AizgXQAdA+A9AZQA2AVA9gQQA7gPAqgnQAsgnAJgz");
	this.shape_88.setTransform(681,286.3);

	this.shape_89 = new cjs.Shape();
	this.shape_89.graphics.f().s("#121111").ss(12,1,1).p("AiygbQAdA+A9AZQA2AVA9gOQA5gOArgmQAsgmAIgw");
	this.shape_89.setTransform(680.9,286.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_69}]}).to({state:[{t:this.shape_69}]},53).to({state:[{t:this.shape_70}]},1).to({state:[{t:this.shape_71}]},1).to({state:[{t:this.shape_72}]},1).to({state:[{t:this.shape_73}]},1).to({state:[{t:this.shape_74}]},1).to({state:[{t:this.shape_75}]},1).to({state:[{t:this.shape_76}]},1).to({state:[{t:this.shape_77}]},1).to({state:[{t:this.shape_78}]},1).to({state:[{t:this.shape_79}]},1).to({state:[{t:this.shape_80}]},1).to({state:[{t:this.shape_81}]},1).to({state:[{t:this.shape_82}]},1).to({state:[{t:this.shape_83}]},1).to({state:[{t:this.shape_84}]},1).to({state:[{t:this.shape_85}]},1).to({state:[{t:this.shape_85}]},1).to({state:[{t:this.shape_85}]},7).to({state:[{t:this.shape_86}]},1).to({state:[{t:this.shape_87}]},1).to({state:[{t:this.shape_88}]},1).to({state:[{t:this.shape_89}]},1).to({state:[{t:this.shape_69}]},1).wait(1));

	// Layer 5
	this.shape_90 = new cjs.Shape();
	this.shape_90.graphics.f().s("#121111").ss(11.5,1,1).p("Ai7BRQAPh3BEg7QA9g1BLAPQBLAPAsBKQAsBJgIBw");
	this.shape_90.setTransform(715.6,239.3);

	this.shape_91 = new cjs.Shape();
	this.shape_91.graphics.f().s("#121111").ss(11.5,1,1).p("Ai7BMQAPhvBEg3QA9gxBLANQBLAOAsBFQAsBEgIBp");
	this.shape_91.setTransform(715.6,240.2);

	this.shape_92 = new cjs.Shape();
	this.shape_92.graphics.f().s("#121111").ss(11.5,1,1).p("Ai7BHQAPhoBEgzQA9guBLANQBLANAsBAQAsBAgIBh");
	this.shape_92.setTransform(715.6,241.1);

	this.shape_93 = new cjs.Shape();
	this.shape_93.graphics.f().s("#121111").ss(11.5,1,1).p("Ai7BDQAPhiBEgwQA9gsBLANQBLAMAsA9QAsA7gIBc");
	this.shape_93.setTransform(715.6,241.8);

	this.shape_94 = new cjs.Shape();
	this.shape_94.graphics.f().s("#121111").ss(11.5,1,1).p("Ai7BAQAPhdBEguQA9gpBLAMQBLALAsA6QAsA5gIBW");
	this.shape_94.setTransform(715.6,242.3);

	this.shape_95 = new cjs.Shape();
	this.shape_95.graphics.f().s("#121111").ss(11.5,1,1).p("Ai7A8QAPhYBEgsQA9gnBLALQBLALAsA3QAsA2gIBT");
	this.shape_95.setTransform(715.6,242.8);

	this.shape_96 = new cjs.Shape();
	this.shape_96.graphics.f().s("#121111").ss(11.5,1,1).p("Ai7A7QAPhWBEgrQA9gmBLALQBLALAsA1QAsA1gIBQ");
	this.shape_96.setTransform(715.6,243.1);

	this.shape_97 = new cjs.Shape();
	this.shape_97.graphics.f().s("#121111").ss(11.5,1,1).p("Ai7A5QAPhUBEgpQA9gmBLALQBLALAsA0QAsAzgIBP");
	this.shape_97.setTransform(715.6,243.3);

	this.shape_98 = new cjs.Shape();
	this.shape_98.graphics.f().s("#121111").ss(11.5,1,1).p("Ai7A5QAPhTBEgqQA9glBLALQBLAKAsA0QAsAzgIBP");
	this.shape_98.setTransform(715.6,243.4);

	this.shape_99 = new cjs.Shape();
	this.shape_99.graphics.f().s("#121111").ss(11.5,1,1).p("Ai7A/QAPhcBEguQA9gpBLAMQBLAMAsA5QAsA4gIBW");
	this.shape_99.setTransform(715.6,242.4);

	this.shape_100 = new cjs.Shape();
	this.shape_100.graphics.f().s("#121111").ss(11.5,1,1).p("Ai7BEQAPhjBEgyQA9gsBLANQBLAMAsA+QAsA9gIBd");
	this.shape_100.setTransform(715.6,241.6);

	this.shape_101 = new cjs.Shape();
	this.shape_101.graphics.f().s("#121111").ss(11.5,1,1).p("Ai7BIQAPhqBEg0QA9guBLANQBLANAsBBQAsBBgIBj");
	this.shape_101.setTransform(715.6,240.9);

	this.shape_102 = new cjs.Shape();
	this.shape_102.graphics.f().s("#121111").ss(11.5,1,1).p("Ai7BMQAPhvBEg3QA9gxBLAOQBLAOAsBFQAsBEgIBn");
	this.shape_102.setTransform(715.6,240.3);

	this.shape_103 = new cjs.Shape();
	this.shape_103.graphics.f().s("#121111").ss(11.5,1,1).p("Ai7BOQAPhyBEg5QA9gzBLAPQBLAOAsBHQAsBGgIBs");
	this.shape_103.setTransform(715.6,239.8);

	this.shape_104 = new cjs.Shape();
	this.shape_104.graphics.f().s("#121111").ss(11.5,1,1).p("Ai7BQQAPh1BEg6QA9g0BLAOQBLAPAsBJQAsBIgIBu");
	this.shape_104.setTransform(715.6,239.5);

	this.shape_105 = new cjs.Shape();
	this.shape_105.graphics.f().s("#121111").ss(11.5,1,1).p("Ai7BRQAPh3BEg7QA9g0BLAPQBLAOAsBKQAsBJgIBw");
	this.shape_105.setTransform(715.6,239.3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_90}]}).to({state:[{t:this.shape_90}]},7).to({state:[{t:this.shape_91}]},1).to({state:[{t:this.shape_92}]},1).to({state:[{t:this.shape_93}]},1).to({state:[{t:this.shape_94}]},1).to({state:[{t:this.shape_95}]},1).to({state:[{t:this.shape_96}]},1).to({state:[{t:this.shape_97}]},1).to({state:[{t:this.shape_98}]},1).to({state:[{t:this.shape_98}]},46).to({state:[{t:this.shape_99}]},1).to({state:[{t:this.shape_100}]},1).to({state:[{t:this.shape_101}]},1).to({state:[{t:this.shape_102}]},1).to({state:[{t:this.shape_103}]},1).to({state:[{t:this.shape_104}]},1).to({state:[{t:this.shape_105}]},1).to({state:[{t:this.shape_90}]},1).wait(14));

	// Layer 4
	this.shape_106 = new cjs.Shape();
	this.shape_106.graphics.f().s("#121111").ss(11.5,1,1).p("Ai7BRQAPh3BEg7QA9g1BLAPQBLAPAsBKQAsBJgIBw");
	this.shape_106.setTransform(644.5,230.9);

	this.shape_107 = new cjs.Shape();
	this.shape_107.graphics.f().s("#121111").ss(11.5,1,1).p("Ai7BMQAPhvBEg3QA9gxBLAOQBLAOAsBFQAsBEgIBo");
	this.shape_107.setTransform(644.5,232.4);

	this.shape_108 = new cjs.Shape();
	this.shape_108.graphics.f().s("#121111").ss(11.5,1,1).p("Ai7BHQAPhoBEgzQA9guBLANQBLANAsBAQAsBAgIBh");
	this.shape_108.setTransform(644.5,233.8);

	this.shape_109 = new cjs.Shape();
	this.shape_109.graphics.f().s("#121111").ss(11.5,1,1).p("Ai7BDQAPhiBEgwQA9grBLAMQBLAMAsA9QAsA8gIBb");
	this.shape_109.setTransform(644.5,234.9);

	this.shape_110 = new cjs.Shape();
	this.shape_110.graphics.f().s("#121111").ss(11.5,1,1).p("Ai7A/QAPhcBEguQA9gpBLAMQBLALAsA6QAsA4gIBX");
	this.shape_110.setTransform(644.5,235.9);

	this.shape_111 = new cjs.Shape();
	this.shape_111.graphics.f().s("#121111").ss(11.5,1,1).p("Ai7A8QAPhYBEgsQA9gnBLALQBLALAsA3QAsA2gIBT");
	this.shape_111.setTransform(644.5,236.6);

	this.shape_112 = new cjs.Shape();
	this.shape_112.graphics.f().s("#121111").ss(11.5,1,1).p("Ai7A7QAPhWBEgrQA9gmBLALQBLALAsA1QAsA1gIBQ");
	this.shape_112.setTransform(644.5,237.1);

	this.shape_113 = new cjs.Shape();
	this.shape_113.graphics.f().s("#121111").ss(11.5,1,1).p("Ai7A6QAPhUBEgqQA9glBLAKQBLALAsA0QAsAzgIBP");
	this.shape_113.setTransform(644.5,237.4);

	this.shape_114 = new cjs.Shape();
	this.shape_114.graphics.f().s("#121111").ss(11.5,1,1).p("Ai7A5QAPhTBEgqQA9glBLALQBLAKAsA0QAsAzgIBP");
	this.shape_114.setTransform(644.5,237.5);

	this.shape_115 = new cjs.Shape();
	this.shape_115.graphics.f().s("#121111").ss(11.5,1,1).p("Ai7A/QAPhcBEguQA9goBLALQBLAMAsA5QAsA4gIBX");
	this.shape_115.setTransform(644.5,236);

	this.shape_116 = new cjs.Shape();
	this.shape_116.graphics.f().s("#121111").ss(11.5,1,1).p("Ai7BEQAPhjBEgyQA9gsBLANQBLAMAsA+QAsA9gIBd");
	this.shape_116.setTransform(644.5,234.6);

	this.shape_117 = new cjs.Shape();
	this.shape_117.graphics.f().s("#121111").ss(11.5,1,1).p("Ai7BIQAPhpBEg0QA9gvBLANQBLANAsBCQAsBAgIBj");
	this.shape_117.setTransform(644.5,233.5);

	this.shape_118 = new cjs.Shape();
	this.shape_118.graphics.f().s("#121111").ss(11.5,1,1).p("Ai7BLQAPhuBEg3QA9gxBLAOQBLAOAsBEQAsBEgIBo");
	this.shape_118.setTransform(644.5,232.6);

	this.shape_119 = new cjs.Shape();
	this.shape_119.graphics.f().s("#121111").ss(11.5,1,1).p("Ai7BOQAPhyBEg5QA9gzBLAPQBLAOAsBHQAsBGgIBs");
	this.shape_119.setTransform(644.5,231.8);

	this.shape_120 = new cjs.Shape();
	this.shape_120.graphics.f().s("#121111").ss(11.5,1,1).p("Ai7BQQAPh1BEg6QA9g0BLAOQBLAPAsBJQAsBIgIBu");
	this.shape_120.setTransform(644.5,231.3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_106,p:{y:230.9}}]}).to({state:[{t:this.shape_106,p:{y:230.9}}]},7).to({state:[{t:this.shape_107}]},1).to({state:[{t:this.shape_108}]},1).to({state:[{t:this.shape_109}]},1).to({state:[{t:this.shape_110}]},1).to({state:[{t:this.shape_111}]},1).to({state:[{t:this.shape_112}]},1).to({state:[{t:this.shape_113}]},1).to({state:[{t:this.shape_114}]},1).to({state:[{t:this.shape_114}]},46).to({state:[{t:this.shape_115}]},1).to({state:[{t:this.shape_116}]},1).to({state:[{t:this.shape_117}]},1).to({state:[{t:this.shape_118}]},1).to({state:[{t:this.shape_119}]},1).to({state:[{t:this.shape_120}]},1).to({state:[{t:this.shape_106,p:{y:231}}]},1).to({state:[{t:this.shape_106,p:{y:230.9}}]},1).wait(14));

	// Layer 22
	this.instance_2 = new lib.pack();
	this.instance_2.parent = this;
	this.instance_2.setTransform(732,153);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(83));

	// Layer 3
	this.instance_3 = new lib.pack();
	this.instance_3.parent = this;
	this.instance_3.setTransform(530,153);

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(83));

	// Layer 11
	this.instance_4 = new lib.Symbol8();
	this.instance_4.parent = this;
	this.instance_4.setTransform(590,248.2,1,1,0,0,0,-2.2,27.9);

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(39).to({regX:-2.3,regY:27.8,rotation:-45,x:535.6,y:249.7},8,cjs.Ease.get(1)).to({regX:-2.1,regY:27.9,rotation:15,x:602,y:248.2},5,cjs.Ease.get(-1)).to({regX:-2.2,rotation:0,x:590},3,cjs.Ease.get(1)).wait(28));

	// Layer 12
	this.shape_121 = new cjs.Shape();
	this.shape_121.graphics.f().s("#121111").ss(12,1,1).p("AE3JlQnihWhtkWQh6k4Fsol");
	this.shape_121.setTransform(596.6,305.1);

	this.shape_122 = new cjs.Shape();
	this.shape_122.graphics.f().s("#121111").ss(12,1,1).p("AigpcQj1IWCHErQB9ESHQBn");
	this.shape_122.setTransform(595.8,305.9);

	this.shape_123 = new cjs.Shape();
	this.shape_123.graphics.f().s("#121111").ss(12,1,1).p("AkFpWQiNIJCTEgQCLEOG/B2");
	this.shape_123.setTransform(594.6,306.6);

	this.shape_124 = new cjs.Shape();
	this.shape_124.graphics.f().s("#121111").ss(12,1,1).p("AlQpQQg2H9CdEYQCXEKGxCC");
	this.shape_124.setTransform(592.6,307.1);

	this.shape_125 = new cjs.Shape();
	this.shape_125.graphics.f().s("#121111").ss(12,1,1).p("Al+pMQARH0ClEQQCiEIGlCM");
	this.shape_125.setTransform(589.4,307.6);

	this.shape_126 = new cjs.Shape();
	this.shape_126.graphics.f().s("#121111").ss(12,1,1).p("AmcpIQBIHtCsEKQCoEGGdCU");
	this.shape_126.setTransform(586.4,308);

	this.shape_127 = new cjs.Shape();
	this.shape_127.graphics.f().s("#121111").ss(12,1,1).p("AmypFQBxHnCwEGQCtEFGXCZ");
	this.shape_127.setTransform(584.2,308.2);

	this.shape_128 = new cjs.Shape();
	this.shape_128.graphics.f().s("#121111").ss(12,1,1).p("Am/pEQCIHlCzEDQCxEEGTCd");
	this.shape_128.setTransform(582.9,308.4);

	this.shape_129 = new cjs.Shape();
	this.shape_129.graphics.f().s("#121111").ss(12,1,1).p("AHEJEQmSieiykDQizkDiQnj");
	this.shape_129.setTransform(582.5,308.4);

	this.shape_130 = new cjs.Shape();
	this.shape_130.graphics.f().s("#121111").ss(12,1,1).p("Am2pEQB5HjCwEFQCvEFGVCc");
	this.shape_130.setTransform(583.8,308.4);

	this.shape_131 = new cjs.Shape();
	this.shape_131.graphics.f().s("#121111").ss(12,1,1).p("AmOpFQAzHgCnEMQCmEMGdCT");
	this.shape_131.setTransform(587.8,308.2);

	this.shape_132 = new cjs.Shape();
	this.shape_132.graphics.f().s("#121111").ss(12,1,1).p("AlCpHQhCHcCXEWQCXEYGtCF");
	this.shape_132.setTransform(593.4,308);

	this.shape_133 = new cjs.Shape();
	this.shape_133.graphics.f().s("#121111").ss(12,1,1).p("AilpLQjmHXCBEmQCBEnHDBz");
	this.shape_133.setTransform(596.3,307.7);

	this.shape_134 = new cjs.Shape();
	this.shape_134.graphics.f().s("#121111").ss(12,1,1).p("AEsJQQnfhahlk7Qhlk6G3nQ");
	this.shape_134.setTransform(597.7,307.2);

	this.shape_135 = new cjs.Shape();
	this.shape_135.graphics.f().s("#121111").ss(12,1,1).p("AAEpbQmNH/BxE5QBpEnHhBY");
	this.shape_135.setTransform(597.1,306.1);

	this.shape_136 = new cjs.Shape();
	this.shape_136.graphics.f().s("#121111").ss(12,1,1).p("AgbpiQl1IcB4E5QBsEZHiBX");
	this.shape_136.setTransform(596.7,305.4);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_121}]}).to({state:[{t:this.shape_121}]},39).to({state:[{t:this.shape_122}]},1).to({state:[{t:this.shape_123}]},1).to({state:[{t:this.shape_124}]},1).to({state:[{t:this.shape_125}]},1).to({state:[{t:this.shape_126}]},1).to({state:[{t:this.shape_127}]},1).to({state:[{t:this.shape_128}]},1).to({state:[{t:this.shape_129}]},1).to({state:[{t:this.shape_130}]},1).to({state:[{t:this.shape_131}]},1).to({state:[{t:this.shape_132}]},1).to({state:[{t:this.shape_133}]},1).to({state:[{t:this.shape_134}]},1).to({state:[{t:this.shape_135}]},1).to({state:[{t:this.shape_136}]},1).to({state:[{t:this.shape_121}]},1).wait(28));

	// Layer 2
	this.instance_5 = new lib.bg_part();
	this.instance_5.parent = this;
	this.instance_5.setTransform(379,394);

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(83));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(540,340,1153,680);
// library properties:
lib.properties = {
	id: 'B38583368D43DC4AA2355CAB60ADC773',
	width: 1080,
	height: 680,
	fps: 30,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: [
		{src:"assets/images/pack19.png", id:"pack"}
	],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['B38583368D43DC4AA2355CAB60ADC773'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}



})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;

$(function () {
    var canvas, stage, exportRoot, anim_container, dom_overlay_container, fnStartAnimation;
    function init() {
        canvas = document.getElementById("canvas19");
        anim_container = document.getElementById("animation_container19");
        dom_overlay_container = document.getElementById("dom_overlay_container19");
        var comp=AdobeAn.getComposition("B38583368D43DC4AA2355CAB60ADC773");
        var lib=comp.getLibrary();
        var loader = new createjs.LoadQueue(false);
        loader.addEventListener("fileload", function(evt){handleFileLoad(evt,comp)});
        loader.addEventListener("complete", function(evt){handleComplete(evt,comp)});
        var lib=comp.getLibrary();
        loader.loadManifest(lib.properties.manifest);
    }
    function handleFileLoad(evt, comp) {
        var images=comp.getImages();
        if (evt && (evt.item.type == "image")) { images[evt.item.id] = evt.result; }
    }
    function handleComplete(evt,comp) {
        //This function is always called, irrespective of the content. You can use the variable "stage" after it is created in token create_stage.
        var lib=comp.getLibrary();
        var ss=comp.getSpriteSheet();
        var queue = evt.target;
        var ssMetadata = lib.ssMetadata;
        for(i=0; i<ssMetadata.length; i++) {
            ss[ssMetadata[i].name] = new createjs.SpriteSheet( {"images": [queue.getResult(ssMetadata[i].name)], "frames": ssMetadata[i].frames} )
        }
        exportRoot = new lib.Main();
        stage = new lib.Stage(canvas);
        //Registers the "tick" event listener.
        fnStartAnimation = function() {
            stage.addChild(exportRoot);
            createjs.Ticker.setFPS(lib.properties.fps);
            createjs.Ticker.addEventListener("tick", stage);
        }
        //Code to support hidpi screens and responsive scaling.
        function makeResponsive(isResp, respDim, isScale, scaleType) {
            var lastW, lastH, lastS=1;
            window.addEventListener('resize', resizeCanvas);
            resizeCanvas();
            function resizeCanvas() {
                var w = lib.properties.width, h = lib.properties.height;
                var iw = window.innerWidth, ih=window.innerHeight;
                var pRatio = window.devicePixelRatio || 1, xRatio=iw/w, yRatio=ih/h, sRatio=1;
                if(isResp) {
                    if((respDim=='width'&&lastW==iw) || (respDim=='height'&&lastH==ih)) {
                        sRatio = lastS;
                    }
                    else if(!isScale) {
                        if(iw<w || ih<h)
                            sRatio = Math.min(xRatio, yRatio);
                    }
                    else if(scaleType==1) {
                        sRatio = Math.min(xRatio, yRatio);
                    }
                    else if(scaleType==2) {
                        sRatio = Math.max(xRatio, yRatio);
                    }
                }
                canvas.width = w*pRatio*sRatio;
                canvas.height = h*pRatio*sRatio;
                canvas.style.width = dom_overlay_container.style.width = anim_container.style.width =  w*sRatio+'px';
                canvas.style.height = anim_container.style.height = dom_overlay_container.style.height = h*sRatio+'px';
                stage.scaleX = pRatio*sRatio;
                stage.scaleY = pRatio*sRatio;
                lastW = iw; lastH = ih; lastS = sRatio;
                stage.tickOnUpdate = false;
                stage.update();
                stage.tickOnUpdate = true;
            }
        }
        makeResponsive(true,'both',true,2);
        AdobeAn.compositionLoaded(lib.properties.id);
        fnStartAnimation();
    }
    init();
});