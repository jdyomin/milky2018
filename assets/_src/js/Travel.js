(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [];


// symbols:



(lib.bag = function() {
	this.initialize(img.bag);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,259,463);


(lib.bg_part = function() {
	this.initialize(img.bg_part);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,685,267);


(lib.hand2 = function() {
	this.initialize(img.hand2);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,39,30);


(lib.pack = function() {
	this.initialize(img.pack);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,347,479);// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.Symbol7 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(11.5,1,1).p("AhpAZQApgpCqgI");
	this.shape.setTransform(10.6,2.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol7, new cjs.Rectangle(-5.7,-5.7,32.8,16.6), null);


(lib.Symbol6 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(11.5,1,1).p("AgPCMQgTiCA3iV");
	this.shape.setTransform(2.1,14);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol6, new cjs.Rectangle(-5.7,-5.7,15.7,39.6), null);


(lib.Symbol4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.hand2();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol4, new cjs.Rectangle(0,0,39,30), null);


(lib.Symbol3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.bag();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol3, new cjs.Rectangle(0,0,259,463), null);


(lib.Symbol2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#F51431").s().p("AAAAqQgRAAgNgMQgMgMAAgSQAAgQAMgNQANgNARAAIADAAQAPABALAMQANANAAAQQAAASgNAMQgLAMgPAAIgDAAg");
	this.shape.setTransform(4.3,4.3);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol2, new cjs.Rectangle(0,0,8.5,8.5), null);


(lib.Symbol9 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Symbol 7
	this.instance = new lib.Symbol7();
	this.instance.parent = this;
	this.instance.setTransform(14.8,29.2,1,1,0,0,0,10.6,2.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	// Symbol 6
	this.instance_1 = new lib.Symbol6();
	this.instance_1.parent = this;
	this.instance_1.setTransform(14,14,1,1,0,0,0,2,14);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(1));

	// Layer 4
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(11.5,1,1).p("AAdCGQhAh1AIiW");
	this.shape.setTransform(2.9,17.9);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol9, new cjs.Rectangle(-5.7,-5.7,36.9,43.2), null);


(lib.Symbol8 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Symbol 7
	this.instance = new lib.Symbol9();
	this.instance.parent = this;
	this.instance.setTransform(97.2,48.3,1,1,0,0,0,3.7,32.1);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(14).to({regX:3.8,rotation:30,x:121.8,y:45.9},15,cjs.Ease.get(1)).wait(10).to({regX:3.7,rotation:0,x:97.2,y:48.3},15,cjs.Ease.get(1)).wait(27));

	// Layer 5
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(11.5,1,1).p("AnumQQBRKyDZBeQDYBeHbmb");
	this.shape.setTransform(49.5,40.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(11.5,1,1).p("An8mJQBlKqDaBYQDaBZHfmG");
	this.shape_1.setTransform(51,39.2);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(11.5,1,1).p("AoJmCQB3KiDcBTQDcBTHklx");
	this.shape_2.setTransform(52.4,38.4);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(11.5,1,1).p("AoVl7QCIKaDeBPQDdBOHolf");
	this.shape_3.setTransform(53.7,37.6);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(11.5,1,1).p("Aogl1QCYKTDfBLQDfBJHrlN");
	this.shape_4.setTransform(54.9,36.8);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(11.5,1,1).p("AoqlvQClKNDhBGQDgBFHvk9");
	this.shape_5.setTransform(56,36.2);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(11.5,1,1).p("AozlqQCyKHDiBCQDhBCHykv");
	this.shape_6.setTransform(57,35.6);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(11.5,1,1).p("Ao7lmQC9KDDkA/QDhA+H1ki");
	this.shape_7.setTransform(57.9,35);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(11.5,1,1).p("ApCliQDIJ+DjA8QDjA8H3kX");
	this.shape_8.setTransform(58.7,34.5);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(11.5,1,1).p("ApIleQDQJ6DlA5QDjA5H5kN");
	this.shape_9.setTransform(59.4,34.1);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(11.5,1,1).p("ApOlbQDYJ2DlA4QDlA3H7kF");
	this.shape_10.setTransform(60,33.8);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(11.5,1,1).p("ApSlZQDeJ0DmA2QDlA1H8j+");
	this.shape_11.setTransform(60.4,33.5);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(11.5,1,1).p("ApVlXQDiJyDnA0QDlA0H9j5");
	this.shape_12.setTransform(60.8,33.3);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(11.5,1,1).p("ApYlWQDmJxDnAzQDlAzH/j1");
	this.shape_13.setTransform(61.1,33.1);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(11.5,1,1).p("ApZlVQDoJwDmAyQDmAyH/jy");
	this.shape_14.setTransform(61.2,33);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(11.5,1,1).p("ApalVQDpJwDnAyQDmAyH/jy");
	this.shape_15.setTransform(61.3,33);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(11.5,1,1).p("ApMldQDVJ4DmA5QDkA3H6kI");
	this.shape_16.setTransform(59.7,33.9);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(11.5,1,1).p("Ao/lkQDDKADkA+QDiA9H2kc");
	this.shape_17.setTransform(58.3,34.8);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#000000").ss(11.5,1,1).p("AoolwQCjKODgBHQDfBGHvlA");
	this.shape_18.setTransform(55.8,36.3);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#000000").ss(11.5,1,1).p("Aoel2QCVKUDfBLQDeBLHrlQ");
	this.shape_19.setTransform(54.7,37);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#000000").ss(11.5,1,1).p("AoMl/QB8KfDdBSQDbBRHllr");
	this.shape_20.setTransform(52.8,38.1);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#000000").ss(11.5,1,1).p("AoFmDQByKjDcBVQDbBUHil2");
	this.shape_21.setTransform(52,38.6);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#000000").ss(11.5,1,1).p("An/mHQBpKnDbBYQDaBXHhmB");
	this.shape_22.setTransform(51.4,39);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#000000").ss(11.5,1,1).p("An6mKQBiKrDbBaQDZBZHfmJ");
	this.shape_23.setTransform(50.8,39.3);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#000000").ss(11.5,1,1).p("An2mMQBcKtDbBcQDYBaHemP");
	this.shape_24.setTransform(50.3,39.6);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f().s("#000000").ss(11.5,1,1).p("AnymOQBXKvDaBdQDYBcHcmV");
	this.shape_25.setTransform(50,39.9);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f().s("#000000").ss(11.5,1,1).p("AnwmPQBUKwDZBeQDYBdHcmY");
	this.shape_26.setTransform(49.7,40);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f().s("#000000").ss(11.5,1,1).p("AnumQQBRKxDaBfQDXBeHbmb");
	this.shape_27.setTransform(49.5,40.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},14).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_15}]},10).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_27}]},1).to({state:[{t:this.shape}]},1).wait(27));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-5.7,-5.7,130.4,91.8);


(lib.Symbol1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#1B1814").ss(11.5,1,1).p("AimhjQgJBcA0A4QAuAzBGAAQBEAAAxgwQA3g0ADhR");
	this.shape.setTransform(188.8,-96.9);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(40));

	// Layer 2
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#1B1814").ss(11.5,1,1).p("Ai2hsQgJBlA4A9QAyA3BNAAQBKAAA3g0QA7g5AEhY");
	this.shape_1.setTransform(120.6,-96.5);

	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(40));

	// Layer 3
	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#1B1814").ss(11,1,1).p("AkLhfQBDDEDRgGQBWgDBJgrQBKgtAahG");
	this.shape_2.setTransform(156,-52.6);

	this.timeline.addTween(cjs.Tween.get(this.shape_2).wait(40));

	// Layer 5
	this.instance = new lib.Symbol2();
	this.instance.parent = this;
	this.instance.setTransform(137.9,-48.7,2.017,2.017,0,46.1,51.9,5.4,7.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1).to({regX:4.3,regY:4.3,scaleX:2.02,scaleY:2.02,skewX:45.7,skewY:51.5,x:141.4,y:-54.8},0).wait(1).to({scaleX:2.01,scaleY:2.02,skewX:44.8,skewY:50.4,x:141.8,y:-54.4},0).wait(1).to({scaleX:2.01,scaleY:2.02,skewX:43.2,skewY:48.6,x:142.7,y:-53.7},0).wait(1).to({scaleX:2,scaleY:2.03,skewX:40.7,skewY:45.8,x:144.2,y:-52.8},0).wait(1).to({scaleX:2,scaleY:2.04,skewX:37.3,skewY:41.9,x:146.4,y:-51.7},0).wait(1).to({scaleX:1.98,scaleY:2.05,skewX:32.8,skewY:36.8,x:149.5,y:-50.7},0).wait(1).to({scaleX:1.97,scaleY:2.06,skewX:27.4,skewY:30.6,x:153.3,y:-50.1},0).wait(1).to({scaleX:1.96,scaleY:2.08,skewX:21.5,skewY:23.9,x:157.6},0).wait(1).to({scaleX:1.94,scaleY:2.09,skewX:16,skewY:17.6,x:161.1,y:-50.5},0).wait(1).to({scaleX:1.93,scaleY:2.1,skewX:11.4,skewY:12.4,x:164,y:-51.2},0).wait(1).to({scaleX:1.92,scaleY:2.11,skewX:8,skewY:8.6,x:166.1,y:-52},0).wait(1).to({scaleX:1.92,scaleY:2.11,skewX:5.8,skewY:6,x:167.5,y:-52.6},0).wait(1).to({scaleX:1.91,scaleY:2.12,skewX:4.5,skewY:4.6,x:168.2,y:-53},0).wait(1).to({regX:5.4,regY:7.5,scaleX:1.91,scaleY:2.12,rotation:4,skewX:0,skewY:0,x:170.2,y:-46.3},0).wait(7).to({regX:4.3,regY:4.3,scaleX:1.91,scaleY:2.12,rotation:0,skewX:4.6,skewY:4.7,x:168.2,y:-52.9},0).wait(1).to({scaleX:1.92,scaleY:2.11,skewX:6,skewY:6.2,x:167.3,y:-52.5},0).wait(1).to({scaleX:1.92,scaleY:2.11,skewX:8.3,skewY:8.8,x:165.8,y:-51.9},0).wait(1).to({scaleX:1.93,scaleY:2.1,skewX:11.5,skewY:12.6,x:163.6,y:-51.1},0).wait(1).to({scaleX:1.94,scaleY:2.09,skewX:15.7,skewY:17.4,x:160.5,y:-50.4},0).wait(1).to({scaleX:1.95,scaleY:2.08,skewX:20.7,skewY:23,x:157,y:-50},0).wait(1).to({scaleX:1.97,scaleY:2.06,skewX:26.1,skewY:29.1,x:153.3,y:-50.1},0).wait(1).to({scaleX:1.98,scaleY:2.05,skewX:31.2,skewY:35,x:150.1,y:-50.5},0).wait(1).to({scaleX:1.99,scaleY:2.04,skewX:35.9,skewY:40.2,x:147.3,y:-51.3},0).wait(1).to({scaleX:2,scaleY:2.03,skewX:39.6,skewY:44.5,x:145.1,y:-52.3},0).wait(1).to({scaleX:2.01,scaleY:2.03,skewX:42.5,skewY:47.7,x:143.3,y:-53.2},0).wait(1).to({scaleX:2.01,scaleY:2.02,skewX:44.4,skewY:50,x:142.1,y:-54.1},0).wait(1).to({scaleX:2.02,scaleY:2.02,skewX:45.6,skewY:51.3,x:141.5,y:-54.7},0).wait(1).to({regX:5.4,regY:7.5,scaleX:2.02,scaleY:2.02,skewX:46.1,skewY:51.9,x:137.8,y:-48.7},0).wait(6));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(96.5,-113.2,115,75.7);


// stage content:
(lib.Travel = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 10
	this.instance = new lib.Symbol4();
	this.instance.parent = this;
	this.instance.setTransform(777.8,234.3,1,1,0,0,0,0,11.4);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({regX:-0.1,rotation:-8.2,x:797.5,y:236.8},14,cjs.Ease.get(1)).wait(15).to({regX:0,rotation:0,x:777.8,y:234.3},15,cjs.Ease.get(1)).wait(1));

	// Layer 8
	this.instance_1 = new lib.Symbol8();
	this.instance_1.parent = this;
	this.instance_1.setTransform(550.4,371.2);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(45));

	// Layer 4
	this.instance_2 = new lib.Symbol1();
	this.instance_2.parent = this;
	this.instance_2.setTransform(584.3,433.7,1,1,0,0,0,80,54);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(45));

	// Layer 3
	this.instance_3 = new lib.Symbol3();
	this.instance_3.parent = this;
	this.instance_3.setTransform(854.5,368.4,1,1,0,0,0,129.5,231.5);

	this.timeline.addTween(cjs.Tween.get(this.instance_3).to({x:874.5},14,cjs.Ease.get(1)).wait(15).to({x:854.5},15,cjs.Ease.get(1)).wait(1));

	// Layer 11 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("AvuS0QAprnCYr6IAit1IZ/gRMAB7Alng");
	mask.setTransform(812.3,266.3);

	// Layer 9
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(11.5,1,1).p("AmdIeQDPg9B3i3QBNh4BFjxQAriQAVg9QAUg9A5hSQA5hSCdgw");
	this.shape.setTransform(744.9,285.4);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(11.5,1,1).p("AmrIdQDFhJB7i1QBWh8BGjcQAuiHAdg/QAchBA7hSQA7hRCeg5");
	this.shape_1.setTransform(746.3,285.5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(11.5,1,1).p("Am3IcQC6hWB+iwQBgiCBFjGQAyh/AkhDQAkhDA8hRQA9hSCfhB");
	this.shape_2.setTransform(747.6,285.6);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(11.5,1,1).p("AnCIbQCxhgCBiuQBoiGBFizQA0h4AshFQArhGA+hRQA+hRCfhJ");
	this.shape_3.setTransform(748.8,285.8);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(11.5,1,1).p("AnMIbQCohsCEiqQBwiKBEiiQA3hxAyhIQAxhIBAhQQA/hRCghR");
	this.shape_4.setTransform(749.9,285.9);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(11.5,1,1).p("AnWIaQChh0CHipQB3iNBEiSQA5hrA3hKQA3hKBBhQQBBhRChhX");
	this.shape_5.setTransform(750.9,286);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(11.5,1,1).p("AnfIaQCbh9CJimQB9iRBEiEQA7hmA9hMQA8hLBDhQQBBhQChhe");
	this.shape_6.setTransform(751.7,286.1);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(11.5,1,1).p("AnmIZQCUiECMikQCCiTBEh5QA+hgBAhNQBBhOBDhQQBDhPCihj");
	this.shape_7.setTransform(752.5,286.2);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(11.5,1,1).p("AnsIZQCOiLCOiiQCGiWBFhtQA/hcBEhQQBFhPBEhPQBEhPCiho");
	this.shape_8.setTransform(753.2,286.2);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(11.5,1,1).p("AnyIYQCLiQCPigQCKiZBFhjQBAhZBIhRQBIhQBFhPQBEhPCjhr");
	this.shape_9.setTransform(753.7,286.3);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(11.5,1,1).p("An2IYQCHiUCRifQCNiaBEhdQBBhVBLhSQBLhSBFhOQBFhPCjhv");
	this.shape_10.setTransform(754.2,286.3);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(11.5,1,1).p("An5IYQCEiYCRieQCQicBFhWQBChUBMhSQBNhTBGhOQBFhPCkhx");
	this.shape_11.setTransform(754.6,286.4);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(11.5,1,1).p("An8IYQCCiaCSieQCSidBFhSQBChSBPhTQBOhTBGhOQBGhPCjhz");
	this.shape_12.setTransform(754.8,286.4);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(11.5,1,1).p("An+IYQCBicCTidQCTidBEhQQBDhRBQhTQBOhUBHhOQBGhPCjhz");
	this.shape_13.setTransform(755,286.4);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(11.5,1,1).p("An+IYQCAicCUieQCTidBEhPQBDhQBQhUQBPhUBGhOQBGhOCkh0");
	this.shape_14.setTransform(755,286.4);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(11.5,1,1).p("AnyIYQCLiPCPihQCKiYBFhkQBAhZBIhRQBIhQBFhPQBEhPCihr");
	this.shape_15.setTransform(753.7,286.3);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(11.5,1,1).p("AnmIZQCUiECMikQCBiUBFh4QA9hfBBhOQBBhOBDhQQBDhPCihj");
	this.shape_16.setTransform(752.5,286.2);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(11.5,1,1).p("AnbIaQCdh6CIinQB6iPBFiKQA6hnA7hLQA6hMBChQQBBhQChhb");
	this.shape_17.setTransform(751.4,286);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#000000").ss(11.5,1,1).p("AnRIbQClhwCGipQByiMBFibQA4huA0hIQA1hJBAhQQBAhRCghU");
	this.shape_18.setTransform(750.3,285.9);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#000000").ss(11.5,1,1).p("AnIIbQCshnCDisQBsiIBFipQA2h0AvhHQAuhHA/hRQA/hQCghO");
	this.shape_19.setTransform(749.4,285.8);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#000000").ss(11.5,1,1).p("AnAIcQCzhfCBiuQBmiFBFi3QA0h6AqhFQAphEA+hRQA+hRCfhJ");
	this.shape_20.setTransform(748.6,285.7);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#000000").ss(11.5,1,1).p("Am5IcQC5hXB/ixQBhiCBFjDQAyh+AlhDQAmhDA8hSQA9hRCfhD");
	this.shape_21.setTransform(747.8,285.7);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#000000").ss(11.5,1,1).p("AmyIdQC+hSB9iyQBch/BFjOQAwiDAihBQAhhCA8hSQA7hRCfg/");
	this.shape_22.setTransform(747.1,285.6);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#000000").ss(11.5,1,1).p("AmtIdQDDhLB7i0QBZh9BFjYQAuiGAfhAQAehAA7hTQA6hSCfg6");
	this.shape_23.setTransform(746.5,285.5);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#000000").ss(11.5,1,1).p("AmoIdQDGhHB7i1QBUh7BGjgQAtiJAbg/QAcg/A6hSQA6hSCeg3");
	this.shape_24.setTransform(746.1,285.5);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f().s("#000000").ss(11.5,1,1).p("AmkIeQDJhEB5i2QBSh6BGjmQAsiLAZg/QAZg+A6hSQA5hSCeg1");
	this.shape_25.setTransform(745.7,285.4);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f().s("#000000").ss(11.5,1,1).p("AmhIeQDMhAB4i3QBQh5BFjrQAsiOAXg9QAXg+A5hSQA5hSCegz");
	this.shape_26.setTransform(745.3,285.4);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f().s("#000000").ss(11.5,1,1).p("AmfIeQDNg+B4i4QBOh4BGjuQAriQAWg8QAVg9A5hTQA6hSCdgx");
	this.shape_27.setTransform(745.1,285.4);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f().s("#000000").ss(11.5,1,1).p("AmeIeQDPg9B3i4QBNh3BGjxQAqiQAWg9QAUg8A6hTQA4hSCegw");
	this.shape_28.setTransform(745,285.4);

	var maskedShapeInstanceList = [this.shape,this.shape_1,this.shape_2,this.shape_3,this.shape_4,this.shape_5,this.shape_6,this.shape_7,this.shape_8,this.shape_9,this.shape_10,this.shape_11,this.shape_12,this.shape_13,this.shape_14,this.shape_15,this.shape_16,this.shape_17,this.shape_18,this.shape_19,this.shape_20,this.shape_21,this.shape_22,this.shape_23,this.shape_24,this.shape_25,this.shape_26,this.shape_27,this.shape_28];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_14}]},15).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_27}]},1).to({state:[{t:this.shape_28}]},1).to({state:[{t:this.shape}]},1).wait(1));

	// Слой_2
	this.instance_4 = new lib.pack();
	this.instance_4.parent = this;
	this.instance_4.setTransform(439,141);

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(45));

	// Слой_1
	this.instance_5 = new lib.bg_part();
	this.instance_5.parent = this;
	this.instance_5.setTransform(395,414);

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(45));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(540,340,1080,681);
// library properties:
lib.properties = {
	id: 'DBBA7156AB916E47A654A7DA74924EF6',
	width: 1080,
	height: 680,
	fps: 30,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: [
		{src:"assets/images/bag13.png", id:"bag"},
		// {src:"assets/images/bg_part13.png", id:"bg_part"},
		{src:"assets/images/hand213.png", id:"hand2"},
		{src:"assets/images/pack13.png", id:"pack"}
	],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['DBBA7156AB916E47A654A7DA74924EF6'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}



})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;

$(function () {
    var canvas, stage, exportRoot, anim_container, dom_overlay_container, fnStartAnimation;
    function init() {
        canvas = document.getElementById("canvas14");
        anim_container = document.getElementById("animation_container14");
        dom_overlay_container = document.getElementById("dom_overlay_container14");
        var comp=AdobeAn.getComposition("DBBA7156AB916E47A654A7DA74924EF6");
        var lib=comp.getLibrary();
        var loader = new createjs.LoadQueue(false);
        loader.addEventListener("fileload", function(evt){handleFileLoad(evt,comp)});
        loader.addEventListener("complete", function(evt){handleComplete(evt,comp)});
        var lib=comp.getLibrary();
        loader.loadManifest(lib.properties.manifest);
    }
    function handleFileLoad(evt, comp) {
        var images=comp.getImages();
        if (evt && (evt.item.type == "image")) { images[evt.item.id] = evt.result; }
    }
    function handleComplete(evt,comp) {
        //This function is always called, irrespective of the content. You can use the variable "stage" after it is created in token create_stage.
        var lib=comp.getLibrary();
        var ss=comp.getSpriteSheet();
        var queue = evt.target;
        var ssMetadata = lib.ssMetadata;
        for(i=0; i<ssMetadata.length; i++) {
            ss[ssMetadata[i].name] = new createjs.SpriteSheet( {"images": [queue.getResult(ssMetadata[i].name)], "frames": ssMetadata[i].frames} )
        }
        exportRoot = new lib.Travel();
        stage = new lib.Stage(canvas);
        //Registers the "tick" event listener.
        fnStartAnimation = function() {
            stage.addChild(exportRoot);
            createjs.Ticker.setFPS(lib.properties.fps);
            createjs.Ticker.addEventListener("tick", stage);
        }
        //Code to support hidpi screens and responsive scaling.
        function makeResponsive(isResp, respDim, isScale, scaleType) {
            var lastW, lastH, lastS=1;
            window.addEventListener('resize', resizeCanvas);
            resizeCanvas();
            function resizeCanvas() {
                var w = lib.properties.width, h = lib.properties.height;
                var iw = window.innerWidth, ih=window.innerHeight;
                var pRatio = window.devicePixelRatio || 1, xRatio=iw/w, yRatio=ih/h, sRatio=1;
                if(isResp) {
                    if((respDim=='width'&&lastW==iw) || (respDim=='height'&&lastH==ih)) {
                        sRatio = lastS;
                    }
                    else if(!isScale) {
                        if(iw<w || ih<h)
                            sRatio = Math.min(xRatio, yRatio);
                    }
                    else if(scaleType==1) {
                        sRatio = Math.min(xRatio, yRatio);
                    }
                    else if(scaleType==2) {
                        sRatio = Math.max(xRatio, yRatio);
                    }
                }
                canvas.width = w*pRatio*sRatio;
                canvas.height = h*pRatio*sRatio;
                canvas.style.width = dom_overlay_container.style.width = anim_container.style.width =  w*sRatio+'px';
                canvas.style.height = anim_container.style.height = dom_overlay_container.style.height = h*sRatio+'px';
                stage.scaleX = pRatio*sRatio;
                stage.scaleY = pRatio*sRatio;
                lastW = iw; lastH = ih; lastS = sRatio;
                stage.tickOnUpdate = false;
                stage.update();
                stage.tickOnUpdate = true;
            }
        }
        makeResponsive(true,'both',true,2);
        AdobeAn.compositionLoaded(lib.properties.id);
        fnStartAnimation();
    }
    init();
});