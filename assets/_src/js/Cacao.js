(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [];


// symbols:



(lib.bg_pack = function() {
	this.initialize(img.bg_pack);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,1080,680);


(lib.Symbol10 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 4
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(12,1,1).p("AB0BlQARiBhQgxQhkgnhHAb");
	this.shape.setTransform(18.3504,107.2006);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(12,1,1).p("Ah8hQQBEgYBjAjQBUAqgCB2");
	this.shape_1.setTransform(14.5023,106.8998);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(12,1,1).p("AiDhIQBCgVBjAfQBWAkAMBs");
	this.shape_2.setTransform(11.2,106.626);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(12,1,1).p("AiJhBQA/gTBjAcQBZAfAYBj");
	this.shape_3.setTransform(8.3,106.378);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(12,1,1).p("AiOg7QA9gQBiAYQBcAbAiBb");
	this.shape_4.setTransform(5.775,106.18);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(12,1,1).p("AiSg3QA7gNBiAVQBeAXAqBV");
	this.shape_5.setTransform(3.65,106.0109);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(12,1,1).p("AiWgyQA6gNBiATQBfAUAyBQ");
	this.shape_6.setTransform(1.9,105.852);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(12,1,1).p("AiYgvQA4gLBiARQBgASA3BL");
	this.shape_7.setTransform(0.525,105.7627);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(12,1,1).p("AiagtQA3gKBjAQQBgAQA7BJ");
	this.shape_8.setTransform(-0.45,105.6827);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(12,1,1).p("AibgsQA3gJBiAQQBhAOA9BH");
	this.shape_9.setTransform(-1.025,105.6097);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(12,1,1).p("ACdAvQg+hGhigOQhigPg3AJ");
	this.shape_10.setTransform(-1.225,105.608);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(12,1,1).p("AiTgnQAtgWBiAQQBgAQA4BS");
	this.shape_11.setTransform(-0.325,105.0837);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(12,1,1).p("AiKgjQAigiBhASQBgARAyBd");
	this.shape_12.setTransform(0.575,104.4914);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(12,1,1).p("AiBgeQAXgvBhATQBfATAsBo");
	this.shape_13.setTransform(1.5,103.8765);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(12,1,1).p("Ah4gaQANg8BgAVQBeAUAmB0");
	this.shape_14.setTransform(2.4,103.2528);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(12,1,1).p("ABvBNQgfh/hegVQhegWgCBI");
	this.shape_15.setTransform(3.3,102.6125);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(12,1,1).p("AhpAAQgGhPBdAQQBcARAhBw");
	this.shape_16.setTransform(3.7819,103.7082);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(12,1,1).p("AhkAVQgNhWBbAMQBaALAiBi");
	this.shape_17.setTransform(4.2132,104.7825);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#000000").ss(12,1,1).p("AhdAqQgWhcBaAGQBZAGAjBT");
	this.shape_18.setTransform(4.5927,105.8069);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#000000").ss(12,1,1).p("AhXAyQgdhjBYABQBXAAAkBF");
	this.shape_19.setTransform(4.9171,108.2495);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#000000").ss(12,1,1).p("AhQA4QglhqBXgFQBVgFAlA4");
	this.shape_20.setTransform(5.223,110.8045);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#000000").ss(12,1,1).p("AhJA/QgthxBVgKQBUgKAmAp");
	this.shape_21.setTransform(5.5104,113.319);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#000000").ss(12,1,1).p("ABXg1QgngbhTAQQhTAPA0B4");
	this.shape_22.setTransform(5.7566,115.7136);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#000000").ss(12,1,1).p("AhMA8QgphuBWgIQBUgIAmAv");
	this.shape_23.setTransform(5.3996,112.3418);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#000000").ss(12,1,1).p("AhWAzQgehlBYAAQBWgBAlBD");
	this.shape_24.setTransform(4.9825,108.7743);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f().s("#000000").ss(12,1,1).p("AhfAmQgUhbBbAHQBZAHAiBW");
	this.shape_25.setTransform(4.519,105.6003);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f().s("#000000").ss(12,1,1).p("AhnAIQgJhRBcAOQBcAOAhBr");
	this.shape_26.setTransform(3.9601,104.1411);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f().s("#000000").ss(12,1,1).p("AiRg3QA7gOBiAWQBdAXApBW");
	this.shape_27.setTransform(3.925,106.0278);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f().s("#000000").ss(12,1,1).p("AiBhKQBCgVBiAfQBWAmAJBu");
	this.shape_28.setTransform(11.875,106.6889);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f().s("#000000").ss(12,1,1).p("Ah8hQQBFgYBjAjQBTAqgCB2");
	this.shape_29.setTransform(14.6286,106.8998);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f().s("#000000").ss(12,1,1).p("Ah4hVQBGgaBjAmQBSAtgLB9");
	this.shape_30.setTransform(16.6849,107.0806);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f().s("#000000").ss(12,1,1).p("Ah3hYQBIgbBjAnQBQAwgPCA");
	this.shape_31.setTransform(17.9184,107.1606);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},9).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_27}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_28}]},1).to({state:[{t:this.shape_29}]},1).to({state:[{t:this.shape_30}]},1).to({state:[{t:this.shape_31}]},1).to({state:[{t:this.shape}]},1).wait(1));

	// Layer 3
	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f().s("#000000").ss(12,1,1).p("ABgAdQhNg0hPgLQgiAVgBAw");
	this.shape_32.setTransform(19.8,113.175);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f().s("#000000").ss(12,1,1).p("AhqAcQALgmArgRQBOAIBQAp");
	this.shape_33.setTransform(15.8,113.175);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f().s("#000000").ss(12,1,1).p("AhzAVQAUgbAzgOQBOAFBSAe");
	this.shape_34.setTransform(12.175,113.175);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f().s("#000000").ss(12,1,1).p("Ah8APQAcgSA7gLQBNACBVAV");
	this.shape_35.setTransform(9,113.175);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f().s("#000000").ss(12,1,1).p("AiEAKQAkgLBAgIQBNgBBYAO");
	this.shape_36.setTransform(6.275,113.1719);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f().s("#000000").ss(12,1,1).p("AiKAGQApgFBGgFQBMgDBaAI");
	this.shape_37.setTransform(3.95,113.1466);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f().s("#000000").ss(12,1,1).p("AiPAEQAuAABKgEQBMgEBbAD");
	this.shape_38.setTransform(2.025,113.04);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f().s("#000000").ss(12,1,1).p("AiTACQAyAEBNgDQBMgFBcgB");
	this.shape_39.setTransform(0.575,112.9481);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f().s("#000000").ss(12,1,1).p("AiWAAQA0AHBQgCQBNgGBcgE");
	this.shape_40.setTransform(-0.5,112.9132);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f().s("#000000").ss(12,1,1).p("AiYAAQA2AIBSgCQBMgGBdgG");
	this.shape_41.setTransform(-1.15,112.9107);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f().s("#000000").ss(12,1,1).p("ACagGQheAGhMAHQhRABg4gJ");
	this.shape_42.setTransform(-1.35,112.9049);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f().s("#000000").ss(12,1,1).p("AiMANQAegMBSgBQBMgGBdgH");
	this.shape_43.setTransform(-0.125,113.55);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f().s("#000000").ss(12,1,1).p("AiAAYQAGggBRgBQBMgIBegG");
	this.shape_44.setTransform(1.125,114.625);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f().s("#000000").ss(12,1,1).p("AhyAjQgTg2BSgBQBMgHBdgH");
	this.shape_45.setTransform(2.1704,115.675);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.f().s("#000000").ss(12,1,1).p("AhfAtQgshKBRgCQBMgHBegG");
	this.shape_46.setTransform(2.8287,116.75);

	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.f().s("#000000").ss(12,1,1).p("ABrg3QhdAGhMAIQhRABBEBg");
	this.shape_47.setTransform(3.2666,117.825);

	this.shape_48 = new cjs.Shape();
	this.shape_48.graphics.f().s("#000000").ss(12,1,1).p("AhQA1QhDheBbgIQBIADBdgG");
	this.shape_48.setTransform(3.0666,117.5);

	this.shape_49 = new cjs.Shape();
	this.shape_49.graphics.f().s("#000000").ss(12,1,1).p("AhVA2QhBhbBkgQQBEAPBegG");
	this.shape_49.setTransform(2.8355,116.725);

	this.shape_50 = new cjs.Shape();
	this.shape_50.graphics.f().s("#000000").ss(12,1,1).p("AhaA5Qg/hZBtgYQBAAbBegG");
	this.shape_50.setTransform(2.6112,115.825);

	this.shape_51 = new cjs.Shape();
	this.shape_51.graphics.f().s("#000000").ss(12,1,1).p("AhfA8Qg8hXB2ggQA8AnBdgG");
	this.shape_51.setTransform(2.3776,114.925);

	this.shape_52 = new cjs.Shape();
	this.shape_52.graphics.f().s("#000000").ss(12,1,1).p("AhjA+Qg7hUCAgnQA3AyBegG");
	this.shape_52.setTransform(2.13,114.025);

	this.shape_53 = new cjs.Shape();
	this.shape_53.graphics.f().s("#000000").ss(12,1,1).p("AhoBBQg5hSCJgvQA0A+BdgG");
	this.shape_53.setTransform(1.8501,113.125);

	this.shape_54 = new cjs.Shape();
	this.shape_54.graphics.f().s("#000000").ss(12,1,1).p("AB8AAQhdAGgwhIQiSA2A3BP");
	this.shape_54.setTransform(1.5862,112.225);

	this.shape_55 = new cjs.Shape();
	this.shape_55.graphics.f().s("#000000").ss(12,1,1).p("AhmBAQg6hTCGgsQA1A6BdgH");
	this.shape_55.setTransform(1.9595,113.475);

	this.shape_56 = new cjs.Shape();
	this.shape_56.graphics.f().s("#000000").ss(12,1,1).p("AhgA8Qg8hXB4ggQA7ApBegG");
	this.shape_56.setTransform(2.325,114.75);

	this.shape_57 = new cjs.Shape();
	this.shape_57.graphics.f().s("#000000").ss(12,1,1).p("AhZA5Qg/haBrgXQBBAZBdgG");
	this.shape_57.setTransform(2.6611,116);

	this.shape_58 = new cjs.Shape();
	this.shape_58.graphics.f().s("#000000").ss(12,1,1).p("AhSA1QhChdBegMQBHAJBdgH");
	this.shape_58.setTransform(2.9801,117.275);

	this.shape_59 = new cjs.Shape();
	this.shape_59.graphics.f().s("#000000").ss(12,1,1).p("AiJAHQAogGBFgGQBNgCBZAI");
	this.shape_59.setTransform(4.25,113.149);

	this.shape_60 = new cjs.Shape();
	this.shape_60.graphics.f().s("#000000").ss(12,1,1).p("AhyAWQATgdAxgOQBOAFBTAh");
	this.shape_60.setTransform(12.9,113.175);

	this.shape_61 = new cjs.Shape();
	this.shape_61.graphics.f().s("#000000").ss(12,1,1).p("AhpAcQALgmAqgRQBOAIBQAp");
	this.shape_61.setTransform(15.925,113.175);

	this.shape_62 = new cjs.Shape();
	this.shape_62.graphics.f().s("#000000").ss(12,1,1).p("AhjAgQAFgsAmgTQBOAKBOAv");
	this.shape_62.setTransform(18.075,113.175);

	this.shape_63 = new cjs.Shape();
	this.shape_63.graphics.f().s("#000000").ss(12,1,1).p("AhgAiQACgvAjgUQBOALBOAy");
	this.shape_63.setTransform(19.375,113.175);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_32}]}).to({state:[{t:this.shape_32}]},9).to({state:[{t:this.shape_33}]},1).to({state:[{t:this.shape_34}]},1).to({state:[{t:this.shape_35}]},1).to({state:[{t:this.shape_36}]},1).to({state:[{t:this.shape_37}]},1).to({state:[{t:this.shape_38}]},1).to({state:[{t:this.shape_39}]},1).to({state:[{t:this.shape_40}]},1).to({state:[{t:this.shape_41}]},1).to({state:[{t:this.shape_42}]},1).to({state:[{t:this.shape_43}]},1).to({state:[{t:this.shape_44}]},1).to({state:[{t:this.shape_45}]},1).to({state:[{t:this.shape_46}]},1).to({state:[{t:this.shape_47}]},1).to({state:[{t:this.shape_48}]},1).to({state:[{t:this.shape_49}]},1).to({state:[{t:this.shape_50}]},1).to({state:[{t:this.shape_51}]},1).to({state:[{t:this.shape_52}]},1).to({state:[{t:this.shape_53}]},1).to({state:[{t:this.shape_54}]},1).to({state:[{t:this.shape_55}]},1).to({state:[{t:this.shape_56}]},1).to({state:[{t:this.shape_57}]},1).to({state:[{t:this.shape_58}]},1).to({state:[{t:this.shape_47}]},1).to({state:[{t:this.shape_46}]},1).to({state:[{t:this.shape_45}]},1).to({state:[{t:this.shape_44}]},1).to({state:[{t:this.shape_43}]},1).to({state:[{t:this.shape_42}]},1).to({state:[{t:this.shape_59}]},1).to({state:[{t:this.shape_35}]},1).to({state:[{t:this.shape_60}]},1).to({state:[{t:this.shape_61}]},1).to({state:[{t:this.shape_62}]},1).to({state:[{t:this.shape_63}]},1).to({state:[{t:this.shape_32}]},1).wait(1));

	// Layer 2
	this.shape_64 = new cjs.Shape();
	this.shape_64.graphics.f().s("#000000").ss(12,1,1).p("AG9pTQBODrgND0QgOD+htCzQh4DGjVA7QjuBClHiD");
	this.shape_64.setTransform(80.5179,65.2306);

	this.shape_65 = new cjs.Shape();
	this.shape_65.graphics.f().s("#000000").ss(12,1,1).p("AoCIFQE5BnDgg/QDLg5CBi2QB3inAfj7QAfjyhDju");
	this.shape_65.setTransform(78.0653,63.7301);

	this.shape_66 = new cjs.Shape();
	this.shape_66.graphics.f().s("#000000").ss(12,1,1).p("AoHILQEtBODTg7QDCg3CIipQCBicAvj3QAvjyg5jw");
	this.shape_66.setTransform(76.1319,62.4204);

	this.shape_67 = new cjs.Shape();
	this.shape_67.graphics.f().s("#000000").ss(12,1,1).p("AoNIRQEiA4DIg5QC7g1COidQCJiTA9j0QA+jwgxjz");
	this.shape_67.setTransform(74.5364,61.3188);

	this.shape_68 = new cjs.Shape();
	this.shape_68.graphics.f().s("#000000").ss(12,1,1).p("AoUIVQEaAlC+g2QC0g0CUiSQCPiMBKjyQBKjugpj1");
	this.shape_68.setTransform(73.2206,60.4361);

	this.shape_69 = new cjs.Shape();
	this.shape_69.graphics.f().s("#000000").ss(12,1,1).p("AoaIYQESAUC2g0QCugzCZiJQCViFBVjvQBUjugjj2");
	this.shape_69.setTransform(72.2001,59.7797);

	this.shape_70 = new cjs.Shape();
	this.shape_70.graphics.f().s("#000000").ss(12,1,1).p("AoeIYQELAHCvgyQCqgyCciCQCbh/BdjuQBcjtgej3");
	this.shape_70.setTransform(71.3923,59.368);

	this.shape_71 = new cjs.Shape();
	this.shape_71.graphics.f().s("#000000").ss(12,1,1).p("AojIYQEGgECqgxQCngwCfh9QCeh8BkjsQBjjtgaj4");
	this.shape_71.setTransform(70.7693,59.2);

	this.shape_72 = new cjs.Shape();
	this.shape_72.graphics.f().s("#000000").ss(12,1,1).p("AomIXQECgLCngwQCjgxCih4QChh4BojsQBpjsgYj5");
	this.shape_72.setTransform(70.3657,59.1);

	this.shape_73 = new cjs.Shape();
	this.shape_73.graphics.f().s("#000000").ss(12,1,1).p("AooIWQEAgPCkgvQCjgwCjh3QCih2BrjrQBrjsgVj5");
	this.shape_73.setTransform(70.1049,59.05);

	this.shape_74 = new cjs.Shape();
	this.shape_74.graphics.f().s("#000000").ss(12,1,1).p("AImoVQAVD6hsDrQhsDrijB2QikB1iiAwQijAvj/AR");
	this.shape_74.setTransform(70.0223,59.025);

	this.shape_75 = new cjs.Shape();
	this.shape_75.graphics.f().s("#000000").ss(12,1,1).p("AoYIXQESAXC3g0QCvgzCYiLQCViGBTjvQBTjvgkj1");
	this.shape_75.setTransform(72.3468,59.8648);

	this.shape_76 = new cjs.Shape();
	this.shape_76.graphics.f().s("#000000").ss(12,1,1).p("AoNIRQEiA4DIg5QC6g1CPidQCIiTA+j0QA+jwgxjz");
	this.shape_76.setTransform(74.5364,61.3188);

	this.shape_77 = new cjs.Shape();
	this.shape_77.graphics.f().s("#000000").ss(12,1,1).p("AoGIKQEwBTDWg8QDDg3CHisQB+ieAtj4QAsjyg8jv");
	this.shape_77.setTransform(76.4853,62.6837);

	this.shape_78 = new cjs.Shape();
	this.shape_78.graphics.f().s("#000000").ss(12,1,1).p("AoCIEQE6BoDgg+QDMg5CAi3QB3ioAfj6QAejzhDjt");
	this.shape_78.setTransform(78.1496,63.773);

	this.shape_79 = new cjs.Shape();
	this.shape_79.graphics.f().s("#000000").ss(12,1,1).p("AoAIAQFBB3DohAQDRg6B8i/QBxiuAVj9QAVjzhJjs");
	this.shape_79.setTransform(79.4333,64.5836);

	this.shape_80 = new cjs.Shape();
	this.shape_80.graphics.f().s("#000000").ss(12,1,1).p("An/H9QFGCADshBQDUg7B5jEQBuiyAQj9QAPj0hNjr");
	this.shape_80.setTransform(80.2473,65.0586);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_64}]}).to({state:[{t:this.shape_64}]},9).to({state:[{t:this.shape_65}]},1).to({state:[{t:this.shape_66}]},1).to({state:[{t:this.shape_67}]},1).to({state:[{t:this.shape_68}]},1).to({state:[{t:this.shape_69}]},1).to({state:[{t:this.shape_70}]},1).to({state:[{t:this.shape_71}]},1).to({state:[{t:this.shape_72}]},1).to({state:[{t:this.shape_73}]},1).to({state:[{t:this.shape_74}]},1).to({state:[{t:this.shape_74}]},22).to({state:[{t:this.shape_75}]},1).to({state:[{t:this.shape_76}]},1).to({state:[{t:this.shape_77}]},1).to({state:[{t:this.shape_78}]},1).to({state:[{t:this.shape_79}]},1).to({state:[{t:this.shape_80}]},1).to({state:[{t:this.shape_64}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-22.9,-0.3,160.9,131.3);


(lib.Symbol9 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 6
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(9.5,1,1).p("AiUgpQB2AWCzA9");
	this.shape.setTransform(78.175,19.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(9.5,1,1).p("AiWghQB3AQC1Az");
	this.shape_1.setTransform(78.7,18.625);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(9.5,1,1).p("AiWgbQB3ALC2As");
	this.shape_2.setTransform(79.175,18.1);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(9.5,1,1).p("AiYgUQB4AGC5Ak");
	this.shape_3.setTransform(79.6,17.65);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(9.5,1,1).p("AiYgPQB4ABC5Ae");
	this.shape_4.setTransform(79.975,17.275);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(9.5,1,1).p("AiZgLQB4gBC7AY");
	this.shape_5.setTransform(80.275,16.9208);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(9.5,1,1).p("AiZgHQB4gEC7AU");
	this.shape_6.setTransform(80.525,16.6353);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(9.5,1,1).p("AiagEQB5gGC8AQ");
	this.shape_7.setTransform(80.725,16.362);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(9.5,1,1).p("AiagBQB5gIC8AO");
	this.shape_8.setTransform(80.85,16.1609);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(9.5,1,1).p("AiaAAQB4gJC9AN");
	this.shape_9.setTransform(80.95,16.0038);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(9.5,1,1).p("AiaAAQB4gIC9AM");
	this.shape_10.setTransform(80.975,15.9744);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(9.5,1,1).p("AiZgJQB4gDC7AW");
	this.shape_11.setTransform(80.4,16.808);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(9.5,1,1).p("AiYgRQB4ADC5Ag");
	this.shape_12.setTransform(79.875,17.375);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(9.5,1,1).p("AiXgXQB3AIC4An");
	this.shape_13.setTransform(79.425,17.85);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(9.5,1,1).p("AiWgcQB3AMC2At");
	this.shape_14.setTransform(79.05,18.25);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(9.5,1,1).p("AiVghQB2AQC1Az");
	this.shape_15.setTransform(78.725,18.6);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(9.5,1,1).p("AiVglQB2ATC1A4");
	this.shape_16.setTransform(78.5,18.85);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(9.5,1,1).p("AiUgnQB2AVC0A6");
	this.shape_17.setTransform(78.3,19.05);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#000000").ss(9.5,1,1).p("AiUgoQB2AVCzA9");
	this.shape_18.setTransform(78.225,19.15);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},9).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_10}]},20).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape}]},1).wait(1));

	// Layer 3
	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#000000").ss(9.5,1,1).p("AiUBDQB0hLC1g6");
	this.shape_19.setTransform(78.175,8.3);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#000000").ss(9.5,1,1).p("AiTBFQBzhMC0g9");
	this.shape_20.setTransform(78.575,8.275);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#000000").ss(9.5,1,1).p("AiTBHQBzhOC0g/");
	this.shape_21.setTransform(78.95,8.25);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#000000").ss(9.5,1,1).p("AiSBJQByhQCzhB");
	this.shape_22.setTransform(79.25,8.25);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#000000").ss(9.5,1,1).p("AiRBKQBxhQCyhD");
	this.shape_23.setTransform(79.5,8.225);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#000000").ss(9.5,1,1).p("AiQBLQBwhRCxhE");
	this.shape_24.setTransform(79.75,8.2);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f().s("#000000").ss(9.5,1,1).p("AiQBMQBvhRCyhG");
	this.shape_25.setTransform(79.95,8.2);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f().s("#000000").ss(9.5,1,1).p("AiQBNQBwhSCxhH");
	this.shape_26.setTransform(80.075,8.175);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f().s("#000000").ss(9.5,1,1).p("AiQBNQBvhSCyhH");
	this.shape_27.setTransform(80.2,8.175);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f().s("#000000").ss(9.5,1,1).p("AiQBOQBwhTCxhI");
	this.shape_28.setTransform(80.25,8.175);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f().s("#000000").ss(9.5,1,1).p("AiPBOQBvhTCwhI");
	this.shape_29.setTransform(80.275,8.175);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f().s("#000000").ss(9.5,1,1).p("AiQBMQBwhSCxhE");
	this.shape_30.setTransform(79.825,8.2);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f().s("#000000").ss(9.5,1,1).p("AiSBIQByhPCzhA");
	this.shape_31.setTransform(79.1,8.225);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f().s("#000000").ss(9.5,1,1).p("AiTBGQBzhNC0g+");
	this.shape_32.setTransform(78.825,8.275);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f().s("#000000").ss(9.5,1,1).p("AiUBEQB0hMC1g7");
	this.shape_33.setTransform(78.425,8.275);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f().s("#000000").ss(9.5,1,1).p("AiUBDQB1hLC0g6");
	this.shape_34.setTransform(78.2,8.3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_19}]}).to({state:[{t:this.shape_19}]},9).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23,p:{x:79.5,y:8.225}}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_27}]},1).to({state:[{t:this.shape_28}]},1).to({state:[{t:this.shape_29}]},1).to({state:[{t:this.shape_29}]},20).to({state:[{t:this.shape_30}]},1).to({state:[{t:this.shape_23,p:{x:79.425,y:8.2}}]},1).to({state:[{t:this.shape_31}]},1).to({state:[{t:this.shape_32}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_33,p:{x:78.425,y:8.275}}]},1).to({state:[{t:this.shape_33,p:{x:78.3,y:8.3}}]},1).to({state:[{t:this.shape_34}]},1).to({state:[{t:this.shape_19}]},1).wait(1));

	// Layer 1
	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f().s("#000000").ss(9.5,1,1).p("AiPhJQCYAvCIBk");
	this.shape_35.setTransform(13.85,7.525);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f().s("#000000").ss(9.5,1,1).p("AiRhGQCZAsCKBh");
	this.shape_36.setTransform(13.5,7.925);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f().s("#000000").ss(9.5,1,1).p("AiShDQCaApCLBe");
	this.shape_37.setTransform(13.2,8.3);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f().s("#000000").ss(9.5,1,1).p("AiThBQCaAnCNBc");
	this.shape_38.setTransform(12.925,8.625);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f().s("#000000").ss(9.5,1,1).p("AiUg/QCbAkCOBb");
	this.shape_39.setTransform(12.675,8.925);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f().s("#000000").ss(9.5,1,1).p("AiVg9QCcAiCPBZ");
	this.shape_40.setTransform(12.5,9.15);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f().s("#000000").ss(9.5,1,1).p("AiWg8QCcAhCRBY");
	this.shape_41.setTransform(12.325,9.35);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f().s("#000000").ss(9.5,1,1).p("AiWg7QCcAgCRBW");
	this.shape_42.setTransform(12.2,9.5);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f().s("#000000").ss(9.5,1,1).p("AiXg6QCdAfCSBW");
	this.shape_43.setTransform(12.1,9.625);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f().s("#000000").ss(9.5,1,1).p("AiXg5QCcAeCTBV");
	this.shape_44.setTransform(12.05,9.675);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f().s("#000000").ss(9.5,1,1).p("AiXg5QCdAeCSBV");
	this.shape_45.setTransform(12.025,9.7);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.f().s("#000000").ss(9.5,1,1).p("AiVg8QCbAhCQBY");
	this.shape_46.setTransform(12.425,9.225);

	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.f().s("#000000").ss(9.5,1,1).p("AiUg/QCbAlCOBa");
	this.shape_47.setTransform(12.75,8.825);

	this.shape_48 = new cjs.Shape();
	this.shape_48.graphics.f().s("#000000").ss(9.5,1,1).p("AiThCQCbAoCMBd");
	this.shape_48.setTransform(13.025,8.475);

	this.shape_49 = new cjs.Shape();
	this.shape_49.graphics.f().s("#000000").ss(9.5,1,1).p("AiShEQCaAqCLBf");
	this.shape_49.setTransform(13.275,8.2);

	this.shape_50 = new cjs.Shape();
	this.shape_50.graphics.f().s("#000000").ss(9.5,1,1).p("AiRhHQCZAtCJBi");
	this.shape_50.setTransform(13.65,7.75);

	this.shape_51 = new cjs.Shape();
	this.shape_51.graphics.f().s("#000000").ss(9.5,1,1).p("AiQhIQCZAuCIBj");
	this.shape_51.setTransform(13.75,7.65);

	this.shape_52 = new cjs.Shape();
	this.shape_52.graphics.f().s("#000000").ss(9.5,1,1).p("AiQhIQCZAvCIBj");
	this.shape_52.setTransform(13.825,7.55);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_35}]}).to({state:[{t:this.shape_35}]},9).to({state:[{t:this.shape_36,p:{y:7.925}}]},1).to({state:[{t:this.shape_37}]},1).to({state:[{t:this.shape_38}]},1).to({state:[{t:this.shape_39}]},1).to({state:[{t:this.shape_40}]},1).to({state:[{t:this.shape_41}]},1).to({state:[{t:this.shape_42}]},1).to({state:[{t:this.shape_43}]},1).to({state:[{t:this.shape_44}]},1).to({state:[{t:this.shape_45}]},1).to({state:[{t:this.shape_45}]},20).to({state:[{t:this.shape_46}]},1).to({state:[{t:this.shape_47}]},1).to({state:[{t:this.shape_48}]},1).to({state:[{t:this.shape_49}]},1).to({state:[{t:this.shape_36,p:{y:7.95}}]},1).to({state:[{t:this.shape_50}]},1).to({state:[{t:this.shape_51}]},1).to({state:[{t:this.shape_52}]},1).to({state:[{t:this.shape_35}]},1).wait(1));

	// Layer 2
	this.shape_53 = new cjs.Shape();
	this.shape_53.graphics.f().s("#000000").ss(9.5,1,1).p("AiUAiQB+gsCrgX");
	this.shape_53.setTransform(13.375,18.3);

	this.shape_54 = new cjs.Shape();
	this.shape_54.graphics.f().s("#000000").ss(9.5,1,1).p("AiVAdQCAgnCrgS");
	this.shape_54.setTransform(13.075,17.95);

	this.shape_55 = new cjs.Shape();
	this.shape_55.graphics.f().s("#000000").ss(9.5,1,1).p("AiVAaQCAglCrgN");
	this.shape_55.setTransform(12.825,17.65);

	this.shape_56 = new cjs.Shape();
	this.shape_56.graphics.f().s("#000000").ss(9.5,1,1).p("AiWAWQCBgiCsgJ");
	this.shape_56.setTransform(12.6,17.375);

	this.shape_57 = new cjs.Shape();
	this.shape_57.graphics.f().s("#000000").ss(9.5,1,1).p("AiXATQCDgfCsgF");
	this.shape_57.setTransform(12.4,17.15);

	this.shape_58 = new cjs.Shape();
	this.shape_58.graphics.f().s("#000000").ss(9.5,1,1).p("AiXAQQCDgcCsgD");
	this.shape_58.setTransform(12.225,16.95);

	this.shape_59 = new cjs.Shape();
	this.shape_59.graphics.f().s("#000000").ss(9.5,1,1).p("AiXAOQCDgbCsAA");
	this.shape_59.setTransform(12.1,16.775);

	this.shape_60 = new cjs.Shape();
	this.shape_60.graphics.f().s("#000000").ss(9.5,1,1).p("AiXAMQCDgZCsAC");
	this.shape_60.setTransform(11.975,16.668);

	this.shape_61 = new cjs.Shape();
	this.shape_61.graphics.f().s("#000000").ss(9.5,1,1).p("AiXALQCDgYCtAD");
	this.shape_61.setTransform(11.9,16.5535);

	this.shape_62 = new cjs.Shape();
	this.shape_62.graphics.f().s("#000000").ss(9.5,1,1).p("AiYALQCEgYCtAE");
	this.shape_62.setTransform(11.85,16.4907);

	this.shape_63 = new cjs.Shape();
	this.shape_63.graphics.f().s("#000000").ss(9.5,1,1).p("AiYAKQCEgXCtAE");
	this.shape_63.setTransform(11.85,16.4651);

	this.shape_64 = new cjs.Shape();
	this.shape_64.graphics.f().s("#000000").ss(9.5,1,1).p("AiXAPQCDgcCsgB");
	this.shape_64.setTransform(12.175,16.875);

	this.shape_65 = new cjs.Shape();
	this.shape_65.graphics.f().s("#000000").ss(9.5,1,1).p("AiWATQCCgfCrgG");
	this.shape_65.setTransform(12.45,17.2);

	this.shape_66 = new cjs.Shape();
	this.shape_66.graphics.f().s("#000000").ss(9.5,1,1).p("AiWAYQCBgkCsgK");
	this.shape_66.setTransform(12.7,17.5);

	this.shape_67 = new cjs.Shape();
	this.shape_67.graphics.f().s("#000000").ss(9.5,1,1).p("AiVAaQCAglCrgP");
	this.shape_67.setTransform(12.925,17.75);

	this.shape_68 = new cjs.Shape();
	this.shape_68.graphics.f().s("#000000").ss(9.5,1,1).p("AiVAfQB/gqCsgU");
	this.shape_68.setTransform(13.2,18.1);

	this.shape_69 = new cjs.Shape();
	this.shape_69.graphics.f().s("#000000").ss(9.5,1,1).p("AiVAhQB/grCsgW");
	this.shape_69.setTransform(13.3,18.225);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_53,p:{y:18.3}}]}).to({state:[{t:this.shape_53,p:{y:18.3}}]},9).to({state:[{t:this.shape_54,p:{y:17.95}}]},1).to({state:[{t:this.shape_55}]},1).to({state:[{t:this.shape_56}]},1).to({state:[{t:this.shape_57}]},1).to({state:[{t:this.shape_58}]},1).to({state:[{t:this.shape_59}]},1).to({state:[{t:this.shape_60}]},1).to({state:[{t:this.shape_61}]},1).to({state:[{t:this.shape_62}]},1).to({state:[{t:this.shape_63}]},1).to({state:[{t:this.shape_63}]},20).to({state:[{t:this.shape_64}]},1).to({state:[{t:this.shape_65}]},1).to({state:[{t:this.shape_66}]},1).to({state:[{t:this.shape_67}]},1).to({state:[{t:this.shape_54,p:{y:17.925}}]},1).to({state:[{t:this.shape_68}]},1).to({state:[{t:this.shape_69}]},1).to({state:[{t:this.shape_53,p:{y:18.275}}]},1).to({state:[{t:this.shape_53,p:{y:18.3}}]},1).wait(1));

	// Layer 7
	this.shape_70 = new cjs.Shape();
	this.shape_70.graphics.f().s("#000000").ss(9.5,1,1).p("AjegoQAOAYAVATQBDBBBvADQBuAABLhLQAlgmAKgc");
	this.shape_70.setTransform(44.775,43.325);

	this.shape_71 = new cjs.Shape();
	this.shape_71.graphics.f().s("#000000").ss(9.5,1,1).p("AjkgpQAUAfAVAUQBDBABvAEQBugBBLhLQAlglAQgq");
	this.shape_71.setTransform(44.75,42.675);

	this.shape_72 = new cjs.Shape();
	this.shape_72.graphics.f().s("#000000").ss(9.5,1,1).p("AjpgqQAaAmAVATQBDBBBvAEQBugBBLhLQAlgmAUg1");
	this.shape_72.setTransform(44.7,42.1);

	this.shape_73 = new cjs.Shape();
	this.shape_73.graphics.f().s("#000000").ss(9.5,1,1).p("AjugrQAfArAVAUQBDBBBvAEQBugBBLhLQAlgmAZg/");
	this.shape_73.setTransform(44.675,41.6);

	this.shape_74 = new cjs.Shape();
	this.shape_74.graphics.f().s("#000000").ss(9.5,1,1).p("AjygsQAjAwAVAVQBDBABvAEQBugBBLhLQAlglAdhI");
	this.shape_74.setTransform(44.65,41.15);

	this.shape_75 = new cjs.Shape();
	this.shape_75.graphics.f().s("#000000").ss(9.5,1,1).p("Aj1gsQAmA0AVAVQBDBABvAEQBugBBLhLQAmglAfhQ");
	this.shape_75.setTransform(44.625,40.775);

	this.shape_76 = new cjs.Shape();
	this.shape_76.graphics.f().s("#000000").ss(9.5,1,1).p("Aj4gtQApA4AVAVQBDBABvAEQBvgBBLhLQAlglAihW");
	this.shape_76.setTransform(44.6,40.475);

	this.shape_77 = new cjs.Shape();
	this.shape_77.graphics.f().s("#000000").ss(9.5,1,1).p("Aj6gtQArA7AVAUQBDBBBvADQBvAABLhMQAlglAkha");
	this.shape_77.setTransform(44.6,40.225);

	this.shape_78 = new cjs.Shape();
	this.shape_78.graphics.f().s("#000000").ss(9.5,1,1).p("Aj7gtQAtA9AVAUQBDBBBvADQBuAABLhMQAlglAlhe");
	this.shape_78.setTransform(44.6,40.05);

	this.shape_79 = new cjs.Shape();
	this.shape_79.graphics.f().s("#000000").ss(9.5,1,1).p("Aj8guQAuA+AVAVQBDBABvAEQBugBBLhLQAlglAmhg");
	this.shape_79.setTransform(44.575,39.95);

	this.shape_80 = new cjs.Shape();
	this.shape_80.graphics.f().s("#000000").ss(9.5,1,1).p("Aj8guQAuA/AVAUQBDBBBvADQBuAABLhMQAlglAmhg");
	this.shape_80.setTransform(44.575,39.925);

	this.shape_81 = new cjs.Shape();
	this.shape_81.graphics.f().s("#000000").ss(9.5,1,1).p("Aj2gtQAoA2AVAVQBDBABvAEQBugBBLhLQAlglAghS");
	this.shape_81.setTransform(44.6,40.65);

	this.shape_82 = new cjs.Shape();
	this.shape_82.graphics.f().s("#000000").ss(9.5,1,1).p("AjxgsQAiAvAVAVQBDBABvAEQBugBBLhLQAlglAchG");
	this.shape_82.setTransform(44.65,41.275);

	this.shape_83 = new cjs.Shape();
	this.shape_83.graphics.f().s("#000000").ss(9.5,1,1).p("AjrgrQAcAqAVATQBDBBBvADQBuAABLhMQAlglAXg6");
	this.shape_83.setTransform(44.7,41.825);

	this.shape_84 = new cjs.Shape();
	this.shape_84.graphics.f().s("#000000").ss(9.5,1,1).p("AjogqQAYAkAVAUQBDBABvAEQBugBBMhLQAlglATgy");
	this.shape_84.setTransform(44.7,42.275);

	this.shape_85 = new cjs.Shape();
	this.shape_85.graphics.f().s("#000000").ss(9.5,1,1).p("AjkgqQAUAgAVAUQBDBABvAEQBugBBLhLQAmglAPgq");
	this.shape_85.setTransform(44.725,42.65);

	this.shape_86 = new cjs.Shape();
	this.shape_86.graphics.f().s("#000000").ss(9.5,1,1).p("AjigpQASAcAVAUQBDBABvAEQBugBBLhKQAlgmAOgk");
	this.shape_86.setTransform(44.75,42.95);

	this.shape_87 = new cjs.Shape();
	this.shape_87.graphics.f().s("#000000").ss(9.5,1,1).p("AjggpQAQAaAVAUQBDBABvAEQBugBBLhKQAlgmAMgg");
	this.shape_87.setTransform(44.775,43.15);

	this.shape_88 = new cjs.Shape();
	this.shape_88.graphics.f().s("#000000").ss(9.5,1,1).p("AjfgoQAPAYAVAUQBDBABvAEQBugBBLhKQAlgmALge");
	this.shape_88.setTransform(44.775,43.275);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_70}]}).to({state:[{t:this.shape_70}]},9).to({state:[{t:this.shape_71}]},1).to({state:[{t:this.shape_72}]},1).to({state:[{t:this.shape_73}]},1).to({state:[{t:this.shape_74}]},1).to({state:[{t:this.shape_75}]},1).to({state:[{t:this.shape_76}]},1).to({state:[{t:this.shape_77}]},1).to({state:[{t:this.shape_78}]},1).to({state:[{t:this.shape_79}]},1).to({state:[{t:this.shape_80}]},1).to({state:[{t:this.shape_80}]},20).to({state:[{t:this.shape_81}]},1).to({state:[{t:this.shape_82}]},1).to({state:[{t:this.shape_83}]},1).to({state:[{t:this.shape_84}]},1).to({state:[{t:this.shape_85}]},1).to({state:[{t:this.shape_86}]},1).to({state:[{t:this.shape_87}]},1).to({state:[{t:this.shape_88}]},1).to({state:[{t:this.shape_70}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-8.1,-4.6,109.39999999999999,59.800000000000004);


// stage content:
(lib.Cacao = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 5
	this.instance = new lib.Symbol9();
	this.instance.parent = this;
	this.instance.setTransform(730.4,274.5,1.3299,1.3299,0,0,0,14.5,7.4);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(49));

	// Layer 4
	this.instance_1 = new lib.Symbol10();
	this.instance_1.parent = this;
	this.instance_1.setTransform(832.9,384.65,1.0699,1.0699,1.8079,0,0,69,65.5);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(49));

	// Layer_3
	this.shape = new cjs.Shape();
	this.shape.graphics.lf(["#FFFFFF","rgba(255,255,255,0)"],[0,1],-171.5,0,171.5,0).s().p("EgayA1IMAAAhqPMA1lAAAMAAABqPg");
	this.shape.setTransform(171.5,340);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(49));

	// Layer 2
	this.instance_2 = new lib.bg_pack();
	this.instance_2.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(49));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(540,340,540,340);
// library properties:
lib.properties = {
	id: '8D42A21715BB9C4DA17AE6A3ADD39A6E',
	width: 1080,
	height: 680,
	fps: 30,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: [
		{src:"assets/images/bg_pack30.jpg", id:"bg_pack"}
	],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['8D42A21715BB9C4DA17AE6A3ADD39A6E'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}



})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;

$(function () {
    var canvas, stage, exportRoot, anim_container, dom_overlay_container, fnStartAnimation;
    function init() {
        canvas = document.getElementById("canvas30");
        anim_container = document.getElementById("animation_container30");
        dom_overlay_container = document.getElementById("dom_overlay_container30");
        var comp=AdobeAn.getComposition("8D42A21715BB9C4DA17AE6A3ADD39A6E");
        var lib=comp.getLibrary();
        var loader = new createjs.LoadQueue(false);
        loader.addEventListener("fileload", function(evt){handleFileLoad(evt,comp)});
        loader.addEventListener("complete", function(evt){handleComplete(evt,comp)});
        var lib=comp.getLibrary();
        loader.loadManifest(lib.properties.manifest);
    }
    function handleFileLoad(evt, comp) {
        var images=comp.getImages();
        if (evt && (evt.item.type == "image")) { images[evt.item.id] = evt.result; }
    }
    function handleComplete(evt,comp) {
        //This function is always called, irrespective of the content. You can use the variable "stage" after it is created in token create_stage.
        var lib=comp.getLibrary();
        var ss=comp.getSpriteSheet();
        var queue = evt.target;
        var ssMetadata = lib.ssMetadata;
        for(i=0; i<ssMetadata.length; i++) {
            ss[ssMetadata[i].name] = new createjs.SpriteSheet( {"images": [queue.getResult(ssMetadata[i].name)], "frames": ssMetadata[i].frames} )
        }
        var preloaderDiv = document.getElementById("_preload_div_");
        preloaderDiv.style.display = 'none';
        canvas.style.display = 'block';
        exportRoot = new lib.Cacao();
        stage = new lib.Stage(canvas);
        //Registers the "tick" event listener.
        fnStartAnimation = function() {
            stage.addChild(exportRoot);
            createjs.Ticker.setFPS(lib.properties.fps);
            createjs.Ticker.addEventListener("tick", stage);
        }
        //Code to support hidpi screens and responsive scaling.
        function makeResponsive(isResp, respDim, isScale, scaleType) {
            var lastW, lastH, lastS=1;
            window.addEventListener('resize', resizeCanvas);
            resizeCanvas();
            function resizeCanvas() {
                var w = lib.properties.width, h = lib.properties.height;
                var iw = window.innerWidth, ih=window.innerHeight;
                var pRatio = window.devicePixelRatio || 1, xRatio=iw/w, yRatio=ih/h, sRatio=1;
                if(isResp) {
                    if((respDim=='width'&&lastW==iw) || (respDim=='height'&&lastH==ih)) {
                        sRatio = lastS;
                    }
                    else if(!isScale) {
                        if(iw<w || ih<h)
                            sRatio = Math.min(xRatio, yRatio);
                    }
                    else if(scaleType==1) {
                        sRatio = Math.min(xRatio, yRatio);
                    }
                    else if(scaleType==2) {
                        sRatio = Math.max(xRatio, yRatio);
                    }
                }
                canvas.width = w*pRatio*sRatio;
                canvas.height = h*pRatio*sRatio;
                canvas.style.width = anim_container.style.width = dom_overlay_container.style.width = preloaderDiv.style.width = w*sRatio+'px';
                canvas.style.height = anim_container.style.height = dom_overlay_container.style.height = preloaderDiv.style.height = h*sRatio+'px';
                stage.scaleX = pRatio*sRatio;
                stage.scaleY = pRatio*sRatio;
                lastW = iw; lastH = ih; lastS = sRatio;
                stage.tickOnUpdate = false;
                stage.update();
                stage.tickOnUpdate = true;
            }
        }
        makeResponsive(true,'both',true,2);
        AdobeAn.compositionLoaded(lib.properties.id);
        fnStartAnimation();
    }
    init();
});