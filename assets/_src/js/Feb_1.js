(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [];


// symbols:



(lib.bg = function() {
	this.initialize(img.bg);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,1086,686);


(lib.cap = function() {
	this.initialize(img.cap);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,224,173);


(lib.pack = function() {
	this.initialize(img.pack);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,408,502);


(lib.patch = function() {
	this.initialize(img.patch);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,49,40);


(lib.pillow = function() {
	this.initialize(img.pillow);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,407,308);// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.Symbol11 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.pillow();
	this.instance.parent = this;
	this.instance.setTransform(-203.5,-154);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol11, new cjs.Rectangle(-203.5,-154,407,308), null);


(lib.Symbol9 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AgpArQgRgRAAgaQAAgZARgRQARgTAYABQAYgBASATQARARAAAZQAAAagRARQgSATgYgBQgYABgRgTg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol9, new cjs.Rectangle(-5.9,-6.1,11.9,12.3), null);


(lib.Symbol8 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.patch();
	this.instance.parent = this;
	this.instance.setTransform(-24.5,-20);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol8, new cjs.Rectangle(-24.5,-20,49,40), null);


(lib.Symbol2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.cap();
	this.instance.parent = this;
	this.instance.setTransform(-112,-86.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol2, new cjs.Rectangle(-112,-86.5,224,173), null);


(lib.Symbol1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_3
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(8.3,1,1).p("AAbAYQgLAKgQAAQgOAAgMgKQgKgJgBgNQAAgBAAAAQAAgJAFgIQADgDADgEQAMgKAOAAQAQAAALAKQADAEADADQAFAIAAAJQAAAAAAABQgBANgKAJg");
	this.shape.setTransform(-190.8,187.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#000000").s().p("AgcAdQgNgMAAgRIAAgBQABgKAFgIIAHgJQAMgMAQAAQARAAAMAMIAHAIQAFAJABAKIAAABQAAARgNAMQgMANgRAAQgQAAgMgNgAglAAQAAAMAKAKQAMAKAPAAQAPAAALgKQgLAKgPAAQgPAAgMgKQgKgKAAgMIAAgBQAAgJAEgIIAGgHQAMgKAPAAQAPAAALAKIAHAHQAEAIAAAJIAAABQgBAMgKAKQAKgKABgMIAAgBQAAgJgEgIIgHgHQgLgKgPAAQgPAAgMAKIgGAHQgEAIAAAJIAAABIAAAAg");
	this.shape_1.setTransform(-190.75,187.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer_2
	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(13,1,1).p("ACIANQAKhlkZB5");
	this.shape_2.setTransform(-204.5829,185.8202);

	this.timeline.addTween(cjs.Tween.get(this.shape_2).wait(1));

	// Layer_1
	this.instance = new lib.pack();
	this.instance.parent = this;
	this.instance.setTransform(-204,-251);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol1, new cjs.Rectangle(-224.7,-251,428.7,502), null);


(lib.Symbol_12_Layer_9 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_9
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(13,1,1).p("AH3poQBUFIglETQgkEJiSClQiUCnjnAcQj1AdkwiE");
	this.shape.setTransform(105.8384,-9.1646);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(13,1,1).p("AoxH6QEwCFD1gcQDmgbCVinQCTikAkkJQAmkThTlI");
	this.shape_1.setTransform(105.6192,-9.2815);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(13,1,1).p("Ao0HxQEuCID1gZQDngZCWilQCUijAnkIQApkRhQlJ");
	this.shape_2.setTransform(104.9306,-9.6987);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(13,1,1).p("Ao5HhQErCOD0gVQDngUCZiiQCWifAtkHQAtkQhJlJ");
	this.shape_3.setTransform(103.6449,-10.4643);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(13,1,1).p("ApBHIQElCWD1gOQDlgOCeicQCaiaA0kGQA1kMg/lK");
	this.shape_4.setTransform(101.7482,-11.5693);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(13,1,1).p("ApMGoQEeCgD0gEQDlgGCjiWQCfiUA+kCQA/kKg0lL");
	this.shape_5.setTransform(99.4832,-12.9188);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(13,1,1).p("ApZGGQEYCrDzAFQDlAECoiQQCliNBGkAQBKkGgnlM");
	this.shape_6.setTransform(97.1867,-14.2946);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(13,1,1).p("ApnFnQESC1DyANQDlAMCtiJQCpiHBRj9QBSkDgalO");
	this.shape_7.setTransform(95.2115,-15.5517);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(13,1,1).p("ApzFLQEMC9DyAVQDkAUCxiEQCuiCBYj6QBbkBgPlP");
	this.shape_8.setTransform(93.5878,-16.5745);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(13,1,1).p("Ap+EzQEHDFDyAbQDkAaC0iAQCyh9Bfj4QBhj/gGlP");
	this.shape_9.setTransform(92.2703,-17.4298);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(13,1,1).p("AqIEfQEDDLDyAgQDjAfC4h7QC1h6Bkj2QBoj9AAlQ");
	this.shape_10.setTransform(91.225,-18.114);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(13,1,1).p("AqREOQEADQDxAlQDkAjC6h4QC4h3Bpj1QBsj7AHlR");
	this.shape_11.setTransform(90.4,-18.6648);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(13,1,1).p("AqYEAQD9DUDyApQDjAmC8h2QC5h0BujzQBwj7AMlQ");
	this.shape_12.setTransform(89.7,-19.1264);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(13,1,1).p("AqdD1QD6DXDxAsQDkApC+hzQC7hyBwjzQBzj5AQlR");
	this.shape_13.setTransform(89.125,-19.5015);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(13,1,1).p("AqiDrQD5DaDxAuQDjAsC/hyQC9hwBzjyQB1j4AUlS");
	this.shape_14.setTransform(88.65,-19.7974);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(13,1,1).p("AqmDjQD4DdDwAwQDkAtDAhwQC+hvB0jxQB5j4AWlR");
	this.shape_15.setTransform(88.275,-20.0428);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(13,1,1).p("AqpDdQD2DeDxAyQDjAvDChvQC+huB2jwQB7j4AYlR");
	this.shape_16.setTransform(87.975,-20.2259);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(13,1,1).p("AqrDYQD1DgDxAzQDjAwDChuQC/htB4jwQB7j3AalS");
	this.shape_17.setTransform(87.725,-20.3709);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#000000").ss(13,1,1).p("AqtDVQD1DhDwA0QDjAxDDhuQDAhsB4jwQB9j3AblS");
	this.shape_18.setTransform(87.575,-20.47);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#000000").ss(13,1,1).p("AquDTQD0DhDwA1QDjAxDEhtQDAhsB5jvQB9j3AclS");
	this.shape_19.setTransform(87.45,-20.552);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#000000").ss(13,1,1).p("AKwn3QgdFSh9D3Qh6DvjABsQjDBtjjgyQjxg1j0ji");
	this.shape_20.setTransform(87.375,-20.5888);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#000000").ss(13,1,1).p("AquDSQD0DiDwA1QDjAxDEhtQC/hsB6jvQB9j3AclS");
	this.shape_21.setTransform(87.45,-20.552);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#000000").ss(13,1,1).p("AqsDXQD1DgDxA0QDjAwDChuQC/hsB4jwQB9j3AalS");
	this.shape_22.setTransform(87.65,-20.408);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#000000").ss(13,1,1).p("AqoDfQD3DeDxAxQDjAvDBhwQC+huB2jxQB5j4AYlR");
	this.shape_23.setTransform(88.075,-20.1429);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#000000").ss(13,1,1).p("AqhDuQD6DZDxAuQDjArC/hyQC8hxByjyQB1j5ATlR");
	this.shape_24.setTransform(88.775,-19.7111);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f().s("#000000").ss(13,1,1).p("AqWEEQD+DTDxAnQDjAmC8h2QC5h1Bsj0QBvj7ALlQ");
	this.shape_25.setTransform(89.875,-19.0228);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f().s("#000000").ss(13,1,1).p("AqHEiQEEDKDyAgQDkAdC3h8QC0h6Bjj3QBnj9gBlQ");
	this.shape_26.setTransform(91.4001,-17.9915);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f().s("#000000").ss(13,1,1).p("Ap0FJQELC+DyAVQDlAUCxiDQCuiBBZj6QBckBgPlP");
	this.shape_27.setTransform(93.4556,-16.668);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f().s("#000000").ss(13,1,1).p("ApiFwQETCyDzALQDkAKCsiMQCoiJBNj9QBQkEgelN");
	this.shape_28.setTransform(95.7841,-15.1666);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f().s("#000000").ss(13,1,1).p("ApVGSQEbCnDzACQDlAACmiRQCjiQBEkAQBFkIgrlM");
	this.shape_29.setTransform(97.9718,-13.8249);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f().s("#000000").ss(13,1,1).p("ApKGuQEfCeD0gGQDmgHChiXQCfiVA8kDQA9kKg1lL");
	this.shape_30.setTransform(99.8445,-12.7122);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f().s("#000000").ss(13,1,1).p("ApDHCQEkCYD0gMQDmgMCeicQCciYA1kFQA3kMg9lL");
	this.shape_31.setTransform(101.3233,-11.8275);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f().s("#000000").ss(13,1,1).p("Ao+HSQEoCTD0gRQDmgQCcifQCYicAxkGQAykOhDlK");
	this.shape_32.setTransform(102.5138,-11.1165);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f().s("#000000").ss(13,1,1).p("Ao6HfQEqCOD1gUQDmgUCZihQCXifAtkGQAvkQhJlJ");
	this.shape_33.setTransform(103.4585,-10.5711);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f().s("#000000").ss(13,1,1).p("Ao3HoQEsCLD1gXQDmgWCYijQCVihArkHQArkRhMlJ");
	this.shape_34.setTransform(104.2134,-10.1367);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f().s("#000000").ss(13,1,1).p("Ao1HvQEuCJD1gZQDmgYCXilQCUiiAokIQApkRhPlJ");
	this.shape_35.setTransform(104.7504,-9.7999);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f().s("#000000").ss(13,1,1).p("AozH1QEvCHD1gbQDmgZCWimQCUijAmkJQAokRhSlJ");
	this.shape_36.setTransform(105.1834,-9.5507);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f().s("#000000").ss(13,1,1).p("AoyH4QEwCGD1gbQDmgbCWinQCSijAmkKQAmkShTlI");
	this.shape_37.setTransform(105.485,-9.3659);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f().s("#000000").ss(13,1,1).p("AoxH7QEwCFD1gdQDngbCVinQCSilAkkJQAmkShUlJ");
	this.shape_38.setTransform(105.6918,-9.2485);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f().s("#000000").ss(13,1,1).p("AoxH8QExCFD1gdQDmgcCVinQCSilAkkJQAlkThUlI");
	this.shape_39.setTransform(105.8013,-9.1896);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},39).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_20}]},18).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_27}]},1).to({state:[{t:this.shape_28}]},1).to({state:[{t:this.shape_29}]},1).to({state:[{t:this.shape_30}]},1).to({state:[{t:this.shape_31}]},1).to({state:[{t:this.shape_32}]},1).to({state:[{t:this.shape_33}]},1).to({state:[{t:this.shape_34}]},1).to({state:[{t:this.shape_35}]},1).to({state:[{t:this.shape_36}]},1).to({state:[{t:this.shape_37}]},1).to({state:[{t:this.shape_38}]},1).to({state:[{t:this.shape_39}]},1).to({state:[{t:this.shape}]},1).wait(3));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_12_Layer_8 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_8
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(13,1,1).p("AHFHGQm4svnRhc");
	this.shape.setTransform(-51.975,-43.775);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(13,1,1).p("AnDnHQHOBfG5Mw");
	this.shape_1.setTransform(-51.85,-43.9);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(13,1,1).p("Am+nLQHFBnG4Mv");
	this.shape_2.setTransform(-51.425,-44.3);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(13,1,1).p("Am4nRQG5BzG4Mw");
	this.shape_3.setTransform(-50.775,-44.925);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(13,1,1).p("AmunaQGmCFG3Mw");
	this.shape_4.setTransform(-49.825,-45.85);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(13,1,1).p("AminmQGOCdG3Mw");
	this.shape_5.setTransform(-48.6,-47);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(13,1,1).p("AmTn0QFwC5G3Mw");
	this.shape_6.setTransform(-47.125,-48.425);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(13,1,1).p("AGDIFQm3svlOjb");
	this.shape_7.setTransform(-45.375,-50.1);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(13,1,1).p("AmSndQFuCLG3Mw");
	this.shape_8.setTransform(-47.025,-46.15);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(13,1,1).p("Amjm2QGQA9G3Mv");
	this.shape_9.setTransform(-48.675,-42.2);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(13,1,1).p("AmzmOQGwgSG3Mv");
	this.shape_10.setTransform(-50.325,-38.2944);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(13,1,1).p("AHFFtQm4swnRBh");
	this.shape_11.setTransform(-51.975,-34.84);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(13,1,1).p("AnEl9QHRgyG4Mw");
	this.shape_12.setTransform(-51.975,-36.8193);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(13,1,1).p("AnEmWQHRgCG4Mv");
	this.shape_13.setTransform(-51.975,-39.0504);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(13,1,1).p("AnEmuQHRAtG4Mw");
	this.shape_14.setTransform(-51.975,-41.4);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},9).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape}]},1).wait(61));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_10_Layer_9 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_9
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(13,1,1).p("Ai8gwQANA/A7AkQA1AhBFgJQBEgIAzgrQA3gtAJg3");
	this.shape.setTransform(-28.075,33.4881);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(13,1,1).p("AjGgxQAVBAA7AjQA2AhBHgJQBHgIA0grQA3gtAOg1");
	this.shape_1.setTransform(-28.525,33.5714);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(13,1,1).p("AjPgyQAcBAA7AkQA2AhBKgJQBKgIAzgrQA3guAUg0");
	this.shape_2.setTransform(-28.9,33.637);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(13,1,1).p("AjWgzQAjBBA5AjQA3AhBMgJQBNgIAzgrQA3guAXgy");
	this.shape_3.setTransform(-29.275,33.7203);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(13,1,1).p("AjdgzQApBAA5AjQA3AiBOgJQBPgJAzgrQA3gtAbgy");
	this.shape_4.setTransform(-29.6,33.7703);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(13,1,1).p("Ajjg0QAuBBA5AjQA3AhBQgIQBQgJA0grQA3guAegx");
	this.shape_5.setTransform(-29.825,33.811);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(13,1,1).p("Ajog0QAyBBA5AiQA3AiBRgJQBSgJA0grQA3gtAggw");
	this.shape_6.setTransform(-30.05,33.8692);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(13,1,1).p("Ajrg0QA1BBA4AiQA4AiBSgJQBTgJA0grQA3gtAigw");
	this.shape_7.setTransform(-30.2,33.8942);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(13,1,1).p("Ajug0QA3BBA5AiQA4AiBSgJQBUgJA0grQA3gtAkgw");
	this.shape_8.setTransform(-30.325,33.8942);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(13,1,1).p("Ajvg1QA4BBA4AjQA5AiBTgJQBUgJAzgrQA3guAlgv");
	this.shape_9.setTransform(-30.4,33.9192);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(13,1,1).p("Ajwg1QA5BBA4AjQA4AiBUgJQBUgJA0grQA3guAlgv");
	this.shape_10.setTransform(-30.425,33.9186);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(13,1,1).p("Ajkg0QAvBBA5AjQA3AhBQgIQBRgJAzgrQA3guAfgx");
	this.shape_11.setTransform(-29.875,33.811);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(13,1,1).p("AjZgzQAlBBA6AjQA3AhBNgJQBNgIA0grQA3guAYgy");
	this.shape_12.setTransform(-29.4,33.7203);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(13,1,1).p("AjQgyQAdBAA7AjQA2AhBLgIQBKgJA0grQA3gtATg0");
	this.shape_13.setTransform(-29,33.662);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(13,1,1).p("AjJgyQAYBAA6AkQA2AhBJgJQBIgIAzgrQA3guAQg0");
	this.shape_14.setTransform(-28.675,33.612);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(13,1,1).p("AjDgxQASBAA7AkQA2AgBGgIQBHgIAzgrQA3guANg1");
	this.shape_15.setTransform(-28.4,33.5381);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(13,1,1).p("Ai/gxQAPBAA7AkQA1AgBGgIQBFgIAzgrQA4guAKg2");
	this.shape_16.setTransform(-28.2,33.5131);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(13,1,1).p("Ai9gwQANA/A7AkQA2AhBEgJQBFgIAzgrQA3gtAJg3");
	this.shape_17.setTransform(-28.1,33.4881);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},39).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_10}]},37).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape}]},1).wait(6));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_10_Layer_8 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_8
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#C7C7C7").s().p("AhjAIQApgpA8AAQA9AAAjAiQAjAjgSgNQgSgNglgQQglgRgcACQgdACgPAJQgPAJgmAZQgPAKgDAAQgEAAAZgag");
	this.shape.setTransform(-68.9275,-22.0279);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#C7C7C7").s().p("AiTA1QADgdArgrQArgsA8AAQA9AAArAsQArArgBAXQgBAXg3gxQg3gvg2AKQg4AKgmAsQgYAagIAAQgFAAABgLg");
	this.shape_1.setTransform(-68.9097,-19.0359);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#C7C7C7").s().p("ABwBLQgkgphAAHQg/AHgwAgQgxAgACg6QABg4ArgtQArgsA8AAQA9AAArAsQArAtAAA6QgBAkgMAAQgJAAgOgRg");
	this.shape_2.setTransform(-68.8036,-16.2799);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#C7C7C7").s().p("AAAB7Qg7gFgrgNQgsgOAAg/QAAg+AqgsQAsgsA8AAQA9AAArAsQArAsAAA+QAAA/gqASQgjAOguAAIgYAAg");
	this.shape_3.setTransform(-68.7,-13.105);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#C7C7C7").s().p("AhoBrQgqgsAAg/QAAg+AqgtQAsgsA8AAQA9AAArAsQArAtAAA+QAAA/grAsQgrAsg9AAQg8AAgsgsg");
	this.shape_4.setTransform(-68.7,-10.3);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#C7C7C7").s().p("AAAB7Qg7gFgrgNQgsgOAAg/QAAg+ArgsQArgsA8AAQA9AAArAsQArAsAAA+QAAA/gqASQgjAOguAAIgYAAg");
	this.shape_5.setTransform(-68.7,-13.105);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape}]},12).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape}]},1).to({state:[]},1).to({state:[{t:this.shape}]},44).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape}]},1).to({state:[]},1).wait(24));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_10_Layer_7 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_7
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AhnBrQgrgsAAg/QAAg+ArgtQArgsA8AAQA9AAArAsQArAtAAA+QAAA/grAsQgrAsg9AAQg8AAgrgsg");
	this.shape.setTransform(-3.7,-10.2);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(100));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_10_Layer_2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AhnBrQgrgsAAg/QAAg+ArgtQArgrA8gBQA9ABArArQArAtAAA+QAAA/grAsQgrAtg9AAQg8AAgrgtg");
	this.shape.setTransform(-68.7,-10.3);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(100));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_10_Layer_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#C7C7C7").s().p("AhjAIQApgpA8AAQA9AAAjAiQAjAjgSgNQgSgNglgQQglgRgcACQgdACgPAJQgPAJgmAZQgPAKgDAAQgEAAAZgag");
	this.shape.setTransform(-3.9306,-21.9285);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#C7C7C7").s().p("AiTA1QADgdArgrQArgsA8AAQA9AAArAsQArArgBAXQgBAXg3gxQg3gvg3AKQg3AKgnAsQgXAagIAAQgFAAABgLg");
	this.shape_1.setTransform(-3.9213,-18.9323);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#C7C7C7").s().p("ABvBLQgjgphAAHQg/AHgxAgQgwAgABg6QACg4ArgtQArgsA8AAQA9AAArAsQArAtAAA6QgBAkgMAAQgJAAgPgRg");
	this.shape_2.setTransform(-3.8156,-16.1805);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#C7C7C7").s().p("AAAB7Qg7gFgsgNQgrgOAAg/QAAg+ArgsQArgsA8AAQA9AAArAsQArAsAAA+QAAA/grASQgiAOgtAAIgZAAg");
	this.shape_3.setTransform(-3.7,-13.0028);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#C7C7C7").s().p("AhnBrQgrgsAAg/QAAg+ArgtQArgsA8AAQA9AAArAsQArAtAAA+QAAA/grAsQgrAsg9AAQg8AAgrgsg");
	this.shape_4.setTransform(-3.7,-10.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape}]},12).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape}]},1).to({state:[]},1).to({state:[{t:this.shape}]},44).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape}]},1).to({state:[]},1).wait(24));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_7_Symbol_6 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Symbol_6
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(13,1,1).p("AB9BoQAkj4khAw");
	this.shape.setTransform(-0.0061,-0.8379);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol_7_Symbol_6, null, null);


(lib.Symbol_7_Layer_3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_3
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(13,1,1).p("ABYAUQiiiCgNCX");
	this.shape.setTransform(0.475,7.2805);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol_7_Layer_3, null, null);


(lib.Symbol_5_Symbol_4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Symbol_4
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(12,1,1).p("ABgAsQhRiQhuBg");
	this.shape.setTransform(1.925,-2.653);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(12,1,1).p("AhfgGQBuheBRCQ");
	this.shape_1.setTransform(1.925,-2.6894);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(12,1,1).p("AhfgLQBthYBSCQ");
	this.shape_2.setTransform(1.95,-2.8118);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(12,1,1).p("AhfgTQBthOBSCQ");
	this.shape_3.setTransform(2,-3.0247);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(12,1,1).p("AhegfQBrg+BSCQ");
	this.shape_4.setTransform(2.075,-3.3663);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(12,1,1).p("AhdgtQBpgrBSCQ");
	this.shape_5.setTransform(2.15,-3.8769);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(12,1,1).p("Ahcg8QBogUBRCQ");
	this.shape_6.setTransform(2.25,-4.6712);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(12,1,1).p("ABcBMQhSiQhlgH");
	this.shape_7.setTransform(2.375,-5.925);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(12,1,1).p("AhihFQBzgEBSCQ");
	this.shape_8.setTransform(1.7,-5.3554);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(12,1,1).p("Ahog/QB/gQBSCQ");
	this.shape_9.setTransform(1.05,-4.8498);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(12,1,1).p("Ahvg4QCNgbBSCQ");
	this.shape_10.setTransform(0.375,-4.4119);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(12,1,1).p("AB3A5QhSiQibAn");
	this.shape_11.setTransform(-0.3,-4.0289);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(12,1,1).p("AhwgmQCQg1BRCQ");
	this.shape_12.setTransform(0.25,-3.6093);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(12,1,1).p("AhqgbQCEhEBRCQ");
	this.shape_13.setTransform(0.825,-3.2471);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(12,1,1).p("AhlgQQB5hSBSCQ");
	this.shape_14.setTransform(1.375,-2.9311);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},9).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape}]},1).wait(61));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_5_Symbol_3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Symbol_3
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(12,1,1).p("AB1BOQhXjEiSA0");
	this.shape.setTransform(-0.125,-8.7347);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(12,1,1).p("AhzhEQCRgxBWDD");
	this.shape_1.setTransform(-0.075,-8.798);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(12,1,1).p("AhyhHQCOgsBXDD");
	this.shape_2.setTransform(0.075,-8.9775);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(12,1,1).p("AhvhNQCJgjBWDD");
	this.shape_3.setTransform(0.325,-9.2896);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(12,1,1).p("AhshVQCCgWBXDD");
	this.shape_4.setTransform(0.675,-9.7858);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(12,1,1).p("AhnheQB4gGBXDE");
	this.shape_5.setTransform(1.15,-10.5075);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(12,1,1).p("AhihpQBuAPBXDE");
	this.shape_6.setTransform(1.7,-11.5);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(12,1,1).p("ABcB2QhXjEhggn");
	this.shape_7.setTransform(2.35,-12.7);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(12,1,1).p("AhkhwQByAdBXDE");
	this.shape_8.setTransform(1.475,-12.2);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(12,1,1).p("AhshrQCDATBWDE");
	this.shape_9.setTransform(0.625,-11.7);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(12,1,1).p("Ah1hmQCVAJBWDE");
	this.shape_10.setTransform(-0.25,-11.2);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(12,1,1).p("AB/BiQhXjEimAC");
	this.shape_11.setTransform(-1.125,-10.7006);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(12,1,1).p("Ah7haQChgOBWDE");
	this.shape_12.setTransform(-0.875,-10.1216);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(12,1,1).p("Ah5hSQCcgbBXDE");
	this.shape_13.setTransform(-0.625,-9.6075);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(12,1,1).p("Ah2hLQCXgnBWDE");
	this.shape_14.setTransform(-0.375,-9.1479);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},9).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape}]},1).wait(61));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_5_Layer_4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_4
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(12,1,1).p("AALhYIgVCx");
	this.shape.setTransform(10.725,7.7);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(12,1,1).p("AgKBWIAViq");
	this.shape_1.setTransform(10.775,7.65);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(12,1,1).p("AgJBMIAUiX");
	this.shape_2.setTransform(10.9,7.475);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(12,1,1).p("AgJA9IATh5");
	this.shape_3.setTransform(11.075,7.225);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(12,1,1).p("AgJAnIAThN");
	this.shape_4.setTransform(11.35,6.85);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(12,1,1).p("AgIALIASgV");
	this.shape_5.setTransform(11.7,6.375);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(12,1,1).p("AgIgWIARAt");
	this.shape_6.setTransform(12.15,5.8);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(12,1,1).p("AgHg+IAPB8");
	this.shape_7.setTransform(12.65,5.1);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(12,1,1).p("AgMgaQAbAdgCAY");
	this.shape_8.setTransform(11.8101,5.525);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(12,1,1).p("AgRAIQAugCgNgN");
	this.shape_9.setTransform(11.0994,5.925);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(12,1,1).p("AgYAsQBDgjgZg0");
	this.shape_10.setTransform(10.4262,6.35);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(12,1,1).p("AAVhOQAjBahWBD");
	this.shape_11.setTransform(9.7346,6.75);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(12,1,1).p("AgYBSQBDhJgYhZ");
	this.shape_12.setTransform(9.913,7);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(12,1,1).p("AgTBUQAwhOgMhZ");
	this.shape_13.setTransform(10.102,7.225);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(12,1,1).p("AgOBWQAdhTgBhY");
	this.shape_14.setTransform(10.3516,7.475);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},9).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape}]},1).wait(61));

}).prototype = p = new cjs.MovieClip();


(lib.Scene_1_Layer_2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.instance = new lib.bg();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Scene_1_Layer_2, null, null);


(lib.Symbol7 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Symbol_6_obj_
	this.Symbol_6 = new lib.Symbol_7_Symbol_6();
	this.Symbol_6.name = "Symbol_6";
	this.Symbol_6.parent = this;
	this.Symbol_6.setTransform(0,-0.8,1,1,0,0,0,0,-0.8);
	this.Symbol_6.depth = 0;
	this.Symbol_6.isAttachedToCamera = 0
	this.Symbol_6.isAttachedToMask = 0
	this.Symbol_6.layerDepth = 0
	this.Symbol_6.layerIndex = 0
	this.Symbol_6.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Symbol_6).wait(1));

	// Layer_3_obj_
	this.Layer_3 = new lib.Symbol_7_Layer_3();
	this.Layer_3.name = "Layer_3";
	this.Layer_3.parent = this;
	this.Layer_3.setTransform(0.5,7.3,1,1,0,0,0,0.5,7.3);
	this.Layer_3.depth = 0;
	this.Layer_3.isAttachedToCamera = 0
	this.Layer_3.isAttachedToMask = 0
	this.Layer_3.layerDepth = 0
	this.Layer_3.layerIndex = 1
	this.Layer_3.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_3).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol7, new cjs.Rectangle(-19.4,-17.8,38.8,35.7), null);


(lib.Symbol5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_99 = function() {
		this.___loopingOver___ = true;
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(99).call(this.frame_99).wait(1));

	// Symbol_3_obj_
	this.Symbol_3 = new lib.Symbol_5_Symbol_3();
	this.Symbol_3.name = "Symbol_3";
	this.Symbol_3.parent = this;
	this.Symbol_3.setTransform(-0.1,-8.8,1,1,0,0,0,-0.1,-8.8);
	this.Symbol_3.depth = 0;
	this.Symbol_3.isAttachedToCamera = 0
	this.Symbol_3.isAttachedToMask = 0
	this.Symbol_3.layerDepth = 0
	this.Symbol_3.layerIndex = 0
	this.Symbol_3.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Symbol_3).wait(100));

	// Symbol_4_obj_
	this.Symbol_4 = new lib.Symbol_5_Symbol_4();
	this.Symbol_4.name = "Symbol_4";
	this.Symbol_4.parent = this;
	this.Symbol_4.setTransform(1.9,-2.6,1,1,0,0,0,1.9,-2.6);
	this.Symbol_4.depth = 0;
	this.Symbol_4.isAttachedToCamera = 0
	this.Symbol_4.isAttachedToMask = 0
	this.Symbol_4.layerDepth = 0
	this.Symbol_4.layerIndex = 1
	this.Symbol_4.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Symbol_4).wait(100));

	// Layer_4_obj_
	this.Layer_4 = new lib.Symbol_5_Layer_4();
	this.Layer_4.name = "Layer_4";
	this.Layer_4.parent = this;
	this.Layer_4.setTransform(10.7,7.7,1,1,0,0,0,10.7,7.7);
	this.Layer_4.depth = 0;
	this.Layer_4.isAttachedToCamera = 0
	this.Layer_4.isAttachedToMask = 0
	this.Layer_4.layerDepth = 0
	this.Layer_4.layerIndex = 2
	this.Layer_4.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_4).wait(100));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-19.8,-30.4,39.3,53);


(lib.Symbol_12_Symbol_11 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Symbol_11
	this.instance = new lib.Symbol11();
	this.instance.parent = this;
	this.instance.setTransform(-97.3,159.9,1,1,0,0,0,-5.3,127.9);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(18).to({scaleY:0.9518,y:159.95},2).to({scaleY:1,y:159.9},4).wait(9).to({scaleY:0.9518,y:159.95},2).to({scaleY:1,y:159.9},4).wait(61));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_12_Symbol_7 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Symbol_7
	this.instance = new lib.Symbol7();
	this.instance.parent = this;
	this.instance.setTransform(54.85,44.45,1,1,0,0,0,12.9,11.3);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(40).to({regX:0,regY:0,x:41.6391,y:32.7595},0).wait(1).to({x:40.5795,y:31.4284},0).wait(1).to({x:38.5822,y:28.9194},0).wait(1).to({x:35.5804,y:25.1485},0).wait(1).to({x:31.8338,y:20.4422},0).wait(1).to({x:27.8928,y:15.4915},0).wait(1).to({x:24.2304,y:10.8908},0).wait(1).to({x:21.048,y:6.8931},0).wait(1).to({x:18.3625,y:3.5197},0).wait(1).to({x:16.1218,y:0.705},0).wait(1).to({x:14.261,y:-1.6325},0).wait(1).to({x:12.7208,y:-3.5673},0).wait(1).to({x:11.4518,y:-5.1614},0).wait(1).to({x:10.4141,y:-6.4649},0).wait(1).to({x:9.5754,y:-7.5185},0).wait(1).to({x:8.9096,y:-8.3549},0).wait(1).to({x:8.3953,y:-9.0009},0).wait(1).to({x:8.015,y:-9.4787},0).wait(1).to({x:7.754,y:-9.8066},0).wait(1).to({regX:12.9,regY:11.3,x:20.5,y:1.3},0).wait(19).to({regX:0,regY:0,x:7.743,y:-9.8204},0).wait(1).to({x:8.2278,y:-9.2114},0).wait(1).to({x:9.1657,y:-8.0331},0).wait(1).to({x:10.7175,y:-6.0839},0).wait(1).to({x:13.0974,y:-3.0942},0).wait(1).to({x:16.5009,y:1.1812},0).wait(1).to({x:20.8083,y:6.5921},0).wait(1).to({x:25.3145,y:12.2527},0).wait(1).to({x:29.2691,y:17.2204},0).wait(1).to({x:32.4387,y:21.2021},0).wait(1).to({x:34.9083,y:24.3044},0).wait(1).to({x:36.8228,y:26.7093},0).wait(1).to({x:38.305,y:28.5713},0).wait(1).to({x:39.4471,y:30.0059},0).wait(1).to({x:40.3164,y:31.0978},0).wait(1).to({x:40.9626,y:31.9096},0).wait(1).to({x:41.4233,y:32.4884},0).wait(1).to({x:41.7272,y:32.8702},0).wait(1).to({x:41.8968,y:33.0832},0).wait(1).to({regX:12.9,regY:11.3,x:54.85,y:44.45},0).wait(3));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_12_Symbol_5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Symbol_5
	this.instance = new lib.Symbol5();
	this.instance.parent = this;
	this.instance.setTransform(-93.25,-88.3,1,1,0,0,0,15.2,-2.2);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(9).to({rotation:29.9992,x:-83.1,y:-102.1},7,cjs.Ease.get(-1)).to({rotation:0,x:-97.25,y:-70.3},4).to({x:-93.25,y:-88.3},4).to({rotation:29.9992,x:-83.1,y:-102.1},7,cjs.Ease.get(-1)).to({rotation:0,x:-97.25,y:-70.3},4).to({x:-93.25,y:-88.3},4).wait(61));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_12_Symbol_2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Symbol_2
	this.instance = new lib.Symbol2();
	this.instance.parent = this;
	this.instance.setTransform(-26.5,28.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(40).to({x:-26.8109,y:28.1095},0).wait(1).to({x:-27.8705,y:26.7784},0).wait(1).to({x:-29.8678,y:24.2694},0).wait(1).to({x:-32.8696,y:20.4985},0).wait(1).to({x:-36.6162,y:15.7922},0).wait(1).to({x:-40.5572,y:10.8415},0).wait(1).to({x:-44.2196,y:6.2408},0).wait(1).to({x:-47.402,y:2.2431},0).wait(1).to({x:-50.0875,y:-1.1303},0).wait(1).to({x:-52.3282,y:-3.945},0).wait(1).to({x:-54.189,y:-6.2825},0).wait(1).to({x:-55.7292,y:-8.2173},0).wait(1).to({x:-56.9982,y:-9.8114},0).wait(1).to({x:-58.0359,y:-11.1149},0).wait(1).to({x:-58.8746,y:-12.1685},0).wait(1).to({x:-59.5404,y:-13.0049},0).wait(1).to({x:-60.0547,y:-13.6509},0).wait(1).to({x:-60.435,y:-14.1287},0).wait(1).to({x:-60.696,y:-14.4566},0).wait(1).to({x:-60.85,y:-14.65},0).wait(19).to({x:-60.707,y:-14.4704},0).wait(1).to({x:-60.2222,y:-13.8614},0).wait(1).to({x:-59.2843,y:-12.6831},0).wait(1).to({x:-57.7325,y:-10.7339},0).wait(1).to({x:-55.3526,y:-7.7442},0).wait(1).to({x:-51.9491,y:-3.4688},0).wait(1).to({x:-47.6417,y:1.9421},0).wait(1).to({x:-43.1355,y:7.6027},0).wait(1).to({x:-39.1809,y:12.5704},0).wait(1).to({x:-36.0113,y:16.5521},0).wait(1).to({x:-33.5417,y:19.6544},0).wait(1).to({x:-31.6272,y:22.0593},0).wait(1).to({x:-30.145,y:23.9213},0).wait(1).to({x:-29.0029,y:25.3559},0).wait(1).to({x:-28.1336,y:26.4478},0).wait(1).to({x:-27.4874,y:27.2596},0).wait(1).to({x:-27.0267,y:27.8384},0).wait(1).to({x:-26.7228,y:28.2202},0).wait(1).to({x:-26.5532,y:28.4332},0).wait(1).to({x:-26.5,y:28.5},0).wait(3));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_12_Symbol_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Symbol_1
	this.instance = new lib.Symbol1();
	this.instance.parent = this;
	this.instance.setTransform(91.5,0);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(100));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_10_Symbol_9_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Symbol_9
	this.instance = new lib.Symbol9();
	this.instance.parent = this;
	this.instance.setTransform(-7.5,-14.15);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(4).to({x:-12,y:-9.65},6,cjs.Ease.get(1)).wait(26).to({x:-3,y:-14.15},6,cjs.Ease.get(1)).wait(44).to({x:-7.5},6,cjs.Ease.get(1)).wait(8));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_10_Symbol_9 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Symbol_9
	this.instance = new lib.Symbol9();
	this.instance.parent = this;
	this.instance.setTransform(-72.5,-14.25);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(4).to({x:-77,y:-9.75},6,cjs.Ease.get(1)).wait(26).to({x:-68,y:-14.25},6,cjs.Ease.get(1)).wait(44).to({x:-72.5},6,cjs.Ease.get(1)).wait(8));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_10_Symbol_8_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Symbol_8
	this.instance = new lib.Symbol8();
	this.instance.parent = this;
	this.instance.setTransform(0,5.35);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(100));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_10_Symbol_8 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Symbol_8
	this.instance = new lib.Symbol8();
	this.instance.parent = this;
	this.instance.setTransform(-72.4,2.85,1,1,0,0,180);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(100));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol10 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_99 = function() {
		this.___loopingOver___ = true;
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(99).call(this.frame_99).wait(1));

	// Layer_9_obj_
	this.Layer_9 = new lib.Symbol_10_Layer_9();
	this.Layer_9.name = "Layer_9";
	this.Layer_9.parent = this;
	this.Layer_9.setTransform(-28.1,33.5,1,1,0,0,0,-28.1,33.5);
	this.Layer_9.depth = 0;
	this.Layer_9.isAttachedToCamera = 0
	this.Layer_9.isAttachedToMask = 0
	this.Layer_9.layerDepth = 0
	this.Layer_9.layerIndex = 0
	this.Layer_9.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_9).wait(100));

	// Symbol_8_obj_
	this.Symbol_8 = new lib.Symbol_10_Symbol_8();
	this.Symbol_8.name = "Symbol_8";
	this.Symbol_8.parent = this;
	this.Symbol_8.setTransform(-72.4,2.9,1,1,0,0,0,-72.4,2.9);
	this.Symbol_8.depth = 0;
	this.Symbol_8.isAttachedToCamera = 0
	this.Symbol_8.isAttachedToMask = 0
	this.Symbol_8.layerDepth = 0
	this.Symbol_8.layerIndex = 1
	this.Symbol_8.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Symbol_8).wait(100));

	// Layer_8_obj_
	this.Layer_8 = new lib.Symbol_10_Layer_8();
	this.Layer_8.name = "Layer_8";
	this.Layer_8.parent = this;
	this.Layer_8.depth = 0;
	this.Layer_8.isAttachedToCamera = 0
	this.Layer_8.isAttachedToMask = 0
	this.Layer_8.layerDepth = 0
	this.Layer_8.layerIndex = 2
	this.Layer_8.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_8).wait(100));

	// Symbol_9_obj_
	this.Symbol_9 = new lib.Symbol_10_Symbol_9();
	this.Symbol_9.name = "Symbol_9";
	this.Symbol_9.parent = this;
	this.Symbol_9.setTransform(-72.5,-14.3,1,1,0,0,0,-72.5,-14.3);
	this.Symbol_9.depth = 0;
	this.Symbol_9.isAttachedToCamera = 0
	this.Symbol_9.isAttachedToMask = 0
	this.Symbol_9.layerDepth = 0
	this.Symbol_9.layerIndex = 3
	this.Symbol_9.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Symbol_9).wait(100));

	// Layer_2_obj_
	this.Layer_2 = new lib.Symbol_10_Layer_2();
	this.Layer_2.name = "Layer_2";
	this.Layer_2.parent = this;
	this.Layer_2.setTransform(-68.7,-10.3,1,1,0,0,0,-68.7,-10.3);
	this.Layer_2.depth = 0;
	this.Layer_2.isAttachedToCamera = 0
	this.Layer_2.isAttachedToMask = 0
	this.Layer_2.layerDepth = 0
	this.Layer_2.layerIndex = 4
	this.Layer_2.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_2).wait(100));

	// Symbol_8_obj_
	this.Symbol_8_1 = new lib.Symbol_10_Symbol_8_1();
	this.Symbol_8_1.name = "Symbol_8_1";
	this.Symbol_8_1.parent = this;
	this.Symbol_8_1.setTransform(0,5.4,1,1,0,0,0,0,5.4);
	this.Symbol_8_1.depth = 0;
	this.Symbol_8_1.isAttachedToCamera = 0
	this.Symbol_8_1.isAttachedToMask = 0
	this.Symbol_8_1.layerDepth = 0
	this.Symbol_8_1.layerIndex = 5
	this.Symbol_8_1.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Symbol_8_1).wait(100));

	// Layer_1_obj_
	this.Layer_1 = new lib.Symbol_10_Layer_1();
	this.Layer_1.name = "Layer_1";
	this.Layer_1.parent = this;
	this.Layer_1.depth = 0;
	this.Layer_1.isAttachedToCamera = 0
	this.Layer_1.isAttachedToMask = 0
	this.Layer_1.layerDepth = 0
	this.Layer_1.layerIndex = 6
	this.Layer_1.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_1).wait(100));

	// Symbol_9_obj_
	this.Symbol_9_1 = new lib.Symbol_10_Symbol_9_1();
	this.Symbol_9_1.name = "Symbol_9_1";
	this.Symbol_9_1.parent = this;
	this.Symbol_9_1.setTransform(-7.5,-14.2,1,1,0,0,0,-7.5,-14.2);
	this.Symbol_9_1.depth = 0;
	this.Symbol_9_1.isAttachedToCamera = 0
	this.Symbol_9_1.isAttachedToMask = 0
	this.Symbol_9_1.layerDepth = 0
	this.Symbol_9_1.layerIndex = 7
	this.Symbol_9_1.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Symbol_9_1).wait(100));

	// Layer_7_obj_
	this.Layer_7 = new lib.Symbol_10_Layer_7();
	this.Layer_7.name = "Layer_7";
	this.Layer_7.parent = this;
	this.Layer_7.setTransform(-3.7,-10.2,1,1,0,0,0,-3.7,-10.2);
	this.Layer_7.depth = 0;
	this.Layer_7.isAttachedToCamera = 0
	this.Layer_7.isAttachedToMask = 0
	this.Layer_7.layerDepth = 0
	this.Layer_7.layerIndex = 8
	this.Layer_7.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_7).wait(100));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-96.9,-25.4,121.4,73.19999999999999);


(lib.Symbol_12_Symbol_10 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Symbol_10
	this.instance = new lib.Symbol10();
	this.instance.parent = this;
	this.instance.setTransform(84,-113.35);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(100));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol12 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_99 = function() {
		this.___loopingOver___ = true;
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(99).call(this.frame_99).wait(1));

	// Symbol_10_obj_
	this.Symbol_10 = new lib.Symbol_12_Symbol_10();
	this.Symbol_10.name = "Symbol_10";
	this.Symbol_10.parent = this;
	this.Symbol_10.setTransform(47.8,-102.3,1,1,0,0,0,47.8,-102.3);
	this.Symbol_10.depth = 0;
	this.Symbol_10.isAttachedToCamera = 0
	this.Symbol_10.isAttachedToMask = 0
	this.Symbol_10.layerDepth = 0
	this.Symbol_10.layerIndex = 0
	this.Symbol_10.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Symbol_10).wait(100));

	// Symbol_7_obj_
	this.Symbol_7 = new lib.Symbol_12_Symbol_7();
	this.Symbol_7.name = "Symbol_7";
	this.Symbol_7.parent = this;
	this.Symbol_7.setTransform(42,33.1,1,1,0,0,0,42,33.1);
	this.Symbol_7.depth = 0;
	this.Symbol_7.isAttachedToCamera = 0
	this.Symbol_7.isAttachedToMask = 0
	this.Symbol_7.layerDepth = 0
	this.Symbol_7.layerIndex = 1
	this.Symbol_7.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Symbol_7).wait(40).to({regX:24.8,regY:11.6,x:24.8,y:11.6},0).wait(19).to({regX:42,regY:33.1,x:42,y:33.1},0).wait(19).to({regX:24.8,regY:11.6,x:24.8,y:11.6},0).wait(19).to({regX:42,regY:33.1,x:42,y:33.1},0).wait(3));

	// Layer_9_obj_
	this.Layer_9 = new lib.Symbol_12_Layer_9();
	this.Layer_9.name = "Layer_9";
	this.Layer_9.parent = this;
	this.Layer_9.setTransform(105.9,-9.2,1,1,0,0,0,105.9,-9.2);
	this.Layer_9.depth = 0;
	this.Layer_9.isAttachedToCamera = 0
	this.Layer_9.isAttachedToMask = 0
	this.Layer_9.layerDepth = 0
	this.Layer_9.layerIndex = 2
	this.Layer_9.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_9).wait(100));

	// Symbol_2_obj_
	this.Symbol_2 = new lib.Symbol_12_Symbol_2();
	this.Symbol_2.name = "Symbol_2";
	this.Symbol_2.parent = this;
	this.Symbol_2.setTransform(-26.5,28.5,1,1,0,0,0,-26.5,28.5);
	this.Symbol_2.depth = 0;
	this.Symbol_2.isAttachedToCamera = 0
	this.Symbol_2.isAttachedToMask = 0
	this.Symbol_2.layerDepth = 0
	this.Symbol_2.layerIndex = 3
	this.Symbol_2.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Symbol_2).wait(40).to({regX:-43.7,regY:6.9,x:-43.7,y:6.9},0).wait(19).to({regX:-26.5,regY:28.5,x:-26.5,y:28.5},0).wait(19).to({regX:-43.7,regY:6.9,x:-43.7,y:6.9},0).wait(19).to({regX:-26.5,regY:28.5,x:-26.5,y:28.5},0).wait(3));

	// Symbol_1_obj_
	this.Symbol_1 = new lib.Symbol_12_Symbol_1();
	this.Symbol_1.name = "Symbol_1";
	this.Symbol_1.parent = this;
	this.Symbol_1.setTransform(81.2,0,1,1,0,0,0,81.2,0);
	this.Symbol_1.depth = 0;
	this.Symbol_1.isAttachedToCamera = 0
	this.Symbol_1.isAttachedToMask = 0
	this.Symbol_1.layerDepth = 0
	this.Symbol_1.layerIndex = 4
	this.Symbol_1.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Symbol_1).wait(100));

	// Symbol_5_obj_
	this.Symbol_5 = new lib.Symbol_12_Symbol_5();
	this.Symbol_5.name = "Symbol_5";
	this.Symbol_5.parent = this;
	this.Symbol_5.setTransform(-108.5,-86.1,1,1,0,0,0,-108.5,-86.1);
	this.Symbol_5.depth = 0;
	this.Symbol_5.isAttachedToCamera = 0
	this.Symbol_5.isAttachedToMask = 0
	this.Symbol_5.layerDepth = 0
	this.Symbol_5.layerIndex = 5
	this.Symbol_5.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Symbol_5).wait(100));

	// Layer_8_obj_
	this.Layer_8 = new lib.Symbol_12_Layer_8();
	this.Layer_8.name = "Layer_8";
	this.Layer_8.parent = this;
	this.Layer_8.setTransform(-52,-43.8,1,1,0,0,0,-52,-43.8);
	this.Layer_8.depth = 0;
	this.Layer_8.isAttachedToCamera = 0
	this.Layer_8.isAttachedToMask = 0
	this.Layer_8.layerDepth = 0
	this.Layer_8.layerIndex = 6
	this.Layer_8.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_8).wait(100));

	// Symbol_11_obj_
	this.Symbol_11 = new lib.Symbol_12_Symbol_11();
	this.Symbol_11.name = "Symbol_11";
	this.Symbol_11.parent = this;
	this.Symbol_11.setTransform(-92,32,1,1,0,0,0,-92,32);
	this.Symbol_11.depth = 0;
	this.Symbol_11.isAttachedToCamera = 0
	this.Symbol_11.isAttachedToMask = 0
	this.Symbol_11.layerDepth = 0
	this.Symbol_11.layerIndex = 7
	this.Symbol_11.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Symbol_11).wait(100));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-295.5,-251,591,502);


(lib.Scene_1_Layer_14 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_14
	this.instance = new lib.Symbol12();
	this.instance.parent = this;
	this.instance.setTransform(790.5,389);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Scene_1_Layer_14, null, null);


// stage content:
(lib.Feb_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	this.___GetDepth___ = function(obj) {
		var depth = obj.depth;
		var cameraObj = this.___camera___instance;
		if(cameraObj && cameraObj.depth && obj.isAttachedToCamera)
		{
			depth += depth + cameraObj.depth;
		}
		return depth;
		}
	this.___needSorting___ = function() {
		for (var i = 0; i < this.getNumChildren() - 1; i++)
		{
			var prevDepth = this.___GetDepth___(this.getChildAt(i));
			var nextDepth = this.___GetDepth___(this.getChildAt(i + 1));
			if (prevDepth < nextDepth)
				return true;
		}
		return false;
	}
	this.___sortFunction___ = function(obj1, obj2) {
		return (this.exportRoot.___GetDepth___(obj2) - this.exportRoot.___GetDepth___(obj1));
	}
	this.on('tick', function (event){
		var curTimeline = event.currentTarget;
		if (curTimeline.___needSorting___()){
			this.sortChildren(curTimeline.___sortFunction___);
		}
	});

	// Layer_14_obj_
	this.Layer_14 = new lib.Scene_1_Layer_14();
	this.Layer_14.name = "Layer_14";
	this.Layer_14.parent = this;
	this.Layer_14.setTransform(790.5,389,1,1,0,0,0,790.5,389);
	this.Layer_14.depth = 0;
	this.Layer_14.isAttachedToCamera = 0
	this.Layer_14.isAttachedToMask = 0
	this.Layer_14.layerDepth = 0
	this.Layer_14.layerIndex = 0
	this.Layer_14.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_14).wait(1));

	// Layer_2_obj_
	this.Layer_2 = new lib.Scene_1_Layer_2();
	this.Layer_2.name = "Layer_2";
	this.Layer_2.parent = this;
	this.Layer_2.setTransform(543,343,1,1,0,0,0,543,343);
	this.Layer_2.depth = 0;
	this.Layer_2.isAttachedToCamera = 0
	this.Layer_2.isAttachedToMask = 0
	this.Layer_2.layerDepth = 0
	this.Layer_2.layerIndex = 1
	this.Layer_2.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_2).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(540,340,546,346);
// library properties:
lib.properties = {
	id: '999180F02E8CB740B5BF8019D718D656',
	width: 1080,
	height: 680,
	fps: 30,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: [
		{src:"assets/images/cap37.png", id:"cap"},
		{src:"assets/images/pack37.png", id:"pack"},
		{src:"assets/images/patch37.png", id:"patch"},
		{src:"assets/images/pillow37.png", id:"pillow"}
	],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['999180F02E8CB740B5BF8019D718D656'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}


// Layer depth API : 

AdobeAn.Layer = new function() {
	this.getLayerZDepth = function(timeline, layerName)
	{
		if(layerName === "Camera")
		layerName = "___camera___instance";
		var script = "if(timeline." + layerName + ") timeline." + layerName + ".depth; else 0;";
		return eval(script);
	}
	this.setLayerZDepth = function(timeline, layerName, zDepth)
	{
		const MAX_zDepth = 10000;
		const MIN_zDepth = -5000;
		if(zDepth > MAX_zDepth)
			zDepth = MAX_zDepth;
		else if(zDepth < MIN_zDepth)
			zDepth = MIN_zDepth;
		if(layerName === "Camera")
		layerName = "___camera___instance";
		var script = "if(timeline." + layerName + ") timeline." + layerName + ".depth = " + zDepth + ";";
		eval(script);
	}
	this.removeLayer = function(timeline, layerName)
	{
		if(layerName === "Camera")
		layerName = "___camera___instance";
		var script = "if(timeline." + layerName + ") timeline.removeChild(timeline." + layerName + ");";
		eval(script);
	}
	this.addNewLayer = function(timeline, layerName, zDepth)
	{
		if(layerName === "Camera")
		layerName = "___camera___instance";
		zDepth = typeof zDepth !== 'undefined' ? zDepth : 0;
		var layer = new createjs.MovieClip();
		layer.name = layerName;
		layer.depth = zDepth;
		layer.layerIndex = 0;
		timeline.addChild(layer);
	}
}


})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;

$(function () {
    var canvas, stage, exportRoot, anim_container, dom_overlay_container, fnStartAnimation;
    function init() {
        canvas = document.getElementById("canvas37");
        anim_container = document.getElementById("animation_container37");
        dom_overlay_container = document.getElementById("dom_overlay_container37");
        var comp=AdobeAn.getComposition("999180F02E8CB740B5BF8019D718D656");
        var lib=comp.getLibrary();
        var loader = new createjs.LoadQueue(false);
        loader.addEventListener("fileload", function(evt){handleFileLoad(evt,comp)});
        loader.addEventListener("complete", function(evt){handleComplete(evt,comp)});
        var lib=comp.getLibrary();
        loader.loadManifest(lib.properties.manifest);
    }
    function handleFileLoad(evt, comp) {
        var images=comp.getImages();
        if (evt && (evt.item.type == "image")) { images[evt.item.id] = evt.result; }
    }
    function handleComplete(evt,comp) {
        //This function is always called, irrespective of the content. You can use the variable "stage" after it is created in token create_stage.
        var lib=comp.getLibrary();
        var ss=comp.getSpriteSheet();
        var queue = evt.target;
        var ssMetadata = lib.ssMetadata;
        for(i=0; i<ssMetadata.length; i++) {
            ss[ssMetadata[i].name] = new createjs.SpriteSheet( {"images": [queue.getResult(ssMetadata[i].name)], "frames": ssMetadata[i].frames} )
        }
        var preloaderDiv = document.getElementById("_preload_div_37");
        preloaderDiv.style.display = 'none';
        canvas.style.display = 'block';
        exportRoot = new lib.Feb_1();
        stage = new lib.Stage(canvas);
        //Registers the "tick" event listener.
        fnStartAnimation = function() {
            stage.addChild(exportRoot);
            createjs.Ticker.setFPS(lib.properties.fps);
            createjs.Ticker.addEventListener("tick", stage)
            stage.addEventListener("tick", handleTick)
            function getProjectionMatrix(container, totalDepth) {
                var focalLength = 528.25;
                var projectionCenter = { x : lib.properties.width/2, y : lib.properties.height/2 };
                var scale = (totalDepth + focalLength)/focalLength;
                var scaleMat = new createjs.Matrix2D;
                scaleMat.a = 1/scale;
                scaleMat.d = 1/scale;
                var projMat = new createjs.Matrix2D;
                projMat.tx = -projectionCenter.x;
                projMat.ty = -projectionCenter.y;
                projMat = projMat.prependMatrix(scaleMat);
                projMat.tx += projectionCenter.x;
                projMat.ty += projectionCenter.y;
                return projMat;
            }
            function handleTick(event) {
                var cameraInstance = exportRoot.___camera___instance;
                if(cameraInstance !== undefined && cameraInstance.pinToObject !== undefined)
                {
                    cameraInstance.x = cameraInstance.pinToObject.x + cameraInstance.pinToObject.pinOffsetX;
                    cameraInstance.y = cameraInstance.pinToObject.y + cameraInstance.pinToObject.pinOffsetY;
                    if(cameraInstance.pinToObject.parent !== undefined && cameraInstance.pinToObject.parent.depth !== undefined)
                        cameraInstance.depth = cameraInstance.pinToObject.parent.depth + cameraInstance.pinToObject.pinOffsetZ;
                }
                applyLayerZDepth(exportRoot);
            }
            function applyLayerZDepth(parent)
            {
                var cameraInstance = parent.___camera___instance;
                var focalLength = 528.25;
                var projectionCenter = { 'x' : 0, 'y' : 0};
                if(parent === exportRoot)
                {
                    var stageCenter = { 'x' : lib.properties.width/2, 'y' : lib.properties.height/2 };
                    projectionCenter.x = stageCenter.x;
                    projectionCenter.y = stageCenter.y;
                }
                for(child in parent.children)
                {
                    var layerObj = parent.children[child];
                    if(layerObj == cameraInstance)
                        continue;
                    applyLayerZDepth(layerObj, cameraInstance);
                    if(layerObj.layerDepth === undefined)
                        continue;
                    if(layerObj.currentFrame != layerObj.parent.currentFrame)
                    {
                        layerObj.gotoAndPlay(layerObj.parent.currentFrame);
                    }
                    var matToApply = new createjs.Matrix2D;
                    var cameraMat = new createjs.Matrix2D;
                    var totalDepth = layerObj.layerDepth ? layerObj.layerDepth : 0;
                    var cameraDepth = 0;
                    if(cameraInstance && !layerObj.isAttachedToCamera)
                    {
                        var mat = cameraInstance.getMatrix();
                        mat.tx -= projectionCenter.x;
                        mat.ty -= projectionCenter.y;
                        cameraMat = mat.invert();
                        cameraMat.prependTransform(projectionCenter.x, projectionCenter.y, 1, 1, 0, 0, 0, 0, 0);
                        cameraMat.appendTransform(-projectionCenter.x, -projectionCenter.y, 1, 1, 0, 0, 0, 0, 0);
                        if(cameraInstance.depth)
                            cameraDepth = cameraInstance.depth;
                    }
                    if(layerObj.depth)
                    {
                        totalDepth = layerObj.depth;
                    }
                    //Offset by camera depth
                    totalDepth -= cameraDepth;
                    if(totalDepth < -focalLength)
                    {
                        matToApply.a = 0;
                        matToApply.d = 0;
                    }
                    else
                    {
                        if(layerObj.layerDepth)
                        {
                            var sizeLockedMat = getProjectionMatrix(parent, layerObj.layerDepth);
                            if(sizeLockedMat)
                            {
                                sizeLockedMat.invert();
                                matToApply.prependMatrix(sizeLockedMat);
                            }
                        }
                        matToApply.prependMatrix(cameraMat);
                        var projMat = getProjectionMatrix(parent, totalDepth);
                        if(projMat)
                        {
                            matToApply.prependMatrix(projMat);
                        }
                    }
                    layerObj.transformMatrix = matToApply;
                }
            }
        }
        //Code to support hidpi screens and responsive scaling.
        function makeResponsive(isResp, respDim, isScale, scaleType) {
            var lastW, lastH, lastS=1;
            window.addEventListener('resize', resizeCanvas);
            resizeCanvas();
            function resizeCanvas() {
                var w = lib.properties.width, h = lib.properties.height;
                var iw = window.innerWidth, ih=window.innerHeight;
                var pRatio = window.devicePixelRatio || 1, xRatio=iw/w, yRatio=ih/h, sRatio=1;
                if(isResp) {
                    if((respDim=='width'&&lastW==iw) || (respDim=='height'&&lastH==ih)) {
                        sRatio = lastS;
                    }
                    else if(!isScale) {
                        if(iw<w || ih<h)
                            sRatio = Math.min(xRatio, yRatio);
                    }
                    else if(scaleType==1) {
                        sRatio = Math.min(xRatio, yRatio);
                    }
                    else if(scaleType==2) {
                        sRatio = Math.max(xRatio, yRatio);
                    }
                }
                canvas.width = w*pRatio*sRatio;
                canvas.height = h*pRatio*sRatio;
                canvas.style.width = anim_container.style.width = dom_overlay_container.style.width = preloaderDiv.style.width = w*sRatio+'px';
                canvas.style.height = anim_container.style.height = dom_overlay_container.style.height = preloaderDiv.style.height = h*sRatio+'px';
                stage.scaleX = pRatio*sRatio;
                stage.scaleY = pRatio*sRatio;
                lastW = iw; lastH = ih; lastS = sRatio;
                stage.tickOnUpdate = false;
                stage.update();
                stage.tickOnUpdate = true;
            }
        }
        makeResponsive(true,'both',true,2);
        AdobeAn.compositionLoaded(lib.properties.id);
        fnStartAnimation();
    }
    init();
});