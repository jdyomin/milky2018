(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [];


// symbols:



(lib.bg_part = function() {
	this.initialize(img.bg_part);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,971,680);


(lib.cent = function() {
	this.initialize(img.cent);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,93,124);


(lib.pack = function() {
	this.initialize(img.pack);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,231,489);


(lib.phone = function() {
	this.initialize(img.phone);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,534,270);


(lib.phone_shadow = function() {
	this.initialize(img.phone_shadow);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,239,253);


(lib.shadow = function() {
	this.initialize(img.shadow);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,419,110);// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.Symbol17 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#FFFFFF").ss(2.5,1,1).p("AgjAjIBHhF");
	this.shape.setTransform(-0.025,0);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol17, new cjs.Rectangle(-4.9,-4.7,9.8,9.4), null);


(lib.Symbol16 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#FFFFFF").ss(2.5,1,1).p("AAmAlIhLhJ");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol16, new cjs.Rectangle(-5,-4.9,10,9.9), null);


(lib.Symbol14 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.cent();
	this.instance.parent = this;
	this.instance.setTransform(-46.5,-62);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol14, new cjs.Rectangle(-46.5,-62,93,124), null);


(lib.Symbol13 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("AvPSMIAAmCQgBwMhCvWIf6hDQA2RogORnIqYFog");
	mask.setTransform(4.0188,0.4);

	// Layer_1
	this.instance = new lib.phone_shadow();
	this.instance.parent = this;
	this.instance.setTransform(-120,-124);

	var maskedShapeInstanceList = [this.instance];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol13, new cjs.Rectangle(-100.2,-124,208.5,253), null);


(lib.Symbol11 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AgvAwQgTgUAAgcQAAgbATgUQAUgUAbAAQAcAAAUAUQATAUAAAbQAAAcgTAUQgUATgcABQgbgBgUgTg");
	this.shape.setTransform(6.725,6.75);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol11, new cjs.Rectangle(0,0,13.5,13.5), null);


(lib.Symbol10 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AgvAwQgTgUAAgcQAAgbATgUQAUgUAbAAQAcAAAUAUQATAUAAAbQAAAcgTAUQgUATgcABQgbgBgUgTg");
	this.shape.setTransform(6.725,6.75);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol10, new cjs.Rectangle(0,0,13.5,13.5), null);


(lib.Symbol9 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#B7B7B7").s().p("AiMCMQg5g6gBhSQABhSA5g6QA6g5BSgBQBSABA6A5QA6A6AABSQAABSg6A6Qg6A6hSAAQhSAAg6g6g");
	this.shape.setTransform(19.85,19.85);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol9, new cjs.Rectangle(0,0,39.7,39.7), null);


(lib.Symbol7 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#B7B7B7").s().p("AiMCMQg5g6gBhSQABhSA5g6QA6g5BSgBQBSABA6A5QA6A6AABSQAABSg6A6Qg6A6hSAAQhSAAg6g6g");
	this.shape.setTransform(19.85,19.85);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol7, new cjs.Rectangle(0,0,39.7,39.7), null);


(lib.Symbol3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_4
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(13,1,1).p("AA2BpQhjhngIhq");
	this.shape.setTransform(15.05,-11.125);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	// Layer_3
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(13,1,1).p("AAsCTQB0j1jxgw");
	this.shape_1.setTransform(1.4797,-23.1);

	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(1));

	// Layer_2
	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(13,1,1).p("ACIBcQh0h9ibg6");
	this.shape_2.setTransform(-3.775,-13.775);

	this.timeline.addTween(cjs.Tween.get(this.shape_2).wait(1));

	// Layer_1
	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(13,1,1).p("ABpgpQhRB7iAg7");
	this.shape_3.setTransform(0.025,0.0126);

	this.timeline.addTween(cjs.Tween.get(this.shape_3).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol3, new cjs.Rectangle(-23.8,-44.2,50.8,55), null);


(lib.Symbol_15_shadow_png = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// shadow_png
	this.instance = new lib.shadow();
	this.instance.parent = this;
	this.instance.setTransform(-289,141.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(94));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_15_phone_png = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// phone_png
	this.instance = new lib.phone();
	this.instance.parent = this;
	this.instance.setTransform(-245,-67.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(94));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_15_pack_png = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// pack_png
	this.instance = new lib.pack();
	this.instance.parent = this;
	this.instance.setTransform(-6,-251.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(94));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_15_Layer_15 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_15
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#FFFFFF").ss(2.5,1,1).p("ACDinQkJA9AFES");
	this.shape.setTransform(-149.4036,-82.025);
	this.shape._off = true;

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(19).to({_off:false},0).wait(75));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_15_Layer_12 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_12
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(13,1,1).p("Ar6kmQCQDOC1CPQC6CTDFA5QGxB+GAk9");
	this.shape.setTransform(-62.375,110.0419);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(13,1,1).p("AsCkFQCeDDC9CBQDCCHDIArQG3BgFplT");
	this.shape_1.setTransform(-61.65,106.7807);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(13,1,1).p("AsHjtQCnC7DEB3QDJB8DKAhQG7BJFWln");
	this.shape_2.setTransform(-61.075,104.3661);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(13,1,1).p("AsLjdQCuC1DJBwQDNB1DLAZQG/A5FJl0");
	this.shape_3.setTransform(-60.675,102.7221);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(13,1,1).p("AsOjTQCzCxDLBsQDRBwDMAVQHAAuFCl7");
	this.shape_4.setTransform(-60.425,101.7532);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(13,1,1).p("AsPjQQC0CwDNBrQDRBuDMAUQHCArE/l+");
	this.shape_5.setTransform(-60.35,101.442);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(13,1,1).p("AsKjjQCsC3DHBzQDMB3DLAdQG9A+FOlu");
	this.shape_6.setTransform(-60.825,103.3292);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(13,1,1).p("AsGj0QClC9DCB7QDHB+DKAkQG5BQFcli");
	this.shape_7.setTransform(-61.225,105.0281);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(13,1,1).p("AsCkDQCeDDC+CAQDDCFDJAqQG2BeFnlW");
	this.shape_8.setTransform(-61.575,106.5026);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(13,1,1).p("Ar/kPQCZDGC7CGQDACKDHAwQG0BpFwlO");
	this.shape_9.setTransform(-61.875,107.7571);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(13,1,1).p("Ar9kZQCVDKC5CJQC8CPDHAzQGzBzF3lG");
	this.shape_10.setTransform(-62.1,108.7381);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(13,1,1).p("Ar7kgQCSDMC2CMQC7CRDGA3QGyB5F8lB");
	this.shape_11.setTransform(-62.25,109.4562);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(13,1,1).p("Ar7klQCRDOC2COQC6CTDFA4QGxB9GAk+");
	this.shape_12.setTransform(-62.35,109.9014);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},30).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_5}]},15).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape}]},1).wait(36));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_15_Layer_2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(13,1,1).p("AD+moQBkEOATDYQASDchYBTQhZBTizggQizghjnkz");
	this.shape.setTransform(151.7599,-6.3252);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(13,1,1).p("Al6BnQDxEeC1AhQCyAgBXhZQBWhZgTjbQgSjYhkkO");
	this.shape_1.setTransform(151.4388,-5.7536);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(13,1,1).p("Al9B+QD6EMC2AhQCxAgBVhfQBUhdgSjcQgTjYhkkO");
	this.shape_2.setTransform(151.167,-5.2484);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(13,1,1).p("AmACTQEDD8C3AhQCwAgBUhkQBShigSjbQgTjYhkkO");
	this.shape_3.setTransform(150.8953,-4.7886);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(13,1,1).p("AmCCmQEKDtC4AiQCuAgBUhpQBQhlgSjcQgTjYhkkO");
	this.shape_4.setTransform(150.6729,-4.4013);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(13,1,1).p("AmEC1QEQDhC4AiQCuAgBThtQBPhogTjbQgSjYhkkO");
	this.shape_5.setTransform(150.5005,-4.0706);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(13,1,1).p("AmGDCQEVDXC5AiQCtAgBShwQBOhqgSjcQgTjYhkkO");
	this.shape_6.setTransform(150.3523,-3.8043);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(13,1,1).p("AmHDLQEZDQC6AiQCsAgBShzQBNhsgTjbQgSjYhkkO");
	this.shape_7.setTransform(150.2292,-3.5852);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(13,1,1).p("AmIDSQEbDLC7AiQCsAfBRh0QBMhtgSjcQgTjYhkkO");
	this.shape_8.setTransform(150.1551,-3.4386);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(13,1,1).p("AmIDXQEdDHC6AiQCsAfBRh1QBMhugTjcQgSjYhkkO");
	this.shape_9.setTransform(150.1061,-3.342);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(13,1,1).p("AEPnGQBkEOATDYQASDchMBuQhQB2isggQi7gikdjG");
	this.shape_10.setTransform(150.0811,-3.317);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(13,1,1).p("Al/EHQEFCbDJAWQDAAVBEhqQBAhogbjVQgcjShkkO");
	this.shape_11.setTransform(152.361,-4.4521);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(13,1,1).p("Al5EpQD0B6DTAPQDQANA5hjQA5hhgjjRQgijOhkkO");
	this.shape_12.setTransform(154.129,-5.269);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(13,1,1).p("ADbmtQBkEOAnDMQAnDNgzBeQgzBejbgJQjagIjnhl");
	this.shape_13.setTransform(155.3472,-5.8138);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(13,1,1).p("Al6EkQD2B/DSAPQDOAPA7hkQA5highjRQgijPhkkO");
	this.shape_14.setTransform(153.895,-5.1772);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(13,1,1).p("Al+ENQECCVDLAVQDDAUBChqQA/hmgdjUQgdjShkkO");
	this.shape_15.setTransform(152.7071,-4.6156);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(13,1,1).p("AmBD6QEMCnDEAZQC8AYBGhtQBEhpgZjXQgZjUhkkO");
	this.shape_16.setTransform(151.7477,-4.1593);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(13,1,1).p("AmFDrQEUC0DAAeQC1AbBLhxQBHhsgWjYQgWjWhkkO");
	this.shape_17.setTransform(151.0088,-3.7896);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#000000").ss(13,1,1).p("AmGDhQEZC+C9AgQCwAdBOhzQBKhtgVjbQgUjXhkkO");
	this.shape_18.setTransform(150.4974,-3.5249);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#000000").ss(13,1,1).p("AmIDaQEdDEC7AiQCtAfBQh1QBLhugTjbQgTjYhkkO");
	this.shape_19.setTransform(150.191,-3.3753);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#000000").ss(13,1,1).p("AlxBwQD2EcCxAUQCyAUBRhXQBRhXghjWQghjSh0kB");
	this.shape_20.setTransform(155.9276,-6.7631);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#000000").ss(13,1,1).p("AlnDUQEkDZCsgSQCsgTA6hjQA6hkhMjBQhLjBikja");
	this.shape_21.setTransform(169.1552,-7.3773);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#000000").ss(13,1,1).p("AifmiQDyCaCSCiQCUCjgUB4QgUB3ijBTQijBTlxhp");
	this.shape_22.setTransform(192.7444,-6.4629);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#000000").ss(13,1,1).p("AmgkxQEXhIDZAFQDcAFBKBhQBJBggzCuQgzCwlKDF");
	this.shape_23.setTransform(219.2738,-17.8382);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#000000").ss(13,1,1).p("AmekoQEZhBDYAKQDcALBIBiQBGBig3CtQg4CujrCW");
	this.shape_24.setTransform(218.9052,-18.9167);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f().s("#000000").ss(13,1,1).p("AmbkfQEag6DYAQQDbAPBFBlQBFBkg8CrQg8CtiNBn");
	this.shape_25.setTransform(218.5508,-20.0146);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f().s("#000000").ss(13,1,1).p("AmZkWQEbgyDYAVQDbAUBDBnQBCBlhACqQhBCrguA4");
	this.shape_26.setTransform(218.2004,-21.1834);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f().s("#000000").ss(13,1,1).p("Al6lpQEPAEDEA4QDFA5A7BlQA6BlgrCkQgtClglBL");
	this.shape_27.setTransform(215.2571,-12.775);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f().s("#000000").ss(13,1,1).p("AlbmqQEDA5CvBdQCwBcAzBkQAzBjgYCgQgYCfgdBd");
	this.shape_28.setTransform(212.3981,-6.125);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f().s("#000000").ss(13,1,1).p("Ak/nrQD3BuCaCBQCcCAArBjQArBjgECZQgDCZgVBw");
	this.shape_29.setTransform(209.6882,0.5);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f().s("#000000").ss(13,1,1).p("AksosQDsCkCFCkQCGCkAkBiQAjBiARCUQAQCTgLCC");
	this.shape_30.setTransform(207.8599,7.125);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f().s("#000000").ss(13,1,1).p("AlInVQD7BdChB0QCjB0AtBkQAuBigKCcQgLCbgXBp");
	this.shape_31.setTransform(210.5401,-1.725);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f().s("#000000").ss(13,1,1).p("Alvl+QELAUC8BFQC/BEA4BmQA4BlglCiQgmCjgjBR");
	this.shape_32.setTransform(214.29,-10.55);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f().s("#000000").ss(13,1,1).p("AmckkQEZg9DYANQDcANBGBjQBGBjg6CsQg6Cui8B+");
	this.shape_33.setTransform(218.7267,-19.4384);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f().s("#000000").ss(13,1,1).p("AlmD2QE1DBCqggQCqggAyhoQAyhohbi7Qhai6i1jN");
	this.shape_34.setTransform(174.0115,-7.3908);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f().s("#000000").ss(13,1,1).p("AlsCbQEKEACvAEQCvAEBHhcQBIhdgzjNQgzjLiHjx");
	this.shape_35.setTransform(161.3366,-7.1436);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f().s("#000000").ss(13,1,1).p("Al0BgQDwEmCyAaQCzAZBUhVQBUhWgbjYQgajUhtkH");
	this.shape_36.setTransform(154.0678,-6.5659);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},19).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_10}]},13).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape}]},18).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_27}]},1).to({state:[{t:this.shape_28}]},1).to({state:[{t:this.shape_29}]},1).to({state:[{t:this.shape_30}]},1).to({state:[{t:this.shape_30}]},2).to({state:[{t:this.shape_31}]},1).to({state:[{t:this.shape_32}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_33}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_34}]},1).to({state:[{t:this.shape_35}]},1).to({state:[{t:this.shape_36}]},1).to({state:[{t:this.shape}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_15_Спеши__количество_призов_ограничено_ = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Спеши__количество_призов_ограничено_
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgGBYQgDgCgBgEIABgDQAEgGAIgBIAFACQAEACABAFQAAAEgEADQgFADgEAAQgEAAgCgDgAgHA5QAAAAAAgBQgBAAAAgBQAAAAAAgBQAAgBAAAAIgBgHIgBgVQgEgoADgjIAAgcQAAgDADgEQADgEADgBIACAAQAJABgBAKIAAAWIAAARIAAAsIAAAGIAFAjQABAEgBAFQgCAHgHABIgCAAQgFAAgEgFg");
	this.shape.setTransform(-56.435,-122.125);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgVBQQgEgJgBgFQgBgJAAgFIABgdIABgfIAAgfQAAgfAEgJQAHgKAIgBQATgEAKAPQAFAIAAARIAAAmQAAASgBAQIgCAQIgBARQAAAVgFAJQgHAKgJABIgDAAQgNAAgIgMgAgChKQgHADgCAMIAAAYIAAAjIgBAgIAAAPIAAANQACAOAMAAQAFAAABgNIABgKIABgJQAHgqgEg3QgBgJgDgGQgBgDgEgBIgEgBIgCABg");
	this.shape_1.setTransform(-63.8,-122.1096);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgbBLIAAgRIABgQIAAhiIABgZQAAgEADgDQADgCAFABQADACAAAjQAAAkABABQAAABAKgBIAKgBQABgBgBgfQgBggACgFQACgHAEAAQAGAAAAADQAEBdABBOQAAAHgFADQgFABgEgCQgCgFAAgNQAAglgCgiQgCgCgRADQgEAAADA9QABAWgDAFQgBACgIAAQgFAAgBgSg");
	this.shape_2.setTransform(-71.025,-122.175);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AgRBPQgEgzgDg6IgCghQgCgRAIgFIAdgFQAHgBADADQADADgCAFQgEAGgKAAQgLAAgEAEIACAeIACAfQALACgBAHQAAAEgJADIADAjIACAiQAPgEAKAFQADABgCAFQgBAFgDAAIggADQgIAAAAgMg");
	this.shape_3.setTransform(-78.154,-122.0375);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AANBbQgCgFAAgNQABglgDgiQgCgCgRADQgPABgBgQIAAgsIAAgZQABgEACgDQAEgCAEABQAEACAAAjQgBAkABABQAAADALgDIAJgBQACgBgBgfQgCggACgFQADgHAEAAQAGAAAAADQAEBdAABOQAAAHgFADIgDABQgDAAgDgCg");
	this.shape_4.setTransform(-85.325,-122.1969);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AgYBaQgDgLABgdIgBgxQAAgiABgQIAAgkQABgIAFAAQAIABACANQABAEAAAUIgBBSIADgMQAOgmAAgbIACgOIACgNQACgLAFgDQAGgBABAGQADASAAAhIAAA3IgBAeIgBAdQAAAPgIgCQgFABgCgGQgCgGAAgHIAEhSQgMBIgJAXQgFAHgFgBQgEAAgCgDg");
	this.shape_5.setTransform(-92.9333,-121.85);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AgbBLIAAgRIABgQIAAhiIABgZQAAgEADgDQADgCAFABQADACAAAjQAAAkABABQAAABAKgBIAKgBQABgBgBgfQgBggACgFQACgHAEAAQAGAAAAADQAEBdABBOQAAAHgFADQgFABgEgCQgCgFAAgNQAAglgCgiQgCgCgRADQgEAAADA9QABAWgDAFQgBACgIAAQgFAAgBgSg");
	this.shape_6.setTransform(-100.275,-122.175);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AghBZQgDgGABgYIADgXIAEgXIAIgkIAIgjIAFgXQAFgNAFAEQADACAEATIAIA5QAGAjAFAXQAJAigCAGQgLAHgEgKIgGgYIgJgrIgHABIgIAAQgEAOgCAWQgFAlgBAAIgGACQgEAAgCgDgAgDgVIgEATIAJAAQAAgZgDgBIAAAAIgCAHg");
	this.shape_7.setTransform(-108.0284,-122.0436);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AgZBgQgEgBgBgLIAAiRQAAghACAAQAFgDATACQAUACAIAGQAFADACARQACASgMAMQgEAEgOACQgMABAAABQAFAAgJBdIAAALIAAAMQgCAKgGAAIgEgBgAgOhPQgCACADAcQAAAEAMgBQANAAADgFQADgDAAgJQABgKgDgDQgEgFgNAAIgEAAQgKAAABACg");
	this.shape_8.setTransform(-115.2163,-122.416);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AgKBaQgEgDgCgQIgIiAIgBgKIgBgJQAAgJAGgDIAIABIAHgBQANgDAGAAQAMAAABAGQABAIgOADIgMABIgJABIABAgIAFA2IADArIABAJIAAAKQAAALgDACQgDADgDAAQgBAAAAAAQgBAAAAgBQgBAAAAAAQgBAAAAgBg");
	this.shape_9.setTransform(-122.6734,-122.1252);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("AgVBQQgFgJAAgFQgCgJABgFIABgdIABgfIAAgfQAAgfAEgJQAGgKAIgBQAUgEAKAPQAEAIABARIAAAmQAAASgBAQIgBAQIgBARQgBAVgFAJQgHAKgJABIgDAAQgNAAgIgMgAgBhKQgJADgBAMIAAAYIAAAjIgBAgIAAAPIAAANQABAOAMAAQAGAAABgNIABgKIABgJQAHgqgEg3QgBgJgDgGQgCgDgDgBIgEgBIgBABg");
	this.shape_10.setTransform(-130.2,-122.1096);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("AgEBeQgJgBgEgDQgGgEAAgVIgDhAIAAgRIgBgVIgBgWIgBgVQACgLARgCQAPgDALAKQAIAHACAGQAGANgEANQgEANgMAIQAJAPADAMQAIAcgCAYQgFAigPAGQgEACgGAAIgEgBgAgIgRIABAkIABAmQABAVACABQAEABAFgGQAFgFABgFQAEgbgEgcQgGgagNgCIgBACgAgKhQIAAAVIACAVQAKABAHgIQAGgIgCgMQgBgHgHgFQgFgDgFAAIgFAAg");
	this.shape_11.setTransform(-144.0536,-122.3368);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("AgUBQQgGgJAAgFQgBgJAAgFIABgdIABgfIAAgfQAAgfAFgJQAGgKAHgBQAUgEAKAPQAFAIgBARIABAmQABASgCAQIgCAQIAAARQgBAVgGAJQgGAKgJABIgDAAQgNAAgHgMgAgBhKQgJADgBAMIAAAYIAAAjIAAAgIgBAPIAAANQABAOAMAAQAHAAAAgNIAAgKIABgJQAIgqgEg3QgBgJgDgGQgCgDgDgBIgEgBIgBABg");
	this.shape_12.setTransform(-151.95,-122.1096);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFFFFF").s().p("AgWBSQgEgDABgGQAAgIAFgCQAFgCAEADIAFAFIAEAEQADACAIgDQAGgDABgSQABgKgBgKQgBgNgEgCQgCgCgJACQgIABgDgBQgEgCgDgEQgDgGACgFIAOgJQAJgIAEgLQAIgVgEgMQgCgKgKAAQgIAAgHAPIgCAHIgCAIQgEAHgHgEQgGgDABgJIACgHQAIgeAYgEQALgBANANQAKALgCAZIgEAUQgBAEgFAGQgEAGAAADQAAADAFADIAFAGQANAYgGAfQgCANgHAIQgHAIgNABIgDABQgOAAgKgLg");
	this.shape_13.setTransform(-160.2452,-122.2988);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FFFFFF").s().p("AgYBaQgDgLABgdIgBgxQAAgiABgQIAAgkQABgIAFAAQAIABACANQABAEAAAUIgBBSIADgMQAOgmAAgbIACgOIACgNQACgLAFgDQAGgBABAGQADASAAAhIAAA3IgBAeIgBAdQAAAPgIgCQgFABgCgGQgCgGAAgHIAEhSQgMBIgJAXQgFAHgFgBQgEAAgCgDg");
	this.shape_14.setTransform(-168.5333,-121.85);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FFFFFF").s().p("AgZBgQgEgBgBgLIAAiRQAAghACAAQAFgDATACQAUACAIAGQAFADACARQACASgMAMQgEAEgOACQgMABAAABQAFAAgJBdIAAALIAAAMQgCAKgGAAIgEgBgAgOhPQgCACADAcQAAAEAMgBQANAAADgFQADgDAAgJQABgKgDgDQgEgFgNAAIgEAAQgKAAABACg");
	this.shape_15.setTransform(-175.6663,-122.416);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FFFFFF").s().p("AgWBbQgEgBABgOIAAgNIABgNQAAg5gBgRIgBgQIgBgWQABgbAHAAIAQgBIAXgBQAIAAgCAVIAAAJIABA9IABA/QABAUgGAFQgDAEgGgDQgDgBAAgFQABgTgCgmQgCgnABgSIABgqIgDABQgQgBABADQgCAVACAvQACAtgCAWQgCAbgIAAIgEgBg");
	this.shape_16.setTransform(-183.7673,-121.9853);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#FFFFFF").s().p("AgUBQQgGgJAAgFQgBgJAAgFIABgdIABgfIAAgfQAAgfAFgJQAGgKAHgBQAUgEAKAPQAFAIgBARIABAmQABASgCAQIgCAQIAAARQgBAVgGAJQgGAKgJABIgDAAQgNAAgHgMgAgBhKQgJADgBAMIAAAYIAAAjIAAAgIgBAPIAAANQABAOAMAAQAHAAAAgNIAAgKIABgJQAIgqgEg3QgBgJgDgGQgCgDgDgBIgEgBIgBABg");
	this.shape_17.setTransform(-61.05,-147.1096);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#FFFFFF").s().p("AgEBeQgJgBgEgDQgGgEAAgVIgDhAIAAgRIgBgVIgBgWIgBgVQACgLARgCQAPgDALAKQAIAHACAGQAGANgEANQgEANgMAIQAJAPADAMQAIAcgCAYQgFAigPAGQgEACgGAAIgEgBgAgIgRIABAkIABAmQABAVACABQAEABAFgGQAFgFABgFQAEgbgEgcQgGgagNgCIgBACgAgKhQIAAAVIACAVQAKABAHgIQAGgIgCgMQgBgHgHgFQgFgDgFAAIgFAAg");
	this.shape_18.setTransform(-68.6536,-147.3368);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#FFFFFF").s().p("AgDBbQgIgEABgfIAAgMIAAgLQAAg0ADgzIAAAAIAAAAQAAADgQgEQgKgDAAgIQABgIAYgBIAgABQAHABACAIQACAIgEAAQgDACgJgCQgIAAAAABQgDAVAAAsQABA0gBARIAAALIAAALQgCAIgFAAIgEgBg");
	this.shape_19.setTransform(-76.4179,-147.1199);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#FFFFFF").s().p("AADBcQgOAAgIgMQgFgJgCgeIgDgrQgBg2AEgNQAGgUARgCQAJgCAJAHQAGADAEANQADAHgGAEQgGAEgCgEQgFgLgGgCQgIgDgCANQgCAGAAAQQgBA4ADAfQAEAhAJgBQAJgCAAgFQACgGAFgBQADgBADADIACAEQABACgFAIQgIALgOAAIgBAAg");
	this.shape_20.setTransform(-83.9077,-146.988);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#FFFFFF").s().p("AgRBPQgEgzgDg6IgCghQgCgRAIgFIAdgFQAHgBADADQADADgCAFQgEAGgKAAQgLAAgEAEIACAeIACAfQALACgBAHQAAAEgJADIADAjIACAiQAPgEAKAFQADABgCAFQgBAFgDAAIggADQgIAAAAgMg");
	this.shape_21.setTransform(-91.304,-147.0375);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#FFFFFF").s().p("AANBbQgCgFAAgNQABglgDgiQgCgCgRADQgPABgBgQIAAgsIAAgZQABgEACgDQAEgCAEABQAEACAAAjQgBAkABABQAAADALgDIAJgBQACgBgBgfQgCggACgFQADgHAEAAQAGAAAAADQAEBdAABOQAAAHgFADIgDABQgDAAgDgCg");
	this.shape_22.setTransform(-98.475,-147.1969);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#FFFFFF").s().p("AgYBaQgDgLABgdIgBgxQAAgiABgQIAAgkQABgIAFAAQAIAAACAOQABAFAAATIgBBSIADgMQAOgnAAgaIACgOIACgOQACgKAFgCQAGgCABAFQADATAAAhIAAA2IgBAfIgBAeQAAAOgIgDQgFACgCgGQgCgFAAgJIAEhRQgMBIgJAYQgFAFgFAAQgEAAgCgDg");
	this.shape_23.setTransform(-106.0833,-146.85);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#FFFFFF").s().p("AAQBGQgCgcABgsQgBgugDgXQAAgBgLgBIAAAPIAAA0IACAzQAAAVgCAKQgEAOgLABIgDAAQgEAAgEgCQgHgCACgIQACgIAJACIADAAQADgCAAgdIAAgYIgBgRIAAgbIgBgeIAAgTQgJgCgBgCQgDgMAIAAQAJgBAOACIATABQAIAAgBAbIABAVIAAARQAAARADBTQACAOgEABIgEABQgIAAgCgWg");
	this.shape_24.setTransform(-114.0186,-147.1795);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#FFFFFF").s().p("AgUBQQgGgJAAgFQgCgJABgFIABgdIABgfIAAgfQAAgfAFgJQAGgKAHgBQAUgEAKAPQAEAIAAARIABAmQABASgCAQIgBAQIgBARQgBAVgGAJQgGAKgJABIgDAAQgNAAgHgMgAgBhKQgJADgBAMIAAAYIAAAjIAAAgIgBAPIAAANQABAOAMAAQAHAAAAgNIAAgKIABgJQAIgqgEg3QgBgJgDgGQgCgDgDgBIgEgBIgBABg");
	this.shape_25.setTransform(-121.45,-147.1096);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#FFFFFF").s().p("AgfBaQgBgaADg4QADg5AAgXQgBgIABgDQACgFADgCQAFgDAEAEIABAUIABAVQAEgDAHgRQAHgRAFgEIAHgDQAFAAABADQACAFgIANIgZA1QAMAUASAyQAJAXgCAIQgBADgEACQgFABgDgCQgFgDgCgJQgKgigOgmQgDAQAABFQAAAFgIAAIgCAAQgGAAAAgDg");
	this.shape_26.setTransform(-128.6754,-147.0254);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#FFFFFF").s().p("AgCAPIgBgHIAAgFIgCgCIgCgBQgDgDAAgEQABgFAEgBIAGgBQAJABABAIIAAACIgCAKQAAABAAABQAAAAgBABQAAAAAAABQgBAAAAABQgCADgFAAIgCAAg");
	this.shape_27.setTransform(-141.6031,-138.5646);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#FFFFFF").s().p("AgYBaQgDgLABgdIgBgxQAAgiABgQIAAgkQABgIAFAAQAIAAACAOQABAFAAATIgBBSIADgMQAOgnAAgaIACgOIACgOQACgKAFgCQAGgCABAFQADATAAAhIAAA2IgBAfIgBAeQAAAOgIgDQgFACgCgGQgCgFAAgJIAEhRQgMBIgJAYQgFAFgFAAQgEAAgCgDg");
	this.shape_28.setTransform(-147.7833,-146.85);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#FFFFFF").s().p("AgIBdQgXAAgFgBQgFgDABgHQAAhPADhdQAAgCAHAAQAEAAACAHQACAFgCAfQgBB5ACACQABAAAOABIAAAAIAAgRIAAgRIABhiIAAgZQABgEADgCQADgCAEABQADACAAAjQgBB9ACACQAAAAAFAAQAKgBABAAQABgCgBh5QgBgfACgFQACgHAEAAQAGAAAAACQAEBdABBPQAAAHgGADQgEABgOAAIgaAAg");
	this.shape_29.setTransform(-156.9,-147.15);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#FFFFFF").s().p("AgRBPQgEgzgDg6IgCghQgCgRAIgFIAdgFQAHgBADADQADADgCAFQgEAGgKAAQgLAAgEAEIACAeIACAfQALACgBAHQAAAEgJADIADAjIACAiQAPgEAKAFQADABgCAFQgBAFgDAAIggADQgIAAAAgMg");
	this.shape_30.setTransform(-165.704,-147.0375);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#FFFFFF").s().p("AgWBbQgEgBABgOIAAgNIABgNQAAg5gBgRIgBgQIgBgWQABgbAHAAIAQgBIAXgBQAIAAgCAVIAAAJIABA9IABA/QABAUgGAFQgDAEgGgDQgDgBAAgFQABgTgCgmQgCgnABgSIABgqIgDABQgQgBABADQgCAVACAvQACAtgCAWQgCAbgIAAIgEgBg");
	this.shape_31.setTransform(-172.9173,-146.9853);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#FFFFFF").s().p("AADBcQgOAAgIgMQgFgJgCgeIgDgrQgBg2AEgNQAGgUARgCQAJgCAJAHQAGADAEANQADAHgGAEQgGAEgCgEQgFgLgGgCQgIgDgCANQgCAGAAAQQgBA4ADAfQAEAhAJgBQAJgCAAgFQACgGAFgBQADgBADADIACAEQABACgFAIQgIALgOAAIgBAAg");
	this.shape_32.setTransform(-180.4077,-146.988);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(94));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_5_Layer_5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_5
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(13,1,1).p("AisArIFZhV");
	this.shape.setTransform(-5.575,-43.55);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(13,1,1).p("AifA8IE/h3");
	this.shape_1.setTransform(-6.925,-45.3);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(13,1,1).p("AiUBLIEoiV");
	this.shape_2.setTransform(-8.05,-46.775);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(13,1,1).p("AiKBXIEVit");
	this.shape_3.setTransform(-9,-47.975);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(13,1,1).p("AiDBgIEHi/");
	this.shape_4.setTransform(-9.725,-48.925);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(13,1,1).p("Ah+BnID9jN");
	this.shape_5.setTransform(-10.225,-49.6);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(13,1,1).p("Ah7BrID3jV");
	this.shape_6.setTransform(-10.55,-50);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(13,1,1).p("Ah6BsID0jX");
	this.shape_7.setTransform(-10.65,-50.125);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(13,1,1).p("AiHBbIEPi1");
	this.shape_8.setTransform(-9.3,-48.375);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(13,1,1).p("AiSBMIEliX");
	this.shape_9.setTransform(-8.175,-46.9);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(13,1,1).p("AicBAIE5h/");
	this.shape_10.setTransform(-7.225,-45.7);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(13,1,1).p("AijA3IFHht");
	this.shape_11.setTransform(-6.5,-44.75);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(13,1,1).p("AioAwIFRhf");
	this.shape_12.setTransform(-6,-44.075);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(13,1,1).p("AirAsIFXhX");
	this.shape_13.setTransform(-5.675,-43.675);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},44).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_7}]},35).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_5_Layer_4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_4
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(13,1,1).p("AggCPIBBkd");
	this.shape.setTransform(-19.575,-53.6);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(94));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_5_Layer_3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_3
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(13,1,1).p("ABOh/IibD/");
	this.shape.setTransform(-59.375,-3.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(13,1,1).p("AhZB4ICzjv");
	this.shape_1.setTransform(-60.55,-4);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(13,1,1).p("AhjBxIDHjh");
	this.shape_2.setTransform(-61.55,-4.675);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(13,1,1).p("AhrBrIDXjV");
	this.shape_3.setTransform(-62.375,-5.225);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(13,1,1).p("AhyBnIDljN");
	this.shape_4.setTransform(-63,-5.675);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(13,1,1).p("Ah2BkIDtjH");
	this.shape_5.setTransform(-63.45,-5.975);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(13,1,1).p("Ah5BiIDzjD");
	this.shape_6.setTransform(-63.725,-6.175);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(13,1,1).p("AB7hgIj1DB");
	this.shape_7.setTransform(-63.825,-6.225);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(13,1,1).p("AhuBpIDdjR");
	this.shape_8.setTransform(-62.65,-5.425);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(13,1,1).p("AhkBwIDJjf");
	this.shape_9.setTransform(-61.65,-4.75);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(13,1,1).p("AhcB2IC5jr");
	this.shape_10.setTransform(-60.825,-4.2);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(13,1,1).p("AhVB6ICrjz");
	this.shape_11.setTransform(-60.2,-3.75);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(13,1,1).p("AhRB9ICjj5");
	this.shape_12.setTransform(-59.75,-3.45);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(13,1,1).p("AhOB/ICdj9");
	this.shape_13.setTransform(-59.475,-3.25);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},44).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_7}]},35).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_5_Layer_2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(13,1,1).p("AC3gEIltAJ");
	this.shape.setTransform(-69.8,-15.45);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(94));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_5_Layer_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(13,1,1).p("AjFBwQBTBBBdgTQBPgQBAhHQA5g/APhGQAPhJgjga");
	this.shape.setTransform(0,-0.0101);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(13,1,1).p("AjJB/QBdA2BWgaQBLgXA/hGQA6g/AUhAQAVhCgggf");
	this.shape_1.setTransform(-0.6209,-0.4525);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(13,1,1).p("AjNCMQBmAsBQgfQBIgfA+hFQA6hAAZg6QAZg7gcgl");
	this.shape_2.setTransform(-1.0854,-0.7722);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(13,1,1).p("AjRCWQBuAkBLgkQBFgkA+hEQA7hBAcg1QAdg2gZgp");
	this.shape_3.setTransform(-1.4784,-0.9875);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(13,1,1).p("AjUCdQBzAeBHgoQBEgoA9hEQA7hBAggxQAggygYgs");
	this.shape_4.setTransform(-1.748,-1.0902);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(13,1,1).p("AjWCiQB3AaBEgrQBDgrA8hDQA8hCAiguQAigvgWgv");
	this.shape_5.setTransform(-1.9429,-1.1698);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(13,1,1).p("AjXClQB5AYBDgtQBBgtA9hDQA8hCAjgsQAjgtgWgx");
	this.shape_6.setTransform(-2.0409,-1.2046);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(13,1,1).p("AjXCmQB6AXBCguQBBgtA8hDQA8hCAkgsQAjgsgVgx");
	this.shape_7.setTransform(-2.0847,-1.2125);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(13,1,1).p("AjSCZQBwAiBJgmQBFgmA9hEQA7hBAegzQAeg0gYgr");
	this.shape_8.setTransform(-1.5743,-1.0472);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(13,1,1).p("AjOCNQBnAsBQghQBHgfA+hEQA7hBAZg5QAag6gcgm");
	this.shape_9.setTransform(-1.1423,-0.8051);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(13,1,1).p("AjKCCQBfA0BVgcQBKgZA+hGQA6g/AWg/QAWg/gfgh");
	this.shape_10.setTransform(-0.7305,-0.5464);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(13,1,1).p("AjIB6QBaA6BZgYQBMgVA/hGQA5hAAThBQAThEghge");
	this.shape_11.setTransform(-0.4245,-0.3466);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(13,1,1).p("AjGB1QBWA9BbgVQBNgSBAhGQA5hAARhEQAQhHgigb");
	this.shape_12.setTransform(-0.1804,-0.1577);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(13,1,1).p("AjFBxQBUBABdgUQBOgQA/hHQA5g/AQhGQAPhIgiga");
	this.shape_13.setTransform(-0.0597,-0.0473);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},44).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_7}]},35).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_4_Layer_2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(13,1,1).p("AhtAoQDWigAFCg");
	this.shape.setTransform(5.375,9.0125);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(13,1,1).p("AhsAsQDRilAICT");
	this.shape_1.setTransform(5.225,8.5746);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(13,1,1).p("AhoA4QDDiyAOBr");
	this.shape_2.setTransform(4.725,7.0699);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(13,1,1).p("AhjBUQCsjHAbAn");
	this.shape_3.setTransform(3.925,3.7886);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(13,1,1).p("AhbCPQCLjlAsg4");
	this.shape_4.setTransform(2.775,-2.85);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(13,1,1).p("Ah3BdQCxjCA+AJ");
	this.shape_5.setTransform(5.95,2.928);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(13,1,1).p("AiSA3QDVigBQBL");
	this.shape_6.setTransform(9.125,7.5358);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(13,1,1).p("AiWAvQDRiMBcBG");
	this.shape_7.setTransform(9.8,8.2389);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(13,1,1).p("AigAXQDGhNB7A1");
	this.shape_8.setTransform(11.8,10.2779);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(13,1,1).p("AiwgZIFhAz");
	this.shape_9.setTransform(15.15,14.65);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(13,1,1).p("AicgMQC8gfB9BC");
	this.shape_10.setTransform(12.15,13.6597);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(13,1,1).p("AiLAFQDFhNBSBk");
	this.shape_11.setTransform(9.725,12.0506);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(13,1,1).p("Ah+AVQDNhyAwB/");
	this.shape_12.setTransform(7.825,10.7413);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(13,1,1).p("Ah0AgQDRiMAYCR");
	this.shape_13.setTransform(6.475,9.7674);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[]},1).to({state:[{t:this.shape}]},28).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[]},1).wait(31));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_4_Layer_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(13,1,1).p("AhIB4QhIjhDqgO");
	this.shape.setTransform(0.0203,0);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(13,1,1).p("AhGB6QhIjgDmgT");
	this.shape_1.setTransform(-0.1909,-0.275);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(13,1,1).p("Ag/CDQhIjhDZgk");
	this.shape_2.setTransform(-0.8531,-1.1);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(13,1,1).p("Ag0CRQhIjhDEhA");
	this.shape_3.setTransform(-1.9734,-2.5);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(13,1,1).p("AgkCkQhIjgCnhn");
	this.shape_4.setTransform(-3.5678,-4.45);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(13,1,1).p("AhNCUQgqjmDMhB");
	this.shape_5.setTransform(1.2172,-1.625);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(13,1,1).p("AhxCDQgMjrDwga");
	this.shape_6.setTransform(5.5488,1.175);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(13,1,1).p("Ah4B6QAJjWDogd");
	this.shape_7.setTransform(6.575,1.85);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(13,1,1).p("AiLBfQBGiYDRgl");
	this.shape_8.setTransform(9.575,3.85);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(13,1,1).p("AiqAxIFVhh");
	this.shape_9.setTransform(14.575,7.175);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(13,1,1).p("AiPBGQBhhlC+gm");
	this.shape_10.setTransform(10.375,4.975);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(13,1,1).p("Ah4BYQAkiSDNgd");
	this.shape_11.setTransform(6.975,3.2);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(13,1,1).p("AhmBmQgMi0DagX");
	this.shape_12.setTransform(4.2963,1.8);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(13,1,1).p("AhXBwQgtjNDjgR");
	this.shape_13.setTransform(2.0034,0.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[]},1).to({state:[{t:this.shape}]},28).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[]},1).wait(31));

}).prototype = p = new cjs.MovieClip();


(lib.Scene_1_Layer_13 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_13
	this.instance = new lib.bg_part();
	this.instance.parent = this;
	this.instance.setTransform(447,0);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Scene_1_Layer_13, null, null);


(lib.Scene_1_Layer_2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#E7191F").s().p("EhUXA1IMAAAhqPMCovAAAMAAABqPg");
	this.shape.setTransform(540,340);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Scene_1_Layer_2, null, null);


(lib.Symbol2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#F51431").s().p("AAAAqQgRAAgNgMQgMgMAAgSQAAgQAMgNQANgNARAAIADAAQAPABALAMQANANAAAQQAAASgNAMQgLAMgPAAIgDAAg");
	this.shape.setTransform(4.25,4.25);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol2, new cjs.Rectangle(0,0,8.5,8.5), null);


(lib.Symbol12 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Symbol_6 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("ABZCMQg6g6AAhSQAAhSA6g6QA6g5BTAAQBSAAA5A5QA7A6AABSQAABSg7A6Qg5A6hSABQhTgBg6g6g");
	mask.setTransform(42.8,18.4);

	// Symbol_7
	this.instance = new lib.Symbol7();
	this.instance.parent = this;
	this.instance.setTransform(65.95,58.75,1,1,0,0,0,19.9,19.9);

	var maskedShapeInstanceList = [this.instance];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1).to({y:54.4},0).wait(1).to({y:51.75},0).wait(1).to({y:49.9},0).wait(1).to({y:48.5},0).wait(1).to({y:47.4},0).wait(1).to({y:46.6},0).wait(1).to({y:45.95},0).wait(1).to({y:45.4},0).wait(1).to({y:45},0).wait(1).to({y:44.7},0).wait(1).to({y:44.45},0).wait(1).to({y:44.25},0).wait(1).to({y:44.1},0).wait(1).to({y:44.05},0).wait(1).to({y:44},0).wait(37).to({x:67.75,y:46.5},0).to({y:58.75},8,cjs.Ease.get(1)).wait(34));

	// Symbol_8 (mask)
	var mask_1 = new cjs.Shape();
	mask_1._off = true;
	mask_1.graphics.p("AiMCMQg5g6gBhSQABhSA5g6QA6g5BSgBQBSABA6A5QA6A6AABSQAABSg6A6Qg6A6hSAAQhSAAg6g6g");
	mask_1.setTransform(19.85,19.85);

	// Symbol_9
	this.instance_1 = new lib.Symbol9();
	this.instance_1.parent = this;
	this.instance_1.setTransform(20.2,60.5,1,1,0,0,0,19.9,19.9);

	var maskedShapeInstanceList = [this.instance_1];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask_1;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(1).to({y:56.15},0).wait(1).to({y:53.5},0).wait(1).to({y:51.65},0).wait(1).to({y:50.25},0).wait(1).to({y:49.15},0).wait(1).to({y:48.35},0).wait(1).to({y:47.7},0).wait(1).to({y:47.15},0).wait(1).to({y:46.75},0).wait(1).to({y:46.45},0).wait(1).to({y:46.2},0).wait(1).to({y:46},0).wait(1).to({y:45.85},0).wait(1).to({y:45.8},0).wait(1).to({y:45.75},0).wait(37).to({x:22,y:48.25},0).to({y:60.5},8,cjs.Ease.get(1)).wait(34));

	// Symbol_11
	this.instance_2 = new lib.Symbol11();
	this.instance_2.parent = this;
	this.instance_2.setTransform(60.65,23.15,1,1,0,0,0,6.7,6.8);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(15).to({x:64.15,y:21.3},7,cjs.Ease.get(1)).wait(3).to({x:60.65,y:23.15},7,cjs.Ease.get(1)).wait(3).to({x:64.15,y:21.3},7,cjs.Ease.get(1)).wait(3).to({x:60.65,y:23.15},7,cjs.Ease.get(1)).wait(42));

	// Symbol_10
	this.instance_3 = new lib.Symbol10();
	this.instance_3.parent = this;
	this.instance_3.setTransform(12.5,24.15,1,1,0,0,0,6.7,6.8);

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(15).to({x:16,y:22.3},7,cjs.Ease.get(1)).wait(3).to({x:12.5,y:24.15},7,cjs.Ease.get(1)).wait(3).to({x:16,y:22.3},7,cjs.Ease.get(1)).wait(3).to({x:12.5,y:24.15},7,cjs.Ease.get(1)).wait(42));

	// Layer_8
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AiMCMQg5g6gBhSQABhSA5g6QA6g5BSgBQBSABA6A5QA6A6AABSQAABSg6A6Qg6A6hSAAQhSAAg6g6g");
	this.shape.setTransform(19.85,19.85);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(94));

	// Layer_9
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AiMCMQg5g6gBhSQABhSA5g6QA6g5BSAAQBSAAA6A5QA6A6AABSQAABSg6A6Qg6A6hSABQhSgBg6g6g");
	this.shape_1.setTransform(65.75,18.4);

	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(94));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,-1.4,85.6,41.1);


(lib.Symbol5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_93 = function() {
		this.___loopingOver___ = true;
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(93).call(this.frame_93).wait(1));

	// Layer_5_obj_
	this.Layer_5 = new lib.Symbol_5_Layer_5();
	this.Layer_5.name = "Layer_5";
	this.Layer_5.parent = this;
	this.Layer_5.setTransform(-5.6,-43.6,1,1,0,0,0,-5.6,-43.6);
	this.Layer_5.depth = 0;
	this.Layer_5.isAttachedToCamera = 0
	this.Layer_5.isAttachedToMask = 0
	this.Layer_5.layerDepth = 0
	this.Layer_5.layerIndex = 0
	this.Layer_5.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_5).wait(94));

	// Layer_4_obj_
	this.Layer_4 = new lib.Symbol_5_Layer_4();
	this.Layer_4.name = "Layer_4";
	this.Layer_4.parent = this;
	this.Layer_4.setTransform(-19.6,-53.6,1,1,0,0,0,-19.6,-53.6);
	this.Layer_4.depth = 0;
	this.Layer_4.isAttachedToCamera = 0
	this.Layer_4.isAttachedToMask = 0
	this.Layer_4.layerDepth = 0
	this.Layer_4.layerIndex = 1
	this.Layer_4.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_4).wait(94));

	// Layer_3_obj_
	this.Layer_3 = new lib.Symbol_5_Layer_3();
	this.Layer_3.name = "Layer_3";
	this.Layer_3.parent = this;
	this.Layer_3.setTransform(-59.4,-3.2,1,1,0,0,0,-59.4,-3.2);
	this.Layer_3.depth = 0;
	this.Layer_3.isAttachedToCamera = 0
	this.Layer_3.isAttachedToMask = 0
	this.Layer_3.layerDepth = 0
	this.Layer_3.layerIndex = 2
	this.Layer_3.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_3).wait(94));

	// Layer_2_obj_
	this.Layer_2 = new lib.Symbol_5_Layer_2();
	this.Layer_2.name = "Layer_2";
	this.Layer_2.parent = this;
	this.Layer_2.setTransform(-69.8,-15.5,1,1,0,0,0,-69.8,-15.5);
	this.Layer_2.depth = 0;
	this.Layer_2.isAttachedToCamera = 0
	this.Layer_2.isAttachedToMask = 0
	this.Layer_2.layerDepth = 0
	this.Layer_2.layerIndex = 3
	this.Layer_2.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_2).wait(94));

	// Layer_1_obj_
	this.Layer_1 = new lib.Symbol_5_Layer_1();
	this.Layer_1.name = "Layer_1";
	this.Layer_1.parent = this;
	this.Layer_1.depth = 0;
	this.Layer_1.isAttachedToCamera = 0
	this.Layer_1.isAttachedToMask = 0
	this.Layer_1.layerDepth = 0
	this.Layer_1.layerIndex = 4
	this.Layer_1.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_1).wait(94));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-94.5,-74.4,120.8,97.10000000000001);


(lib.Symbol4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_74 = function() {
		this.___loopingOver___ = true;
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(74).call(this.frame_74).wait(1));

	// Layer_2_obj_
	this.Layer_2 = new lib.Symbol_4_Layer_2();
	this.Layer_2.name = "Layer_2";
	this.Layer_2.parent = this;
	this.Layer_2.setTransform(5.4,9,1,1,0,0,0,5.4,9);
	this.Layer_2.depth = 0;
	this.Layer_2.isAttachedToCamera = 0
	this.Layer_2.isAttachedToMask = 0
	this.Layer_2.layerDepth = 0
	this.Layer_2.layerIndex = 0
	this.Layer_2.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_2).wait(75));

	// Layer_1_obj_
	this.Layer_1 = new lib.Symbol_4_Layer_1();
	this.Layer_1.name = "Layer_1";
	this.Layer_1.parent = this;
	this.Layer_1.depth = 0;
	this.Layer_1.isAttachedToCamera = 0
	this.Layer_1.isAttachedToMask = 0
	this.Layer_1.layerDepth = 0
	this.Layer_1.layerIndex = 1
	this.Layer_1.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_1).wait(75));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-15.9,-27.3,55.3,51.1);


(lib.Symbol1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#1B1814").ss(11,1,1).p("AkLhfQBDDEDRgGQBWgDBJgrQBKgtAahG");
	this.shape.setTransform(156.025,-52.5925);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(94));

	// Layer_3
	this.instance = new lib.Symbol2();
	this.instance.parent = this;
	this.instance.setTransform(137.2,-49.4,2.0168,1.0421,0,46.0839,51.8739,5.4,7.5);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(12).to({_off:false},0).to({scaleY:2.0168,guide:{path:[137.2,-49.3,137.5,-49,137.9,-48.7]}},4).wait(1).to({regX:4.3,regY:4.3,scaleX:2.0159,scaleY:2.0177,skewX:45.7195,skewY:51.4594,x:141.35,y:-54.8},0).wait(1).to({scaleX:2.0136,scaleY:2.0199,skewX:44.7878,skewY:50.3997,x:141.8,y:-54.45},0).wait(1).to({scaleX:2.0096,scaleY:2.0237,skewX:43.1697,skewY:48.559,x:142.7,y:-53.7},0).wait(1).to({scaleX:2.0035,scaleY:2.0294,skewX:40.7228,skewY:45.7757,x:144.15,y:-52.85},0).wait(1).to({scaleX:1.995,scaleY:2.0375,skewX:37.2999,skewY:41.8822,x:146.35,y:-51.75},0).wait(1).to({scaleX:1.9839,scaleY:2.0481,skewX:32.8139,skewY:36.7794,x:149.5,y:-50.75},0).wait(1).to({scaleX:1.9705,scaleY:2.0609,skewX:27.3853,skewY:30.6045,x:153.3,y:-50.15},0).wait(1).to({scaleX:1.9559,scaleY:2.0748,skewX:21.5115,skewY:23.9231,x:157.55,y:-50.1},0).wait(1).to({scaleX:1.9422,scaleY:2.0879,skewX:15.9743,skewY:17.6245,x:161.1,y:-50.5},0).wait(1).to({scaleX:1.9309,scaleY:2.0987,skewX:11.4074,skewY:12.4298,x:163.95,y:-51.25},0).wait(1).to({scaleX:1.9225,scaleY:2.1066,skewX:8.0351,skewY:8.5938,x:166.05,y:-52},0).wait(1).to({scaleX:1.917,scaleY:2.1119,skewX:5.788,skewY:6.0378,x:167.45,y:-52.6},0).wait(1).to({scaleX:1.9137,scaleY:2.115,skewX:4.4942,skewY:4.5662,x:168.2,y:-53},0).wait(1).to({regX:5.4,regY:7.5,scaleX:1.9125,scaleY:2.1162,rotation:3.9793,skewX:0,skewY:0,x:170.2,y:-46.35},0).wait(7).to({regX:4.3,regY:4.3,scaleX:1.914,scaleY:2.1148,rotation:0,skewX:4.5733,skewY:4.6561,x:168.2,y:-52.95},0).wait(1).to({scaleX:1.9174,scaleY:2.1115,skewX:5.9626,skewY:6.2364,x:167.3,y:-52.5},0).wait(1).to({scaleX:1.923,scaleY:2.1061,skewX:8.2536,skewY:8.8424,x:165.8,y:-51.9},0).wait(1).to({scaleX:1.9312,scaleY:2.0984,skewX:11.5185,skewY:12.5562,x:163.55,y:-51.1},0).wait(1).to({scaleX:1.9416,scaleY:2.0884,skewX:15.7359,skewY:17.3534,x:160.55,y:-50.4},0).wait(1).to({scaleX:1.9539,scaleY:2.0767,skewX:20.7133,skewY:23.0151,x:156.95,y:-50},0).wait(1).to({scaleX:1.9672,scaleY:2.0641,skewX:26.0541,skewY:29.0903,x:153.3,y:-50.15},0).wait(1).to({scaleX:1.98,scaleY:2.0518,skewX:31.248,skewY:34.9982,x:150.1,y:-50.5},0).wait(1).to({scaleX:1.9914,scaleY:2.0409,skewX:35.8517,skewY:40.2349,x:147.3,y:-51.3},0).wait(1).to({scaleX:2.0007,scaleY:2.0321,skewX:39.611,skewY:44.5111,x:145.05,y:-52.25},0).wait(1).to({scaleX:2.0078,scaleY:2.0254,skewX:42.4537,skewY:47.7446,x:143.25,y:-53.25},0).wait(1).to({scaleX:2.0127,scaleY:2.0207,skewX:44.4197,skewY:49.9809,x:142.1,y:-54.1},0).wait(1).to({scaleX:2.0156,scaleY:2.0179,skewX:45.5968,skewY:51.3198,x:141.45,y:-54.7},0).wait(1).to({regX:5.4,regY:7.5,scaleX:2.0168,scaleY:2.0168,skewX:46.0839,skewY:51.8739,x:137.85,y:-48.75},0).to({regX:5.5,regY:7.4,scaleY:1.2283,skewX:46.0842,x:134.95,y:-45.55},4).to({_off:true},1).wait(39));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(123.8,-67.8,64.50000000000001,30.299999999999997);


(lib.Symbol_15_Symbol_14_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Symbol_14
	this.instance = new lib.Symbol14();
	this.instance.parent = this;
	this.instance.setTransform(63.85,-5.4,1,1,11.6297,0,0,0.1,-0.1);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(19).to({regX:0,regY:0,rotation:0,x:65.5,y:28.5},10,cjs.Ease.get(1)).wait(13).to({regX:0.3,regY:0.4,scaleX:0.5452,scaleY:0.5452,rotation:-22.9983,x:126.65,y:99.2,alpha:0.5},11).to({regY:0.5,scaleX:0.5451,scaleY:0.5451,rotation:-44.9864,x:207.4,y:184.85,alpha:0},15).to({_off:true},1).wait(25));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_15_Symbol_14 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Symbol_14
	this.instance = new lib.Symbol14();
	this.instance.parent = this;
	this.instance.setTransform(213.6,107.2,0.6894,0.6894,-106.7976,0,0,0.1,-0.1);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(83).to({_off:false},0).to({scaleX:1,scaleY:1,rotation:-106.7982,x:260.15,y:58.5},3).to({regY:0,scaleX:0.9999,scaleY:0.9999,rotation:-53.842,x:183.25,y:65.95},2).to({regY:0.1,rotation:-38.842,x:114.7,y:62},1).to({regX:0.2,regY:-0.1,scaleX:1,scaleY:1,rotation:11.6297,x:63.95,y:-5.35},4,cjs.Ease.get(1)).wait(1));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_15_Symbol_13 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Symbol_13
	this.instance = new lib.Symbol13();
	this.instance.parent = this;
	this.instance.setTransform(117.5,103);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(94));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_15_Symbol_5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Symbol_5
	this.instance = new lib.Symbol5("synched",0,false);
	this.instance.parent = this;
	this.instance.setTransform(-63.8,24.3);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(94));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_15_Symbol_4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Symbol_4
	this.instance = new lib.Symbol4("single",0);
	this.instance.parent = this;
	this.instance.setTransform(19.25,103.6);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(30).to({startPosition:0},0).to({rotation:-10.6877,x:20.75,y:73.9,mode:"synched",startPosition:29,loop:false},5,cjs.Ease.get(1)).wait(15).to({mode:"single",startPosition:0},0).to({rotation:0,x:19.25,y:103.6},8,cjs.Ease.get(1)).wait(36));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_15_Symbol_3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Symbol_3
	this.instance = new lib.Symbol3();
	this.instance.parent = this;
	this.instance.setTransform(115.85,5.2,1,1,14.9992,0,0,19.8,0);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(19).to({rotation:0,x:119.35,y:24.45},10,cjs.Ease.get(1)).wait(13).to({regY:-0.1,rotation:-14.9992,y:27.35},3,cjs.Ease.get(0.5)).to({regY:0,rotation:0,y:24.45},7,cjs.Ease.get(1)).wait(18).to({rotation:14.9992,x:115.85,y:5.2},0).to({regY:0.1,scaleX:0.9985,scaleY:0.9985,rotation:-29.5893,x:157.9,y:31.1},3,cjs.Ease.get(-1)).to({scaleX:0.9974,scaleY:0.9974,rotation:-51.9186,x:218.15,y:22.15},1).to({regX:19.7,regY:0.2,scaleX:0.9999,scaleY:0.9999,rotation:-96.6917,x:248.35,y:7.55},3).to({x:237.55,y:63.55},4).wait(2).to({x:248.35,y:7.55},3).to({regX:19.8,regY:0.1,scaleX:0.9974,scaleY:0.9974,rotation:-51.9186,x:218.15,y:22.15},2).to({scaleX:0.9985,scaleY:0.9985,rotation:-29.5893,x:157.9,y:31.1},1).to({regY:0,scaleX:1,scaleY:1,rotation:14.9992,x:115.85,y:5.2},4,cjs.Ease.get(1)).wait(1));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_15_Layer_16 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_16
	this.instance = new lib.Symbol16();
	this.instance.parent = this;
	this.instance.setTransform(-163.1,-66.45,0.46,0.46,44.9987,0,0,3.7,3.7);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(32).to({_off:false},0).to({regX:3.8,scaleX:1,scaleY:1,rotation:0,x:-162.4,y:-65.25},6,cjs.Ease.get(1)).wait(33).to({regY:3.8,rotation:50.9528,x:-162.5,y:-65.15},10,cjs.Ease.get(-1)).to({_off:true},1).wait(12));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_15_Layer_14 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_14
	this.instance = new lib.Symbol17();
	this.instance.parent = this;
	this.instance.setTransform(-163.05,-66.45,0.489,0.489,-45,0,0,-3.6,3.4);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(32).to({_off:false},0).to({regY:3.5,scaleX:1,scaleY:1,rotation:0,x:-162.4,y:-65.2},6,cjs.Ease.get(1)).wait(33).to({regY:3.4,rotation:-41.2534,x:-162.45,y:-65.3},10,cjs.Ease.get(-1)).to({_off:true},1).wait(12));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol2_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.instance = new lib.Symbol12();
	this.instance.parent = this;
	this.instance.setTransform(30.35,-14.4,1.0998,1.0998,0,0.0485,-179.9507,43.4,34.9);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	// Layer_1
	this.instance_1 = new lib.Symbol1();
	this.instance_1.parent = this;
	this.instance_1.setTransform(27.6,15.75,1.06,1.06,0,0,0,156,-52.6);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol2_1, new cjs.Rectangle(-16.2,-54.4,94.4,90), null);


(lib.Symbol_15_Symbol_2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Symbol_2
	this.instance = new lib.Symbol2_1();
	this.instance.parent = this;
	this.instance.setTransform(60.4,-58.5,0.94,0.94,0,0,0,27.6,15.7);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(94));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol15 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_93 = function() {
		this.___loopingOver___ = true;
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(93).call(this.frame_93).wait(1));

	// Layer_14_obj_
	this.Layer_14 = new lib.Symbol_15_Layer_14();
	this.Layer_14.name = "Layer_14";
	this.Layer_14.parent = this;
	this.Layer_14.depth = 0;
	this.Layer_14.isAttachedToCamera = 0
	this.Layer_14.isAttachedToMask = 0
	this.Layer_14.layerDepth = 0
	this.Layer_14.layerIndex = 0
	this.Layer_14.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_14).wait(94));

	// Layer_16_obj_
	this.Layer_16 = new lib.Symbol_15_Layer_16();
	this.Layer_16.name = "Layer_16";
	this.Layer_16.parent = this;
	this.Layer_16.depth = 0;
	this.Layer_16.isAttachedToCamera = 0
	this.Layer_16.isAttachedToMask = 0
	this.Layer_16.layerDepth = 0
	this.Layer_16.layerIndex = 1
	this.Layer_16.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_16).wait(94));

	// Layer_17 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	var mask_graphics_19 = new cjs.Graphics().p("AigCiQhEhDAAhfQAAhdBEhDQBChEBeAAQBeAABEBEQBDBDgBBdQABBfhDBDQhEBDheAAQheAAhChDg");
	var mask_graphics_20 = new cjs.Graphics().p("AihCiQhDhDABhfQgBhdBDhDQBEhEBdAAQBfAABCBEQBEBDAABdQAABfhEBDQhCBDhfAAQhdAAhEhDg");
	var mask_graphics_21 = new cjs.Graphics().p("AihCiQhDhDABhfQgBhdBDhDQBEhEBdAAQBfAABCBEQBEBDAABdQAABfhEBDQhCBDhfAAQhdAAhEhDg");
	var mask_graphics_22 = new cjs.Graphics().p("AihCiQhDhDABhfQgBhdBDhDQBDhEBeAAQBeAABDBEQBEBDAABdQAABfhEBDQhDBDheAAQheAAhDhDg");
	var mask_graphics_23 = new cjs.Graphics().p("AigCiQhDhDgBhfQABhdBDhDQBDhEBdAAQBfAABDBEQBDBDgBBdQABBfhDBDQhDBDhfAAQhdAAhDhDg");
	var mask_graphics_24 = new cjs.Graphics().p("AihCiQhChDAAhfQAAhdBChDQBEhEBdAAQBfAABCBEQBDBDAABdQAABfhDBDQhCBDhfAAQhdAAhEhDg");
	var mask_graphics_25 = new cjs.Graphics().p("AihCiQhDhDABhfQgBhdBDhDQBEhEBdAAQBfAABCBEQBEBDAABdQAABfhEBDQhCBDhfAAQhdAAhEhDg");
	var mask_graphics_26 = new cjs.Graphics().p("AihCiQhChDAAhfQAAhdBChDQBDhEBeAAQBeAABEBEQBCBDAABdQAABfhCBDQhEBDheAAQheAAhDhDg");
	var mask_graphics_27 = new cjs.Graphics().p("AihCiQhDhDABhfQgBhdBDhDQBDhEBeAAQBeAABDBEQBEBDAABdQAABfhEBDQhDBDheAAQheAAhDhDg");
	var mask_graphics_28 = new cjs.Graphics().p("AihCiQhChDAAhfQAAhdBChDQBDhEBeAAQBeAABEBEQBCBDAABdQAABfhCBDQhEBDheAAQheAAhDhDg");
	var mask_graphics_29 = new cjs.Graphics().p("AihCiQhChDAAhfQAAhdBChDQBDhEBeAAQBeAABEBEQBCBDAABdQAABfhCBDQhEBDheAAQheAAhDhDg");
	var mask_graphics_30 = new cjs.Graphics().p("AihCiQhDhDAAhfQAAhdBDhDQBDhEBeAAQBeAABDBEQBDBDABBdQgBBfhDBDQhDBDheAAQheAAhDhDg");
	var mask_graphics_31 = new cjs.Graphics().p("AigCiQhDhDgBhfQABhdBDhDQBDhEBdAAQBfAABDBEQBDBDgBBdQABBfhDBDQhDBDhfAAQhdAAhDhDg");
	var mask_graphics_32 = new cjs.Graphics().p("AigCiQhEhDAAhfQAAhdBEhDQBDhEBdAAQBfAABDBEQBDBDgBBdQABBfhDBDQhDBDhfAAQhdAAhDhDg");
	var mask_graphics_33 = new cjs.Graphics().p("AihCiQhDhDABhfQgBhdBDhDQBEhEBdAAQBfAABCBEQBEBDAABdQAABfhEBDQhCBDhfAAQhdAAhEhDg");
	var mask_graphics_34 = new cjs.Graphics().p("AigCiQhDhDgBhfQABhdBDhDQBDhEBdAAQBeAABDBEQBDBDABBdQgBBfhDBDQhDBDheAAQhdAAhDhDg");
	var mask_graphics_69 = new cjs.Graphics().p("AigCiQhDhDgBhfQABhdBDhDQBDhEBdAAQBeAABDBEQBDBDABBdQgBBfhDBDQhDBDheAAQhdAAhDhDg");
	var mask_graphics_70 = new cjs.Graphics().p("AigCiQhEhDAAhfQAAhdBEhDQBChEBeAAQBeAABEBEQBDBDgBBdQABBfhDBDQhEBDheAAQheAAhChDg");
	var mask_graphics_71 = new cjs.Graphics().p("AihCiQhDhDAAhfQAAhdBDhDQBDhEBeAAQBeAABDBEQBDBDABBdQgBBfhDBDQhDBDheAAQheAAhDhDg");
	var mask_graphics_72 = new cjs.Graphics().p("AihCiQhDhDABhfQgBhdBDhDQBEhEBdAAQBfAABCBEQBEBDAABdQAABfhEBDQhCBDhfAAQhdAAhEhDg");
	var mask_graphics_73 = new cjs.Graphics().p("AigCiQhEhDAAhfQAAhdBEhDQBDhEBdAAQBfAABDBEQBDBDgBBdQABBfhDBDQhDBDhfAAQhdAAhDhDg");
	var mask_graphics_74 = new cjs.Graphics().p("AihCiQhChDAAhfQAAhdBChDQBEhEBdAAQBfAABCBEQBDBDAABdQAABfhDBDQhCBDhfAAQhdAAhEhDg");
	var mask_graphics_75 = new cjs.Graphics().p("AihCiQhDhDABhfQgBhdBDhDQBDhEBeAAQBeAABDBEQBEBDAABdQAABfhEBDQhDBDheAAQheAAhDhDg");
	var mask_graphics_76 = new cjs.Graphics().p("AihCiQhDhDAAhfQAAhdBDhDQBDhEBeAAQBeAABDBEQBDBDABBdQgBBfhDBDQhDBDheAAQheAAhDhDg");
	var mask_graphics_77 = new cjs.Graphics().p("AihCiQhChDAAhfQAAhdBChDQBDhEBeAAQBeAABEBEQBCBDAABdQAABfhCBDQhEBDheAAQheAAhDhDg");
	var mask_graphics_78 = new cjs.Graphics().p("AihCiQhChDAAhfQAAhdBChDQBEhEBdAAQBfAABCBEQBDBDAABdQAABfhDBDQhCBDhfAAQhdAAhEhDg");
	var mask_graphics_79 = new cjs.Graphics().p("AihCiQhDhDABhfQgBhdBDhDQBEhEBdAAQBfAABCBEQBEBDAABdQAABfhEBDQhCBDhfAAQhdAAhEhDg");
	var mask_graphics_80 = new cjs.Graphics().p("AihCiQhDhDAAhfQAAhdBDhDQBDhEBeAAQBeAABDBEQBDBDABBdQgBBfhDBDQhDBDheAAQheAAhDhDg");
	var mask_graphics_81 = new cjs.Graphics().p("AihCiQhDhDABhfQgBhdBDhDQBDhEBeAAQBeAABDBEQBEBDAABdQAABfhEBDQhDBDheAAQheAAhDhDg");
	var mask_graphics_82 = new cjs.Graphics().p("AihCiQhDhDABhfQgBhdBDhDQBDhEBeAAQBeAABDBEQBEBDAABdQAABfhEBDQhDBDheAAQheAAhDhDg");
	var mask_graphics_83 = new cjs.Graphics().p("AigCiQhDhDgBhfQABhdBDhDQBDhEBdAAQBeAABDBEQBDBDABBdQgBBfhDBDQhDBDheAAQhdAAhDhDg");
	var mask_graphics_84 = new cjs.Graphics().p("AihCiQhDhDAAhfQAAhdBDhDQBDhEBeAAQBeAABDBEQBDBDABBdQgBBfhDBDQhDBDheAAQheAAhDhDg");
	var mask_graphics_85 = new cjs.Graphics().p("AihCiQhChDAAhfQAAhdBChDQBEhEBdAAQBfAABCBEQBDBDAABdQAABfhDBDQhCBDhfAAQhdAAhEhDg");
	var mask_graphics_86 = new cjs.Graphics().p("AihCiQhChDAAhfQAAhdBChDQBDhEBeAAQBeAABEBEQBCBDAABdQAABfhCBDQhEBDheAAQheAAhDhDg");

	this.timeline.addTween(cjs.Tween.get(mask).to({graphics:null,x:0,y:0}).wait(19).to({graphics:mask_graphics_19,x:-112.2,y:-104.075}).wait(1).to({graphics:mask_graphics_20,x:-112.8,y:-104.075}).wait(1).to({graphics:mask_graphics_21,x:-114.6,y:-104.125}).wait(1).to({graphics:mask_graphics_22,x:-117.65,y:-104.175}).wait(1).to({graphics:mask_graphics_23,x:-121.85,y:-104.275}).wait(1).to({graphics:mask_graphics_24,x:-127.3,y:-104.375}).wait(1).to({graphics:mask_graphics_25,x:-133.95,y:-104.525}).wait(1).to({graphics:mask_graphics_26,x:-141.8,y:-104.675}).wait(1).to({graphics:mask_graphics_27,x:-144.2,y:-99.925}).wait(1).to({graphics:mask_graphics_28,x:-146.3,y:-95.775}).wait(1).to({graphics:mask_graphics_29,x:-148.1,y:-92.275}).wait(1).to({graphics:mask_graphics_30,x:-149.5,y:-89.425}).wait(1).to({graphics:mask_graphics_31,x:-150.65,y:-87.175}).wait(1).to({graphics:mask_graphics_32,x:-151.45,y:-85.575}).wait(1).to({graphics:mask_graphics_33,x:-151.95,y:-84.625}).wait(1).to({graphics:mask_graphics_34,x:-152.1,y:-84.325}).wait(35).to({graphics:mask_graphics_69,x:-152.1,y:-84.325}).wait(1).to({graphics:mask_graphics_70,x:-152.25,y:-83.875}).wait(1).to({graphics:mask_graphics_71,x:-152.65,y:-82.625}).wait(1).to({graphics:mask_graphics_72,x:-153.3,y:-80.625}).wait(1).to({graphics:mask_graphics_73,x:-154.15,y:-77.975}).wait(1).to({graphics:mask_graphics_74,x:-155.2,y:-74.775}).wait(1).to({graphics:mask_graphics_75,x:-156.35,y:-71.175}).wait(1).to({graphics:mask_graphics_76,x:-157.6,y:-67.375}).wait(1).to({graphics:mask_graphics_77,x:-158.9,y:-63.425}).wait(1).to({graphics:mask_graphics_78,x:-160.15,y:-59.475}).wait(1).to({graphics:mask_graphics_79,x:-161.4,y:-55.675}).wait(1).to({graphics:mask_graphics_80,x:-162.55,y:-52.175}).wait(1).to({graphics:mask_graphics_81,x:-163.55,y:-49.025}).wait(1).to({graphics:mask_graphics_82,x:-164.45,y:-46.325}).wait(1).to({graphics:mask_graphics_83,x:-165.15,y:-44.125}).wait(1).to({graphics:mask_graphics_84,x:-165.7,y:-42.525}).wait(1).to({graphics:mask_graphics_85,x:-166,y:-41.575}).wait(1).to({graphics:mask_graphics_86,x:-166.1,y:-41.225}).wait(8));

	// Layer_15_obj_
	this.Layer_15 = new lib.Symbol_15_Layer_15();
	this.Layer_15.name = "Layer_15";
	this.Layer_15.parent = this;
	this.Layer_15.depth = 0;
	this.Layer_15.isAttachedToCamera = 0
	this.Layer_15.isAttachedToMask = 0
	this.Layer_15.layerDepth = 0
	this.Layer_15.layerIndex = 2
	this.Layer_15.maskLayerName = 0

	var maskedShapeInstanceList = [this.Layer_15];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.Layer_15).wait(94));

	// Symbol_4_obj_
	this.Symbol_4 = new lib.Symbol_15_Symbol_4();
	this.Symbol_4.name = "Symbol_4";
	this.Symbol_4.parent = this;
	this.Symbol_4.setTransform(22.9,104.2,1,1,0,0,0,22.9,104.2);
	this.Symbol_4.depth = 0;
	this.Symbol_4.isAttachedToCamera = 0
	this.Symbol_4.isAttachedToMask = 0
	this.Symbol_4.layerDepth = 0
	this.Symbol_4.layerIndex = 3
	this.Symbol_4.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Symbol_4).wait(94));

	// Layer_12_obj_
	this.Layer_12 = new lib.Symbol_15_Layer_12();
	this.Layer_12.name = "Layer_12";
	this.Layer_12.parent = this;
	this.Layer_12.setTransform(-62.4,110,1,1,0,0,0,-62.4,110);
	this.Layer_12.depth = 0;
	this.Layer_12.isAttachedToCamera = 0
	this.Layer_12.isAttachedToMask = 0
	this.Layer_12.layerDepth = 0
	this.Layer_12.layerIndex = 4
	this.Layer_12.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_12).wait(94));

	// Layer_5 (mask)
	var mask_1 = new cjs.Shape();
	mask_1._off = true;
	var mask_1_graphics_77 = new cjs.Graphics().p("AjUMEQFZj2AFgaIAeguIARguIAFgxQADgmgQgkQgKgbgQgXQgfgsgugWIgZgMQgtgVgwgUIgKgEIgDgBIgIgDIAAgBI1hosICdwoMAqRAAcIgnXGIA/XYI6SAZg");

	this.timeline.addTween(cjs.Tween.get(mask_1).to({graphics:null,x:0,y:0}).wait(77).to({graphics:mask_1_graphics_77,x:268.9,y:55.925}).wait(10).to({graphics:null,x:0,y:0}).wait(7));

	// Symbol_3_obj_
	this.Symbol_3 = new lib.Symbol_15_Symbol_3();
	this.Symbol_3.name = "Symbol_3";
	this.Symbol_3.parent = this;
	this.Symbol_3.setTransform(102.5,-15.7,1,1,0,0,0,102.5,-15.7);
	this.Symbol_3.depth = 0;
	this.Symbol_3.isAttachedToCamera = 0
	this.Symbol_3.isAttachedToMask = 0
	this.Symbol_3.layerDepth = 0
	this.Symbol_3.layerIndex = 5
	this.Symbol_3.maskLayerName = 0

	var maskedShapeInstanceList = [this.Symbol_3];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask_1;
	}

	this.timeline.addTween(cjs.Tween.get(this.Symbol_3).wait(94));

	// Layer_2_obj_
	this.Layer_2 = new lib.Symbol_15_Layer_2();
	this.Layer_2.name = "Layer_2";
	this.Layer_2.parent = this;
	this.Layer_2.setTransform(151.8,-6.4,1,1,0,0,0,151.8,-6.4);
	this.Layer_2.depth = 0;
	this.Layer_2.isAttachedToCamera = 0
	this.Layer_2.isAttachedToMask = 0
	this.Layer_2.layerDepth = 0
	this.Layer_2.layerIndex = 6
	this.Layer_2.maskLayerName = 0

	var maskedShapeInstanceList = [this.Layer_2];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask_1;
	}

	this.timeline.addTween(cjs.Tween.get(this.Layer_2).wait(94));

	// Symbol_14_obj_
	this.Symbol_14 = new lib.Symbol_15_Symbol_14();
	this.Symbol_14.name = "Symbol_14";
	this.Symbol_14.parent = this;
	this.Symbol_14.depth = 0;
	this.Symbol_14.isAttachedToCamera = 0
	this.Symbol_14.isAttachedToMask = 0
	this.Symbol_14.layerDepth = 0
	this.Symbol_14.layerIndex = 7
	this.Symbol_14.maskLayerName = 0

	var maskedShapeInstanceList = [this.Symbol_14];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask_1;
	}

	this.timeline.addTween(cjs.Tween.get(this.Symbol_14).wait(94));

	// Symbol_5_obj_
	this.Symbol_5 = new lib.Symbol_15_Symbol_5();
	this.Symbol_5.name = "Symbol_5";
	this.Symbol_5.parent = this;
	this.Symbol_5.setTransform(-98,-1.6,1,1,0,0,0,-98,-1.6);
	this.Symbol_5.depth = 0;
	this.Symbol_5.isAttachedToCamera = 0
	this.Symbol_5.isAttachedToMask = 0
	this.Symbol_5.layerDepth = 0
	this.Symbol_5.layerIndex = 8
	this.Symbol_5.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Symbol_5).wait(94));

	// Layer_18 (mask)
	var mask_2 = new cjs.Shape();
	mask_2._off = true;
	var mask_2_graphics_0 = new cjs.Graphics().p("Aw1NhMgI3gi/MAx+gAjIBbbyI3XQRg");
	var mask_2_graphics_19 = new cjs.Graphics().p("Aw1NhMgI3gi/MAx+gAjIBbbyI3XQRg");

	this.timeline.addTween(cjs.Tween.get(mask_2).to({graphics:mask_2_graphics_0,x:118.825,y:44.1}).wait(19).to({graphics:mask_2_graphics_19,x:118.825,y:44.1}).wait(75));

	// Symbol_14_obj_
	this.Symbol_14_1 = new lib.Symbol_15_Symbol_14_1();
	this.Symbol_14_1.name = "Symbol_14_1";
	this.Symbol_14_1.parent = this;
	this.Symbol_14_1.setTransform(63.8,-5.3,1,1,0,0,0,63.8,-5.3);
	this.Symbol_14_1.depth = 0;
	this.Symbol_14_1.isAttachedToCamera = 0
	this.Symbol_14_1.isAttachedToMask = 0
	this.Symbol_14_1.layerDepth = 0
	this.Symbol_14_1.layerIndex = 9
	this.Symbol_14_1.maskLayerName = 0

	var maskedShapeInstanceList = [this.Symbol_14_1];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask_2;
	}

	this.timeline.addTween(cjs.Tween.get(this.Symbol_14_1).wait(94));

	// Symbol_2_obj_
	this.Symbol_2 = new lib.Symbol_15_Symbol_2();
	this.Symbol_2.name = "Symbol_2";
	this.Symbol_2.parent = this;
	this.Symbol_2.setTransform(6.4,-65.3,1,1,0,0,0,6.4,-65.3);
	this.Symbol_2.depth = 0;
	this.Symbol_2.isAttachedToCamera = 0
	this.Symbol_2.isAttachedToMask = 0
	this.Symbol_2.layerDepth = 0
	this.Symbol_2.layerIndex = 10
	this.Symbol_2.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Symbol_2).wait(94));

	// Спеши__количество_призов_ограничено__obj_
	this.Спеши__количество_призов_ограничено_ = new lib.Symbol_15_Спеши__количество_призов_ограничено_();
	this.Спеши__количество_призов_ограничено_.name = "Спеши__количество_призов_ограничено_";
	this.Спеши__количество_призов_ограничено_.parent = this;
	this.Спеши__количество_призов_ограничено_.setTransform(-120.9,-134.7,1,1,0,0,0,-120.9,-134.7);
	this.Спеши__количество_призов_ограничено_.depth = 0;
	this.Спеши__количество_призов_ограничено_.isAttachedToCamera = 0
	this.Спеши__количество_призов_ограничено_.isAttachedToMask = 0
	this.Спеши__количество_призов_ограничено_.layerDepth = 0
	this.Спеши__количество_призов_ограничено_.layerIndex = 11
	this.Спеши__количество_призов_ограничено_.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Спеши__количество_призов_ограничено_).wait(94));

	// phone_png_obj_
	this.phone_png = new lib.Symbol_15_phone_png();
	this.phone_png.name = "phone_png";
	this.phone_png.parent = this;
	this.phone_png.setTransform(22,67.5,1,1,0,0,0,22,67.5);
	this.phone_png.depth = 0;
	this.phone_png.isAttachedToCamera = 0
	this.phone_png.isAttachedToMask = 0
	this.phone_png.layerDepth = 0
	this.phone_png.layerIndex = 12
	this.phone_png.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.phone_png).wait(94));

	// Symbol_13_obj_
	this.Symbol_13 = new lib.Symbol_15_Symbol_13();
	this.Symbol_13.name = "Symbol_13";
	this.Symbol_13.parent = this;
	this.Symbol_13.setTransform(117,103.4,1,1,0,0,0,117,103.4);
	this.Symbol_13.depth = 0;
	this.Symbol_13.isAttachedToCamera = 0
	this.Symbol_13.isAttachedToMask = 0
	this.Symbol_13.layerDepth = 0
	this.Symbol_13.layerIndex = 13
	this.Symbol_13.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Symbol_13).wait(94));

	// pack_png_obj_
	this.pack_png = new lib.Symbol_15_pack_png();
	this.pack_png.name = "pack_png";
	this.pack_png.parent = this;
	this.pack_png.setTransform(109.5,-7,1,1,0,0,0,109.5,-7);
	this.pack_png.depth = 0;
	this.pack_png.isAttachedToCamera = 0
	this.pack_png.isAttachedToMask = 0
	this.pack_png.layerDepth = 0
	this.pack_png.layerIndex = 14
	this.pack_png.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.pack_png).wait(94));

	// shadow_png_obj_
	this.shadow_png = new lib.Symbol_15_shadow_png();
	this.shadow_png.name = "shadow_png";
	this.shadow_png.parent = this;
	this.shadow_png.setTransform(-79.5,196.5,1,1,0,0,0,-79.5,196.5);
	this.shadow_png.depth = 0;
	this.shadow_png.isAttachedToCamera = 0
	this.shadow_png.isAttachedToMask = 0
	this.shadow_png.layerDepth = 0
	this.shadow_png.layerIndex = 15
	this.shadow_png.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.shadow_png).wait(94));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-289,-251.5,622.1,503);


(lib.Scene_1_Layer_12 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_12
	this.instance = new lib.Symbol15();
	this.instance.parent = this;
	this.instance.setTransform(724,385.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Scene_1_Layer_12, null, null);


// stage content:
(lib.OGO1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	this.___GetDepth___ = function(obj) {
		var depth = obj.depth;
		var cameraObj = this.___camera___instance;
		if(cameraObj && cameraObj.depth && obj.isAttachedToCamera)
		{
			depth += depth + cameraObj.depth;
		}
		return depth;
		}
	this.___needSorting___ = function() {
		for (var i = 0; i < this.getNumChildren() - 1; i++)
		{
			var prevDepth = this.___GetDepth___(this.getChildAt(i));
			var nextDepth = this.___GetDepth___(this.getChildAt(i + 1));
			if (prevDepth < nextDepth)
				return true;
		}
		return false;
	}
	this.___sortFunction___ = function(obj1, obj2) {
		return (this.exportRoot.___GetDepth___(obj2) - this.exportRoot.___GetDepth___(obj1));
	}
	this.on('tick', function (event){
		var curTimeline = event.currentTarget;
		if (curTimeline.___needSorting___()){
			this.sortChildren(curTimeline.___sortFunction___);
		}
	});

	// Layer_12_obj_
	this.Layer_12 = new lib.Scene_1_Layer_12();
	this.Layer_12.name = "Layer_12";
	this.Layer_12.parent = this;
	this.Layer_12.setTransform(724,385.5,1,1,0,0,0,724,385.5);
	this.Layer_12.depth = 0;
	this.Layer_12.isAttachedToCamera = 0
	this.Layer_12.isAttachedToMask = 0
	this.Layer_12.layerDepth = 0
	this.Layer_12.layerIndex = 0
	this.Layer_12.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_12).wait(1));

	// Layer_13_obj_
	this.Layer_13 = new lib.Scene_1_Layer_13();
	this.Layer_13.name = "Layer_13";
	this.Layer_13.parent = this;
	this.Layer_13.setTransform(932.5,340,1,1,0,0,0,932.5,340);
	this.Layer_13.depth = 0;
	this.Layer_13.isAttachedToCamera = 0
	this.Layer_13.isAttachedToMask = 0
	this.Layer_13.layerDepth = 0
	this.Layer_13.layerIndex = 1
	this.Layer_13.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_13).wait(1));

	// Layer_2_obj_
	this.Layer_2 = new lib.Scene_1_Layer_2();
	this.Layer_2.name = "Layer_2";
	this.Layer_2.parent = this;
	this.Layer_2.setTransform(540,340,1,1,0,0,0,540,340);
	this.Layer_2.depth = 0;
	this.Layer_2.isAttachedToCamera = 0
	this.Layer_2.isAttachedToMask = 0
	this.Layer_2.layerDepth = 0
	this.Layer_2.layerIndex = 2
	this.Layer_2.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_2).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(540,340,878,340);
// library properties:
lib.properties = {
	id: 'AC48A0D6122B844EA104CA47B513EA5D',
	width: 1080,
	height: 680,
	fps: 30,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: [
		{src:"assets/images/bg_part44.jpg", id:"bg_part"},
		{src:"assets/images/cent44.png", id:"cent"},
		{src:"assets/images/pack44.png", id:"pack"},
		{src:"assets/images/phone44.png", id:"phone"},
		{src:"assets/images/phone_shadow44.png", id:"phone_shadow"},
		{src:"assets/images/shadow44.png", id:"shadow"}
	],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['AC48A0D6122B844EA104CA47B513EA5D'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}


// Layer depth API : 

AdobeAn.Layer = new function() {
	this.getLayerZDepth = function(timeline, layerName)
	{
		if(layerName === "Camera")
		layerName = "___camera___instance";
		var script = "if(timeline." + layerName + ") timeline." + layerName + ".depth; else 0;";
		return eval(script);
	}
	this.setLayerZDepth = function(timeline, layerName, zDepth)
	{
		const MAX_zDepth = 10000;
		const MIN_zDepth = -5000;
		if(zDepth > MAX_zDepth)
			zDepth = MAX_zDepth;
		else if(zDepth < MIN_zDepth)
			zDepth = MIN_zDepth;
		if(layerName === "Camera")
		layerName = "___camera___instance";
		var script = "if(timeline." + layerName + ") timeline." + layerName + ".depth = " + zDepth + ";";
		eval(script);
	}
	this.removeLayer = function(timeline, layerName)
	{
		if(layerName === "Camera")
		layerName = "___camera___instance";
		var script = "if(timeline." + layerName + ") timeline.removeChild(timeline." + layerName + ");";
		eval(script);
	}
	this.addNewLayer = function(timeline, layerName, zDepth)
	{
		if(layerName === "Camera")
		layerName = "___camera___instance";
		zDepth = typeof zDepth !== 'undefined' ? zDepth : 0;
		var layer = new createjs.MovieClip();
		layer.name = layerName;
		layer.depth = zDepth;
		layer.layerIndex = 0;
		timeline.addChild(layer);
	}
}


})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;

$(function () {
    var canvas, stage, exportRoot, anim_container, dom_overlay_container, fnStartAnimation;
    function init() {
        canvas = document.getElementById("canvas44");
        anim_container = document.getElementById("animation_container44");
        dom_overlay_container = document.getElementById("dom_overlay_container44");
        var comp=AdobeAn.getComposition("AC48A0D6122B844EA104CA47B513EA5D");
        var lib=comp.getLibrary();
        createjs.MotionGuidePlugin.install();
        var loader = new createjs.LoadQueue(false);
        loader.addEventListener("fileload", function(evt){handleFileLoad(evt,comp)});
        loader.addEventListener("complete", function(evt){handleComplete(evt,comp)});
        var lib=comp.getLibrary();
        loader.loadManifest(lib.properties.manifest);
    }
    function handleFileLoad(evt, comp) {
        var images=comp.getImages();
        if (evt && (evt.item.type == "image")) { images[evt.item.id] = evt.result; }
    }
    function handleComplete(evt,comp) {
        //This function is always called, irrespective of the content. You can use the variable "stage" after it is created in token create_stage.
        var lib=comp.getLibrary();
        var ss=comp.getSpriteSheet();
        var queue = evt.target;
        var ssMetadata = lib.ssMetadata;
        for(i=0; i<ssMetadata.length; i++) {
            ss[ssMetadata[i].name] = new createjs.SpriteSheet( {"images": [queue.getResult(ssMetadata[i].name)], "frames": ssMetadata[i].frames} )
        }
        var preloaderDiv = document.getElementById("_preload_div_44");
		var elems = document.querySelectorAll("._preload_div_");
		var index = 0, length = elems.length;
		for ( ; index < length; index++) {
			elems[index].style.display = 'none';
		}
        canvas.style.display = 'block';
        exportRoot = new lib.OGO1();
        stage = new lib.Stage(canvas);
        //Registers the "tick" event listener.
        fnStartAnimation = function() {
            stage.addChild(exportRoot);
            createjs.Ticker.setFPS(lib.properties.fps);
            createjs.Ticker.addEventListener("tick", stage)
            stage.addEventListener("tick", handleTick)
            function getProjectionMatrix(container, totalDepth) {
                var focalLength = 528.25;
                var projectionCenter = { x : lib.properties.width/2, y : lib.properties.height/2 };
                var scale = (totalDepth + focalLength)/focalLength;
                var scaleMat = new createjs.Matrix2D;
                scaleMat.a = 1/scale;
                scaleMat.d = 1/scale;
                var projMat = new createjs.Matrix2D;
                projMat.tx = -projectionCenter.x;
                projMat.ty = -projectionCenter.y;
                projMat = projMat.prependMatrix(scaleMat);
                projMat.tx += projectionCenter.x;
                projMat.ty += projectionCenter.y;
                return projMat;
            }
            function handleTick(event) {
                var cameraInstance = exportRoot.___camera___instance;
                if(cameraInstance !== undefined && cameraInstance.pinToObject !== undefined)
                {
                    cameraInstance.x = cameraInstance.pinToObject.x + cameraInstance.pinToObject.pinOffsetX;
                    cameraInstance.y = cameraInstance.pinToObject.y + cameraInstance.pinToObject.pinOffsetY;
                    if(cameraInstance.pinToObject.parent !== undefined && cameraInstance.pinToObject.parent.depth !== undefined)
                        cameraInstance.depth = cameraInstance.pinToObject.parent.depth + cameraInstance.pinToObject.pinOffsetZ;
                }
                applyLayerZDepth(exportRoot);
            }
            function applyLayerZDepth(parent)
            {
                var cameraInstance = parent.___camera___instance;
                var focalLength = 528.25;
                var projectionCenter = { 'x' : 0, 'y' : 0};
                if(parent === exportRoot)
                {
                    var stageCenter = { 'x' : lib.properties.width/2, 'y' : lib.properties.height/2 };
                    projectionCenter.x = stageCenter.x;
                    projectionCenter.y = stageCenter.y;
                }
                for(child in parent.children)
                {
                    var layerObj = parent.children[child];
                    if(layerObj == cameraInstance)
                        continue;
                    applyLayerZDepth(layerObj, cameraInstance);
                    if(layerObj.layerDepth === undefined)
                        continue;
                    if(layerObj.currentFrame != layerObj.parent.currentFrame)
                    {
                        layerObj.gotoAndPlay(layerObj.parent.currentFrame);
                    }
                    var matToApply = new createjs.Matrix2D;
                    var cameraMat = new createjs.Matrix2D;
                    var totalDepth = layerObj.layerDepth ? layerObj.layerDepth : 0;
                    var cameraDepth = 0;
                    if(cameraInstance && !layerObj.isAttachedToCamera)
                    {
                        var mat = cameraInstance.getMatrix();
                        mat.tx -= projectionCenter.x;
                        mat.ty -= projectionCenter.y;
                        cameraMat = mat.invert();
                        cameraMat.prependTransform(projectionCenter.x, projectionCenter.y, 1, 1, 0, 0, 0, 0, 0);
                        cameraMat.appendTransform(-projectionCenter.x, -projectionCenter.y, 1, 1, 0, 0, 0, 0, 0);
                        if(cameraInstance.depth)
                            cameraDepth = cameraInstance.depth;
                    }
                    if(layerObj.depth)
                    {
                        totalDepth = layerObj.depth;
                    }
                    //Offset by camera depth
                    totalDepth -= cameraDepth;
                    if(totalDepth < -focalLength)
                    {
                        matToApply.a = 0;
                        matToApply.d = 0;
                    }
                    else
                    {
                        if(layerObj.layerDepth)
                        {
                            var sizeLockedMat = getProjectionMatrix(parent, layerObj.layerDepth);
                            if(sizeLockedMat)
                            {
                                sizeLockedMat.invert();
                                matToApply.prependMatrix(sizeLockedMat);
                            }
                        }
                        matToApply.prependMatrix(cameraMat);
                        var projMat = getProjectionMatrix(parent, totalDepth);
                        if(projMat)
                        {
                            matToApply.prependMatrix(projMat);
                        }
                    }
                    layerObj.transformMatrix = matToApply;
                }
            }
        }
        //Code to support hidpi screens and responsive scaling.
        function makeResponsive(isResp, respDim, isScale, scaleType) {
            var lastW, lastH, lastS=1;
            window.addEventListener('resize', resizeCanvas);
            resizeCanvas();
            function resizeCanvas() {
                var w = lib.properties.width, h = lib.properties.height;
                var iw = window.innerWidth, ih=window.innerHeight;
                var pRatio = window.devicePixelRatio || 1, xRatio=iw/w, yRatio=ih/h, sRatio=1;
                if(isResp) {
                    if((respDim=='width'&&lastW==iw) || (respDim=='height'&&lastH==ih)) {
                        sRatio = lastS;
                    }
                    else if(!isScale) {
                        if(iw<w || ih<h)
                            sRatio = Math.min(xRatio, yRatio);
                    }
                    else if(scaleType==1) {
                        sRatio = Math.min(xRatio, yRatio);
                    }
                    else if(scaleType==2) {
                        sRatio = Math.max(xRatio, yRatio);
                    }
                }
                canvas.width = w*pRatio*sRatio;
                canvas.height = h*pRatio*sRatio;
                canvas.style.width = anim_container.style.width = dom_overlay_container.style.width = preloaderDiv.style.width = w*sRatio+'px';
                canvas.style.height = anim_container.style.height = dom_overlay_container.style.height = preloaderDiv.style.height = h*sRatio+'px';
                stage.scaleX = pRatio*sRatio;
                stage.scaleY = pRatio*sRatio;
                lastW = iw; lastH = ih; lastS = sRatio;
                stage.tickOnUpdate = false;
                stage.update();
                stage.tickOnUpdate = true;
            }
        }
        makeResponsive(true,'both',true,2);
        AdobeAn.compositionLoaded(lib.properties.id);
        fnStartAnimation();
    }
    init();
});