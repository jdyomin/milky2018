(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [];


// symbols:



(lib.bg_part = function() {
	this.initialize(img.bg_part);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,731,287);


(lib.hand1 = function() {
	this.initialize(img.hand1);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,40,50);


(lib.pack = function() {
	this.initialize(img.pack);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,448,544);// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.Symbol11 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("Ag4A5QgYgYAAghQAAggAYgYQAXgYAhAAQAiAAAXAYQAYAYAAAgQAAAhgYAYQgXAYgiAAQghAAgXgYg");
	this.shape.setTransform(8.1,8.1);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol11, new cjs.Rectangle(0,0,16.2,16.2), null);


(lib.Symbol10 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AifCgQhDhCAAheQAAhdBDhDQBChCBdAAQBeAABCBCQBDBDAABdQAABehDBCQhCBDheAAQhdAAhChDg");
	this.shape.setTransform(22.7,22.7);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol10, new cjs.Rectangle(0,0,45.4,45.4), null);


(lib.Symbol9 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("Ag4A5QgZgYAAghQAAggAZgYQAXgYAhAAQAiAAAXAYQAYAYAAAgQAAAhgYAYQgXAYgiAAQghAAgXgYg");
	this.shape.setTransform(8.2,8.1);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol9, new cjs.Rectangle(0,0,16.3,16.2), null);


(lib.Symbol8 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AifCgQhDhCAAheQAAhdBDhDQBChCBdAAQBeAABCBCQBDBDAABdQAABehDBCQhCBDheAAQhdAAhChDg");
	this.shape.setTransform(22.7,22.7);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol8, new cjs.Rectangle(0,0,45.4,45.4), null);


(lib.Symbol5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Symbol 4
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(14.5,1,1).p("AgyhxQCPBfg7CE");
	this.shape.setTransform(30.1,21.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(14.5,1,1).p("AgwhuQCSBghKB9");
	this.shape_1.setTransform(29.7,21.7);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(14.5,1,1).p("AguhsQCVBjhZB3");
	this.shape_2.setTransform(29.3,21.6);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(14.5,1,1).p("AgshqQCXBmhoBv");
	this.shape_3.setTransform(29,21.5);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(14.5,1,1).p("AgrhoQCbBoh3Bp");
	this.shape_4.setTransform(28.7,21.4);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},23).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},2).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape}]},1).wait(44));
	this.timeline.addTween(cjs.Tween.get(this.shape).wait(23).to({_off:true},1).wait(7).to({_off:false},0).wait(2).to({_off:true},1).wait(7).to({_off:false},0).wait(44));

	// Symbol 3
	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(14.5,1,1).p("AB5gnQiQg/hhCg");
	this.shape_5.setTransform(12.1,15.4);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(14.5,1,1).p("Ah0A7QBaiiCPA/");
	this.shape_6.setTransform(12.4,15.5);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(14.5,1,1).p("AhwA7QBSiiCPA/");
	this.shape_7.setTransform(12.8,15.5);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(14.5,1,1).p("AhtA7QBLijCQA/");
	this.shape_8.setTransform(13.2,15.6);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(14.5,1,1).p("ABqgpQiPg/hECk");
	this.shape_9.setTransform(13.5,15.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_5}]}).to({state:[{t:this.shape_5}]},23).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_5}]},2).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_5}]},1).wait(44));
	this.timeline.addTween(cjs.Tween.get(this.shape_5).wait(23).to({_off:true},1).wait(7).to({_off:false},0).wait(2).to({_off:true},1).wait(7).to({_off:false},0).wait(44));

	// Symbol 2
	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(14.5,1,1).p("AB6AeQhKhoipBL");
	this.shape_10.setTransform(15.2,3.1);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(14.5,1,1).p("Ah1AMQCchbBPBt");
	this.shape_11.setTransform(15.6,3.1);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(14.5,1,1).p("AhyAWQCQhrBVBz");
	this.shape_12.setTransform(15.9,3.1);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(14.5,1,1).p("AhuAfQCDh7BaB5");
	this.shape_13.setTransform(16.3,3.2);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(14.5,1,1).p("ABsAYQhhh/h1CM");
	this.shape_14.setTransform(16.7,3.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_10}]}).to({state:[{t:this.shape_10}]},23).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_10}]},2).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_10}]},1).wait(44));
	this.timeline.addTween(cjs.Tween.get(this.shape_10).wait(23).to({_off:true},1).wait(7).to({_off:false},0).wait(2).to({_off:true},1).wait(7).to({_off:false},0).wait(44));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-7.2,-7.2,49.7,47.6);


(lib.Symbol1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.hand1();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol1, new cjs.Rectangle(0,0,40,50), null);


(lib.Symbol12 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Symbol 6 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("ABrCgQhChCAAheQAAhdBChDQBDhCBdAAQBfAABCBCQBDBDAABdQAABehDBCQhCBDhfAAQhdAAhDhDg");
	mask.setTransform(49.5,22.7);

	// Layer 9
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#B8B8B8").s().p("AifCgQhDhCAAheQAAhdBDhDQBChCBdAAQBeAABCBCQBDBDAABdQAABehDBCQhCBDheAAQhdAAhChDg");
	this.shape.setTransform(79,67.9);

	var maskedShapeInstanceList = [this.shape];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(30).to({y:64},0).wait(1).to({y:60.5},0).wait(1).to({y:57.5},0).wait(1).to({y:54.8},0).wait(1).to({y:52.6},0).wait(1).to({y:50.7},0).wait(1).to({y:49.3},0).wait(1).to({y:48.3},0).wait(1).to({y:47.7},0).wait(1).to({y:47.5},0).wait(26).to({y:49.5},0).wait(1).to({y:51.6},0).wait(1).to({y:53.6},0).wait(1).to({y:55.6},0).wait(1).to({y:57.7},0).wait(1).to({y:59.7},0).wait(1).to({y:61.8},0).wait(1).to({y:63.8},0).wait(1).to({y:65.8},0).wait(1).to({y:67.9},0).wait(11));

	// Symbol 9
	this.instance = new lib.Symbol9();
	this.instance.parent = this;
	this.instance.setTransform(76.6,23.3,1,1,0,0,0,8.2,8.1);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(2).to({x:80.6,y:15.7},7).to({x:76.6,y:9.7},13,cjs.Ease.get(0.5)).wait(7).to({y:23.3},9,cjs.Ease.get(1)).wait(47));

	// Symbol 8
	this.instance_1 = new lib.Symbol8();
	this.instance_1.parent = this;
	this.instance_1.setTransform(76.3,22.7,1,1,0,0,0,22.7,22.7);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(85));

	// Symbol 7 (mask)
	var mask_1 = new cjs.Shape();
	mask_1._off = true;
	mask_1.graphics.p("AifC0QhDhCAAheQAAhdBDhDQBChCBdAAQBeAABCBCQBDBDAABdQAABehDBCQhCBDheAAQhdAAhChDg");
	mask_1.setTransform(22.7,24.7);

	// Layer 8
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#B8B8B8").s().p("AifCgQhDhCAAheQAAhdBDhDQBChCBdAAQBeAABCBCQBDBDAABdQAABehDBCQhCBDheAAQhdAAhChDg");
	this.shape_1.setTransform(25.6,71.8);

	var maskedShapeInstanceList = [this.shape_1];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask_1;
	}

	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(30).to({y:67.9},0).wait(1).to({y:64.5},0).wait(1).to({y:61.4},0).wait(1).to({y:58.8},0).wait(1).to({y:56.5},0).wait(1).to({y:54.7},0).wait(1).to({y:53.3},0).wait(1).to({y:52.2},0).wait(1).to({y:51.6},0).wait(1).to({y:51.4},0).wait(26).to({y:53.5},0).wait(1).to({y:55.5},0).wait(1).to({y:57.5},0).wait(1).to({y:59.6},0).wait(1).to({y:61.6},0).wait(1).to({y:63.7},0).wait(1).to({y:65.7},0).wait(1).to({y:67.7},0).wait(1).to({y:69.8},0).wait(1).to({y:71.8},0).wait(11));

	// Symbol 11
	this.instance_2 = new lib.Symbol11();
	this.instance_2.parent = this;
	this.instance_2.setTransform(25.3,28.2,1,1,0,0,0,8.1,8.1);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(2).to({x:29.3,y:20.6},7).to({x:25.3,y:14.6},13,cjs.Ease.get(0.5)).wait(7).to({y:28.2},9,cjs.Ease.get(1)).wait(47));

	// Symbol 10
	this.instance_3 = new lib.Symbol10();
	this.instance_3.parent = this;
	this.instance_3.setTransform(22.7,26.7,1,1,0,0,0,22.7,22.7);

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(85));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,99,49.4);


// stage content:
(lib.School = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 7
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(15.5,1,1).p("AikAOQDNB5B8jC");
	this.shape.setTransform(827.3,318.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(15.5,1,1).p("AivAGQDLCDCUjD");
	this.shape_1.setTransform(826.6,320.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(15.5,1,1).p("Ai5AAQDKCLCpjD");
	this.shape_2.setTransform(826,321.2);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(15.5,1,1).p("AjCgGQDJCTC8jE");
	this.shape_3.setTransform(825.4,322.3);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(15.5,1,1).p("AjKgMQDJCaDMjE");
	this.shape_4.setTransform(824.9,323.2);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(15.5,1,1).p("AjRgRQDJCgDajE");
	this.shape_5.setTransform(824.5,324);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(15.5,1,1).p("AjXgVQDIClDnjF");
	this.shape_6.setTransform(824.1,324.7);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(15.5,1,1).p("AjbgYQDHCpDwjF");
	this.shape_7.setTransform(823.8,325.3);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(15.5,1,1).p("AjfgbQDHCtD4jG");
	this.shape_8.setTransform(823.6,325.7);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(15.5,1,1).p("AjigdQDHCvD+jG");
	this.shape_9.setTransform(823.4,326.1);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(15.5,1,1).p("AjjgeQDGCwEBjG");
	this.shape_10.setTransform(823.3,326.3);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(15.5,1,1).p("AjkgfQDHCxECjG");
	this.shape_11.setTransform(823.2,326.3);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(15.5,1,1).p("AjZgWQDICnDrjG");
	this.shape_12.setTransform(823.9,325);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(15.5,1,1).p("AjPgPQDJCeDWjF");
	this.shape_13.setTransform(824.6,323.8);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(15.5,1,1).p("AjGgJQDKCWDDjE");
	this.shape_14.setTransform(825.2,322.7);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(15.5,1,1).p("Ai+gDQDKCPCzjE");
	this.shape_15.setTransform(825.7,321.8);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(15.5,1,1).p("Ai3ABQDKCKCljE");
	this.shape_16.setTransform(826.1,321);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(15.5,1,1).p("AixAFQDLCFCYjD");
	this.shape_17.setTransform(826.5,320.3);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#000000").ss(15.5,1,1).p("AitAIQDMCBCPjD");
	this.shape_18.setTransform(826.8,319.7);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#000000").ss(15.5,1,1).p("AipAKQDMB+CHjD");
	this.shape_19.setTransform(827,319.3);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#000000").ss(15.5,1,1).p("AimAMQDNB7CAjC");
	this.shape_20.setTransform(827.2,319);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#000000").ss(15.5,1,1).p("AilANQDNB6B+jC");
	this.shape_21.setTransform(827.3,318.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},9).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_11}]},39).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape}]},1).wait(15));

	// Слой_2
	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#000000").ss(14,1,1).p("AjWBTQA1haBGgqQBAgnBBAHQA+AGAvAsQAwArAUBB");
	this.shape_22.setTransform(857.3,272);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#000000").ss(14,1,1).p("AjWBcQA1hkBGgvQBAgrBBAIQA+AHAvAwQAwAwAUBI");
	this.shape_23.setTransform(857.3,271.2);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#000000").ss(14,1,1).p("AjWBkQA1hsBGgzQBAgvBBAJQA+AHAvA1QAwA0AUBN");
	this.shape_24.setTransform(857.3,270.4);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f().s("#000000").ss(14,1,1).p("AjWBqQA1h0BGg2QBAgxBBAJQA+AHAvA5QAwA3AUBT");
	this.shape_25.setTransform(857.3,269.8);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f().s("#000000").ss(14,1,1).p("AjWBwQA1h6BGg5QBAg0BBAKQA+AIAvA7QAwA6AUBX");
	this.shape_26.setTransform(857.3,269.2);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f().s("#000000").ss(14,1,1).p("AjWB0QA1h+BGg7QBAg2BBAKQA+AIAvA+QAwA8AUBa");
	this.shape_27.setTransform(857.3,268.8);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f().s("#000000").ss(14,1,1).p("AjWB3QA1iBBGg9QBAg3BBAKQA+AIAvBAQAwA+AUBc");
	this.shape_28.setTransform(857.3,268.5);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f().s("#000000").ss(14,1,1).p("AjWB5QA1iEBGg9QBAg5BBALQA+AIAvBBQAwA/AUBd");
	this.shape_29.setTransform(857.3,268.3);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f().s("#000000").ss(14,1,1).p("AjWB5QA1iEBGg+QBAg4BBAKQA+AJAvBAQAwBAAUBe");
	this.shape_30.setTransform(857.3,268.3);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f().s("#000000").ss(14,1,1).p("AjWBwQA1h6BGg5QBAg1BBAKQA+AIAvA8QAwA6AUBX");
	this.shape_31.setTransform(857.3,269.2);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f().s("#000000").ss(14,1,1).p("AjWBpQA1hyBGg2QBAgwBBAJQA+AHAvA4QAwA2AUBR");
	this.shape_32.setTransform(857.3,269.9);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f().s("#000000").ss(14,1,1).p("AjWBiQA1hqBGgzQBAgtBBAIQA+AHAvA0QAwAzAUBM");
	this.shape_33.setTransform(857.3,270.6);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f().s("#000000").ss(14,1,1).p("AjWBdQA1hlBGgvQBAgrBBAIQA+AHAvAxQAwAwAUBI");
	this.shape_34.setTransform(857.3,271.1);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f().s("#000000").ss(14,1,1).p("AjWBYQA1hgBGgtQBAgpBBAIQA+AGAvAvQAwAuAUBE");
	this.shape_35.setTransform(857.3,271.5);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f().s("#000000").ss(14,1,1).p("AjWBVQA1hcBGgsQBAgoBBAIQA+AGAvAtQAwAsAUBD");
	this.shape_36.setTransform(857.3,271.8);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f().s("#000000").ss(14,1,1).p("AjWBUQA1hbBGgrQBAgmBBAHQA+AGAvAsQAwArAUBB");
	this.shape_37.setTransform(857.3,272);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_22}]}).to({state:[{t:this.shape_22}]},9).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_27}]},1).to({state:[{t:this.shape_28}]},1).to({state:[{t:this.shape_29}]},1).to({state:[{t:this.shape_30}]},1).to({state:[{t:this.shape_30}]},42).to({state:[{t:this.shape_31}]},1).to({state:[{t:this.shape_32}]},1).to({state:[{t:this.shape_33}]},1).to({state:[{t:this.shape_34}]},1).to({state:[{t:this.shape_35}]},1).to({state:[{t:this.shape_36}]},1).to({state:[{t:this.shape_37}]},1).to({state:[{t:this.shape_22}]},1).wait(18));

	// Слой_1
	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f().s("#000000").ss(14,1,1).p("AjWBTQA1haBGgqQBAgnBBAHQA+AGAvAsQAwArAUBB");
	this.shape_38.setTransform(777.8,272);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f().s("#000000").ss(14,1,1).p("AjWBcQA1hkBGgvQBAgrBBAIQA+AHAvAwQAwAwAUBI");
	this.shape_39.setTransform(777.8,271.2);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f().s("#000000").ss(14,1,1).p("AjWBkQA1hsBGgzQBAgvBBAJQA+AHAvA1QAwA0AUBN");
	this.shape_40.setTransform(777.8,270.4);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f().s("#000000").ss(14,1,1).p("AjWBqQA1h0BGg2QBAgxBBAJQA+AHAvA5QAwA3AUBT");
	this.shape_41.setTransform(777.8,269.8);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f().s("#000000").ss(14,1,1).p("AjWBwQA1h6BGg5QBAg0BBAKQA+AIAvA7QAwA6AUBX");
	this.shape_42.setTransform(777.8,269.2);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f().s("#000000").ss(14,1,1).p("AjWB0QA1h+BGg7QBAg2BBAKQA+AIAvA+QAwA8AUBa");
	this.shape_43.setTransform(777.8,268.8);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f().s("#000000").ss(14,1,1).p("AjWB3QA1iBBGg9QBAg3BBAKQA+AIAvBAQAwA+AUBc");
	this.shape_44.setTransform(777.8,268.5);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f().s("#000000").ss(14,1,1).p("AjWB5QA1iEBGg9QBAg5BBALQA+AIAvBBQAwA/AUBd");
	this.shape_45.setTransform(777.8,268.3);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.f().s("#000000").ss(14,1,1).p("AjWB5QA1iEBGg+QBAg4BBAKQA+AJAvBAQAwBAAUBe");
	this.shape_46.setTransform(777.8,268.3);

	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.f().s("#000000").ss(14,1,1).p("AjWBwQA1h6BGg5QBAg1BBAKQA+AIAvA8QAwA6AUBX");
	this.shape_47.setTransform(777.8,269.2);

	this.shape_48 = new cjs.Shape();
	this.shape_48.graphics.f().s("#000000").ss(14,1,1).p("AjWBpQA1hyBGg2QBAgwBBAJQA+AHAvA4QAwA2AUBR");
	this.shape_48.setTransform(777.8,269.9);

	this.shape_49 = new cjs.Shape();
	this.shape_49.graphics.f().s("#000000").ss(14,1,1).p("AjWBiQA1hqBGgzQBAgtBBAIQA+AHAvA0QAwAzAUBM");
	this.shape_49.setTransform(777.8,270.6);

	this.shape_50 = new cjs.Shape();
	this.shape_50.graphics.f().s("#000000").ss(14,1,1).p("AjWBdQA1hlBGgvQBAgrBBAIQA+AHAvAxQAwAwAUBI");
	this.shape_50.setTransform(777.8,271.1);

	this.shape_51 = new cjs.Shape();
	this.shape_51.graphics.f().s("#000000").ss(14,1,1).p("AjWBYQA1hgBGgtQBAgpBBAIQA+AGAvAvQAwAuAUBE");
	this.shape_51.setTransform(777.8,271.5);

	this.shape_52 = new cjs.Shape();
	this.shape_52.graphics.f().s("#000000").ss(14,1,1).p("AjWBVQA1hcBGgsQBAgoBBAIQA+AGAvAtQAwAsAUBD");
	this.shape_52.setTransform(777.8,271.8);

	this.shape_53 = new cjs.Shape();
	this.shape_53.graphics.f().s("#000000").ss(14,1,1).p("AjWBUQA1hbBGgrQBAgmBBAHQA+AGAvAsQAwArAUBB");
	this.shape_53.setTransform(777.8,272);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_38}]}).to({state:[{t:this.shape_38}]},9).to({state:[{t:this.shape_39}]},1).to({state:[{t:this.shape_40}]},1).to({state:[{t:this.shape_41}]},1).to({state:[{t:this.shape_42}]},1).to({state:[{t:this.shape_43}]},1).to({state:[{t:this.shape_44}]},1).to({state:[{t:this.shape_45}]},1).to({state:[{t:this.shape_46}]},1).to({state:[{t:this.shape_46}]},42).to({state:[{t:this.shape_47}]},1).to({state:[{t:this.shape_48}]},1).to({state:[{t:this.shape_49}]},1).to({state:[{t:this.shape_50}]},1).to({state:[{t:this.shape_51}]},1).to({state:[{t:this.shape_52}]},1).to({state:[{t:this.shape_53}]},1).to({state:[{t:this.shape_38}]},1).wait(18));

	// Layer 18
	this.instance = new lib.Symbol1();
	this.instance.parent = this;
	this.instance.setTransform(934.3,461.3,1,1,0,0,0,29.3,11.1);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(23).to({y:428.9},14,cjs.Ease.get(1)).wait(33).to({y:461.3},14,cjs.Ease.get(1)).wait(1));

	// Layer 21
	this.instance_1 = new lib.Symbol12();
	this.instance_1.parent = this;
	this.instance_1.setTransform(680.9,422.6,1,1,0,0,0,50.8,37.1);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(29).to({y:414.6},10).wait(25).to({y:422.6},10,cjs.Ease.get(1)).wait(11));

	// Layer 9
	this.shape_54 = new cjs.Shape();
	this.shape_54.graphics.f().s("#000000").ss(12.5,1,1).p("AjGAAQD1AjCZgw");
	this.shape_54.setTransform(680.3,453.4);

	this.shape_55 = new cjs.Shape();
	this.shape_55.graphics.f().s("#000000").ss(12.5,1,1).p("AjGgEQDyAxCcg9");
	this.shape_55.setTransform(680.3,452.6);

	this.shape_56 = new cjs.Shape();
	this.shape_56.graphics.f().s("#000000").ss(12.5,1,1).p("AjGgHQDvA+CfhL");
	this.shape_56.setTransform(680.3,451.7);

	this.shape_57 = new cjs.Shape();
	this.shape_57.graphics.f().s("#000000").ss(12.5,1,1).p("AjGgLQDsBMCihY");
	this.shape_57.setTransform(680.3,450.9);

	this.shape_58 = new cjs.Shape();
	this.shape_58.graphics.f().s("#000000").ss(12.5,1,1).p("AjGgOQDpBaClhm");
	this.shape_58.setTransform(680.3,450);

	this.shape_59 = new cjs.Shape();
	this.shape_59.graphics.f().s("#000000").ss(12.5,1,1).p("AjGgRQDmBnCoh0");
	this.shape_59.setTransform(680.3,449.1);

	this.shape_60 = new cjs.Shape();
	this.shape_60.graphics.f().s("#000000").ss(12.5,1,1).p("AjGgVQDjB1CriB");
	this.shape_60.setTransform(680.3,448.3);

	this.shape_61 = new cjs.Shape();
	this.shape_61.graphics.f().s("#000000").ss(12.5,1,1).p("AjGgYQDgCCCuiP");
	this.shape_61.setTransform(680.3,447.4);

	this.shape_62 = new cjs.Shape();
	this.shape_62.graphics.f().s("#000000").ss(12.5,1,1).p("AjGgcQDdCQCxic");
	this.shape_62.setTransform(680.3,446.6);

	this.shape_63 = new cjs.Shape();
	this.shape_63.graphics.f().s("#000000").ss(12.5,1,1).p("AjGgfQDaCeC0iq");
	this.shape_63.setTransform(680.3,445.7);

	this.shape_64 = new cjs.Shape();
	this.shape_64.graphics.f().s("#000000").ss(12.5,1,1).p("AjGgiQDXCrC3i4");
	this.shape_64.setTransform(680.3,444.8);

	this.shape_65 = new cjs.Shape();
	this.shape_65.graphics.f().s("#000000").ss(12.5,1,1).p("AjGgcQDdCSCxie");
	this.shape_65.setTransform(680.3,446.5);

	this.shape_66 = new cjs.Shape();
	this.shape_66.graphics.f().s("#000000").ss(12.5,1,1).p("AjGgWQDiB6CsiH");
	this.shape_66.setTransform(680.3,447.9);

	this.shape_67 = new cjs.Shape();
	this.shape_67.graphics.f().s("#000000").ss(12.5,1,1).p("AjGgRQDmBmCohz");
	this.shape_67.setTransform(680.3,449.2);

	this.shape_68 = new cjs.Shape();
	this.shape_68.graphics.f().s("#000000").ss(12.5,1,1).p("AjGgNQDqBUCkhg");
	this.shape_68.setTransform(680.3,450.4);

	this.shape_69 = new cjs.Shape();
	this.shape_69.graphics.f().s("#000000").ss(12.5,1,1).p("AjGgJQDuBFCghR");
	this.shape_69.setTransform(680.3,451.3);

	this.shape_70 = new cjs.Shape();
	this.shape_70.graphics.f().s("#000000").ss(12.5,1,1).p("AjGgGQDwA5CehF");
	this.shape_70.setTransform(680.3,452.1);

	this.shape_71 = new cjs.Shape();
	this.shape_71.graphics.f().s("#000000").ss(12.5,1,1).p("AjGgDQDzAvCbg8");
	this.shape_71.setTransform(680.3,452.6);

	this.shape_72 = new cjs.Shape();
	this.shape_72.graphics.f().s("#000000").ss(12.5,1,1).p("AjGgCQD0ApCag1");
	this.shape_72.setTransform(680.3,453.1);

	this.shape_73 = new cjs.Shape();
	this.shape_73.graphics.f().s("#000000").ss(12.5,1,1).p("AjGgBQD1AkCZgw");
	this.shape_73.setTransform(680.3,453.4);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_54}]}).to({state:[{t:this.shape_54}]},29).to({state:[{t:this.shape_55}]},1).to({state:[{t:this.shape_56}]},1).to({state:[{t:this.shape_57}]},1).to({state:[{t:this.shape_58}]},1).to({state:[{t:this.shape_59}]},1).to({state:[{t:this.shape_60}]},1).to({state:[{t:this.shape_61}]},1).to({state:[{t:this.shape_62}]},1).to({state:[{t:this.shape_63}]},1).to({state:[{t:this.shape_64}]},1).to({state:[{t:this.shape_64}]},25).to({state:[{t:this.shape_65}]},1).to({state:[{t:this.shape_66}]},1).to({state:[{t:this.shape_67}]},1).to({state:[{t:this.shape_68}]},1).to({state:[{t:this.shape_69}]},1).to({state:[{t:this.shape_70}]},1).to({state:[{t:this.shape_71}]},1).to({state:[{t:this.shape_72}]},1).to({state:[{t:this.shape_73}]},1).to({state:[{t:this.shape_54}]},1).wait(11));

	// Layer 3
	this.instance_2 = new lib.pack();
	this.instance_2.parent = this;
	this.instance_2.setTransform(541,125);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(85));

	// Layer 4
	this.instance_3 = new lib.Symbol5();
	this.instance_3.parent = this;
	this.instance_3.setTransform(668,206.5,1,1,53.7,0,0,29.7,4.3);

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(9).to({rotation:0,x:642.3,y:279.9},14,cjs.Ease.get(1)).wait(36).to({rotation:53.7,x:668,y:206.5},14,cjs.Ease.get(1)).wait(12));

	// Layer 16
	this.shape_74 = new cjs.Shape();
	this.shape_74.graphics.f().s("#000000").ss(14.5,1,1).p("AGHLYQhWmDhxjZQhxjZhuidQhvichfiGQhgiGg5g1");
	this.shape_74.setTransform(708.7,282.4);

	this.shape_75 = new cjs.Shape();
	this.shape_75.graphics.f().s("#000000").ss(14.5,1,1).p("AmZqlQBBAXBkBvQBlBvByCUQBwCUB1DXQB0DYBeF/");
	this.shape_75.setTransform(706.8,287.3);

	this.shape_76 = new cjs.Shape();
	this.shape_76.graphics.f().s("#000000").ss(14.5,1,1).p("Amsp4QBIgDBqBaQBpBaB0CMQB0CLB4DWQB4DVBlF+");
	this.shape_76.setTransform(705,291.8);

	this.shape_77 = new cjs.Shape();
	this.shape_77.graphics.f().s("#000000").ss(14.5,1,1).p("Am8pKQBPgcBuBHQBtBGB3CEQB1CFB8DTQB7DUBsF8");
	this.shape_77.setTransform(703.3,295.6);

	this.shape_78 = new cjs.Shape();
	this.shape_78.graphics.f().s("#000000").ss(14.5,1,1).p("AnLobQBUgyBzA0QBwA0B6B+QB3B+B/DTQB+DRByF6");
	this.shape_78.setTransform(701.8,298.6);

	this.shape_79 = new cjs.Shape();
	this.shape_79.graphics.f().s("#000000").ss(14.5,1,1).p("AnZntQBahHB3AkQBzAlB8B3QB5B3CCDSQCBDRB3F3");
	this.shape_79.setTransform(700.4,301);

	this.shape_80 = new cjs.Shape();
	this.shape_80.graphics.f().s("#000000").ss(14.5,1,1).p("AnlnCQBfhZB5AWQB3AWB9ByQB8ByCDDQQCEDQB8F2");
	this.shape_80.setTransform(699.2,302.8);

	this.shape_81 = new cjs.Shape();
	this.shape_81.graphics.f().s("#000000").ss(14.5,1,1).p("AnwmaQBjhpB9AJQB5AJB/BuQB9BsCGDQQCFDOCBF1");
	this.shape_81.setTransform(698.1,304.3);

	this.shape_82 = new cjs.Shape();
	this.shape_82.graphics.f().s("#000000").ss(14.5,1,1).p("An5l2QBmh3CAgCQB7gCCBBpQB+BpCIDOQCHDOCEFz");
	this.shape_82.setTransform(697.2,305.5);

	this.shape_83 = new cjs.Shape();
	this.shape_83.graphics.f().s("#000000").ss(14.5,1,1).p("AoBlXQBqiDCBgLQB+gLCBBlQCABlCJDOQCJDNCIFy");
	this.shape_83.setTransform(696.4,306.4);

	this.shape_84 = new cjs.Shape();
	this.shape_84.graphics.f().s("#000000").ss(14.5,1,1).p("AoIk9QBsiMCEgUQB/gSCCBiQCBBjCKDNQCKDLCLFy");
	this.shape_84.setTransform(695.7,307.1);

	this.shape_85 = new cjs.Shape();
	this.shape_85.graphics.f().s("#000000").ss(14.5,1,1).p("AoNkpQBuiTCFgaQCAgYCDBgQCCBgCLDNQCLDLCNFx");
	this.shape_85.setTransform(695.2,307.6);

	this.shape_86 = new cjs.Shape();
	this.shape_86.graphics.f().s("#000000").ss(14.5,1,1).p("AoQkZQBviaCGgdQCBgdCEBfQCCBeCMDNQCLDKCPFx");
	this.shape_86.setTransform(694.9,307.9);

	this.shape_87 = new cjs.Shape();
	this.shape_87.graphics.f().s("#000000").ss(14.5,1,1).p("AoTkRQBxicCGggQCCggCEBeQCCBeCNDMQCMDKCPFx");
	this.shape_87.setTransform(694.6,308.1);

	this.shape_88 = new cjs.Shape();
	this.shape_88.graphics.f().s("#000000").ss(14.5,1,1).p("AIVHVQiQlwiMjLQiMjMiDhdQiEhdiCAgQiGAhhyCd");
	this.shape_88.setTransform(694.6,308.2);

	this.shape_89 = new cjs.Shape();
	this.shape_89.graphics.f().s("#000000").ss(14.5,1,1).p("AoAldQBpiACBgKQB9gJCCBmQCABlCIDOQCJDNCHFz");
	this.shape_89.setTransform(696.5,306.2);

	this.shape_90 = new cjs.Shape();
	this.shape_90.graphics.f().s("#000000").ss(14.5,1,1).p("AnumiQBjhmB7AMQB6AMB+BuQB9BuCFDPQCFDPCAF1");
	this.shape_90.setTransform(698.3,304);

	this.shape_91 = new cjs.Shape();
	this.shape_91.graphics.f().s("#000000").ss(14.5,1,1).p("AndneQBbhNB4AeQB1AgB8B2QB6B1CCDRQCCDQB5F3");
	this.shape_91.setTransform(700,301.7);

	this.shape_92 = new cjs.Shape();
	this.shape_92.graphics.f().s("#000000").ss(14.5,1,1).p("AnOoRQBWg3BzAxQBxAxB6B8QB4B8B/DTQB/DSBzF5");
	this.shape_92.setTransform(701.5,299.1);

	this.shape_93 = new cjs.Shape();
	this.shape_93.graphics.f().s("#000000").ss(14.5,1,1).p("AnAo9QBQgiBwBBQBuBBB3CDQB2CCB9DUQB8DTBuF7");
	this.shape_93.setTransform(702.9,296.5);

	this.shape_94 = new cjs.Shape();
	this.shape_94.graphics.f().s("#000000").ss(14.5,1,1).p("Am0pgQBLgQBtBPQBqBQB2CIQB1CIB6DUQB5DVBpF8");
	this.shape_94.setTransform(704.1,293.9);

	this.shape_95 = new cjs.Shape();
	this.shape_95.graphics.f().s("#000000").ss(14.5,1,1).p("Ampp9QBHAABpBcQBoBcB1CNQByCNB4DWQB4DVBkF+");
	this.shape_95.setTransform(705.2,291.3);

	this.shape_96 = new cjs.Shape();
	this.shape_96.graphics.f().s("#000000").ss(14.5,1,1).p("AmgqVQBEAOBnBnQBlBoBzCRQByCRB2DWQB2DXBgF/");
	this.shape_96.setTransform(706.1,288.9);

	this.shape_97 = new cjs.Shape();
	this.shape_97.graphics.f().s("#000000").ss(14.5,1,1).p("AmYqpQBBAaBkBxQBkBxByCUQBwCVB0DXQB1DXBdGA");
	this.shape_97.setTransform(706.9,286.9);

	this.shape_98 = new cjs.Shape();
	this.shape_98.graphics.f().s("#000000").ss(14.5,1,1).p("AmSq5QA/AjBiB4QBjB5BwCXQBwCXBzDYQBzDZBbGA");
	this.shape_98.setTransform(707.6,285.3);

	this.shape_99 = new cjs.Shape();
	this.shape_99.graphics.f().s("#000000").ss(14.5,1,1).p("AmNrGQA8AqBiB/QBhB+BwCaQBvCZByDZQByDZBZGB");
	this.shape_99.setTransform(708.1,284);

	this.shape_100 = new cjs.Shape();
	this.shape_100.graphics.f().s("#000000").ss(14.5,1,1).p("AmJrPQA7AwBgCDQBgCCBwCbQBuCcByDYQBxDaBXGB");
	this.shape_100.setTransform(708.4,283.1);

	this.shape_101 = new cjs.Shape();
	this.shape_101.graphics.f().s("#000000").ss(14.5,1,1).p("AmHrVQA6A0BgCFQBfCFBwCcQBtCcBxDZQBxDaBXGC");
	this.shape_101.setTransform(708.6,282.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_74}]}).to({state:[{t:this.shape_74}]},9).to({state:[{t:this.shape_75}]},1).to({state:[{t:this.shape_76}]},1).to({state:[{t:this.shape_77}]},1).to({state:[{t:this.shape_78}]},1).to({state:[{t:this.shape_79}]},1).to({state:[{t:this.shape_80}]},1).to({state:[{t:this.shape_81}]},1).to({state:[{t:this.shape_82}]},1).to({state:[{t:this.shape_83}]},1).to({state:[{t:this.shape_84}]},1).to({state:[{t:this.shape_85}]},1).to({state:[{t:this.shape_86}]},1).to({state:[{t:this.shape_87}]},1).to({state:[{t:this.shape_88}]},1).to({state:[{t:this.shape_88}]},36).to({state:[{t:this.shape_89}]},1).to({state:[{t:this.shape_90}]},1).to({state:[{t:this.shape_91}]},1).to({state:[{t:this.shape_92}]},1).to({state:[{t:this.shape_93}]},1).to({state:[{t:this.shape_94}]},1).to({state:[{t:this.shape_95}]},1).to({state:[{t:this.shape_96}]},1).to({state:[{t:this.shape_97}]},1).to({state:[{t:this.shape_98}]},1).to({state:[{t:this.shape_99}]},1).to({state:[{t:this.shape_100}]},1).to({state:[{t:this.shape_101}]},1).to({state:[{t:this.shape_74}]},1).wait(12));

	// Layer 23
	this.shape_102 = new cjs.Shape();
	this.shape_102.graphics.f().s("#000000").ss(14.5,1,1).p("AmMpkQGAAODBC3QDBC2AUD0QAUD0iKCpQiKCojiAV");
	this.shape_102.setTransform(941.3,397.1);

	this.shape_103 = new cjs.Shape();
	this.shape_103.graphics.f().s("#000000").ss(14.5,1,1).p("AmTpQQGAAODICwQDJCxAUDrQASDtiQCjQiRCjjiAU");
	this.shape_103.setTransform(942,394.9);

	this.shape_104 = new cjs.Shape();
	this.shape_104.graphics.f().s("#000000").ss(14.5,1,1).p("AmZo9QF/ANDQCrQDQCqASDlQASDliXCeQiWCdjjAU");
	this.shape_104.setTransform(942.7,392.8);

	this.shape_105 = new cjs.Shape();
	this.shape_105.graphics.f().s("#000000").ss(14.5,1,1).p("AmfosQF/ANDWCmQDXClARDeQARDeicCZQicCZjjAT");
	this.shape_105.setTransform(943.3,390.9);

	this.shape_106 = new cjs.Shape();
	this.shape_106.graphics.f().s("#000000").ss(14.5,1,1).p("AmlocQGAAMDcChQDcChARDYQAQDYihCVQiiCUjiAT");
	this.shape_106.setTransform(943.8,389.2);

	this.shape_107 = new cjs.Shape();
	this.shape_107.graphics.f().s("#000000").ss(14.5,1,1).p("AmpoPQF/AMDiCdQDiCdAPDSQAQDSimCRQimCRjjAT");
	this.shape_107.setTransform(944.3,387.6);

	this.shape_108 = new cjs.Shape();
	this.shape_108.graphics.f().s("#000000").ss(14.5,1,1).p("AmuoCQGAAMDmCZQDnCZAPDNQAPDNiqCOQirCNjiAS");
	this.shape_108.setTransform(944.7,386.2);

	this.shape_109 = new cjs.Shape();
	this.shape_109.graphics.f().s("#000000").ss(14.5,1,1).p("Amyn3QGAALDqCWQDrCWAPDIQAODJiuCLQiuCKjiAR");
	this.shape_109.setTransform(945.1,385);

	this.shape_110 = new cjs.Shape();
	this.shape_110.graphics.f().s("#000000").ss(14.5,1,1).p("Am1ntQF/ALDuCTQDvCTAODEQAODFixCIQixCIjjAR");
	this.shape_110.setTransform(945.5,383.9);

	this.shape_111 = new cjs.Shape();
	this.shape_111.graphics.f().s("#000000").ss(14.5,1,1).p("Am4nlQF/ALDxCQQDyCRAODBQANDCizCFQi0CGjjAR");
	this.shape_111.setTransform(945.8,383);

	this.shape_112 = new cjs.Shape();
	this.shape_112.graphics.f().s("#000000").ss(14.5,1,1).p("Am6neQF/AKD0CPQD0COAOC/QANC/i2CEQi2CDjjAR");
	this.shape_112.setTransform(946,382.2);

	this.shape_113 = new cjs.Shape();
	this.shape_113.graphics.f().s("#000000").ss(14.5,1,1).p("Am8nZQF/ALD2CNQD2CNANC8QANC9i3CCQi4CDjjAQ");
	this.shape_113.setTransform(946.2,381.6);

	this.shape_114 = new cjs.Shape();
	this.shape_114.graphics.f().s("#000000").ss(14.5,1,1).p("Am+nVQGAAKD3CMQD3CMAOC7QAMC7i5CCQi5CBjiAQ");
	this.shape_114.setTransform(946.3,381.2);

	this.shape_115 = new cjs.Shape();
	this.shape_115.graphics.f().s("#000000").ss(14.5,1,1).p("Am+nTQF/AKD4CMQD5CLANC6QAMC6i5CCQi6CAjjAQ");
	this.shape_115.setTransform(946.4,381);

	this.shape_116 = new cjs.Shape();
	this.shape_116.graphics.f().s("#000000").ss(14.5,1,1).p("Am/nSQGAAKD4CLQD5CLANC6QANC6i7CBQi6CAjiAQ");
	this.shape_116.setTransform(946.4,380.9);

	this.shape_117 = new cjs.Shape();
	this.shape_117.graphics.f().s("#000000").ss(14.5,1,1).p("Am3nmQF/ALDxCRQDxCQAODCQANDDizCGQizCFjjAR");
	this.shape_117.setTransform(945.7,383.1);

	this.shape_118 = new cjs.Shape();
	this.shape_118.graphics.f().s("#000000").ss(14.5,1,1).p("Amxn5QF/AMDqCWQDqCXAPDJQAODKitCLQitCLjjAR");
	this.shape_118.setTransform(945.1,385.2);

	this.shape_119 = new cjs.Shape();
	this.shape_119.graphics.f().s("#000000").ss(14.5,1,1).p("AmroKQF/AMDjCbQDkCcAQDQQAPDRioCPQinCQjjAS");
	this.shape_119.setTransform(944.5,387.1);

	this.shape_120 = new cjs.Shape();
	this.shape_120.graphics.f().s("#000000").ss(14.5,1,1).p("AmmoaQGAAMDdChQDeCgAQDWQAQDXiiCUQijCUjiAS");
	this.shape_120.setTransform(943.9,388.9);

	this.shape_121 = new cjs.Shape();
	this.shape_121.graphics.f().s("#000000").ss(14.5,1,1).p("AmhooQGAANDXClQDZCkARDcQARDcieCYQieCYjiAT");
	this.shape_121.setTransform(943.4,390.4);

	this.shape_122 = new cjs.Shape();
	this.shape_122.graphics.f().s("#000000").ss(14.5,1,1).p("Amco0QF/ANDTCoQDUCoASDhQARDhiaCcQiZCbjjAT");
	this.shape_122.setTransform(943,391.8);

	this.shape_123 = new cjs.Shape();
	this.shape_123.graphics.f().s("#000000").ss(14.5,1,1).p("AmYo/QF/ANDPCrQDPCsATDlQARDmiVCeQiWCfjjAT");
	this.shape_123.setTransform(942.6,393.1);

	this.shape_124 = new cjs.Shape();
	this.shape_124.graphics.f().s("#000000").ss(14.5,1,1).p("AmVpJQGAANDLCvQDMCuASDpQASDqiSChQiTChjiAU");
	this.shape_124.setTransform(942.2,394.2);

	this.shape_125 = new cjs.Shape();
	this.shape_125.graphics.f().s("#000000").ss(14.5,1,1).p("AmSpRQF/AODICxQDJCwATDtQATDtiQCjQiQCjjjAU");
	this.shape_125.setTransform(942,395.1);

	this.shape_126 = new cjs.Shape();
	this.shape_126.graphics.f().s("#000000").ss(14.5,1,1).p("AmQpYQGAAODFCzQDGCzAUDvQATDwiOClQiOCljiAU");
	this.shape_126.setTransform(941.7,395.8);

	this.shape_127 = new cjs.Shape();
	this.shape_127.graphics.f().s("#000000").ss(14.5,1,1).p("AmOpdQF/AODEC0QDEC0AUDyQATDxiMCnQiMCmjjAV");
	this.shape_127.setTransform(941.6,396.4);

	this.shape_128 = new cjs.Shape();
	this.shape_128.graphics.f().s("#000000").ss(14.5,1,1).p("AmNphQGAAODCC2QDDC1ATDzQAUDziLCoQiLCnjiAV");
	this.shape_128.setTransform(941.4,396.8);

	this.shape_129 = new cjs.Shape();
	this.shape_129.graphics.f().s("#000000").ss(14.5,1,1).p("AmMpjQGAAODBC2QDCC2AUD0QATD0iKCoQiKCojiAV");
	this.shape_129.setTransform(941.3,397.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_102}]}).to({state:[{t:this.shape_102}]},23).to({state:[{t:this.shape_103}]},1).to({state:[{t:this.shape_104}]},1).to({state:[{t:this.shape_105}]},1).to({state:[{t:this.shape_106}]},1).to({state:[{t:this.shape_107}]},1).to({state:[{t:this.shape_108}]},1).to({state:[{t:this.shape_109}]},1).to({state:[{t:this.shape_110}]},1).to({state:[{t:this.shape_111}]},1).to({state:[{t:this.shape_112}]},1).to({state:[{t:this.shape_113}]},1).to({state:[{t:this.shape_114}]},1).to({state:[{t:this.shape_115}]},1).to({state:[{t:this.shape_116}]},1).to({state:[{t:this.shape_116}]},33).to({state:[{t:this.shape_117}]},1).to({state:[{t:this.shape_118}]},1).to({state:[{t:this.shape_119}]},1).to({state:[{t:this.shape_120}]},1).to({state:[{t:this.shape_121}]},1).to({state:[{t:this.shape_122}]},1).to({state:[{t:this.shape_123}]},1).to({state:[{t:this.shape_124}]},1).to({state:[{t:this.shape_125}]},1).to({state:[{t:this.shape_126}]},1).to({state:[{t:this.shape_127}]},1).to({state:[{t:this.shape_128}]},1).to({state:[{t:this.shape_129}]},1).to({state:[{t:this.shape_102}]},1).wait(1));

	// Layer 2
	this.instance_4 = new lib.bg_part();
	this.instance_4.parent = this;
	this.instance_4.setTransform(349,393);

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(85));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(540,340,1080,680);
// library properties:
lib.properties = {
	id: '354FE720274D074B9AFBA8BCA840E545',
	width: 1080,
	height: 680,
	fps: 30,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: [
		{src:"assets/images/hand122.png", id:"hand1"},
		{src:"assets/images/pack22.png", id:"pack"}
	],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['354FE720274D074B9AFBA8BCA840E545'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}



})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;

$(function () {
    var canvas, stage, exportRoot, anim_container, dom_overlay_container, fnStartAnimation;
    function init() {
        canvas = document.getElementById("canvas22");
        anim_container = document.getElementById("animation_container22");
        dom_overlay_container = document.getElementById("dom_overlay_container22");
        var comp=AdobeAn.getComposition("354FE720274D074B9AFBA8BCA840E545");
        var lib=comp.getLibrary();
        var loader = new createjs.LoadQueue(false);
        loader.addEventListener("fileload", function(evt){handleFileLoad(evt,comp)});
        loader.addEventListener("complete", function(evt){handleComplete(evt,comp)});
        var lib=comp.getLibrary();
        loader.loadManifest(lib.properties.manifest);
    }
    function handleFileLoad(evt, comp) {
        var images=comp.getImages();
        if (evt && (evt.item.type == "image")) { images[evt.item.id] = evt.result; }
    }
    function handleComplete(evt,comp) {
        //This function is always called, irrespective of the content. You can use the variable "stage" after it is created in token create_stage.
        var lib=comp.getLibrary();
        var ss=comp.getSpriteSheet();
        var queue = evt.target;
        var ssMetadata = lib.ssMetadata;
        for(i=0; i<ssMetadata.length; i++) {
            ss[ssMetadata[i].name] = new createjs.SpriteSheet( {"images": [queue.getResult(ssMetadata[i].name)], "frames": ssMetadata[i].frames} )
        }
        exportRoot = new lib.School();
        stage = new lib.Stage(canvas);
        //Registers the "tick" event listener.
        fnStartAnimation = function() {
            stage.addChild(exportRoot);
            createjs.Ticker.setFPS(lib.properties.fps);
            createjs.Ticker.addEventListener("tick", stage);
        }
        //Code to support hidpi screens and responsive scaling.
        function makeResponsive(isResp, respDim, isScale, scaleType) {
            var lastW, lastH, lastS=1;
            window.addEventListener('resize', resizeCanvas);
            resizeCanvas();
            function resizeCanvas() {
                var w = lib.properties.width, h = lib.properties.height;
                var iw = window.innerWidth, ih=window.innerHeight;
                var pRatio = window.devicePixelRatio || 1, xRatio=iw/w, yRatio=ih/h, sRatio=1;
                if(isResp) {
                    if((respDim=='width'&&lastW==iw) || (respDim=='height'&&lastH==ih)) {
                        sRatio = lastS;
                    }
                    else if(!isScale) {
                        if(iw<w || ih<h)
                            sRatio = Math.min(xRatio, yRatio);
                    }
                    else if(scaleType==1) {
                        sRatio = Math.min(xRatio, yRatio);
                    }
                    else if(scaleType==2) {
                        sRatio = Math.max(xRatio, yRatio);
                    }
                }
                canvas.width = w*pRatio*sRatio;
                canvas.height = h*pRatio*sRatio;
                canvas.style.width = dom_overlay_container.style.width = anim_container.style.width =  w*sRatio+'px';
                canvas.style.height = anim_container.style.height = dom_overlay_container.style.height = h*sRatio+'px';
                stage.scaleX = pRatio*sRatio;
                stage.scaleY = pRatio*sRatio;
                lastW = iw; lastH = ih; lastS = sRatio;
                stage.tickOnUpdate = false;
                stage.update();
                stage.tickOnUpdate = true;
            }
        }
        makeResponsive(true,'both',true,2);
        AdobeAn.compositionLoaded(lib.properties.id);
        fnStartAnimation();
    }
    init();
});