(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [];


// symbols:



(lib.bg1 = function() {
	this.initialize(img.bg1);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,1086,686);


(lib.glasses = function() {
	this.initialize(img.glasses);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,235,115);


(lib.glasses_shadow = function() {
	this.initialize(img.glasses_shadow);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,222,128);


(lib.pack = function() {
	this.initialize(img.pack);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,312,510);// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.Symbol14 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_3
	this.shape = new cjs.Shape();
	this.shape.graphics.lf(["rgba(176,212,219,0)","#B2D6DD","#B2D6DD"],[0,0.365,1],0,223.1,0,-223.1).s().p("EhTLAi3MAAAhFtMCmXAAAMAAABFtg");
	this.shape.setTransform(11.05,-393);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	// Layer_2
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.lf(["rgba(176,212,219,0)","#B2D6DD","#B2D6DD"],[0,0.153,1],0,-223.1,0,223.1).s().p("EhTLAi3MAAAhFtMCmXAAAMAAABFtg");
	this.shape_1.setTransform(11.05,474.15);

	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(1));

	// Layer_1
	this.instance = new lib.bg1();
	this.instance.parent = this;
	this.instance.setTransform(-543,-343);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol14, new cjs.Rectangle(-543,-616.1,1086.5,1313.4), null);


(lib.Symbol13 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(13,1,1).p("Ai3lIQBIgMB6AQQB5AQBpAoQBoAnBIA4QBHA3AABAQgLA0hsAjQhrAjjMA0QjLA0iDAyQiDAxhIA/");
	this.shape.setTransform(0,0.0179);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol13, new cjs.Rectangle(-54.9,-39.8,109.8,79.69999999999999), null);


(lib.Symbol12 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.glasses_shadow();
	this.instance.parent = this;
	this.instance.setTransform(-111,-64);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol12, new cjs.Rectangle(-111,-64,222,128), null);


(lib.Symbol11 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.glasses();
	this.instance.parent = this;
	this.instance.setTransform(-117.5,-57.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol11, new cjs.Rectangle(-117.5,-57.5,235,115), null);


(lib.Symbol10 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("EgjbAW9QFM9/S4xTIVqD0IMulfMAMbA0Bg");
	mask.setTransform(-17,118.3);

	// Layer_1
	this.instance = new lib.pack();
	this.instance.parent = this;
	this.instance.setTransform(-156,-255);

	var maskedShapeInstanceList = [this.instance];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol10, new cjs.Rectangle(-156,-48.2,312,303.2), null);


(lib.Symbol8 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.pack();
	this.instance.parent = this;
	this.instance.setTransform(-156,-255);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol8, new cjs.Rectangle(-156,-255,312,510), null);


(lib.Symbol_4_Layer_3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_3
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(13,1,1).p("AnlLMQBxmfBpjAQBqjACfirQCfirBShLQBShLCliM");
	this.shape.setTransform(-12.225,183.025);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(13,1,1).p("AoDIiQCRknByiPQByiPCiiEQCjiFBdhBQBchBCUhz");
	this.shape_1.setTransform(-15.75,164.425);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(13,1,1).p("AohF5QCyiwB6heQB7heClheQCmheBog3QBng4CCha");
	this.shape_2.setTransform(-19.275,145.8);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(13,1,1).p("Ao/DQQDRg5CEgtQCDguCog3QCqg3BygtQByguBxhB");
	this.shape_3.setTransform(-22.8,127.2);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(13,1,1).p("ApcAFQDxA+CLAEQCMAECsgRQCtgRB9gkQB9gjBegp");
	this.shape_4.setTransform(-26.325,111.9246);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(13,1,1).p("AoKi7QCsCmBuBNQBuBMCVAdQCVAdBzgFQBzgFB9AI");
	this.shape_5.setTransform(-18.8,83.6);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(13,1,1).p("AnEl6QBxD+BWCKQBVCKCCBEQCBBDBqAVQBpAVCXAy");
	this.shape_6.setTransform(-12.45,62.475);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(13,1,1).p("AmKoXQBBFGBBC+QBBC8ByBjQBwBjBjArQBiAqCrBU");
	this.shape_7.setTransform(-7.275,45.175);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(13,1,1).p("AleqRQAcF/AxDlQAxDjBlB8QBmB7BbA7QBdA7C8Bv");
	this.shape_8.setTransform(-3.225,31.725);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(13,1,1).p("Ak+roQABGnAmEBQAmD/BcCOQBcCNBYBHQBYBGDICC");
	this.shape_9.setTransform(-0.325,22.125);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(13,1,1).p("AkoscQgQG+AgESQAfERBXCYQBXCYBVBOQBWBNDOCN");
	this.shape_10.setTransform(1.1305,16.35);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(13,1,1).p("AkgstQgVHGAdEYQAdEWBVCbQBVCbBVBRQBVBQDRCQ");
	this.shape_11.setTransform(1.534,14.425);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(13,1,1).p("AkvsLQgLG3AiEMQAhELBZCVQBYCUBWBMQBXBLDMCJ");
	this.shape_12.setTransform(0.6997,18.175);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(13,1,1).p("AlWqlQAUGIAvDrQAvDqBjCAQBjCABbA+QBcA9C+Bz");
	this.shape_13.setTransform(-2.55,29.475);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(13,1,1).p("AmVn6QBJE5BFC0QBFCzB1BeQB0BdBkAmQBjAmCoBO");
	this.shape_14.setTransform(-8.2,48.325);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(13,1,1).p("AnskMQCTDMBkBmQBjBnCOAtQCMAtBvAGQBvAGCHAa");
	this.shape_15.setTransform(-16.15,74.675);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(13,1,1).p("Ao4D1QDKhTCCg4QCBg4Cog/QCohBBwgvQBwgwB0hH");
	this.shape_16.setTransform(-22.025,131.325);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(13,1,1).p("AoaGfQCqjLB5hpQB5hpCkhmQClhmBmg6QBlg6CFhg");
	this.shape_17.setTransform(-18.5,149.925);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#000000").ss(13,1,1).p("AnzKBQCAlqBuirQBtiqCgiaQChiZBXhHQBXhHCdiB");
	this.shape_18.setTransform(-13.8,174.75);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#000000").ss(13,1,1).p("AnpK5QB1mRBri7QBpi6CginQCfinBUhJQBUhKCjiK");
	this.shape_19.setTransform(-12.6,180.95);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_11}]},39).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape}]},1).wait(24));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_3_Layer_2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(13,1,1).p("AAghYQhWA/AgBy");
	this.shape.setTransform(7.9885,4.475);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(13,1,1).p("AgLBNQguhaBVg/");
	this.shape_1.setTransform(8.3265,3.325);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(13,1,1).p("AgCBEQg6hIBWg/");
	this.shape_2.setTransform(8.552,2.425);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(13,1,1).p("AADA+QhAg9BWg+");
	this.shape_3.setTransform(8.6806,1.775);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(13,1,1).p("AAHA6QhFg1BWg+");
	this.shape_4.setTransform(8.7595,1.4);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(13,1,1).p("AAYg4QhWA+BHAz");
	this.shape_5.setTransform(8.7766,1.275);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(13,1,1).p("AgRBUQgnhoBWg/");
	this.shape_6.setTransform(8.1571,3.975);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(13,1,1).p("AgVBYQghhwBVg/");
	this.shape_7.setTransform(8.0324,4.35);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},49).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_5}]},25).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_3_Layer_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(13,1,1).p("ABbg6Qh1hghADw");
	this.shape.setTransform(0.125,0.1597);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(13,1,1).p("AhOBSQAYjfCFBS");
	this.shape_1.setTransform(1.35,0.2245);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(13,1,1).p("AhEBOQgJjQCSBH");
	this.shape_2.setTransform(2.3018,0.2498);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(13,1,1).p("Ag7BMQgfjGCbA/");
	this.shape_3.setTransform(2.743,0.2926);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(13,1,1).p("Ag0BKQgtjACgA7");
	this.shape_4.setTransform(2.9432,0.2946);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(13,1,1).p("AA/g7Qiig5AxC+");
	this.shape_5.setTransform(2.9872,0.2954);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(13,1,1).p("AhVBUQAvjoB8Ba");
	this.shape_6.setTransform(0.675,0.1731);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(13,1,1).p("AhZBWQA8jvB3Bf");
	this.shape_7.setTransform(0.25,0.1631);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},49).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_5}]},25).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_2_Layer_3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_3
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(13,1,1).p("AgpA3QBogHgahl");
	this.shape.setTransform(8.0092,6.95);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(13,1,1).p("AgyAsQBeAPAHho");
	this.shape_1.setTransform(8.9,8.0247);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(13,1,1).p("Ag9AeQBUAmAnhr");
	this.shape_2.setTransform(10.025,9.4212);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(13,1,1).p("AhJAOQBKA8BJht");
	this.shape_3.setTransform(11.175,10.9604);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(13,1,1).p("AhHgCQBIA3BHhJ");
	this.shape_4.setTransform(10.975,12.7481);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(13,1,1).p("AhFgOQBGA0BFgp");
	this.shape_5.setTransform(10.8,13.8707);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(13,1,1).p("AhEgSQBEAxBFgQ");
	this.shape_6.setTransform(10.65,14.3032);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(13,1,1).p("AhDgaQBDAvBEAG");
	this.shape_7.setTransform(10.55,15.075);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(13,1,1).p("AhCghQBCAtBDAW");
	this.shape_8.setTransform(10.45,15.825);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(13,1,1).p("AhBgnQBBAsBCAj");
	this.shape_9.setTransform(10.375,16.375);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(13,1,1).p("AhBgqQBBAsBBAp");
	this.shape_10.setTransform(10.35,16.7);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(13,1,1).p("AhAgrICBBX");
	this.shape_11.setTransform(10.325,16.8);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(13,1,1).p("AhBgoQBCArBAAm");
	this.shape_12.setTransform(10.35,16.525);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(13,1,1).p("AhCggQBCAuBDAT");
	this.shape_13.setTransform(10.45,15.7);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(13,1,1).p("AhDgTQBEAxBDgM");
	this.shape_14.setTransform(10.625,14.4325);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(13,1,1).p("AhGgMQBHA1BGg2");
	this.shape_15.setTransform(10.875,13.7188);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(13,1,1).p("AhAAaQBRAsAwhr");
	this.shape_16.setTransform(10.325,9.7995);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(13,1,1).p("Ag4AlQBZAbAYhp");
	this.shape_17.setTransform(9.475,8.698);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#000000").ss(13,1,1).p("AgvAvQBgAKgBhn");
	this.shape_18.setTransform(8.6011,7.7439);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_11}]},39).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape}]},1).wait(26));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_2_Layer_2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(13,1,1).p("AgfBcQB+idh9ga");
	this.shape.setTransform(4.9336,5.875);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(13,1,1).p("AghBkQB+idhtgq");
	this.shape_1.setTransform(5.1599,5.075);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(13,1,1).p("AgkBsQB+iehbg5");
	this.shape_2.setTransform(5.424,4.3);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(13,1,1).p("AgnB0QB+iehLhJ");
	this.shape_3.setTransform(5.7346,3.5);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(13,1,1).p("AgtB2QB+idgwhO");
	this.shape_4.setTransform(6.3452,3.25);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(13,1,1).p("Ag0B4QB+idgZhS");
	this.shape_5.setTransform(7.0731,3.05);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(13,1,1).p("Ag8B6QB+idgFhW");
	this.shape_6.setTransform(7.8595,2.875);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(13,1,1).p("AhEB7QB+idALhY");
	this.shape_7.setTransform(8.675,2.725);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(13,1,1).p("AhLB8QB+idAZha");
	this.shape_8.setTransform(9.3,2.625);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(13,1,1).p("AhPB9QB+idAhhc");
	this.shape_9.setTransform(9.775,2.55);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(13,1,1).p("AhSB+QB+ieAnhd");
	this.shape_10.setTransform(10.025,2.5);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(13,1,1).p("AhTB+QB+idAphe");
	this.shape_11.setTransform(10.125,2.475);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(13,1,1).p("AhRB9QB+idAlhc");
	this.shape_12.setTransform(9.9,2.525);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(13,1,1).p("AhKB8QB/idAWha");
	this.shape_13.setTransform(9.2,2.65);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(13,1,1).p("Ag+B6QB+idgBhW");
	this.shape_14.setTransform(8.0259,2.85);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(13,1,1).p("AgxB3QB+idgihQ");
	this.shape_15.setTransform(6.7746,3.125);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(13,1,1).p("AglBuQB+iehYg9");
	this.shape_16.setTransform(5.5009,4.1);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(13,1,1).p("AgiBoQB+iehkgx");
	this.shape_17.setTransform(5.2871,4.7);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#000000").ss(13,1,1).p("AghBiQB+idhwgm");
	this.shape_18.setTransform(5.1032,5.275);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_11}]},39).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape}]},1).wait(26));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_2_Layer_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(13,1,1).p("AARBJQBSh7iRgW");
	this.shape.setTransform(-4.3369,5.875);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(13,1,1).p("AgohWQCDAyhSB7");
	this.shape_1.setTransform(-3.5408,4.475);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(13,1,1).p("AABBlQBSh7h1hO");
	this.shape_2.setTransform(-2.7311,3.05);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(13,1,1).p("AgUB4QBRh7hPh1");
	this.shape_3.setTransform(-0.5372,1.15);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(13,1,1).p("AgVB7QBRh7hHh6");
	this.shape_4.setTransform(-0.4293,0.875);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(13,1,1).p("AgWB9QBRh8g/h+");
	this.shape_5.setTransform(-0.3174,0.65);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(13,1,1).p("AgXB/QBRh8g6iB");
	this.shape_6.setTransform(-0.2193,0.475);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(13,1,1).p("AgYCBQBRh8g1iF");
	this.shape_7.setTransform(-0.1308,0.3);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(13,1,1).p("AgYCCQBQh8gxiH");
	this.shape_8.setTransform(-0.0648,0.175);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(13,1,1).p("AgZCDQBRh8gviJ");
	this.shape_9.setTransform(-0.0054,0.1);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(13,1,1).p("AgZCEQBQh9gtiK");
	this.shape_10.setTransform(0.0254,0.05);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(13,1,1).p("AgZCEQBQh9gsiK");
	this.shape_11.setTransform(0.0358,0.025);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(13,1,1).p("AgZCDQBQh8guiJ");
	this.shape_12.setTransform(0.0048,0.075);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(13,1,1).p("AgYCCQBQh8gyiH");
	this.shape_13.setTransform(-0.0744,0.2);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(13,1,1).p("AgXCAQBRh9g5iC");
	this.shape_14.setTransform(-0.2021,0.425);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(13,1,1).p("AgVB9QBQh9hCh8");
	this.shape_15.setTransform(-0.3636,0.75);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(13,1,1).p("AgmhbQB+A7hRB8");
	this.shape_16.setTransform(-3.2821,4);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(13,1,1).p("AgqhSQCHAphRB8");
	this.shape_17.setTransform(-3.798,4.925);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_11}]},39).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape}]},1).wait(26));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_1_Layer_3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_3
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(13,1,1).p("Aj+huQAQBRA7A5QA8A6BiATQBgASBFglQBEgmArhX");
	this.shape.setTransform(27.3,61.5803);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(13,1,1).p("AkGhnQAPBRA6AzQA6AzBjARQBhARBGgdQBFgeA7hk");
	this.shape_1.setTransform(28.175,60.8608);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(13,1,1).p("AkOhhQAPBRA5AtQA4AuBkAQQBjAQBGgXQBFgXBLhv");
	this.shape_2.setTransform(28.95,60.2575);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(13,1,1).p("AkUhcQAPBRA3AoQA4AqBkAPQBkAPBGgRQBGgSBXh4");
	this.shape_3.setTransform(29.575,59.7766);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(13,1,1).p("AkZhYQAPBRA2AlQA3AlBlAOQBkAPBGgNQBHgNBhh/");
	this.shape_4.setTransform(30.075,59.3894);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(13,1,1).p("AkdhVQAPBRA2AhQA1AjBmAOQBlAOBHgKQBGgJBpiF");
	this.shape_5.setTransform(30.45,59.1039);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(13,1,1).p("AkYhZQAPBRA3AmQA2AmBmAOQBkAPBGgOQBGgOBfh9");
	this.shape_6.setTransform(29.95,59.4754);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(13,1,1).p("AkThcQAPBQA4ApQA3AqBlAPQBjAQBGgSQBGgSBVh3");
	this.shape_7.setTransform(29.475,59.8336);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(13,1,1).p("AkPhgQAPBRA4AsQA5AtBkAQQBiAPBGgVQBGgWBNhx");
	this.shape_8.setTransform(29.075,60.1541);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(13,1,1).p("AkMhjQAQBRA5AvQA5AwBkAQQBhAQBGgZQBFgZBHhr");
	this.shape_9.setTransform(28.7,60.453);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(13,1,1).p("AkIhlQAPBQA6AyQA5AyBkARQBhARBGgcQBFgcA/hn");
	this.shape_10.setTransform(28.375,60.7211);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(13,1,1).p("AkGhnQAPBQA7A0QA6A0BjARQBhASBFgfQBFgeA7hj");
	this.shape_11.setTransform(28.1,60.944);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(13,1,1).p("AkDhpQAPBQA7A2QA7A2BiARQBhASBFghQBFggA1hf");
	this.shape_12.setTransform(27.85,61.1313);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(13,1,1).p("AkBhrQAPBRA7A3QA7A3BiASQBhASBGgiQBEgjAxhc");
	this.shape_13.setTransform(27.65,61.2973);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(13,1,1).p("AkAhsQAPBQA8A4QA7A5BjASQBgASBFgjQBEgkAvha");
	this.shape_14.setTransform(27.5,61.4278);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(13,1,1).p("Aj/htQAPBQA8A5QA8A5BiATQBgASBFglQBEgkAthZ");
	this.shape_15.setTransform(27.4,61.5083);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(13,1,1).p("Aj+huQAPBRA8A5QA8A6BiASQBgATBFglQBEgmArhX");
	this.shape_16.setTransform(27.325,61.5583);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},34).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_5}]},20).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape}]},1).wait(14));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_1_Layer_2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(12.5,1,1).p("AjFh/QgQBeAtA/QAsBBBNAYQBLAXBFgkQBFgkAkhm");
	this.shape.setTransform(72.3616,22.3081);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(85));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_1_Layer_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(12.5,1,1).p("Ai8h2QgOBOApA/QAoA/BMAYQBLAXBFgkQBFgkAZhj");
	this.shape.setTransform(0.0313,-0.0109);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(85));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol9 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_3 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("AyzSIMgIogoEMA23AFsMgMKAg4IsaFVg");
	mask.setTransform(4.9,-132.025);

	// Symbol_8
	this.instance = new lib.Symbol8();
	this.instance.parent = this;
	this.instance.setTransform(-24.45,17.55);

	var maskedShapeInstanceList = [this.instance];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol9, new cjs.Rectangle(-170.7,-237.4,302.29999999999995,245.9), null);


(lib.Symbol6 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Symbol12();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(34).to({scaleX:0.9978,scaleY:1.003,skewX:4.4672,skewY:2.07,x:2.25,y:-7.8},5,cjs.Ease.get(1)).to({scaleX:1,scaleY:1,skewX:0,skewY:0,x:0,y:0},8,cjs.Ease.get(1)).wait(38));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-113.4,-75.8,231.4,139.8);


(lib.Symbol5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Symbol11();
	this.instance.parent = this;
	this.instance.setTransform(113.2,-16.1,1,1,0,0,0,113.2,-16.1);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(34).to({regY:-16.2,scaleX:0.9952,scaleY:1.0059,skewX:0.286,skewY:2.9179,x:113.25,y:-16.15},5,cjs.Ease.get(1)).to({regY:-16.1,scaleX:1,scaleY:1,skewX:0,skewY:0,x:113.2,y:-16.1},8,cjs.Ease.get(1)).wait(38));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-117.5,-69.4,235.3,127.60000000000001);


(lib.Symbol3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_84 = function() {
		this.___loopingOver___ = true;
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(84).call(this.frame_84).wait(1));

	// Layer_2_obj_
	this.Layer_2 = new lib.Symbol_3_Layer_2();
	this.Layer_2.name = "Layer_2";
	this.Layer_2.parent = this;
	this.Layer_2.setTransform(8,4.5,1,1,0,0,0,8,4.5);
	this.Layer_2.depth = 0;
	this.Layer_2.isAttachedToCamera = 0
	this.Layer_2.isAttachedToMask = 0
	this.Layer_2.layerDepth = 0
	this.Layer_2.layerIndex = 0
	this.Layer_2.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_2).wait(85));

	// Layer_1_obj_
	this.Layer_1 = new lib.Symbol_3_Layer_1();
	this.Layer_1.name = "Layer_1";
	this.Layer_1.parent = this;
	this.Layer_1.setTransform(0.1,0.1,1,1,0,0,0,0.1,0.1);
	this.Layer_1.depth = 0;
	this.Layer_1.isAttachedToCamera = 0
	this.Layer_1.isAttachedToMask = 0
	this.Layer_1.layerDepth = 0
	this.Layer_1.layerIndex = 1
	this.Layer_1.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_1).wait(85));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-15.5,-15,33.2,34.9);


(lib.Symbol2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_84 = function() {
		this.___loopingOver___ = true;
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(84).call(this.frame_84).wait(1));

	// Layer_3_obj_
	this.Layer_3 = new lib.Symbol_2_Layer_3();
	this.Layer_3.name = "Layer_3";
	this.Layer_3.parent = this;
	this.Layer_3.setTransform(8,7,1,1,0,0,0,8,7);
	this.Layer_3.depth = 0;
	this.Layer_3.isAttachedToCamera = 0
	this.Layer_3.isAttachedToMask = 0
	this.Layer_3.layerDepth = 0
	this.Layer_3.layerIndex = 0
	this.Layer_3.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_3).wait(85));

	// Layer_2_obj_
	this.Layer_2 = new lib.Symbol_2_Layer_2();
	this.Layer_2.name = "Layer_2";
	this.Layer_2.parent = this;
	this.Layer_2.setTransform(4.9,5.9,1,1,0,0,0,4.9,5.9);
	this.Layer_2.depth = 0;
	this.Layer_2.isAttachedToCamera = 0
	this.Layer_2.isAttachedToMask = 0
	this.Layer_2.layerDepth = 0
	this.Layer_2.layerIndex = 1
	this.Layer_2.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_2).wait(85));

	// Layer_1_obj_
	this.Layer_1 = new lib.Symbol_2_Layer_1();
	this.Layer_1.name = "Layer_1";
	this.Layer_1.parent = this;
	this.Layer_1.setTransform(-4.4,5.9,1,1,0,0,0,-4.4,5.9);
	this.Layer_1.depth = 0;
	this.Layer_1.isAttachedToCamera = 0
	this.Layer_1.isAttachedToMask = 0
	this.Layer_1.layerDepth = 0
	this.Layer_1.layerIndex = 2
	this.Layer_1.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_1).wait(85));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-15.5,-19.6,40.6,47.3);


(lib.Symbol1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_84 = function() {
		this.___loopingOver___ = true;
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(84).call(this.frame_84).wait(1));

	// Layer_3_obj_
	this.Layer_3 = new lib.Symbol_1_Layer_3();
	this.Layer_3.name = "Layer_3";
	this.Layer_3.parent = this;
	this.Layer_3.setTransform(27.3,61.6,1,1,0,0,0,27.3,61.6);
	this.Layer_3.depth = 0;
	this.Layer_3.isAttachedToCamera = 0
	this.Layer_3.isAttachedToMask = 0
	this.Layer_3.layerDepth = 0
	this.Layer_3.layerIndex = 0
	this.Layer_3.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_3).wait(85));

	// Layer_2_obj_
	this.Layer_2 = new lib.Symbol_1_Layer_2();
	this.Layer_2.name = "Layer_2";
	this.Layer_2.parent = this;
	this.Layer_2.setTransform(72.4,22.3,1,1,0,0,0,72.4,22.3);
	this.Layer_2.depth = 0;
	this.Layer_2.isAttachedToCamera = 0
	this.Layer_2.isAttachedToMask = 0
	this.Layer_2.layerDepth = 0
	this.Layer_2.layerIndex = 1
	this.Layer_2.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_2).wait(85));

	// Layer_1_obj_
	this.Layer_1 = new lib.Symbol_1_Layer_1();
	this.Layer_1.name = "Layer_1";
	this.Layer_1.parent = this;
	this.Layer_1.depth = 0;
	this.Layer_1.isAttachedToCamera = 0
	this.Layer_1.isAttachedToMask = 0
	this.Layer_1.layerDepth = 0
	this.Layer_1.layerIndex = 2
	this.Layer_1.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_1).wait(85));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-25.5,-18.1,124.3,97.30000000000001);


(lib.Symbol_4_Symbol_2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Symbol_2
	this.instance = new lib.Symbol2();
	this.instance.parent = this;
	this.instance.setTransform(-61.85,255.2,1,1,169.9865,0,0,1.2,17.3);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({rotation:268.7444,x:-86.8,y:112.35},4).to({rotation:360,x:-27.45,y:-65.4},7,cjs.Ease.get(1)).wait(5).to({regX:4.8,regY:4,rotation:364.0728,x:-22.9,y:-78.35},0).wait(1).to({rotation:375.9258,x:-20.3,y:-77.2},0).wait(1).to({regX:1.2,regY:17.3,rotation:379.9986,x:-27.35,y:-65.4},0).wait(1).to({regX:4.8,regY:4,rotation:377.7414,x:-19.9,y:-76.95},0).wait(1).to({rotation:369.189,x:-21.7,y:-77.95},0).wait(1).to({rotation:355.8104,x:-24.75,y:-78.9},0).wait(1).to({rotation:347.258,x:-26.8,y:-79.15},0).wait(1).to({regX:1.2,regY:17.3,rotation:345.0008,x:-27.45,y:-65.4},0).wait(1).to({regX:4.8,regY:4,rotation:347.258,x:-26.8,y:-79.15},0).wait(1).to({rotation:355.8104,x:-24.75,y:-78.9},0).wait(1).to({rotation:369.189,x:-21.7,y:-77.95},0).wait(1).to({rotation:377.7414,x:-19.9,y:-76.95},0).wait(1).to({regX:1.2,regY:17.3,rotation:379.9986,x:-27.35,y:-65.4},0).wait(1).to({regX:4.8,regY:4,rotation:377.7414,x:-19.9,y:-76.95},0).wait(1).to({rotation:369.189,x:-21.7,y:-77.95},0).wait(1).to({rotation:355.8104,x:-24.75,y:-78.9},0).wait(1).to({rotation:347.258,x:-26.8,y:-79.15},0).wait(1).to({regX:1.2,regY:17.3,rotation:345.0008,x:-27.45,y:-65.4},0).wait(1).to({regX:4.8,regY:4,rotation:347.258,x:-26.8,y:-79.15},0).wait(1).to({rotation:355.8104,x:-24.75,y:-78.9},0).wait(1).to({rotation:369.189,x:-21.7,y:-77.95},0).wait(1).to({rotation:377.7414,x:-19.9,y:-76.95},0).wait(1).to({regX:1.2,regY:17.3,rotation:379.9986,x:-27.35,y:-65.4},0).wait(1).to({regX:4.8,regY:4,rotation:377.7414,x:-19.9,y:-76.95},0).wait(1).to({rotation:369.189,x:-21.7,y:-77.95},0).wait(1).to({rotation:355.8104,x:-24.75,y:-78.9},0).wait(1).to({rotation:347.258,x:-26.8,y:-79.15},0).wait(1).to({regX:1.2,regY:17.3,rotation:345.0008,x:-27.45,y:-65.4},0).wait(1).to({regX:4.8,regY:4,rotation:348.0555,x:-26.65,y:-79.15},0).wait(1).to({rotation:356.9454,x:-24.5,y:-78.85},0).wait(1).to({regX:1.2,regY:17.3,rotation:360,x:-27.45,y:-65.4},0).wait(4).to({rotation:268.7444,x:-86.8,y:112.35},5,cjs.Ease.get(-1)).to({rotation:169.9864,x:-60.95,y:254.9},6,cjs.Ease.get(1)).wait(24));

}).prototype = p = new cjs.MovieClip();


(lib.Scene_1_Layer_11 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_11
	this.instance = new lib.Symbol14();
	this.instance.parent = this;
	this.instance.setTransform(47.8,369.75,0.6375,0.6375,0,0,0,0,0.1);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Scene_1_Layer_11, null, null);


(lib.Scene_1_Layer_10 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_10
	this.instance = new lib.Symbol3();
	this.instance.parent = this;
	this.instance.setTransform(216.65,404.75,0.6375,0.6375);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Scene_1_Layer_10, null, null);


(lib.Scene_1_Layer_8 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_8
	this.instance = new lib.Symbol13();
	this.instance.parent = this;
	this.instance.setTransform(253.55,382.2,0.6375,0.6375,0,0,0,0.1,0.1);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Scene_1_Layer_8, null, null);


(lib.Scene_1_Layer_3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_3
	this.instance = new lib.Symbol10();
	this.instance.parent = this;
	this.instance.setTransform(177,386.8,0.6375,0.6375,0,0,0,0,0.1);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Scene_1_Layer_3, null, null);


(lib.Symbol7 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Symbol_5
	this.instance = new lib.Symbol5();
	this.instance.parent = this;
	this.instance.setTransform(-100.5,70.6,1,1,0,0,0,-104.5,75.1);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(34).to({regY:75,scaleX:0.9999,scaleY:0.9999,rotation:2.0465,x:-100.55,y:70.5},5,cjs.Ease.get(1)).to({regY:75.1,scaleX:1,scaleY:1,rotation:0,x:-100.5,y:70.6},8,cjs.Ease.get(1)).wait(38));

	// Symbol_6
	this.instance_1 = new lib.Symbol6();
	this.instance_1.parent = this;
	this.instance_1.setTransform(-100.5,70.6,1,1,0,0,0,-90,70.6);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(34).to({regY:70.5,scaleX:0.9999,scaleY:0.9999,rotation:2.0465,y:70.55},5,cjs.Ease.get(1)).to({regY:70.6,scaleX:1,scaleY:1,rotation:0,y:70.6},8,cjs.Ease.get(1)).wait(38));

	// Symbol_1
	this.instance_2 = new lib.Symbol1();
	this.instance_2.parent = this;
	this.instance_2.setTransform(-100.55,70.65,1,1,0,0,0,-27.7,85);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(34).to({regY:84.9,scaleX:0.9999,scaleY:0.9999,rotation:2.0465,x:-97.1,y:63.6},5,cjs.Ease.get(1)).to({regY:85,scaleX:1,scaleY:1,rotation:0,x:-100.55,y:70.65},8,cjs.Ease.get(1)).wait(38));

	// Layer_6
	this.instance_3 = new lib.Symbol9();
	this.instance_3.parent = this;
	this.instance_3.setTransform(-100.55,70.55,1,1,0,0,0,-114.6,-15.9);

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(34).to({regY:-15.8,rotation:2.1836,x:-100.65,y:70.65},5,cjs.Ease.get(1)).to({regY:-15.9,rotation:0,x:-100.55,y:70.55},8,cjs.Ease.get(1)).wait(38));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-177.4,-188,381.4,556.2);


(lib.Symbol4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_84 = function() {
		this.___loopingOver___ = true;
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(84).call(this.frame_84).wait(1));

	// Symbol_2_obj_
	this.Symbol_2 = new lib.Symbol_4_Symbol_2();
	this.Symbol_2.name = "Symbol_2";
	this.Symbol_2.parent = this;
	this.Symbol_2.setTransform(-60.3,266.6,1,1,0,0,0,-60.3,266.6);
	this.Symbol_2.depth = 0;
	this.Symbol_2.isAttachedToCamera = 0
	this.Symbol_2.isAttachedToMask = 0
	this.Symbol_2.layerDepth = 0
	this.Symbol_2.layerIndex = 0
	this.Symbol_2.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Symbol_2).wait(16).to({regX:-58.3,regY:94.2,x:-58.3,y:94.2},0).wait(2).to({regX:-60.3,regY:266.6,x:-60.3,y:266.6},0).wait(1).to({regX:-58.3,regY:94.2,x:-58.3,y:94.2},0).wait(4).to({regX:-60.3,regY:266.6,x:-60.3,y:266.6},0).wait(1).to({regX:-58.3,regY:94.2,x:-58.3,y:94.2},0).wait(4).to({regX:-60.3,regY:266.6,x:-60.3,y:266.6},0).wait(1).to({regX:-58.3,regY:94.2,x:-58.3,y:94.2},0).wait(4).to({regX:-60.3,regY:266.6,x:-60.3,y:266.6},0).wait(1).to({regX:-58.3,regY:94.2,x:-58.3,y:94.2},0).wait(4).to({regX:-60.3,regY:266.6,x:-60.3,y:266.6},0).wait(1).to({regX:-58.3,regY:94.2,x:-58.3,y:94.2},0).wait(4).to({regX:-60.3,regY:266.6,x:-60.3,y:266.6},0).wait(1).to({regX:-58.3,regY:94.2,x:-58.3,y:94.2},0).wait(2).to({regX:-60.3,regY:266.6,x:-60.3,y:266.6},0).wait(39));

	// Layer_3_obj_
	this.Layer_3 = new lib.Symbol_4_Layer_3();
	this.Layer_3.name = "Layer_3";
	this.Layer_3.parent = this;
	this.Layer_3.setTransform(-12.3,183,1,1,0,0,0,-12.3,183);
	this.Layer_3.depth = 0;
	this.Layer_3.isAttachedToCamera = 0
	this.Layer_3.isAttachedToMask = 0
	this.Layer_3.layerDepth = 0
	this.Layer_3.layerIndex = 1
	this.Layer_3.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_3).wait(85));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-114.1,-95,157,379.3);


(lib.Scene_1_Layer_9 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_9
	this.instance = new lib.Symbol4();
	this.instance.parent = this;
	this.instance.setTransform(108.65,303.4,0.6375,0.6375,0,0,0,0.1,0.1);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Scene_1_Layer_9, null, null);


(lib.Scene_1_Layer_5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_5
	this.instance = new lib.Symbol7();
	this.instance.parent = this;
	this.instance.setTransform(183.7,320.75,0.6375,0.6375,0,0,0,0,0.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Scene_1_Layer_5, null, null);


// stage content:
(lib.Apr_3_vert = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	this.___GetDepth___ = function(obj) {
		var depth = obj.depth;
		var cameraObj = this.___camera___instance;
		if(cameraObj && cameraObj.depth && obj.isAttachedToCamera)
		{
			depth += depth + cameraObj.depth;
		}
		return depth;
		}
	this.___needSorting___ = function() {
		for (var i = 0; i < this.getNumChildren() - 1; i++)
		{
			var prevDepth = this.___GetDepth___(this.getChildAt(i));
			var nextDepth = this.___GetDepth___(this.getChildAt(i + 1));
			if (prevDepth < nextDepth)
				return true;
		}
		return false;
	}
	this.___sortFunction___ = function(obj1, obj2) {
		return (this.exportRoot.___GetDepth___(obj2) - this.exportRoot.___GetDepth___(obj1));
	}
	this.on('tick', function (event){
		var curTimeline = event.currentTarget;
		if (curTimeline.___needSorting___()){
			this.sortChildren(curTimeline.___sortFunction___);
		}
	});

	// Layer_10_obj_
	this.Layer_10 = new lib.Scene_1_Layer_10();
	this.Layer_10.name = "Layer_10";
	this.Layer_10.parent = this;
	this.Layer_10.setTransform(217.3,406.3,1,1,0,0,0,217.3,406.3);
	this.Layer_10.depth = 0;
	this.Layer_10.isAttachedToCamera = 0
	this.Layer_10.isAttachedToMask = 0
	this.Layer_10.layerDepth = 0
	this.Layer_10.layerIndex = 0
	this.Layer_10.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_10).wait(1));

	// Layer_8_obj_
	this.Layer_8 = new lib.Scene_1_Layer_8();
	this.Layer_8.name = "Layer_8";
	this.Layer_8.parent = this;
	this.Layer_8.setTransform(253.5,382.2,1,1,0,0,0,253.5,382.2);
	this.Layer_8.depth = 0;
	this.Layer_8.isAttachedToCamera = 0
	this.Layer_8.isAttachedToMask = 0
	this.Layer_8.layerDepth = 0
	this.Layer_8.layerIndex = 1
	this.Layer_8.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_8).wait(1));

	// Layer_5_obj_
	this.Layer_5 = new lib.Scene_1_Layer_5();
	this.Layer_5.name = "Layer_5";
	this.Layer_5.parent = this;
	this.Layer_5.setTransform(192.7,375.6,1,1,0,0,0,192.7,375.6);
	this.Layer_5.depth = 0;
	this.Layer_5.isAttachedToCamera = 0
	this.Layer_5.isAttachedToMask = 0
	this.Layer_5.layerDepth = 0
	this.Layer_5.layerIndex = 2
	this.Layer_5.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_5).wait(1));

	// Layer_3_obj_
	this.Layer_3 = new lib.Scene_1_Layer_3();
	this.Layer_3.name = "Layer_3";
	this.Layer_3.parent = this;
	this.Layer_3.setTransform(166.2,396.3,1,1,0,0,0,166.2,396.3);
	this.Layer_3.depth = 0;
	this.Layer_3.isAttachedToCamera = 0
	this.Layer_3.isAttachedToMask = 0
	this.Layer_3.layerDepth = 0
	this.Layer_3.layerIndex = 3
	this.Layer_3.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_3).wait(1));

	// Layer_9_obj_
	this.Layer_9 = new lib.Scene_1_Layer_9();
	this.Layer_9.name = "Layer_9";
	this.Layer_9.parent = this;
	this.Layer_9.setTransform(96.8,394.2,1,1,0,0,0,96.8,394.2);
	this.Layer_9.depth = 0;
	this.Layer_9.isAttachedToCamera = 0
	this.Layer_9.isAttachedToMask = 0
	this.Layer_9.layerDepth = 0
	this.Layer_9.layerIndex = 4
	this.Layer_9.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_9).wait(1));

	// Layer_11_obj_
	this.Layer_11 = new lib.Scene_1_Layer_11();
	this.Layer_11.name = "Layer_11";
	this.Layer_11.parent = this;
	this.Layer_11.setTransform(47.9,395.6,1,1,0,0,0,47.9,395.6);
	this.Layer_11.depth = 0;
	this.Layer_11.isAttachedToCamera = 0
	this.Layer_11.isAttachedToMask = 0
	this.Layer_11.layerDepth = 0
	this.Layer_11.layerIndex = 5
	this.Layer_11.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_11).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-110.9,382.9,505.20000000000005,431.4);
// library properties:
lib.properties = {
	id: '915D52BCEAF31E4580F80C1783849BC9',
	width: 375,
	height: 812,
	fps: 24,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: [
		{src:"assets/images/bg141-2.jpg", id:"bg1"},
		{src:"assets/images/glasses41-2.png", id:"glasses"},
		{src:"assets/images/glasses_shadow41-2.png", id:"glasses_shadow"},
		{src:"assets/images/pack41-2.png", id:"pack"}
	],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['915D52BCEAF31E4580F80C1783849BC9'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}


// Layer depth API : 

AdobeAn.Layer = new function() {
	this.getLayerZDepth = function(timeline, layerName)
	{
		if(layerName === "Camera")
		layerName = "___camera___instance";
		var script = "if(timeline." + layerName + ") timeline." + layerName + ".depth; else 0;";
		return eval(script);
	}
	this.setLayerZDepth = function(timeline, layerName, zDepth)
	{
		const MAX_zDepth = 10000;
		const MIN_zDepth = -5000;
		if(zDepth > MAX_zDepth)
			zDepth = MAX_zDepth;
		else if(zDepth < MIN_zDepth)
			zDepth = MIN_zDepth;
		if(layerName === "Camera")
		layerName = "___camera___instance";
		var script = "if(timeline." + layerName + ") timeline." + layerName + ".depth = " + zDepth + ";";
		eval(script);
	}
	this.removeLayer = function(timeline, layerName)
	{
		if(layerName === "Camera")
		layerName = "___camera___instance";
		var script = "if(timeline." + layerName + ") timeline.removeChild(timeline." + layerName + ");";
		eval(script);
	}
	this.addNewLayer = function(timeline, layerName, zDepth)
	{
		if(layerName === "Camera")
		layerName = "___camera___instance";
		zDepth = typeof zDepth !== 'undefined' ? zDepth : 0;
		var layer = new createjs.MovieClip();
		layer.name = layerName;
		layer.depth = zDepth;
		layer.layerIndex = 0;
		timeline.addChild(layer);
	}
}


})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;

$(function () {
    var canvas, stage, exportRoot, anim_container, dom_overlay_container, fnStartAnimation;
    function init() {
        canvas = document.getElementById("canvas41-2");
        anim_container = document.getElementById("animation_container41-2");
        dom_overlay_container = document.getElementById("dom_overlay_container41-2");
        var comp=AdobeAn.getComposition("915D52BCEAF31E4580F80C1783849BC9");
        var lib=comp.getLibrary();
        var loader = new createjs.LoadQueue(false);
        loader.addEventListener("fileload", function(evt){handleFileLoad(evt,comp)});
        loader.addEventListener("complete", function(evt){handleComplete(evt,comp)});
        var lib=comp.getLibrary();
        loader.loadManifest(lib.properties.manifest);
    }
    function handleFileLoad(evt, comp) {
        var images=comp.getImages();
        if (evt && (evt.item.type == "image")) { images[evt.item.id] = evt.result; }
    }
    function handleComplete(evt,comp) {
        //This function is always called, irrespective of the content. You can use the variable "stage" after it is created in token create_stage.
        var lib=comp.getLibrary();
        var ss=comp.getSpriteSheet();
        var queue = evt.target;
        var ssMetadata = lib.ssMetadata;
        for(i=0; i<ssMetadata.length; i++) {
            ss[ssMetadata[i].name] = new createjs.SpriteSheet( {"images": [queue.getResult(ssMetadata[i].name)], "frames": ssMetadata[i].frames} )
        }
        var preloaderDiv = document.getElementById("_preload_div_41");
        preloaderDiv.style.display = 'none';
        canvas.style.display = 'block';
        exportRoot = new lib.Apr_3_vert();
        stage = new lib.Stage(canvas);
        //Registers the "tick" event listener.
        fnStartAnimation = function() {
            stage.addChild(exportRoot);
            createjs.Ticker.setFPS(lib.properties.fps);
            createjs.Ticker.addEventListener("tick", stage)
            stage.addEventListener("tick", handleTick)
            function getProjectionMatrix(container, totalDepth) {
                var focalLength = 528.25;
                var projectionCenter = { x : lib.properties.width/2, y : lib.properties.height/2 };
                var scale = (totalDepth + focalLength)/focalLength;
                var scaleMat = new createjs.Matrix2D;
                scaleMat.a = 1/scale;
                scaleMat.d = 1/scale;
                var projMat = new createjs.Matrix2D;
                projMat.tx = -projectionCenter.x;
                projMat.ty = -projectionCenter.y;
                projMat = projMat.prependMatrix(scaleMat);
                projMat.tx += projectionCenter.x;
                projMat.ty += projectionCenter.y;
                return projMat;
            }
            function handleTick(event) {
                var cameraInstance = exportRoot.___camera___instance;
                if(cameraInstance !== undefined && cameraInstance.pinToObject !== undefined)
                {
                    cameraInstance.x = cameraInstance.pinToObject.x + cameraInstance.pinToObject.pinOffsetX;
                    cameraInstance.y = cameraInstance.pinToObject.y + cameraInstance.pinToObject.pinOffsetY;
                    if(cameraInstance.pinToObject.parent !== undefined && cameraInstance.pinToObject.parent.depth !== undefined)
                        cameraInstance.depth = cameraInstance.pinToObject.parent.depth + cameraInstance.pinToObject.pinOffsetZ;
                }
                applyLayerZDepth(exportRoot);
            }
            function applyLayerZDepth(parent)
            {
                var cameraInstance = parent.___camera___instance;
                var focalLength = 528.25;
                var projectionCenter = { 'x' : 0, 'y' : 0};
                if(parent === exportRoot)
                {
                    var stageCenter = { 'x' : lib.properties.width/2, 'y' : lib.properties.height/2 };
                    projectionCenter.x = stageCenter.x;
                    projectionCenter.y = stageCenter.y;
                }
                for(child in parent.children)
                {
                    var layerObj = parent.children[child];
                    if(layerObj == cameraInstance)
                        continue;
                    applyLayerZDepth(layerObj, cameraInstance);
                    if(layerObj.layerDepth === undefined)
                        continue;
                    if(layerObj.currentFrame != layerObj.parent.currentFrame)
                    {
                        layerObj.gotoAndPlay(layerObj.parent.currentFrame);
                    }
                    var matToApply = new createjs.Matrix2D;
                    var cameraMat = new createjs.Matrix2D;
                    var totalDepth = layerObj.layerDepth ? layerObj.layerDepth : 0;
                    var cameraDepth = 0;
                    if(cameraInstance && !layerObj.isAttachedToCamera)
                    {
                        var mat = cameraInstance.getMatrix();
                        mat.tx -= projectionCenter.x;
                        mat.ty -= projectionCenter.y;
                        cameraMat = mat.invert();
                        cameraMat.prependTransform(projectionCenter.x, projectionCenter.y, 1, 1, 0, 0, 0, 0, 0);
                        cameraMat.appendTransform(-projectionCenter.x, -projectionCenter.y, 1, 1, 0, 0, 0, 0, 0);
                        if(cameraInstance.depth)
                            cameraDepth = cameraInstance.depth;
                    }
                    if(layerObj.depth)
                    {
                        totalDepth = layerObj.depth;
                    }
                    //Offset by camera depth
                    totalDepth -= cameraDepth;
                    if(totalDepth < -focalLength)
                    {
                        matToApply.a = 0;
                        matToApply.d = 0;
                    }
                    else
                    {
                        if(layerObj.layerDepth)
                        {
                            var sizeLockedMat = getProjectionMatrix(parent, layerObj.layerDepth);
                            if(sizeLockedMat)
                            {
                                sizeLockedMat.invert();
                                matToApply.prependMatrix(sizeLockedMat);
                            }
                        }
                        matToApply.prependMatrix(cameraMat);
                        var projMat = getProjectionMatrix(parent, totalDepth);
                        if(projMat)
                        {
                            matToApply.prependMatrix(projMat);
                        }
                    }
                    layerObj.transformMatrix = matToApply;
                }
            }
        }
        //Code to support hidpi screens and responsive scaling.
        function makeResponsive(isResp, respDim, isScale, scaleType) {
            var lastW, lastH, lastS=1;
            window.addEventListener('resize', resizeCanvas);
            resizeCanvas();
            function resizeCanvas() {
                var w = lib.properties.width, h = lib.properties.height;
                var iw = window.innerWidth, ih=window.innerHeight;
                var pRatio = window.devicePixelRatio || 1, xRatio=iw/w, yRatio=ih/h, sRatio=1;
                if(isResp) {
                    if((respDim=='width'&&lastW==iw) || (respDim=='height'&&lastH==ih)) {
                        sRatio = lastS;
                    }
                    else if(!isScale) {
                        if(iw<w || ih<h)
                            sRatio = Math.min(xRatio, yRatio);
                    }
                    else if(scaleType==1) {
                        sRatio = Math.min(xRatio, yRatio);
                    }
                    else if(scaleType==2) {
                        sRatio = Math.max(xRatio, yRatio);
                    }
                }
                canvas.width = w*pRatio*sRatio;
                canvas.height = h*pRatio*sRatio;
                canvas.style.width = anim_container.style.width = dom_overlay_container.style.width = preloaderDiv.style.width = w*sRatio+'px';
                canvas.style.height = anim_container.style.height = dom_overlay_container.style.height = preloaderDiv.style.height = h*sRatio+'px';
                stage.scaleX = pRatio*sRatio;
                stage.scaleY = pRatio*sRatio;
                lastW = iw; lastH = ih; lastS = sRatio;
                stage.tickOnUpdate = false;
                stage.update();
                stage.tickOnUpdate = true;
            }
        }
        makeResponsive(true,'both',true,2);
        AdobeAn.compositionLoaded(lib.properties.id);
        fnStartAnimation();
    }
    init();
});