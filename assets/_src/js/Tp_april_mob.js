(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [];


// symbols:



(lib.bg = function() {
	this.initialize(img.bg);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,320,454);


(lib.D = function() {
	this.initialize(img.D);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,28,80);


(lib.pack_1 = function() {
	this.initialize(img.pack_1);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,97,220);


(lib.pack_3 = function() {
	this.initialize(img.pack_3);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,148,262);


(lib.pack_4 = function() {
	this.initialize(img.pack_4);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,43,278);


(lib.pack_part1 = function() {
	this.initialize(img.pack_part1);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,25,56);


(lib.pack_part2 = function() {
	this.initialize(img.pack_part2);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,53,102);


(lib.pack_part2_shadow = function() {
	this.initialize(img.pack_part2_shadow);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,40,90);


(lib.plashka1 = function() {
	this.initialize(img.plashka1);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,304,102);// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.Symbol28 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.pack_3();
	this.instance.parent = this;
	this.instance.setTransform(-74,-131);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol28, new cjs.Rectangle(-74,-131,148,262), null);


(lib.Symbol27 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.pack_1();
	this.instance.parent = this;
	this.instance.setTransform(-48.5,-110);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol27, new cjs.Rectangle(-48.5,-110,97,220), null);


(lib.Symbol26 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.pack_4();
	this.instance.parent = this;
	this.instance.setTransform(-21.5,-139);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol26, new cjs.Rectangle(-21.5,-139,43,278), null);


(lib.Symbol16 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.plashka1();
	this.instance.parent = this;
	this.instance.setTransform(-152,-51);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol16, new cjs.Rectangle(-152,-51,304,102), null);


(lib.Symbol5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.pack_part2_shadow();
	this.instance.parent = this;
	this.instance.setTransform(-20,-45);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol5, new cjs.Rectangle(-20,-45,40,90), null);


(lib.Symbol4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.pack_part2();
	this.instance.parent = this;
	this.instance.setTransform(-26.5,-51);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol4, new cjs.Rectangle(-26.5,-51,53,102), null);


(lib.Symbol3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("AjMDaICzn5IDmAAIgOHRIibBug");
	mask.setTransform(-3.975,-0.975);

	// Layer_1
	this.instance = new lib.pack_part1();
	this.instance.parent = this;
	this.instance.setTransform(-12.5,-28);

	var maskedShapeInstanceList = [this.instance];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol3, new cjs.Rectangle(-12.5,-28,25,55.9), null);


(lib.Symbol_25_Layer_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#3DACE5").s().p("Ah8FOQgPgXAGhbQACgdAJg2IAPhWQACgWAch1IAdiEQAMg/AIgXQASgwAXARQALAHANBHIAgDWQAVCEAUBVQAgB+gIAZQgpAZgQgkIgWhcIgiifIAAAAIAAAAQgJACgSAAQgZAAgEABQgQA1gJBSQgQCKgEAAQgOAGgKAAQgNAAgHgJgAgNhSQgEAQgKA5QAdAAAFgDQAAhZgNgGIAAAAQgBAAgGAZg");
	this.shape.setTransform(129.5764,3.243);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#3DACE5").s().p("AgNFVQgegOAEh3QABgRgCgaIAAgoQAAjHAMi+QgCALg8gPQgmgLADgdQABghBdgDQATgCBnAFQAaAFAHAgQAIAegPAAQgMAFgigFQgdgDAAAHQgMBOABCjQACDEgEBCIAAAnQABAbgCAOQgFAhgUAAQgHAAgJgFg");
	this.shape_1.setTransform(102.7813,2.8677);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#3DACE5").s().p("AAJFaQg3gBgcgtQgUgigJhxIgIikQgEjKAPgwQAWhJA+gLQAkgFAkAYQAXANAOAvQALAdgYAPQgXAPgIgRQgTgqgWgGQgegKgKAxQgFAVgCA7QgEDUANB1QAOB9AkgHQAhgGADgTQAFgUAVgGQAMgEAKAMQAJAJAAAHQABAHgTAeQgcAqg4AAIgCAAg");
	this.shape_2.setTransform(76.4371,3.408);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#3DACE5").s().p("AhCEnQgQjAgKjZQgBgjgJhYQgGhDAfgQQAFgCBpgSQAagEANALQAMAMgKATQgNAVgmABQgtACgQAMQAAAlAIBNQAHBNgBAnQAwAJgFAZQgCAQgiAMQADAkAGBeQAFBTAFAtQA7gQAmATQAJAGgHASQgGAUgKAAQg9AHg+AEQgcAAAAgvg");
	this.shape_3.setTransform(50.2755,3.2828);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#3DACE5").s().p("AA8FRQgBgCAAhNQAAkHALinQgHAEgjA/QgOAXgGAFQgEACgGgCQgcgFgVhHQgGgOgEgEIgBA0IgBAqQAABHgFBhIgBBbQABA7gCAaQgEBGgOAGQgUAJgIgQQgFgOgCh7IgCguQgBgeABgSQAFhNACh2IADjuQAFgZAlAGQAKAAAOArIAKAiIAHAXQAHAOADAPQAUAsACADQgDgEBNiNQAKgSACgBQAZgOAOAVQADACgDBJQgIDygBFKQAAAYgaAFQgFABgFAAQgMAAgIgMg");
	this.shape_4.setTransform(22.875,2.446);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#3DACE5").s().p("AhQEpQgTgegCgVQgEggACgUQACgkAAhIQAChVACgjIAAh1QAAhyASggQAVglAegGQBLgNAnA4QARAbAABAIABCRQABBDgEA8IgFA7QgEAnAAAaQgEBRgTAfQgYAmghADIgNAAQg0AAgbgugAgKkYQgdALgFAsQgDAPADBLQABAyAABRIgCB8IgDA3QgCAhACARQAFAyAvACQAXAAADgwIACgoQABgYACgJQAeifgSjNQgDgigLgYQgFgIgNgGQgHgDgHAAQgFAAgGADg");
	this.shape_5.setTransform(-28.5967,2.9386);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#3DACE5").s().p("AgoFSQgOgOgIg5QgEgggZnBIgFgnQgDgXAAgMQgBgiAYgIIAcABQAPAAALgCQA1gLAVgBQAtgBADAZQAEAdg1ALQgQADgdACIgjAEQgCANAHBoQAHBpALBkQALB4ACApQAAALADAZIACAkQABAqgNAHQgPAKgLAAQgJAAgFgGg");
	this.shape_6.setTransform(-52.4116,2.8524);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#3DACE5").s().p("AhQEpQgTgegCgVQgEggACgUQACgkAAhIQAChVACgjIAAh1QAAhyASggQAVglAegGQBLgNAnA4QARAbAABAIABCRQABBDgEA8IgFA7QgEAnAAAaQgEBRgTAfQgYAmghADIgNAAQg0AAgbgugAgKkYQgdALgFAsQgDAPADBLQABAyAABRIgCB8IgDA3QgCAhACARQAFAyAvACQAXAAADgwIACgoQABgYACgJQAeifgSjNQgDgigLgYQgFgIgNgGQgHgDgHAAQgFAAgGADg");
	this.shape_7.setTransform(-78.9467,2.9386);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#3DACE5").s().p("AhSFaQgTABgBhDIAAg+IACg9IABlyQAAhXABgGQAEgPAJgJQAMgKARAHQAOAGAACCQgCCFAFAFQAAAEAngDQAjgBACgCQAEgHgDhzQgFh2AIgUQAKgZAOAAQAXAAgBAKQAPFeACEmQgBAagRAKQgTAFgPgKQgHgRgBgxQACiIgJiAQgIgMhCAOQgPAAAMDlQAEBQgLAVQgEAGgTAAIgMgBg");
	this.shape_8.setTransform(-104.5,2.6711);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#3DACE5").s().p("AA8FRQgBgCAAhNQAAkHALinQgHAEgjA/QgOAXgGAFQgEACgGgCQgcgFgVhHQgGgOgEgEIgBA0IgBAqQAABHgFBhIgBBbQABA7gCAaQgEBGgOAGQgUAJgIgQQgFgOgCh7IgCguQgBgeABgSQAFhNACh2IADjuQAFgZAlAGQAKAAAOArIAKAiIAHAXQAHAOADAPQAUAsACADQgDgEBNiNQAKgSACgBQAZgOAOAVQADACgDBJQgIDygBFKQAAAYgaAFQgFABgFAAQgMAAgIgMg");
	this.shape_9.setTransform(-133.125,2.446);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol_25_Layer_1, null, null);


(lib.Symbol_24_Layer_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgNFVQgegOAEh3QABgRgCgaIAAgoQAAjHAMi+QgCALg8gPQgmgLADgdQABghBdgDQATgCBnAFQAaAFAHAgQAIAegPAAQgMAFgigFQgdgDAAAHQgMBOABCjQACDEgEBCIAAAnQABAbgCAOQgFAhgUAAQgHAAgJgFg");
	this.shape.setTransform(108.0313,2.8677);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("Ag9FkQgQjAgKjZQgBgjgJhYQgGhDAfgQQAGgCBogSQAagEANALQANAMgLATQgNAVgmABQgsACgRAMQAAAlAIBNQAHBNgBAmQAxAJgGAZQgCARgiAMQAEAkAFBeQAEBTAGAtQA7gQAmATQAKAGgHASQgGAUgLAAQg8AHg/AEQgcAAAAgvgAhRkdQgZgIgDgYQgDggAVgWQAOgOATAGQAUAEAHATIAFAVQgCAZgNAPQgMAMgQAAQgGAAgGgCgAAMk5QgSgCgGgPQgQggAVgXQAKgQAMgBQAWgEAMAXQAGAJAAAWQACARgQAPQgIAHgNAAIgIAAg");
	this.shape_1.setTransform(81.2655,-2.8296);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AA8FRQgBgCAAhNQAAkHALinQgHAEgjA/QgOAXgGAFQgEACgGgCQgcgFgVhHQgGgOgEgEIgBA0IgBAqQAABHgFBhIgBBbQABA7gCAaQgEBGgOAGQgUAJgIgQQgFgOgCh7IgCguQgBgeABgSQAFhNACh2IADjuQAFgZAlAGQAKAAAOArIAKAiIAHAXQAHAOADAPQAUAsACADQgDgEBNiNQAKgSACgBQAZgOAOAVQADACgDBJQgIDygBFKQAAAYgaAFQgFABgFAAQgMAAgIgMg");
	this.shape_2.setTransform(53.975,2.446);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AhFGbQgRAAgIgPQgJgmAChsQABgrgCiQQgCh+ADg9IABiGQADgeAUAAQAeABAIAxQADATgCBJIgDE3IANgwQA1iSABhgQAAgNAIgqIAIgyQAGgpAVgHQAVgIADAWQALBFgBB7QgBCpABAlQACAqgEBKQgEBSABAbQgBA2gegIQgRAGgJgXQgIgVABgeIAQk0QgvEPgkBZQgQAWgSAAIgCAAgAgQk1IgQgNQgHgDgJgOQgDgGgEgDIgKgOQgQgUAFgRQAEgRARAJQAGANAGAGIAJAKQAGAIAIAHIAHADQANAHAMAAIAHABQAAAAABAAQAAgBAAAAQABAAAAgBQABAAABAAIAEgCQADgBAFgDIASgNQAEgBAFgKIAJgQQAagFgLAaQgBAIgCAGQgIAWgFAIIgKAKQgDAFgBAAQgEAAgEAEQgFAFgCAAQgCAAgGAGQgKAHgLAAQgMAAgQgLg");
	this.shape_3.setTransform(24.4325,-2.2896);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("Ah8FOQgPgXAGhbQACgdAJg2IAPhWQACgWAch1IAdiEQAMg/AIgXQASgwAXARQALAHANBHIAgDWQAVCEAUBVQAgB+gIAZQgpAZgQgkIgWhcIgiifIAAAAIAAAAQgJACgSAAQgZAAgEABQgQA1gJBSQgQCKgEAAQgOAGgKAAQgNAAgHgJgAgNhSQgEAQgKA5QAdAAAFgDQAAhZgNgGIAAAAQgBAAgGAZg");
	this.shape_4.setTransform(-3.3736,3.243);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AhWEzQgNgLACgWQADgdARgKQARgIAPANIAUATQAJAKAGAEQAPAJAegNQAVgKAHhEQACgkgFglQgEgxgOgKQgIgIgfAIQghAGgOgFQgQgHgKgRQgLgVAJgVQACgDAxgfQAmgcAOgrQAchOgMguQgLgjgjAAQggAAgaA4QgDAFgEAUQgFATgFAJQgNAegagQQgXgNACghIAJgaQAchvBagOQAvgHAvAzQAlAogGBcIgRBOQgCALgSAZQgPAXAAAKQAAAKAQAPQASASACAEQAvBZgUB0QgIAzgaAfQgcAcguAFIgMAAQg3AAgogng");
	this.shape_5.setTransform(-33.8723,2.2843);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AhCEnQgQjAgKjZQgBgjgJhYQgGhDAfgQQAFgCBpgSQAagEANALQAMAMgKATQgNAVgmABQgtACgQAMQAAAlAIBNQAHBNgBAnQAwAJgFAZQgCAQgiAMQADAkAGBeQAFBTAFAtQA7gQAmATQAJAGgHASQgGAUgKAAQg9AHg+AEQgcAAAAgvg");
	this.shape_6.setTransform(-84.1745,3.2828);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AhSFaQgSABgChDIABg+IACg9IAAlyQAAhXABgGQAEgPAIgJQAMgKATAHQANAGgBCCQgBCFAGAFQAAAEAmgDQAjgBACgCQAEgHgDhzQgFh2AIgUQAKgZAOAAQAWAAABAKQAOFeACEmQAAAagSAKQgTAFgOgKQgJgRAAgxQABiIgIiAQgIgMhBAOQgQAAALDlQAFBQgLAVQgEAGgTAAIgMgBg");
	this.shape_7.setTransform(-109.3,2.6711);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol_24_Layer_1, null, null);


(lib.Symbol_23_Layer_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#3DACE5").s().p("Ah8FOQgPgXAGhbQACgdAJg2IAPhWQACgWAch1IAdiEQAMg/AIgXQASgwAXARQALAHANBHIAgDWQAVCEAUBVQAgB+gIAZQgpAZgQgkIgWhcIgiifIAAAAIAAAAQgJACgSAAQgZAAgEABQgQA1gJBSQgQCKgEAAQgOAGgKAAQgNAAgHgJgAgNhSQgEAQgKA5QAdAAAFgDQAAhZgNgGIAAAAQgBAAgGAZg");
	this.shape.setTransform(117.3764,3.243);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#3DACE5").s().p("AhSFaQgTABgBhDIAAg+IACg9IABlyQAAhXABgGQAEgPAJgJQAMgKARAHQAOAGAACCQgCCFAFAFQABAEAmgDQAjgBACgCQAEgHgDhzQgFh2AIgUQAKgZAOAAQAXAAgBAKQAPFeACEmQgBAagRAKQgTAFgPgKQgHgRgBgxQACiIgJiAQgIgMhCAOQgPAAAMDlQAEBQgLAVQgEAGgTAAIgMgBg");
	this.shape_1.setTransform(90.8,2.6711);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#3DACE5").s().p("AhQEpQgTgegCgVQgEggACgUQACgkAAhIQAChVACgjIAAh1QAAhyASggQAVglAegGQBLgNAnA4QARAbAABAIABCRQABBDgEA8IgFA7QgEAnAAAaQgEBRgTAfQgYAmghADIgNAAQg0AAgbgugAgKkYQgdALgFAsQgDAPADBLQABAyAABRIgCB8IgDA3QgCAhACARQAFAyAvACQAXAAADgwIACgoQABgYACgJQAeifgSjNQgDgigLgYQgFgIgNgGQgHgDgHAAQgFAAgGADg");
	this.shape_2.setTransform(63.3533,2.9386);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#3DACE5").s().p("AgQFhQgigFgRgMQgVgPgBhOIgKjtIgChFIgChPQAAgRgFhBIgGhXQgBgjAZgHIAcABQAUAAABAzQABA0ACAZIAFCYQBigqAkByQAUBugKBYQgRB/g4AWQgOAHgVAAIgTgBgAgcgWQgEACAAADQABAaABA/IAECMQADBPALAFQAQAEAUgWQARgVADgSQAShkgShoQgJhAgjAAQgMAAgQAHg");
	this.shape_3.setTransform(13.9036,2.1448);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#3DACE5").s().p("AhfFoQgSgEgDgpIAAokQAAh4AKgCQARgKBJAHQBNAHAeAVQATAOAGA/QAGBFgqAsQgOAPg2AGQgvAEAAAFQARAAggFdQgBALAAAfQAAAfgCANQgFAlgXAAQgGAAgIgCgAg5kpQgGAGAKBrQAAALAwgCQAwgBAOgQQAKgLABgkQACgkgKgMQgNgSg1gCIgMAAQgqAAADAKg");
	this.shape_4.setTransform(-13.2932,1.7993);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#3DACE5").s().p("AhCEnQgQjAgKjZQgBgjgJhYQgGhDAfgQQAFgCBpgSQAagEANALQAMAMgKATQgNAVgmABQgtACgQAMQAAAlAIBNQAHBNgBAnQAwAJgFAZQgCAQgiAMQADAkAGBeQAFBTAFAtQA7gQAmATQAJAGgHASQgGAUgKAAQg9AHg+AEQgcAAAAgvg");
	this.shape_5.setTransform(-40.7745,3.2828);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#3DACE5").s().p("AhUFTQgQgCABg1IADgvIACgvQAAjagFhAIgCg9QgDgxABgfQABhkAbAAIA+gGQA4gEAgABQAeAAgIBNIgBAkQAABMAECaIAEDsQADBKgUAUQgNAPgWgLQgLgGAAgRQADhHgGiQQgHiUAChCQAFh6gBgiIgLABQg+gCACAJQgGBSAGCsQAHCrgHBWQgFBhgfAAQgHAAgHgEg");
	this.shape_6.setTransform(-66.6004,3.3765);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#3DACE5").s().p("AhCEnQgQjAgKjZQgBgjgJhYQgGhDAfgQQAFgCBpgSQAagEANALQAMAMgKATQgNAVgmABQgtACgQAMQAAAlAIBNQAHBNgBAnQAwAJgFAZQgCAQgiAMQADAkAGBeQAFBTAFAtQA7gQAmATQAJAGgHASQgGAUgKAAQg9AHg+AEQgcAAAAgvg");
	this.shape_7.setTransform(-92.8745,3.2828);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#3DACE5").s().p("AgNFVQgegOAEh3QABgRgCgaIAAgoQAAjHAMi+QgCALg8gPQgmgLADgdQABghBdgDQATgCBnAFQAaAFAHAgQAIAegPAAQgMAFgigFQgdgDAAAHQgMBOABCjQACDEgEBCIAAAnQABAbgCAOQgFAhgUAAQgHAAgJgFg");
	this.shape_8.setTransform(-118.6187,2.8677);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol_23_Layer_1, null, null);


(lib.Symbol_22_Layer_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#3DACE5").s().p("AhfFoQgSgEgDgpIAAokQAAh4AKgCQARgKBJAHQBNAHAeAVQATAOAGA/QAGBFgqAsQgOAPg2AGQgvAEAAAFQARAAggFdIgBAqQAAAfgCANQgFAlgXAAQgGAAgIgCgAg5kpQgGAGAKBrQAAALAwgCQAwgBAOgQQAKgLABgkQACgkgKgMQgNgSg1gCIgMAAQgqAAADAKg");
	this.shape.setTransform(260.9568,1.7993);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#3DACE5").s().p("AhCEnQgQjAgKjZQgBgjgJhYQgGhDAfgQQAFgCBpgSQAagEANALQAMAMgKATQgNAVgmABQgtACgQAMQAAAlAIBNQAHBNgBAnQAwAJgFAZQgCAQgiAMQADAkAGBeQAFBTAFAtQA7gQAmATQAJAGgHASQgGAUgKAAQg9AHg+AEQgcAAAAgvg");
	this.shape_1.setTransform(234.0755,3.2828);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#3DACE5").s().p("AhSFaQgTABgChDIABg+IADg9IABlyQAAhXABgGQADgPAIgJQAMgKATAHQANAGgBCCQAACFAEAFQAAAEAngDQAjgBACgCQAFgHgEhzQgFh2AIgUQAKgZAOAAQAWAAABAKQAPFeAAEmQABAagTAKQgSAFgOgKQgJgRABgxQABiIgJiAQgHgMhCAOQgRAAAMDlQAFBQgLAVQgEAGgTAAIgMgBg");
	this.shape_2.setTransform(209.55,2.6711);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#3DACE5").s().p("AhSFaQgSABgDhDIABg+IADg9IABlyQAAhXABgGQACgPAJgJQANgKARAHQAOAGAACCQgBCFAEAFQAAAEAngDQAjgBACgCQAFgHgEhzQgFh2AIgUQAKgZAOAAQAWAAAAAKQAQFeAAEmQABAagTAKQgSAFgOgKQgIgRAAgxQABiIgJiAQgHgMhCAOQgRAAAMDlQAFBQgLAVQgEAGgTAAIgMgBg");
	this.shape_3.setTransform(183.8,2.6711);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#3DACE5").s().p("Ah8FOQgPgXAGhbQACgdAJg2IAPhWQACgWAch1IAdiEQAMg/AIgXQASgwAXARQALAHANBHIAgDWQAVCEAUBVQAgB+gIAZQgpAZgQgkIgWhcIgiifIAAAAIAAAAQgJACgSAAQgZAAgEABQgQA1gJBSQgQCKgEAAQgOAGgKAAQgNAAgHgJgAgNhSQgEAQgKA5QAdAAAFgDQAAhZgNgGIAAAAQgBAAgGAZg");
	this.shape_4.setTransform(156.6264,3.243);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#3DACE5").s().p("AgRFnQghgEgRgNQgVgPgChNIgJjuIgChFIgBhOQAAgSgGhBIgGhWQgBgjAYgHIAcAAQAOAAAMgBQA1gMAVgBQAtgCADAaQAEAeg1AKIgsAGIgjADQgBAGACATIAEAsIAFCZQBigqAkByQAUBtgJBYQgSB/g3AXQgOAHgWAAIgUgCgAgcgPQgFABAAAEQACAZABA/IAECMQADBQALAEQAQAFAUgXQAQgUAEgTQAShkgShoQgJhAgjAAQgMAAgQAIg");
	this.shape_5.setTransform(129.6651,1.4626);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#3DACE5").s().p("AgNFVQgegOAEh3QABgRgCgaIAAgoQAAjHAMi+QgCALg8gPQgmgLADgdQABghBdgDQATgCBnAFQAaAFAHAgQAIAegPAAQgMAFgigFQgdgDAAAHQgMBOABCjQACDEgEBCIAAAnQABAbgCAOQgFAhgUAAQgHAAgJgFg");
	this.shape_6.setTransform(80.9313,2.8677);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#3DACE5").s().p("AhQEpQgTgegCgVQgEggACgUQACgkAAhIQAChVACgjIAAh1QAAhyASggQAVglAegGQBLgNAnA4QARAbAABAIABCRQABBDgEA8IgFA7QgEAnAAAaQgEBRgTAfQgYAmghADIgNAAQg0AAgbgugAgKkYQgdALgFAsQgDAPADBLQABAyAABRIgCB8IgDA3QgCAhACARQAFAyAvACQAXAAADgwIACgoQABgYACgJQAeifgSjNQgDgigLgYQgFgIgNgGQgHgDgHAAQgFAAgGADg");
	this.shape_7.setTransform(53.2533,2.9386);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#3DACE5").s().p("AgNFVQgegOAEh3QABgRgCgaIAAgoQAAjHAMi+QgCALg8gPQgmgLADgdQABghBdgDQATgCBnAFQAaAFAHAgQAIAegPAAQgMAFgigFQgdgDAAAHQgMBOABCjQACDEgEBCIAAAnQABAbgCAOQgFAhgUAAQgHAAgJgFg");
	this.shape_8.setTransform(27.2813,2.8677);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#3DACE5").s().p("AhDFHQgXgNgNgvQgKgeAYgPQAXgOAIARQARAqAWAHQAfAIALgxQAGgVADg7QADg9ABg1QgGADgJAAQgTABglAAIgIAAQgFgCgFAAQgJAAgOgWIAAgLIABgJQAGgWARAAQAKAAAPADIATABIAugFQAAhZgFg+QgJh+gmAIQggAGgFASQgFAUgVAHQgLAEgKgMQgJgLAAgFQgBgIAUgdQAfgrA4ABQA4AAAZAtQAUAjAGBwIACCkQgCDLgQAvQgYBKg/AKIgOABQgdAAgbgTg");
	this.shape_9.setTransform(-1.1524,3.4329);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#3DACE5").s().p("Ah3FOQgEhfAMjTQANjWgDhVQgCgfAFgNQAEgSAOgIQARgJAQAOQACAaABAyIADBPQASgOAbg+QAcg/ATgQQAJgIAPgCQASgCAEAMQAGASgbAxQhHCXgZAuQAtBQBFC7QAiBUgIAeQgDAOgRAFQgRAFgKgIQgTgLgJgiQgjiAg5iNQgKA8AAEDQAAAQgfABIgEAAQgbAAAAgNg");
	this.shape_10.setTransform(-49.5057,3.314);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#3DACE5").s().p("Ah8FOQgPgXAGhbQACgdAJg2IAPhWQACgWAch1IAdiEQAMg/AIgXQASgwAXARQALAHANBHIAgDWQAVCEAUBVQAgB+gIAZQgpAZgQgkIgWhcIgiifIAAAAIAAAAQgJACgSAAQgZAAgEABQgQA1gJBSQgQCKgEAAQgOAGgKAAQgNAAgHgJgAgNhSQgEAQgKA5QAdAAAFgDQAAhZgNgGIAAAAQgBAAgGAZg");
	this.shape_11.setTransform(-78.5736,3.243);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#3DACE5").s().p("Ah3FOQgEhfAMjTQANjWgDhVQgCgfAFgNQAEgSAOgIQARgJAQAOQACAaABAyIADBPQASgOAbg+QAcg/ATgQQAJgIAPgCQASgCAEAMQAGASgbAxQhHCXgZAuQAtBQBFC7QAiBUgIAeQgDAOgRAFQgRAFgKgIQgTgLgJgiQgjiAg5iNQgKA8AAEDQAAAQgfABIgEAAQgbAAAAgNg");
	this.shape_12.setTransform(-104.5057,3.314);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#3DACE5").s().p("AhQEpQgTgegCgVQgEggACgUQACgkAAhIQAChVACgjIAAh1QAAhyASggQAVglAegGQBLgNAnA4QARAbAABAIABCRQABBDgEA8IgFA7QgEAnAAAaQgEBRgTAfQgYAmghADIgNAAQg0AAgbgugAgKkYQgdALgFAsQgDAPADBLQABAyAABRIgCB8IgDA3QgCAhACARQAFAyAvACQAXAAADgwIACgoQABgYACgJQAeifgSjNQgDgigLgYQgFgIgNgGQgHgDgHAAQgFAAgGADg");
	this.shape_13.setTransform(-154.9467,2.9386);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#3DACE5").s().p("AA8FRQgBgCAAhNQAAkHALinQgHAEgjA/QgOAXgGAFQgEACgGgCQgcgFgVhHQgGgOgEgEIgBA0IgBAqQAABHgFBhIgBBbQABA7gCAaQgEBGgOAGQgUAJgIgQQgFgOgCh7IgCguQgBgeABgSQAFhNACh2IADjuQAFgZAlAGQAKAAAOArIAKAiIAHAXQAHAOADAPQAUAsACADQgDgEBNiNQAKgSACgBQAZgOAOAVQADACgDBJQgIDygBFKQAAAYgaAFQgFABgFAAQgMAAgIgMg");
	this.shape_14.setTransform(-182.975,2.446);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#3DACE5").s().p("ABZFVQgcgTAAg6IgDibQgChmABg3QghgFgZAlQgqA3AAB4QgLCmACgDQgLAYgTgDQgRgDgJgSQgBgCAAhHQgDiFANhCQAHgpAzhMIg0grQgfgegGgvQgHg0AcgsQAcguBdgNQBXgLAMASQASgEgBBeIgCBTIgCBMQAAA2AGCUQAECEgCBOQgLAVgOAAQgJAAgJgGgAALkTQg5AOgNARQgKAMAEAkQAFAiAKALQAOAQAxADQAvAFAAgLQAOiEgKgHQACgGgLAAQgNAAgfAIg");
	this.shape_15.setTransform(-215.4064,2.7799);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#3DACE5").s().p("AhfFoQgSgEgDgpIAAokQAAh4AKgCQARgKBJAHQBNAHAeAVQATAOAGA/QAGBFgqAsQgOAPg2AGQgvAEAAAFQARAAggFdIgBAqQAAAfgCANQgFAlgXAAQgGAAgIgCgAg5kpQgGAGAKBrQAAALAwgCQAwgBAOgQQAKgLABgkQACgkgKgMQgNgSg1gCIgMAAQgqAAADAKg");
	this.shape_16.setTransform(-243.5932,1.7993);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#3DACE5").s().p("AhUFTQgQgCACg1IACgvIACgvQAAjagEhAIgDg9QgDgxABgfQABhkAbAAIA+gGQA4gEAgABQAeAAgIBNIgBAkQAABMAECaIAEDsQACBKgTAUQgMAPgXgLQgLgGAAgRQADhHgGiQQgHiUAChCQAFh6gBgiIgLABQg+gCACAJQgGBSAGCsQAHCrgHBWQgFBhgfAAQgHAAgHgEg");
	this.shape_17.setTransform(-272.1004,3.3765);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol_22_Layer_1, null, null);


(lib.Symbol_21_Layer_2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.instance = new lib.D();
	this.instance.parent = this;
	this.instance.setTransform(-89.05,-31.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol_21_Layer_2, null, null);


(lib.Symbol_21_Layer_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AiCAeQgGgCgEAAQgLAAgPgQIAAgIIAAgGQAHgRAUAAQAKAAARACIAVABIB6gIQAmgBAHgBQAZgDAZAAQARgBAPALQAKAFgBAMQAAAGgFAFQgNAKgMAAQg+AAgOgBQgZgCgnAGQgGACgMACQgCAAgIgEQgHAHgUAAIg/ABg");
	this.shape.setTransform(229.05,10.2957);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AhCEnQgQjAgKjZQgBgjgJhYQgGhDAfgQQAFgCBpgSQAagEANALQAMAMgKATQgNAVgmABQgtACgQAMQAAAlAIBNQAHBNgBAnQAwAJgFAZQgCAQgiAMQADAkAGBeQAFBTAFAtQA7gQAmATQAJAGgHASQgGAUgKAAQg9AHg+AEQgcAAAAgvg");
	this.shape_1.setTransform(176.1255,3.2828);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AhCEnQgQjAgKjZQgBgjgJhYQgGhDAfgQQAFgCBpgSQAagEANALQAMAMgKATQgNAVgmABQgtACgQAMQAAAlAIBNQAHBNgBAnQAwAJgFAZQgCAQgiAMQADAkAGBeQAFBTAFAtQA7gQAmATQAJAGgHASQgGAUgKAAQg9AHg+AEQgcAAAAgvg");
	this.shape_2.setTransform(151.9255,3.2828);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AhCEnQgQjAgKjZQgBgjgJhYQgGhDAfgQQAFgCBpgSQAagEANALQAMAMgKATQgNAVgmABQgtACgQAMQAAAlAIBNQAHBNgBAnQAwAJgFAZQgCAQgiAMQADAkAGBeQAFBTAFAtQA7gQAmATQAJAGgHASQgGAUgKAAQg9AHg+AEQgcAAAAgvg");
	this.shape_3.setTransform(105.4255,3.2828);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AgNFVQgegOAEh3QABgRgCgaIAAgoQAAjHAMi+QgCALg8gPQgmgLADgdQABghBdgDQATgCBnAFQAaAFAHAgQAIAegPAAQgMAFgigFQgdgDAAAHQgMBOABCjQACDEgEBCIAAAnQABAbgCAOQgFAhgUAAQgHAAgJgFg");
	this.shape_4.setTransform(79.6813,2.8677);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AhFGbQgRAAgIgPQgJgmAChsQABgrgCiQQgCh+ADg9IABiGQADgeAUAAQAeABAIAxQADATgCBJIgDE3IANgwQA1iSABhgQAAgNAIgqIAIgyQAGgpAVgHQAVgIADAWQALBFgBB7QgBCpABAlQACAqgEBKQgEBSABAbQgBA2gegIQgRAGgJgXQgIgVABgeIAQk0QgvEPgkBZQgQAWgSAAIgCAAgAgQk1IgQgNQgHgDgJgOQgDgGgEgDIgKgOQgQgUAFgRQAEgRARAJQAGANAGAGIAJAKQAGAIAIAHIAHADQANAHAMAAIAHABQAAAAABAAQAAgBAAAAQABAAAAgBQABAAABAAIAEgCQADgBAFgDIASgNQAEgBAFgKIAJgQQAagFgLAaQgBAIgCAGQgIAWgFAIIgKAKQgDAFgBAAQgEAAgEAEQgFAFgCAAQgCAAgGAGQgKAHgLAAQgMAAgQgLg");
	this.shape_5.setTransform(51.8325,-2.2896);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("Ah8FOQgPgXAGhbQACgdAJg2IAPhWQACgWAch1IAdiEQAMg/AIgXQASgwAXARQALAHANBHIAgDWQAVCEAUBVQAgB+gIAZQgpAZgQgkIgWhcIgiifIAAAAIAAAAQgJACgSAAQgZAAgEABQgQA1gJBSQgQCKgEAAQgOAGgKAAQgNAAgHgJgAgNhSQgEAQgKA5QAdAAAFgDQAAhZgNgGIAAAAQgBAAgGAZg");
	this.shape_6.setTransform(24.0264,3.243);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AgTFhQgggFgRgMQgUgPgDhOIgJjtIgChFIgBhPQAAgRgGhBQgEgyADgbQAFgrA/gIQA8gIApAjQAeAcAJAVQAWAwgRAzQgQAvgqAdQAiA6ALAuQAeBrgJBYQgSB/g3AWQgPAHgWAAIgUgBgAgjhDIAECLIAECMQADBPALAFQAQAEATgWQARgVAEgSQAShkgShoQgUhlg1gHQgFACAAAEgAgqktQgBAbAEAyIAEBQQApADAZgeQAZgdgIgsQgFgdgagRQgSgMgXAAQgJAAgJABg");
	this.shape_7.setTransform(-3.9591,2.1029);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AhCFlQghgFgRgMQgUgPgChOIgKjtIgChFIgBhPQAAgRgGhBIgGhXQgBgjAZgHIAcABQAUAAABAzIADBNIAFCYQBigqAlByQATBugJBYQgSB/g3AWQgOAHgVAAIgVgBgAhNgSQgEACAAADQACAaABA/IADCMQADBPALAFQARAEAUgWQAQgVAEgSQARhkgRhoQgJhAgkAAQgMAAgQAHgAB3FcQgVgWgChPIgEj3QgDiigEhQIgCglQgCgWABgLQACghAWgKQAZgMAIAzIANDAQAGBFgBCcQgBCYAHBLQABAQgLAHQgJAFgHAAQgLAAgHgIg");
	this.shape_8.setTransform(-38.9978,1.7305);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("Ah8FOQgPgXAGhbQACgdAJg2IAPhWQACgWAch1IAdiEQAMg/AIgXQASgwAXARQALAHANBHIAgDWQAVCEAUBVQAgB+gIAZQgpAZgQgkIgWhcIgiifIAAAAIAAAAQgJACgSAAQgZAAgEABQgQA1gJBSQgQCKgEAAQgOAGgKAAQgNAAgHgJgAgNhSQgEAQgKA5QAdAAAFgDQAAhZgNgGIAAAAQgBAAgGAZg");
	this.shape_9.setTransform(-103.4736,3.243);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("AA7EFQgIhnAFiqQgDisgLhSQAAgHgugEIAAA4IAEDDIAGDBQACBQgLAmQgPA0goAFIgMgBQgRgCgPgHQgYgIAHgdQAGgdAiAIIAKABQAPgLAAhrQABgkgDg0IgDg/QABgigChIIgDhxIABhGQgkgGgDgIQgLgtAfAAQAggDA5AGIBIAGQAbAAgDBkQACAfABAwIABA9QgBBBAOE3QAFA1gPADQgJAFgHAAQgeAAgGhTg");
	this.shape_10.setTransform(-132.2568,2.6659);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("Ah3FOQgEhfAMjTQANjWgDhVQgCgfAFgNQAEgSAOgIQARgJAQAOQACAaABAyIADBPQASgOAbg+QAcg/ATgQQAJgIAPgCQASgCAEAMQAGASgbAxQhHCXgZAuQAtBQBFC7QAiBUgIAeQgDAOgRAFQgRAFgKgIQgTgLgJgiQgjiAg5iNQgKA8AAEDQAAAQgfABIgEAAQgbAAAAgNg");
	this.shape_11.setTransform(-157.7557,3.314);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("AAJFaQg3gBgcgtQgUgigJhxIgIikQgEjKAPgwQAWhJA+gLQAkgFAkAYQAXANAOAvQALAdgYAPQgXAPgIgRQgTgqgWgGQgegKgKAxQgFAVgCA7QgEDUANB1QAOB9AkgHQAhgGADgTQAFgUAVgGQAMgEAKAMQAJAJAAAHQABAHgTAeQgcAqg4AAIgCAAg");
	this.shape_12.setTransform(-185.5629,3.408);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFFFFF").s().p("AhFFeQgRAAgIgPQgJgmAChsQABgrgCiQQgCh+ADg9IABiGQADgeAUAAQAeABAIAxQADATgCBJIgDE3IANgwQA1iSABhgQAAgNAIgqIAIgyQAGgpAVgHQAVgIADAWQALBFgBB7QgBCpABAlQACAqgEBKQgEBSABAbQgBA2gegIQgRAGgJgXQgIgVABgeIAQkzQgvEOgkBZQgQAWgSAAIgCAAg");
	this.shape_13.setTransform(-236.2175,3.8271);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol_21_Layer_1, null, null);


(lib.Symbol_20_Layer_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#3DACE5").s().p("AgsFOQgLgFARh0QAKg6AUhxQAUh2AIg0Ihlh7QgcgmgBgIQAAgRARgHQAQgHAQAKQARAJAgApQAgAsAOAJQADgKAEgnQAEglAGgQQAKgQAOgGQAQgHAQANQALAGgWCJIgUB3IgQBpQgGAqgOBMIgUB2QgNBFgFADIgLAAQgZAAgKgIg");
	this.shape.setTransform(175.3723,2.7765);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#3DACE5").s().p("Ah3FOQgEhfAMjTQANjWgDhVQgCgfAFgNQAEgSAOgIQARgJAQAOQACAaABAyIADBPQASgOAbg+QAcg/ATgQQAJgIAPgCQASgCAEAMQAGASgbAxQhHCXgZAuQAtBQBFC7QAiBUgIAeQgDAOgRAFQgRAFgKgIQgTgLgJgiQgjiAg5iNQgKA8AAEDQAAAQgfABIgEAAQgbAAAAgNg");
	this.shape_1.setTransform(150.4943,3.314);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#3DACE5").s().p("AgTFhQgggFgRgMQgUgPgDhOIgJjtIgChFIgBhPQAAgRgGhBQgEgyADgbQAFgrA/gIQA8gIApAjQAeAcAJAVQAWAwgRAzQgQAvgqAdQAiA6ALAuQAeBrgJBYQgSB/g3AWQgPAHgWAAIgUgBgAgjhDIAECLIAECMQADBPALAFQAQAEATgWQARgVAEgSQAShkgShoQgUhlg1gHQgFACAAAEgAgqktQgBAbAEAyIAEBQQApADAZgeQAZgdgIgsQgFgdgagRQgSgMgXAAQgJAAgJABg");
	this.shape_2.setTransform(120.8409,2.1029);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#3DACE5").s().p("AhQEpQgTgegCgVQgEggACgUQACgkAAhIQAChVACgjIAAh1QAAhyASggQAVglAegGQBLgNAnA4QARAbAABAIABCRQABBDgEA8IgFA7QgEAnAAAaQgEBRgTAfQgYAmghADIgNAAQg0AAgbgugAgKkYQgdALgFAsQgDAPADBLQABAyAABRIgCB8IgDA3QgCAhACARQAFAyAvACQAXAAADgwIACgoQABgYACgJQAeifgSjNQgDgigLgYQgFgIgNgGQgHgDgHAAQgFAAgGADg");
	this.shape_3.setTransform(92.5033,2.9386);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#3DACE5").s().p("Ah3FOQgEhfAMjTQANjWgDhVQgCgfAFgNQAEgSAOgIQARgJAQAOQACAaABAyIADBPQASgOAbg+QAcg/ATgQQAJgIAPgCQASgCAEAMQAGASgbAxQhHCXgZAuQAtBQBFC7QAiBUgIAeQgDAOgRAFQgRAFgKgIQgTgLgJgiQgjiAg5iNQgKA8AAEDQAAAQgfABIgEAAQgbAAAAgNg");
	this.shape_4.setTransform(66.5943,3.314);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#3DACE5").s().p("Ah8FOQgPgXAGhbQACgdAJg2IAPhWQACgWAch1IAdiEQAMg/AIgXQASgwAXARQALAHANBHIAgDWQAVCEAUBVQAgB+gIAZQgpAZgQgkIgWhcIgiifIAAAAIAAAAQgJACgSAAQgZAAgEABQgQA1gJBSQgQCKgEAAQgOAGgKAAQgNAAgHgJgAgNhSQgEAQgKA5QAdAAAFgDQAAhZgNgGIAAAAQgBAAgGAZg");
	this.shape_5.setTransform(36.9264,3.243);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#3DACE5").s().p("AhUFTQgQgCACg1IACgvIACgvQAAjagEhAIgDg9QgDgxABgfQABhkAbAAIA+gGQA3gEAhABQAeAAgJBNIAAAkQAABMAECaIAEDsQADBKgUAUQgMAPgXgLQgLgGAAgRQADhHgGiQQgHiUAChCQAFh6gBgiIgLABQg+gCACAJQgHBSAHCsQAGCrgGBWQgFBhgfAAQgHAAgHgEg");
	this.shape_6.setTransform(9.6496,3.3765);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#3DACE5").s().p("AgsFOQgLgFARh0QAKg6AUhxQAUh2AIg0Ihlh7QgcgmgBgIQAAgRARgHQAQgHAQAKQARAJAgApQAgAsAOAJQADgKAEgnQAEglAGgQQAKgQAOgGQAQgHAQANQALAGgWCJIgUB3IgQBpQgGAqgOBMIgUB2QgNBFgFADIgLAAQgZAAgKgIg");
	this.shape_7.setTransform(-17.4777,2.7765);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#3DACE5").s().p("AhCEnQgQjAgKjZQgBgjgJhYQgGhDAfgQQAFgCBpgSQAagEANALQAMAMgKATQgNAVgmABQgtACgQAMQAAAlAIBNQAHBNgBAnQAwAJgFAZQgCAQgiAMQADAkAGBeQAFBTAFAtQA7gQAmATQAJAGgHASQgGAUgKAAQg9AHg+AEQgcAAAAgvg");
	this.shape_8.setTransform(-63.7745,3.2828);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#3DACE5").s().p("AgNFVQgegOAEh3QABgRgCgaIAAgoQAAjHAMi+QgCALg8gPQgmgLADgdQABghBdgDQATgCBnAFQAaAFAHAgQAIAegPAAQgMAFgigFQgdgDAAAHQgMBOABCjQACDEgEBCIAAAnQABAbgCAOQgFAhgUAAQgHAAgJgFg");
	this.shape_9.setTransform(-89.5187,2.8677);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#3DACE5").s().p("AhFGbQgRAAgIgPQgJgmAChsQABgrgCiQQgCh+ADg9IABiGQADgeAUAAQAeABAIAxQADATgCBJIgDE3IANgwQA1iSABhgQAAgNAIgqIAIgyQAGgpAVgHQAVgIADAWQALBFgBB7QgBCpABAlQACAqgEBKQgEBSABAbQgBA2gegIQgRAGgJgXQgIgVABgeIAQk0QgvEPgkBZQgQAWgSAAIgCAAgAgQk1IgQgNQgHgDgJgOQgDgGgEgDIgKgOQgQgUAFgRQAEgRARAJQAGANAGAGIAJAKQAGAIAIAHIAHADQANAHAMAAIAHABQAAAAABAAQAAgBAAAAQABAAAAgBQABAAABAAIAEgCQADgBAFgDIASgNQAEgBAFgKIAJgQQAagFgLAaQgBAIgCAGQgIAWgFAIIgKAKQgDAFgBAAQgEAAgEAEQgFAFgCAAQgCAAgGAGQgKAHgLAAQgMAAgQgLg");
	this.shape_10.setTransform(-117.3675,-2.2896);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#3DACE5").s().p("AhQEpQgTgegCgVQgEggACgUQACgkAAhIQAChVACgjIAAh1QAAhyASggQAVglAegGQBLgNAnA4QARAbAABAIABCRQABBDgEA8IgFA7QgEAnAAAaQgEBRgTAfQgYAmghADIgNAAQg0AAgbgugAgKkYQgdALgFAsQgDAPADBLQABAyAABRIgCB8IgDA3QgCAhACARQAFAyAvACQAXAAADgwIACgoQABgYACgJQAeifgSjNQgDgigLgYQgFgIgNgGQgHgDgHAAQgFAAgGADg");
	this.shape_11.setTransform(-144.8467,2.9386);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#3DACE5").s().p("AA8FRQgBgCAAhNQAAkHALinQgHAEgjA/QgOAXgGAFQgEACgGgCQgcgFgVhHQgGgOgEgEIgBA0IgBAqQAABHgFBhIgBBbQABA7gCAaQgEBGgOAGQgUAJgIgQQgFgOgCh7IgCguQgBgeABgSQAFhNACh2IADjuQAFgZAlAGQAKAAAOArIAKAiIAHAXQAHAOADAPQAUAsACADQgDgEBNiNQAKgSACgBQAZgOAOAVQADACgDBJQgIDygBFKQAAAYgaAFQgFABgFAAQgMAAgIgMg");
	this.shape_12.setTransform(-173.475,2.446);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol_20_Layer_1, null, null);


(lib.Symbol_19_Layer_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#03A2E3").s().p("AgQFhQgigFgRgMQgVgPgBhOIgKjtIgChFIgChPQAAgRgFhBIgGhXQgBgjAZgHIAcABQAUAAABAzQABA0ACAZIAFCYQBigqAkByQAUBugKBYQgRB/g4AWQgOAHgVAAIgTgBgAgcgWQgEACAAADQABAaABA/IAECMQADBPALAFQAQAEAUgWQARgVADgSQAShkgShoQgJhAgjAAQgMAAgQAHg");
	this.shape.setTransform(207.1036,2.1448);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#03A2E3").s().p("AAJFaQg3gBgcgtQgUgigJhxIgIikQgEjKAPgwQAWhJA+gLQAkgFAkAYQAXANAOAvQALAdgYAPQgXAPgIgRQgTgqgWgGQgegKgKAxQgFAVgCA7QgEDUANB1QAOB9AkgHQAhgGADgTQAFgUAVgGQAMgEAKAMQAJAJAAAHQABAHgTAeQgcAqg4AAIgCAAg");
	this.shape_1.setTransform(179.6871,3.408);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#03A2E3").s().p("AhCEnQgQjAgKjZQgBgjgJhYQgGhDAfgQQAFgCBpgSQAagEANALQAMAMgKATQgNAVgmABQgtACgQAMQAAAlAIBNQAHBNgBAnQAwAJgFAZQgCAQgiAMQADAkAGBeQAFBTAFAtQA7gQAmATQAJAGgHASQgGAUgKAAQg9AHg+AEQgcAAAAgvg");
	this.shape_2.setTransform(153.1255,3.2828);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#03A2E3").s().p("AgNFVQgegOAEh3QABgRgCgaIAAgoQAAjHAMi+QgCALg8gPQgmgLADgdQABghBdgDQATgCBnAFQAaAFAHAgQAIAegPAAQgMAFgigFQgdgDAAAHQgMBOABCjQACDEgEBCIAAAnQABAbgCAOQgFAhgUAAQgHAAgJgFg");
	this.shape_3.setTransform(127.3813,2.8677);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#03A2E3").s().p("AhFGbQgRAAgIgPQgJgmAChsQABgrgCiQQgCh+ADg9IABiGQADgeAUAAQAeABAIAxQADATgCBJIgDE3IANgwQA1iSABhgQAAgNAIgqIAIgyQAGgpAVgHQAVgIADAWQALBFgBB7QgBCpABAlQACAqgEBKQgEBSABAbQgBA2gegIQgRAGgJgXQgIgVABgeIAQk0QgvEPgkBZQgQAWgSAAIgCAAgAgQk1IgQgNQgHgDgJgOQgDgGgEgDIgKgOQgQgUAFgRQAEgRARAJQAGANAGAGIAJAKQAGAIAIAHIAHADQANAHAMAAIAHABQAAAAABAAQAAgBAAAAQABAAAAgBQABAAABAAIAEgCQADgBAFgDIASgNQAEgBAFgKIAJgQQAagFgLAaQgBAIgCAGQgIAWgFAIIgKAKQgDAFgBAAQgEAAgEAEQgFAFgCAAQgCAAgGAGQgKAHgLAAQgMAAgQgLg");
	this.shape_4.setTransform(99.5325,-2.2896);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#03A2E3").s().p("AgsFOQgLgFARh0QAKg6AUhxQAUh2AIg0Ihlh7QgcgmgBgIQAAgRARgHQAQgHAQAKQARAJAgApQAgAsAOAJQADgKAEgnQAEglAGgQQAKgQAOgGQAQgHAQANQALAGgWCJIgUB3IgQBpQgGAqgOBMIgUB2QgNBFgFADIgLAAQgZAAgKgIg");
	this.shape_5.setTransform(73.2223,2.7765);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#03A2E3").s().p("AhfFoQgSgEgDgpIAAokQAAh4AKgCQARgKBJAHQBNAHAeAVQATAOAGA/QAGBFgqAsQgOAPg2AGQgvAEAAAFQARAAggFdIgBAqQAAAfgCANQgFAlgXAAQgGAAgIgCgAg5kpQgGAGAKBrQAAALAwgCQAwgBAOgQQAKgLABgkQACgkgKgMQgNgSg1gCIgMAAQgqAAADAKg");
	this.shape_6.setTransform(48.9568,1.7993);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#03A2E3").s().p("AhFFeQgRAAgIgPQgJgmAChsQABgrgCiQQgCh+ADg9IABiGQADgeAUAAQAeABAIAxQADATgCBJIgDE3IANgwQA1iSABhgQAAgNAIgqIAIgyQAGgpAVgHQAVgIADAWQALBFgBB7QgBCpABAlQACAqgEBKQgEBSABAbQgBA2gegIQgRAGgJgXQgIgVABgeIAQkzQgvEOgkBZQgQAWgSAAIgCAAg");
	this.shape_7.setTransform(19.6825,3.8271);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#03A2E3").s().p("AA7EFQgIhnAFiqQgDisgLhSQAAgHgugEIAAA4IAEDDIAGDBQACBQgLAmQgPA0goAFIgMgBQgRgCgPgHQgYgIAHgdQAGgdAiAIIAKABQAPgLAAhrQABgkgDg0IgDg/QABgigChIIgDhxIABhGQgkgGgDgIQgLgtAfAAQAggDA5AGIBIAGQAbAAgDBkQACAfABAwIABA9QgBBBAOE3QAFA1gPADQgJAFgHAAQgeAAgGhTg");
	this.shape_8.setTransform(-8.9068,2.6659);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#03A2E3").s().p("AhQEpQgTgegCgVQgEggACgUQACgkAAhIQAChVACgjIAAh1QAAhyASggQAVglAegGQBLgNAnA4QARAbAABAIABCRQABBDgEA8IgFA7QgEAnAAAaQgEBRgTAfQgYAmghADIgNAAQg0AAgbgugAgKkYQgdALgFAsQgDAPADBLQABAyAABRIgCB8IgDA3QgCAhACARQAFAyAvACQAXAAADgwIACgoQABgYACgJQAeifgSjNQgDgigLgYQgFgIgNgGQgHgDgHAAQgFAAgGADg");
	this.shape_9.setTransform(-35.5467,2.9386);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#03A2E3").s().p("AhWEzQgNgLACgWQADgdARgKQARgIAPANIAUATQAJAKAGAEQAPAJAegNQAVgKAHhEQACgkgFglQgEgxgOgKQgIgIgfAIQghAGgOgFQgQgHgKgRQgLgVAJgVQACgDAxgfQAmgcAOgrQAchOgMguQgLgjgjAAQggAAgaA4QgDAFgEAUQgFATgFAJQgNAegagQQgXgNACghIAJgaQAchvBagOQAvgHAvAzQAlAogGBcIgRBOQgCALgSAZQgPAXAAAKQAAAKAQAPQASASACAEQAvBZgUB0QgIAzgaAfQgcAcguAFIgMAAQg3AAgogng");
	this.shape_10.setTransform(-65.4223,2.2843);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#03A2E3").s().p("AhFFeQgRAAgIgPQgJgmAChsQABgrgCiQQgCh+ADg9IABiGQADgeAUAAQAeABAIAxQADATgCBJIgDE3IANgwQA1iSABhgQAAgNAIgqIAIgyQAGgpAVgHQAVgIADAWQALBFgBB7QgBCpABAlQACAqgEBKQgEBSABAbQgBA2gegIQgRAGgJgXQgIgVABgeIAQkzQgvEOgkBZQgQAWgSAAIgCAAg");
	this.shape_11.setTransform(-95.2175,3.8271);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#03A2E3").s().p("AhQEpQgTgegCgVQgEggACgUQACgkAAhIQAChVACgjIAAh1QAAhyASggQAVglAegGQBLgNAnA4QARAbAABAIABCRQABBDgEA8IgFA7QgEAnAAAaQgEBRgTAfQgYAmghADIgNAAQg0AAgbgugAgKkYQgdALgFAsQgDAPADBLQABAyAABRIgCB8IgDA3QgCAhACARQAFAyAvACQAXAAADgwIACgoQABgYACgJQAeifgSjNQgDgigLgYQgFgIgNgGQgHgDgHAAQgFAAgGADg");
	this.shape_12.setTransform(-122.6967,2.9386);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#03A2E3").s().p("AA8FRQgBgCAAhNQAAkHALinQgHAEgjA/QgOAXgGAFQgEACgGgCQgcgFgVhHQgGgOgEgEIgBA0IgBAqQAABHgFBhIgBBbQABA7gCAaQgEBGgOAGQgUAJgIgQQgFgOgCh7IgCguQgBgeABgSQAFhNACh2IADjuQAFgZAlAGQAKAAAOArIAKAiIAHAXQAHAOADAPQAUAsACADQgDgEBNiNQAKgSACgBQAZgOAOAVQADACgDBJQgIDygBFKQAAAYgaAFQgFABgFAAQgMAAgIgMg");
	this.shape_13.setTransform(-151.325,2.446);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#03A2E3").s().p("Ah8FOQgPgXAGhbQACgdAJg2IAPhWQACgWAch1IAdiEQAMg/AIgXQASgwAXARQALAHANBHIAgDWQAVCEAUBVQAgB+gIAZQgpAZgQgkIgWhcIgiifIAAAAIAAAAQgJACgSAAQgZAAgEABQgQA1gJBSQgQCKgEAAQgOAGgKAAQgNAAgHgJgAgNhSQgEAQgKA5QAdAAAFgDQAAhZgNgGIAAAAQgBAAgGAZg");
	this.shape_14.setTransform(-181.6236,3.243);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#03A2E3").s().p("AAJFaQg3gBgcgtQgUgigJhxIgIikQgEjKAPgwQAWhJA+gLQAkgFAkAYQAXANAOAvQALAdgYAPQgXAPgIgRQgTgqgWgGQgegKgKAxQgFAVgCA7QgEDUANB1QAOB9AkgHQAhgGADgTQAFgUAVgGQAMgEAKAMQAJAJAAAHQABAHgTAeQgcAqg4AAIgCAAg");
	this.shape_15.setTransform(-207.7629,3.408);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol_19_Layer_1, null, null);


(lib.Symbol_18_Layer_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AhQEpQgTgegCgVQgEggACgUQACgkAAhIQAChVACgjIAAh1QAAhyASggQAVglAegGQBLgNAnA4QARAbAABAIABCRQABBDgEA8IgFA7QgEAnAAAaQgEBRgTAfQgYAmghADIgNAAQg0AAgbgugAgKkYQgdALgFAsQgDAPADBLQABAyAABRIgCB8IgDA3QgCAhACARQAFAyAvACQAXAAADgwIACgoQABgYACgJQAeifgSjNQgDgigLgYQgFgIgNgGQgHgDgHAAQgFAAgGADg");
	this.shape.setTransform(354.8033,2.9386);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AhSFaQgTABgBhDIABg+IABg9IABlyQAAhXABgGQAEgPAJgJQAMgKARAHQAOAGAACCQgCCFAFAFQAAAEAngDQAjgBACgCQAEgHgDhzQgFh2AIgUQAKgZAOAAQAXAAgBAKQAPFeACEmQgBAagRAKQgTAFgPgKQgHgRgBgxQACiIgJiAQgIgMhCAOQgPAAAMDlQAEBQgLAVQgEAGgTAAIgMgBg");
	this.shape_1.setTransform(328.85,2.6711);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AAzFUQgJgRAAgxQACiJgJiAQgIgMhCAOQg7AFgCg+IgBimQAAhYABgGQAEgPAJgIQAMgKASAGQAOAHgCCCQgDCFAGAHQAAAIAngJQAkgCACgCQAGgGgFhzQgFh3AIgUQAKgYAOAAQAXAAgBAJQAQFeABEmQgBAbgRAJQgHACgGAAQgLAAgJgGg");
	this.shape_2.setTransform(301.95,2.6226);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AhFFeQgRAAgIgPQgJgmAChsQABgrgCiQQgCh+ADg9IABiGQADgeAUAAQAeABAIAxQADATgCBJIgDE3IANgwQA1iSABhgQAAgNAIgqIAIgyQAGgpAVgHQAVgIADAWQALBFgBB7QgBCpABAlQACAqgEBKQgEBSABAbQgBA2gegIQgRAGgJgXQgIgVABgeIAQkzQgvEOgkBZQgQAWgSAAIgCAAg");
	this.shape_3.setTransform(274.7325,3.8271);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AgoFSQgOgOgIg5QgEgggZnBIgFgnQgDgXAAgMQgBgiAYgIIAcABQAPAAALgCQA1gLAVgBQAtgBADAZQAEAdg1ALQgQADgdACIgjAEQgCANAHBoQAHBpALBkQALB4ACApQAAALADAZIACAkQABAqgNAHQgPAKgLAAQgJAAgFgGg");
	this.shape_4.setTransform(250.0884,2.8524);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AhQEpQgTgegCgVQgEggACgUQACgkAAhIQAChVACgjIAAh1QAAhyASggQAVglAegGQBLgNAnA4QARAbAABAIABCRQABBDgEA8IgFA7QgEAnAAAaQgEBRgTAfQgYAmghADIgNAAQg0AAgbgugAgKkYQgdALgFAsQgDAPADBLQABAyAABRIgCB8IgDA3QgCAhACARQAFAyAvACQAXAAADgwIACgoQABgYACgJQAeifgSjNQgDgigLgYQgFgIgNgGQgHgDgHAAQgFAAgGADg");
	this.shape_5.setTransform(223.1533,2.9386);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AA7EFQgIhnAFiqQgDisgLhSQAAgHgugEIAAA4IAEDDIAGDBQACBQgLAmQgPA0goAFIgMgBQgRgCgPgHQgYgIAHgdQAGgdAiAIIAKABQAPgLAAhrQABgkgDg0IgDg/QABgigChIIgDhxIABhGQgkgGgDgIQgLgtAfAAQAggDA5AGIBIAGQAbAAgDBkQACAfABAwIABA9QgBBBAOE3QAFA1gPADQgJAFgHAAQgeAAgGhTg");
	this.shape_6.setTransform(194.9932,2.6659);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#C3E7F9").s().p("AhQEpQgTgegCgVQgEggACgUQACgkAAhIQAChVACgjIAAh1QAAhyASggQAVglAegGQBLgNAnA4QARAbAABAIABCRQABBDgEA8IgFA7QgEAnAAAaQgEBRgTAfQgYAmghADIgNAAQg0AAgbgugAgKkYQgdALgFAsQgDAPADBLQABAyAABRIgCB8IgDA3QgCAhACARQAFAyAvACQAXAAADgwIACgoQABgYACgJQAeifgSjNQgDgigLgYQgFgIgNgGQgHgDgHAAQgFAAgGADg");
	this.shape_7.setTransform(168.3533,2.9386);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#C3E7F9").s().p("Ah3FOQgEhfAMjTQANjWgDhVQgCgfAFgNQAEgSAOgIQARgJAQAOQACAaABAyIADBPQASgOAbg+QAcg/ATgQQAJgIAPgCQASgCAEAMQAGASgbAxQhHCXgZAuQAtBQBFC7QAiBUgIAeQgDAOgRAFQgRAFgKgIQgTgLgJgiQgjiAg5iNQgKA8AAEDQAAAQgfABIgEAAQgbAAAAgNg");
	this.shape_8.setTransform(142.4443,3.314);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#C3E7F9").s().p("AhDFHQgXgNgNgvQgKgeAYgPQAXgOAIARQARAqAWAHQAfAIALgxQAGgVADg7QADg9ABg1QgGADgJAAQgTABglAAIgIAAQgFgCgFAAQgJAAgOgWIAAgLIABgJQAGgWARAAQAKAAAPADIATABIAugFQAAhZgFg+QgJh+gmAIQggAGgFASQgFAUgVAHQgLAEgKgMQgJgLAAgFQgBgIAUgdQAfgrA4ABQA4AAAZAtQAUAjAGBwIACCkQgCDLgQAvQgYBKg/AKIgOABQgdAAgbgTg");
	this.shape_9.setTransform(112.3476,3.4329);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol_18_Layer_1, null, null);


(lib.Symbol_1_Layer_16 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_16
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#E7E7E7").s().p("AwQFDIAAqFMAggAAAIAAKFg");
	this.shape.setTransform(168.6,195.7);
	this.shape._off = true;

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(111).to({_off:false},0).to({_off:true},12).wait(142));

}).prototype = p = new cjs.MovieClip();


(lib.Scene_1_Layer_5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_5
	this.instance = new lib.bg();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(265));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol25 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1_obj_
	this.Layer_1 = new lib.Symbol_25_Layer_1();
	this.Layer_1.name = "Layer_1";
	this.Layer_1.parent = this;
	this.Layer_1.setTransform(-1.3,2.8,1,1,0,0,0,-1.3,2.8);
	this.Layer_1.depth = 0;
	this.Layer_1.isAttachedToCamera = 0
	this.Layer_1.isAttachedToMask = 0
	this.Layer_1.layerDepth = 0
	this.Layer_1.layerIndex = 0
	this.Layer_1.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_1).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol25, new cjs.Rectangle(-150.6,-58.1,296.79999999999995,116.2), null);


(lib.Symbol24 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1_obj_
	this.Layer_1 = new lib.Symbol_24_Layer_1();
	this.Layer_1.name = "Layer_1";
	this.Layer_1.parent = this;
	this.Layer_1.setTransform(0.5,-2.3,1,1,0,0,0,0.5,-2.3);
	this.Layer_1.depth = 0;
	this.Layer_1.isAttachedToCamera = 0
	this.Layer_1.isAttachedToMask = 0
	this.Layer_1.layerDepth = 0
	this.Layer_1.layerIndex = 0
	this.Layer_1.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_1).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol24, new cjs.Rectangle(-124.2,-58.1,248.5,116.2), null);


(lib.Symbol23 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1_obj_
	this.Layer_1 = new lib.Symbol_23_Layer_1();
	this.Layer_1.name = "Layer_1";
	this.Layer_1.parent = this;
	this.Layer_1.setTransform(-0.1,1.8,1,1,0,0,0,-0.1,1.8);
	this.Layer_1.depth = 0;
	this.Layer_1.isAttachedToCamera = 0
	this.Layer_1.isAttachedToMask = 0
	this.Layer_1.layerDepth = 0
	this.Layer_1.layerIndex = 0
	this.Layer_1.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_1).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol23, new cjs.Rectangle(-134.4,-58.1,268.8,116.2), null);


(lib.Symbol22 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1_obj_
	this.Layer_1 = new lib.Symbol_22_Layer_1();
	this.Layer_1.name = "Layer_1";
	this.Layer_1.parent = this;
	this.Layer_1.setTransform(-5,1.7,1,1,0,0,0,-5,1.7);
	this.Layer_1.depth = 0;
	this.Layer_1.isAttachedToCamera = 0
	this.Layer_1.isAttachedToMask = 0
	this.Layer_1.layerDepth = 0
	this.Layer_1.layerIndex = 0
	this.Layer_1.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_1).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol22, new cjs.Rectangle(-287.9,-58.1,563.2,116.2), null);


(lib.Symbol21 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2_obj_
	this.Layer_2 = new lib.Symbol_21_Layer_2();
	this.Layer_2.name = "Layer_2";
	this.Layer_2.parent = this;
	this.Layer_2.setTransform(-75,8.5,1,1,0,0,0,-75,8.5);
	this.Layer_2.depth = 0;
	this.Layer_2.isAttachedToCamera = 0
	this.Layer_2.isAttachedToMask = 0
	this.Layer_2.layerDepth = 0
	this.Layer_2.layerIndex = 0
	this.Layer_2.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_2).wait(1));

	// Layer_1_obj_
	this.Layer_1 = new lib.Symbol_21_Layer_1();
	this.Layer_1.name = "Layer_1";
	this.Layer_1.parent = this;
	this.Layer_1.setTransform(-0.4,-2.3,1,1,0,0,0,-0.4,-2.3);
	this.Layer_1.depth = 0;
	this.Layer_1.isAttachedToCamera = 0
	this.Layer_1.isAttachedToMask = 0
	this.Layer_1.layerDepth = 0
	this.Layer_1.layerIndex = 1
	this.Layer_1.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_1).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol21, new cjs.Rectangle(-251.2,-58.1,502.5,116.2), null);


(lib.Symbol20 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1_obj_
	this.Layer_1 = new lib.Symbol_20_Layer_1();
	this.Layer_1.name = "Layer_1";
	this.Layer_1.parent = this;
	this.Layer_1.setTransform(0.1,-2.3,1,1,0,0,0,0.1,-2.3);
	this.Layer_1.depth = 0;
	this.Layer_1.isAttachedToCamera = 0
	this.Layer_1.isAttachedToMask = 0
	this.Layer_1.layerDepth = 0
	this.Layer_1.layerIndex = 0
	this.Layer_1.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_1).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol20, new cjs.Rectangle(-190.9,-58.1,381.8,116.2), null);


(lib.Symbol19 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1_obj_
	this.Layer_1 = new lib.Symbol_19_Layer_1();
	this.Layer_1.name = "Layer_1";
	this.Layer_1.parent = this;
	this.Layer_1.setTransform(-0.3,-2.3,1,1,0,0,0,-0.3,-2.3);
	this.Layer_1.depth = 0;
	this.Layer_1.isAttachedToCamera = 0
	this.Layer_1.isAttachedToMask = 0
	this.Layer_1.layerDepth = 0
	this.Layer_1.layerIndex = 0
	this.Layer_1.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_1).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol19, new cjs.Rectangle(-223.8,-58.1,447.70000000000005,116.2), null);


(lib.Symbol18 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1_obj_
	this.Layer_1 = new lib.Symbol_18_Layer_1();
	this.Layer_1.name = "Layer_1";
	this.Layer_1.parent = this;
	this.Layer_1.setTransform(233.4,3.4,1,1,0,0,0,233.4,3.4);
	this.Layer_1.depth = 0;
	this.Layer_1.isAttachedToCamera = 0
	this.Layer_1.isAttachedToMask = 0
	this.Layer_1.layerDepth = 0
	this.Layer_1.layerIndex = 0
	this.Layer_1.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_1).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol18, new cjs.Rectangle(96.5,-58.1,275,116.2), null);


(lib.Symbol_14_Layer_11 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_11
	this.instance = new lib.Symbol16();
	this.instance.parent = this;
	this.instance.setTransform(-126,101.6);
	this.instance.alpha = 0;
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(2).to({_off:false},0).wait(1).to({x:-64.847,alpha:0.6115},0).wait(1).to({x:-46.3357,alpha:0.7966},0).wait(1).to({x:-36.5418,alpha:0.8946},0).wait(1).to({x:-30.8717,alpha:0.9513},0).wait(1).to({x:-27.6329,alpha:0.9837},0).wait(1).to({x:-26,alpha:1},0).wait(72).to({scaleX:1.0091,x:-24.0203},0).wait(1).to({scaleX:1.0398,x:-17.2918},0).wait(1).to({scaleX:1.0991,x:-4.3201},0).wait(1).to({scaleX:1.1949,x:16.6185},0).wait(1).to({scaleX:1.3297,x:46.1025},0).wait(1).to({scaleX:1.4846,x:79.9977},0).wait(1).to({scaleX:1.619,x:109.3915},0).wait(1).to({scaleX:1.7083,x:128.93},0).wait(1).to({scaleX:1.7547,x:139.0654},0).wait(1).to({scaleX:1.7681,x:141.9},0).wait(81).to({scaleX:1.7559,x:144.062,y:101.6007},0).wait(1).to({scaleX:1.7138,x:151.5235,y:101.6033},0).wait(1).to({scaleX:1.6315,x:166.1514,y:101.6082},0).wait(1).to({scaleX:1.498,x:189.8449,y:101.6163},0).wait(1).to({scaleX:1.3196,x:221.5192,y:101.6271},0).wait(1).to({scaleX:1.1438,x:252.7309,y:101.6377},0).wait(1).to({scaleX:1.021,x:274.5269,y:101.6451},0).wait(1).to({scaleX:0.9576,x:285.7926,y:101.6489},0).wait(1).to({regX:0.2,regY:0.1,scaleX:0.9396,x:289.05,y:101.7},0).wait(70).to({regX:0,regY:0,x:286.2852,y:101.6,alpha:0.9853},0).wait(1).to({x:277.4333,alpha:0.9346},0).wait(1).to({x:260.0796,alpha:0.8351},0).wait(1).to({x:231.971,alpha:0.674},0).wait(1).to({x:194.3944,alpha:0.4587},0).wait(1).to({x:157.3667,alpha:0.2465},0).wait(1).to({x:131.5093,alpha:0.0983},0).wait(1).to({x:118.1443,alpha:0.0217},0).wait(1).to({regX:0.2,regY:0.1,x:114.55,y:101.7,alpha:0},0).to({_off:true},1).wait(8));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_14_Layer_10 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_10
	this.instance = new lib.Symbol25();
	this.instance.parent = this;
	this.instance.setTransform(36.75,195.75);
	this.instance.alpha = 0;
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(175).to({_off:false},0).wait(1).to({regX:-1.3,regY:2.8,x:167.65,y:198.55,alpha:0.48},0).wait(1).to({x:217.4,alpha:0.6605},0).wait(1).to({x:247.35,alpha:0.7692},0).wait(1).to({x:267.55,alpha:0.8426},0).wait(1).to({x:281.9,alpha:0.8946},0).wait(1).to({x:292.25,alpha:0.9322},0).wait(1).to({x:299.75,alpha:0.9594},0).wait(1).to({x:305.05,alpha:0.9787},0).wait(1).to({x:308.65,alpha:0.9918},0).wait(1).to({regX:0,regY:0,x:312.25,y:195.75,alpha:1},0).wait(66).to({regX:-1.3,regY:2.8,x:307.45,y:198.55,alpha:0.9853},0).wait(1).to({x:295.5,alpha:0.9346},0).wait(1).to({x:272.1,alpha:0.8351},0).wait(1).to({x:234.15,alpha:0.674},0).wait(1).to({x:183.45,alpha:0.4587},0).wait(1).to({x:133.5,alpha:0.2465},0).wait(1).to({x:98.6,alpha:0.0983},0).wait(1).to({x:80.55,alpha:0.0217},0).wait(1).to({regX:0,regY:0,x:76.75,y:195.75,alpha:0},0).wait(6));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_14_Layer_9 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_9
	this.instance = new lib.Symbol24();
	this.instance.parent = this;
	this.instance.setTransform(10.4,98.25);
	this.instance.alpha = 0;
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(171).to({_off:false},0).wait(1).to({regX:0.5,regY:-2.3,x:144.35,y:95.95,alpha:0.48},0).wait(1).to({x:194.55,alpha:0.6605},0).wait(1).to({x:224.8,alpha:0.7692},0).wait(1).to({x:245.2,alpha:0.8426},0).wait(1).to({x:259.65,alpha:0.8946},0).wait(1).to({x:270.1,alpha:0.9322},0).wait(1).to({x:277.65,alpha:0.9594},0).wait(1).to({x:283.05,alpha:0.9787},0).wait(1).to({x:286.7,alpha:0.9918},0).wait(1).to({regX:0,regY:0,x:288.5,y:98.25,alpha:1},0).wait(68).to({regX:0.5,regY:-2.3,x:284.3,y:95.95,alpha:0.9853},0).wait(1).to({x:268.15,alpha:0.9346},0).wait(1).to({x:236.55,alpha:0.8351},0).wait(1).to({x:185.3,alpha:0.674},0).wait(1).to({x:116.8,alpha:0.4587},0).wait(1).to({x:49.3,alpha:0.2465},0).wait(1).to({x:2.15,alpha:0.0983},0).wait(1).to({x:-22.15,alpha:0.0217},0).wait(1).to({regX:0,regY:0,x:-29.6,y:98.25,alpha:0},0).to({_off:true},1).wait(7));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_14_Layer_8 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_8
	this.instance = new lib.Symbol23();
	this.instance.parent = this;
	this.instance.setTransform(20.55,0);
	this.instance.alpha = 0;
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(169).to({_off:false},0).wait(1).to({regX:-0.2,regY:1.8,x:153.35,y:1.8,alpha:0.48},0).wait(1).to({x:203.35,alpha:0.6605},0).wait(1).to({x:233.45,alpha:0.7692},0).wait(1).to({x:253.8,alpha:0.8426},0).wait(1).to({x:268.2,alpha:0.8946},0).wait(1).to({x:278.65,alpha:0.9322},0).wait(1).to({x:286.15,alpha:0.9594},0).wait(1).to({x:291.5,alpha:0.9787},0).wait(1).to({x:295.15,alpha:0.9918},0).wait(1).to({regX:0,regY:0,x:297.65,y:0,alpha:1},0).wait(66).to({regX:-0.2,regY:1.8,x:293.95,y:1.8,alpha:0.9853},0).wait(1).to({x:281.9,alpha:0.9346},0).wait(1).to({x:258.35,alpha:0.8351},0).wait(1).to({x:220.15,alpha:0.674},0).wait(1).to({x:169.1,alpha:0.4587},0).wait(1).to({x:118.75,alpha:0.2465},0).wait(1).to({x:83.65,alpha:0.0983},0).wait(1).to({x:65.5,alpha:0.0217},0).wait(1).to({regX:0,regY:0,x:60.55,y:0,alpha:0},0).to({_off:true},1).wait(11));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_14_Layer_6 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_6
	this.instance = new lib.Symbol22();
	this.instance.parent = this;
	this.instance.setTransform(19.55,195.75);
	this.instance.alpha = 0;
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(85).to({_off:false},0).wait(1).to({regX:-4.9,regY:1.7,x:95.1,y:197.45,alpha:0.5062},0).wait(1).to({x:124.15,alpha:0.6889},0).wait(1).to({x:141.3,alpha:0.7966},0).wait(1).to({x:152.6,alpha:0.8678},0).wait(1).to({x:160.4,alpha:0.9169},0).wait(1).to({x:165.9,alpha:0.9513},0).wait(1).to({x:169.65,alpha:0.975},0).wait(1).to({x:172.15,alpha:0.9906},0).wait(1).to({regX:0,regY:0,x:178.55,y:195.75,alpha:1},0).wait(76).to({x:244.05,alpha:0},6,cjs.Ease.get(-1)).to({_off:true},1).wait(88));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_14_Layer_5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_5
	this.instance = new lib.Symbol21();
	this.instance.parent = this;
	this.instance.setTransform(-2.05,97.5);
	this.instance.alpha = 0;
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(83).to({_off:false},0).wait(1).to({regX:-0.4,regY:2.6,x:72.5,y:100.1,alpha:0.5062},0).wait(1).to({x:99.55,alpha:0.6889},0).wait(1).to({x:115.5,alpha:0.7966},0).wait(1).to({x:126.05,alpha:0.8678},0).wait(1).to({x:133.35,alpha:0.9169},0).wait(1).to({x:138.4,alpha:0.9513},0).wait(1).to({x:141.9,alpha:0.975},0).wait(1).to({x:144.25,alpha:0.9906},0).wait(1).to({regX:0,regY:0,x:146.05,y:97.5,alpha:1},0).wait(75).to({x:207.15,alpha:0},6,cjs.Ease.get(-1)).to({_off:true},1).wait(91));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_14_Layer_4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_4
	this.instance = new lib.Symbol20();
	this.instance.parent = this;
	this.instance.setTransform(-97.2,0);
	this.instance.alpha = 0;
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(80).to({_off:false},0).wait(1).to({regX:0.2,regY:-2.3,x:-3.15,y:-2.3,alpha:0.5062},0).wait(1).to({x:30.7,alpha:0.6889},0).wait(1).to({x:50.65,alpha:0.7966},0).wait(1).to({x:63.85,alpha:0.8678},0).wait(1).to({x:73,alpha:0.9169},0).wait(1).to({x:79.35,alpha:0.9513},0).wait(1).to({x:83.75,alpha:0.975},0).wait(1).to({x:86.65,alpha:0.9906},0).wait(1).to({regX:0,regY:0,x:88.2,y:0,alpha:1},0).wait(75).to({x:167.1,alpha:0},6,cjs.Ease.get(-1)).to({_off:true},1).wait(94));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_14_Layer_2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.instance = new lib.Symbol18();
	this.instance.parent = this;
	this.instance.setTransform(-330.8,97.5);
	this.instance.alpha = 0;
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(5).to({_off:false},0).wait(1).to({regX:233.4,regY:3.4,x:-63.8,y:100.9,alpha:0.48},0).wait(1).to({x:-51.15,alpha:0.6605},0).wait(1).to({x:-43.55,alpha:0.7692},0).wait(1).to({x:-38.4,alpha:0.8426},0).wait(1).to({x:-34.75,alpha:0.8946},0).wait(1).to({x:-32.1,alpha:0.9322},0).wait(1).to({x:-30.2,alpha:0.9594},0).wait(1).to({x:-28.85,alpha:0.9787},0).wait(1).to({x:-27.95,alpha:0.9918},0).wait(1).to({regX:0,regY:0,x:-260.8,y:97.5,alpha:1},0).wait(63).to({regX:233.4,regY:3.4,x:-24.1,y:100.9,alpha:0.9853},0).wait(1).to({x:-12.75,alpha:0.9346},0).wait(1).to({x:9.55,alpha:0.8351},0).wait(1).to({x:45.6,alpha:0.674},0).wait(1).to({x:93.8,alpha:0.4587},0).wait(1).to({x:141.35,alpha:0.2465},0).wait(1).to({x:174.5,alpha:0.0983},0).wait(1).to({x:191.65,alpha:0.0217},0).wait(1).to({regX:0,regY:0,x:-36.9,y:97.5,alpha:0},0).to({_off:true},1).wait(178));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_14_Layer_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Symbol19();
	this.instance.parent = this;
	this.instance.setTransform(-50,0);
	this.instance.alpha = 0;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1).to({regX:-0.2,regY:-2.3,x:2.55,y:-2.3,alpha:0.48},0).wait(1).to({x:22.45,alpha:0.6605},0).wait(1).to({x:34.4,alpha:0.7692},0).wait(1).to({x:42.45,alpha:0.8426},0).wait(1).to({x:48.2,alpha:0.8946},0).wait(1).to({x:52.3,alpha:0.9322},0).wait(1).to({x:55.3,alpha:0.9594},0).wait(1).to({x:57.45,alpha:0.9787},0).wait(1).to({x:58.85,alpha:0.9918},0).wait(1).to({regX:0,regY:0,x:60,y:0,alpha:1},0).wait(65).to({regX:-0.2,regY:-2.3,x:62.7,y:-2.3,alpha:0.9853},0).wait(1).to({x:72.8,alpha:0.9346},0).wait(1).to({x:92.6,alpha:0.8351},0).wait(1).to({x:124.65,alpha:0.674},0).wait(1).to({x:167.5,alpha:0.4587},0).wait(1).to({x:209.7,alpha:0.2465},0).wait(1).to({x:239.2,alpha:0.0983},0).wait(1).to({x:254.45,alpha:0.0217},0).wait(1).to({regX:0,regY:0,x:259,y:0,alpha:0},0).to({_off:true},1).wait(181));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_12_Layer_3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_3
	this.instance = new lib.Symbol28();
	this.instance.parent = this;
	this.instance.setTransform(-0.05,-0.55,2.2,2.2,0,0,0,-0.2,0);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol_12_Layer_3, null, null);


(lib.Symbol_11_Layer_3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_3
	this.instance = new lib.Symbol28();
	this.instance.parent = this;
	this.instance.setTransform(-0.05,-0.55,2.2,2.2,0,0,0,-0.2,0);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol_11_Layer_3, null, null);


(lib.Symbol_10_Layer_3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_3
	this.instance = new lib.Symbol28();
	this.instance.parent = this;
	this.instance.setTransform(-0.05,-0.55,2.2,2.2,0,0,0,-0.2,0);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol_10_Layer_3, null, null);


(lib.Symbol_9_Layer_3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_3
	this.instance = new lib.Symbol28();
	this.instance.parent = this;
	this.instance.setTransform(-0.05,-0.55,2.2,2.2,0,0,0,-0.2,0);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol_9_Layer_3, null, null);


(lib.Symbol_8_Layer_3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_3
	this.instance = new lib.Symbol28();
	this.instance.parent = this;
	this.instance.setTransform(-0.05,-0.55,2.2,2.2,0,0,0,-0.2,0);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol_8_Layer_3, null, null);


(lib.Symbol_7_Layer_2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.instance = new lib.Symbol26();
	this.instance.parent = this;
	this.instance.setTransform(0.75,-0.15,2.207,2.207,0,0,0,0.1,0.1);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol_7_Layer_2, null, null);


(lib.Symbol_6_Layer_2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.instance = new lib.Symbol28();
	this.instance.parent = this;
	this.instance.setTransform(-0.05,-0.55,2.2,2.2,0,0,0,-0.2,0);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol_6_Layer_2, null, null);


(lib.Symbol_2_Layer_2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.instance = new lib.Symbol27();
	this.instance.parent = this;
	this.instance.setTransform(-0.55,0.4,2.2016,2.2016,0,0,0,-0.1,0.1);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol_2_Layer_2, null, null);


(lib.Symbol_1_pack_part2_shadow_png = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// pack_part2_shadow_png
	this.instance = new lib.Symbol5();
	this.instance.parent = this;
	this.instance.setTransform(265.7,-175.45);
	this.instance.alpha = 0;
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(69).to({_off:false},0).wait(1).to({scaleX:1.0916,scaleY:1.3361,skewX:-0.1745,x:264.3873,y:-177.9927,alpha:0.2606},0).wait(1).to({scaleX:1.2286,scaleY:1.8384,skewX:-0.4354,x:262.4249,y:-181.7936,alpha:0.6501},0).wait(1).to({scaleX:1.3179,scaleY:2.1658,skewX:-0.6054,x:261.1461,y:-184.2707,alpha:0.904},0).wait(1).to({regX:0.1,regY:-0.2,scaleX:1.3516,scaleY:2.2896,skewX:-0.6698,x:260.75,y:-185.2,alpha:1},0).to({regX:0,regY:0,scaleX:1,scaleY:1,skewX:0,x:265.7,y:-175.45},2,cjs.Ease.get(1)).to({_off:true},35).wait(155));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_1_pack_part2_png_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// pack_part2_png
	this.instance = new lib.Symbol4();
	this.instance.parent = this;
	this.instance.setTransform(247.45,-220.75,0.8458,0.7918,0,3.3949,-8.2115,-23.8,-8.3);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(56).to({regX:0,regY:0,scaleX:0.8456,skewX:3.3595,skewY:-8.1603,x:266.9649,y:-217.061},0).wait(1).to({scaleX:0.8454,scaleY:0.7917,skewX:3.3266,skewY:-8.1128,x:266.9783,y:-217.0713},0).wait(1).to({scaleX:0.8452,scaleY:0.7916,skewX:3.2964,skewY:-8.0692,x:266.9907,y:-217.0807},0).wait(1).to({regX:-23.8,regY:-8.3,scaleX:0.8451,skewX:3.2688,skewY:-8.0293,x:247.45,y:-220.8},0).wait(1).to({regX:0,regY:0,scaleX:0.8453,scaleY:0.7917,skewX:1.3239,skewY:-9.9994,x:263.1704,y:-217.2994},0).wait(1).to({regX:-23.6,regY:-8.3,scaleX:0.8454,skewX:-0.4163,skewY:-11.7622,x:240,y:-220},0).wait(1).to({regX:0,regY:0,scaleX:0.8453,scaleY:0.7916,skewX:1.5602,skewY:-9.7655,x:268.5879,y:-217.0933},0).wait(1).to({regX:-23.7,regY:-8.3,scaleX:0.8451,skewX:3.2732,skewY:-8.035,x:256.95,y:-220.5},0).wait(1).to({regX:0,regY:0,scaleX:0.8453,scaleY:0.7917,skewX:1.244,skewY:-10.085,x:267.1931,y:-217.1655},0).wait(1).to({regX:-23.6,regY:-8.3,scaleX:0.8454,skewX:-0.4163,skewY:-11.7622,x:240,y:-220},0).wait(1).to({regX:0,regY:0,scaleX:0.8453,scaleY:0.7916,skewX:1.5602,skewY:-9.7655,x:268.5879,y:-217.0933},0).wait(1).to({regX:-23.7,regY:-8.3,scaleX:0.8451,skewX:3.2732,skewY:-8.035,x:256.95,y:-220.5},0).wait(1).to({regX:0,regY:0,scaleX:0.8455,scaleY:0.7917,skewX:3.3401,skewY:-8.1321,x:271.1553,y:-216.8865},0).wait(1).to({regX:-23.8,regY:-8.3,scaleX:0.8458,scaleY:0.7918,skewX:3.3949,skewY:-8.2115,x:247.45,y:-220.75},0).wait(1).to({regX:0,regY:0,scaleX:0.914,scaleY:0.8362,skewX:0.5958,skewY:-6.1738,x:268.9261,y:-216.1139},0).wait(1).to({scaleX:1.0159,scaleY:0.9024,skewX:-3.5884,skewY:-3.1278,x:271.9164,y:-214.5266},0).wait(1).to({scaleX:1.0823,scaleY:0.9456,skewX:-6.3152,skewY:-1.1427,x:273.8792,y:-213.3753},0).wait(1).to({regX:-24,regY:-7.9,scaleX:1.1074,scaleY:0.962,skewX:-7.347,skewY:-0.3916,x:247.3,y:-220.65},0).to({regX:-24.3,regY:-7.5,scaleX:1,scaleY:1,skewX:0,skewY:0,x:247.25,y:-220.25},2,cjs.Ease.get(1)).to({_off:true},35).wait(155));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_1_pack_part2_png = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// pack_part2_png
	this.instance = new lib.Symbol4();
	this.instance.parent = this;
	this.instance.setTransform(247.5,-209.25,0.7243,0.7895,0,2.5489,-19.6999,-23.4,-8.6);
	this.instance.alpha = 0;
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(253).to({_off:false},0).wait(1).to({regX:0,regY:0,scaleX:0.7878,scaleY:0.7907,skewX:2.9908,skewY:-13.6984,x:265.0524,y:-212.8166,alpha:0.5224},0).wait(1).to({scaleX:0.812,scaleY:0.7912,skewX:3.1591,skewY:-11.4135,x:265.7471,y:-214.5007,alpha:0.7213},0).wait(1).to({scaleX:0.8257,scaleY:0.7915,skewX:3.2548,skewY:-10.114,x:266.1332,y:-215.4362,alpha:0.8344},0).wait(1).to({scaleX:0.8343,scaleY:0.7916,skewX:3.3146,skewY:-9.3021,x:266.3707,y:-216.0124,alpha:0.9051},0).wait(1).to({scaleX:0.8398,scaleY:0.7917,skewX:3.3527,skewY:-8.7844,x:266.5207,y:-216.3766,alpha:0.9501},0).wait(1).to({scaleX:0.8432,scaleY:0.7918,skewX:3.3763,skewY:-8.464,x:266.6128,y:-216.6007,alpha:0.978},0).wait(1).to({scaleX:0.845,skewX:3.3895,skewY:-8.2848,x:266.6642,y:-216.7256,alpha:0.9936},0).wait(1).to({regX:-23.8,regY:-8.3,scaleX:0.8458,skewX:3.3949,skewY:-8.2115,x:247.45,y:-220.75,alpha:1},0).wait(4));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_1_pack_part1_png = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// pack_part1_png
	this.instance = new lib.Symbol3();
	this.instance.parent = this;
	this.instance.setTransform(80.6,-220.35,1,1,-26.7484,0,0,9.5,-24.8);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(69).to({_off:false},0).wait(1).to({regX:0,regY:0,scaleX:1.089,rotation:-18.871,x:78.8337,y:-193.4996},0).wait(1).to({scaleX:1.2221,rotation:-7.0958,x:72.1888,y:-194.2527},0).wait(1).to({scaleX:1.3089,rotation:0.5782,x:67.9906,y:-195.6116},0).wait(1).to({regX:9.5,regY:-24.9,scaleX:1.3417,rotation:3.4817,x:80.65,y:-220.3},0).to({regX:9.4,regY:-24.8,scaleX:1,rotation:0,x:80.55,y:-220.25},2,cjs.Ease.get(1)).to({_off:true},35).wait(155));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol14 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_264 = function() {
		this.___loopingOver___ = true;
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(264).call(this.frame_264).wait(1));

	// Layer_10_obj_
	this.Layer_10 = new lib.Symbol_14_Layer_10();
	this.Layer_10.name = "Layer_10";
	this.Layer_10.parent = this;
	this.Layer_10.depth = 0;
	this.Layer_10.isAttachedToCamera = 0
	this.Layer_10.isAttachedToMask = 0
	this.Layer_10.layerDepth = 0
	this.Layer_10.layerIndex = 0
	this.Layer_10.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_10).wait(176).to({regX:173.2,regY:198.5,x:173.2,y:198.5},0).wait(9).to({regX:0,regY:0,x:0,y:0},0).wait(66).to({regX:173.2,regY:198.5,x:173.2,y:198.5},0).wait(8).to({regX:0,regY:0,x:0,y:0},0).wait(6));

	// Layer_9_obj_
	this.Layer_9 = new lib.Symbol_14_Layer_9();
	this.Layer_9.name = "Layer_9";
	this.Layer_9.parent = this;
	this.Layer_9.depth = 0;
	this.Layer_9.isAttachedToCamera = 0
	this.Layer_9.isAttachedToMask = 0
	this.Layer_9.layerDepth = 0
	this.Layer_9.layerIndex = 1
	this.Layer_9.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_9).wait(172).to({regX:130,regY:96,x:130,y:96},0).wait(9).to({regX:0,regY:0,x:0,y:0},0).wait(68).to({regX:130,regY:96,x:130,y:96},0).wait(8).to({regX:0,regY:0,x:0,y:0},0).wait(8));

	// Layer_8_obj_
	this.Layer_8 = new lib.Symbol_14_Layer_8();
	this.Layer_8.name = "Layer_8";
	this.Layer_8.parent = this;
	this.Layer_8.depth = 0;
	this.Layer_8.isAttachedToCamera = 0
	this.Layer_8.isAttachedToMask = 0
	this.Layer_8.layerDepth = 0
	this.Layer_8.layerIndex = 2
	this.Layer_8.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_8).wait(170).to({regX:158.9,regY:1.8,x:158.9,y:1.8},0).wait(9).to({regX:0,regY:0,x:0,y:0},0).wait(66).to({regX:158.9,regY:1.8,x:158.9,y:1.8},0).wait(8).to({regX:0,regY:0,x:0,y:0},0).wait(12));

	// Layer_6_obj_
	this.Layer_6 = new lib.Symbol_14_Layer_6();
	this.Layer_6.name = "Layer_6";
	this.Layer_6.parent = this;
	this.Layer_6.depth = 0;
	this.Layer_6.isAttachedToCamera = 0
	this.Layer_6.isAttachedToMask = 0
	this.Layer_6.layerDepth = 0
	this.Layer_6.layerIndex = 3
	this.Layer_6.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_6).wait(86).to({regX:126.9,regY:197.5,x:126.9,y:197.5},0).wait(8).to({regX:0,regY:0,x:0,y:0},0).wait(171));

	// Layer_5_obj_
	this.Layer_5 = new lib.Symbol_14_Layer_5();
	this.Layer_5.name = "Layer_5";
	this.Layer_5.parent = this;
	this.Layer_5.depth = 0;
	this.Layer_5.isAttachedToCamera = 0
	this.Layer_5.isAttachedToMask = 0
	this.Layer_5.layerDepth = 0
	this.Layer_5.layerIndex = 4
	this.Layer_5.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_5).wait(84).to({regX:102.2,regY:100.1,x:102.2,y:100.1},0).wait(8).to({regX:0,regY:0,x:0,y:0},0).wait(173));

	// Layer_4_obj_
	this.Layer_4 = new lib.Symbol_14_Layer_4();
	this.Layer_4.name = "Layer_4";
	this.Layer_4.parent = this;
	this.Layer_4.depth = 0;
	this.Layer_4.isAttachedToCamera = 0
	this.Layer_4.isAttachedToMask = 0
	this.Layer_4.layerDepth = 0
	this.Layer_4.layerIndex = 5
	this.Layer_4.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_4).wait(81).to({regX:35.1,regY:-2.3,x:35.1,y:-2.3},0).wait(8).to({regX:0,regY:0,x:0,y:0},0).wait(176));

	// Layer_2_obj_
	this.Layer_2 = new lib.Symbol_14_Layer_2();
	this.Layer_2.name = "Layer_2";
	this.Layer_2.parent = this;
	this.Layer_2.depth = 0;
	this.Layer_2.isAttachedToCamera = 0
	this.Layer_2.isAttachedToMask = 0
	this.Layer_2.layerDepth = 0
	this.Layer_2.layerIndex = 6
	this.Layer_2.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_2).wait(6).to({regX:49.6,regY:100.9,x:49.6,y:100.9},0).wait(9).to({regX:0,regY:0,x:0,y:0},0).wait(63).to({regX:49.6,regY:100.9,x:49.6,y:100.9},0).wait(8).to({regX:0,regY:0,x:0,y:0},0).wait(179));

	// Layer_1_obj_
	this.Layer_1 = new lib.Symbol_14_Layer_1();
	this.Layer_1.name = "Layer_1";
	this.Layer_1.parent = this;
	this.Layer_1.setTransform(-50.3,-2.3,1,1,0,0,0,-50.3,-2.3);
	this.Layer_1.depth = 0;
	this.Layer_1.isAttachedToCamera = 0
	this.Layer_1.isAttachedToMask = 0
	this.Layer_1.layerDepth = 0
	this.Layer_1.layerIndex = 7
	this.Layer_1.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_1).wait(1).to({regX:104.3,x:104.3},0).wait(9).to({regX:-50.3,x:-50.3},0).wait(65).to({regX:104.3,x:104.3},0).wait(8).to({regX:-50.3,x:-50.3},0).wait(182));

	// Layer_11_obj_
	this.Layer_11 = new lib.Symbol_14_Layer_11();
	this.Layer_11.name = "Layer_11";
	this.Layer_11.parent = this;
	this.Layer_11.depth = 0;
	this.Layer_11.isAttachedToCamera = 0
	this.Layer_11.isAttachedToMask = 0
	this.Layer_11.layerDepth = 0
	this.Layer_11.layerIndex = 8
	this.Layer_11.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_11).wait(3).to({regX:76.8,regY:101.6,x:76.8,y:101.6},0).wait(5).to({regX:0,regY:0,x:0,y:0},0).wait(72).to({regX:76.8,regY:101.6,x:76.8,y:101.6},0).wait(9).to({regX:0,regY:0,x:0,y:0},0).wait(81).to({regX:76.8,regY:101.6,x:76.8,y:101.6},0).wait(8).to({regX:0,regY:0,x:0,y:0},0).wait(70).to({regX:76.8,regY:101.6,x:76.8,y:101.6},0).wait(8).to({regX:0,regY:0,x:0,y:0},0).wait(9));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-288.1,-58.1,807.5,312);


(lib.Symbol12 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("A3DLZIAAuAIIooxIZ/BIILgKvIAAK6g");
	mask.setTransform(-8.2,230.9);

	// Layer_3_obj_
	this.Layer_3 = new lib.Symbol_12_Layer_3();
	this.Layer_3.name = "Layer_3";
	this.Layer_3.parent = this;
	this.Layer_3.setTransform(0.4,-0.6,1,1,0,0,0,0.4,-0.6);
	this.Layer_3.depth = 0;
	this.Layer_3.isAttachedToCamera = 0
	this.Layer_3.isAttachedToMask = 0
	this.Layer_3.layerDepth = 0
	this.Layer_3.layerIndex = 0
	this.Layer_3.maskLayerName = 0

	var maskedShapeInstanceList = [this.Layer_3];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.Layer_3).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol12, new cjs.Rectangle(-162.4,-288.7,325.6,576.4), null);


(lib.Symbol11 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("AtrHSIp0nNIAAncMAu6AAAIAFILIrQGkg");
	mask.setTransform(-11.925,-261.55);

	// Layer_3_obj_
	this.Layer_3 = new lib.Symbol_11_Layer_3();
	this.Layer_3.name = "Layer_3";
	this.Layer_3.parent = this;
	this.Layer_3.setTransform(0.4,-0.6,1,1,0,0,0,0.4,-0.6);
	this.Layer_3.depth = 0;
	this.Layer_3.isAttachedToCamera = 0
	this.Layer_3.isAttachedToMask = 0
	this.Layer_3.layerDepth = 0
	this.Layer_3.layerIndex = 0
	this.Layer_3.maskLayerName = 0

	var maskedShapeInstanceList = [this.Layer_3];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.Layer_3).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol11, new cjs.Rectangle(-162.4,-288.7,325.6,576.4), null);


(lib.Symbol10 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("EgEaglTIJNGyMgALA6QIpaJlg");
	mask.setTransform(-130.1,-19.55);

	// Layer_3_obj_
	this.Layer_3 = new lib.Symbol_10_Layer_3();
	this.Layer_3.name = "Layer_3";
	this.Layer_3.parent = this;
	this.Layer_3.setTransform(0.4,-0.6,1,1,0,0,0,0.4,-0.6);
	this.Layer_3.depth = 0;
	this.Layer_3.isAttachedToCamera = 0
	this.Layer_3.isAttachedToMask = 0
	this.Layer_3.layerDepth = 0
	this.Layer_3.layerIndex = 0
	this.Layer_3.maskLayerName = 0

	var maskedShapeInstanceList = [this.Layer_3];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.Layer_3).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol10, new cjs.Rectangle(-162.4,-288.7,325.6,576.4), null);


(lib.Symbol9 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("AmgbzMAAHg7WIM6ngMgA6BOHg");
	mask.setTransform(107.175,-12.475);

	// Layer_3_obj_
	this.Layer_3 = new lib.Symbol_9_Layer_3();
	this.Layer_3.name = "Layer_3";
	this.Layer_3.parent = this;
	this.Layer_3.setTransform(0.4,-0.6,1,1,0,0,0,0.4,-0.6);
	this.Layer_3.depth = 0;
	this.Layer_3.isAttachedToCamera = 0
	this.Layer_3.isAttachedToMask = 0
	this.Layer_3.layerDepth = 0
	this.Layer_3.layerIndex = 0
	this.Layer_3.maskLayerName = 0

	var maskedShapeInstanceList = [this.Layer_3];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.Layer_3).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol9, new cjs.Rectangle(-162.4,-288.7,325.6,576.4), null);


(lib.Symbol8 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("AtGckMAAKg6RIaDAAMgAQA7bg");
	mask.setTransform(-16.6,-24.75);

	// Layer_3_obj_
	this.Layer_3 = new lib.Symbol_8_Layer_3();
	this.Layer_3.name = "Layer_3";
	this.Layer_3.parent = this;
	this.Layer_3.setTransform(0.4,-0.6,1,1,0,0,0,0.4,-0.6);
	this.Layer_3.depth = 0;
	this.Layer_3.isAttachedToCamera = 0
	this.Layer_3.isAttachedToMask = 0
	this.Layer_3.layerDepth = 0
	this.Layer_3.layerIndex = 0
	this.Layer_3.maskLayerName = 0

	var maskedShapeInstanceList = [this.Layer_3];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.Layer_3).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol8, new cjs.Rectangle(-162.4,-288.7,325.6,576.4), null);


(lib.Symbol7 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2_obj_
	this.Layer_2 = new lib.Symbol_7_Layer_2();
	this.Layer_2.name = "Layer_2";
	this.Layer_2.parent = this;
	this.Layer_2.setTransform(0.6,-0.4,1,1,0,0,0,0.6,-0.4);
	this.Layer_2.depth = 0;
	this.Layer_2.isAttachedToCamera = 0
	this.Layer_2.isAttachedToMask = 0
	this.Layer_2.layerDepth = 0
	this.Layer_2.layerIndex = 0
	this.Layer_2.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_2).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol7, new cjs.Rectangle(-48,-307.1,96,613.6), null);


(lib.Symbol6 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2_obj_
	this.Layer_2 = new lib.Symbol_6_Layer_2();
	this.Layer_2.name = "Layer_2";
	this.Layer_2.parent = this;
	this.Layer_2.setTransform(0.4,-0.6,1,1,0,0,0,0.4,-0.6);
	this.Layer_2.depth = 0;
	this.Layer_2.isAttachedToCamera = 0
	this.Layer_2.isAttachedToMask = 0
	this.Layer_2.layerDepth = 0
	this.Layer_2.layerIndex = 0
	this.Layer_2.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_2).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol6, new cjs.Rectangle(-163.5,-289,327,578), null);


(lib.Symbol2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2_obj_
	this.Layer_2 = new lib.Symbol_2_Layer_2();
	this.Layer_2.name = "Layer_2";
	this.Layer_2.parent = this;
	this.Layer_2.setTransform(-0.4,0.2,1,1,0,0,0,-0.4,0.2);
	this.Layer_2.depth = 0;
	this.Layer_2.isAttachedToCamera = 0
	this.Layer_2.isAttachedToMask = 0
	this.Layer_2.layerDepth = 0
	this.Layer_2.layerIndex = 0
	this.Layer_2.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_2).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol2, new cjs.Rectangle(-107.1,-242,213.6,484.4), null);


(lib.Symbol_1_pack3_png = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// pack3_png
	this.instance = new lib.Symbol7();
	this.instance.parent = this;
	this.instance.setTransform(152.4,-14.1,1.4792,1);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(178).to({_off:false},0).to({scaleX:1},5,cjs.Ease.get(1)).wait(67).to({scaleX:2.1031,scaleY:0.8173,x:183.4},3,cjs.Ease.get(-1)).to({_off:true},1).wait(11));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_1_pack2_png_4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// pack2_png
	this.instance = new lib.Symbol9();
	this.instance.parent = this;
	this.instance.setTransform(244.9,7.7,0.5873,1.1485,0,0,0,65.5,-12.4);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(109).to({_off:false},0).to({scaleX:0.9174,scaleY:1.0297,x:244.95,y:-12},4).to({regY:-12.5,scaleX:1,scaleY:1,x:244.9,y:-17},9,cjs.Ease.get(1)).to({_off:true},51).wait(92));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_1_pack2_png_3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// pack2_png
	this.instance = new lib.Symbol10();
	this.instance.parent = this;
	this.instance.setTransform(79.9,-24.1,0.0009,1,0,0,0,-113,-19.6);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(109).to({_off:false},0).to({regX:-112.7,regY:-19.4,scaleX:0.8066,scaleY:1.0974,x:80.05,y:-0.8},1).to({regX:-112.5,regY:-19.3,scaleX:1,scaleY:1.0243,x:76.1,y:-18.2},3).to({scaleY:1,x:67,y:-23.7},9,cjs.Ease.get(1)).to({_off:true},51).wait(92));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_1_pack2_png_2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// pack2_png
	this.instance = new lib.Symbol8();
	this.instance.parent = this;
	this.instance.setTransform(162,-219.4,1,1.1341,0,0,0,-17.4,-214.9);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(109).to({_off:false},0).to({regY:-214.8,scaleY:1.1178},1).to({scaleY:1.0294,y:-219.5},3).to({regY:-215,scaleY:1},9,cjs.Ease.get(1)).to({_off:true},51).wait(92));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_1_pack2_png_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// pack2_png
	this.instance = new lib.Symbol12();
	this.instance.parent = this;
	this.instance.setTransform(171.4,196,0.8634,0.2929,1.7971,0,0,-7.9,158.3);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(110).to({_off:false},0).to({regX:-7.7,regY:158.5,scaleX:0.9044,scaleY:0.9711,rotation:0.7869,x:174.7,y:171.65},2).to({scaleX:0.9522,scaleY:0.9855,rotation:0.2857,x:172.95,y:162.65},1).to({regX:-8.2,regY:158,scaleX:1,scaleY:1,rotation:0,x:171.2,y:153.5},9,cjs.Ease.get(1)).to({_off:true},51).wait(92));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_1_pack2_png = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// pack2_png
	this.instance = new lib.Symbol11();
	this.instance.parent = this;
	this.instance.setTransform(167.4,-219.05,1,0.7516,0,-0.2408,0,-12,-214.6);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(110).to({_off:false},0).to({regY:-214.7,scaleY:0.9379,rotation:-0.0149,skewX:0,x:167.45,y:-219},3).to({regY:-214.3,scaleY:1,rotation:0,x:167.4,y:-218.8},9,cjs.Ease.get(1)).to({_off:true},51).wait(92));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_1_pack1_png_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// pack1_png
	this.instance = new lib.Symbol2();
	this.instance.parent = this;
	this.instance.setTransform(245.35,213.45,1,1,0,0,0,60.1,241.2);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(59).to({regX:60.2,rotation:-1.266,x:245.45,y:213.5},2).to({scaleY:1.0002,rotation:0,skewX:1.0438,y:213.45},2).to({scaleY:1,rotation:-1.266,skewX:0,y:213.5},2).to({scaleY:1.0002,rotation:0,skewX:1.0438,y:213.45},2).to({regX:60.1,scaleY:1,skewX:0,x:245.35},2).to({_off:true},41).wait(155));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_1_pack1_png = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// pack1_png
	this.instance = new lib.Symbol2();
	this.instance.parent = this;
	this.instance.setTransform(245.6,223.45,0.7706,1,0,0,0,60.6,241.2);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(253).to({_off:false},0).wait(1).to({regX:-0.3,regY:0.2,scaleX:0.8905,x:191.25,y:-22.75},0).wait(1).to({scaleX:0.9361,x:188.4,y:-24.75},0).wait(1).to({scaleX:0.962,x:186.8,y:-25.85},0).wait(1).to({scaleX:0.9782,x:185.8,y:-26.6},0).wait(1).to({scaleX:0.9886,x:185.2,y:-27.05},0).wait(1).to({scaleX:0.995,x:184.8,y:-27.3},0).wait(1).to({scaleX:0.9985,x:184.55,y:-27.45},0).wait(1).to({regX:60.1,regY:241.2,scaleX:1,x:245.35,y:213.45},0).wait(4));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_1_Layer_17 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_17
	this.instance = new lib.Symbol6();
	this.instance.parent = this;
	this.instance.setTransform(179.4,-4.45);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(173).to({_off:false},0).to({scaleX:0.3517,x:164.4},4,cjs.Ease.get(-1)).to({_off:true},1).wait(87));

}).prototype = p = new cjs.MovieClip();


(lib.Scene_1_Layer_7 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_7
	this.instance = new lib.Symbol14("synched",0,false);
	this.instance.parent = this;
	this.instance.setTransform(108.1,42.1,0.4585,0.4585);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(265));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_264 = function() {
		this.___loopingOver___ = true;
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(264).call(this.frame_264).wait(1));

	// Layer_5 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	var mask_graphics_261 = new cjs.Graphics().p("AjYJCIAAyDIGxAAIgQSDg");

	this.timeline.addTween(cjs.Tween.get(mask).to({graphics:null,x:0,y:0}).wait(261).to({graphics:mask_graphics_261,x:264.375,y:-213.85}).wait(4));

	// pack_part2_png_obj_
	this.pack_part2_png = new lib.Symbol_1_pack_part2_png();
	this.pack_part2_png.name = "pack_part2_png";
	this.pack_part2_png.parent = this;
	this.pack_part2_png.depth = 0;
	this.pack_part2_png.isAttachedToCamera = 0
	this.pack_part2_png.isAttachedToMask = 0
	this.pack_part2_png.layerDepth = 0
	this.pack_part2_png.layerIndex = 0
	this.pack_part2_png.maskLayerName = 0

	var maskedShapeInstanceList = [this.pack_part2_png];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.pack_part2_png).wait(254).to({regX:266.8,regY:-211,x:266.8,y:-211},0).wait(7).to({regX:0,regY:0,x:0,y:0},0).wait(4));

	// pack1_png_obj_
	this.pack1_png = new lib.Symbol_1_pack1_png();
	this.pack1_png.name = "pack1_png";
	this.pack1_png.parent = this;
	this.pack1_png.depth = 0;
	this.pack1_png.isAttachedToCamera = 0
	this.pack1_png.isAttachedToMask = 0
	this.pack1_png.layerDepth = 0
	this.pack1_png.layerIndex = 1
	this.pack1_png.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.pack1_png).wait(254).to({regX:184.8,regY:-22.6,x:184.8,y:-22.6},0).wait(7).to({regX:0,regY:0,x:0,y:0},0).wait(4));

	// pack3_png_obj_
	this.pack3_png = new lib.Symbol_1_pack3_png();
	this.pack3_png.name = "pack3_png";
	this.pack3_png.parent = this;
	this.pack3_png.depth = 0;
	this.pack3_png.isAttachedToCamera = 0
	this.pack3_png.isAttachedToMask = 0
	this.pack3_png.layerDepth = 0
	this.pack3_png.layerIndex = 2
	this.pack3_png.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.pack3_png).wait(265));

	// Layer_17_obj_
	this.Layer_17 = new lib.Symbol_1_Layer_17();
	this.Layer_17.name = "Layer_17";
	this.Layer_17.parent = this;
	this.Layer_17.depth = 0;
	this.Layer_17.isAttachedToCamera = 0
	this.Layer_17.isAttachedToMask = 0
	this.Layer_17.layerDepth = 0
	this.Layer_17.layerIndex = 3
	this.Layer_17.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_17).wait(265));

	// Layer_13 (mask)
	var mask_1 = new cjs.Shape();
	mask_1._off = true;
	var mask_1_graphics_110 = new cjs.Graphics().p("At1G9IlNi2IAArIMAmFAAAIAALGIm9C9g");
	var mask_1_graphics_111 = new cjs.Graphics().p("At7G9Il1jeIAAqgMAnhAAAIAAKWInxDtg");
	var mask_1_graphics_112 = new cjs.Graphics().p("AuAG9ImdkGIAAp4MAo7AAAIAAJwIojETg");
	var mask_1_graphics_113 = new cjs.Graphics().p("AuEG9InNk+IAApAMAqjAAAIAAI6IpbFJg");
	var mask_1_graphics_114 = new cjs.Graphics().p("AuAG9Inkk8IAApCMArJAAAIAAJCIprFBg");
	var mask_1_graphics_115 = new cjs.Graphics().p("At+G9In2k6IAApEMArpAAAIAAJIIp4E7g");
	var mask_1_graphics_116 = new cjs.Graphics().p("At7G9IoHk4IAApGMAsFAAAIAAJOIqEE1g");
	var mask_1_graphics_117 = new cjs.Graphics().p("At5G9IoUk3IAApHMAscAAAIAAJTIqOEwg");
	var mask_1_graphics_118 = new cjs.Graphics().p("At4G9Iofk2IAApIMAsvAAAIAAJXIqVEsg");
	var mask_1_graphics_119 = new cjs.Graphics().p("At2G9Iopk1IAApJMAs+AAAIAAJaIqbEpg");
	var mask_1_graphics_120 = new cjs.Graphics().p("At1G9Iovk0IAApKMAtJAAAIAAJcIqgEng");
	var mask_1_graphics_121 = new cjs.Graphics().p("At1G9Ioyk0IAApKMAtPAAAIAAJeIqiElg");
	var mask_1_graphics_122 = new cjs.Graphics().p("At1G9Iozk0IAApKMAtRAAAIAAJeIqjElg");

	this.timeline.addTween(cjs.Tween.get(mask_1).to({graphics:null,x:0,y:0}).wait(110).to({graphics:mask_1_graphics_110,x:168.375,y:-263.775}).wait(1).to({graphics:mask_1_graphics_111,x:168.975,y:-263.775}).wait(1).to({graphics:mask_1_graphics_112,x:169.475,y:-263.775}).wait(1).to({graphics:mask_1_graphics_113,x:169.875,y:-263.775}).wait(1).to({graphics:mask_1_graphics_114,x:169.55,y:-263.775}).wait(1).to({graphics:mask_1_graphics_115,x:169.275,y:-263.775}).wait(1).to({graphics:mask_1_graphics_116,x:169.05,y:-263.775}).wait(1).to({graphics:mask_1_graphics_117,x:168.85,y:-263.775}).wait(1).to({graphics:mask_1_graphics_118,x:168.675,y:-263.775}).wait(1).to({graphics:mask_1_graphics_119,x:168.55,y:-263.775}).wait(1).to({graphics:mask_1_graphics_120,x:168.45,y:-263.775}).wait(1).to({graphics:mask_1_graphics_121,x:168.4,y:-263.775}).wait(1).to({graphics:mask_1_graphics_122,x:168.375,y:-263.775}).wait(51).to({graphics:null,x:0,y:0}).wait(92));

	// pack2_png_obj_
	this.pack2_png = new lib.Symbol_1_pack2_png();
	this.pack2_png.name = "pack2_png";
	this.pack2_png.parent = this;
	this.pack2_png.depth = 0;
	this.pack2_png.isAttachedToCamera = 0
	this.pack2_png.isAttachedToMask = 0
	this.pack2_png.layerDepth = 0
	this.pack2_png.layerIndex = 4
	this.pack2_png.maskLayerName = 0

	var maskedShapeInstanceList = [this.pack2_png];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask_1;
	}

	this.timeline.addTween(cjs.Tween.get(this.pack2_png).wait(265));

	// pack2_png_obj_
	this.pack2_png_1 = new lib.Symbol_1_pack2_png_1();
	this.pack2_png_1.name = "pack2_png_1";
	this.pack2_png_1.parent = this;
	this.pack2_png_1.depth = 0;
	this.pack2_png_1.isAttachedToCamera = 0
	this.pack2_png_1.isAttachedToMask = 0
	this.pack2_png_1.layerDepth = 0
	this.pack2_png_1.layerIndex = 5
	this.pack2_png_1.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.pack2_png_1).wait(265));

	// pack2_png_obj_
	this.pack2_png_2 = new lib.Symbol_1_pack2_png_2();
	this.pack2_png_2.name = "pack2_png_2";
	this.pack2_png_2.parent = this;
	this.pack2_png_2.depth = 0;
	this.pack2_png_2.isAttachedToCamera = 0
	this.pack2_png_2.isAttachedToMask = 0
	this.pack2_png_2.layerDepth = 0
	this.pack2_png_2.layerIndex = 6
	this.pack2_png_2.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.pack2_png_2).wait(265));

	// Layer_15 (mask)
	var mask_2 = new cjs.Shape();
	mask_2._off = true;
	var mask_2_graphics_109 = new cjs.Graphics().p("EgRTgmMMAinACEMAAABBXI5kI9g");
	var mask_2_graphics_110 = new cjs.Graphics().p("EgNjgrPIbGOHMAAABBXI5cHBg");
	var mask_2_graphics_111 = new cjs.Graphics().p("EgOtgu3IdbRNMAAABBXI4CLLg");
	var mask_2_graphics_112 = new cjs.Graphics().p("EgOYgwEIcwR9MAAABBYI0WM0g");
	var mask_2_graphics_113 = new cjs.Graphics().p("EgN0guoIboSRMAAABBXI5cJpg");
	var mask_2_graphics_114 = new cjs.Graphics().p("EgNmgujIbNSQMAAABBXI5fJgg");
	var mask_2_graphics_115 = new cjs.Graphics().p("EgNagueIa0SOMAAABBXI5fJYg");
	var mask_2_graphics_116 = new cjs.Graphics().p("EgNPguaIafSNMAAABBXI5hJRg");
	var mask_2_graphics_117 = new cjs.Graphics().p("EgNGguXIaNSMMAAABBXI5iJLg");
	var mask_2_graphics_118 = new cjs.Graphics().p("EgM/guUIZ+SLMAAABBXI5iJHg");
	var mask_2_graphics_119 = new cjs.Graphics().p("EgM5guRIZzSKMAAABBXI5jJCg");
	var mask_2_graphics_120 = new cjs.Graphics().p("EgM1guQIZrSKMAAABBXI5kJAg");
	var mask_2_graphics_121 = new cjs.Graphics().p("EgMyguPIZlSJMAAABBYI5kI+g");
	var mask_2_graphics_122 = new cjs.Graphics().p("EgMyguOIZkSJMAAABBXI5kI9g");

	this.timeline.addTween(cjs.Tween.get(mask_2).to({graphics:null,x:0,y:0}).wait(109).to({graphics:mask_2_graphics_109,x:-30.3,y:11.1}).wait(1).to({graphics:mask_2_graphics_110,x:-6.2,y:-33.725}).wait(1).to({graphics:mask_2_graphics_111,x:-13.7,y:-30.35}).wait(1).to({graphics:mask_2_graphics_112,x:-11.5,y:-27.475}).wait(1).to({graphics:mask_2_graphics_113,x:-7.9,y:-38.625}).wait(1).to({graphics:mask_2_graphics_114,x:-6.525,y:-39}).wait(1).to({graphics:mask_2_graphics_115,x:-5.3,y:-39.325}).wait(1).to({graphics:mask_2_graphics_116,x:-4.225,y:-39.6}).wait(1).to({graphics:mask_2_graphics_117,x:-3.325,y:-39.85}).wait(1).to({graphics:mask_2_graphics_118,x:-2.6,y:-40.05}).wait(1).to({graphics:mask_2_graphics_119,x:-2.025,y:-40.2}).wait(1).to({graphics:mask_2_graphics_120,x:-1.625,y:-40.325}).wait(1).to({graphics:mask_2_graphics_121,x:-1.375,y:-40.375}).wait(1).to({graphics:mask_2_graphics_122,x:-1.3,y:-40.4}).wait(51).to({graphics:null,x:0,y:0}).wait(92));

	// pack2_png_obj_
	this.pack2_png_3 = new lib.Symbol_1_pack2_png_3();
	this.pack2_png_3.name = "pack2_png_3";
	this.pack2_png_3.parent = this;
	this.pack2_png_3.depth = 0;
	this.pack2_png_3.isAttachedToCamera = 0
	this.pack2_png_3.isAttachedToMask = 0
	this.pack2_png_3.layerDepth = 0
	this.pack2_png_3.layerIndex = 7
	this.pack2_png_3.maskLayerName = 0

	var maskedShapeInstanceList = [this.pack2_png_3];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask_2;
	}

	this.timeline.addTween(cjs.Tween.get(this.pack2_png_3).wait(265));

	// Layer_14 (mask)
	var mask_3 = new cjs.Shape();
	mask_3._off = true;
	var mask_3_graphics_109 = new cjs.Graphics().p("AkC9mIIFolMAAABFLIoFHMg");
	var mask_3_graphics_110 = new cjs.Graphics().p("EgEoAkUMAAAhCKIJRpFMgBpBN3g");
	var mask_3_graphics_111 = new cjs.Graphics().p("EgFOAiMMAAAhAnIKdplMgCOBQBg");
	var mask_3_graphics_112 = new cjs.Graphics().p("AlqfYMAAAg/GILVo3MgCLBRLg");
	var mask_3_graphics_113 = new cjs.Graphics().p("AmFeAMAAAg9kIMLoKMgBuBPcg");
	var mask_3_graphics_114 = new cjs.Graphics().p("AmLdlMAAAg9PIMXoBMgBkBPXg");
	var mask_3_graphics_115 = new cjs.Graphics().p("AmQdNMAAAg89IMgn4MgBaBPRg");
	var mask_3_graphics_116 = new cjs.Graphics().p("AmUc3MAAAg8tIMpnwMgBSBPNg");
	var mask_3_graphics_117 = new cjs.Graphics().p("AmYclMAAAg8fIMxnrMgBLBPKg");
	var mask_3_graphics_118 = new cjs.Graphics().p("AmbcXMAAAg8VIM3nlMgBFBPHg");
	var mask_3_graphics_119 = new cjs.Graphics().p("AmdcMMAAAg8NIM7nhMgBBBPFg");
	var mask_3_graphics_120 = new cjs.Graphics().p("AmfcEMAAAg8HIM+neMgA9BPDg");
	var mask_3_graphics_121 = new cjs.Graphics().p("Amgb/MAAAg8DINBndMgA9BPDg");
	var mask_3_graphics_122 = new cjs.Graphics().p("Amgb+MAAAg8CINBncMgA7BPBg");

	this.timeline.addTween(cjs.Tween.get(mask_3).to({graphics:null,x:0,y:0}).wait(109).to({graphics:mask_3_graphics_109,x:270.825,y:-29.825}).wait(1).to({graphics:mask_3_graphics_110,x:274.625,y:-28.225}).wait(1).to({graphics:mask_3_graphics_111,x:278.425,y:-24.5}).wait(1).to({graphics:mask_3_graphics_112,x:281.15,y:-16.2}).wait(1).to({graphics:mask_3_graphics_113,x:283.875,y:-17.25}).wait(1).to({graphics:mask_3_graphics_114,x:284.45,y:-16.575}).wait(1).to({graphics:mask_3_graphics_115,x:284.95,y:-15.975}).wait(1).to({graphics:mask_3_graphics_116,x:285.375,y:-15.425}).wait(1).to({graphics:mask_3_graphics_117,x:285.75,y:-15}).wait(1).to({graphics:mask_3_graphics_118,x:286.05,y:-14.65}).wait(1).to({graphics:mask_3_graphics_119,x:286.275,y:-14.35}).wait(1).to({graphics:mask_3_graphics_120,x:286.45,y:-14.15}).wait(1).to({graphics:mask_3_graphics_121,x:286.55,y:-14.025}).wait(1).to({graphics:mask_3_graphics_122,x:286.575,y:-14}).wait(51).to({graphics:null,x:0,y:0}).wait(92));

	// pack2_png_obj_
	this.pack2_png_4 = new lib.Symbol_1_pack2_png_4();
	this.pack2_png_4.name = "pack2_png_4";
	this.pack2_png_4.parent = this;
	this.pack2_png_4.depth = 0;
	this.pack2_png_4.isAttachedToCamera = 0
	this.pack2_png_4.isAttachedToMask = 0
	this.pack2_png_4.layerDepth = 0
	this.pack2_png_4.layerIndex = 8
	this.pack2_png_4.maskLayerName = 0

	var maskedShapeInstanceList = [this.pack2_png_4];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask_3;
	}

	this.timeline.addTween(cjs.Tween.get(this.pack2_png_4).wait(265));

	// Layer_16_obj_
	this.Layer_16 = new lib.Symbol_1_Layer_16();
	this.Layer_16.name = "Layer_16";
	this.Layer_16.parent = this;
	this.Layer_16.depth = 0;
	this.Layer_16.isAttachedToCamera = 0
	this.Layer_16.isAttachedToMask = 0
	this.Layer_16.layerDepth = 0
	this.Layer_16.layerIndex = 9
	this.Layer_16.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_16).wait(265));

	// Layer_8 (mask)
	var mask_4 = new cjs.Shape();
	mask_4._off = true;
	var mask_4_graphics_0 = new cjs.Graphics().p("AjYJCIAAyDIGxAAIgQSDg");
	var mask_4_graphics_55 = new cjs.Graphics().p("AjYJCIAAyDIGxAAIgQSDg");
	var mask_4_graphics_56 = new cjs.Graphics().p("AjYJCIAAyDIGxAAIgQSDg");
	var mask_4_graphics_57 = new cjs.Graphics().p("AjYJCIAAyDIGxAAIgQSDg");
	var mask_4_graphics_58 = new cjs.Graphics().p("AjYJCIAAyDIGxAAIgQSDg");
	var mask_4_graphics_59 = new cjs.Graphics().p("AjYJCIAAyDIGxAAIgQSDg");
	var mask_4_graphics_60 = new cjs.Graphics().p("AjYJCIAAyDIGxAAIgQSDg");
	var mask_4_graphics_61 = new cjs.Graphics().p("AjYJCIAAyDIGxAAIgQSDg");
	var mask_4_graphics_62 = new cjs.Graphics().p("AjYJCIAAyDIGxAAIgQSDg");
	var mask_4_graphics_63 = new cjs.Graphics().p("AjYJCIAAyDIGxAAIgQSDg");
	var mask_4_graphics_64 = new cjs.Graphics().p("AjYJCIAAyDIGxAAIgQSDg");
	var mask_4_graphics_65 = new cjs.Graphics().p("AjYJCIAAyDIGxAAIgQSDg");
	var mask_4_graphics_66 = new cjs.Graphics().p("AjYJCIAAyDIGxAAIgQSDg");
	var mask_4_graphics_67 = new cjs.Graphics().p("AjYJCIAAyDIGxAAIgQSDg");
	var mask_4_graphics_68 = new cjs.Graphics().p("AjYJCIAAyDIGxAAIgQSDg");
	var mask_4_graphics_69 = new cjs.Graphics().p("AjYJCIAAyDIGxAAIgQSDg");
	var mask_4_graphics_70 = new cjs.Graphics().p("AkbJCIAAyDII3AAIgISDg");
	var mask_4_graphics_71 = new cjs.Graphics().p("AldJCIAAyDIK7AAIAASDg");

	this.timeline.addTween(cjs.Tween.get(mask_4).to({graphics:mask_4_graphics_0,x:264.375,y:-213.85}).wait(55).to({graphics:mask_4_graphics_55,x:264.375,y:-213.85}).wait(1).to({graphics:mask_4_graphics_56,x:264.375,y:-213.85}).wait(1).to({graphics:mask_4_graphics_57,x:264.375,y:-213.85}).wait(1).to({graphics:mask_4_graphics_58,x:264.375,y:-213.85}).wait(1).to({graphics:mask_4_graphics_59,x:264.375,y:-213.85}).wait(1).to({graphics:mask_4_graphics_60,x:260.175,y:-213.85}).wait(1).to({graphics:mask_4_graphics_61,x:255.975,y:-213.85}).wait(1).to({graphics:mask_4_graphics_62,x:264.375,y:-213.85}).wait(1).to({graphics:mask_4_graphics_63,x:272.775,y:-213.85}).wait(1).to({graphics:mask_4_graphics_64,x:264.375,y:-213.85}).wait(1).to({graphics:mask_4_graphics_65,x:255.975,y:-213.85}).wait(1).to({graphics:mask_4_graphics_66,x:264.375,y:-213.85}).wait(1).to({graphics:mask_4_graphics_67,x:272.775,y:-213.85}).wait(1).to({graphics:mask_4_graphics_68,x:268.575,y:-213.85}).wait(1).to({graphics:mask_4_graphics_69,x:264.375,y:-213.85}).wait(1).to({graphics:mask_4_graphics_70,x:271.025,y:-213.85}).wait(1).to({graphics:mask_4_graphics_71,x:277.65,y:-213.85}).wait(39).to({graphics:null,x:0,y:0}).wait(155));

	// pack_part2_png_obj_
	this.pack_part2_png_1 = new lib.Symbol_1_pack_part2_png_1();
	this.pack_part2_png_1.name = "pack_part2_png_1";
	this.pack_part2_png_1.parent = this;
	this.pack_part2_png_1.setTransform(266.9,-217.1,1,1,0,0,0,266.9,-217.1);
	this.pack_part2_png_1.depth = 0;
	this.pack_part2_png_1.isAttachedToCamera = 0
	this.pack_part2_png_1.isAttachedToMask = 0
	this.pack_part2_png_1.layerDepth = 0
	this.pack_part2_png_1.layerIndex = 10
	this.pack_part2_png_1.maskLayerName = 0

	var maskedShapeInstanceList = [this.pack_part2_png_1];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask_4;
	}

	this.timeline.addTween(cjs.Tween.get(this.pack_part2_png_1).wait(56).to({regX:273.9,regY:-212.8,x:273.9,y:-212.8},0).wait(3).to({regX:266.9,regY:-217.1,x:266.9,y:-217.1},0).wait(1).to({regX:273.9,regY:-212.8,x:273.9,y:-212.8},0).wait(1).to({regX:266.9,regY:-217.1,x:266.9,y:-217.1},0).wait(1).to({regX:273.9,regY:-212.8,x:273.9,y:-212.8},0).wait(1).to({regX:266.9,regY:-217.1,x:266.9,y:-217.1},0).wait(1).to({regX:273.9,regY:-212.8,x:273.9,y:-212.8},0).wait(1).to({regX:266.9,regY:-217.1,x:266.9,y:-217.1},0).wait(1).to({regX:273.9,regY:-212.8,x:273.9,y:-212.8},0).wait(1).to({regX:266.9,regY:-217.1,x:266.9,y:-217.1},0).wait(1).to({regX:273.9,regY:-212.8,x:273.9,y:-212.8},0).wait(1).to({regX:266.9,regY:-217.1,x:266.9,y:-217.1},0).wait(1).to({regX:273.9,regY:-212.8,x:273.9,y:-212.8},0).wait(3).to({regX:266.9,regY:-217.1,x:266.9,y:-217.1},0).wait(192));

	// Layer_2 (mask)
	var mask_5 = new cjs.Shape();
	mask_5._off = true;
	var mask_5_graphics_69 = new cjs.Graphics().p("Ai1OZIAA2bIFrmWIAAcxg");

	this.timeline.addTween(cjs.Tween.get(mask_5).to({graphics:null,x:0,y:0}).wait(69).to({graphics:mask_5_graphics_69,x:266.875,y:-169.375}).wait(6).to({graphics:null,x:0,y:0}).wait(190));

	// pack_part2_shadow_png_obj_
	this.pack_part2_shadow_png = new lib.Symbol_1_pack_part2_shadow_png();
	this.pack_part2_shadow_png.name = "pack_part2_shadow_png";
	this.pack_part2_shadow_png.parent = this;
	this.pack_part2_shadow_png.depth = 0;
	this.pack_part2_shadow_png.isAttachedToCamera = 0
	this.pack_part2_shadow_png.isAttachedToMask = 0
	this.pack_part2_shadow_png.layerDepth = 0
	this.pack_part2_shadow_png.layerIndex = 11
	this.pack_part2_shadow_png.maskLayerName = 0

	var maskedShapeInstanceList = [this.pack_part2_shadow_png];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask_5;
	}

	this.timeline.addTween(cjs.Tween.get(this.pack_part2_shadow_png).wait(70).to({regX:260.6,regY:-184.8,x:260.6,y:-184.8},0).wait(3).to({regX:0,regY:0,x:0,y:0},0).wait(192));

	// pack1_png_obj_
	this.pack1_png_1 = new lib.Symbol_1_pack1_png_1();
	this.pack1_png_1.name = "pack1_png_1";
	this.pack1_png_1.parent = this;
	this.pack1_png_1.setTransform(184.9,-27.6,1,1,0,0,0,184.9,-27.6);
	this.pack1_png_1.depth = 0;
	this.pack1_png_1.isAttachedToCamera = 0
	this.pack1_png_1.isAttachedToMask = 0
	this.pack1_png_1.layerDepth = 0
	this.pack1_png_1.layerIndex = 12
	this.pack1_png_1.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.pack1_png_1).wait(265));

	// pack_part1_png_obj_
	this.pack_part1_png = new lib.Symbol_1_pack_part1_png();
	this.pack_part1_png.name = "pack_part1_png";
	this.pack_part1_png.parent = this;
	this.pack_part1_png.depth = 0;
	this.pack_part1_png.isAttachedToCamera = 0
	this.pack_part1_png.isAttachedToMask = 0
	this.pack_part1_png.layerDepth = 0
	this.pack_part1_png.layerIndex = 13
	this.pack_part1_png.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.pack_part1_png).wait(70).to({regX:77.5,regY:-194,x:77.5,y:-194},0).wait(3).to({regX:0,regY:0,x:0,y:0},0).wait(192));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(17,-321.2,325.6,671.7);


(lib.Scene_1_Layer_11 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_11
	this.instance = new lib.Symbol1("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(79.75,306.65,0.4527,0.4527,0,0,0,0,0.2);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(74).to({startPosition:74,loop:false},0).wait(1).to({regX:179.8,regY:-11.2,x:161.15,y:301.5,startPosition:75},0).wait(1).to({startPosition:76},0).wait(1).to({startPosition:77},0).wait(1).to({x:161.2,startPosition:78},0).wait(1).to({x:161.25,startPosition:79},0).wait(1).to({x:161.35,startPosition:80},0).wait(1).to({x:161.45,startPosition:81},0).wait(1).to({x:161.55,startPosition:82},0).wait(1).to({x:161.65,startPosition:83},0).wait(1).to({x:161.75,startPosition:84},0).wait(1).to({x:161.8,startPosition:85},0).wait(1).to({x:161.9,startPosition:86},0).wait(1).to({x:161.95,startPosition:87},0).wait(1).to({startPosition:88},0).wait(1).to({x:162,startPosition:89},0).wait(1).to({startPosition:90},0).wait(1).to({x:162.05,startPosition:91},0).wait(1).to({startPosition:92},0).wait(1).to({startPosition:93},0).wait(1).to({regX:0.1,regY:0.2,x:80.7,y:306.65,startPosition:94},0).to({x:111.3,startPosition:169},75).wait(1).to({regX:179.8,regY:-11.2,x:192.9,y:301.5,startPosition:170},0).wait(1).to({x:193.9,startPosition:171},0).wait(1).to({x:195.8,startPosition:172},0).wait(1).to({x:198.75,startPosition:173},0).wait(1).to({x:203.05,startPosition:174},0).wait(1).to({x:208.8,startPosition:175},0).wait(1).to({x:215.85,startPosition:176},0).wait(1).to({x:223.5,startPosition:177},0).wait(1).to({x:230.85,startPosition:178},0).wait(1).to({x:237.4,startPosition:179},0).wait(1).to({x:242.9,startPosition:180},0).wait(1).to({x:247.35,startPosition:181},0).wait(1).to({x:251,startPosition:182},0).wait(1).to({x:253.95,startPosition:183},0).wait(1).to({x:256.25,startPosition:184},0).wait(1).to({x:258.05,startPosition:185},0).wait(1).to({x:259.45,startPosition:186},0).wait(1).to({x:260.45,startPosition:187},0).wait(1).to({x:261.15,startPosition:188},0).wait(1).to({regX:0.4,regY:0.2,x:180.3,y:306.65,startPosition:189},0).wait(55).to({startPosition:244},0).wait(1).to({regX:179.8,regY:-11.2,x:261.05,y:301.5,startPosition:245},0).wait(1).to({x:259.6,startPosition:246},0).wait(1).to({x:256.85,startPosition:247},0).wait(1).to({x:252.55,startPosition:248},0).wait(1).to({x:246.3,startPosition:249},0).wait(1).to({x:237.9,startPosition:250},0).wait(1).to({x:227.65,startPosition:251},0).wait(1).to({x:216.5,startPosition:252},0).wait(1).to({x:205.75,startPosition:253},0).wait(1).to({x:196.25,startPosition:254},0).wait(1).to({x:188.25,startPosition:255},0).wait(1).to({x:181.7,startPosition:256},0).wait(1).to({x:176.4,startPosition:257},0).wait(1).to({x:172.1,startPosition:258},0).wait(1).to({x:168.75,startPosition:259},0).wait(1).to({x:166.1,startPosition:260},0).wait(1).to({x:164.1,startPosition:261},0).wait(1).to({x:162.6,startPosition:262},0).wait(1).to({x:161.6,startPosition:263},0).wait(1).to({regX:0,regY:0.2,x:79.75,y:306.65,startPosition:264},0).wait(1));

}).prototype = p = new cjs.MovieClip();


// stage content:
(lib._320x454 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	this.___GetDepth___ = function(obj) {
		var depth = obj.depth;
		var cameraObj = this.___camera___instance;
		if(cameraObj && cameraObj.depth && obj.isAttachedToCamera)
		{
			depth += depth + cameraObj.depth;
		}
		return depth;
		}
	this.___needSorting___ = function() {
		for (var i = 0; i < this.getNumChildren() - 1; i++)
		{
			var prevDepth = this.___GetDepth___(this.getChildAt(i));
			var nextDepth = this.___GetDepth___(this.getChildAt(i + 1));
			if (prevDepth < nextDepth)
				return true;
		}
		return false;
	}
	this.___sortFunction___ = function(obj1, obj2) {
		return (this.exportRoot.___GetDepth___(obj2) - this.exportRoot.___GetDepth___(obj1));
	}
	this.on('tick', function (event){
		var curTimeline = event.currentTarget;
		if (curTimeline.___needSorting___()){
			this.sortChildren(curTimeline.___sortFunction___);
		}
	});

	// timeline functions:
	this.frame_264 = function() {
		this.___loopingOver___ = true;
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(264).call(this.frame_264).wait(1));

	// Layer_11_obj_
	this.Layer_11 = new lib.Scene_1_Layer_11();
	this.Layer_11.name = "Layer_11";
	this.Layer_11.parent = this;
	this.Layer_11.setTransform(145.8,293.7,1,1,0,0,0,145.8,293.7);
	this.Layer_11.depth = 0;
	this.Layer_11.isAttachedToCamera = 0
	this.Layer_11.isAttachedToMask = 0
	this.Layer_11.layerDepth = 0
	this.Layer_11.layerIndex = 0
	this.Layer_11.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_11).wait(75).to({regX:211.3,regY:301.5,x:211.3,y:301.5},0).wait(19).to({regX:145.8,regY:293.7,x:145.8,y:293.7},0).wait(76).to({regX:211.3,regY:301.5,x:211.3,y:301.5},0).wait(19).to({regX:145.8,regY:293.7,x:145.8,y:293.7},0).wait(56).to({regX:211.3,regY:301.5,x:211.3,y:301.5},0).wait(19).to({regX:145.8,regY:293.7,x:145.8,y:293.7},0).wait(1));

	// Layer_8 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	var mask_graphics_14 = new cjs.Graphics().p("EhwfA1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_graphics_15 = new cjs.Graphics().p("EhweA1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_graphics_16 = new cjs.Graphics().p("EhwcA1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_graphics_17 = new cjs.Graphics().p("EhwbA1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_graphics_18 = new cjs.Graphics().p("EhwZA1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_graphics_19 = new cjs.Graphics().p("EhwYA1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_graphics_20 = new cjs.Graphics().p("EhwWA1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_graphics_21 = new cjs.Graphics().p("EhwVA1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_graphics_22 = new cjs.Graphics().p("EhwTA1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_graphics_23 = new cjs.Graphics().p("EhwSA1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_graphics_24 = new cjs.Graphics().p("EhwRA1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_graphics_25 = new cjs.Graphics().p("EhwPA1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_graphics_26 = new cjs.Graphics().p("EhwOA1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_graphics_27 = new cjs.Graphics().p("EhwMA1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_graphics_28 = new cjs.Graphics().p("EhwLA1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_graphics_29 = new cjs.Graphics().p("EhwJA1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_graphics_30 = new cjs.Graphics().p("EhwIA1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_graphics_31 = new cjs.Graphics().p("EhwHA1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_graphics_32 = new cjs.Graphics().p("EhwFA1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_graphics_33 = new cjs.Graphics().p("EhwEA1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_graphics_34 = new cjs.Graphics().p("EhwCA1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_graphics_35 = new cjs.Graphics().p("EhwBA1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_graphics_36 = new cjs.Graphics().p("Ehv/A1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_graphics_37 = new cjs.Graphics().p("Ehv+A1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_graphics_38 = new cjs.Graphics().p("Ehv8A1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_graphics_39 = new cjs.Graphics().p("Ehv7A1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_graphics_40 = new cjs.Graphics().p("Ehv6A1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_graphics_41 = new cjs.Graphics().p("Ehv4A1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_graphics_42 = new cjs.Graphics().p("Ehv3A1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_graphics_43 = new cjs.Graphics().p("Ehv1A1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_graphics_44 = new cjs.Graphics().p("Ehv0A1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_graphics_45 = new cjs.Graphics().p("EhvyA1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_graphics_46 = new cjs.Graphics().p("EhvxA1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_graphics_47 = new cjs.Graphics().p("EhvvA1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_graphics_48 = new cjs.Graphics().p("EhvuA1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_graphics_49 = new cjs.Graphics().p("EhvtA1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_graphics_50 = new cjs.Graphics().p("EhvrA1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_graphics_51 = new cjs.Graphics().p("EhvqA1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_graphics_52 = new cjs.Graphics().p("EhvoA1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_graphics_53 = new cjs.Graphics().p("EhvnA1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_graphics_54 = new cjs.Graphics().p("EhvlA1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_graphics_55 = new cjs.Graphics().p("EhvkA1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_graphics_56 = new cjs.Graphics().p("EhvjA1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_graphics_57 = new cjs.Graphics().p("EhvhA1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_graphics_58 = new cjs.Graphics().p("EhvgA1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_graphics_59 = new cjs.Graphics().p("EhveA1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_graphics_60 = new cjs.Graphics().p("EhvdA1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_graphics_61 = new cjs.Graphics().p("EhvcA1IMAAAhqPMDhAAAAMAAABqPg");
	var mask_graphics_62 = new cjs.Graphics().p("EhvaA1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_graphics_63 = new cjs.Graphics().p("EhvYA1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_graphics_64 = new cjs.Graphics().p("EhvXA1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_graphics_65 = new cjs.Graphics().p("EhvWA1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_graphics_66 = new cjs.Graphics().p("EhvUA1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_graphics_67 = new cjs.Graphics().p("EhvTA1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_graphics_68 = new cjs.Graphics().p("EhvRA1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_graphics_69 = new cjs.Graphics().p("EhvQA1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_graphics_70 = new cjs.Graphics().p("EhvOA1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_graphics_71 = new cjs.Graphics().p("EhvNA1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_graphics_72 = new cjs.Graphics().p("EhvMA1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_graphics_73 = new cjs.Graphics().p("EhvKA1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_graphics_74 = new cjs.Graphics().p("EhvJA1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_graphics_75 = new cjs.Graphics().p("EhvAA1IMAAAhqPMDguAAAMAAABqPg");
	var mask_graphics_76 = new cjs.Graphics().p("EhuiA1IMAAAhqPMDf1AAAMAAABqPg");
	var mask_graphics_77 = new cjs.Graphics().p("EhtpA1IMAAAhqPMDeIAAAMAAABqPg");
	var mask_graphics_78 = new cjs.Graphics().p("EhsPA1IMAAAhqPMDbcAAAMAAABqPg");
	var mask_graphics_79 = new cjs.Graphics().p("EhqOA1IMAAAhqPMDXlAAAMAAABqPg");
	var mask_graphics_80 = new cjs.Graphics().p("EhnfA1IMAAAhqPMDSXAAAMAAABqPg");
	var mask_graphics_81 = new cjs.Graphics().p("EhkKA1IMAAAhqPMDMAAAAMAAABqPg");
	var mask_graphics_82 = new cjs.Graphics().p("EhgiA1IMAAAhqPMDFFAAAMAAABqPg");
	var mask_graphics_83 = new cjs.Graphics().p("EhdDA1IMAAAhqPMC+bAAAMAAABqPg");
	var mask_graphics_84 = new cjs.Graphics().p("EhZ9A1IMAAAhqPMC4hAAAMAAABqPg");
	var mask_graphics_85 = new cjs.Graphics().p("EhXXA1IMAAAhqPMCzkAAAMAAABqPg");
	var mask_graphics_86 = new cjs.Graphics().p("EhVPA1IMAAAhqPMCvgAAAMAAABqPg");
	var mask_graphics_87 = new cjs.Graphics().p("EhThA1IMAAAhqPMCsNAAAMAAABqPg");
	var mask_graphics_88 = new cjs.Graphics().p("EhSIA1IMAAAhqPMCpkAAAMAAABqPg");
	var mask_graphics_89 = new cjs.Graphics().p("EhRCA1IMAAAhqPMCneAAAMAAABqPg");
	var mask_graphics_90 = new cjs.Graphics().p("EhQLA1IMAAAhqPMCl1AAAMAAABqPg");
	var mask_graphics_91 = new cjs.Graphics().p("EhPhA1IMAAAhqPMCklAAAMAAABqPg");
	var mask_graphics_92 = new cjs.Graphics().p("EhPCA1IMAAAhqPMCjqAAAMAAABqPg");
	var mask_graphics_93 = new cjs.Graphics().p("EhOtA1IMAAAhqPMCjBAAAMAAABqPg");
	var mask_graphics_94 = new cjs.Graphics().p("EhOgA1IMAAAhqPMCipAAAMAAABqPg");
	var mask_graphics_95 = new cjs.Graphics().p("EhOfA1IMAAAhqPMCioAAAMAAABqPg");
	var mask_graphics_96 = new cjs.Graphics().p("EhOfA1IMAAAhqPMCipAAAMAAABqPg");
	var mask_graphics_97 = new cjs.Graphics().p("EhOeA1IMAAAhqPMCipAAAMAAABqPg");
	var mask_graphics_98 = new cjs.Graphics().p("EhOdA1IMAAAhqPMCipAAAMAAABqPg");
	var mask_graphics_99 = new cjs.Graphics().p("EhOcA1IMAAAhqPMCipAAAMAAABqPg");
	var mask_graphics_100 = new cjs.Graphics().p("EhObA1IMAAAhqPMCipAAAMAAABqPg");
	var mask_graphics_101 = new cjs.Graphics().p("EhOaA1IMAAAhqPMCipAAAMAAABqPg");
	var mask_graphics_102 = new cjs.Graphics().p("EhOZA1IMAAAhqPMCipAAAMAAABqPg");
	var mask_graphics_103 = new cjs.Graphics().p("EhOYA1IMAAAhqPMCioAAAMAAABqPg");
	var mask_graphics_104 = new cjs.Graphics().p("EhOXA1IMAAAhqPMCioAAAMAAABqPg");
	var mask_graphics_105 = new cjs.Graphics().p("EhOWA1IMAAAhqPMCioAAAMAAABqPg");
	var mask_graphics_106 = new cjs.Graphics().p("EhOWA1IMAAAhqPMCipAAAMAAABqPg");
	var mask_graphics_107 = new cjs.Graphics().p("EhOVA1IMAAAhqPMCipAAAMAAABqPg");
	var mask_graphics_108 = new cjs.Graphics().p("EhOUA1IMAAAhqPMCipAAAMAAABqPg");
	var mask_graphics_109 = new cjs.Graphics().p("EhOTA1IMAAAhqPMCipAAAMAAABqPg");
	var mask_graphics_110 = new cjs.Graphics().p("EhOSA1IMAAAhqPMCipAAAMAAABqPg");
	var mask_graphics_111 = new cjs.Graphics().p("EhORA1IMAAAhqPMCipAAAMAAABqPg");
	var mask_graphics_112 = new cjs.Graphics().p("EhOQA1IMAAAhqPMCipAAAMAAABqPg");
	var mask_graphics_113 = new cjs.Graphics().p("EhOPA1IMAAAhqPMCioAAAMAAABqPg");
	var mask_graphics_114 = new cjs.Graphics().p("EhOOA1IMAAAhqPMCioAAAMAAABqPg");
	var mask_graphics_115 = new cjs.Graphics().p("EhONA1IMAAAhqPMCioAAAMAAABqPg");
	var mask_graphics_116 = new cjs.Graphics().p("EhONA1IMAAAhqPMCipAAAMAAABqPg");
	var mask_graphics_117 = new cjs.Graphics().p("EhOMA1IMAAAhqPMCipAAAMAAABqPg");
	var mask_graphics_118 = new cjs.Graphics().p("EhOLA1IMAAAhqPMCipAAAMAAABqPg");
	var mask_graphics_119 = new cjs.Graphics().p("EhOKA1IMAAAhqPMCipAAAMAAABqPg");
	var mask_graphics_120 = new cjs.Graphics().p("EhOJA1IMAAAhqPMCipAAAMAAABqPg");
	var mask_graphics_121 = new cjs.Graphics().p("EhOIA1IMAAAhqPMCipAAAMAAABqPg");
	var mask_graphics_122 = new cjs.Graphics().p("EhOHA1IMAAAhqPMCipAAAMAAABqPg");
	var mask_graphics_123 = new cjs.Graphics().p("EhOGA1IMAAAhqPMCioAAAMAAABqPg");
	var mask_graphics_124 = new cjs.Graphics().p("EhOFA1IMAAAhqPMCioAAAMAAABqPg");
	var mask_graphics_125 = new cjs.Graphics().p("EhOEA1IMAAAhqPMCioAAAMAAABqPg");
	var mask_graphics_126 = new cjs.Graphics().p("EhOEA1IMAAAhqPMCipAAAMAAABqPg");
	var mask_graphics_127 = new cjs.Graphics().p("EhODA1IMAAAhqPMCipAAAMAAABqPg");
	var mask_graphics_128 = new cjs.Graphics().p("EhOCA1IMAAAhqPMCipAAAMAAABqPg");
	var mask_graphics_129 = new cjs.Graphics().p("EhOBA1IMAAAhqPMCipAAAMAAABqPg");
	var mask_graphics_130 = new cjs.Graphics().p("EhOAA1IMAAAhqPMCipAAAMAAABqPg");
	var mask_graphics_131 = new cjs.Graphics().p("EhN/A1IMAAAhqPMCipAAAMAAABqPg");
	var mask_graphics_132 = new cjs.Graphics().p("EhN+A1IMAAAhqPMCipAAAMAAABqPg");
	var mask_graphics_133 = new cjs.Graphics().p("EhN9A1IMAAAhqPMCioAAAMAAABqPg");
	var mask_graphics_134 = new cjs.Graphics().p("EhN8A1IMAAAhqPMCioAAAMAAABqPg");
	var mask_graphics_135 = new cjs.Graphics().p("EhN7A1IMAAAhqPMCioAAAMAAABqPg");
	var mask_graphics_136 = new cjs.Graphics().p("EhN7A1IMAAAhqPMCipAAAMAAABqPg");
	var mask_graphics_137 = new cjs.Graphics().p("EhN6A1IMAAAhqPMCipAAAMAAABqPg");
	var mask_graphics_138 = new cjs.Graphics().p("EhN5A1IMAAAhqPMCipAAAMAAABqPg");
	var mask_graphics_139 = new cjs.Graphics().p("EhN4A1IMAAAhqPMCipAAAMAAABqPg");
	var mask_graphics_140 = new cjs.Graphics().p("EhN3A1IMAAAhqPMCipAAAMAAABqPg");
	var mask_graphics_141 = new cjs.Graphics().p("EhN2A1IMAAAhqPMCipAAAMAAABqPg");
	var mask_graphics_142 = new cjs.Graphics().p("EhN1A1IMAAAhqPMCipAAAMAAABqPg");
	var mask_graphics_143 = new cjs.Graphics().p("EhN0A1IMAAAhqPMCioAAAMAAABqPg");
	var mask_graphics_144 = new cjs.Graphics().p("EhNzA1IMAAAhqPMCioAAAMAAABqPg");
	var mask_graphics_145 = new cjs.Graphics().p("EhNyA1IMAAAhqPMCioAAAMAAABqPg");
	var mask_graphics_146 = new cjs.Graphics().p("EhNyA1IMAAAhqPMCipAAAMAAABqPg");
	var mask_graphics_147 = new cjs.Graphics().p("EhNxA1IMAAAhqPMCipAAAMAAABqPg");
	var mask_graphics_148 = new cjs.Graphics().p("EhNwA1IMAAAhqPMCipAAAMAAABqPg");
	var mask_graphics_149 = new cjs.Graphics().p("EhNvA1IMAAAhqPMCipAAAMAAABqPg");
	var mask_graphics_150 = new cjs.Graphics().p("EhNuA1IMAAAhqPMCipAAAMAAABqPg");
	var mask_graphics_151 = new cjs.Graphics().p("EhNtA1IMAAAhqPMCipAAAMAAABqPg");
	var mask_graphics_152 = new cjs.Graphics().p("EhNsA1IMAAAhqPMCipAAAMAAABqPg");
	var mask_graphics_153 = new cjs.Graphics().p("EhNrA1IMAAAhqPMCioAAAMAAABqPg");
	var mask_graphics_154 = new cjs.Graphics().p("EhNqA1IMAAAhqPMCioAAAMAAABqPg");
	var mask_graphics_155 = new cjs.Graphics().p("EhNpA1IMAAAhqPMCioAAAMAAABqPg");
	var mask_graphics_156 = new cjs.Graphics().p("EhNpA1IMAAAhqPMCipAAAMAAABqPg");
	var mask_graphics_157 = new cjs.Graphics().p("EhNoA1IMAAAhqPMCipAAAMAAABqPg");
	var mask_graphics_158 = new cjs.Graphics().p("EhNnA1IMAAAhqPMCipAAAMAAABqPg");
	var mask_graphics_159 = new cjs.Graphics().p("EhNmA1IMAAAhqPMCipAAAMAAABqPg");
	var mask_graphics_160 = new cjs.Graphics().p("EhNlA1IMAAAhqPMCipAAAMAAABqPg");
	var mask_graphics_161 = new cjs.Graphics().p("EhNkA1IMAAAhqPMCipAAAMAAABqPg");
	var mask_graphics_162 = new cjs.Graphics().p("EhNjA1IMAAAhqPMCipAAAMAAABqPg");
	var mask_graphics_163 = new cjs.Graphics().p("EhNiA1IMAAAhqPMCioAAAMAAABqPg");
	var mask_graphics_164 = new cjs.Graphics().p("EhNhA1IMAAAhqPMCioAAAMAAABqPg");
	var mask_graphics_165 = new cjs.Graphics().p("EhNgA1IMAAAhqPMCioAAAMAAABqPg");
	var mask_graphics_166 = new cjs.Graphics().p("EhNgA1IMAAAhqPMCipAAAMAAABqPg");
	var mask_graphics_167 = new cjs.Graphics().p("EhNfA1IMAAAhqPMCipAAAMAAABqPg");
	var mask_graphics_168 = new cjs.Graphics().p("EhNeA1IMAAAhqPMCipAAAMAAABqPg");
	var mask_graphics_169 = new cjs.Graphics().p("EhNdA1IMAAAhqPMCipAAAMAAABqPg");
	var mask_graphics_170 = new cjs.Graphics().p("EhNPA1IMAAAhqPMCiTAAAMAAABqPg");
	var mask_graphics_171 = new cjs.Graphics().p("EhMiA1IMAAAhqPMChLAAAMAAABqPg");
	var mask_graphics_172 = new cjs.Graphics().p("EhLNA1IMAAAhqPMCfDAAAMAAABqPg");
	var mask_graphics_173 = new cjs.Graphics().p("EhJGA1IMAAAhqPMCbqAAAMAAABqPg");
	var mask_graphics_174 = new cjs.Graphics().p("EhGEA1IMAAAhqPMCWzAAAMAAABqPg");
	var mask_graphics_175 = new cjs.Graphics().p("EhB+A1IMAAAhqPMCQPAAAMAAABqPg");
	var mask_graphics_176 = new cjs.Graphics().p("Eg8/A1IMAAAhqPMCIPAAAMAAABqPg");
	var mask_graphics_177 = new cjs.Graphics().p("Eg3jA1IMAAAhqPMB/iAAAMAAABqPg");
	var mask_graphics_178 = new cjs.Graphics().p("EgyUA1IMAAAhqPMB3IAAAMAAABqPg");
	var mask_graphics_179 = new cjs.Graphics().p("EgtrA1IMAAAhqPMBvsAAAMAAABqPg");
	var mask_graphics_180 = new cjs.Graphics().p("EgpyA1IMAAAhqPMBpdAAAMAAABqPg");
	var mask_graphics_181 = new cjs.Graphics().p("EgmmA1IMAAAhqPMBkWAAAMAAABqPg");
	var mask_graphics_182 = new cjs.Graphics().p("EgkBA1IMAAAhqPMBgNAAAMAAABqPg");
	var mask_graphics_183 = new cjs.Graphics().p("Egh8A1IMAAAhqPMBc4AAAMAAABqPg");
	var mask_graphics_184 = new cjs.Graphics().p("EggSA1IMAAAhqPMBaOAAAMAAABqPg");
	var mask_graphics_185 = new cjs.Graphics().p("EgfAA1IMAAAhqPMBYLAAAMAAABqPg");
	var mask_graphics_186 = new cjs.Graphics().p("EgeBA1IMAAAhqPMBWmAAAMAAABqPg");
	var mask_graphics_187 = new cjs.Graphics().p("EgdTA1IMAAAhqPMBVcAAAMAAABqPg");
	var mask_graphics_188 = new cjs.Graphics().p("EgczA1IMAAAhqPMBUpAAAMAAABqPg");
	var mask_graphics_189 = new cjs.Graphics().p("EgchA1IMAAAhqPMBULAAAMAAABqPg");
	var mask_graphics_244 = new cjs.Graphics().p("EgchA1IMAAAhqPMBULAAAMAAABqPg");
	var mask_graphics_245 = new cjs.Graphics().p("Egc4A1IMAAAhqPMBUyAAAMAAABqPg");
	var mask_graphics_246 = new cjs.Graphics().p("EgeFA1IMAAAhqPMBWzAAAMAAABqPg");
	var mask_graphics_247 = new cjs.Graphics().p("EggXA1IMAAAhqPMBaoAAAMAAABqPg");
	var mask_graphics_248 = new cjs.Graphics().p("Egj+A1IMAAAhqPMBgrAAAMAAABqPg");
	var mask_graphics_249 = new cjs.Graphics().p("EgpMA1IMAAAhqPMBpbAAAMAAABqPg");
	var mask_graphics_250 = new cjs.Graphics().p("EgwNA1IMAAAhqPMB1MAAAMAAABqPg");
	var mask_graphics_251 = new cjs.Graphics().p("Eg4xA1IMAAAhqPMCDjAAAMAAABqPg");
	var mask_graphics_252 = new cjs.Graphics().p("EhCGA1IMAAAhqPMCTMAAAMAAABqPg");
	var mask_graphics_253 = new cjs.Graphics().p("EhLFA1IMAAAhqPMCiQAAAMAAABqPg");
	var mask_graphics_254 = new cjs.Graphics().p("EhTCA1IMAAAhqPMCvlAAAMAAABqPg");
	var mask_graphics_255 = new cjs.Graphics().p("EhZtA1IMAAAhqPMC6xAAAMAAABqPg");
	var mask_graphics_256 = new cjs.Graphics().p("EhfMA1IMAAAhqPMDD9AAAMAAABqPg");
	var mask_graphics_257 = new cjs.Graphics().p("EhjnA1IMAAAhqPMDLYAAAMAAABqPg");
	var mask_graphics_258 = new cjs.Graphics().p("EhnLA1IMAAAhqPMDRXAAAMAAABqPg");
	var mask_graphics_259 = new cjs.Graphics().p("EhqAA1IMAAAhqPMDWGAAAMAAABqPg");
	var mask_graphics_260 = new cjs.Graphics().p("EhsNA1IMAAAhqPMDZyAAAMAAABqPg");
	var mask_graphics_261 = new cjs.Graphics().p("Eht5A1IMAAAhqPMDcnAAAMAAABqPg");
	var mask_graphics_262 = new cjs.Graphics().p("EhvIA1IMAAAhqPMDesAAAMAAABqPg");
	var mask_graphics_263 = new cjs.Graphics().p("Ehv/A1IMAAAhqPMDgIAAAMAAABqPg");
	var mask_graphics_264 = new cjs.Graphics().p("EhwfA1IMAAAhqPMDg/AAAMAAABqPg");

	this.timeline.addTween(cjs.Tween.get(mask).to({graphics:null,x:0,y:0}).wait(14).to({graphics:mask_graphics_14,x:720,y:340}).wait(1).to({graphics:mask_graphics_15,x:720.15,y:340}).wait(1).to({graphics:mask_graphics_16,x:720.3,y:340}).wait(1).to({graphics:mask_graphics_17,x:720.425,y:340}).wait(1).to({graphics:mask_graphics_18,x:720.575,y:340}).wait(1).to({graphics:mask_graphics_19,x:720.725,y:340}).wait(1).to({graphics:mask_graphics_20,x:720.875,y:340}).wait(1).to({graphics:mask_graphics_21,x:721,y:340}).wait(1).to({graphics:mask_graphics_22,x:721.15,y:340}).wait(1).to({graphics:mask_graphics_23,x:721.3,y:340}).wait(1).to({graphics:mask_graphics_24,x:721.45,y:340}).wait(1).to({graphics:mask_graphics_25,x:721.575,y:340}).wait(1).to({graphics:mask_graphics_26,x:721.725,y:340}).wait(1).to({graphics:mask_graphics_27,x:721.875,y:340}).wait(1).to({graphics:mask_graphics_28,x:722.025,y:340}).wait(1).to({graphics:mask_graphics_29,x:722.15,y:340}).wait(1).to({graphics:mask_graphics_30,x:722.3,y:340}).wait(1).to({graphics:mask_graphics_31,x:722.45,y:340}).wait(1).to({graphics:mask_graphics_32,x:722.6,y:340}).wait(1).to({graphics:mask_graphics_33,x:722.725,y:340}).wait(1).to({graphics:mask_graphics_34,x:722.875,y:340}).wait(1).to({graphics:mask_graphics_35,x:723.025,y:340}).wait(1).to({graphics:mask_graphics_36,x:723.175,y:340}).wait(1).to({graphics:mask_graphics_37,x:723.3,y:340}).wait(1).to({graphics:mask_graphics_38,x:723.45,y:340}).wait(1).to({graphics:mask_graphics_39,x:723.6,y:340}).wait(1).to({graphics:mask_graphics_40,x:723.75,y:340}).wait(1).to({graphics:mask_graphics_41,x:723.875,y:340}).wait(1).to({graphics:mask_graphics_42,x:724.025,y:340}).wait(1).to({graphics:mask_graphics_43,x:724.175,y:340}).wait(1).to({graphics:mask_graphics_44,x:724.325,y:340}).wait(1).to({graphics:mask_graphics_45,x:724.45,y:340}).wait(1).to({graphics:mask_graphics_46,x:724.6,y:340}).wait(1).to({graphics:mask_graphics_47,x:724.75,y:340}).wait(1).to({graphics:mask_graphics_48,x:724.9,y:340}).wait(1).to({graphics:mask_graphics_49,x:725.025,y:340}).wait(1).to({graphics:mask_graphics_50,x:725.175,y:340}).wait(1).to({graphics:mask_graphics_51,x:725.325,y:340}).wait(1).to({graphics:mask_graphics_52,x:725.475,y:340}).wait(1).to({graphics:mask_graphics_53,x:725.6,y:340}).wait(1).to({graphics:mask_graphics_54,x:725.75,y:340}).wait(1).to({graphics:mask_graphics_55,x:725.9,y:340}).wait(1).to({graphics:mask_graphics_56,x:726.05,y:340}).wait(1).to({graphics:mask_graphics_57,x:726.175,y:340}).wait(1).to({graphics:mask_graphics_58,x:726.325,y:340}).wait(1).to({graphics:mask_graphics_59,x:726.475,y:340}).wait(1).to({graphics:mask_graphics_60,x:726.625,y:340}).wait(1).to({graphics:mask_graphics_61,x:726.75,y:340}).wait(1).to({graphics:mask_graphics_62,x:726.9,y:340}).wait(1).to({graphics:mask_graphics_63,x:727.05,y:340}).wait(1).to({graphics:mask_graphics_64,x:727.2,y:340}).wait(1).to({graphics:mask_graphics_65,x:727.325,y:340}).wait(1).to({graphics:mask_graphics_66,x:727.475,y:340}).wait(1).to({graphics:mask_graphics_67,x:727.625,y:340}).wait(1).to({graphics:mask_graphics_68,x:727.775,y:340}).wait(1).to({graphics:mask_graphics_69,x:727.9,y:340}).wait(1).to({graphics:mask_graphics_70,x:728.05,y:340}).wait(1).to({graphics:mask_graphics_71,x:728.2,y:340}).wait(1).to({graphics:mask_graphics_72,x:728.325,y:340}).wait(1).to({graphics:mask_graphics_73,x:728.475,y:340}).wait(1).to({graphics:mask_graphics_74,x:728.625,y:340}).wait(1).to({graphics:mask_graphics_75,x:727.791,y:340}).wait(1).to({graphics:mask_graphics_76,x:725.0518,y:340}).wait(1).to({graphics:mask_graphics_77,x:719.8992,y:340}).wait(1).to({graphics:mask_graphics_78,x:711.745,y:340}).wait(1).to({graphics:mask_graphics_79,x:699.9182,y:340}).wait(1).to({graphics:mask_graphics_80,x:684.0406,y:340}).wait(1).to({graphics:mask_graphics_81,x:664.6368,y:340}).wait(1).to({graphics:mask_graphics_82,x:643.543,y:340}).wait(1).to({graphics:mask_graphics_83,x:623.1838,y:340.025}).wait(1).to({graphics:mask_graphics_84,x:605.1966,y:340.025}).wait(1).to({graphics:mask_graphics_85,x:590.0645,y:340.025}).wait(1).to({graphics:mask_graphics_86,x:577.6605,y:340.025}).wait(1).to({graphics:mask_graphics_87,x:567.6176,y:340.025}).wait(1).to({graphics:mask_graphics_88,x:559.5684,y:340.025}).wait(1).to({graphics:mask_graphics_89,x:553.187,y:340.025}).wait(1).to({graphics:mask_graphics_90,x:548.1918,y:340.025}).wait(1).to({graphics:mask_graphics_91,x:544.3618,y:340.025}).wait(1).to({graphics:mask_graphics_92,x:541.5701,y:340.025}).wait(1).to({graphics:mask_graphics_93,x:539.6344,y:340.025}).wait(1).to({graphics:mask_graphics_94,x:538.4746,y:340}).wait(1).to({graphics:mask_graphics_95,x:538.5441,y:339.95}).wait(1).to({graphics:mask_graphics_96,x:538.6191,y:339.95}).wait(1).to({graphics:mask_graphics_97,x:538.7191,y:339.95}).wait(1).to({graphics:mask_graphics_98,x:538.7941,y:339.95}).wait(1).to({graphics:mask_graphics_99,x:538.8941,y:339.95}).wait(1).to({graphics:mask_graphics_100,x:538.9941,y:339.95}).wait(1).to({graphics:mask_graphics_101,x:539.0691,y:339.95}).wait(1).to({graphics:mask_graphics_102,x:539.1691,y:339.95}).wait(1).to({graphics:mask_graphics_103,x:539.2441,y:339.95}).wait(1).to({graphics:mask_graphics_104,x:539.3441,y:339.95}).wait(1).to({graphics:mask_graphics_105,x:539.4441,y:339.95}).wait(1).to({graphics:mask_graphics_106,x:539.5191,y:339.95}).wait(1).to({graphics:mask_graphics_107,x:539.6191,y:339.95}).wait(1).to({graphics:mask_graphics_108,x:539.6941,y:339.95}).wait(1).to({graphics:mask_graphics_109,x:539.7941,y:339.95}).wait(1).to({graphics:mask_graphics_110,x:539.8941,y:339.95}).wait(1).to({graphics:mask_graphics_111,x:539.9691,y:339.95}).wait(1).to({graphics:mask_graphics_112,x:540.0691,y:339.95}).wait(1).to({graphics:mask_graphics_113,x:540.1441,y:339.95}).wait(1).to({graphics:mask_graphics_114,x:540.2441,y:339.95}).wait(1).to({graphics:mask_graphics_115,x:540.3441,y:339.95}).wait(1).to({graphics:mask_graphics_116,x:540.4191,y:339.95}).wait(1).to({graphics:mask_graphics_117,x:540.5191,y:339.95}).wait(1).to({graphics:mask_graphics_118,x:540.5941,y:339.95}).wait(1).to({graphics:mask_graphics_119,x:540.6941,y:339.95}).wait(1).to({graphics:mask_graphics_120,x:540.7941,y:339.95}).wait(1).to({graphics:mask_graphics_121,x:540.8691,y:339.95}).wait(1).to({graphics:mask_graphics_122,x:540.9691,y:339.95}).wait(1).to({graphics:mask_graphics_123,x:541.0441,y:339.95}).wait(1).to({graphics:mask_graphics_124,x:541.1441,y:339.95}).wait(1).to({graphics:mask_graphics_125,x:541.2441,y:339.95}).wait(1).to({graphics:mask_graphics_126,x:541.3191,y:339.95}).wait(1).to({graphics:mask_graphics_127,x:541.4191,y:339.95}).wait(1).to({graphics:mask_graphics_128,x:541.4941,y:339.95}).wait(1).to({graphics:mask_graphics_129,x:541.5941,y:339.95}).wait(1).to({graphics:mask_graphics_130,x:541.6941,y:339.95}).wait(1).to({graphics:mask_graphics_131,x:541.7691,y:339.95}).wait(1).to({graphics:mask_graphics_132,x:541.8691,y:339.95}).wait(1).to({graphics:mask_graphics_133,x:541.9441,y:339.95}).wait(1).to({graphics:mask_graphics_134,x:542.0441,y:339.95}).wait(1).to({graphics:mask_graphics_135,x:542.1441,y:339.95}).wait(1).to({graphics:mask_graphics_136,x:542.2191,y:339.95}).wait(1).to({graphics:mask_graphics_137,x:542.3191,y:339.95}).wait(1).to({graphics:mask_graphics_138,x:542.3941,y:339.95}).wait(1).to({graphics:mask_graphics_139,x:542.4941,y:339.95}).wait(1).to({graphics:mask_graphics_140,x:542.5941,y:339.95}).wait(1).to({graphics:mask_graphics_141,x:542.6691,y:339.95}).wait(1).to({graphics:mask_graphics_142,x:542.7691,y:339.95}).wait(1).to({graphics:mask_graphics_143,x:542.8441,y:339.95}).wait(1).to({graphics:mask_graphics_144,x:542.9441,y:339.95}).wait(1).to({graphics:mask_graphics_145,x:543.0441,y:339.95}).wait(1).to({graphics:mask_graphics_146,x:543.1191,y:339.95}).wait(1).to({graphics:mask_graphics_147,x:543.2191,y:339.95}).wait(1).to({graphics:mask_graphics_148,x:543.2941,y:339.95}).wait(1).to({graphics:mask_graphics_149,x:543.3941,y:339.95}).wait(1).to({graphics:mask_graphics_150,x:543.4941,y:339.95}).wait(1).to({graphics:mask_graphics_151,x:543.5691,y:339.95}).wait(1).to({graphics:mask_graphics_152,x:543.6691,y:339.95}).wait(1).to({graphics:mask_graphics_153,x:543.7441,y:339.95}).wait(1).to({graphics:mask_graphics_154,x:543.8441,y:339.95}).wait(1).to({graphics:mask_graphics_155,x:543.9441,y:339.95}).wait(1).to({graphics:mask_graphics_156,x:544.0191,y:339.95}).wait(1).to({graphics:mask_graphics_157,x:544.1191,y:339.95}).wait(1).to({graphics:mask_graphics_158,x:544.1941,y:339.95}).wait(1).to({graphics:mask_graphics_159,x:544.2941,y:339.95}).wait(1).to({graphics:mask_graphics_160,x:544.3941,y:339.95}).wait(1).to({graphics:mask_graphics_161,x:544.4691,y:339.95}).wait(1).to({graphics:mask_graphics_162,x:544.5691,y:339.95}).wait(1).to({graphics:mask_graphics_163,x:544.6441,y:339.95}).wait(1).to({graphics:mask_graphics_164,x:544.7441,y:339.95}).wait(1).to({graphics:mask_graphics_165,x:544.8441,y:339.95}).wait(1).to({graphics:mask_graphics_166,x:544.9191,y:339.95}).wait(1).to({graphics:mask_graphics_167,x:545.0191,y:339.95}).wait(1).to({graphics:mask_graphics_168,x:545.0941,y:339.95}).wait(1).to({graphics:mask_graphics_169,x:545.2246,y:340}).wait(1).to({graphics:mask_graphics_170,x:544.3753,y:339.95}).wait(1).to({graphics:mask_graphics_171,x:541.6626,y:339.95}).wait(1).to({graphics:mask_graphics_172,x:536.5623,y:339.95}).wait(1).to({graphics:mask_graphics_173,x:528.4485,y:339.95}).wait(1).to({graphics:mask_graphics_174,x:516.6927,y:339.95}).wait(1).to({graphics:mask_graphics_175,x:500.9118,y:339.95}).wait(1).to({graphics:mask_graphics_176,x:481.6212,y:339.95}).wait(1).to({graphics:mask_graphics_177,x:460.6656,y:339.95}).wait(1).to({graphics:mask_graphics_178,x:440.4238,y:339.95}).wait(1).to({graphics:mask_graphics_179,x:422.5421,y:339.95}).wait(1).to({graphics:mask_graphics_180,x:407.511,y:339.95}).wait(1).to({graphics:mask_graphics_181,x:395.2012,y:339.95}).wait(1).to({graphics:mask_graphics_182,x:385.1959,y:339.95}).wait(1).to({graphics:mask_graphics_183,x:377.223,y:339.95}).wait(1).to({graphics:mask_graphics_184,x:370.8488,y:339.95}).wait(1).to({graphics:mask_graphics_185,x:365.8779,y:339.95}).wait(1).to({graphics:mask_graphics_186,x:362.0815,y:339.95}).wait(1).to({graphics:mask_graphics_187,x:359.2803,y:339.95}).wait(1).to({graphics:mask_graphics_188,x:357.4114,y:339.95}).wait(1).to({graphics:mask_graphics_189,x:356.2229,y:340}).wait(55).to({graphics:mask_graphics_244,x:356.2229,y:340}).wait(1).to({graphics:mask_graphics_245,x:357.7952,y:340}).wait(1).to({graphics:mask_graphics_246,x:363.0471,y:340}).wait(1).to({graphics:mask_graphics_247,x:372.9,y:340}).wait(1).to({graphics:mask_graphics_248,x:388.518,y:340}).wait(1).to({graphics:mask_graphics_249,x:411.1256,y:340}).wait(1).to({graphics:mask_graphics_250,x:441.4842,y:340}).wait(1).to({graphics:mask_graphics_251,x:478.6035,y:340}).wait(1).to({graphics:mask_graphics_252,x:518.9529,y:340}).wait(1).to({graphics:mask_graphics_253,x:557.9039,y:339.95}).wait(1).to({graphics:mask_graphics_254,x:592.2924,y:339.95}).wait(1).to({graphics:mask_graphics_255,x:621.2361,y:339.95}).wait(1).to({graphics:mask_graphics_256,x:644.9248,y:339.95}).wait(1).to({graphics:mask_graphics_257,x:664.148,y:339.95}).wait(1).to({graphics:mask_graphics_258,x:679.5646,y:339.95}).wait(1).to({graphics:mask_graphics_259,x:691.7952,y:339.95}).wait(1).to({graphics:mask_graphics_260,x:701.3473,y:339.95}).wait(1).to({graphics:mask_graphics_261,x:708.6432,y:339.95}).wait(1).to({graphics:mask_graphics_262,x:714.0111,y:339.95}).wait(1).to({graphics:mask_graphics_263,x:717.6907,y:339.95}).wait(1).to({graphics:mask_graphics_264,x:720,y:340}).wait(1));

	// Layer_7_obj_
	this.Layer_7 = new lib.Scene_1_Layer_7();
	this.Layer_7.name = "Layer_7";
	this.Layer_7.parent = this;
	this.Layer_7.setTransform(85,41.1,1,1,0,0,0,85,41.1);
	this.Layer_7.depth = 0;
	this.Layer_7.isAttachedToCamera = 0
	this.Layer_7.isAttachedToMask = 0
	this.Layer_7.layerDepth = 0
	this.Layer_7.layerIndex = 1
	this.Layer_7.maskLayerName = 0

	var maskedShapeInstanceList = [this.Layer_7];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.Layer_7).wait(265));

	// Layer_6 (mask)
	var mask_1 = new cjs.Shape();
	mask_1._off = true;
	var mask_1_graphics_14 = new cjs.Graphics().p("EhwfA1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_1_graphics_15 = new cjs.Graphics().p("EhweA1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_1_graphics_16 = new cjs.Graphics().p("EhwcA1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_1_graphics_17 = new cjs.Graphics().p("EhwbA1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_1_graphics_18 = new cjs.Graphics().p("EhwZA1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_1_graphics_19 = new cjs.Graphics().p("EhwYA1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_1_graphics_20 = new cjs.Graphics().p("EhwWA1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_1_graphics_21 = new cjs.Graphics().p("EhwVA1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_1_graphics_22 = new cjs.Graphics().p("EhwTA1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_1_graphics_23 = new cjs.Graphics().p("EhwSA1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_1_graphics_24 = new cjs.Graphics().p("EhwRA1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_1_graphics_25 = new cjs.Graphics().p("EhwPA1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_1_graphics_26 = new cjs.Graphics().p("EhwOA1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_1_graphics_27 = new cjs.Graphics().p("EhwMA1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_1_graphics_28 = new cjs.Graphics().p("EhwLA1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_1_graphics_29 = new cjs.Graphics().p("EhwJA1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_1_graphics_30 = new cjs.Graphics().p("EhwIA1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_1_graphics_31 = new cjs.Graphics().p("EhwHA1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_1_graphics_32 = new cjs.Graphics().p("EhwFA1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_1_graphics_33 = new cjs.Graphics().p("EhwEA1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_1_graphics_34 = new cjs.Graphics().p("EhwCA1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_1_graphics_35 = new cjs.Graphics().p("EhwBA1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_1_graphics_36 = new cjs.Graphics().p("Ehv/A1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_1_graphics_37 = new cjs.Graphics().p("Ehv+A1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_1_graphics_38 = new cjs.Graphics().p("Ehv8A1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_1_graphics_39 = new cjs.Graphics().p("Ehv7A1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_1_graphics_40 = new cjs.Graphics().p("Ehv6A1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_1_graphics_41 = new cjs.Graphics().p("Ehv4A1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_1_graphics_42 = new cjs.Graphics().p("Ehv3A1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_1_graphics_43 = new cjs.Graphics().p("Ehv1A1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_1_graphics_44 = new cjs.Graphics().p("Ehv0A1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_1_graphics_45 = new cjs.Graphics().p("EhvyA1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_1_graphics_46 = new cjs.Graphics().p("EhvxA1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_1_graphics_47 = new cjs.Graphics().p("EhvvA1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_1_graphics_48 = new cjs.Graphics().p("EhvuA1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_1_graphics_49 = new cjs.Graphics().p("EhvtA1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_1_graphics_50 = new cjs.Graphics().p("EhvrA1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_1_graphics_51 = new cjs.Graphics().p("EhvqA1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_1_graphics_52 = new cjs.Graphics().p("EhvoA1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_1_graphics_53 = new cjs.Graphics().p("EhvnA1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_1_graphics_54 = new cjs.Graphics().p("EhvlA1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_1_graphics_55 = new cjs.Graphics().p("EhvkA1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_1_graphics_56 = new cjs.Graphics().p("EhvjA1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_1_graphics_57 = new cjs.Graphics().p("EhvhA1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_1_graphics_58 = new cjs.Graphics().p("EhvgA1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_1_graphics_59 = new cjs.Graphics().p("EhveA1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_1_graphics_60 = new cjs.Graphics().p("EhvdA1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_1_graphics_61 = new cjs.Graphics().p("EhvcA1IMAAAhqPMDhAAAAMAAABqPg");
	var mask_1_graphics_62 = new cjs.Graphics().p("EhvaA1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_1_graphics_63 = new cjs.Graphics().p("EhvYA1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_1_graphics_64 = new cjs.Graphics().p("EhvXA1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_1_graphics_65 = new cjs.Graphics().p("EhvWA1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_1_graphics_66 = new cjs.Graphics().p("EhvUA1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_1_graphics_67 = new cjs.Graphics().p("EhvTA1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_1_graphics_68 = new cjs.Graphics().p("EhvRA1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_1_graphics_69 = new cjs.Graphics().p("EhvQA1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_1_graphics_70 = new cjs.Graphics().p("EhvOA1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_1_graphics_71 = new cjs.Graphics().p("EhvNA1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_1_graphics_72 = new cjs.Graphics().p("EhvMA1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_1_graphics_73 = new cjs.Graphics().p("EhvKA1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_1_graphics_74 = new cjs.Graphics().p("EhvJA1IMAAAhqPMDg/AAAMAAABqPg");
	var mask_1_graphics_75 = new cjs.Graphics().p("EhvAA1IMAAAhqPMDguAAAMAAABqPg");
	var mask_1_graphics_76 = new cjs.Graphics().p("EhuiA1IMAAAhqPMDf1AAAMAAABqPg");
	var mask_1_graphics_77 = new cjs.Graphics().p("EhtpA1IMAAAhqPMDeIAAAMAAABqPg");
	var mask_1_graphics_78 = new cjs.Graphics().p("EhsPA1IMAAAhqPMDbcAAAMAAABqPg");
	var mask_1_graphics_79 = new cjs.Graphics().p("EhqOA1IMAAAhqPMDXlAAAMAAABqPg");
	var mask_1_graphics_80 = new cjs.Graphics().p("EhnfA1IMAAAhqPMDSXAAAMAAABqPg");
	var mask_1_graphics_81 = new cjs.Graphics().p("EhkKA1IMAAAhqPMDMAAAAMAAABqPg");
	var mask_1_graphics_82 = new cjs.Graphics().p("EhgiA1IMAAAhqPMDFFAAAMAAABqPg");
	var mask_1_graphics_83 = new cjs.Graphics().p("EhdDA1IMAAAhqPMC+bAAAMAAABqPg");
	var mask_1_graphics_84 = new cjs.Graphics().p("EhZ9A1IMAAAhqPMC4hAAAMAAABqPg");
	var mask_1_graphics_85 = new cjs.Graphics().p("EhXXA1IMAAAhqPMCzkAAAMAAABqPg");
	var mask_1_graphics_86 = new cjs.Graphics().p("EhVPA1IMAAAhqPMCvgAAAMAAABqPg");
	var mask_1_graphics_87 = new cjs.Graphics().p("EhThA1IMAAAhqPMCsNAAAMAAABqPg");
	var mask_1_graphics_88 = new cjs.Graphics().p("EhSIA1IMAAAhqPMCpkAAAMAAABqPg");
	var mask_1_graphics_89 = new cjs.Graphics().p("EhRCA1IMAAAhqPMCneAAAMAAABqPg");
	var mask_1_graphics_90 = new cjs.Graphics().p("EhQLA1IMAAAhqPMCl1AAAMAAABqPg");
	var mask_1_graphics_91 = new cjs.Graphics().p("EhPhA1IMAAAhqPMCklAAAMAAABqPg");
	var mask_1_graphics_92 = new cjs.Graphics().p("EhPCA1IMAAAhqPMCjqAAAMAAABqPg");
	var mask_1_graphics_93 = new cjs.Graphics().p("EhOtA1IMAAAhqPMCjBAAAMAAABqPg");
	var mask_1_graphics_94 = new cjs.Graphics().p("EhOgA1IMAAAhqPMCipAAAMAAABqPg");
	var mask_1_graphics_95 = new cjs.Graphics().p("EhOfA1IMAAAhqPMCioAAAMAAABqPg");
	var mask_1_graphics_96 = new cjs.Graphics().p("EhOfA1IMAAAhqPMCipAAAMAAABqPg");
	var mask_1_graphics_97 = new cjs.Graphics().p("EhOeA1IMAAAhqPMCipAAAMAAABqPg");
	var mask_1_graphics_98 = new cjs.Graphics().p("EhOdA1IMAAAhqPMCipAAAMAAABqPg");
	var mask_1_graphics_99 = new cjs.Graphics().p("EhOcA1IMAAAhqPMCipAAAMAAABqPg");
	var mask_1_graphics_100 = new cjs.Graphics().p("EhObA1IMAAAhqPMCipAAAMAAABqPg");
	var mask_1_graphics_101 = new cjs.Graphics().p("EhOaA1IMAAAhqPMCipAAAMAAABqPg");
	var mask_1_graphics_102 = new cjs.Graphics().p("EhOZA1IMAAAhqPMCipAAAMAAABqPg");
	var mask_1_graphics_103 = new cjs.Graphics().p("EhOYA1IMAAAhqPMCioAAAMAAABqPg");
	var mask_1_graphics_104 = new cjs.Graphics().p("EhOXA1IMAAAhqPMCioAAAMAAABqPg");
	var mask_1_graphics_105 = new cjs.Graphics().p("EhOWA1IMAAAhqPMCioAAAMAAABqPg");
	var mask_1_graphics_106 = new cjs.Graphics().p("EhOWA1IMAAAhqPMCipAAAMAAABqPg");
	var mask_1_graphics_107 = new cjs.Graphics().p("EhOVA1IMAAAhqPMCipAAAMAAABqPg");
	var mask_1_graphics_108 = new cjs.Graphics().p("EhOUA1IMAAAhqPMCipAAAMAAABqPg");
	var mask_1_graphics_109 = new cjs.Graphics().p("EhOTA1IMAAAhqPMCipAAAMAAABqPg");
	var mask_1_graphics_110 = new cjs.Graphics().p("EhOSA1IMAAAhqPMCipAAAMAAABqPg");
	var mask_1_graphics_111 = new cjs.Graphics().p("EhORA1IMAAAhqPMCipAAAMAAABqPg");
	var mask_1_graphics_112 = new cjs.Graphics().p("EhOQA1IMAAAhqPMCipAAAMAAABqPg");
	var mask_1_graphics_113 = new cjs.Graphics().p("EhOPA1IMAAAhqPMCioAAAMAAABqPg");
	var mask_1_graphics_114 = new cjs.Graphics().p("EhOOA1IMAAAhqPMCioAAAMAAABqPg");
	var mask_1_graphics_115 = new cjs.Graphics().p("EhONA1IMAAAhqPMCioAAAMAAABqPg");
	var mask_1_graphics_116 = new cjs.Graphics().p("EhONA1IMAAAhqPMCipAAAMAAABqPg");
	var mask_1_graphics_117 = new cjs.Graphics().p("EhOMA1IMAAAhqPMCipAAAMAAABqPg");
	var mask_1_graphics_118 = new cjs.Graphics().p("EhOLA1IMAAAhqPMCipAAAMAAABqPg");
	var mask_1_graphics_119 = new cjs.Graphics().p("EhOKA1IMAAAhqPMCipAAAMAAABqPg");
	var mask_1_graphics_120 = new cjs.Graphics().p("EhOJA1IMAAAhqPMCipAAAMAAABqPg");
	var mask_1_graphics_121 = new cjs.Graphics().p("EhOIA1IMAAAhqPMCipAAAMAAABqPg");
	var mask_1_graphics_122 = new cjs.Graphics().p("EhOHA1IMAAAhqPMCipAAAMAAABqPg");
	var mask_1_graphics_123 = new cjs.Graphics().p("EhOGA1IMAAAhqPMCioAAAMAAABqPg");
	var mask_1_graphics_124 = new cjs.Graphics().p("EhOFA1IMAAAhqPMCioAAAMAAABqPg");
	var mask_1_graphics_125 = new cjs.Graphics().p("EhOEA1IMAAAhqPMCioAAAMAAABqPg");
	var mask_1_graphics_126 = new cjs.Graphics().p("EhOEA1IMAAAhqPMCipAAAMAAABqPg");
	var mask_1_graphics_127 = new cjs.Graphics().p("EhODA1IMAAAhqPMCipAAAMAAABqPg");
	var mask_1_graphics_128 = new cjs.Graphics().p("EhOCA1IMAAAhqPMCipAAAMAAABqPg");
	var mask_1_graphics_129 = new cjs.Graphics().p("EhOBA1IMAAAhqPMCipAAAMAAABqPg");
	var mask_1_graphics_130 = new cjs.Graphics().p("EhOAA1IMAAAhqPMCipAAAMAAABqPg");
	var mask_1_graphics_131 = new cjs.Graphics().p("EhN/A1IMAAAhqPMCipAAAMAAABqPg");
	var mask_1_graphics_132 = new cjs.Graphics().p("EhN+A1IMAAAhqPMCipAAAMAAABqPg");
	var mask_1_graphics_133 = new cjs.Graphics().p("EhN9A1IMAAAhqPMCioAAAMAAABqPg");
	var mask_1_graphics_134 = new cjs.Graphics().p("EhN8A1IMAAAhqPMCioAAAMAAABqPg");
	var mask_1_graphics_135 = new cjs.Graphics().p("EhN7A1IMAAAhqPMCioAAAMAAABqPg");
	var mask_1_graphics_136 = new cjs.Graphics().p("EhN7A1IMAAAhqPMCipAAAMAAABqPg");
	var mask_1_graphics_137 = new cjs.Graphics().p("EhN6A1IMAAAhqPMCipAAAMAAABqPg");
	var mask_1_graphics_138 = new cjs.Graphics().p("EhN5A1IMAAAhqPMCipAAAMAAABqPg");
	var mask_1_graphics_139 = new cjs.Graphics().p("EhN4A1IMAAAhqPMCipAAAMAAABqPg");
	var mask_1_graphics_140 = new cjs.Graphics().p("EhN3A1IMAAAhqPMCipAAAMAAABqPg");
	var mask_1_graphics_141 = new cjs.Graphics().p("EhN2A1IMAAAhqPMCipAAAMAAABqPg");
	var mask_1_graphics_142 = new cjs.Graphics().p("EhN1A1IMAAAhqPMCipAAAMAAABqPg");
	var mask_1_graphics_143 = new cjs.Graphics().p("EhN0A1IMAAAhqPMCioAAAMAAABqPg");
	var mask_1_graphics_144 = new cjs.Graphics().p("EhNzA1IMAAAhqPMCioAAAMAAABqPg");
	var mask_1_graphics_145 = new cjs.Graphics().p("EhNyA1IMAAAhqPMCioAAAMAAABqPg");
	var mask_1_graphics_146 = new cjs.Graphics().p("EhNyA1IMAAAhqPMCipAAAMAAABqPg");
	var mask_1_graphics_147 = new cjs.Graphics().p("EhNxA1IMAAAhqPMCipAAAMAAABqPg");
	var mask_1_graphics_148 = new cjs.Graphics().p("EhNwA1IMAAAhqPMCipAAAMAAABqPg");
	var mask_1_graphics_149 = new cjs.Graphics().p("EhNvA1IMAAAhqPMCipAAAMAAABqPg");
	var mask_1_graphics_150 = new cjs.Graphics().p("EhNuA1IMAAAhqPMCipAAAMAAABqPg");
	var mask_1_graphics_151 = new cjs.Graphics().p("EhNtA1IMAAAhqPMCipAAAMAAABqPg");
	var mask_1_graphics_152 = new cjs.Graphics().p("EhNsA1IMAAAhqPMCipAAAMAAABqPg");
	var mask_1_graphics_153 = new cjs.Graphics().p("EhNrA1IMAAAhqPMCioAAAMAAABqPg");
	var mask_1_graphics_154 = new cjs.Graphics().p("EhNqA1IMAAAhqPMCioAAAMAAABqPg");
	var mask_1_graphics_155 = new cjs.Graphics().p("EhNpA1IMAAAhqPMCioAAAMAAABqPg");
	var mask_1_graphics_156 = new cjs.Graphics().p("EhNpA1IMAAAhqPMCipAAAMAAABqPg");
	var mask_1_graphics_157 = new cjs.Graphics().p("EhNoA1IMAAAhqPMCipAAAMAAABqPg");
	var mask_1_graphics_158 = new cjs.Graphics().p("EhNnA1IMAAAhqPMCipAAAMAAABqPg");
	var mask_1_graphics_159 = new cjs.Graphics().p("EhNmA1IMAAAhqPMCipAAAMAAABqPg");
	var mask_1_graphics_160 = new cjs.Graphics().p("EhNlA1IMAAAhqPMCipAAAMAAABqPg");
	var mask_1_graphics_161 = new cjs.Graphics().p("EhNkA1IMAAAhqPMCipAAAMAAABqPg");
	var mask_1_graphics_162 = new cjs.Graphics().p("EhNjA1IMAAAhqPMCipAAAMAAABqPg");
	var mask_1_graphics_163 = new cjs.Graphics().p("EhNiA1IMAAAhqPMCioAAAMAAABqPg");
	var mask_1_graphics_164 = new cjs.Graphics().p("EhNhA1IMAAAhqPMCioAAAMAAABqPg");
	var mask_1_graphics_165 = new cjs.Graphics().p("EhNgA1IMAAAhqPMCioAAAMAAABqPg");
	var mask_1_graphics_166 = new cjs.Graphics().p("EhNgA1IMAAAhqPMCipAAAMAAABqPg");
	var mask_1_graphics_167 = new cjs.Graphics().p("EhNfA1IMAAAhqPMCipAAAMAAABqPg");
	var mask_1_graphics_168 = new cjs.Graphics().p("EhNeA1IMAAAhqPMCipAAAMAAABqPg");
	var mask_1_graphics_169 = new cjs.Graphics().p("EhNdA1IMAAAhqPMCipAAAMAAABqPg");
	var mask_1_graphics_170 = new cjs.Graphics().p("EhNPA1IMAAAhqPMCiTAAAMAAABqPg");
	var mask_1_graphics_171 = new cjs.Graphics().p("EhMiA1IMAAAhqPMChLAAAMAAABqPg");
	var mask_1_graphics_172 = new cjs.Graphics().p("EhLNA1IMAAAhqPMCfDAAAMAAABqPg");
	var mask_1_graphics_173 = new cjs.Graphics().p("EhJGA1IMAAAhqPMCbqAAAMAAABqPg");
	var mask_1_graphics_174 = new cjs.Graphics().p("EhGEA1IMAAAhqPMCWzAAAMAAABqPg");
	var mask_1_graphics_175 = new cjs.Graphics().p("EhB+A1IMAAAhqPMCQPAAAMAAABqPg");
	var mask_1_graphics_176 = new cjs.Graphics().p("Eg8/A1IMAAAhqPMCIPAAAMAAABqPg");
	var mask_1_graphics_177 = new cjs.Graphics().p("Eg3jA1IMAAAhqPMB/iAAAMAAABqPg");
	var mask_1_graphics_178 = new cjs.Graphics().p("EgyUA1IMAAAhqPMB3IAAAMAAABqPg");
	var mask_1_graphics_179 = new cjs.Graphics().p("EgtrA1IMAAAhqPMBvsAAAMAAABqPg");
	var mask_1_graphics_180 = new cjs.Graphics().p("EgpyA1IMAAAhqPMBpdAAAMAAABqPg");
	var mask_1_graphics_181 = new cjs.Graphics().p("EgmmA1IMAAAhqPMBkWAAAMAAABqPg");
	var mask_1_graphics_182 = new cjs.Graphics().p("EgkBA1IMAAAhqPMBgNAAAMAAABqPg");
	var mask_1_graphics_183 = new cjs.Graphics().p("Egh8A1IMAAAhqPMBc4AAAMAAABqPg");
	var mask_1_graphics_184 = new cjs.Graphics().p("EggSA1IMAAAhqPMBaOAAAMAAABqPg");
	var mask_1_graphics_185 = new cjs.Graphics().p("EgfAA1IMAAAhqPMBYLAAAMAAABqPg");
	var mask_1_graphics_186 = new cjs.Graphics().p("EgeBA1IMAAAhqPMBWmAAAMAAABqPg");
	var mask_1_graphics_187 = new cjs.Graphics().p("EgdTA1IMAAAhqPMBVcAAAMAAABqPg");
	var mask_1_graphics_188 = new cjs.Graphics().p("EgczA1IMAAAhqPMBUpAAAMAAABqPg");
	var mask_1_graphics_189 = new cjs.Graphics().p("EgchA1IMAAAhqPMBULAAAMAAABqPg");
	var mask_1_graphics_244 = new cjs.Graphics().p("EgchA1IMAAAhqPMBULAAAMAAABqPg");
	var mask_1_graphics_245 = new cjs.Graphics().p("Egc4A1IMAAAhqPMBUyAAAMAAABqPg");
	var mask_1_graphics_246 = new cjs.Graphics().p("EgeFA1IMAAAhqPMBWzAAAMAAABqPg");
	var mask_1_graphics_247 = new cjs.Graphics().p("EggXA1IMAAAhqPMBaoAAAMAAABqPg");
	var mask_1_graphics_248 = new cjs.Graphics().p("Egj+A1IMAAAhqPMBgrAAAMAAABqPg");
	var mask_1_graphics_249 = new cjs.Graphics().p("EgpMA1IMAAAhqPMBpbAAAMAAABqPg");
	var mask_1_graphics_250 = new cjs.Graphics().p("EgwNA1IMAAAhqPMB1MAAAMAAABqPg");
	var mask_1_graphics_251 = new cjs.Graphics().p("Eg4xA1IMAAAhqPMCDjAAAMAAABqPg");
	var mask_1_graphics_252 = new cjs.Graphics().p("EhCGA1IMAAAhqPMCTMAAAMAAABqPg");
	var mask_1_graphics_253 = new cjs.Graphics().p("EhLFA1IMAAAhqPMCiQAAAMAAABqPg");
	var mask_1_graphics_254 = new cjs.Graphics().p("EhTCA1IMAAAhqPMCvlAAAMAAABqPg");
	var mask_1_graphics_255 = new cjs.Graphics().p("EhZtA1IMAAAhqPMC6xAAAMAAABqPg");
	var mask_1_graphics_256 = new cjs.Graphics().p("EhfMA1IMAAAhqPMDD9AAAMAAABqPg");
	var mask_1_graphics_257 = new cjs.Graphics().p("EhjnA1IMAAAhqPMDLYAAAMAAABqPg");
	var mask_1_graphics_258 = new cjs.Graphics().p("EhnLA1IMAAAhqPMDRXAAAMAAABqPg");
	var mask_1_graphics_259 = new cjs.Graphics().p("EhqAA1IMAAAhqPMDWGAAAMAAABqPg");
	var mask_1_graphics_260 = new cjs.Graphics().p("EhsNA1IMAAAhqPMDZyAAAMAAABqPg");
	var mask_1_graphics_261 = new cjs.Graphics().p("Eht5A1IMAAAhqPMDcnAAAMAAABqPg");
	var mask_1_graphics_262 = new cjs.Graphics().p("EhvIA1IMAAAhqPMDesAAAMAAABqPg");
	var mask_1_graphics_263 = new cjs.Graphics().p("Ehv/A1IMAAAhqPMDgIAAAMAAABqPg");
	var mask_1_graphics_264 = new cjs.Graphics().p("EhwfA1IMAAAhqPMDg/AAAMAAABqPg");

	this.timeline.addTween(cjs.Tween.get(mask_1).to({graphics:null,x:0,y:0}).wait(14).to({graphics:mask_1_graphics_14,x:720,y:340}).wait(1).to({graphics:mask_1_graphics_15,x:720.15,y:340}).wait(1).to({graphics:mask_1_graphics_16,x:720.3,y:340}).wait(1).to({graphics:mask_1_graphics_17,x:720.425,y:340}).wait(1).to({graphics:mask_1_graphics_18,x:720.575,y:340}).wait(1).to({graphics:mask_1_graphics_19,x:720.725,y:340}).wait(1).to({graphics:mask_1_graphics_20,x:720.875,y:340}).wait(1).to({graphics:mask_1_graphics_21,x:721,y:340}).wait(1).to({graphics:mask_1_graphics_22,x:721.15,y:340}).wait(1).to({graphics:mask_1_graphics_23,x:721.3,y:340}).wait(1).to({graphics:mask_1_graphics_24,x:721.45,y:340}).wait(1).to({graphics:mask_1_graphics_25,x:721.575,y:340}).wait(1).to({graphics:mask_1_graphics_26,x:721.725,y:340}).wait(1).to({graphics:mask_1_graphics_27,x:721.875,y:340}).wait(1).to({graphics:mask_1_graphics_28,x:722.025,y:340}).wait(1).to({graphics:mask_1_graphics_29,x:722.15,y:340}).wait(1).to({graphics:mask_1_graphics_30,x:722.3,y:340}).wait(1).to({graphics:mask_1_graphics_31,x:722.45,y:340}).wait(1).to({graphics:mask_1_graphics_32,x:722.6,y:340}).wait(1).to({graphics:mask_1_graphics_33,x:722.725,y:340}).wait(1).to({graphics:mask_1_graphics_34,x:722.875,y:340}).wait(1).to({graphics:mask_1_graphics_35,x:723.025,y:340}).wait(1).to({graphics:mask_1_graphics_36,x:723.175,y:340}).wait(1).to({graphics:mask_1_graphics_37,x:723.3,y:340}).wait(1).to({graphics:mask_1_graphics_38,x:723.45,y:340}).wait(1).to({graphics:mask_1_graphics_39,x:723.6,y:340}).wait(1).to({graphics:mask_1_graphics_40,x:723.75,y:340}).wait(1).to({graphics:mask_1_graphics_41,x:723.875,y:340}).wait(1).to({graphics:mask_1_graphics_42,x:724.025,y:340}).wait(1).to({graphics:mask_1_graphics_43,x:724.175,y:340}).wait(1).to({graphics:mask_1_graphics_44,x:724.325,y:340}).wait(1).to({graphics:mask_1_graphics_45,x:724.45,y:340}).wait(1).to({graphics:mask_1_graphics_46,x:724.6,y:340}).wait(1).to({graphics:mask_1_graphics_47,x:724.75,y:340}).wait(1).to({graphics:mask_1_graphics_48,x:724.9,y:340}).wait(1).to({graphics:mask_1_graphics_49,x:725.025,y:340}).wait(1).to({graphics:mask_1_graphics_50,x:725.175,y:340}).wait(1).to({graphics:mask_1_graphics_51,x:725.325,y:340}).wait(1).to({graphics:mask_1_graphics_52,x:725.475,y:340}).wait(1).to({graphics:mask_1_graphics_53,x:725.6,y:340}).wait(1).to({graphics:mask_1_graphics_54,x:725.75,y:340}).wait(1).to({graphics:mask_1_graphics_55,x:725.9,y:340}).wait(1).to({graphics:mask_1_graphics_56,x:726.05,y:340}).wait(1).to({graphics:mask_1_graphics_57,x:726.175,y:340}).wait(1).to({graphics:mask_1_graphics_58,x:726.325,y:340}).wait(1).to({graphics:mask_1_graphics_59,x:726.475,y:340}).wait(1).to({graphics:mask_1_graphics_60,x:726.625,y:340}).wait(1).to({graphics:mask_1_graphics_61,x:726.75,y:340}).wait(1).to({graphics:mask_1_graphics_62,x:726.9,y:340}).wait(1).to({graphics:mask_1_graphics_63,x:727.05,y:340}).wait(1).to({graphics:mask_1_graphics_64,x:727.2,y:340}).wait(1).to({graphics:mask_1_graphics_65,x:727.325,y:340}).wait(1).to({graphics:mask_1_graphics_66,x:727.475,y:340}).wait(1).to({graphics:mask_1_graphics_67,x:727.625,y:340}).wait(1).to({graphics:mask_1_graphics_68,x:727.775,y:340}).wait(1).to({graphics:mask_1_graphics_69,x:727.9,y:340}).wait(1).to({graphics:mask_1_graphics_70,x:728.05,y:340}).wait(1).to({graphics:mask_1_graphics_71,x:728.2,y:340}).wait(1).to({graphics:mask_1_graphics_72,x:728.325,y:340}).wait(1).to({graphics:mask_1_graphics_73,x:728.475,y:340}).wait(1).to({graphics:mask_1_graphics_74,x:728.625,y:340}).wait(1).to({graphics:mask_1_graphics_75,x:727.791,y:340}).wait(1).to({graphics:mask_1_graphics_76,x:725.0518,y:340}).wait(1).to({graphics:mask_1_graphics_77,x:719.8992,y:340}).wait(1).to({graphics:mask_1_graphics_78,x:711.745,y:340}).wait(1).to({graphics:mask_1_graphics_79,x:699.9182,y:340}).wait(1).to({graphics:mask_1_graphics_80,x:684.0406,y:340}).wait(1).to({graphics:mask_1_graphics_81,x:664.6368,y:340}).wait(1).to({graphics:mask_1_graphics_82,x:643.543,y:340}).wait(1).to({graphics:mask_1_graphics_83,x:623.1838,y:340.025}).wait(1).to({graphics:mask_1_graphics_84,x:605.1966,y:340.025}).wait(1).to({graphics:mask_1_graphics_85,x:590.0645,y:340.025}).wait(1).to({graphics:mask_1_graphics_86,x:577.6605,y:340.025}).wait(1).to({graphics:mask_1_graphics_87,x:567.6176,y:340.025}).wait(1).to({graphics:mask_1_graphics_88,x:559.5684,y:340.025}).wait(1).to({graphics:mask_1_graphics_89,x:553.187,y:340.025}).wait(1).to({graphics:mask_1_graphics_90,x:548.1918,y:340.025}).wait(1).to({graphics:mask_1_graphics_91,x:544.3618,y:340.025}).wait(1).to({graphics:mask_1_graphics_92,x:541.5701,y:340.025}).wait(1).to({graphics:mask_1_graphics_93,x:539.6344,y:340.025}).wait(1).to({graphics:mask_1_graphics_94,x:538.4746,y:340}).wait(1).to({graphics:mask_1_graphics_95,x:538.5441,y:339.95}).wait(1).to({graphics:mask_1_graphics_96,x:538.6191,y:339.95}).wait(1).to({graphics:mask_1_graphics_97,x:538.7191,y:339.95}).wait(1).to({graphics:mask_1_graphics_98,x:538.7941,y:339.95}).wait(1).to({graphics:mask_1_graphics_99,x:538.8941,y:339.95}).wait(1).to({graphics:mask_1_graphics_100,x:538.9941,y:339.95}).wait(1).to({graphics:mask_1_graphics_101,x:539.0691,y:339.95}).wait(1).to({graphics:mask_1_graphics_102,x:539.1691,y:339.95}).wait(1).to({graphics:mask_1_graphics_103,x:539.2441,y:339.95}).wait(1).to({graphics:mask_1_graphics_104,x:539.3441,y:339.95}).wait(1).to({graphics:mask_1_graphics_105,x:539.4441,y:339.95}).wait(1).to({graphics:mask_1_graphics_106,x:539.5191,y:339.95}).wait(1).to({graphics:mask_1_graphics_107,x:539.6191,y:339.95}).wait(1).to({graphics:mask_1_graphics_108,x:539.6941,y:339.95}).wait(1).to({graphics:mask_1_graphics_109,x:539.7941,y:339.95}).wait(1).to({graphics:mask_1_graphics_110,x:539.8941,y:339.95}).wait(1).to({graphics:mask_1_graphics_111,x:539.9691,y:339.95}).wait(1).to({graphics:mask_1_graphics_112,x:540.0691,y:339.95}).wait(1).to({graphics:mask_1_graphics_113,x:540.1441,y:339.95}).wait(1).to({graphics:mask_1_graphics_114,x:540.2441,y:339.95}).wait(1).to({graphics:mask_1_graphics_115,x:540.3441,y:339.95}).wait(1).to({graphics:mask_1_graphics_116,x:540.4191,y:339.95}).wait(1).to({graphics:mask_1_graphics_117,x:540.5191,y:339.95}).wait(1).to({graphics:mask_1_graphics_118,x:540.5941,y:339.95}).wait(1).to({graphics:mask_1_graphics_119,x:540.6941,y:339.95}).wait(1).to({graphics:mask_1_graphics_120,x:540.7941,y:339.95}).wait(1).to({graphics:mask_1_graphics_121,x:540.8691,y:339.95}).wait(1).to({graphics:mask_1_graphics_122,x:540.9691,y:339.95}).wait(1).to({graphics:mask_1_graphics_123,x:541.0441,y:339.95}).wait(1).to({graphics:mask_1_graphics_124,x:541.1441,y:339.95}).wait(1).to({graphics:mask_1_graphics_125,x:541.2441,y:339.95}).wait(1).to({graphics:mask_1_graphics_126,x:541.3191,y:339.95}).wait(1).to({graphics:mask_1_graphics_127,x:541.4191,y:339.95}).wait(1).to({graphics:mask_1_graphics_128,x:541.4941,y:339.95}).wait(1).to({graphics:mask_1_graphics_129,x:541.5941,y:339.95}).wait(1).to({graphics:mask_1_graphics_130,x:541.6941,y:339.95}).wait(1).to({graphics:mask_1_graphics_131,x:541.7691,y:339.95}).wait(1).to({graphics:mask_1_graphics_132,x:541.8691,y:339.95}).wait(1).to({graphics:mask_1_graphics_133,x:541.9441,y:339.95}).wait(1).to({graphics:mask_1_graphics_134,x:542.0441,y:339.95}).wait(1).to({graphics:mask_1_graphics_135,x:542.1441,y:339.95}).wait(1).to({graphics:mask_1_graphics_136,x:542.2191,y:339.95}).wait(1).to({graphics:mask_1_graphics_137,x:542.3191,y:339.95}).wait(1).to({graphics:mask_1_graphics_138,x:542.3941,y:339.95}).wait(1).to({graphics:mask_1_graphics_139,x:542.4941,y:339.95}).wait(1).to({graphics:mask_1_graphics_140,x:542.5941,y:339.95}).wait(1).to({graphics:mask_1_graphics_141,x:542.6691,y:339.95}).wait(1).to({graphics:mask_1_graphics_142,x:542.7691,y:339.95}).wait(1).to({graphics:mask_1_graphics_143,x:542.8441,y:339.95}).wait(1).to({graphics:mask_1_graphics_144,x:542.9441,y:339.95}).wait(1).to({graphics:mask_1_graphics_145,x:543.0441,y:339.95}).wait(1).to({graphics:mask_1_graphics_146,x:543.1191,y:339.95}).wait(1).to({graphics:mask_1_graphics_147,x:543.2191,y:339.95}).wait(1).to({graphics:mask_1_graphics_148,x:543.2941,y:339.95}).wait(1).to({graphics:mask_1_graphics_149,x:543.3941,y:339.95}).wait(1).to({graphics:mask_1_graphics_150,x:543.4941,y:339.95}).wait(1).to({graphics:mask_1_graphics_151,x:543.5691,y:339.95}).wait(1).to({graphics:mask_1_graphics_152,x:543.6691,y:339.95}).wait(1).to({graphics:mask_1_graphics_153,x:543.7441,y:339.95}).wait(1).to({graphics:mask_1_graphics_154,x:543.8441,y:339.95}).wait(1).to({graphics:mask_1_graphics_155,x:543.9441,y:339.95}).wait(1).to({graphics:mask_1_graphics_156,x:544.0191,y:339.95}).wait(1).to({graphics:mask_1_graphics_157,x:544.1191,y:339.95}).wait(1).to({graphics:mask_1_graphics_158,x:544.1941,y:339.95}).wait(1).to({graphics:mask_1_graphics_159,x:544.2941,y:339.95}).wait(1).to({graphics:mask_1_graphics_160,x:544.3941,y:339.95}).wait(1).to({graphics:mask_1_graphics_161,x:544.4691,y:339.95}).wait(1).to({graphics:mask_1_graphics_162,x:544.5691,y:339.95}).wait(1).to({graphics:mask_1_graphics_163,x:544.6441,y:339.95}).wait(1).to({graphics:mask_1_graphics_164,x:544.7441,y:339.95}).wait(1).to({graphics:mask_1_graphics_165,x:544.8441,y:339.95}).wait(1).to({graphics:mask_1_graphics_166,x:544.9191,y:339.95}).wait(1).to({graphics:mask_1_graphics_167,x:545.0191,y:339.95}).wait(1).to({graphics:mask_1_graphics_168,x:545.0941,y:339.95}).wait(1).to({graphics:mask_1_graphics_169,x:545.2246,y:340}).wait(1).to({graphics:mask_1_graphics_170,x:544.3753,y:339.95}).wait(1).to({graphics:mask_1_graphics_171,x:541.6626,y:339.95}).wait(1).to({graphics:mask_1_graphics_172,x:536.5623,y:339.95}).wait(1).to({graphics:mask_1_graphics_173,x:528.4485,y:339.95}).wait(1).to({graphics:mask_1_graphics_174,x:516.6927,y:339.95}).wait(1).to({graphics:mask_1_graphics_175,x:500.9118,y:339.95}).wait(1).to({graphics:mask_1_graphics_176,x:481.6212,y:339.95}).wait(1).to({graphics:mask_1_graphics_177,x:460.6656,y:339.95}).wait(1).to({graphics:mask_1_graphics_178,x:440.4238,y:339.95}).wait(1).to({graphics:mask_1_graphics_179,x:422.5421,y:339.95}).wait(1).to({graphics:mask_1_graphics_180,x:407.511,y:339.95}).wait(1).to({graphics:mask_1_graphics_181,x:395.2012,y:339.95}).wait(1).to({graphics:mask_1_graphics_182,x:385.1959,y:339.95}).wait(1).to({graphics:mask_1_graphics_183,x:377.223,y:339.95}).wait(1).to({graphics:mask_1_graphics_184,x:370.8488,y:339.95}).wait(1).to({graphics:mask_1_graphics_185,x:365.8779,y:339.95}).wait(1).to({graphics:mask_1_graphics_186,x:362.0815,y:339.95}).wait(1).to({graphics:mask_1_graphics_187,x:359.2803,y:339.95}).wait(1).to({graphics:mask_1_graphics_188,x:357.4114,y:339.95}).wait(1).to({graphics:mask_1_graphics_189,x:356.2229,y:340}).wait(55).to({graphics:mask_1_graphics_244,x:356.2229,y:340}).wait(1).to({graphics:mask_1_graphics_245,x:357.7952,y:340}).wait(1).to({graphics:mask_1_graphics_246,x:363.0471,y:340}).wait(1).to({graphics:mask_1_graphics_247,x:372.9,y:340}).wait(1).to({graphics:mask_1_graphics_248,x:388.518,y:340}).wait(1).to({graphics:mask_1_graphics_249,x:411.1256,y:340}).wait(1).to({graphics:mask_1_graphics_250,x:441.4842,y:340}).wait(1).to({graphics:mask_1_graphics_251,x:478.6035,y:340}).wait(1).to({graphics:mask_1_graphics_252,x:518.9529,y:340}).wait(1).to({graphics:mask_1_graphics_253,x:557.9039,y:339.95}).wait(1).to({graphics:mask_1_graphics_254,x:592.2924,y:339.95}).wait(1).to({graphics:mask_1_graphics_255,x:621.2361,y:339.95}).wait(1).to({graphics:mask_1_graphics_256,x:644.9248,y:339.95}).wait(1).to({graphics:mask_1_graphics_257,x:664.148,y:339.95}).wait(1).to({graphics:mask_1_graphics_258,x:679.5646,y:339.95}).wait(1).to({graphics:mask_1_graphics_259,x:691.7952,y:339.95}).wait(1).to({graphics:mask_1_graphics_260,x:701.3473,y:339.95}).wait(1).to({graphics:mask_1_graphics_261,x:708.6432,y:339.95}).wait(1).to({graphics:mask_1_graphics_262,x:714.0111,y:339.95}).wait(1).to({graphics:mask_1_graphics_263,x:717.6907,y:339.95}).wait(1).to({graphics:mask_1_graphics_264,x:720,y:340}).wait(1));

	// Layer_5_obj_
	this.Layer_5 = new lib.Scene_1_Layer_5();
	this.Layer_5.name = "Layer_5";
	this.Layer_5.parent = this;
	this.Layer_5.setTransform(160,227,1,1,0,0,0,160,227);
	this.Layer_5.depth = 0;
	this.Layer_5.isAttachedToCamera = 0
	this.Layer_5.isAttachedToMask = 0
	this.Layer_5.layerDepth = 0
	this.Layer_5.layerIndex = 2
	this.Layer_5.maskLayerName = 0

	var maskedShapeInstanceList = [this.Layer_5];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask_1;
	}

	this.timeline.addTween(cjs.Tween.get(this.Layer_5).wait(265));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(140.7,227,205.5,227);
// library properties:
lib.properties = {
	id: '70307C49E07A104EB48CF442B7FFCC18',
	width: 320,
	height: 454,
	fps: 30,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: [
		{src:"assets/images/bg60-2.png", id:"bg"},
		{src:"assets/images/D60-2.png", id:"D"},
		{src:"assets/images/pack_160-2.png", id:"pack_1"},
		{src:"assets/images/pack_360-2.png", id:"pack_3"},
		{src:"assets/images/pack_460-2.png", id:"pack_4"},
		{src:"assets/images/pack_part160-2.png", id:"pack_part1"},
		{src:"assets/images/pack_part260-2.png", id:"pack_part2"},
		{src:"assets/images/pack_part2_shadow60-2.png", id:"pack_part2_shadow"},
		{src:"assets/images/plashka160-2.png", id:"plashka1"}
	],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['70307C49E07A104EB48CF442B7FFCC18'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}


// Layer depth API :

AdobeAn.Layer = new function() {
	this.getLayerZDepth = function(timeline, layerName)
	{
		if(layerName === "Camera")
		layerName = "___camera___instance";
		var script = "if(timeline." + layerName + ") timeline." + layerName + ".depth; else 0;";
		return eval(script);
	}
	this.setLayerZDepth = function(timeline, layerName, zDepth)
	{
		const MAX_zDepth = 10000;
		const MIN_zDepth = -5000;
		if(zDepth > MAX_zDepth)
			zDepth = MAX_zDepth;
		else if(zDepth < MIN_zDepth)
			zDepth = MIN_zDepth;
		if(layerName === "Camera")
		layerName = "___camera___instance";
		var script = "if(timeline." + layerName + ") timeline." + layerName + ".depth = " + zDepth + ";";
		eval(script);
	}
	this.removeLayer = function(timeline, layerName)
	{
		if(layerName === "Camera")
		layerName = "___camera___instance";
		var script = "if(timeline." + layerName + ") timeline.removeChild(timeline." + layerName + ");";
		eval(script);
	}
	this.addNewLayer = function(timeline, layerName, zDepth)
	{
		if(layerName === "Camera")
		layerName = "___camera___instance";
		zDepth = typeof zDepth !== 'undefined' ? zDepth : 0;
		var layer = new createjs.MovieClip();
		layer.name = layerName;
		layer.depth = zDepth;
		layer.layerIndex = 0;
		timeline.addChild(layer);
	}
}


})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;

$(function(){
	var canvas, stage, exportRoot, anim_container, dom_overlay_container, fnStartAnimation;
	function init() {
		canvas = document.getElementById("canvas60-2");
		anim_container = document.getElementById("animation_container60-2");
		dom_overlay_container = document.getElementById("dom_overlay_container60-2");
		var comp=AdobeAn.getComposition("70307C49E07A104EB48CF442B7FFCC18");
		var lib=comp.getLibrary();
		var loader = new createjs.LoadQueue(false);
		loader.addEventListener("fileload", function(evt){handleFileLoad(evt,comp)});
		loader.addEventListener("complete", function(evt){handleComplete(evt,comp)});
		var lib=comp.getLibrary();
		loader.loadManifest(lib.properties.manifest);
	}
	function handleFileLoad(evt, comp) {
		var images=comp.getImages();
		if (evt && (evt.item.type == "image")) { images[evt.item.id] = evt.result; }
	}
	function handleComplete(evt,comp) {
		//This function is always called, irrespective of the content. You can use the variable "stage" after it is created in token create_stage.
		var lib=comp.getLibrary();
		var ss=comp.getSpriteSheet();
		var queue = evt.target;
		var ssMetadata = lib.ssMetadata;
		for(i=0; i<ssMetadata.length; i++) {
			ss[ssMetadata[i].name] = new createjs.SpriteSheet( {"images": [queue.getResult(ssMetadata[i].name)], "frames": ssMetadata[i].frames} )
		}
		exportRoot = new lib._320x454();
		stage = new lib.Stage(canvas);
		//Registers the "tick" event listener.
		fnStartAnimation = function() {
			stage.addChild(exportRoot);
			createjs.Ticker.setFPS(lib.properties.fps);
			createjs.Ticker.addEventListener("tick", stage)
			stage.addEventListener("tick", handleTick)
			function getProjectionMatrix(container, totalDepth) {
				var focalLength = 528.25;
				var projectionCenter = { x : lib.properties.width/2, y : lib.properties.height/2 };
				var scale = (totalDepth + focalLength)/focalLength;
				var scaleMat = new createjs.Matrix2D;
				scaleMat.a = 1/scale;
				scaleMat.d = 1/scale;
				var projMat = new createjs.Matrix2D;
				projMat.tx = -projectionCenter.x;
				projMat.ty = -projectionCenter.y;
				projMat = projMat.prependMatrix(scaleMat);
				projMat.tx += projectionCenter.x;
				projMat.ty += projectionCenter.y;
				return projMat;
			}
			function handleTick(event) {
				var cameraInstance = exportRoot.___camera___instance;
				if(cameraInstance !== undefined && cameraInstance.pinToObject !== undefined)
				{
					cameraInstance.x = cameraInstance.pinToObject.x + cameraInstance.pinToObject.pinOffsetX;
					cameraInstance.y = cameraInstance.pinToObject.y + cameraInstance.pinToObject.pinOffsetY;
					if(cameraInstance.pinToObject.parent !== undefined && cameraInstance.pinToObject.parent.depth !== undefined)
					cameraInstance.depth = cameraInstance.pinToObject.parent.depth + cameraInstance.pinToObject.pinOffsetZ;
				}
				applyLayerZDepth(exportRoot);
			}
			function applyLayerZDepth(parent)
			{
				var cameraInstance = parent.___camera___instance;
				var focalLength = 528.25;
				var projectionCenter = { 'x' : 0, 'y' : 0};
				if(parent === exportRoot)
				{
					var stageCenter = { 'x' : lib.properties.width/2, 'y' : lib.properties.height/2 };
					projectionCenter.x = stageCenter.x;
					projectionCenter.y = stageCenter.y;
				}
				for(child in parent.children)
				{
					var layerObj = parent.children[child];
					if(layerObj == cameraInstance)
						continue;
					applyLayerZDepth(layerObj, cameraInstance);
					if(layerObj.layerDepth === undefined)
						continue;
					if(layerObj.currentFrame != layerObj.parent.currentFrame)
					{
						layerObj.gotoAndPlay(layerObj.parent.currentFrame);
					}
					var matToApply = new createjs.Matrix2D;
					var cameraMat = new createjs.Matrix2D;
					var totalDepth = layerObj.layerDepth ? layerObj.layerDepth : 0;
					var cameraDepth = 0;
					if(cameraInstance && !layerObj.isAttachedToCamera)
					{
						var mat = cameraInstance.getMatrix();
						mat.tx -= projectionCenter.x;
						mat.ty -= projectionCenter.y;
						cameraMat = mat.invert();
						cameraMat.prependTransform(projectionCenter.x, projectionCenter.y, 1, 1, 0, 0, 0, 0, 0);
						cameraMat.appendTransform(-projectionCenter.x, -projectionCenter.y, 1, 1, 0, 0, 0, 0, 0);
						if(cameraInstance.depth)
							cameraDepth = cameraInstance.depth;
					}
					if(layerObj.depth)
					{
						totalDepth = layerObj.depth;
					}
					//Offset by camera depth
					totalDepth -= cameraDepth;
					if(totalDepth < -focalLength)
					{
						matToApply.a = 0;
						matToApply.d = 0;
					}
					else
					{
						if(layerObj.layerDepth)
						{
							var sizeLockedMat = getProjectionMatrix(parent, layerObj.layerDepth);
							if(sizeLockedMat)
							{
								sizeLockedMat.invert();
								matToApply.prependMatrix(sizeLockedMat);
							}
						}
						matToApply.prependMatrix(cameraMat);
						var projMat = getProjectionMatrix(parent, totalDepth);
						if(projMat)
						{
							matToApply.prependMatrix(projMat);
						}
					}
					layerObj.transformMatrix = matToApply;
				}
			}
		}
		//Code to support hidpi screens and responsive scaling.
		function makeResponsive(isResp, respDim, isScale, scaleType) {
			var lastW, lastH, lastS=1;
			window.addEventListener('resize', resizeCanvas);
			resizeCanvas();
			function resizeCanvas() {
				var w = lib.properties.width, h = lib.properties.height;
				var iw = window.innerWidth, ih=window.innerHeight;
				var pRatio = window.devicePixelRatio || 1, xRatio=iw/w, yRatio=ih/h, sRatio=1;
				if(isResp) {
					if((respDim=='width'&&lastW==iw) || (respDim=='height'&&lastH==ih)) {
						sRatio = lastS;
					}
					else if(!isScale) {
						if(iw<w || ih<h)
							sRatio = Math.min(xRatio, yRatio);
					}
					else if(scaleType==1) {
						sRatio = Math.min(xRatio, yRatio);
					}
					else if(scaleType==2) {
						sRatio = Math.max(xRatio, yRatio);
					}
				}
				canvas.width = w*pRatio*sRatio;
				canvas.height = h*pRatio*sRatio;
				canvas.style.width = dom_overlay_container.style.width = anim_container.style.width =  w*sRatio+'px';
				canvas.style.height = anim_container.style.height = dom_overlay_container.style.height = h*sRatio+'px';
				stage.scaleX = pRatio*sRatio;
				stage.scaleY = pRatio*sRatio;
				lastW = iw; lastH = ih; lastS = sRatio;
				stage.tickOnUpdate = false;
				stage.update();
				stage.tickOnUpdate = true;
			}
		}
		makeResponsive(false,'both',false,2);
		AdobeAn.compositionLoaded(lib.properties.id);
		fnStartAnimation();
	}
	init();
});
