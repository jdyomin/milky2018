(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [];


// symbols:



(lib.hand1 = function() {
	this.initialize(img.hand1);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,110,50);


(lib.hand1_part = function() {
	this.initialize(img.hand1_part);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,45,64);


(lib.hands = function() {
	this.initialize(img.hands);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,128,176);


(lib.mouth1 = function() {
	this.initialize(img.mouth1);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,35,97);


(lib.pack1 = function() {
	this.initialize(img.pack1);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,308,453);


(lib.pack2 = function() {
	this.initialize(img.pack2);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,292,483);


(lib.teeth = function() {
	this.initialize(img.teeth);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,16,15);// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.Symbol10 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 4
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(12,1,1).p("AB0BlQARiBhQgxQhkgnhHAb");
	this.shape.setTransform(18.4,107.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(12,1,1).p("Ah8hQQBEgYBjAjQBUAqgCB2");
	this.shape_1.setTransform(14.5,106.9);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(12,1,1).p("AiDhIQBCgVBjAfQBWAkAMBs");
	this.shape_2.setTransform(11.2,106.6);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(12,1,1).p("AiJhBQA/gTBjAcQBZAfAYBj");
	this.shape_3.setTransform(8.3,106.4);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(12,1,1).p("AiOg7QA9gQBiAYQBcAbAiBb");
	this.shape_4.setTransform(5.8,106.2);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(12,1,1).p("AiSg3QA7gNBiAVQBeAXAqBV");
	this.shape_5.setTransform(3.7,106);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(12,1,1).p("AiWgyQA6gNBiATQBfAUAyBQ");
	this.shape_6.setTransform(1.9,105.9);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(12,1,1).p("AiYgvQA4gLBiARQBgASA3BL");
	this.shape_7.setTransform(0.5,105.8);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(12,1,1).p("AiagtQA3gKBjAQQBgAQA7BJ");
	this.shape_8.setTransform(-0.4,105.7);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(12,1,1).p("AibgsQA3gJBiAQQBhAOA9BH");
	this.shape_9.setTransform(-1,105.6);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(12,1,1).p("ACdAvQg+hGhigOQhigPg3AJ");
	this.shape_10.setTransform(-1.2,105.6);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(12,1,1).p("AiRg3QA7gOBiAWQBdAXApBW");
	this.shape_11.setTransform(3.9,106);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(12,1,1).p("AiBhKQBCgVBiAfQBWAmAJBu");
	this.shape_12.setTransform(11.9,106.7);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(12,1,1).p("Ah8hQQBFgYBjAjQBTAqgCB2");
	this.shape_13.setTransform(14.6,106.9);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(12,1,1).p("Ah4hVQBGgaBjAmQBSAtgLB9");
	this.shape_14.setTransform(16.7,107.1);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(12,1,1).p("Ah3hYQBIgbBjAnQBQAwgPCA");
	this.shape_15.setTransform(17.9,107.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},9).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_10}]},22).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape}]},1).wait(1));

	// Layer 3
	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(12,1,1).p("ABgAdQhNg0hPgLQgiAVgBAw");
	this.shape_16.setTransform(19.8,113.2);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(12,1,1).p("AhqAcQALgmArgRQBOAIBQAp");
	this.shape_17.setTransform(15.8,113.2);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#000000").ss(12,1,1).p("AhzAVQAUgbAzgOQBOAFBSAe");
	this.shape_18.setTransform(12.2,113.2);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#000000").ss(12,1,1).p("Ah8APQAcgSA7gLQBNACBVAV");
	this.shape_19.setTransform(9,113.2);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#000000").ss(12,1,1).p("AiEAKQAkgLBAgIQBNgBBYAO");
	this.shape_20.setTransform(6.3,113.2);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#000000").ss(12,1,1).p("AiKAGQApgFBGgFQBMgDBaAI");
	this.shape_21.setTransform(4,113.1);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#000000").ss(12,1,1).p("AiPAEQAuAABKgEQBMgEBbAD");
	this.shape_22.setTransform(2,113);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#000000").ss(12,1,1).p("AiTACQAyAEBNgDQBMgFBcgB");
	this.shape_23.setTransform(0.6,112.9);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#000000").ss(12,1,1).p("AiWAAQA0AHBQgCQBNgGBcgE");
	this.shape_24.setTransform(-0.5,112.9);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f().s("#000000").ss(12,1,1).p("AiYAAQA2AIBSgCQBMgGBdgG");
	this.shape_25.setTransform(-1.1,112.9);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f().s("#000000").ss(12,1,1).p("ACagGQheAGhMAHQhRABg4gJ");
	this.shape_26.setTransform(-1.3,112.9);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f().s("#000000").ss(12,1,1).p("AiJAHQAogGBFgGQBNgCBZAI");
	this.shape_27.setTransform(4.3,113.1);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f().s("#000000").ss(12,1,1).p("AhyAWQATgdAxgOQBOAFBTAh");
	this.shape_28.setTransform(12.9,113.2);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f().s("#000000").ss(12,1,1).p("AhpAcQALgmAqgRQBOAIBQAp");
	this.shape_29.setTransform(15.9,113.2);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f().s("#000000").ss(12,1,1).p("AhjAgQAFgsAmgTQBOAKBOAv");
	this.shape_30.setTransform(18.1,113.2);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f().s("#000000").ss(12,1,1).p("AhgAiQACgvAjgUQBOALBOAy");
	this.shape_31.setTransform(19.4,113.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_16}]}).to({state:[{t:this.shape_16}]},9).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_26}]},22).to({state:[{t:this.shape_27}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_28}]},1).to({state:[{t:this.shape_29}]},1).to({state:[{t:this.shape_30}]},1).to({state:[{t:this.shape_31}]},1).to({state:[{t:this.shape_16}]},1).wait(1));

	// Layer 2
	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f().s("#000000").ss(12,1,1).p("AG9pTQBODrgND0QgOD+htCzQh4DGjVA7QjuBClHiD");
	this.shape_32.setTransform(80.5,65.2);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f().s("#000000").ss(12,1,1).p("AoCIFQE5BnDgg/QDLg5CBi2QB3inAfj7QAfjyhDju");
	this.shape_33.setTransform(78.1,63.7);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f().s("#000000").ss(12,1,1).p("AoHILQEtBODTg7QDCg3CIipQCBicAvj3QAvjyg5jw");
	this.shape_34.setTransform(76.1,62.4);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f().s("#000000").ss(12,1,1).p("AoNIRQEiA4DIg5QC7g1COidQCJiTA9j0QA+jwgxjz");
	this.shape_35.setTransform(74.5,61.3);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f().s("#000000").ss(12,1,1).p("AoUIVQEaAlC+g2QC0g0CUiSQCPiMBKjyQBKjugpj1");
	this.shape_36.setTransform(73.2,60.4);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f().s("#000000").ss(12,1,1).p("AoaIYQESAUC2g0QCugzCZiJQCViFBVjvQBUjugjj2");
	this.shape_37.setTransform(72.2,59.8);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f().s("#000000").ss(12,1,1).p("AoeIYQELAHCvgyQCqgyCciCQCbh/BdjuQBcjtgej3");
	this.shape_38.setTransform(71.4,59.4);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f().s("#000000").ss(12,1,1).p("AojIYQEGgECqgxQCngwCfh9QCeh8BkjsQBjjtgaj4");
	this.shape_39.setTransform(70.8,59.2);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f().s("#000000").ss(12,1,1).p("AomIXQECgLCngwQCjgxCih4QChh4BojsQBpjsgYj5");
	this.shape_40.setTransform(70.4,59.1);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f().s("#000000").ss(12,1,1).p("AooIWQEAgPCkgvQCjgwCjh3QCih2BrjrQBrjsgVj5");
	this.shape_41.setTransform(70.1,59.1);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f().s("#000000").ss(12,1,1).p("AImoVQAVD6hsDrQhsDrijB2QikB1iiAwQijAvj/AR");
	this.shape_42.setTransform(70,59);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f().s("#000000").ss(12,1,1).p("AoYIXQESAXC3g0QCvgzCYiLQCViGBTjvQBTjvgkj1");
	this.shape_43.setTransform(72.3,59.9);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f().s("#000000").ss(12,1,1).p("AoNIRQEiA4DIg5QC6g1CPidQCIiTA+j0QA+jwgxjz");
	this.shape_44.setTransform(74.5,61.3);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f().s("#000000").ss(12,1,1).p("AoGIKQEwBTDWg8QDDg3CHisQB+ieAtj4QAsjyg8jv");
	this.shape_45.setTransform(76.5,62.7);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.f().s("#000000").ss(12,1,1).p("AoCIEQE6BoDgg+QDMg5CAi3QB3ioAfj6QAejzhDjt");
	this.shape_46.setTransform(78.1,63.8);

	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.f().s("#000000").ss(12,1,1).p("AoAIAQFBB3DohAQDRg6B8i/QBxiuAVj9QAVjzhJjs");
	this.shape_47.setTransform(79.4,64.6);

	this.shape_48 = new cjs.Shape();
	this.shape_48.graphics.f().s("#000000").ss(12,1,1).p("An/H9QFGCADshBQDUg7B5jEQBuiyAQj9QAPj0hNjr");
	this.shape_48.setTransform(80.2,65.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_32}]}).to({state:[{t:this.shape_32}]},9).to({state:[{t:this.shape_33}]},1).to({state:[{t:this.shape_34}]},1).to({state:[{t:this.shape_35}]},1).to({state:[{t:this.shape_36}]},1).to({state:[{t:this.shape_37}]},1).to({state:[{t:this.shape_38}]},1).to({state:[{t:this.shape_39}]},1).to({state:[{t:this.shape_40}]},1).to({state:[{t:this.shape_41}]},1).to({state:[{t:this.shape_42}]},1).to({state:[{t:this.shape_42}]},22).to({state:[{t:this.shape_43}]},1).to({state:[{t:this.shape_44}]},1).to({state:[{t:this.shape_45}]},1).to({state:[{t:this.shape_46}]},1).to({state:[{t:this.shape_47}]},1).to({state:[{t:this.shape_48}]},1).to({state:[{t:this.shape_32}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,-0.3,138,131.4);


(lib.Symbol9 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 6
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(12,1,1).p("AiUgpQB2AWCzA9");
	this.shape.setTransform(78.5,18);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(12,1,1).p("AiVgaQB2AKC0Ar");
	this.shape_1.setTransform(78.9,17.2);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(12,1,1).p("AiVgMQB1gBC2Aa");
	this.shape_2.setTransform(79.3,16.5);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(12,1,1).p("AiVABQB0gJC3AL");
	this.shape_3.setTransform(79.6,15.7);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(12,1,1).p("AiVAKQBzgRC4gC");
	this.shape_4.setTransform(80,15.4);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(12,1,1).p("AiVASQBzgXC4gM");
	this.shape_5.setTransform(80.2,15);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(12,1,1).p("AiVAZQBygdC5gU");
	this.shape_6.setTransform(80.4,14.6);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(12,1,1).p("AiVAfQBygiC5gb");
	this.shape_7.setTransform(80.6,14.3);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(12,1,1).p("AiVAjQByglC5gg");
	this.shape_8.setTransform(80.7,14.1);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(12,1,1).p("AiVAmQBxgoC6gj");
	this.shape_9.setTransform(80.8,14);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(12,1,1).p("AiVAmQBxgnC6gk");
	this.shape_10.setTransform(80.8,13.9);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(12,1,1).p("AiVAVQBzgaC4gP");
	this.shape_11.setTransform(80.3,14.8);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(12,1,1).p("AiVAHQB0gPC3AD");
	this.shape_12.setTransform(79.9,15.5);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(12,1,1).p("AiVgFQB1gFC2AR");
	this.shape_13.setTransform(79.5,16.1);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(12,1,1).p("AiVgQQB1ADC2Ae");
	this.shape_14.setTransform(79.2,16.7);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(12,1,1).p("AiUgZQB1AKC0Ap");
	this.shape_15.setTransform(78.9,17.2);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(12,1,1).p("AiUggQB2APCzAy");
	this.shape_16.setTransform(78.7,17.6);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(12,1,1).p("AiUglQB2ATCzA4");
	this.shape_17.setTransform(78.6,17.8);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#000000").ss(12,1,1).p("AiUgoQB2AVCzA8");
	this.shape_18.setTransform(78.5,18);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},9).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_10}]},20).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape}]},1).wait(1));

	// Layer 3
	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#000000").ss(12,1,1).p("AiUBDQB0hLC1g6");
	this.shape_19.setTransform(78.5,7.1);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#000000").ss(12,1,1).p("AiPBMQBvhSCvhF");
	this.shape_20.setTransform(78.4,7);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#000000").ss(12,1,1).p("AiKBTQBqhXCqhO");
	this.shape_21.setTransform(78.3,6.8);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#000000").ss(12,1,1).p("AiFBaQBlhcCmhX");
	this.shape_22.setTransform(78.3,6.7);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#000000").ss(12,1,1).p("AiBBgQBghhCjhe");
	this.shape_23.setTransform(78.2,6.7);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#000000").ss(12,1,1).p("Ah+BlQBehlCfhk");
	this.shape_24.setTransform(78.1,6.6);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f().s("#000000").ss(12,1,1).p("Ah7BpQBbhpCcho");
	this.shape_25.setTransform(78.1,6.5);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f().s("#000000").ss(12,1,1).p("Ah5BsQBZhrCahs");
	this.shape_26.setTransform(78.1,6.4);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f().s("#000000").ss(12,1,1).p("Ah3BvQBWhtCZhw");
	this.shape_27.setTransform(78.1,6.4);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f().s("#000000").ss(12,1,1).p("Ah3BwQBWhuCZhx");
	this.shape_28.setTransform(78,6.4);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f().s("#000000").ss(12,1,1).p("Ah2BwQBVhuCYhx");
	this.shape_29.setTransform(78,6.4);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f().s("#000000").ss(12,1,1).p("Ah8BnQBchnCehm");
	this.shape_30.setTransform(78.1,6.5);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f().s("#000000").ss(12,1,1).p("AiCBfQBihgCjhd");
	this.shape_31.setTransform(78.2,6.7);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f().s("#000000").ss(12,1,1).p("AiHBXQBnhaCohT");
	this.shape_32.setTransform(78.3,6.8);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f().s("#000000").ss(12,1,1).p("AiLBRQBrhWCshL");
	this.shape_33.setTransform(78.4,6.9);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f().s("#000000").ss(12,1,1).p("AiOBMQBuhSCvhF");
	this.shape_34.setTransform(78.4,7);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f().s("#000000").ss(12,1,1).p("AiRBIQBxhPCyhA");
	this.shape_35.setTransform(78.4,7);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f().s("#000000").ss(12,1,1).p("AiTBFQBzhNC0g8");
	this.shape_36.setTransform(78.5,7.1);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f().s("#000000").ss(12,1,1).p("AiUBDQB0hLC1g7");
	this.shape_37.setTransform(78.5,7.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_19}]}).to({state:[{t:this.shape_19}]},9).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_27}]},1).to({state:[{t:this.shape_28}]},1).to({state:[{t:this.shape_29}]},1).to({state:[{t:this.shape_29}]},20).to({state:[{t:this.shape_30}]},1).to({state:[{t:this.shape_31}]},1).to({state:[{t:this.shape_32}]},1).to({state:[{t:this.shape_33}]},1).to({state:[{t:this.shape_34}]},1).to({state:[{t:this.shape_35}]},1).to({state:[{t:this.shape_36}]},1).to({state:[{t:this.shape_37}]},1).to({state:[{t:this.shape_19}]},1).wait(1));

	// Layer 1
	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f().s("#000000").ss(12,1,1).p("AiQhJQCZAvCIBk");
	this.shape_38.setTransform(14.5,7.4);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f().s("#000000").ss(12,1,1).p("AiLhPQCVA2CCBp");
	this.shape_39.setTransform(14.7,7.1);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f().s("#000000").ss(12,1,1).p("AiHhVQCSA8B9Bv");
	this.shape_40.setTransform(14.9,6.9);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f().s("#000000").ss(12,1,1).p("AiDhaQCPBCB4Bz");
	this.shape_41.setTransform(15.1,6.7);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f().s("#000000").ss(12,1,1).p("AiAhfQCNBHB0B4");
	this.shape_42.setTransform(15.2,6.5);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f().s("#000000").ss(12,1,1).p("Ah+hjQCMBLBxB8");
	this.shape_43.setTransform(15.4,6.4);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f().s("#000000").ss(12,1,1).p("Ah8hmQCKBPBvB+");
	this.shape_44.setTransform(15.5,6.2);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f().s("#000000").ss(12,1,1).p("Ah6hoQCJBRBsCA");
	this.shape_45.setTransform(15.6,6.1);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.f().s("#000000").ss(12,1,1).p("Ah5hqQCIBTBrCC");
	this.shape_46.setTransform(15.6,6.1);

	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.f().s("#000000").ss(12,1,1).p("Ah4hrQCHBUBqCD");
	this.shape_47.setTransform(15.7,6);

	this.shape_48 = new cjs.Shape();
	this.shape_48.graphics.f().s("#000000").ss(12,1,1).p("Ah4hsQCHBVBqCE");
	this.shape_48.setTransform(15.7,6);

	this.shape_49 = new cjs.Shape();
	this.shape_49.graphics.f().s("#000000").ss(12,1,1).p("Ah9hkQCLBNBwB8");
	this.shape_49.setTransform(15.4,6.3);

	this.shape_50 = new cjs.Shape();
	this.shape_50.graphics.f().s("#000000").ss(12,1,1).p("AiBheQCNBGB2B3");
	this.shape_50.setTransform(15.2,6.5);

	this.shape_51 = new cjs.Shape();
	this.shape_51.graphics.f().s("#000000").ss(12,1,1).p("AiFhYQCQA/B7By");
	this.shape_51.setTransform(15,6.8);

	this.shape_52 = new cjs.Shape();
	this.shape_52.graphics.f().s("#000000").ss(12,1,1).p("AiIhUQCTA7B+Bu");
	this.shape_52.setTransform(14.8,7);

	this.shape_53 = new cjs.Shape();
	this.shape_53.graphics.f().s("#000000").ss(12,1,1).p("AiLhPQCVA2CCBq");
	this.shape_53.setTransform(14.7,7.1);

	this.shape_54 = new cjs.Shape();
	this.shape_54.graphics.f().s("#000000").ss(12,1,1).p("AiNhNQCXA0CEBn");
	this.shape_54.setTransform(14.6,7.2);

	this.shape_55 = new cjs.Shape();
	this.shape_55.graphics.f().s("#000000").ss(12,1,1).p("AiOhLQCXAxCGBl");
	this.shape_55.setTransform(14.5,7.3);

	this.shape_56 = new cjs.Shape();
	this.shape_56.graphics.f().s("#000000").ss(12,1,1).p("AiPhJQCYAvCHBk");
	this.shape_56.setTransform(14.5,7.4);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_38}]}).to({state:[{t:this.shape_38}]},9).to({state:[{t:this.shape_39}]},1).to({state:[{t:this.shape_40}]},1).to({state:[{t:this.shape_41}]},1).to({state:[{t:this.shape_42}]},1).to({state:[{t:this.shape_43}]},1).to({state:[{t:this.shape_44}]},1).to({state:[{t:this.shape_45}]},1).to({state:[{t:this.shape_46}]},1).to({state:[{t:this.shape_47}]},1).to({state:[{t:this.shape_48}]},1).to({state:[{t:this.shape_48}]},20).to({state:[{t:this.shape_49}]},1).to({state:[{t:this.shape_50}]},1).to({state:[{t:this.shape_51}]},1).to({state:[{t:this.shape_52}]},1).to({state:[{t:this.shape_53}]},1).to({state:[{t:this.shape_54}]},1).to({state:[{t:this.shape_55}]},1).to({state:[{t:this.shape_56}]},1).to({state:[{t:this.shape_38}]},1).wait(1));

	// Layer 2
	this.shape_57 = new cjs.Shape();
	this.shape_57.graphics.f().s("#000000").ss(12,1,1).p("AiUAiQB+gsCrgX");
	this.shape_57.setTransform(14,18.2);

	this.shape_58 = new cjs.Shape();
	this.shape_58.graphics.f().s("#000000").ss(12,1,1).p("AiUATQCAgfCpgG");
	this.shape_58.setTransform(13.8,17.1);

	this.shape_59 = new cjs.Shape();
	this.shape_59.graphics.f().s("#000000").ss(12,1,1).p("AiTAHQCAgTCnAJ");
	this.shape_59.setTransform(13.7,15.9);

	this.shape_60 = new cjs.Shape();
	this.shape_60.graphics.f().s("#000000").ss(12,1,1).p("AiTgEQCBgKCmAW");
	this.shape_60.setTransform(13.5,15);

	this.shape_61 = new cjs.Shape();
	this.shape_61.graphics.f().s("#000000").ss(12,1,1).p("AiSgPQCCgBCkAh");
	this.shape_61.setTransform(13.4,14.4);

	this.shape_62 = new cjs.Shape();
	this.shape_62.graphics.f().s("#000000").ss(12,1,1).p("AiSgYQCCAGCjAr");
	this.shape_62.setTransform(13.3,13.8);

	this.shape_63 = new cjs.Shape();
	this.shape_63.graphics.f().s("#000000").ss(12,1,1).p("AiSgfQCDANCiAy");
	this.shape_63.setTransform(13.2,13.2);

	this.shape_64 = new cjs.Shape();
	this.shape_64.graphics.f().s("#000000").ss(12,1,1).p("AiSglQCEASChA5");
	this.shape_64.setTransform(13.2,12.8);

	this.shape_65 = new cjs.Shape();
	this.shape_65.graphics.f().s("#000000").ss(12,1,1).p("AiSgpQCFAVCgA+");
	this.shape_65.setTransform(13.1,12.5);

	this.shape_66 = new cjs.Shape();
	this.shape_66.graphics.f().s("#000000").ss(12,1,1).p("AiRgrQCEAXCfBA");
	this.shape_66.setTransform(13.1,12.4);

	this.shape_67 = new cjs.Shape();
	this.shape_67.graphics.f().s("#000000").ss(12,1,1).p("AiRgsQCEAYCfBB");
	this.shape_67.setTransform(13.1,12.3);

	this.shape_68 = new cjs.Shape();
	this.shape_68.graphics.f().s("#000000").ss(12,1,1).p("AiSgbQCDAJCiAu");
	this.shape_68.setTransform(13.3,13.5);

	this.shape_69 = new cjs.Shape();
	this.shape_69.graphics.f().s("#000000").ss(12,1,1).p("AiSgNQCBgDCkAe");
	this.shape_69.setTransform(13.4,14.6);

	this.shape_70 = new cjs.Shape();
	this.shape_70.graphics.f().s("#000000").ss(12,1,1).p("AiTACQCBgNCmAP");
	this.shape_70.setTransform(13.6,15.2);

	this.shape_71 = new cjs.Shape();
	this.shape_71.graphics.f().s("#000000").ss(12,1,1).p("AiUAKQCBgXCoAF");
	this.shape_71.setTransform(13.7,16.3);

	this.shape_72 = new cjs.Shape();
	this.shape_72.graphics.f().s("#000000").ss(12,1,1).p("AiUATQCAgfCpgF");
	this.shape_72.setTransform(13.8,17);

	this.shape_73 = new cjs.Shape();
	this.shape_73.graphics.f().s("#000000").ss(12,1,1).p("AiUAZQB/gkCqgN");
	this.shape_73.setTransform(13.9,17.5);

	this.shape_74 = new cjs.Shape();
	this.shape_74.graphics.f().s("#000000").ss(12,1,1).p("AiUAeQB/gpCqgS");
	this.shape_74.setTransform(13.9,17.9);

	this.shape_75 = new cjs.Shape();
	this.shape_75.graphics.f().s("#000000").ss(12,1,1).p("AiUAhQB+gsCrgV");
	this.shape_75.setTransform(14,18.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_57}]}).to({state:[{t:this.shape_57}]},9).to({state:[{t:this.shape_58}]},1).to({state:[{t:this.shape_59}]},1).to({state:[{t:this.shape_60}]},1).to({state:[{t:this.shape_61}]},1).to({state:[{t:this.shape_62}]},1).to({state:[{t:this.shape_63}]},1).to({state:[{t:this.shape_64}]},1).to({state:[{t:this.shape_65}]},1).to({state:[{t:this.shape_66}]},1).to({state:[{t:this.shape_67}]},1).to({state:[{t:this.shape_67}]},20).to({state:[{t:this.shape_68}]},1).to({state:[{t:this.shape_69}]},1).to({state:[{t:this.shape_70}]},1).to({state:[{t:this.shape_71}]},1).to({state:[{t:this.shape_72}]},1).to({state:[{t:this.shape_73}]},1).to({state:[{t:this.shape_74}]},1).to({state:[{t:this.shape_75}]},1).to({state:[{t:this.shape_57}]},1).wait(1));

	// Layer 7
	this.shape_76 = new cjs.Shape();
	this.shape_76.graphics.f().s("#000000").ss(12,1,1).p("AjegoQAOAYAVATQBDBBBvADQBuAABLhLQAlgmAKgc");
	this.shape_76.setTransform(46.1,54.3);

	this.shape_77 = new cjs.Shape();
	this.shape_77.graphics.f().s("#000000").ss(12,1,1).p("AjkgpQAUAfAVAUQBDBABvAEQBugBBLhLQAmglAPgq");
	this.shape_77.setTransform(46.1,53.6);

	this.shape_78 = new cjs.Shape();
	this.shape_78.graphics.f().s("#000000").ss(12,1,1).p("AjpgqQAaAmAVATQBDBBBvADQBuAABLhMQAlglAUg1");
	this.shape_78.setTransform(46.1,53.1);

	this.shape_79 = new cjs.Shape();
	this.shape_79.graphics.f().s("#000000").ss(12,1,1).p("AjugrQAfArAVAUQBDBBBvADQBuAABLhMQAlglAZg/");
	this.shape_79.setTransform(46,52.6);

	this.shape_80 = new cjs.Shape();
	this.shape_80.graphics.f().s("#000000").ss(12,1,1).p("AjygsQAjAwAVAVQBDBABvAEQBuAABLhMQAmglAchI");
	this.shape_80.setTransform(46,52.1);

	this.shape_81 = new cjs.Shape();
	this.shape_81.graphics.f().s("#000000").ss(12,1,1).p("Aj1gsQAmA0AVAVQBDBABvAEQBugBBLhLQAmglAfhQ");
	this.shape_81.setTransform(46,51.7);

	this.shape_82 = new cjs.Shape();
	this.shape_82.graphics.f().s("#000000").ss(12,1,1).p("Aj4gtQApA4AVAVQBDBABvAEQBvgBBLhLQAlglAhhW");
	this.shape_82.setTransform(46,51.4);

	this.shape_83 = new cjs.Shape();
	this.shape_83.graphics.f().s("#000000").ss(12,1,1).p("Aj6gtQArA7AVAUQBDBBBvADQBvAABLhMQAlglAjha");
	this.shape_83.setTransform(46,51.2);

	this.shape_84 = new cjs.Shape();
	this.shape_84.graphics.f().s("#000000").ss(12,1,1).p("Aj7gtQAsA8AVAVQBDBABvAEQBvAABLhMQAlglAlhe");
	this.shape_84.setTransform(46,51);

	this.shape_85 = new cjs.Shape();
	this.shape_85.graphics.f().s("#000000").ss(12,1,1).p("Aj8guQAuA+AVAVQBDBABvAEQBugBBLhLQAlglAmhg");
	this.shape_85.setTransform(45.9,50.9);

	this.shape_86 = new cjs.Shape();
	this.shape_86.graphics.f().s("#000000").ss(12,1,1).p("Aj8guQAuA/AVAUQBDBBBvADQBuAABLhMQAlglAmhg");
	this.shape_86.setTransform(45.9,50.9);

	this.shape_87 = new cjs.Shape();
	this.shape_87.graphics.f().s("#000000").ss(12,1,1).p("Aj2gtQAnA2AVAVQBDBABvAEQBvAABLhMQAlglAghS");
	this.shape_87.setTransform(46,51.6);

	this.shape_88 = new cjs.Shape();
	this.shape_88.graphics.f().s("#000000").ss(12,1,1).p("AjxgsQAiAvAVAVQBDBABvAEQBugBBLhLQAmglAbhG");
	this.shape_88.setTransform(46,52.2);

	this.shape_89 = new cjs.Shape();
	this.shape_89.graphics.f().s("#000000").ss(12,1,1).p("AjsgrQAdAqAVATQBDBBBvADQBuAABLhMQAlglAXg6");
	this.shape_89.setTransform(46.1,52.8);

	this.shape_90 = new cjs.Shape();
	this.shape_90.graphics.f().s("#000000").ss(12,1,1).p("AjngqQAYAkAVAUQBDBABvAEQBugBBLhLQAlglATgy");
	this.shape_90.setTransform(46.1,53.2);

	this.shape_91 = new cjs.Shape();
	this.shape_91.graphics.f().s("#000000").ss(12,1,1).p("AjkgqQAUAgAVAUQBDBABvAEQBuAABLhMQAmglAPgq");
	this.shape_91.setTransform(46.1,53.6);

	this.shape_92 = new cjs.Shape();
	this.shape_92.graphics.f().s("#000000").ss(12,1,1).p("AjigpQASAcAVAUQBDBABvAEQBugBBLhKQAmgmANgk");
	this.shape_92.setTransform(46.1,53.9);

	this.shape_93 = new cjs.Shape();
	this.shape_93.graphics.f().s("#000000").ss(12,1,1).p("AjggoQAQAZAVAUQBDBABvAEQBuAABLhLQAlgmAMgg");
	this.shape_93.setTransform(46.1,54.1);

	this.shape_94 = new cjs.Shape();
	this.shape_94.graphics.f().s("#000000").ss(12,1,1).p("AjfgoQAPAYAVAUQBDBABvAEQBugBBLhKQAlgmALge");
	this.shape_94.setTransform(46.1,54.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_76}]}).to({state:[{t:this.shape_76}]},9).to({state:[{t:this.shape_77}]},1).to({state:[{t:this.shape_78}]},1).to({state:[{t:this.shape_79}]},1).to({state:[{t:this.shape_80}]},1).to({state:[{t:this.shape_81}]},1).to({state:[{t:this.shape_82}]},1).to({state:[{t:this.shape_83}]},1).to({state:[{t:this.shape_84}]},1).to({state:[{t:this.shape_85}]},1).to({state:[{t:this.shape_86}]},1).to({state:[{t:this.shape_86}]},20).to({state:[{t:this.shape_87}]},1).to({state:[{t:this.shape_88}]},1).to({state:[{t:this.shape_89}]},1).to({state:[{t:this.shape_90}]},1).to({state:[{t:this.shape_91}]},1).to({state:[{t:this.shape_92}]},1).to({state:[{t:this.shape_93}]},1).to({state:[{t:this.shape_94}]},1).to({state:[{t:this.shape_76}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-6.9,-6,106.4,73.4);


(lib.Symbol8 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.hand1();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol8, new cjs.Rectangle(0,0,110,50), null);


(lib.Symbol7 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.hand1_part();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol7, new cjs.Rectangle(0,0,45,64), null);


(lib.Symbol6 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AgZAaQgLgLAAgPQAAgOALgMQALgLAOAAQAPAAALALQALAMAAAOQAAAPgLALQgLALgPAAQgOAAgLgLg");
	this.shape.setTransform(3.7,3.8);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol6, new cjs.Rectangle(0,0,7.5,7.5), null);


(lib.Symbol5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AgZAaQgLgLAAgPQAAgOALgMQALgLAOAAQAPAAALALQALAMAAAOQAAAPgLALQgLALgPAAQgOAAgLgLg");
	this.shape.setTransform(3.7,3.8);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol5, new cjs.Rectangle(0,0,7.5,7.5), null);


(lib.Symbol3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.hands();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol3, new cjs.Rectangle(0,0,128,176), null);


(lib.Symbol2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.teeth();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol2, new cjs.Rectangle(0,0,16,15), null);


(lib.Symbol1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.mouth1();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol1, new cjs.Rectangle(0,0,35,97), null);


(lib.Symbol4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// hand1_part.png
	this.instance = new lib.Symbol7();
	this.instance.parent = this;
	this.instance.setTransform(25.3,62.6,1,1,0,0,0,25.3,62.6);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({rotation:-24.7},4).to({rotation:9.5},5).to({rotation:0},5).wait(6));

	// Layer 4
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(12,1,1).p("AAPAtIgdhZ");
	this.shape.setTransform(23.7,58.2);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(20));

	// hand1.png
	this.instance_1 = new lib.Symbol8();
	this.instance_1.parent = this;
	this.instance_1.setTransform(122.7,76.9,1,1,0,0,0,103.8,18.9);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(20));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,128.9,108);


// stage content:
(lib._1april_Driver = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 16
	this.instance = new lib.Symbol9();
	this.instance.parent = this;
	this.instance.setTransform(800,278.9,1,1,0,0,0,14.5,7.4);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(19).to({y:283.9},3).to({y:278.9},4).to({y:283.9},3).to({y:278.9},4).to({y:283.9},3).to({y:278.9},4).wait(9));

	// Layer 9
	this.instance_1 = new lib.Symbol2();
	this.instance_1.parent = this;
	this.instance_1.setTransform(664,298.5,1,1,0,0,0,8,7.5);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(2).to({y:316.7},5).to({y:292.2},4).to({y:298.5},4).wait(3).to({y:316.7},5).to({y:292.2},4).to({y:298.5},4).wait(3).to({y:316.7},5).to({y:292.2},4).to({y:298.5},4).wait(2));

	// Layer 8
	this.instance_2 = new lib.Symbol1();
	this.instance_2.parent = this;
	this.instance_2.setTransform(667.6,335.6,1,1,0,0,0,17.5,48.5);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(2).to({scaleY:0.61},5).to({scaleY:1.11},4).to({scaleY:1},4).wait(3).to({scaleY:0.61},5).to({scaleY:1.11},4).to({scaleY:1},4).wait(3).to({scaleY:0.61},5).to({scaleY:1.11},4).to({scaleY:1},4).wait(2));

	// Layer 7
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#CFCFCF").s().p("AhxAMQAvg2BLAAQBLAAAkAkQAjAkgogcQgngagOgCQgPgDgfABQgfABgXAGQgXAHgyAoQgVARgDAAQgFAAAbgfg");
	this.shape.setTransform(690.2,244);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#CFCFCF").s().p("AioBFQgDgoAyg7QAzg7BLAAQBLAAA3A6QA2A3gegEQgfgEghgIQgggIhVAYQhWAYgdAdQgNANgHAAQgKAAgBgVg");
	this.shape_1.setTransform(690.7,248.7);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#CFCFCF").s().p("ACECKQg1gMhLgIQhKgIg1AUQg2ATgEhVQgChUAyg8QAzg6BMAAQBKAAA3A6QA3A8ADBUQAEBLgpAAIgMgBg");
	this.shape_2.setTransform(691.9,253.6);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#CFCFCF").s().p("Ah7CVQg2gcgEhVQgChUAyg8QAzg7BMAAQBKAAA3A7QA3A8ADBUQAEBVg0ALIh/AdQgaAHgZAAQgrAAgjgTg");
	this.shape_3.setTransform(691.9,256.5);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#CFCFCF").s().p("Ah6CQQg3g7gEhVQgChUAyg8QAzg7BMAAQBKAAA3A7QA3A8ADBUQAEBVg0A7QgyA7hKABQhLgBg4g7g");
	this.shape_4.setTransform(691.9,260.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape}]},19).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape}]},1).to({state:[]},1).wait(20));

	// Layer 11
	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#CFCFCF").s().p("AhxAMQAvg2BLAAQBMAAAjAkQAkAkgogcQgpgagNgCQgPgDgfABQgfABgXAGQgXAHgxAnQgWASgDAAQgFAAAbgfg");
	this.shape_5.setTransform(647.3,240.8);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#CFCFCF").s().p("AipBEQgCgnAyg8QAzg6BLAAQBLAAA3A5QA2A4gegEQgfgEghgIQgggIhVAYQhWAYgdAdQgMANgIAAQgKAAgCgWg");
	this.shape_6.setTransform(647.7,245.5);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#CFCFCF").s().p("ACECKQg1gMhLgIQhKgIg1AUQg2ATgEhUQgChUAyg9QAzg6BMAAQBKAAA3A6QA4A9ACBUQAEBKgpAAIgMgBg");
	this.shape_7.setTransform(648.9,250.4);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#CFCFCF").s().p("Ah6CWQg3gdgEhVQgChUAyg8QAzg6BMAAQBKAAA3A6QA4A8ACBUQAFBVg1AMIh/AcQgbAGgZAAQgqAAgigRg");
	this.shape_8.setTransform(648.9,253.2);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#CFCFCF").s().p("Ah6CPQg3g7gEhUQgChUAyg9QAzg5BMAAQBKAAA3A5QA4A9ACBUQAFBUg1A7QgyA9hKgBQhLABg4g9g");
	this.shape_9.setTransform(648.9,256.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_5}]},19).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_5}]},1).to({state:[]},1).wait(20));

	// Layer 12
	this.instance_3 = new lib.Symbol5();
	this.instance_3.parent = this;
	this.instance_3.setTransform(682.5,254.4,1.728,1.785,0,1.2,0,3.8,3.8);

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(9).to({x:689.3,y:261.6},5,cjs.Ease.get(0.7)).wait(21).to({x:682.5,y:254.4},5).wait(9));

	// Layer 13
	this.instance_4 = new lib.Symbol6();
	this.instance_4.parent = this;
	this.instance_4.setTransform(641.9,253.2,1.728,1.785,0,1.2,0,3.8,3.8);

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(9).to({regX:3.6,x:648.4,y:260.4},5,cjs.Ease.get(0.7)).wait(21).to({regX:3.8,x:641.9,y:253.2},5).wait(9));

	// Layer 14
	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("Ah6CQQg3g7gEhVQgChUAyg8QAzg7BMAAQBKAAA3A7QA3A8ADBUQAEBVg0A7QgyA7hKABQhLgBg4g7g");
	this.shape_10.setTransform(691.9,260.1);

	this.timeline.addTween(cjs.Tween.get(this.shape_10).wait(49));

	// Layer 15
	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("Ah6CPQg3g7gEhUQgChUAyg9QAzg5BMAAQBKAAA3A5QA4A9ACBUQAFBUg1A7QgyA9hKgBQhLABg4g9g");
	this.shape_11.setTransform(648.9,256.9);

	this.timeline.addTween(cjs.Tween.get(this.shape_11).wait(49));

	// Layer 5
	this.instance_5 = new lib.Symbol10();
	this.instance_5.parent = this;
	this.instance_5.setTransform(890.5,394.7,1,1,0,0,0,69,65.5);

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(49));

	// Layer 3
	this.instance_6 = new lib.pack2();
	this.instance_6.parent = this;
	this.instance_6.setTransform(770,158.8);

	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(49));

	// Layer 4
	this.instance_7 = new lib.Symbol3();
	this.instance_7.parent = this;
	this.instance_7.setTransform(760.5,336.4,1,1,0,0,0,73.3,5.2);

	this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(1).to({regX:64,regY:88,x:751.2,y:419.2},0).wait(1).to({skewX:-0.1,x:751.3},0).wait(1).to({skewX:-0.2,x:751.5},0).wait(1).to({skewX:-0.4,x:751.7},0).wait(1).to({scaleY:1,skewX:-0.6,x:752},0).wait(1).to({skewX:-0.9,x:752.4},0).wait(1).to({skewX:-1.2,x:752.9,y:419.3},0).wait(1).to({scaleY:1,skewX:-1.6,x:753.5},0).wait(1).to({skewX:-2,x:754.1},0).wait(1).to({scaleY:1,skewX:-2.6,x:754.9},0).wait(1).to({skewX:-3.1,x:755.7},0).wait(1).to({scaleY:1,skewX:-3.7,x:756.5},0).wait(1).to({scaleY:1,skewX:-4.2,x:757.3},0).wait(1).to({skewX:-4.8,x:758.1},0).wait(1).to({scaleY:1.01,skewX:-5.3,x:758.8},0).wait(1).to({skewX:-5.7,x:759.4},0).wait(1).to({scaleY:1.01,skewX:-6.1,x:760},0).wait(1).to({skewX:-6.4,x:760.4,y:419.2},0).wait(1).to({skewX:-6.6,x:760.8},0).wait(1).to({skewX:-6.8,x:761},0).wait(1).to({scaleY:1.01,skewX:-7,x:761.3},0).wait(1).to({skewX:-7.1,x:761.4},0).wait(1).to({x:761.5},0).wait(1).to({regX:73.3,regY:5.2,skewX:-7.2,x:760.5,y:336.4},0).wait(1).to({regX:64,regY:88,skewX:-7.1,x:761.5,y:419.2},0).wait(1).to({skewX:-7,x:761.4},0).wait(1).to({skewX:-6.9,x:761.2},0).wait(1).to({scaleY:1.01,skewX:-6.8,x:761},0).wait(1).to({skewX:-6.6,x:760.7},0).wait(1).to({skewX:-6.3,x:760.3},0).wait(1).to({skewX:-6,x:759.8,y:419.3},0).wait(1).to({scaleY:1.01,skewX:-5.6,x:759.2},0).wait(1).to({skewX:-5.1,x:758.6},0).wait(1).to({scaleY:1,skewX:-4.6,x:757.8},0).wait(1).to({scaleY:1,skewX:-4.1,x:757},0).wait(1).to({skewX:-3.5,x:756.2},0).wait(1).to({scaleY:1,skewX:-2.9,x:755.4},0).wait(1).to({skewX:-2.4,x:754.6},0).wait(1).to({scaleY:1,skewX:-1.9,x:753.9},0).wait(1).to({skewX:-1.5,x:753.3},0).wait(1).to({scaleY:1,skewX:-1.1,x:752.7},0).wait(1).to({skewX:-0.8,x:752.3,y:419.2},0).wait(1).to({skewX:-0.6,x:752},0).wait(1).to({scaleY:1,skewX:-0.4,x:751.7},0).wait(1).to({skewX:-0.2,x:751.4},0).wait(1).to({skewX:-0.1,x:751.3},0).wait(1).to({skewX:0,x:751.2},0).wait(1).to({regX:73.3,regY:5.2,x:760.5,y:336.4},0).wait(1));

	// Layer 2
	this.instance_8 = new lib.pack1();
	this.instance_8.parent = this;
	this.instance_8.setTransform(566,147);

	this.timeline.addTween(cjs.Tween.get(this.instance_8).wait(49));

	// Layer 6
	this.instance_9 = new lib.Symbol4();
	this.instance_9.parent = this;
	this.instance_9.setTransform(602,377,1,1,0,0,0,119,81);

	this.timeline.addTween(cjs.Tween.get(this.instance_9).wait(49));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(540,340,1080,680);
// library properties:
lib.properties = {
	id: 'C09776AF3DC41D4D8499991B092F0732',
	width: 1080,
	height: 680,
	fps: 30,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: [
		{src:"assets/images/hand1.png", id:"hand1"},
		{src:"assets/images/hand1_part.png", id:"hand1_part"},
		{src:"assets/images/hands.png", id:"hands"},
		{src:"assets/images/mouth1.png", id:"mouth1"},
		{src:"assets/images/pack3.png", id:"pack1"},
		{src:"assets/images/pack4.png", id:"pack2"},
		{src:"assets/images/teeth.png", id:"teeth"}
	],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['C09776AF3DC41D4D8499991B092F0732'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}



})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;

$(function () {
    var canvas, stage, exportRoot, anim_container, dom_overlay_container, fnStartAnimation;
    function init() {
        canvas = document.getElementById("canvas2");
        anim_container = document.getElementById("animation_container2");
        dom_overlay_container = document.getElementById("dom_overlay_container2");
        var comp=AdobeAn.getComposition("C09776AF3DC41D4D8499991B092F0732");
        var lib=comp.getLibrary();
        var loader = new createjs.LoadQueue(false);
        loader.addEventListener("fileload", function(evt){handleFileLoad(evt,comp)});
        loader.addEventListener("complete", function(evt){handleComplete(evt,comp)});
        var lib=comp.getLibrary();
        loader.loadManifest(lib.properties.manifest);
    }
    function handleFileLoad(evt, comp) {
        var images=comp.getImages();
        if (evt && (evt.item.type == "image")) { images[evt.item.id] = evt.result; }
    }
    function handleComplete(evt,comp) {
        //This function is always called, irrespective of the content. You can use the variable "stage" after it is created in token create_stage.
        var lib=comp.getLibrary();
        var ss=comp.getSpriteSheet();
        var queue = evt.target;
        var ssMetadata = lib.ssMetadata;
        for(i=0; i<ssMetadata.length; i++) {
            ss[ssMetadata[i].name] = new createjs.SpriteSheet( {"images": [queue.getResult(ssMetadata[i].name)], "frames": ssMetadata[i].frames} )
        }
        exportRoot = new lib._1april_Driver();
        stage = new lib.Stage(canvas);
        //Registers the "tick" event listener.
        fnStartAnimation = function() {
            stage.addChild(exportRoot);
            createjs.Ticker.setFPS(lib.properties.fps);
            createjs.Ticker.addEventListener("tick", stage);
        }
        //Code to support hidpi screens and responsive scaling.
        function makeResponsive(isResp, respDim, isScale, scaleType) {
            var lastW, lastH, lastS=1;
            window.addEventListener('resize', resizeCanvas);
            resizeCanvas();
            function resizeCanvas() {
                var w = lib.properties.width, h = lib.properties.height;
                var iw = window.innerWidth, ih=window.innerHeight;
                var pRatio = window.devicePixelRatio || 1, xRatio=iw/w, yRatio=ih/h, sRatio=1;
                if(isResp) {
                    if((respDim=='width'&&lastW==iw) || (respDim=='height'&&lastH==ih)) {
                        sRatio = lastS;
                    }
                    else if(!isScale) {
                        if(iw<w || ih<h)
                            sRatio = Math.min(xRatio, yRatio);
                    }
                    else if(scaleType==1) {
                        sRatio = Math.min(xRatio, yRatio);
                    }
                    else if(scaleType==2) {
                        sRatio = Math.max(xRatio, yRatio);
                    }
                }
                canvas.width = w*pRatio*sRatio;
                canvas.height = h*pRatio*sRatio;
                canvas.style.width = dom_overlay_container.style.width = anim_container.style.width =  w*sRatio+'px';
                canvas.style.height = anim_container.style.height = dom_overlay_container.style.height = h*sRatio+'px';
                stage.scaleX = pRatio*sRatio;
                stage.scaleY = pRatio*sRatio;
                lastW = iw; lastH = ih; lastS = sRatio;
                stage.tickOnUpdate = false;
                stage.update();
                stage.tickOnUpdate = true;
            }
        }
        makeResponsive(true,'both',true,1);
        AdobeAn.compositionLoaded(lib.properties.id);
        fnStartAnimation();
    }
    init();
});