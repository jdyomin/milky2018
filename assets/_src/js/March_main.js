(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [];


// symbols:



(lib.bg = function() {
	this.initialize(img.bg);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,1086,686);


(lib.hand1 = function() {
	this.initialize(img.hand1);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,34,42);


(lib.pack1_1 = function() {
	this.initialize(img.pack1_1);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,278,448);


(lib.pack2_1 = function() {
	this.initialize(img.pack2_1);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,285,407);// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.Symbol11 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(11,1,1).p("ADqg4InTBx");
	this.shape.setTransform(0,-0.025);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol11, new cjs.Rectangle(-28.9,-11.2,57.8,22.4), null);


(lib.Symbol8 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(11,1,1).p("ADAhOQjhBHieBW");
	this.shape.setTransform(-0.025,0);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol8, new cjs.Rectangle(-24.7,-13.4,49.4,26.8), null);


(lib.Symbol5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.hand1();
	this.instance.parent = this;
	this.instance.setTransform(-17,-21);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol5, new cjs.Rectangle(-17,-21,34,42), null);


(lib.Symbol_13_Layer_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(11,1,1).p("AgjA4QAOg+A5gx");

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(11,1,1).p("AgiA4QAOg+A3gx");
	this.shape_1.setTransform(-0.025,-0.025);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(11,1,1).p("AghA4QAOg+A2gy");
	this.shape_2.setTransform(-0.1,-0.05);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(11,1,1).p("AggA5QAOg+Azgz");
	this.shape_3.setTransform(-0.225,-0.125);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(11,1,1).p("AgeA7QAOg/Avg2");
	this.shape_4.setTransform(-0.45,-0.25);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(11,1,1).p("AgbA8QAOg+Apg5");
	this.shape_5.setTransform(-0.75,-0.425);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(11,1,1).p("AgXA/QAOg/Ahg+");
	this.shape_6.setTransform(-1.125,-0.65);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(11,1,1).p("AgTBBQAOg/AZhC");
	this.shape_7.setTransform(-1.55,-0.9);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(11,1,1).p("AgPBEQAOg/ARhI");
	this.shape_8.setTransform(-1.975,-1.15);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(11,1,1).p("AgLBGQANg/AKhM");
	this.shape_9.setTransform(-2.35,-1.375);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(11,1,1).p("AgIBIQANhAAEhP");
	this.shape_10.setTransform(-2.65,-1.55);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(11,1,1).p("AgGBJQANg/AAhS");
	this.shape_11.setTransform(-2.8741,-1.675);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(11,1,1).p("AgFBKQANg/gDhU");
	this.shape_12.setTransform(-2.9735,-1.75);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(11,1,1).p("AgEBKQANg/gFhU");
	this.shape_13.setTransform(-3.0203,-1.775);
	this.shape_13._off = true;

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(11,1,1).p("AgGBJQANg/gBhS");
	this.shape_14.setTransform(-2.8967,-1.675);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(11,1,1).p("AgHBIQANg/AChQ");
	this.shape_15.setTransform(-2.725,-1.575);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(11,1,1).p("AgKBGQANg/AIhM");
	this.shape_16.setTransform(-2.475,-1.425);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(11,1,1).p("AgNBFQANg/AOhK");
	this.shape_17.setTransform(-2.15,-1.25);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#000000").ss(11,1,1).p("AgRBCQAOg/AVhE");
	this.shape_18.setTransform(-1.75,-1.025);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#000000").ss(11,1,1).p("AgVBAQAOg/AdhA");
	this.shape_19.setTransform(-1.35,-0.775);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#000000").ss(11,1,1).p("AgZA9QAOg+Alg8");
	this.shape_20.setTransform(-0.95,-0.55);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#000000").ss(11,1,1).p("AgcA8QAOg+Arg5");
	this.shape_21.setTransform(-0.625,-0.375);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#000000").ss(11,1,1).p("AgfA6QAOg+Axg1");
	this.shape_22.setTransform(-0.375,-0.225);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#000000").ss(11,1,1).p("AghA5QAPg+A0gz");
	this.shape_23.setTransform(-0.2,-0.125);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#000000").ss(11,1,1).p("AgiA4QAOg+A3gy");
	this.shape_24.setTransform(-0.075,-0.05);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape_1,p:{y:-0.025}}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12,p:{x:-2.9735}}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_12,p:{x:-2.99}}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_1,p:{y:0}}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape_1,p:{y:-0.025}}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12,p:{x:-2.9735}}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_12,p:{x:-2.99}}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_1,p:{y:0}}]},1).to({state:[{t:this.shape}]},1).wait(1));
	this.timeline.addTween(cjs.Tween.get(this.shape_13).wait(13).to({_off:false},0).wait(1).to({x:-3.0342,y:-1.8},0).wait(1).to({x:-3.0203},0).to({_off:true},1).wait(26).to({_off:false,y:-1.775},0).wait(1).to({x:-3.0342,y:-1.8},0).wait(1).to({x:-3.0203},0).to({_off:true},1).wait(14));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_12_Layer_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(11,1,1).p("AAYBOQg1hWAHhF");
	this.shape.setTransform(-0.2689,-0.025);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(11,1,1).p("AgWhNQgHBFA1BW");
	this.shape_1.setTransform(-0.2655,-0.025);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(11,1,1).p("AgWhNQgFBFAyBW");
	this.shape_2.setTransform(-0.2471,-0.025);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(11,1,1).p("AgWhNQgCBEAvBX");
	this.shape_3.setTransform(-0.229,-0.025);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(11,1,1).p("AgWhNQADBDAqBY");
	this.shape_4.setTransform(-0.225,-0.025);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(11,1,1).p("AgWhNQAKBBAjBa");
	this.shape_5.setTransform(-0.225,-0.025);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(11,1,1).p("AgWhNQATA/AaBc");
	this.shape_6.setTransform(-0.225,-0.025);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(11,1,1).p("AgWhNQAcA8ARBf");
	this.shape_7.setTransform(-0.225,-0.025);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(11,1,1).p("AgWhNQAmA5AHBi");
	this.shape_8.setTransform(-0.225,-0.025);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(11,1,1).p("AgWhNQAvA3gCBk");
	this.shape_9.setTransform(-0.221,-0.025);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(11,1,1).p("AgXhNQA3A1gJBm");
	this.shape_10.setTransform(-0.1622,-0.025);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(11,1,1).p("AgYhNQA8A0gOBn");
	this.shape_11.setTransform(-0.0935,-0.025);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(11,1,1).p("AgYhNQA+AzgRBo");
	this.shape_12.setTransform(-0.0455,-0.025);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(11,1,1).p("AgYhNQBAAzgTBo");
	this.shape_13.setTransform(-0.0114,-0.025);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(11,1,1).p("AAVBOQAUhohBgz");
	this.shape_14.setTransform(-0.0026,-0.025);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(11,1,1).p("AgYhNQA/AzgSBo");
	this.shape_15.setTransform(-0.0371,-0.025);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(11,1,1).p("AgYhNQA9A0gPBn");
	this.shape_16.setTransform(-0.0779,-0.025);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(11,1,1).p("AgXhNQA4A1gLBm");
	this.shape_17.setTransform(-0.1433,-0.025);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#000000").ss(11,1,1).p("AgWhNQAyA3gFBk");
	this.shape_18.setTransform(-0.2068,-0.025);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#000000").ss(11,1,1).p("AgWhNQAqA4ADBj");
	this.shape_19.setTransform(-0.225,-0.025);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#000000").ss(11,1,1).p("AgWhNQAhA7AMBg");
	this.shape_20.setTransform(-0.225,-0.025);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#000000").ss(11,1,1).p("AgWhNQAXA9AWBe");
	this.shape_21.setTransform(-0.225,-0.025);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#000000").ss(11,1,1).p("AgWhNQAPBAAeBb");
	this.shape_22.setTransform(-0.225,-0.025);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#000000").ss(11,1,1).p("AgWhNQAHBBAmBa");
	this.shape_23.setTransform(-0.225,-0.025);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#000000").ss(11,1,1).p("AgWhNQABBDAsBY");
	this.shape_24.setTransform(-0.225,-0.025);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f().s("#000000").ss(11,1,1).p("AgWhNQgDBEAwBX");
	this.shape_25.setTransform(-0.2336,-0.025);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f().s("#000000").ss(11,1,1).p("AgWhNQgFBFAzBW");
	this.shape_26.setTransform(-0.2513,-0.025);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_10_Layer_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(11,1,1).p("AgtBEQAehPA9g4");
	this.shape.setTransform(0,0.025);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(11,1,1).p("AgsBEQAdhPA8g4");
	this.shape_1.setTransform(-0.025,0.025);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(11,1,1).p("AgrBFQAdhQA6g5");
	this.shape_2.setTransform(-0.125,0);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(11,1,1).p("AgqBFQAehQA3g5");
	this.shape_3.setTransform(-0.275,0);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(11,1,1).p("AgnBFQAdhPAyg6");
	this.shape_4.setTransform(-0.525,-0.05);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(11,1,1).p("AgkBGQAehPArg8");
	this.shape_5.setTransform(-0.875,-0.1);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(11,1,1).p("AgfBGQAdhPAig8");
	this.shape_6.setTransform(-1.325,-0.15);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(11,1,1).p("AgaBHQAdhPAYg+");
	this.shape_7.setTransform(-1.85,-0.2);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(11,1,1).p("AgVBHQAdhPAOg+");
	this.shape_8.setTransform(-2.375,-0.275);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(11,1,1).p("AgQBIQAchPAFhA");
	this.shape_9.setTransform(-2.825,-0.325);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(11,1,1).p("AgNBIQAdhPgChA");
	this.shape_10.setTransform(-3.1687,-0.375);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(11,1,1).p("AgLBJQAdhPgHhC");
	this.shape_11.setTransform(-3.3579,-0.425);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(11,1,1).p("AgKBJQAchPgKhC");
	this.shape_12.setTransform(-3.4484,-0.425);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(11,1,1).p("AgJBJQAchPgMhC");
	this.shape_13.setTransform(-3.5015,-0.45);
	this.shape_13._off = true;

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(11,1,1).p("AgKBJQAdhPgLhC");
	this.shape_14.setTransform(-3.4622,-0.425);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(11,1,1).p("AgLBJQAdhPgIhC");
	this.shape_15.setTransform(-3.374,-0.425);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(11,1,1).p("AgMBJQAchPgDhC");
	this.shape_16.setTransform(-3.2314,-0.4);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(11,1,1).p("AgPBIQAdhPAChA");
	this.shape_17.setTransform(-2.95,-0.35);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#000000").ss(11,1,1).p("AgTBIQAdhQAKg/");
	this.shape_18.setTransform(-2.55,-0.3);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#000000").ss(11,1,1).p("AgXBHQAchPAUg+");
	this.shape_19.setTransform(-2.1,-0.25);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#000000").ss(11,1,1).p("AgcBGQAchPAeg8");
	this.shape_20.setTransform(-1.6,-0.175);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#000000").ss(11,1,1).p("AghBGQAehPAlg8");
	this.shape_21.setTransform(-1.15,-0.125);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#000000").ss(11,1,1).p("AglBFQAdhPAug6");
	this.shape_22.setTransform(-0.75,-0.075);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#000000").ss(11,1,1).p("AgoBFQAdhPA0g6");
	this.shape_23.setTransform(-0.45,-0.025);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#000000").ss(11,1,1).p("AgqBFQAdhQA4g5");
	this.shape_24.setTransform(-0.25,0);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f().s("#000000").ss(11,1,1).p("AgrBFQAdhQA7g5");
	this.shape_25.setTransform(-0.1,0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape}]},1).wait(1));
	this.timeline.addTween(cjs.Tween.get(this.shape_13).wait(13).to({_off:false},0).wait(1).to({x:-3.514},0).wait(1).to({x:-3.5015},0).to({_off:true},1).wait(26).to({_off:false},0).wait(1).to({x:-3.514},0).wait(1).to({x:-3.5015},0).to({_off:true},1).wait(14));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_9_Layer_2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(12,1,1).p("Ag7CXQhWiKAyhSQAxhRCgAA");
	this.shape.setTransform(159.6059,55.525);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(12,1,1).p("Ag+CbQhOiGAzhSQAzhRCUgM");
	this.shape_1.setTransform(159.9879,55.05);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(12,1,1).p("AhCCgQhFiEA0hRQA0hRCJgZ");
	this.shape_2.setTransform(160.3657,54.55);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(12,1,1).p("AhGClQg9iCA2hRQA2hQB+gm");
	this.shape_3.setTransform(160.7072,54.075);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(12,1,1).p("AhJCqQg0iAA3hQQA3hQBzgz");
	this.shape_4.setTransform(161.043,53.575);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(12,1,1).p("AhKCTQg6iBBAhKQBAhKBrgQ");
	this.shape_5.setTransform(161.1713,55.925);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(12,1,1).p("AhMB+Qg/iBBJhEQBIhFBkAU");
	this.shape_6.setTransform(161.3158,58.0402);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(12,1,1).p("AhNBxQhEiBBSg/QBRg/BbA4");
	this.shape_7.setTransform(161.4161,59.3018);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(12,1,1).p("AhOBpQhKiCBbg5QBag5BTBc");
	this.shape_8.setTransform(161.5406,60.1347);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(12,1,1).p("AhLBuQhMiCBUg+QBTg9BgBN");
	this.shape_9.setTransform(161.245,59.5969);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(12,1,1).p("AhIB0QhOiEBOhCQBMhBBsA+");
	this.shape_10.setTransform(160.9437,59.005);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(12,1,1).p("AhFB7QhQiFBHhGQBGhFB5Au");
	this.shape_11.setTransform(160.6488,58.33);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(12,1,1).p("AhCCCQhSiGBAhKQA/hJCGAe");
	this.shape_12.setTransform(160.3144,57.5506);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(12,1,1).p("Ag+CMQhUiJA5hNQA4hNCSAP");
	this.shape_13.setTransform(159.9729,56.6451);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},18).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape}]},1).wait(17));
	this.timeline.addTween(cjs.Tween.get(this.shape).wait(18).to({_off:true},1).wait(13).to({_off:false},0).to({_off:true},1).wait(13).to({_off:false},0).to({_off:true},1).wait(13).to({_off:false},0).wait(17));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_9_Layer_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(12,1,1).p("ABWgDQipiMgCDL");
	this.shape.setTransform(164.975,59.0512);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(12,1,1).p("AhVA8QAIi/CjBy");
	this.shape_1.setTransform(165.025,59.0648);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(12,1,1).p("AhWA9QAPi0CeBY");
	this.shape_2.setTransform(165.075,59.0219);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(12,1,1).p("AhWA+QAVipCYA/");
	this.shape_3.setTransform(165.1,58.9116);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(12,1,1).p("ABYg4QiTglgcCd");
	this.shape_4.setTransform(165.15,58.6913);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(12,1,1).p("AhUBNQgDi5CsAn");
	this.shape_5.setTransform(164.8715,57.3877);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(12,1,1).p("AhPBaQgjjVDHAo");
	this.shape_6.setTransform(164.3574,56.0784);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(12,1,1).p("AhHBoQhDjyDhAq");
	this.shape_7.setTransform(163.584,54.7405);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(12,1,1).p("ABbhuQj8grBjEO");
	this.shape_8.setTransform(162.6808,53.4242);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(12,1,1).p("AhDBqQhSkDDuA8");
	this.shape_9.setTransform(163.176,54.5375);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(12,1,1).p("AhHBfQhCj3DgBL");
	this.shape_10.setTransform(163.6366,55.5703);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(12,1,1).p("AhMBWQgwjtDSBc");
	this.shape_11.setTransform(164.0718,56.5406);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(12,1,1).p("AhQBNQgfjiDEBs");
	this.shape_12.setTransform(164.4579,57.4315);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(12,1,1).p("AhTBEQgPjWC3B8");
	this.shape_13.setTransform(164.7683,58.2721);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},18).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape}]},1).wait(17));
	this.timeline.addTween(cjs.Tween.get(this.shape).wait(18).to({_off:true},1).wait(13).to({_off:false},0).to({_off:true},1).wait(13).to({_off:false},0).to({_off:true},1).wait(13).to({_off:false},0).wait(17));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_7_Layer_13 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_13
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(11,1,1).p("AEehAIo7CB");
	this.shape.setTransform(-34.4,-20.675);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(59));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_7_Layer_11 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_11
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(11,1,1).p("AFChOQlCBHlBBW");
	this.shape.setTransform(19.2,-7.9);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(59));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_7_Layer_9 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_9
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(11,1,1).p("AA9A1QgnhShSgX");
	this.shape.setTransform(-214.25,-39.05);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(11,1,1).p("Ag7g0QBQAYAnBR");
	this.shape_1.setTransform(-214.325,-39.225);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(11,1,1).p("Ag6g0QBOAYAnBR");
	this.shape_2.setTransform(-214.55,-39.775);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(11,1,1).p("Ag4g2QBJAaAoBS");
	this.shape_3.setTransform(-214.975,-40.8);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(11,1,1).p("Ag1g3QBDAcAoBT");
	this.shape_4.setTransform(-215.675,-42.425);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(11,1,1).p("Agxg6QA5AfAqBW");
	this.shape_5.setTransform(-216.6,-44.65);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(11,1,1).p("Agrg9QAtAjArBY");
	this.shape_6.setTransform(-217.8,-47.525);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(11,1,1).p("AglhAQAhAnAqBa");
	this.shape_7.setTransform(-219.15,-50.775);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(11,1,1).p("AgghEQAUAsAtBd");
	this.shape_8.setTransform(-220.55,-54.05);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(11,1,1).p("AgahHQAIAwAtBf");
	this.shape_9.setTransform(-221.75,-56.925);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(11,1,1).p("AgWhJQgCAyAvBh");
	this.shape_10.setTransform(-222.679,-59.15);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(11,1,1).p("AgShLQgJA1AvBi");
	this.shape_11.setTransform(-223.4389,-60.775);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(11,1,1).p("AgQhNQgNA3AwBk");
	this.shape_12.setTransform(-223.9363,-61.8);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(11,1,1).p("AgOhNQgPA4AwBj");
	this.shape_13.setTransform(-224.2112,-62.35);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(11,1,1).p("AASBOQgwhjAQg4");
	this.shape_14.setTransform(-224.2966,-62.525);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(11,1,1).p("AgPhNQgOA3AwBk");
	this.shape_15.setTransform(-223.9708,-61.9);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(11,1,1).p("AgRhMQgKA2AvBi");
	this.shape_16.setTransform(-223.5535,-61);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(11,1,1).p("AgVhKQgEA0AvBh");
	this.shape_17.setTransform(-222.9154,-59.65);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#000000").ss(11,1,1).p("AgYhIQAEAxAuBg");
	this.shape_18.setTransform(-222.1,-57.75);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#000000").ss(11,1,1).p("AgdhFQAOAuAtBd");
	this.shape_19.setTransform(-221.05,-55.275);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#000000").ss(11,1,1).p("AgjhCQAbApAsBc");
	this.shape_20.setTransform(-219.825,-52.35);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#000000").ss(11,1,1).p("Agog/QAnAmAqBZ");
	this.shape_21.setTransform(-218.525,-49.225);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#000000").ss(11,1,1).p("Agug8QAzAhAqBX");
	this.shape_22.setTransform(-217.3,-46.3);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#000000").ss(11,1,1).p("Agzg5QA+AeApBV");
	this.shape_23.setTransform(-216.25,-43.825);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#000000").ss(11,1,1).p("Ag2g3QBFAbAoBU");
	this.shape_24.setTransform(-215.45,-41.925);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f().s("#000000").ss(11,1,1).p("Ag5g1QBLAZAoBS");
	this.shape_25.setTransform(-214.875,-40.575);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape_1,p:{y:-39.225}}]},1).to({state:[{t:this.shape_2,p:{x:-214.55,y:-39.775}}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13,p:{y:-62.35}}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_13,p:{y:-62.375}}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_2,p:{x:-214.525,y:-39.675}}]},1).to({state:[{t:this.shape_1,p:{y:-39.2}}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape_1,p:{y:-39.225}}]},1).to({state:[{t:this.shape_2,p:{x:-214.55,y:-39.775}}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13,p:{y:-62.35}}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_13,p:{y:-62.375}}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_2,p:{x:-214.525,y:-39.675}}]},1).to({state:[{t:this.shape_1,p:{y:-39.2}}]},1).to({state:[{t:this.shape}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_7_Layer_8 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_8
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(11,1,1).p("AGFiTQjqAnkqBCQjDArgyCT");
	this.shape.setTransform(-169.125,-49.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(11,1,1).p("AmFCTQAziSDEgqQEphCDrgn");
	this.shape_1.setTransform(-169.175,-49.275);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(11,1,1).p("AmHCRQA2iPDEgqQEqhCDrgm");
	this.shape_2.setTransform(-169.35,-49.525);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(11,1,1).p("AmJCNQA7iLDEgoQEqhBDqgl");
	this.shape_3.setTransform(-169.675,-49.975);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(11,1,1).p("AmOCGQBEiCDEgoQEqg+Drgj");
	this.shape_4.setTransform(-170.2,-50.7);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(11,1,1).p("AmVB9QBQh3DFgmQEqg7Drgh");
	this.shape_5.setTransform(-170.9,-51.675);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(11,1,1).p("AmdBxQBfhoDFgkQEsg3Drge");
	this.shape_6.setTransform(-171.8,-52.95);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(11,1,1).p("AmmBjQBwhYDGggQEsgzDsga");
	this.shape_7.setTransform(-172.8,-54.375);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(11,1,1).p("AmwBWQCChIDGgdQEtgvDsgX");
	this.shape_8.setTransform(-173.85,-55.85);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(11,1,1).p("Am5BKQCRg5DHgbQEtgqDugV");
	this.shape_9.setTransform(-174.75,-57.125);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(11,1,1).p("Am/BAQCdguDHgYQEtgoDugS");
	this.shape_10.setTransform(-175.45,-58.1);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(11,1,1).p("AnEA6QCmgmDHgXQEuglDugR");
	this.shape_11.setTransform(-175.975,-58.825);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(11,1,1).p("AnGA2QCrghDHgWQEvgkDtgQ");
	this.shape_12.setTransform(-176.3,-59.275);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(11,1,1).p("AnIAzQCugeDHgVQEvgkDtgO");
	this.shape_13.setTransform(-176.475,-59.525);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(11,1,1).p("AHKgyQjuAPkuAjQjIAVivAe");
	this.shape_14.setTransform(-176.525,-59.6);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(11,1,1).p("AnHA1QCsggDHgWQEugkDugP");
	this.shape_15.setTransform(-176.325,-59.325);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(11,1,1).p("AnEA5QCnglDHgXQEuglDugQ");
	this.shape_16.setTransform(-176.05,-58.925);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(11,1,1).p("AnAA+QCfgrDHgYQEugnDtgR");
	this.shape_17.setTransform(-175.625,-58.325);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#000000").ss(11,1,1).p("Am7BGQCWg1DGgaQEugpDtgT");
	this.shape_18.setTransform(-175.025,-57.475);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#000000").ss(11,1,1).p("Am0BRQCJhBDGgcQEtgtDtgW");
	this.shape_19.setTransform(-174.25,-56.4);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#000000").ss(11,1,1).p("AmrBdQB4hRDHgeQEsgxDsgZ");
	this.shape_20.setTransform(-173.3,-55.075);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#000000").ss(11,1,1).p("AmiBqQBohgDFghQEsg1Dsgd");
	this.shape_21.setTransform(-172.35,-53.725);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#000000").ss(11,1,1).p("AmaB2QBZhvDFgkQErg4Drgg");
	this.shape_22.setTransform(-171.4,-52.4);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#000000").ss(11,1,1).p("AmSCAQBLh7DFgmQEqg8Drgi");
	this.shape_23.setTransform(-170.625,-51.325);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#000000").ss(11,1,1).p("AmNCIQBCiEDEgoQEqg/Drgk");
	this.shape_24.setTransform(-170.025,-50.475);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f().s("#000000").ss(11,1,1).p("AmICOQA5iMDEgpQEqhADqgm");
	this.shape_25.setTransform(-169.6,-49.875);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f().s("#000000").ss(11,1,1).p("AmGCRQA1iPDEgqQEqhCDqgm");
	this.shape_26.setTransform(-169.325,-49.475);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_7_Layer_6 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_6
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(11,1,1).p("AF3hmIrtDN");
	this.shape.setTransform(-124.8,-44.725);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(11,1,1).p("Al1BoILrjP");
	this.shape_1.setTransform(-124.725,-44.675);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(11,1,1).p("AlzBpILnjR");
	this.shape_2.setTransform(-124.5,-44.525);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(11,1,1).p("AlvBsILfjX");
	this.shape_3.setTransform(-124.05,-44.25);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(11,1,1).p("AloBwILRjf");
	this.shape_4.setTransform(-123.375,-43.85);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(11,1,1).p("AleB2IK9jr");
	this.shape_5.setTransform(-122.425,-43.25);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(11,1,1).p("AlSB+IKlj6");
	this.shape_6.setTransform(-121.2,-42.5);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(11,1,1).p("AlECGIKJkL");
	this.shape_7.setTransform(-119.8,-41.625);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(11,1,1).p("Ak2CPIJtkd");
	this.shape_8.setTransform(-118.4,-40.775);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(11,1,1).p("AkqCWIJVkr");
	this.shape_9.setTransform(-117.175,-40.025);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(11,1,1).p("AkgCcIJBk3");
	this.shape_10.setTransform(-116.225,-39.425);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(11,1,1).p("AkaCgII1k/");
	this.shape_11.setTransform(-115.55,-39.025);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(11,1,1).p("AkVCjIIrlF");
	this.shape_12.setTransform(-115.1,-38.75);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(11,1,1).p("AkTCkIInlI");
	this.shape_13.setTransform(-114.875,-38.6);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(11,1,1).p("AETikIolFJ");
	this.shape_14.setTransform(-114.8,-38.55);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(11,1,1).p("AkZChIIylB");
	this.shape_15.setTransform(-115.45,-38.95);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(11,1,1).p("AkeCdII9k6");
	this.shape_16.setTransform(-116.025,-39.3);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(11,1,1).p("AkmCYIJNkw");
	this.shape_17.setTransform(-116.825,-39.8);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#000000").ss(11,1,1).p("AkxCSIJjkj");
	this.shape_18.setTransform(-117.9,-40.45);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#000000").ss(11,1,1).p("Ak9CKIJ7kT");
	this.shape_19.setTransform(-119.125,-41.225);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#000000").ss(11,1,1).p("AlLCCIKXkD");
	this.shape_20.setTransform(-120.475,-42.05);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#000000").ss(11,1,1).p("AlXB6IKvjz");
	this.shape_21.setTransform(-121.7,-42.825);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#000000").ss(11,1,1).p("AliB0ILFjn");
	this.shape_22.setTransform(-122.775,-43.475);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#000000").ss(11,1,1).p("AlqBvILVjd");
	this.shape_23.setTransform(-123.575,-43.975);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#000000").ss(11,1,1).p("AlvBrILgjV");
	this.shape_24.setTransform(-124.15,-44.325);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2,p:{x:-124.5,y:-44.525}}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12,p:{x:-115.1,y:-38.75}}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_12,p:{x:-115.075,y:-38.725}}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_2,p:{x:-124.525,y:-44.55}}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2,p:{x:-124.5,y:-44.525}}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12,p:{x:-115.1,y:-38.75}}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_12,p:{x:-115.075,y:-38.725}}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_2,p:{x:-124.525,y:-44.55}}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_6_Layer_4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_4
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(11,1,1).p("Ah2hTQAABWAlAsQAgAnAxgCQAvgBAiglQAmgnAAg7");
	this.shape.setTransform(22.1247,45.5278);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(11,1,1).p("Ah2g8QAAA+AlAgQAgAbAxgBQAvgBAigaQAmgcAAgq");
	this.shape_1.setTransform(22.1247,43.6018);
	this.shape_1._off = true;

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(11,1,1).p("Ah2gkQAAAlAlAUQAgAQAxAAQAvgBAigQQAmgRAAgZ");
	this.shape_2.setTransform(22.1247,41.6257);
	this.shape_2._off = true;

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(11,1,1).p("Ah2hIQAABKAlAnQAgAiAxgCQAvgBAiggQAmgiAAgz");
	this.shape_3.setTransform(22.1247,44.4782);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(11,1,1).p("Ah2htQAABwAlA6QAgAzAxgCQAvgCAigwQAmgzAAhN");
	this.shape_4.setTransform(22.1247,47.3288);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(11,1,1).p("Ah2hoQAABrAlA3QAgAwAxgBQAvgCAiguQAmgxAAhJ");
	this.shape_5.setTransform(22.1247,46.9773);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(11,1,1).p("Ah2hjQAABmAlA0QAgAuAxgCQAvgBAigsQAmguAAhG");
	this.shape_6.setTransform(22.1247,46.6042);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(11,1,1).p("Ah2heQAABhAlAyQAgArAxgCQAvgCAigoQAmgtAAhB");
	this.shape_7.setTransform(22.1247,46.2525);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(11,1,1).p("Ah2hYQAABbAlAvQAgApAxgCQAvgBAignQAmgqAAg+");
	this.shape_8.setTransform(22.1247,45.8797);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},12).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},11).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape}]},1).wait(15));
	this.timeline.addTween(cjs.Tween.get(this.shape).wait(12).to({_off:true},1).wait(3).to({_off:false},0).to({_off:true},1).wait(3).to({_off:false},0).to({_off:true},1).wait(3).to({_off:false},0).to({_off:true},1).wait(3).to({_off:false},0).to({_off:true},1).wait(8).to({_off:false},0).wait(11).to({_off:true},1).wait(3).to({_off:false},0).to({_off:true},1).wait(3).to({_off:false},0).to({_off:true},1).wait(8).to({_off:false},0).wait(15));
	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(13).to({_off:false},0).to({_off:true},1).wait(1).to({_off:false},0).to({_off:true},1).wait(1).to({_off:false},0).to({_off:true},1).wait(1).to({_off:false},0).to({_off:true},1).wait(1).to({_off:false},0).to({_off:true},1).wait(1).to({_off:false},0).to({_off:true},1).wait(1).to({_off:false},0).to({_off:true},1).wait(1).to({_off:false},0).to({_off:true},1).wait(1).to({_off:false},0).to({_off:true},1).wait(19).to({_off:false},0).to({_off:true},1).wait(1).to({_off:false},0).to({_off:true},1).wait(1).to({_off:false},0).to({_off:true},1).wait(1).to({_off:false},0).to({_off:true},1).wait(1).to({_off:false},0).to({_off:true},1).wait(22));
	this.timeline.addTween(cjs.Tween.get(this.shape_2).wait(14).to({_off:false},0).to({_off:true},1).wait(3).to({_off:false},0).to({_off:true},1).wait(3).to({_off:false},0).to({_off:true},1).wait(3).to({_off:false},0).to({_off:true},1).wait(3).to({_off:false},0).to({_off:true},1).wait(19).to({_off:false},0).to({_off:true},1).wait(3).to({_off:false},0).to({_off:true},1).wait(3).to({_off:false},0).to({_off:true},1).wait(21));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_6_Layer_3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_3
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(11,1,1).p("Ah3ArQAMg/ArgfQAmgcAuAIQAvAHAcAlQAfApgIA8");
	this.shape.setTransform(22.2071,32.8458);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(11,1,1).p("Ah3AgQAMgvArgXQAmgWAuAGQAvAFAcAdQAfAegIAu");
	this.shape_1.setTransform(22.2071,34.669);
	this.shape_1._off = true;

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(11,1,1).p("Ah3AWQAMggArgQQAmgPAuAEQAvAEAcAUQAfAUgIAg");
	this.shape_2.setTransform(22.2071,36.4829);
	this.shape_2._off = true;

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(11,1,1).p("Ah3AnQAMg5ArgcQAmgaAuAHQAvAGAcAiQAfAlgIA3");
	this.shape_3.setTransform(22.2071,33.6508);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(11,1,1).p("Ah3A3QAMhRArgpQAmgkAuAKQAvAJAcAwQAfA1gIBP");
	this.shape_4.setTransform(22.2071,30.8175);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(11,1,1).p("Ah3A1QAMhOArgnQAmgiAuAJQAvAJAcAuQAfAzgIBL");
	this.shape_5.setTransform(22.2071,31.2224);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(11,1,1).p("Ah3AyQAMhKArgkQAmghAuAJQAvAIAcAsQAfAwgIBH");
	this.shape_6.setTransform(22.2071,31.6286);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(11,1,1).p("Ah3AwQAMhGArgjQAmggAuAJQAvAIAcAqQAfAtgIBE");
	this.shape_7.setTransform(22.2071,32.0347);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(11,1,1).p("Ah3AtQAMhCArghQAmgeAuAIQAvAIAcAnQAfArgIBB");
	this.shape_8.setTransform(22.2071,32.4408);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},12).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},11).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape}]},1).wait(15));
	this.timeline.addTween(cjs.Tween.get(this.shape).wait(12).to({_off:true},1).wait(3).to({_off:false},0).to({_off:true},1).wait(3).to({_off:false},0).to({_off:true},1).wait(3).to({_off:false},0).to({_off:true},1).wait(3).to({_off:false},0).to({_off:true},1).wait(8).to({_off:false},0).wait(11).to({_off:true},1).wait(3).to({_off:false},0).to({_off:true},1).wait(3).to({_off:false},0).to({_off:true},1).wait(8).to({_off:false},0).wait(15));
	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(13).to({_off:false},0).to({_off:true},1).wait(1).to({_off:false},0).to({_off:true},1).wait(1).to({_off:false},0).to({_off:true},1).wait(1).to({_off:false},0).to({_off:true},1).wait(1).to({_off:false},0).to({_off:true},1).wait(1).to({_off:false},0).to({_off:true},1).wait(1).to({_off:false},0).to({_off:true},1).wait(1).to({_off:false},0).to({_off:true},1).wait(1).to({_off:false},0).to({_off:true},1).wait(19).to({_off:false},0).to({_off:true},1).wait(1).to({_off:false},0).to({_off:true},1).wait(1).to({_off:false},0).to({_off:true},1).wait(1).to({_off:false},0).to({_off:true},1).wait(1).to({_off:false},0).to({_off:true},1).wait(22));
	this.timeline.addTween(cjs.Tween.get(this.shape_2).wait(14).to({_off:false},0).to({_off:true},1).wait(3).to({_off:false},0).to({_off:true},1).wait(3).to({_off:false},0).to({_off:true},1).wait(3).to({_off:false},0).to({_off:true},1).wait(3).to({_off:false},0).to({_off:true},1).wait(19).to({_off:false},0).to({_off:true},1).wait(3).to({_off:false},0).to({_off:true},1).wait(3).to({_off:false},0).to({_off:true},1).wait(21));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_6_Layer_2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(11,1,1).p("AiJAEQA/hIAvgJQAtgJAvAYQAvAYAPAjQAPAjgHAx");
	this.shape.setTransform(59.7006,4.4388);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(11,1,1).p("AiJADQA/gyAvgGQAtgGAvARQAvAQAPAZQAPAXgHAi");
	this.shape_1.setTransform(59.7006,4.745);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(11,1,1).p("AiJACQA/giAvgEQAtgEAvALQAvAMAPAQQAPAQgHAX");
	this.shape_2.setTransform(59.7006,4.9717);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(11,1,1).p("AiJACQA/gYAvgDQAtgDAvAIQAvAIAPAMQAPALgHAQ");
	this.shape_3.setTransform(59.7006,5.0859);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(11,1,1).p("AiJABQA/gUAvgDQAtgCAvAHQAvAHAPAKQAPAJgHAP");
	this.shape_4.setTransform(59.7006,5.1421);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(11,1,1).p("AiJADQA/gsAvgFQAtgFAvAOQAvAOAPAWQAPAUgHAe");
	this.shape_5.setTransform(59.7006,4.8359);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(11,1,1).p("AiJADQA/g7AvgIQAtgHAvAUQAvAUAPAdQAPAcgHAo");
	this.shape_6.setTransform(59.7006,4.6093);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(11,1,1).p("AiJAEQA/hFAvgJQAtgIAvAXQAvAWAPAiQAPAhgHAv");
	this.shape_7.setTransform(59.7006,4.4951);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},12).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_4}]},17).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape}]},1).wait(43));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_6_Layer_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(11,1,1).p("AhwBEQgLguAdgmQAbgjAtgMQAsgMAmATQAqAVAOAr");
	this.shape.setTransform(0.0256,0.0139);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(11,1,1).p("AhwAvQgLggAdgZQAbgYAtgJQAsgIAmANQAqAOAOAe");
	this.shape_1.setTransform(0.0256,1.6939);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(11,1,1).p("AhwAgQgLgVAdgSQAbgQAtgGQAsgFAmAJQAqAJAOAU");
	this.shape_2.setTransform(0.0256,2.8957);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(11,1,1).p("AhwAXQgLgQAdgMQAbgLAtgEQAsgEAmAGQAqAHAOAO");
	this.shape_3.setTransform(0.0256,3.62);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(11,1,1).p("AhwAUQgLgOAdgKQAbgKAtgEQAsgDAmAFQAqAGAOAM");
	this.shape_4.setTransform(0.0256,3.8569);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(11,1,1).p("AhwApQgLgcAdgWQAbgVAtgHQAsgHAmALQAqAMAOAa");
	this.shape_5.setTransform(0.0256,2.1889);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(11,1,1).p("AhwA4QgLgmAdgfQAbgcAtgKQAsgKAmAPQAqARAOAk");
	this.shape_6.setTransform(0.0256,0.9789);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(11,1,1).p("AhwBBQgLgsAdgkQAbgiAtgLQAsgLAmARQAqAUAOAq");
	this.shape_7.setTransform(0.0256,0.2627);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},12).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_4}]},17).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape}]},1).wait(43));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_4_Layer_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(11,1,1).p("ADvprQA8H9hsE8Qh7FolHA2");
	this.shape.setTransform(0.0318,0);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(11,1,1).p("AkEJpQFIgzB7lnQBtk7g8n8");
	this.shape_1.setTransform(-0.0714,-0.225);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(11,1,1).p("AkHJjQFMgtB8lkQBuk4g8n8");
	this.shape_2.setTransform(-0.3662,-0.875);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(11,1,1).p("AkMJYQFSgjB+ldQBxk0g8n7");
	this.shape_3.setTransform(-0.9065,-1.975);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(11,1,1).p("AkUJHQFcgSCBlVQB0kug9n4");
	this.shape_4.setTransform(-1.6881,-3.625);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(11,1,1).p("AkfIxQFpADCFlJQB5klg9n2");
	this.shape_5.setTransform(-2.7416,-5.8736);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(11,1,1).p("AksITQF5AeCKk4QB/kbg+nz");
	this.shape_6.setTransform(-4.0858,-8.5686);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(11,1,1).p("Ak7HvQGMA9CPknQCHkPg/nu");
	this.shape_7.setTransform(-5.6227,-11.4024);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(11,1,1).p("AlKHJQGeBbCVkVQCOkDhAnq");
	this.shape_8.setTransform(-7.1582,-14.014);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(11,1,1).p("AlYGkQGvB4CakFQCVj5hBnn");
	this.shape_9.setTransform(-8.5379,-16.1852);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(11,1,1).p("AljGFQG9CPCdj4QCbjwhCnk");
	this.shape_10.setTransform(-9.6749,-17.86);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(11,1,1).p("AlrFsQHHCgCgjtQCfjphCni");
	this.shape_11.setTransform(-10.5298,-19.0888);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(11,1,1).p("AlxFaQHPCtCijmQCijlhDnf");
	this.shape_12.setTransform(-11.1552,-19.9163);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(11,1,1).p("Al1FOQHTC2CkjiQCkjihDne");
	this.shape_13.setTransform(-11.5534,-20.433);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(11,1,1).p("Al3FIQHWC6CljgQCkjfhDne");
	this.shape_14.setTransform(-11.7555,-20.7184);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(11,1,1).p("AFmmaQBDHdilDfQilDfnXi7");
	this.shape_15.setTransform(-11.8287,-20.8061);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(11,1,1).p("Al3FIQHWC6CljgQCkjghDne");
	this.shape_16.setTransform(-11.7555,-20.6934);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(11,1,1).p("Al1FRQHTCzCkjiQCjjihDnf");
	this.shape_17.setTransform(-11.4762,-20.332);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#000000").ss(11,1,1).p("AlvFgQHMCpCijpQChjmhDng");
	this.shape_18.setTransform(-10.9531,-19.6476);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#000000").ss(11,1,1).p("AlnF4QHCCYCfjyQCdjthCni");
	this.shape_19.setTransform(-10.1233,-18.5062);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#000000").ss(11,1,1).p("AlcGZQG0CACbkAQCXj2hBnl");
	this.shape_20.setTransform(-8.947,-16.8056);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#000000").ss(11,1,1).p("AlNHBQGiBiCVkSQCQkChAnp");
	this.shape_21.setTransform(-7.438,-14.4841);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#000000").ss(11,1,1).p("Ak8HsQGNA/CQklQCHkNg/nv");
	this.shape_22.setTransform(-5.7753,-11.6595);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#000000").ss(11,1,1).p("AktIRQF6AgCKk4QCAkag+ny");
	this.shape_23.setTransform(-4.1884,-8.7304);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#000000").ss(11,1,1).p("AkgIvQFqAFCFlHQB6klg9n2");
	this.shape_24.setTransform(-2.8444,-6.0712);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f().s("#000000").ss(11,1,1).p("AkVJFQFdgQCClTQB0ktg8n5");
	this.shape_25.setTransform(-1.816,-3.9);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f().s("#000000").ss(11,1,1).p("AkOJVQFUggB/lcQBxkyg8n7");
	this.shape_26.setTransform(-1.0595,-2.3);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f().s("#000000").ss(11,1,1).p("AkJJgQFOgqB9liQBvk3g8n8");
	this.shape_27.setTransform(-0.5474,-1.2);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f().s("#000000").ss(11,1,1).p("AkGJnQFKgxB8lmQBtk5g8n9");
	this.shape_28.setTransform(-0.1995,-0.5);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f().s("#000000").ss(11,1,1).p("AkEJqQFIg0B7loQBsk6g7n9");
	this.shape_29.setTransform(-0.0182,-0.125);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},14).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_15}]},30).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_27}]},1).to({state:[{t:this.shape_28}]},1).to({state:[{t:this.shape_29}]},1).to({state:[{t:this.shape}]},1).wait(6));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_3_Layer_3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_3
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(10,1,1).p("AgTAqIAnhT");
	this.shape.setTransform(-5.35,-9.975);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(10,1,1).p("AgaAnQAZgtAcgg");
	this.shape_1.setTransform(-4.625,-9.675);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(10,1,1).p("AghAkQAegvAlgY");
	this.shape_2.setTransform(-3.975,-9.425);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(10,1,1).p("AgnAiQAigyAtgR");
	this.shape_3.setTransform(-3.375,-9.2);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(10,1,1).p("AgsAgQAmg2AzgJ");
	this.shape_4.setTransform(-2.875,-9);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(10,1,1).p("AgwAeQAog3A5gE");
	this.shape_5.setTransform(-2.45,-8.825);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(10,1,1).p("AgzAdQAqg5A9AA");
	this.shape_6.setTransform(-2.125,-8.7002);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(10,1,1).p("Ag2AcQAsg7BBAE");
	this.shape_7.setTransform(-1.85,-8.5876);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(10,1,1).p("Ag4AbQAug7BDAG");
	this.shape_8.setTransform(-1.65,-8.5315);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(10,1,1).p("Ag5AbQAug8BFAI");
	this.shape_9.setTransform(-1.55,-8.521);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(10,1,1).p("Ag6AbQAwg9BFAJ");
	this.shape_10.setTransform(-1.5,-8.5041);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(10,1,1).p("AgyAeQAqg5A7gB");
	this.shape_11.setTransform(-2.225,-8.75);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(10,1,1).p("AgmAiQAhgyAsgR");
	this.shape_12.setTransform(-3.475,-9.225);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(10,1,1).p("AgdAmQAbgtAgge");
	this.shape_13.setTransform(-4.375,-9.6);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(10,1,1).p("AgZAnQAYgrAbgi");
	this.shape_14.setTransform(-4.725,-9.725);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(10,1,1).p("AgWApQAWgrAYgm");
	this.shape_15.setTransform(-5,-9.85);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(10,1,1).p("AgVApQAVgpAVgo");
	this.shape_16.setTransform(-5.2,-9.925);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(10,1,1).p("AgUAqQAUgqAVgo");
	this.shape_17.setTransform(-5.3,-9.95);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},24).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_10}]},30).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape}]},1).wait(6));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_3_Layer_2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(10,1,1).p("AhcAJQCihmAXCB");
	this.shape.setTransform(5.75,-7.7413);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(10,1,1).p("AhYABQCrhdAGCC");
	this.shape_1.setTransform(5.425,-6.9136);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(10,1,1).p("AhVgFQC0hVgJCC");
	this.shape_2.setTransform(5.1441,-6.2133);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(10,1,1).p("AhUgLQC8hPgWCD");
	this.shape_3.setTransform(4.9647,-5.5887);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(10,1,1).p("AhTgRQDDhIghCE");
	this.shape_4.setTransform(4.8695,-5.0425);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(10,1,1).p("AhSgVQDJhCgrCD");
	this.shape_5.setTransform(4.8273,-4.6111);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(10,1,1).p("AhSgYQDOg+gzCD");
	this.shape_6.setTransform(4.78,-4.2632);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(10,1,1).p("AhSgbQDRg7g5CE");
	this.shape_7.setTransform(4.7584,-3.9685);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(10,1,1).p("AhSgdQDUg4g9CE");
	this.shape_8.setTransform(4.7639,-3.7796);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(10,1,1).p("AhSgeQDWg3hACE");
	this.shape_9.setTransform(4.7604,-3.6671);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(10,1,1).p("AhSgfQDXg2hCCE");
	this.shape_10.setTransform(4.7582,-3.6321);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(10,1,1).p("AhSgXQDMhAgwCE");
	this.shape_11.setTransform(4.789,-4.3786);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(10,1,1).p("AhUgKQC7hPgUCC");
	this.shape_12.setTransform(4.9962,-5.6574);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(10,1,1).p("AhXAAQCuhbABCC");
	this.shape_13.setTransform(5.325,-6.6651);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(10,1,1).p("AhZACQCqheAJCC");
	this.shape_14.setTransform(5.475,-7.0383);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(10,1,1).p("AhaAGQCmhiAPCB");
	this.shape_15.setTransform(5.6,-7.3603);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(10,1,1).p("AhbAIQCjhlAUCC");
	this.shape_16.setTransform(5.675,-7.5703);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(10,1,1).p("AhbAJQChhmAWCB");
	this.shape_17.setTransform(5.725,-7.6967);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},24).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_10}]},30).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape}]},1).wait(6));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_3_Layer_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(10,1,1).p("Agdg/QBmBShJAt");
	this.shape.setTransform(0.0091,0.025);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(10,1,1).p("Agig8QBzBBhMA4");
	this.shape_1.setTransform(0.5042,-0.275);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(10,1,1).p("Agng6QB/AzhOBC");
	this.shape_2.setTransform(0.9553,-0.55);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(10,1,1).p("Agrg3QCJAlhQBK");
	this.shape_3.setTransform(1.3563,-0.8);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(10,1,1).p("Agug1QCRAahRBR");
	this.shape_4.setTransform(1.717,-1.025);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(10,1,1).p("AgxgzQCZAQhSBX");
	this.shape_5.setTransform(2.0246,-1.2);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(10,1,1).p("Ag0gyQCfAIhTBd");
	this.shape_6.setTransform(2.2675,-1.35);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(10,1,1).p("Ag2gxQCkABhUBi");
	this.shape_7.setTransform(2.4557,-1.45);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(10,1,1).p("Ag3gwQCngDhUBk");
	this.shape_8.setTransform(2.5889,-1.5309);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(10,1,1).p("Ag4gvQCpgGhUBl");
	this.shape_9.setTransform(2.6778,-1.5916);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(10,1,1).p("Ag4gvQCqgHhVBm");
	this.shape_10.setTransform(2.7026,-1.622);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(10,1,1).p("AgzgyQCdAKhTBb");
	this.shape_11.setTransform(2.1793,-1.3);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(10,1,1).p("Agqg3QCHAnhPBI");
	this.shape_12.setTransform(1.313,-0.775);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(10,1,1).p("Agkg7QB4A8hNA7");
	this.shape_13.setTransform(0.676,-0.375);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(10,1,1).p("Aghg9QBxBEhMA3");
	this.shape_14.setTransform(0.42,-0.225);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(10,1,1).p("Agfg+QBsBLhLAy");
	this.shape_15.setTransform(0.2406,-0.125);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(10,1,1).p("Ageg/QBpBQhLAv");
	this.shape_16.setTransform(0.1125,-0.05);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(10,1,1).p("Agdg/QBnBShKAt");
	this.shape_17.setTransform(0.0297,0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},24).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_10}]},30).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape}]},1).wait(6));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_2_Layer_1copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(11,1,1).p("AAZnaQjaDMgzC/QgrCiBSCJQBGB1COBJQCCBCB7gB");
	this.shape.setTransform(0.0211,0.0267);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(11,1,1).p("AAbnZQjbDLgzC/QgsChBSCJQBFB2COBIQCCBDB8gC");
	this.shape_1.setTransform(-0.1069,-0.0233);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(11,1,1).p("AAenYQjbDKg1C+QgtChBQCJQBFB2CNBIQCDBCB9gC");
	this.shape_2.setTransform(-0.4703,-0.1971);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(11,1,1).p("AAlnVQjeDHg3C8QgvChBOCJQBCB2CPBIQCBBCCBgD");
	this.shape_3.setTransform(-1.1871,-0.4954);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(11,1,1).p("AAwnQQjhDBg7C7QgyCfBKCJQA/B4COBGQCBBCCHgD");
	this.shape_4.setTransform(-2.2621,-0.9412);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(11,1,1).p("ABAnKQjmC6hAC5Qg3CdBECJQA7B5CNBGQCCBBCPgF");
	this.shape_5.setTransform(-3.8123,-1.5854);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(11,1,1).p("ABTnCQjsCxhGC2Qg8CbA8CIQA1B8CMBEQCCBACagG");
	this.shape_6.setTransform(-5.7938,-2.3952);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(11,1,1).p("ABqm4QjzCnhNCyQhDCYA1CIQAuB+CLBCQCCBAClgI");
	this.shape_7.setTransform(-8.0831,-3.3052);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(11,1,1).p("ACCmvQj6CdhVCvQhJCVAtCIQAnB/CKBBQCDA/CwgK");
	this.shape_8.setTransform(-10.4331,-4.2123);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(11,1,1).p("ACXmnQkACThcCtQhOCSAlCIQAiCCCIA/QCEA+C7gL");
	this.shape_9.setTransform(-12.5452,-5.0166);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(11,1,1).p("ACpmhQkFCNhhCqQhTCQAfCHQAeCECHA+QCEA9DDgM");
	this.shape_10.setTransform(-14.3083,-5.6694);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(11,1,1).p("AC2mcQkJCHhlCoQhWCPAbCHQAZCFCHA9QCEA9DKgO");
	this.shape_11.setTransform(-15.6226,-6.1769);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(11,1,1).p("ADAmYQkMCDhoCnQhZCNAYCHQAXCGCGA9QCFA8DOgO");
	this.shape_12.setTransform(-16.581,-6.5176);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(11,1,1).p("ADGmWQkNCAhqCmQhbCNAWCHQAWCGCGA9QCEA8DRgP");
	this.shape_13.setTransform(-17.2151,-6.7339);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(11,1,1).p("ADKmVQkOB/hsCmQhbCMAVCHQAVCHCFA8QCFA8DSgP");
	this.shape_14.setTransform(-17.5635,-6.8589);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(11,1,1).p("ADLmVQkPB/hrClQhcCNAVCHQAUCGCGA8QCEA9DTgQ");
	this.shape_15.setTransform(-17.6631,-6.8998);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(11,1,1).p("ACnmiQkECNhhCrQhTCQAhCIQAeCDCHA+QCEA+DCgN");
	this.shape_16.setTransform(-14.1092,-5.5944);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(11,1,1).p("ACImtQj8CahXCvQhKCUAqCIQAnCACIBAQCEA/CzgK");
	this.shape_17.setTransform(-10.9752,-4.431);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#000000").ss(11,1,1).p("ABtm4Qj0CmhOCzQhDCXAzCIQAuB+CLBCQCCBACmgJ");
	this.shape_18.setTransform(-8.3191,-3.3998);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#000000").ss(11,1,1).p("ABWnBQjsCwhIC2Qg9CaA8CIQA0B8CMBEQCCBACbgG");
	this.shape_19.setTransform(-6.0626,-2.495);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#000000").ss(11,1,1).p("ABDnIQjnC4hBC5Qg3CcBCCJQA6B6CNBFQCBBBCRgF");
	this.shape_20.setTransform(-4.1624,-1.7321);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#000000").ss(11,1,1).p("AA0nPQjjDAg8C6QgzCfBICJQA+B4COBGQCBBCCKgE");
	this.shape_21.setTransform(-2.6323,-1.0911);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#000000").ss(11,1,1).p("AAonUQjeDFg5C9QgvCgBMCIQBCB3COBIQCBBCCDgD");
	this.shape_22.setTransform(-1.4598,-0.5954);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#000000").ss(11,1,1).p("AAgnXQjcDJg2C9QgtChBQCJQBEB2CNBIQCCBDB/gC");
	this.shape_23.setTransform(-0.6498,-0.2471);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#000000").ss(11,1,1).p("AAbnZQjbDLgzC+QgsCiBRCJQBGB2CNBIQCDBCB7gB");
	this.shape_24.setTransform(-0.1463,-0.0483);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},19).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_15}]},20).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape}]},1).wait(16));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_1_Layer_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(12,1,1).p("AkwnAQhTDiAXC1QAXC0BoCEQBpCECcAkQCpAmCxhl");
	this.shape.setTransform(643.7312,394.5944);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(12,1,1).p("Ak9mzQhODkAdC1QAdCyBsB+QBtB+CcAaQCoAdCrhu");
	this.shape_1.setTransform(645.2702,394.2432);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(12,1,1).p("AlKmnQhIDlAjC2QAiCxBxB4QBwB3CcARQCoATCjh3");
	this.shape_2.setTransform(646.7666,393.9245);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(12,1,1).p("AlWmbQhDDmApC2QAnCwB1ByQB1BwCbAIQCoAJCdiA");
	this.shape_3.setTransform(648.2313,393.7046);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(12,1,1).p("AlimQQg+DoAwC2QAsCvB5BrQB4BqCcgBQCngBCWiJ");
	this.shape_4.setTransform(649.665,393.5505);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(12,1,1).p("AlumGQg4DqA1C2QAxCvB+BkQB9BjCagKQCngKCQiS");
	this.shape_5.setTransform(651.068,393.4457);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(12,1,1).p("Al6l8QgyDrA7C3QA3CtCCBeQCABdCbgUQCmgUCJib");
	this.shape_6.setTransform(652.4158,393.444);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(12,1,1).p("AmFl0QgtDtBCC3QA7CsCHBYQCEBWCagcQCmgeCCik");
	this.shape_7.setTransform(653.7837,393.4782);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(12,1,1).p("AmQlrQgnDuBHC4QBBCqCLBRQCIBQCaglQCmgoB7it");
	this.shape_8.setTransform(655.0722,393.5984);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(12,1,1).p("Al7l7QgyDrA9C3QA3CtCCBdQCBBcCbgUQCmgWCIic");
	this.shape_9.setTransform(652.5938,393.436);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(12,1,1).p("AlomLQg7DpAyC2QAvCvB8BoQB6BmCbgFQCngGCTiO");
	this.shape_10.setTransform(650.3605,393.489);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(12,1,1).p("AlXmaQhDDnArC2QAnCwB2BxQB0BvCcAHQCoAICbiB");
	this.shape_11.setTransform(648.3964,393.6706);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(12,1,1).p("Ak+mxQhODjAfC2QAdCyBtB9QBsB9CdAZQCoAbCphv");
	this.shape_12.setTransform(645.4443,394.1891);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(12,1,1).p("Ak3m5QhQDjAaC1QAaCzBqCBQBqCBCdAfQCpAhCthq");
	this.shape_13.setTransform(644.5219,394.3931);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(12,1,1).p("Akxm+QhTDiAYC1QAYC0BpCDQBoCDCdAjQCpAlCwhn");
	this.shape_14.setTransform(643.9308,394.5407);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},11).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_8}]},43).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape}]},1).wait(10));

}).prototype = p = new cjs.MovieClip();


(lib.Scene_1_Layer_13 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_13
	this.instance = new lib.pack1_1();
	this.instance.parent = this;
	this.instance.setTransform(802,191);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Scene_1_Layer_13, null, null);


(lib.Scene_1_Layer_12 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_12
	this.instance = new lib.pack2_1();
	this.instance.parent = this;
	this.instance.setTransform(687,193);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Scene_1_Layer_12, null, null);


(lib.Scene_1_Layer_2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.instance = new lib.bg();
	this.instance.parent = this;
	this.instance.setTransform(-6,-6);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Scene_1_Layer_2, null, null);


(lib.Symbol2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#F51431").s().p("AgIAwQgRgIgLgUQgLgUADgTQAEgUAQgIIACgCQAPgGAPAIQATAIALAUQALATgFAUQgCATgOAHIgCACQgIAEgJAAQgJAAgIgEg");
	this.shape.setTransform(3.032,2.8606);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol2, new cjs.Rectangle(-1.5,-2.3,9.1,10.399999999999999), null);


(lib.Symbol13 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_58 = function() {
		this.___loopingOver___ = true;
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(58).call(this.frame_58).wait(1));

	// Layer_1_obj_
	this.Layer_1 = new lib.Symbol_13_Layer_1();
	this.Layer_1.name = "Layer_1";
	this.Layer_1.parent = this;
	this.Layer_1.depth = 0;
	this.Layer_1.isAttachedToCamera = 0
	this.Layer_1.isAttachedToMask = 0
	this.Layer_1.layerDepth = 0
	this.Layer_1.layerIndex = 0
	this.Layer_1.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_1).wait(59));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-9,-14.7,18.1,25.799999999999997);


(lib.Symbol12 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_58 = function() {
		this.___loopingOver___ = true;
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(58).call(this.frame_58).wait(1));

	// Layer_1_obj_
	this.Layer_1 = new lib.Symbol_12_Layer_1();
	this.Layer_1.name = "Layer_1";
	this.Layer_1.parent = this;
	this.Layer_1.setTransform(-0.3,-0.1,1,1,0,0,0,-0.3,-0.1);
	this.Layer_1.depth = 0;
	this.Layer_1.isAttachedToCamera = 0
	this.Layer_1.isAttachedToMask = 0
	this.Layer_1.layerDepth = 0
	this.Layer_1.layerIndex = 0
	this.Layer_1.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_1).wait(59));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-8.1,-13.3,16.2,26.6);


(lib.Symbol10 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_58 = function() {
		this.___loopingOver___ = true;
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(58).call(this.frame_58).wait(1));

	// Layer_1_obj_
	this.Layer_1 = new lib.Symbol_10_Layer_1();
	this.Layer_1.name = "Layer_1";
	this.Layer_1.parent = this;
	this.Layer_1.depth = 0;
	this.Layer_1.isAttachedToCamera = 0
	this.Layer_1.isAttachedToMask = 0
	this.Layer_1.layerDepth = 0
	this.Layer_1.layerIndex = 0
	this.Layer_1.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_1).wait(59));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-10,-13.2,20.1,25.6);


(lib.Symbol9 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_76 = function() {
		this.___loopingOver___ = true;
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(76).call(this.frame_76).wait(1));

	// Layer_1_obj_
	this.Layer_1 = new lib.Symbol_9_Layer_1();
	this.Layer_1.name = "Layer_1";
	this.Layer_1.parent = this;
	this.Layer_1.setTransform(165,59.1,1,1,0,0,0,165,59.1);
	this.Layer_1.depth = 0;
	this.Layer_1.isAttachedToCamera = 0
	this.Layer_1.isAttachedToMask = 0
	this.Layer_1.layerDepth = 0
	this.Layer_1.layerIndex = 0
	this.Layer_1.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_1).wait(77));

	// Layer_2_obj_
	this.Layer_2 = new lib.Symbol_9_Layer_2();
	this.Layer_2.name = "Layer_2";
	this.Layer_2.parent = this;
	this.Layer_2.setTransform(159.6,55.5,1,1,0,0,0,159.6,55.5);
	this.Layer_2.depth = 0;
	this.Layer_2.isAttachedToCamera = 0
	this.Layer_2.isAttachedToMask = 0
	this.Layer_2.layerDepth = 0
	this.Layer_2.layerIndex = 1
	this.Layer_2.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_2).wait(77));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(142.2,30.6,37.70000000000002,45.99999999999999);


(lib.Symbol6 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_79 = function() {
		this.___loopingOver___ = true;
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(79).call(this.frame_79).wait(1));

	// Layer_3_obj_
	this.Layer_3 = new lib.Symbol_6_Layer_3();
	this.Layer_3.name = "Layer_3";
	this.Layer_3.parent = this;
	this.Layer_3.setTransform(22.2,32.9,1,1,0,0,0,22.2,32.9);
	this.Layer_3.depth = 0;
	this.Layer_3.isAttachedToCamera = 0
	this.Layer_3.isAttachedToMask = 0
	this.Layer_3.layerDepth = 0
	this.Layer_3.layerIndex = 0
	this.Layer_3.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_3).wait(80));

	// Layer_4_obj_
	this.Layer_4 = new lib.Symbol_6_Layer_4();
	this.Layer_4.name = "Layer_4";
	this.Layer_4.parent = this;
	this.Layer_4.setTransform(22.1,45.5,1,1,0,0,0,22.1,45.5);
	this.Layer_4.depth = 0;
	this.Layer_4.isAttachedToCamera = 0
	this.Layer_4.isAttachedToMask = 0
	this.Layer_4.layerDepth = 0
	this.Layer_4.layerIndex = 1
	this.Layer_4.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_4).wait(80));

	// Layer_2_obj_
	this.Layer_2 = new lib.Symbol_6_Layer_2();
	this.Layer_2.name = "Layer_2";
	this.Layer_2.parent = this;
	this.Layer_2.setTransform(59.7,4.5,1,1,0,0,0,59.7,4.5);
	this.Layer_2.depth = 0;
	this.Layer_2.isAttachedToCamera = 0
	this.Layer_2.isAttachedToMask = 0
	this.Layer_2.layerDepth = 0
	this.Layer_2.layerIndex = 2
	this.Layer_2.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_2).wait(80));

	// Layer_1_obj_
	this.Layer_1 = new lib.Symbol_6_Layer_1();
	this.Layer_1.name = "Layer_1";
	this.Layer_1.parent = this;
	this.Layer_1.depth = 0;
	this.Layer_1.isAttachedToCamera = 0
	this.Layer_1.isAttachedToMask = 0
	this.Layer_1.layerDepth = 0
	this.Layer_1.layerIndex = 3
	this.Layer_1.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_1).wait(80));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-17.1,-12.3,96.1,76.1);


(lib.Symbol3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_79 = function() {
		this.___loopingOver___ = true;
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(79).call(this.frame_79).wait(1));

	// Layer_3_obj_
	this.Layer_3 = new lib.Symbol_3_Layer_3();
	this.Layer_3.name = "Layer_3";
	this.Layer_3.parent = this;
	this.Layer_3.setTransform(-5.4,-10,1,1,0,0,0,-5.4,-10);
	this.Layer_3.depth = 0;
	this.Layer_3.isAttachedToCamera = 0
	this.Layer_3.isAttachedToMask = 0
	this.Layer_3.layerDepth = 0
	this.Layer_3.layerIndex = 0
	this.Layer_3.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_3).wait(80));

	// Layer_2_obj_
	this.Layer_2 = new lib.Symbol_3_Layer_2();
	this.Layer_2.name = "Layer_2";
	this.Layer_2.parent = this;
	this.Layer_2.setTransform(5.8,-7.8,1,1,0,0,0,5.8,-7.8);
	this.Layer_2.depth = 0;
	this.Layer_2.isAttachedToCamera = 0
	this.Layer_2.isAttachedToMask = 0
	this.Layer_2.layerDepth = 0
	this.Layer_2.layerIndex = 1
	this.Layer_2.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_2).wait(80));

	// Layer_1_obj_
	this.Layer_1 = new lib.Symbol_3_Layer_1();
	this.Layer_1.name = "Layer_1";
	this.Layer_1.parent = this;
	this.Layer_1.depth = 0;
	this.Layer_1.isAttachedToCamera = 0
	this.Layer_1.isAttachedToMask = 0
	this.Layer_1.layerDepth = 0
	this.Layer_1.layerIndex = 2
	this.Layer_1.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_1).wait(80));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-12.3,-19.1,32.3,30.6);


(lib.Symbol1copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(14,1,1).p("Ai8AsQBEhrA9gVQA8gWBFAXQBFAWAbAtQAcAsgGBE");
	this.shape.setTransform(200.0791,-99.744);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(40));

	// Layer_2
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(14,1,1).p("AidBNQADhCAqgqQAognA5gGQA4gGAvAfQA0AhASBB");
	this.shape_1.setTransform(119.325,-105.6493);

	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(40));

	// Layer_3
	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(13,1,1).p("AkLhYQAcCfCnAOQCnAOBJgrQBKgtAahG");
	this.shape_2.setTransform(156.025,-53.2786);

	this.timeline.addTween(cjs.Tween.get(this.shape_2).wait(40));

	// Layer_5
	this.instance = new lib.Symbol2();
	this.instance.parent = this;
	this.instance.setTransform(137.85,-48.75,2.0168,2.0168,0,46.0839,51.8739,5.4,7.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1).to({regX:3,regY:2.9,scaleX:2.0159,scaleY:2.0177,skewX:45.7195,skewY:51.4594,x:141.7,y:-58.8},0).wait(1).to({scaleX:2.0136,scaleY:2.0199,skewX:44.7878,skewY:50.3997,x:142.1,y:-58.45},0).wait(1).to({scaleX:2.0096,scaleY:2.0237,skewX:43.1697,skewY:48.559,x:142.95,y:-57.75},0).wait(1).to({scaleX:2.0035,scaleY:2.0294,skewX:40.7228,skewY:45.7757,x:144.2,y:-56.85},0).wait(1).to({scaleX:1.995,scaleY:2.0375,skewX:37.2999,skewY:41.8822,x:146.1,y:-55.75},0).wait(1).to({scaleX:1.9839,scaleY:2.0481,skewX:32.8139,skewY:36.7794,x:148.95,y:-54.7},0).wait(1).to({scaleX:1.9705,scaleY:2.0609,skewX:27.3853,skewY:30.6045,x:152.45,y:-54},0).wait(1).to({scaleX:1.9559,scaleY:2.0748,skewX:21.5115,skewY:23.9231,x:156.25,y:-53.8},0).wait(1).to({scaleX:1.9422,scaleY:2.0879,skewX:15.9743,skewY:17.6245,x:159.5,y:-54.15},0).wait(1).to({scaleX:1.9309,scaleY:2.0987,skewX:11.4074,skewY:12.4298,x:162.1,y:-54.7},0).wait(1).to({scaleX:1.9225,scaleY:2.1066,skewX:8.0351,skewY:8.5938,x:164,y:-55.3},0).wait(1).to({scaleX:1.917,scaleY:2.1119,skewX:5.788,skewY:6.0378,x:165.25,y:-55.8},0).wait(1).to({scaleX:1.9137,scaleY:2.115,skewX:4.4942,skewY:4.5662,x:165.9,y:-56.15},0).wait(1).to({regX:5.4,regY:7.5,scaleX:1.9125,scaleY:2.1162,rotation:3.9793,skewX:0,skewY:0,x:170.2,y:-46.35},0).wait(7).to({regX:3,regY:2.9,scaleX:1.914,scaleY:2.1148,rotation:0,skewX:4.5733,skewY:4.6561,x:165.95,y:-56.1},0).wait(1).to({scaleX:1.9174,scaleY:2.1115,skewX:5.9626,skewY:6.2364,x:165.1,y:-55.75},0).wait(1).to({scaleX:1.923,scaleY:2.1061,skewX:8.2536,skewY:8.8424,x:163.75,y:-55.15},0).wait(1).to({scaleX:1.9312,scaleY:2.0984,skewX:11.5185,skewY:12.5562,x:161.7,y:-54.55},0).wait(1).to({scaleX:1.9416,scaleY:2.0884,skewX:15.7359,skewY:17.3534,x:158.9,y:-53.95},0).wait(1).to({scaleX:1.9539,scaleY:2.0767,skewX:20.7133,skewY:23.0151,x:155.6,y:-53.7},0).wait(1).to({scaleX:1.9672,scaleY:2.0641,skewX:26.0541,skewY:29.0903,x:152.3,y:-53.95},0).wait(1).to({scaleX:1.98,scaleY:2.0518,skewX:31.248,skewY:34.9982,x:149.5,y:-54.45},0).wait(1).to({scaleX:1.9914,scaleY:2.0409,skewX:35.8517,skewY:40.2349,x:147,y:-55.3},0).wait(1).to({scaleX:2.0007,scaleY:2.0321,skewX:39.611,skewY:44.5111,x:145,y:-56.35},0).wait(1).to({scaleX:2.0078,scaleY:2.0254,skewX:42.4537,skewY:47.7446,x:143.45,y:-57.3},0).wait(1).to({scaleX:2.0127,scaleY:2.0207,skewX:44.4197,skewY:49.9809,x:142.45,y:-58.15},0).wait(1).to({scaleX:2.0156,scaleY:2.0179,skewX:45.5968,skewY:51.3198,x:141.85,y:-58.7},0).wait(1).to({regX:5.4,regY:7.5,scaleX:2.0168,scaleY:2.0168,skewX:46.0839,skewY:51.8739,x:137.8,y:-48.7},0).wait(6));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(96.5,-120.4,129.5,82.5);


(lib.Symbol_7_Symbol_11 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Symbol_11
	this.instance = new lib.Symbol11();
	this.instance.parent = this;
	this.instance.setTransform(-63.05,-14.15,1,1,-66.1901,0,0,23.4,-5.8);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1).to({regX:0,regY:0,rotation:-65.7308,x:-67.3895,y:9.5655},0).wait(1).to({rotation:-64.2252,x:-68.0086,y:9.4414},0).wait(1).to({rotation:-61.4503,x:-69.1404,y:9.1701},0).wait(1).to({rotation:-57.1532,x:-70.8637,y:8.6428},0).wait(1).to({rotation:-51.1117,x:-73.21,y:7.6859},0).wait(1).to({rotation:-43.3041,x:-76.0709,y:6.0928},0).wait(1).to({rotation:-34.1962,x:-79.0992,y:3.7635},0).wait(1).to({rotation:-24.8656,x:-81.7804,y:0.9103},0).wait(1).to({rotation:-16.5408,x:-83.7552,y:-1.9737},0).wait(1).to({rotation:-9.9491,x:-85.0106,y:-4.4423},0).wait(1).to({rotation:-5.2179,x:-85.733,y:-6.2951},0).wait(1).to({rotation:-2.1591,x:-86.1179,y:-7.5222},0).wait(1).to({rotation:-0.5034,x:-86.2988,y:-8.1946},0).wait(1).to({regX:23.4,regY:-5.8,rotation:0,x:-63,y:-14.2},0).wait(1).to({regX:0,regY:0,rotation:-0.4168,x:-86.3578,y:-8.2295},0).wait(1).to({rotation:-1.7767,x:-86.2114,y:-7.6755},0).wait(1).to({rotation:-4.2689,x:-85.9093,y:-6.6698},0).wait(1).to({rotation:-8.106,x:-85.3596,y:-5.1498},0).wait(1).to({rotation:-13.4799,x:-84.422,y:-3.0903},0).wait(1).to({rotation:-20.4438,x:-82.9283,y:-0.5684},0).wait(1).to({rotation:-28.7054,x:-80.7772,y:2.1612},0).wait(1).to({rotation:-37.4847,x:-78.0885,y:4.6902},0).wait(1).to({rotation:-45.7463,x:-75.2345,y:6.6687},0).wait(1).to({rotation:-52.7102,x:-72.6294,y:8.0029},0).wait(1).to({rotation:-58.0841,x:-70.5202,y:8.8104},0).wait(1).to({rotation:-61.9212,x:-68.9728,y:9.2642},0).wait(1).to({rotation:-64.4134,x:-67.953,y:9.5029},0).wait(1).to({rotation:-65.7733,x:-67.3925,y:9.6144},0).wait(1).to({regX:23.4,regY:-5.8,rotation:-66.1901,x:-63.05,y:-14.15},0).wait(1).to({regX:0,regY:0,rotation:-65.7308,x:-67.3895,y:9.5655},0).wait(1).to({rotation:-64.2252,x:-68.0086,y:9.4414},0).wait(1).to({rotation:-61.4503,x:-69.1404,y:9.1701},0).wait(1).to({rotation:-57.1532,x:-70.8637,y:8.6428},0).wait(1).to({rotation:-51.1117,x:-73.21,y:7.6859},0).wait(1).to({rotation:-43.3041,x:-76.0709,y:6.0928},0).wait(1).to({rotation:-34.1962,x:-79.0992,y:3.7635},0).wait(1).to({rotation:-24.8656,x:-81.7804,y:0.9103},0).wait(1).to({rotation:-16.5408,x:-83.7552,y:-1.9737},0).wait(1).to({rotation:-9.9491,x:-85.0106,y:-4.4423},0).wait(1).to({rotation:-5.2179,x:-85.733,y:-6.2951},0).wait(1).to({rotation:-2.1591,x:-86.1179,y:-7.5222},0).wait(1).to({rotation:-0.5034,x:-86.2988,y:-8.1946},0).wait(1).to({regX:23.4,regY:-5.8,rotation:0,x:-63,y:-14.2},0).wait(1).to({regX:0,regY:0,rotation:-0.4168,x:-86.3578,y:-8.2295},0).wait(1).to({rotation:-1.7767,x:-86.2114,y:-7.6755},0).wait(1).to({rotation:-4.2689,x:-85.9093,y:-6.6698},0).wait(1).to({rotation:-8.106,x:-85.3596,y:-5.1498},0).wait(1).to({rotation:-13.4799,x:-84.422,y:-3.0903},0).wait(1).to({rotation:-20.4438,x:-82.9283,y:-0.5684},0).wait(1).to({rotation:-28.7054,x:-80.7772,y:2.1612},0).wait(1).to({rotation:-37.4847,x:-78.0885,y:4.6902},0).wait(1).to({rotation:-45.7463,x:-75.2345,y:6.6687},0).wait(1).to({rotation:-52.7102,x:-72.6294,y:8.0029},0).wait(1).to({rotation:-58.0841,x:-70.5202,y:8.8104},0).wait(1).to({rotation:-61.9212,x:-68.9728,y:9.2642},0).wait(1).to({rotation:-64.4134,x:-67.953,y:9.5029},0).wait(1).to({rotation:-65.7733,x:-67.3925,y:9.6144},0).wait(1).to({regX:23.4,regY:-5.8,rotation:-66.1901,x:-63.05,y:-14.15},0).wait(1));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_7_Symbol_8 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Symbol_8
	this.instance = new lib.Symbol8();
	this.instance.parent = this;
	this.instance.setTransform(-12.95,0,1,1,0,0,0,19.2,-7.9);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1).to({regX:0,regY:0,rotation:-0.4463,x:-32.0878,y:8.049},0).wait(1).to({rotation:-1.91,x:-31.8758,y:8.5342},0).wait(1).to({rotation:-4.606,x:-31.4531,y:9.413},0).wait(1).to({rotation:-8.7664,x:-30.721,y:10.7276},0).wait(1).to({rotation:-14.563,x:-29.5462,y:12.4635},0).wait(1).to({rotation:-21.9215,x:-27.8128,y:14.4814},0).wait(1).to({rotation:-30.2726,x:-25.5517,y:16.481},0).wait(1).to({rotation:-38.5694,x:-23.0415,y:18.1216},0).wait(1).to({rotation:-45.8011,x:-20.6801,y:19.2433},0).wait(1).to({rotation:-51.4616,x:-18.7445,y:19.9085},0).wait(1).to({rotation:-55.5138,x:-17.3233,y:20.2658},0).wait(1).to({rotation:-58.1386,x:-16.3906,y:20.4432},0).wait(1).to({rotation:-59.5645,x:-15.8809,y:20.5217},0).wait(1).to({regX:19.1,regY:-7.9,rotation:-59.9996,x:-13,y:0},0).wait(1).to({regX:0,regY:0,rotation:-59.6385,x:-15.8291,y:20.4823},0).wait(1).to({rotation:-58.4621,x:-16.249,y:20.4195},0).wait(1).to({rotation:-56.3091,x:-17.0138,y:20.2824},0).wait(1).to({rotation:-52.9944,x:-18.1798,y:20.0155},0).wait(1).to({rotation:-48.3402,x:-19.7868,y:19.5279},0).wait(1).to({rotation:-42.2632,x:-21.816,y:18.6981},0).wait(1).to({rotation:-34.9463,x:-24.1258,y:17.4213},0).wait(1).to({rotation:-27.0033,x:-26.4268,y:15.7149},0).wait(1).to({rotation:-19.3612,x:-28.398,y:13.7882},0).wait(1).to({rotation:-12.8127,x:-29.8706,y:11.9409},0).wait(1).to({rotation:-7.7142,x:-30.8656,y:10.3935},0).wait(1).to({rotation:-4.0617,x:-31.4919,y:9.2336},0).wait(1).to({rotation:-1.6889,x:-31.8586,y:8.4597},0).wait(1).to({rotation:-0.3957,x:-32.0449,y:8.0318},0).wait(1).to({regX:19.2,regY:-7.9,rotation:0,x:-12.95,y:0},0).wait(1).to({regX:0,regY:0,rotation:-0.4463,x:-32.0878,y:8.049},0).wait(1).to({rotation:-1.91,x:-31.8758,y:8.5342},0).wait(1).to({rotation:-4.606,x:-31.4531,y:9.413},0).wait(1).to({rotation:-8.7664,x:-30.721,y:10.7276},0).wait(1).to({rotation:-14.563,x:-29.5462,y:12.4635},0).wait(1).to({rotation:-21.9215,x:-27.8128,y:14.4814},0).wait(1).to({rotation:-30.2726,x:-25.5517,y:16.481},0).wait(1).to({rotation:-38.5694,x:-23.0415,y:18.1216},0).wait(1).to({rotation:-45.8011,x:-20.6801,y:19.2433},0).wait(1).to({rotation:-51.4616,x:-18.7445,y:19.9085},0).wait(1).to({rotation:-55.5138,x:-17.3233,y:20.2658},0).wait(1).to({rotation:-58.1386,x:-16.3906,y:20.4432},0).wait(1).to({rotation:-59.5645,x:-15.8809,y:20.5217},0).wait(1).to({regX:19.1,regY:-7.9,rotation:-59.9996,x:-13,y:0},0).wait(1).to({regX:0,regY:0,rotation:-59.6385,x:-15.8291,y:20.4823},0).wait(1).to({rotation:-58.4621,x:-16.249,y:20.4195},0).wait(1).to({rotation:-56.3091,x:-17.0138,y:20.2824},0).wait(1).to({rotation:-52.9944,x:-18.1798,y:20.0155},0).wait(1).to({rotation:-48.3402,x:-19.7868,y:19.5279},0).wait(1).to({rotation:-42.2632,x:-21.816,y:18.6981},0).wait(1).to({rotation:-34.9463,x:-24.1258,y:17.4213},0).wait(1).to({rotation:-27.0033,x:-26.4268,y:15.7149},0).wait(1).to({rotation:-19.3612,x:-28.398,y:13.7882},0).wait(1).to({rotation:-12.8127,x:-29.8706,y:11.9409},0).wait(1).to({rotation:-7.7142,x:-30.8656,y:10.3935},0).wait(1).to({rotation:-4.0617,x:-31.4919,y:9.2336},0).wait(1).to({rotation:-1.6889,x:-31.8586,y:8.4597},0).wait(1).to({rotation:-0.3957,x:-32.0449,y:8.0318},0).wait(1).to({regX:19.2,regY:-7.9,rotation:0,x:-12.95,y:0},0).wait(1));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_7_Layer_7 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_7
	this.instance = new lib.Symbol13();
	this.instance.parent = this;
	this.instance.setTransform(-162.3,-34.35,1,1,0,0,0,-3.6,5.6);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1).to({regX:0,regY:-1.8,rotation:-0.5446,x:-158.6,y:-41.65},0).wait(1).to({rotation:-2.3326,x:-158.35,y:-41.5},0).wait(1).to({rotation:-5.6322,x:-157.95,y:-41.1},0).wait(1).to({rotation:-10.7397,x:-157.25,y:-40.45},0).wait(1).to({rotation:-17.8915,x:-156.35,y:-39.5},0).wait(1).to({rotation:-27.0358,x:-155.2,y:-38.1},0).wait(1).to({rotation:-37.4999,x:-153.9,y:-36.2},0).wait(1).to({rotation:-47.964,x:-152.55,y:-34.05},0).wait(1).to({rotation:-57.1083,x:-151.25,y:-31.95},0).wait(1).to({rotation:-64.2601,x:-150.15,y:-30.2},0).wait(1).to({rotation:-69.3676,x:-149.35,y:-28.9},0).wait(1).to({rotation:-72.6671,x:-148.8,y:-28},0).wait(1).to({rotation:-74.4552,x:-148.5,y:-27.5},0).wait(1).to({regX:-3.5,regY:5.5,rotation:-74.9998,x:-142.25,y:-22.05},0).wait(1).to({regX:0,regY:-1.8,rotation:-74.5275,x:-148.45,y:-27.45},0).wait(1).to({rotation:-72.9865,x:-148.7,y:-27.85},0).wait(1).to({rotation:-70.1627,x:-149.2,y:-28.6},0).wait(1).to({rotation:-65.8149,x:-149.9,y:-29.75},0).wait(1).to({rotation:-59.7258,x:-150.85,y:-31.25},0).wait(1).to({rotation:-51.835,x:-151.95,y:-33.1},0).wait(1).to({rotation:-42.4737,x:-153.25,y:-35.15},0).wait(1).to({rotation:-32.526,x:-154.5,y:-37.05},0).wait(1).to({rotation:-23.1648,x:-155.7,y:-38.65},0).wait(1).to({rotation:-15.274,x:-156.7,y:-39.85},0).wait(1).to({rotation:-9.1849,x:-157.5,y:-40.65},0).wait(1).to({rotation:-4.8371,x:-158.05,y:-41.15},0).wait(1).to({rotation:-2.0132,x:-158.45,y:-41.45},0).wait(1).to({rotation:-0.4723,x:-158.65,y:-41.65},0).wait(1).to({regX:-3.6,regY:5.6,rotation:0,x:-162.3,y:-34.35},0).wait(1).to({regX:0,regY:-1.8,rotation:-0.5446,x:-158.6,y:-41.65},0).wait(1).to({rotation:-2.3326,x:-158.35,y:-41.5},0).wait(1).to({rotation:-5.6322,x:-157.95,y:-41.1},0).wait(1).to({rotation:-10.7397,x:-157.25,y:-40.45},0).wait(1).to({rotation:-17.8915,x:-156.35,y:-39.5},0).wait(1).to({rotation:-27.0358,x:-155.2,y:-38.1},0).wait(1).to({rotation:-37.4999,x:-153.9,y:-36.2},0).wait(1).to({rotation:-47.964,x:-152.55,y:-34.05},0).wait(1).to({rotation:-57.1083,x:-151.25,y:-31.95},0).wait(1).to({rotation:-64.2601,x:-150.15,y:-30.2},0).wait(1).to({rotation:-69.3676,x:-149.35,y:-28.9},0).wait(1).to({rotation:-72.6671,x:-148.8,y:-28},0).wait(1).to({rotation:-74.4552,x:-148.5,y:-27.5},0).wait(1).to({regX:-3.5,regY:5.5,rotation:-74.9998,x:-142.25,y:-22.05},0).wait(1).to({regX:0,regY:-1.8,rotation:-74.5275,x:-148.45,y:-27.45},0).wait(1).to({rotation:-72.9865,x:-148.7,y:-27.85},0).wait(1).to({rotation:-70.1627,x:-149.2,y:-28.6},0).wait(1).to({rotation:-65.8149,x:-149.9,y:-29.75},0).wait(1).to({rotation:-59.7258,x:-150.85,y:-31.25},0).wait(1).to({rotation:-51.835,x:-151.95,y:-33.1},0).wait(1).to({rotation:-42.4737,x:-153.25,y:-35.15},0).wait(1).to({rotation:-32.526,x:-154.5,y:-37.05},0).wait(1).to({rotation:-23.1648,x:-155.7,y:-38.65},0).wait(1).to({rotation:-15.274,x:-156.7,y:-39.85},0).wait(1).to({rotation:-9.1849,x:-157.5,y:-40.65},0).wait(1).to({rotation:-4.8371,x:-158.05,y:-41.15},0).wait(1).to({rotation:-2.0132,x:-158.45,y:-41.45},0).wait(1).to({rotation:-0.4723,x:-158.65,y:-41.65},0).wait(1).to({regX:-3.6,regY:5.6,rotation:0,x:-162.3,y:-34.35},0).wait(1));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_7_Layer_3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_3
	this.instance = new lib.Symbol12();
	this.instance.parent = this;
	this.instance.setTransform(-71.3,32.85,1,1,-90,0,0,2.6,7.8);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1).to({regX:0,regY:0,rotation:-89.3754,x:-79.5344,y:35.2703},0).wait(1).to({rotation:-87.3282,x:-80.8307,y:34.6961},0).wait(1).to({rotation:-83.5551,x:-83.0557,y:33.6209},0).wait(1).to({rotation:-77.7123,x:-86.3553,y:31.8489},0).wait(1).to({rotation:-69.4976,x:-90.7763,y:29.1048},0).wait(1).to({rotation:-58.8814,x:-96.0895,y:25.0821},0).wait(1).to({rotation:-46.4973,x:-101.6685,y:19.5864},0).wait(1).to({rotation:-33.8103,x:-106.5024,y:12.8609},0).wait(1).to({rotation:-22.4908,x:-109.7835,y:5.7427},0).wait(1).to({rotation:-13.528,x:-111.4309,y:-0.599},0).wait(1).to({rotation:-7.0949,x:-111.991,y:-5.3609},0).wait(1).to({rotation:-2.9358,x:-112.0699,y:-8.4291},0).wait(1).to({rotation:-0.6845,x:-112.0252,y:-10.0607},0).wait(1).to({regX:2.6,regY:7.8,rotation:0,x:-109.4,y:-2.75},0).wait(1).to({regX:0,regY:0,rotation:-0.5668,x:-112.0293,y:-10.1991},0).wait(1).to({rotation:-2.4159,x:-112.113,y:-9.0843},0).wait(1).to({rotation:-5.8046,x:-112.2057,y:-7.0646},0).wait(1).to({rotation:-11.0219,x:-112.1693,y:-3.9286},0).wait(1).to({rotation:-18.3289,x:-111.6982,y:0.5451},0).wait(1).to({rotation:-27.7978,x:-110.25,y:6.4013},0).wait(1).to({rotation:-39.0314,x:-107.1357,y:13.2417},0).wait(1).to({rotation:-50.9686,x:-102.1225,y:20.0201},0).wait(1).to({rotation:-62.2022,x:-96.0857,y:25.5961},0).wait(1).to({rotation:-71.6711,x:-90.4169,y:29.5589},0).wait(1).to({rotation:-78.9781,x:-85.9086,y:32.1642},0).wait(1).to({rotation:-84.1954,x:-82.6788,y:33.8085},0).wait(1).to({rotation:-87.5841,x:-80.586,y:34.7897},0).wait(1).to({rotation:-89.4332,x:-79.4477,y:35.298},0).wait(1).to({regX:2.6,regY:7.8,rotation:-90,x:-71.3,y:32.85},0).wait(1).to({regX:0,regY:0,rotation:-89.3754,x:-79.5344,y:35.2703},0).wait(1).to({rotation:-87.3282,x:-80.8307,y:34.6961},0).wait(1).to({rotation:-83.5551,x:-83.0557,y:33.6209},0).wait(1).to({rotation:-77.7123,x:-86.3553,y:31.8489},0).wait(1).to({rotation:-69.4976,x:-90.7763,y:29.1048},0).wait(1).to({rotation:-58.8814,x:-96.0895,y:25.0821},0).wait(1).to({rotation:-46.4973,x:-101.6685,y:19.5864},0).wait(1).to({rotation:-33.8103,x:-106.5024,y:12.8609},0).wait(1).to({rotation:-22.4908,x:-109.7835,y:5.7427},0).wait(1).to({rotation:-13.528,x:-111.4309,y:-0.599},0).wait(1).to({rotation:-7.0949,x:-111.991,y:-5.3609},0).wait(1).to({rotation:-2.9358,x:-112.0699,y:-8.4291},0).wait(1).to({rotation:-0.6845,x:-112.0252,y:-10.0607},0).wait(1).to({regX:2.6,regY:7.8,rotation:0,x:-109.4,y:-2.75},0).wait(1).to({regX:0,regY:0,rotation:-0.5668,x:-112.0293,y:-10.1991},0).wait(1).to({rotation:-2.4159,x:-112.113,y:-9.0843},0).wait(1).to({rotation:-5.8046,x:-112.2057,y:-7.0646},0).wait(1).to({rotation:-11.0219,x:-112.1693,y:-3.9286},0).wait(1).to({rotation:-18.3289,x:-111.6982,y:0.5451},0).wait(1).to({rotation:-27.7978,x:-110.25,y:6.4013},0).wait(1).to({rotation:-39.0314,x:-107.1357,y:13.2417},0).wait(1).to({rotation:-50.9686,x:-102.1225,y:20.0201},0).wait(1).to({rotation:-62.2022,x:-96.0857,y:25.5961},0).wait(1).to({rotation:-71.6711,x:-90.4169,y:29.5589},0).wait(1).to({rotation:-78.9781,x:-85.9086,y:32.1642},0).wait(1).to({rotation:-84.1954,x:-82.6788,y:33.8085},0).wait(1).to({rotation:-87.5841,x:-80.586,y:34.7897},0).wait(1).to({rotation:-89.4332,x:-79.4477,y:35.298},0).wait(1).to({regX:2.6,regY:7.8,rotation:-90,x:-71.3,y:32.85},0).wait(1));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_7_Layer_2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.instance = new lib.Symbol10();
	this.instance.parent = this;
	this.instance.setTransform(-51.4,15.2,1,1,0,0,0,-4.5,6.9);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1).to({regX:0,regY:-0.5,rotation:-0.8926,x:-46.9,y:8},0).wait(1).to({rotation:-3.82,x:-46.95,y:8.75},0).wait(1).to({rotation:-9.2122,x:-47,y:10.05},0).wait(1).to({rotation:-17.533,x:-46.95,y:12.2},0).wait(1).to({rotation:-29.1262,x:-46.65,y:15.4},0).wait(1).to({rotation:-43.8434,x:-45.7,y:19.75},0).wait(1).to({rotation:-60.5458,x:-43.55,y:24.8},0).wait(1).to({rotation:-77.1396,x:-40.2,y:29.85},0).wait(1).to({rotation:-91.603,x:-36.4,y:33.95},0).wait(1).to({rotation:-102.9241,x:-33,y:36.95},0).wait(1).to({rotation:-111.0286,x:-30.35,y:39},0).wait(1).to({rotation:-116.2783,x:-28.6,y:40.15},0).wait(1).to({rotation:-119.1301,x:-27.65,y:40.85},0).wait(1).to({regX:-4.6,regY:6.9,rotation:-120.0004,x:-18.6,y:41.3},0).wait(1).to({regX:0,regY:-0.5,rotation:-119.2781,x:-27.6,y:40.8},0).wait(1).to({rotation:-116.9253,x:-28.4,y:40.3},0).wait(1).to({rotation:-112.6191,x:-29.75,y:39.3},0).wait(1).to({rotation:-105.9898,x:-31.75,y:37.8},0).wait(1).to({rotation:-96.6813,x:-34.25,y:35.55},0).wait(1).to({rotation:-84.5272,x:-37.2,y:32.45},0).wait(1).to({rotation:-69.8932,x:-40.15,y:28.65},0).wait(1).to({rotation:-54.0071,x:-42.85,y:24.15},0).wait(1).to({rotation:-38.7227,x:-44.75,y:19.65},0).wait(1).to({rotation:-25.6256,x:-45.95,y:15.6},0).wait(1).to({rotation:-15.4285,x:-46.55,y:12.4},0).wait(1).to({rotation:-8.1235,x:-46.75,y:10.15},0).wait(1).to({rotation:-3.3778,x:-46.85,y:8.75},0).wait(1).to({rotation:-0.7914,x:-46.8,y:8},0).wait(1).to({regX:-4.5,regY:6.9,rotation:0,x:-51.4,y:15.2},0).wait(1).to({regX:0,regY:-0.5,rotation:-0.8926,x:-46.9,y:8},0).wait(1).to({rotation:-3.82,x:-46.95,y:8.75},0).wait(1).to({rotation:-9.2122,x:-47,y:10.05},0).wait(1).to({rotation:-17.533,x:-46.95,y:12.2},0).wait(1).to({rotation:-29.1262,x:-46.65,y:15.4},0).wait(1).to({rotation:-43.8434,x:-45.7,y:19.75},0).wait(1).to({rotation:-60.5458,x:-43.55,y:24.8},0).wait(1).to({rotation:-77.1396,x:-40.2,y:29.85},0).wait(1).to({rotation:-91.603,x:-36.4,y:33.95},0).wait(1).to({rotation:-102.9241,x:-33,y:36.95},0).wait(1).to({rotation:-111.0286,x:-30.35,y:39},0).wait(1).to({rotation:-116.2783,x:-28.6,y:40.15},0).wait(1).to({rotation:-119.1301,x:-27.65,y:40.85},0).wait(1).to({regX:-4.6,regY:6.9,rotation:-120.0004,x:-18.6,y:41.3},0).wait(1).to({regX:0,regY:-0.5,rotation:-119.2781,x:-27.6,y:40.8},0).wait(1).to({rotation:-116.9253,x:-28.4,y:40.3},0).wait(1).to({rotation:-112.6191,x:-29.75,y:39.3},0).wait(1).to({rotation:-105.9898,x:-31.75,y:37.8},0).wait(1).to({rotation:-96.6813,x:-34.25,y:35.55},0).wait(1).to({rotation:-84.5272,x:-37.2,y:32.45},0).wait(1).to({rotation:-69.8932,x:-40.15,y:28.65},0).wait(1).to({rotation:-54.0071,x:-42.85,y:24.15},0).wait(1).to({rotation:-38.7227,x:-44.75,y:19.65},0).wait(1).to({rotation:-25.6256,x:-45.95,y:15.6},0).wait(1).to({rotation:-15.4285,x:-46.55,y:12.4},0).wait(1).to({rotation:-8.1235,x:-46.75,y:10.15},0).wait(1).to({rotation:-3.3778,x:-46.85,y:8.75},0).wait(1).to({rotation:-0.7914,x:-46.8,y:8},0).wait(1).to({regX:-4.5,regY:6.9,rotation:0,x:-51.4,y:15.2},0).wait(1));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_4_Layer_2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.instance = new lib.Symbol5();
	this.instance.parent = this;
	this.instance.setTransform(-21.8,61.95,1,1,0,0,0,11.2,-6.8);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(15).to({regX:0,regY:0,rotation:0.2842,x:-33.2702,y:68.2208},0).wait(1).to({rotation:1.0375,x:-33.9851,y:66.8176},0).wait(1).to({rotation:2.3528,x:-35.2285,y:64.3649},0).wait(1).to({rotation:4.3266,x:-37.0831,y:60.678},0).wait(1).to({rotation:7.0252,x:-39.5965,y:55.626},0).wait(1).to({rotation:10.4115,x:-42.7129,y:49.27},0).wait(1).to({rotation:14.2547,x:-46.1982,y:42.037},0).wait(1).to({rotation:18.1371,x:-49.6621,y:34.7128},0).wait(1).to({rotation:21.6361,x:-52.734,y:28.1003},0).wait(1).to({rotation:24.5073,x:-55.219,y:22.6681},0).wait(1).to({rotation:26.6969,x:-57.0923,y:18.5227},0).wait(1).to({rotation:28.2566,x:-58.4151,y:15.569},0).wait(1).to({rotation:29.2718,x:-59.2709,y:13.6461},0).wait(1).to({rotation:29.828,x:-59.738,y:12.5926},0).wait(1).to({regX:11.2,regY:-6.7,rotation:29.9992,x:-46.85,y:12},0).wait(31).to({regX:0,regY:0,rotation:29.7878,x:-59.7229,y:12.6004},0).wait(1).to({rotation:29.0827,x:-59.1312,y:13.9363},0).wait(1).to({rotation:27.7552,x:-58.0119,y:16.451},0).wait(1).to({rotation:25.6567,x:-56.2282,y:20.4254},0).wait(1).to({rotation:22.6712,x:-53.6606,y:26.0769},0).wait(1).to({rotation:18.8604,x:-50.3322,y:33.2833},0).wait(1).to({rotation:14.6327,x:-46.5737,y:41.2636},0).wait(1).to({rotation:10.6168,x:-42.9404,y:48.8257},0).wait(1).to({rotation:7.2543,x:-39.8519,y:55.1406},0).wait(1).to({rotation:4.658,x:-37.4389,y:60.0045},0).wait(1).to({rotation:2.7589,x:-35.6587,y:63.5546},0).wait(1).to({rotation:1.4415,x:-34.4163,y:66.0135},0).wait(1).to({rotation:0.5978,x:-33.6174,y:67.5864},0).wait(1).to({rotation:0.1401,x:-33.1831,y:68.4391},0).wait(1).to({regX:11.2,regY:-6.8,rotation:0,x:-21.8,y:61.95},0).wait(6));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_2_Layer_2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.instance = new lib.Symbol3();
	this.instance.parent = this;
	this.instance.setTransform(26,47.45,1,1,0,0,0,-7,-6.2);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(20).to({regX:3.8,regY:-3.8,rotation:0.3156,x:36.65,y:49.75},0).wait(1).to({rotation:1.3521,x:36.25,y:49.65},0).wait(1).to({rotation:3.2631,x:35.4,y:49.4},0).wait(1).to({rotation:6.2133,x:34.2,y:49},0).wait(1).to({rotation:10.3224,x:32.45,y:48.5},0).wait(1).to({rotation:15.5356,x:30.05,y:47.8},0).wait(1).to({rotation:21.461,x:27.35,y:46.95},0).wait(1).to({rotation:27.3978,x:24.45,y:46.05},0).wait(1).to({rotation:32.6762,x:21.85,y:45.1},0).wait(1).to({rotation:36.9509,x:19.7,y:44.35},0).wait(1).to({rotation:40.1785,x:18,y:43.75},0).wait(1).to({rotation:42.4621,x:16.8,y:43.3},0).wait(1).to({rotation:43.9422,x:16.1,y:43},0).wait(1).to({rotation:44.7509,x:15.65,y:42.85},0).wait(1).to({regX:-7,regY:-6.2,rotation:44.9994,x:9.6,y:33.45},0).wait(20).to({rotation:0,x:26,y:47.45},10,cjs.Ease.get(1)).wait(16));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_1_Layer_2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.instance = new lib.Symbol9();
	this.instance.parent = this;
	this.instance.setTransform(675.25,432.8,1,1,29.9992,0,0,153.8,70.6);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(11).to({regX:153.7,rotation:0,x:693.6,y:410.55},8).wait(43).to({regX:153.8,rotation:29.9992,x:675.25,y:432.8},8,cjs.Ease.get(1)).wait(10));

}).prototype = p = new cjs.MovieClip();


(lib.Scene_1_Layer_9 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_9
	this.instance = new lib.Symbol6();
	this.instance.parent = this;
	this.instance.setTransform(879.5,313.7);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Scene_1_Layer_9, null, null);


(lib.Scene_1_Layer_5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_5
	this.instance = new lib.Symbol1copy();
	this.instance.parent = this;
	this.instance.setTransform(732.9,413.75,0.687,0.687,0,0,0,80.3,54.3);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Scene_1_Layer_5, null, null);


(lib.Symbol7 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_58 = function() {
		this.___loopingOver___ = true;
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(58).call(this.frame_58).wait(1));

	// Layer_9_obj_
	this.Layer_9 = new lib.Symbol_7_Layer_9();
	this.Layer_9.name = "Layer_9";
	this.Layer_9.parent = this;
	this.Layer_9.setTransform(-214.3,-39.1,1,1,0,0,0,-214.3,-39.1);
	this.Layer_9.depth = 0;
	this.Layer_9.isAttachedToCamera = 0
	this.Layer_9.isAttachedToMask = 0
	this.Layer_9.layerDepth = 0
	this.Layer_9.layerIndex = 0
	this.Layer_9.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_9).wait(59));

	// Layer_8_obj_
	this.Layer_8 = new lib.Symbol_7_Layer_8();
	this.Layer_8.name = "Layer_8";
	this.Layer_8.parent = this;
	this.Layer_8.setTransform(-169.2,-49.2,1,1,0,0,0,-169.2,-49.2);
	this.Layer_8.depth = 0;
	this.Layer_8.isAttachedToCamera = 0
	this.Layer_8.isAttachedToMask = 0
	this.Layer_8.layerDepth = 0
	this.Layer_8.layerIndex = 1
	this.Layer_8.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_8).wait(59));

	// Layer_7_obj_
	this.Layer_7 = new lib.Symbol_7_Layer_7();
	this.Layer_7.name = "Layer_7";
	this.Layer_7.parent = this;
	this.Layer_7.setTransform(-158.7,-40,1,1,0,0,0,-158.7,-40);
	this.Layer_7.depth = 0;
	this.Layer_7.isAttachedToCamera = 0
	this.Layer_7.isAttachedToMask = 0
	this.Layer_7.layerDepth = 0
	this.Layer_7.layerIndex = 2
	this.Layer_7.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_7).wait(1).to({regX:-151.4,regY:-35.1,x:-151.4,y:-35.1},0).wait(13).to({regX:-158.7,regY:-40,x:-158.7,y:-40},0).wait(1).to({regX:-151.4,regY:-35.1,x:-151.4,y:-35.1},0).wait(14).to({regX:-158.7,regY:-40,x:-158.7,y:-40},0).wait(1).to({regX:-151.4,regY:-35.1,x:-151.4,y:-35.1},0).wait(13).to({regX:-158.7,regY:-40,x:-158.7,y:-40},0).wait(1).to({regX:-151.4,regY:-35.1,x:-151.4,y:-35.1},0).wait(14).to({regX:-158.7,regY:-40,x:-158.7,y:-40},0).wait(1));

	// Layer_6_obj_
	this.Layer_6 = new lib.Symbol_7_Layer_6();
	this.Layer_6.name = "Layer_6";
	this.Layer_6.parent = this;
	this.Layer_6.setTransform(-124.8,-44.8,1,1,0,0,0,-124.8,-44.8);
	this.Layer_6.depth = 0;
	this.Layer_6.isAttachedToCamera = 0
	this.Layer_6.isAttachedToMask = 0
	this.Layer_6.layerDepth = 0
	this.Layer_6.layerIndex = 3
	this.Layer_6.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_6).wait(59));

	// Layer_3_obj_
	this.Layer_3 = new lib.Symbol_7_Layer_3();
	this.Layer_3.name = "Layer_3";
	this.Layer_3.parent = this;
	this.Layer_3.setTransform(-77.8,31.15,1,1,0,0,0,-79.2,35.7);
	this.Layer_3.depth = 0;
	this.Layer_3.isAttachedToCamera = 0
	this.Layer_3.isAttachedToMask = 0
	this.Layer_3.layerDepth = 0
	this.Layer_3.layerIndex = 4
	this.Layer_3.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_3).wait(1).to({regX:0,regY:0,x:1.3962,y:-4.538},0).wait(13).to({regX:-79.2,regY:35.7,x:-77.8,y:31.15},0).wait(1).to({regX:0,regY:0,x:1.3962,y:-4.538},0).wait(14).to({regX:-79.2,regY:35.7,x:-77.8,y:31.15},0).wait(1).to({regX:0,regY:0,x:1.3962,y:-4.538},0).wait(13).to({regX:-79.2,regY:35.7,x:-77.8,y:31.15},0).wait(1).to({regX:0,regY:0,x:1.3962,y:-4.538},0).wait(14).to({regX:-79.2,regY:35.7,x:-77.8,y:31.15},0).wait(1));

	// Symbol_11_obj_
	this.Symbol_11 = new lib.Symbol_7_Symbol_11();
	this.Symbol_11.name = "Symbol_11";
	this.Symbol_11.parent = this;
	this.Symbol_11.setTransform(-67.3,9.6,1,1,0,0,0,-67.3,9.6);
	this.Symbol_11.depth = 0;
	this.Symbol_11.isAttachedToCamera = 0
	this.Symbol_11.isAttachedToMask = 0
	this.Symbol_11.layerDepth = 0
	this.Symbol_11.layerIndex = 5
	this.Symbol_11.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Symbol_11).wait(1).to({regX:-80.4,regY:9.3,x:-80.4,y:9.3},0).wait(13).to({regX:-67.3,regY:9.6,x:-67.3,y:9.6},0).wait(1).to({regX:-80.4,regY:9.3,x:-80.4,y:9.3},0).wait(14).to({regX:-67.3,regY:9.6,x:-67.3,y:9.6},0).wait(1).to({regX:-80.4,regY:9.3,x:-80.4,y:9.3},0).wait(13).to({regX:-67.3,regY:9.6,x:-67.3,y:9.6},0).wait(1).to({regX:-80.4,regY:9.3,x:-80.4,y:9.3},0).wait(14).to({regX:-67.3,regY:9.6,x:-67.3,y:9.6},0).wait(1));

	// Layer_13_obj_
	this.Layer_13 = new lib.Symbol_7_Layer_13();
	this.Layer_13.name = "Layer_13";
	this.Layer_13.parent = this;
	this.Layer_13.setTransform(-34.4,-20.7,1,1,0,0,0,-34.4,-20.7);
	this.Layer_13.depth = 0;
	this.Layer_13.isAttachedToCamera = 0
	this.Layer_13.isAttachedToMask = 0
	this.Layer_13.layerDepth = 0
	this.Layer_13.layerIndex = 6
	this.Layer_13.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_13).wait(59));

	// Layer_2_obj_
	this.Layer_2 = new lib.Symbol_7_Layer_2();
	this.Layer_2.name = "Layer_2";
	this.Layer_2.parent = this;
	this.Layer_2.setTransform(-51.45,15.15,1,1,0,0,0,-46.9,8.3);
	this.Layer_2.depth = 0;
	this.Layer_2.isAttachedToCamera = 0
	this.Layer_2.isAttachedToMask = 0
	this.Layer_2.layerDepth = 0
	this.Layer_2.layerIndex = 7
	this.Layer_2.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_2).wait(1).to({regX:0,regY:0,x:-4.55,y:6.85},0).wait(13).to({regX:-46.9,regY:8.3,x:-51.45,y:15.15},0).wait(1).to({regX:0,regY:0,x:-4.55,y:6.85},0).wait(14).to({regX:-46.9,regY:8.3,x:-51.45,y:15.15},0).wait(1).to({regX:0,regY:0,x:-4.55,y:6.85},0).wait(13).to({regX:-46.9,regY:8.3,x:-51.45,y:15.15},0).wait(1).to({regX:0,regY:0,x:-4.55,y:6.85},0).wait(14).to({regX:-46.9,regY:8.3,x:-51.45,y:15.15},0).wait(1));

	// Symbol_8_obj_
	this.Symbol_8 = new lib.Symbol_7_Symbol_8();
	this.Symbol_8.name = "Symbol_8";
	this.Symbol_8.parent = this;
	this.Symbol_8.setTransform(-32.2,7.9,1,1,0,0,0,-32.2,7.9);
	this.Symbol_8.depth = 0;
	this.Symbol_8.isAttachedToCamera = 0
	this.Symbol_8.isAttachedToMask = 0
	this.Symbol_8.layerDepth = 0
	this.Symbol_8.layerIndex = 8
	this.Symbol_8.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Symbol_8).wait(1).to({regX:-24.5,regY:20.4,x:-24.5,y:20.4},0).wait(13).to({regX:-32.2,regY:7.9,x:-32.2,y:7.9},0).wait(1).to({regX:-24.5,regY:20.4,x:-24.5,y:20.4},0).wait(14).to({regX:-32.2,regY:7.9,x:-32.2,y:7.9},0).wait(1).to({regX:-24.5,regY:20.4,x:-24.5,y:20.4},0).wait(13).to({regX:-32.2,regY:7.9,x:-32.2,y:7.9},0).wait(1).to({regX:-24.5,regY:20.4,x:-24.5,y:20.4},0).wait(14).to({regX:-32.2,regY:7.9,x:-32.2,y:7.9},0).wait(1));

	// Layer_11_obj_
	this.Layer_11 = new lib.Symbol_7_Layer_11();
	this.Layer_11.name = "Layer_11";
	this.Layer_11.parent = this;
	this.Layer_11.setTransform(19.2,-7.9,1,1,0,0,0,19.2,-7.9);
	this.Layer_11.depth = 0;
	this.Layer_11.isAttachedToCamera = 0
	this.Layer_11.isAttachedToMask = 0
	this.Layer_11.layerDepth = 0
	this.Layer_11.layerIndex = 9
	this.Layer_11.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_11).wait(59));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-231.6,-75.8,288.5,131.4);


(lib.Symbol4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_79 = function() {
		this.___loopingOver___ = true;
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(79).call(this.frame_79).wait(1));

	// Layer_2_obj_
	this.Layer_2 = new lib.Symbol_4_Layer_2();
	this.Layer_2.name = "Layer_2";
	this.Layer_2.parent = this;
	this.Layer_2.setTransform(-33,68.8,1,1,0,0,0,-33,68.8);
	this.Layer_2.depth = 0;
	this.Layer_2.isAttachedToCamera = 0
	this.Layer_2.isAttachedToMask = 0
	this.Layer_2.layerDepth = 0
	this.Layer_2.layerIndex = 0
	this.Layer_2.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_2).wait(15).to({regX:-50.6,regY:37.6,x:-50.6,y:37.6},0).wait(14).to({regX:-33,regY:68.8,x:-33,y:68.8},0).wait(31).to({regX:-50.6,regY:37.6,x:-50.6,y:37.6},0).wait(14).to({regX:-33,regY:68.8,x:-33,y:68.8},0).wait(6));

	// Layer_1_obj_
	this.Layer_1 = new lib.Symbol_4_Layer_1();
	this.Layer_1.name = "Layer_1";
	this.Layer_1.parent = this;
	this.Layer_1.depth = 0;
	this.Layer_1.isAttachedToCamera = 0
	this.Layer_1.isAttachedToMask = 0
	this.Layer_1.layerDepth = 0
	this.Layer_1.layerIndex = 1
	this.Layer_1.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_1).wait(80));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-85.1,-67.4,116.69999999999999,157.2);


(lib.Symbol2_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_79 = function() {
		this.___loopingOver___ = true;
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(79).call(this.frame_79).wait(1));

	// Layer_3 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("AzNt6IUCgeIADIHIOWAAIEASUI+bCWg");
	mask.setTransform(-15.025,33.125);

	// Layer_2_obj_
	this.Layer_2 = new lib.Symbol_2_Layer_2();
	this.Layer_2.name = "Layer_2";
	this.Layer_2.parent = this;
	this.Layer_2.setTransform(36.8,49.8,1,1,0,0,0,36.8,49.8);
	this.Layer_2.depth = 0;
	this.Layer_2.isAttachedToCamera = 0
	this.Layer_2.isAttachedToMask = 0
	this.Layer_2.layerDepth = 0
	this.Layer_2.layerIndex = 0
	this.Layer_2.maskLayerName = 0

	var maskedShapeInstanceList = [this.Layer_2];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.Layer_2).wait(20).to({regX:23.2,regY:43.8,x:23.2,y:43.8},0).wait(14).to({regX:36.8,regY:49.8,x:36.8,y:49.8},0).wait(46));

	// Layer_1_obj_
	this.Layer_1 = new lib.Symbol_2_Layer_1copy();
	this.Layer_1.name = "Layer_1";
	this.Layer_1.parent = this;
	this.Layer_1.depth = 0;
	this.Layer_1.isAttachedToCamera = 0
	this.Layer_1.isAttachedToMask = 0
	this.Layer_1.layerDepth = 0
	this.Layer_1.layerIndex = 1
	this.Layer_1.maskLayerName = 0

	var maskedShapeInstanceList = [this.Layer_1];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.Layer_1).wait(80));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-48.3,-52.9,101.3,118);


(lib.Symbol1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_79 = function() {
		this.___loopingOver___ = true;
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(79).call(this.frame_79).wait(1));

	// Layer_1_obj_
	this.Layer_1 = new lib.Symbol_1_Layer_1();
	this.Layer_1.name = "Layer_1";
	this.Layer_1.parent = this;
	this.Layer_1.setTransform(643.8,394.6,1,1,0,0,0,643.8,394.6);
	this.Layer_1.depth = 0;
	this.Layer_1.isAttachedToCamera = 0
	this.Layer_1.isAttachedToMask = 0
	this.Layer_1.layerDepth = 0
	this.Layer_1.layerIndex = 0
	this.Layer_1.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_1).wait(80));

	// Layer_2_obj_
	this.Layer_2 = new lib.Symbol_1_Layer_2();
	this.Layer_2.name = "Layer_2";
	this.Layer_2.parent = this;
	this.Layer_2.setTransform(688.9,423.3,1,1,0,0,0,688.9,423.3);
	this.Layer_2.depth = 0;
	this.Layer_2.isAttachedToCamera = 0
	this.Layer_2.isAttachedToMask = 0
	this.Layer_2.layerDepth = 0
	this.Layer_2.layerIndex = 1
	this.Layer_2.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_2).wait(80));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(600.8,343.7,118.70000000000005,105.90000000000003);


(lib.Scene_1_Layer_10 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_10
	this.instance = new lib.Symbol7();
	this.instance.parent = this;
	this.instance.setTransform(912.3,623.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Scene_1_Layer_10, null, null);


(lib.Scene_1_Layer_8 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_8
	this.instance = new lib.Symbol4();
	this.instance.parent = this;
	this.instance.setTransform(971.3,434.35);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Scene_1_Layer_8, null, null);


(lib.Scene_1_Layer_7 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_7
	this.instance = new lib.Symbol2_1();
	this.instance.parent = this;
	this.instance.setTransform(757.9,428.45);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Scene_1_Layer_7, null, null);


(lib.Scene_1_Layer_6 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_6
	this.instance = new lib.Symbol1();
	this.instance.parent = this;
	this.instance.setTransform(834.35,404.95,0.7707,0.7707,0,26.2175,-153.7825,654.4,405);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Scene_1_Layer_6, null, null);


// stage content:
(lib.March_main = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	this.___GetDepth___ = function(obj) {
		var depth = obj.depth;
		var cameraObj = this.___camera___instance;
		if(cameraObj && cameraObj.depth && obj.isAttachedToCamera)
		{
			depth += depth + cameraObj.depth;
		}
		return depth;
		}
	this.___needSorting___ = function() {
		for (var i = 0; i < this.getNumChildren() - 1; i++)
		{
			var prevDepth = this.___GetDepth___(this.getChildAt(i));
			var nextDepth = this.___GetDepth___(this.getChildAt(i + 1));
			if (prevDepth < nextDepth)
				return true;
		}
		return false;
	}
	this.___sortFunction___ = function(obj1, obj2) {
		return (this.exportRoot.___GetDepth___(obj2) - this.exportRoot.___GetDepth___(obj1));
	}
	this.on('tick', function (event){
		var curTimeline = event.currentTarget;
		if (curTimeline.___needSorting___()){
			this.sortChildren(curTimeline.___sortFunction___);
		}
	});

	// Layer_9_obj_
	this.Layer_9 = new lib.Scene_1_Layer_9();
	this.Layer_9.name = "Layer_9";
	this.Layer_9.parent = this;
	this.Layer_9.setTransform(910.4,337.3,1,1,0,0,0,910.4,337.3);
	this.Layer_9.depth = 0;
	this.Layer_9.isAttachedToCamera = 0
	this.Layer_9.isAttachedToMask = 0
	this.Layer_9.layerDepth = 0
	this.Layer_9.layerIndex = 0
	this.Layer_9.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_9).wait(1));

	// Layer_8_obj_
	this.Layer_8 = new lib.Scene_1_Layer_8();
	this.Layer_8.name = "Layer_8";
	this.Layer_8.parent = this;
	this.Layer_8.setTransform(962.1,445.5,1,1,0,0,0,962.1,445.5);
	this.Layer_8.depth = 0;
	this.Layer_8.isAttachedToCamera = 0
	this.Layer_8.isAttachedToMask = 0
	this.Layer_8.layerDepth = 0
	this.Layer_8.layerIndex = 1
	this.Layer_8.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_8).wait(1));

	// Layer_5_obj_
	this.Layer_5 = new lib.Scene_1_Layer_5();
	this.Layer_5.name = "Layer_5";
	this.Layer_5.parent = this;
	this.Layer_5.setTransform(783,353.8,1,1,0,0,0,783,353.8);
	this.Layer_5.depth = 0;
	this.Layer_5.isAttachedToCamera = 0
	this.Layer_5.isAttachedToMask = 0
	this.Layer_5.layerDepth = 0
	this.Layer_5.layerIndex = 2
	this.Layer_5.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_5).wait(1));

	// Layer_13_obj_
	this.Layer_13 = new lib.Scene_1_Layer_13();
	this.Layer_13.name = "Layer_13";
	this.Layer_13.parent = this;
	this.Layer_13.setTransform(941,415,1,1,0,0,0,941,415);
	this.Layer_13.depth = 0;
	this.Layer_13.isAttachedToCamera = 0
	this.Layer_13.isAttachedToMask = 0
	this.Layer_13.layerDepth = 0
	this.Layer_13.layerIndex = 3
	this.Layer_13.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_13).wait(1));

	// Layer_7_obj_
	this.Layer_7 = new lib.Scene_1_Layer_7();
	this.Layer_7.name = "Layer_7";
	this.Layer_7.parent = this;
	this.Layer_7.setTransform(742.9,461.6,1,1,0,0,0,742.9,461.6);
	this.Layer_7.depth = 0;
	this.Layer_7.isAttachedToCamera = 0
	this.Layer_7.isAttachedToMask = 0
	this.Layer_7.layerDepth = 0
	this.Layer_7.layerIndex = 4
	this.Layer_7.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_7).wait(1));

	// Layer_6_obj_
	this.Layer_6 = new lib.Scene_1_Layer_6();
	this.Layer_6.name = "Layer_6";
	this.Layer_6.parent = this;
	this.Layer_6.setTransform(834.4,398.3,1,1,0,0,0,834.4,398.3);
	this.Layer_6.depth = 0;
	this.Layer_6.isAttachedToCamera = 0
	this.Layer_6.isAttachedToMask = 0
	this.Layer_6.layerDepth = 0
	this.Layer_6.layerIndex = 5
	this.Layer_6.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_6).wait(1));

	// Layer_12_obj_
	this.Layer_12 = new lib.Scene_1_Layer_12();
	this.Layer_12.name = "Layer_12";
	this.Layer_12.parent = this;
	this.Layer_12.setTransform(829.5,396.5,1,1,0,0,0,829.5,396.5);
	this.Layer_12.depth = 0;
	this.Layer_12.isAttachedToCamera = 0
	this.Layer_12.isAttachedToMask = 0
	this.Layer_12.layerDepth = 0
	this.Layer_12.layerIndex = 6
	this.Layer_12.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_12).wait(1));

	// Layer_10_obj_
	this.Layer_10 = new lib.Scene_1_Layer_10();
	this.Layer_10.name = "Layer_10";
	this.Layer_10.parent = this;
	this.Layer_10.setTransform(827.9,610.6,1,1,0,0,0,827.9,610.6);
	this.Layer_10.depth = 0;
	this.Layer_10.isAttachedToCamera = 0
	this.Layer_10.isAttachedToMask = 0
	this.Layer_10.layerDepth = 0
	this.Layer_10.layerIndex = 7
	this.Layer_10.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_10).wait(1));

	// Layer_2_obj_
	this.Layer_2 = new lib.Scene_1_Layer_2();
	this.Layer_2.name = "Layer_2";
	this.Layer_2.parent = this;
	this.Layer_2.setTransform(537,337,1,1,0,0,0,537,337);
	this.Layer_2.depth = 0;
	this.Layer_2.isAttachedToCamera = 0
	this.Layer_2.isAttachedToMask = 0
	this.Layer_2.layerDepth = 0
	this.Layer_2.layerIndex = 8
	this.Layer_2.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_2).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(534,334,546,346);
// library properties:
lib.properties = {
	id: 'F19E807789E29F4982CBF553A6B89EC7',
	width: 1080,
	height: 680,
	fps: 24,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: [
		{src:"assets/images/bg40.jpg", id:"bg"},
		{src:"assets/images/hand140.png", id:"hand1"},
		{src:"assets/images/pack1_140.png", id:"pack1_1"},
		{src:"assets/images/pack2_140.png", id:"pack2_1"}
	],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['F19E807789E29F4982CBF553A6B89EC7'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}


// Layer depth API : 

AdobeAn.Layer = new function() {
	this.getLayerZDepth = function(timeline, layerName)
	{
		if(layerName === "Camera")
		layerName = "___camera___instance";
		var script = "if(timeline." + layerName + ") timeline." + layerName + ".depth; else 0;";
		return eval(script);
	}
	this.setLayerZDepth = function(timeline, layerName, zDepth)
	{
		const MAX_zDepth = 10000;
		const MIN_zDepth = -5000;
		if(zDepth > MAX_zDepth)
			zDepth = MAX_zDepth;
		else if(zDepth < MIN_zDepth)
			zDepth = MIN_zDepth;
		if(layerName === "Camera")
		layerName = "___camera___instance";
		var script = "if(timeline." + layerName + ") timeline." + layerName + ".depth = " + zDepth + ";";
		eval(script);
	}
	this.removeLayer = function(timeline, layerName)
	{
		if(layerName === "Camera")
		layerName = "___camera___instance";
		var script = "if(timeline." + layerName + ") timeline.removeChild(timeline." + layerName + ");";
		eval(script);
	}
	this.addNewLayer = function(timeline, layerName, zDepth)
	{
		if(layerName === "Camera")
		layerName = "___camera___instance";
		zDepth = typeof zDepth !== 'undefined' ? zDepth : 0;
		var layer = new createjs.MovieClip();
		layer.name = layerName;
		layer.depth = zDepth;
		layer.layerIndex = 0;
		timeline.addChild(layer);
	}
}


})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;

$(function () {
    var canvas, stage, exportRoot, anim_container, dom_overlay_container, fnStartAnimation;
    function init() {
        canvas = document.getElementById("canvas40");
        anim_container = document.getElementById("animation_container40");
        dom_overlay_container = document.getElementById("dom_overlay_container40");
        var comp=AdobeAn.getComposition("F19E807789E29F4982CBF553A6B89EC7");
        var lib=comp.getLibrary();
        var loader = new createjs.LoadQueue(false);
        loader.addEventListener("fileload", function(evt){handleFileLoad(evt,comp)});
        loader.addEventListener("complete", function(evt){handleComplete(evt,comp)});
        var lib=comp.getLibrary();
        loader.loadManifest(lib.properties.manifest);
    }
    function handleFileLoad(evt, comp) {
        var images=comp.getImages();
        if (evt && (evt.item.type == "image")) { images[evt.item.id] = evt.result; }
    }
    function handleComplete(evt,comp) {
        //This function is always called, irrespective of the content. You can use the variable "stage" after it is created in token create_stage.
        var lib=comp.getLibrary();
        var ss=comp.getSpriteSheet();
        var queue = evt.target;
        var ssMetadata = lib.ssMetadata;
        for(i=0; i<ssMetadata.length; i++) {
            ss[ssMetadata[i].name] = new createjs.SpriteSheet( {"images": [queue.getResult(ssMetadata[i].name)], "frames": ssMetadata[i].frames} )
        }
        var preloaderDiv = document.getElementById("_preload_div_40");
        preloaderDiv.style.display = 'none';
        canvas.style.display = 'block';
        exportRoot = new lib.March_main();
        stage = new lib.Stage(canvas);
        //Registers the "tick" event listener.
        fnStartAnimation = function() {
            stage.addChild(exportRoot);
            createjs.Ticker.setFPS(lib.properties.fps);
            createjs.Ticker.addEventListener("tick", stage)
            stage.addEventListener("tick", handleTick)
            function getProjectionMatrix(container, totalDepth) {
                var focalLength = 528.25;
                var projectionCenter = { x : lib.properties.width/2, y : lib.properties.height/2 };
                var scale = (totalDepth + focalLength)/focalLength;
                var scaleMat = new createjs.Matrix2D;
                scaleMat.a = 1/scale;
                scaleMat.d = 1/scale;
                var projMat = new createjs.Matrix2D;
                projMat.tx = -projectionCenter.x;
                projMat.ty = -projectionCenter.y;
                projMat = projMat.prependMatrix(scaleMat);
                projMat.tx += projectionCenter.x;
                projMat.ty += projectionCenter.y;
                return projMat;
            }
            function handleTick(event) {
                var cameraInstance = exportRoot.___camera___instance;
                if(cameraInstance !== undefined && cameraInstance.pinToObject !== undefined)
                {
                    cameraInstance.x = cameraInstance.pinToObject.x + cameraInstance.pinToObject.pinOffsetX;
                    cameraInstance.y = cameraInstance.pinToObject.y + cameraInstance.pinToObject.pinOffsetY;
                    if(cameraInstance.pinToObject.parent !== undefined && cameraInstance.pinToObject.parent.depth !== undefined)
                        cameraInstance.depth = cameraInstance.pinToObject.parent.depth + cameraInstance.pinToObject.pinOffsetZ;
                }
                applyLayerZDepth(exportRoot);
            }
            function applyLayerZDepth(parent)
            {
                var cameraInstance = parent.___camera___instance;
                var focalLength = 528.25;
                var projectionCenter = { 'x' : 0, 'y' : 0};
                if(parent === exportRoot)
                {
                    var stageCenter = { 'x' : lib.properties.width/2, 'y' : lib.properties.height/2 };
                    projectionCenter.x = stageCenter.x;
                    projectionCenter.y = stageCenter.y;
                }
                for(child in parent.children)
                {
                    var layerObj = parent.children[child];
                    if(layerObj == cameraInstance)
                        continue;
                    applyLayerZDepth(layerObj, cameraInstance);
                    if(layerObj.layerDepth === undefined)
                        continue;
                    if(layerObj.currentFrame != layerObj.parent.currentFrame)
                    {
                        layerObj.gotoAndPlay(layerObj.parent.currentFrame);
                    }
                    var matToApply = new createjs.Matrix2D;
                    var cameraMat = new createjs.Matrix2D;
                    var totalDepth = layerObj.layerDepth ? layerObj.layerDepth : 0;
                    var cameraDepth = 0;
                    if(cameraInstance && !layerObj.isAttachedToCamera)
                    {
                        var mat = cameraInstance.getMatrix();
                        mat.tx -= projectionCenter.x;
                        mat.ty -= projectionCenter.y;
                        cameraMat = mat.invert();
                        cameraMat.prependTransform(projectionCenter.x, projectionCenter.y, 1, 1, 0, 0, 0, 0, 0);
                        cameraMat.appendTransform(-projectionCenter.x, -projectionCenter.y, 1, 1, 0, 0, 0, 0, 0);
                        if(cameraInstance.depth)
                            cameraDepth = cameraInstance.depth;
                    }
                    if(layerObj.depth)
                    {
                        totalDepth = layerObj.depth;
                    }
                    //Offset by camera depth
                    totalDepth -= cameraDepth;
                    if(totalDepth < -focalLength)
                    {
                        matToApply.a = 0;
                        matToApply.d = 0;
                    }
                    else
                    {
                        if(layerObj.layerDepth)
                        {
                            var sizeLockedMat = getProjectionMatrix(parent, layerObj.layerDepth);
                            if(sizeLockedMat)
                            {
                                sizeLockedMat.invert();
                                matToApply.prependMatrix(sizeLockedMat);
                            }
                        }
                        matToApply.prependMatrix(cameraMat);
                        var projMat = getProjectionMatrix(parent, totalDepth);
                        if(projMat)
                        {
                            matToApply.prependMatrix(projMat);
                        }
                    }
                    layerObj.transformMatrix = matToApply;
                }
            }
        }
        //Code to support hidpi screens and responsive scaling.
        function makeResponsive(isResp, respDim, isScale, scaleType) {
            var lastW, lastH, lastS=1;
            window.addEventListener('resize', resizeCanvas);
            resizeCanvas();
            function resizeCanvas() {
                var w = lib.properties.width, h = lib.properties.height;
                var iw = window.innerWidth, ih=window.innerHeight;
                var pRatio = window.devicePixelRatio || 1, xRatio=iw/w, yRatio=ih/h, sRatio=1;
                if(isResp) {
                    if((respDim=='width'&&lastW==iw) || (respDim=='height'&&lastH==ih)) {
                        sRatio = lastS;
                    }
                    else if(!isScale) {
                        if(iw<w || ih<h)
                            sRatio = Math.min(xRatio, yRatio);
                    }
                    else if(scaleType==1) {
                        sRatio = Math.min(xRatio, yRatio);
                    }
                    else if(scaleType==2) {
                        sRatio = Math.max(xRatio, yRatio);
                    }
                }
                canvas.width = w*pRatio*sRatio;
                canvas.height = h*pRatio*sRatio;
                canvas.style.width = anim_container.style.width = dom_overlay_container.style.width = preloaderDiv.style.width = w*sRatio+'px';
                canvas.style.height = anim_container.style.height = dom_overlay_container.style.height = preloaderDiv.style.height = h*sRatio+'px';
                stage.scaleX = pRatio*sRatio;
                stage.scaleY = pRatio*sRatio;
                lastW = iw; lastH = ih; lastS = sRatio;
                stage.tickOnUpdate = false;
                stage.update();
                stage.tickOnUpdate = true;
            }
        }
        makeResponsive(true,'both',true,2);
        AdobeAn.compositionLoaded(lib.properties.id);
        fnStartAnimation();
    }
    init();
})