(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [];


// symbols:



(lib.bg = function() {
	this.initialize(img.bg);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,1080,680);// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.Symbol17 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Symbol 16
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(4.5,1,1).p("AgGAaQgCgeAQgV");
	this.shape.setTransform(0.7,5.9);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(4.5,1,1).p("AgEAMQgHgOASgJ");
	this.shape_1.setTransform(1,5.7);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(4.5,1,1).p("AgCAAQgKAAASAA");
	this.shape_2.setTransform(1.1,5.6);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(4.5,1,1).p("AAAgJQgNAJATAK");
	this.shape_3.setTransform(1.2,5.5);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(4.5,1,1).p("AABgPQgPAPAUAQ");
	this.shape_4.setTransform(1.3,5.5);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(4.5,1,1).p("AABgTQgPATAUAU");
	this.shape_5.setTransform(1.3,5.4);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(4.5,1,1).p("AAGAVQgUgVAQgU");
	this.shape_6.setTransform(1.3,5.4);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(4.5,1,1).p("AAAgGQgNAGATAH");
	this.shape_7.setTransform(1.2,5.5);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(4.5,1,1).p("AgDAFQgIgHASgC");
	this.shape_8.setTransform(1,5.7);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(4.5,1,1).p("AgEAPQgGgRARgM");
	this.shape_9.setTransform(0.9,5.8);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(4.5,1,1).p("AgGAVQgDgYAQgR");
	this.shape_10.setTransform(0.8,5.8);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(4.5,1,1).p("AgGAZQgDgcAQgV");
	this.shape_11.setTransform(0.8,5.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},9).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_6}]},49).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape}]},1).wait(37));

	// Symbol 15
	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(4.5,1,1).p("AAmAWQgdg5guAS");
	this.shape_12.setTransform(5,5.1);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(4.5,1,1).p("AgegOQAtgTAQA1");
	this.shape_13.setTransform(4.3,4.9);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(4.5,1,1).p("AgZgMQAtgTAGAx");
	this.shape_14.setTransform(3.8,4.7);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(4.5,1,1).p("AgVgLQAtgSgCAu");
	this.shape_15.setTransform(3.4,4.5);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(4.5,1,1).p("AgTgKQAugSgJAs");
	this.shape_16.setTransform(3.2,4.4);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(4.5,1,1).p("AgRgJQAtgTgMAr");
	this.shape_17.setTransform(3,4.4);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#000000").ss(4.5,1,1).p("AAPAPQANgqgtAS");
	this.shape_18.setTransform(3,4.3);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#000000").ss(4.5,1,1).p("AgWgLQAtgTAAAv");
	this.shape_19.setTransform(3.5,4.6);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#000000").ss(4.5,1,1).p("AgbgNQAtgTALAz");
	this.shape_20.setTransform(4.1,4.8);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#000000").ss(4.5,1,1).p("AgfgPQAtgSATA2");
	this.shape_21.setTransform(4.5,4.9);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#000000").ss(4.5,1,1).p("AgjgQQAugSAZA4");
	this.shape_22.setTransform(4.8,5);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#000000").ss(4.5,1,1).p("AgkgRQAtgSAcA5");
	this.shape_23.setTransform(4.9,5.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_12}]}).to({state:[{t:this.shape_12}]},9).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_18}]},49).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_12}]},1).wait(37));

	// Symbol 14
	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#000000").ss(4.5,1,1).p("AAsgKQg9gMgaAk");
	this.shape_24.setTransform(4.7,1.4);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f().s("#000000").ss(4.5,1,1).p("AgnAMQAiglAtAW");
	this.shape_25.setTransform(4.3,1.6);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f().s("#000000").ss(4.5,1,1).p("AgkALQAognAhAf");
	this.shape_26.setTransform(4,1.7);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f().s("#000000").ss(4.5,1,1).p("AgiALQAugpAXAm");
	this.shape_27.setTransform(3.8,1.7);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f().s("#000000").ss(4.5,1,1).p("AggAKQAxgpAQAq");
	this.shape_28.setTransform(3.6,1.8);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f().s("#000000").ss(4.5,1,1).p("AgfAJQA0gqALAt");
	this.shape_29.setTransform(3.5,1.9);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f().s("#000000").ss(4.5,1,1).p("AAgANQgKgvg1Aq");
	this.shape_30.setTransform(3.5,2);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f().s("#000000").ss(4.5,1,1).p("AgjALQAsgoAbAj");
	this.shape_31.setTransform(3.9,1.7);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f().s("#000000").ss(4.5,1,1).p("AgmAMQAmgnAnAb");
	this.shape_32.setTransform(4.2,1.6);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f().s("#000000").ss(4.5,1,1).p("AgoAMQAhglAwAV");
	this.shape_33.setTransform(4.4,1.6);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f().s("#000000").ss(4.5,1,1).p("AgqANQAdgkA4AQ");
	this.shape_34.setTransform(4.6,1.5);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f().s("#000000").ss(4.5,1,1).p("AgrAOQAbgkA8AN");
	this.shape_35.setTransform(4.7,1.4);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_24}]}).to({state:[{t:this.shape_24}]},9).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_27}]},1).to({state:[{t:this.shape_28}]},1).to({state:[{t:this.shape_29}]},1).to({state:[{t:this.shape_30}]},1).to({state:[{t:this.shape_30}]},49).to({state:[{t:this.shape_31}]},1).to({state:[{t:this.shape_32}]},1).to({state:[{t:this.shape_33}]},1).to({state:[{t:this.shape_34}]},1).to({state:[{t:this.shape_35}]},1).to({state:[{t:this.shape_24}]},1).wait(37));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-2.2,-2.2,13.7,13);


(lib.Symbol13 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(4.5,1,1).p("AgNg5QgIBAAkAz");
	this.shape.setTransform(1.5,5.8);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol13, new cjs.Rectangle(-2.2,-2.2,7.5,16.2), null);


(lib.Symbol12 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(5,1,1).p("AhbgpQgDAQATAcQAUAcAvAIQAvAIAYgRQAYgRAFgG");
	this.shape.setTransform(18.1,29.5);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(4.9,1,1).p("AhcgmQABAQATAYQAUAaAuAIQAvAHAWgNQAYgOAGgG");
	this.shape_1.setTransform(18.1,29.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(4.8,1,1).p("AhdgjQADAQAUAVQAVAXAtAIQAtAHAXgKQAXgLAHgG");
	this.shape_2.setTransform(18.1,28.8);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(4.8,1,1).p("AheghQAFAQAUATQAXAUArAIQAsAHAXgHQAXgJAIgG");
	this.shape_3.setTransform(18.2,28.5);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(4.7,1,1).p("AhegfQAHAQAUAQQAYATApAIQArAHAXgGQAXgGAIgG");
	this.shape_4.setTransform(18.2,28.2);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(4.7,1,1).p("AhfgdQAJAPAUAPQAZARAoAHQAqAIAXgEQAXgEAJgG");
	this.shape_5.setTransform(18.2,28);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(4.6,1,1).p("AhfgcQAKAPAVANQAYAQAoAHQApAIAYgDQAWgCAJgG");
	this.shape_6.setTransform(18.2,27.9);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(4.6,1,1).p("AhfgbQALAPAUAMQAaAOAmAIQAqAHAXgBQAWgCAKgF");
	this.shape_7.setTransform(18.2,27.8);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(4.5,1,1).p("AhggbQANAPAUAMQAaANAlAIQAqAHAXAAQAWgBAKgG");
	this.shape_8.setTransform(18.2,27.7);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(4.5,1,1).p("AhggaQANAPAUALQAaANAmAHQApAIAXgBQAWAAAKgF");
	this.shape_9.setTransform(18.2,27.6);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(4.5,1,1).p("AhggaQAfAiBGANQBIAMAUgL");
	this.shape_10.setTransform(18.2,27.6);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(4.5,1,1).p("AhggaQAeAiBHANQBHAMAVgL");
	this.shape_11.setTransform(18.2,27.6);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(4.6,1,1).p("AhfgcQAKAPAUANQAZAQAoAIQApAHAYgDQAWgDAJgF");
	this.shape_12.setTransform(18.2,27.9);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(4.7,1,1).p("AhegfQAHAQAUAQQAYATApAHQArAIAYgGQAWgGAIgG");
	this.shape_13.setTransform(18.2,28.2);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(4.8,1,1).p("AhdghQAFAQATASQAXAVArAIQAsAHAXgIQAXgIAHgG");
	this.shape_14.setTransform(18.1,28.5);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(4.9,1,1).p("AhcgkQABAPATAXQAVAZAtAIQAuAHAXgMQAYgNAGgG");
	this.shape_15.setTransform(18.1,29);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(4.9,1,1).p("AhbgmQgBAQATAYQAVAaAuAIQAvAHAWgNQAYgOAGgG");
	this.shape_16.setTransform(18.1,29.2);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(5,1,1).p("AhbgnQgCAQAUAaQATAaAvAJQAvAHAXgPQAYgPAFgH");
	this.shape_17.setTransform(18.1,29.3);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#000000").ss(5,1,1).p("AhbgoQgCAQATAbQATAbAwAJQAvAHAXgQQAYgQAFgG");
	this.shape_18.setTransform(18.1,29.4);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#000000").ss(5,1,1).p("AhbgoQgDAQATAbQATAcAwAIQAwAHAXgQQAYgQAFgH");
	this.shape_19.setTransform(18.1,29.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},19).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},45).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape}]},1).wait(23));

	// Layer 6
	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#000000").ss(5,1,1).p("Ag9gHQAngsAsAQQAsAQgEA8");
	this.shape_20.setTransform(35.7,18);

	this.timeline.addTween(cjs.Tween.get(this.shape_20).wait(107));

	// Layer 5
	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#000000").ss(5,1,1).p("Ag3APQANgtAuAEQAwAEAFAx");
	this.shape_21.setTransform(8.6,9.4);

	this.timeline.addTween(cjs.Tween.get(this.shape_21).wait(107));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0.5,4.2,44,32);


(lib.Symbol11 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Symbol 10
	this.instance = new lib.Symbol17();
	this.instance.parent = this;
	this.instance.setTransform(16.7,49.8,1,1,0,0,0,-1.2,3);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(9).to({rotation:-15,x:42.9,y:44.6},23,cjs.Ease.get(1)).wait(32).to({rotation:0,x:16.7,y:49.8},23,cjs.Ease.get(1)).wait(20));

	// Layer 5
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(4.5,1,1).p("ABKj4QiYDIgHCGQgICWCzAN");
	this.shape.setTransform(8.7,24.9);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(4.5,1,1).p("AA4j1QiPDKAACEQgCCSCyAL");
	this.shape_1.setTransform(10.6,24.7);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(4.5,1,1).p("AAnjzQiGDNAFCBQAFCPCwAK");
	this.shape_2.setTransform(12.4,24.6);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(4.5,1,1).p("AAXjxQh+DQALB/QALCLCuAJ");
	this.shape_3.setTransform(14.2,24.4);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(4.5,1,1).p("AAIjvQh2DSARB9QAQCICtAI");
	this.shape_4.setTransform(15.8,24.3);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(4.5,1,1).p("AgFjsQhwDUAWB6QAXCFCrAG");
	this.shape_5.setTransform(17.3,24.1);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(4.5,1,1).p("AgSjqQhpDVAcB5QAbCCCqAF");
	this.shape_6.setTransform(18.7,24);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(4.5,1,1).p("AgejoQhiDXAgB3QAhB/CoAE");
	this.shape_7.setTransform(19.9,23.8);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(4.5,1,1).p("AgqjnQhcDaAmB1QAkB8CoAE");
	this.shape_8.setTransform(21.2,23.7);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(4.5,1,1).p("Ag0jlQhWDbApBzQApB7CnAC");
	this.shape_9.setTransform(22.3,23.6);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(4.5,1,1).p("Ag9jkQhRDdAtByQAuB4ClAC");
	this.shape_10.setTransform(23.3,23.5);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(4.5,1,1).p("AhGjiQhLDdAxBxQAxB2CkAB");
	this.shape_11.setTransform(24.2,23.4);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(4.5,1,1).p("AhNjhQhHDfA0BwQA0B0CjgB");
	this.shape_12.setTransform(25,23.3);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(4.5,1,1).p("AhUjgQhDDgA3BvQA4ByCigB");
	this.shape_13.setTransform(25.8,23.2);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(4.5,1,1).p("AhbjfQg/DhA6BuQA6BwCigB");
	this.shape_14.setTransform(26.5,23.2);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(4.5,1,1).p("AhgjdQg7DhA8BtQA9BvChgC");
	this.shape_15.setTransform(27,23.1);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(4.5,1,1).p("AhljdQg4DjA+BsQA/BuChgD");
	this.shape_16.setTransform(27.6,23);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(4.5,1,1).p("AhpjcQg2DjBBBsQBABtCggD");
	this.shape_17.setTransform(28,23);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#000000").ss(4.5,1,1).p("AhsjbQg0DjBCBsQBDBrCfgD");
	this.shape_18.setTransform(28.4,22.9);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#000000").ss(4.5,1,1).p("AhwjbQgxDlBDBqQBEBrCfgD");
	this.shape_19.setTransform(28.7,22.9);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#000000").ss(4.5,1,1).p("AhxjaQgxDkBFBrQBFBqCegE");
	this.shape_20.setTransform(28.9,22.9);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#000000").ss(4.5,1,1).p("AhzjaQgvDkBFBrQBGBqCegE");
	this.shape_21.setTransform(29.1,22.9);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#000000").ss(4.5,1,1).p("Ah0jaQguDlBFBqQBGBqCfgE");
	this.shape_22.setTransform(29.2,22.8);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#000000").ss(4.5,1,1).p("Ah0jaQgvDlBGBqQBGBqCfgE");
	this.shape_23.setTransform(29.2,22.8);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#000000").ss(4.5,1,1).p("AhmjdQg4DjA/BsQBABuCggD");
	this.shape_24.setTransform(27.7,23);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f().s("#000000").ss(4.5,1,1).p("AhZjfQhADgA5BvQA5BxCigB");
	this.shape_25.setTransform(26.2,23.2);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f().s("#000000").ss(4.5,1,1).p("AhLjhQhIDfAzBwQAzB0CkAA");
	this.shape_26.setTransform(24.7,23.3);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f().s("#000000").ss(4.5,1,1).p("Ag+jjQhQDcAtByQAuB4ClAB");
	this.shape_27.setTransform(23.3,23.5);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f().s("#000000").ss(4.5,1,1).p("AgxjmQhXDaAoB1QAnB6CnAE");
	this.shape_28.setTransform(21.9,23.7);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f().s("#000000").ss(4.5,1,1).p("AgkjoQhfDZAjB2QAjB+CoAE");
	this.shape_29.setTransform(20.6,23.8);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f().s("#000000").ss(4.5,1,1).p("AgYjqQhmDXAeB4QAeCBCpAF");
	this.shape_30.setTransform(19.3,23.9);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f().s("#000000").ss(4.5,1,1).p("AgNjrQhrDVAZB5QAZCDCrAG");
	this.shape_31.setTransform(18.1,24);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f().s("#000000").ss(4.5,1,1).p("AgCjtQhyDTAWB8QAVCFCrAH");
	this.shape_32.setTransform(16.9,24.1);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f().s("#000000").ss(4.5,1,1).p("AAHjuQh2DRASB9QAQCICtAH");
	this.shape_33.setTransform(15.8,24.2);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f().s("#000000").ss(4.5,1,1).p("AARjwQh7DRAOB+QANCJCuAJ");
	this.shape_34.setTransform(14.8,24.3);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f().s("#000000").ss(4.5,1,1).p("AAajxQiADPALB/QAJCMCvAJ");
	this.shape_35.setTransform(13.8,24.4);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f().s("#000000").ss(4.5,1,1).p("AAijyQiDDNAHCBQAGCNCwAK");
	this.shape_36.setTransform(12.9,24.5);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f().s("#000000").ss(4.5,1,1).p("AAqjzQiIDMAFCCQAECPCwAK");
	this.shape_37.setTransform(12.1,24.6);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f().s("#000000").ss(4.5,1,1).p("AAwj1QiLDMACCDQACCRCxAL");
	this.shape_38.setTransform(11.4,24.7);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f().s("#000000").ss(4.5,1,1).p("AA3j1QiPDKAACEQgBCSCyAL");
	this.shape_39.setTransform(10.8,24.7);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f().s("#000000").ss(4.5,1,1).p("AA8j2QiRDKgCCEQgDCTCyAM");
	this.shape_40.setTransform(10.2,24.8);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f().s("#000000").ss(4.5,1,1).p("ABAj2QiTDJgDCFQgFCUCzAM");
	this.shape_41.setTransform(9.7,24.8);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f().s("#000000").ss(4.5,1,1).p("ABEj3QiVDJgFCFQgFCVCyAM");
	this.shape_42.setTransform(9.3,24.9);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f().s("#000000").ss(4.5,1,1).p("ABGj4QiWDJgGCFQgHCWC0AN");
	this.shape_43.setTransform(9.1,24.9);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f().s("#000000").ss(4.5,1,1).p("ABJj4QiXDJgHCFQgICWC0AN");
	this.shape_44.setTransform(8.8,24.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},9).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_23}]},32).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_27}]},1).to({state:[{t:this.shape_28}]},1).to({state:[{t:this.shape_29}]},1).to({state:[{t:this.shape_30}]},1).to({state:[{t:this.shape_31}]},1).to({state:[{t:this.shape_32}]},1).to({state:[{t:this.shape_33}]},1).to({state:[{t:this.shape_34}]},1).to({state:[{t:this.shape_35}]},1).to({state:[{t:this.shape_36}]},1).to({state:[{t:this.shape_37}]},1).to({state:[{t:this.shape_38}]},1).to({state:[{t:this.shape_39}]},1).to({state:[{t:this.shape_40}]},1).to({state:[{t:this.shape_41}]},1).to({state:[{t:this.shape_42}]},1).to({state:[{t:this.shape_43}]},1).to({state:[{t:this.shape_44}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).wait(20));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-2.2,-2.2,31.6,59.8);


(lib.Symbol7 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("AncHxIDGu+QLygiABgCIicPQImOAJQlrAKggAAIgEgBgAFBHfIAAAAIAAAAIAAAAg");
	mask.setTransform(47.5,26.3);

	// Symbol 6
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(4.5,1,1).p("AAZAKQgBAAgXgKQgJgDgQgG");
	this.shape.setTransform(47.8,14.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(4.5,1,1).p("AgZgHQAPAGAKADQAWAKAEgF");
	this.shape_1.setTransform(48,14);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(4.5,1,1).p("AgbgHQAQAHAKADQAWAJAHgJ");
	this.shape_2.setTransform(48.1,13.9);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(4.5,1,1).p("AgcgGQAPAGALADQAWAKAJgO");
	this.shape_3.setTransform(48.3,13.9);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(4.5,1,1).p("AAegFQgMASgWgJQgKgEgQgG");
	this.shape_4.setTransform(48.4,13.8);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(4.5,1,1).p("AgcgGQAPAGALADQAWAKAJgN");
	this.shape_5.setTransform(48.3,13.9);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(4.5,1,1).p("AgagHQAQAGAKADQAWAKAFgG");
	this.shape_6.setTransform(48,14);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(4.5,1,1).p("AgZgIQAQAHAJADQAXAJADgD");
	this.shape_7.setTransform(47.9,14);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(4.5,1,1).p("AgYgJQAPAHAKADQAWAJACgB");
	this.shape_8.setTransform(47.9,14.1);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(4.5,1,1).p("AgYgJQAQAHAJADQAXAJABAA");
	this.shape_9.setTransform(47.8,14.2);

	var maskedShapeInstanceList = [this.shape,this.shape_1,this.shape_2,this.shape_3,this.shape_4,this.shape_5,this.shape_6,this.shape_7,this.shape_8,this.shape_9];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},9).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},4).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape}]},1).wait(15));
	this.timeline.addTween(cjs.Tween.get(this.shape).wait(9).to({_off:true},1).wait(10).to({_off:false},0).wait(4).to({_off:true},1).wait(10).to({_off:false},0).wait(15));

	// Symbol 4
	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(4.5,1,1).p("AAGgvQgUAWARBJ");
	this.shape_10.setTransform(47.7,9.4);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(4.5,1,1).p("AgBAtQgRhJAagQ");
	this.shape_11.setTransform(48.1,9.7);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(4.5,1,1).p("AgEApQgShJAhgJ");
	this.shape_12.setTransform(48.5,10.1);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(4.5,1,1).p("AgIAmQgShJAogC");
	this.shape_13.setTransform(48.9,10.4);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(4.5,1,1).p("AARgiQgugEARBJ");
	this.shape_14.setTransform(49.2,10.7);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(4.5,1,1).p("AgIAnQgRhKAngD");
	this.shape_15.setTransform(48.8,10.4);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(4.5,1,1).p("AgFApQgRhJAhgJ");
	this.shape_16.setTransform(48.5,10.1);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(4.5,1,1).p("AgCAsQgRhKAcgN");
	this.shape_17.setTransform(48.2,9.8);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#000000").ss(4.5,1,1).p("AAAAuQgRhKAZgR");
	this.shape_18.setTransform(48,9.6);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#000000").ss(4.5,1,1).p("AABAvQgQhJAVgU");
	this.shape_19.setTransform(47.8,9.5);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#000000").ss(4.5,1,1).p("AACAwQgQhJAUgW");
	this.shape_20.setTransform(47.7,9.4);

	var maskedShapeInstanceList = [this.shape_10,this.shape_11,this.shape_12,this.shape_13,this.shape_14,this.shape_15,this.shape_16,this.shape_17,this.shape_18,this.shape_19,this.shape_20];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_10}]}).to({state:[{t:this.shape_10}]},9).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_10}]},4).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_10}]},1).wait(15));
	this.timeline.addTween(cjs.Tween.get(this.shape_10).wait(9).to({_off:true},1).wait(10).to({_off:false},0).wait(4).to({_off:true},1).wait(10).to({_off:false},0).wait(15));

	// Symbol 5
	this.instance = new lib.Symbol13();
	this.instance.parent = this;
	this.instance.setTransform(46.4,12.5,1,1,0,0,0,2.9,12.5);

	var maskedShapeInstanceList = [this.instance];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(9).to({regX:3,regY:12.4,scaleY:0.86,rotation:36.9,x:45.4,y:14.5},4).to({regX:2.9,regY:12.5,scaleY:1,rotation:0,x:46.4,y:12.5},7,cjs.Ease.get(1)).wait(4).to({regX:3,regY:12.4,scaleY:0.86,rotation:36.9,x:45.4,y:14.5},4).to({regX:2.9,regY:12.5,scaleY:1,rotation:0,x:46.4,y:12.5},7,cjs.Ease.get(1)).wait(15));

	// Layer 5
	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#000000").ss(4.5,1,1).p("AjkCXQDMArCFiPQBNhRAriC");
	this.shape_21.setTransform(22.9,28);

	var maskedShapeInstanceList = [this.shape_21];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.shape_21).wait(50));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-0.2,-2.2,52.8,48.6);


// stage content:
(lib.Holiday = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 5
	this.instance = new lib.Symbol11();
	this.instance.parent = this;
	this.instance.setTransform(669.8,308,2.565,2.565,10.1,0,0,16.2,0.1);

	this.instance_1 = new lib.Symbol7();
	this.instance_1.parent = this;
	this.instance_1.setTransform(797.8,406.9,2.565,2.565,10.1,0,0,-0.1,43.1);

	this.instance_2 = new lib.Symbol12();
	this.instance_2.parent = this;
	this.instance_2.setTransform(767.2,285.7,2.565,2.565,10.1,0,0,20.7,16.3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_2},{t:this.instance_1},{t:this.instance}]}).wait(1));

	// Layer 2
	this.instance_3 = new lib.bg();
	this.instance_3.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(540,340,1080,680);
// library properties:
lib.properties = {
	id: '3F36265D47477F4AB03D5E07F220A841',
	width: 1080,
	height: 680,
	fps: 30,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: [
		{src:"assets/images/bg11.jpg", id:"bg"}
	],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['3F36265D47477F4AB03D5E07F220A841'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}



})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;

$(function () {
    var canvas, stage, exportRoot, anim_container, dom_overlay_container, fnStartAnimation;
    function init() {
        canvas = document.getElementById("canvas12");
        anim_container = document.getElementById("animation_container12");
        dom_overlay_container = document.getElementById("dom_overlay_container12");
        var comp=AdobeAn.getComposition("3F36265D47477F4AB03D5E07F220A841");
        var lib=comp.getLibrary();
        var loader = new createjs.LoadQueue(false);
        loader.addEventListener("fileload", function(evt){handleFileLoad(evt,comp)});
        loader.addEventListener("complete", function(evt){handleComplete(evt,comp)});
        var lib=comp.getLibrary();
        loader.loadManifest(lib.properties.manifest);
    }
    function handleFileLoad(evt, comp) {
        var images=comp.getImages();
        if (evt && (evt.item.type == "image")) { images[evt.item.id] = evt.result; }
    }
    function handleComplete(evt,comp) {
        //This function is always called, irrespective of the content. You can use the variable "stage" after it is created in token create_stage.
        var lib=comp.getLibrary();
        var ss=comp.getSpriteSheet();
        var queue = evt.target;
        var ssMetadata = lib.ssMetadata;
        for(i=0; i<ssMetadata.length; i++) {
            ss[ssMetadata[i].name] = new createjs.SpriteSheet( {"images": [queue.getResult(ssMetadata[i].name)], "frames": ssMetadata[i].frames} )
        }
        exportRoot = new lib.Holiday();
        stage = new lib.Stage(canvas);
        //Registers the "tick" event listener.
        fnStartAnimation = function() {
            stage.addChild(exportRoot);
            createjs.Ticker.setFPS(lib.properties.fps);
            createjs.Ticker.addEventListener("tick", stage);
        }
        //Code to support hidpi screens and responsive scaling.
        function makeResponsive(isResp, respDim, isScale, scaleType) {
            var lastW, lastH, lastS=1;
            window.addEventListener('resize', resizeCanvas);
            resizeCanvas();
            function resizeCanvas() {
                var w = lib.properties.width, h = lib.properties.height;
                var iw = window.innerWidth, ih=window.innerHeight;
                var pRatio = window.devicePixelRatio || 1, xRatio=iw/w, yRatio=ih/h, sRatio=1;
                if(isResp) {
                    if((respDim=='width'&&lastW==iw) || (respDim=='height'&&lastH==ih)) {
                        sRatio = lastS;
                    }
                    else if(!isScale) {
                        if(iw<w || ih<h)
                            sRatio = Math.min(xRatio, yRatio);
                    }
                    else if(scaleType==1) {
                        sRatio = Math.min(xRatio, yRatio);
                    }
                    else if(scaleType==2) {
                        sRatio = Math.max(xRatio, yRatio);
                    }
                }
                canvas.width = w*pRatio*sRatio;
                canvas.height = h*pRatio*sRatio;
                canvas.style.width = dom_overlay_container.style.width = anim_container.style.width =  w*sRatio+'px';
                canvas.style.height = anim_container.style.height = dom_overlay_container.style.height = h*sRatio+'px';
                stage.scaleX = pRatio*sRatio;
                stage.scaleY = pRatio*sRatio;
                lastW = iw; lastH = ih; lastS = sRatio;
                stage.tickOnUpdate = false;
                stage.update();
                stage.tickOnUpdate = true;
            }
        }
        makeResponsive(true,'both',true,2);
        AdobeAn.compositionLoaded(lib.properties.id);
        fnStartAnimation();
    }
    init();
});