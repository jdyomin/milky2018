(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [];


// symbols:



(lib.bg1_vert = function() {
	this.initialize(img.bg1_vert);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,864,991);


(lib.glasses = function() {
	this.initialize(img.glasses);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,456,169);


(lib.phone = function() {
	this.initialize(img.phone);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,142,278);// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.Symbol13 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.bg1_vert();
	this.instance.parent = this;
	this.instance.setTransform(21.65,-604.95);

	this.shape = new cjs.Shape();
	this.shape.graphics.f("#5C9400").s().p("EiePB+fMAAAj89ME8fAAAMAAAD89g");
	this.shape.setTransform(0,0.025);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape},{t:this.instance}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol13, new cjs.Rectangle(-1012.7,-809.4,2025.5,1618.9), null);


(lib.Symbol11 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FB0000").s().p("AgJAkQgYgHgQgPQgPgOAFgPQADgOAVgHQAVgFAYAHQAYAGAPAPQAQAPgFAOQgDAPgVAGQgKACgLAAQgLAAgNgDg");
	this.shape.setTransform(0,-0.002);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol11, new cjs.Rectangle(-6.1,-3.9,12.3,7.9), null);


(lib.Symbol9 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_3
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(7,1,1).p("AA2AQQgYgzhUAg");
	this.shape.setTransform(-3.3,-8.1381);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	// Layer_2
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(7,1,1).p("AA5g4Qh3gNAHCA");
	this.shape_1.setTransform(-3.8193,-1.6572);

	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(1));

	// Layer_1
	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(7,1,1).p("AAchDQhXAeAzBp");
	this.shape_2.setTransform(-0.014,0.025);

	this.timeline.addTween(cjs.Tween.get(this.shape_2).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol9, new cjs.Rectangle(-13,-13.2,19.3,23.5), null);


(lib.Symbol7 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_3
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(7,1,1).p("AgtBGQhXhTDMg4");
	this.shape.setTransform(12.3165,-1.475);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	// Layer_2
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(7,1,1).p("AAZB1QhchPBQia");
	this.shape_1.setTransform(2.5258,-6.575);

	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(1));

	// Layer_1
	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(7,1,1).p("ABOA4Qh8gLgfhk");
	this.shape_2.setTransform(0,-0.025);

	this.timeline.addTween(cjs.Tween.get(this.shape_2).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol7, new cjs.Rectangle(-11.2,-21.8,34.2,30.9), null);


(lib.Symbol5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.glasses();
	this.instance.parent = this;
	this.instance.setTransform(-228,-84.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol5, new cjs.Rectangle(-228,-84.5,456,169), null);


(lib.Symbol2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.phone();
	this.instance.parent = this;
	this.instance.setTransform(-71,-139);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol2, new cjs.Rectangle(-71,-139,142,278), null);


(lib.Symbol_10_Layer_11 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_11
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AhnBrQgrgsAAg/QAAg+ArgsQArgtA8ABQA9gBAqAtQAsAsAAA+QAAA/gsAsQgqAsg9AAQg8AAgrgsg");
	this.shape.setTransform(62.1,11.7);

	this.timeline.addTween(cjs.Tween.get(this.shape).to({_off:true},45).wait(49).to({_off:false},0).wait(16));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_10_Layer_9 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_9
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#C7C7C7").s().p("AhjAIQApgpA8AAQA9AAAjAiQAjAjgSgNQgSgNglgQQglgRgcACQgdACgPAJQgPAJgmAZQgPAKgDAAQgEAAAZgag");
	this.shape.setTransform(61.8725,-0.0279);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#C7C7C7").s().p("AiTA1QADgdArgrQArgsA8AAQA9AAArAsQArArgBAXQgBAXg3gxQg3gvg2AKQg4AKgmAsQgYAagIAAQgFAAABgLg");
	this.shape_1.setTransform(61.8903,2.9641);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#C7C7C7").s().p("ABwBLQgkgphAAHQg/AHgwAgQgxAgACg6QABg4ArgtQArgsA8AAQA9AAArAsQArAtAAA6QgBAkgMAAQgJAAgOgRg");
	this.shape_2.setTransform(61.9964,5.7201);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#C7C7C7").s().p("AABB7Qg9gFgqgNQgsgOAAg/QAAg+ArgsQArgsA8AAQA9AAAqAsQAsAsAAA+QAAA/grASQgiAOguAAIgXAAg");
	this.shape_3.setTransform(62.1,8.895);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#C7C7C7").s().p("AhnBrQgrgsAAg/QAAg+ArgsQArgtA8ABQA9gBAqAtQAsAsAAA+QAAA/gsAsQgqAsg9AAQg8AAgrgsg");
	this.shape_4.setTransform(62.1,11.7);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#C7C7C7").s().p("AAAB7Qg7gFgsgNQgrgOAAg/QAAg+AqgsQAsgsA8AAQA9AAAqAsQAsAsAAA+QAAA/gqASQgjAOguAAIgYAAg");
	this.shape_5.setTransform(62.1,8.895);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#C7C7C7").s().p("AgDB/Qg9gLgpgQQgpgQAAg/QAAg+ArgsQArgsA8AAQA9AAAqAsQAsAsAAA+QAAA/gsAbQggATgoAAQgRAAgRgDg");
	this.shape_6.setTransform(62.1,9.5819);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#C7C7C7").s().p("ABdBcQgygZg1gDQg2gDgigPQgigPgJgUQgJgUArgsQArgsA9AAQA8AAArAsQArAsAAA+QAAAtgaAAQgKAAgOgGg");
	this.shape_7.setTransform(62.4717,6.3949);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#C7C7C7").s().p("ABiBLQgugug4ALQg5AMgjgIQgkgIgJgVQgJgTArgtQArgsA9AAQA8AAArAsQArAsAAA+QAAAlgPAAQgLAAgTgTg");
	this.shape_8.setTransform(62.4717,5.951);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#C7C7C7").s().p("ABjA8QgwgzhCAAQhDgBgZATQgaATgJgVQgJgVArgrQArgsA9AAQA8AAArAsQArArACA1QABAbgMAAQgLAAgXgYg");
	this.shape_9.setTransform(62.5626,4.9654);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#C7C7C7").s().p("ABpArQgphDhIAQQhJAPgaAUQgaAVgJgVQgJgVArgrQArgsA9AAQA8AAArAsQArArACA1QABAXgHAAQgKAAgXgng");
	this.shape_10.setTransform(62.5629,4.7619);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape}]},12).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape}]},1).to({state:[]},1).to({state:[{t:this.shape}]},19).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[]},1).to({state:[{t:this.shape_10}]},38).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape}]},1).to({state:[]},1).wait(11));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_10_Layer_8 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_8
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AhoBrQgqgsAAg/QAAg+AqgtQAsgrA8gBQA9ABAqArQAsAtAAA+QAAA/gsAsQgqAtg9AAQg8AAgsgtg");
	this.shape.setTransform(0.1,6.8);

	this.timeline.addTween(cjs.Tween.get(this.shape).to({_off:true},45).wait(49).to({_off:false},0).wait(16));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_10_Layer_6 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_6
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#C7C7C7").s().p("AhjAIQApgpA8AAQA9AAAjAiQAjAjgSgNQgSgNglgQQglgRgcACQgdACgPAJQgPAJgmAZQgPAKgDAAQgEAAAZgag");
	this.shape.setTransform(-0.1275,-4.9279);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#C7C7C7").s().p("AiTA1QADgdArgrQArgsA8AAQA9AAArAsQArArgBAXQgBAXg3gxQg3gvg2AKQg4AKgmAsQgYAagIAAQgFAAABgLg");
	this.shape_1.setTransform(-0.1097,-1.9359);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#C7C7C7").s().p("ABwBLQgkgphAAHQg/AHgwAgQgxAgACg6QABg4ArgtQArgsA8AAQA9AAArAsQArAtAAA6QgBAkgMAAQgJAAgOgRg");
	this.shape_2.setTransform(-0.0036,0.8201);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#C7C7C7").s().p("AABB7Qg9gFgrgNQgrgOAAg/QAAg+AqgsQAsgsA8AAQA9AAAqAsQAsAsAAA+QAAA/grASQgiAOguAAIgXAAg");
	this.shape_3.setTransform(0.1,3.995);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#C7C7C7").s().p("AhoBrQgqgsAAg/QAAg+AqgtQAsgrA8gBQA9ABAqArQAsAtAAA+QAAA/gsAsQgqAtg9AAQg8AAgsgtg");
	this.shape_4.setTransform(0.1,6.8);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#C7C7C7").s().p("AhfBtQgzgMAAg/QAAg+AqgsQAsgsA8AAQA9AAAqAsQAsAsAAA+QAABAgiAGQgiAGg/AFQgSACgSAAQgoAAgjgIg");
	this.shape_5.setTransform(0.1,3.3544);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#C7C7C7").s().p("ABuBaQgigBhCgTQhBgUgsASQgsARgBgrQgCgqAqgtQAsgsA8AAQA9AAAqAsQAsAtgCAtQgBAtghAAIgBAAg");
	this.shape_6.setTransform(0.1349,0.613);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#C7C7C7").s().p("ABwBWQgggHg4g4Qg2g4g4A8Qg5A9gBgrQgCgsAqgrQAsgsA8AAQA9AAAqAsQAsAsgCAtQgBAogYAAIgIgBg");
	this.shape_7.setTransform(0.1349,0.3457);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#C7C7C7").s().p("AB/BDQgRgfhHgfQhFgeg5A7Qg4A9gCgrQgCgrArgsQArgsA9AAQA8AAArAsQArAsAAAsQgBAbgGAAQgEAAgIgNg");
	this.shape_8.setTransform(0.1587,-0.3667);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#C7C7C7").s().p("ACCBDQgRgfhMgmQhKgog5A6Qg5A7AEghQADghArgsQArgsA8AAQA9AAArAsQArAsAAAsQgBAbgGAAQgEAAgIgNg");
	this.shape_9.setTransform(-0.1366,-0.3667);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape}]},12).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape}]},1).to({state:[]},1).to({state:[{t:this.shape}]},19).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[]},1).to({state:[{t:this.shape_9}]},38).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape}]},1).to({state:[]},1).wait(11));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_10_Layer_3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_3
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(7,1,1).p("AhRgGQAhAKAhACQAfABAUAAQATAAAbgH");
	this.shape.setTransform(29.375,57.7254);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(7,1,1).p("AhggUQATAUAbAKQANAGANACQAMACAMABQAZAAAUgFQAcgJAYgR");
	this.shape_1.setTransform(30.025,56.3521);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(7,1,1).p("AhuggQARAfAeARQAPAJAQAEQAPAEAOAAQAfABAZgLQAjgQAWga");
	this.shape_2.setTransform(30.6,55.1761);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(7,1,1).p("Ah5grQAOAqAiAWQARAMASAGQARAEARABQAjABAdgPQAogWAWgj");
	this.shape_3.setTransform(31.05,54.1281);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(7,1,1).p("AiDg0QANAyAkAbQASAPAVAGQASAGATABQAnABAhgTQAsgbAWgr");
	this.shape_4.setTransform(31.475,53.2776);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(7,1,1).p("AiLg7QALA4AnAgQATAQAXAIQATAGAVABQApABAlgWQAvgfAWgw");
	this.shape_5.setTransform(31.825,52.5548);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(7,1,1).p("AiRhBQAKA9AoAjQAUASAYAJQAUAHAXABQAsABAngYQAygjAVg1");
	this.shape_6.setTransform(32.075,52.002);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(7,1,1).p("AiVhFQAIBBAqAlQAVATAZAKQAVAHAXABQAtABApgaQA1glAUg4");
	this.shape_7.setTransform(32.275,51.6041);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(7,1,1).p("AiYhIQAIBEArAnQAVATAZAKQAWAIAYAAQAuACAqgbQA1gmAVg7");
	this.shape_8.setTransform(32.375,51.354);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(7,1,1).p("AiZhIQAIBEArAnQAnAlA1ABQA0ACAsggQAvgiAVg7");
	this.shape_9.setTransform(32.425,51.2784);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(7,1,1).p("Ah/gxQANAvAjAaQASANAUAGQARAGASAAQAlABAhgRQAqgZAWgo");
	this.shape_10.setTransform(31.35,53.6027);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(7,1,1).p("AhrgeQARAeAeAPQAPAJAPADQAPADANABQAeABAYgKQAhgPAXgY");
	this.shape_11.setTransform(30.45,55.3798);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(7,1,1).p("AhdgRQAVARAZAJQAMAEANACQAMACAKABQAYAAATgEQAbgHAYgO");
	this.shape_12.setTransform(29.875,56.6775);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(7,1,1).p("AhUgJQAWAJAXAFQALACALABQAKABAIAAQAWABAPgBQAWgDAZgH");
	this.shape_13.setTransform(29.5,57.4583);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},45).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_9}]},35).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape}]},1).wait(16));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_10_Layer_2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(7,1,1).p("AiPhVQgMAsAhA5QAhA6A+AKQA+ALAxgoQAxgpAOhH");
	this.shape.setTransform(61.7791,17.9461);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(7,1,1).p("AiRg3QAFAYAkAnQAkAnAwAHQAwAIAvgYQAvgXAYgq");
	this.shape_1.setTransform(61.925,14.9);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(7,1,1).p("AiRgaQAUAFAnAVQAnAUAjAFQAiAFAtgHQAtgIAigO");
	this.shape_2.setTransform(61.925,12);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(7,1,1).p("AiRgIQAjgNAqAEQAqADAWACQAVACArAIQArAHArAO");
	this.shape_3.setTransform(61.925,10.204);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(7,1,1).p("AiRATQAUgkAugQQAsgPApACQAqACApAYQApAYAQAr");
	this.shape_4.setTransform(61.925,7.4129);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(7,1,1).p("AiRAeQAPgsAwgXQAsgWAxACQAwACAoAeQApAeAGA1");
	this.shape_5.setTransform(61.925,6.2915);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(7,1,1).p("AiRAkQAMgxAxgbQAtgZA0ACQA0ABAoAiQAoAhABA7");
	this.shape_6.setTransform(61.925,5.6708);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(7,1,1).p("AiRAoQAKg0AxgdQAugbA2ABQA3ACAoAkQAnAjgCA+");
	this.shape_7.setTransform(61.9324,5.2961);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(7,1,1).p("AiRAqQAIg2AygfQAugcA3ACQA5ABAnAlQAoAkgFBB");
	this.shape_8.setTransform(61.9528,5.0712);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(7,1,1).p("AiRAsQAIg3AxggQAugdA5ABQA5ACAoAmQAnAlgGBC");
	this.shape_9.setTransform(61.9646,4.8964);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(7,1,1).p("AiRAtQAHg4AyggQAugeA5ABQA6ACAoAmQAnAmgHBD");
	this.shape_10.setTransform(61.9777,4.7964);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(7,1,1).p("AiRAuQAHg5AyghQAugeA6ACQA6ABAnAnQAoAmgIBE");
	this.shape_11.setTransform(61.9848,4.7464);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(7,1,1).p("AiRAuQAHg5AyghQAugeA5ABQA7ACAoAnQAnAlgIBF");
	this.shape_12.setTransform(61.9894,4.6963);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(7,1,1).p("AiRAoQAKg0AxgdQAugbA2ACQA2ABAoAkQAoAigCA/");
	this.shape_13.setTransform(61.9324,5.346);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(7,1,1).p("AiRAUQATglAvgQQAsgQAqACQAqACApAZQApAYAPAs");
	this.shape_14.setTransform(61.925,7.2886);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#959595").ss(7,1,1).p("AiRhAQAAAeAkAsQAiAsA1AJQAzAIAwgcQAvgdAWgy");
	this.shape_15.setTransform(61.925,15.7726);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#C7C7C7").ss(7,1,1).p("AiPhVQgMAsAhA5QAhA6A+AKQA+ALAxgoQAxgpAOhH");
	this.shape_16.setTransform(61.7791,17.9461);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape}]},45).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_12}]},32).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[]},1).wait(15));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_10_Layer_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(7,1,1).p("AiUhHQgDArAWAoQAWAoBHAQQBGAPAwgmQAvgmAVgy");
	this.shape.setTransform(-0.0134,12.0436);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(7,1,1).p("AiVgsQAEAYAbAYQAbAZBBALQBBAMArgVQArgUAZgb");
	this.shape_1.setTransform(0,9.3066);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(7,1,1).p("AiVgSQALAGAfAKQAfAIA8AIQA7AIAngEQAmgFAegE");
	this.shape_2.setTransform(0,6.7446);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(7,1,1).p("AiVgDQARgLAkgGQAkgFA2AFQA1AEAjAMQAiAKAiAS");
	this.shape_3.setTransform(0,5.2734);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(7,1,1).p("AiVAXQAMgjArgVQApgUA5ADQA6ADAkAZQAnAaANAv");
	this.shape_4.setTransform(0,2.5054);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(7,1,1).p("AiVAiQAKgtAugbQArgZA6ADQA7ACAlAeQApAgAFA6");
	this.shape_5.setTransform(0,1.4886);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(7,1,1).p("AiVAnQAJgxAvgeQAtgcA6ACQA8ACAlAhQAqAjABA/");
	this.shape_6.setTransform(0,0.9183);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(7,1,1).p("AiVArQAIg1AxggQAtgdA6ACQA8ABAmAjQAqAlgCBD");
	this.shape_7.setTransform(0.0026,0.5937);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(7,1,1).p("AiVAtQAIg3AxghQAtgfA6ACQA9ACAmAjQAqAngDBF");
	this.shape_8.setTransform(0.0099,0.3689);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(7,1,1).p("AiVAuQAIg4AxghQAuggA6ACQA9ABAmAkQAqAogEBG");
	this.shape_9.setTransform(0.0172,0.2216);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(7,1,1).p("AiVAvQAHg4AygjQAuggA6ACQA9ABAmAlQArAogFBH");
	this.shape_10.setTransform(0.026,0.1216);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(7,1,1).p("AiVAwQAHg5AygjQAuggA7ABQA9ACAmAlQAqAogFBI");
	this.shape_11.setTransform(0.0263,0.0467);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(7,1,1).p("AiVAwQAHg5AygjQAughA7ACQA9ABAmAlQArApgGBI");
	this.shape_12.setTransform(0.0312,0.0217);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(7,1,1).p("AiVAqQAIg0AxggQAtgdA6ACQA8ACAlAiQAqAlgBBC");
	this.shape_13.setTransform(0.0012,0.6187);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(7,1,1).p("AiVAZQALglAsgWQAqgUA4ADQA6ADAlAaQAnAaAMAx");
	this.shape_14.setTransform(0,2.3995);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#959595").ss(7,1,1).p("AiVgzQADAdAZAcQAZAeBDAMQBDANAsgaQAsgZAYgh");
	this.shape_15.setTransform(0,10.0806);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#C7C7C7").ss(7,1,1).p("AiUhHQgDArAWAoQAWAoBHAQQBGAPAwgmQAvgmAVgy");
	this.shape_16.setTransform(-0.0134,12.0436);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape}]},45).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_12}]},32).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[]},1).wait(15));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_6_Layer_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(7,1,1).p("AHWjPQhFCnhcBfQhdBfhwAiQhvAhiCgMQiBgMjLhB");
	this.shape.setTransform(-1.25,9.5736);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(7,1,1).p("AnRBGQC4BUB/AaQB/AaBvgbQBwgbBhhbQBghaBNij");
	this.shape_1.setTransform(-1.025,7.9002);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(7,1,1).p("AnNARQCmBkB8AnQB9AnBwgWQBxgWBkhWQBjhWBUif");
	this.shape_2.setTransform(-0.8,6.6102);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(7,1,1).p("AnKgeQCWByB7AyQB7AyBwgRQBygRBnhSQBnhSBZic");
	this.shape_3.setTransform(-0.6,5.5407);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(7,1,1).p("AnHhIQCJB/B4A7QB5A8BxgNQBzgMBqhPQBphPBeia");
	this.shape_4.setTransform(-0.45,4.6585);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(7,1,1).p("AnEhsQB9CKB3BDQB3BEBygJQBzgJBshMQBrhMBiiX");
	this.shape_5.setTransform(-0.3,3.9776);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(7,1,1).p("AnCiKQB0CTB1BLQB2BKBygGQBzgGBuhKQBuhKBliV");
	this.shape_6.setTransform(-0.2,3.3974);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(7,1,1).p("AnBiaQBtCaB1BPQB0BQBzgEQBzgEBvhIQBvhJBpiT");
	this.shape_7.setTransform(-0.1,2.2845);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(7,1,1).p("Am/ihQBnCfB1BTQBzBTBzgCQBzgCBxhHQBvhIBqiR");
	this.shape_8.setTransform(-0.05,1.0273);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(7,1,1).p("Am/ilQBlCiB0BVQBzBWBzgCQBzgBBxhGQBwhHBsiR");
	this.shape_9.setTransform(-0.025,0.2763);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(7,1,1).p("AHAh3QhsCRhxBHQhxBGhzABQhzABh0hXQhzhVhkik");
	this.shape_10.setTransform(0,0.0256);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(7,1,1).p("AnDiAQB3CQB2BIQB3BJBxgIQBzgHBthLQBthKBliW");
	this.shape_11.setTransform(-0.225,3.5852);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(7,1,1).p("AnKgYQCYBxB7AwQB7AwBwgRQBygRBnhTQBmhTBYid");
	this.shape_12.setTransform(-0.65,5.6723);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(7,1,1).p("AnPA0QCxBaB+AeQB+AeBwgZQBwgZBihZQBhhYBPii");
	this.shape_13.setTransform(-0.925,7.4432);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(7,1,1).p("AnSBQQC7BQB/AYQCAAYBvgcQBxgcBghcQBfhaBMik");
	this.shape_14.setTransform(-1.05,8.1612);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(7,1,1).p("AnTBlQDCBKCAATQCBASBvgeQBwgfBfhdQBdhcBJim");
	this.shape_15.setTransform(-1.15,8.7625);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(7,1,1).p("AnUB0QDHBFCAAPQCCAPBvggQBwggBdhfQBdhdBHin");
	this.shape_16.setTransform(-1.2,9.2011);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(7,1,1).p("AnVB9QDKBCCBANQCCAMBvggQBvgiBehfQBcheBGin");
	this.shape_17.setTransform(-1.225,9.4868);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},44).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_10}]},40).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape}]},1).wait(6));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol9_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#000000").s().p("AgpArQgRgRAAgaQAAgZARgRQARgTAYABQAYgBASATQARARAAAZQAAAagRARQgSATgYgBQgYABgRgTg");

	this.timeline.addTween(cjs.Tween.get(this.shape_3).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol9_1, new cjs.Rectangle(-5.9,-6.1,11.9,12.3), null);


(lib.Symbol8 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.instance = new lib.Symbol9();
	this.instance.parent = this;
	this.instance.setTransform(-5.15,38.35);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(7,1,1).p("AiZlFQB1AEBRBOQBMBJAYBvQAXBvgnBmQgqBvhnA9");
	this.shape.setTransform(0.0168,0);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol8, new cjs.Rectangle(-18.8,-36.1,37.7,84.80000000000001), null);


(lib.Symbol_10_Symbol_9_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Symbol_9
	this.instance = new lib.Symbol9_1();
	this.instance.parent = this;
	this.instance.setTransform(58.3,7.75);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(4).to({x:53.8,y:12.25},6,cjs.Ease.get(1)).wait(26).to({x:62.8,y:7.75},6,cjs.Ease.get(1)).to({_off:true},3).wait(49).to({_off:false,x:53.8,y:12.25},0).wait(3).to({x:58.3,y:7.75},6).wait(7));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_10_Symbol_9 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Symbol_9
	this.instance = new lib.Symbol9_1();
	this.instance.parent = this;
	this.instance.setTransform(-3.7,2.85);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(4).to({x:-8.2,y:7.35},6,cjs.Ease.get(1)).wait(26).to({x:0.8,y:2.85},6,cjs.Ease.get(1)).to({_off:true},3).wait(49).to({_off:false,x:-8.2,y:7.35},0).wait(3).to({x:-3.7,y:2.85},6).wait(7));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_10_Layer_5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_5
	this.instance = new lib.Symbol5();
	this.instance.parent = this;
	this.instance.setTransform(109.7,-7.05,0.395,0.3957,0,-1.5027,-4.9756,118.9,-25.1);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(45).to({regX:118.5,regY:-25.4,scaleY:0.395,skewX:0,skewY:0,x:110.75,y:-11.05},9,cjs.Ease.get(1)).wait(34).to({regX:118.9,regY:-25.1,scaleY:0.3957,skewX:-1.5027,skewY:-4.9756,x:109.7,y:-7.05},9,cjs.Ease.get(1)).wait(13));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_10_Layer_4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_4
	this.instance = new lib.Symbol11();
	this.instance.parent = this;
	this.instance.setTransform(42.1,51.85,0.5164,0.5565,0,0,-12.4469,-5.4,-1.7);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(50).to({_off:false},0).wait(1).to({regX:0,regY:0,scaleX:0.729,scaleY:0.7515,skewY:-6.9748,x:46.8079,y:53.0932},0).wait(1).to({scaleX:0.836,scaleY:0.8496,skewY:-4.2202,x:47.8172,y:53.626},0).wait(1).to({scaleX:0.8966,scaleY:0.9052,skewY:-2.6603,x:48.3862,y:53.9523},0).wait(1).to({scaleX:0.9339,scaleY:0.9394,skewY:-1.701,x:48.7349,y:54.1619},0).wait(1).to({scaleX:0.958,scaleY:0.9614,skewY:-1.0821,x:48.9592,y:54.3007},0).wait(1).to({scaleX:0.9739,scaleY:0.976,skewY:-0.6727,x:49.1072,y:54.394},0).wait(1).to({scaleX:0.9845,scaleY:0.9857,skewY:-0.4,x:49.2057,y:54.4568},0).wait(1).to({scaleX:0.9915,scaleY:0.9922,skewY:-0.2202,x:49.2706,y:54.4986},0).wait(1).to({scaleX:0.9959,scaleY:0.9963,skewY:-0.105,x:49.3121,y:54.5254},0).wait(1).to({scaleX:0.9986,scaleY:0.9987,skewY:0,x:49.337,y:54.5416},0).wait(1).to({regX:-5.7,regY:-1.8,scaleX:1,scaleY:1,x:43.95,y:52.85},0).wait(24).to({regX:-5.4,regY:-1.7,scaleX:0.5164,scaleY:0.5565,skewY:-12.4469,x:42.1,y:51.85},5,cjs.Ease.get(-1)).to({_off:true},1).wait(19));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_6_Layer_4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_4
	this.instance = new lib.Symbol2();
	this.instance.parent = this;
	this.instance.setTransform(-63.45,-31.4,0.395,0.395,-14.9989,0,0,0.2,0.2);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(44).to({regX:0.3,regY:0.4,rotation:0,x:-48.3,y:-73},10,cjs.Ease.get(1)).wait(40).to({regX:0.2,regY:0.2,rotation:-14.9989,x:-63.45,y:-31.4},10,cjs.Ease.get(1)).wait(6));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol_6_Layer_2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.instance = new lib.Symbol7();
	this.instance.parent = this;
	this.instance.setTransform(-56,18.6,1,1,-14.9992);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(44).to({rotation:0,x:-54.1,y:-22.75},10,cjs.Ease.get(1)).wait(40).to({rotation:-14.9992,x:-56,y:18.6},10,cjs.Ease.get(1)).wait(6));

}).prototype = p = new cjs.MovieClip();


(lib.Scene_1_Layer_12 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_12
	this.instance = new lib.Symbol13();
	this.instance.parent = this;
	this.instance.setTransform(34.6,375.3,0.6279,0.6279,0,0,0,-0.1,0.1);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(110));

}).prototype = p = new cjs.MovieClip();


(lib.Scene_1_Layer_8 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_8
	this.instance = new lib.Symbol8();
	this.instance.parent = this;
	this.instance.setTransform(251.3,378.1,0.8793,0.8793);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(110));

}).prototype = p = new cjs.MovieClip();


(lib.Symbol10 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_109 = function() {
		this.___loopingOver___ = true;
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(109).call(this.frame_109).wait(1));

	// Layer_5_obj_
	this.Layer_5 = new lib.Symbol_10_Layer_5();
	this.Layer_5.name = "Layer_5";
	this.Layer_5.parent = this;
	this.Layer_5.setTransform(63.1,7,1,1,0,0,0,63.1,7);
	this.Layer_5.depth = 0;
	this.Layer_5.isAttachedToCamera = 0
	this.Layer_5.isAttachedToMask = 0
	this.Layer_5.layerDepth = 0
	this.Layer_5.layerIndex = 0
	this.Layer_5.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_5).wait(110));

	// Layer_2_obj_
	this.Layer_2 = new lib.Symbol_10_Layer_2();
	this.Layer_2.name = "Layer_2";
	this.Layer_2.parent = this;
	this.Layer_2.depth = 0;
	this.Layer_2.isAttachedToCamera = 0
	this.Layer_2.isAttachedToMask = 0
	this.Layer_2.layerDepth = 0
	this.Layer_2.layerIndex = 1
	this.Layer_2.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_2).wait(110));

	// Layer_1_obj_
	this.Layer_1 = new lib.Symbol_10_Layer_1();
	this.Layer_1.name = "Layer_1";
	this.Layer_1.parent = this;
	this.Layer_1.depth = 0;
	this.Layer_1.isAttachedToCamera = 0
	this.Layer_1.isAttachedToMask = 0
	this.Layer_1.layerDepth = 0
	this.Layer_1.layerIndex = 2
	this.Layer_1.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_1).wait(110));

	// Layer_6_obj_
	this.Layer_6 = new lib.Symbol_10_Layer_6();
	this.Layer_6.name = "Layer_6";
	this.Layer_6.parent = this;
	this.Layer_6.depth = 0;
	this.Layer_6.isAttachedToCamera = 0
	this.Layer_6.isAttachedToMask = 0
	this.Layer_6.layerDepth = 0
	this.Layer_6.layerIndex = 3
	this.Layer_6.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_6).wait(110));

	// Symbol_9_obj_
	this.Symbol_9 = new lib.Symbol_10_Symbol_9();
	this.Symbol_9.name = "Symbol_9";
	this.Symbol_9.parent = this;
	this.Symbol_9.setTransform(-3.7,2.9,1,1,0,0,0,-3.7,2.9);
	this.Symbol_9.depth = 0;
	this.Symbol_9.isAttachedToCamera = 0
	this.Symbol_9.isAttachedToMask = 0
	this.Symbol_9.layerDepth = 0
	this.Symbol_9.layerIndex = 4
	this.Symbol_9.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Symbol_9).wait(110));

	// Layer_8_obj_
	this.Layer_8 = new lib.Symbol_10_Layer_8();
	this.Layer_8.name = "Layer_8";
	this.Layer_8.parent = this;
	this.Layer_8.setTransform(0.1,6.8,1,1,0,0,0,0.1,6.8);
	this.Layer_8.depth = 0;
	this.Layer_8.isAttachedToCamera = 0
	this.Layer_8.isAttachedToMask = 0
	this.Layer_8.layerDepth = 0
	this.Layer_8.layerIndex = 5
	this.Layer_8.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_8).wait(110));

	// Layer_9_obj_
	this.Layer_9 = new lib.Symbol_10_Layer_9();
	this.Layer_9.name = "Layer_9";
	this.Layer_9.parent = this;
	this.Layer_9.depth = 0;
	this.Layer_9.isAttachedToCamera = 0
	this.Layer_9.isAttachedToMask = 0
	this.Layer_9.layerDepth = 0
	this.Layer_9.layerIndex = 6
	this.Layer_9.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_9).wait(110));

	// Symbol_9_obj_
	this.Symbol_9_1 = new lib.Symbol_10_Symbol_9_1();
	this.Symbol_9_1.name = "Symbol_9_1";
	this.Symbol_9_1.parent = this;
	this.Symbol_9_1.setTransform(58.3,7.8,1,1,0,0,0,58.3,7.8);
	this.Symbol_9_1.depth = 0;
	this.Symbol_9_1.isAttachedToCamera = 0
	this.Symbol_9_1.isAttachedToMask = 0
	this.Symbol_9_1.layerDepth = 0
	this.Symbol_9_1.layerIndex = 7
	this.Symbol_9_1.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Symbol_9_1).wait(110));

	// Layer_11_obj_
	this.Layer_11 = new lib.Symbol_10_Layer_11();
	this.Layer_11.name = "Layer_11";
	this.Layer_11.parent = this;
	this.Layer_11.setTransform(62.1,11.7,1,1,0,0,0,62.1,11.7);
	this.Layer_11.depth = 0;
	this.Layer_11.isAttachedToCamera = 0
	this.Layer_11.isAttachedToMask = 0
	this.Layer_11.layerDepth = 0
	this.Layer_11.layerIndex = 8
	this.Layer_11.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_11).wait(110));

	// Layer_3_obj_
	this.Layer_3 = new lib.Symbol_10_Layer_3();
	this.Layer_3.name = "Layer_3";
	this.Layer_3.parent = this;
	this.Layer_3.setTransform(29.4,57.7,1,1,0,0,0,29.4,57.7);
	this.Layer_3.depth = 0;
	this.Layer_3.isAttachedToCamera = 0
	this.Layer_3.isAttachedToMask = 0
	this.Layer_3.layerDepth = 0
	this.Layer_3.layerIndex = 9
	this.Layer_3.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_3).wait(110));

	// Layer_4_obj_
	this.Layer_4 = new lib.Symbol_10_Layer_4();
	this.Layer_4.name = "Layer_4";
	this.Layer_4.parent = this;
	this.Layer_4.depth = 0;
	this.Layer_4.isAttachedToCamera = 0
	this.Layer_4.isAttachedToMask = 0
	this.Layer_4.layerDepth = 0
	this.Layer_4.layerIndex = 10
	this.Layer_4.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_4).wait(51).to({regX:48.7,regY:54,x:48.7,y:54},0).wait(10).to({regX:0,regY:0,x:0,y:0},0).wait(49));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-27.4,-34.4,181.4,96.5);


(lib.Symbol6 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_109 = function() {
		this.___loopingOver___ = true;
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(109).call(this.frame_109).wait(1));

	// Layer_2_obj_
	this.Layer_2 = new lib.Symbol_6_Layer_2();
	this.Layer_2.name = "Layer_2";
	this.Layer_2.parent = this;
	this.Layer_2.setTransform(-52,10.9,1,1,0,0,0,-52,10.9);
	this.Layer_2.depth = 0;
	this.Layer_2.isAttachedToCamera = 0
	this.Layer_2.isAttachedToMask = 0
	this.Layer_2.layerDepth = 0
	this.Layer_2.layerIndex = 0
	this.Layer_2.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_2).wait(110));

	// Layer_4_obj_
	this.Layer_4 = new lib.Symbol_6_Layer_4();
	this.Layer_4.name = "Layer_4";
	this.Layer_4.parent = this;
	this.Layer_4.setTransform(-63.6,-31.5,1,1,0,0,0,-63.6,-31.5);
	this.Layer_4.depth = 0;
	this.Layer_4.isAttachedToCamera = 0
	this.Layer_4.isAttachedToMask = 0
	this.Layer_4.layerDepth = 0
	this.Layer_4.layerIndex = 1
	this.Layer_4.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_4).wait(110));

	// Layer_1_obj_
	this.Layer_1 = new lib.Symbol_6_Layer_1();
	this.Layer_1.name = "Layer_1";
	this.Layer_1.parent = this;
	this.Layer_1.setTransform(-1.3,9.6,1,1,0,0,0,-1.3,9.6);
	this.Layer_1.depth = 0;
	this.Layer_1.isAttachedToCamera = 0
	this.Layer_1.isAttachedToMask = 0
	this.Layer_1.layerDepth = 0
	this.Layer_1.layerIndex = 2
	this.Layer_1.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_1).wait(110));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-104.8,-169.1,220.5,203);


(lib.Scene_1_Layer_7 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_7
	this.instance = new lib.Symbol6("synched",0,false);
	this.instance.parent = this;
	this.instance.setTransform(94.2,374.55,0.8793,0.8793,0,0,0,0.1,0.1);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(110));

}).prototype = p = new cjs.MovieClip();


(lib.Scene_1_Layer_4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_4
	this.instance = new lib.Symbol10("synched",0,false);
	this.instance.parent = this;
	this.instance.setTransform(141.8,292.8,0.8793,0.8793);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(110));

}).prototype = p = new cjs.MovieClip();


// stage content:
(lib.MasterKV_Vert = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	this.___GetDepth___ = function(obj) {
		var depth = obj.depth;
		var cameraObj = this.___camera___instance;
		if(cameraObj && cameraObj.depth && obj.isAttachedToCamera)
		{
			depth += depth + cameraObj.depth;
		}
		return depth;
		}
	this.___needSorting___ = function() {
		for (var i = 0; i < this.getNumChildren() - 1; i++)
		{
			var prevDepth = this.___GetDepth___(this.getChildAt(i));
			var nextDepth = this.___GetDepth___(this.getChildAt(i + 1));
			if (prevDepth < nextDepth)
				return true;
		}
		return false;
	}
	this.___sortFunction___ = function(obj1, obj2) {
		return (this.exportRoot.___GetDepth___(obj2) - this.exportRoot.___GetDepth___(obj1));
	}
	this.on('tick', function (event){
		var curTimeline = event.currentTarget;
		if (curTimeline.___needSorting___()){
			this.sortChildren(curTimeline.___sortFunction___);
		}
	});

	// timeline functions:
	this.frame_109 = function() {
		this.___loopingOver___ = true;
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(109).call(this.frame_109).wait(1));

	// Layer_8_obj_
	this.Layer_8 = new lib.Scene_1_Layer_8();
	this.Layer_8.name = "Layer_8";
	this.Layer_8.parent = this;
	this.Layer_8.setTransform(251.3,383.6,1,1,0,0,0,251.3,383.6);
	this.Layer_8.depth = 0;
	this.Layer_8.isAttachedToCamera = 0
	this.Layer_8.isAttachedToMask = 0
	this.Layer_8.layerDepth = 0
	this.Layer_8.layerIndex = 0
	this.Layer_8.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_8).wait(110));

	// Layer_10 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("EggKAaIMAAAgthIY2EnMAnfgLVMgHbAg7IjuggIsdggIhuBBQg0JeAYJ1g");
	mask.setTransform(76.775,320.125);

	// Layer_7_obj_
	this.Layer_7 = new lib.Scene_1_Layer_7();
	this.Layer_7.name = "Layer_7";
	this.Layer_7.parent = this;
	this.Layer_7.setTransform(69.7,349,1,1,0,0,0,69.7,349);
	this.Layer_7.depth = 0;
	this.Layer_7.isAttachedToCamera = 0
	this.Layer_7.isAttachedToMask = 0
	this.Layer_7.layerDepth = 0
	this.Layer_7.layerIndex = 1
	this.Layer_7.maskLayerName = 0

	var maskedShapeInstanceList = [this.Layer_7];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.Layer_7).wait(110));

	// Layer_4_obj_
	this.Layer_4 = new lib.Scene_1_Layer_4();
	this.Layer_4.name = "Layer_4";
	this.Layer_4.parent = this;
	this.Layer_4.setTransform(197.3,304.9,1,1,0,0,0,197.3,304.9);
	this.Layer_4.depth = 0;
	this.Layer_4.isAttachedToCamera = 0
	this.Layer_4.isAttachedToMask = 0
	this.Layer_4.layerDepth = 0
	this.Layer_4.layerIndex = 2
	this.Layer_4.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_4).wait(110));

	// Layer_12_obj_
	this.Layer_12 = new lib.Scene_1_Layer_12();
	this.Layer_12.name = "Layer_12";
	this.Layer_12.parent = this;
	this.Layer_12.setTransform(34.6,375.3,1,1,0,0,0,34.6,375.3);
	this.Layer_12.depth = 0;
	this.Layer_12.isAttachedToCamera = 0
	this.Layer_12.isAttachedToMask = 0
	this.Layer_12.layerDepth = 0
	this.Layer_12.layerIndex = 3
	this.Layer_12.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_12).wait(110));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-413.7,273,1260.6,610.6);
// library properties:
lib.properties = {
	id: '63CAEBC58495FA459684A970EE06FB09',
	width: 375,
	height: 812,
	fps: 30,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: [
		{src:"assets/images/bg1_vertpr2.jpg", id:"bg1_vert"},
		{src:"assets/images/glassespr2.png", id:"glasses"},
		{src:"assets/images/phonepr2.png", id:"phone"}
	],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['63CAEBC58495FA459684A970EE06FB09'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}


// Layer depth API : 

AdobeAn.Layer = new function() {
	this.getLayerZDepth = function(timeline, layerName)
	{
		if(layerName === "Camera")
		layerName = "___camera___instance";
		var script = "if(timeline." + layerName + ") timeline." + layerName + ".depth; else 0;";
		return eval(script);
	}
	this.setLayerZDepth = function(timeline, layerName, zDepth)
	{
		const MAX_zDepth = 10000;
		const MIN_zDepth = -5000;
		if(zDepth > MAX_zDepth)
			zDepth = MAX_zDepth;
		else if(zDepth < MIN_zDepth)
			zDepth = MIN_zDepth;
		if(layerName === "Camera")
		layerName = "___camera___instance";
		var script = "if(timeline." + layerName + ") timeline." + layerName + ".depth = " + zDepth + ";";
		eval(script);
	}
	this.removeLayer = function(timeline, layerName)
	{
		if(layerName === "Camera")
		layerName = "___camera___instance";
		var script = "if(timeline." + layerName + ") timeline.removeChild(timeline." + layerName + ");";
		eval(script);
	}
	this.addNewLayer = function(timeline, layerName, zDepth)
	{
		if(layerName === "Camera")
		layerName = "___camera___instance";
		zDepth = typeof zDepth !== 'undefined' ? zDepth : 0;
		var layer = new createjs.MovieClip();
		layer.name = layerName;
		layer.depth = zDepth;
		layer.layerIndex = 0;
		timeline.addChild(layer);
	}
}


})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;

$(function () {
  var canvas, stage, exportRoot, anim_container, dom_overlay_container, fnStartAnimation;
  function init() {
    canvas = document.getElementById("canvaspr2-2");
    anim_container = document.getElementById("animation_containerpr2-2");
    dom_overlay_container = document.getElementById("dom_overlay_containerpr2-2");
    var comp=AdobeAn.getComposition("63CAEBC58495FA459684A970EE06FB09");
    var lib=comp.getLibrary();
    var loader = new createjs.LoadQueue(false);
    loader.addEventListener("fileload", function(evt){handleFileLoad(evt,comp)});
    loader.addEventListener("complete", function(evt){handleComplete(evt,comp)});
    var lib=comp.getLibrary();
    loader.loadManifest(lib.properties.manifest);
  }
  function handleFileLoad(evt, comp) {
    var images=comp.getImages();
    if (evt && (evt.item.type == "image")) { images[evt.item.id] = evt.result; }
  }
  function handleComplete(evt,comp) {
    //This function is always called, irrespective of the content. You can use the variable "stage" after it is created in token create_stage.
    var lib=comp.getLibrary();
    var ss=comp.getSpriteSheet();
    var queue = evt.target;
    var ssMetadata = lib.ssMetadata;
    for(i=0; i<ssMetadata.length; i++) {
      ss[ssMetadata[i].name] = new createjs.SpriteSheet( {"images": [queue.getResult(ssMetadata[i].name)], "frames": ssMetadata[i].frames} )
    }
    var preloaderDiv = document.getElementById("_preload_div_pr");
    preloaderDiv.style.display = 'none';
    canvas.style.display = 'block';
    exportRoot = new lib.MasterKV_Vert();
    stage = new lib.Stage(canvas);
    //Registers the "tick" event listener.
    fnStartAnimation = function() {
      stage.addChild(exportRoot);
      createjs.Ticker.setFPS(lib.properties.fps);
      createjs.Ticker.addEventListener("tick", stage)
      stage.addEventListener("tick", handleTick)
      function getProjectionMatrix(container, totalDepth) {
        var focalLength = 528.25;
        var projectionCenter = { x : lib.properties.width/2, y : lib.properties.height/2 };
        var scale = (totalDepth + focalLength)/focalLength;
        var scaleMat = new createjs.Matrix2D;
        scaleMat.a = 1/scale;
        scaleMat.d = 1/scale;
        var projMat = new createjs.Matrix2D;
        projMat.tx = -projectionCenter.x;
        projMat.ty = -projectionCenter.y;
        projMat = projMat.prependMatrix(scaleMat);
        projMat.tx += projectionCenter.x;
        projMat.ty += projectionCenter.y;
        return projMat;
      }
      function handleTick(event) {
        var cameraInstance = exportRoot.___camera___instance;
        if(cameraInstance !== undefined && cameraInstance.pinToObject !== undefined)
        {
          cameraInstance.x = cameraInstance.pinToObject.x + cameraInstance.pinToObject.pinOffsetX;
          cameraInstance.y = cameraInstance.pinToObject.y + cameraInstance.pinToObject.pinOffsetY;
          if(cameraInstance.pinToObject.parent !== undefined && cameraInstance.pinToObject.parent.depth !== undefined)
            cameraInstance.depth = cameraInstance.pinToObject.parent.depth + cameraInstance.pinToObject.pinOffsetZ;
        }
        applyLayerZDepth(exportRoot);
      }
      function applyLayerZDepth(parent)
      {
        var cameraInstance = parent.___camera___instance;
        var focalLength = 528.25;
        var projectionCenter = { 'x' : 0, 'y' : 0};
        if(parent === exportRoot)
        {
          var stageCenter = { 'x' : lib.properties.width/2, 'y' : lib.properties.height/2 };
          projectionCenter.x = stageCenter.x;
          projectionCenter.y = stageCenter.y;
        }
        for(child in parent.children)
        {
          var layerObj = parent.children[child];
          if(layerObj == cameraInstance)
            continue;
          applyLayerZDepth(layerObj, cameraInstance);
          if(layerObj.layerDepth === undefined)
            continue;
          if(layerObj.currentFrame != layerObj.parent.currentFrame)
          {
            layerObj.gotoAndPlay(layerObj.parent.currentFrame);
          }
          var matToApply = new createjs.Matrix2D;
          var cameraMat = new createjs.Matrix2D;
          var totalDepth = layerObj.layerDepth ? layerObj.layerDepth : 0;
          var cameraDepth = 0;
          if(cameraInstance && !layerObj.isAttachedToCamera)
          {
            var mat = cameraInstance.getMatrix();
            mat.tx -= projectionCenter.x;
            mat.ty -= projectionCenter.y;
            cameraMat = mat.invert();
            cameraMat.prependTransform(projectionCenter.x, projectionCenter.y, 1, 1, 0, 0, 0, 0, 0);
            cameraMat.appendTransform(-projectionCenter.x, -projectionCenter.y, 1, 1, 0, 0, 0, 0, 0);
            if(cameraInstance.depth)
              cameraDepth = cameraInstance.depth;
          }
          if(layerObj.depth)
          {
            totalDepth = layerObj.depth;
          }
          //Offset by camera depth
          totalDepth -= cameraDepth;
          if(totalDepth < -focalLength)
          {
            matToApply.a = 0;
            matToApply.d = 0;
          }
          else
          {
            if(layerObj.layerDepth)
            {
              var sizeLockedMat = getProjectionMatrix(parent, layerObj.layerDepth);
              if(sizeLockedMat)
              {
                sizeLockedMat.invert();
                matToApply.prependMatrix(sizeLockedMat);
              }
            }
            matToApply.prependMatrix(cameraMat);
            var projMat = getProjectionMatrix(parent, totalDepth);
            if(projMat)
            {
              matToApply.prependMatrix(projMat);
            }
          }
          layerObj.transformMatrix = matToApply;
        }
      }
    }
    //Code to support hidpi screens and responsive scaling.
    function makeResponsive(isResp, respDim, isScale, scaleType) {
      var lastW, lastH, lastS=1;
      window.addEventListener('resize', resizeCanvas);
      resizeCanvas();
      function resizeCanvas() {
        var w = lib.properties.width, h = lib.properties.height;
        var iw = window.innerWidth, ih=window.innerHeight;
        var pRatio = window.devicePixelRatio || 1, xRatio=iw/w, yRatio=ih/h, sRatio=1;
        if(isResp) {
          if((respDim=='width'&&lastW==iw) || (respDim=='height'&&lastH==ih)) {
            sRatio = lastS;
          }
          else if(!isScale) {
            if(iw<w || ih<h)
              sRatio = Math.min(xRatio, yRatio);
          }
          else if(scaleType==1) {
            sRatio = Math.min(xRatio, yRatio);
          }
          else if(scaleType==2) {
            sRatio = Math.max(xRatio, yRatio);
          }
        }
        canvas.width = w*pRatio*sRatio;
        canvas.height = h*pRatio*sRatio;
        canvas.style.width = anim_container.style.width = dom_overlay_container.style.width = preloaderDiv.style.width = w*sRatio+'px';
        canvas.style.height = anim_container.style.height = dom_overlay_container.style.height = preloaderDiv.style.height = h*sRatio+'px';
        stage.scaleX = pRatio*sRatio;
        stage.scaleY = pRatio*sRatio;
        lastW = iw; lastH = ih; lastS = sRatio;
        stage.tickOnUpdate = false;
        stage.update();
        stage.tickOnUpdate = true;
      }
    }
    makeResponsive(true,'both',true,2);
    AdobeAn.compositionLoaded(lib.properties.id);
    fnStartAnimation();
  }
  init();
});