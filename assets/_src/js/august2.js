(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [];


// symbols:



(lib.bg = function() {
	this.initialize(img.bg);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,1080,680);


(lib.calendar = function() {
	this.initialize(img.calendar);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,371,425);


(lib.pack = function() {
	this.initialize(img.pack);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,281,371);


(lib.pencil = function() {
	this.initialize(img.pencil);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,40,41);// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.Symbol8 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(10,1,1).p("ABjA/QhPjhh2Cy");
	this.shape.setTransform(9.9,6.3);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol8, new cjs.Rectangle(-5,-5,29.8,22.6), null);


(lib.Symbol7 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.pencil();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol7, new cjs.Rectangle(0,0,40,41), null);


(lib.Symbol6 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AgZAaQgLgLAAgPQAAgOALgMQALgLAOAAQAPAAALALQALAMAAAOQAAAPgLALQgLALgPAAQgOAAgLgLg");
	this.shape.setTransform(3.7,3.8);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol6, new cjs.Rectangle(0,0,7.5,7.5), null);


(lib.Symbol5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AgZAaQgLgLAAgPQAAgOALgMQALgLAOAAQAPAAALALQALAMAAAOQAAAPgLALQgLALgPAAQgOAAgLgLg");
	this.shape.setTransform(3.7,3.8);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol5, new cjs.Rectangle(0,0,7.5,7.5), null);


(lib.Symbol5_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Symbol 2
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(9,1,1).p("ABMBKQjNAHBKia");
	this.shape_1.setTransform(7.6,10.4);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(9,1,1).p("AgqhKQhaCFDKAQ");
	this.shape_2.setTransform(8,10.7);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(9,1,1).p("AgdhLQhqBzDJAk");
	this.shape_3.setTransform(8.3,10.9);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(9,1,1).p("AgShMQh2BjDHA2");
	this.shape_4.setTransform(8.5,11.1);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(9,1,1).p("AgIhNQiBBUDFBH");
	this.shape_5.setTransform(8.7,11.3);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(9,1,1).p("AAAhOQiLBKDFBT");
	this.shape_6.setTransform(8.9,11.4);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(9,1,1).p("AAHhPQiSBBDDBe");
	this.shape_7.setTransform(8.9,11.5);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(9,1,1).p("AAMhPQiYA4DDBn");
	this.shape_8.setTransform(9,11.6);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(9,1,1).p("AAQhPQicAzDCBs");
	this.shape_9.setTransform(9.1,11.7);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(9,1,1).p("AAShQQieAwDBBx");
	this.shape_10.setTransform(9.1,11.7);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(9,1,1).p("AA1BRQjBhyCfgv");
	this.shape_11.setTransform(9.1,11.7);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(9,1,1).p("AAEhOQiPBDDEBa");
	this.shape_12.setTransform(8.9,11.5);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(9,1,1).p("AgThMQh1BlDHA0");
	this.shape_13.setTransform(8.5,11.1);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(9,1,1).p("AglhLQhgCADKAX");
	this.shape_14.setTransform(8.1,10.8);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(9,1,1).p("AgshKQhYCJDLAM");
	this.shape_15.setTransform(7.9,10.6);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(9,1,1).p("AgxhKQhSCRDMAE");
	this.shape_16.setTransform(7.8,10.6);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(9,1,1).p("Ag0hJQhOCWDMgD");
	this.shape_17.setTransform(7.7,10.5);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#000000").ss(9,1,1).p("Ag2hJQhMCZDNgG");
	this.shape_18.setTransform(7.6,10.4);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1}]}).to({state:[{t:this.shape_1}]},4).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_11}]},16).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_1}]},1).wait(35));

	// Symbol 3
	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#000000").ss(9,1,1).p("AAtBgQiUhnBghY");
	this.shape_19.setTransform(12.3,9.6);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#000000").ss(9,1,1).p("AAAhaQhoBPCUBm");
	this.shape_20.setTransform(12.5,10);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#000000").ss(9,1,1).p("AAHhVQhxBFCUBm");
	this.shape_21.setTransform(12.6,10.5);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#000000").ss(9,1,1).p("AAOhSQh5A+CUBn");
	this.shape_22.setTransform(12.7,10.9);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#000000").ss(9,1,1).p("AAThOQh/A3CUBm");
	this.shape_23.setTransform(12.8,11.2);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#000000").ss(9,1,1).p("AAYhLQiFAxCUBm");
	this.shape_24.setTransform(12.9,11.5);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f().s("#000000").ss(9,1,1).p("AAchJQiJAtCUBm");
	this.shape_25.setTransform(13,11.7);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f().s("#000000").ss(9,1,1).p("AAfhHQiNApCUBm");
	this.shape_26.setTransform(13,11.9);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f().s("#000000").ss(9,1,1).p("AAhhGQiPAnCUBm");
	this.shape_27.setTransform(13.1,12);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f().s("#000000").ss(9,1,1).p("AAihFQiQAlCUBm");
	this.shape_28.setTransform(13.1,12.1);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f().s("#000000").ss(9,1,1).p("AAmBGQiUhmCRgl");
	this.shape_29.setTransform(13.1,12.1);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f().s("#000000").ss(9,1,1).p("AAahKQiHAvCUBm");
	this.shape_30.setTransform(13,11.6);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f().s("#000000").ss(9,1,1).p("AAMhSQh3A/CUBm");
	this.shape_31.setTransform(12.7,10.8);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f().s("#000000").ss(9,1,1).p("AAChYQhrBLCUBm");
	this.shape_32.setTransform(12.5,10.2);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f().s("#000000").ss(9,1,1).p("AgBhbQhnBQCUBn");
	this.shape_33.setTransform(12.5,10);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f().s("#000000").ss(9,1,1).p("AgDhcQhkBTCUBm");
	this.shape_34.setTransform(12.4,9.8);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f().s("#000000").ss(9,1,1).p("AgFheQhiBXCUBm");
	this.shape_35.setTransform(12.3,9.7);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f().s("#000000").ss(9,1,1).p("AgHheQhgBXCUBm");
	this.shape_36.setTransform(12.3,9.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_19}]}).to({state:[{t:this.shape_19}]},4).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_27}]},1).to({state:[{t:this.shape_28}]},1).to({state:[{t:this.shape_29}]},1).to({state:[{t:this.shape_29}]},16).to({state:[{t:this.shape_30}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_31}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_32}]},1).to({state:[{t:this.shape_33}]},1).to({state:[{t:this.shape_34}]},1).to({state:[{t:this.shape_35}]},1).to({state:[{t:this.shape_36}]},1).to({state:[{t:this.shape_19}]},1).wait(35));

	// Symbol 4
	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f().s("#000000").ss(9,1,1).p("AgZBAQBWhXg8go");
	this.shape_37.setTransform(19,14.8);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f().s("#000000").ss(9,1,1).p("AgYA+QBWhWg/gl");
	this.shape_38.setTransform(19,14.9);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f().s("#000000").ss(9,1,1).p("AgYA9QBWhWhCgj");
	this.shape_39.setTransform(18.9,15);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f().s("#000000").ss(9,1,1).p("AgXA9QBWhXhFgi");
	this.shape_40.setTransform(18.9,15.1);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f().s("#000000").ss(9,1,1).p("AgXA8QBWhXhGgg");
	this.shape_41.setTransform(18.8,15.1);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f().s("#000000").ss(9,1,1).p("AgXA7QBWhXhIge");
	this.shape_42.setTransform(18.8,15.2);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f().s("#000000").ss(9,1,1).p("AgWA7QBWhXhKge");
	this.shape_43.setTransform(18.8,15.3);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f().s("#000000").ss(9,1,1).p("AgWA6QBWhXhMgc");
	this.shape_44.setTransform(18.8,15.3);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f().s("#000000").ss(9,1,1).p("AgWA6QBWhXhNgc");
	this.shape_45.setTransform(18.8,15.3);
	this.shape_45._off = true;

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.f().s("#000000").ss(9,1,1).p("AgXA9QBWhXhEgi");
	this.shape_46.setTransform(18.9,15.1);

	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.f().s("#000000").ss(9,1,1).p("AgYA+QBWhXhAgk");
	this.shape_47.setTransform(18.9,14.9);

	this.shape_48 = new cjs.Shape();
	this.shape_48.graphics.f().s("#000000").ss(9,1,1).p("AgYA/QBWhXg/gm");
	this.shape_48.setTransform(19,14.9);

	this.shape_49 = new cjs.Shape();
	this.shape_49.graphics.f().s("#000000").ss(9,1,1).p("AgYA/QBWhXg+gm");
	this.shape_49.setTransform(19,14.8);

	this.shape_50 = new cjs.Shape();
	this.shape_50.graphics.f().s("#000000").ss(9,1,1).p("AgZA/QBWhWg9gn");
	this.shape_50.setTransform(19,14.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_37}]}).to({state:[{t:this.shape_37}]},4).to({state:[{t:this.shape_38}]},1).to({state:[{t:this.shape_39}]},1).to({state:[{t:this.shape_40}]},1).to({state:[{t:this.shape_41}]},1).to({state:[{t:this.shape_42}]},1).to({state:[{t:this.shape_43,p:{y:15.3}}]},1).to({state:[{t:this.shape_44}]},1).to({state:[{t:this.shape_45}]},1).to({state:[{t:this.shape_45}]},1).to({state:[{t:this.shape_45}]},1).to({state:[{t:this.shape_45}]},16).to({state:[{t:this.shape_43,p:{y:15.2}}]},1).to({state:[{t:this.shape_41}]},1).to({state:[{t:this.shape_46}]},1).to({state:[{t:this.shape_39}]},1).to({state:[{t:this.shape_47}]},1).to({state:[{t:this.shape_48}]},1).to({state:[{t:this.shape_49}]},1).to({state:[{t:this.shape_50}]},1).to({state:[{t:this.shape_37}]},1).to({state:[{t:this.shape_37}]},1).wait(35));
	this.timeline.addTween(cjs.Tween.get(this.shape_45).wait(12).to({_off:false},0).wait(1).to({x:18.7,y:15.4},0).wait(17).to({_off:true},1).wait(44));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-4.5,-4.5,30.6,30.1);


(lib.Symbol12 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Symbol 5
	this.instance = new lib.Symbol5_1();
	this.instance.parent = this;
	this.instance.setTransform(15.6,44.9,1,1,0,0,0,15.6,22.9);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(4).to({regY:22.8,rotation:30,x:10.6,y:17.4},10,cjs.Ease.get(1)).wait(16).to({regY:22.9,rotation:0,x:15.6,y:44.9},10,cjs.Ease.get(1)).wait(35));

	// Layer 3
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(9,1,1).p("ADmmEQAWEdg6DRQg0C7hfBEQhbBBhQhSQhWhbgZjk");
	this.shape.setTransform(38.8,38.9);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(9,1,1).p("AjrgBQAKDhBLBfQBEBZBdg4QBfg4A/i2QBHjMgEkb");
	this.shape_1.setTransform(38,37);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(9,1,1).p("AjugZQgCDfA/BjQA7BeBeguQBfgvBJiyQBTjGAMkZ");
	this.shape_2.setTransform(37.5,35.3);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(9,1,1).p("AjwguQgNDdA2BmQAzBjBegmQBfgnBSitQBdjBAbkZ");
	this.shape_3.setTransform(36.9,33.9);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(9,1,1).p("AjvhBQgXDcAuBpQArBnBfgfQBfggBaipQBmi9AnkY");
	this.shape_4.setTransform(36.3,32.6);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(9,1,1).p("AjuhRQgfDaAmBsQAmBqBfgZQBggaBgimQBti6AykW");
	this.shape_5.setTransform(35.7,31.6);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(9,1,1).p("AjshfQgmDaAhBuQAgBsBggUQBggUBlikQBzi3A7kV");
	this.shape_6.setTransform(35.1,30.8);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(9,1,1).p("AjrhqQgrDZAdBwQAcBvBggRQBggQBqiiQB3i1BCkU");
	this.shape_7.setTransform(34.6,30.2);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(9,1,1).p("AjphxQgvDYAZBxQAZBwBhgNQBggOBsihQB8izBGkU");
	this.shape_8.setTransform(34.2,29.7);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(9,1,1).p("Ajoh2QgyDYAYByQAYBxBggMQBhgMBuigQB9iyBJkU");
	this.shape_9.setTransform(34,29.5);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(9,1,1).p("AELk4QhKEUh+CxQhuCghhALQhgAMgXhyQgXhyAyjX");
	this.shape_10.setTransform(33.9,29.4);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(9,1,1).p("AjthaQgkDZAjBuQAiBrBggVQBggXBjikQBxi4A4kW");
	this.shape_11.setTransform(35.3,31.1);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(9,1,1).p("AjvgrQgMDdA3BmQA0BiBfgnQBegoBRiuQBcjCAZkY");
	this.shape_12.setTransform(37,34.1);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(9,1,1).p("AjsgJQAGDgBHBhQBBBaBeg0QBeg2BCi0QBMjJACkb");
	this.shape_13.setTransform(37.8,36.4);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(9,1,1).p("AjrADQANDiBMBeQBHBYBcg5QBfg7A9i2QBFjNgGkb");
	this.shape_14.setTransform(38.1,37.3);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(9,1,1).p("AjrANQASDjBRBcQBLBVBbg8QBfg/A5i4QBBjPgNkc");
	this.shape_15.setTransform(38.4,38);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(9,1,1).p("AjrAUQAWDjBUBcQBNBTBcg/QBehBA3i6QA9jQgSkd");
	this.shape_16.setTransform(38.6,38.5);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(9,1,1).p("AjrAXQAYDkBWBbQBPBTBbhBQBfhDA0i7QA8jRgVkd");
	this.shape_17.setTransform(38.8,38.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},4).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_10}]},16).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape}]},1).wait(35));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-4.5,-4.5,71.5,86.9);


(lib.Symbol2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 4
	this.instance = new lib.Symbol8();
	this.instance.parent = this;
	this.instance.setTransform(31.2,21.4,1,1,0,0,0,19.8,12.6);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(38).to({regY:12.5,rotation:-9.4,x:28.5,y:32.7},10).to({rotation:20.7,x:56.1,y:20.7},5).to({regY:12.4,scaleX:1,scaleY:1,rotation:33.3,x:50.8,y:16.1},1).to({regY:12.5,scaleX:1,scaleY:1,rotation:36.2,x:24.3,y:6.3},4).to({scaleX:1,scaleY:1,rotation:37.8,x:9.6,y:14.4},2).to({regX:19.7,scaleX:1,scaleY:1,rotation:36.1,x:5.9,y:24.3},1).to({regX:19.8,rotation:9.6,x:8.4,y:26.6},1).to({regX:19.7,scaleX:1,scaleY:1,rotation:8.5,x:10.9,y:28.5},1).to({regX:19.8,scaleX:1,scaleY:1,rotation:-1.4,x:13.3,y:27.4},1).to({regY:12.4,rotation:0.2,x:16.2,y:29.7},1).to({regY:12.5,rotation:-5.1,x:20.8,y:27.5},1).to({regX:19.9,rotation:-17.5,x:23.7,y:24.7},1).to({regX:19.8,scaleX:1,scaleY:1,rotation:-13,x:26.2,y:21.2},1).to({regY:12.6,rotation:0,x:31.2,y:21.4},6,cjs.Ease.get(1)).wait(1));

	// pencil.png
	this.instance_1 = new lib.Symbol7();
	this.instance_1.parent = this;
	this.instance_1.setTransform(20,20.5,1,1,0,0,0,20,20.5);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(38).to({regY:20.4,rotation:-9.4,x:17.3,y:33.6},10).to({regY:20.5,rotation:-19.5,x:23.2,y:30},1).to({regX:20.1,regY:20.4,scaleX:1,scaleY:1,rotation:-24.7,x:29,y:26.3},1).to({rotation:-22.7,x:34.8,y:22.7},1).to({regX:20.2,scaleX:1,scaleY:1,rotation:-15.1,x:40.6,y:19.1},1).to({regX:20.1,rotation:2,x:46.4,y:15.4},1).to({regX:20,rotation:20.2,x:42.4,y:8.8},1).to({regY:20.6,rotation:27.2,x:35.4,y:6.9},1).to({regX:20.1,regY:20.4,rotation:24.1,x:29.4,y:2.6},1).to({scaleX:1,scaleY:1,rotation:23.2,x:22.3,y:1},1).to({regY:20.5,scaleX:1,scaleY:1,rotation:17.5,x:16.4,y:-1.3},1).to({regY:20.4,scaleX:1,scaleY:1,rotation:18.3,x:12.2,y:0.3},1).to({scaleX:1,scaleY:1,rotation:19.2,x:2,y:6.5},1).to({rotation:9,x:-3.1,y:17.1},1).to({regX:20,regY:20.5,rotation:-8,x:-2.3,y:23},1).to({regX:20.1,rotation:-10.2,x:0.4,y:25.5},1).to({regX:20,scaleX:1,scaleY:1,rotation:-20,x:2.4,y:26.2},1).to({regX:20.1,rotation:-19,x:6,y:28.1},1).to({rotation:-28.3,x:10.2,y:24.9},1).to({rotation:-30,x:12.3,y:23.8},1).to({regY:20.4,scaleX:1,scaleY:1,rotation:-31.7,x:15.1,y:22.2},1).to({regX:20,regY:20.5,rotation:0,x:20,y:20.5},6,cjs.Ease.get(1)).wait(1));

	// Layer 3
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(10,1,1).p("AmOBQIMeif");
	this.shape.setTransform(63.8,16);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(10,1,1).p("AmNBWIMbir");
	this.shape_1.setTransform(63.4,16.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(10,1,1).p("AmLBdIMXi5");
	this.shape_2.setTransform(63,17.2);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(10,1,1).p("AmJBjIMTjF");
	this.shape_3.setTransform(62.6,17.8);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(10,1,1).p("AmHBpIMPjS");
	this.shape_4.setTransform(62.2,18.4);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(10,1,1).p("AmFBwIMLjf");
	this.shape_5.setTransform(61.8,19.1);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(10,1,1).p("AmDB3IMHjs");
	this.shape_6.setTransform(61.4,19.7);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(10,1,1).p("AmBB9IMEj5");
	this.shape_7.setTransform(61,20.3);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(10,1,1).p("AmACEIMBkH");
	this.shape_8.setTransform(60.6,20.9);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(10,1,1).p("Al+CKIL9kT");
	this.shape_9.setTransform(60.2,21.5);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(10,1,1).p("Al8CQIL5kf");
	this.shape_10.setTransform(59.8,22.1);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(10,1,1).p("AltB+QGEhHFXi0");
	this.shape_11.setTransform(63.9,20.3);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(10,1,1).p("AleBrQGNACEwjX");
	this.shape_12.setTransform(68.1,18.5);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(10,1,1).p("AlOBPQGUBNEJj8");
	this.shape_13.setTransform(72.2,17.6);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(10,1,1).p("Ak/AsQGcCVDjkf");
	this.shape_14.setTransform(76.4,17.5);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(10,1,1).p("AkwAGQGlDfC8lE");
	this.shape_15.setTransform(80.6,17.6);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(10,1,1).p("AlTgIQHfDTDIkc");
	this.shape_16.setTransform(77,16.2);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(10,1,1).p("Al3gWQIaDHDVjz");
	this.shape_17.setTransform(73.5,14.7);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#000000").ss(10,1,1).p("AmagmQJUC9DhjL");
	this.shape_18.setTransform(70,13.3);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#000000").ss(10,1,1).p("Am9guQKNCyDuij");
	this.shape_19.setTransform(66.5,11.2);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#000000").ss(10,1,1).p("AnggvQLICmD5h6");
	this.shape_20.setTransform(62.9,8.4);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#000000").ss(10,1,1).p("AoBgYQMJBzD6h6");
	this.shape_21.setTransform(59.7,11.2);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#000000").ss(10,1,1).p("AoiARQNLBCD6h6");
	this.shape_22.setTransform(56.4,12);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#000000").ss(10,1,1).p("Ao3BXQN1gzD6h6");
	this.shape_23.setTransform(54.3,16.7);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#000000").ss(10,1,1).p("AosBjQNfhLD6h6");
	this.shape_24.setTransform(55.4,17.9);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f().s("#000000").ss(10,1,1).p("AohBvQNJhjD6h6");
	this.shape_25.setTransform(56.5,19.1);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f().s("#000000").ss(10,1,1).p("AoPBzQMohmD3h/");
	this.shape_26.setTransform(58.4,19.6);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f().s("#000000").ss(10,1,1).p("An8B4QMFhrD1iE");
	this.shape_27.setTransform(60.3,20.2);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f().s("#000000").ss(10,1,1).p("AnpB4QLchuD3iB");
	this.shape_28.setTransform(62.2,20.2);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f().s("#000000").ss(10,1,1).p("AndBkQLDhKD4h9");
	this.shape_29.setTransform(63.3,18.1);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f().s("#000000").ss(10,1,1).p("AnSBQQKrglD5h6");
	this.shape_30.setTransform(64.4,16);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f().s("#000000").ss(10,1,1).p("Am9BQQJUgyEnht");
	this.shape_31.setTransform(64.2,16);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f().s("#000000").ss(10,1,1).p("AmsBQQIMg9FNhi");
	this.shape_32.setTransform(64.1,16);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f().s("#000000").ss(10,1,1).p("AmfBQQHVhFFqha");
	this.shape_33.setTransform(64,16);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f().s("#000000").ss(10,1,1).p("AmWBQQGvhLF+hU");
	this.shape_34.setTransform(63.9,16);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f().s("#000000").ss(10,1,1).p("AmQBQQGWhPGLhQ");
	this.shape_35.setTransform(63.8,16);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},38).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_27}]},1).to({state:[{t:this.shape_28}]},1).to({state:[{t:this.shape_29}]},1).to({state:[{t:this.shape_30}]},1).to({state:[{t:this.shape_31}]},1).to({state:[{t:this.shape_32}]},1).to({state:[{t:this.shape_33}]},1).to({state:[{t:this.shape_34}]},1).to({state:[{t:this.shape_35}]},1).to({state:[{t:this.shape}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,108.8,41);


(lib.Symbol1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 3
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#C8C8C8").s().p("ABjAfQgYghgKgFQgLgGgZgKQgZgIgUgCQgTgCgzAQQgzAQA2gbQA0gdA7AYQA7AXARAmQAKAVgCAAQgCAAgLgQg");
	this.shape.setTransform(673.2,204.6,0.93,0.93);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#C8C8C8").s().p("ACEBGQgXgNgYgRQgXgPhKgIQhMgHgdANQgfAOAIgeQAIgfA5gfQA5gdA7AXQA7AXAbA8QAYAygNAAIgGgCg");
	this.shape_1.setTransform(671.9,207,0.93,0.93);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#C8C8C8").s().p("ABTCCQgngZg5gdQg4gegwgBQgwgCAVhBQAVhCA5gfQA5geA7AYQA7AXAbA+QAcA+gWBCQgQAxgYAAQgJAAgKgHg");
	this.shape_2.setTransform(671.1,213,0.93,0.93);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#C8C8C8").s().p("ABNCOIhsgRQg/gJgjgoQgkgnAWhBQAVhCA5gfQA4geA7AYQA8AXAbA+QAbA+gWBCQgTA9gmAAIgIgBg");
	this.shape_3.setTransform(671,213.6,0.93,0.93);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#C8C8C8").s().p("AgxCgQg9gXgbhAQgbg/AWhBQAVhCA5gfQA4geA7AYQA8AXAbA+QAbA/gWBBQgVBDg5AeQggASghAAQgYAAgZgKg");
	this.shape_4.setTransform(671,216.1,0.93,0.93);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#C8C8C8").s().p("AB7BBQgWgMgWgQQgWgOhEgHQhHgHgbANQgdANAIgdQAIgcA1gdQA0gbA3AVQA3AWAZA4QAWAugLAAIgGgCg");
	this.shape_5.setTransform(671.9,207);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape}]},14).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape}]},1).to({state:[]},1).wait(51));

	// Layer 4
	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#C8C8C8").s().p("ABgAbQgYgegLgFQgLgGgYgHQgagHgRgBQgUgBgxATQgwASAygdQAxgeA7ASQA6ATAVAlQAKAUgDAAQgCAAgMgPg");
	this.shape_6.setTransform(639.7,196.9);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#C8C8C8").s().p("ACCBAQgYgLgXgPQgYgPhIgCQhIgDgeAQQgdAPAHgeQAHgeA1ghQA0ggA7ASQA6ATAfA5QAZAwgNAAIgFgCg");
	this.shape_7.setTransform(638.7,199.3);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#C8C8C8").s().p("ABXB8QgpgWg2gYQg6gagtACQgvACAPhBQAShCA1ghQA1ggA6ATQA7ASAeA7QAeA7gRBCQgMAwgYAAQgJAAgJgFg");
	this.shape_8.setTransform(638.2,205.6);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#C8C8C8").s().p("ABSCHIhpgKQg+gFgkgkQglgjAQhBQAShBA1ghQA0ggA7ASQA6ATAfA6QAeA7gRBCQgQA9gnABIgFgBg");
	this.shape_9.setTransform(638.1,206.3);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#C8C8C8").s().p("AgmCdQg7gTgfg7Qgdg7APhBQAShBA1ghQA1ggA6ASQA7ATAeA6QAeA8gRBBQgQBBg2AgQgiAWgkAAQgTAAgVgHg");
	this.shape_10.setTransform(638.1,209.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_6}]},14).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_6}]},1).to({state:[]},1).wait(51));

	// Layer 5
	this.instance = new lib.Symbol5();
	this.instance.parent = this;
	this.instance.setTransform(673.4,222.6,1.354,1.354,23.7,0,0,3.8,3.8);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(4).to({regX:3.7,x:676.7,y:210.6},5,cjs.Ease.get(1)).wait(21).to({regX:3.8,x:673.4,y:222.6},5,cjs.Ease.get(1)).wait(40));

	// Layer 6
	this.instance_1 = new lib.Symbol6();
	this.instance_1.parent = this;
	this.instance_1.setTransform(642.2,214.6,1.411,1.411,20.2,0,0,3.7,3.6);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(4).to({regX:3.8,regY:3.7,x:645.7,y:204.1},5,cjs.Ease.get(1)).wait(21).to({regX:3.7,regY:3.6,x:642.2,y:214.6},5,cjs.Ease.get(1)).wait(40));

	// Layer 7
	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("Ag1CRQg3gYgWg7QgVg6AVg9QAXg7A2gaQA1gZA1AXQA2AYAYA7QAWA7gZA7QgUA9g2AZQgcANgbAAQgaAAgagLg");
	this.shape_11.setTransform(671.1,216);

	this.timeline.addTween(cjs.Tween.get(this.shape_11).wait(75));

	// Layer 8
	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("AgsCbQg6gWgdg8Qgag9ATg+QAUhAA2gfQA2geA5AWQA5AUAcA8QAbA9gUA+QgTBBg3AeQggASghAAQgWAAgWgIg");
	this.shape_12.setTransform(638.1,209);

	this.timeline.addTween(cjs.Tween.get(this.shape_12).wait(75));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(623.4,192.7,61.9,39);


// stage content:
(lib.Holiday = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 8
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(10,1,1).p("Ah6AgQAzg2A7gIQBLgLA8BJ");
	this.shape.setTransform(919.1,384);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	// Layer 7
	this.instance = new lib.Symbol12();
	this.instance.parent = this;
	this.instance.setTransform(945.9,446.5,1.1,1.1,0,0,0,31.2,38.9);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	// Layer 6
	this.instance_1 = new lib.Symbol1();
	this.instance_1.parent = this;
	this.instance_1.setTransform(902.3,343.3,0.799,0.799,0,16,-164,655.3,214.3);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(1));

	// Layer 4
	this.instance_2 = new lib.pack();
	this.instance_2.parent = this;
	this.instance_2.setTransform(779.9,241.9);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(1));

	// Layer 10
	this.instance_3 = new lib.Symbol2();
	this.instance_3.parent = this;
	this.instance_3.setTransform(843.9,457.5,1,1,0,0,0,54.4,20.5);

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(1));

	// Layer 3
	this.instance_4 = new lib.calendar();
	this.instance_4.parent = this;
	this.instance_4.setTransform(628,154);

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(1));

	// Layer 2
	this.instance_5 = new lib.bg();
	this.instance_5.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(540,340,1080,680);
// library properties:
lib.properties = {
	id: '25542E323BD361409FFDF73B2A1FFBE4',
	width: 1080,
	height: 680,
	fps: 30,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: [
		//{src:"assets/images/bg20.png", id:"bg"},
		{src:"assets/images/calendar20.png", id:"calendar"},
		{src:"assets/images/pack20.png", id:"pack"},
		{src:"assets/images/pencil20.png", id:"pencil"}
	],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['25542E323BD361409FFDF73B2A1FFBE4'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}



})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;

$(function () {
    var canvas, stage, exportRoot, anim_container, dom_overlay_container, fnStartAnimation;
    function init() {
        canvas = document.getElementById("canvas20");
        anim_container = document.getElementById("animation_container20");
        dom_overlay_container = document.getElementById("dom_overlay_container20");
        var comp=AdobeAn.getComposition("25542E323BD361409FFDF73B2A1FFBE4");
        var lib=comp.getLibrary();
        var loader = new createjs.LoadQueue(false);
        loader.addEventListener("fileload", function(evt){handleFileLoad(evt,comp)});
        loader.addEventListener("complete", function(evt){handleComplete(evt,comp)});
        var lib=comp.getLibrary();
        loader.loadManifest(lib.properties.manifest);
    }
    function handleFileLoad(evt, comp) {
        var images=comp.getImages();
        if (evt && (evt.item.type == "image")) { images[evt.item.id] = evt.result; }
    }
    function handleComplete(evt,comp) {
        //This function is always called, irrespective of the content. You can use the variable "stage" after it is created in token create_stage.
        var lib=comp.getLibrary();
        var ss=comp.getSpriteSheet();
        var queue = evt.target;
        var ssMetadata = lib.ssMetadata;
        for(i=0; i<ssMetadata.length; i++) {
            ss[ssMetadata[i].name] = new createjs.SpriteSheet( {"images": [queue.getResult(ssMetadata[i].name)], "frames": ssMetadata[i].frames} )
        }
        exportRoot = new lib.Holiday();
        stage = new lib.Stage(canvas);
        //Registers the "tick" event listener.
        fnStartAnimation = function() {
            stage.addChild(exportRoot);
            createjs.Ticker.setFPS(lib.properties.fps);
            createjs.Ticker.addEventListener("tick", stage);
        }
        //Code to support hidpi screens and responsive scaling.
        function makeResponsive(isResp, respDim, isScale, scaleType) {
            var lastW, lastH, lastS=1;
            window.addEventListener('resize', resizeCanvas);
            resizeCanvas();
            function resizeCanvas() {
                var w = lib.properties.width, h = lib.properties.height;
                var iw = window.innerWidth, ih=window.innerHeight;
                var pRatio = window.devicePixelRatio || 1, xRatio=iw/w, yRatio=ih/h, sRatio=1;
                if(isResp) {
                    if((respDim=='width'&&lastW==iw) || (respDim=='height'&&lastH==ih)) {
                        sRatio = lastS;
                    }
                    else if(!isScale) {
                        if(iw<w || ih<h)
                            sRatio = Math.min(xRatio, yRatio);
                    }
                    else if(scaleType==1) {
                        sRatio = Math.min(xRatio, yRatio);
                    }
                    else if(scaleType==2) {
                        sRatio = Math.max(xRatio, yRatio);
                    }
                }
                canvas.width = w*pRatio*sRatio;
                canvas.height = h*pRatio*sRatio;
                canvas.style.width = dom_overlay_container.style.width = anim_container.style.width =  w*sRatio+'px';
                canvas.style.height = anim_container.style.height = dom_overlay_container.style.height = h*sRatio+'px';
                stage.scaleX = pRatio*sRatio;
                stage.scaleY = pRatio*sRatio;
                lastW = iw; lastH = ih; lastS = sRatio;
                stage.tickOnUpdate = false;
                stage.update();
                stage.tickOnUpdate = true;
            }
        }
        makeResponsive(true,'both',true,2);
        AdobeAn.compositionLoaded(lib.properties.id);
        fnStartAnimation();
    }
    init();
});