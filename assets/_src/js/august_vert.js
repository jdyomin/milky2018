(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [];


// symbols:



(lib.bg_part_vert = function() {
	this.initialize(img.bg_part_vert);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,717,262);


(lib.pack = function() {
	this.initialize(img.pack);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,421,473);// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.Symbol7 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#121111").ss(12,1,1).p("AAEhIQgOBEAOBN");
	this.shape.setTransform(0.4,7.3);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol7, new cjs.Rectangle(-6,-6,12.8,26.7), null);


(lib.Symbol6 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#121111").ss(12,1,1).p("Ag8CCQAmiYBThr");
	this.shape.setTransform(6.1,13);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol6, new cjs.Rectangle(-6,-6,24.2,38), null);


(lib.Symbol5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#121111").ss(12,1,1).p("AhaBiQA4iBB9hC");
	this.shape.setTransform(9.1,9.8);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol5, new cjs.Rectangle(-6,-6,30.3,31.5), null);


(lib.Символ2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#121111").ss(12,1,1).p("AAEh0Qg1CMBFBd");
	this.shape.setTransform(2,11.7);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Символ2, new cjs.Rectangle(-6,-6,16,35.3), null);


(lib.Символ1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#121111").ss(12,1,1).p("AgdheQgrDOBxgT");
	this.shape.setTransform(4.2,9.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Символ1, new cjs.Rectangle(-6,-6,20.3,31), null);


(lib.Symbol8 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.Symbol7();
	this.instance.parent = this;
	this.instance.setTransform(0.7,27.4,1,1,0,0,0,0.4,0);

	this.instance_1 = new lib.Symbol5();
	this.instance_1.parent = this;
	this.instance_1.setTransform(2,28.7,1,1,0,0,0,0,19.5);

	this.instance_2 = new lib.Symbol6();
	this.instance_2.parent = this;
	this.instance_2.setTransform(0,25.9,1,1,0,0,0,0,25.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_2},{t:this.instance_1},{t:this.instance}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol8, new cjs.Rectangle(-6,-6,32.3,54.1), null);


(lib.Символ3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой_1
	this.instance = new lib.Символ1();
	this.instance.parent = this;
	this.instance.setTransform(8.2,9.5,1,1,0,0,0,4.2,9.5);

	this.instance_1 = new lib.Символ2();
	this.instance_1.parent = this;
	this.instance_1.setTransform(1.9,14,1,1,0,0,0,1.9,11.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_1},{t:this.instance}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Символ3, new cjs.Rectangle(-6,-6,24.3,37.6), null);


(lib.Символ5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой_1
	this.instance = new lib.Символ3();
	this.instance.parent = this;
	this.instance.setTransform(90.7,116.4,1,1,0,0,0,6.1,0);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(2).to({x:89.6,y:90.2},10,cjs.Ease.get(1)).wait(14).to({x:90.7,y:116.4},10,cjs.Ease.get(1)).wait(47));

	// Слой_2
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#121111").ss(12,1,1).p("AgkoOQFcLTmuFK");
	this.shape.setTransform(98.8,66);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#121111").ss(12,1,1).p("AiCH1QHflsmOp9");
	this.shape_1.setTransform(100.1,63.4);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#121111").ss(12,1,1).p("AiNHdQIKmLm6ou");
	this.shape_2.setTransform(101.3,61);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#121111").ss(12,1,1).p("AiWHIQIxmlninq");
	this.shape_3.setTransform(102.3,58.9);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#121111").ss(12,1,1).p("AieG2QJSm8oEmv");
	this.shape_4.setTransform(103.2,57.1);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#121111").ss(12,1,1).p("AilGnQJunQohl9");
	this.shape_5.setTransform(104,55.6);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#121111").ss(12,1,1).p("AirGaQKFngo4lT");
	this.shape_6.setTransform(104.6,54.3);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#121111").ss(12,1,1).p("AivGRQKXnupLkz");
	this.shape_7.setTransform(105.1,53.4);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#121111").ss(12,1,1).p("AiyGKQKkn2pYkc");
	this.shape_8.setTransform(105.4,52.7);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#121111").ss(12,1,1).p("Ai0GGQKrn8pgkP");
	this.shape_9.setTransform(105.7,52.3);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#121111").ss(12,1,1).p("AhpmDQJiEKquH9");
	this.shape_10.setTransform(105.7,52.1);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#121111").ss(12,1,1).p("AipGfQJ9nbowli");
	this.shape_11.setTransform(104.4,54.8);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#121111").ss(12,1,1).p("AiVHLQIsmindnz");
	this.shape_12.setTransform(102.2,59.2);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#121111").ss(12,1,1).p("AiGHsQHul3mdpg");
	this.shape_13.setTransform(100.5,62.5);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#121111").ss(12,1,1).p("AiAH5QHXlnmGqK");
	this.shape_14.setTransform(99.9,63.8);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#121111").ss(12,1,1).p("Ah8IDQHFlblzqq");
	this.shape_15.setTransform(99.4,64.8);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#121111").ss(12,1,1).p("Ah4IJQG4lRlnrB");
	this.shape_16.setTransform(99.1,65.5);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#121111").ss(12,1,1).p("Ah3IOQGxlMlfrP");
	this.shape_17.setTransform(98.9,65.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},2).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_10}]},14).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape}]},1).wait(47));

	// Слой_3
	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#121111").ss(12,1,1).p("AiygdQAdA/A9AYQA2AWA9gOQA5gNArgmQArgmAJgv");
	this.shape_18.setTransform(-6.6,-46.6);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#121111").ss(12,1,1).p("AizgZQAdA/A9AYQA3AWA9gQQA6gOAqgnQAsgnAJgx");
	this.shape_19.setTransform(-6.5,-47);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#121111").ss(12,1,1).p("Ai0gWQAdA/A+AYQA2AWA9gRQA7gQArgoQArgnAKg0");
	this.shape_20.setTransform(-6.5,-47.3);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#121111").ss(12,1,1).p("Ai1gSQAeA+A9AZQA2AVA+gSQA8gSAqgoQArgnALg2");
	this.shape_21.setTransform(-6.4,-47.7);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#121111").ss(12,1,1).p("Ai1gPQAdA+A9AZQA2AVA+gTQA9gTAqgpQArgoALg4");
	this.shape_22.setTransform(-6.3,-48);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#121111").ss(12,1,1).p("Ai2gMQAdA+A9AZQA2AVA/gVQA9gUArgpQAqgoAMg6");
	this.shape_23.setTransform(-6.2,-48.3);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#121111").ss(12,1,1).p("Ai3gKQAeA/A9AYQA2AWA/gWQA+gWAqgpQAqgoANg9");
	this.shape_24.setTransform(-6.2,-48.5);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f().s("#121111").ss(12,1,1).p("Ai3gHQAdA+A9AZQA3AVA/gXQA+gWAqgqQAqgpANg+");
	this.shape_25.setTransform(-6.1,-48.8);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f().s("#121111").ss(12,1,1).p("Ai4gFQAdA+A+AZQA2AVA/gXQA/gYAqgqQAqgpAOhA");
	this.shape_26.setTransform(-6.1,-49);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f().s("#121111").ss(12,1,1).p("Ai4gEQAdA/A9AYQA3AWA/gZQBAgYApgqQAqgqAOhB");
	this.shape_27.setTransform(-6,-49.1);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f().s("#121111").ss(12,1,1).p("Ai5gCQAdA/A+AYQA2AWBAgZQBAgaApgqQAqgqAPhC");
	this.shape_28.setTransform(-6,-49.3);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f().s("#121111").ss(12,1,1).p("Ai5AAQAdA+A+AZQA2AVBAgaQBAgZApgrQAqgqAPhD");
	this.shape_29.setTransform(-6,-49.5);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f().s("#121111").ss(12,1,1).p("Ai5AAQAdA/A+AZQA2AVBAgaQBAgaAqgrQApgqAPhE");
	this.shape_30.setTransform(-5.9,-49.6);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f().s("#121111").ss(12,1,1).p("Ai5ABQAdA/A9AZQA2AVBBgbQBAgaAqgrQApgrAPhE");
	this.shape_31.setTransform(-5.9,-49.7);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f().s("#121111").ss(12,1,1).p("Ai6ABQAeBAA9AYQA2AWBBgbQBAgbAqgsQApgqAQhF");
	this.shape_32.setTransform(-5.9,-49.7);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f().s("#121111").ss(12,1,1).p("Ai6ACQAdBAA+AYQA2AWBAgcQBBgbAqgrQApgrAQhF");
	this.shape_33.setTransform(-5.9,-49.8);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f().s("#121111").ss(12,1,1).p("Ai6ADQAdA/A+AZQA2AVBAgbQBBgcAqgrQApgrAQhG");
	this.shape_34.setTransform(-5.9,-49.9);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f().s("#121111").ss(12,1,1).p("Ai3gIQAdA+A9AZQA3AVA/gWQA+gXAqgpQAqgpANg+");
	this.shape_35.setTransform(-6.1,-48.7);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f().s("#121111").ss(12,1,1).p("Ai1gRQAdA/A+AYQA2AWA+gTQA8gSAqgoQArgoALg3");
	this.shape_36.setTransform(-6.4,-47.8);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f().s("#121111").ss(12,1,1).p("AizgXQAdA+A9AZQA2AVA9gQQA7gPAqgnQAsgnAJgz");
	this.shape_37.setTransform(-6.5,-47.2);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f().s("#121111").ss(12,1,1).p("AiygbQAdA+A9AZQA2AVA9gOQA5gOArgmQAsgmAIgw");
	this.shape_38.setTransform(-6.6,-46.8);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f().s("#121111").ss(12,1,1).p("AiygdQAdA/A9AYQA3AWA8gOQA5gNArgmQAsgmAIgv");
	this.shape_39.setTransform(-6.6,-46.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_18}]}).to({state:[{t:this.shape_18}]},53).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_27}]},1).to({state:[{t:this.shape_28}]},1).to({state:[{t:this.shape_29}]},1).to({state:[{t:this.shape_30}]},1).to({state:[{t:this.shape_31}]},1).to({state:[{t:this.shape_32}]},1).to({state:[{t:this.shape_33}]},1).to({state:[{t:this.shape_34}]},1).to({state:[{t:this.shape_34}]},1).to({state:[{t:this.shape_34}]},7).to({state:[{t:this.shape_35}]},1).to({state:[{t:this.shape_36}]},1).to({state:[{t:this.shape_37}]},1).to({state:[{t:this.shape_38}]},1).to({state:[{t:this.shape_39}]},1).wait(1));

	// Слой_4
	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f().s("#121111").ss(11.5,1,1).p("Ai7BRQAPh3BEg7QA9g1BLAPQBLAPAsBKQAsBJgIBw");
	this.shape_40.setTransform(28.1,-94.2);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f().s("#121111").ss(11.5,1,1).p("Ai7BMQAPhvBEg3QA9gxBLANQBLAOAsBFQAsBEgIBp");
	this.shape_41.setTransform(28.1,-93.3);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f().s("#121111").ss(11.5,1,1).p("Ai7BHQAPhoBEgzQA9guBLANQBLANAsBAQAsBAgIBh");
	this.shape_42.setTransform(28.1,-92.4);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f().s("#121111").ss(11.5,1,1).p("Ai7BDQAPhiBEgwQA9gsBLANQBLAMAsA9QAsA7gIBc");
	this.shape_43.setTransform(28.1,-91.7);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f().s("#121111").ss(11.5,1,1).p("Ai7BAQAPhdBEguQA9gpBLAMQBLALAsA6QAsA5gIBW");
	this.shape_44.setTransform(28.1,-91.2);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f().s("#121111").ss(11.5,1,1).p("Ai7A8QAPhYBEgsQA9gnBLALQBLALAsA3QAsA2gIBT");
	this.shape_45.setTransform(28.1,-90.7);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.f().s("#121111").ss(11.5,1,1).p("Ai7A7QAPhWBEgrQA9gmBLALQBLALAsA1QAsA1gIBQ");
	this.shape_46.setTransform(28.1,-90.4);

	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.f().s("#121111").ss(11.5,1,1).p("Ai7A5QAPhUBEgpQA9gmBLALQBLALAsA0QAsAzgIBP");
	this.shape_47.setTransform(28.1,-90.2);

	this.shape_48 = new cjs.Shape();
	this.shape_48.graphics.f().s("#121111").ss(11.5,1,1).p("Ai7A5QAPhTBEgqQA9glBLALQBLAKAsA0QAsAzgIBP");
	this.shape_48.setTransform(28.1,-90.1);

	this.shape_49 = new cjs.Shape();
	this.shape_49.graphics.f().s("#121111").ss(11.5,1,1).p("Ai7A/QAPhcBEguQA9gpBLAMQBLAMAsA5QAsA4gIBW");
	this.shape_49.setTransform(28.1,-91.1);

	this.shape_50 = new cjs.Shape();
	this.shape_50.graphics.f().s("#121111").ss(11.5,1,1).p("Ai7BEQAPhjBEgyQA9gsBLANQBLAMAsA+QAsA9gIBd");
	this.shape_50.setTransform(28.1,-91.9);

	this.shape_51 = new cjs.Shape();
	this.shape_51.graphics.f().s("#121111").ss(11.5,1,1).p("Ai7BIQAPhqBEg0QA9guBLANQBLANAsBBQAsBBgIBj");
	this.shape_51.setTransform(28.1,-92.6);

	this.shape_52 = new cjs.Shape();
	this.shape_52.graphics.f().s("#121111").ss(11.5,1,1).p("Ai7BMQAPhvBEg3QA9gxBLAOQBLAOAsBFQAsBEgIBn");
	this.shape_52.setTransform(28.1,-93.2);

	this.shape_53 = new cjs.Shape();
	this.shape_53.graphics.f().s("#121111").ss(11.5,1,1).p("Ai7BOQAPhyBEg5QA9gzBLAPQBLAOAsBHQAsBGgIBs");
	this.shape_53.setTransform(28.1,-93.7);

	this.shape_54 = new cjs.Shape();
	this.shape_54.graphics.f().s("#121111").ss(11.5,1,1).p("Ai7BQQAPh1BEg6QA9g0BLAOQBLAPAsBJQAsBIgIBu");
	this.shape_54.setTransform(28.1,-94);

	this.shape_55 = new cjs.Shape();
	this.shape_55.graphics.f().s("#121111").ss(11.5,1,1).p("Ai7BRQAPh3BEg7QA9g0BLAPQBLAOAsBKQAsBJgIBw");
	this.shape_55.setTransform(28.1,-94.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_40}]}).to({state:[{t:this.shape_40}]},7).to({state:[{t:this.shape_41}]},1).to({state:[{t:this.shape_42}]},1).to({state:[{t:this.shape_43}]},1).to({state:[{t:this.shape_44}]},1).to({state:[{t:this.shape_45}]},1).to({state:[{t:this.shape_46}]},1).to({state:[{t:this.shape_47}]},1).to({state:[{t:this.shape_48}]},1).to({state:[{t:this.shape_48}]},46).to({state:[{t:this.shape_49}]},1).to({state:[{t:this.shape_50}]},1).to({state:[{t:this.shape_51}]},1).to({state:[{t:this.shape_52}]},1).to({state:[{t:this.shape_53}]},1).to({state:[{t:this.shape_54}]},1).to({state:[{t:this.shape_55}]},1).to({state:[{t:this.shape_40}]},1).wait(14));

	// Слой_5
	this.shape_56 = new cjs.Shape();
	this.shape_56.graphics.f().s("#121111").ss(11.5,1,1).p("Ai7BRQAPh3BEg7QA9g1BLAPQBLAPAsBKQAsBJgIBw");
	this.shape_56.setTransform(-43,-102.6);

	this.shape_57 = new cjs.Shape();
	this.shape_57.graphics.f().s("#121111").ss(11.5,1,1).p("Ai7BMQAPhvBEg3QA9gxBLAOQBLAOAsBFQAsBEgIBo");
	this.shape_57.setTransform(-43,-101.1);

	this.shape_58 = new cjs.Shape();
	this.shape_58.graphics.f().s("#121111").ss(11.5,1,1).p("Ai7BHQAPhoBEgzQA9guBLANQBLANAsBAQAsBAgIBh");
	this.shape_58.setTransform(-43,-99.7);

	this.shape_59 = new cjs.Shape();
	this.shape_59.graphics.f().s("#121111").ss(11.5,1,1).p("Ai7BDQAPhiBEgwQA9grBLAMQBLAMAsA9QAsA8gIBb");
	this.shape_59.setTransform(-43,-98.6);

	this.shape_60 = new cjs.Shape();
	this.shape_60.graphics.f().s("#121111").ss(11.5,1,1).p("Ai7A/QAPhcBEguQA9gpBLAMQBLALAsA6QAsA4gIBX");
	this.shape_60.setTransform(-43,-97.6);

	this.shape_61 = new cjs.Shape();
	this.shape_61.graphics.f().s("#121111").ss(11.5,1,1).p("Ai7A8QAPhYBEgsQA9gnBLALQBLALAsA3QAsA2gIBT");
	this.shape_61.setTransform(-43,-96.9);

	this.shape_62 = new cjs.Shape();
	this.shape_62.graphics.f().s("#121111").ss(11.5,1,1).p("Ai7A7QAPhWBEgrQA9gmBLALQBLALAsA1QAsA1gIBQ");
	this.shape_62.setTransform(-43,-96.4);

	this.shape_63 = new cjs.Shape();
	this.shape_63.graphics.f().s("#121111").ss(11.5,1,1).p("Ai7A6QAPhUBEgqQA9glBLAKQBLALAsA0QAsAzgIBP");
	this.shape_63.setTransform(-43,-96.1);

	this.shape_64 = new cjs.Shape();
	this.shape_64.graphics.f().s("#121111").ss(11.5,1,1).p("Ai7A5QAPhTBEgqQA9glBLALQBLAKAsA0QAsAzgIBP");
	this.shape_64.setTransform(-43,-96);

	this.shape_65 = new cjs.Shape();
	this.shape_65.graphics.f().s("#121111").ss(11.5,1,1).p("Ai7A/QAPhcBEguQA9goBLALQBLAMAsA5QAsA4gIBX");
	this.shape_65.setTransform(-43,-97.5);

	this.shape_66 = new cjs.Shape();
	this.shape_66.graphics.f().s("#121111").ss(11.5,1,1).p("Ai7BEQAPhjBEgyQA9gsBLANQBLAMAsA+QAsA9gIBd");
	this.shape_66.setTransform(-43,-98.9);

	this.shape_67 = new cjs.Shape();
	this.shape_67.graphics.f().s("#121111").ss(11.5,1,1).p("Ai7BIQAPhpBEg0QA9gvBLANQBLANAsBCQAsBAgIBj");
	this.shape_67.setTransform(-43,-100);

	this.shape_68 = new cjs.Shape();
	this.shape_68.graphics.f().s("#121111").ss(11.5,1,1).p("Ai7BLQAPhuBEg3QA9gxBLAOQBLAOAsBEQAsBEgIBo");
	this.shape_68.setTransform(-43,-100.9);

	this.shape_69 = new cjs.Shape();
	this.shape_69.graphics.f().s("#121111").ss(11.5,1,1).p("Ai7BOQAPhyBEg5QA9gzBLAPQBLAOAsBHQAsBGgIBs");
	this.shape_69.setTransform(-43,-101.7);

	this.shape_70 = new cjs.Shape();
	this.shape_70.graphics.f().s("#121111").ss(11.5,1,1).p("Ai7BQQAPh1BEg6QA9g0BLAOQBLAPAsBJQAsBIgIBu");
	this.shape_70.setTransform(-43,-102.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_56,p:{y:-102.6}}]}).to({state:[{t:this.shape_56,p:{y:-102.6}}]},7).to({state:[{t:this.shape_57}]},1).to({state:[{t:this.shape_58}]},1).to({state:[{t:this.shape_59}]},1).to({state:[{t:this.shape_60}]},1).to({state:[{t:this.shape_61}]},1).to({state:[{t:this.shape_62}]},1).to({state:[{t:this.shape_63}]},1).to({state:[{t:this.shape_64}]},1).to({state:[{t:this.shape_64}]},46).to({state:[{t:this.shape_65}]},1).to({state:[{t:this.shape_66}]},1).to({state:[{t:this.shape_67}]},1).to({state:[{t:this.shape_68}]},1).to({state:[{t:this.shape_69}]},1).to({state:[{t:this.shape_70}]},1).to({state:[{t:this.shape_56,p:{y:-102.5}}]},1).to({state:[{t:this.shape_56,p:{y:-102.6}}]},1).wait(14));

	// Слой_6
	this.instance_1 = new lib.pack();
	this.instance_1.parent = this;
	this.instance_1.setTransform(-157,-181);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(83));

	// Слой_7
	this.instance_2 = new lib.Symbol8();
	this.instance_2.parent = this;
	this.instance_2.setTransform(-97.5,-85.3,1,1,0,0,0,-2.2,27.9);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(39).to({regX:-2.3,regY:27.8,rotation:-45,x:-151.9,y:-83.8},8,cjs.Ease.get(1)).to({regX:-2.1,regY:27.9,rotation:15,x:-85.5,y:-85.3},5,cjs.Ease.get(-1)).to({regX:-2.2,rotation:0,x:-97.5},3,cjs.Ease.get(1)).wait(28));

	// Слой_8
	this.shape_71 = new cjs.Shape();
	this.shape_71.graphics.f().s("#121111").ss(12,1,1).p("AE3JlQnihWhtkWQh6k4Fsol");
	this.shape_71.setTransform(-90.9,-28.4);

	this.shape_72 = new cjs.Shape();
	this.shape_72.graphics.f().s("#121111").ss(12,1,1).p("AigpdQj1IXCHErQB9ESHQBn");
	this.shape_72.setTransform(-91.7,-27.6);

	this.shape_73 = new cjs.Shape();
	this.shape_73.graphics.f().s("#121111").ss(12,1,1).p("AkFpWQiNIJCTEgQCLEOG/B2");
	this.shape_73.setTransform(-92.9,-26.9);

	this.shape_74 = new cjs.Shape();
	this.shape_74.graphics.f().s("#121111").ss(12,1,1).p("AlQpQQg2H9CdEYQCXEKGxCC");
	this.shape_74.setTransform(-94.9,-26.4);

	this.shape_75 = new cjs.Shape();
	this.shape_75.graphics.f().s("#121111").ss(12,1,1).p("Al+pMQARH0ClERQCiEHGlCN");
	this.shape_75.setTransform(-98.1,-25.9);

	this.shape_76 = new cjs.Shape();
	this.shape_76.graphics.f().s("#121111").ss(12,1,1).p("AmcpIQBIHtCsEKQCoEGGdCU");
	this.shape_76.setTransform(-101.1,-25.5);

	this.shape_77 = new cjs.Shape();
	this.shape_77.graphics.f().s("#121111").ss(12,1,1).p("AmypFQBxHnCwEGQCtEFGXCZ");
	this.shape_77.setTransform(-103.3,-25.3);

	this.shape_78 = new cjs.Shape();
	this.shape_78.graphics.f().s("#121111").ss(12,1,1).p("Am/pEQCIHlCzEDQCxEEGTCd");
	this.shape_78.setTransform(-104.6,-25.1);

	this.shape_79 = new cjs.Shape();
	this.shape_79.graphics.f().s("#121111").ss(12,1,1).p("AHEJEQmRieizkDQizkDiQnj");
	this.shape_79.setTransform(-105,-25.1);

	this.shape_80 = new cjs.Shape();
	this.shape_80.graphics.f().s("#121111").ss(12,1,1).p("Am2pEQB5HjCwEFQCvEFGVCc");
	this.shape_80.setTransform(-103.7,-25.1);

	this.shape_81 = new cjs.Shape();
	this.shape_81.graphics.f().s("#121111").ss(12,1,1).p("AmOpFQAyHgCnEMQCnEMGdCT");
	this.shape_81.setTransform(-99.7,-25.3);

	this.shape_82 = new cjs.Shape();
	this.shape_82.graphics.f().s("#121111").ss(12,1,1).p("AlCpIQhCHdCXEXQCXEXGtCG");
	this.shape_82.setTransform(-94.1,-25.5);

	this.shape_83 = new cjs.Shape();
	this.shape_83.graphics.f().s("#121111").ss(12,1,1).p("AilpLQjmHXCBEmQCBEnHDBz");
	this.shape_83.setTransform(-91.2,-25.8);

	this.shape_84 = new cjs.Shape();
	this.shape_84.graphics.f().s("#121111").ss(12,1,1).p("AEsJQQnfhahlk7Qhlk6G3nQ");
	this.shape_84.setTransform(-89.8,-26.3);

	this.shape_85 = new cjs.Shape();
	this.shape_85.graphics.f().s("#121111").ss(12,1,1).p("AAEpbQmNH/BxE5QBpEnHhBY");
	this.shape_85.setTransform(-90.4,-27.4);

	this.shape_86 = new cjs.Shape();
	this.shape_86.graphics.f().s("#121111").ss(12,1,1).p("AgbpiQl1IcB4E5QBsEaHiBW");
	this.shape_86.setTransform(-90.8,-28.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_71}]}).to({state:[{t:this.shape_71}]},39).to({state:[{t:this.shape_72}]},1).to({state:[{t:this.shape_73}]},1).to({state:[{t:this.shape_74}]},1).to({state:[{t:this.shape_75}]},1).to({state:[{t:this.shape_76}]},1).to({state:[{t:this.shape_77}]},1).to({state:[{t:this.shape_78}]},1).to({state:[{t:this.shape_79}]},1).to({state:[{t:this.shape_80}]},1).to({state:[{t:this.shape_81}]},1).to({state:[{t:this.shape_82}]},1).to({state:[{t:this.shape_83}]},1).to({state:[{t:this.shape_84}]},1).to({state:[{t:this.shape_85}]},1).to({state:[{t:this.shape_86}]},1).to({state:[{t:this.shape_71}]},1).wait(28));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-157,-181,421,473);


// stage content:
(lib.Main_vert = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой_2
	this.instance = new lib.Символ5();
	this.instance.parent = this;
	this.instance.setTransform(298.6,723.2,1.291,1.291,0,0,0,0.1,0.1);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	// Слой_3
	this.instance_1 = new lib.bg_part_vert();
	this.instance_1.parent = this;
	this.instance_1.setTransform(-389,795,1.926,1.926);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(1));

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#92DEF1").s().p("EggbAh0QvYi4ufmrQwKncqMn/QqMoAgKqxQgKqzRQmbQRPmbathpQTEhLW6AfQLdAPHpAeMBL3AWRQklDPsyGNQsxGMwuJdQwuJdwKGuQwKGuqFA0QiPAMihAAQowAAr9iPg");
	this.shape.setTransform(242.1,1381.2);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-91.4,650,1383.5,1611.9);
// library properties:
lib.properties = {
	id: 'B38583368D43DC4AA2355CAB60ADC772',
	width: 600,
	height: 1300,
	fps: 30,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: [
        {src:"assets/images/bg_part_vert-19.png", id:"bg_part_vert"},
		{src:"assets/images/pack-19-2.png", id:"pack"}
	],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['B38583368D43DC4AA2355CAB60ADC772'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}



})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;

$(function () {
    var canvas, stage, exportRoot, anim_container, dom_overlay_container, fnStartAnimation;
    function init() {
        canvas = document.getElementById("canvas19-2");
        anim_container = document.getElementById("animation_container19-2");
        dom_overlay_container = document.getElementById("dom_overlay_container19-2");
        var comp=AdobeAn.getComposition("B38583368D43DC4AA2355CAB60ADC772");
        var lib=comp.getLibrary();
        var loader = new createjs.LoadQueue(false);
        loader.addEventListener("fileload", function(evt){handleFileLoad(evt,comp)});
        loader.addEventListener("complete", function(evt){handleComplete(evt,comp)});
        var lib=comp.getLibrary();
        loader.loadManifest(lib.properties.manifest);
    }
    function handleFileLoad(evt, comp) {
        var images=comp.getImages();
        if (evt && (evt.item.type == "image")) { images[evt.item.id] = evt.result; }
    }
    function handleComplete(evt,comp) {
        //This function is always called, irrespective of the content. You can use the variable "stage" after it is created in token create_stage.
        var lib=comp.getLibrary();
        var ss=comp.getSpriteSheet();
        var queue = evt.target;
        var ssMetadata = lib.ssMetadata;
        for(i=0; i<ssMetadata.length; i++) {
            ss[ssMetadata[i].name] = new createjs.SpriteSheet( {"images": [queue.getResult(ssMetadata[i].name)], "frames": ssMetadata[i].frames} )
        }
        exportRoot = new lib.Main_vert();
        stage = new lib.Stage(canvas);
        //Registers the "tick" event listener.
        fnStartAnimation = function() {
            stage.addChild(exportRoot);
            createjs.Ticker.setFPS(lib.properties.fps);
            createjs.Ticker.addEventListener("tick", stage);
        }
        //Code to support hidpi screens and responsive scaling.
        function makeResponsive(isResp, respDim, isScale, scaleType) {
            var lastW, lastH, lastS=1;
            window.addEventListener('resize', resizeCanvas);
            resizeCanvas();
            function resizeCanvas() {
                var w = lib.properties.width, h = lib.properties.height;
                var iw = window.innerWidth, ih=window.innerHeight;
                var pRatio = window.devicePixelRatio || 1, xRatio=iw/w, yRatio=ih/h, sRatio=1;
                if(isResp) {
                    if((respDim=='width'&&lastW==iw) || (respDim=='height'&&lastH==ih)) {
                        sRatio = lastS;
                    }
                    else if(!isScale) {
                        if(iw<w || ih<h)
                            sRatio = Math.min(xRatio, yRatio);
                    }
                    else if(scaleType==1) {
                        sRatio = Math.min(xRatio, yRatio);
                    }
                    else if(scaleType==2) {
                        sRatio = Math.max(xRatio, yRatio);
                    }
                }
                canvas.width = w*pRatio*sRatio;
                canvas.height = h*pRatio*sRatio;
                canvas.style.width = dom_overlay_container.style.width = anim_container.style.width =  w*sRatio+'px';
                canvas.style.height = anim_container.style.height = dom_overlay_container.style.height = h*sRatio+'px';
                stage.scaleX = pRatio*sRatio;
                stage.scaleY = pRatio*sRatio;
                lastW = iw; lastH = ih; lastS = sRatio;
                stage.tickOnUpdate = false;
                stage.update();
                stage.tickOnUpdate = true;
            }
        }
        makeResponsive(true,'both',true,2);
        AdobeAn.compositionLoaded(lib.properties.id);
        fnStartAnimation();
    }
    init();
});