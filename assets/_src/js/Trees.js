(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [];


// symbols:



(lib.bg_part = function() {
	this.initialize(img.bg_part);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,730,306);


(lib.leaf1 = function() {
	this.initialize(img.leaf1);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,131,103);


(lib.leaf10 = function() {
	this.initialize(img.leaf10);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,100,130);


(lib.leaf11 = function() {
	this.initialize(img.leaf11);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,157,67);


(lib.leaf2 = function() {
	this.initialize(img.leaf2);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,133,155);


(lib.leaf3 = function() {
	this.initialize(img.leaf3);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,56,203);


(lib.leaf4 = function() {
	this.initialize(img.leaf4);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,83,194);


(lib.leaf5 = function() {
	this.initialize(img.leaf5);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,64,208);


(lib.leaf6 = function() {
	this.initialize(img.leaf6);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,73,155);


(lib.leaf7 = function() {
	this.initialize(img.leaf7);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,66,173);


(lib.leaf8 = function() {
	this.initialize(img.leaf8);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,114,135);


(lib.leaf9 = function() {
	this.initialize(img.leaf9);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,55,169);


(lib.mouth1 = function() {
	this.initialize(img.mouth1);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,35,97);


(lib.pack = function() {
	this.initialize(img.pack);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,347,479);


(lib.shadow = function() {
	this.initialize(img.shadow);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,307,70);


(lib.teeth = function() {
	this.initialize(img.teeth);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,16,15);// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.Symbol16 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.shadow();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol16, new cjs.Rectangle(0,0,307,70), null);


(lib.Symbol15 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.leaf11();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol15, new cjs.Rectangle(0,0,157,67), null);


(lib.Symbol14 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.leaf10();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol14, new cjs.Rectangle(0,0,100,130), null);


(lib.Symbol13 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.leaf9();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol13, new cjs.Rectangle(0,0,55,169), null);


(lib.Symbol12 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.leaf8();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol12, new cjs.Rectangle(0,0,114,135), null);


(lib.Symbol11 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.leaf7();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol11, new cjs.Rectangle(0,0,66,173), null);


(lib.Symbol10 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.leaf6();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol10, new cjs.Rectangle(0,0,73,155), null);


(lib.Symbol8 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.leaf5();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol8, new cjs.Rectangle(0,0,64,208), null);


(lib.Symbol7 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.leaf4();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol7, new cjs.Rectangle(0,0,83,194), null);


(lib.Symbol6 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.leaf3();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol6, new cjs.Rectangle(0,0,56,203), null);


(lib.Symbol5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.leaf2();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol5, new cjs.Rectangle(0,0,133,155), null);


(lib.Symbol4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.leaf1();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol4, new cjs.Rectangle(0,0,131,103), null);


(lib.Symbol3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.teeth();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol3, new cjs.Rectangle(0,0,16,15), null);


(lib.Symbol1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.mouth1();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol1, new cjs.Rectangle(0,0,35,97), null);


(lib.Symbol6_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AgZAaQgLgLAAgPQAAgOALgMQALgLAOAAQAPAAALALQALAMAAAOQAAAPgLALQgLALgPAAQgOAAgLgLg");
	this.shape.setTransform(3.7,3.8);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol6_1, new cjs.Rectangle(0,0,7.5,7.5), null);


(lib.Symbol5_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AgZAaQgLgLAAgPQAAgOALgMQALgLAOAAQAPAAALALQALAMAAAOQAAAPgLALQgLALgPAAQgOAAgLgLg");
	this.shape.setTransform(3.7,3.8);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol5_1, new cjs.Rectangle(0,0,7.5,7.5), null);


(lib.Symbol2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.Symbol3();
	this.instance.parent = this;
	this.instance.setTransform(8,6.7,1.119,1.119,20.4,0,0,8,7.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol2, new cjs.Rectangle(-3.3,-4.3,22.7,22), null);


// stage content:
(lib.Trees = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 9
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#CFCFCF").s().p("AhjAKQAoguBCAAQBBAAAgAfQAgAegkgXQgjgWgMgCQgNgCgbAAQgbABgUAGQgUAFgqAhQgTAQgDAAQgEAAAXgbg");
	this.shape.setTransform(766.4,271.4);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#CFCFCF").s().p("AiTA6QgDghArgzQArgxBCAAQBBAAAxAxQAxAugbgDQgbgEgdgGQgcgHhKAUQhLAVgYAYQgKALgHAAQgJAAgCgSg");
	this.shape_1.setTransform(766.9,275.3);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#CFCFCF").s().p("AB1B0QgvgKhCgGQhAgHguAQQgvARgEhIQgEhGArg0QArgxBCAAQBBAAAxAxQAyA0ADBGQAFBAgkAAIgKgCg");
	this.shape_2.setTransform(768,279.5);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#CFCFCF").s().p("AhpB+QgwgYgEhIQgEhGArg0QArgxBCAAQBBAAAxAxQAyA0ADBGQAGBIguAKIhuAYQgXAGgWAAQglAAgfgQg");
	this.shape_3.setTransform(768,281.9);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#CFCFCF").s().p("AhoB5QgxgxgEhIQgEhGArg0QArgxBCAAQBBAAAxAxQAyA0ADBGQAGBIgtAxQgrAzhBAAQhBAAgygzg");
	this.shape_4.setTransform(768,285);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape}]},19).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape}]},1).to({state:[]},1).wait(10));

	// Layer 11
	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#CFCFCF").s().p("AhiAKQAoguBBAAQBCAAAfAfQAgAegjgXQgkgXgMgBQgNgCgbAAQgbABgTAFQgVAGgqAhQgTAQgDAAQgDAAAXgbg");
	this.shape_5.setTransform(730.6,269.9);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#CFCFCF").s().p("AiSA6QgDghArgzQArgxBBAAQBCAAAxAxQAwAugagDQgcgEgcgHQgdgGhKAUQhKAUgYAZQgLALgHAAQgJAAgBgSg");
	this.shape_6.setTransform(731.1,273.8);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#CFCFCF").s().p("AB1B0QgvgKhCgGQhAgHguAQQgvARgFhIQgDhGArg0QArgxBCAAQBBAAAxAxQAxA0AEBGQAFBAgkAAIgKgCg");
	this.shape_7.setTransform(732.3,278);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#CFCFCF").s().p("AhpB/QgwgZgFhHQgDhHArgzQArgxBCAAQBBAAAxAxQAxAzAEBHQAFBHgtAKIhvAYQgXAFgWAAQglAAgegOg");
	this.shape_8.setTransform(732.3,280.4);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#CFCFCF").s().p("AhoB5QgxgxgFhIQgDhGArg0QArgxBCAAQBBAAAxAxQAxA0AEBGQAFBIgsAxQgrAzhBAAQhBAAgygzg");
	this.shape_9.setTransform(732.3,283.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_5}]},19).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_5}]},1).to({state:[]},1).wait(10));

	// Layer 12
	this.instance = new lib.Symbol5_1();
	this.instance.parent = this;
	this.instance.setTransform(762.1,291.3,1.509,1.509,0,0,0,3.7,3.8);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(6).to({x:767.9,y:281.7},5,cjs.Ease.get(0.7)).wait(18).to({x:762.1,y:291.3},5).wait(5));

	// Layer 13
	this.instance_1 = new lib.Symbol6_1();
	this.instance_1.parent = this;
	this.instance_1.setTransform(729.6,291.3,1.509,1.509,0,0,0,3.7,3.8);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(6).to({x:734.3,y:281.9},5,cjs.Ease.get(0.7)).wait(18).to({x:729.6,y:291.3},5).wait(5));

	// Layer 14
	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("AhoB5QgxgxgEhIQgEhGArg0QArgxBCAAQBBAAAxAxQAyA0ADBGQAGBIgtAxQgrAzhBAAQhBAAgygzg");
	this.shape_10.setTransform(768,285);

	this.timeline.addTween(cjs.Tween.get(this.shape_10).wait(39));

	// Layer 15
	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("AhoB5QgxgxgFhIQgDhGArg0QArgxBCAAQBBAAAxAxQAxA0AEBGQAFBIgsAxQgrAzhBAAQhBAAgygzg");
	this.shape_11.setTransform(732.3,283.5);

	this.timeline.addTween(cjs.Tween.get(this.shape_11).wait(39));

	// Layer 8
	this.instance_2 = new lib.Symbol2();
	this.instance_2.parent = this;
	this.instance_2.setTransform(750.8,321.1,0.763,0.763,-2.7,0,0,7.9,7.5);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).to({regY:7.4,x:751.4,y:334.9},4).to({regY:7.5,x:750.5,y:316.3},4).to({x:750.8,y:321.1},4).wait(1).to({regY:7.4,x:751.4,y:334.9},4).to({regY:7.5,x:750.5,y:316.3},4).to({x:750.8,y:321.1},4).wait(1).to({regY:7.4,x:751.4,y:334.9},4).to({regY:7.5,x:750.5,y:316.3},4).to({x:750.8,y:321.1},4).wait(1));

	// Layer 7
	this.instance_3 = new lib.Symbol1();
	this.instance_3.parent = this;
	this.instance_3.setTransform(753.9,347,0.763,0.763,-2.7,0,0,17.5,48.6);

	this.timeline.addTween(cjs.Tween.get(this.instance_3).to({regX:17.6,scaleY:0.47,x:754},4).to({regX:17.5,regY:48.5,scaleY:0.85,x:753.9,y:346.9},4).to({regY:48.6,scaleY:0.76,y:347},4).wait(1).to({regX:17.6,scaleY:0.47,x:754},4).to({regX:17.5,regY:48.5,scaleY:0.85,x:753.9,y:346.9},4).to({regY:48.6,scaleY:0.76,y:347},4).wait(1).to({regX:17.6,scaleY:0.47,x:754},4).to({regX:17.5,regY:48.5,scaleY:0.85,x:753.9,y:346.9},4).to({regY:48.6,scaleY:0.76,y:347},4).wait(1));

	// Layer 18
	this.instance_4 = new lib.Symbol5();
	this.instance_4.parent = this;
	this.instance_4.setTransform(587.6,269.7,1,1,53.2,0,0,131.1,2.4);

	this.timeline.addTween(cjs.Tween.get(this.instance_4).to({regX:131.2,regY:2.5,rotation:15,x:559.1,y:357.2},8,cjs.Ease.get(-1)).to({regY:2.4,rotation:0,x:559.2,y:389.4},8,cjs.Ease.get(1)).wait(4).to({regY:2.5,rotation:15,x:559.1,y:357.2},8,cjs.Ease.get(-1)).to({regX:131.1,regY:2.4,rotation:53.2,x:587.6,y:269.7},8,cjs.Ease.get(1)).wait(3));

	// Layer 6
	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(12,1,1).p("AKnHGQlAsdwNhu");
	this.shape_12.setTransform(932.1,413.7);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(12,1,1).p("Aqom9QQNBuFEMN");
	this.shape_13.setTransform(932.3,412.8);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(12,1,1).p("AqvmkQQNBtFSLc");
	this.shape_14.setTransform(932.9,410.4);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(12,1,1).p("Aq6l6QQOBtFnKI");
	this.shape_15.setTransform(934,406.2);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(12,1,1).p("ArJlAQQOBtGFIV");
	this.shape_16.setTransform(935.5,400.4);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(12,1,1).p("Arcj2QQNBtGsGA");
	this.shape_17.setTransform(937.5,393);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#000000").ss(12,1,1).p("Ar0ibQQNBtHcDK");
	this.shape_18.setTransform(939.8,383.9);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#000000").ss(12,1,1).p("AsQgwQQNBtIUgN");
	this.shape_19.setTransform(942.6,373.1);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#000000").ss(12,1,1).p("AswA8QQNBtJUkE");
	this.shape_20.setTransform(945.9,362.3);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#000000").ss(12,1,1).p("AsbB6QQDBdI0lj");
	this.shape_21.setTransform(944,356.7);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#000000").ss(12,1,1).p("AsJCtQP6BQIZm2");
	this.shape_22.setTransform(942.4,352);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#000000").ss(12,1,1).p("Ar5DYQPzBEIAn7");
	this.shape_23.setTransform(941,348.2);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#000000").ss(12,1,1).p("ArtD7QPuA6Hto0");
	this.shape_24.setTransform(939.8,345.1);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f().s("#000000").ss(12,1,1).p("ArjEWQPoAzHfph");
	this.shape_25.setTransform(939,342.7);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f().s("#000000").ss(12,1,1).p("ArcEoQPlAuHUqB");
	this.shape_26.setTransform(938.3,341);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f().s("#000000").ss(12,1,1).p("ArYE0QPkAqHMqT");
	this.shape_27.setTransform(938,340);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f().s("#000000").ss(12,1,1).p("ALXk5QnLKavigp");
	this.shape_28.setTransform(937.8,339.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_12}]}).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_27}]},1).to({state:[{t:this.shape_28}]},1).to({state:[{t:this.shape_28}]},4).to({state:[{t:this.shape_27}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_12}]},1).wait(3));

	// Layer 27
	this.instance_5 = new lib.Symbol15();
	this.instance_5.parent = this;
	this.instance_5.setTransform(979.5,423.7,1,1,105,0,0,3.5,39);

	this.timeline.addTween(cjs.Tween.get(this.instance_5).to({regX:3.6,rotation:25,x:991.9,y:366.7},8,cjs.Ease.get(-1)).to({regX:3.5,rotation:0,x:979.5,y:341},8,cjs.Ease.get(1)).wait(4).to({regX:3.6,rotation:25,x:991.9,y:366.7},8,cjs.Ease.get(-1)).to({regX:3.5,rotation:105,x:979.5,y:423.7},8,cjs.Ease.get(1)).wait(3));

	// Layer 26
	this.instance_6 = new lib.Symbol14();
	this.instance_6.parent = this;
	this.instance_6.setTransform(1000.1,461.2,1,1,75,0,0,2.7,128.3);

	this.timeline.addTween(cjs.Tween.get(this.instance_6).to({regX:2.8,rotation:9.5,x:1027.7,y:353},8,cjs.Ease.get(-1)).to({regX:2.7,rotation:0,x:1008.7,y:308.3},8,cjs.Ease.get(1)).wait(4).to({regX:2.8,rotation:9.5,x:1027.7,y:353},8,cjs.Ease.get(-1)).to({regX:2.7,rotation:75,x:1000.1,y:461.2},8,cjs.Ease.get(1)).wait(3));

	// Layer 23
	this.instance_7 = new lib.Symbol11();
	this.instance_7.parent = this;
	this.instance_7.setTransform(922.2,384.2,1,1,32,0,0,10.2,171.1);

	this.timeline.addTween(cjs.Tween.get(this.instance_7).to({rotation:0,x:924.2,y:371.6},8,cjs.Ease.get(-1)).to({y:362.1},8,cjs.Ease.get(1)).wait(4).to({y:371.6},8,cjs.Ease.get(-1)).to({rotation:32,x:922.2,y:384.2},8,cjs.Ease.get(1)).wait(3));

	// Layer 24
	this.instance_8 = new lib.Symbol12();
	this.instance_8.parent = this;
	this.instance_8.setTransform(951.7,393.8,1,1,32,0,0,110.3,134.6);

	this.timeline.addTween(cjs.Tween.get(this.instance_8).to({rotation:0,x:957.1,y:369.8},8,cjs.Ease.get(-1)).to({x:954.3,y:354.6},8,cjs.Ease.get(1)).wait(4).to({x:957.1,y:369.8},8,cjs.Ease.get(-1)).to({rotation:32,x:951.7,y:393.8},8,cjs.Ease.get(1)).wait(3));

	// Layer 25
	this.instance_9 = new lib.Symbol13();
	this.instance_9.parent = this;
	this.instance_9.setTransform(991.7,435.1,1,1,0,0,0,25.7,167.7);

	this.timeline.addTween(cjs.Tween.get(this.instance_9).to({rotation:-10,x:1007.8,y:362},8,cjs.Ease.get(-1)).to({rotation:0,x:991.7,y:328.7},8,cjs.Ease.get(1)).wait(4).to({rotation:-10,x:1007.8,y:362},8,cjs.Ease.get(-1)).to({rotation:0,x:991.7,y:435.1},8,cjs.Ease.get(1)).wait(3));

	// Layer 2
	this.instance_10 = new lib.pack();
	this.instance_10.parent = this;
	this.instance_10.setTransform(632,141);

	this.timeline.addTween(cjs.Tween.get(this.instance_10).wait(39));

	// Layer 16
	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f().s("#000000").ss(12,1,1).p("AqJoDQEBNCQSDF");
	this.shape_29.setTransform(648.4,318.6);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f().s("#000000").ss(12,1,1).p("AqMn8QEHMzQSDG");
	this.shape_30.setTransform(648.1,319.3);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f().s("#000000").ss(12,1,1).p("AqUnlQEXMGQSDF");
	this.shape_31.setTransform(647.3,321.6);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f().s("#000000").ss(12,1,1).p("AqinAQEzK7QSDG");
	this.shape_32.setTransform(645.9,325.3);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f().s("#000000").ss(12,1,1).p("Aq2mLQFbJRQSDG");
	this.shape_33.setTransform(643.9,330.6);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f().s("#000000").ss(12,1,1).p("ArPlIQGNHLQSDF");
	this.shape_34.setTransform(641.4,337.4);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f().s("#000000").ss(12,1,1).p("Aruj1QHLElQSDG");
	this.shape_35.setTransform(638.3,345.6);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f().s("#000000").ss(12,1,1).p("AsTiTQIVBjQSDE");
	this.shape_36.setTransform(634.7,355.4);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f().s("#000000").ss(12,1,1).p("As9gKQJph+QSDE");
	this.shape_37.setTransform(630.4,364.2);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f().s("#000000").ss(12,1,1).p("As4A4QJejWQSDE");
	this.shape_38.setTransform(631,366.4);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f().s("#000000").ss(12,1,1).p("AszBYQJVkkQSDF");
	this.shape_39.setTransform(631.4,371);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f().s("#000000").ss(12,1,1).p("AsvBzQJNllQSDG");
	this.shape_40.setTransform(631.8,374.7);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f().s("#000000").ss(12,1,1).p("AssCLQJHmaQSDF");
	this.shape_41.setTransform(632.1,377.7);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f().s("#000000").ss(12,1,1).p("AsqCeQJDnEQSDF");
	this.shape_42.setTransform(632.3,380);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f().s("#000000").ss(12,1,1).p("AsoCrQI/niQSDG");
	this.shape_43.setTransform(632.5,381.6);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f().s("#000000").ss(12,1,1).p("AsnCzQI9nzQSDF");
	this.shape_44.setTransform(632.6,382.6);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f().s("#000000").ss(12,1,1).p("AsnC2QI9n5QSDF");
	this.shape_45.setTransform(632.7,382.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_29}]}).to({state:[{t:this.shape_30}]},1).to({state:[{t:this.shape_31}]},1).to({state:[{t:this.shape_32}]},1).to({state:[{t:this.shape_33}]},1).to({state:[{t:this.shape_34}]},1).to({state:[{t:this.shape_35}]},1).to({state:[{t:this.shape_36}]},1).to({state:[{t:this.shape_37}]},1).to({state:[{t:this.shape_38}]},1).to({state:[{t:this.shape_39}]},1).to({state:[{t:this.shape_40}]},1).to({state:[{t:this.shape_41}]},1).to({state:[{t:this.shape_42}]},1).to({state:[{t:this.shape_43}]},1).to({state:[{t:this.shape_44}]},1).to({state:[{t:this.shape_45}]},1).to({state:[{t:this.shape_45}]},4).to({state:[{t:this.shape_44}]},1).to({state:[{t:this.shape_43}]},1).to({state:[{t:this.shape_42}]},1).to({state:[{t:this.shape_41}]},1).to({state:[{t:this.shape_40}]},1).to({state:[{t:this.shape_39}]},1).to({state:[{t:this.shape_38}]},1).to({state:[{t:this.shape_37}]},1).to({state:[{t:this.shape_36}]},1).to({state:[{t:this.shape_35}]},1).to({state:[{t:this.shape_34}]},1).to({state:[{t:this.shape_33}]},1).to({state:[{t:this.shape_32}]},1).to({state:[{t:this.shape_31}]},1).to({state:[{t:this.shape_30}]},1).to({state:[{t:this.shape_29}]},1).wait(3));

	// Layer 19
	this.instance_11 = new lib.Symbol6();
	this.instance_11.parent = this;
	this.instance_11.setTransform(567.4,269.4,1,1,42.2,0,0,33.6,2.6);

	this.timeline.addTween(cjs.Tween.get(this.instance_11).to({regX:33.7,rotation:15,x:536.1,y:374.8},8,cjs.Ease.get(-1)).to({rotation:0,x:542.7,y:417.6},8,cjs.Ease.get(1)).wait(4).to({rotation:15,x:536.1,y:374.8},8,cjs.Ease.get(-1)).to({regX:33.6,rotation:42.2,x:567.4,y:269.4},8,cjs.Ease.get(1)).wait(3));

	// Layer 22
	this.instance_12 = new lib.Symbol10();
	this.instance_12.parent = this;
	this.instance_12.setTransform(675.9,357.6,1,1,0,0,0,49.9,-3.8);

	this.timeline.addTween(cjs.Tween.get(this.instance_12).to({y:361.4},8,cjs.Ease.get(-1)).to({y:365.2},8,cjs.Ease.get(1)).wait(4).to({y:361.4},8,cjs.Ease.get(-1)).to({y:357.6},8,cjs.Ease.get(1)).wait(3));

	// Layer 20
	this.instance_13 = new lib.Symbol7();
	this.instance_13.parent = this;
	this.instance_13.setTransform(616.7,324.7,1,1,26.2,0,0,83,0);

	this.timeline.addTween(cjs.Tween.get(this.instance_13).to({rotation:3.2,x:611.9,y:358.9},8,cjs.Ease.get(-1)).to({rotation:0,x:612,y:375},8,cjs.Ease.get(1)).wait(4).to({rotation:3.2,x:611.9,y:358.9},8,cjs.Ease.get(-1)).to({rotation:26.2,x:616.7,y:324.7},8,cjs.Ease.get(1)).wait(3));

	// Layer 21
	this.instance_14 = new lib.Symbol8();
	this.instance_14.parent = this;
	this.instance_14.setTransform(630.9,337.9,1,1,-4.7,0,0,46,-4.6);

	this.timeline.addTween(cjs.Tween.get(this.instance_14).to({regY:-4.5,rotation:-1,y:360.8},8,cjs.Ease.get(-1)).to({rotation:0,x:631,y:366.5},8,cjs.Ease.get(1)).wait(4).to({rotation:-1,x:630.9,y:360.8},8,cjs.Ease.get(-1)).to({regY:-4.6,rotation:-4.7,y:337.9},8,cjs.Ease.get(1)).wait(3));

	// Layer 17
	this.instance_15 = new lib.Symbol4();
	this.instance_15.parent = this;
	this.instance_15.setTransform(646.9,344,1,1,34,0,0,128,96);

	this.timeline.addTween(cjs.Tween.get(this.instance_15).to({rotation:0,x:647,y:358.2},8,cjs.Ease.get(-1)).to({y:362},8,cjs.Ease.get(1)).wait(4).to({y:358.2},8,cjs.Ease.get(-1)).to({rotation:34,x:646.9,y:344},8,cjs.Ease.get(1)).wait(3));

	// Layer 29
	this.instance_16 = new lib.Symbol16();
	this.instance_16.parent = this;
	this.instance_16.setTransform(567.5,584.5,1.072,1.2,0,0,0,153.5,35);
	this.instance_16.alpha = 0.199;

	this.timeline.addTween(cjs.Tween.get(this.instance_16).to({scaleX:1,scaleY:1,x:578.5,y:587.5,alpha:1},16).wait(4).to({scaleX:1.07,scaleY:1.2,x:567.5,y:584.5,alpha:0.199},16).wait(3));

	// Layer 28
	this.instance_17 = new lib.bg_part();
	this.instance_17.parent = this;
	this.instance_17.setTransform(350.5,374.5);

	this.timeline.addTween(cjs.Tween.get(this.instance_17).wait(39));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(540,340,1149.2,680.5);
// library properties:
lib.properties = {
	id: 'F03766E562AAAA48BB838AE899817E3D',
	width: 1080,
	height: 680,
	fps: 30,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: [
		{src:"assets/images/leaf1.png", id:"leaf1"},
		{src:"assets/images/leaf10.png", id:"leaf10"},
		{src:"assets/images/leaf11.png", id:"leaf11"},
		{src:"assets/images/leaf22.png", id:"leaf2"},
		{src:"assets/images/leaf33.png", id:"leaf3"},
		{src:"assets/images/leaf4.png", id:"leaf4"},
		{src:"assets/images/leaf5.png", id:"leaf5"},
		{src:"assets/images/leaf6.png", id:"leaf6"},
		{src:"assets/images/leaf7.png", id:"leaf7"},
		{src:"assets/images/leaf8.png", id:"leaf8"},
		{src:"assets/images/leaf9.png", id:"leaf9"},
		{src:"assets/images/mouth9.png", id:"mouth1"},
		{src:"assets/images/pack9.png", id:"pack"},
		{src:"assets/images/shadow2.png", id:"shadow"},
		{src:"assets/images/teeth2.png", id:"teeth"}
	],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['F03766E562AAAA48BB838AE899817E3D'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}



})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;

$(function () {
    var canvas, stage, exportRoot, anim_container, dom_overlay_container, fnStartAnimation;
    function init() {
        canvas = document.getElementById("canvas9");
        anim_container = document.getElementById("animation_container9");
        dom_overlay_container = document.getElementById("dom_overlay_container9");
        var comp=AdobeAn.getComposition("F03766E562AAAA48BB838AE899817E3D");
        var lib=comp.getLibrary();
        var loader = new createjs.LoadQueue(false);
        loader.addEventListener("fileload", function(evt){handleFileLoad(evt,comp)});
        loader.addEventListener("complete", function(evt){handleComplete(evt,comp)});
        var lib=comp.getLibrary();
        loader.loadManifest(lib.properties.manifest);
    }
    function handleFileLoad(evt, comp) {
        var images=comp.getImages();
        if (evt && (evt.item.type == "image")) { images[evt.item.id] = evt.result; }
    }
    function handleComplete(evt,comp) {
        //This function is always called, irrespective of the content. You can use the variable "stage" after it is created in token create_stage.
        var lib=comp.getLibrary();
        var ss=comp.getSpriteSheet();
        var queue = evt.target;
        var ssMetadata = lib.ssMetadata;
        for(i=0; i<ssMetadata.length; i++) {
            ss[ssMetadata[i].name] = new createjs.SpriteSheet( {"images": [queue.getResult(ssMetadata[i].name)], "frames": ssMetadata[i].frames} )
        }
        exportRoot = new lib.Trees();
        stage = new lib.Stage(canvas);
        //Registers the "tick" event listener.
        fnStartAnimation = function() {
            stage.addChild(exportRoot);
            createjs.Ticker.setFPS(lib.properties.fps);
            createjs.Ticker.addEventListener("tick", stage);
        }
        //Code to support hidpi screens and responsive scaling.
        function makeResponsive(isResp, respDim, isScale, scaleType) {
            var lastW, lastH, lastS=1;
            window.addEventListener('resize', resizeCanvas);
            resizeCanvas();
            function resizeCanvas() {
                var w = lib.properties.width, h = lib.properties.height;
                var iw = window.innerWidth, ih=window.innerHeight;
                var pRatio = window.devicePixelRatio || 1, xRatio=iw/w, yRatio=ih/h, sRatio=1;
                if(isResp) {
                    if((respDim=='width'&&lastW==iw) || (respDim=='height'&&lastH==ih)) {
                        sRatio = lastS;
                    }
                    else if(!isScale) {
                        if(iw<w || ih<h)
                            sRatio = Math.min(xRatio, yRatio);
                    }
                    else if(scaleType==1) {
                        sRatio = Math.min(xRatio, yRatio);
                    }
                    else if(scaleType==2) {
                        sRatio = Math.max(xRatio, yRatio);
                    }
                }
                canvas.width = w*pRatio*sRatio;
                canvas.height = h*pRatio*sRatio;
                canvas.style.width = dom_overlay_container.style.width = anim_container.style.width =  w*sRatio+'px';
                canvas.style.height = anim_container.style.height = dom_overlay_container.style.height = h*sRatio+'px';
                stage.scaleX = pRatio*sRatio;
                stage.scaleY = pRatio*sRatio;
                lastW = iw; lastH = ih; lastS = sRatio;
                stage.tickOnUpdate = false;
                stage.update();
                stage.tickOnUpdate = true;
            }
        }
        makeResponsive(true,'both',true,2);
        AdobeAn.compositionLoaded(lib.properties.id);
        fnStartAnimation();
    }
    init();
});