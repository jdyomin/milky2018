$(function() {
  if ($('.js-habits-slider').length && $(window).width() > 767) {
    var habitsCarousel = new Swiper('.js-habits-slider', {
      speed: 400,
      direction: "vertical",
      autoHeight: true,
      slidesPerView: 4,
      navigation: {
        nextEl: '.js-habits-next',
        prevEl: '.js-habits-prev',
      }
    });
  }
  $('.js-habits-nav-item').on('click', function() {
    var target = $(this).data('target');
    $('.js-habits-tab[data-tab="'+target+'"]').addClass('active');
    $('.js-habits').addClass('opened');
  });
  $('.js-habits-back').on('click', function() {
    $('.js-habits-tab').removeClass('active');
    if ($(window).width() > 767) {
      $('.js-habits').removeClass('opened');
    }
  });
  $('.js-habits-start').on('click', function() {
    $('.js-habits').addClass('opened');
  });
});
