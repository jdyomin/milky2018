$(function() {
    $('.js-pas-validate').on('blur keyup keydown paste change input', function () {
        validateField($(this));
    });

    function validateField(field) {
        if (field.val().length >= 6) {
            $('.js-min-length').addClass('valid');
        } else {
            $('.js-min-length').removeClass('valid');
        }

        if (/^[A-Za-z0-9!"#$%&'()*+.\/:;<=>?@\[\\\]^_'{|}~-]+$/g.test(field.val())) {
            $('.js-english').addClass('valid');
        }else {
            $('.js-english').removeClass('valid');
        }

        validateForm(field);
    }

    function validateForm(input) {
        var form = $('.js-validate-form'),
            btn = form.find('.form__submit'),
            context = input.parent().find('.js-validate-block'),
            validEls = context.find('li.valid'),
            totalEls = context.find('li');

        if (validEls.length == totalEls.length) {
            btn.prop('disabled', '');
        } else {
            btn.attr('disabled', 'disabled');
        }
    }
});