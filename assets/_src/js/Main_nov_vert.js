(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [];


// symbols:



(lib.basket = function() {
	this.initialize(img.basket);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,586,428);


(lib.bg_vert = function() {
	this.initialize(img.bg_vert);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,721,455);


(lib.pack_bg_vert = function() {
	this.initialize(img.pack_bg_vert);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,486,328);// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.Symbol15 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.basket();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol15, new cjs.Rectangle(0,0,586,428), null);


(lib.Symbol13 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(11,1,1).p("AhfAgIC/g/");
	this.shape.setTransform(9.6,3.2);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol13, new cjs.Rectangle(-5.5,-5.5,30.3,17.4), null);


(lib.Symbol11 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AgvAwQgTgUAAgcQAAgbATgUQAUgUAbAAQAcAAAUAUQATAUAAAbQAAAcgTAUQgUATgcABQgbgBgUgTg");
	this.shape.setTransform(6.7,6.8);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol11, new cjs.Rectangle(0,0,13.5,13.5), null);


(lib.Symbol10 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AgvAwQgTgUAAgcQAAgbATgUQAUgUAbAAQAcAAAUAUQATAUAAAbQAAAcgTAUQgUATgcABQgbgBgUgTg");
	this.shape.setTransform(6.7,6.8);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol10, new cjs.Rectangle(0,0,13.5,13.5), null);


(lib.Symbol9 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#B7B7B7").s().p("AiMCMQg5g6gBhSQABhSA5g6QA6g5BSgBQBSABA6A5QA6A6AABSQAABSg6A6Qg6A6hSAAQhSAAg6g6g");
	this.shape.setTransform(19.9,19.9);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol9, new cjs.Rectangle(0,0,39.7,39.7), null);


(lib.Symbol7 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#B7B7B7").s().p("AiMCMQg5g6gBhSQABhSA5g6QA6g5BSgBQBSABA6A5QA6A6AABSQAABSg6A6Qg6A6hSAAQhSAAg6g6g");
	this.shape.setTransform(19.9,19.9);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol7, new cjs.Rectangle(0,0,39.7,39.7), null);


(lib.Symbol5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(11,1,1).p("AjEhPQAIBEA1AqQAwAmBHAJQBCAIA8gUQA+gVAZgs");
	this.shape.setTransform(33.5,48.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(11,1,1).p("AjLhMQAOBFA3AlQAyAiBHAKQBCAJA7gRQA8gSAggt");
	this.shape_1.setTransform(33.5,47.8);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(11,1,1).p("AjRhKQAUBHA4AhQA1AfBGAKQBCAKA5gOQA7gPAmgu");
	this.shape_2.setTransform(33.5,47.4);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(11,1,1).p("AjWhIQAZBIA6AeQA2AcBFAKQBDAKA4gLQA5gMAsgv");
	this.shape_3.setTransform(33.6,47.1);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(11,1,1).p("AjbhGQAeBHA6AcQA4AZBFALQBDALA3gJQA3gKAxgw");
	this.shape_4.setTransform(33.6,46.9);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(11,1,1).p("AjghFQAiBIA7AaQA6AXBFAMQBDAKA1gHQA3gHA1gx");
	this.shape_5.setTransform(33.6,46.7);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(11,1,1).p("AjjhEQAlBJA8AXQA7AWBEALQBDAMA2gGQA1gGA5gx");
	this.shape_6.setTransform(33.6,46.5);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(11,1,1).p("AjmhDQAoBJA9AVQA7AVBFAMQBCALA1gEQA1gFA7gy");
	this.shape_7.setTransform(33.6,46.4);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(11,1,1).p("AjnhCQApBJA9AUQA8AUBFAMQBCALA0gDQA1gEA9gy");
	this.shape_8.setTransform(33.6,46.3);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(11,1,1).p("AjohCQAqBJA9AUQA9ATBEAMQBDAMA0gDQA0gDA+gz");
	this.shape_9.setTransform(33.6,46.3);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(11,1,1).p("AjphCQArBJA9AUQA9ATBEAMQBDALA0gCQA0gDA/gz");
	this.shape_10.setTransform(33.6,46.3);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(11,1,1).p("AjihEQAlBIA7AYQA7AXBEALQBDALA1gGQA2gGA4gy");
	this.shape_11.setTransform(33.6,46.6);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(11,1,1).p("AjWhIQAZBHA5AeQA3AdBFAKQBDAKA4gMQA5gMArgv");
	this.shape_12.setTransform(33.6,47.2);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(11,1,1).p("AjNhLQAQBFA4AkQAzAhBGAKQBDAJA6gQQA7gQAigu");
	this.shape_13.setTransform(33.5,47.7);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(11,1,1).p("AjKhNQANBFA3AmQAyAjBGAKQBDAIA6gRQA9gSAfgt");
	this.shape_14.setTransform(33.5,47.9);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(11,1,1).p("AjHhOQAKBFA2AnQAyAkBGAKQBCAJA8gTQA9gUAcgs");
	this.shape_15.setTransform(33.5,48);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(11,1,1).p("AjFhPQAJBFA2AoQAwAmBGAJQBDAIA8gTQA9gVAags");
	this.shape_16.setTransform(33.5,48.1);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(11,1,1).p("AjEhPQAIBEA1AqQAwAlBHAJQBCAJA8gUQA+gVAZgs");
	this.shape_17.setTransform(33.5,48.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},11).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_10}]},43).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape}]},1).wait(36));

	// Layer 3
	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#000000").ss(11,1,1).p("AhxAnQAIgxAngaQAigXAsAEQAtAEAdAdQAfAegEAx");
	this.shape_18.setTransform(11.4,5.7);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#000000").ss(11,1,1).p("AhxApQAIg0AngaQAigZAsAEQAtAFAdAeQAfAggEAy");
	this.shape_19.setTransform(11.4,4.7);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#000000").ss(11,1,1).p("AhxAqQAIg1AngcQAigZAsAEQAtAEAdAgQAfAhgEA0");
	this.shape_20.setTransform(11.4,3.8);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#000000").ss(11,1,1).p("AhxArQAIg3AngcQAigaAsAEQAtAFAdAgQAfAigEA2");
	this.shape_21.setTransform(11.4,3);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#000000").ss(11,1,1).p("AhxAtQAIg5AngdQAigbAsAFQAtAFAdAhQAfAjgEA3");
	this.shape_22.setTransform(11.4,2.3);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#000000").ss(11,1,1).p("AhxAuQAIg6AngeQAigbAsAEQAtAFAdAiQAfAkgEA5");
	this.shape_23.setTransform(11.4,1.8);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#000000").ss(11,1,1).p("AhxAuQAIg7AngeQAigcAsAFQAtAFAdAiQAfAlgEA5");
	this.shape_24.setTransform(11.4,1.3);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f().s("#000000").ss(11,1,1).p("AhxAvQAIg8AngeQAigdAsAFQAtAFAdAjQAfAlgEA6");
	this.shape_25.setTransform(11.4,1);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f().s("#000000").ss(11,1,1).p("AhxAvQAIg8AngeQAigdAsAFQAtAFAdAjQAfAlgEA7");
	this.shape_26.setTransform(11.4,0.7);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f().s("#000000").ss(11,1,1).p("AhxAwQAIg9AngfQAigdAsAFQAtAFAdAkQAfAlgEA8");
	this.shape_27.setTransform(11.4,0.5);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f().s("#000000").ss(11,1,1).p("AhxAuQAIg6AngeQAigcAsAFQAtAEAdAjQAfAkgEA5");
	this.shape_28.setTransform(11.4,1.5);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f().s("#000000").ss(11,1,1).p("AhxArQAIg2AngdQAigaAsAFQAtAEAdAgQAfAigEA2");
	this.shape_29.setTransform(11.4,3.1);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f().s("#000000").ss(11,1,1).p("AhxApQAIg0AngbQAigZAsAFQAtAEAdAfQAfAggEAz");
	this.shape_30.setTransform(11.4,4.4);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f().s("#000000").ss(11,1,1).p("AhxAoQAIgyAngbQAigYAsAEQAtAEAdAeQAfAggEAy");
	this.shape_31.setTransform(11.4,4.8);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f().s("#000000").ss(11,1,1).p("AhxAoQAIgyAngaQAigYAsAEQAtAEAdAeQAfAfgEAx");
	this.shape_32.setTransform(11.4,5.2);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f().s("#000000").ss(11,1,1).p("AhxAnQAIgxAngaQAigYAsAEQAtAEAdAdQAfAfgEAx");
	this.shape_33.setTransform(11.4,5.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_18,p:{y:5.7}}]}).to({state:[{t:this.shape_18,p:{y:5.7}}]},11).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_27}]},1).to({state:[{t:this.shape_27}]},1).to({state:[{t:this.shape_27}]},43).to({state:[{t:this.shape_28}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_29}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_30}]},1).to({state:[{t:this.shape_31}]},1).to({state:[{t:this.shape_32}]},1).to({state:[{t:this.shape_33}]},1).to({state:[{t:this.shape_18,p:{y:5.6}}]},1).to({state:[{t:this.shape_18,p:{y:5.7}}]},1).wait(36));

	// Layer 4
	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f().s("#000000").ss(11,1,1).p("AiLgDQAsg9A5gPQAxgNAvAYQAtAXAXArQAXAsgOAr");
	this.shape_34.setTransform(70.7,20.3);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f().s("#000000").ss(11,1,1).p("AiLgDQAshAA5gPQAxgOAvAZQAtAYAXAtQAXAugOAs");
	this.shape_35.setTransform(70.7,20);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f().s("#000000").ss(11,1,1).p("AiLgDQAshDA5gQQAxgOAvAaQAtAZAXAvQAXAvgOAu");
	this.shape_36.setTransform(70.7,19.7);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f().s("#000000").ss(11,1,1).p("AiLgDQAshFA5gQQAxgPAvAbQAtAZAXAxQAXAxgOAv");
	this.shape_37.setTransform(70.7,19.4);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f().s("#000000").ss(11,1,1).p("AiLgEQAshGA5gQQAxgPAvAbQAtAaAXAyQAXAygOAx");
	this.shape_38.setTransform(70.7,19.1);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f().s("#000000").ss(11,1,1).p("AiLgEQAshHA5gRQAxgQAvAcQAtAbAXAzQAXAzgOAy");
	this.shape_39.setTransform(70.7,18.9);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f().s("#000000").ss(11,1,1).p("AiLgEQAshJA5gRQAxgQAvAdQAtAaAXA0QAXA1gOAy");
	this.shape_40.setTransform(70.7,18.8);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f().s("#000000").ss(11,1,1).p("AiLgEQAshKA5gRQAxgQAvAdQAtAbAXA1QAXA1gOAz");
	this.shape_41.setTransform(70.7,18.6);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f().s("#000000").ss(11,1,1).p("AiLgEQAshKA5gSQAxgQAvAdQAtAbAXA2QAXA1gOA0");
	this.shape_42.setTransform(70.7,18.5);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f().s("#000000").ss(11,1,1).p("AiLgEQAshLA5gSQAxgQAvAeQAtAbAXA1QAXA2gOA0");
	this.shape_43.setTransform(70.7,18.5);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f().s("#000000").ss(11,1,1).p("AiLgEQAshLA5gSQAxgQAvAeQAtAbAXA2QAXA2gOA0");
	this.shape_44.setTransform(70.7,18.4);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f().s("#000000").ss(11,1,1).p("AiLgEQAshIA5gSQAxgPAvAcQAtAbAXAzQAXA1gOAy");
	this.shape_45.setTransform(70.7,18.8);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.f().s("#000000").ss(11,1,1).p("AiLgEQAshEA5gQQAxgOAvAaQAtAZAXAxQAXAxgOAv");
	this.shape_46.setTransform(70.7,19.4);

	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.f().s("#000000").ss(11,1,1).p("AiLgDQAshBA5gPQAxgOAvAZQAtAYAXAuQAXAugOAt");
	this.shape_47.setTransform(70.7,19.9);

	this.shape_48 = new cjs.Shape();
	this.shape_48.graphics.f().s("#000000").ss(11,1,1).p("AiLgDQAshAA5gPQAxgNAvAZQAtAXAXAtQAXAugOAs");
	this.shape_48.setTransform(70.7,20);

	this.shape_49 = new cjs.Shape();
	this.shape_49.graphics.f().s("#000000").ss(11,1,1).p("AiLgDQAsg/A5gPQAxgNAvAZQAtAXAXAsQAXAtgOAr");
	this.shape_49.setTransform(70.7,20.2);

	this.shape_50 = new cjs.Shape();
	this.shape_50.graphics.f().s("#000000").ss(11,1,1).p("AiLgDQAsg+A5gPQAxgNAvAYQAtAXAXAsQAXAsgOAr");
	this.shape_50.setTransform(70.7,20.3);

	this.shape_51 = new cjs.Shape();
	this.shape_51.graphics.f().s("#000000").ss(11,1,1).p("AiLgDQAsg+A5gOQAxgNAvAYQAtAWAXAsQAXAsgOAr");
	this.shape_51.setTransform(70.7,20.3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_34}]}).to({state:[{t:this.shape_34}]},11).to({state:[{t:this.shape_35}]},1).to({state:[{t:this.shape_36}]},1).to({state:[{t:this.shape_37}]},1).to({state:[{t:this.shape_38}]},1).to({state:[{t:this.shape_39}]},1).to({state:[{t:this.shape_40}]},1).to({state:[{t:this.shape_41}]},1).to({state:[{t:this.shape_42}]},1).to({state:[{t:this.shape_43}]},1).to({state:[{t:this.shape_44}]},1).to({state:[{t:this.shape_44}]},43).to({state:[{t:this.shape_45}]},1).to({state:[{t:this.shape_38}]},1).to({state:[{t:this.shape_46}]},1).to({state:[{t:this.shape_36}]},1).to({state:[{t:this.shape_47}]},1).to({state:[{t:this.shape_48}]},1).to({state:[{t:this.shape_49}]},1).to({state:[{t:this.shape_50}]},1).to({state:[{t:this.shape_51}]},1).to({state:[{t:this.shape_34}]},1).wait(36));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-5.5,-5.5,95.8,67.2);


(lib.Symbol4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(11,1,1).p("AAKg+IgTB9");
	this.shape.setTransform(15.4,18.9);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	// Layer 1
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(11,1,1).p("ABIBKQA0jUjNBf");
	this.shape_1.setTransform(8.2,7.3);

	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol4, new cjs.Rectangle(-5.5,-5.5,27.5,36.2), null);


(lib.Symbol3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(11,1,1).p("ABRgzQi+hRAiDP");
	this.shape.setTransform(-5.9,8.3);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(11,1,1).p("AhYA3QgMijC9BR");
	this.shape_1.setTransform(-6.7,5.9);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(11,1,1).p("AhhAnQAFh+C+BR");
	this.shape_2.setTransform(-7.6,3.8);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(11,1,1).p("AhoAaQAUhgC9BR");
	this.shape_3.setTransform(-8.3,2.1);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(11,1,1).p("AhuANQAfhIC+BQ");
	this.shape_4.setTransform(-8.9,1);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(11,1,1).p("AhyAAQAog4C9BQ");
	this.shape_5.setTransform(-9.3,0.7);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(11,1,1).p("Ah1gIQAtgvC+BR");
	this.shape_6.setTransform(-9.6,0.6);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(11,1,1).p("AB3AbQi+hRgvAr");
	this.shape_7.setTransform(-9.6,0.5);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(11,1,1).p("Ah1gJQAtgtC+BQ");
	this.shape_8.setTransform(-9.6,0.5);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(11,1,1).p("AhzgCQApg2C+BR");
	this.shape_9.setTransform(-9.4,0.7);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(11,1,1).p("AhwAHQAjhBC+BR");
	this.shape_10.setTransform(-9.1,0.9);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(11,1,1).p("AhsAWQAbhUC+BR");
	this.shape_11.setTransform(-8.6,1.3);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(11,1,1).p("AhmAfQAPhrC+BR");
	this.shape_12.setTransform(-8.1,2.7);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(11,1,1).p("AhfArQABiHC+BR");
	this.shape_13.setTransform(-7.4,4.3);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(11,1,1).p("AhWA5QgPioC9BR");
	this.shape_14.setTransform(-6.6,6.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},4).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},25).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape}]},1).wait(11));
	this.timeline.addTween(cjs.Tween.get(this.shape).wait(4).to({_off:true},1).wait(14).to({_off:false},0).wait(25).to({_off:true},1).wait(14).to({_off:false},0).wait(11));

	// Layer 1
	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(11,1,1).p("AAlg8QhWhMAPDZ");
	this.shape_15.setTransform(3.7,8.1);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(11,1,1).p("AgrA7QAAipBXBM");
	this.shape_16.setTransform(3.1,5.4);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(11,1,1).p("AgxAoQANh/BXBM");
	this.shape_17.setTransform(2.4,3.1);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#000000").ss(11,1,1).p("Ag3AZQAZhbBWBL");
	this.shape_18.setTransform(1.9,1);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#000000").ss(11,1,1).p("Ag8AIQAig+BXBL");
	this.shape_19.setTransform(1.4,-0.1);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#000000").ss(11,1,1).p("Ag/gKQApgoBWBL");
	this.shape_20.setTransform(1,-0.5);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#000000").ss(11,1,1).p("AhCgWQAugYBXBL");
	this.shape_21.setTransform(0.8,-0.9);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#000000").ss(11,1,1).p("AhDgdQAxgOBWBL");
	this.shape_22.setTransform(0.6,-1.3);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#000000").ss(11,1,1).p("ABFAhQhXhLgyAL");
	this.shape_23.setTransform(0.6,-1.4);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#000000").ss(11,1,1).p("Ag9ABQAkg2BXBL");
	this.shape_24.setTransform(1.3,-0.3);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f().s("#000000").ss(11,1,1).p("Ag3AaQAZhcBWBL");
	this.shape_25.setTransform(1.9,1.1);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f().s("#000000").ss(11,1,1).p("AgyAnQAOh9BXBM");
	this.shape_26.setTransform(2.4,3);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f().s("#000000").ss(11,1,1).p("AgtA0QAFiZBWBM");
	this.shape_27.setTransform(2.8,4.6);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f().s("#000000").ss(11,1,1).p("AgqA+QgCiwBXBM");
	this.shape_28.setTransform(3.2,5.9);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f().s("#000000").ss(11,1,1).p("AgmBGQgIjCBWBM");
	this.shape_29.setTransform(3.4,6.8);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f().s("#000000").ss(11,1,1).p("AgkBMQgMjPBWBM");
	this.shape_30.setTransform(3.6,7.5);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f().s("#000000").ss(11,1,1).p("AgjBQQgOjXBXBM");
	this.shape_31.setTransform(3.7,7.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_15}]}).to({state:[{t:this.shape_15}]},4).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_27}]},1).to({state:[{t:this.shape_28}]},1).to({state:[{t:this.shape_29}]},1).to({state:[{t:this.shape_30}]},1).to({state:[{t:this.shape_31}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_15}]},23).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_27}]},1).to({state:[{t:this.shape_28}]},1).to({state:[{t:this.shape_29}]},1).to({state:[{t:this.shape_30}]},1).to({state:[{t:this.shape_31}]},1).to({state:[{t:this.shape_15}]},1).wait(9));
	this.timeline.addTween(cjs.Tween.get(this.shape_15).wait(4).to({_off:true},1).wait(16).to({_off:false},0).wait(23).to({_off:true},1).wait(16).to({_off:false},0).wait(9));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-19.4,-5.5,32.4,27.2);


(lib.Symbol2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(11,1,1).p("ABjA5QAbjSjiCw");
	this.shape.setTransform(10.1,5.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(11,1,1).p("AhhAOQDUitgTDd");
	this.shape_1.setTransform(10.4,5.3);
	this.shape_1._off = true;

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(11,1,1).p("AhdAGQDFirgLDo");
	this.shape_2.setTransform(10.7,4.8);
	this.shape_2._off = true;

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(11,1,1).p("AhagCQC4ipgEDz");
	this.shape_3.setTransform(11,4.3);
	this.shape_3._off = true;

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(11,1,1).p("AhWgLQCpinAED/");
	this.shape_4.setTransform(11.3,3.8);
	this.shape_4._off = true;

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(11,1,1).p("ABUBTQgMkKibCk");
	this.shape_5.setTransform(11.6,3.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},21).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},32).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape}]},1).wait(17));
	this.timeline.addTween(cjs.Tween.get(this.shape).wait(21).to({_off:true},1).wait(9).to({_off:false},0).to({_off:true},1).wait(9).to({_off:false},0).wait(32).to({_off:true},1).wait(9).to({_off:false},0).to({_off:true},1).wait(9).to({_off:false},0).wait(17));
	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(22).to({_off:false},0).to({_off:true},1).wait(7).to({_off:false},0).to({_off:true},1).wait(1).to({_off:false},0).to({_off:true},1).wait(7).to({_off:false},0).to({_off:true},1).wait(33).to({_off:false},0).to({_off:true},1).wait(7).to({_off:false},0).to({_off:true},1).wait(1).to({_off:false},0).to({_off:true},1).wait(7).to({_off:false},0).to({_off:true},1).wait(17));
	this.timeline.addTween(cjs.Tween.get(this.shape_2).wait(23).to({_off:false},0).to({_off:true},1).wait(5).to({_off:false},0).to({_off:true},1).wait(3).to({_off:false},0).to({_off:true},1).wait(5).to({_off:false},0).to({_off:true},1).wait(35).to({_off:false},0).to({_off:true},1).wait(5).to({_off:false},0).to({_off:true},1).wait(3).to({_off:false},0).to({_off:true},1).wait(5).to({_off:false},0).to({_off:true},1).wait(18));
	this.timeline.addTween(cjs.Tween.get(this.shape_3).wait(24).to({_off:false},0).to({_off:true},1).wait(3).to({_off:false},0).to({_off:true},1).wait(5).to({_off:false},0).to({_off:true},1).wait(3).to({_off:false},0).to({_off:true},1).wait(37).to({_off:false},0).to({_off:true},1).wait(3).to({_off:false},0).to({_off:true},1).wait(5).to({_off:false},0).to({_off:true},1).wait(3).to({_off:false},0).to({_off:true},1).wait(19));
	this.timeline.addTween(cjs.Tween.get(this.shape_4).wait(25).to({_off:false},0).to({_off:true},1).wait(1).to({_off:false},0).to({_off:true},1).wait(7).to({_off:false},0).to({_off:true},1).wait(1).to({_off:false},0).to({_off:true},1).wait(39).to({_off:false},0).to({_off:true},1).wait(1).to({_off:false},0).to({_off:true},1).wait(7).to({_off:false},0).to({_off:true},1).wait(1).to({_off:false},0).to({_off:true},1).wait(20));

	// Layer 2
	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(11,1,1).p("AArBiQBEkeitCG");
	this.shape_6.setTransform(11.3,-7.3);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(11,1,1).p("Ag+g3QCtiEhDEc");
	this.shape_7.setTransform(11.2,-7.2);
	this.shape_7._off = true;

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(11,1,1).p("Ag+g4QCqh/g9EY");
	this.shape_8.setTransform(10.8,-7);
	this.shape_8._off = true;

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(11,1,1).p("Ag/g6QCoh4g1ES");
	this.shape_9.setTransform(10.2,-6.7);
	this.shape_9._off = true;

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(11,1,1).p("AhBg9QCkhtgpEI");
	this.shape_10.setTransform(9.4,-6.2);
	this.shape_10._off = true;

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(11,1,1).p("ABBBcQAaj9ifBg");
	this.shape_11.setTransform(8.5,-5.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_6}]}).to({state:[{t:this.shape_6}]},21).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_6}]},31).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_6}]},1).wait(16));
	this.timeline.addTween(cjs.Tween.get(this.shape_6).wait(22).to({_off:true},1).wait(9).to({_off:false},0).to({_off:true},1).wait(9).to({_off:false},0).wait(32).to({_off:true},1).wait(9).to({_off:false},0).to({_off:true},1).wait(9).to({_off:false},0).wait(16));
	this.timeline.addTween(cjs.Tween.get(this.shape_7).wait(23).to({_off:false},0).to({_off:true},1).wait(7).to({_off:false},0).to({_off:true},1).wait(1).to({_off:false},0).to({_off:true},1).wait(7).to({_off:false},0).to({_off:true},1).wait(33).to({_off:false},0).to({_off:true},1).wait(7).to({_off:false},0).to({_off:true},1).wait(1).to({_off:false},0).to({_off:true},1).wait(7).to({_off:false},0).to({_off:true},1).wait(16));
	this.timeline.addTween(cjs.Tween.get(this.shape_8).wait(24).to({_off:false},0).to({_off:true},1).wait(5).to({_off:false},0).to({_off:true},1).wait(3).to({_off:false},0).to({_off:true},1).wait(5).to({_off:false},0).to({_off:true},1).wait(35).to({_off:false},0).to({_off:true},1).wait(5).to({_off:false},0).to({_off:true},1).wait(3).to({_off:false},0).to({_off:true},1).wait(5).to({_off:false},0).to({_off:true},1).wait(17));
	this.timeline.addTween(cjs.Tween.get(this.shape_9).wait(25).to({_off:false},0).to({_off:true},1).wait(3).to({_off:false},0).to({_off:true},1).wait(5).to({_off:false},0).to({_off:true},1).wait(3).to({_off:false},0).to({_off:true},1).wait(37).to({_off:false},0).to({_off:true},1).wait(3).to({_off:false},0).to({_off:true},1).wait(5).to({_off:false},0).to({_off:true},1).wait(3).to({_off:false},0).to({_off:true},1).wait(18));
	this.timeline.addTween(cjs.Tween.get(this.shape_10).wait(26).to({_off:false},0).to({_off:true},1).wait(1).to({_off:false},0).to({_off:true},1).wait(7).to({_off:false},0).to({_off:true},1).wait(1).to({_off:false},0).to({_off:true},1).wait(39).to({_off:false},0).to({_off:true},1).wait(1).to({_off:false},0).to({_off:true},1).wait(7).to({_off:false},0).to({_off:true},1).wait(1).to({_off:false},0).to({_off:true},1).wait(19));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-5.5,-22.5,31.3,39.6);


(lib.Symbol2_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#F51431").s().p("AAAAqQgRAAgNgMQgMgMAAgSQAAgQAMgNQANgNARAAIADAAQAPABALAMQANANAAAQQAAASgNAMQgLAMgPAAIgDAAg");
	this.shape_12.setTransform(4.3,4.3);

	this.timeline.addTween(cjs.Tween.get(this.shape_12).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol2_1, new cjs.Rectangle(0,0,8.5,8.5), null);


(lib.Symbol14 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Symbol 2
	this.instance = new lib.Symbol2();
	this.instance.parent = this;
	this.instance.setTransform(18.4,52,1,1,0,0,0,18.4,10.1);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(21).to({x:18.7,y:30.1},20,cjs.Ease.get(1)).wait(32).to({x:18.4,y:52},20,cjs.Ease.get(1)).wait(17));

	// Layer 3
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(11,1,1).p("AG2lfQgXDNhGCiQhKCrhvBXQh3BeiQgTQifgWivif");
	this.shape.setTransform(62.3,35.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(11,1,1).p("Am1CZQCqClCaAYQCNAWB4hcQBwhVBNipQBKigAbjL");
	this.shape_1.setTransform(62.3,34.5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(11,1,1).p("Am0CLQCkCrCWAaQCJAZB5hZQBxhUBRinQBMieAfjL");
	this.shape_2.setTransform(62.2,33.9);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(11,1,1).p("AmzB9QCfCxCRAcQCHAbB5hXQByhRBTimQBQidAijK");
	this.shape_3.setTransform(62.2,33.3);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(11,1,1).p("AmzBxQCbC2COAeQCDAcB5hUQBzhQBXikQBSibAmjJ");
	this.shape_4.setTransform(62.2,32.8);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(11,1,1).p("AmyBlQCWC7CKAgQCBAeB5hSQB1hOBZiiQBUiaApjJ");
	this.shape_5.setTransform(62.1,32.2);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(11,1,1).p("AmyBaQCTC/CGAiQB/AgB5hQQB2hNBbihQBXiYAsjI");
	this.shape_6.setTransform(62.1,31.8);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(11,1,1).p("AmxBQQCODDCDAjQB9AiB6hOQB2hLBeigQBZiXAvjH");
	this.shape_7.setTransform(62.1,31.3);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(11,1,1).p("AmxBGQCMDHB+AlQB7AjB6hMQB4hKBfieQBbiXAyjG");
	this.shape_8.setTransform(62.1,30.9);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(11,1,1).p("AmxA9QCJDLB8AmQB4AlB7hLQB4hJBiidQBciVA1jG");
	this.shape_9.setTransform(62,30.6);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(11,1,1).p("AmwA1QCFDOB6AnQB2AmB7hJQB5hHBjidQBfiUA2jG");
	this.shape_10.setTransform(62,30.2);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(11,1,1).p("AmwAuQCCDRB4AoQB1AoB7hIQB6hHBlibQBgiTA4jG");
	this.shape_11.setTransform(62,29.9);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(11,1,1).p("AmwAnQCBDUB1ApQBzApB7hHQB7hGBmiaQBiiTA6jF");
	this.shape_12.setTransform(62,29.6);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(11,1,1).p("AmwAhQB/DWBzAqQByAqB8hGQB6hFBpiaQBiiSA8jE");
	this.shape_13.setTransform(62,29.4);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(11,1,1).p("AmwAcQB9DYByArQBxArB7hFQB7hEBpiaQBkiRA+jE");
	this.shape_14.setTransform(62,29.2);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(11,1,1).p("AmvAYQB7DaBwArQBwArB8hEQB7hEBriYQBkiRA+jE");
	this.shape_15.setTransform(62,29);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(11,1,1).p("AmvAVQB5DbBwAsQBvArB8hDQB7hDBsiZQBkiQBAjE");
	this.shape_16.setTransform(62,28.9);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(11,1,1).p("AmvASQB5DcBvAsQBuAsB8hCQB8hDBriYQBmiQBAjE");
	this.shape_17.setTransform(61.9,28.8);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#000000").ss(11,1,1).p("AmvAQQB4DdBuAtQBuAsB8hCQB8hDBsiYQBmiQBBjD");
	this.shape_18.setTransform(61.9,28.7);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#000000").ss(11,1,1).p("AmvAPQB4DdBuAtQBtAtB8hDQB9hCBriYQBniPBBjE");
	this.shape_19.setTransform(61.9,28.6);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#000000").ss(11,1,1).p("AGwkqQhBDEhnCPQhsCYh8BCQh8BChugsQhtgth4je");
	this.shape_20.setTransform(61.9,28.6);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#000000").ss(11,1,1).p("AmwAeQB9DXBzArQBxAqB8hFQB7hEBoiaQBjiRA+jF");
	this.shape_21.setTransform(62,29.2);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#000000").ss(11,1,1).p("AmwAsQCCDSB3AoQB0AoB7hIQB6hGBmibQBgiTA5jF");
	this.shape_22.setTransform(62,29.8);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#000000").ss(11,1,1).p("AmxA6QCHDMB8AmQB3AmB6hKQB5hIBjidQBdiVA2jG");
	this.shape_23.setTransform(62,30.4);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#000000").ss(11,1,1).p("AmyBSQCQDCCDAjQB9AiB6hPQB2hLBeigQBYiYAvjH");
	this.shape_24.setTransform(62.1,31.4);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f().s("#000000").ss(11,1,1).p("AmyBdQCUC+CHAhQB/AgB6hRQB1hNBbihQBWiZArjI");
	this.shape_25.setTransform(62.1,31.9);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f().s("#000000").ss(11,1,1).p("AmzBoQCYC5CKAgQCCAeB4hTQB1hOBZijQBTiaAqjJ");
	this.shape_26.setTransform(62.2,32.3);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f().s("#000000").ss(11,1,1).p("AmzB6QCeCyCQAdQCFAbB5hWQBzhRBUilQBRicAjjK");
	this.shape_27.setTransform(62.2,33.1);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f().s("#000000").ss(11,1,1).p("AmzCCQCgCvCTAbQCHAaB5hXQByhSBTimQBOieAijK");
	this.shape_28.setTransform(62.2,33.5);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f().s("#000000").ss(11,1,1).p("Am0CJQCkCsCVAaQCIAZB6hZQBxhTBQinQBOieAfjL");
	this.shape_29.setTransform(62.2,33.8);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f().s("#000000").ss(11,1,1).p("Am0CQQCmCpCXAZQCKAYB5haQBwhUBQioQBLifAejL");
	this.shape_30.setTransform(62.2,34.1);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f().s("#000000").ss(11,1,1).p("Am1CVQCoCoCaAYQCLAWB5hbQBwhUBOipQBKifAdjM");
	this.shape_31.setTransform(62.3,34.3);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f().s("#000000").ss(11,1,1).p("Am1CaQCqClCbAYQCMAVB4hbQBxhWBNipQBJigAbjM");
	this.shape_32.setTransform(62.3,34.6);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f().s("#000000").ss(11,1,1).p("Am1CfQCrCjCdAXQCOAVB4hdQBuhVBNiqQBJihAZjM");
	this.shape_33.setTransform(62.3,34.7);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f().s("#000000").ss(11,1,1).p("Am1ChQCtCiCdAXQCOAUB4hdQBwhWBLiqQBHiiAZjM");
	this.shape_34.setTransform(62.3,34.9);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f().s("#000000").ss(11,1,1).p("Am1CkQCuChCdAWQCQAUB3heQBvhWBLirQBHihAYjN");
	this.shape_35.setTransform(62.3,35);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f().s("#000000").ss(11,1,1).p("Am1CmQCuCgCfAWQCPAUB4hfQBvhWBKirQBHiiAXjM");
	this.shape_36.setTransform(62.3,35.1);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f().s("#000000").ss(11,1,1).p("Am1CnQCvCgCeAVQCQAUB4heQBuhXBLirQBGiiAXjN");
	this.shape_37.setTransform(62.3,35.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},21).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_20}]},32).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_27}]},1).to({state:[{t:this.shape_28}]},1).to({state:[{t:this.shape_29}]},1).to({state:[{t:this.shape_30}]},1).to({state:[{t:this.shape_31}]},1).to({state:[{t:this.shape_32}]},1).to({state:[{t:this.shape_33}]},1).to({state:[{t:this.shape_34}]},1).to({state:[{t:this.shape_35}]},1).to({state:[{t:this.shape_36}]},1).to({state:[{t:this.shape_37}]},1).to({state:[{t:this.shape}]},1).wait(17));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-5.5,-5.5,117.1,81.4);


(lib.Symbol12 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Symbol 6 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("ABcCMQg5g6AAhSQAAhRA5g7QA7g6BSAAQBSAAA6A6QA7A7AABRQAABSg7A6Qg6A7hSAAQhSAAg7g7g");
	mask.setTransform(43.2,18);

	// Symbol 7
	this.instance = new lib.Symbol7();
	this.instance.parent = this;
	this.instance.setTransform(70,63,1,1,0,0,0,19.9,19.9);

	var maskedShapeInstanceList = [this.instance];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(10).to({x:69,y:57},0).wait(1).to({x:68.4,y:53.4},0).wait(1).to({x:67.9,y:50.8},0).wait(1).to({x:67.6,y:48.9},0).wait(1).to({x:67.4,y:47.4},0).wait(1).to({x:67.2,y:46.3},0).wait(1).to({x:67,y:45.4},0).wait(1).to({x:66.9,y:44.7},0).wait(1).to({x:66.8,y:44.1},0).wait(1).to({x:66.7,y:43.7},0).wait(1).to({y:43.3},0).wait(1).to({x:66.6,y:43.1},0).wait(1).to({y:42.9},0).wait(1).to({y:42.8},0).wait(1).to({y:42.7},0).wait(37).to({x:71.8,y:63},8,cjs.Ease.get(1)).wait(6));

	// Symbol 8 (mask)
	var mask_1 = new cjs.Shape();
	mask_1._off = true;
	mask_1.graphics.p("AiMCMQg5g6gBhSQABhSA5g6QA6g5BSgBQBSABA6A5QA6A6AABSQAABSg6A6Qg6A6hSAAQhSAAg6g6g");
	mask_1.setTransform(19.9,19.9);

	// Symbol 9
	this.instance_1 = new lib.Symbol9();
	this.instance_1.parent = this;
	this.instance_1.setTransform(23.5,65.1,1,1,0,0,0,19.9,19.9);

	var maskedShapeInstanceList = [this.instance_1];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask_1;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(10).to({x:22.5,y:59.2},0).wait(1).to({x:21.9,y:55.5},0).wait(1).to({x:21.4,y:53},0).wait(1).to({x:21.1,y:51},0).wait(1).to({x:20.9,y:49.6},0).wait(1).to({x:20.7,y:48.4},0).wait(1).to({x:20.5,y:47.5},0).wait(1).to({x:20.4,y:46.8},0).wait(1).to({x:20.3,y:46.3},0).wait(1).to({x:20.2,y:45.8},0).wait(1).to({y:45.5},0).wait(1).to({x:20.1,y:45.2},0).wait(1).to({y:45},0).wait(1).to({y:44.9},0).wait(38).to({x:25.3,y:65.1},8,cjs.Ease.get(1)).wait(6));

	// Symbol 11
	this.instance_2 = new lib.Symbol11();
	this.instance_2.parent = this;
	this.instance_2.setTransform(61.2,21.7,1,1,0,0,0,6.7,6.8);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(24).to({x:64.7,y:19.9},7,cjs.Ease.get(1)).wait(3).to({x:61.2,y:21.7},7,cjs.Ease.get(1)).wait(3).to({x:64.7,y:19.9},7,cjs.Ease.get(1)).wait(3).to({x:61.2,y:21.7},7,cjs.Ease.get(1)).wait(14));

	// Symbol 10
	this.instance_3 = new lib.Symbol10();
	this.instance_3.parent = this;
	this.instance_3.setTransform(12.3,23.1,1,1,0,0,0,6.7,6.8);

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(24).to({x:15.8,y:21.3},7,cjs.Ease.get(1)).wait(3).to({x:12.3,y:23.1},7,cjs.Ease.get(1)).wait(3).to({x:15.8,y:21.3},7,cjs.Ease.get(1)).wait(3).to({x:12.3,y:23.1},7,cjs.Ease.get(1)).wait(14));

	// Layer 8
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AiMCMQg5g6gBhSQABhSA5g6QA6g5BSgBQBSABA6A5QA6A6AABSQAABSg6A6Qg6A6hSAAQhSAAg6g6g");
	this.shape.setTransform(19.9,19.9);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(75));

	// Layer 9
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AiMCMQg6g6AAhSQAAhRA6g7QA7g6BRAAQBSAAA6A6QA7A7gBBRQABBSg7A6Qg6A7hSAAQhRAAg7g7g");
	this.shape_1.setTransform(66.5,18);

	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(75));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,-1.8,86.4,41.6);


(lib.Symbol1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#1B1814").ss(11,1,1).p("AkLhfQBDDEDRgGQBWgDBJgrQBKgtAahG");
	this.shape.setTransform(156,-52.6);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(75));

	// Layer 3
	this.instance = new lib.Symbol2_1();
	this.instance.parent = this;
	this.instance.setTransform(137.2,-49.4,2.017,1.042,0,46.1,51.9,5.4,7.5);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(12).to({_off:false},0).to({scaleY:2.02,guide:{path:[137.2,-49.3,137.5,-49,137.9,-48.7]}},4).wait(1).to({regX:4.3,regY:4.3,scaleX:2.02,scaleY:2.02,skewX:45.7,skewY:51.5,x:141.4,y:-54.8},0).wait(1).to({scaleX:2.01,scaleY:2.02,skewX:44.8,skewY:50.4,x:141.8,y:-54.4},0).wait(1).to({scaleX:2.01,scaleY:2.02,skewX:43.2,skewY:48.6,x:142.7,y:-53.7},0).wait(1).to({scaleX:2,scaleY:2.03,skewX:40.7,skewY:45.8,x:144.2,y:-52.8},0).wait(1).to({scaleX:2,scaleY:2.04,skewX:37.3,skewY:41.9,x:146.4,y:-51.7},0).wait(1).to({scaleX:1.98,scaleY:2.05,skewX:32.8,skewY:36.8,x:149.5,y:-50.7},0).wait(1).to({scaleX:1.97,scaleY:2.06,skewX:27.4,skewY:30.6,x:153.3,y:-50.1},0).wait(1).to({scaleX:1.96,scaleY:2.08,skewX:21.5,skewY:23.9,x:157.6},0).wait(1).to({scaleX:1.94,scaleY:2.09,skewX:16,skewY:17.6,x:161.1,y:-50.5},0).wait(1).to({scaleX:1.93,scaleY:2.1,skewX:11.4,skewY:12.4,x:164,y:-51.2},0).wait(1).to({scaleX:1.92,scaleY:2.11,skewX:8,skewY:8.6,x:166.1,y:-52},0).wait(1).to({scaleX:1.92,scaleY:2.11,skewX:5.8,skewY:6,x:167.5,y:-52.6},0).wait(1).to({scaleX:1.91,scaleY:2.12,skewX:4.5,skewY:4.6,x:168.2,y:-53},0).wait(1).to({regX:5.4,regY:7.5,scaleX:1.91,scaleY:2.12,rotation:4,skewX:0,skewY:0,x:170.2,y:-46.3},0).wait(7).to({regX:4.3,regY:4.3,scaleX:1.91,scaleY:2.12,rotation:0,skewX:4.6,skewY:4.7,x:168.2,y:-52.9},0).wait(1).to({scaleX:1.92,scaleY:2.11,skewX:6,skewY:6.2,x:167.3,y:-52.5},0).wait(1).to({scaleX:1.92,scaleY:2.11,skewX:8.3,skewY:8.8,x:165.8,y:-51.9},0).wait(1).to({scaleX:1.93,scaleY:2.1,skewX:11.5,skewY:12.6,x:163.6,y:-51.1},0).wait(1).to({scaleX:1.94,scaleY:2.09,skewX:15.7,skewY:17.4,x:160.6,y:-50.4},0).wait(1).to({scaleX:1.95,scaleY:2.08,skewX:20.7,skewY:23,x:157,y:-50},0).wait(1).to({scaleX:1.97,scaleY:2.06,skewX:26.1,skewY:29.1,x:153.3,y:-50.1},0).wait(1).to({scaleX:1.98,scaleY:2.05,skewX:31.2,skewY:35,x:150.1,y:-50.5},0).wait(1).to({scaleX:1.99,scaleY:2.04,skewX:35.9,skewY:40.2,x:147.3,y:-51.3},0).wait(1).to({scaleX:2,scaleY:2.03,skewX:39.6,skewY:44.5,x:145.1,y:-52.2},0).wait(1).to({scaleX:2.01,scaleY:2.03,skewX:42.5,skewY:47.7,x:143.3,y:-53.2},0).wait(1).to({scaleX:2.01,scaleY:2.02,skewX:44.4,skewY:50,x:142.1,y:-54.1},0).wait(1).to({scaleX:2.02,scaleY:2.02,skewX:45.6,skewY:51.3,x:141.5,y:-54.7},0).wait(1).to({regX:5.4,regY:7.5,scaleX:2.02,scaleY:2.02,skewX:46.1,skewY:51.9,x:137.9,y:-48.7},0).to({regX:5.5,regY:7.4,scaleY:1.23,x:135,y:-45.5},4).to({_off:true},1).wait(20));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(123.8,-67.6,64.6,30.1);


// stage content:
(lib.Main_vert = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(6.5,1,1).p("AAzAHQgahthLCO");
	this.shape.setTransform(144.5,488.6);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(70));

	// Слой_2
	this.instance = new lib.Symbol3();
	this.instance.parent = this;
	this.instance.setTransform(47.2,431.5,0.597,0.597,0,0,0,4,8.3);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(4).to({x:57.9},15,cjs.Ease.get(1)).wait(26).to({x:47.2},15,cjs.Ease.get(1)).wait(10));

	// Слой_3
	this.instance_1 = new lib.Symbol14();
	this.instance_1.parent = this;
	this.instance_1.setTransform(236.9,479.8,0.597,0.597,0,0,0,53.5,35.5);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(70));

	// Слой_4
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(6.5,1,1).p("ABfltQAGLmjDgM");
	this.shape_1.setTransform(160.3,452.9);

	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(70));

	// Слой_5
	this.instance_2 = new lib.Symbol15();
	this.instance_2.parent = this;
	this.instance_2.setTransform(189.1,455.9,0.597,0.597,0,0,0,293.3,214.2);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(70));

	// Слой_6
	this.instance_3 = new lib.Symbol4();
	this.instance_3.parent = this;
	this.instance_3.setTransform(145.5,485.7,0.597,0.597,0,0,0,8.6,7.7);

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(70));

	// Слой_7
	this.instance_4 = new lib.Symbol13();
	this.instance_4.parent = this;
	this.instance_4.setTransform(47.2,433.7,0.597,0.597,0,0,0,0.4,6.7);

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(4).to({x:58},15,cjs.Ease.get(1)).wait(26).to({x:47.2},15,cjs.Ease.get(1)).wait(10));

	// Слой_8
	this.instance_5 = new lib.Symbol12();
	this.instance_5.parent = this;
	this.instance_5.setTransform(108.3,404.7,0.516,0.516,-2.2,0,0,43.5,35);

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(70));

	// Слой_9
	this.instance_6 = new lib.Symbol1();
	this.instance_6.parent = this;
	this.instance_6.setTransform(110.5,418.6,0.492,0.492,-10.1,0,0,156.3,-52.3);

	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(70));

	// Слой_10
	this.instance_7 = new lib.Symbol5();
	this.instance_7.parent = this;
	this.instance_7.setTransform(232.7,405.1,0.597,0.597,0,0,0,42.6,28.4);

	this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(70));

	// Слой_11 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("EgIHAmGIgSvbIQhAAIASPbg");
	mask.setTransform(31.1,243.8);

	// Слой_12
	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(6.5,1,1).p("ADRhCQhZCJh7gEQh1gDhYh3");
	this.shape_2.setTransform(68.2,439.1);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(6.5,1,1).p("AjJhAQBbCFByAEQB4AFBPiW");
	this.shape_3.setTransform(68.9,439.7);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(6.5,1,1).p("AjDhIQBeCRBvAGQB2AGBEij");
	this.shape_4.setTransform(69.5,440.2);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(6.5,1,1).p("Ai9hQQBhCdBtAHQBzAHA6iu");
	this.shape_5.setTransform(70.1,440.8);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#000000").ss(6.5,1,1).p("Ai4hXQBjCoBsAIQBwAIAyi5");
	this.shape_6.setTransform(70.6,441.2);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#000000").ss(6.5,1,1).p("AizhdQBlCyBqAIQBvAJApjC");
	this.shape_7.setTransform(71.1,441.6);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(6.5,1,1).p("AiuhiQBnC7BoAJQBsAKAijL");
	this.shape_8.setTransform(71.6,441.9);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#000000").ss(6.5,1,1).p("AiqhmQBpDDBnAKQBqAKAbjS");
	this.shape_9.setTransform(72,442.2);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#000000").ss(6.5,1,1).p("AimhqQBqDKBlAKQBpALAWjZ");
	this.shape_10.setTransform(72.4,442.4);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#000000").ss(6.5,1,1).p("AikhtQBsDPBkAMQBoALAQjf");
	this.shape_11.setTransform(72.7,442.6);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#000000").ss(6.5,1,1).p("AihhwQBsDVBlALQBmAMAMjk");
	this.shape_12.setTransform(72.9,442.8);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(6.5,1,1).p("AifhzQBuDZBjANQBlAMAJjo");
	this.shape_13.setTransform(73.1,443);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#000000").ss(6.5,1,1).p("Aidh0QBuDcBjANQBkAMAGjr");
	this.shape_14.setTransform(73.3,443.1);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#000000").ss(6.5,1,1).p("Aich2QBuDfBjANQBkAMAEjt");
	this.shape_15.setTransform(73.4,443.2);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#000000").ss(6.5,1,1).p("Aibh2QBvDgBiANQBjANADjv");
	this.shape_16.setTransform(73.5,443.2);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#000000").ss(6.5,1,1).p("ACchsQgDDwhjgNQhigNhvjh");
	this.shape_17.setTransform(73.5,443.2);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#000000").ss(6.5,1,1).p("AiihvQBsDTBkALQBnAMAOji");
	this.shape_18.setTransform(72.8,442.8);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#000000").ss(6.5,1,1).p("AiohoQBpDGBmALQBqAKAYjW");
	this.shape_19.setTransform(72.2,442.3);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#000000").ss(6.5,1,1).p("AizhcQBkCwBqAJQBvAIAqjA");
	this.shape_20.setTransform(71.1,441.5);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#000000").ss(6.5,1,1).p("Ai4hWQBiCmBsAIQBxAHAyi2");
	this.shape_21.setTransform(70.6,441.2);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#000000").ss(6.5,1,1).p("AjBhLQBfCVBuAGQB1AGBBim");
	this.shape_22.setTransform(69.7,440.4);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#000000").ss(6.5,1,1).p("AjFhHQBeCPBwAFQB2AGBHig");
	this.shape_23.setTransform(69.3,440.1);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#000000").ss(6.5,1,1).p("AjIhCQBcCIBxAFQB4AFBMia");
	this.shape_24.setTransform(69,439.8);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f().s("#000000").ss(6.5,1,1).p("AjKg/QBbCDBxAEQB5AFBQiV");
	this.shape_25.setTransform(68.8,439.6);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f().s("#000000").ss(6.5,1,1).p("AjMg8QBaB/ByADQB6AFBTiR");
	this.shape_26.setTransform(68.6,439.4);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f().s("#000000").ss(6.5,1,1).p("AjOg6QBZB8B0ADQB6AEBWiN");
	this.shape_27.setTransform(68.4,439.3);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f().s("#000000").ss(6.5,1,1).p("AjPg5QBZB6B0ADQB6AEBYiL");
	this.shape_28.setTransform(68.3,439.2);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f().s("#000000").ss(6.5,1,1).p("AjQg4QBZB4B0ADQB7AEBZiK");
	this.shape_29.setTransform(68.2,439.1);

	var maskedShapeInstanceList = [this.shape_2,this.shape_3,this.shape_4,this.shape_5,this.shape_6,this.shape_7,this.shape_8,this.shape_9,this.shape_10,this.shape_11,this.shape_12,this.shape_13,this.shape_14,this.shape_15,this.shape_16,this.shape_17,this.shape_18,this.shape_19,this.shape_20,this.shape_21,this.shape_22,this.shape_23,this.shape_24,this.shape_25,this.shape_26,this.shape_27,this.shape_28,this.shape_29];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2}]}).to({state:[{t:this.shape_2}]},4).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_17}]},26).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_27}]},1).to({state:[{t:this.shape_28}]},1).to({state:[{t:this.shape_29}]},1).to({state:[{t:this.shape_2}]},1).wait(10));

	// Слой_15
	this.instance_8 = new lib.pack_bg_vert();
	this.instance_8.parent = this;
	this.instance_8.setTransform(-63,313);

	this.timeline.addTween(cjs.Tween.get(this.instance_8).wait(70));

	// Слой_16
	this.instance_9 = new lib.bg_vert();
	this.instance_9.parent = this;
	this.instance_9.setTransform(-187,464);

	this.timeline.addTween(cjs.Tween.get(this.instance_9).wait(70));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0.5,466,721,859);
// library properties:
lib.properties = {
	id: 'C2CB929D15996B4EBA93E2B3D3007698',
	width: 375,
	height: 812,
	fps: 30,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: [
		{src:"assets/images/basket_vert29.png", id:"basket"},
		{src:"assets/images/pack_bg_vert29.png", id:"pack_bg_vert"}
	],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['C2CB929D15996B4EBA93E2B3D3007698'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}



})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;

$(function () {
    var canvas, stage, exportRoot, anim_container, dom_overlay_container, fnStartAnimation;
    function init() {
        canvas = document.getElementById("canvas29-2");
        anim_container = document.getElementById("animation_container29-2");
        dom_overlay_container = document.getElementById("dom_overlay_container29-2");
        var comp=AdobeAn.getComposition("C2CB929D15996B4EBA93E2B3D3007698");
        var lib=comp.getLibrary();
        createjs.MotionGuidePlugin.install();
        var loader = new createjs.LoadQueue(false);
        loader.addEventListener("fileload", function(evt){handleFileLoad(evt,comp)});
        loader.addEventListener("complete", function(evt){handleComplete(evt,comp)});
        var lib=comp.getLibrary();
        loader.loadManifest(lib.properties.manifest);
    }
    function handleFileLoad(evt, comp) {
        var images=comp.getImages();
        if (evt && (evt.item.type == "image")) { images[evt.item.id] = evt.result; }
    }
    function handleComplete(evt,comp) {
        //This function is always called, irrespective of the content. You can use the variable "stage" after it is created in token create_stage.
        var lib=comp.getLibrary();
        var ss=comp.getSpriteSheet();
        var queue = evt.target;
        var ssMetadata = lib.ssMetadata;
        for(i=0; i<ssMetadata.length; i++) {
            ss[ssMetadata[i].name] = new createjs.SpriteSheet( {"images": [queue.getResult(ssMetadata[i].name)], "frames": ssMetadata[i].frames} )
        }
        exportRoot = new lib.Main_vert();
        stage = new lib.Stage(canvas);
        //Registers the "tick" event listener.
        fnStartAnimation = function() {
            stage.addChild(exportRoot);
            createjs.Ticker.setFPS(lib.properties.fps);
            createjs.Ticker.addEventListener("tick", stage);
        }
        //Code to support hidpi screens and responsive scaling.
        function makeResponsive(isResp, respDim, isScale, scaleType) {
            var lastW, lastH, lastS=1;
            window.addEventListener('resize', resizeCanvas);
            resizeCanvas();
            function resizeCanvas() {
                var w = lib.properties.width, h = lib.properties.height;
                var iw = window.innerWidth, ih=window.innerHeight;
                var pRatio = window.devicePixelRatio || 1, xRatio=iw/w, yRatio=ih/h, sRatio=1;
                if(isResp) {
                    if((respDim=='width'&&lastW==iw) || (respDim=='height'&&lastH==ih)) {
                        sRatio = lastS;
                    }
                    else if(!isScale) {
                        if(iw<w || ih<h)
                            sRatio = Math.min(xRatio, yRatio);
                    }
                    else if(scaleType==1) {
                        sRatio = Math.min(xRatio, yRatio);
                    }
                    else if(scaleType==2) {
                        sRatio = Math.max(xRatio, yRatio);
                    }
                }
                canvas.width = w*pRatio*sRatio;
                canvas.height = h*pRatio*sRatio;
                canvas.style.width = dom_overlay_container.style.width = anim_container.style.width =  w*sRatio+'px';
                canvas.style.height = anim_container.style.height = dom_overlay_container.style.height = h*sRatio+'px';
                stage.scaleX = pRatio*sRatio;
                stage.scaleY = pRatio*sRatio;
                lastW = iw; lastH = ih; lastS = sRatio;
                stage.tickOnUpdate = false;
                stage.update();
                stage.tickOnUpdate = true;
            }
        }
        makeResponsive(true,'both',true,2);
        AdobeAn.compositionLoaded(lib.properties.id);
        fnStartAnimation();
    }
    init();
});