$.fn.milkyButton = function() {
    return this.each(function(btn) {
        var $btn = $(this);
        var direction = true;
        var minY, waveEl, coordTop, isActive = false;


        $btn.prepend('<span class="wave-el"></span>');
        var timerFn = function() {
            if ((direction && coordTop > 0) || (
                !direction && coordTop < minY
                )) {
                coordTop = coordTop + (direction? -1: 1);
                waveEl.css('top', coordTop - 10);
                timerUp = requestAnimationFrame(timerFn, 20);
            } else if (!direction){
                $(this).removeClass('wave');
                isActive = false;
            } else {
                timerUp = requestAnimationFrame(timerFn, 20);
            }
        };

        $btn.hover(
            function() {
                direction = true;
                if (!isActive){
                    isActive = true;
                    $(this).addClass('wave');
                    waveEl = $(this).find('.wave-el');
                    minY = $(this).height() + 22;
                    coordTop = minY;

                    timerFn();
                }
            },
            function() {
                direction = false;
            }
        );
    });
};