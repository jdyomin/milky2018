(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [];


// symbols:



(lib.bg = function() {
	this.initialize(img.bg);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,1080,680);


(lib.pack = function() {
	this.initialize(img.pack);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,321,430);


(lib.par1 = function() {
	this.initialize(img.par1);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,489,459);


(lib.par2 = function() {
	this.initialize(img.par2);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,458,486);


(lib.shadow = function() {
	this.initialize(img.shadow);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,550,190);// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.Symbol10 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Symbol 7
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#141313").ss(6.5,1,1).p("AA2BwQihgqBPi1");
	this.shape.setTransform(5.5,11.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#141313").ss(6.5,1,1).p("AgZhuQhSCzChAq");
	this.shape_1.setTransform(5.5,11.3);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#141313").ss(6.5,1,1).p("AgXhtQhVCyChAp");
	this.shape_2.setTransform(5.6,11.4);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#141313").ss(6.5,1,1).p("AgVhtQhXCxChAq");
	this.shape_3.setTransform(5.6,11.4);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#141313").ss(6.5,1,1).p("AgThtQhaCxChAq");
	this.shape_4.setTransform(5.7,11.5);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#141313").ss(6.5,1,1).p("AgShsQhbCvChAq");
	this.shape_5.setTransform(5.7,11.5);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#141313").ss(6.5,1,1).p("AgRhsQhdCvChAq");
	this.shape_6.setTransform(5.8,11.6);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#141313").ss(6.5,1,1).p("AgPhrQhfCtChAq");
	this.shape_7.setTransform(5.8,11.6);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#141313").ss(6.5,1,1).p("AgOhrQhgCtChAq");
	this.shape_8.setTransform(5.8,11.6);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#141313").ss(6.5,1,1).p("AgNhqQhhCsChAq");
	this.shape_9.setTransform(5.8,11.7);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#141313").ss(6.5,1,1).p("AgMhqQhjCrChAq");
	this.shape_10.setTransform(5.9,11.7);
	this.shape_10._off = true;

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#141313").ss(6.5,1,1).p("AgLhqQhkCrChAq");
	this.shape_11.setTransform(5.9,11.7);
	this.shape_11._off = true;

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#141313").ss(6.5,1,1).p("AAyBrQihgqBkir");
	this.shape_12.setTransform(5.9,11.7);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#141313").ss(6.5,1,1).p("AgQhrQheCtChAq");
	this.shape_13.setTransform(5.8,11.6);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#141313").ss(6.5,1,1).p("AgRhsQhcCvChAq");
	this.shape_14.setTransform(5.7,11.5);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#141313").ss(6.5,1,1).p("AgThsQhaCvChAq");
	this.shape_15.setTransform(5.7,11.5);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#141313").ss(6.5,1,1).p("AgUhtQhZCxChAq");
	this.shape_16.setTransform(5.7,11.5);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#141313").ss(6.5,1,1).p("AgWhtQhWCyChAp");
	this.shape_17.setTransform(5.6,11.4);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#141313").ss(6.5,1,1).p("AgYhuQhUCzChAq");
	this.shape_18.setTransform(5.6,11.3);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#141313").ss(6.5,1,1).p("AgahvQhRC1ChAp");
	this.shape_19.setTransform(5.5,11.3);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#141313").ss(6.5,1,1).p("AgahuQhRCzChAq");
	this.shape_20.setTransform(5.5,11.3);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#141313").ss(6.5,1,1).p("AgWhtQhWCxChAq");
	this.shape_21.setTransform(5.6,11.4);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#141313").ss(6.5,1,1).p("AgVhtQhYCxChAq");
	this.shape_22.setTransform(5.7,11.4);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#141313").ss(6.5,1,1).p("AgQhsQheCvChAq");
	this.shape_23.setTransform(5.8,11.6);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#141313").ss(6.5,1,1).p("AgNhqQhiCrChAq");
	this.shape_24.setTransform(5.9,11.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_12}]},36).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},10).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_12}]},28).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape}]},1).wait(1));
	this.timeline.addTween(cjs.Tween.get(this.shape_10).wait(10).to({_off:false},0).wait(1).to({_off:true},1).wait(39).to({_off:false},0).to({_off:true},1).wait(33).to({_off:false},0).to({_off:true},1).wait(32).to({_off:false},0).to({_off:true},1).wait(12));
	this.timeline.addTween(cjs.Tween.get(this.shape_11).wait(12).to({_off:false},0).wait(1).to({_off:true},1).wait(72).to({_off:false},0).wait(2).to({_off:true},1).wait(42));

	// Symbol 6
	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f().s("#141313").ss(6.5,1,1).p("AAbBJQhDgYALgxQAJgoAzgg");
	this.shape_25.setTransform(9,15.4);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f().s("#141313").ss(6.5,1,1).p("AAaBJQhDgZALgwQAJgoA1gg");
	this.shape_26.setTransform(9.1,15.5);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f().s("#141313").ss(6.5,1,1).p("AAaBIQhEgYAMgwQAJgpA2ge");
	this.shape_27.setTransform(9.2,15.5);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f().s("#141313").ss(6.5,1,1).p("AAZBIQhEgZAMgvQAJgpA4ge");
	this.shape_28.setTransform(9.3,15.5);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f().s("#141313").ss(6.5,1,1).p("AAYBHQhDgYALgwQAJgpA6gd");
	this.shape_29.setTransform(9.3,15.6);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f().s("#141313").ss(6.5,1,1).p("AAYBHQhEgYAMgwQAJgpA6gc");
	this.shape_30.setTransform(9.4,15.6);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f().s("#141313").ss(6.5,1,1).p("AAXBHQhEgYAMgwQAJgpA8gc");
	this.shape_31.setTransform(9.5,15.6);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f().s("#141313").ss(6.5,1,1).p("AAWBHQhDgZALgvQAJgpA9gc");
	this.shape_32.setTransform(9.5,15.6);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f().s("#141313").ss(6.5,1,1).p("AAWBHQhDgZALgvQAJgpA+gb");
	this.shape_33.setTransform(9.5,15.7);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f().s("#141313").ss(6.5,1,1).p("AAWBHQhEgZAMgvQAJgpA+gb");
	this.shape_34.setTransform(9.6,15.7);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f().s("#141313").ss(6.5,1,1).p("AAVBGQhDgYALgwQAJgpA/ga");
	this.shape_35.setTransform(9.6,15.7);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f().s("#141313").ss(6.5,1,1).p("AAVBGQhDgYALgwQAJgpBAga");
	this.shape_36.setTransform(9.6,15.7);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f().s("#141313").ss(6.5,1,1).p("AAVBGQhEgYAMgwQAJgpBAga");
	this.shape_37.setTransform(9.7,15.7);
	this.shape_37._off = true;

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f().s("#141313").ss(6.5,1,1).p("AAXBHQhDgYALgwQAJgpA7gc");
	this.shape_38.setTransform(9.4,15.6);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f().s("#141313").ss(6.5,1,1).p("AAYBHQhEgYAMgwQAJgpA6gd");
	this.shape_39.setTransform(9.4,15.6);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f().s("#141313").ss(6.5,1,1).p("AAYBHQhDgYALgwQAJgpA5gd");
	this.shape_40.setTransform(9.3,15.6);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f().s("#141313").ss(6.5,1,1).p("AAZBIQhDgYALgwQAJgpA3ge");
	this.shape_41.setTransform(9.2,15.5);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f().s("#141313").ss(6.5,1,1).p("AAbBJQhEgZAMgwQAJgoA0gg");
	this.shape_42.setTransform(9.1,15.4);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f().s("#141313").ss(6.5,1,1).p("AAZBIQhDgYALgwQAJgpA4ge");
	this.shape_43.setTransform(9.2,15.5);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f().s("#141313").ss(6.5,1,1).p("AAWBGQhEgYAMgwQAJgpA+ga");
	this.shape_44.setTransform(9.6,15.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_25}]}).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_27}]},1).to({state:[{t:this.shape_28}]},1).to({state:[{t:this.shape_29}]},1).to({state:[{t:this.shape_30}]},1).to({state:[{t:this.shape_31}]},1).to({state:[{t:this.shape_32}]},1).to({state:[{t:this.shape_33}]},1).to({state:[{t:this.shape_34}]},1).to({state:[{t:this.shape_35}]},1).to({state:[{t:this.shape_36}]},1).to({state:[{t:this.shape_36}]},1).to({state:[{t:this.shape_37}]},1).to({state:[{t:this.shape_37}]},1).to({state:[{t:this.shape_37}]},36).to({state:[{t:this.shape_35}]},1).to({state:[{t:this.shape_34}]},1).to({state:[{t:this.shape_32}]},1).to({state:[{t:this.shape_31}]},1).to({state:[{t:this.shape_38}]},1).to({state:[{t:this.shape_39}]},1).to({state:[{t:this.shape_40}]},1).to({state:[{t:this.shape_28}]},1).to({state:[{t:this.shape_41}]},1).to({state:[{t:this.shape_27}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_42}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_25}]},10).to({state:[{t:this.shape_42}]},1).to({state:[{t:this.shape_27}]},1).to({state:[{t:this.shape_43}]},1).to({state:[{t:this.shape_28}]},1).to({state:[{t:this.shape_39}]},1).to({state:[{t:this.shape_38}]},1).to({state:[{t:this.shape_31}]},1).to({state:[{t:this.shape_32}]},1).to({state:[{t:this.shape_33}]},1).to({state:[{t:this.shape_34}]},1).to({state:[{t:this.shape_44}]},1).to({state:[{t:this.shape_35}]},1).to({state:[{t:this.shape_36}]},1).to({state:[{t:this.shape_37}]},1).to({state:[{t:this.shape_37}]},1).to({state:[{t:this.shape_37}]},1).to({state:[{t:this.shape_37}]},28).to({state:[{t:this.shape_35}]},1).to({state:[{t:this.shape_34}]},1).to({state:[{t:this.shape_32}]},1).to({state:[{t:this.shape_31}]},1).to({state:[{t:this.shape_38}]},1).to({state:[{t:this.shape_39}]},1).to({state:[{t:this.shape_40}]},1).to({state:[{t:this.shape_28}]},1).to({state:[{t:this.shape_41}]},1).to({state:[{t:this.shape_27}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_42}]},1).to({state:[{t:this.shape_25}]},1).wait(1));
	this.timeline.addTween(cjs.Tween.get(this.shape_37).wait(13).to({_off:false},0).wait(37).to({_off:true},1).wait(36).to({_off:false},0).wait(30).to({_off:true},1).wait(13));

	// Symbol 5
	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f().s("#141313").ss(6.5,1,1).p("AgnAxQBOgDABhe");
	this.shape_45.setTransform(15.8,17.8);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.f().s("#141313").ss(6.5,1,1).p("AgmAzQBOgDgChi");
	this.shape_46.setTransform(15.6,17.6);

	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.f().s("#141313").ss(6.5,1,1).p("AgkA1QBOgCgFhn");
	this.shape_47.setTransform(15.5,17.4);

	this.shape_48 = new cjs.Shape();
	this.shape_48.graphics.f().s("#141313").ss(6.5,1,1).p("AgjA3QBOgDgIhq");
	this.shape_48.setTransform(15.3,17.2);

	this.shape_49 = new cjs.Shape();
	this.shape_49.graphics.f().s("#141313").ss(6.5,1,1).p("AgiA4QBOgCgKhu");
	this.shape_49.setTransform(15.2,17.1);

	this.shape_50 = new cjs.Shape();
	this.shape_50.graphics.f().s("#141313").ss(6.5,1,1).p("AghA6QBOgCgNhx");
	this.shape_50.setTransform(15.1,16.9);

	this.shape_51 = new cjs.Shape();
	this.shape_51.graphics.f().s("#141313").ss(6.5,1,1).p("AggA7QBOgCgPhz");
	this.shape_51.setTransform(15.1,16.8);

	this.shape_52 = new cjs.Shape();
	this.shape_52.graphics.f().s("#141313").ss(6.5,1,1).p("AggA9QBOgDgRh2");
	this.shape_52.setTransform(15,16.6);

	this.shape_53 = new cjs.Shape();
	this.shape_53.graphics.f().s("#141313").ss(6.5,1,1).p("AgfA+QBOgDgTh4");
	this.shape_53.setTransform(15,16.5);

	this.shape_54 = new cjs.Shape();
	this.shape_54.graphics.f().s("#141313").ss(6.5,1,1).p("AgfA/QBOgDgUh6");
	this.shape_54.setTransform(14.9,16.4);

	this.shape_55 = new cjs.Shape();
	this.shape_55.graphics.f().s("#141313").ss(6.5,1,1).p("AgeA/QBOgCgVh8");
	this.shape_55.setTransform(14.9,16.4);

	this.shape_56 = new cjs.Shape();
	this.shape_56.graphics.f().s("#141313").ss(6.5,1,1).p("AgeBAQBOgDgWh8");
	this.shape_56.setTransform(14.8,16.3);

	this.shape_57 = new cjs.Shape();
	this.shape_57.graphics.f().s("#141313").ss(6.5,1,1).p("AgeBBQBOgDgWh+");
	this.shape_57.setTransform(14.8,16.3);

	this.shape_58 = new cjs.Shape();
	this.shape_58.graphics.f().s("#141313").ss(6.5,1,1).p("AgeBBQBOgDgXh+");
	this.shape_58.setTransform(14.8,16.2);
	this.shape_58._off = true;

	this.shape_59 = new cjs.Shape();
	this.shape_59.graphics.f().s("#141313").ss(6.5,1,1).p("AgfA+QBOgCgTh5");
	this.shape_59.setTransform(14.9,16.5);

	this.shape_60 = new cjs.Shape();
	this.shape_60.graphics.f().s("#141313").ss(6.5,1,1).p("AgfA9QBOgDgSh2");
	this.shape_60.setTransform(15,16.6);

	this.shape_61 = new cjs.Shape();
	this.shape_61.graphics.f().s("#141313").ss(6.5,1,1).p("AggA8QBOgDgQh0");
	this.shape_61.setTransform(15.1,16.7);

	this.shape_62 = new cjs.Shape();
	this.shape_62.graphics.f().s("#141313").ss(6.5,1,1).p("AghA6QBOgCgNhy");
	this.shape_62.setTransform(15.1,16.9);

	this.shape_63 = new cjs.Shape();
	this.shape_63.graphics.f().s("#141313").ss(6.5,1,1).p("AghA5QBOgCgMhv");
	this.shape_63.setTransform(15.2,17);

	this.shape_64 = new cjs.Shape();
	this.shape_64.graphics.f().s("#141313").ss(6.5,1,1).p("AgiA4QBOgCgKht");
	this.shape_64.setTransform(15.3,17.1);

	this.shape_65 = new cjs.Shape();
	this.shape_65.graphics.f().s("#141313").ss(6.5,1,1).p("AgjA3QBOgCgIhr");
	this.shape_65.setTransform(15.3,17.2);

	this.shape_66 = new cjs.Shape();
	this.shape_66.graphics.f().s("#141313").ss(6.5,1,1).p("AgkA2QBOgDgGho");
	this.shape_66.setTransform(15.4,17.3);

	this.shape_67 = new cjs.Shape();
	this.shape_67.graphics.f().s("#141313").ss(6.5,1,1).p("AglA1QBOgDgEhm");
	this.shape_67.setTransform(15.5,17.5);

	this.shape_68 = new cjs.Shape();
	this.shape_68.graphics.f().s("#141313").ss(6.5,1,1).p("AglAzQBOgCgDhj");
	this.shape_68.setTransform(15.6,17.6);

	this.shape_69 = new cjs.Shape();
	this.shape_69.graphics.f().s("#141313").ss(6.5,1,1).p("AgmAyQBOgCgBhh");
	this.shape_69.setTransform(15.7,17.7);

	this.shape_70 = new cjs.Shape();
	this.shape_70.graphics.f().s("#141313").ss(6.5,1,1).p("AgmAzQBOgDgBhi");
	this.shape_70.setTransform(15.6,17.6);

	this.shape_71 = new cjs.Shape();
	this.shape_71.graphics.f().s("#141313").ss(6.5,1,1).p("AgjA2QBOgCgHhp");
	this.shape_71.setTransform(15.4,17.3);

	this.shape_72 = new cjs.Shape();
	this.shape_72.graphics.f().s("#141313").ss(6.5,1,1).p("AgiA4QBOgDgJhs");
	this.shape_72.setTransform(15.3,17.1);

	this.shape_73 = new cjs.Shape();
	this.shape_73.graphics.f().s("#141313").ss(6.5,1,1).p("AggA8QBOgDgPh0");
	this.shape_73.setTransform(15.1,16.7);

	this.shape_74 = new cjs.Shape();
	this.shape_74.graphics.f().s("#141313").ss(6.5,1,1).p("AgfA/QBOgDgTh6");
	this.shape_74.setTransform(14.9,16.5);

	this.shape_75 = new cjs.Shape();
	this.shape_75.graphics.f().s("#141313").ss(6.5,1,1).p("AgeA/QBOgCgVh7");
	this.shape_75.setTransform(14.9,16.4);

	this.shape_76 = new cjs.Shape();
	this.shape_76.graphics.f().s("#141313").ss(6.5,1,1).p("AgeBAQBOgCgWh9");
	this.shape_76.setTransform(14.8,16.3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_45}]}).to({state:[{t:this.shape_46}]},1).to({state:[{t:this.shape_47}]},1).to({state:[{t:this.shape_48}]},1).to({state:[{t:this.shape_49}]},1).to({state:[{t:this.shape_50}]},1).to({state:[{t:this.shape_51}]},1).to({state:[{t:this.shape_52}]},1).to({state:[{t:this.shape_53}]},1).to({state:[{t:this.shape_54}]},1).to({state:[{t:this.shape_55}]},1).to({state:[{t:this.shape_56,p:{x:14.8}}]},1).to({state:[{t:this.shape_57}]},1).to({state:[{t:this.shape_58}]},1).to({state:[{t:this.shape_58}]},1).to({state:[{t:this.shape_58}]},36).to({state:[{t:this.shape_55}]},1).to({state:[{t:this.shape_59}]},1).to({state:[{t:this.shape_60}]},1).to({state:[{t:this.shape_61}]},1).to({state:[{t:this.shape_62}]},1).to({state:[{t:this.shape_63}]},1).to({state:[{t:this.shape_64}]},1).to({state:[{t:this.shape_65}]},1).to({state:[{t:this.shape_66}]},1).to({state:[{t:this.shape_67}]},1).to({state:[{t:this.shape_68}]},1).to({state:[{t:this.shape_69}]},1).to({state:[{t:this.shape_45}]},1).to({state:[{t:this.shape_45}]},10).to({state:[{t:this.shape_70}]},1).to({state:[{t:this.shape_67}]},1).to({state:[{t:this.shape_71}]},1).to({state:[{t:this.shape_72}]},1).to({state:[{t:this.shape_63}]},1).to({state:[{t:this.shape_62}]},1).to({state:[{t:this.shape_73}]},1).to({state:[{t:this.shape_52}]},1).to({state:[{t:this.shape_53}]},1).to({state:[{t:this.shape_74}]},1).to({state:[{t:this.shape_75}]},1).to({state:[{t:this.shape_56,p:{x:14.9}}]},1).to({state:[{t:this.shape_76}]},1).to({state:[{t:this.shape_57}]},1).to({state:[{t:this.shape_58}]},1).to({state:[{t:this.shape_58}]},1).to({state:[{t:this.shape_58}]},28).to({state:[{t:this.shape_55}]},1).to({state:[{t:this.shape_59}]},1).to({state:[{t:this.shape_60}]},1).to({state:[{t:this.shape_61}]},1).to({state:[{t:this.shape_62}]},1).to({state:[{t:this.shape_63}]},1).to({state:[{t:this.shape_64}]},1).to({state:[{t:this.shape_65}]},1).to({state:[{t:this.shape_66}]},1).to({state:[{t:this.shape_67}]},1).to({state:[{t:this.shape_68}]},1).to({state:[{t:this.shape_69}]},1).to({state:[{t:this.shape_45}]},1).wait(1));
	this.timeline.addTween(cjs.Tween.get(this.shape_58).wait(13).to({_off:false},0).wait(37).to({_off:true},1).wait(37).to({_off:false},0).wait(29).to({_off:true},1).wait(13));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-3.2,-3.2,26.3,29.2);


(lib.Symbol9 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Symbol 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#141313").ss(6.5,1,1).p("Ag9BlQCggQgxi5");
	this.shape.setTransform(13.5,10.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#141313").ss(6.5,1,1).p("Ag8BkQCggQgzi3");
	this.shape_1.setTransform(13.4,10.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#141313").ss(6.5,1,1).p("Ag7BkQCfgQg1i3");
	this.shape_2.setTransform(13.3,10.1);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#141313").ss(6.5,1,1).p("Ag7BjQCggQg3i1");
	this.shape_3.setTransform(13.3,10.2);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#141313").ss(6.5,1,1).p("Ag6BjQCfgQg4i1");
	this.shape_4.setTransform(13.2,10.2);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#141313").ss(6.5,1,1).p("Ag6BjQCggQg6i1");
	this.shape_5.setTransform(13.2,10.3);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#141313").ss(6.5,1,1).p("Ag6BiQCggQg8iz");
	this.shape_6.setTransform(13.2,10.3);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#141313").ss(6.5,1,1).p("Ag5BiQCfgQg9iz");
	this.shape_7.setTransform(13.1,10.3);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#141313").ss(6.5,1,1).p("Ag5BiQCggQg+iz");
	this.shape_8.setTransform(13.1,10.3);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#141313").ss(6.5,1,1).p("Ag5BiQCggQg/iz");
	this.shape_9.setTransform(13.1,10.3);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#141313").ss(6.5,1,1).p("Ag5BiQCggRhAiy");
	this.shape_10.setTransform(13.1,10.4);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#141313").ss(6.5,1,1).p("Ag4BiQCfgRhAiy");
	this.shape_11.setTransform(13,10.4);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#141313").ss(6.5,1,1).p("Ag4BhQCfgQhAix");
	this.shape_12.setTransform(13,10.4);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#141313").ss(6.5,1,1).p("Ag4BhQCfgQhBix");
	this.shape_13.setTransform(13,10.4);
	this.shape_13._off = true;

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#141313").ss(6.5,1,1).p("Ag5BiQCfgQg8iz");
	this.shape_14.setTransform(13.1,10.3);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#141313").ss(6.5,1,1).p("Ag6BjQCggQg7i1");
	this.shape_15.setTransform(13.2,10.3);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#141313").ss(6.5,1,1).p("Ag8BkQCggQg0i3");
	this.shape_16.setTransform(13.4,10.1);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#141313").ss(6.5,1,1).p("Ag8BkQCfgQgyi3");
	this.shape_17.setTransform(13.4,10.1);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#141313").ss(6.5,1,1).p("Ag8BkQCfgQgxi3");
	this.shape_18.setTransform(13.4,10.1);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#141313").ss(6.5,1,1).p("Ag8BlQCfgQgxi5");
	this.shape_19.setTransform(13.4,10.1);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#141313").ss(6.5,1,1).p("Ag7BkQCfgQg0i3");
	this.shape_20.setTransform(13.3,10.1);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#141313").ss(6.5,1,1).p("Ag7BkQCfgQg2i3");
	this.shape_21.setTransform(13.3,10.2);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#141313").ss(6.5,1,1).p("Ag7BjQCggQg4i1");
	this.shape_22.setTransform(13.3,10.2);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#141313").ss(6.5,1,1).p("Ag6BjQCfgQg5i1");
	this.shape_23.setTransform(13.2,10.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2,p:{y:10.1}}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_13}]},36).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_2,p:{y:10.2}}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},10).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_13}]},28).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_2,p:{y:10.2}}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).wait(1));
	this.timeline.addTween(cjs.Tween.get(this.shape).to({_off:true},1).wait(61).to({_off:false},0).wait(11).to({_off:true},1).wait(55).to({_off:false},0).wait(2));
	this.timeline.addTween(cjs.Tween.get(this.shape_13).wait(13).to({_off:false},0).wait(37).to({_off:true},1).wait(37).to({_off:false},0).wait(29).to({_off:true},1).wait(13));

	// Symbol 2
	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#141313").ss(6.5,1,1).p("AggBGQBEgOgEgwQgCgngsgm");
	this.shape_24.setTransform(9.7,13.3);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f().s("#141313").ss(6.5,1,1).p("AggBGQBEgOgEgwQgCgngtgm");
	this.shape_25.setTransform(9.7,13.3);
	this.shape_25._off = true;

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f().s("#141313").ss(6.5,1,1).p("AggBGQBEgNgEgwQgCgogtgm");
	this.shape_26.setTransform(9.7,13.3);
	this.shape_26._off = true;

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f().s("#141313").ss(6.5,1,1).p("AggBGQBEgNgEgwQgCgogugm");
	this.shape_27.setTransform(9.7,13.3);
	this.shape_27._off = true;

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f().s("#141313").ss(6.5,1,1).p("AggBHQBEgOgEgwQgCgogugn");
	this.shape_28.setTransform(9.7,13.3);
	this.shape_28._off = true;

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f().s("#141313").ss(6.5,1,1).p("AggBHQBEgOgEgwQgCgogvgn");
	this.shape_29.setTransform(9.7,13.3);
	this.shape_29._off = true;

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f().s("#141313").ss(6.5,1,1).p("AggBHQBEgOgEgwQgCgngvgo");
	this.shape_30.setTransform(9.7,13.2);
	this.shape_30._off = true;

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f().s("#141313").ss(6.5,1,1).p("AggBHQBEgOgEgwQgCgngwgo");
	this.shape_31.setTransform(9.7,13.2);
	this.shape_31._off = true;

	this.timeline.addTween(cjs.Tween.get(this.shape_24).to({_off:true},1).wait(59).to({_off:false},0).wait(13).to({_off:true},1).wait(53).to({_off:false},0).wait(4));
	this.timeline.addTween(cjs.Tween.get(this.shape_25).wait(1).to({_off:false},0).to({_off:true},1).wait(56).to({_off:false},0).wait(1).to({_off:true},1).wait(14).to({_off:false},0).to({_off:true},1).wait(50).to({_off:false},0).wait(1).to({_off:true},1).wait(4));
	this.timeline.addTween(cjs.Tween.get(this.shape_26).wait(2).to({_off:false},0).to({_off:true},1).wait(53).to({_off:false},0).wait(1).to({_off:true},1).wait(17).to({_off:false},0).wait(1).to({_off:true},1).wait(46).to({_off:false},0).wait(1).to({_off:true},1).wait(6));
	this.timeline.addTween(cjs.Tween.get(this.shape_27).wait(3).to({_off:false},0).wait(1).to({_off:true},1).wait(49).to({_off:false},0).wait(1).to({_off:true},1).wait(21).to({_off:false},0).to({_off:true},1).wait(43).to({_off:false},0).wait(1).to({_off:true},1).wait(8));
	this.timeline.addTween(cjs.Tween.get(this.shape_28).wait(5).to({_off:false},0).to({_off:true},1).wait(47).to({_off:false},0).to({_off:true},1).wait(24).to({_off:false},0).wait(1).to({_off:true},1).wait(40).to({_off:false},0).to({_off:true},1).wait(10));
	this.timeline.addTween(cjs.Tween.get(this.shape_29).wait(6).to({_off:false},0).wait(2).to({_off:true},1).wait(43).to({_off:false},0).to({_off:true},1).wait(27).to({_off:false},0).wait(2).to({_off:true},1).wait(36).to({_off:false},0).to({_off:true},1).wait(11));
	this.timeline.addTween(cjs.Tween.get(this.shape_30).wait(9).to({_off:false},0).wait(1).to({_off:true},1).wait(40).to({_off:false},0).to({_off:true},1).wait(31).to({_off:false},0).wait(1).to({_off:true},1).wait(33).to({_off:false},0).to({_off:true},1).wait(12));
	this.timeline.addTween(cjs.Tween.get(this.shape_31).wait(11).to({_off:false},0).wait(39).to({_off:true},1).wait(34).to({_off:false},0).wait(32).to({_off:true},1).wait(13));

	// Symbol 4
	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f().s("#141313").ss(6.5,1,1).p("AAgA1QhKgPANha");
	this.shape_32.setTransform(3.2,15.1);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f().s("#141313").ss(6.5,1,1).p("Agag1QgRBdBKAO");
	this.shape_33.setTransform(3.4,14.9);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f().s("#141313").ss(6.5,1,1).p("AgYg2QgVBfBKAO");
	this.shape_34.setTransform(3.5,14.8);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f().s("#141313").ss(6.5,1,1).p("AgVg3QgZBhBKAO");
	this.shape_35.setTransform(3.6,14.7);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f().s("#141313").ss(6.5,1,1).p("AgSg4QgdBiBKAP");
	this.shape_36.setTransform(3.7,14.7);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f().s("#141313").ss(6.5,1,1).p("AgQg4QggBjBKAO");
	this.shape_37.setTransform(3.8,14.6);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f().s("#141313").ss(6.5,1,1).p("AgOg5QgiBlBKAO");
	this.shape_38.setTransform(3.9,14.5);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f().s("#141313").ss(6.5,1,1).p("AgMg6QglBnBKAO");
	this.shape_39.setTransform(3.9,14.4);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f().s("#141313").ss(6.5,1,1).p("AgKg6QgnBnBKAO");
	this.shape_40.setTransform(4,14.4);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f().s("#141313").ss(6.5,1,1).p("AgJg7QgpBpBKAO");
	this.shape_41.setTransform(4,14.3);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f().s("#141313").ss(6.5,1,1).p("AgIg7QgqBpBKAO");
	this.shape_42.setTransform(4.1,14.3);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f().s("#141313").ss(6.5,1,1).p("AgHg7QgrBpBKAO");
	this.shape_43.setTransform(4.1,14.3);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f().s("#141313").ss(6.5,1,1).p("AgGg8QgsBqBKAP");
	this.shape_44.setTransform(4.1,14.3);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f().s("#141313").ss(6.5,1,1).p("AgGg8QgtBrBKAO");
	this.shape_45.setTransform(4.1,14.2);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.f().s("#141313").ss(6.5,1,1).p("AAXA9QhKgOAthr");
	this.shape_46.setTransform(4.1,14.2);

	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.f().s("#141313").ss(6.5,1,1).p("AgKg7QgoBoBKAO");
	this.shape_47.setTransform(4,14.4);

	this.shape_48 = new cjs.Shape();
	this.shape_48.graphics.f().s("#141313").ss(6.5,1,1).p("AgNg6QgkBmBKAP");
	this.shape_48.setTransform(3.9,14.5);

	this.shape_49 = new cjs.Shape();
	this.shape_49.graphics.f().s("#141313").ss(6.5,1,1).p("AgQg5QggBkBKAP");
	this.shape_49.setTransform(3.8,14.6);

	this.shape_50 = new cjs.Shape();
	this.shape_50.graphics.f().s("#141313").ss(6.5,1,1).p("AgTg4QgcBiBKAP");
	this.shape_50.setTransform(3.7,14.7);

	this.shape_51 = new cjs.Shape();
	this.shape_51.graphics.f().s("#141313").ss(6.5,1,1).p("AgVg3QgZBgBKAP");
	this.shape_51.setTransform(3.6,14.8);

	this.shape_52 = new cjs.Shape();
	this.shape_52.graphics.f().s("#141313").ss(6.5,1,1).p("AgXg2QgWBfBKAO");
	this.shape_52.setTransform(3.6,14.8);

	this.shape_53 = new cjs.Shape();
	this.shape_53.graphics.f().s("#141313").ss(6.5,1,1).p("AgZg1QgTBdBKAO");
	this.shape_53.setTransform(3.5,14.9);

	this.shape_54 = new cjs.Shape();
	this.shape_54.graphics.f().s("#141313").ss(6.5,1,1).p("Agag1QgSBdBKAO");
	this.shape_54.setTransform(3.4,14.9);

	this.shape_55 = new cjs.Shape();
	this.shape_55.graphics.f().s("#141313").ss(6.5,1,1).p("Agbg0QgQBbBKAO");
	this.shape_55.setTransform(3.3,15);

	this.shape_56 = new cjs.Shape();
	this.shape_56.graphics.f().s("#141313").ss(6.5,1,1).p("Agcg0QgOBbBKAO");
	this.shape_56.setTransform(3.3,15);

	this.shape_57 = new cjs.Shape();
	this.shape_57.graphics.f().s("#141313").ss(6.5,1,1).p("Agdg0QgNBbBKAO");
	this.shape_57.setTransform(3.3,15);

	this.shape_58 = new cjs.Shape();
	this.shape_58.graphics.f().s("#141313").ss(6.5,1,1).p("Agdg0QgNBaBKAP");
	this.shape_58.setTransform(3.2,15.1);

	this.shape_59 = new cjs.Shape();
	this.shape_59.graphics.f().s("#141313").ss(6.5,1,1).p("Agbg1QgQBcBKAP");
	this.shape_59.setTransform(3.4,15);

	this.shape_60 = new cjs.Shape();
	this.shape_60.graphics.f().s("#141313").ss(6.5,1,1).p("AgYg2QgUBeBKAO");
	this.shape_60.setTransform(3.5,14.9);

	this.shape_61 = new cjs.Shape();
	this.shape_61.graphics.f().s("#141313").ss(6.5,1,1).p("AgWg2QgXBfBKAO");
	this.shape_61.setTransform(3.6,14.8);

	this.shape_62 = new cjs.Shape();
	this.shape_62.graphics.f().s("#141313").ss(6.5,1,1).p("AgUg3QgaBhBKAO");
	this.shape_62.setTransform(3.7,14.7);

	this.shape_63 = new cjs.Shape();
	this.shape_63.graphics.f().s("#141313").ss(6.5,1,1).p("AgSg4QgdBjBKAO");
	this.shape_63.setTransform(3.8,14.6);

	this.shape_64 = new cjs.Shape();
	this.shape_64.graphics.f().s("#141313").ss(6.5,1,1).p("AgPg5QghBkBKAP");
	this.shape_64.setTransform(3.8,14.6);

	this.shape_65 = new cjs.Shape();
	this.shape_65.graphics.f().s("#141313").ss(6.5,1,1).p("AgJg7QgpBoBKAO");
	this.shape_65.setTransform(4,14.4);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_32}]}).to({state:[{t:this.shape_33}]},1).to({state:[{t:this.shape_34}]},1).to({state:[{t:this.shape_35}]},1).to({state:[{t:this.shape_36}]},1).to({state:[{t:this.shape_37}]},1).to({state:[{t:this.shape_38}]},1).to({state:[{t:this.shape_39}]},1).to({state:[{t:this.shape_40}]},1).to({state:[{t:this.shape_41}]},1).to({state:[{t:this.shape_42,p:{x:4.1}}]},1).to({state:[{t:this.shape_43}]},1).to({state:[{t:this.shape_44}]},1).to({state:[{t:this.shape_45}]},1).to({state:[{t:this.shape_46}]},1).to({state:[{t:this.shape_46}]},36).to({state:[{t:this.shape_47}]},1).to({state:[{t:this.shape_48}]},1).to({state:[{t:this.shape_49}]},1).to({state:[{t:this.shape_50}]},1).to({state:[{t:this.shape_51}]},1).to({state:[{t:this.shape_52}]},1).to({state:[{t:this.shape_53}]},1).to({state:[{t:this.shape_54}]},1).to({state:[{t:this.shape_55}]},1).to({state:[{t:this.shape_56}]},1).to({state:[{t:this.shape_57}]},1).to({state:[{t:this.shape_58}]},1).to({state:[{t:this.shape_32}]},1).to({state:[{t:this.shape_32}]},10).to({state:[{t:this.shape_59}]},1).to({state:[{t:this.shape_60}]},1).to({state:[{t:this.shape_61}]},1).to({state:[{t:this.shape_62}]},1).to({state:[{t:this.shape_63}]},1).to({state:[{t:this.shape_64}]},1).to({state:[{t:this.shape_38}]},1).to({state:[{t:this.shape_39}]},1).to({state:[{t:this.shape_40}]},1).to({state:[{t:this.shape_65}]},1).to({state:[{t:this.shape_42,p:{x:4}}]},1).to({state:[{t:this.shape_43}]},1).to({state:[{t:this.shape_44}]},1).to({state:[{t:this.shape_44}]},1).to({state:[{t:this.shape_45}]},1).to({state:[{t:this.shape_46}]},1).to({state:[{t:this.shape_46}]},28).to({state:[{t:this.shape_47}]},1).to({state:[{t:this.shape_48}]},1).to({state:[{t:this.shape_49}]},1).to({state:[{t:this.shape_50}]},1).to({state:[{t:this.shape_51}]},1).to({state:[{t:this.shape_52}]},1).to({state:[{t:this.shape_53}]},1).to({state:[{t:this.shape_54}]},1).to({state:[{t:this.shape_55}]},1).to({state:[{t:this.shape_56}]},1).to({state:[{t:this.shape_57}]},1).to({state:[{t:this.shape_58}]},1).to({state:[{t:this.shape_32}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-3.2,-3.2,26.2,26.8);


(lib.Symbol5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.par1();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol5, new cjs.Rectangle(0,0,489,459), null);


(lib.Symbol4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.par2();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol4, new cjs.Rectangle(0,0,458,486), null);


(lib.Symbol3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.shadow();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol3, new cjs.Rectangle(0,0,550,190), null);


(lib.Symbol2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#F51431").s().p("AAAAqQgRAAgNgMQgMgMAAgSQAAgQAMgNQANgNARAAIADAAQAPABALAMQANANAAAQQAAASgNAMQgLAMgPAAIgDAAg");
	this.shape.setTransform(4.3,4.3);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol2, new cjs.Rectangle(0,0,8.5,8.5), null);


(lib.Symbol2copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Symbol 7
	this.instance = new lib.Symbol10();
	this.instance.parent = this;
	this.instance.setTransform(16.6,85,1,1,0,0,0,16.6,23.4);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(9).to({rotation:31.7,x:25.7,y:78.1},13,cjs.Ease.get(1)).wait(28).to({rotation:0,x:16.6,y:85},13,cjs.Ease.get(1)).wait(10).to({rotation:31.7,x:25.7,y:78.1},16,cjs.Ease.get(1)).wait(28).to({rotation:0,x:16.6,y:85},13,cjs.Ease.get(1)).wait(1));

	// Layer 3
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#141313").ss(6.5,1,1).p("AEXBbQlkCWjJl2");
	this.shape.setTransform(44.5,98.3);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#141313").ss(6.5,1,1).p("AkPiJQC7F/FkiW");
	this.shape_1.setTransform(45.2,97.8);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#141313").ss(6.5,1,1).p("AkJiOQCvGJFkiW");
	this.shape_2.setTransform(45.8,97.3);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#141313").ss(6.5,1,1).p("AkDiSQCjGSFkiW");
	this.shape_3.setTransform(46.4,96.8);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#141313").ss(6.5,1,1).p("Aj+iVQCZGZFkiW");
	this.shape_4.setTransform(46.9,96.4);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#141313").ss(6.5,1,1).p("Aj5iYQCPGfFkiW");
	this.shape_5.setTransform(47.3,96.1);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#141313").ss(6.5,1,1).p("Aj1ibQCHGmFkiW");
	this.shape_6.setTransform(47.8,95.7);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#141313").ss(6.5,1,1).p("AjxidQCAGrFkiW");
	this.shape_7.setTransform(48.1,95.4);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#141313").ss(6.5,1,1).p("AjvigQB6GwFkiW");
	this.shape_8.setTransform(48.4,95.2);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#141313").ss(6.5,1,1).p("AjsihQB1GzFkiW");
	this.shape_9.setTransform(48.7,95);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#141313").ss(6.5,1,1).p("AjqijQBxG2FkiW");
	this.shape_10.setTransform(48.8,94.9);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#141313").ss(6.5,1,1).p("AjpikQBvG4FkiW");
	this.shape_11.setTransform(49,94.8);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#141313").ss(6.5,1,1).p("AjoikQBtG5FkiW");
	this.shape_12.setTransform(49.1,94.7);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#141313").ss(6.5,1,1).p("ADpB/QlkCWhtm5");
	this.shape_13.setTransform(49.1,94.7);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#141313").ss(6.5,1,1).p("Aj6iXQCSGdFkiW");
	this.shape_14.setTransform(47.2,96.2);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#141313").ss(6.5,1,1).p("AkAiUQCdGWFkiW");
	this.shape_15.setTransform(46.7,96.6);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#141313").ss(6.5,1,1).p("AkEiRQClGQFkiW");
	this.shape_16.setTransform(46.2,96.9);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#141313").ss(6.5,1,1).p("AkMiLQC1GEFkiW");
	this.shape_17.setTransform(45.5,97.5);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#141313").ss(6.5,1,1).p("AkSiIQDAF8FkiW");
	this.shape_18.setTransform(44.9,98);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#141313").ss(6.5,1,1).p("AkTiGQDDF5FkiW");
	this.shape_19.setTransform(44.7,98.1);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#141313").ss(6.5,1,1).p("AkViFQDHF3FkiW");
	this.shape_20.setTransform(44.6,98.2);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#141313").ss(6.5,1,1).p("AkWiFQDIF2FkiW");
	this.shape_21.setTransform(44.5,98.3);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#141313").ss(6.5,1,1).p("AkQiIQC9F9FkiW");
	this.shape_22.setTransform(45,97.9);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#141313").ss(6.5,1,1).p("AkLiMQCzGFFkiW");
	this.shape_23.setTransform(45.6,97.5);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#141313").ss(6.5,1,1).p("AkGiPQCpGMFkiW");
	this.shape_24.setTransform(46.1,97.1);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f().s("#141313").ss(6.5,1,1).p("AkCiSQCgGTFkiW");
	this.shape_25.setTransform(46.5,96.7);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f().s("#141313").ss(6.5,1,1).p("Aj+iVQCYGZFkiW");
	this.shape_26.setTransform(46.9,96.4);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f().s("#141313").ss(6.5,1,1).p("Aj6iYQCRGfFkiW");
	this.shape_27.setTransform(47.3,96.1);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f().s("#141313").ss(6.5,1,1).p("Aj2iaQCJGkFkiW");
	this.shape_28.setTransform(47.6,95.8);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f().s("#141313").ss(6.5,1,1).p("AjzicQCDGoFkiW");
	this.shape_29.setTransform(47.9,95.6);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f().s("#141313").ss(6.5,1,1).p("AjwieQB+GsFkiW");
	this.shape_30.setTransform(48.2,95.4);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f().s("#141313").ss(6.5,1,1).p("AjuigQB5GwFkiW");
	this.shape_31.setTransform(48.4,95.2);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f().s("#141313").ss(6.5,1,1).p("AjqiiQByG1FjiW");
	this.shape_32.setTransform(48.8,94.9);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f().s("#141313").ss(6.5,1,1).p("AjpijQBvG3FkiW");
	this.shape_33.setTransform(48.9,94.8);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f().s("#141313").ss(6.5,1,1).p("AjpikQBuG5FkiW");
	this.shape_34.setTransform(49,94.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},9).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9,p:{x:48.7}}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_13}]},28).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},10).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_27}]},1).to({state:[{t:this.shape_28}]},1).to({state:[{t:this.shape_29}]},1).to({state:[{t:this.shape_30}]},1).to({state:[{t:this.shape_31}]},1).to({state:[{t:this.shape_9,p:{x:48.6}}]},1).to({state:[{t:this.shape_32}]},1).to({state:[{t:this.shape_33}]},1).to({state:[{t:this.shape_34}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_13}]},28).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape}]},1).wait(1));
	this.timeline.addTween(cjs.Tween.get(this.shape).wait(9).to({_off:true},1).wait(53).to({_off:false},0).wait(10).to({_off:true},1).wait(56).to({_off:false},0).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-3.2,58.4,78.9,56.6);


(lib.Symbol2_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Symbol 1
	this.instance = new lib.Symbol9();
	this.instance.parent = this;
	this.instance.setTransform(202.2,83.7,1,1,0,0,0,6.9,22.9);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(9).to({rotation:-22.7,x:200,y:76.6},13,cjs.Ease.get(1)).wait(28).to({rotation:0,x:202.2,y:83.7},13,cjs.Ease.get(1)).wait(10).to({rotation:-22.7,x:200,y:76.6},16,cjs.Ease.get(1)).wait(28).to({rotation:0,x:202.2,y:83.7},13,cjs.Ease.get(1)).wait(1));

	// Layer 4
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#141313").ss(6.5,1,1).p("AjnBfQD+BmByg3QB6g7gjjz");
	this.shape_1.setTransform(179.6,98.4);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#141313").ss(6.5,1,1).p("AjmBmQD9BgByg4QB6g6glj1");
	this.shape_2.setTransform(179.5,97.7);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#141313").ss(6.5,1,1).p("AjlBtQD7BaB0g4QB5g7gmj3");
	this.shape_3.setTransform(179.4,97);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#141313").ss(6.5,1,1).p("AjkBzQD6BUB0g4QB5g7goj4");
	this.shape_4.setTransform(179.3,96.4);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#141313").ss(6.5,1,1).p("AjjB4QD5BQB0g5QB5g7gqj6");
	this.shape_5.setTransform(179.2,95.9);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#141313").ss(6.5,1,1).p("AjiB9QD4BLB1g5QB4g7grj7");
	this.shape_6.setTransform(179.1,95.4);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#141313").ss(6.5,1,1).p("AjiCBQD4BIB1g6QB4g7gsj8");
	this.shape_7.setTransform(179.1,95);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#141313").ss(6.5,1,1).p("AjhCFQD3BEB1g5QB3g7gsj+");
	this.shape_8.setTransform(179,94.6);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#141313").ss(6.5,1,1).p("AjhCIQD3BCB2g6QB2g7gtj/");
	this.shape_9.setTransform(179,94.3);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#141313").ss(6.5,1,1).p("AjgCKQD2BAB2g6QB3g7gukA");
	this.shape_10.setTransform(178.9,94.1);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#141313").ss(6.5,1,1).p("AjgCMQD2A+B2g6QB2g7gukA");
	this.shape_11.setTransform(178.9,93.9);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#141313").ss(6.5,1,1).p("AjgCOQD2A8B2g6QB2g7gukA");
	this.shape_12.setTransform(178.9,93.7);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#141313").ss(6.5,1,1).p("AjgCOQD2A8B2g6QB2g7gvkB");
	this.shape_13.setTransform(178.9,93.7);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#141313").ss(6.5,1,1).p("AjgCPQD2A7B2g6QB2g6gvkC");
	this.shape_14.setTransform(178.9,93.6);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#141313").ss(6.5,1,1).p("AjiCBQD4BIB1g6QB4g6gsj9");
	this.shape_15.setTransform(179.1,95);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#141313").ss(6.5,1,1).p("AjjB7QD5BNB1g5QB4g7grj7");
	this.shape_16.setTransform(179.2,95.6);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#141313").ss(6.5,1,1).p("AjjB2QD5BSB1g5QB4g7gpj5");
	this.shape_17.setTransform(179.2,96.1);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#141313").ss(6.5,1,1).p("AjkBxQD6BWB0g4QB5g7goj4");
	this.shape_18.setTransform(179.3,96.6);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#141313").ss(6.5,1,1).p("AjlBtQD7BZB0g4QB5g6gnj3");
	this.shape_19.setTransform(179.4,97);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#141313").ss(6.5,1,1).p("AjlBpQD8BdBzg4QB5g6gmj2");
	this.shape_20.setTransform(179.4,97.4);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#141313").ss(6.5,1,1).p("AjmBjQD8BiBzg3QB6g7gkj0");
	this.shape_21.setTransform(179.5,98);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#141313").ss(6.5,1,1).p("AjnBhQD9BkBzg3QB6g7gkjz");
	this.shape_22.setTransform(179.6,98.2);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#141313").ss(6.5,1,1).p("AjnBgQD+BlByg3QB6g7gjjz");
	this.shape_23.setTransform(179.6,98.3);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#141313").ss(6.5,1,1).p("AjmBlQD9BhByg4QB6g7gkj0");
	this.shape_24.setTransform(179.5,97.8);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f().s("#141313").ss(6.5,1,1).p("AjlBqQD7BcBzg4QB6g6gmj2");
	this.shape_25.setTransform(179.4,97.3);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f().s("#141313").ss(6.5,1,1).p("AjkBvQD7BYBzg4QB5g7gnj4");
	this.shape_26.setTransform(179.3,96.8);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f().s("#141313").ss(6.5,1,1).p("AjkB0QD6BTB0g4QB5g7goj5");
	this.shape_27.setTransform(179.3,96.3);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f().s("#141313").ss(6.5,1,1).p("AjiB8QD4BMB1g5QB4g6gqj8");
	this.shape_28.setTransform(179.1,95.5);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f().s("#141313").ss(6.5,1,1).p("AjiCAQD4BJB1g6QB4g6grj9");
	this.shape_29.setTransform(179.1,95.1);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f().s("#141313").ss(6.5,1,1).p("AjhCDQD3BGB2g5QB3g7gsj9");
	this.shape_30.setTransform(179,94.8);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f().s("#141313").ss(6.5,1,1).p("AjhCGQD3BDB1g5QB3g7gsj+");
	this.shape_31.setTransform(179,94.5);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f().s("#141313").ss(6.5,1,1).p("AjgCKQD2BAB2g6QB3g7guj/");
	this.shape_32.setTransform(178.9,94.1);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f().s("#141313").ss(6.5,1,1).p("AjgCNQD2A9B2g6QB3g7gvkA");
	this.shape_33.setTransform(178.9,93.8);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f().s("#141313").ss(6.5,1,1).p("AjgCPQD2A7B2g6QB2g7gvkB");
	this.shape_34.setTransform(178.9,93.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1}]}).to({state:[{t:this.shape_1}]},9).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_14}]},28).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_1}]},10).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_27}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_28}]},1).to({state:[{t:this.shape_29}]},1).to({state:[{t:this.shape_30}]},1).to({state:[{t:this.shape_31}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_32}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_33}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_34}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_14}]},28).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_1}]},1).wait(1));
	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(9).to({_off:true},1).wait(52).to({_off:false},0).wait(11).to({_off:true},1).wait(55).to({_off:false},0).wait(2));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(153.2,57.6,65,60.3);


(lib.Symbol1copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#1B1814").ss(11.5,1,1).p("Ai3hoQgJBhA4A6QAzA1BNAAQBLAAA2gyQA8g2AEhV");
	this.shape.setTransform(187.5,-93.1);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(40));

	// Layer 2
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#1B1814").ss(11.5,1,1).p("Ai2hsQgJBkA4A9QAyA4BNAAQBKAAA3g1QA7g4AEhY");
	this.shape_1.setTransform(119.2,-94.8);

	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(40));

	// Layer 3
	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#1B1814").ss(11,1,1).p("AkLhfQBDDEDRgGQBWgDBJgrQBKgtAahG");
	this.shape_2.setTransform(156,-52.6);

	this.timeline.addTween(cjs.Tween.get(this.shape_2).wait(40));

	// Layer 5
	this.instance = new lib.Symbol2();
	this.instance.parent = this;
	this.instance.setTransform(137.9,-48.7,2.017,2.017,0,46.1,51.9,5.4,7.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1).to({regX:4.3,regY:4.3,scaleX:2.02,scaleY:2.02,skewX:45.7,skewY:51.5,x:141.4,y:-54.8},0).wait(1).to({scaleX:2.01,scaleY:2.02,skewX:44.8,skewY:50.4,x:141.8,y:-54.4},0).wait(1).to({scaleX:2.01,scaleY:2.02,skewX:43.2,skewY:48.6,x:142.7,y:-53.7},0).wait(1).to({scaleX:2,scaleY:2.03,skewX:40.7,skewY:45.8,x:144.2,y:-52.8},0).wait(1).to({scaleX:2,scaleY:2.04,skewX:37.3,skewY:41.9,x:146.4,y:-51.7},0).wait(1).to({scaleX:1.98,scaleY:2.05,skewX:32.8,skewY:36.8,x:149.5,y:-50.7},0).wait(1).to({scaleX:1.97,scaleY:2.06,skewX:27.4,skewY:30.6,x:153.3,y:-50.1},0).wait(1).to({scaleX:1.96,scaleY:2.08,skewX:21.5,skewY:23.9,x:157.6},0).wait(1).to({scaleX:1.94,scaleY:2.09,skewX:16,skewY:17.6,x:161.1,y:-50.5},0).wait(1).to({scaleX:1.93,scaleY:2.1,skewX:11.4,skewY:12.4,x:164,y:-51.2},0).wait(1).to({scaleX:1.92,scaleY:2.11,skewX:8,skewY:8.6,x:166.1,y:-52},0).wait(1).to({scaleX:1.92,scaleY:2.11,skewX:5.8,skewY:6,x:167.5,y:-52.6},0).wait(1).to({scaleX:1.91,scaleY:2.12,skewX:4.5,skewY:4.6,x:168.2,y:-53},0).wait(1).to({regX:5.4,regY:7.5,scaleX:1.91,scaleY:2.12,rotation:4,skewX:0,skewY:0,x:170.2,y:-46.3},0).wait(7).to({regX:4.3,regY:4.3,scaleX:1.91,scaleY:2.12,rotation:0,skewX:4.6,skewY:4.7,x:168.2,y:-52.9},0).wait(1).to({scaleX:1.92,scaleY:2.11,skewX:6,skewY:6.2,x:167.3,y:-52.5},0).wait(1).to({scaleX:1.92,scaleY:2.11,skewX:8.3,skewY:8.8,x:165.8,y:-51.9},0).wait(1).to({scaleX:1.93,scaleY:2.1,skewX:11.5,skewY:12.6,x:163.6,y:-51.1},0).wait(1).to({scaleX:1.94,scaleY:2.09,skewX:15.7,skewY:17.4,x:160.5,y:-50.4},0).wait(1).to({scaleX:1.95,scaleY:2.08,skewX:20.7,skewY:23,x:157,y:-50},0).wait(1).to({scaleX:1.97,scaleY:2.06,skewX:26.1,skewY:29.1,x:153.3,y:-50.1},0).wait(1).to({scaleX:1.98,scaleY:2.05,skewX:31.2,skewY:35,x:150.1,y:-50.5},0).wait(1).to({scaleX:1.99,scaleY:2.04,skewX:35.9,skewY:40.2,x:147.3,y:-51.3},0).wait(1).to({scaleX:2,scaleY:2.03,skewX:39.6,skewY:44.5,x:145.1,y:-52.3},0).wait(1).to({scaleX:2.01,scaleY:2.03,skewX:42.5,skewY:47.7,x:143.3,y:-53.2},0).wait(1).to({scaleX:2.01,scaleY:2.02,skewX:44.4,skewY:50,x:142.1,y:-54.1},0).wait(1).to({scaleX:2.02,scaleY:2.02,skewX:45.6,skewY:51.3,x:141.5,y:-54.7},0).wait(1).to({regX:5.4,regY:7.5,scaleX:2.02,scaleY:2.02,skewX:46.1,skewY:51.9,x:137.8,y:-48.7},0).wait(6));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(95,-111.5,116.8,74);


(lib.Symbol1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 7
	this.instance = new lib.Symbol1copy();
	this.instance.parent = this;
	this.instance.setTransform(47.8,251.8,1.068,1.068,0,0,0,80,54);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(120));

	// Layer 2
	this.instance_1 = new lib.Symbol2_1();
	this.instance_1.parent = this;
	this.instance_1.setTransform(131,210.1,2,2,0,0,0,104,112);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(120));

	// Layer 1
	this.instance_2 = new lib.pack();
	this.instance_2.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(120));

	// Layer 8
	this.instance_3 = new lib.Symbol5();
	this.instance_3.parent = this;
	this.instance_3.setTransform(136.5,145.5,1,1,0,0,0,244.5,229.5);

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(1).to({y:145.3},0).wait(1).to({y:145.2},0).wait(1).to({y:145},0).wait(1).to({y:144.8},0).wait(1).to({y:144.6},0).wait(1).to({y:144.4},0).wait(1).to({y:144.1},0).wait(1).to({y:143.9},0).wait(1).to({y:143.6},0).wait(1).to({y:143.3},0).wait(1).to({y:143},0).wait(1).to({y:142.7},0).wait(1).to({y:142.4},0).wait(1).to({y:142},0).wait(1).to({y:141.6},0).wait(1).to({y:141.2},0).wait(1).to({y:140.8},0).wait(1).to({y:140.3},0).wait(1).to({y:139.8},0).wait(1).to({y:139.3},0).wait(1).to({y:138.8},0).wait(1).to({y:138.3},0).wait(1).to({y:137.7},0).wait(1).to({y:137.1},0).wait(1).to({y:136.6},0).wait(1).to({y:136},0).wait(1).to({y:135.4},0).wait(1).to({y:134.8},0).wait(1).to({y:134.2},0).wait(1).to({y:133.7},0).wait(1).to({y:133.1},0).wait(1).to({y:132.6},0).wait(1).to({y:132.1},0).wait(1).to({y:131.6},0).wait(1).to({y:131.1},0).wait(1).to({y:130.7},0).wait(1).to({y:130.3},0).wait(1).to({y:129.9},0).wait(1).to({y:129.5},0).wait(1).to({y:129.2},0).wait(1).to({y:128.8},0).wait(1).to({y:128.5},0).wait(1).to({y:128.2},0).wait(1).to({y:128},0).wait(1).to({y:127.7},0).wait(1).to({y:127.5},0).wait(1).to({y:127.2},0).wait(1).to({y:127},0).wait(1).to({y:126.8},0).wait(1).to({y:126.6},0).wait(1).to({y:126.5},0).wait(1).to({y:126.3},0).wait(1).to({y:126.2},0).wait(1).to({y:126},0).wait(1).to({y:125.9},0).wait(1).to({y:125.8},0).wait(1).to({y:125.7},0).wait(1).to({y:125.6},0).wait(1).to({y:125.5},0).wait(1).to({y:125.7},0).wait(1).to({y:125.8},0).wait(1).to({y:126},0).wait(1).to({y:126.2},0).wait(1).to({y:126.4},0).wait(1).to({y:126.6},0).wait(1).to({y:126.8},0).wait(1).to({y:127.1},0).wait(1).to({y:127.3},0).wait(1).to({y:127.6},0).wait(1).to({y:127.9},0).wait(1).to({y:128.2},0).wait(1).to({y:128.6},0).wait(1).to({y:128.9},0).wait(1).to({y:129.3},0).wait(1).to({y:129.7},0).wait(1).to({y:130.1},0).wait(1).to({y:130.6},0).wait(1).to({y:131},0).wait(1).to({y:131.5},0).wait(1).to({y:132},0).wait(1).to({y:132.5},0).wait(1).to({y:133.1},0).wait(1).to({y:133.6},0).wait(1).to({y:134.2},0).wait(1).to({y:134.8},0).wait(1).to({y:135.3},0).wait(1).to({y:135.9},0).wait(1).to({y:136.5},0).wait(1).to({y:137},0).wait(1).to({y:137.6},0).wait(1).to({y:138.1},0).wait(1).to({y:138.6},0).wait(1).to({y:139.1},0).wait(1).to({y:139.6},0).wait(1).to({y:140},0).wait(1).to({y:140.5},0).wait(1).to({y:140.9},0).wait(1).to({y:141.3},0).wait(1).to({y:141.6},0).wait(1).to({y:141.9},0).wait(1).to({y:142.3},0).wait(1).to({y:142.6},0).wait(1).to({y:142.9},0).wait(1).to({y:143.1},0).wait(1).to({y:143.4},0).wait(1).to({y:143.6},0).wait(1).to({y:143.8},0).wait(1).to({y:144},0).wait(1).to({y:144.2},0).wait(1).to({y:144.4},0).wait(1).to({y:144.5},0).wait(1).to({y:144.7},0).wait(1).to({y:144.8},0).wait(1).to({y:145},0).wait(1).to({y:145.1},0).wait(1).to({y:145.2},0).wait(1).to({y:145.3},0).wait(1).to({y:145.4},0).wait(1).to({y:145.5},0).wait(1));

	// Layer 6
	this.instance_4 = new lib.Symbol2copy();
	this.instance_4.parent = this;
	this.instance_4.setTransform(131,210.1,2,2,0,0,0,104,112);

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(120));

	// Layer 9
	this.instance_5 = new lib.Symbol4();
	this.instance_5.parent = this;
	this.instance_5.setTransform(155,249,1,1,0,0,0,229,243);

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(1).to({y:249.2},0).wait(1).to({y:249.3},0).wait(1).to({y:249.5},0).wait(1).to({y:249.7},0).wait(1).to({y:249.9},0).wait(1).to({y:250.1},0).wait(1).to({y:250.4},0).wait(1).to({y:250.6},0).wait(1).to({y:250.9},0).wait(1).to({y:251.2},0).wait(1).to({y:251.5},0).wait(1).to({y:251.8},0).wait(1).to({y:252.1},0).wait(1).to({y:252.5},0).wait(1).to({y:252.9},0).wait(1).to({y:253.3},0).wait(1).to({y:253.7},0).wait(1).to({y:254.2},0).wait(1).to({y:254.7},0).wait(1).to({y:255.2},0).wait(1).to({y:255.7},0).wait(1).to({y:256.2},0).wait(1).to({y:256.8},0).wait(1).to({y:257.4},0).wait(1).to({y:257.9},0).wait(1).to({y:258.5},0).wait(1).to({y:259.1},0).wait(1).to({y:259.7},0).wait(1).to({y:260.3},0).wait(1).to({y:260.8},0).wait(1).to({y:261.4},0).wait(1).to({y:261.9},0).wait(1).to({y:262.4},0).wait(1).to({y:262.9},0).wait(1).to({y:263.4},0).wait(1).to({y:263.8},0).wait(1).to({y:264.2},0).wait(1).to({y:264.6},0).wait(1).to({y:265},0).wait(1).to({y:265.3},0).wait(1).to({y:265.7},0).wait(1).to({y:266},0).wait(1).to({y:266.3},0).wait(1).to({y:266.5},0).wait(1).to({y:266.8},0).wait(1).to({y:267},0).wait(1).to({y:267.3},0).wait(1).to({y:267.5},0).wait(1).to({y:267.7},0).wait(1).to({y:267.9},0).wait(1).to({y:268},0).wait(1).to({y:268.2},0).wait(1).to({y:268.3},0).wait(1).to({y:268.5},0).wait(1).to({y:268.6},0).wait(1).to({y:268.7},0).wait(1).to({y:268.8},0).wait(1).to({y:268.9},0).wait(1).to({y:269},0).wait(1).to({y:268.8},0).wait(1).to({y:268.7},0).wait(1).to({y:268.5},0).wait(1).to({y:268.3},0).wait(1).to({y:268.1},0).wait(1).to({y:267.9},0).wait(1).to({y:267.7},0).wait(1).to({y:267.4},0).wait(1).to({y:267.2},0).wait(1).to({y:266.9},0).wait(1).to({y:266.6},0).wait(1).to({y:266.3},0).wait(1).to({y:265.9},0).wait(1).to({y:265.6},0).wait(1).to({y:265.2},0).wait(1).to({y:264.8},0).wait(1).to({y:264.4},0).wait(1).to({y:263.9},0).wait(1).to({y:263.5},0).wait(1).to({y:263},0).wait(1).to({y:262.5},0).wait(1).to({y:262},0).wait(1).to({y:261.4},0).wait(1).to({y:260.9},0).wait(1).to({y:260.3},0).wait(1).to({y:259.7},0).wait(1).to({y:259.2},0).wait(1).to({y:258.6},0).wait(1).to({y:258},0).wait(1).to({y:257.5},0).wait(1).to({y:256.9},0).wait(1).to({y:256.4},0).wait(1).to({y:255.9},0).wait(1).to({y:255.4},0).wait(1).to({y:254.9},0).wait(1).to({y:254.5},0).wait(1).to({y:254},0).wait(1).to({y:253.6},0).wait(1).to({y:253.2},0).wait(1).to({y:252.9},0).wait(1).to({y:252.6},0).wait(1).to({y:252.2},0).wait(1).to({y:251.9},0).wait(1).to({y:251.6},0).wait(1).to({y:251.4},0).wait(1).to({y:251.1},0).wait(1).to({y:250.9},0).wait(1).to({y:250.7},0).wait(1).to({y:250.5},0).wait(1).to({y:250.3},0).wait(1).to({y:250.1},0).wait(1).to({y:250},0).wait(1).to({y:249.8},0).wait(1).to({y:249.7},0).wait(1).to({y:249.5},0).wait(1).to({y:249.4},0).wait(1).to({y:249.3},0).wait(1).to({y:249.2},0).wait(1).to({y:249.1},0).wait(1).to({y:249},0).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-108,-84,492,576.1);


// stage content:
(lib.Main_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 3
	this.instance = new lib.Symbol1();
	this.instance.parent = this;
	this.instance.setTransform(770.5,353,1,1,0,0,0,160.5,215);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1).to({regX:138,regY:204,x:748,y:342.1},0).wait(1).to({y:342.2},0).wait(1).to({y:342.3},0).wait(1).to({y:342.5},0).wait(1).to({y:342.7},0).wait(1).to({y:343},0).wait(1).to({y:343.4},0).wait(1).to({y:343.8},0).wait(1).to({y:344.3},0).wait(1).to({y:344.8},0).wait(1).to({y:345.4},0).wait(1).to({y:346},0).wait(1).to({y:346.6},0).wait(1).to({y:347.3},0).wait(1).to({y:347.9},0).wait(1).to({y:348.5},0).wait(1).to({y:349.1},0).wait(1).to({y:349.6},0).wait(1).to({y:350},0).wait(1).to({y:350.4},0).wait(1).to({y:350.8},0).wait(1).to({y:351.1},0).wait(1).to({y:351.3},0).wait(1).to({y:351.5},0).wait(1).to({y:351.7},0).wait(1).to({y:351.8},0).wait(1).to({y:351.9},0).wait(1).to({y:352},0).wait(1).to({regX:160.5,regY:215,x:770.5,y:363},0).wait(1).to({regX:138,regY:204,x:748,y:351.9},0).wait(1).to({y:351.8},0).wait(1).to({y:351.7},0).wait(1).to({y:351.5},0).wait(1).to({y:351.3},0).wait(1).to({y:351},0).wait(1).to({y:350.7},0).wait(1).to({y:350.3},0).wait(1).to({y:349.9},0).wait(1).to({y:349.4},0).wait(1).to({y:348.8},0).wait(1).to({y:348.3},0).wait(1).to({y:347.6},0).wait(1).to({y:347},0).wait(1).to({y:346.4},0).wait(1).to({y:345.8},0).wait(1).to({y:345.2},0).wait(1).to({y:344.7},0).wait(1).to({y:344.2},0).wait(1).to({y:343.8},0).wait(1).to({y:343.5},0).wait(1).to({y:343.2},0).wait(1).to({y:342.9},0).wait(1).to({y:342.7},0).wait(1).to({y:342.5},0).wait(1).to({y:342.3},0).wait(1).to({y:342.2},0).wait(1).to({y:342.1},0).wait(1).to({y:342},0).wait(1).to({regX:160.5,regY:215,x:770.5,y:353},0).wait(1));

	// Layer 4
	this.instance_1 = new lib.Symbol3();
	this.instance_1.parent = this;
	this.instance_1.setTransform(761.5,585.4,1.1,1.1,0,0,0,255,105);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(1).to({regX:275,regY:95,scaleX:1.1,scaleY:1.1,x:783.5,y:574.4},0).wait(1).to({scaleX:1.1,scaleY:1.1,x:783.4},0).wait(1).to({scaleX:1.1,scaleY:1.1},0).wait(1).to({scaleX:1.1,scaleY:1.1,x:783.3,y:574.5},0).wait(1).to({scaleX:1.09,scaleY:1.09},0).wait(1).to({scaleX:1.09,scaleY:1.09,x:783.2},0).wait(1).to({scaleX:1.09,scaleY:1.09},0).wait(1).to({scaleX:1.08,scaleY:1.08,x:783.1,y:574.6},0).wait(1).to({scaleX:1.08,scaleY:1.08,x:783},0).wait(1).to({scaleX:1.07,scaleY:1.07,x:782.9,y:574.7},0).wait(1).to({scaleX:1.07,scaleY:1.07,x:782.7},0).wait(1).to({scaleX:1.06,scaleY:1.06,x:782.6,y:574.8},0).wait(1).to({scaleX:1.05,scaleY:1.05,x:782.5,y:574.9},0).wait(1).to({scaleX:1.05,scaleY:1.05,x:782.4},0).wait(1).to({scaleX:1.04,scaleY:1.04,x:782.2,y:575},0).wait(1).to({scaleX:1.03,scaleY:1.03,y:575.1},0).wait(1).to({scaleX:1.03,scaleY:1.03,x:782},0).wait(1).to({scaleX:1.02,scaleY:1.02,y:575.2},0).wait(1).to({scaleX:1.02,scaleY:1.02,x:781.8},0).wait(1).to({scaleX:1.02,scaleY:1.02},0).wait(1).to({scaleX:1.01,scaleY:1.01,x:781.7,y:575.3},0).wait(1).to({scaleX:1.01,scaleY:1.01,x:781.6},0).wait(1).to({scaleX:1.01,scaleY:1.01},0).wait(1).to({scaleX:1,scaleY:1,x:781.5},0).wait(1).to({scaleX:1,scaleY:1},0).wait(1).to({scaleX:1,scaleY:1,y:575.4},0).wait(1).to({scaleX:1,scaleY:1},0).wait(1).to({scaleX:1,scaleY:1,x:781.4},0).wait(1).to({regX:255,regY:105,x:761.5,y:585.4},0).wait(1).to({regX:275,regY:95,scaleX:1,scaleY:1,x:781.5,y:575.4},0).wait(1).to({scaleX:1,scaleY:1},0).wait(1).to({scaleX:1,scaleY:1},0).wait(1).to({scaleX:1,scaleY:1},0).wait(1).to({scaleX:1.01,scaleY:1.01,x:781.6,y:575.3},0).wait(1).to({scaleX:1.01,scaleY:1.01},0).wait(1).to({scaleX:1.01,scaleY:1.01,x:781.7},0).wait(1).to({scaleX:1.02,scaleY:1.02,x:781.8},0).wait(1).to({scaleX:1.02,scaleY:1.02,x:781.9,y:575.2},0).wait(1).to({scaleX:1.03,scaleY:1.03,x:782,y:575.1},0).wait(1).to({scaleX:1.03,scaleY:1.03,x:782.1},0).wait(1).to({scaleX:1.04,scaleY:1.04,x:782.2,y:575},0).wait(1).to({scaleX:1.04,scaleY:1.04,x:782.3},0).wait(1).to({scaleX:1.05,scaleY:1.05,x:782.5,y:574.9},0).wait(1).to({scaleX:1.06,scaleY:1.06,x:782.6},0).wait(1).to({scaleX:1.06,scaleY:1.06,x:782.7,y:574.8},0).wait(1).to({scaleX:1.07,scaleY:1.07,x:782.8,y:574.7},0).wait(1).to({scaleX:1.07,scaleY:1.07,x:782.9},0).wait(1).to({scaleX:1.08,scaleY:1.08,x:783,y:574.6},0).wait(1).to({scaleX:1.08,scaleY:1.08,x:783.1},0).wait(1).to({scaleX:1.09,scaleY:1.09,y:574.5},0).wait(1).to({scaleX:1.09,scaleY:1.09,x:783.2},0).wait(1).to({scaleX:1.09,scaleY:1.09,x:783.3},0).wait(1).to({scaleX:1.09,scaleY:1.09,y:574.4},0).wait(1).to({scaleX:1.1,scaleY:1.1,x:783.4},0).wait(1).to({scaleX:1.1,scaleY:1.1},0).wait(1).to({scaleX:1.1,scaleY:1.1},0).wait(1).to({scaleX:1.1,scaleY:1.1},0).wait(1).to({scaleX:1.1,scaleY:1.1,x:783.5},0).wait(1).to({regX:255.1,regY:105,x:761.5,y:585.4},0).wait(1));

	// Layer 2
	this.instance_2 = new lib.bg();
	this.instance_2.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(60));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(540,340,1086,680);
// library properties:
lib.properties = {
	id: '273F45E4908ABC429A41BE56345D9809',
	width: 1080,
	height: 680,
	fps: 30,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: [
		{src:"assets/images/bg16.jpg", id:"bg"},
		{src:"assets/images/pack16.png", id:"pack"},
		{src:"assets/images/par116.png", id:"par1"},
		{src:"assets/images/par216.png", id:"par2"},
		{src:"assets/images/shadow16.png", id:"shadow"}
	],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['273F45E4908ABC429A41BE56345D9809'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}



})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;

$(function () {
    var canvas, stage, exportRoot, anim_container, dom_overlay_container, fnStartAnimation;
    function init() {
        canvas = document.getElementById("canvas16");
        anim_container = document.getElementById("animation_container16");
        dom_overlay_container = document.getElementById("dom_overlay_container16");
        var comp=AdobeAn.getComposition("273F45E4908ABC429A41BE56345D9809");
        var lib=comp.getLibrary();
        var loader = new createjs.LoadQueue(false);
        loader.addEventListener("fileload", function(evt){handleFileLoad(evt,comp)});
        loader.addEventListener("complete", function(evt){handleComplete(evt,comp)});
        var lib=comp.getLibrary();
        loader.loadManifest(lib.properties.manifest);
    }
    function handleFileLoad(evt, comp) {
        var images=comp.getImages();
        if (evt && (evt.item.type == "image")) { images[evt.item.id] = evt.result; }
    }
    function handleComplete(evt,comp) {
        //This function is always called, irrespective of the content. You can use the variable "stage" after it is created in token create_stage.
        var lib=comp.getLibrary();
        var ss=comp.getSpriteSheet();
        var queue = evt.target;
        var ssMetadata = lib.ssMetadata;
        for(i=0; i<ssMetadata.length; i++) {
            ss[ssMetadata[i].name] = new createjs.SpriteSheet( {"images": [queue.getResult(ssMetadata[i].name)], "frames": ssMetadata[i].frames} )
        }
        exportRoot = new lib.Main_1();
        stage = new lib.Stage(canvas);
        //Registers the "tick" event listener.
        fnStartAnimation = function() {
            stage.addChild(exportRoot);
            createjs.Ticker.setFPS(lib.properties.fps);
            createjs.Ticker.addEventListener("tick", stage);
        }
        //Code to support hidpi screens and responsive scaling.
        function makeResponsive(isResp, respDim, isScale, scaleType) {
            var lastW, lastH, lastS=1;
            window.addEventListener('resize', resizeCanvas);
            resizeCanvas();
            function resizeCanvas() {
                var w = lib.properties.width, h = lib.properties.height;
                var iw = window.innerWidth, ih=window.innerHeight;
                var pRatio = window.devicePixelRatio || 1, xRatio=iw/w, yRatio=ih/h, sRatio=1;
                if(isResp) {
                    if((respDim=='width'&&lastW==iw) || (respDim=='height'&&lastH==ih)) {
                        sRatio = lastS;
                    }
                    else if(!isScale) {
                        if(iw<w || ih<h)
                            sRatio = Math.min(xRatio, yRatio);
                    }
                    else if(scaleType==1) {
                        sRatio = Math.min(xRatio, yRatio);
                    }
                    else if(scaleType==2) {
                        sRatio = Math.max(xRatio, yRatio);
                    }
                }
                canvas.width = w*pRatio*sRatio;
                canvas.height = h*pRatio*sRatio;
                canvas.style.width = dom_overlay_container.style.width = anim_container.style.width =  w*sRatio+'px';
                canvas.style.height = anim_container.style.height = dom_overlay_container.style.height = h*sRatio+'px';
                stage.scaleX = pRatio*sRatio;
                stage.scaleY = pRatio*sRatio;
                lastW = iw; lastH = ih; lastS = sRatio;
                stage.tickOnUpdate = false;
                stage.update();
                stage.tickOnUpdate = true;
            }
        }
        makeResponsive(true,'both',true,2);
        AdobeAn.compositionLoaded(lib.properties.id);
        fnStartAnimation();
    }
    init();
});